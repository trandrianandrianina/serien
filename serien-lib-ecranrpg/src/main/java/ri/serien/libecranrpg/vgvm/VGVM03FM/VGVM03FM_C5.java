
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_C5 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] CLTNS_Value = { " ", "1", "2", "5", "6", "9" };
  private String[] CLFIL1_Value = { " ", "3", "4", };
  private Icon isoleil = null;
  private Icon inuage = null;
  
  final DefaultComboBoxModel model = new DefaultComboBoxModel(new String[] { "", "DN", "ND", "RF", "NC", });
  
  /**
   * Constructeur.
   */
  public VGVM03FM_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CLTNS.setValeurs(CLTNS_Value, null);
    CLFIL1.setValeurs(CLFIL1_Value, null);
    EBIN08.setValeursSelection("1", " ");
    
    
    OBJ_50.setIcon(lexique.chargerImage("images/avert.gif", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    isoleil = lexique.chargerImage("images/soleil.png", true);
    inuage = lexique.chargerImage("images/nuage.png", true);
    
    // Titre
    setTitle("Comptabilité / Divers");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_77_OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL14R@")).trim());
    OBJ_79_OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL13R@")).trim());
    CLDAT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLDAT2@")).trim());
    OBJ_40_OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDPL@")).trim());
    OBJ_33_OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean ind_Depassement = lexique.isTrue("(57) AND (N40) AND (N87)");
    boolean isModif = (!lexique.isTrue("53"));
    
    navig_valid.setVisible(isModif);
    
    CLTNS.setVisible(true);
    OBJ_64_OBJ_64.setVisible(interpreteurD.analyseExpression("@WDPL@").isEmpty());
    WEDEP.setVisible(ind_Depassement);
    OBJ_53_OBJ_53.setVisible(ind_Depassement);
    OBJ_50.setVisible(ind_Depassement);
    mess_1.setVisible(lexique.HostFieldGetData("WDPL").equalsIgnoreCase("X"));
    
    OBJ_40_OBJ_40.setVisible(lexique.isPresent("WDPL"));
    OBJ_90_OBJ_90.setEnabled(CLCEE.isVisible());
    OBJ_47_OBJ_47.setVisible(interpreteurD.analyseExpression("@WDPL@").isEmpty());
    CLPLF3.setEnabled(!lexique.isTrue("53"));
    CLVAE.forceVisibility();
    CLVAE.setEnabled(!lexique.isTrue("53"));
    CLPLF3.forceVisibility();
    OBJ_73_OBJ_73.setEnabled(CLATT.isVisible());
    
    OBJ_31_OBJ_31.setEnabled(CLACT.isVisible());
    OBJ_79_OBJ_79.setVisible(lexique.isPresent("ZPL13R"));
    OBJ_77_OBJ_77.setVisible(lexique.isPresent("ZPL14R"));
    OBJ_75_OBJ_75.setEnabled(lexique.isPresent("DDVX"));
    OBJ_85_OBJ_85.setVisible(CLNIK.isVisible());
    OBJ_43_OBJ_44.setVisible(CLDAT3.isVisible());
    OBJ_33_OBJ_33.setEnabled(lexique.isPresent("ACLIBR"));
    OBJ_92_OBJ_92.setVisible(true);
    label1.setVisible(lexique.isTrue("08"));
    CLDAT2.setVisible(lexique.isTrue("08"));
    
    CLNOT.setEnabled(isModif);
    String entree_CLNOT = lexique.HostFieldGetData("CLNOT").trim();
    if (entree_CLNOT.isEmpty() || entree_CLNOT.equalsIgnoreCase("DN") || entree_CLNOT.equalsIgnoreCase("ND")
        || entree_CLNOT.equalsIgnoreCase("RF") || entree_CLNOT.equalsIgnoreCase("NC")) {
      CLNOT.setSelectedIndex(model.getIndexOf(entree_CLNOT));
    }
    else {
      CLNOT.setSelectedItem(entree_CLNOT);
    }
    
    CLNOT.addActionListener(new ActionListener() {
      private int selectedIndex = -1;
      
      @Override
      public void actionPerformed(ActionEvent e) {
        int index = CLNOT.getSelectedIndex();
        if (index >= 0) {
          selectedIndex = index;
        }
        else if ("comboBoxEdited".equals(e.getActionCommand())) {
          Object newValue = model.getSelectedItem();
          model.removeElementAt(selectedIndex);
          model.addElement(newValue);
          CLNOT.setSelectedItem(newValue);
          selectedIndex = model.getIndexOf(newValue);
        }
      }
    });
    
    WEDEP.setForeground(Color.RED);
    
    if (lexique.HostFieldGetData("CLTNS").trim().isEmpty()) {
      OBJ_71.setIcon(isoleil);
    }
    else {
      OBJ_71.setIcon(inuage);
    }
    
    // Le bouton du menu à droite "Historique plafonds" est rendu non visible en attendant que le programme RPG soit refait
    riSousMenu7.setVisible(false);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    lexique.HostFieldPutData("CLNOT", 0, CLNOT.getSelectedItem().toString());
    if (CLNOT.getSelectedIndex() >= 0) {
      if ((model.getElementAt(CLNOT.getSelectedIndex()).toString().equalsIgnoreCase(""))
          || (model.getElementAt(CLNOT.getSelectedIndex()).toString().equalsIgnoreCase("DN"))
          || (model.getElementAt(CLNOT.getSelectedIndex()).toString().equalsIgnoreCase("ND"))
          || (model.getElementAt(CLNOT.getSelectedIndex()).toString().equalsIgnoreCase("RF"))
          || (model.getElementAt(CLNOT.getSelectedIndex()).toString().equalsIgnoreCase("NC"))) {
        lexique.HostFieldPutData("CLNOT", 0, model.getElementAt(CLNOT.getSelectedIndex()).toString());
      }
    }
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // ScriptCall("G_PARAM")
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    CLTNS = new XRiComboBox();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_85_OBJ_85 = new JLabel();
    CLATT = new XRiTextField();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_71 = new JLabel();
    CLVAE = new XRiTextField();
    CLSRN = new XRiTextField();
    OBJ_90_OBJ_90 = new JLabel();
    ZPC13 = new XRiTextField();
    CLSRT = new XRiTextField();
    CLCEE = new XRiTextField();
    OBJ_83_OBJ_83 = new JLabel();
    CLNIK = new XRiTextField();
    CLNIP = new XRiTextField();
    OBJ_82_OBJ_82 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    DDVX = new XRiCalendrier();
    ZPC14X = new XRiCalendrier();
    label1 = new JLabel();
    CLDAT2 = new RiZoneSortie();
    panel3 = new JPanel();
    OBJ_62_OBJ_62 = new JLabel();
    CLIDAS = new XRiTextField();
    OBJ_66_OBJ_66 = new JLabel();
    CLPLF3 = new XRiTextField();
    OBJ_58_OBJ_58 = new JLabel();
    CLCAS = new XRiTextField();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_64_OBJ_64 = new JLabel();
    CLDP3X = new XRiCalendrier();
    CLDV3X = new XRiCalendrier();
    OBJ_41_OBJ_41 = new JLabel();
    CLNOT = new JComboBox();
    panel2 = new JPanel();
    mess_1 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_50 = new JLabel();
    BCLPF2 = new XRiTextField();
    OBJ_47_OBJ_47 = new JLabel();
    WEDEP = new XRiTextField();
    CLPLF = new XRiTextField();
    CLPLF2 = new XRiTextField();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    CLDPLX = new XRiCalendrier();
    CLDAT3 = new XRiTextField();
    OBJ_43_OBJ_44 = new JLabel();
    OBJ_75_OBJ_76 = new JLabel();
    CLFIL1 = new XRiComboBox();
    EBIN08 = new XRiCheckBox();
    panel1 = new JPanel();
    OBJ_33_OBJ_33 = new RiZoneSortie();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    CLNCG = new XRiTextField();
    CLNCA = new XRiTextField();
    CLACT = new XRiTextField();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_22 = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(925, 610));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 280));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Encours comptable");
              riSousMenu_bt6.setToolTipText("Encours comptable");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique plafonds");
              riSousMenu_bt7.setToolTipText("Historique plafonds (F17)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);
          
          // ---- CLTNS ----
          CLTNS.setModel(new DefaultComboBoxModel(new String[] { "Client actif", "Alerte en rouge", "Client interdit",
              "Acompte obligatoire", "Paiement \u00e0 la commande", "Client d\u00e9sactiv\u00e9" }));
          CLTNS.setToolTipText("Restrictions d'utilisation de ce client");
          CLTNS.setComponentPopupMenu(BTDA);
          CLTNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLTNS.setName("CLTNS");
          panel4.add(CLTNS);
          CLTNS.setBounds(390, 16, 210, CLTNS.getPreferredSize().height);
          
          // ---- OBJ_92_OBJ_92 ----
          OBJ_92_OBJ_92.setText("Montant autoris\u00e9 pour assimil\u00e9 export");
          OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
          panel4.add(OBJ_92_OBJ_92);
          OBJ_92_OBJ_92.setBounds(15, 174, 252, 20);
          
          // ---- OBJ_85_OBJ_85 ----
          OBJ_85_OBJ_85.setText("Num\u00e9ro d'indentification UE");
          OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
          panel4.add(OBJ_85_OBJ_85);
          OBJ_85_OBJ_85.setBounds(15, 144, 252, 20);
          
          // ---- CLATT ----
          CLATT.setComponentPopupMenu(BTDA);
          CLATT.setName("CLATT");
          panel4.add(CLATT);
          CLATT.setBounds(145, 15, 190, CLATT.getPreferredSize().height);
          
          // ---- OBJ_75_OBJ_75 ----
          OBJ_75_OBJ_75.setText("Derni\u00e8re visite le");
          OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
          panel4.add(OBJ_75_OBJ_75);
          OBJ_75_OBJ_75.setBounds(18, 49, 120, 20);
          
          // ---- OBJ_77_OBJ_77 ----
          OBJ_77_OBJ_77.setText("@ZPL14R@");
          OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
          panel4.add(OBJ_77_OBJ_77);
          OBJ_77_OBJ_77.setBounds(18, 79, 120, 20);
          
          // ---- OBJ_79_OBJ_79 ----
          OBJ_79_OBJ_79.setText("@ZPL13R@");
          OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");
          panel4.add(OBJ_79_OBJ_79);
          OBJ_79_OBJ_79.setBounds(18, 109, 120, 20);
          
          // ---- OBJ_73_OBJ_73 ----
          OBJ_73_OBJ_73.setText("Message d'alerte");
          OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");
          panel4.add(OBJ_73_OBJ_73);
          OBJ_73_OBJ_73.setBounds(20, 19, 120, 20);
          
          // ---- OBJ_71 ----
          OBJ_71.setIcon(null);
          OBJ_71.setName("OBJ_71");
          panel4.add(OBJ_71);
          OBJ_71.setBounds(340, 5, 45, 45);
          
          // ---- CLVAE ----
          CLVAE.setComponentPopupMenu(BTDA);
          CLVAE.setName("CLVAE");
          panel4.add(CLVAE);
          CLVAE.setBounds(270, 170, 76, CLVAE.getPreferredSize().height);
          
          // ---- CLSRN ----
          CLSRN.setToolTipText("N\u00b0 Siren");
          CLSRN.setName("CLSRN");
          panel4.add(CLSRN);
          CLSRN.setBounds(355, 140, 90, CLSRN.getPreferredSize().height);
          
          // ---- OBJ_90_OBJ_90 ----
          OBJ_90_OBJ_90.setText("Pays U.E.");
          OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
          panel4.add(OBJ_90_OBJ_90);
          OBJ_90_OBJ_90.setBounds(540, 144, 70, 20);
          
          // ---- ZPC13 ----
          ZPC13.setComponentPopupMenu(BTDA);
          ZPC13.setName("ZPC13");
          panel4.add(ZPC13);
          ZPC13.setBounds(145, 105, 67, ZPC13.getPreferredSize().height);
          
          // ---- CLSRT ----
          CLSRT.setToolTipText("Dernier n\u00b0 du code siret");
          CLSRT.setName("CLSRT");
          panel4.add(CLSRT);
          CLSRT.setBounds(455, 140, 60, CLSRT.getPreferredSize().height);
          
          // ---- CLCEE ----
          CLCEE.setComponentPopupMenu(BTDA);
          CLCEE.setName("CLCEE");
          panel4.add(CLCEE);
          CLCEE.setBounds(610, 140, 40, CLCEE.getPreferredSize().height);
          
          // ---- OBJ_83_OBJ_83 ----
          OBJ_83_OBJ_83.setText("Siret");
          OBJ_83_OBJ_83.setName("OBJ_83_OBJ_83");
          panel4.add(OBJ_83_OBJ_83);
          OBJ_83_OBJ_83.setBounds(360, 144, 39, 20);
          
          // ---- CLNIK ----
          CLNIK.setToolTipText("Cl\u00e9");
          CLNIK.setName("CLNIK");
          panel4.add(CLNIK);
          CLNIK.setBounds(310, 140, 30, CLNIK.getPreferredSize().height);
          
          // ---- CLNIP ----
          CLNIP.setToolTipText("Code  pays");
          CLNIP.setName("CLNIP");
          panel4.add(CLNIP);
          CLNIP.setBounds(270, 140, 30, CLNIP.getPreferredSize().height);
          
          // ---- OBJ_82_OBJ_82 ----
          OBJ_82_OBJ_82.setText("Cl\u00e9");
          OBJ_82_OBJ_82.setName("OBJ_82_OBJ_82");
          panel4.add(OBJ_82_OBJ_82);
          OBJ_82_OBJ_82.setBounds(315, 144, 30, 20);
          
          // ---- OBJ_81_OBJ_81 ----
          OBJ_81_OBJ_81.setText("CP");
          OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");
          panel4.add(OBJ_81_OBJ_81);
          OBJ_81_OBJ_81.setBounds(275, 144, 25, 20);
          
          // ---- DDVX ----
          DDVX.setName("DDVX");
          panel4.add(DDVX);
          DDVX.setBounds(145, 45, 105, DDVX.getPreferredSize().height);
          
          // ---- ZPC14X ----
          ZPC14X.setName("ZPC14X");
          panel4.add(ZPC14X);
          ZPC14X.setBounds(145, 75, 105, ZPC14X.getPreferredSize().height);
          
          // ---- label1 ----
          label1.setText("Identification export/import");
          label1.setHorizontalTextPosition(SwingConstants.RIGHT);
          label1.setHorizontalAlignment(SwingConstants.RIGHT);
          label1.setName("label1");
          panel4.add(label1);
          label1.setBounds(400, 176, 160, 22);
          
          // ---- CLDAT2 ----
          CLDAT2.setText("@CLDAT2@");
          CLDAT2.setName("CLDAT2");
          panel4.add(CLDAT2);
          CLDAT2.setBounds(575, 175, 75, CLDAT2.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(10, 380, 730, 220);
        
        // ======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Assurance"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- OBJ_62_OBJ_62 ----
          OBJ_62_OBJ_62.setText("Plafond assurance");
          OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
          panel3.add(OBJ_62_OBJ_62);
          OBJ_62_OBJ_62.setBounds(20, 64, 120, 20);
          
          // ---- CLIDAS ----
          CLIDAS.setName("CLIDAS");
          panel3.add(CLIDAS);
          CLIDAS.setBounds(145, 30, 160, CLIDAS.getPreferredSize().height);
          
          // ---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("Date de validit\u00e9");
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
          panel3.add(OBJ_66_OBJ_66);
          OBJ_66_OBJ_66.setBounds(20, 95, 97, 20);
          
          // ---- CLPLF3 ----
          CLPLF3.setName("CLPLF3");
          panel3.add(CLPLF3);
          CLPLF3.setBounds(145, 60, 92, CLPLF3.getPreferredSize().height);
          
          // ---- OBJ_58_OBJ_58 ----
          OBJ_58_OBJ_58.setText("Identifiant");
          OBJ_58_OBJ_58.setName("OBJ_58_OBJ_58");
          panel3.add(OBJ_58_OBJ_58);
          OBJ_58_OBJ_58.setBounds(20, 34, 85, 20);
          
          // ---- CLCAS ----
          CLCAS.setComponentPopupMenu(BTD);
          CLCAS.setName("CLCAS");
          panel3.add(CLCAS);
          CLCAS.setBounds(390, 30, 60, CLCAS.getPreferredSize().height);
          
          // ---- OBJ_60_OBJ_60 ----
          OBJ_60_OBJ_60.setText("Code");
          OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
          panel3.add(OBJ_60_OBJ_60);
          OBJ_60_OBJ_60.setBounds(340, 34, 50, 20);
          
          // ---- OBJ_64_OBJ_64 ----
          OBJ_64_OBJ_64.setText("au");
          OBJ_64_OBJ_64.setName("OBJ_64_OBJ_64");
          panel3.add(OBJ_64_OBJ_64);
          OBJ_64_OBJ_64.setBounds(340, 64, 35, 20);
          
          // ---- CLDP3X ----
          CLDP3X.setName("CLDP3X");
          panel3.add(CLDP3X);
          CLDP3X.setBounds(390, 60, 110, CLDP3X.getPreferredSize().height);
          
          // ---- CLDV3X ----
          CLDV3X.setName("CLDV3X");
          panel3.add(CLDV3X);
          CLDV3X.setBounds(145, 90, 105, CLDV3X.getPreferredSize().height);
          
          // ---- OBJ_41_OBJ_41 ----
          OBJ_41_OBJ_41.setText("Code assurance");
          OBJ_41_OBJ_41.setName("OBJ_41_OBJ_41");
          panel3.add(OBJ_41_OBJ_41);
          OBJ_41_OBJ_41.setBounds(485, 34, 105, 20);
          
          // ---- CLNOT ----
          CLNOT.setComponentPopupMenu(BTD);
          CLNOT.setModel(new DefaultComboBoxModel(
              new String[] { "  ", "D\u00e9nomm\u00e9", "Non d\u00e9nomm\u00e9", "Refus\u00e9", "Non d\u00e9clar\u00e9" }));
          CLNOT.setEditable(true);
          CLNOT.setName("CLNOT");
          panel3.add(CLNOT);
          CLNOT.setBounds(595, 32, 120, 24);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 240, 730, 135);
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Encours"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- mess_1 ----
          mess_1.setText("Plafond direction non retenu date d\u00e9pass\u00e9e");
          mess_1.setForeground(Color.red);
          mess_1.setName("mess_1");
          panel2.add(mess_1);
          mess_1.setBounds(435, 61, 263, 26);
          
          // ---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("D\u00e9passement (par rapport au plafond )");
          OBJ_53_OBJ_53.setForeground(Color.red);
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
          panel2.add(OBJ_53_OBJ_53);
          OBJ_53_OBJ_53.setBounds(435, 34, 220, 20);
          
          // ---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("Plafond exceptionnel");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          panel2.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(25, 64, 125, 20);
          
          // ---- OBJ_50 ----
          OBJ_50.setIcon(new ImageIcon("images/avert.gif"));
          OBJ_50.setName("OBJ_50");
          panel2.add(OBJ_50);
          OBJ_50.setBounds(670, 27, 45, 34);
          
          // ---- BCLPF2 ----
          BCLPF2.setName("BCLPF2");
          panel2.add(BCLPF2);
          BCLPF2.setBounds(145, 60, 84, BCLPF2.getPreferredSize().height);
          
          // ---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("(Jusqu'au)");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          panel2.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(250, 64, 70, 20);
          
          // ---- WEDEP ----
          WEDEP.setForeground(Color.red);
          WEDEP.setName("WEDEP");
          panel2.add(WEDEP);
          WEDEP.setBounds(320, 30, 84, WEDEP.getPreferredSize().height);
          
          // ---- CLPLF ----
          CLPLF.setName("CLPLF");
          panel2.add(CLPLF);
          CLPLF.setBounds(145, 30, 84, CLPLF.getPreferredSize().height);
          
          // ---- CLPLF2 ----
          CLPLF2.setName("CLPLF2");
          panel2.add(CLPLF2);
          CLPLF2.setBounds(145, 60, 85, CLPLF2.getPreferredSize().height);
          
          // ---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Plafond encours");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel2.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(25, 33, 115, 22);
          
          // ---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("@WDPL@");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
          panel2.add(OBJ_40_OBJ_40);
          OBJ_40_OBJ_40.setBounds(250, 33, 50, 23);
          
          // ---- CLDPLX ----
          CLDPLX.setName("CLDPLX");
          panel2.add(CLDPLX);
          CLDPLX.setBounds(320, 60, 105, CLDPLX.getPreferredSize().height);
          
          // ---- CLDAT3 ----
          CLDAT3.setForeground(Color.red);
          CLDAT3.setName("CLDAT3");
          panel2.add(CLDAT3);
          CLDAT3.setBounds(145, 90, 84, CLDAT3.getPreferredSize().height);
          
          // ---- OBJ_43_OBJ_44 ----
          OBJ_43_OBJ_44.setText("Maxi pour d\u00e9blocage");
          OBJ_43_OBJ_44.setName("OBJ_43_OBJ_44");
          panel2.add(OBJ_43_OBJ_44);
          OBJ_43_OBJ_44.setBounds(25, 93, 120, 22);
          
          // ---- OBJ_75_OBJ_76 ----
          OBJ_75_OBJ_76.setText("Blocage d\u00e9passement");
          OBJ_75_OBJ_76.setName("OBJ_75_OBJ_76");
          panel2.add(OBJ_75_OBJ_76);
          OBJ_75_OBJ_76.setBounds(250, 94, 145, 20);
          
          // ---- CLFIL1 ----
          CLFIL1.setModel(
              new DefaultComboBoxModel(new String[] { " ", "Livraison interdite si d\u00e9passement", "Attente si d\u00e9passement" }));
          CLFIL1.setToolTipText("Restrictions d'utilisation de ce client");
          CLFIL1.setComponentPopupMenu(BTDA);
          CLFIL1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CLFIL1.setName("CLFIL1");
          panel2.add(CLFIL1);
          CLFIL1.setBounds(434, 91, 270, CLFIL1.getPreferredSize().height);
          
          // ---- EBIN08 ----
          EBIN08.setText("Exclusion commandes de l'encours");
          EBIN08.setName("EBIN08");
          panel2.add(EBIN08);
          EBIN08.setBounds(23, 120, 247, 20);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 85, 730, 155);
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Comptabilisation"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("@ACLIBR@");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel1.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(445, 32, 270, OBJ_33_OBJ_33.getPreferredSize().height);
          
          // ---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("Num\u00e9ro de compte");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");
          panel1.add(OBJ_28_OBJ_28);
          OBJ_28_OBJ_28.setBounds(25, 34, 127, 20);
          
          // ---- OBJ_31_OBJ_31 ----
          OBJ_31_OBJ_31.setText("Affaire analytique");
          OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
          panel1.add(OBJ_31_OBJ_31);
          OBJ_31_OBJ_31.setBounds(290, 34, 105, 20);
          
          // ---- CLNCG ----
          CLNCG.setComponentPopupMenu(BTDA);
          CLNCG.setName("CLNCG");
          panel1.add(CLNCG);
          CLNCG.setBounds(145, 30, 60, CLNCG.getPreferredSize().height);
          
          // ---- CLNCA ----
          CLNCA.setComponentPopupMenu(BTDA);
          CLNCA.setName("CLNCA");
          panel1.add(CLNCA);
          CLNCA.setBounds(212, 30, 60, CLNCA.getPreferredSize().height);
          
          // ---- CLACT ----
          CLACT.setComponentPopupMenu(BTD);
          CLACT.setName("CLACT");
          panel1.add(CLACT);
          CLACT.setBounds(390, 30, 50, CLACT.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 730, 75);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTDA ========
    {
      BTDA.setName("BTDA");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }
    
    // ======== OBJ_22 ========
    {
      OBJ_22.setName("OBJ_22");
      
      // ---- OBJ_23 ----
      OBJ_23.setText("Param\u00e9trages");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      OBJ_22.add(OBJ_23);
      
      // ---- OBJ_24 ----
      OBJ_24.setText("Informations");
      OBJ_24.setName("OBJ_24");
      OBJ_22.add(OBJ_24);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);
      
      // ---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel4;
  private XRiComboBox CLTNS;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_85_OBJ_85;
  private XRiTextField CLATT;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_71;
  private XRiTextField CLVAE;
  private XRiTextField CLSRN;
  private JLabel OBJ_90_OBJ_90;
  private XRiTextField ZPC13;
  private XRiTextField CLSRT;
  private XRiTextField CLCEE;
  private JLabel OBJ_83_OBJ_83;
  private XRiTextField CLNIK;
  private XRiTextField CLNIP;
  private JLabel OBJ_82_OBJ_82;
  private JLabel OBJ_81_OBJ_81;
  private XRiCalendrier DDVX;
  private XRiCalendrier ZPC14X;
  private JLabel label1;
  private RiZoneSortie CLDAT2;
  private JPanel panel3;
  private JLabel OBJ_62_OBJ_62;
  private XRiTextField CLIDAS;
  private JLabel OBJ_66_OBJ_66;
  private XRiTextField CLPLF3;
  private JLabel OBJ_58_OBJ_58;
  private XRiTextField CLCAS;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_64_OBJ_64;
  private XRiCalendrier CLDP3X;
  private XRiCalendrier CLDV3X;
  private JLabel OBJ_41_OBJ_41;
  private JComboBox CLNOT;
  private JPanel panel2;
  private JLabel mess_1;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_50;
  private XRiTextField BCLPF2;
  private JLabel OBJ_47_OBJ_47;
  private XRiTextField WEDEP;
  private XRiTextField CLPLF;
  private XRiTextField CLPLF2;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_40_OBJ_40;
  private XRiCalendrier CLDPLX;
  private XRiTextField CLDAT3;
  private JLabel OBJ_43_OBJ_44;
  private JLabel OBJ_75_OBJ_76;
  private XRiComboBox CLFIL1;
  private XRiCheckBox EBIN08;
  private JPanel panel1;
  private RiZoneSortie OBJ_33_OBJ_33;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_31_OBJ_31;
  private XRiTextField CLNCG;
  private XRiTextField CLNCA;
  private XRiTextField CLACT;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
