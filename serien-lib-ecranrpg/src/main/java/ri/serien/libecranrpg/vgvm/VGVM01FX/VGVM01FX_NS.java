
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_NS extends SNPanelEcranRPG implements ioFrame {
  
  private boolean isConsultation = false;
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  public VGVM01FX_NS(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    NSALB.setValeursSelection("1", " ");
    NSASUB.setValeursSelection("X", " ");
    NSSVT.setValeursSelection("1", " ");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    

    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    lbLibelle = new SNLabelChamp();
    NSLIB = new XRiTextField();
    pnlOptions = new SNPanel();
    NSASUB = new XRiCheckBox();
    NSALB = new XRiCheckBox();
    NSSVT = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlPersonnalisation.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlPersonnalisation.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlPersonnalisation.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlPersonnalisation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCodeCategorie ----
        lbCodeCategorie.setText("code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- INDIND ----
        INDIND.setPreferredSize(new Dimension(40, 30));
        INDIND.setMinimumSize(new Dimension(40, 30));
        INDIND.setMaximumSize(new Dimension(40, 30));
        INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDIND.setName("INDIND");
        pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setMinimumSize(new Dimension(80, 30));
        lbLibelle.setMaximumSize(new Dimension(80, 30));
        lbLibelle.setPreferredSize(new Dimension(80, 30));
        lbLibelle.setName("lbLibelle");
        pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- NSLIB ----
        NSLIB.setPreferredSize(new Dimension(400, 30));
        NSLIB.setMinimumSize(new Dimension(400, 30));
        NSLIB.setMaximumSize(new Dimension(400, 30));
        NSLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
        NSLIB.setName("NSLIB");
        pnlPersonnalisation.add(NSLIB, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlPersonnalisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlOptions ========
      {
        pnlOptions.setName("pnlOptions");
        pnlOptions.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- NSASUB ----
        NSASUB.setText("Article de substitution obligatoire");
        NSASUB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        NSASUB.setMinimumSize(new Dimension(199, 30));
        NSASUB.setPreferredSize(new Dimension(199, 30));
        NSASUB.setFont(new Font("sansserif", Font.PLAIN, 14));
        NSASUB.setName("NSASUB");
        pnlOptions.add(NSASUB, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- NSALB ----
        NSALB.setText("Alerte en cr\u00e9ation de ligne de vente");
        NSALB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        NSALB.setMinimumSize(new Dimension(199, 30));
        NSALB.setPreferredSize(new Dimension(199, 30));
        NSALB.setFont(new Font("sansserif", Font.PLAIN, 14));
        NSALB.setName("NSALB");
        pnlOptions.add(NSALB, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- NSSVT ----
        NSSVT.setText("Prise en compte de la survente");
        NSSVT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        NSSVT.setMinimumSize(new Dimension(199, 30));
        NSSVT.setPreferredSize(new Dimension(199, 30));
        NSSVT.setFont(new Font("sansserif", Font.PLAIN, 14));
        NSSVT.setName("NSSVT");
        pnlOptions.add(NSSVT, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNLabelChamp lbLibelle;
  private XRiTextField NSLIB;
  private SNPanel pnlOptions;
  private XRiCheckBox NSASUB;
  private XRiCheckBox NSALB;
  private XRiCheckBox NSSVT;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
