
package ri.serien.libecranrpg.vgvm.VGVM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM21FM_QT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM21FM_QT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBCNQ@")).trim()));
    QTE1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE1@")).trim());
    QTE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE2@")).trim());
    QTE3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE3@")).trim());
    QTE4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE4@")).trim());
    QTE5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE5@")).trim());
    QTE6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE6@")).trim());
    QTE7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE7@")).trim());
    QTE8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE8@")).trim());
    QTE9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE9@")).trim());
    QTE10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QTE10@")).trim());
    TYP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP1@")).trim());
    TYP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP2@")).trim());
    TYP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP3@")).trim());
    TYP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP4@")).trim());
    TYP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP5@")).trim());
    TYP6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP6@")).trim());
    TYP7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP7@")).trim());
    TYP8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP8@")).trim());
    TYP9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP9@")).trim());
    TYP10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP10@")).trim());
    CND01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND01@")).trim());
    CND02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND02@")).trim());
    CND03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND03@")).trim());
    CND04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND04@")).trim());
    CND05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND05@")).trim());
    CND06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND06@")).trim());
    CND07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND07@")).trim());
    CND08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND08@")).trim());
    CND09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND09@")).trim());
    CND10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CND10@")).trim());
    W1P10X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P10X@")).trim());
    W1P11X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P11X@")).trim());
    W1P12X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P12X@")).trim());
    W1P13X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P13X@")).trim());
    W1P14X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P14X@")).trim());
    W1P15X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P15X@")).trim());
    W1P16X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P16X@")).trim());
    W1P17X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P17X@")).trim());
    W1P18X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P18X@")).trim());
    W1P19X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1P19X@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    RiZoneSortie[] prix = {W1P11X, W1P12X, W1P13X, W1P14X, W1P15X, W1P16X, W1P17X, W1P18X, W1P19X, W1P10X};
    
    for (int i = 0; i < prix.length; i++) {
      if (lexique.HostFieldGetData(prix[i].getName()).trim().equals("*****")) {
        prix[i].setText("non applicable");
      }
    }
    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    L1PVN = new XRiTextField();
    lb1 = new JLabel();
    lbPrixNet = new JLabel();
    lbPrixBase = new JLabel();
    L1PVB = new XRiTextField();
    lbPrixTarif = new JLabel();
    PRITAR = new XRiTextField();
    lb2 = new JLabel();
    lb3 = new JLabel();
    lb4 = new JLabel();
    lb5 = new JLabel();
    lb6 = new JLabel();
    lb7 = new JLabel();
    lb8 = new JLabel();
    lb9 = new JLabel();
    lb10 = new JLabel();
    QTE1 = new RiZoneSortie();
    QTE2 = new RiZoneSortie();
    QTE3 = new RiZoneSortie();
    QTE4 = new RiZoneSortie();
    QTE5 = new RiZoneSortie();
    QTE6 = new RiZoneSortie();
    QTE7 = new RiZoneSortie();
    QTE8 = new RiZoneSortie();
    QTE9 = new RiZoneSortie();
    QTE10 = new RiZoneSortie();
    TYP1 = new RiZoneSortie();
    TYP2 = new RiZoneSortie();
    TYP3 = new RiZoneSortie();
    TYP4 = new RiZoneSortie();
    TYP5 = new RiZoneSortie();
    TYP6 = new RiZoneSortie();
    TYP7 = new RiZoneSortie();
    TYP8 = new RiZoneSortie();
    TYP9 = new RiZoneSortie();
    TYP10 = new RiZoneSortie();
    CND01 = new RiZoneSortie();
    CND02 = new RiZoneSortie();
    CND03 = new RiZoneSortie();
    CND04 = new RiZoneSortie();
    CND05 = new RiZoneSortie();
    CND06 = new RiZoneSortie();
    CND07 = new RiZoneSortie();
    CND08 = new RiZoneSortie();
    CND09 = new RiZoneSortie();
    CND10 = new RiZoneSortie();
    W1P10X = new RiZoneSortie();
    W1P11X = new RiZoneSortie();
    W1P12X = new RiZoneSortie();
    W1P13X = new RiZoneSortie();
    W1P14X = new RiZoneSortie();
    W1P15X = new RiZoneSortie();
    W1P16X = new RiZoneSortie();
    W1P17X = new RiZoneSortie();
    W1P18X = new RiZoneSortie();
    W1P19X = new RiZoneSortie();
    PRIREV = new XRiTextField();
    lbPrixRevient = new JLabel();
    lbPrixNet2 = new JLabel();
    lbPrixNet3 = new JLabel();
    lbPrixNet4 = new JLabel();
    lbPrixNet5 = new JLabel();
    lbPrixNet6 = new JLabel();
    lbPrixNet7 = new JLabel();
    lbPrixNet8 = new JLabel();
    lbPrixNet9 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(1025, 305));
    setPreferredSize(new Dimension(1025, 305));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("@ULBCNQ@"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- L1PVN ----
          L1PVN.setName("L1PVN");
          panel2.add(L1PVN);
          L1PVN.setBounds(160, 30, 84, L1PVN.getPreferredSize().height);

          //---- lb1 ----
          lb1.setText("1");
          lb1.setHorizontalAlignment(SwingConstants.RIGHT);
          lb1.setFont(lb1.getFont().deriveFont(lb1.getFont().getStyle() | Font.BOLD));
          lb1.setName("lb1");
          panel2.add(lb1);
          lb1.setBounds(20, 85, 20, 20);

          //---- lbPrixNet ----
          lbPrixNet.setText("Prix net");
          lbPrixNet.setName("lbPrixNet");
          panel2.add(lbPrixNet);
          lbPrixNet.setBounds(85, 34, 70, 20);

          //---- lbPrixBase ----
          lbPrixBase.setText("Prix de base");
          lbPrixBase.setName("lbPrixBase");
          panel2.add(lbPrixBase);
          lbPrixBase.setBounds(350, 34, 70, 20);

          //---- L1PVB ----
          L1PVB.setName("L1PVB");
          panel2.add(L1PVB);
          L1PVB.setBounds(430, 30, 84, L1PVB.getPreferredSize().height);

          //---- lbPrixTarif ----
          lbPrixTarif.setText("Prix tarif");
          lbPrixTarif.setName("lbPrixTarif");
          panel2.add(lbPrixTarif);
          lbPrixTarif.setBounds(620, 34, 70, 20);

          //---- PRITAR ----
          PRITAR.setName("PRITAR");
          panel2.add(PRITAR);
          PRITAR.setBounds(700, 30, 84, PRITAR.getPreferredSize().height);

          //---- lb2 ----
          lb2.setText("2");
          lb2.setHorizontalAlignment(SwingConstants.RIGHT);
          lb2.setFont(lb2.getFont().deriveFont(lb2.getFont().getStyle() | Font.BOLD));
          lb2.setName("lb2");
          panel2.add(lb2);
          lb2.setBounds(20, 115, 20, 20);

          //---- lb3 ----
          lb3.setText("3");
          lb3.setHorizontalAlignment(SwingConstants.RIGHT);
          lb3.setFont(lb3.getFont().deriveFont(lb3.getFont().getStyle() | Font.BOLD));
          lb3.setName("lb3");
          panel2.add(lb3);
          lb3.setBounds(20, 145, 20, 20);

          //---- lb4 ----
          lb4.setText("4");
          lb4.setHorizontalAlignment(SwingConstants.RIGHT);
          lb4.setFont(lb4.getFont().deriveFont(lb4.getFont().getStyle() | Font.BOLD));
          lb4.setName("lb4");
          panel2.add(lb4);
          lb4.setBounds(20, 175, 20, 20);

          //---- lb5 ----
          lb5.setText("5");
          lb5.setHorizontalAlignment(SwingConstants.RIGHT);
          lb5.setFont(lb5.getFont().deriveFont(lb5.getFont().getStyle() | Font.BOLD));
          lb5.setName("lb5");
          panel2.add(lb5);
          lb5.setBounds(20, 205, 20, 20);

          //---- lb6 ----
          lb6.setText("6");
          lb6.setHorizontalAlignment(SwingConstants.RIGHT);
          lb6.setFont(lb6.getFont().deriveFont(lb6.getFont().getStyle() | Font.BOLD));
          lb6.setName("lb6");
          panel2.add(lb6);
          lb6.setBounds(435, 85, 20, 20);

          //---- lb7 ----
          lb7.setText("7");
          lb7.setHorizontalAlignment(SwingConstants.RIGHT);
          lb7.setFont(lb7.getFont().deriveFont(lb7.getFont().getStyle() | Font.BOLD));
          lb7.setName("lb7");
          panel2.add(lb7);
          lb7.setBounds(435, 115, 20, 20);

          //---- lb8 ----
          lb8.setText("8");
          lb8.setHorizontalAlignment(SwingConstants.RIGHT);
          lb8.setFont(lb8.getFont().deriveFont(lb8.getFont().getStyle() | Font.BOLD));
          lb8.setName("lb8");
          panel2.add(lb8);
          lb8.setBounds(435, 145, 20, 20);

          //---- lb9 ----
          lb9.setText("9");
          lb9.setHorizontalAlignment(SwingConstants.RIGHT);
          lb9.setFont(lb9.getFont().deriveFont(lb9.getFont().getStyle() | Font.BOLD));
          lb9.setName("lb9");
          panel2.add(lb9);
          lb9.setBounds(435, 175, 20, 20);

          //---- lb10 ----
          lb10.setText("10");
          lb10.setHorizontalAlignment(SwingConstants.RIGHT);
          lb10.setFont(lb10.getFont().deriveFont(lb10.getFont().getStyle() | Font.BOLD));
          lb10.setName("lb10");
          panel2.add(lb10);
          lb10.setBounds(435, 205, 20, 20);

          //---- QTE1 ----
          QTE1.setText("@QTE1@");
          QTE1.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE1.setName("QTE1");
          panel2.add(QTE1);
          QTE1.setBounds(45, 85, 80, QTE1.getPreferredSize().height);

          //---- QTE2 ----
          QTE2.setText("@QTE2@");
          QTE2.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE2.setName("QTE2");
          panel2.add(QTE2);
          QTE2.setBounds(45, 115, 80, QTE2.getPreferredSize().height);

          //---- QTE3 ----
          QTE3.setText("@QTE3@");
          QTE3.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE3.setName("QTE3");
          panel2.add(QTE3);
          QTE3.setBounds(45, 145, 80, QTE3.getPreferredSize().height);

          //---- QTE4 ----
          QTE4.setText("@QTE4@");
          QTE4.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE4.setName("QTE4");
          panel2.add(QTE4);
          QTE4.setBounds(45, 175, 80, QTE4.getPreferredSize().height);

          //---- QTE5 ----
          QTE5.setText("@QTE5@");
          QTE5.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE5.setName("QTE5");
          panel2.add(QTE5);
          QTE5.setBounds(45, 205, 80, QTE5.getPreferredSize().height);

          //---- QTE6 ----
          QTE6.setText("@QTE6@");
          QTE6.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE6.setName("QTE6");
          panel2.add(QTE6);
          QTE6.setBounds(460, 85, 80, QTE6.getPreferredSize().height);

          //---- QTE7 ----
          QTE7.setText("@QTE7@");
          QTE7.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE7.setName("QTE7");
          panel2.add(QTE7);
          QTE7.setBounds(460, 115, 80, QTE7.getPreferredSize().height);

          //---- QTE8 ----
          QTE8.setText("@QTE8@");
          QTE8.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE8.setName("QTE8");
          panel2.add(QTE8);
          QTE8.setBounds(460, 145, 80, QTE8.getPreferredSize().height);

          //---- QTE9 ----
          QTE9.setText("@QTE9@");
          QTE9.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE9.setName("QTE9");
          panel2.add(QTE9);
          QTE9.setBounds(460, 175, 80, QTE9.getPreferredSize().height);

          //---- QTE10 ----
          QTE10.setText("@QTE10@");
          QTE10.setHorizontalAlignment(SwingConstants.RIGHT);
          QTE10.setName("QTE10");
          panel2.add(QTE10);
          QTE10.setBounds(460, 205, 80, QTE10.getPreferredSize().height);

          //---- TYP1 ----
          TYP1.setText("@TYP1@");
          TYP1.setName("TYP1");
          panel2.add(TYP1);
          TYP1.setBounds(130, 85, 25, TYP1.getPreferredSize().height);

          //---- TYP2 ----
          TYP2.setText("@TYP2@");
          TYP2.setName("TYP2");
          panel2.add(TYP2);
          TYP2.setBounds(130, 115, 25, TYP2.getPreferredSize().height);

          //---- TYP3 ----
          TYP3.setText("@TYP3@");
          TYP3.setName("TYP3");
          panel2.add(TYP3);
          TYP3.setBounds(130, 145, 25, TYP3.getPreferredSize().height);

          //---- TYP4 ----
          TYP4.setText("@TYP4@");
          TYP4.setName("TYP4");
          panel2.add(TYP4);
          TYP4.setBounds(130, 175, 25, TYP4.getPreferredSize().height);

          //---- TYP5 ----
          TYP5.setText("@TYP5@");
          TYP5.setName("TYP5");
          panel2.add(TYP5);
          TYP5.setBounds(130, 205, 25, TYP5.getPreferredSize().height);

          //---- TYP6 ----
          TYP6.setText("@TYP6@");
          TYP6.setName("TYP6");
          panel2.add(TYP6);
          TYP6.setBounds(545, 85, 25, TYP6.getPreferredSize().height);

          //---- TYP7 ----
          TYP7.setText("@TYP7@");
          TYP7.setName("TYP7");
          panel2.add(TYP7);
          TYP7.setBounds(545, 115, 25, TYP7.getPreferredSize().height);

          //---- TYP8 ----
          TYP8.setText("@TYP8@");
          TYP8.setName("TYP8");
          panel2.add(TYP8);
          TYP8.setBounds(545, 145, 25, TYP8.getPreferredSize().height);

          //---- TYP9 ----
          TYP9.setText("@TYP9@");
          TYP9.setName("TYP9");
          panel2.add(TYP9);
          TYP9.setBounds(545, 175, 25, TYP9.getPreferredSize().height);

          //---- TYP10 ----
          TYP10.setText("@TYP10@");
          TYP10.setName("TYP10");
          panel2.add(TYP10);
          TYP10.setBounds(545, 205, 25, TYP10.getPreferredSize().height);

          //---- CND01 ----
          CND01.setText("@CND01@");
          CND01.setHorizontalAlignment(SwingConstants.RIGHT);
          CND01.setName("CND01");
          panel2.add(CND01);
          CND01.setBounds(160, 85, 120, CND01.getPreferredSize().height);

          //---- CND02 ----
          CND02.setText("@CND02@");
          CND02.setHorizontalAlignment(SwingConstants.RIGHT);
          CND02.setName("CND02");
          panel2.add(CND02);
          CND02.setBounds(160, 115, 120, CND02.getPreferredSize().height);

          //---- CND03 ----
          CND03.setText("@CND03@");
          CND03.setHorizontalAlignment(SwingConstants.RIGHT);
          CND03.setName("CND03");
          panel2.add(CND03);
          CND03.setBounds(160, 145, 120, CND03.getPreferredSize().height);

          //---- CND04 ----
          CND04.setText("@CND04@");
          CND04.setHorizontalAlignment(SwingConstants.RIGHT);
          CND04.setName("CND04");
          panel2.add(CND04);
          CND04.setBounds(160, 175, 120, CND04.getPreferredSize().height);

          //---- CND05 ----
          CND05.setText("@CND05@");
          CND05.setHorizontalAlignment(SwingConstants.RIGHT);
          CND05.setName("CND05");
          panel2.add(CND05);
          CND05.setBounds(160, 205, 120, CND05.getPreferredSize().height);

          //---- CND06 ----
          CND06.setText("@CND06@");
          CND06.setHorizontalAlignment(SwingConstants.RIGHT);
          CND06.setName("CND06");
          panel2.add(CND06);
          CND06.setBounds(575, 85, 120, CND06.getPreferredSize().height);

          //---- CND07 ----
          CND07.setText("@CND07@");
          CND07.setHorizontalAlignment(SwingConstants.RIGHT);
          CND07.setName("CND07");
          panel2.add(CND07);
          CND07.setBounds(575, 115, 120, CND07.getPreferredSize().height);

          //---- CND08 ----
          CND08.setText("@CND08@");
          CND08.setHorizontalAlignment(SwingConstants.RIGHT);
          CND08.setName("CND08");
          panel2.add(CND08);
          CND08.setBounds(575, 145, 120, CND08.getPreferredSize().height);

          //---- CND09 ----
          CND09.setText("@CND09@");
          CND09.setHorizontalAlignment(SwingConstants.RIGHT);
          CND09.setName("CND09");
          panel2.add(CND09);
          CND09.setBounds(575, 175, 120, CND09.getPreferredSize().height);

          //---- CND10 ----
          CND10.setText("@CND10@");
          CND10.setHorizontalAlignment(SwingConstants.RIGHT);
          CND10.setName("CND10");
          panel2.add(CND10);
          CND10.setBounds(575, 205, 120, CND10.getPreferredSize().height);

          //---- W1P10X ----
          W1P10X.setText("@W1P10X@");
          W1P10X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P10X.setName("W1P10X");
          panel2.add(W1P10X);
          W1P10X.setBounds(700, 205, 110, W1P10X.getPreferredSize().height);

          //---- W1P11X ----
          W1P11X.setText("@W1P11X@");
          W1P11X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P11X.setName("W1P11X");
          panel2.add(W1P11X);
          W1P11X.setBounds(285, 85, 110, W1P11X.getPreferredSize().height);

          //---- W1P12X ----
          W1P12X.setText("@W1P12X@");
          W1P12X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P12X.setName("W1P12X");
          panel2.add(W1P12X);
          W1P12X.setBounds(285, 115, 110, W1P12X.getPreferredSize().height);

          //---- W1P13X ----
          W1P13X.setText("@W1P13X@");
          W1P13X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P13X.setName("W1P13X");
          panel2.add(W1P13X);
          W1P13X.setBounds(285, 145, 110, W1P13X.getPreferredSize().height);

          //---- W1P14X ----
          W1P14X.setText("@W1P14X@");
          W1P14X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P14X.setName("W1P14X");
          panel2.add(W1P14X);
          W1P14X.setBounds(285, 175, 110, W1P14X.getPreferredSize().height);

          //---- W1P15X ----
          W1P15X.setText("@W1P15X@");
          W1P15X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P15X.setName("W1P15X");
          panel2.add(W1P15X);
          W1P15X.setBounds(285, 205, 110, W1P15X.getPreferredSize().height);

          //---- W1P16X ----
          W1P16X.setText("@W1P16X@");
          W1P16X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P16X.setName("W1P16X");
          panel2.add(W1P16X);
          W1P16X.setBounds(700, 85, 110, W1P16X.getPreferredSize().height);

          //---- W1P17X ----
          W1P17X.setText("@W1P17X@");
          W1P17X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P17X.setName("W1P17X");
          panel2.add(W1P17X);
          W1P17X.setBounds(700, 115, 110, W1P17X.getPreferredSize().height);

          //---- W1P18X ----
          W1P18X.setText("@W1P18X@");
          W1P18X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P18X.setName("W1P18X");
          panel2.add(W1P18X);
          W1P18X.setBounds(700, 145, 110, W1P18X.getPreferredSize().height);

          //---- W1P19X ----
          W1P19X.setText("@W1P19X@");
          W1P19X.setHorizontalAlignment(SwingConstants.RIGHT);
          W1P19X.setName("W1P19X");
          panel2.add(W1P19X);
          W1P19X.setBounds(700, 175, 110, W1P19X.getPreferredSize().height);

          //---- PRIREV ----
          PRIREV.setName("PRIREV");
          panel2.add(PRIREV);
          PRIREV.setBounds(700, 235, 84, PRIREV.getPreferredSize().height);

          //---- lbPrixRevient ----
          lbPrixRevient.setText("Prix de revient");
          lbPrixRevient.setName("lbPrixRevient");
          panel2.add(lbPrixRevient);
          lbPrixRevient.setBounds(605, 240, 95, 20);

          //---- lbPrixNet2 ----
          lbPrixNet2.setText("Quantit\u00e9");
          lbPrixNet2.setFont(lbPrixNet2.getFont().deriveFont(lbPrixNet2.getFont().getStyle() | Font.BOLD));
          lbPrixNet2.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet2.setName("lbPrixNet2");
          panel2.add(lbPrixNet2);
          lbPrixNet2.setBounds(45, 65, 80, 20);

          //---- lbPrixNet3 ----
          lbPrixNet3.setText("T");
          lbPrixNet3.setFont(lbPrixNet3.getFont().deriveFont(lbPrixNet3.getFont().getStyle() | Font.BOLD));
          lbPrixNet3.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet3.setName("lbPrixNet3");
          panel2.add(lbPrixNet3);
          lbPrixNet3.setBounds(130, 65, 25, 20);

          //---- lbPrixNet4 ----
          lbPrixNet4.setText("Condition");
          lbPrixNet4.setFont(lbPrixNet4.getFont().deriveFont(lbPrixNet4.getFont().getStyle() | Font.BOLD));
          lbPrixNet4.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet4.setName("lbPrixNet4");
          panel2.add(lbPrixNet4);
          lbPrixNet4.setBounds(160, 65, 120, 20);

          //---- lbPrixNet5 ----
          lbPrixNet5.setText("Prix");
          lbPrixNet5.setFont(lbPrixNet5.getFont().deriveFont(lbPrixNet5.getFont().getStyle() | Font.BOLD));
          lbPrixNet5.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet5.setName("lbPrixNet5");
          panel2.add(lbPrixNet5);
          lbPrixNet5.setBounds(285, 65, 110, 20);

          //---- lbPrixNet6 ----
          lbPrixNet6.setText("Quantit\u00e9");
          lbPrixNet6.setFont(lbPrixNet6.getFont().deriveFont(lbPrixNet6.getFont().getStyle() | Font.BOLD));
          lbPrixNet6.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet6.setName("lbPrixNet6");
          panel2.add(lbPrixNet6);
          lbPrixNet6.setBounds(460, 65, 80, 20);

          //---- lbPrixNet7 ----
          lbPrixNet7.setText("T");
          lbPrixNet7.setFont(lbPrixNet7.getFont().deriveFont(lbPrixNet7.getFont().getStyle() | Font.BOLD));
          lbPrixNet7.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet7.setName("lbPrixNet7");
          panel2.add(lbPrixNet7);
          lbPrixNet7.setBounds(545, 65, 25, 20);

          //---- lbPrixNet8 ----
          lbPrixNet8.setText("Condition");
          lbPrixNet8.setFont(lbPrixNet8.getFont().deriveFont(lbPrixNet8.getFont().getStyle() | Font.BOLD));
          lbPrixNet8.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet8.setName("lbPrixNet8");
          panel2.add(lbPrixNet8);
          lbPrixNet8.setBounds(575, 65, 120, 20);

          //---- lbPrixNet9 ----
          lbPrixNet9.setText("Prix");
          lbPrixNet9.setFont(lbPrixNet9.getFont().deriveFont(lbPrixNet9.getFont().getStyle() | Font.BOLD));
          lbPrixNet9.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet9.setName("lbPrixNet9");
          panel2.add(lbPrixNet9);
          lbPrixNet9.setBounds(700, 65, 110, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 831, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 279, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField L1PVN;
  private JLabel lb1;
  private JLabel lbPrixNet;
  private JLabel lbPrixBase;
  private XRiTextField L1PVB;
  private JLabel lbPrixTarif;
  private XRiTextField PRITAR;
  private JLabel lb2;
  private JLabel lb3;
  private JLabel lb4;
  private JLabel lb5;
  private JLabel lb6;
  private JLabel lb7;
  private JLabel lb8;
  private JLabel lb9;
  private JLabel lb10;
  private RiZoneSortie QTE1;
  private RiZoneSortie QTE2;
  private RiZoneSortie QTE3;
  private RiZoneSortie QTE4;
  private RiZoneSortie QTE5;
  private RiZoneSortie QTE6;
  private RiZoneSortie QTE7;
  private RiZoneSortie QTE8;
  private RiZoneSortie QTE9;
  private RiZoneSortie QTE10;
  private RiZoneSortie TYP1;
  private RiZoneSortie TYP2;
  private RiZoneSortie TYP3;
  private RiZoneSortie TYP4;
  private RiZoneSortie TYP5;
  private RiZoneSortie TYP6;
  private RiZoneSortie TYP7;
  private RiZoneSortie TYP8;
  private RiZoneSortie TYP9;
  private RiZoneSortie TYP10;
  private RiZoneSortie CND01;
  private RiZoneSortie CND02;
  private RiZoneSortie CND03;
  private RiZoneSortie CND04;
  private RiZoneSortie CND05;
  private RiZoneSortie CND06;
  private RiZoneSortie CND07;
  private RiZoneSortie CND08;
  private RiZoneSortie CND09;
  private RiZoneSortie CND10;
  private RiZoneSortie W1P10X;
  private RiZoneSortie W1P11X;
  private RiZoneSortie W1P12X;
  private RiZoneSortie W1P13X;
  private RiZoneSortie W1P14X;
  private RiZoneSortie W1P15X;
  private RiZoneSortie W1P16X;
  private RiZoneSortie W1P17X;
  private RiZoneSortie W1P18X;
  private RiZoneSortie W1P19X;
  private XRiTextField PRIREV;
  private JLabel lbPrixRevient;
  private JLabel lbPrixNet2;
  private JLabel lbPrixNet3;
  private JLabel lbPrixNet4;
  private JLabel lbPrixNet5;
  private JLabel lbPrixNet6;
  private JLabel lbPrixNet7;
  private JLabel lbPrixNet8;
  private JLabel lbPrixNet9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
