
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_CO extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] CONSA_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "R", "G", "F", };
  
  public VGVM01FX_CO(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CONSA.setValeurs(CONSA_Value, null);
    CODGR.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COCOL2@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COCOL1@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COCOC2@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COCOC1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*LIB");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*LIB");
    }
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*LIC");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*LIC");
    }
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*RE1");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*RE1");
    }
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*RE2");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*RE2");
    }
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*NUM");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*NUM");
    }
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*MTD");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*MTD");
    }
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*FAC");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*FAC");
    }
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*REP");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*REP");
    }
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*DFA");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*DFA");
    }
  }
  
  private void OBJ_30ItemStateChanged(ItemEvent e) {
    if (BTD.getInvoker().getName().equalsIgnoreCase("COLI1")) {
      COLI1.setText(COLI1.getText() + "*dfa");
    }
    else if (BTD.getInvoker().getName().equalsIgnoreCase("COLI2")) {
      COLI2.setText(COLI2.getText() + "*dfa");
    }
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CONSA = new XRiComboBox();
    COLIB = new XRiTextField();
    CODGR = new XRiCheckBox();
    OBJ_45 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_87 = new JLabel();
    CONCG = new XRiTextField();
    COSAN = new XRiTextField();
    SAN1 = new XRiTextField();
    SAN2 = new XRiTextField();
    SAN3 = new XRiTextField();
    SAN4 = new XRiTextField();
    SAN5 = new XRiTextField();
    SAN6 = new XRiTextField();
    SAN7 = new XRiTextField();
    SAN8 = new XRiTextField();
    SAN9 = new XRiTextField();
    OBJ_78 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_86 = new JLabel();
    COCOL = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    COLI1 = new XRiTextField();
    COLI2 = new XRiTextField();
    OBJ_74 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_27 = new RiZoneSortie();
    OBJ_102 = new RiZoneSortie();
    COCOS1 = new XRiTextField();
    COCOS2 = new XRiTextField();
    COCOT1 = new XRiTextField();
    COCOT2 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setText("@INDIND@");
          INDIND.setOpaque(false);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(860, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Postes de comptabilisation");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();

            //---- CONSA ----
            CONSA.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "1",
              "2",
              "3",
              "4",
              "5",
              "6",
              "7",
              "8",
              "9",
              "R",
              "G",
              "F"
            }));
            CONSA.setComponentPopupMenu(null);
            CONSA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CONSA.setName("CONSA");

            //---- COLIB ----
            COLIB.setComponentPopupMenu(null);
            COLIB.setName("COLIB");

            //---- CODGR ----
            CODGR.setText("Non regroupement");
            CODGR.setComponentPopupMenu(BTD);
            CODGR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CODGR.setName("CODGR");

            //---- OBJ_45 ----
            OBJ_45.setText("Num\u00e9ro de compte");
            OBJ_45.setName("OBJ_45");

            //---- OBJ_76 ----
            OBJ_76.setText("Code analytique");
            OBJ_76.setName("OBJ_76");

            //---- OBJ_73 ----
            OBJ_73.setText("Libell\u00e9 \u00e9critures");
            OBJ_73.setName("OBJ_73");

            //---- OBJ_72 ----
            OBJ_72.setText("Collectif");
            OBJ_72.setName("OBJ_72");

            //---- OBJ_87 ----
            OBJ_87.setText("Pointeur");
            OBJ_87.setName("OBJ_87");

            //---- CONCG ----
            CONCG.setComponentPopupMenu(null);
            CONCG.setName("CONCG");

            //---- COSAN ----
            COSAN.setComponentPopupMenu(null);
            COSAN.setName("COSAN");

            //---- SAN1 ----
            SAN1.setComponentPopupMenu(null);
            SAN1.setName("SAN1");

            //---- SAN2 ----
            SAN2.setComponentPopupMenu(null);
            SAN2.setName("SAN2");

            //---- SAN3 ----
            SAN3.setComponentPopupMenu(null);
            SAN3.setName("SAN3");

            //---- SAN4 ----
            SAN4.setComponentPopupMenu(null);
            SAN4.setName("SAN4");

            //---- SAN5 ----
            SAN5.setComponentPopupMenu(null);
            SAN5.setName("SAN5");

            //---- SAN6 ----
            SAN6.setComponentPopupMenu(null);
            SAN6.setName("SAN6");

            //---- SAN7 ----
            SAN7.setComponentPopupMenu(null);
            SAN7.setName("SAN7");

            //---- SAN8 ----
            SAN8.setComponentPopupMenu(null);
            SAN8.setName("SAN8");

            //---- SAN9 ----
            SAN9.setComponentPopupMenu(null);
            SAN9.setName("SAN9");

            //---- OBJ_78 ----
            OBJ_78.setText("1");
            OBJ_78.setName("OBJ_78");

            //---- OBJ_79 ----
            OBJ_79.setText("2");
            OBJ_79.setName("OBJ_79");

            //---- OBJ_80 ----
            OBJ_80.setText("3");
            OBJ_80.setName("OBJ_80");

            //---- OBJ_81 ----
            OBJ_81.setText("4");
            OBJ_81.setName("OBJ_81");

            //---- OBJ_82 ----
            OBJ_82.setText("5");
            OBJ_82.setName("OBJ_82");

            //---- OBJ_83 ----
            OBJ_83.setText("6");
            OBJ_83.setName("OBJ_83");

            //---- OBJ_84 ----
            OBJ_84.setText("7");
            OBJ_84.setName("OBJ_84");

            //---- OBJ_85 ----
            OBJ_85.setText("8");
            OBJ_85.setName("OBJ_85");

            //---- OBJ_86 ----
            OBJ_86.setText("9");
            OBJ_86.setName("OBJ_86");

            //---- COCOL ----
            COCOL.setComponentPopupMenu(null);
            COCOL.setName("COCOL");

            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(20, 20, 20)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 97, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 102, GroupLayout.PREFERRED_SIZE))
                      .addGap(7, 7, 7)
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                          .addComponent(CONCG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                          .addGap(130, 130, 130)
                          .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                          .addGap(5, 5, 5)
                          .addComponent(COCOL, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                        .addComponent(COSAN, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COLIB, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                      .addGap(95, 95, 95)
                      .addComponent(CODGR, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(215, 215, 215)
                      .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(15, 15, 15)
                      .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(20, 20, 20)
                      .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                      .addGap(73, 73, 73)
                      .addComponent(CONSA, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(SAN1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN2, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(1, 1, 1)
                      .addComponent(SAN3, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN4, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN5, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN6, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN7, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN8, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                      .addGap(6, 6, 6)
                      .addComponent(SAN9, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap(182, Short.MAX_VALUE))
            );
            xTitledPanel1ContentContainerLayout.setVerticalGroup(
              xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(CONCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COCOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(22, 22, 22)
                      .addComponent(COSAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(COLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CODGR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(7, 7, 7)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_85, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                    .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CONSA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SAN9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Incrustation libell\u00e9 si non regroupement");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_28 ----
            OBJ_28.setText("@COCOL2@");
            OBJ_28.setName("OBJ_28");
            xTitledPanel2ContentContainer.add(OBJ_28);
            OBJ_28.setBounds(555, 132, 203, OBJ_28.getPreferredSize().height);

            //---- OBJ_103 ----
            OBJ_103.setText("@COCOL1@");
            OBJ_103.setName("OBJ_103");
            xTitledPanel2ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(555, 107, 203, OBJ_103.getPreferredSize().height);

            //---- COLI1 ----
            COLI1.setComponentPopupMenu(BTD);
            COLI1.setFont(new Font("Courier New", Font.PLAIN, 17));
            COLI1.setName("COLI1");
            xTitledPanel2ContentContainer.add(COLI1);
            COLI1.setBounds(149, 35, 270, COLI1.getPreferredSize().height);

            //---- COLI2 ----
            COLI2.setComponentPopupMenu(BTD);
            COLI2.setFont(new Font("Courier New", Font.PLAIN, 17));
            COLI2.setName("COLI2");
            xTitledPanel2ContentContainer.add(COLI2);
            COLI2.setBounds(149, 65, 270, COLI2.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("1...5....10...15...20...26");
            OBJ_74.setFont(new Font("Courier New", Font.PLAIN, 17));
            OBJ_74.setName("OBJ_74");
            xTitledPanel2ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(155, 10, 264, 30);

            //---- OBJ_75 ----
            OBJ_75.setText("Ecriture sans devise");
            OBJ_75.setName("OBJ_75");
            xTitledPanel2ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(20, 41, 124, 20);

            //---- OBJ_101 ----
            OBJ_101.setText("Num\u00e9ro de compte");
            OBJ_101.setName("OBJ_101");
            xTitledPanel2ContentContainer.add(OBJ_101);
            OBJ_101.setBounds(235, 110, 118, 20);

            //---- OBJ_105 ----
            OBJ_105.setText("Num\u00e9ro de compte");
            OBJ_105.setName("OBJ_105");
            xTitledPanel2ContentContainer.add(OBJ_105);
            OBJ_105.setBounds(235, 135, 118, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Ecriture en devise");
            OBJ_77.setName("OBJ_77");
            xTitledPanel2ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(20, 71, 111, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("Substitution type");
            OBJ_100.setName("OBJ_100");
            xTitledPanel2ContentContainer.add(OBJ_100);
            OBJ_100.setBounds(20, 110, 100, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("Substitution type");
            OBJ_104.setName("OBJ_104");
            xTitledPanel2ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(20, 135, 100, 20);

            //---- OBJ_27 ----
            OBJ_27.setText("@COCOC2@");
            OBJ_27.setName("OBJ_27");
            xTitledPanel2ContentContainer.add(OBJ_27);
            OBJ_27.setBounds(435, 132, 110, OBJ_27.getPreferredSize().height);

            //---- OBJ_102 ----
            OBJ_102.setText("@COCOC1@");
            OBJ_102.setName("OBJ_102");
            xTitledPanel2ContentContainer.add(OBJ_102);
            OBJ_102.setBounds(435, 107, 110, OBJ_102.getPreferredSize().height);

            //---- COCOS1 ----
            COCOS1.setComponentPopupMenu(null);
            COCOS1.setName("COCOS1");
            xTitledPanel2ContentContainer.add(COCOS1);
            COCOS1.setBounds(385, 105, 36, COCOS1.getPreferredSize().height);

            //---- COCOS2 ----
            COCOS2.setComponentPopupMenu(null);
            COCOS2.setName("COCOS2");
            xTitledPanel2ContentContainer.add(COCOS2);
            COCOS2.setBounds(385, 130, 36, COCOS2.getPreferredSize().height);

            //---- COCOT1 ----
            COCOT1.setComponentPopupMenu(null);
            COCOT1.setName("COCOT1");
            xTitledPanel2ContentContainer.add(COCOT1);
            COCOT1.setBounds(150, 105, 20, COCOT1.getPreferredSize().height);

            //---- COCOT2 ----
            COCOT2.setComponentPopupMenu(null);
            COCOT2.setName("COCOT2");
            xTitledPanel2ContentContainer.add(COCOT2);
            COCOT2.setBounds(150, 130, 20, COCOT2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 227, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Inclure libell\u00e9 CO");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_20 ----
      OBJ_20.setText("Inclure libell\u00e9 client");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("R\u00e9f\u00e9rence courte");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("R\u00e9f\u00e9rence longue");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("num\u00e9ro de commande");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Montant devise");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("num\u00e9ro de facture");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("Repr\u00e9sentant");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);

      //---- OBJ_29 ----
      OBJ_29.setText("Fact. JJ.MM.AA");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("Fact. JJMMAA");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          OBJ_30ItemStateChanged(e);
        }
      });
      BTD.add(OBJ_30);
      BTD.addSeparator();

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox CONSA;
  private XRiTextField COLIB;
  private XRiCheckBox CODGR;
  private JLabel OBJ_45;
  private JLabel OBJ_76;
  private JLabel OBJ_73;
  private JLabel OBJ_72;
  private JLabel OBJ_87;
  private XRiTextField CONCG;
  private XRiTextField COSAN;
  private XRiTextField SAN1;
  private XRiTextField SAN2;
  private XRiTextField SAN3;
  private XRiTextField SAN4;
  private XRiTextField SAN5;
  private XRiTextField SAN6;
  private XRiTextField SAN7;
  private XRiTextField SAN8;
  private XRiTextField SAN9;
  private JLabel OBJ_78;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JLabel OBJ_83;
  private JLabel OBJ_84;
  private JLabel OBJ_85;
  private JLabel OBJ_86;
  private XRiTextField COCOL;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_103;
  private XRiTextField COLI1;
  private XRiTextField COLI2;
  private JLabel OBJ_74;
  private JLabel OBJ_75;
  private JLabel OBJ_101;
  private JLabel OBJ_105;
  private JLabel OBJ_77;
  private JLabel OBJ_100;
  private JLabel OBJ_104;
  private RiZoneSortie OBJ_27;
  private RiZoneSortie OBJ_102;
  private XRiTextField COCOS1;
  private XRiTextField COCOS2;
  private XRiTextField COCOT1;
  private XRiTextField COCOT2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
