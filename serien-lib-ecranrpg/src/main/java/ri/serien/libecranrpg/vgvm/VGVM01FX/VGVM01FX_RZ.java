
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_RZ extends SNPanelEcranRPG implements ioFrame {
  
  // private static final String BOUTON_EDITER = "Editer paramètres";
  private static final String BOUTON_MODIFIER = "Modifier";
  private static final String BOUTON_CONSULTER = "Consulter";
  
  public VGVM01FX_RZ(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_CONSULTER, 'c', true);
    // snBarreBouton.ajouterBouton(BOUTON_EDITER, 'e', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    boolean isModeModification = lexique.isTrue("52");
    boolean isModeConsultation = lexique.isTrue("53");
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    lbZones.setText("Recherche zones " + lexique.HostFieldGetData("FICHE"));
    
    snEtablissement.setSession(getSession());
    snEtablissement.charger(true);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    RZL1.setVisible(!RZL1.getText().trim().equals(""));
    RZP1.setVisible(!RZL1.getText().trim().equals(""));
    RZL2.setVisible(!RZL2.getText().trim().equals(""));
    RZP2.setVisible(!RZL2.getText().trim().equals(""));
    RZL3.setVisible(!RZL3.getText().trim().equals(""));
    RZP3.setVisible(!RZL3.getText().trim().equals(""));
    RZL4.setVisible(!RZL4.getText().trim().equals(""));
    RZP4.setVisible(!RZL4.getText().trim().equals(""));
    RZL5.setVisible(!RZL5.getText().trim().equals(""));
    RZP5.setVisible(!RZL5.getText().trim().equals(""));
    RZL6.setVisible(!RZL6.getText().trim().equals(""));
    RZP6.setVisible(!RZL6.getText().trim().equals(""));
    RZL7.setVisible(!RZL7.getText().trim().equals(""));
    RZP7.setVisible(!RZL7.getText().trim().equals(""));
    RZL8.setVisible(!RZL8.getText().trim().equals(""));
    RZP8.setVisible(!RZL8.getText().trim().equals(""));
    RZL9.setVisible(!RZL9.getText().trim().equals(""));
    RZP9.setVisible(!RZL9.getText().trim().equals(""));
    RZL10.setVisible(!RZL10.getText().trim().equals(""));
    RZP10.setVisible(!RZL10.getText().trim().equals(""));
    
    snBarreBouton.activerBouton(BOUTON_MODIFIER, isModeConsultation);
    snBarreBouton.activerBouton(BOUTON_CONSULTER, isModeModification);
    
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isModeConsultation);
    
    
    
    

    
    p_bpresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      // else if (pSNBouton.isBouton(BOUTON_EDITER)) {
      // lexique.HostScreenSendKey(this, "F10");
      // }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_CONSULTER)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlIndicatif = new SNPanelTitre();
    lbCode = new SNLabelChamp();
    INDIND = new XRiTextField();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlDonnees = new SNPanel();
    lbZones = new SNLabelTitre();
    lbTitre2 = new SNLabelTitre();
    RZL1 = new XRiTextField();
    RZP1 = new XRiTextField();
    RZL2 = new XRiTextField();
    RZP2 = new XRiTextField();
    RZL3 = new XRiTextField();
    RZP3 = new XRiTextField();
    RZL4 = new XRiTextField();
    RZP4 = new XRiTextField();
    RZL5 = new XRiTextField();
    RZP5 = new XRiTextField();
    RZL6 = new XRiTextField();
    RZP6 = new XRiTextField();
    RZL7 = new XRiTextField();
    RZP7 = new XRiTextField();
    RZL8 = new XRiTextField();
    RZP8 = new XRiTextField();
    RZL9 = new XRiTextField();
    RZP9 = new XRiTextField();
    RZL10 = new XRiTextField();
    RZP10 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setFont(new Font("sansserif", Font.PLAIN, 14));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //---- snBarreBouton ----
      snBarreBouton.setFont(new Font("sansserif", Font.PLAIN, 14));
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlIndicatif ========
        {
          pnlIndicatif.setTitre("Recherche de zone texte (RZ)");
          pnlIndicatif.setName("pnlIndicatif");
          pnlIndicatif.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlIndicatif.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlIndicatif.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlIndicatif.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlIndicatif.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbCode ----
          lbCode.setText("Code");
          lbCode.setName("lbCode");
          pnlIndicatif.add(lbCode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- INDIND ----
          INDIND.setMinimumSize(new Dimension(60, 30));
          INDIND.setPreferredSize(new Dimension(60, 30));
          INDIND.setName("INDIND");
          pnlIndicatif.add(INDIND, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlIndicatif.add(lbEtablissement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlIndicatif.add(snEtablissement, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlIndicatif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));

        //======== pnlDonnees ========
        {
          pnlDonnees.setName("pnlDonnees");
          pnlDonnees.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDonnees.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDonnees.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDonnees.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlDonnees.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbZones ----
          lbZones.setText("text");
          lbZones.setName("lbZones");
          pnlDonnees.add(lbZones, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbTitre2 ----
          lbTitre2.setText("Poids");
          lbTitre2.setName("lbTitre2");
          pnlDonnees.add(lbTitre2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL1 ----
          RZL1.setMaximumSize(new Dimension(240, 30));
          RZL1.setMinimumSize(new Dimension(240, 30));
          RZL1.setPreferredSize(new Dimension(240, 30));
          RZL1.setFocusable(false);
          RZL1.setEnabled(false);
          RZL1.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL1.setName("RZL1");
          pnlDonnees.add(RZL1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP1 ----
          RZP1.setMaximumSize(new Dimension(40, 30));
          RZP1.setMinimumSize(new Dimension(40, 30));
          RZP1.setPreferredSize(new Dimension(40, 30));
          RZP1.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP1.setName("RZP1");
          pnlDonnees.add(RZP1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL2 ----
          RZL2.setMaximumSize(new Dimension(240, 30));
          RZL2.setMinimumSize(new Dimension(240, 30));
          RZL2.setPreferredSize(new Dimension(240, 30));
          RZL2.setFocusable(false);
          RZL2.setEnabled(false);
          RZL2.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL2.setName("RZL2");
          pnlDonnees.add(RZL2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP2 ----
          RZP2.setMaximumSize(new Dimension(40, 30));
          RZP2.setMinimumSize(new Dimension(40, 30));
          RZP2.setPreferredSize(new Dimension(40, 30));
          RZP2.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP2.setName("RZP2");
          pnlDonnees.add(RZP2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL3 ----
          RZL3.setMaximumSize(new Dimension(240, 30));
          RZL3.setMinimumSize(new Dimension(240, 30));
          RZL3.setPreferredSize(new Dimension(240, 30));
          RZL3.setFocusable(false);
          RZL3.setEnabled(false);
          RZL3.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL3.setName("RZL3");
          pnlDonnees.add(RZL3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP3 ----
          RZP3.setMaximumSize(new Dimension(40, 30));
          RZP3.setMinimumSize(new Dimension(40, 30));
          RZP3.setPreferredSize(new Dimension(40, 30));
          RZP3.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP3.setName("RZP3");
          pnlDonnees.add(RZP3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL4 ----
          RZL4.setMaximumSize(new Dimension(240, 30));
          RZL4.setMinimumSize(new Dimension(240, 30));
          RZL4.setPreferredSize(new Dimension(240, 30));
          RZL4.setFocusable(false);
          RZL4.setEnabled(false);
          RZL4.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL4.setName("RZL4");
          pnlDonnees.add(RZL4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP4 ----
          RZP4.setMaximumSize(new Dimension(40, 30));
          RZP4.setMinimumSize(new Dimension(40, 30));
          RZP4.setPreferredSize(new Dimension(40, 30));
          RZP4.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP4.setName("RZP4");
          pnlDonnees.add(RZP4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL5 ----
          RZL5.setMaximumSize(new Dimension(240, 30));
          RZL5.setMinimumSize(new Dimension(240, 30));
          RZL5.setPreferredSize(new Dimension(240, 30));
          RZL5.setFocusable(false);
          RZL5.setEnabled(false);
          RZL5.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL5.setName("RZL5");
          pnlDonnees.add(RZL5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP5 ----
          RZP5.setMaximumSize(new Dimension(40, 30));
          RZP5.setMinimumSize(new Dimension(40, 30));
          RZP5.setPreferredSize(new Dimension(40, 30));
          RZP5.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP5.setName("RZP5");
          pnlDonnees.add(RZP5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL6 ----
          RZL6.setMaximumSize(new Dimension(240, 30));
          RZL6.setMinimumSize(new Dimension(240, 30));
          RZL6.setPreferredSize(new Dimension(240, 30));
          RZL6.setFocusable(false);
          RZL6.setEnabled(false);
          RZL6.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL6.setName("RZL6");
          pnlDonnees.add(RZL6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP6 ----
          RZP6.setMaximumSize(new Dimension(40, 30));
          RZP6.setMinimumSize(new Dimension(40, 30));
          RZP6.setPreferredSize(new Dimension(40, 30));
          RZP6.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP6.setName("RZP6");
          pnlDonnees.add(RZP6, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL7 ----
          RZL7.setMaximumSize(new Dimension(240, 30));
          RZL7.setMinimumSize(new Dimension(240, 30));
          RZL7.setPreferredSize(new Dimension(240, 30));
          RZL7.setFocusable(false);
          RZL7.setEnabled(false);
          RZL7.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL7.setName("RZL7");
          pnlDonnees.add(RZL7, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP7 ----
          RZP7.setMaximumSize(new Dimension(40, 30));
          RZP7.setMinimumSize(new Dimension(40, 30));
          RZP7.setPreferredSize(new Dimension(40, 30));
          RZP7.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP7.setName("RZP7");
          pnlDonnees.add(RZP7, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL8 ----
          RZL8.setMaximumSize(new Dimension(240, 30));
          RZL8.setMinimumSize(new Dimension(240, 30));
          RZL8.setPreferredSize(new Dimension(240, 30));
          RZL8.setFocusable(false);
          RZL8.setEnabled(false);
          RZL8.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL8.setName("RZL8");
          pnlDonnees.add(RZL8, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP8 ----
          RZP8.setMaximumSize(new Dimension(40, 30));
          RZP8.setMinimumSize(new Dimension(40, 30));
          RZP8.setPreferredSize(new Dimension(40, 30));
          RZP8.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP8.setName("RZP8");
          pnlDonnees.add(RZP8, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL9 ----
          RZL9.setMaximumSize(new Dimension(240, 30));
          RZL9.setMinimumSize(new Dimension(240, 30));
          RZL9.setPreferredSize(new Dimension(240, 30));
          RZL9.setFocusable(false);
          RZL9.setEnabled(false);
          RZL9.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL9.setName("RZL9");
          pnlDonnees.add(RZL9, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP9 ----
          RZP9.setMaximumSize(new Dimension(40, 30));
          RZP9.setMinimumSize(new Dimension(40, 30));
          RZP9.setPreferredSize(new Dimension(40, 30));
          RZP9.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP9.setName("RZP9");
          pnlDonnees.add(RZP9, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- RZL10 ----
          RZL10.setMaximumSize(new Dimension(240, 30));
          RZL10.setMinimumSize(new Dimension(240, 30));
          RZL10.setPreferredSize(new Dimension(240, 30));
          RZL10.setFocusable(false);
          RZL10.setEnabled(false);
          RZL10.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZL10.setName("RZL10");
          pnlDonnees.add(RZL10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RZP10 ----
          RZP10.setMaximumSize(new Dimension(40, 30));
          RZP10.setMinimumSize(new Dimension(40, 30));
          RZP10.setPreferredSize(new Dimension(40, 30));
          RZP10.setFont(new Font("sansserif", Font.PLAIN, 14));
          RZP10.setName("RZP10");
          pnlDonnees.add(RZP10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlContenu.add(pnlDonnees, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlIndicatif;
  private SNLabelChamp lbCode;
  private XRiTextField INDIND;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanel pnlDonnees;
  private SNLabelTitre lbZones;
  private SNLabelTitre lbTitre2;
  private XRiTextField RZL1;
  private XRiTextField RZP1;
  private XRiTextField RZL2;
  private XRiTextField RZP2;
  private XRiTextField RZL3;
  private XRiTextField RZP3;
  private XRiTextField RZL4;
  private XRiTextField RZP4;
  private XRiTextField RZL5;
  private XRiTextField RZP5;
  private XRiTextField RZL6;
  private XRiTextField RZP6;
  private XRiTextField RZL7;
  private XRiTextField RZP7;
  private XRiTextField RZL8;
  private XRiTextField RZP8;
  private XRiTextField RZL9;
  private XRiTextField RZP9;
  private XRiTextField RZL10;
  private XRiTextField RZP10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
