
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_TA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TAIN2_Value = { "", "1", "2", };
  
  public VGVM01FX_TA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TAIN2.setValeurs(TAIN2_Value, null);
    TAIN3.setValeursSelection("1", " ");
    TAIN1.setValeursSelection("1", " ");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    TAIN21.setSelected(lexique.HostFieldGetData("TAIN2").trim().equals("Q"));
    TATIR.setVisible(!lexique.HostFieldGetData("TAIN2").trim().equals("Q"));
    TAIN2.setEnabled(!lexique.HostFieldGetData("TAIN2").trim().equals("Q"));
    TAIN21.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (TAIN21.isSelected()) {
      lexique.HostFieldPutData("TAIN2", 0, "Q");
    }
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void TAIN21ActionPerformed(ActionEvent e) {
    TATIR.setVisible(!TATIR.isVisible());
    TAIN2.setEnabled(!TAIN2.isEnabled());
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    TAIN2 = new XRiComboBox();
    TALIB = new XRiTextField();
    OBJ_98 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_104 = new JLabel();
    TAART = new XRiTextField();
    OBJ_54 = new JLabel();
    TATIR = new XRiTextField();
    TALB1 = new XRiTextField();
    TALB6 = new XRiTextField();
    TALB2 = new XRiTextField();
    TALB7 = new XRiTextField();
    TALB3 = new XRiTextField();
    TALB8 = new XRiTextField();
    TALB4 = new XRiTextField();
    TALB9 = new XRiTextField();
    TALB5 = new XRiTextField();
    TALB10 = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_61 = new JLabel();
    TACO1 = new XRiTextField();
    TACO6 = new XRiTextField();
    TACO2 = new XRiTextField();
    TACO7 = new XRiTextField();
    TACO3 = new XRiTextField();
    TACO8 = new XRiTextField();
    TACO4 = new XRiTextField();
    TACO9 = new XRiTextField();
    TACO5 = new XRiTextField();
    TACO10 = new XRiTextField();
    OBJ_58 = new JLabel();
    OBJ_60 = new JLabel();
    TADAPX = new XRiCalendrier();
    panel1 = new JPanel();
    TAIN1 = new XRiCheckBox();
    TAIN3 = new XRiCheckBox();
    TAIN21 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("R\u00e9f\u00e9rence tarif");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- TAIN2 ----
            TAIN2.setModel(new DefaultComboBoxModel(new String[] {
              "Pas de rattachement",
              "Rattachement magasin",
              "Rattachement type de vente"
            }));
            TAIN2.setComponentPopupMenu(BTD);
            TAIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TAIN2.setName("TAIN2");
            xTitledPanel3ContentContainer.add(TAIN2);
            TAIN2.setBounds(185, 395, 197, TAIN2.getPreferredSize().height);

            //---- TALIB ----
            TALIB.setComponentPopupMenu(BTD);
            TALIB.setName("TALIB");
            xTitledPanel3ContentContainer.add(TALIB);
            TALIB.setBounds(185, 15, 310, TALIB.getPreferredSize().height);

            //---- OBJ_98 ----
            OBJ_98.setText("Date d'application du tarif en pr\u00e9paration");
            OBJ_98.setName("OBJ_98");
            xTitledPanel3ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(15, 274, 244, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Code article support de prix");
            OBJ_56.setName("OBJ_56");
            xTitledPanel3ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(15, 50, 166, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("Type de rattachement");
            OBJ_104.setName("OBJ_104");
            xTitledPanel3ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(15, 398, 134, 20);

            //---- TAART ----
            TAART.setComponentPopupMenu(BTD);
            TAART.setName("TAART");
            xTitledPanel3ContentContainer.add(TAART);
            TAART.setBounds(185, 45, 110, TAART.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("D\u00e9signation tarif");
            OBJ_54.setName("OBJ_54");
            xTitledPanel3ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(15, 20, 115, 20);

            //---- TATIR ----
            TATIR.setComponentPopupMenu(BTD);
            TATIR.setName("TATIR");
            xTitledPanel3ContentContainer.add(TATIR);
            TATIR.setBounds(420, 394, 140, TATIR.getPreferredSize().height);

            //---- TALB1 ----
            TALB1.setComponentPopupMenu(BTD);
            TALB1.setName("TALB1");
            xTitledPanel3ContentContainer.add(TALB1);
            TALB1.setBounds(185, 110, 110, TALB1.getPreferredSize().height);

            //---- TALB6 ----
            TALB6.setComponentPopupMenu(BTD);
            TALB6.setName("TALB6");
            xTitledPanel3ContentContainer.add(TALB6);
            TALB6.setBounds(595, 110, 110, TALB6.getPreferredSize().height);

            //---- TALB2 ----
            TALB2.setComponentPopupMenu(BTD);
            TALB2.setName("TALB2");
            xTitledPanel3ContentContainer.add(TALB2);
            TALB2.setBounds(185, 140, 110, TALB2.getPreferredSize().height);

            //---- TALB7 ----
            TALB7.setComponentPopupMenu(BTD);
            TALB7.setName("TALB7");
            xTitledPanel3ContentContainer.add(TALB7);
            TALB7.setBounds(595, 140, 110, TALB7.getPreferredSize().height);

            //---- TALB3 ----
            TALB3.setComponentPopupMenu(BTD);
            TALB3.setName("TALB3");
            xTitledPanel3ContentContainer.add(TALB3);
            TALB3.setBounds(185, 170, 110, TALB3.getPreferredSize().height);

            //---- TALB8 ----
            TALB8.setComponentPopupMenu(BTD);
            TALB8.setName("TALB8");
            xTitledPanel3ContentContainer.add(TALB8);
            TALB8.setBounds(595, 170, 110, TALB8.getPreferredSize().height);

            //---- TALB4 ----
            TALB4.setComponentPopupMenu(BTD);
            TALB4.setName("TALB4");
            xTitledPanel3ContentContainer.add(TALB4);
            TALB4.setBounds(185, 200, 110, TALB4.getPreferredSize().height);

            //---- TALB9 ----
            TALB9.setComponentPopupMenu(BTD);
            TALB9.setName("TALB9");
            xTitledPanel3ContentContainer.add(TALB9);
            TALB9.setBounds(595, 200, 110, TALB9.getPreferredSize().height);

            //---- TALB5 ----
            TALB5.setComponentPopupMenu(BTD);
            TALB5.setName("TALB5");
            xTitledPanel3ContentContainer.add(TALB5);
            TALB5.setBounds(185, 230, 110, TALB5.getPreferredSize().height);

            //---- TALB10 ----
            TALB10.setComponentPopupMenu(BTD);
            TALB10.setName("TALB10");
            xTitledPanel3ContentContainer.add(TALB10);
            TALB10.setBounds(595, 230, 110, TALB10.getPreferredSize().height);

            //---- OBJ_89 ----
            OBJ_89.setText("Prix num\u00e9ro 10");
            OBJ_89.setName("OBJ_89");
            xTitledPanel3ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(420, 234, 90, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("Prix num\u00e9ro 6");
            OBJ_65.setName("OBJ_65");
            xTitledPanel3ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(420, 114, 84, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Prix num\u00e9ro 2");
            OBJ_68.setName("OBJ_68");
            xTitledPanel3ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(15, 144, 84, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("Prix num\u00e9ro 7");
            OBJ_71.setName("OBJ_71");
            xTitledPanel3ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(420, 144, 84, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Prix num\u00e9ro 3");
            OBJ_74.setName("OBJ_74");
            xTitledPanel3ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(15, 174, 84, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Prix num\u00e9ro 8");
            OBJ_77.setName("OBJ_77");
            xTitledPanel3ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(420, 174, 84, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("Prix num\u00e9ro 4");
            OBJ_80.setName("OBJ_80");
            xTitledPanel3ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(15, 204, 84, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("Prix num\u00e9ro 9");
            OBJ_83.setName("OBJ_83");
            xTitledPanel3ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(420, 204, 84, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Prix num\u00e9ro 5");
            OBJ_86.setName("OBJ_86");
            xTitledPanel3ContentContainer.add(OBJ_86);
            OBJ_86.setBounds(15, 234, 84, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("Prix num\u00e9ro 1");
            OBJ_62.setName("OBJ_62");
            xTitledPanel3ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(15, 114, 83, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("Coefficient");
            OBJ_59.setName("OBJ_59");
            xTitledPanel3ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(300, 85, 66, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Coefficient");
            OBJ_61.setName("OBJ_61");
            xTitledPanel3ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(710, 85, 66, 20);

            //---- TACO1 ----
            TACO1.setName("TACO1");
            xTitledPanel3ContentContainer.add(TACO1);
            TACO1.setBounds(300, 110, 58, TACO1.getPreferredSize().height);

            //---- TACO6 ----
            TACO6.setComponentPopupMenu(BTD);
            TACO6.setName("TACO6");
            xTitledPanel3ContentContainer.add(TACO6);
            TACO6.setBounds(710, 110, 58, TACO6.getPreferredSize().height);

            //---- TACO2 ----
            TACO2.setComponentPopupMenu(BTD);
            TACO2.setName("TACO2");
            xTitledPanel3ContentContainer.add(TACO2);
            TACO2.setBounds(300, 140, 58, TACO2.getPreferredSize().height);

            //---- TACO7 ----
            TACO7.setComponentPopupMenu(BTD);
            TACO7.setName("TACO7");
            xTitledPanel3ContentContainer.add(TACO7);
            TACO7.setBounds(710, 140, 58, TACO7.getPreferredSize().height);

            //---- TACO3 ----
            TACO3.setComponentPopupMenu(BTD);
            TACO3.setName("TACO3");
            xTitledPanel3ContentContainer.add(TACO3);
            TACO3.setBounds(300, 170, 58, TACO3.getPreferredSize().height);

            //---- TACO8 ----
            TACO8.setComponentPopupMenu(BTD);
            TACO8.setName("TACO8");
            xTitledPanel3ContentContainer.add(TACO8);
            TACO8.setBounds(710, 170, 58, TACO8.getPreferredSize().height);

            //---- TACO4 ----
            TACO4.setComponentPopupMenu(BTD);
            TACO4.setName("TACO4");
            xTitledPanel3ContentContainer.add(TACO4);
            TACO4.setBounds(300, 200, 58, TACO4.getPreferredSize().height);

            //---- TACO9 ----
            TACO9.setComponentPopupMenu(BTD);
            TACO9.setName("TACO9");
            xTitledPanel3ContentContainer.add(TACO9);
            TACO9.setBounds(710, 200, 58, TACO9.getPreferredSize().height);

            //---- TACO5 ----
            TACO5.setComponentPopupMenu(BTD);
            TACO5.setName("TACO5");
            xTitledPanel3ContentContainer.add(TACO5);
            TACO5.setBounds(300, 230, 58, TACO5.getPreferredSize().height);

            //---- TACO10 ----
            TACO10.setComponentPopupMenu(BTD);
            TACO10.setName("TACO10");
            xTitledPanel3ContentContainer.add(TACO10);
            TACO10.setBounds(710, 230, 58, TACO10.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("Libell\u00e9");
            OBJ_58.setName("OBJ_58");
            xTitledPanel3ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(185, 85, 43, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Libell\u00e9");
            OBJ_60.setName("OBJ_60");
            xTitledPanel3ContentContainer.add(OBJ_60);
            OBJ_60.setBounds(595, 85, 43, 20);

            //---- TADAPX ----
            TADAPX.setName("TADAPX");
            xTitledPanel3ContentContainer.add(TADAPX);
            TADAPX.setBounds(300, 270, 105, TADAPX.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- TAIN1 ----
              TAIN1.setText("Mise \u00e0 jour auto Euro/Devise locale");
              TAIN1.setComponentPopupMenu(BTD);
              TAIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TAIN1.setName("TAIN1");
              panel1.add(TAIN1);
              TAIN1.setBounds(5, 20, 237, 20);

              //---- TAIN3 ----
              TAIN3.setText("Tarif en pr\u00e9paration si commande sur stock nul");
              TAIN3.setComponentPopupMenu(BTD);
              TAIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TAIN3.setName("TAIN3");
              panel1.add(TAIN3);
              TAIN3.setBounds(255, 20, 290, 20);

              //---- TAIN21 ----
              TAIN21.setText("Gestion des tarifs par quantit\u00e9s");
              TAIN21.setComponentPopupMenu(BTD);
              TAIN21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TAIN21.setName("TAIN21");
              TAIN21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  TAIN21ActionPerformed(e);
                }
              });
              panel1.add(TAIN21);
              TAIN21.setBounds(560, 20, 215, 20);
            }
            xTitledPanel3ContentContainer.add(panel1);
            panel1.setBounds(5, 315, 780, 60);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(TAIN1);
    buttonGroup1.add(TAIN3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private XRiComboBox TAIN2;
  private XRiTextField TALIB;
  private JLabel OBJ_98;
  private JLabel OBJ_56;
  private JLabel OBJ_104;
  private XRiTextField TAART;
  private JLabel OBJ_54;
  private XRiTextField TATIR;
  private XRiTextField TALB1;
  private XRiTextField TALB6;
  private XRiTextField TALB2;
  private XRiTextField TALB7;
  private XRiTextField TALB3;
  private XRiTextField TALB8;
  private XRiTextField TALB4;
  private XRiTextField TALB9;
  private XRiTextField TALB5;
  private XRiTextField TALB10;
  private JLabel OBJ_89;
  private JLabel OBJ_65;
  private JLabel OBJ_68;
  private JLabel OBJ_71;
  private JLabel OBJ_74;
  private JLabel OBJ_77;
  private JLabel OBJ_80;
  private JLabel OBJ_83;
  private JLabel OBJ_86;
  private JLabel OBJ_62;
  private JLabel OBJ_59;
  private JLabel OBJ_61;
  private XRiTextField TACO1;
  private XRiTextField TACO6;
  private XRiTextField TACO2;
  private XRiTextField TACO7;
  private XRiTextField TACO3;
  private XRiTextField TACO8;
  private XRiTextField TACO4;
  private XRiTextField TACO9;
  private XRiTextField TACO5;
  private XRiTextField TACO10;
  private JLabel OBJ_58;
  private JLabel OBJ_60;
  private XRiCalendrier TADAPX;
  private JPanel panel1;
  private XRiCheckBox TAIN1;
  private XRiCheckBox TAIN3;
  private XRiCheckBox TAIN21;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
