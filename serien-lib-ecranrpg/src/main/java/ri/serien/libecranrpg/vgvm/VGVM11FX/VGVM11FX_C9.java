
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_C9 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_C9(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(button1);
    
    setDialog(true);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WASB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WASB@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    WSTKAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKAX@")).trim());
    WDISAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISAX@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    L1LIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB1@")).trim());
    L1LIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB2@")).trim());
    L1LIB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB3@")).trim());
    L1LIB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB4@")).trim());
    WSTKX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WDISX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WASB = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    label5 = new JLabel();
    WSTKAX = new RiZoneSortie();
    label6 = new JLabel();
    WDISAX = new RiZoneSortie();
    label7 = new JLabel();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    button2 = new JButton();
    panel1 = new JPanel();
    WARTT = new RiZoneSortie();
    label2 = new JLabel();
    L1LIB1 = new RiZoneSortie();
    L1LIB2 = new RiZoneSortie();
    L1LIB3 = new RiZoneSortie();
    L1LIB4 = new RiZoneSortie();
    WSTKX = new RiZoneSortie();
    WDISX = new RiZoneSortie();
    label3 = new JLabel();
    label4 = new JLabel();
    button1 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(990, 300));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Aiguill\u00e9 sur l'article"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WASB ----
          WASB.setComponentPopupMenu(null);
          WASB.setText("@WASB@");
          WASB.setName("WASB");
          panel2.add(WASB);
          WASB.setBounds(20, 50, 190, WASB.getPreferredSize().height);

          //---- A1LIB ----
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          panel2.add(A1LIB);
          A1LIB.setBounds(20, 85, 350, A1LIB.getPreferredSize().height);

          //---- A1LB1 ----
          A1LB1.setText("@A1LB1@");
          A1LB1.setName("A1LB1");
          panel2.add(A1LB1);
          A1LB1.setBounds(20, 110, 350, A1LB1.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Stock");
          label5.setName("label5");
          panel2.add(label5);
          label5.setBounds(20, 230, 50, 25);

          //---- WSTKAX ----
          WSTKAX.setText("@WSTKAX@");
          WSTKAX.setHorizontalAlignment(SwingConstants.RIGHT);
          WSTKAX.setName("WSTKAX");
          panel2.add(WSTKAX);
          WSTKAX.setBounds(new Rectangle(new Point(70, 230), WSTKAX.getPreferredSize()));

          //---- label6 ----
          label6.setText("Disponible");
          label6.setName("label6");
          panel2.add(label6);
          label6.setBounds(195, 230, 70, 25);

          //---- WDISAX ----
          WDISAX.setText("@WDISAX@");
          WDISAX.setHorizontalAlignment(SwingConstants.RIGHT);
          WDISAX.setName("WDISAX");
          panel2.add(WDISAX);
          WDISAX.setBounds(new Rectangle(new Point(270, 230), WDISAX.getPreferredSize()));

          //---- label7 ----
          label7.setText("Code");
          label7.setName("label7");
          panel2.add(label7);
          label7.setBounds(20, 25, 190, 25);

          //---- A1LB2 ----
          A1LB2.setText("@A1LB2@");
          A1LB2.setName("A1LB2");
          panel2.add(A1LB2);
          A1LB2.setBounds(20, 135, 350, A1LB2.getPreferredSize().height);

          //---- A1LB3 ----
          A1LB3.setText("@A1LB3@");
          A1LB3.setName("A1LB3");
          panel2.add(A1LB3);
          A1LB3.setBounds(20, 160, 350, A1LB3.getPreferredSize().height);

          //---- button2 ----
          button2.setText("Choisir cet article");
          button2.setFont(button2.getFont().deriveFont(button2.getFont().getStyle() | Font.BOLD));
          button2.setName("button2");
          button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button2ActionPerformed(e);
            }
          });
          panel2.add(button2);
          button2.setBounds(180, 190, 190, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(415, 10, 395, 280);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Article"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(null);
          WARTT.setText("@WARTT@");
          WARTT.setName("WARTT");
          panel1.add(WARTT);
          WARTT.setBounds(20, 50, 190, WARTT.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Code");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(20, 25, 190, 25);

          //---- L1LIB1 ----
          L1LIB1.setText("@L1LIB1@");
          L1LIB1.setName("L1LIB1");
          panel1.add(L1LIB1);
          L1LIB1.setBounds(20, 85, 350, L1LIB1.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setText("@L1LIB2@");
          L1LIB2.setName("L1LIB2");
          panel1.add(L1LIB2);
          L1LIB2.setBounds(20, 110, 350, L1LIB2.getPreferredSize().height);

          //---- L1LIB3 ----
          L1LIB3.setText("@L1LIB3@");
          L1LIB3.setName("L1LIB3");
          panel1.add(L1LIB3);
          L1LIB3.setBounds(20, 135, 350, L1LIB3.getPreferredSize().height);

          //---- L1LIB4 ----
          L1LIB4.setText("@L1LIB4@");
          L1LIB4.setName("L1LIB4");
          panel1.add(L1LIB4);
          L1LIB4.setBounds(20, 160, 350, L1LIB4.getPreferredSize().height);

          //---- WSTKX ----
          WSTKX.setText("@WSTKX@");
          WSTKX.setHorizontalAlignment(SwingConstants.RIGHT);
          WSTKX.setName("WSTKX");
          panel1.add(WSTKX);
          WSTKX.setBounds(70, 230, 100, WSTKX.getPreferredSize().height);

          //---- WDISX ----
          WDISX.setText("@WDISX@");
          WDISX.setHorizontalAlignment(SwingConstants.RIGHT);
          WDISX.setName("WDISX");
          panel1.add(WDISX);
          WDISX.setBounds(new Rectangle(new Point(270, 230), WDISX.getPreferredSize()));

          //---- label3 ----
          label3.setText("Stock");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(20, 230, 50, 25);

          //---- label4 ----
          label4.setText("Disponible");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(195, 230, 70, 25);

          //---- button1 ----
          button1.setText("Choisir cet article");
          button1.setFont(button1.getFont().deriveFont(button1.getFont().getStyle() | Font.BOLD));
          button1.setName("button1");
          button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              button1ActionPerformed(e);
            }
          });
          panel1.add(button1);
          button1.setBounds(180, 190, 190, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 395, 280);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie WASB;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private JLabel label5;
  private RiZoneSortie WSTKAX;
  private JLabel label6;
  private RiZoneSortie WDISAX;
  private JLabel label7;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private JButton button2;
  private JPanel panel1;
  private RiZoneSortie WARTT;
  private JLabel label2;
  private RiZoneSortie L1LIB1;
  private RiZoneSortie L1LIB2;
  private RiZoneSortie L1LIB3;
  private RiZoneSortie L1LIB4;
  private RiZoneSortie WSTKX;
  private RiZoneSortie WDISX;
  private JLabel label3;
  private JLabel label4;
  private JButton button1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
