
package ri.serien.libecranrpg.vgvm.VGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM13FM_GT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM13FM_GT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    U09.setVisible(lexique.isPresent("U09"));
    U08.setVisible(lexique.isPresent("U08"));
    U07.setVisible(lexique.isPresent("U07"));
    U06.setVisible(lexique.isPresent("U06"));
    U05.setVisible(lexique.isPresent("U05"));
    U04.setVisible(lexique.isPresent("U04"));
    U03.setVisible(lexique.isPresent("U03"));
    U02.setVisible(lexique.isPresent("U02"));
    U01.setVisible(lexique.isPresent("U01"));
    DLMAXX.setVisible(lexique.isPresent("DLMAXX"));
    DLMINX.setVisible(lexique.isPresent("DLMINX"));
    NBBON.setVisible(lexique.isPresent("NBBON"));
    M2PT.setVisible(lexique.isPresent("M2PT"));
    LGMX.setVisible(lexique.isPresent("LGMX"));
    VOLT.setVisible(lexique.isPresent("VOLT"));
    PDST.setVisible(lexique.isPresent("PDST"));
    TOT1.setVisible(lexique.isPresent("TOT1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("PREPARATION"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    L01 = new XRiTextField();
    L02 = new XRiTextField();
    L03 = new XRiTextField();
    L04 = new XRiTextField();
    L05 = new XRiTextField();
    L06 = new XRiTextField();
    L07 = new XRiTextField();
    L08 = new XRiTextField();
    L09 = new XRiTextField();
    TOT1 = new XRiTextField();
    OBJ_24 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_56 = new JXTitledSeparator();
    OBJ_33 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_27 = new JLabel();
    PDST = new XRiTextField();
    VOLT = new XRiTextField();
    LGMX = new XRiTextField();
    M2PT = new XRiTextField();
    NBBON = new XRiTextField();
    DLMINX = new XRiTextField();
    DLMAXX = new XRiTextField();
    U01 = new XRiTextField();
    U02 = new XRiTextField();
    U03 = new XRiTextField();
    U04 = new XRiTextField();
    U05 = new XRiTextField();
    U06 = new XRiTextField();
    U07 = new XRiTextField();
    U08 = new XRiTextField();
    U09 = new XRiTextField();
    OBJ_22 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Information pr\u00e9paration");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder("Synth\u00e8se des commandes s\u00e9lectionn\u00e9es"));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- L01 ----
          L01.setName("L01");
          p_recup.add(L01);
          L01.setBounds(160, 295, 160, L01.getPreferredSize().height);

          //---- L02 ----
          L02.setName("L02");
          p_recup.add(L02);
          L02.setBounds(160, 325, 160, L02.getPreferredSize().height);

          //---- L03 ----
          L03.setName("L03");
          p_recup.add(L03);
          L03.setBounds(160, 355, 160, L03.getPreferredSize().height);

          //---- L04 ----
          L04.setName("L04");
          p_recup.add(L04);
          L04.setBounds(160, 385, 160, L04.getPreferredSize().height);

          //---- L05 ----
          L05.setName("L05");
          p_recup.add(L05);
          L05.setBounds(160, 415, 160, L05.getPreferredSize().height);

          //---- L06 ----
          L06.setName("L06");
          p_recup.add(L06);
          L06.setBounds(160, 445, 160, L06.getPreferredSize().height);

          //---- L07 ----
          L07.setName("L07");
          p_recup.add(L07);
          L07.setBounds(160, 475, 160, L07.getPreferredSize().height);

          //---- L08 ----
          L08.setName("L08");
          p_recup.add(L08);
          L08.setBounds(160, 505, 160, L08.getPreferredSize().height);

          //---- L09 ----
          L09.setName("L09");
          p_recup.add(L09);
          L09.setBounds(160, 535, 160, L09.getPreferredSize().height);

          //---- TOT1 ----
          TOT1.setName("TOT1");
          p_recup.add(TOT1);
          TOT1.setBounds(160, 75, 155, TOT1.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("Chiffre d'affaire HT");
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(25, 79, 114, 20);

          //---- OBJ_58 ----
          OBJ_58.setText("Nombre de bons");
          OBJ_58.setName("OBJ_58");
          p_recup.add(OBJ_58);
          OBJ_58.setBounds(25, 109, 114, 20);

          //---- OBJ_56 ----
          OBJ_56.setTitle("Unit\u00e9(s) de transport");
          OBJ_56.setName("OBJ_56");
          p_recup.add(OBJ_56);
          OBJ_56.setBounds(25, 265, 320, 16);

          //---- OBJ_33 ----
          OBJ_33.setText("Surface plancher");
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(25, 229, 105, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("Livraisons du");
          OBJ_20.setName("OBJ_20");
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(25, 49, 100, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Volume total");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(25, 169, 96, 20);

          //---- OBJ_31 ----
          OBJ_31.setText("Longueur max");
          OBJ_31.setName("OBJ_31");
          p_recup.add(OBJ_31);
          OBJ_31.setBounds(25, 199, 96, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Poids  total");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(25, 139, 88, 20);

          //---- PDST ----
          PDST.setName("PDST");
          p_recup.add(PDST);
          PDST.setBounds(160, 135, 70, PDST.getPreferredSize().height);

          //---- VOLT ----
          VOLT.setName("VOLT");
          p_recup.add(VOLT);
          VOLT.setBounds(160, 165, 70, VOLT.getPreferredSize().height);

          //---- LGMX ----
          LGMX.setName("LGMX");
          p_recup.add(LGMX);
          LGMX.setBounds(160, 195, 70, LGMX.getPreferredSize().height);

          //---- M2PT ----
          M2PT.setName("M2PT");
          p_recup.add(M2PT);
          M2PT.setBounds(160, 225, 70, M2PT.getPreferredSize().height);

          //---- NBBON ----
          NBBON.setName("NBBON");
          p_recup.add(NBBON);
          NBBON.setBounds(160, 105, 65, NBBON.getPreferredSize().height);

          //---- DLMINX ----
          DLMINX.setName("DLMINX");
          p_recup.add(DLMINX);
          DLMINX.setBounds(160, 45, 52, DLMINX.getPreferredSize().height);

          //---- DLMAXX ----
          DLMAXX.setName("DLMAXX");
          p_recup.add(DLMAXX);
          DLMAXX.setBounds(273, 45, 52, DLMAXX.getPreferredSize().height);

          //---- U01 ----
          U01.setName("U01");
          p_recup.add(U01);
          U01.setBounds(120, 295, 34, U01.getPreferredSize().height);

          //---- U02 ----
          U02.setName("U02");
          p_recup.add(U02);
          U02.setBounds(120, 325, 34, U02.getPreferredSize().height);

          //---- U03 ----
          U03.setName("U03");
          p_recup.add(U03);
          U03.setBounds(120, 355, 34, U03.getPreferredSize().height);

          //---- U04 ----
          U04.setName("U04");
          p_recup.add(U04);
          U04.setBounds(120, 385, 34, U04.getPreferredSize().height);

          //---- U05 ----
          U05.setName("U05");
          p_recup.add(U05);
          U05.setBounds(120, 415, 34, U05.getPreferredSize().height);

          //---- U06 ----
          U06.setName("U06");
          p_recup.add(U06);
          U06.setBounds(120, 445, 34, U06.getPreferredSize().height);

          //---- U07 ----
          U07.setName("U07");
          p_recup.add(U07);
          U07.setBounds(120, 475, 34, U07.getPreferredSize().height);

          //---- U08 ----
          U08.setName("U08");
          p_recup.add(U08);
          U08.setBounds(120, 505, 34, U08.getPreferredSize().height);

          //---- U09 ----
          U09.setName("U09");
          p_recup.add(U09);
          U09.setBounds(120, 535, 34, U09.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("au");
          OBJ_22.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(235, 49, 25, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 366, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(24, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 592, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(23, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField L01;
  private XRiTextField L02;
  private XRiTextField L03;
  private XRiTextField L04;
  private XRiTextField L05;
  private XRiTextField L06;
  private XRiTextField L07;
  private XRiTextField L08;
  private XRiTextField L09;
  private XRiTextField TOT1;
  private JLabel OBJ_24;
  private JLabel OBJ_58;
  private JXTitledSeparator OBJ_56;
  private JLabel OBJ_33;
  private JLabel OBJ_20;
  private JLabel OBJ_29;
  private JLabel OBJ_31;
  private JLabel OBJ_27;
  private XRiTextField PDST;
  private XRiTextField VOLT;
  private XRiTextField LGMX;
  private XRiTextField M2PT;
  private XRiTextField NBBON;
  private XRiTextField DLMINX;
  private XRiTextField DLMAXX;
  private XRiTextField U01;
  private XRiTextField U02;
  private XRiTextField U03;
  private XRiTextField U04;
  private XRiTextField U05;
  private XRiTextField U06;
  private XRiTextField U07;
  private XRiTextField U08;
  private XRiTextField U09;
  private JLabel OBJ_22;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
