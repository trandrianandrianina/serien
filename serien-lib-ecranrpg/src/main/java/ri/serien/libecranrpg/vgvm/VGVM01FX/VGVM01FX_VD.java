
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.stock.sntypestockagemagasin.SNTypeStockageMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_VD extends SNPanelEcranRPG implements ioFrame {
  
  private String[] VDTIC5_Value = { " ", "1", "2", };
  private String[] VDCPT5_Value = { " ", "1", "2", };
  private String[] VDFAC5_Value = { " ", "1", "2", };
  private String[] VDEXP5_Value = { " ", "1", "2", };
  private String[] VDHOM5_Value = { " ", "1", "2", };
  private String[] VDDEV5_Value = { " ", "1", "2", };
  private String[] VDTIC2_Value = { " ", "1", "2", "3", "4", "5", };
  private String[] VDTIC3_Value = { " ", "1", "2", "3", "4", "5" };
  private String[] VDCPT3_Value = { " ", "1", "2", "3", "4", "5" };
  private String[] VDFAC3_Value = { " ", "1", "2", "3", "4", "5" };
  private String[] VDEXP3_Value = { " ", "1", "2", "3", "4", "5" };
  private String[] VDHOM3_Value = { " ", "1", "2", "3", "4", "5" };
  private String[] VDDEV3_Value = { " ", "1", "2", "3", "4", "5" };
  private String[] VDCPT2_Value = { " ", "1", "2", "3", "4", "5", };
  private String[] VDFAC2_Value = { " ", "1", "2", "3", "4", "5", };
  private String[] VDEXP2_Value = { " ", "1", "2", "3", "4", "5", };
  private String[] VDHOM2_Value = { " ", "1", "2", "3", "4", "5", };
  private String[] VDDEV2_Value = { " ", "1", "2", "3", "4", "5", };
  private String[] VDTIC4_Value = { " ", "1", "2", "3", "4", "5", "6", };
  private String[] VDCPT4_Value = { " ", "1", "2", "3", "4", "5", "6", };
  private String[] VDFAC4_Value = { " ", "1", "2", "3", "4", "5", "6", };
  private String[] VDEXP4_Value = { " ", "1", "2", "3", "4", "5", "6", };
  private String[] VDHOM4_Value = { " ", "1", "2", "3", "4", "5", "6", };
  private String[] VDDEV4_Value = { " ", "1", "2", "3", "4", "5", "6", };
  private String[] VDRG4_Value = { " ", "1", "2", "3", };
  private String[] VDVDP_Value = { " ", "J", "S", "M" };
  
  private boolean isConsultation = false;
  
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  public VGVM01FX_VD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    VDTIC5.setValeurs(VDTIC5_Value, null);
    VDCPT5.setValeurs(VDCPT5_Value, null);
    VDFAC5.setValeurs(VDFAC5_Value, null);
    VDEXP5.setValeurs(VDEXP5_Value, null);
    VDHOM5.setValeurs(VDHOM5_Value, null);
    VDDEV5.setValeurs(VDDEV5_Value, null);
    VDTIC2.setValeurs(VDTIC2_Value, null);
    VDTIC3.setValeurs(VDTIC3_Value, null);
    VDCPT3.setValeurs(VDCPT3_Value, null);
    VDFAC3.setValeurs(VDFAC3_Value, null);
    VDEXP3.setValeurs(VDEXP3_Value, null);
    VDHOM3.setValeurs(VDHOM3_Value, null);
    VDDEV3.setValeurs(VDDEV3_Value, null);
    VDCPT2.setValeurs(VDCPT2_Value, null);
    VDFAC2.setValeurs(VDFAC2_Value, null);
    VDEXP2.setValeurs(VDEXP2_Value, null);
    VDHOM2.setValeurs(VDHOM2_Value, null);
    VDDEV2.setValeurs(VDDEV2_Value, null);
    VDTIC4.setValeurs(VDTIC4_Value, null);
    VDCPT4.setValeurs(VDCPT4_Value, null);
    VDFAC4.setValeurs(VDFAC4_Value, null);
    VDEXP4.setValeurs(VDEXP4_Value, null);
    VDHOM4.setValeurs(VDHOM4_Value, null);
    VDDEV4.setValeurs(VDDEV4_Value, null);
    VDVDP.setValeurs(VDVDP_Value, null);
    VDMAV.setValeursSelection("X", " ");
    VDHOM.setValeursSelection("X", " ");
    VDCHQ.setValeursSelection("O", "N");
    VDRG4.setValeurs(VDRG4_Value, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    // Initialisation des types de stockage magasin
    snTypeStockageMagasin1.setSession(getSession());
    snTypeStockageMagasin1.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeStockageMagasin1.charger(false);
    snTypeStockageMagasin1.setSelectionParChampRPG(lexique, "VDMS1");
    snTypeStockageMagasin1.setEnabled(!isConsultation);
    
    snTypeStockageMagasin2.setSession(getSession());
    snTypeStockageMagasin2.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeStockageMagasin2.charger(false);
    snTypeStockageMagasin2.setSelectionParChampRPG(lexique, "VDMS2");
    snTypeStockageMagasin2.setEnabled(!isConsultation);
    
    snTypeStockageMagasin3.setSession(getSession());
    snTypeStockageMagasin3.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeStockageMagasin3.charger(false);
    snTypeStockageMagasin3.setSelectionParChampRPG(lexique, "VDMS3");
    snTypeStockageMagasin3.setEnabled(!isConsultation);
    
    snTypeStockageMagasin4.setSession(getSession());
    snTypeStockageMagasin4.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeStockageMagasin4.charger(false);
    snTypeStockageMagasin4.setSelectionParChampRPG(lexique, "VDMS4");
    snTypeStockageMagasin4.setEnabled(!isConsultation);
    
    snTypeStockageMagasin5.setSession(getSession());
    snTypeStockageMagasin5.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeStockageMagasin5.charger(false);
    snTypeStockageMagasin5.setSelectionParChampRPG(lexique, "VDMS5");
    snTypeStockageMagasin5.setEnabled(!isConsultation);
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    

    if (snEtablissement.getIdSelection() != null) {
      p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snTypeStockageMagasin1.renseignerChampRPG(lexique, "VDMS1");
    snTypeStockageMagasin2.renseignerChampRPG(lexique, "VDMS2");
    snTypeStockageMagasin3.renseignerChampRPG(lexique, "VDMS3");
    snTypeStockageMagasin4.renseignerChampRPG(lexique, "VDMS4");
    snTypeStockageMagasin5.renseignerChampRPG(lexique, "VDMS5");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlbandeau = new SNPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    lbLibelle = new SNLabelChamp();
    CCLIB = new XRiTextField();
    sNPanel1 = new SNPanel();
    lbNom = new SNLabelChamp();
    VDLIB = new XRiTextField();
    lbTelephone = new SNLabelChamp();
    VDTEL = new XRiTextField();
    lbMail = new SNLabelChamp();
    VDMAI = new XRiTextField();
    tabbedPane1 = new JTabbedPane();
    pnlGeneral = new SNPanelContenu();
    lbPourcentageMaxiRemise2 = new SNLabelChamp();
    pnlValidite = new SNPanel();
    VDVDV = new XRiTextField();
    VDVDP = new XRiComboBox();
    lbPourcentageMaxiRemise = new SNLabelChamp();
    pnlRemiseMax = new SNPanel();
    VDPMR = new XRiTextField();
    lbPourcent = new SNLabelUnite();
    lbJOCaisse = new SNLabelChamp();
    VDCJO = new XRiTextField();
    lbReglementGEMDirect = new SNLabelChamp();
    VDRG4 = new XRiComboBox();
    lbRechercheRestreinte = new SNLabelChamp();
    VDZP1 = new XRiTextField();
    VDCHQ = new XRiCheckBox();
    VDMAV = new XRiCheckBox();
    pnlDocuments = new SNPanelContenu();
    sNPanel3 = new SNPanel();
    lbReglementGEMDirect2 = new SNLabelChamp();
    sNPanel2 = new SNPanel();
    VDO01 = new XRiTextField();
    VDO02 = new XRiTextField();
    VDO03 = new XRiTextField();
    VDO04 = new XRiTextField();
    VDO05 = new XRiTextField();
    VDO06 = new XRiTextField();
    VDO07 = new XRiTextField();
    VDO08 = new XRiTextField();
    VDO09 = new XRiTextField();
    VDO10 = new XRiTextField();
    VDHOM = new XRiCheckBox();
    sNPanelTitre1 = new SNPanelTitre();
    lbenteteRechercheClient = new SNLabelUnite();
    lbLigne = new SNLabelUnite();
    lbComplementLigne = new SNLabelUnite();
    lbComplementEntete = new SNLabelUnite();
    lbOptions = new SNLabelUnite();
    lbDevis = new SNLabelChamp();
    VDDEV2 = new XRiComboBox();
    VDDEV3 = new XRiComboBox();
    VDDEV4 = new XRiComboBox();
    VDDEV5 = new XRiComboBox();
    VDDEV1 = new XRiTextField();
    lbCommande = new SNLabelChamp();
    VDHOM2 = new XRiComboBox();
    VDHOM3 = new XRiComboBox();
    VDHOM4 = new XRiComboBox();
    VDHOM5 = new XRiComboBox();
    VDHOM1 = new XRiTextField();
    lbLivraisons = new SNLabelChamp();
    VDEXP2 = new XRiComboBox();
    VDEXP3 = new XRiComboBox();
    VDEXP4 = new XRiComboBox();
    VDEXP5 = new XRiComboBox();
    VDEXP1 = new XRiTextField();
    lbFactures = new SNLabelChamp();
    VDFAC2 = new XRiComboBox();
    VDFAC3 = new XRiComboBox();
    VDFAC4 = new XRiComboBox();
    VDFAC5 = new XRiComboBox();
    VDFAC1 = new XRiTextField();
    lbFacturesDirectes = new SNLabelChamp();
    VDCPT2 = new XRiComboBox();
    VDCPT3 = new XRiComboBox();
    VDCPT4 = new XRiComboBox();
    VDCPT5 = new XRiComboBox();
    VDCPT1 = new XRiTextField();
    lbTickets = new SNLabelChamp();
    VDTIC2 = new XRiComboBox();
    VDTIC3 = new XRiComboBox();
    VDTIC4 = new XRiComboBox();
    VDTIC5 = new XRiComboBox();
    VDTIC1 = new XRiTextField();
    pnlPreparateur = new SNPanelContenu();
    lbTypeStockage1 = new SNLabelChamp();
    snTypeStockageMagasin1 = new SNTypeStockageMagasin();
    lbTypeStockage2 = new SNLabelChamp();
    snTypeStockageMagasin2 = new SNTypeStockageMagasin();
    lbTypeStockage3 = new SNLabelChamp();
    snTypeStockageMagasin3 = new SNTypeStockageMagasin();
    lbTypeStockage4 = new SNLabelChamp();
    snTypeStockageMagasin4 = new SNTypeStockageMagasin();
    lbTypeStockage5 = new SNLabelChamp();
    snTypeStockageMagasin5 = new SNTypeStockageMagasin();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlbandeau ========
    {
      pnlbandeau.setName("pnlbandeau");
      pnlbandeau.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      pnlbandeau.add(p_bpresentation);
    }
    add(pnlbandeau, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeCategorie ----
        lbCodeCategorie.setText("code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- INDIND ----
        INDIND.setPreferredSize(new Dimension(40, 30));
        INDIND.setMinimumSize(new Dimension(40, 30));
        INDIND.setMaximumSize(new Dimension(40, 30));
        INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDIND.setName("INDIND");
        pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setMinimumSize(new Dimension(80, 30));
        lbLibelle.setMaximumSize(new Dimension(80, 30));
        lbLibelle.setPreferredSize(new Dimension(80, 30));
        lbLibelle.setName("lbLibelle");
        pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- CCLIB ----
        CCLIB.setPreferredSize(new Dimension(400, 30));
        CCLIB.setMinimumSize(new Dimension(400, 30));
        CCLIB.setMaximumSize(new Dimension(400, 30));
        CCLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
        CCLIB.setName("CCLIB");
        pnlPersonnalisation.add(CCLIB, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlPersonnalisation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNom ----
        lbNom.setText("Nom");
        lbNom.setName("lbNom");
        sNPanel1.add(lbNom, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- VDLIB ----
        VDLIB.setPreferredSize(new Dimension(350, 30));
        VDLIB.setMinimumSize(new Dimension(350, 30));
        VDLIB.setName("VDLIB");
        sNPanel1.add(VDLIB, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setName("lbTelephone");
        sNPanel1.add(lbTelephone, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- VDTEL ----
        VDTEL.setMinimumSize(new Dimension(210, 30));
        VDTEL.setMaximumSize(new Dimension(210, 30));
        VDTEL.setPreferredSize(new Dimension(210, 30));
        VDTEL.setName("VDTEL");
        sNPanel1.add(VDTEL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbMail ----
        lbMail.setText("Mail");
        lbMail.setName("lbMail");
        sNPanel1.add(lbMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- VDMAI ----
        VDMAI.setPreferredSize(new Dimension(450, 30));
        VDMAI.setMinimumSize(new Dimension(450, 30));
        VDMAI.setName("VDMAI");
        sNPanel1.add(VDMAI, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelContenu1.add(sNPanel1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
        tabbedPane1.setName("tabbedPane1");
        
        // ======== pnlGeneral ========
        {
          pnlGeneral.setName("pnlGeneral");
          pnlGeneral.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPourcentageMaxiRemise2 ----
          lbPourcentageMaxiRemise2.setText("Dur\u00e9e par d\u00e9faut de la validit\u00e9 des devis");
          lbPourcentageMaxiRemise2.setPreferredSize(new Dimension(300, 30));
          lbPourcentageMaxiRemise2.setMinimumSize(new Dimension(300, 30));
          lbPourcentageMaxiRemise2.setMaximumSize(new Dimension(350, 30));
          lbPourcentageMaxiRemise2.setName("lbPourcentageMaxiRemise2");
          pnlGeneral.add(lbPourcentageMaxiRemise2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlValidite ========
          {
            pnlValidite.setName("pnlValidite");
            pnlValidite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlValidite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlValidite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlValidite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlValidite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- VDVDV ----
            VDVDV.setMinimumSize(new Dimension(60, 30));
            VDVDV.setMaximumSize(new Dimension(60, 30));
            VDVDV.setPreferredSize(new Dimension(60, 30));
            VDVDV.setName("VDVDV");
            pnlValidite.add(VDVDV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- VDVDP ----
            VDVDP.setModel(new DefaultComboBoxModel(new String[] { "  ", "Jours", "Semaines", "Mois" }));
            VDVDP.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDVDP.setBackground(Color.white);
            VDVDP.setName("VDVDP");
            pnlValidite.add(VDVDP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGeneral.add(pnlValidite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPourcentageMaxiRemise ----
          lbPourcentageMaxiRemise.setText("Pourcentage maximal de remise autoris\u00e9");
          lbPourcentageMaxiRemise.setName("lbPourcentageMaxiRemise");
          pnlGeneral.add(lbPourcentageMaxiRemise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlRemiseMax ========
          {
            pnlRemiseMax.setName("pnlRemiseMax");
            pnlRemiseMax.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemiseMax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRemiseMax.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemiseMax.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemiseMax.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- VDPMR ----
            VDPMR.setPreferredSize(new Dimension(40, 30));
            VDPMR.setMinimumSize(new Dimension(40, 30));
            VDPMR.setName("VDPMR");
            pnlRemiseMax.add(VDPMR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcent ----
            lbPourcent.setText("%");
            lbPourcent.setName("lbPourcent");
            pnlRemiseMax.add(lbPourcent, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGeneral.add(pnlRemiseMax, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbJOCaisse ----
          lbJOCaisse.setText("Journal de comptabilisation de caisse");
          lbJOCaisse.setMinimumSize(new Dimension(250, 30));
          lbJOCaisse.setMaximumSize(new Dimension(250, 30));
          lbJOCaisse.setPreferredSize(new Dimension(250, 30));
          lbJOCaisse.setName("lbJOCaisse");
          pnlGeneral.add(lbJOCaisse, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- VDCJO ----
          VDCJO.setPreferredSize(new Dimension(40, 30));
          VDCJO.setMinimumSize(new Dimension(40, 30));
          VDCJO.setMaximumSize(new Dimension(40, 30));
          VDCJO.setName("VDCJO");
          pnlGeneral.add(VDCJO, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbReglementGEMDirect ----
          lbReglementGEMDirect.setText("R\u00e8glement GEM direct");
          lbReglementGEMDirect.setPreferredSize(new Dimension(200, 30));
          lbReglementGEMDirect.setMinimumSize(new Dimension(200, 30));
          lbReglementGEMDirect.setMaximumSize(new Dimension(200, 30));
          lbReglementGEMDirect.setName("lbReglementGEMDirect");
          pnlGeneral.add(lbReglementGEMDirect, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- VDRG4 ----
          VDRG4.setModel(new DefaultComboBoxModel(new String[] { " Non", "Acc\u00e8s aux r\u00e8glements GEM depuis le bon",
              "G\u00e9n\u00e9ration automatique du r\u00e8glement dans GEM (sans obligation de saisir les r\u00e9f\u00e9rences du ch\u00e8que)",
              "Acc\u00e8s syst\u00e9matique aux r\u00e8glements de GEM" }));
          VDRG4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VDRG4.setFont(new Font("sansserif", Font.PLAIN, 14));
          VDRG4.setBackground(Color.white);
          VDRG4.setName("VDRG4");
          pnlGeneral.add(VDRG4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRechercheRestreinte ----
          lbRechercheRestreinte.setText("Recherche d'articles restreinte \u00e0 la ZP1");
          lbRechercheRestreinte.setName("lbRechercheRestreinte");
          pnlGeneral.add(lbRechercheRestreinte, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- VDZP1 ----
          VDZP1.setToolTipText("zone personnelle");
          VDZP1.setMinimumSize(new Dimension(40, 30));
          VDZP1.setMaximumSize(new Dimension(40, 30));
          VDZP1.setPreferredSize(new Dimension(40, 30));
          VDZP1.setName("VDZP1");
          pnlGeneral.add(VDZP1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- VDCHQ ----
          VDCHQ.setText("Remises de ch\u00e8ques autoris\u00e9es sur ce vendeur");
          VDCHQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VDCHQ.setFont(new Font("sansserif", Font.PLAIN, 14));
          VDCHQ.setMinimumSize(new Dimension(350, 30));
          VDCHQ.setMaximumSize(new Dimension(350, 30));
          VDCHQ.setPreferredSize(new Dimension(350, 30));
          VDCHQ.setName("VDCHQ");
          pnlGeneral.add(VDCHQ, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- VDMAV ----
          VDMAV.setText("Afficher les informations techniques sur les lignes de commandes");
          VDMAV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VDMAV.setFont(new Font("sansserif", Font.PLAIN, 14));
          VDMAV.setPreferredSize(new Dimension(350, 30));
          VDMAV.setMinimumSize(new Dimension(350, 30));
          VDMAV.setMaximumSize(new Dimension(350, 30));
          VDMAV.setName("VDMAV");
          pnlGeneral.add(VDMAV, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("G\u00e9n\u00e9ral", pnlGeneral);
        
        // ======== pnlDocuments ========
        {
          pnlDocuments.setName("pnlDocuments");
          pnlDocuments.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDocuments.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDocuments.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDocuments.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDocuments.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== sNPanel3 ========
          {
            sNPanel3.setName("sNPanel3");
            sNPanel3.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbReglementGEMDirect2 ----
            lbReglementGEMDirect2.setText("Options de fin autoris\u00e9es");
            lbReglementGEMDirect2.setPreferredSize(new Dimension(200, 30));
            lbReglementGEMDirect2.setMinimumSize(new Dimension(200, 30));
            lbReglementGEMDirect2.setMaximumSize(new Dimension(200, 30));
            lbReglementGEMDirect2.setName("lbReglementGEMDirect2");
            sNPanel3.add(lbReglementGEMDirect2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel2 ========
            {
              sNPanel2.setName("sNPanel2");
              sNPanel2.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- VDO01 ----
              VDO01.setPreferredSize(new Dimension(50, 30));
              VDO01.setMinimumSize(new Dimension(50, 30));
              VDO01.setMaximumSize(new Dimension(50, 30));
              VDO01.setName("VDO01");
              sNPanel2.add(VDO01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO02 ----
              VDO02.setPreferredSize(new Dimension(50, 30));
              VDO02.setMinimumSize(new Dimension(50, 30));
              VDO02.setMaximumSize(new Dimension(50, 30));
              VDO02.setName("VDO02");
              sNPanel2.add(VDO02, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO03 ----
              VDO03.setPreferredSize(new Dimension(50, 30));
              VDO03.setMinimumSize(new Dimension(50, 30));
              VDO03.setMaximumSize(new Dimension(50, 30));
              VDO03.setName("VDO03");
              sNPanel2.add(VDO03, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO04 ----
              VDO04.setPreferredSize(new Dimension(50, 30));
              VDO04.setMinimumSize(new Dimension(50, 30));
              VDO04.setMaximumSize(new Dimension(50, 30));
              VDO04.setName("VDO04");
              sNPanel2.add(VDO04, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO05 ----
              VDO05.setPreferredSize(new Dimension(50, 30));
              VDO05.setMinimumSize(new Dimension(50, 30));
              VDO05.setMaximumSize(new Dimension(50, 30));
              VDO05.setName("VDO05");
              sNPanel2.add(VDO05, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO06 ----
              VDO06.setPreferredSize(new Dimension(50, 30));
              VDO06.setMinimumSize(new Dimension(50, 30));
              VDO06.setMaximumSize(new Dimension(50, 30));
              VDO06.setName("VDO06");
              sNPanel2.add(VDO06, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO07 ----
              VDO07.setPreferredSize(new Dimension(50, 30));
              VDO07.setMinimumSize(new Dimension(50, 30));
              VDO07.setMaximumSize(new Dimension(50, 30));
              VDO07.setName("VDO07");
              sNPanel2.add(VDO07, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO08 ----
              VDO08.setPreferredSize(new Dimension(50, 30));
              VDO08.setMinimumSize(new Dimension(50, 30));
              VDO08.setMaximumSize(new Dimension(50, 30));
              VDO08.setName("VDO08");
              sNPanel2.add(VDO08, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO09 ----
              VDO09.setPreferredSize(new Dimension(50, 30));
              VDO09.setMinimumSize(new Dimension(50, 30));
              VDO09.setMaximumSize(new Dimension(50, 30));
              VDO09.setName("VDO09");
              sNPanel2.add(VDO09, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- VDO10 ----
              VDO10.setPreferredSize(new Dimension(50, 30));
              VDO10.setMinimumSize(new Dimension(50, 30));
              VDO10.setMaximumSize(new Dimension(50, 30));
              VDO10.setName("VDO10");
              sNPanel2.add(VDO10, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel3.add(sNPanel2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- VDHOM ----
            VDHOM.setText("Validation automatique des commandes");
            VDHOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDHOM.setPreferredSize(new Dimension(350, 30));
            VDHOM.setMinimumSize(new Dimension(350, 30));
            VDHOM.setMaximumSize(new Dimension(350, 30));
            VDHOM.setName("VDHOM");
            sNPanel3.add(VDHOM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDocuments.add(sNPanel3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== sNPanelTitre1 ========
          {
            sNPanelTitre1.setTitre("Personnalisation par des documents de ventes");
            sNPanelTitre1.setName("sNPanelTitre1");
            sNPanelTitre1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbenteteRechercheClient ----
            lbenteteRechercheClient.setText("Recherche client");
            lbenteteRechercheClient.setMinimumSize(new Dimension(150, 30));
            lbenteteRechercheClient.setMaximumSize(new Dimension(150, 30));
            lbenteteRechercheClient.setPreferredSize(new Dimension(150, 30));
            lbenteteRechercheClient.setName("lbenteteRechercheClient");
            sNPanelTitre1.add(lbenteteRechercheClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbLigne ----
            lbLigne.setText("Ligne");
            lbLigne.setPreferredSize(new Dimension(150, 30));
            lbLigne.setMinimumSize(new Dimension(150, 30));
            lbLigne.setMaximumSize(new Dimension(150, 30));
            lbLigne.setName("lbLigne");
            sNPanelTitre1.add(lbLigne, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbComplementLigne ----
            lbComplementLigne.setText("Compl\u00e9ment sur ligne");
            lbComplementLigne.setPreferredSize(new Dimension(200, 30));
            lbComplementLigne.setMinimumSize(new Dimension(200, 30));
            lbComplementLigne.setMaximumSize(new Dimension(200, 30));
            lbComplementLigne.setName("lbComplementLigne");
            sNPanelTitre1.add(lbComplementLigne, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbComplementEntete ----
            lbComplementEntete.setText("Compl\u00e9ment sur ent\u00eate");
            lbComplementEntete.setPreferredSize(new Dimension(200, 30));
            lbComplementEntete.setMinimumSize(new Dimension(200, 30));
            lbComplementEntete.setMaximumSize(new Dimension(200, 30));
            lbComplementEntete.setName("lbComplementEntete");
            sNPanelTitre1.add(lbComplementEntete, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbOptions ----
            lbOptions.setText("Option de fin par d\u00e9faut");
            lbOptions.setPreferredSize(new Dimension(200, 30));
            lbOptions.setMinimumSize(new Dimension(200, 30));
            lbOptions.setMaximumSize(new Dimension(200, 30));
            lbOptions.setName("lbOptions");
            sNPanelTitre1.add(lbOptions, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDevis ----
            lbDevis.setText("Devis");
            lbDevis.setName("lbDevis");
            sNPanelTitre1.add(lbDevis, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDDEV2 ----
            VDDEV2.setModel(new DefaultComboBoxModel(
                new String[] { "Etendue", "Normale", "Aucune", "Normal+Rech.cli", "R\u00e9duite+Rech.cli", "Aucune+Rech.cli" }));
            VDDEV2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDDEV2.setBackground(Color.white);
            VDDEV2.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDDEV2.setName("VDDEV2");
            sNPanelTitre1.add(VDDEV2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDDEV3 ----
            VDDEV3.setModel(new DefaultComboBoxModel(new String[] { "Etendue", "Normale", "Lig. r\u00e9d. sans stk", "Vente grand public",
                "Saisie simplifi\u00e9e", "Saisie pleine page" }));
            VDDEV3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDDEV3.setBackground(Color.white);
            VDDEV3.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDDEV3.setName("VDDEV3");
            sNPanelTitre1.add(VDDEV3, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDDEV4 ----
            VDDEV4.setModel(new DefaultComboBoxModel(new String[] { " ", "Tarif sur ligne vente", "Aff du P.U.M.P.",
                "Aff. cond. quantitatives", "Recherche magasin", "Affichage stock sp\u00e9cial", "Stock global" }));
            VDDEV4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDDEV4.setBackground(Color.white);
            VDDEV4.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDDEV4.setName("VDDEV4");
            sNPanelTitre1.add(VDDEV4, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDDEV5 ----
            VDDEV5.setModel(new DefaultComboBoxModel(new String[] { " ", "Aff Bloc-Notes", "Aff Encours" }));
            VDDEV5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDDEV5.setBackground(Color.white);
            VDDEV5.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDDEV5.setName("VDDEV5");
            sNPanelTitre1.add(VDDEV5, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDDEV1 ----
            VDDEV1.setPreferredSize(new Dimension(50, 30));
            VDDEV1.setMinimumSize(new Dimension(50, 30));
            VDDEV1.setMaximumSize(new Dimension(50, 30));
            VDDEV1.setName("VDDEV1");
            sNPanelTitre1.add(VDDEV1, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCommande ----
            lbCommande.setText("Commandes");
            lbCommande.setName("lbCommande");
            sNPanelTitre1.add(lbCommande, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDHOM2 ----
            VDHOM2.setModel(new DefaultComboBoxModel(
                new String[] { "Etendue", "Normale", "Aucune", "Normal+Rech.cli", "R\u00e9duite+Rech.cli", "Aucune+Rech.cli" }));
            VDHOM2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDHOM2.setBackground(Color.white);
            VDHOM2.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDHOM2.setName("VDHOM2");
            sNPanelTitre1.add(VDHOM2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDHOM3 ----
            VDHOM3.setModel(new DefaultComboBoxModel(new String[] { "Etendue", "Normale", "Lig. r\u00e9d. sans stk", "Vente grand public",
                "Saisie simplifi\u00e9e", "Saisie pleine page" }));
            VDHOM3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDHOM3.setBackground(Color.white);
            VDHOM3.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDHOM3.setName("VDHOM3");
            sNPanelTitre1.add(VDHOM3, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDHOM4 ----
            VDHOM4.setModel(new DefaultComboBoxModel(new String[] { " ", "Tarif sur ligne vente", "Aff du P.U.M.P.",
                "Aff. cond. quantitatives", "Recherche magasin", "Affichage stock sp\u00e9cial", "Stock global" }));
            VDHOM4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDHOM4.setBackground(Color.white);
            VDHOM4.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDHOM4.setName("VDHOM4");
            sNPanelTitre1.add(VDHOM4, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDHOM5 ----
            VDHOM5.setModel(new DefaultComboBoxModel(new String[] { " ", "Aff Bloc-Notes", "Aff Encours" }));
            VDHOM5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDHOM5.setBackground(Color.white);
            VDHOM5.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDHOM5.setName("VDHOM5");
            sNPanelTitre1.add(VDHOM5, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDHOM1 ----
            VDHOM1.setPreferredSize(new Dimension(50, 30));
            VDHOM1.setMinimumSize(new Dimension(50, 30));
            VDHOM1.setMaximumSize(new Dimension(50, 30));
            VDHOM1.setName("VDHOM1");
            sNPanelTitre1.add(VDHOM1, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLivraisons ----
            lbLivraisons.setText("Livraisons");
            lbLivraisons.setName("lbLivraisons");
            sNPanelTitre1.add(lbLivraisons, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDEXP2 ----
            VDEXP2.setModel(new DefaultComboBoxModel(
                new String[] { "Etendue", "Normale", "Aucune", "Normal+Rech.cli", "R\u00e9duite+Rech.cli", "Aucune+Rech.cli" }));
            VDEXP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDEXP2.setBackground(Color.white);
            VDEXP2.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDEXP2.setName("VDEXP2");
            sNPanelTitre1.add(VDEXP2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDEXP3 ----
            VDEXP3.setModel(new DefaultComboBoxModel(new String[] { "Etendue", "Normale", "Lig. r\u00e9d. sans stk", "Vente grand public",
                "Saisie simplifi\u00e9e", "Saisie pleine page" }));
            VDEXP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDEXP3.setBackground(Color.white);
            VDEXP3.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDEXP3.setName("VDEXP3");
            sNPanelTitre1.add(VDEXP3, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDEXP4 ----
            VDEXP4.setModel(new DefaultComboBoxModel(new String[] { " ", "Tarif sur ligne vente", "Aff du P.U.M.P.",
                "Aff. cond. quantitatives", "Recherche magasin", "Affichage stock sp\u00e9cial", "Stock global" }));
            VDEXP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDEXP4.setBackground(Color.white);
            VDEXP4.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDEXP4.setName("VDEXP4");
            sNPanelTitre1.add(VDEXP4, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDEXP5 ----
            VDEXP5.setModel(new DefaultComboBoxModel(new String[] { " ", "Aff Bloc-Notes", "Aff Encours" }));
            VDEXP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDEXP5.setBackground(Color.white);
            VDEXP5.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDEXP5.setName("VDEXP5");
            sNPanelTitre1.add(VDEXP5, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDEXP1 ----
            VDEXP1.setPreferredSize(new Dimension(50, 30));
            VDEXP1.setMinimumSize(new Dimension(50, 30));
            VDEXP1.setMaximumSize(new Dimension(50, 30));
            VDEXP1.setName("VDEXP1");
            sNPanelTitre1.add(VDEXP1, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFactures ----
            lbFactures.setText("Factures");
            lbFactures.setName("lbFactures");
            sNPanelTitre1.add(lbFactures, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDFAC2 ----
            VDFAC2.setModel(new DefaultComboBoxModel(
                new String[] { "Etendue", "Normale", "Aucune", "Normal+Rech.cli", "R\u00e9duite+Rech.cli", "Aucune+Rech.cli" }));
            VDFAC2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDFAC2.setBackground(Color.white);
            VDFAC2.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDFAC2.setName("VDFAC2");
            sNPanelTitre1.add(VDFAC2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDFAC3 ----
            VDFAC3.setModel(new DefaultComboBoxModel(new String[] { "Etendue", "Normale", "Lig. r\u00e9d. sans stk", "Vente grand public",
                "Saisie simplifi\u00e9e", "Saisie pleine page" }));
            VDFAC3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDFAC3.setBackground(Color.white);
            VDFAC3.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDFAC3.setName("VDFAC3");
            sNPanelTitre1.add(VDFAC3, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDFAC4 ----
            VDFAC4.setModel(new DefaultComboBoxModel(new String[] { " ", "Tarif sur ligne vente", "Aff du P.U.M.P.",
                "Aff. cond. quantitatives", "Recherche magasin", "Affichage stock sp\u00e9cial", "Stock global" }));
            VDFAC4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDFAC4.setBackground(Color.white);
            VDFAC4.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDFAC4.setName("VDFAC4");
            sNPanelTitre1.add(VDFAC4, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDFAC5 ----
            VDFAC5.setModel(new DefaultComboBoxModel(new String[] { " ", "Aff Bloc-Notes", "Aff Encours" }));
            VDFAC5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDFAC5.setBackground(Color.white);
            VDFAC5.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDFAC5.setName("VDFAC5");
            sNPanelTitre1.add(VDFAC5, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDFAC1 ----
            VDFAC1.setPreferredSize(new Dimension(50, 30));
            VDFAC1.setMinimumSize(new Dimension(50, 30));
            VDFAC1.setMaximumSize(new Dimension(50, 30));
            VDFAC1.setName("VDFAC1");
            sNPanelTitre1.add(VDFAC1, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFacturesDirectes ----
            lbFacturesDirectes.setText("Factures directes");
            lbFacturesDirectes.setName("lbFacturesDirectes");
            sNPanelTitre1.add(lbFacturesDirectes, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDCPT2 ----
            VDCPT2.setModel(new DefaultComboBoxModel(
                new String[] { "Etendue", "Normale", "Aucune", "Normal+Rech.cli", "R\u00e9duite+Rech.cli", "Aucune+Rech.cli" }));
            VDCPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDCPT2.setBackground(Color.white);
            VDCPT2.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDCPT2.setName("VDCPT2");
            sNPanelTitre1.add(VDCPT2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDCPT3 ----
            VDCPT3.setModel(new DefaultComboBoxModel(new String[] { "Etendue", "Normale", "Lig. r\u00e9d. sans stk", "Vente grand public",
                "Saisie simplifi\u00e9e", "Saisie pleine page" }));
            VDCPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDCPT3.setBackground(Color.white);
            VDCPT3.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDCPT3.setName("VDCPT3");
            sNPanelTitre1.add(VDCPT3, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDCPT4 ----
            VDCPT4.setModel(new DefaultComboBoxModel(new String[] { " ", "Tarif sur ligne vente", "Aff du P.U.M.P.",
                "Aff. cond. quantitatives", "Recherche magasin", "Affichage stock sp\u00e9cial", "Stock global" }));
            VDCPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDCPT4.setBackground(Color.white);
            VDCPT4.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDCPT4.setName("VDCPT4");
            sNPanelTitre1.add(VDCPT4, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDCPT5 ----
            VDCPT5.setModel(new DefaultComboBoxModel(new String[] { " ", "Aff Bloc-Notes", "Aff Encours" }));
            VDCPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDCPT5.setBackground(Color.white);
            VDCPT5.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDCPT5.setName("VDCPT5");
            sNPanelTitre1.add(VDCPT5, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- VDCPT1 ----
            VDCPT1.setPreferredSize(new Dimension(50, 30));
            VDCPT1.setMinimumSize(new Dimension(50, 30));
            VDCPT1.setMaximumSize(new Dimension(50, 30));
            VDCPT1.setName("VDCPT1");
            sNPanelTitre1.add(VDCPT1, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTickets ----
            lbTickets.setText("Tickets de caisse");
            lbTickets.setName("lbTickets");
            sNPanelTitre1.add(lbTickets, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- VDTIC2 ----
            VDTIC2.setModel(new DefaultComboBoxModel(
                new String[] { "Etendue", "Normale", "Aucune", "Normal+Rech.cli", "R\u00e9duite+Rech.cli", "Aucune+Rech.cli" }));
            VDTIC2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDTIC2.setBackground(Color.white);
            VDTIC2.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDTIC2.setName("VDTIC2");
            sNPanelTitre1.add(VDTIC2, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- VDTIC3 ----
            VDTIC3.setModel(new DefaultComboBoxModel(new String[] { "Etendue", "Normale", "Lig. r\u00e9d. sans stk", "Vente grand public",
                "Saisie simplifi\u00e9e", "Saisie pleine page" }));
            VDTIC3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDTIC3.setBackground(Color.white);
            VDTIC3.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDTIC3.setName("VDTIC3");
            sNPanelTitre1.add(VDTIC3, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- VDTIC4 ----
            VDTIC4.setModel(new DefaultComboBoxModel(new String[] { " ", "Tarif sur ligne vente", "Aff du P.U.M.P.",
                "Aff. cond. quantitatives", "Recherche magasin", "Affichage stock sp\u00e9cial", "Stock global" }));
            VDTIC4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDTIC4.setBackground(Color.white);
            VDTIC4.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDTIC4.setName("VDTIC4");
            sNPanelTitre1.add(VDTIC4, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- VDTIC5 ----
            VDTIC5.setModel(new DefaultComboBoxModel(new String[] { " ", "Aff Bloc-Notes", "Aff Encours" }));
            VDTIC5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            VDTIC5.setBackground(Color.white);
            VDTIC5.setFont(new Font("sansserif", Font.PLAIN, 14));
            VDTIC5.setName("VDTIC5");
            sNPanelTitre1.add(VDTIC5, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- VDTIC1 ----
            VDTIC1.setPreferredSize(new Dimension(50, 30));
            VDTIC1.setMinimumSize(new Dimension(50, 30));
            VDTIC1.setMaximumSize(new Dimension(50, 30));
            VDTIC1.setName("VDTIC1");
            sNPanelTitre1.add(VDTIC1, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDocuments.add(sNPanelTitre1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("Documents vente", pnlDocuments);
        
        // ======== pnlPreparateur ========
        {
          pnlPreparateur.setName("pnlPreparateur");
          pnlPreparateur.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPreparateur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPreparateur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlPreparateur.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPreparateur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTypeStockage1 ----
          lbTypeStockage1.setText("Type stockage magasin 1");
          lbTypeStockage1.setPreferredSize(new Dimension(200, 30));
          lbTypeStockage1.setMinimumSize(new Dimension(200, 30));
          lbTypeStockage1.setMaximumSize(new Dimension(200, 30));
          lbTypeStockage1.setName("lbTypeStockage1");
          pnlPreparateur.add(lbTypeStockage1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTypeStockageMagasin1 ----
          snTypeStockageMagasin1.setName("snTypeStockageMagasin1");
          pnlPreparateur.add(snTypeStockageMagasin1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeStockage2 ----
          lbTypeStockage2.setText("Type stockage magasin 2");
          lbTypeStockage2.setPreferredSize(new Dimension(200, 30));
          lbTypeStockage2.setMinimumSize(new Dimension(200, 30));
          lbTypeStockage2.setMaximumSize(new Dimension(200, 30));
          lbTypeStockage2.setName("lbTypeStockage2");
          pnlPreparateur.add(lbTypeStockage2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTypeStockageMagasin2 ----
          snTypeStockageMagasin2.setName("snTypeStockageMagasin2");
          pnlPreparateur.add(snTypeStockageMagasin2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeStockage3 ----
          lbTypeStockage3.setText("Type stockage magasin 3");
          lbTypeStockage3.setPreferredSize(new Dimension(200, 30));
          lbTypeStockage3.setMinimumSize(new Dimension(200, 30));
          lbTypeStockage3.setMaximumSize(new Dimension(200, 30));
          lbTypeStockage3.setName("lbTypeStockage3");
          pnlPreparateur.add(lbTypeStockage3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTypeStockageMagasin3 ----
          snTypeStockageMagasin3.setName("snTypeStockageMagasin3");
          pnlPreparateur.add(snTypeStockageMagasin3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeStockage4 ----
          lbTypeStockage4.setText("Type stockage magasin 4");
          lbTypeStockage4.setPreferredSize(new Dimension(200, 30));
          lbTypeStockage4.setMinimumSize(new Dimension(200, 30));
          lbTypeStockage4.setMaximumSize(new Dimension(200, 30));
          lbTypeStockage4.setName("lbTypeStockage4");
          pnlPreparateur.add(lbTypeStockage4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTypeStockageMagasin4 ----
          snTypeStockageMagasin4.setName("snTypeStockageMagasin4");
          pnlPreparateur.add(snTypeStockageMagasin4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeStockage5 ----
          lbTypeStockage5.setText("Type stockage magasin 5");
          lbTypeStockage5.setPreferredSize(new Dimension(200, 30));
          lbTypeStockage5.setMinimumSize(new Dimension(200, 30));
          lbTypeStockage5.setMaximumSize(new Dimension(200, 30));
          lbTypeStockage5.setName("lbTypeStockage5");
          pnlPreparateur.add(lbTypeStockage5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snTypeStockageMagasin5 ----
          snTypeStockageMagasin5.setName("snTypeStockageMagasin5");
          pnlPreparateur.add(snTypeStockageMagasin5, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("Pr\u00e9parateur", pnlPreparateur);
      }
      sNPanelContenu1.add(tabbedPane1,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlbandeau;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNLabelChamp lbLibelle;
  private XRiTextField CCLIB;
  private SNPanel sNPanel1;
  private SNLabelChamp lbNom;
  private XRiTextField VDLIB;
  private SNLabelChamp lbTelephone;
  private XRiTextField VDTEL;
  private SNLabelChamp lbMail;
  private XRiTextField VDMAI;
  private JTabbedPane tabbedPane1;
  private SNPanelContenu pnlGeneral;
  private SNLabelChamp lbPourcentageMaxiRemise2;
  private SNPanel pnlValidite;
  private XRiTextField VDVDV;
  private XRiComboBox VDVDP;
  private SNLabelChamp lbPourcentageMaxiRemise;
  private SNPanel pnlRemiseMax;
  private XRiTextField VDPMR;
  private SNLabelUnite lbPourcent;
  private SNLabelChamp lbJOCaisse;
  private XRiTextField VDCJO;
  private SNLabelChamp lbReglementGEMDirect;
  private XRiComboBox VDRG4;
  private SNLabelChamp lbRechercheRestreinte;
  private XRiTextField VDZP1;
  private XRiCheckBox VDCHQ;
  private XRiCheckBox VDMAV;
  private SNPanelContenu pnlDocuments;
  private SNPanel sNPanel3;
  private SNLabelChamp lbReglementGEMDirect2;
  private SNPanel sNPanel2;
  private XRiTextField VDO01;
  private XRiTextField VDO02;
  private XRiTextField VDO03;
  private XRiTextField VDO04;
  private XRiTextField VDO05;
  private XRiTextField VDO06;
  private XRiTextField VDO07;
  private XRiTextField VDO08;
  private XRiTextField VDO09;
  private XRiTextField VDO10;
  private XRiCheckBox VDHOM;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelUnite lbenteteRechercheClient;
  private SNLabelUnite lbLigne;
  private SNLabelUnite lbComplementLigne;
  private SNLabelUnite lbComplementEntete;
  private SNLabelUnite lbOptions;
  private SNLabelChamp lbDevis;
  private XRiComboBox VDDEV2;
  private XRiComboBox VDDEV3;
  private XRiComboBox VDDEV4;
  private XRiComboBox VDDEV5;
  private XRiTextField VDDEV1;
  private SNLabelChamp lbCommande;
  private XRiComboBox VDHOM2;
  private XRiComboBox VDHOM3;
  private XRiComboBox VDHOM4;
  private XRiComboBox VDHOM5;
  private XRiTextField VDHOM1;
  private SNLabelChamp lbLivraisons;
  private XRiComboBox VDEXP2;
  private XRiComboBox VDEXP3;
  private XRiComboBox VDEXP4;
  private XRiComboBox VDEXP5;
  private XRiTextField VDEXP1;
  private SNLabelChamp lbFactures;
  private XRiComboBox VDFAC2;
  private XRiComboBox VDFAC3;
  private XRiComboBox VDFAC4;
  private XRiComboBox VDFAC5;
  private XRiTextField VDFAC1;
  private SNLabelChamp lbFacturesDirectes;
  private XRiComboBox VDCPT2;
  private XRiComboBox VDCPT3;
  private XRiComboBox VDCPT4;
  private XRiComboBox VDCPT5;
  private XRiTextField VDCPT1;
  private SNLabelChamp lbTickets;
  private XRiComboBox VDTIC2;
  private XRiComboBox VDTIC3;
  private XRiComboBox VDTIC4;
  private XRiComboBox VDTIC5;
  private XRiTextField VDTIC1;
  private SNPanelContenu pnlPreparateur;
  private SNLabelChamp lbTypeStockage1;
  private SNTypeStockageMagasin snTypeStockageMagasin1;
  private SNLabelChamp lbTypeStockage2;
  private SNTypeStockageMagasin snTypeStockageMagasin2;
  private SNLabelChamp lbTypeStockage3;
  private SNTypeStockageMagasin snTypeStockageMagasin3;
  private SNLabelChamp lbTypeStockage4;
  private SNTypeStockageMagasin snTypeStockageMagasin4;
  private SNLabelChamp lbTypeStockage5;
  private SNTypeStockageMagasin snTypeStockageMagasin5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
