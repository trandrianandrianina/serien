//$$david$$ ££06/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_A2 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_CREER = "Créer";
  private static final String BOUTON_DUPLIQUER = "Dupliquer";
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 538, };
  boolean isConsultation = true;
  boolean isCreation = false;
  boolean isDuplication = false;
  
  public VGVM35FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER, 'd', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Etat des actions
    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    isDuplication = lexique.isTrue("56");
    
    // Visibilité
    pnlEtablissementRepresentantCreationDuplication.setVisible(!isConsultation);
    pnlDuplication.setVisible(isDuplication);
    pnlCodeRepresentant.setVisible(!isConsultation);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    if (isCreation) {
      pnlEtablissementRepresentantCreationDuplication.setTitre("Création d'un nouveau représentant");
    }
    
    if (isDuplication) {
      pnlEtablissementRepresentantCreationDuplication.setTitre("Duplication d'un représentant");
    }
    
    snBarreBouton.activerBouton(BOUTON_CREER, isConsultation);
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, WTP01.getSelectedRow() > -1);
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    // Etablissement duplication
    snEtablissementDuplication.setSession(getSession());
    snEtablissementDuplication.charger(false);
    snEtablissementDuplication.setSelectionParChampRPG(lexique, "IN3ETB");
    
    // Représentant duplication
    snRepresentantDuplication.setSession(getSession());
    snRepresentantDuplication.setIdEtablissement(idEtablissement);
    snRepresentantDuplication.charger(false);
    snRepresentantDuplication.setSelectionParChampRPG(lexique, "IN3REP");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    if (isDuplication) {
      if (!lexique.HostFieldGetData("IN3ETB").trim().isEmpty()) {
        snEtablissementDuplication.renseignerChampRPG(lexique, "IN3ETB");
      }
      if (!lexique.HostFieldGetData("IN3REP").trim().isEmpty()) {
        snRepresentantDuplication.renseignerChampRPG(lexique, "IN3REP");
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        if (isCreation || isDuplication) {
          lexique.HostScreenSendKey(this, "F15");
        }
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CREER)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER)) {
        lexique.HostScreenSendKey(this, "F18");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("T");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void DEVERROUILLAGEActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, WTP01.getSelectedRow() > -1);
    
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void ChoisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void STATISTIQUESActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void snEtablissementDuplicationValueChanged(SNComposantEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlEtablissementRepresentantCreationDuplication = new SNPanelTitre();
    pnlCreation = new SNPanel();
    pnlCodeRepresentant = new SNPanel();
    lbCodeRepresentant = new SNLabelChamp();
    INDREP = new XRiTextField();
    pnlEtablissement = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlDuplication = new SNPanel();
    pnlCodeRepresentantDuplication = new SNPanel();
    lbCodeRepresentantDuplication = new SNLabelChamp();
    snRepresentantDuplication = new SNRepresentant();
    pnlDuplicationEtablissement = new SNPanel();
    lbEtablissementDuplication = new SNLabelChamp();
    snEtablissementDuplication = new SNEtablissement();
    pnlResultatsRecherche = new SNPanelTitre();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlBoutonHautBas = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    STATISTIQUES = new JMenuItem();
    DEVERROUILLAGE = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1000, 520));
    setPreferredSize(new Dimension(1000, 520));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Recherche repr\u00e9sentants");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
      
      // ======== pnlEtablissementRepresentantCreationDuplication ========
      {
        pnlEtablissementRepresentantCreationDuplication.setTitre("Etablissement");
        pnlEtablissementRepresentantCreationDuplication.setName("pnlEtablissementRepresentantCreationDuplication");
        pnlEtablissementRepresentantCreationDuplication.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEtablissementRepresentantCreationDuplication.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlEtablissementRepresentantCreationDuplication.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlEtablissementRepresentantCreationDuplication.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlEtablissementRepresentantCreationDuplication.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlCreation ========
        {
          pnlCreation.setName("pnlCreation");
          pnlCreation.setLayout(new GridLayout(1, 0, 5, 5));
          
          // ======== pnlCodeRepresentant ========
          {
            pnlCodeRepresentant.setName("pnlCodeRepresentant");
            pnlCodeRepresentant.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCodeRepresentant.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCodeRepresentant.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCodeRepresentant.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCodeRepresentant.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCodeRepresentant ----
            lbCodeRepresentant.setText("Code repr\u00e9sentant");
            lbCodeRepresentant.setName("lbCodeRepresentant");
            pnlCodeRepresentant.add(lbCodeRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- INDREP ----
            INDREP.setMaximumSize(new Dimension(36, 30));
            INDREP.setMinimumSize(new Dimension(36, 30));
            INDREP.setPreferredSize(new Dimension(36, 30));
            INDREP.setName("INDREP");
            pnlCodeRepresentant.add(INDREP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCreation.add(pnlCodeRepresentant);
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCreation.add(pnlEtablissement);
        }
        pnlEtablissementRepresentantCreationDuplication.add(pnlCreation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDuplication ========
        {
          pnlDuplication.setName("pnlDuplication");
          pnlDuplication.setLayout(new GridLayout(1, 0, 5, 5));
          
          // ======== pnlCodeRepresentantDuplication ========
          {
            pnlCodeRepresentantDuplication.setName("pnlCodeRepresentantDuplication");
            pnlCodeRepresentantDuplication.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCodeRepresentantDuplication.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCodeRepresentantDuplication.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCodeRepresentantDuplication.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCodeRepresentantDuplication.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCodeRepresentantDuplication ----
            lbCodeRepresentantDuplication.setText("Par duplication de");
            lbCodeRepresentantDuplication.setName("lbCodeRepresentantDuplication");
            pnlCodeRepresentantDuplication.add(lbCodeRepresentantDuplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snRepresentantDuplication ----
            snRepresentantDuplication.setName("snRepresentantDuplication");
            pnlCodeRepresentantDuplication.add(snRepresentantDuplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDuplication.add(pnlCodeRepresentantDuplication);
          
          // ======== pnlDuplicationEtablissement ========
          {
            pnlDuplicationEtablissement.setName("pnlDuplicationEtablissement");
            pnlDuplicationEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDuplicationEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlDuplicationEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDuplicationEtablissement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlDuplicationEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbEtablissementDuplication ----
            lbEtablissementDuplication.setText("Etablissement");
            lbEtablissementDuplication.setName("lbEtablissementDuplication");
            pnlDuplicationEtablissement.add(lbEtablissementDuplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snEtablissementDuplication ----
            snEtablissementDuplication.setName("snEtablissementDuplication");
            pnlDuplicationEtablissement.add(snEtablissementDuplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDuplication.add(pnlDuplicationEtablissement);
        }
        pnlEtablissementRepresentantCreationDuplication.add(pnlDuplication, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEtablissementRepresentantCreationDuplication,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ======== pnlResultatsRecherche ========
      {
        pnlResultatsRecherche.setTitre("R\u00e9sultat de la recherche");
        pnlResultatsRecherche.setPreferredSize(new Dimension(501, 265));
        pnlResultatsRecherche.setMaximumSize(new Dimension(2147483647, 265));
        pnlResultatsRecherche.setMinimumSize(new Dimension(70, 265));
        pnlResultatsRecherche.setName("pnlResultatsRecherche");
        pnlResultatsRecherche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlResultatsRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlResultatsRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlResultatsRecherche.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlResultatsRecherche.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
          
          // ---- WTP01 ----
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setMinimumSize(new Dimension(538, 240));
          WTP01.setMaximumSize(new Dimension(2147483647, 240));
          WTP01.setPreferredSize(new Dimension(538, 240));
          WTP01.setPreferredScrollableViewportSize(new Dimension(450, 240));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlResultatsRecherche.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlBoutonHautBas ========
        {
          pnlBoutonHautBas.setName("pnlBoutonHautBas");
          pnlBoutonHautBas.setLayout(new GridLayout(2, 0, 5, 5));
          
          // ---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setToolTipText("page suivante");
          BT_PGUP.setMaximumSize(new Dimension(28, 130));
          BT_PGUP.setMinimumSize(new Dimension(28, 130));
          BT_PGUP.setPreferredSize(new Dimension(28, 130));
          BT_PGUP.setName("BT_PGUP");
          pnlBoutonHautBas.add(BT_PGUP);
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setToolTipText("page pr\u00e9c\u00e9dente");
          BT_PGDOWN.setMaximumSize(new Dimension(28, 130));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 130));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 130));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlBoutonHautBas.add(BT_PGDOWN);
        }
        pnlResultatsRecherche.add(pnlBoutonHautBas, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlResultatsRecherche,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ChoisirActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      
      // ---- STATISTIQUES ----
      STATISTIQUES.setText("Statistiques");
      STATISTIQUES.setName("STATISTIQUES");
      STATISTIQUES.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          STATISTIQUESActionPerformed(e);
        }
      });
      BTD.add(STATISTIQUES);
      
      // ---- DEVERROUILLAGE ----
      DEVERROUILLAGE.setText("D\u00e9verrouillage");
      DEVERROUILLAGE.setName("DEVERROUILLAGE");
      DEVERROUILLAGE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          DEVERROUILLAGEActionPerformed(e);
        }
      });
      BTD.add(DEVERROUILLAGE);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissementRepresentantCreationDuplication;
  private SNPanel pnlCreation;
  private SNPanel pnlCodeRepresentant;
  private SNLabelChamp lbCodeRepresentant;
  private XRiTextField INDREP;
  private SNPanel pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanel pnlDuplication;
  private SNPanel pnlCodeRepresentantDuplication;
  private SNLabelChamp lbCodeRepresentantDuplication;
  private SNRepresentant snRepresentantDuplication;
  private SNPanel pnlDuplicationEtablissement;
  private SNLabelChamp lbEtablissementDuplication;
  private SNEtablissement snEtablissementDuplication;
  private SNPanelTitre pnlResultatsRecherche;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel pnlBoutonHautBas;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem STATISTIQUES;
  private JMenuItem DEVERROUILLAGE;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
