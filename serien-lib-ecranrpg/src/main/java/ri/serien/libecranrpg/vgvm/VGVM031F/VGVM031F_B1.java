
package ri.serien.libecranrpg.vgvm.VGVM031F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM031F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM031F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_46_OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDM@")).trim());
    OBJ_47_OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDEX1@")).trim());
    OBJ_48_OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDEX0@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion CRM"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel5 = new JPanel();
    OBJ_43_OBJ_43 = new JLabel();
    OBJ_25_OBJ_25 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    XPREC = new XRiTextField();
    CLREC = new XRiTextField();
    XPFRE = new XRiTextField();
    CLFRC = new XRiTextField();
    XPMON = new XRiTextField();
    CLMTC = new XRiTextField();
    panel6 = new JPanel();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    XPNUM = new XRiTextField();
    XPSUF = new XRiTextField();
    OBJ_33_OBJ_33 = new JLabel();
    XPEXPX = new XRiCalendrier();
    XPTTC = new XRiTextField();
    panel7 = new JPanel();
    WCRM = new XRiTextField();
    WCR1 = new XRiTextField();
    WCR0 = new XRiTextField();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_47_OBJ_47 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    WRGM = new XRiTextField();
    WRG1 = new XRiTextField();
    WRG0 = new XRiTextField();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(740, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText(" Objectifs rep / clients");
              riSousMenu_bt6.setToolTipText("Acc\u00e9s objectifs repr\u00e9sentants/clients");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Derniers \u00e9v\u00e8nements");
              riSousMenu_bt7.setToolTipText("Derniers \u00e9v\u00e8nements");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Stats crois\u00e9es ");
              riSousMenu_bt8.setToolTipText("Statistiques crois\u00e9es Clients / Articles");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Statistiques clients SN");
              riSousMenu_bt9.setToolTipText("Statistiques clients SN");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Palmar\u00e8s client");
              riSousMenu_bt10.setToolTipText("Palmar\u00e8s client");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Objectifs");
              riSousMenu_bt11.setToolTipText("Objectifs");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("M\u00e9mo");
              riSousMenu_bt14.setToolTipText("M\u00e9mo");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setMinimumSize(new Dimension(550, 330));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("Scoring client"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- OBJ_43_OBJ_43 ----
          OBJ_43_OBJ_43.setText("Fr\u00e9quence");
          OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");
          panel5.add(OBJ_43_OBJ_43);
          OBJ_43_OBJ_43.setBounds(175, 35, 68, 18);

          //---- OBJ_25_OBJ_25 ----
          OBJ_25_OBJ_25.setText("R\u00e9cence");
          OBJ_25_OBJ_25.setName("OBJ_25_OBJ_25");
          panel5.add(OBJ_25_OBJ_25);
          OBJ_25_OBJ_25.setBounds(20, 35, 58, 18);

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("Montant");
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");
          panel5.add(OBJ_67_OBJ_67);
          OBJ_67_OBJ_67.setBounds(330, 35, 52, 18);

          //---- XPREC ----
          XPREC.setName("XPREC");
          panel5.add(XPREC);
          XPREC.setBounds(85, 30, 36, XPREC.getPreferredSize().height);

          //---- CLREC ----
          CLREC.setName("CLREC");
          panel5.add(CLREC);
          CLREC.setBounds(125, 30, 20, CLREC.getPreferredSize().height);

          //---- XPFRE ----
          XPFRE.setName("XPFRE");
          panel5.add(XPFRE);
          XPFRE.setBounds(250, 30, 36, XPFRE.getPreferredSize().height);

          //---- CLFRC ----
          CLFRC.setName("CLFRC");
          panel5.add(CLFRC);
          CLFRC.setBounds(290, 30, 20, CLFRC.getPreferredSize().height);

          //---- XPMON ----
          XPMON.setName("XPMON");
          panel5.add(XPMON);
          XPMON.setBounds(405, 30, 44, XPMON.getPreferredSize().height);

          //---- CLMTC ----
          CLMTC.setName("CLMTC");
          panel5.add(CLMTC);
          CLMTC.setBounds(455, 30, 20, CLMTC.getPreferredSize().height);
        }
        p_contenu.add(panel5);
        panel5.setBounds(20, 20, 525, 75);

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Infos / derni\u00e8re commande"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Montant TTC");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
          panel6.add(OBJ_35_OBJ_35);
          OBJ_35_OBJ_35.setBounds(330, 35, 80, 20);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Num\u00e9ro");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel6.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(20, 35, 51, 20);

          //---- XPNUM ----
          XPNUM.setName("XPNUM");
          panel6.add(XPNUM);
          XPNUM.setBounds(85, 31, 60, XPNUM.getPreferredSize().height);

          //---- XPSUF ----
          XPSUF.setName("XPSUF");
          panel6.add(XPSUF);
          XPSUF.setBounds(150, 31, 20, XPSUF.getPreferredSize().height);

          //---- OBJ_33_OBJ_33 ----
          OBJ_33_OBJ_33.setText("le");
          OBJ_33_OBJ_33.setName("OBJ_33_OBJ_33");
          panel6.add(OBJ_33_OBJ_33);
          OBJ_33_OBJ_33.setBounds(180, 35, 14, 20);

          //---- XPEXPX ----
          XPEXPX.setEditable(false);
          XPEXPX.setName("XPEXPX");
          panel6.add(XPEXPX);
          XPEXPX.setBounds(200, 30, 115, XPEXPX.getPreferredSize().height);

          //---- XPTTC ----
          XPTTC.setName("XPTTC");
          panel6.add(XPTTC);
          XPTTC.setBounds(405, 30, 100, XPTTC.getPreferredSize().height);
        }
        p_contenu.add(panel6);
        panel6.setBounds(20, 120, 525, 75);

        //======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Statistiques"));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);

          //---- WCRM ----
          WCRM.setName("WCRM");
          panel7.add(WCRM);
          WCRM.setBounds(85, 51, 100, WCRM.getPreferredSize().height);

          //---- WCR1 ----
          WCR1.setName("WCR1");
          panel7.add(WCR1);
          WCR1.setBounds(200, 51, 100, WCR1.getPreferredSize().height);

          //---- WCR0 ----
          WCR0.setName("WCR0");
          panel7.add(WCR0);
          WCR0.setBounds(330, 51, 110, WCR0.getPreferredSize().height);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("@WLDM@");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          panel7.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(85, 30, 76, 20);

          //---- OBJ_47_OBJ_47 ----
          OBJ_47_OBJ_47.setText("@WLDEX1@");
          OBJ_47_OBJ_47.setName("OBJ_47_OBJ_47");
          panel7.add(OBJ_47_OBJ_47);
          OBJ_47_OBJ_47.setBounds(200, 30, 105, 20);

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("@WLDEX0@");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
          panel7.add(OBJ_48_OBJ_48);
          OBJ_48_OBJ_48.setBounds(330, 30, 100, 20);

          //---- WRGM ----
          WRGM.setName("WRGM");
          panel7.add(WRGM);
          WRGM.setBounds(85, 81, 84, WRGM.getPreferredSize().height);

          //---- WRG1 ----
          WRG1.setName("WRG1");
          panel7.add(WRG1);
          WRG1.setBounds(200, 81, 84, WRG1.getPreferredSize().height);

          //---- WRG0 ----
          WRG0.setName("WRG0");
          panel7.add(WRG0);
          WRG0.setBounds(330, 81, 84, WRG0.getPreferredSize().height);

          //---- OBJ_53_OBJ_53 ----
          OBJ_53_OBJ_53.setText("Palmar\u00e8s");
          OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
          panel7.add(OBJ_53_OBJ_53);
          OBJ_53_OBJ_53.setBounds(25, 85, 61, 20);

          //---- OBJ_49_OBJ_49 ----
          OBJ_49_OBJ_49.setText("CA");
          OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
          panel7.add(OBJ_49_OBJ_49);
          OBJ_49_OBJ_49.setBounds(25, 55, 21, 20);
        }
        p_contenu.add(panel7);
        panel7.setBounds(20, 220, 525, 130);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Invite");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private JPanel p_contenu;
  private JPanel panel5;
  private JLabel OBJ_43_OBJ_43;
  private JLabel OBJ_25_OBJ_25;
  private JLabel OBJ_67_OBJ_67;
  private XRiTextField XPREC;
  private XRiTextField CLREC;
  private XRiTextField XPFRE;
  private XRiTextField CLFRC;
  private XRiTextField XPMON;
  private XRiTextField CLMTC;
  private JPanel panel6;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField XPNUM;
  private XRiTextField XPSUF;
  private JLabel OBJ_33_OBJ_33;
  private XRiCalendrier XPEXPX;
  private XRiTextField XPTTC;
  private JPanel panel7;
  private XRiTextField WCRM;
  private XRiTextField WCR1;
  private XRiTextField WCR0;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_47_OBJ_47;
  private JLabel OBJ_48_OBJ_48;
  private XRiTextField WRGM;
  private XRiTextField WRG1;
  private XRiTextField WRG0;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_49_OBJ_49;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
