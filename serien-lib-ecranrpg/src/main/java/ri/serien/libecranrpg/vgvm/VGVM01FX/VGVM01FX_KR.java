
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_KR extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_KR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    PO01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO01@")).trim());
    PO02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO02@")).trim());
    PO03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO03@")).trim());
    PO04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO04@")).trim());
    PO05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO05@")).trim());
    PO06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO06@")).trim());
    PO07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO07@")).trim());
    PO08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO08@")).trim());
    PO09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO09@")).trim());
    PO10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO10@")).trim());
    PO11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO11@")).trim());
    PO12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PO12@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    panel3.setVisible(lexique.isTrue("53"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_62 = new JLabel();
    KRLIB = new XRiTextField();
    panel1 = new JPanel();
    KRP01 = new XRiTextField();
    KRP02 = new XRiTextField();
    KRP03 = new XRiTextField();
    KRP04 = new XRiTextField();
    KRP05 = new XRiTextField();
    KRP06 = new XRiTextField();
    KRP07 = new XRiTextField();
    KRP08 = new XRiTextField();
    KRP09 = new XRiTextField();
    KRP10 = new XRiTextField();
    KRP11 = new XRiTextField();
    KRP12 = new XRiTextField();
    OBJ_97 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_93 = new JLabel();
    panel3 = new JPanel();
    PO01 = new RiZoneSortie();
    PO02 = new RiZoneSortie();
    PO03 = new RiZoneSortie();
    PO04 = new RiZoneSortie();
    PO05 = new RiZoneSortie();
    PO06 = new RiZoneSortie();
    PO07 = new RiZoneSortie();
    PO08 = new RiZoneSortie();
    PO09 = new RiZoneSortie();
    PO10 = new RiZoneSortie();
    PO11 = new RiZoneSortie();
    PO12 = new RiZoneSortie();
    OBJ_69 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(550, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Cl\u00e9s de r\u00e9partition");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_62 ----
              OBJ_62.setText("Libell\u00e9");
              OBJ_62.setName("OBJ_62");
              panel2.add(OBJ_62);
              OBJ_62.setBounds(10, 14, 43, 20);

              //---- KRLIB ----
              KRLIB.setComponentPopupMenu(BTD);
              KRLIB.setName("KRLIB");
              panel2.add(KRLIB);
              KRLIB.setBounds(85, 10, 310, KRLIB.getPreferredSize().height);
            }
            xTitledPanel4ContentContainer.add(panel2);
            panel2.setBounds(15, 15, 420, 50);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("P\u00e9riode"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- KRP01 ----
              KRP01.setComponentPopupMenu(BTD);
              KRP01.setName("KRP01");
              panel1.add(KRP01);
              KRP01.setBounds(75, 50, 50, KRP01.getPreferredSize().height);

              //---- KRP02 ----
              KRP02.setComponentPopupMenu(BTD);
              KRP02.setName("KRP02");
              panel1.add(KRP02);
              KRP02.setBounds(75, 75, 50, KRP02.getPreferredSize().height);

              //---- KRP03 ----
              KRP03.setComponentPopupMenu(BTD);
              KRP03.setName("KRP03");
              panel1.add(KRP03);
              KRP03.setBounds(75, 100, 50, KRP03.getPreferredSize().height);

              //---- KRP04 ----
              KRP04.setComponentPopupMenu(BTD);
              KRP04.setName("KRP04");
              panel1.add(KRP04);
              KRP04.setBounds(75, 125, 50, KRP04.getPreferredSize().height);

              //---- KRP05 ----
              KRP05.setComponentPopupMenu(BTD);
              KRP05.setName("KRP05");
              panel1.add(KRP05);
              KRP05.setBounds(75, 150, 50, KRP05.getPreferredSize().height);

              //---- KRP06 ----
              KRP06.setComponentPopupMenu(BTD);
              KRP06.setName("KRP06");
              panel1.add(KRP06);
              KRP06.setBounds(75, 175, 50, KRP06.getPreferredSize().height);

              //---- KRP07 ----
              KRP07.setComponentPopupMenu(BTD);
              KRP07.setName("KRP07");
              panel1.add(KRP07);
              KRP07.setBounds(75, 200, 50, KRP07.getPreferredSize().height);

              //---- KRP08 ----
              KRP08.setComponentPopupMenu(BTD);
              KRP08.setName("KRP08");
              panel1.add(KRP08);
              KRP08.setBounds(75, 225, 50, KRP08.getPreferredSize().height);

              //---- KRP09 ----
              KRP09.setComponentPopupMenu(BTD);
              KRP09.setName("KRP09");
              panel1.add(KRP09);
              KRP09.setBounds(75, 250, 50, KRP09.getPreferredSize().height);

              //---- KRP10 ----
              KRP10.setComponentPopupMenu(BTD);
              KRP10.setName("KRP10");
              panel1.add(KRP10);
              KRP10.setBounds(75, 275, 50, KRP10.getPreferredSize().height);

              //---- KRP11 ----
              KRP11.setComponentPopupMenu(BTD);
              KRP11.setName("KRP11");
              panel1.add(KRP11);
              KRP11.setBounds(75, 300, 50, KRP11.getPreferredSize().height);

              //---- KRP12 ----
              KRP12.setComponentPopupMenu(BTD);
              KRP12.setName("KRP12");
              panel1.add(KRP12);
              KRP12.setBounds(75, 325, 50, KRP12.getPreferredSize().height);

              //---- OBJ_97 ----
              OBJ_97.setText("Poids");
              OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_97.setFont(OBJ_97.getFont().deriveFont(OBJ_97.getFont().getStyle() | Font.BOLD));
              OBJ_97.setName("OBJ_97");
              panel1.add(OBJ_97);
              OBJ_97.setBounds(75, 25, 50, 20);

              //---- OBJ_71 ----
              OBJ_71.setText("01");
              OBJ_71.setName("OBJ_71");
              panel1.add(OBJ_71);
              OBJ_71.setBounds(25, 54, 18, 20);

              //---- OBJ_73 ----
              OBJ_73.setText("02");
              OBJ_73.setName("OBJ_73");
              panel1.add(OBJ_73);
              OBJ_73.setBounds(25, 79, 18, 20);

              //---- OBJ_75 ----
              OBJ_75.setText("03");
              OBJ_75.setName("OBJ_75");
              panel1.add(OBJ_75);
              OBJ_75.setBounds(25, 104, 18, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("04");
              OBJ_77.setName("OBJ_77");
              panel1.add(OBJ_77);
              OBJ_77.setBounds(25, 129, 18, 20);

              //---- OBJ_79 ----
              OBJ_79.setText("05");
              OBJ_79.setName("OBJ_79");
              panel1.add(OBJ_79);
              OBJ_79.setBounds(25, 154, 18, 20);

              //---- OBJ_81 ----
              OBJ_81.setText("06");
              OBJ_81.setName("OBJ_81");
              panel1.add(OBJ_81);
              OBJ_81.setBounds(25, 179, 18, 20);

              //---- OBJ_83 ----
              OBJ_83.setText("07");
              OBJ_83.setName("OBJ_83");
              panel1.add(OBJ_83);
              OBJ_83.setBounds(25, 204, 18, 20);

              //---- OBJ_85 ----
              OBJ_85.setText("08");
              OBJ_85.setName("OBJ_85");
              panel1.add(OBJ_85);
              OBJ_85.setBounds(25, 229, 18, 20);

              //---- OBJ_87 ----
              OBJ_87.setText("09");
              OBJ_87.setName("OBJ_87");
              panel1.add(OBJ_87);
              OBJ_87.setBounds(25, 254, 18, 20);

              //---- OBJ_89 ----
              OBJ_89.setText("10");
              OBJ_89.setName("OBJ_89");
              panel1.add(OBJ_89);
              OBJ_89.setBounds(25, 279, 18, 20);

              //---- OBJ_91 ----
              OBJ_91.setText("11");
              OBJ_91.setName("OBJ_91");
              panel1.add(OBJ_91);
              OBJ_91.setBounds(25, 304, 18, 20);

              //---- OBJ_93 ----
              OBJ_93.setText("12");
              OBJ_93.setName("OBJ_93");
              panel1.add(OBJ_93);
              OBJ_93.setBounds(25, 329, 18, 20);

              //======== panel3 ========
              {
                panel3.setOpaque(false);
                panel3.setName("panel3");
                panel3.setLayout(null);

                //---- PO01 ----
                PO01.setText("@PO01@");
                PO01.setHorizontalAlignment(SwingConstants.RIGHT);
                PO01.setName("PO01");
                panel3.add(PO01);
                PO01.setBounds(10, 27, 95, PO01.getPreferredSize().height);

                //---- PO02 ----
                PO02.setText("@PO02@");
                PO02.setHorizontalAlignment(SwingConstants.RIGHT);
                PO02.setName("PO02");
                panel3.add(PO02);
                PO02.setBounds(10, 52, 95, PO02.getPreferredSize().height);

                //---- PO03 ----
                PO03.setText("@PO03@");
                PO03.setHorizontalAlignment(SwingConstants.RIGHT);
                PO03.setName("PO03");
                panel3.add(PO03);
                PO03.setBounds(10, 77, 95, PO03.getPreferredSize().height);

                //---- PO04 ----
                PO04.setText("@PO04@");
                PO04.setHorizontalAlignment(SwingConstants.RIGHT);
                PO04.setName("PO04");
                panel3.add(PO04);
                PO04.setBounds(10, 102, 95, PO04.getPreferredSize().height);

                //---- PO05 ----
                PO05.setText("@PO05@");
                PO05.setHorizontalAlignment(SwingConstants.RIGHT);
                PO05.setName("PO05");
                panel3.add(PO05);
                PO05.setBounds(10, 127, 95, PO05.getPreferredSize().height);

                //---- PO06 ----
                PO06.setText("@PO06@");
                PO06.setHorizontalAlignment(SwingConstants.RIGHT);
                PO06.setName("PO06");
                panel3.add(PO06);
                PO06.setBounds(10, 152, 95, PO06.getPreferredSize().height);

                //---- PO07 ----
                PO07.setText("@PO07@");
                PO07.setHorizontalAlignment(SwingConstants.RIGHT);
                PO07.setName("PO07");
                panel3.add(PO07);
                PO07.setBounds(10, 177, 95, PO07.getPreferredSize().height);

                //---- PO08 ----
                PO08.setText("@PO08@");
                PO08.setHorizontalAlignment(SwingConstants.RIGHT);
                PO08.setName("PO08");
                panel3.add(PO08);
                PO08.setBounds(10, 202, 95, PO08.getPreferredSize().height);

                //---- PO09 ----
                PO09.setText("@PO09@");
                PO09.setHorizontalAlignment(SwingConstants.RIGHT);
                PO09.setName("PO09");
                panel3.add(PO09);
                PO09.setBounds(10, 227, 95, PO09.getPreferredSize().height);

                //---- PO10 ----
                PO10.setText("@PO10@");
                PO10.setHorizontalAlignment(SwingConstants.RIGHT);
                PO10.setName("PO10");
                panel3.add(PO10);
                PO10.setBounds(10, 252, 95, PO10.getPreferredSize().height);

                //---- PO11 ----
                PO11.setText("@PO11@");
                PO11.setHorizontalAlignment(SwingConstants.RIGHT);
                PO11.setName("PO11");
                panel3.add(PO11);
                PO11.setBounds(10, 277, 95, PO11.getPreferredSize().height);

                //---- PO12 ----
                PO12.setText("@PO12@");
                PO12.setHorizontalAlignment(SwingConstants.RIGHT);
                PO12.setName("PO12");
                panel3.add(PO12);
                PO12.setBounds(10, 302, 95, PO12.getPreferredSize().height);

                //---- OBJ_69 ----
                OBJ_69.setText("Pourcentage");
                OBJ_69.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_69.setFont(OBJ_69.getFont().deriveFont(OBJ_69.getFont().getStyle() | Font.BOLD));
                OBJ_69.setName("OBJ_69");
                panel3.add(OBJ_69);
                OBJ_69.setBounds(10, 5, 95, 20);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel3.getComponentCount(); i++) {
                    Rectangle bounds = panel3.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel3.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel3.setMinimumSize(preferredSize);
                  panel3.setPreferredSize(preferredSize);
                }
              }
              panel1.add(panel3);
              panel3.setBounds(135, 25, 110, 330);
            }
            xTitledPanel4ContentContainer.add(panel1);
            panel1.setBounds(15, 85, 260, 380);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(49, Short.MAX_VALUE)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 456, GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(24, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private JPanel panel2;
  private JLabel OBJ_62;
  private XRiTextField KRLIB;
  private JPanel panel1;
  private XRiTextField KRP01;
  private XRiTextField KRP02;
  private XRiTextField KRP03;
  private XRiTextField KRP04;
  private XRiTextField KRP05;
  private XRiTextField KRP06;
  private XRiTextField KRP07;
  private XRiTextField KRP08;
  private XRiTextField KRP09;
  private XRiTextField KRP10;
  private XRiTextField KRP11;
  private XRiTextField KRP12;
  private JLabel OBJ_97;
  private JLabel OBJ_71;
  private JLabel OBJ_73;
  private JLabel OBJ_75;
  private JLabel OBJ_77;
  private JLabel OBJ_79;
  private JLabel OBJ_81;
  private JLabel OBJ_83;
  private JLabel OBJ_85;
  private JLabel OBJ_87;
  private JLabel OBJ_89;
  private JLabel OBJ_91;
  private JLabel OBJ_93;
  private JPanel panel3;
  private RiZoneSortie PO01;
  private RiZoneSortie PO02;
  private RiZoneSortie PO03;
  private RiZoneSortie PO04;
  private RiZoneSortie PO05;
  private RiZoneSortie PO06;
  private RiZoneSortie PO07;
  private RiZoneSortie PO08;
  private RiZoneSortie PO09;
  private RiZoneSortie PO10;
  private RiZoneSortie PO11;
  private RiZoneSortie PO12;
  private JLabel OBJ_69;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
