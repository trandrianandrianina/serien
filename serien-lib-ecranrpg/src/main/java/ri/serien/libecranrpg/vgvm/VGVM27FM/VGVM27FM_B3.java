
package ri.serien.libecranrpg.vgvm.VGVM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM27FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM27FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR1@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR2@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR3@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR4@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR5@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR6@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR7@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR8@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR9@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR10@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR11@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR12@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR13@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR14@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_63.setVisible(lexique.isPresent("ERR15"));
    OBJ_62.setVisible(lexique.isPresent("ERR14"));
    OBJ_61.setVisible(lexique.isPresent("ERR13"));
    OBJ_60.setVisible(lexique.isPresent("ERR12"));
    OBJ_59.setVisible(lexique.isPresent("ERR11"));
    OBJ_54.setVisible(lexique.isPresent("ERR10"));
    OBJ_52.setVisible(lexique.isPresent("ERR9"));
    OBJ_51.setVisible(lexique.isPresent("ERR8"));
    OBJ_50.setVisible(lexique.isPresent("ERR7"));
    OBJ_49.setVisible(lexique.isPresent("ERR6"));
    OBJ_48.setVisible(lexique.isPresent("ERR5"));
    OBJ_47.setVisible(lexique.isPresent("ERR4"));
    OBJ_46.setVisible(lexique.isPresent("ERR3"));
    OBJ_45.setVisible(lexique.isPresent("ERR2"));
    OBJ_44.setVisible(lexique.isPresent("ERR1"));
    if (lexique.isTrue("92")) {
      p_bpresentation.setText("Barèmes de bonifications");
    }
    else {
      p_bpresentation.setText("Barèmes de remises");
    }
    
    OBJ_12.setVisible(!lexique.isTrue("53"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - BAREME DE CONDITIONS @(TITLE)@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_53 = new JLabel();
    WETB = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    OBJ_55 = new JLabel();
    WCNV = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    Q1 = new XRiTextField();
    R1 = new XRiTextField();
    Q2 = new XRiTextField();
    R2 = new XRiTextField();
    Q3 = new XRiTextField();
    R3 = new XRiTextField();
    Q4 = new XRiTextField();
    R4 = new XRiTextField();
    Q5 = new XRiTextField();
    R5 = new XRiTextField();
    Q6 = new XRiTextField();
    R6 = new XRiTextField();
    Q7 = new XRiTextField();
    R7 = new XRiTextField();
    Q8 = new XRiTextField();
    R8 = new XRiTextField();
    Q9 = new XRiTextField();
    R9 = new XRiTextField();
    Q10 = new XRiTextField();
    R10 = new XRiTextField();
    Q11 = new XRiTextField();
    R11 = new XRiTextField();
    Q12 = new XRiTextField();
    R12 = new XRiTextField();
    Q13 = new XRiTextField();
    R13 = new XRiTextField();
    Q14 = new XRiTextField();
    R14 = new XRiTextField();
    Q15 = new XRiTextField();
    R15 = new XRiTextField();
    separator1 = compFactory.createSeparator("Montant mini", SwingConstants.CENTER);
    separator2 = compFactory.createSeparator("Rang", SwingConstants.CENTER);
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bar\u00e8mes de remises");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_53 ----
          OBJ_53.setText("Etablissement");
          OBJ_53.setName("OBJ_53");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- OBJ_55 ----
          OBJ_55.setText("Code");
          OBJ_55.setName("OBJ_55");

          //---- WCNV ----
          WCNV.setComponentPopupMenu(BTD);
          WCNV.setName("WCNV");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
              .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(260, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_44 ----
            OBJ_44.setText("@ERR1@");
            OBJ_44.setForeground(new Color(153, 0, 0));
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(25, 45, 42, 28);

            //---- OBJ_45 ----
            OBJ_45.setText("@ERR2@");
            OBJ_45.setForeground(new Color(153, 0, 0));
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(25, 76, 42, 28);

            //---- OBJ_46 ----
            OBJ_46.setText("@ERR3@");
            OBJ_46.setForeground(new Color(153, 0, 0));
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(25, 107, 42, 28);

            //---- OBJ_47 ----
            OBJ_47.setText("@ERR4@");
            OBJ_47.setForeground(new Color(153, 0, 0));
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(25, 138, 42, 28);

            //---- OBJ_48 ----
            OBJ_48.setText("@ERR5@");
            OBJ_48.setForeground(new Color(153, 0, 0));
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(25, 169, 42, 28);

            //---- OBJ_49 ----
            OBJ_49.setText("@ERR6@");
            OBJ_49.setForeground(new Color(153, 0, 0));
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(25, 200, 42, 28);

            //---- OBJ_50 ----
            OBJ_50.setText("@ERR7@");
            OBJ_50.setForeground(new Color(153, 0, 0));
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(25, 231, 42, 28);

            //---- OBJ_51 ----
            OBJ_51.setText("@ERR8@");
            OBJ_51.setForeground(new Color(153, 0, 0));
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(25, 262, 42, 28);

            //---- OBJ_52 ----
            OBJ_52.setText("@ERR9@");
            OBJ_52.setForeground(new Color(153, 0, 0));
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(25, 293, 42, 28);

            //---- OBJ_54 ----
            OBJ_54.setText("@ERR10@");
            OBJ_54.setForeground(new Color(153, 0, 0));
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(25, 324, 42, 28);

            //---- OBJ_59 ----
            OBJ_59.setText("@ERR11@");
            OBJ_59.setForeground(new Color(153, 0, 0));
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(25, 355, 42, 28);

            //---- OBJ_60 ----
            OBJ_60.setText("@ERR12@");
            OBJ_60.setForeground(new Color(153, 0, 0));
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(25, 386, 42, 28);

            //---- OBJ_61 ----
            OBJ_61.setText("@ERR13@");
            OBJ_61.setForeground(new Color(153, 0, 0));
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(25, 417, 42, 28);

            //---- OBJ_62 ----
            OBJ_62.setText("@ERR14@");
            OBJ_62.setForeground(new Color(153, 0, 0));
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(25, 448, 42, 28);

            //---- OBJ_63 ----
            OBJ_63.setText("@ERR15@");
            OBJ_63.setForeground(new Color(153, 0, 0));
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(25, 479, 42, 28);

            //---- Q1 ----
            Q1.setHorizontalAlignment(SwingConstants.RIGHT);
            Q1.setName("Q1");
            panel1.add(Q1);
            Q1.setBounds(65, 45, 80, Q1.getPreferredSize().height);

            //---- R1 ----
            R1.setHorizontalAlignment(SwingConstants.RIGHT);
            R1.setName("R1");
            panel1.add(R1);
            R1.setBounds(160, 45, 30, R1.getPreferredSize().height);

            //---- Q2 ----
            Q2.setHorizontalAlignment(SwingConstants.RIGHT);
            Q2.setName("Q2");
            panel1.add(Q2);
            Q2.setBounds(65, 76, 80, Q2.getPreferredSize().height);

            //---- R2 ----
            R2.setHorizontalAlignment(SwingConstants.RIGHT);
            R2.setName("R2");
            panel1.add(R2);
            R2.setBounds(160, 76, 30, R2.getPreferredSize().height);

            //---- Q3 ----
            Q3.setHorizontalAlignment(SwingConstants.RIGHT);
            Q3.setName("Q3");
            panel1.add(Q3);
            Q3.setBounds(65, 107, 80, Q3.getPreferredSize().height);

            //---- R3 ----
            R3.setHorizontalAlignment(SwingConstants.RIGHT);
            R3.setName("R3");
            panel1.add(R3);
            R3.setBounds(160, 107, 30, R3.getPreferredSize().height);

            //---- Q4 ----
            Q4.setHorizontalAlignment(SwingConstants.RIGHT);
            Q4.setName("Q4");
            panel1.add(Q4);
            Q4.setBounds(65, 138, 80, Q4.getPreferredSize().height);

            //---- R4 ----
            R4.setHorizontalAlignment(SwingConstants.RIGHT);
            R4.setName("R4");
            panel1.add(R4);
            R4.setBounds(160, 138, 30, R4.getPreferredSize().height);

            //---- Q5 ----
            Q5.setHorizontalAlignment(SwingConstants.RIGHT);
            Q5.setName("Q5");
            panel1.add(Q5);
            Q5.setBounds(65, 169, 80, Q5.getPreferredSize().height);

            //---- R5 ----
            R5.setHorizontalAlignment(SwingConstants.RIGHT);
            R5.setName("R5");
            panel1.add(R5);
            R5.setBounds(160, 169, 30, R5.getPreferredSize().height);

            //---- Q6 ----
            Q6.setHorizontalAlignment(SwingConstants.RIGHT);
            Q6.setName("Q6");
            panel1.add(Q6);
            Q6.setBounds(65, 200, 80, Q6.getPreferredSize().height);

            //---- R6 ----
            R6.setHorizontalAlignment(SwingConstants.RIGHT);
            R6.setName("R6");
            panel1.add(R6);
            R6.setBounds(160, 200, 30, R6.getPreferredSize().height);

            //---- Q7 ----
            Q7.setHorizontalAlignment(SwingConstants.RIGHT);
            Q7.setName("Q7");
            panel1.add(Q7);
            Q7.setBounds(65, 231, 80, Q7.getPreferredSize().height);

            //---- R7 ----
            R7.setHorizontalAlignment(SwingConstants.RIGHT);
            R7.setName("R7");
            panel1.add(R7);
            R7.setBounds(160, 231, 30, R7.getPreferredSize().height);

            //---- Q8 ----
            Q8.setHorizontalAlignment(SwingConstants.RIGHT);
            Q8.setName("Q8");
            panel1.add(Q8);
            Q8.setBounds(65, 262, 80, Q8.getPreferredSize().height);

            //---- R8 ----
            R8.setHorizontalAlignment(SwingConstants.RIGHT);
            R8.setName("R8");
            panel1.add(R8);
            R8.setBounds(160, 262, 30, R8.getPreferredSize().height);

            //---- Q9 ----
            Q9.setHorizontalAlignment(SwingConstants.RIGHT);
            Q9.setName("Q9");
            panel1.add(Q9);
            Q9.setBounds(65, 293, 80, Q9.getPreferredSize().height);

            //---- R9 ----
            R9.setHorizontalAlignment(SwingConstants.RIGHT);
            R9.setName("R9");
            panel1.add(R9);
            R9.setBounds(160, 293, 30, R9.getPreferredSize().height);

            //---- Q10 ----
            Q10.setHorizontalAlignment(SwingConstants.RIGHT);
            Q10.setName("Q10");
            panel1.add(Q10);
            Q10.setBounds(65, 324, 80, Q10.getPreferredSize().height);

            //---- R10 ----
            R10.setHorizontalAlignment(SwingConstants.RIGHT);
            R10.setName("R10");
            panel1.add(R10);
            R10.setBounds(160, 324, 30, R10.getPreferredSize().height);

            //---- Q11 ----
            Q11.setHorizontalAlignment(SwingConstants.RIGHT);
            Q11.setName("Q11");
            panel1.add(Q11);
            Q11.setBounds(65, 355, 80, Q11.getPreferredSize().height);

            //---- R11 ----
            R11.setHorizontalAlignment(SwingConstants.RIGHT);
            R11.setName("R11");
            panel1.add(R11);
            R11.setBounds(160, 355, 30, R11.getPreferredSize().height);

            //---- Q12 ----
            Q12.setHorizontalAlignment(SwingConstants.RIGHT);
            Q12.setName("Q12");
            panel1.add(Q12);
            Q12.setBounds(65, 386, 80, Q12.getPreferredSize().height);

            //---- R12 ----
            R12.setHorizontalAlignment(SwingConstants.RIGHT);
            R12.setName("R12");
            panel1.add(R12);
            R12.setBounds(160, 386, 30, R12.getPreferredSize().height);

            //---- Q13 ----
            Q13.setHorizontalAlignment(SwingConstants.RIGHT);
            Q13.setName("Q13");
            panel1.add(Q13);
            Q13.setBounds(65, 417, 80, Q13.getPreferredSize().height);

            //---- R13 ----
            R13.setHorizontalAlignment(SwingConstants.RIGHT);
            R13.setName("R13");
            panel1.add(R13);
            R13.setBounds(160, 417, 30, R13.getPreferredSize().height);

            //---- Q14 ----
            Q14.setHorizontalAlignment(SwingConstants.RIGHT);
            Q14.setName("Q14");
            panel1.add(Q14);
            Q14.setBounds(65, 448, 80, Q14.getPreferredSize().height);

            //---- R14 ----
            R14.setHorizontalAlignment(SwingConstants.RIGHT);
            R14.setName("R14");
            panel1.add(R14);
            R14.setBounds(160, 448, 30, R14.getPreferredSize().height);

            //---- Q15 ----
            Q15.setHorizontalAlignment(SwingConstants.RIGHT);
            Q15.setName("Q15");
            panel1.add(Q15);
            Q15.setBounds(65, 479, 80, Q15.getPreferredSize().height);

            //---- R15 ----
            R15.setHorizontalAlignment(SwingConstants.RIGHT);
            R15.setName("R15");
            panel1.add(R15);
            R15.setBounds(160, 479, 30, R15.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(65, 20, 80, separator1.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            panel1.add(separator2);
            separator2.setBounds(155, 20, 40, separator2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(15, 15, 230, 535);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_53;
  private XRiTextField WETB;
  private SNBoutonRecherche BT_ChgSoc;
  private JLabel OBJ_55;
  private XRiTextField WCNV;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private XRiTextField Q1;
  private XRiTextField R1;
  private XRiTextField Q2;
  private XRiTextField R2;
  private XRiTextField Q3;
  private XRiTextField R3;
  private XRiTextField Q4;
  private XRiTextField R4;
  private XRiTextField Q5;
  private XRiTextField R5;
  private XRiTextField Q6;
  private XRiTextField R6;
  private XRiTextField Q7;
  private XRiTextField R7;
  private XRiTextField Q8;
  private XRiTextField R8;
  private XRiTextField Q9;
  private XRiTextField R9;
  private XRiTextField Q10;
  private XRiTextField R10;
  private XRiTextField Q11;
  private XRiTextField R11;
  private XRiTextField Q12;
  private XRiTextField R12;
  private XRiTextField Q13;
  private XRiTextField R13;
  private XRiTextField Q14;
  private XRiTextField R14;
  private XRiTextField Q15;
  private XRiTextField R15;
  private JComponent separator1;
  private JComponent separator2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
