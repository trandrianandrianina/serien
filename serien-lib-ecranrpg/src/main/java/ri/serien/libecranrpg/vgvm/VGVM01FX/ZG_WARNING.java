
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class ZG_WARNING extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  
  public ZG_WARNING(Lexical lexical, iData interpreteur) {
    lexique = lexical;
    interpreteurD = interpreteur;
    initComponents();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Titre
    setTitle("Autres options");
  }
  
  public void getData() {
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    P_Centre = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(645, 170));
    setResizable(false);
    setTitle("Zones g\u00e9ographiques");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());
      
      // ======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());
        
        // ======== navig_retour ========
        {
          navig_retour.setName("navig_retour");
          
          // ---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
    }
    contentPane.add(p_menus, BorderLayout.EAST);
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);
      
      // ---- label1 ----
      label1.setText("Vous ne pouvez s\u00e9lectionner que 40 d\u00e9partements maximum");
      label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
      label1.setHorizontalAlignment(SwingConstants.CENTER);
      label1.setName("label1");
      P_Centre.add(label1);
      label1.setBounds(5, 35, 455, 35);
      
      // ---- label2 ----
      label2.setText("par zone g\u00e9ographique");
      label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 3f));
      label2.setHorizontalAlignment(SwingConstants.CENTER);
      label2.setName("label2");
      P_Centre.add(label2);
      label2.setBounds(5, 70, 455, 35);
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel P_Centre;
  private JLabel label1;
  private JLabel label2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
