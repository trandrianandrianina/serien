
package ri.serien.libecranrpg.vgvm.VGVM11XF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11XF_B3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11XF_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    L1LIB1.activerModeFantome("Libellé");
    WLOTP.activerModeFantome("Lot");
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("Saisie ligne de commande");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCHPA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    L1VAL.setVisible(lexique.isPresent("L1VAL"));
    L1TAR.setVisible(lexique.isPresent("L1TAR"));
    L1IN2.setVisible(lexique.isPresent("L1IN2"));
    L1IN3.setVisible(lexique.isPresent("L1IN3"));
    L1AVR.setVisible(lexique.isPresent("L1AVR"));
    L1REM3.setVisible(lexique.isPresent("L1REM3"));
    L1REM2.setVisible(lexique.isPresent("L1REM2"));
    L1REM1.setVisible(lexique.isPresent("L1REM1"));
    WART23.setVisible(lexique.isPresent("WART23"));
    WART22.setVisible(lexique.isPresent("WART22"));
    WQTEX.setEnabled(lexique.isPresent("WQTEX"));
    WART21.setVisible(lexique.isPresent("WART21"));
    L1PVBX.setEnabled(lexique.isPresent("L1PVBX"));
    L1ART.setVisible(lexique.isPresent("L1ART"));
    OBJ_16.setVisible(lexique.isPresent("WCHPA"));
    WLOTP.setVisible(true);
    WLOTS.setVisible(true);
    WLOTP.setEnabled(lexique.isPresent("WLOTP"));
    WLOTS.setEnabled(WLOTP.isEnabled());
    W11XAN.setEnabled(lexique.isPresent("W11XAN"));
    L1LIB1.setEnabled(lexique.isPresent("L1LIB1"));
    
    if (lexique.isTrue("N23")) {
      L1PVBX.setEnabled(true);
      L1REM1.setEnabled(true);
      L1REM2.setEnabled(true);
      L1REM3.setEnabled(true);
      L1TAR.setEnabled(true);
      L1AVR.setEnabled(true);
      L1VAL.setEnabled(true);
      L1IN3.setEnabled(true);
      L1IN2.setEnabled(true);
    }
    else {
      L1PVBX.setEnabled(false);
      L1REM1.setEnabled(false);
      L1REM2.setEnabled(false);
      L1REM3.setEnabled(false);
      L1TAR.setEnabled(false);
      L1AVR.setEnabled(false);
      L1VAL.setEnabled(false);
      L1IN3.setEnabled(false);
      L1IN2.setEnabled(false);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void L1ARTActionPerformed(ActionEvent e) {
    // Afin d'améliorer la saisie rapide, l'appuie sur Entrée dans le code article met le focus dans le champ quantité
    try {
      WQTEX.requestFocusInWindow();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_16 = new JLabel();
    WART21 = new XRiTextField();
    WART22 = new XRiTextField();
    WART23 = new XRiTextField();
    L1ART = new XRiTextField();
    OBJ_17 = new JLabel();
    WQTEX = new XRiTextField();
    L1PVBX = new XRiTextField();
    L1REM1 = new XRiTextField();
    L1REM2 = new XRiTextField();
    L1REM3 = new XRiTextField();
    OBJ_18 = new JLabel();
    OBJ_19 = new JLabel();
    WLOTP = new XRiTextField();
    panel2 = new JPanel();
    OBJ_20 = new JLabel();
    L1TAR = new XRiTextField();
    OBJ_21 = new JLabel();
    L1AVR = new XRiTextField();
    OBJ_22 = new JLabel();
    L1VAL = new XRiTextField();
    OBJ_23 = new JLabel();
    L1IN3 = new XRiTextField();
    OBJ_24 = new JLabel();
    L1IN2 = new XRiTextField();
    WLOTS = new XRiTextField();
    L1LIB1 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    W11XAN = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(910, 235));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_16 ----
          OBJ_16.setText("@WCHPA@");
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(22, 18, 233, 20);

          //---- WART21 ----
          WART21.setComponentPopupMenu(BTD);
          WART21.setName("WART21");
          panel1.add(WART21);
          WART21.setBounds(20, 40, 130, WART21.getPreferredSize().height);

          //---- WART22 ----
          WART22.setComponentPopupMenu(BTD);
          WART22.setName("WART22");
          panel1.add(WART22);
          WART22.setBounds(155, 40, 50, WART22.getPreferredSize().height);

          //---- WART23 ----
          WART23.setComponentPopupMenu(BTD);
          WART23.setName("WART23");
          panel1.add(WART23);
          WART23.setBounds(210, 40, 50, WART23.getPreferredSize().height);

          //---- L1ART ----
          L1ART.setComponentPopupMenu(BTD);
          L1ART.setName("L1ART");
          L1ART.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              L1ARTActionPerformed(e);
            }
          });
          panel1.add(L1ART);
          L1ART.setBounds(20, 40, 210, L1ART.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("Quantit\u00e9");
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(283, 18, 70, 20);

          //---- WQTEX ----
          WQTEX.setComponentPopupMenu(BTD);
          WQTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          WQTEX.setName("WQTEX");
          panel1.add(WQTEX);
          WQTEX.setBounds(280, 40, 90, WQTEX.getPreferredSize().height);

          //---- L1PVBX ----
          L1PVBX.setComponentPopupMenu(BTD);
          L1PVBX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVBX.setName("L1PVBX");
          panel1.add(L1PVBX);
          L1PVBX.setBounds(385, 40, 110, L1PVBX.getPreferredSize().height);

          //---- L1REM1 ----
          L1REM1.setComponentPopupMenu(BTD);
          L1REM1.setHorizontalAlignment(SwingConstants.RIGHT);
          L1REM1.setName("L1REM1");
          panel1.add(L1REM1);
          L1REM1.setBounds(515, 40, 50, L1REM1.getPreferredSize().height);

          //---- L1REM2 ----
          L1REM2.setComponentPopupMenu(BTD);
          L1REM2.setHorizontalAlignment(SwingConstants.RIGHT);
          L1REM2.setName("L1REM2");
          panel1.add(L1REM2);
          L1REM2.setBounds(565, 40, 50, L1REM2.getPreferredSize().height);

          //---- L1REM3 ----
          L1REM3.setComponentPopupMenu(BTD);
          L1REM3.setHorizontalAlignment(SwingConstants.RIGHT);
          L1REM3.setName("L1REM3");
          panel1.add(L1REM3);
          L1REM3.setBounds(615, 40, 50, L1REM3.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("Prix base");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(387, 18, 77, 20);

          //---- OBJ_19 ----
          OBJ_19.setText("Remises");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(517, 18, 75, 20);

          //---- WLOTP ----
          WLOTP.setComponentPopupMenu(BTD);
          WLOTP.setName("WLOTP");
          panel1.add(WLOTP);
          WLOTP.setBounds(20, 110, 240, WLOTP.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_20 ----
            OBJ_20.setText("Ta");
            OBJ_20.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_20.setName("OBJ_20");
            panel2.add(OBJ_20);
            OBJ_20.setBounds(27, 5, 20, 20);

            //---- L1TAR ----
            L1TAR.setComponentPopupMenu(BTD);
            L1TAR.setName("L1TAR");
            panel2.add(L1TAR);
            L1TAR.setBounds(20, 35, 34, L1TAR.getPreferredSize().height);

            //---- OBJ_21 ----
            OBJ_21.setText("A");
            OBJ_21.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_21.setName("OBJ_21");
            panel2.add(OBJ_21);
            OBJ_21.setBounds(62, 5, 20, 20);

            //---- L1AVR ----
            L1AVR.setComponentPopupMenu(BTD);
            L1AVR.setName("L1AVR");
            panel2.add(L1AVR);
            L1AVR.setBounds(60, 35, 24, L1AVR.getPreferredSize().height);

            //---- OBJ_22 ----
            OBJ_22.setText("V");
            OBJ_22.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22.setName("OBJ_22");
            panel2.add(OBJ_22);
            OBJ_22.setBounds(90, 5, 20, 20);

            //---- L1VAL ----
            L1VAL.setComponentPopupMenu(BTD);
            L1VAL.setName("L1VAL");
            panel2.add(L1VAL);
            L1VAL.setBounds(90, 35, 20, L1VAL.getPreferredSize().height);

            //---- OBJ_23 ----
            OBJ_23.setText("R");
            OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_23.setName("OBJ_23");
            panel2.add(OBJ_23);
            OBJ_23.setBounds(117, 5, 20, 20);

            //---- L1IN3 ----
            L1IN3.setComponentPopupMenu(BTD);
            L1IN3.setName("L1IN3");
            panel2.add(L1IN3);
            L1IN3.setBounds(115, 35, 24, L1IN3.getPreferredSize().height);

            //---- OBJ_24 ----
            OBJ_24.setText("G");
            OBJ_24.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_24.setName("OBJ_24");
            panel2.add(OBJ_24);
            OBJ_24.setBounds(147, 5, 20, 20);

            //---- L1IN2 ----
            L1IN2.setComponentPopupMenu(BTD);
            L1IN2.setName("L1IN2");
            panel2.add(L1IN2);
            L1IN2.setBounds(145, 35, 24, L1IN2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(495, 75, 190, 75);

          //---- WLOTS ----
          WLOTS.setComponentPopupMenu(BTD);
          WLOTS.setName("WLOTS");
          panel1.add(WLOTS);
          WLOTS.setBounds(262, 110, 35, WLOTS.getPreferredSize().height);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          panel1.add(L1LIB1);
          L1LIB1.setBounds(20, 75, 300, L1LIB1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 705, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(35, 35, 35)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- W11XAN ----
    W11XAN.setComponentPopupMenu(BTD);
    W11XAN.setName("W11XAN");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_16;
  private XRiTextField WART21;
  private XRiTextField WART22;
  private XRiTextField WART23;
  private XRiTextField L1ART;
  private JLabel OBJ_17;
  private XRiTextField WQTEX;
  private XRiTextField L1PVBX;
  private XRiTextField L1REM1;
  private XRiTextField L1REM2;
  private XRiTextField L1REM3;
  private JLabel OBJ_18;
  private JLabel OBJ_19;
  private XRiTextField WLOTP;
  private JPanel panel2;
  private JLabel OBJ_20;
  private XRiTextField L1TAR;
  private JLabel OBJ_21;
  private XRiTextField L1AVR;
  private JLabel OBJ_22;
  private XRiTextField L1VAL;
  private JLabel OBJ_23;
  private XRiTextField L1IN3;
  private JLabel OBJ_24;
  private XRiTextField L1IN2;
  private XRiTextField WLOTS;
  private XRiTextField L1LIB1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private XRiTextField W11XAN;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
