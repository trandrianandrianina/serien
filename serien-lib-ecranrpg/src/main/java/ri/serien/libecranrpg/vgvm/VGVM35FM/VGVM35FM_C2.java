//$$david$$ ££12/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNConditionCommissionnement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_C2 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_COMMISSIONNEMENT = "Gérer les com.";
  private String[] RPMOC_Value = { "1", "0", "2", };
  
  public VGVM35FM_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    RPMOC.setValeurs(RPMOC_Value, null);
    RPIN11.setValeursSelection("1", " ");
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(BOUTON_COMMISSIONNEMENT, 'g', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    RPLEM.setEnabled(lexique.isPresent("RPLEM"));
    RPNTA.setEnabled(lexique.isPresent("RPNTA"));
    RPTYA.setEnabled(lexique.isPresent("RPTYA"));
    snConditionCommissionnement.setEnabled(lexique.isPresent("RPCNC"));
    RPCOM.setEnabled(lexique.isPresent("RPCOM"));
    RPSEU.setEnabled(lexique.isPresent("RPSEU"));
    RPMOC.setEnabled(!lexique.isTrue("53"));
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    snConditionCommissionnement.setSession(getSession());
    snConditionCommissionnement.setIdEtablissement(idEtablissement);
    snConditionCommissionnement.charger(false);
    snConditionCommissionnement.setSelectionParChampRPG(lexique, "RPCNC");
    snConditionCommissionnement.setEnabled(!isConsultation);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Fiche représentant"));
    
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snConditionCommissionnement.renseignerChampRPG(lexique, "RPCNC");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_COMMISSIONNEMENT)) {
        lexique.HostScreenSendKey(this, "F2");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    sNPanelTitre1 = new SNPanelTitre();
    sNPanel2 = new SNPanel();
    sNLabelChamp6 = new SNLabelChamp();
    snConditionCommissionnement = new SNConditionCommissionnement();
    sNLabelChamp4 = new SNLabelChamp();
    RPSEU = new XRiTextField();
    sNLabelChamp5 = new SNLabelChamp();
    RPCOM = new XRiTextField();
    sNLabelChamp7 = new SNLabelChamp();
    RPMOC = new XRiComboBox();
    sNPanelTitre2 = new SNPanelTitre();
    sNPanel1 = new SNPanel();
    sNLabelChamp1 = new SNLabelChamp();
    RPTYA = new XRiTextField();
    sNLabelChamp2 = new SNLabelChamp();
    pnlAction = new SNPanel();
    RPNTA = new XRiTextField();
    RPIN11 = new XRiCheckBox();
    sNLabelChamp3 = new SNLabelChamp();
    RPLEM = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(745, 500));
    setTitle("Type d'actin");
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout)sNPanelContenu1.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)sNPanelContenu1.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)sNPanelContenu1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)sNPanelContenu1.getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

      //======== sNPanelTitre1 ========
      {
        sNPanelTitre1.setTitre("Comissionnement");
        sNPanelTitre1.setName("sNPanelTitre1");
        sNPanelTitre1.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanelTitre1.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)sNPanelTitre1.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)sNPanelTitre1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)sNPanelTitre1.getLayout()).rowWeights = new double[] {1.0, 1.0E-4};

        //======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanel2.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanel2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)sNPanel2.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)sNPanel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- sNLabelChamp6 ----
          sNLabelChamp6.setText("Regroupement");
          sNLabelChamp6.setName("sNLabelChamp6");
          sNPanel2.add(sNLabelChamp6, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snConditionCommissionnement ----
          snConditionCommissionnement.setName("snConditionCommissionnement");
          sNPanel2.add(snConditionCommissionnement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabelChamp4 ----
          sNLabelChamp4.setText("Seuil");
          sNLabelChamp4.setName("sNLabelChamp4");
          sNPanel2.add(sNLabelChamp4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RPSEU ----
          RPSEU.setMinimumSize(new Dimension(80, 30));
          RPSEU.setPreferredSize(new Dimension(80, 30));
          RPSEU.setName("RPSEU");
          sNPanel2.add(RPSEU, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabelChamp5 ----
          sNLabelChamp5.setText("Taux");
          sNLabelChamp5.setName("sNLabelChamp5");
          sNPanel2.add(sNLabelChamp5, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RPCOM ----
          RPCOM.setMinimumSize(new Dimension(60, 30));
          RPCOM.setPreferredSize(new Dimension(60, 30));
          RPCOM.setName("RPCOM");
          sNPanel2.add(RPCOM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabelChamp7 ----
          sNLabelChamp7.setText("Base");
          sNLabelChamp7.setName("sNLabelChamp7");
          sNPanel2.add(sNLabelChamp7, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- RPMOC ----
          RPMOC.setModel(new DefaultComboBoxModel(new String[] {
            "Sur marge",
            "Sur chiffre d'affaires",
            "Hors droits sur alcools"
          }));
          RPMOC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RPMOC.setPreferredSize(new Dimension(200, 30));
          RPMOC.setMinimumSize(new Dimension(200, 30));
          RPMOC.setMaximumSize(new Dimension(250, 30));
          RPMOC.setFont(new Font("sansserif", Font.PLAIN, 14));
          RPMOC.setBackground(Color.white);
          RPMOC.setName("RPMOC");
          sNPanel2.add(RPMOC, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre1.add(sNPanel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== sNPanelTitre2 ========
      {
        sNPanelTitre2.setTitre("Actions de prospection");
        sNPanelTitre2.setName("sNPanelTitre2");
        sNPanelTitre2.setLayout(new GridBagLayout());
        ((GridBagLayout)sNPanelTitre2.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)sNPanelTitre2.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)sNPanelTitre2.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)sNPanelTitre2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- sNLabelChamp1 ----
          sNLabelChamp1.setText("Type d'action");
          sNLabelChamp1.setName("sNLabelChamp1");
          sNPanel1.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- RPTYA ----
          RPTYA.setMinimumSize(new Dimension(40, 30));
          RPTYA.setPreferredSize(new Dimension(40, 30));
          RPTYA.setName("RPTYA");
          sNPanel1.add(RPTYA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabelChamp2 ----
          sNLabelChamp2.setText("Nombre d'actions au cours des 12 derniers mois");
          sNLabelChamp2.setPreferredSize(new Dimension(350, 30));
          sNLabelChamp2.setName("sNLabelChamp2");
          sNPanel1.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlAction ========
          {
            pnlAction.setName("pnlAction");
            pnlAction.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlAction.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlAction.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlAction.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlAction.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- RPNTA ----
            RPNTA.setMinimumSize(new Dimension(30, 30));
            RPNTA.setPreferredSize(new Dimension(30, 30));
            RPNTA.setName("RPNTA");
            pnlAction.add(RPNTA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- RPIN11 ----
            RPIN11.setText("Exprim\u00e9 en dixi\u00e8me");
            RPIN11.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPIN11.setMaximumSize(new Dimension(200, 30));
            RPIN11.setMinimumSize(new Dimension(200, 30));
            RPIN11.setPreferredSize(new Dimension(200, 30));
            RPIN11.setName("RPIN11");
            pnlAction.add(RPIN11, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanel1.add(pnlAction, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- sNLabelChamp3 ----
          sNLabelChamp3.setText("Limite derni\u00e8re action en nombre de mois");
          sNLabelChamp3.setName("sNLabelChamp3");
          sNPanel1.add(sNLabelChamp3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- RPLEM ----
          RPLEM.setMinimumSize(new Dimension(30, 30));
          RPLEM.setPreferredSize(new Dimension(30, 30));
          RPLEM.setName("RPLEM");
          sNPanel1.add(RPLEM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre2.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanelTitre2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanelTitre sNPanelTitre1;
  private SNPanel sNPanel2;
  private SNLabelChamp sNLabelChamp6;
  private SNConditionCommissionnement snConditionCommissionnement;
  private SNLabelChamp sNLabelChamp4;
  private XRiTextField RPSEU;
  private SNLabelChamp sNLabelChamp5;
  private XRiTextField RPCOM;
  private SNLabelChamp sNLabelChamp7;
  private XRiComboBox RPMOC;
  private SNPanelTitre sNPanelTitre2;
  private SNPanel sNPanel1;
  private SNLabelChamp sNLabelChamp1;
  private XRiTextField RPTYA;
  private SNLabelChamp sNLabelChamp2;
  private SNPanel pnlAction;
  private XRiTextField RPNTA;
  private XRiCheckBox RPIN11;
  private SNLabelChamp sNLabelChamp3;
  private XRiTextField RPLEM;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
