
package ri.serien.libecranrpg.vgvm.VGVM113F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM113F_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM113F_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTE3.setValeursSelection("1", " ");
    WTE2.setValeursSelection("1", " ");
    WTE1.setValeursSelection("1", " ");
    P13IN7.setValeursSelection("1", "0");
    WVAL.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    setDialog(true);
    
    WIN2.setEnabled(lexique.isPresent("WIN2"));
    WREM3.setEnabled(lexique.isPresent("WREM3"));
    WREM2.setEnabled(lexique.isPresent("WREM2"));
    WREM1.setEnabled(lexique.isPresent("WREM1"));
    WPVN.setEnabled(lexique.isPresent("WPVN"));
    WPVB.setEnabled(lexique.isPresent("WPVB"));
    // P13IN7.setEnabled( lexique.isPresent("P13IN7"));
    // P13IN7.setSelected(lexique.HostFieldGetData("P13IN7").equalsIgnoreCase("1"));
    // WVAL.setEnabled( lexique.isPresent("WVAL"));
    // WVAL.setSelected(!lexique.HostFieldGetData("WVAL").trim().equalsIgnoreCase(""));
    // WTE1.setSelected(!lexique.HostFieldGetData("WTE1").trim().equalsIgnoreCase(""));
    // WTE2.setSelected(!lexique.HostFieldGetData("WTE2").trim().equalsIgnoreCase(""));
    // WTE3.setSelected(!lexique.HostFieldGetData("WTE3").trim().equalsIgnoreCase(""));
    
    // Titre
    setTitle("Elements répercutés sur lignes");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (P13IN7.isSelected())
    // lexique.HostFieldPutData("P13IN7", 0, "1");
    // else
    // lexique.HostFieldPutData("P13IN7", 0, "0");
    
    // if (WVAL.isSelected())
    // lexique.HostFieldPutData("WVAL", 0, "1");
    // else
    // lexique.HostFieldPutData("WVAL", 0, " ");
    
    // if (WTE1.isSelected())
    // lexique.HostFieldPutData("WTE1", 0, "1");
    // else
    // lexique.HostFieldPutData("WTE1", 0, " ");
    // if (WTE2.isSelected())
    // lexique.HostFieldPutData("WTE2", 0, "1");
    // else
    // lexique.HostFieldPutData("WTE2", 0, " ");
    // if (WTE3.isSelected())
    // lexique.HostFieldPutData("WTE3", 0, "1");
    // else
    // lexique.HostFieldPutData("WTE3", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WVAL = new XRiCheckBox();
    OBJ_34 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_32 = new JLabel();
    WPVB = new XRiTextField();
    WPVN = new XRiTextField();
    WREM1 = new XRiTextField();
    WREM2 = new XRiTextField();
    WREM3 = new XRiTextField();
    WIN2 = new XRiTextField();
    P13IN7 = new XRiCheckBox();
    panel1 = new JPanel();
    WTE1 = new XRiCheckBox();
    WTE2 = new XRiCheckBox();
    WTE3 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(570, 390));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("El\u00e9ments r\u00e9percut\u00e9s sur lignes"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- WVAL ----
          WVAL.setText("Ligne en valeur");
          WVAL.setToolTipText("Ligne en valeur");
          WVAL.setComponentPopupMenu(BTD);
          WVAL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WVAL.setName("WVAL");
          panel2.add(WVAL);
          WVAL.setBounds(25, 155, 200, 20);
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Type de gratuit");
          OBJ_34.setName("OBJ_34");
          panel2.add(OBJ_34);
          OBJ_34.setBounds(25, 125, 118, 20);
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Prix de base");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(25, 35, 118, 20);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("Remises");
          OBJ_28.setName("OBJ_28");
          panel2.add(OBJ_28);
          OBJ_28.setBounds(25, 65, 118, 20);
          
          // ---- OBJ_32 ----
          OBJ_32.setText("Prix net");
          OBJ_32.setName("OBJ_32");
          panel2.add(OBJ_32);
          OBJ_32.setBounds(25, 95, 118, 20);
          
          // ---- WPVB ----
          WPVB.setComponentPopupMenu(BTD);
          WPVB.setName("WPVB");
          panel2.add(WPVB);
          WPVB.setBounds(160, 31, 74, WPVB.getPreferredSize().height);
          
          // ---- WPVN ----
          WPVN.setComponentPopupMenu(BTD);
          WPVN.setName("WPVN");
          panel2.add(WPVN);
          WPVN.setBounds(160, 91, 74, WPVN.getPreferredSize().height);
          
          // ---- WREM1 ----
          WREM1.setComponentPopupMenu(BTD);
          WREM1.setName("WREM1");
          panel2.add(WREM1);
          WREM1.setBounds(160, 61, 50, WREM1.getPreferredSize().height);
          
          // ---- WREM2 ----
          WREM2.setComponentPopupMenu(BTD);
          WREM2.setName("WREM2");
          panel2.add(WREM2);
          WREM2.setBounds(210, 61, 50, WREM2.getPreferredSize().height);
          
          // ---- WREM3 ----
          WREM3.setComponentPopupMenu(BTD);
          WREM3.setName("WREM3");
          panel2.add(WREM3);
          WREM3.setBounds(260, 61, 50, WREM3.getPreferredSize().height);
          
          // ---- WIN2 ----
          WIN2.setComponentPopupMenu(BTD);
          WIN2.setName("WIN2");
          panel2.add(WIN2);
          WIN2.setBounds(160, 121, 24, WIN2.getPreferredSize().height);
          
          // ---- P13IN7 ----
          P13IN7.setText("Actif");
          P13IN7.setComponentPopupMenu(BTD);
          P13IN7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P13IN7.setName("P13IN7");
          panel2.add(P13IN7);
          P13IN7.setBounds(300, 0, 60, 16);
        }
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Options d'\u00e9dition"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- WTE1 ----
          WTE1.setText("Ne pas \u00e9diter le code article");
          WTE1.setName("WTE1");
          panel1.add(WTE1);
          WTE1.setBounds(25, 35, 235, WTE1.getPreferredSize().height);
          
          // ---- WTE2 ----
          WTE2.setText("Ne pas \u00e9diter le libell\u00e9");
          WTE2.setName("WTE2");
          panel1.add(WTE2);
          WTE2.setBounds(25, 65, 235, 18);
          
          // ---- WTE3 ----
          WTE3.setText("Ne pas \u00e9diter le prix de vente");
          WTE3.setName("WTE3");
          panel1.add(WTE3);
          WTE3.setBounds(25, 95, 235, 18);
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                    .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(25, 25, 25)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE).addContainerGap(22, Short.MAX_VALUE)));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiCheckBox WVAL;
  private JLabel OBJ_34;
  private JLabel OBJ_26;
  private JLabel OBJ_28;
  private JLabel OBJ_32;
  private XRiTextField WPVB;
  private XRiTextField WPVN;
  private XRiTextField WREM1;
  private XRiTextField WREM2;
  private XRiTextField WREM3;
  private XRiTextField WIN2;
  private XRiCheckBox P13IN7;
  private JPanel panel1;
  private XRiCheckBox WTE1;
  private XRiCheckBox WTE2;
  private XRiCheckBox WTE3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
