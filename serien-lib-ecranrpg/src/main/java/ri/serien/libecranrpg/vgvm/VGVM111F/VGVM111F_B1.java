
package ri.serien.libecranrpg.vgvm.VGVM111F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM111F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM111F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    P11FO2.setValeursSelection("1", " ");
    P11FO1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    RPLB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLB1@")).trim());
    RPLB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    P11TR2.setEnabled(lexique.isPresent("P11TR2"));
    P11TR1.setEnabled(lexique.isPresent("P11TR1"));
    P11RP2.setEnabled(lexique.isPresent("P11RP2"));
    P11RP1.setEnabled(lexique.isPresent("P11RP1"));
    P11CO2.setEnabled(lexique.isPresent("P11CO2"));
    P11CO1.setEnabled(lexique.isPresent("P11CO1"));
    P11RB2.setEnabled(lexique.isPresent("P11RB2"));
    P11RB1.setEnabled(lexique.isPresent("P11RB1"));
    // P11FO2.setVisible( lexique.isPresent("P11FO2"));
    // P11FO2.setEnabled( lexique.isPresent("P11FO2"));
    // P11FO2.setSelected(lexique.HostFieldGetData("P11FO2").equalsIgnoreCase("1"));
    // P11FO1.setVisible( lexique.isPresent("P11FO1"));
    // P11FO1.setEnabled( lexique.isPresent("P11FO1"));
    // P11FO1.setSelected(lexique.HostFieldGetData("P11FO1").equalsIgnoreCase("1"));
    RPLB2.setVisible(lexique.isPresent("RPLB2"));
    RPLB1.setVisible(lexique.isPresent("RPLB1"));
    
    // Titre
    setTitle("Représentants");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (P11FO2.isSelected())
    // lexique.HostFieldPutData("P11FO2", 0, "1");
    // else
    // lexique.HostFieldPutData("P11FO2", 0, " ");
    // if (P11FO1.isSelected())
    // lexique.HostFieldPutData("P11FO1", 0, "1");
    // else
    // lexique.HostFieldPutData("P11FO1", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    P11RP1 = new XRiTextField();
    P11TR1 = new XRiTextField();
    RPLB1 = new RiZoneSortie();
    P11RB1 = new XRiTextField();
    P11CO1 = new XRiTextField();
    P11FO1 = new XRiCheckBox();
    P11FO2 = new XRiCheckBox();
    P11CO2 = new XRiTextField();
    P11RB2 = new XRiTextField();
    RPLB2 = new RiZoneSortie();
    P11TR2 = new XRiTextField();
    P11RP2 = new XRiTextField();
    OBJ_10 = new JLabel();
    OBJ_11 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(760, 190));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Repr\u00e9sentants"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- P11RP1 ----
          P11RP1.setComponentPopupMenu(BTD);
          P11RP1.setName("P11RP1");
          panel1.add(P11RP1);
          P11RP1.setBounds(20, 55, 34, P11RP1.getPreferredSize().height);

          //---- P11TR1 ----
          P11TR1.setComponentPopupMenu(null);
          P11TR1.setName("P11TR1");
          panel1.add(P11TR1);
          P11TR1.setBounds(55, 55, 24, P11TR1.getPreferredSize().height);

          //---- RPLB1 ----
          RPLB1.setText("@RPLB1@");
          RPLB1.setName("RPLB1");
          panel1.add(RPLB1);
          RPLB1.setBounds(85, 57, 240, RPLB1.getPreferredSize().height);

          //---- P11RB1 ----
          P11RB1.setComponentPopupMenu(null);
          P11RB1.setName("P11RB1");
          panel1.add(P11RB1);
          P11RB1.setBounds(330, 55, 74, P11RB1.getPreferredSize().height);

          //---- P11CO1 ----
          P11CO1.setComponentPopupMenu(null);
          P11CO1.setName("P11CO1");
          panel1.add(P11CO1);
          P11CO1.setBounds(410, 55, 50, P11CO1.getPreferredSize().height);

          //---- P11FO1 ----
          P11FO1.setText("For\u00e7age");
          P11FO1.setComponentPopupMenu(BTD);
          P11FO1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P11FO1.setName("P11FO1");
          panel1.add(P11FO1);
          P11FO1.setBounds(470, 59, 76, 20);

          //---- P11FO2 ----
          P11FO2.setText("For\u00e7age");
          P11FO2.setComponentPopupMenu(BTD);
          P11FO2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P11FO2.setName("P11FO2");
          panel1.add(P11FO2);
          P11FO2.setBounds(470, 94, 76, 20);

          //---- P11CO2 ----
          P11CO2.setComponentPopupMenu(null);
          P11CO2.setName("P11CO2");
          panel1.add(P11CO2);
          P11CO2.setBounds(410, 90, 50, P11CO2.getPreferredSize().height);

          //---- P11RB2 ----
          P11RB2.setComponentPopupMenu(null);
          P11RB2.setName("P11RB2");
          panel1.add(P11RB2);
          P11RB2.setBounds(330, 90, 74, P11RB2.getPreferredSize().height);

          //---- RPLB2 ----
          RPLB2.setText("@RPLB2@");
          RPLB2.setName("RPLB2");
          panel1.add(RPLB2);
          RPLB2.setBounds(85, 92, 240, RPLB2.getPreferredSize().height);

          //---- P11TR2 ----
          P11TR2.setComponentPopupMenu(null);
          P11TR2.setName("P11TR2");
          panel1.add(P11TR2);
          P11TR2.setBounds(55, 90, 24, P11TR2.getPreferredSize().height);

          //---- P11RP2 ----
          P11RP2.setComponentPopupMenu(BTD);
          P11RP2.setName("P11RP2");
          panel1.add(P11RP2);
          P11RP2.setBounds(20, 90, 34, P11RP2.getPreferredSize().height);

          //---- OBJ_10 ----
          OBJ_10.setText("Base sp\u00e9ciale");
          OBJ_10.setName("OBJ_10");
          panel1.add(OBJ_10);
          OBJ_10.setBounds(330, 31, 90, 20);

          //---- OBJ_11 ----
          OBJ_11.setText("%Com.");
          OBJ_11.setToolTipText("Pourcentage commissionnement");
          OBJ_11.setName("OBJ_11");
          panel1.add(OBJ_11);
          OBJ_11.setBounds(415, 31, 46, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField P11RP1;
  private XRiTextField P11TR1;
  private RiZoneSortie RPLB1;
  private XRiTextField P11RB1;
  private XRiTextField P11CO1;
  private XRiCheckBox P11FO1;
  private XRiCheckBox P11FO2;
  private XRiTextField P11CO2;
  private XRiTextField P11RB2;
  private RiZoneSortie RPLB2;
  private XRiTextField P11TR2;
  private XRiTextField P11RP2;
  private JLabel OBJ_10;
  private JLabel OBJ_11;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
