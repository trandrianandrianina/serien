
package ri.serien.libecranrpg.vgvm.VGVM04FM;
// Nom Fichier: b_VGVM04FM_FMTB1_FMTF1_1065.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_6);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTOX@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    LTHF2M.setEnabled(lexique.isPresent("LTHF2M"));
    LTHF2H.setEnabled(lexique.isPresent("LTHF2H"));
    LTHD2M.setEnabled(lexique.isPresent("LTHD2M"));
    LTHD2H.setEnabled(lexique.isPresent("LTHD2H"));
    LTHF1M.setEnabled(lexique.isPresent("LTHF1M"));
    LTHF1H.setEnabled(lexique.isPresent("LTHF1H"));
    LTHD1M.setEnabled(lexique.isPresent("LTHD1M"));
    LTHD1H.setEnabled(lexique.isPresent("LTHD1H"));
    INDMEX.setVisible(lexique.isPresent("INDMEX"));
    INDLIV.setVisible(lexique.isPresent("INDLIV"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    LTORT2.setEnabled(lexique.isPresent("LTORT2"));
    LTORT1.setEnabled(lexique.isPresent("LTORT1"));
    CLCDP.setVisible(lexique.isPresent("CLCDP"));
    INDCLI.setVisible(lexique.isPresent("INDCLI"));
    LTTOU2.setEnabled(lexique.isPresent("LTTOU2"));
    LTTOU1.setEnabled(lexique.isPresent("LTTOU1"));
    LTDF2X.setEnabled(lexique.isPresent("LTDF2X"));
    LTDD2X.setEnabled(lexique.isPresent("LTDD2X"));
    LTDF1X.setEnabled(lexique.isPresent("LTDF1X"));
    LTDD1X.setEnabled(lexique.isPresent("LTDD1X"));
    // LTDLIX.setEnabled( lexique.isPresent("LTDLIX"));
    OBJ_61.setEnabled(lexique.isPresent("LTOBS"));
    OBJ_39.setVisible(lexique.isPresent("DTOX"));
    OBJ_36.setVisible(lexique.isPresent("V01F"));
    CLVILR.setVisible(lexique.isPresent("CLVILR"));
    LTOBS.setEnabled(lexique.isPresent("LTOBS"));
    CLLOC.setVisible(lexique.isPresent("CLLOC"));
    CLRUE.setVisible(lexique.isPresent("CLRUE"));
    CLCPL.setVisible(lexique.isPresent("CLCPL"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(INDETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - CLIENT / TOURNEE"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    CMD = new JPopupMenu();
    OBJ_7 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    INDCLI = new XRiTextField();
    OBJ_112 = new JLabel();
    INDETB = new XRiTextField();
    INDLIV = new XRiTextField();
    INDMEX = new XRiTextField();
    OBJ_69 = new JLabel();
    OBJ_113 = new JLabel();
    OBJ_114 = new JLabel();
    bt_Fonctions = new JButton();
    OBJ_36 = new JLabel();
    P_Centre = new JPanel();
    OBJ_39 = new JLabel();
    OBJ_62 = new JButton();
    OBJ_64 = new JButton();
    OBJ_65 = new JButton();
    OBJ_66 = new JButton();
    OBJ_28 = new JLabel();
    LTDLIX = new XRiCalendrier();
    LTDD1X = new XRiTextField();
    LTDF1X = new XRiTextField();
    LTDD2X = new XRiTextField();
    LTDF2X = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_67 = new JLabel();
    LTTOU1 = new XRiTextField();
    LTTOU2 = new XRiTextField();
    LTORT1 = new XRiTextField();
    LTORT2 = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_51 = new JLabel();
    LTHD1H = new XRiTextField();
    LTHD1M = new XRiTextField();
    LTHF1H = new XRiTextField();
    LTHF1M = new XRiTextField();
    LTHD2H = new XRiTextField();
    LTHD2M = new XRiTextField();
    LTHF2H = new XRiTextField();
    LTHF2M = new XRiTextField();
    OBJ_43 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_53 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    CLCPL = new XRiTextField();
    CLRUE = new XRiTextField();
    CLLOC = new XRiTextField();
    CLVILR = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    CLCDP = new XRiTextField();
    LTOBS = new XRiTextField();
    CLNOM = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_7 ----
      OBJ_7.setText("Fin de Travail");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_9 ----
      OBJ_9.setText("R\u00e9afficher");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Recherche multi-crit\u00e8res");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Annuler");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);
      CMD.addSeparator();

      //---- OBJ_12 ----
      OBJ_12.setText("Cr\u00e9ation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Modification");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Interrogation");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Annulation");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      CMD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Duplication");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      CMD.add(OBJ_16);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Invite");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- INDCLI ----
        INDCLI.setComponentPopupMenu(BTDA);
        INDCLI.setName("INDCLI");

        //---- OBJ_112 ----
        OBJ_112.setText("Client");
        OBJ_112.setName("OBJ_112");

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTDA);
        INDETB.setName("INDETB");

        //---- INDLIV ----
        INDLIV.setComponentPopupMenu(BTDA);
        INDLIV.setName("INDLIV");

        //---- INDMEX ----
        INDMEX.setComponentPopupMenu(BTD);
        INDMEX.setName("INDMEX");

        //---- OBJ_69 ----
        OBJ_69.setText("Etablissement");
        OBJ_69.setName("OBJ_69");

        //---- OBJ_113 ----
        OBJ_113.setText("Liv");
        OBJ_113.setName("OBJ_113");

        //---- OBJ_114 ----
        OBJ_114.setText("Ex");
        OBJ_114.setName("OBJ_114");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- OBJ_36 ----
        OBJ_36.setText("@V01F@");
        OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
        OBJ_36.setName("OBJ_36");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(13, 13, 13)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(130, 130, 130)
                  .addComponent(OBJ_69))
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 133, GroupLayout.PREFERRED_SIZE))
              .addGap(5, 5, 5)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
              .addGap(12, 12, 12)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(25, 25, 25)
                  .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
              .addGap(11, 11, 11)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(INDMEX, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 368, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_69, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(3, 3, 3)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(INDMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(bt_Fonctions))
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //---- OBJ_39 ----
      OBJ_39.setText("@DTOX@");
      OBJ_39.setName("OBJ_39");
      P_Centre.add(OBJ_39);
      OBJ_39.setBounds(363, 284, 130, 20);

      //---- OBJ_62 ----
      OBJ_62.setText("P\u00e9riode 1");
      OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_62.setName("OBJ_62");
      P_Centre.add(OBJ_62);
      OBJ_62.setBounds(30, 278, 85, 32);

      //---- OBJ_64 ----
      OBJ_64.setText("P\u00e9riode 2");
      OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_64.setName("OBJ_64");
      P_Centre.add(OBJ_64);
      OBJ_64.setBounds(30, 308, 85, 32);

      //---- OBJ_65 ----
      OBJ_65.setText("Fermeture");
      OBJ_65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_65.setName("OBJ_65");
      P_Centre.add(OBJ_65);
      OBJ_65.setBounds(30, 338, OBJ_65.getPreferredSize().width, 32);

      //---- OBJ_66 ----
      OBJ_66.setText("Horaires");
      OBJ_66.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_66.setName("OBJ_66");
      P_Centre.add(OBJ_66);
      OBJ_66.setBounds(30, 368, 85, 32);

      //---- OBJ_28 ----
      OBJ_28.setText("jusqu'au");
      OBJ_28.setName("OBJ_28");
      P_Centre.add(OBJ_28);
      OBJ_28.setBounds(508, 284, 53, 20);

      //---- LTDLIX ----
      LTDLIX.setComponentPopupMenu(BTDA);
      LTDLIX.setName("LTDLIX");
      P_Centre.add(LTDLIX);
      LTDLIX.setBounds(568, 280, 115, LTDLIX.getPreferredSize().height);

      //---- LTDD1X ----
      LTDD1X.setComponentPopupMenu(BTDA);
      LTDD1X.setName("LTDD1X");
      P_Centre.add(LTDD1X);
      LTDD1X.setBounds(190, 340, 90, LTDD1X.getPreferredSize().height);

      //---- LTDF1X ----
      LTDF1X.setComponentPopupMenu(BTDA);
      LTDF1X.setName("LTDF1X");
      P_Centre.add(LTDF1X);
      LTDF1X.setBounds(308, 340, 90, LTDF1X.getPreferredSize().height);

      //---- LTDD2X ----
      LTDD2X.setComponentPopupMenu(BTDA);
      LTDD2X.setName("LTDD2X");
      P_Centre.add(LTDD2X);
      LTDD2X.setBounds(448, 340, 90, LTDD2X.getPreferredSize().height);

      //---- LTDF2X ----
      LTDF2X.setComponentPopupMenu(BTDA);
      LTDF2X.setName("LTDF2X");
      P_Centre.add(LTDF2X);
      LTDF2X.setBounds(567, 340, 90, LTDF2X.getPreferredSize().height);

      //---- OBJ_41 ----
      OBJ_41.setText("tourn\u00e9e");
      OBJ_41.setName("OBJ_41");
      P_Centre.add(OBJ_41);
      OBJ_41.setBounds(130, 314, 48, 20);

      //---- OBJ_67 ----
      OBJ_67.setText("tourn\u00e9e");
      OBJ_67.setName("OBJ_67");
      P_Centre.add(OBJ_67);
      OBJ_67.setBounds(130, 284, 48, 20);

      //---- LTTOU1 ----
      LTTOU1.setComponentPopupMenu(BTD);
      LTTOU1.setName("LTTOU1");
      P_Centre.add(LTTOU1);
      LTTOU1.setBounds(190, 280, 60, LTTOU1.getPreferredSize().height);

      //---- LTTOU2 ----
      LTTOU2.setComponentPopupMenu(BTD);
      LTTOU2.setName("LTTOU2");
      P_Centre.add(LTTOU2);
      LTTOU2.setBounds(190, 310, 60, LTTOU2.getPreferredSize().height);

      //---- LTORT1 ----
      LTORT1.setComponentPopupMenu(BTDA);
      LTORT1.setName("LTORT1");
      P_Centre.add(LTORT1);
      LTORT1.setBounds(308, 280, 50, LTORT1.getPreferredSize().height);

      //---- LTORT2 ----
      LTORT2.setComponentPopupMenu(BTDA);
      LTORT2.setName("LTORT2");
      P_Centre.add(LTORT2);
      LTORT2.setBounds(308, 310, 50, LTORT2.getPreferredSize().height);

      //---- OBJ_42 ----
      OBJ_42.setText("ordre");
      OBJ_42.setName("OBJ_42");
      P_Centre.add(OBJ_42);
      OBJ_42.setBounds(260, 314, 35, 20);

      //---- OBJ_68 ----
      OBJ_68.setText("ordre");
      OBJ_68.setName("OBJ_68");
      P_Centre.add(OBJ_68);
      OBJ_68.setBounds(260, 284, 35, 20);

      //---- OBJ_47 ----
      OBJ_47.setText("de");
      OBJ_47.setName("OBJ_47");
      P_Centre.add(OBJ_47);
      OBJ_47.setBounds(130, 374, 19, 20);

      //---- OBJ_51 ----
      OBJ_51.setText("de");
      OBJ_51.setHorizontalAlignment(SwingConstants.RIGHT);
      OBJ_51.setName("OBJ_51");
      P_Centre.add(OBJ_51);
      OBJ_51.setBounds(373, 374, 19, 20);

      //---- LTHD1H ----
      LTHD1H.setComponentPopupMenu(BTDA);
      LTHD1H.setName("LTHD1H");
      P_Centre.add(LTHD1H);
      LTHD1H.setBounds(190, 370, 26, LTHD1H.getPreferredSize().height);

      //---- LTHD1M ----
      LTHD1M.setComponentPopupMenu(BTDA);
      LTHD1M.setName("LTHD1M");
      P_Centre.add(LTHD1M);
      LTHD1M.setBounds(239, 370, 26, LTHD1M.getPreferredSize().height);

      //---- LTHF1H ----
      LTHF1H.setComponentPopupMenu(BTDA);
      LTHF1H.setName("LTHF1H");
      P_Centre.add(LTHF1H);
      LTHF1H.setBounds(308, 370, 26, LTHF1H.getPreferredSize().height);

      //---- LTHF1M ----
      LTHF1M.setComponentPopupMenu(BTDA);
      LTHF1M.setName("LTHF1M");
      P_Centre.add(LTHF1M);
      LTHF1M.setBounds(347, 370, 26, LTHF1M.getPreferredSize().height);

      //---- LTHD2H ----
      LTHD2H.setComponentPopupMenu(BTDA);
      LTHD2H.setName("LTHD2H");
      P_Centre.add(LTHD2H);
      LTHD2H.setBounds(403, 370, 26, LTHD2H.getPreferredSize().height);

      //---- LTHD2M ----
      LTHD2M.setComponentPopupMenu(BTDA);
      LTHD2M.setName("LTHD2M");
      P_Centre.add(LTHD2M);
      LTHD2M.setBounds(448, 370, 26, LTHD2M.getPreferredSize().height);

      //---- LTHF2H ----
      LTHF2H.setComponentPopupMenu(BTDA);
      LTHF2H.setName("LTHF2H");
      P_Centre.add(LTHF2H);
      LTHF2H.setBounds(503, 370, 26, LTHF2H.getPreferredSize().height);

      //---- LTHF2M ----
      LTHF2M.setComponentPopupMenu(BTDA);
      LTHF2M.setName("LTHF2M");
      P_Centre.add(LTHF2M);
      LTHF2M.setBounds(548, 370, 26, LTHF2M.getPreferredSize().height);

      //---- OBJ_43 ----
      OBJ_43.setText("du");
      OBJ_43.setName("OBJ_43");
      P_Centre.add(OBJ_43);
      OBJ_43.setBounds(130, 344, 18, 20);

      //---- OBJ_44 ----
      OBJ_44.setText("au");
      OBJ_44.setName("OBJ_44");
      P_Centre.add(OBJ_44);
      OBJ_44.setBounds(285, 344, 18, 20);

      //---- OBJ_45 ----
      OBJ_45.setText("du");
      OBJ_45.setName("OBJ_45");
      P_Centre.add(OBJ_45);
      OBJ_45.setBounds(423, 344, 18, 20);

      //---- OBJ_46 ----
      OBJ_46.setText("au");
      OBJ_46.setName("OBJ_46");
      P_Centre.add(OBJ_46);
      OBJ_46.setBounds(544, 344, 18, 20);

      //---- OBJ_48 ----
      OBJ_48.setText("H");
      OBJ_48.setName("OBJ_48");
      P_Centre.add(OBJ_48);
      OBJ_48.setBounds(221, 374, 13, 20);

      //---- OBJ_50 ----
      OBJ_50.setText("H");
      OBJ_50.setName("OBJ_50");
      P_Centre.add(OBJ_50);
      OBJ_50.setBounds(334, 374, 13, 20);

      //---- OBJ_52 ----
      OBJ_52.setText("H");
      OBJ_52.setName("OBJ_52");
      P_Centre.add(OBJ_52);
      OBJ_52.setBounds(432, 374, 13, 20);

      //---- OBJ_54 ----
      OBJ_54.setText("H");
      OBJ_54.setName("OBJ_54");
      P_Centre.add(OBJ_54);
      OBJ_54.setBounds(532, 374, 13, 20);

      //---- OBJ_49 ----
      OBJ_49.setText("\u00e0");
      OBJ_49.setName("OBJ_49");
      P_Centre.add(OBJ_49);
      OBJ_49.setBounds(270, 375, 15, 20);

      //---- OBJ_53 ----
      OBJ_53.setText("\u00e0");
      OBJ_53.setName("OBJ_53");
      P_Centre.add(OBJ_53);
      OBJ_53.setBounds(482, 374, 11, 20);

      //======== xTitledPanel1 ========
      {
        xTitledPanel1.setBorder(new DropShadowBorder());
        xTitledPanel1.setName("xTitledPanel1");
        Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
        xTitledPanel1ContentContainer.setLayout(null);

        //---- CLCPL ----
        CLCPL.setComponentPopupMenu(BTD);
        CLCPL.setName("CLCPL");
        xTitledPanel1ContentContainer.add(CLCPL);
        CLCPL.setBounds(115, 40, 310, CLCPL.getPreferredSize().height);

        //---- CLRUE ----
        CLRUE.setComponentPopupMenu(BTDA);
        CLRUE.setName("CLRUE");
        xTitledPanel1ContentContainer.add(CLRUE);
        CLRUE.setBounds(115, 70, 310, CLRUE.getPreferredSize().height);

        //---- CLLOC ----
        CLLOC.setComponentPopupMenu(BTDA);
        CLLOC.setName("CLLOC");
        xTitledPanel1ContentContainer.add(CLLOC);
        CLLOC.setBounds(115, 100, 310, CLLOC.getPreferredSize().height);

        //---- CLVILR ----
        CLVILR.setComponentPopupMenu(BTD);
        CLVILR.setName("CLVILR");
        xTitledPanel1ContentContainer.add(CLVILR);
        CLVILR.setBounds(175, 130, 250, CLVILR.getPreferredSize().height);

        //---- OBJ_57 ----
        OBJ_57.setText("Nom");
        OBJ_57.setName("OBJ_57");
        xTitledPanel1ContentContainer.add(OBJ_57);
        OBJ_57.setBounds(25, 14, 85, 20);

        //---- OBJ_58 ----
        OBJ_58.setText("Adresse");
        OBJ_58.setName("OBJ_58");
        xTitledPanel1ContentContainer.add(OBJ_58);
        OBJ_58.setBounds(25, 74, 85, 20);

        //---- OBJ_59 ----
        OBJ_59.setText("Localit\u00e9");
        OBJ_59.setName("OBJ_59");
        xTitledPanel1ContentContainer.add(OBJ_59);
        OBJ_59.setBounds(25, 104, 85, 20);

        //---- OBJ_60 ----
        OBJ_60.setText("Code postal");
        OBJ_60.setName("OBJ_60");
        xTitledPanel1ContentContainer.add(OBJ_60);
        OBJ_60.setBounds(25, 134, 85, 20);

        //---- OBJ_61 ----
        OBJ_61.setText("Observation");
        OBJ_61.setName("OBJ_61");
        xTitledPanel1ContentContainer.add(OBJ_61);
        OBJ_61.setBounds(25, 165, 76, 18);

        //---- CLCDP ----
        CLCDP.setComponentPopupMenu(BTD);
        CLCDP.setName("CLCDP");
        xTitledPanel1ContentContainer.add(CLCDP);
        CLCDP.setBounds(115, 130, 60, CLCDP.getPreferredSize().height);

        //---- LTOBS ----
        LTOBS.setComponentPopupMenu(BTDA);
        LTOBS.setName("LTOBS");
        xTitledPanel1ContentContainer.add(LTOBS);
        LTOBS.setBounds(115, 160, 310, LTOBS.getPreferredSize().height);

        //---- CLNOM ----
        CLNOM.setComponentPopupMenu(BTDA);
        CLNOM.setName("CLNOM");
        xTitledPanel1ContentContainer.add(CLNOM);
        CLNOM.setBounds(115, 10, 310, CLNOM.getPreferredSize().height);
      }
      P_Centre.add(xTitledPanel1);
      xTitledPanel1.setBounds(30, 20, 455, 235);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu CMD;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private XRiTextField INDCLI;
  private JLabel OBJ_112;
  private XRiTextField INDETB;
  private XRiTextField INDLIV;
  private XRiTextField INDMEX;
  private JLabel OBJ_69;
  private JLabel OBJ_113;
  private JLabel OBJ_114;
  private JButton bt_Fonctions;
  private JLabel OBJ_36;
  private JPanel P_Centre;
  private JLabel OBJ_39;
  private JButton OBJ_62;
  private JButton OBJ_64;
  private JButton OBJ_65;
  private JButton OBJ_66;
  private JLabel OBJ_28;
  private XRiCalendrier LTDLIX;
  private XRiTextField LTDD1X;
  private XRiTextField LTDF1X;
  private XRiTextField LTDD2X;
  private XRiTextField LTDF2X;
  private JLabel OBJ_41;
  private JLabel OBJ_67;
  private XRiTextField LTTOU1;
  private XRiTextField LTTOU2;
  private XRiTextField LTORT1;
  private XRiTextField LTORT2;
  private JLabel OBJ_42;
  private JLabel OBJ_68;
  private JLabel OBJ_47;
  private JLabel OBJ_51;
  private XRiTextField LTHD1H;
  private XRiTextField LTHD1M;
  private XRiTextField LTHF1H;
  private XRiTextField LTHF1M;
  private XRiTextField LTHD2H;
  private XRiTextField LTHD2M;
  private XRiTextField LTHF2H;
  private XRiTextField LTHF2M;
  private JLabel OBJ_43;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JLabel OBJ_48;
  private JLabel OBJ_50;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private JLabel OBJ_49;
  private JLabel OBJ_53;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField CLCPL;
  private XRiTextField CLRUE;
  private XRiTextField CLLOC;
  private XRiTextField CLVILR;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_59;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private XRiTextField CLCDP;
  private XRiTextField LTOBS;
  private XRiTextField CLNOM;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
