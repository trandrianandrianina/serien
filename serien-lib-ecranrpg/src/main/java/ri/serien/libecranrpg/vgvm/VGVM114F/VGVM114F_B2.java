
package ri.serien.libecranrpg.vgvm.VGVM114F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM114F_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM114F_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    D3MOD.setValeursSelection("*", " ");
    D2MOD.setValeursSelection("*", " ");
    D1MOD.setValeursSelection("*", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WSER.setVisible(lexique.isPresent("WSER"));
    // D3MOD.setEnabled( lexique.isPresent("D3MOD"));
    // D3MOD.setSelected(lexique.HostFieldGetData("D3MOD").equalsIgnoreCase("*"));
    D2ETA.setEnabled(lexique.isPresent("D2ETA"));
    // D2MOD.setEnabled( lexique.isPresent("D2MOD"));
    // D2MOD.setSelected(lexique.HostFieldGetData("D2MOD").equalsIgnoreCase("*"));
    D1ETA.setEnabled(lexique.isPresent("D1ETA"));
    D1TOU.setEnabled(lexique.isPresent("D1TOU"));
    // D1MOD.setEnabled( lexique.isPresent("D1MOD"));
    // D1MOD.setSelected(lexique.HostFieldGetData("D1MOD").equalsIgnoreCase("*"));
    L1UNV.setEnabled(lexique.isPresent("L1UNV"));
    WVDE.setEnabled(lexique.isPresent("WVDE"));
    WMAG.setEnabled(lexique.isPresent("WMAG"));
    // D2DATX.setEnabled( lexique.isPresent("D2DATX"));
    // D1DATX.setEnabled( lexique.isPresent("D1DATX"));
    D3QTEX.setEnabled(lexique.isPresent("D3QTEX"));
    D2QTEX.setEnabled(lexique.isPresent("D2QTEX"));
    D1QTEX.setEnabled(lexique.isPresent("D1QTEX"));
    L1QTEX.setEnabled(lexique.isPresent("L1QTEX"));
    L1ART.setEnabled(lexique.isPresent("L1ART"));
    L1LIB4.setEnabled(lexique.isPresent("L1LIB4"));
    L1LIB3.setEnabled(lexique.isPresent("L1LIB3"));
    L1LIB2.setEnabled(lexique.isPresent("L1LIB2"));
    L1LIB1.setEnabled(lexique.isPresent("L1LIB1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations livraison"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (D3MOD.isSelected())
    // lexique.HostFieldPutData("D3MOD", 0, "*");
    // else
    // lexique.HostFieldPutData("D3MOD", 0, " ");
    // if (D2MOD.isSelected())
    // lexique.HostFieldPutData("D2MOD", 0, "*");
    // else
    // lexique.HostFieldPutData("D2MOD", 0, " ");
    // if (D1MOD.isSelected())
    // lexique.HostFieldPutData("D1MOD", 0, "*");
    // else
    // lexique.HostFieldPutData("D1MOD", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm114"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    L1LIB3 = new XRiTextField();
    L1LIB4 = new XRiTextField();
    L1ART = new XRiTextField();
    L1QTEX = new XRiTextField();
    OBJ_51 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    WMAG = new XRiTextField();
    WVDE = new XRiTextField();
    L1UNV = new XRiTextField();
    WSER = new XRiTextField();
    panel2 = new JPanel();
    OBJ_57 = new JLabel();
    D1QTEX = new XRiTextField();
    D2QTEX = new XRiTextField();
    D3QTEX = new XRiTextField();
    OBJ_58 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_54 = new JLabel();
    D1MOD = new XRiCheckBox();
    D1TOU = new XRiTextField();
    D1ETA = new XRiTextField();
    D2MOD = new XRiCheckBox();
    D2ETA = new XRiTextField();
    D3MOD = new XRiCheckBox();
    D1DATX = new XRiCalendrier();
    D2DATX = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 320));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          panel1.add(L1LIB1);
          L1LIB1.setBounds(20, 105, 375, L1LIB1.getPreferredSize().height);
          
          // ---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setName("L1LIB2");
          panel1.add(L1LIB2);
          L1LIB2.setBounds(20, 135, 375, L1LIB2.getPreferredSize().height);
          
          // ---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setName("L1LIB3");
          panel1.add(L1LIB3);
          L1LIB3.setBounds(20, 165, 375, L1LIB3.getPreferredSize().height);
          
          // ---- L1LIB4 ----
          L1LIB4.setComponentPopupMenu(BTD);
          L1LIB4.setName("L1LIB4");
          panel1.add(L1LIB4);
          L1LIB4.setBounds(20, 195, 375, L1LIB4.getPreferredSize().height);
          
          // ---- L1ART ----
          L1ART.setComponentPopupMenu(BTD);
          L1ART.setName("L1ART");
          panel1.add(L1ART);
          L1ART.setBounds(100, 45, 210, L1ART.getPreferredSize().height);
          
          // ---- L1QTEX ----
          L1QTEX.setComponentPopupMenu(BTD);
          L1QTEX.setName("L1QTEX");
          panel1.add(L1QTEX);
          L1QTEX.setBounds(100, 75, 74, L1QTEX.getPreferredSize().height);
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Command\u00e9");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(20, 79, 80, 20);
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Magasin");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(20, 19, 80, 20);
          
          // ---- OBJ_49 ----
          OBJ_49.setText("Vendeur");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(160, 19, 54, 20);
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Article");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(20, 49, 80, 20);
          
          // ---- WMAG ----
          WMAG.setComponentPopupMenu(BTD);
          WMAG.setName("WMAG");
          panel1.add(WMAG);
          WMAG.setBounds(100, 15, 34, WMAG.getPreferredSize().height);
          
          // ---- WVDE ----
          WVDE.setComponentPopupMenu(BTD);
          WVDE.setName("WVDE");
          panel1.add(WVDE);
          WVDE.setBounds(220, 15, 34, WVDE.getPreferredSize().height);
          
          // ---- L1UNV ----
          L1UNV.setComponentPopupMenu(BTD);
          L1UNV.setName("L1UNV");
          panel1.add(L1UNV);
          L1UNV.setBounds(175, 75, 34, L1UNV.getPreferredSize().height);
          
          // ---- WSER ----
          WSER.setComponentPopupMenu(BTD);
          WSER.setName("WSER");
          panel1.add(WSER);
          WSER.setBounds(310, 45, 20, WSER.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Sortie magasin"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- OBJ_57 ----
          OBJ_57.setText("Emp. diff\u00e9r\u00e9");
          OBJ_57.setName("OBJ_57");
          panel2.add(OBJ_57);
          OBJ_57.setBounds(20, 84, 75, 20);
          
          // ---- D1QTEX ----
          D1QTEX.setComponentPopupMenu(BTD);
          D1QTEX.setName("D1QTEX");
          panel2.add(D1QTEX);
          D1QTEX.setBounds(135, 50, 74, D1QTEX.getPreferredSize().height);
          
          // ---- D2QTEX ----
          D2QTEX.setComponentPopupMenu(BTD);
          D2QTEX.setName("D2QTEX");
          panel2.add(D2QTEX);
          D2QTEX.setBounds(135, 80, 74, D2QTEX.getPreferredSize().height);
          
          // ---- D3QTEX ----
          D3QTEX.setComponentPopupMenu(BTD);
          D3QTEX.setName("D3QTEX");
          panel2.add(D3QTEX);
          D3QTEX.setBounds(135, 110, 74, D3QTEX.getPreferredSize().height);
          
          // ---- OBJ_58 ----
          OBJ_58.setText("Emport\u00e9");
          OBJ_58.setName("OBJ_58");
          panel2.add(OBJ_58);
          OBJ_58.setBounds(20, 114, 54, 20);
          
          // ---- OBJ_52 ----
          OBJ_52.setText("Quantit\u00e9");
          OBJ_52.setName("OBJ_52");
          panel2.add(OBJ_52);
          OBJ_52.setBounds(137, 30, 52, 20);
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Date");
          OBJ_53.setName("OBJ_53");
          panel2.add(OBJ_53);
          OBJ_53.setBounds(222, 30, 33, 20);
          
          // ---- OBJ_56 ----
          OBJ_56.setText("Livr\u00e9");
          OBJ_56.setName("OBJ_56");
          panel2.add(OBJ_56);
          OBJ_56.setBounds(20, 54, 33, 20);
          
          // ---- OBJ_55 ----
          OBJ_55.setText("Etat");
          OBJ_55.setName("OBJ_55");
          panel2.add(OBJ_55);
          OBJ_55.setBounds(375, 30, 27, 20);
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Ind");
          OBJ_54.setName("OBJ_54");
          panel2.add(OBJ_54);
          OBJ_54.setBounds(340, 30, 21, 20);
          
          // ---- D1MOD ----
          D1MOD.setText("");
          D1MOD.setComponentPopupMenu(BTD);
          D1MOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          D1MOD.setName("D1MOD");
          panel2.add(D1MOD);
          D1MOD.setBounds(110, 54, 19, 20);
          
          // ---- D1TOU ----
          D1TOU.setComponentPopupMenu(BTD);
          D1TOU.setName("D1TOU");
          panel2.add(D1TOU);
          D1TOU.setBounds(340, 50, 20, D1TOU.getPreferredSize().height);
          
          // ---- D1ETA ----
          D1ETA.setComponentPopupMenu(BTD);
          D1ETA.setName("D1ETA");
          panel2.add(D1ETA);
          D1ETA.setBounds(375, 50, 20, D1ETA.getPreferredSize().height);
          
          // ---- D2MOD ----
          D2MOD.setText("");
          D2MOD.setComponentPopupMenu(BTD);
          D2MOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          D2MOD.setName("D2MOD");
          panel2.add(D2MOD);
          D2MOD.setBounds(110, 84, 19, 20);
          
          // ---- D2ETA ----
          D2ETA.setComponentPopupMenu(BTD);
          D2ETA.setName("D2ETA");
          panel2.add(D2ETA);
          D2ETA.setBounds(375, 80, 20, D2ETA.getPreferredSize().height);
          
          // ---- D3MOD ----
          D3MOD.setText("");
          D3MOD.setComponentPopupMenu(BTD);
          D3MOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          D3MOD.setName("D3MOD");
          panel2.add(D3MOD);
          D3MOD.setBounds(110, 114, 19, 20);
          
          // ---- D1DATX ----
          D1DATX.setName("D1DATX");
          panel2.add(D1DATX);
          D1DATX.setBounds(220, 50, 105, D1DATX.getPreferredSize().height);
          
          // ---- D2DATX ----
          D2DATX.setName("D2DATX");
          panel2.add(D2DATX);
          D2DATX.setBounds(220, 80, 105, D2DATX.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(25, 25, 25)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                    .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
                        Short.MAX_VALUE)
                    .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 420, Short.MAX_VALUE))));
        p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { panel1, panel2 });
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(25, 25, 25)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private XRiTextField L1LIB3;
  private XRiTextField L1LIB4;
  private XRiTextField L1ART;
  private XRiTextField L1QTEX;
  private JLabel OBJ_51;
  private JLabel OBJ_26;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private XRiTextField WMAG;
  private XRiTextField WVDE;
  private XRiTextField L1UNV;
  private XRiTextField WSER;
  private JPanel panel2;
  private JLabel OBJ_57;
  private XRiTextField D1QTEX;
  private XRiTextField D2QTEX;
  private XRiTextField D3QTEX;
  private JLabel OBJ_58;
  private JLabel OBJ_52;
  private JLabel OBJ_53;
  private JLabel OBJ_56;
  private JLabel OBJ_55;
  private JLabel OBJ_54;
  private XRiCheckBox D1MOD;
  private XRiTextField D1TOU;
  private XRiTextField D1ETA;
  private XRiCheckBox D2MOD;
  private XRiTextField D2ETA;
  private XRiCheckBox D3MOD;
  private XRiCalendrier D1DATX;
  private XRiCalendrier D2DATX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
