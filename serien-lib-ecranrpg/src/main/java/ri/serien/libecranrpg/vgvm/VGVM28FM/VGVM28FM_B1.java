
package ri.serien.libecranrpg.vgvm.VGVM28FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM28FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM28FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    RFA5C3.setValeursSelection("1", " ");
    RFA5C2.setValeursSelection("1", " ");
    RFA5C1.setValeursSelection("1", " ");
    RFA5CF.setValeursSelection("1", " ");
    RFA4C3.setValeursSelection("1", " ");
    RFA4C2.setValeursSelection("1", " ");
    RFA4C1.setValeursSelection("1", " ");
    RFA4CF.setValeursSelection("1", " ");
    RFA3C3.setValeursSelection("1", " ");
    RFA3C2.setValeursSelection("1", " ");
    RFA3C1.setValeursSelection("1", " ");
    RFA3CF.setValeursSelection("1", " ");
    RFA2C3.setValeursSelection("1", " ");
    RFA2C2.setValeursSelection("1", " ");
    RFA2C1.setValeursSelection("1", " ");
    RFA2CF.setValeursSelection("1", " ");
    RFA1C3.setValeursSelection("1", " ");
    RFA1C2.setValeursSelection("1", " ");
    RFA1C1.setValeursSelection("1", " ");
    RFA1CF.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB1@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB2@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB3@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB4@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // RFA5C3.setVisible( lexique.isPresent("RFA5C3"));
    // RFA5C3.setSelected(lexique.HostFieldGetData("RFA5C3").equalsIgnoreCase("1"));
    // RFA5C2.setVisible( lexique.isPresent("RFA5C2"));
    // RFA5C2.setSelected(lexique.HostFieldGetData("RFA5C2").equalsIgnoreCase("1"));
    // RFA5C1.setVisible( lexique.isPresent("RFA5C1"));
    // RFA5C1.setSelected(lexique.HostFieldGetData("RFA5C1").equalsIgnoreCase("1"));
    // RFA5CF.setVisible( lexique.isPresent("RFA5CF"));
    // RFA5CF.setSelected(lexique.HostFieldGetData("RFA5CF").equalsIgnoreCase("1"));
    // RFA4C3.setVisible( lexique.isPresent("RFA4C3"));
    // RFA4C3.setSelected(lexique.HostFieldGetData("RFA4C3").equalsIgnoreCase("1"));
    // RFA4C2.setVisible( lexique.isPresent("RFA4C2"));
    // RFA4C2.setSelected(lexique.HostFieldGetData("RFA4C2").equalsIgnoreCase("1"));
    // RFA4C1.setVisible( lexique.isPresent("RFA4C1"));
    // RFA4C1.setSelected(lexique.HostFieldGetData("RFA4C1").equalsIgnoreCase("1"));
    // RFA4CF.setVisible( lexique.isPresent("RFA4CF"));
    // RFA4CF.setSelected(lexique.HostFieldGetData("RFA4CF").equalsIgnoreCase("1"));
    // RFA3C3.setVisible( lexique.isPresent("RFA3C3"));
    // RFA3C3.setSelected(lexique.HostFieldGetData("RFA3C3").equalsIgnoreCase("1"));
    // RFA3C2.setVisible( lexique.isPresent("RFA3C2"));
    // RFA3C2.setSelected(lexique.HostFieldGetData("RFA3C2").equalsIgnoreCase("1"));
    // RFA3C1.setVisible( lexique.isPresent("RFA3C1"));
    // RFA3C1.setSelected(lexique.HostFieldGetData("RFA3C1").equalsIgnoreCase("1"));
    // RFA3CF.setVisible( lexique.isPresent("RFA3CF"));
    // RFA3CF.setSelected(lexique.HostFieldGetData("RFA3CF").equalsIgnoreCase("1"));
    // RFA2C3.setVisible( lexique.isPresent("RFA2C3"));
    // RFA2C3.setSelected(lexique.HostFieldGetData("RFA2C3").equalsIgnoreCase("1"));
    // RFA2C2.setVisible( lexique.isPresent("RFA2C2"));
    // RFA2C2.setSelected(lexique.HostFieldGetData("RFA2C2").equalsIgnoreCase("1"));
    // RFA2C1.setVisible( lexique.isPresent("RFA2C1"));
    // RFA2C1.setSelected(lexique.HostFieldGetData("RFA2C1").equalsIgnoreCase("1"));
    // RFA2CF.setVisible( lexique.isPresent("RFA2CF"));
    // RFA2CF.setSelected(lexique.HostFieldGetData("RFA2CF").equalsIgnoreCase("1"));
    // RFA1C3.setVisible( lexique.isPresent("RFA1C3"));
    // RFA1C3.setSelected(lexique.HostFieldGetData("RFA1C3").equalsIgnoreCase("1"));
    // RFA1C2.setVisible( lexique.isPresent("RFA1C2"));
    // RFA1C2.setSelected(lexique.HostFieldGetData("RFA1C2").equalsIgnoreCase("1"));
    // RFA1C1.setVisible( lexique.isPresent("RFA1C1"));
    // RFA1C1.setSelected(lexique.HostFieldGetData("RFA1C1").equalsIgnoreCase("1"));
    // RFA1CF.setVisible( lexique.isPresent("RFA1CF"));
    // RFA1CF.setSelected(lexique.HostFieldGetData("RFA1CF").equalsIgnoreCase("1"));
    RFBAR5.setEnabled(lexique.isPresent("RFBAR5"));
    RFBAR4.setEnabled(lexique.isPresent("RFBAR4"));
    RFBAR3.setEnabled(lexique.isPresent("RFBAR3"));
    RFBAR2.setEnabled(lexique.isPresent("RFBAR2"));
    RFBAR.setEnabled(lexique.isPresent("RFBAR"));
    OBJ_60.setVisible(lexique.isPresent("WLIB5"));
    OBJ_59.setVisible(lexique.isPresent("WLIB4"));
    OBJ_58.setVisible(lexique.isPresent("WLIB3"));
    OBJ_57.setVisible(lexique.isPresent("WLIB2"));
    OBJ_56.setVisible(lexique.isPresent("WLIB1"));
    
    // TODO Icones
    
    // Titre
    if (lexique.isTrue("92")) {
      setTitle(interpreteurD.analyseExpression("Bonifications sur période"));
    }
    else {
      setTitle(interpreteurD.analyseExpression("Remise de fin de période"));
    }
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (RFA5C3.isSelected())
    // lexique.HostFieldPutData("RFA5C3", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA5C3", 0, " ");
    // if (RFA5C2.isSelected())
    // lexique.HostFieldPutData("RFA5C2", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA5C2", 0, " ");
    // if (RFA5C1.isSelected())
    // lexique.HostFieldPutData("RFA5C1", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA5C1", 0, " ");
    // if (RFA5CF.isSelected())
    // lexique.HostFieldPutData("RFA5CF", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA5CF", 0, " ");
    // if (RFA4C3.isSelected())
    // lexique.HostFieldPutData("RFA4C3", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA4C3", 0, " ");
    // if (RFA4C2.isSelected())
    // lexique.HostFieldPutData("RFA4C2", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA4C2", 0, " ");
    // if (RFA4C1.isSelected())
    // lexique.HostFieldPutData("RFA4C1", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA4C1", 0, " ");
    // if (RFA4CF.isSelected())
    // lexique.HostFieldPutData("RFA4CF", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA4CF", 0, " ");
    // if (RFA3C3.isSelected())
    // lexique.HostFieldPutData("RFA3C3", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA3C3", 0, " ");
    // if (RFA3C2.isSelected())
    // lexique.HostFieldPutData("RFA3C2", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA3C2", 0, " ");
    // if (RFA3C1.isSelected())
    // lexique.HostFieldPutData("RFA3C1", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA3C1", 0, " ");
    // if (RFA3CF.isSelected())
    // lexique.HostFieldPutData("RFA3CF", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA3CF", 0, " ");
    // if (RFA2C3.isSelected())
    // lexique.HostFieldPutData("RFA2C3", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA2C3", 0, " ");
    // if (RFA2C2.isSelected())
    // lexique.HostFieldPutData("RFA2C2", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA2C2", 0, " ");
    // if (RFA2C1.isSelected())
    // lexique.HostFieldPutData("RFA2C1", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA2C1", 0, " ");
    // if (RFA2CF.isSelected())
    // lexique.HostFieldPutData("RFA2CF", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA2CF", 0, " ");
    // if (RFA1C3.isSelected())
    // lexique.HostFieldPutData("RFA1C3", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA1C3", 0, " ");
    // if (RFA1C2.isSelected())
    // lexique.HostFieldPutData("RFA1C2", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA1C2", 0, " ");
    // if (RFA1C1.isSelected())
    // lexique.HostFieldPutData("RFA1C1", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA1C1", 0, " ");
    // if (RFA1CF.isSelected())
    // lexique.HostFieldPutData("RFA1CF", 0, "1");
    // else
    // lexique.HostFieldPutData("RFA1CF", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_ann = new RiSousMenu();
    riSousMenu_bt_ann = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_56 = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_58 = new RiZoneSortie();
    OBJ_59 = new RiZoneSortie();
    OBJ_60 = new RiZoneSortie();
    RFBAR = new XRiTextField();
    RFBAR2 = new XRiTextField();
    RFBAR3 = new XRiTextField();
    RFBAR4 = new XRiTextField();
    RFBAR5 = new XRiTextField();
    OBJ_26 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_63 = new JLabel();
    RFA1CF = new XRiCheckBox();
    RFA1C1 = new XRiCheckBox();
    RFA1C2 = new XRiCheckBox();
    RFA1C3 = new XRiCheckBox();
    RFA2CF = new XRiCheckBox();
    RFA2C1 = new XRiCheckBox();
    RFA2C2 = new XRiCheckBox();
    RFA2C3 = new XRiCheckBox();
    RFA3CF = new XRiCheckBox();
    RFA3C1 = new XRiCheckBox();
    RFA3C2 = new XRiCheckBox();
    RFA3C3 = new XRiCheckBox();
    RFA4CF = new XRiCheckBox();
    RFA4C1 = new XRiCheckBox();
    RFA4C2 = new XRiCheckBox();
    RFA4C3 = new XRiCheckBox();
    RFA5CF = new XRiCheckBox();
    RFA5C1 = new XRiCheckBox();
    RFA5C2 = new XRiCheckBox();
    RFA5C3 = new XRiCheckBox();
    OBJ_75 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_78 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    xTitledSeparator3 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(770, 265));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_ann ========
          {
            riSousMenu_ann.setName("riSousMenu_ann");

            //---- riSousMenu_bt_ann ----
            riSousMenu_bt_ann.setText("Annulation");
            riSousMenu_bt_ann.setToolTipText("Annulation");
            riSousMenu_bt_ann.setName("riSousMenu_bt_ann");
            riSousMenu_ann.add(riSousMenu_bt_ann);
          }
          menus_haut.add(riSousMenu_ann);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_56 ----
          OBJ_56.setText("@WLIB1@");
          OBJ_56.setName("OBJ_56");
          panel2.add(OBJ_56);
          OBJ_56.setBounds(130, 47, 316, OBJ_56.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("@WLIB2@");
          OBJ_57.setName("OBJ_57");
          panel2.add(OBJ_57);
          OBJ_57.setBounds(130, 77, 316, OBJ_57.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("@WLIB3@");
          OBJ_58.setName("OBJ_58");
          panel2.add(OBJ_58);
          OBJ_58.setBounds(130, 107, 316, OBJ_58.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("@WLIB4@");
          OBJ_59.setName("OBJ_59");
          panel2.add(OBJ_59);
          OBJ_59.setBounds(130, 137, 316, OBJ_59.getPreferredSize().height);

          //---- OBJ_60 ----
          OBJ_60.setText("@WLIB5@");
          OBJ_60.setName("OBJ_60");
          panel2.add(OBJ_60);
          OBJ_60.setBounds(130, 167, 316, OBJ_60.getPreferredSize().height);

          //---- RFBAR ----
          RFBAR.setComponentPopupMenu(BTD);
          RFBAR.setName("RFBAR");
          panel2.add(RFBAR);
          RFBAR.setBounds(60, 45, 60, RFBAR.getPreferredSize().height);

          //---- RFBAR2 ----
          RFBAR2.setComponentPopupMenu(BTD);
          RFBAR2.setName("RFBAR2");
          panel2.add(RFBAR2);
          RFBAR2.setBounds(60, 75, 60, RFBAR2.getPreferredSize().height);

          //---- RFBAR3 ----
          RFBAR3.setComponentPopupMenu(BTD);
          RFBAR3.setName("RFBAR3");
          panel2.add(RFBAR3);
          RFBAR3.setBounds(60, 105, 60, RFBAR3.getPreferredSize().height);

          //---- RFBAR4 ----
          RFBAR4.setComponentPopupMenu(BTD);
          RFBAR4.setName("RFBAR4");
          panel2.add(RFBAR4);
          RFBAR4.setBounds(60, 135, 60, RFBAR4.getPreferredSize().height);

          //---- RFBAR5 ----
          RFBAR5.setComponentPopupMenu(BTD);
          RFBAR5.setName("RFBAR5");
          panel2.add(RFBAR5);
          RFBAR5.setBounds(60, 165, 60, RFBAR5.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("N\u00b01");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(20, 49, 24, 20);

          //---- OBJ_53 ----
          OBJ_53.setText("N\u00b0 2");
          OBJ_53.setName("OBJ_53");
          panel2.add(OBJ_53);
          OBJ_53.setBounds(20, 79, 24, 20);

          //---- OBJ_61 ----
          OBJ_61.setText("N\u00b0 3");
          OBJ_61.setName("OBJ_61");
          panel2.add(OBJ_61);
          OBJ_61.setBounds(20, 109, 24, 20);

          //---- OBJ_62 ----
          OBJ_62.setText("N\u00b0 4");
          OBJ_62.setName("OBJ_62");
          panel2.add(OBJ_62);
          OBJ_62.setBounds(20, 139, 24, 20);

          //---- OBJ_63 ----
          OBJ_63.setText("N\u00b0 5");
          OBJ_63.setName("OBJ_63");
          panel2.add(OBJ_63);
          OBJ_63.setBounds(20, 169, 24, 20);

          //---- RFA1CF ----
          RFA1CF.setText("");
          RFA1CF.setToolTipText("Application ristourne sur client factur\u00e9");
          RFA1CF.setComponentPopupMenu(BTD);
          RFA1CF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA1CF.setName("RFA1CF");
          panel2.add(RFA1CF);
          RFA1CF.setBounds(455, 49, 21, 20);

          //---- RFA1C1 ----
          RFA1C1.setText("");
          RFA1C1.setToolTipText("Application ristourne sur centrale 1");
          RFA1C1.setComponentPopupMenu(BTD);
          RFA1C1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA1C1.setName("RFA1C1");
          panel2.add(RFA1C1);
          RFA1C1.setBounds(475, 49, 21, 20);

          //---- RFA1C2 ----
          RFA1C2.setText("");
          RFA1C2.setToolTipText("Application ristourne sur centrale 2");
          RFA1C2.setComponentPopupMenu(BTD);
          RFA1C2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA1C2.setName("RFA1C2");
          panel2.add(RFA1C2);
          RFA1C2.setBounds(495, 49, 21, 20);

          //---- RFA1C3 ----
          RFA1C3.setText("");
          RFA1C3.setToolTipText("Application ristourne sur centrale 3");
          RFA1C3.setComponentPopupMenu(BTD);
          RFA1C3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA1C3.setName("RFA1C3");
          panel2.add(RFA1C3);
          RFA1C3.setBounds(515, 49, 21, 20);

          //---- RFA2CF ----
          RFA2CF.setText("");
          RFA2CF.setToolTipText("Application ristourne sur client factur\u00e9");
          RFA2CF.setComponentPopupMenu(BTD);
          RFA2CF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA2CF.setName("RFA2CF");
          panel2.add(RFA2CF);
          RFA2CF.setBounds(455, 79, 21, 20);

          //---- RFA2C1 ----
          RFA2C1.setText("");
          RFA2C1.setToolTipText("Application ristourne sur centrale 1");
          RFA2C1.setComponentPopupMenu(BTD);
          RFA2C1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA2C1.setName("RFA2C1");
          panel2.add(RFA2C1);
          RFA2C1.setBounds(475, 79, 21, 20);

          //---- RFA2C2 ----
          RFA2C2.setText("");
          RFA2C2.setToolTipText("Application ristourne sur centrale 2");
          RFA2C2.setComponentPopupMenu(BTD);
          RFA2C2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA2C2.setName("RFA2C2");
          panel2.add(RFA2C2);
          RFA2C2.setBounds(495, 79, 21, 20);

          //---- RFA2C3 ----
          RFA2C3.setText("");
          RFA2C3.setToolTipText("Application ristourne sur centrale 3");
          RFA2C3.setComponentPopupMenu(BTD);
          RFA2C3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA2C3.setName("RFA2C3");
          panel2.add(RFA2C3);
          RFA2C3.setBounds(515, 79, 21, 20);

          //---- RFA3CF ----
          RFA3CF.setText("");
          RFA3CF.setToolTipText("Application ristourne sur client factur\u00e9");
          RFA3CF.setComponentPopupMenu(BTD);
          RFA3CF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA3CF.setName("RFA3CF");
          panel2.add(RFA3CF);
          RFA3CF.setBounds(455, 109, 21, 20);

          //---- RFA3C1 ----
          RFA3C1.setText("");
          RFA3C1.setToolTipText("Application ristourne sur centrale 1");
          RFA3C1.setComponentPopupMenu(BTD);
          RFA3C1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA3C1.setName("RFA3C1");
          panel2.add(RFA3C1);
          RFA3C1.setBounds(475, 109, 21, 20);

          //---- RFA3C2 ----
          RFA3C2.setText("");
          RFA3C2.setToolTipText("Application ristourne sur centrale 2");
          RFA3C2.setComponentPopupMenu(BTD);
          RFA3C2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA3C2.setName("RFA3C2");
          panel2.add(RFA3C2);
          RFA3C2.setBounds(495, 109, 21, 20);

          //---- RFA3C3 ----
          RFA3C3.setText("");
          RFA3C3.setToolTipText("Application ristourne sur centrale 3");
          RFA3C3.setComponentPopupMenu(BTD);
          RFA3C3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA3C3.setName("RFA3C3");
          panel2.add(RFA3C3);
          RFA3C3.setBounds(515, 109, 21, 20);

          //---- RFA4CF ----
          RFA4CF.setText("");
          RFA4CF.setToolTipText("Application ristourne sur client factur\u00e9");
          RFA4CF.setComponentPopupMenu(BTD);
          RFA4CF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA4CF.setName("RFA4CF");
          panel2.add(RFA4CF);
          RFA4CF.setBounds(455, 139, 21, 20);

          //---- RFA4C1 ----
          RFA4C1.setText("");
          RFA4C1.setToolTipText("Application ristourne sur centrale 1");
          RFA4C1.setComponentPopupMenu(BTD);
          RFA4C1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA4C1.setName("RFA4C1");
          panel2.add(RFA4C1);
          RFA4C1.setBounds(475, 139, 21, 20);

          //---- RFA4C2 ----
          RFA4C2.setText("");
          RFA4C2.setToolTipText("Application ristourne sur centrale 2");
          RFA4C2.setComponentPopupMenu(BTD);
          RFA4C2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA4C2.setName("RFA4C2");
          panel2.add(RFA4C2);
          RFA4C2.setBounds(495, 139, 21, 20);

          //---- RFA4C3 ----
          RFA4C3.setText("");
          RFA4C3.setToolTipText("Application ristourne sur centrale 3");
          RFA4C3.setComponentPopupMenu(BTD);
          RFA4C3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA4C3.setName("RFA4C3");
          panel2.add(RFA4C3);
          RFA4C3.setBounds(515, 139, 21, 20);

          //---- RFA5CF ----
          RFA5CF.setText("");
          RFA5CF.setToolTipText("Application ristourne sur client factur\u00e9");
          RFA5CF.setComponentPopupMenu(BTD);
          RFA5CF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA5CF.setName("RFA5CF");
          panel2.add(RFA5CF);
          RFA5CF.setBounds(455, 169, 21, 20);

          //---- RFA5C1 ----
          RFA5C1.setText("");
          RFA5C1.setToolTipText("Application ristourne sur centrale 1");
          RFA5C1.setComponentPopupMenu(BTD);
          RFA5C1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA5C1.setName("RFA5C1");
          panel2.add(RFA5C1);
          RFA5C1.setBounds(475, 169, 21, 20);

          //---- RFA5C2 ----
          RFA5C2.setText("");
          RFA5C2.setToolTipText("Application ristourne sur centrale 2");
          RFA5C2.setComponentPopupMenu(BTD);
          RFA5C2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA5C2.setName("RFA5C2");
          panel2.add(RFA5C2);
          RFA5C2.setBounds(495, 169, 21, 20);

          //---- RFA5C3 ----
          RFA5C3.setText("");
          RFA5C3.setToolTipText("Application ristourne sur centrale 3");
          RFA5C3.setComponentPopupMenu(BTD);
          RFA5C3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RFA5C3.setName("RFA5C3");
          panel2.add(RFA5C3);
          RFA5C3.setBounds(515, 169, 21, 20);

          //---- OBJ_75 ----
          OBJ_75.setText("C");
          OBJ_75.setToolTipText("Application ristourne sur client factur\u00e9");
          OBJ_75.setName("OBJ_75");
          panel2.add(OBJ_75);
          OBJ_75.setBounds(459, 195, 12, 20);

          //---- OBJ_76 ----
          OBJ_76.setText("1");
          OBJ_76.setToolTipText("Application ristourne sur centrale 1");
          OBJ_76.setName("OBJ_76");
          panel2.add(OBJ_76);
          OBJ_76.setBounds(479, 195, 12, 20);

          //---- OBJ_77 ----
          OBJ_77.setText("2");
          OBJ_77.setToolTipText("Application ristourne sur centrale 2");
          OBJ_77.setName("OBJ_77");
          panel2.add(OBJ_77);
          OBJ_77.setBounds(499, 195, 12, 20);

          //---- OBJ_78 ----
          OBJ_78.setText("3");
          OBJ_78.setToolTipText("Application ristourne sur centrale 3");
          OBJ_78.setName("OBJ_78");
          panel2.add(OBJ_78);
          OBJ_78.setBounds(519, 195, 12, 20);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Bar\u00eames");
          xTitledSeparator1.setName("xTitledSeparator1");
          panel2.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(15, 20, 105, xTitledSeparator1.getPreferredSize().height);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("D\u00e9signation");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel2.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(130, 20, 310, xTitledSeparator2.getPreferredSize().height);

          //---- xTitledSeparator3 ----
          xTitledSeparator3.setTitle("Ristourne");
          xTitledSeparator3.setName("xTitledSeparator3");
          panel2.add(xTitledSeparator3);
          xTitledSeparator3.setBounds(450, 20, 85, xTitledSeparator3.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_ann;
  private RiSousMenu_bt riSousMenu_bt_ann;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie OBJ_56;
  private RiZoneSortie OBJ_57;
  private RiZoneSortie OBJ_58;
  private RiZoneSortie OBJ_59;
  private RiZoneSortie OBJ_60;
  private XRiTextField RFBAR;
  private XRiTextField RFBAR2;
  private XRiTextField RFBAR3;
  private XRiTextField RFBAR4;
  private XRiTextField RFBAR5;
  private JLabel OBJ_26;
  private JLabel OBJ_53;
  private JLabel OBJ_61;
  private JLabel OBJ_62;
  private JLabel OBJ_63;
  private XRiCheckBox RFA1CF;
  private XRiCheckBox RFA1C1;
  private XRiCheckBox RFA1C2;
  private XRiCheckBox RFA1C3;
  private XRiCheckBox RFA2CF;
  private XRiCheckBox RFA2C1;
  private XRiCheckBox RFA2C2;
  private XRiCheckBox RFA2C3;
  private XRiCheckBox RFA3CF;
  private XRiCheckBox RFA3C1;
  private XRiCheckBox RFA3C2;
  private XRiCheckBox RFA3C3;
  private XRiCheckBox RFA4CF;
  private XRiCheckBox RFA4C1;
  private XRiCheckBox RFA4C2;
  private XRiCheckBox RFA4C3;
  private XRiCheckBox RFA5CF;
  private XRiCheckBox RFA5C1;
  private XRiCheckBox RFA5C2;
  private XRiCheckBox RFA5C3;
  private JLabel OBJ_75;
  private JLabel OBJ_76;
  private JLabel OBJ_77;
  private JLabel OBJ_78;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JXTitledSeparator xTitledSeparator3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
