
package ri.serien.libecranrpg.vgvm.VGVM04FM;
// Nom Fichier: b_VGVM04FM_FMTB2_FMTF1_1072.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM04FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] LTIN2_Value = { "", "1", "2", "3", "4", "5", "6", "7", };
  private String[] LTIN1_Value = { "", "1", "2", "3", "4", "5", "6", "7", };
  
  public VGVM04FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    LTIN2.setValeurs(LTIN2_Value, null);
    LTIN1.setValeurs(LTIN1_Value, null);
    TOLU15.setValeursSelection("1", " ");
    TODI25.setValeursSelection("1", " ");
    TODI24.setValeursSelection("1", " ");
    TODI23.setValeursSelection("1", " ");
    TODI22.setValeursSelection("1", " ");
    TODI21.setValeursSelection("1", " ");
    TODI15.setValeursSelection("1", " ");
    TODI14.setValeursSelection("1", " ");
    TODI13.setValeursSelection("1", " ");
    TODI12.setValeursSelection("1", " ");
    TODI11.setValeursSelection("1", " ");
    TOSA25.setValeursSelection("1", " ");
    TOSA24.setValeursSelection("1", " ");
    TOSA23.setValeursSelection("1", " ");
    TOSA22.setValeursSelection("1", " ");
    TOSA21.setValeursSelection("1", " ");
    TOSA15.setValeursSelection("1", " ");
    TOSA14.setValeursSelection("1", " ");
    TOSA13.setValeursSelection("1", " ");
    TOSA12.setValeursSelection("1", " ");
    TOSA11.setValeursSelection("1", " ");
    TOVE25.setValeursSelection("1", " ");
    TOVE24.setValeursSelection("1", " ");
    TOVE23.setValeursSelection("1", " ");
    TOVE22.setValeursSelection("1", " ");
    TOVE21.setValeursSelection("1", " ");
    TOVE15.setValeursSelection("1", " ");
    TOVE14.setValeursSelection("1", " ");
    TOVE13.setValeursSelection("1", " ");
    TOVE12.setValeursSelection("1", " ");
    TOVE11.setValeursSelection("1", " ");
    TOJE25.setValeursSelection("1", " ");
    TOJE24.setValeursSelection("1", " ");
    TOJE23.setValeursSelection("1", " ");
    TOJE22.setValeursSelection("1", " ");
    TOJE21.setValeursSelection("1", " ");
    TOJE15.setValeursSelection("1", " ");
    TOJE14.setValeursSelection("1", " ");
    TOJE13.setValeursSelection("1", " ");
    TOJE12.setValeursSelection("1", " ");
    TOJE11.setValeursSelection("1", " ");
    TOME25.setValeursSelection("1", " ");
    TOME24.setValeursSelection("1", " ");
    TOME23.setValeursSelection("1", " ");
    TOME22.setValeursSelection("1", " ");
    TOME21.setValeursSelection("1", " ");
    TOME15.setValeursSelection("1", " ");
    TOME14.setValeursSelection("1", " ");
    TOME13.setValeursSelection("1", " ");
    TOME12.setValeursSelection("1", " ");
    TOME11.setValeursSelection("1", " ");
    TOMA25.setValeursSelection("1", " ");
    TOMA24.setValeursSelection("1", " ");
    TOMA23.setValeursSelection("1", " ");
    TOMA22.setValeursSelection("1", " ");
    TOMA21.setValeursSelection("1", " ");
    TOMA15.setValeursSelection("1", " ");
    TOMA14.setValeursSelection("1", " ");
    TOMA13.setValeursSelection("1", " ");
    TOMA12.setValeursSelection("1", " ");
    TOMA11.setValeursSelection("1", " ");
    TOLU25.setValeursSelection("1", " ");
    TOLU24.setValeursSelection("1", " ");
    TOLU23.setValeursSelection("1", " ");
    TOLU22.setValeursSelection("1", " ");
    TOLU21.setValeursSelection("1", " ");
    TOLU14.setValeursSelection("1", " ");
    TOLU13.setValeursSelection("1", " ");
    TOLU12.setValeursSelection("1", " ");
    TOLU11.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    xTitledPanel2.setLeftDecoration(panel5);
    
    // Menu Command
    // setMenuCommand(OBJ_6);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_205.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@&L000079@")).trim());
    xTitledPanel3.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@JOUR@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOFP1X@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOFP2X@")).trim());
    OBJ_141.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOFP3X@")).trim());
    OBJ_143.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOFP4X@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@APPE2@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
    OBJ_224.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@APPE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_106.setVisible(lexique.isPresent("TOLU11"));
    OBJ_111.setVisible(lexique.isPresent("TOLU11"));
    OBJ_110.setVisible(lexique.isPresent("TOLU11"));
    OBJ_109.setVisible(lexique.isPresent("TOLU11"));
    OBJ_108.setVisible(lexique.isPresent("TOLU11"));
    OBJ_107.setVisible(lexique.isPresent("TOLU11"));
    OBJ_105.setVisible(lexique.isPresent("TOLU11"));
    OBJ_104.setVisible(lexique.isPresent("TOLU11"));
    OBJ_103.setVisible(lexique.isPresent("TOLU11"));
    OBJ_102.setVisible(lexique.isPresent("TOLU11"));
    OBJ_179.setVisible(lexique.isPresent("LTPMN"));
    // TOLU15.setVisible( lexique.isPresent("TOLU15"));
    // TOLU15.setSelected(lexique.HostFieldGetData("TOLU15").equalsIgnoreCase("1"));
    // TODI25.setVisible( lexique.isPresent("TODI25"));
    // TODI25.setSelected(lexique.HostFieldGetData("TODI25").equalsIgnoreCase("1"));
    // TODI24.setVisible( lexique.isPresent("TODI24"));
    // TODI24.setSelected(lexique.HostFieldGetData("TODI24").equalsIgnoreCase("1"));
    // TODI23.setVisible( lexique.isPresent("TODI23"));
    // TODI23.setSelected(lexique.HostFieldGetData("TODI23").equalsIgnoreCase("1"));
    // TODI22.setVisible( lexique.isPresent("TODI22"));
    // TODI22.setSelected(lexique.HostFieldGetData("TODI22").equalsIgnoreCase("1"));
    // TODI21.setVisible( lexique.isPresent("TODI21"));
    // TODI21.setSelected(lexique.HostFieldGetData("TODI21").equalsIgnoreCase("1"));
    // TODI15.setVisible( lexique.isPresent("TODI15"));
    // TODI15.setSelected(lexique.HostFieldGetData("TODI15").equalsIgnoreCase("1"));
    // TODI14.setVisible( lexique.isPresent("TODI14"));
    // TODI14.setSelected(lexique.HostFieldGetData("TODI14").equalsIgnoreCase("1"));
    // TODI13.setVisible( lexique.isPresent("TODI13"));
    // TODI13.setSelected(lexique.HostFieldGetData("TODI13").equalsIgnoreCase("1"));
    // TODI12.setVisible( lexique.isPresent("TODI12"));
    // TODI12.setSelected(lexique.HostFieldGetData("TODI12").equalsIgnoreCase("1"));
    // TODI11.setVisible( lexique.isPresent("TODI11"));
    // TODI11.setSelected(lexique.HostFieldGetData("TODI11").equalsIgnoreCase("1"));
    // TOSA25.setVisible( lexique.isPresent("TOSA25"));
    // TOSA25.setSelected(lexique.HostFieldGetData("TOSA25").equalsIgnoreCase("1"));
    // TOSA24.setVisible( lexique.isPresent("TOSA24"));
    // TOSA24.setSelected(lexique.HostFieldGetData("TOSA24").equalsIgnoreCase("1"));
    // TOSA23.setVisible( lexique.isPresent("TOSA23"));
    // TOSA23.setSelected(lexique.HostFieldGetData("TOSA23").equalsIgnoreCase("1"));
    // TOSA22.setVisible( lexique.isPresent("TOSA22"));
    // TOSA22.setSelected(lexique.HostFieldGetData("TOSA22").equalsIgnoreCase("1"));
    // TOSA21.setVisible( lexique.isPresent("TOSA21"));
    // TOSA21.setSelected(lexique.HostFieldGetData("TOSA21").equalsIgnoreCase("1"));
    // TOSA15.setVisible( lexique.isPresent("TOSA15"));
    // TOSA15.setSelected(lexique.HostFieldGetData("TOSA15").equalsIgnoreCase("1"));
    // TOSA14.setVisible( lexique.isPresent("TOSA14"));
    // TOSA14.setSelected(lexique.HostFieldGetData("TOSA14").equalsIgnoreCase("1"));
    // TOSA13.setVisible( lexique.isPresent("TOSA13"));
    // TOSA13.setSelected(lexique.HostFieldGetData("TOSA13").equalsIgnoreCase("1"));
    // TOSA12.setVisible( lexique.isPresent("TOSA12"));
    // TOSA12.setSelected(lexique.HostFieldGetData("TOSA12").equalsIgnoreCase("1"));
    // TOSA11.setVisible( lexique.isPresent("TOSA11"));
    // TOSA11.setSelected(lexique.HostFieldGetData("TOSA11").equalsIgnoreCase("1"));
    // TOVE25.setVisible( lexique.isPresent("TOVE25"));
    // TOVE25.setSelected(lexique.HostFieldGetData("TOVE25").equalsIgnoreCase("1"));
    // TOVE24.setVisible( lexique.isPresent("TOVE24"));
    // TOVE24.setSelected(lexique.HostFieldGetData("TOVE24").equalsIgnoreCase("1"));
    // TOVE23.setVisible( lexique.isPresent("TOVE23"));
    // TOVE23.setSelected(lexique.HostFieldGetData("TOVE23").equalsIgnoreCase("1"));
    // TOVE22.setVisible( lexique.isPresent("TOVE22"));
    // TOVE22.setSelected(lexique.HostFieldGetData("TOVE22").equalsIgnoreCase("1"));
    // TOVE21.setVisible( lexique.isPresent("TOVE21"));
    // TOVE21.setSelected(lexique.HostFieldGetData("TOVE21").equalsIgnoreCase("1"));
    // TOVE15.setVisible( lexique.isPresent("TOVE15"));
    // TOVE15.setSelected(lexique.HostFieldGetData("TOVE15").equalsIgnoreCase("1"));
    // TOVE14.setVisible( lexique.isPresent("TOVE14"));
    // TOVE14.setSelected(lexique.HostFieldGetData("TOVE14").equalsIgnoreCase("1"));
    // TOVE13.setVisible( lexique.isPresent("TOVE13"));
    // TOVE13.setSelected(lexique.HostFieldGetData("TOVE13").equalsIgnoreCase("1"));
    // TOVE12.setVisible( lexique.isPresent("TOVE12"));
    // TOVE12.setSelected(lexique.HostFieldGetData("TOVE12").equalsIgnoreCase("1"));
    // TOVE11.setVisible( lexique.isPresent("TOVE11"));
    // TOVE11.setSelected(lexique.HostFieldGetData("TOVE11").equalsIgnoreCase("1"));
    // TOJE25.setVisible( lexique.isPresent("TOJE25"));
    // TOJE25.setSelected(lexique.HostFieldGetData("TOJE25").equalsIgnoreCase("1"));
    // TOJE24.setVisible( lexique.isPresent("TOJE24"));
    // TOJE24.setSelected(lexique.HostFieldGetData("TOJE24").equalsIgnoreCase("1"));
    // TOJE23.setVisible( lexique.isPresent("TOJE23"));
    // TOJE23.setSelected(lexique.HostFieldGetData("TOJE23").equalsIgnoreCase("1"));
    // TOJE22.setVisible( lexique.isPresent("TOJE22"));
    // TOJE22.setSelected(lexique.HostFieldGetData("TOJE22").equalsIgnoreCase("1"));
    // TOJE21.setVisible( lexique.isPresent("TOJE21"));
    // TOJE21.setSelected(lexique.HostFieldGetData("TOJE21").equalsIgnoreCase("1"));
    // TOJE15.setVisible( lexique.isPresent("TOJE15"));
    // TOJE15.setSelected(lexique.HostFieldGetData("TOJE15").equalsIgnoreCase("1"));
    // TOJE14.setVisible( lexique.isPresent("TOJE14"));
    // TOJE14.setSelected(lexique.HostFieldGetData("TOJE14").equalsIgnoreCase("1"));
    // TOJE13.setVisible( lexique.isPresent("TOJE13"));
    // TOJE13.setSelected(lexique.HostFieldGetData("TOJE13").equalsIgnoreCase("1"));
    // TOJE12.setVisible( lexique.isPresent("TOJE12"));
    // TOJE12.setSelected(lexique.HostFieldGetData("TOJE12").equalsIgnoreCase("1"));
    // TOJE11.setVisible( lexique.isPresent("TOJE11"));
    // TOJE11.setSelected(lexique.HostFieldGetData("TOJE11").equalsIgnoreCase("1"));
    // TOME25.setVisible( lexique.isPresent("TOME25"));
    // TOME25.setSelected(lexique.HostFieldGetData("TOME25").equalsIgnoreCase("1"));
    // TOME24.setVisible( lexique.isPresent("TOME24"));
    // TOME24.setSelected(lexique.HostFieldGetData("TOME24").equalsIgnoreCase("1"));
    // TOME23.setVisible( lexique.isPresent("TOME23"));
    // TOME23.setSelected(lexique.HostFieldGetData("TOME23").equalsIgnoreCase("1"));
    // TOME22.setVisible( lexique.isPresent("TOME22"));
    // TOME22.setSelected(lexique.HostFieldGetData("TOME22").equalsIgnoreCase("1"));
    // TOME21.setVisible( lexique.isPresent("TOME21"));
    // TOME21.setSelected(lexique.HostFieldGetData("TOME21").equalsIgnoreCase("1"));
    // TOME15.setVisible( lexique.isPresent("TOME15"));
    // TOME15.setSelected(lexique.HostFieldGetData("TOME15").equalsIgnoreCase("1"));
    // TOME14.setVisible( lexique.isPresent("TOME14"));
    // TOME14.setSelected(lexique.HostFieldGetData("TOME14").equalsIgnoreCase("1"));
    // TOME13.setVisible( lexique.isPresent("TOME13"));
    // TOME13.setSelected(lexique.HostFieldGetData("TOME13").equalsIgnoreCase("1"));
    // TOME12.setVisible( lexique.isPresent("TOME12"));
    // TOME12.setSelected(lexique.HostFieldGetData("TOME12").equalsIgnoreCase("1"));
    // TOME11.setVisible( lexique.isPresent("TOME11"));
    // TOME11.setSelected(lexique.HostFieldGetData("TOME11").equalsIgnoreCase("1"));
    // TOMA25.setVisible( lexique.isPresent("TOMA25"));
    // TOMA25.setSelected(lexique.HostFieldGetData("TOMA25").equalsIgnoreCase("1"));
    // TOMA24.setVisible( lexique.isPresent("TOMA24"));
    // TOMA24.setSelected(lexique.HostFieldGetData("TOMA24").equalsIgnoreCase("1"));
    // TOMA23.setVisible( lexique.isPresent("TOMA23"));
    // TOMA23.setSelected(lexique.HostFieldGetData("TOMA23").equalsIgnoreCase("1"));
    // TOMA22.setVisible( lexique.isPresent("TOMA22"));
    // TOMA22.setSelected(lexique.HostFieldGetData("TOMA22").equalsIgnoreCase("1"));
    // TOMA21.setVisible( lexique.isPresent("TOMA21"));
    // TOMA21.setSelected(lexique.HostFieldGetData("TOMA21").equalsIgnoreCase("1"));
    // TOMA15.setVisible( lexique.isPresent("TOMA15"));
    // TOMA15.setSelected(lexique.HostFieldGetData("TOMA15").equalsIgnoreCase("1"));
    // TOMA14.setVisible( lexique.isPresent("TOMA14"));
    // TOMA14.setSelected(lexique.HostFieldGetData("TOMA14").equalsIgnoreCase("1"));
    // TOMA13.setVisible( lexique.isPresent("TOMA13"));
    // TOMA13.setSelected(lexique.HostFieldGetData("TOMA13").equalsIgnoreCase("1"));
    // TOMA12.setVisible( lexique.isPresent("TOMA12"));
    // TOMA12.setSelected(lexique.HostFieldGetData("TOMA12").equalsIgnoreCase("1"));
    // TOMA11.setVisible( lexique.isPresent("TOMA11"));
    // TOMA11.setSelected(lexique.HostFieldGetData("TOMA11").equalsIgnoreCase("1"));
    // TOLU25.setVisible( lexique.isPresent("TOLU25"));
    // TOLU25.setSelected(lexique.HostFieldGetData("TOLU25").equalsIgnoreCase("1"));
    // TOLU24.setVisible( lexique.isPresent("TOLU24"));
    // TOLU24.setSelected(lexique.HostFieldGetData("TOLU24").equalsIgnoreCase("1"));
    // TOLU23.setVisible( lexique.isPresent("TOLU23"));
    // TOLU23.setSelected(lexique.HostFieldGetData("TOLU23").equalsIgnoreCase("1"));
    // TOLU22.setVisible( lexique.isPresent("TOLU22"));
    // TOLU22.setSelected(lexique.HostFieldGetData("TOLU22").equalsIgnoreCase("1"));
    // TOLU21.setVisible( lexique.isPresent("TOLU21"));
    // TOLU21.setSelected(lexique.HostFieldGetData("TOLU21").equalsIgnoreCase("1"));
    // TOLU14.setVisible( lexique.isPresent("TOLU14"));
    // TOLU14.setSelected(lexique.HostFieldGetData("TOLU14").equalsIgnoreCase("1"));
    // TOLU13.setVisible( lexique.isPresent("TOLU13"));
    // TOLU13.setSelected(lexique.HostFieldGetData("TOLU13").equalsIgnoreCase("1"));
    // TOLU12.setVisible( lexique.isPresent("TOLU12"));
    // TOLU12.setSelected(lexique.HostFieldGetData("TOLU12").equalsIgnoreCase("1"));
    // TOLU11.setVisible( lexique.isPresent("TOLU11"));
    // TOLU11.setSelected(lexique.HostFieldGetData("TOLU11").equalsIgnoreCase("1"));
    OBJ_142.setVisible(lexique.isPresent("TOFP4X"));
    OBJ_140.setVisible(lexique.isPresent("TOFP2X"));
    OBJ_114.setVisible(lexique.isPresent("TOFP3X"));
    OBJ_112.setVisible(lexique.isPresent("TOFP1X"));
    LTPMN.setVisible(lexique.isPresent("LTPMN"));
    LTPHR.setVisible(lexique.isPresent("LTPHR"));
    LTHF2M.setEnabled(lexique.isPresent("LTHF2M"));
    LTHF2H.setEnabled(lexique.isPresent("LTHF2H"));
    LTHD2M.setEnabled(lexique.isPresent("LTHD2M"));
    LTHD2H.setEnabled(lexique.isPresent("LTHD2H"));
    LTHF1M.setEnabled(lexique.isPresent("LTHF1M"));
    LTHF1H.setEnabled(lexique.isPresent("LTHF1H"));
    LTHD1M.setEnabled(lexique.isPresent("LTHD1M"));
    LTHD1H.setEnabled(lexique.isPresent("LTHD1H"));
    INDMEX.setVisible(lexique.isPresent("INDMEX"));
    INDLIV.setVisible(lexique.isPresent("INDLIV"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    OBJ_116.setVisible(lexique.isPresent("TOLU11"));
    OBJ_157.setVisible(lexique.isPresent("TOJE11"));
    OBJ_143.setVisible(lexique.isPresent("TOFP4X"));
    OBJ_141.setVisible(lexique.isPresent("TOFP3X"));
    OBJ_115.setVisible(lexique.isPresent("TOFP2X"));
    OBJ_113.setVisible(lexique.isPresent("TOFP1X"));
    TODP4X.setVisible(lexique.isPresent("TODP4X"));
    TODP3X.setVisible(lexique.isPresent("TODP3X"));
    TODP2X.setVisible(lexique.isPresent("TODP2X"));
    TODP1X.setVisible(lexique.isPresent("TODP1X"));
    LTORT1.setVisible(lexique.isPresent("LTORT1"));
    LTTOU1.setEnabled(lexique.isPresent("LTTOU1"));
    OBJ_129.setVisible(lexique.isPresent("TOMA11"));
    V06FO.setVisible(lexique.isPresent("V06FO"));
    OBJ_220.setVisible(lexique.isPresent("LTORT1"));
    CLCDP.setVisible(lexique.isPresent("CLCDP"));
    INDCLI.setVisible(lexique.isPresent("INDCLI"));
    OBJ_180.setVisible(lexique.isPresent("TOSA11"));
    // CTDATX.setEnabled( lexique.isPresent("CTDATX"));
    // LTDF2X.setEnabled( lexique.isPresent("LTDF2X"));
    // LTDD2X.setEnabled( lexique.isPresent("LTDD2X"));
    // LTDF1X.setEnabled( lexique.isPresent("LTDF1X"));
    // LTDD1X.setEnabled( lexique.isPresent("LTDD1X"));
    OBJ_145.setVisible(lexique.isPresent("TOME11"));
    OBJ_168.setVisible(lexique.isPresent("TOVE11"));
    OBJ_193.setVisible(lexique.isPresent("TODI11"));
    OBJ_191.setEnabled(lexique.isPresent("LTOBS"));
    OBJ_205.setVisible(lexique.HostFieldGetData("V02F").equalsIgnoreCase("ERR"));
    OBJ_128.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("MODIFIC."));
    OBJ_86.setVisible(!lexique.HostFieldGetData("V01F").trim().equalsIgnoreCase("INTERRO."));
    OBJ_42.setVisible(lexique.isPresent("V01F"));
    OBJ_224.setVisible(lexique.isPresent("APPE"));
    CLVILR.setVisible(lexique.isPresent("CLVILR"));
    LTOBS.setEnabled(lexique.isPresent("LTOBS"));
    CLLOC.setVisible(lexique.isPresent("CLLOC"));
    CLRUE.setVisible(lexique.isPresent("CLRUE"));
    CLCPL.setVisible(lexique.isPresent("CLCPL"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
    // LTIN2.setEnabled( lexique.isPresent("LTIN2"));
    // LTIN1.setEnabled( lexique.isPresent("LTIN1"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    OBJ_205.setIcon(lexique.chargerImage("images/w95mbx03.gif", true));
    OBJ_128.setIcon(lexique.chargerImage("images/periode1.gif", true));
    OBJ_86.setIcon(lexique.chargerImage("images/periode1.gif", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(INDETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  CLIENT/TOURNEE"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (TOLU15.isSelected())
    // lexique.HostFieldPutData("TOLU15", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU15", 0, " ");
    // if (TODI25.isSelected())
    // lexique.HostFieldPutData("TODI25", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI25", 0, " ");
    // if (TODI24.isSelected())
    // lexique.HostFieldPutData("TODI24", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI24", 0, " ");
    // if (TODI23.isSelected())
    // lexique.HostFieldPutData("TODI23", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI23", 0, " ");
    // if (TODI22.isSelected())
    // lexique.HostFieldPutData("TODI22", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI22", 0, " ");
    // if (TODI21.isSelected())
    // lexique.HostFieldPutData("TODI21", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI21", 0, " ");
    // if (TODI15.isSelected())
    // lexique.HostFieldPutData("TODI15", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI15", 0, " ");
    // if (TODI14.isSelected())
    // lexique.HostFieldPutData("TODI14", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI14", 0, " ");
    // if (TODI13.isSelected())
    // lexique.HostFieldPutData("TODI13", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI13", 0, " ");
    // if (TODI12.isSelected())
    // lexique.HostFieldPutData("TODI12", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI12", 0, " ");
    // if (TODI11.isSelected())
    // lexique.HostFieldPutData("TODI11", 0, "1");
    // else
    // lexique.HostFieldPutData("TODI11", 0, " ");
    // if (TOSA25.isSelected())
    // lexique.HostFieldPutData("TOSA25", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA25", 0, " ");
    // if (TOSA24.isSelected())
    // lexique.HostFieldPutData("TOSA24", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA24", 0, " ");
    // if (TOSA23.isSelected())
    // lexique.HostFieldPutData("TOSA23", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA23", 0, " ");
    // if (TOSA22.isSelected())
    // lexique.HostFieldPutData("TOSA22", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA22", 0, " ");
    // if (TOSA21.isSelected())
    // lexique.HostFieldPutData("TOSA21", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA21", 0, " ");
    // if (TOSA15.isSelected())
    // lexique.HostFieldPutData("TOSA15", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA15", 0, " ");
    // if (TOSA14.isSelected())
    // lexique.HostFieldPutData("TOSA14", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA14", 0, " ");
    // if (TOSA13.isSelected())
    // lexique.HostFieldPutData("TOSA13", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA13", 0, " ");
    // if (TOSA12.isSelected())
    // lexique.HostFieldPutData("TOSA12", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA12", 0, " ");
    // if (TOSA11.isSelected())
    // lexique.HostFieldPutData("TOSA11", 0, "1");
    // else
    // lexique.HostFieldPutData("TOSA11", 0, " ");
    // if (TOVE25.isSelected())
    // lexique.HostFieldPutData("TOVE25", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE25", 0, " ");
    // if (TOVE24.isSelected())
    // lexique.HostFieldPutData("TOVE24", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE24", 0, " ");
    // if (TOVE23.isSelected())
    // lexique.HostFieldPutData("TOVE23", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE23", 0, " ");
    // if (TOVE22.isSelected())
    // lexique.HostFieldPutData("TOVE22", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE22", 0, " ");
    // if (TOVE21.isSelected())
    // lexique.HostFieldPutData("TOVE21", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE21", 0, " ");
    // if (TOVE15.isSelected())
    // lexique.HostFieldPutData("TOVE15", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE15", 0, " ");
    // if (TOVE14.isSelected())
    // lexique.HostFieldPutData("TOVE14", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE14", 0, " ");
    // if (TOVE13.isSelected())
    // lexique.HostFieldPutData("TOVE13", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE13", 0, " ");
    // if (TOVE12.isSelected())
    // lexique.HostFieldPutData("TOVE12", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE12", 0, " ");
    // if (TOVE11.isSelected())
    // lexique.HostFieldPutData("TOVE11", 0, "1");
    // else
    // lexique.HostFieldPutData("TOVE11", 0, " ");
    // if (TOJE25.isSelected())
    // lexique.HostFieldPutData("TOJE25", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE25", 0, " ");
    // if (TOJE24.isSelected())
    // lexique.HostFieldPutData("TOJE24", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE24", 0, " ");
    // if (TOJE23.isSelected())
    // lexique.HostFieldPutData("TOJE23", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE23", 0, " ");
    // if (TOJE22.isSelected())
    // lexique.HostFieldPutData("TOJE22", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE22", 0, " ");
    // if (TOJE21.isSelected())
    // lexique.HostFieldPutData("TOJE21", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE21", 0, " ");
    // if (TOJE15.isSelected())
    // lexique.HostFieldPutData("TOJE15", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE15", 0, " ");
    // if (TOJE14.isSelected())
    // lexique.HostFieldPutData("TOJE14", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE14", 0, " ");
    // if (TOJE13.isSelected())
    // lexique.HostFieldPutData("TOJE13", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE13", 0, " ");
    // if (TOJE12.isSelected())
    // lexique.HostFieldPutData("TOJE12", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE12", 0, " ");
    // if (TOJE11.isSelected())
    // lexique.HostFieldPutData("TOJE11", 0, "1");
    // else
    // lexique.HostFieldPutData("TOJE11", 0, " ");
    // if (TOME25.isSelected())
    // lexique.HostFieldPutData("TOME25", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME25", 0, " ");
    // if (TOME24.isSelected())
    // lexique.HostFieldPutData("TOME24", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME24", 0, " ");
    // if (TOME23.isSelected())
    // lexique.HostFieldPutData("TOME23", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME23", 0, " ");
    // if (TOME22.isSelected())
    // lexique.HostFieldPutData("TOME22", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME22", 0, " ");
    // if (TOME21.isSelected())
    // lexique.HostFieldPutData("TOME21", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME21", 0, " ");
    // if (TOME15.isSelected())
    // lexique.HostFieldPutData("TOME15", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME15", 0, " ");
    // if (TOME14.isSelected())
    // lexique.HostFieldPutData("TOME14", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME14", 0, " ");
    // if (TOME13.isSelected())
    // lexique.HostFieldPutData("TOME13", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME13", 0, " ");
    // if (TOME12.isSelected())
    // lexique.HostFieldPutData("TOME12", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME12", 0, " ");
    // if (TOME11.isSelected())
    // lexique.HostFieldPutData("TOME11", 0, "1");
    // else
    // lexique.HostFieldPutData("TOME11", 0, " ");
    // if (TOMA25.isSelected())
    // lexique.HostFieldPutData("TOMA25", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA25", 0, " ");
    // if (TOMA24.isSelected())
    // lexique.HostFieldPutData("TOMA24", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA24", 0, " ");
    // if (TOMA23.isSelected())
    // lexique.HostFieldPutData("TOMA23", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA23", 0, " ");
    // if (TOMA22.isSelected())
    // lexique.HostFieldPutData("TOMA22", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA22", 0, " ");
    // if (TOMA21.isSelected())
    // lexique.HostFieldPutData("TOMA21", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA21", 0, " ");
    // if (TOMA15.isSelected())
    // lexique.HostFieldPutData("TOMA15", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA15", 0, " ");
    // if (TOMA14.isSelected())
    // lexique.HostFieldPutData("TOMA14", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA14", 0, " ");
    // if (TOMA13.isSelected())
    // lexique.HostFieldPutData("TOMA13", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA13", 0, " ");
    // if (TOMA12.isSelected())
    // lexique.HostFieldPutData("TOMA12", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA12", 0, " ");
    // if (TOMA11.isSelected())
    // lexique.HostFieldPutData("TOMA11", 0, "1");
    // else
    // lexique.HostFieldPutData("TOMA11", 0, " ");
    // if (TOLU25.isSelected())
    // lexique.HostFieldPutData("TOLU25", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU25", 0, " ");
    // if (TOLU24.isSelected())
    // lexique.HostFieldPutData("TOLU24", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU24", 0, " ");
    // if (TOLU23.isSelected())
    // lexique.HostFieldPutData("TOLU23", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU23", 0, " ");
    // if (TOLU22.isSelected())
    // lexique.HostFieldPutData("TOLU22", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU22", 0, " ");
    // if (TOLU21.isSelected())
    // lexique.HostFieldPutData("TOLU21", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU21", 0, " ");
    // if (TOLU14.isSelected())
    // lexique.HostFieldPutData("TOLU14", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU14", 0, " ");
    // if (TOLU13.isSelected())
    // lexique.HostFieldPutData("TOLU13", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU13", 0, " ");
    // if (TOLU12.isSelected())
    // lexique.HostFieldPutData("TOLU12", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU12", 0, " ");
    // if (TOLU11.isSelected())
    // lexique.HostFieldPutData("TOLU11", 0, "1");
    // else
    // lexique.HostFieldPutData("TOLU11", 0, " ");
    // lexique.HostFieldPutData("LTIN2", 0, LTIN2_Value[LTIN2.getSelectedIndex()]);
    // lexique.HostFieldPutData("LTIN1", 0, LTIN1_Value[LTIN1.getSelectedIndex()]);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F10"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void OBJ_86ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // HostcursorPut(15,3)
    // touche="F4"
    // ScriptCall("G_Touche")
    lexique.HostCursorPut(15, 3);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_128ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F9"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void OBJ_205ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F1"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    CMD = new JPopupMenu();
    OBJ_7 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    INDCLI = new XRiTextField();
    OBJ_38 = new JLabel();
    INDETB = new XRiTextField();
    INDLIV = new XRiTextField();
    INDMEX = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    bt_Fonctions = new JButton();
    OBJ_42 = new JLabel();
    P_Centre = new JPanel();
    panel2 = new JPanel();
    P_PnlOpts = new JPanel();
    OBJ_86 = new JButton();
    OBJ_128 = new JButton();
    OBJ_205 = new JButton();
    panel3 = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CLNOM = new XRiTextField();
    CLCPL = new XRiTextField();
    CLRUE = new XRiTextField();
    CLLOC = new XRiTextField();
    LTOBS = new XRiTextField();
    CLVILR = new XRiTextField();
    OBJ_97 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_156 = new JLabel();
    OBJ_191 = new JLabel();
    CLCDP = new XRiTextField();
    panel4 = new JPanel();
    LTDD1X = new XRiCalendrier();
    LTDF1X = new XRiCalendrier();
    LTDD2X = new XRiCalendrier();
    LTDF2X = new XRiCalendrier();
    LTHD1H = new XRiTextField();
    LTHD1M = new XRiTextField();
    LTHF1H = new XRiTextField();
    LTHF1M = new XRiTextField();
    LTHD2H = new XRiTextField();
    LTHD2M = new XRiTextField();
    LTHF2H = new XRiTextField();
    LTHF2M = new XRiTextField();
    OBJ_210 = new JLabel();
    OBJ_214 = new JLabel();
    OBJ_206 = new JLabel();
    OBJ_207 = new JLabel();
    OBJ_208 = new JLabel();
    OBJ_209 = new JLabel();
    OBJ_211 = new JLabel();
    OBJ_213 = new JLabel();
    OBJ_215 = new JLabel();
    OBJ_217 = new JLabel();
    OBJ_212 = new JLabel();
    OBJ_216 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_193 = new JLabel();
    OBJ_168 = new JLabel();
    OBJ_145 = new JLabel();
    OBJ_180 = new JLabel();
    OBJ_129 = new JLabel();
    TODP1X = new XRiTextField();
    TODP2X = new XRiTextField();
    TODP3X = new XRiTextField();
    TODP4X = new XRiTextField();
    OBJ_113 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_141 = new JLabel();
    OBJ_143 = new JLabel();
    OBJ_157 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_114 = new JLabel();
    OBJ_140 = new JLabel();
    OBJ_142 = new JLabel();
    TOLU11 = new XRiCheckBox();
    TOLU12 = new XRiCheckBox();
    TOLU13 = new XRiCheckBox();
    TOLU14 = new XRiCheckBox();
    TOLU21 = new XRiCheckBox();
    TOLU22 = new XRiCheckBox();
    TOLU23 = new XRiCheckBox();
    TOLU24 = new XRiCheckBox();
    TOLU25 = new XRiCheckBox();
    TOMA11 = new XRiCheckBox();
    TOMA12 = new XRiCheckBox();
    TOMA13 = new XRiCheckBox();
    TOMA14 = new XRiCheckBox();
    TOMA15 = new XRiCheckBox();
    TOMA21 = new XRiCheckBox();
    TOMA22 = new XRiCheckBox();
    TOMA23 = new XRiCheckBox();
    TOMA24 = new XRiCheckBox();
    TOMA25 = new XRiCheckBox();
    TOME11 = new XRiCheckBox();
    TOME12 = new XRiCheckBox();
    TOME13 = new XRiCheckBox();
    TOME14 = new XRiCheckBox();
    TOME15 = new XRiCheckBox();
    TOME21 = new XRiCheckBox();
    TOME22 = new XRiCheckBox();
    TOME23 = new XRiCheckBox();
    TOME24 = new XRiCheckBox();
    TOME25 = new XRiCheckBox();
    TOJE11 = new XRiCheckBox();
    TOJE12 = new XRiCheckBox();
    TOJE13 = new XRiCheckBox();
    TOJE14 = new XRiCheckBox();
    TOJE15 = new XRiCheckBox();
    TOJE21 = new XRiCheckBox();
    TOJE22 = new XRiCheckBox();
    TOJE23 = new XRiCheckBox();
    TOJE24 = new XRiCheckBox();
    TOJE25 = new XRiCheckBox();
    TOVE11 = new XRiCheckBox();
    TOVE12 = new XRiCheckBox();
    TOVE13 = new XRiCheckBox();
    TOVE14 = new XRiCheckBox();
    TOVE15 = new XRiCheckBox();
    TOVE21 = new XRiCheckBox();
    TOVE22 = new XRiCheckBox();
    TOVE23 = new XRiCheckBox();
    TOVE24 = new XRiCheckBox();
    TOVE25 = new XRiCheckBox();
    TOSA11 = new XRiCheckBox();
    TOSA12 = new XRiCheckBox();
    TOSA13 = new XRiCheckBox();
    TOSA14 = new XRiCheckBox();
    TOSA15 = new XRiCheckBox();
    TOSA21 = new XRiCheckBox();
    TOSA22 = new XRiCheckBox();
    TOSA23 = new XRiCheckBox();
    TOSA24 = new XRiCheckBox();
    TOSA25 = new XRiCheckBox();
    TODI11 = new XRiCheckBox();
    TODI12 = new XRiCheckBox();
    TODI13 = new XRiCheckBox();
    TODI14 = new XRiCheckBox();
    TODI15 = new XRiCheckBox();
    TODI21 = new XRiCheckBox();
    TODI22 = new XRiCheckBox();
    TODI23 = new XRiCheckBox();
    TODI24 = new XRiCheckBox();
    TODI25 = new XRiCheckBox();
    TOLU15 = new XRiCheckBox();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_107 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_106 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    panel6 = new JPanel();
    OBJ_204 = new JLabel();
    OBJ_30 = new JLabel();
    CTDATX = new XRiCalendrier();
    OBJ_220 = new JLabel();
    LTORT1 = new XRiTextField();
    LTPHR = new XRiTextField();
    LTPMN = new XRiTextField();
    OBJ_179 = new JLabel();
    OBJ_192 = new JLabel();
    LTIN1 = new XRiComboBox();
    LTIN2 = new XRiComboBox();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    V06FO = new XRiTextField();
    OBJ_222 = new JLabel();
    panel5 = new JPanel();
    LTTOU1 = new XRiTextField();
    label1 = new JLabel();
    OBJ_224 = new JLabel();
    CellConstraints cc = new CellConstraints();

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_7 ----
      OBJ_7.setText("Fin de Travail");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_9 ----
      OBJ_9.setText("R\u00e9afficher");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Recherche multi-crit\u00e8res");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Fonctions");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Annuler");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_14 ----
      OBJ_14.setText("Cr\u00e9ation");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Modification");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      CMD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Interrogation");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      CMD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("Annulation");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      CMD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Duplication");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      CMD.add(OBJ_18);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Invite");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- INDCLI ----
        INDCLI.setComponentPopupMenu(BTDA);
        INDCLI.setName("INDCLI");

        //---- OBJ_38 ----
        OBJ_38.setText("Client");
        OBJ_38.setName("OBJ_38");

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTDA);
        INDETB.setName("INDETB");

        //---- INDLIV ----
        INDLIV.setComponentPopupMenu(BTDA);
        INDLIV.setName("INDLIV");

        //---- INDMEX ----
        INDMEX.setComponentPopupMenu(BTD);
        INDMEX.setName("INDMEX");

        //---- OBJ_37 ----
        OBJ_37.setText("Etablissement");
        OBJ_37.setName("OBJ_37");

        //---- OBJ_39 ----
        OBJ_39.setText("Liv");
        OBJ_39.setName("OBJ_39");

        //---- OBJ_40 ----
        OBJ_40.setText("Ex");
        OBJ_40.setName("OBJ_40");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- OBJ_42 ----
        OBJ_42.setText("@V01F@");
        OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
        OBJ_42.setName("OBJ_42");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(13, 13, 13)
              .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
              .addGap(3, 3, 3)
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
              .addGap(7, 7, 7)
              .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addGap(4, 4, 4)
              .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
              .addGap(6, 6, 6)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
              .addGap(4, 4, 4)
              .addComponent(INDMEX, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 363, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(6, 6, 6)
              .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(INDMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(bt_Fonctions)))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(new BorderLayout());

      //======== panel2 ========
      {
        panel2.setPreferredSize(new Dimension(50, 0));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //======== P_PnlOpts ========
        {
          P_PnlOpts.setName("P_PnlOpts");
          P_PnlOpts.setLayout(null);

          //---- OBJ_86 ----
          OBJ_86.setText("");
          OBJ_86.setToolTipText("Calendrier");
          OBJ_86.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_86.setName("OBJ_86");
          OBJ_86.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_86ActionPerformed(e);
            }
          });
          P_PnlOpts.add(OBJ_86);
          OBJ_86.setBounds(0, 0, 40, 40);

          //---- OBJ_128 ----
          OBJ_128.setText("");
          OBJ_128.setToolTipText("Calendrier");
          OBJ_128.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_128.setName("OBJ_128");
          OBJ_128.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_128ActionPerformed(e);
            }
          });
          P_PnlOpts.add(OBJ_128);
          OBJ_128.setBounds(0, 80, 40, 40);

          //---- OBJ_205 ----
          OBJ_205.setText("");
          OBJ_205.setToolTipText("@&L000079@");
          OBJ_205.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_205.setName("OBJ_205");
          OBJ_205.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_205ActionPerformed(e);
            }
          });
          P_PnlOpts.add(OBJ_205);
          OBJ_205.setBounds(0, 40, 40, 40);
        }
        panel2.add(P_PnlOpts);
        P_PnlOpts.setBounds(5, 5, 40, 516);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(panel2, BorderLayout.EAST);

      //======== panel3 ========
      {
        panel3.setName("panel3");

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- CLNOM ----
          CLNOM.setComponentPopupMenu(BTDA);
          CLNOM.setName("CLNOM");
          xTitledPanel1ContentContainer.add(CLNOM);
          CLNOM.setBounds(95, 15, 310, CLNOM.getPreferredSize().height);

          //---- CLCPL ----
          CLCPL.setComponentPopupMenu(BTDA);
          CLCPL.setName("CLCPL");
          xTitledPanel1ContentContainer.add(CLCPL);
          CLCPL.setBounds(95, 45, 310, CLCPL.getPreferredSize().height);

          //---- CLRUE ----
          CLRUE.setComponentPopupMenu(BTDA);
          CLRUE.setName("CLRUE");
          xTitledPanel1ContentContainer.add(CLRUE);
          CLRUE.setBounds(95, 75, 310, CLRUE.getPreferredSize().height);

          //---- CLLOC ----
          CLLOC.setComponentPopupMenu(BTDA);
          CLLOC.setName("CLLOC");
          xTitledPanel1ContentContainer.add(CLLOC);
          CLLOC.setBounds(95, 105, 310, CLLOC.getPreferredSize().height);

          //---- LTOBS ----
          LTOBS.setComponentPopupMenu(BTDA);
          LTOBS.setName("LTOBS");
          xTitledPanel1ContentContainer.add(LTOBS);
          LTOBS.setBounds(95, 165, 310, LTOBS.getPreferredSize().height);

          //---- CLVILR ----
          CLVILR.setComponentPopupMenu(BTDA);
          CLVILR.setName("CLVILR");
          xTitledPanel1ContentContainer.add(CLVILR);
          CLVILR.setBounds(155, 135, 250, CLVILR.getPreferredSize().height);

          //---- OBJ_97 ----
          OBJ_97.setText("Nom");
          OBJ_97.setName("OBJ_97");
          xTitledPanel1ContentContainer.add(OBJ_97);
          OBJ_97.setBounds(20, 19, 70, 20);

          //---- OBJ_127 ----
          OBJ_127.setText("Adresse");
          OBJ_127.setName("OBJ_127");
          xTitledPanel1ContentContainer.add(OBJ_127);
          OBJ_127.setBounds(20, 79, 76, 20);

          //---- OBJ_144 ----
          OBJ_144.setText("Localit\u00e9");
          OBJ_144.setName("OBJ_144");
          xTitledPanel1ContentContainer.add(OBJ_144);
          OBJ_144.setBounds(20, 109, 76, 20);

          //---- OBJ_156 ----
          OBJ_156.setText("Code postal");
          OBJ_156.setName("OBJ_156");
          xTitledPanel1ContentContainer.add(OBJ_156);
          OBJ_156.setBounds(20, 139, 76, 20);

          //---- OBJ_191 ----
          OBJ_191.setText("Observation");
          OBJ_191.setName("OBJ_191");
          xTitledPanel1ContentContainer.add(OBJ_191);
          OBJ_191.setBounds(20, 170, 76, 18);

          //---- CLCDP ----
          CLCDP.setComponentPopupMenu(BTDA);
          CLCDP.setName("CLCDP");
          xTitledPanel1ContentContainer.add(CLCDP);
          CLCDP.setBounds(95, 135, 60, CLCDP.getPreferredSize().height);
        }

        //======== panel4 ========
        {
          panel4.setName("panel4");
          panel4.setLayout(null);

          //---- LTDD1X ----
          LTDD1X.setComponentPopupMenu(BTDA);
          LTDD1X.setName("LTDD1X");
          panel4.add(LTDD1X);
          LTDD1X.setBounds(110, 15, 115, LTDD1X.getPreferredSize().height);

          //---- LTDF1X ----
          LTDF1X.setComponentPopupMenu(BTDA);
          LTDF1X.setName("LTDF1X");
          panel4.add(LTDF1X);
          LTDF1X.setBounds(260, 15, 115, LTDF1X.getPreferredSize().height);

          //---- LTDD2X ----
          LTDD2X.setComponentPopupMenu(BTDA);
          LTDD2X.setName("LTDD2X");
          panel4.add(LTDD2X);
          LTDD2X.setBounds(110, 45, 115, LTDD2X.getPreferredSize().height);

          //---- LTDF2X ----
          LTDF2X.setComponentPopupMenu(BTDA);
          LTDF2X.setName("LTDF2X");
          panel4.add(LTDF2X);
          LTDF2X.setBounds(260, 45, 115, LTDF2X.getPreferredSize().height);

          //---- LTHD1H ----
          LTHD1H.setComponentPopupMenu(BTDA);
          LTHD1H.setName("LTHD1H");
          panel4.add(LTHD1H);
          LTHD1H.setBounds(110, 105, 26, LTHD1H.getPreferredSize().height);

          //---- LTHD1M ----
          LTHD1M.setComponentPopupMenu(BTDA);
          LTHD1M.setName("LTHD1M");
          panel4.add(LTHD1M);
          LTHD1M.setBounds(160, 105, 26, LTHD1M.getPreferredSize().height);

          //---- LTHF1H ----
          LTHF1H.setComponentPopupMenu(BTDA);
          LTHF1H.setName("LTHF1H");
          panel4.add(LTHF1H);
          LTHF1H.setBounds(205, 105, 26, LTHF1H.getPreferredSize().height);

          //---- LTHF1M ----
          LTHF1M.setComponentPopupMenu(BTDA);
          LTHF1M.setName("LTHF1M");
          panel4.add(LTHF1M);
          LTHF1M.setBounds(255, 105, 26, LTHF1M.getPreferredSize().height);

          //---- LTHD2H ----
          LTHD2H.setComponentPopupMenu(BTDA);
          LTHD2H.setName("LTHD2H");
          panel4.add(LTHD2H);
          LTHD2H.setBounds(110, 135, 26, LTHD2H.getPreferredSize().height);

          //---- LTHD2M ----
          LTHD2M.setComponentPopupMenu(BTDA);
          LTHD2M.setName("LTHD2M");
          panel4.add(LTHD2M);
          LTHD2M.setBounds(160, 135, 26, LTHD2M.getPreferredSize().height);

          //---- LTHF2H ----
          LTHF2H.setComponentPopupMenu(BTDA);
          LTHF2H.setName("LTHF2H");
          panel4.add(LTHF2H);
          LTHF2H.setBounds(205, 135, 26, LTHF2H.getPreferredSize().height);

          //---- LTHF2M ----
          LTHF2M.setComponentPopupMenu(BTDA);
          LTHF2M.setName("LTHF2M");
          panel4.add(LTHF2M);
          LTHF2M.setBounds(255, 135, 26, LTHF2M.getPreferredSize().height);

          //---- OBJ_210 ----
          OBJ_210.setText("de");
          OBJ_210.setName("OBJ_210");
          panel4.add(OBJ_210);
          OBJ_210.setBounds(35, 109, 19, 20);

          //---- OBJ_214 ----
          OBJ_214.setText("de");
          OBJ_214.setName("OBJ_214");
          panel4.add(OBJ_214);
          OBJ_214.setBounds(35, 139, 19, 20);

          //---- OBJ_206 ----
          OBJ_206.setText("du");
          OBJ_206.setName("OBJ_206");
          panel4.add(OBJ_206);
          OBJ_206.setBounds(35, 19, 18, 20);

          //---- OBJ_207 ----
          OBJ_207.setText("au");
          OBJ_207.setName("OBJ_207");
          panel4.add(OBJ_207);
          OBJ_207.setBounds(235, 20, 18, 20);

          //---- OBJ_208 ----
          OBJ_208.setText("du");
          OBJ_208.setName("OBJ_208");
          panel4.add(OBJ_208);
          OBJ_208.setBounds(35, 49, 18, 20);

          //---- OBJ_209 ----
          OBJ_209.setText("au");
          OBJ_209.setName("OBJ_209");
          panel4.add(OBJ_209);
          OBJ_209.setBounds(235, 50, 18, 20);

          //---- OBJ_211 ----
          OBJ_211.setText("H");
          OBJ_211.setName("OBJ_211");
          panel4.add(OBJ_211);
          OBJ_211.setBounds(140, 110, 13, 20);

          //---- OBJ_213 ----
          OBJ_213.setText("H");
          OBJ_213.setName("OBJ_213");
          panel4.add(OBJ_213);
          OBJ_213.setBounds(235, 110, 13, 20);

          //---- OBJ_215 ----
          OBJ_215.setText("H");
          OBJ_215.setName("OBJ_215");
          panel4.add(OBJ_215);
          OBJ_215.setBounds(145, 140, 13, 20);

          //---- OBJ_217 ----
          OBJ_217.setText("H");
          OBJ_217.setName("OBJ_217");
          panel4.add(OBJ_217);
          OBJ_217.setBounds(235, 140, 13, 20);

          //---- OBJ_212 ----
          OBJ_212.setText("\u00e0");
          OBJ_212.setName("OBJ_212");
          panel4.add(OBJ_212);
          OBJ_212.setBounds(190, 110, 11, 20);

          //---- OBJ_216 ----
          OBJ_216.setText("\u00e0");
          OBJ_216.setName("OBJ_216");
          panel4.add(OBJ_216);
          OBJ_216.setBounds(190, 140, 11, 20);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("Horaires");
          xTitledSeparator1.setName("xTitledSeparator1");
          panel4.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(20, 80, 400, xTitledSeparator1.getPreferredSize().height);
        }

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setTitle("@JOUR@");
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
          xTitledPanel3ContentContainer.setLayout(null);

          //---- OBJ_193 ----
          OBJ_193.setText("Dimanche");
          OBJ_193.setName("OBJ_193");
          xTitledPanel3ContentContainer.add(OBJ_193);
          OBJ_193.setBounds(20, 245, 64, 20);

          //---- OBJ_168 ----
          OBJ_168.setText("Vendredi");
          OBJ_168.setName("OBJ_168");
          xTitledPanel3ContentContainer.add(OBJ_168);
          OBJ_168.setBounds(20, 195, 58, 20);

          //---- OBJ_145 ----
          OBJ_145.setText("Mercredi");
          OBJ_145.setName("OBJ_145");
          xTitledPanel3ContentContainer.add(OBJ_145);
          OBJ_145.setBounds(20, 145, 56, 20);

          //---- OBJ_180 ----
          OBJ_180.setText("Samedi");
          OBJ_180.setName("OBJ_180");
          xTitledPanel3ContentContainer.add(OBJ_180);
          OBJ_180.setBounds(20, 220, 50, 20);

          //---- OBJ_129 ----
          OBJ_129.setText("Mardi");
          OBJ_129.setName("OBJ_129");
          xTitledPanel3ContentContainer.add(OBJ_129);
          OBJ_129.setBounds(20, 120, 37, 20);

          //---- TODP1X ----
          TODP1X.setComponentPopupMenu(BTDA);
          TODP1X.setName("TODP1X");
          xTitledPanel3ContentContainer.add(TODP1X);
          TODP1X.setBounds(50, 15, 36, TODP1X.getPreferredSize().height);

          //---- TODP2X ----
          TODP2X.setComponentPopupMenu(BTDA);
          TODP2X.setName("TODP2X");
          xTitledPanel3ContentContainer.add(TODP2X);
          TODP2X.setBounds(245, 15, 36, TODP2X.getPreferredSize().height);

          //---- TODP3X ----
          TODP3X.setComponentPopupMenu(BTDA);
          TODP3X.setName("TODP3X");
          xTitledPanel3ContentContainer.add(TODP3X);
          TODP3X.setBounds(50, 45, 36, TODP3X.getPreferredSize().height);

          //---- TODP4X ----
          TODP4X.setComponentPopupMenu(BTDA);
          TODP4X.setName("TODP4X");
          xTitledPanel3ContentContainer.add(TODP4X);
          TODP4X.setBounds(245, 45, 36, TODP4X.getPreferredSize().height);

          //---- OBJ_113 ----
          OBJ_113.setText("@TOFP1X@");
          OBJ_113.setName("OBJ_113");
          xTitledPanel3ContentContainer.add(OBJ_113);
          OBJ_113.setBounds(145, 19, 36, 20);

          //---- OBJ_115 ----
          OBJ_115.setText("@TOFP2X@");
          OBJ_115.setName("OBJ_115");
          xTitledPanel3ContentContainer.add(OBJ_115);
          OBJ_115.setBounds(320, 19, 36, 20);

          //---- OBJ_141 ----
          OBJ_141.setText("@TOFP3X@");
          OBJ_141.setName("OBJ_141");
          xTitledPanel3ContentContainer.add(OBJ_141);
          OBJ_141.setBounds(145, 49, 36, 20);

          //---- OBJ_143 ----
          OBJ_143.setText("@TOFP4X@");
          OBJ_143.setName("OBJ_143");
          xTitledPanel3ContentContainer.add(OBJ_143);
          OBJ_143.setBounds(320, 49, 36, 20);

          //---- OBJ_157 ----
          OBJ_157.setText("Jeudi");
          OBJ_157.setName("OBJ_157");
          xTitledPanel3ContentContainer.add(OBJ_157);
          OBJ_157.setBounds(20, 170, 36, 20);

          //---- OBJ_116 ----
          OBJ_116.setText("Lundi");
          OBJ_116.setName("OBJ_116");
          xTitledPanel3ContentContainer.add(OBJ_116);
          OBJ_116.setBounds(20, 95, 35, 20);

          //---- OBJ_93 ----
          OBJ_93.setText("du");
          OBJ_93.setName("OBJ_93");
          xTitledPanel3ContentContainer.add(OBJ_93);
          OBJ_93.setBounds(20, 19, 18, 20);

          //---- OBJ_95 ----
          OBJ_95.setText("du");
          OBJ_95.setName("OBJ_95");
          xTitledPanel3ContentContainer.add(OBJ_95);
          OBJ_95.setBounds(215, 19, 18, 20);

          //---- OBJ_98 ----
          OBJ_98.setText("du");
          OBJ_98.setName("OBJ_98");
          xTitledPanel3ContentContainer.add(OBJ_98);
          OBJ_98.setBounds(20, 49, 18, 20);

          //---- OBJ_100 ----
          OBJ_100.setText("du");
          OBJ_100.setName("OBJ_100");
          xTitledPanel3ContentContainer.add(OBJ_100);
          OBJ_100.setBounds(215, 49, 18, 20);

          //---- OBJ_112 ----
          OBJ_112.setText("au");
          OBJ_112.setName("OBJ_112");
          xTitledPanel3ContentContainer.add(OBJ_112);
          OBJ_112.setBounds(115, 19, 18, 20);

          //---- OBJ_114 ----
          OBJ_114.setText("au");
          OBJ_114.setName("OBJ_114");
          xTitledPanel3ContentContainer.add(OBJ_114);
          OBJ_114.setBounds(295, 19, 18, 20);

          //---- OBJ_140 ----
          OBJ_140.setText("au");
          OBJ_140.setName("OBJ_140");
          xTitledPanel3ContentContainer.add(OBJ_140);
          OBJ_140.setBounds(115, 49, 18, 20);

          //---- OBJ_142 ----
          OBJ_142.setText("au");
          OBJ_142.setName("OBJ_142");
          xTitledPanel3ContentContainer.add(OBJ_142);
          OBJ_142.setBounds(295, 49, 18, 20);

          //---- TOLU11 ----
          TOLU11.setText("");
          TOLU11.setComponentPopupMenu(BTDA);
          TOLU11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU11.setName("TOLU11");
          xTitledPanel3ContentContainer.add(TOLU11);
          TOLU11.setBounds(95, 95, 20, 20);

          //---- TOLU12 ----
          TOLU12.setText("");
          TOLU12.setComponentPopupMenu(BTDA);
          TOLU12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU12.setName("TOLU12");
          xTitledPanel3ContentContainer.add(TOLU12);
          TOLU12.setBounds(115, 95, 20, 20);

          //---- TOLU13 ----
          TOLU13.setText("");
          TOLU13.setComponentPopupMenu(BTDA);
          TOLU13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU13.setName("TOLU13");
          xTitledPanel3ContentContainer.add(TOLU13);
          TOLU13.setBounds(135, 95, 20, 20);

          //---- TOLU14 ----
          TOLU14.setText("");
          TOLU14.setComponentPopupMenu(BTDA);
          TOLU14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU14.setName("TOLU14");
          xTitledPanel3ContentContainer.add(TOLU14);
          TOLU14.setBounds(155, 95, 20, 20);

          //---- TOLU21 ----
          TOLU21.setText("");
          TOLU21.setComponentPopupMenu(BTDA);
          TOLU21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU21.setName("TOLU21");
          xTitledPanel3ContentContainer.add(TOLU21);
          TOLU21.setBounds(245, 95, 20, 20);

          //---- TOLU22 ----
          TOLU22.setText("");
          TOLU22.setComponentPopupMenu(BTDA);
          TOLU22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU22.setName("TOLU22");
          xTitledPanel3ContentContainer.add(TOLU22);
          TOLU22.setBounds(265, 95, 20, 20);

          //---- TOLU23 ----
          TOLU23.setText("");
          TOLU23.setComponentPopupMenu(BTDA);
          TOLU23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU23.setName("TOLU23");
          xTitledPanel3ContentContainer.add(TOLU23);
          TOLU23.setBounds(285, 95, 20, 20);

          //---- TOLU24 ----
          TOLU24.setText("");
          TOLU24.setComponentPopupMenu(BTDA);
          TOLU24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU24.setName("TOLU24");
          xTitledPanel3ContentContainer.add(TOLU24);
          TOLU24.setBounds(305, 95, 20, 20);

          //---- TOLU25 ----
          TOLU25.setText("");
          TOLU25.setComponentPopupMenu(BTDA);
          TOLU25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU25.setName("TOLU25");
          xTitledPanel3ContentContainer.add(TOLU25);
          TOLU25.setBounds(325, 95, 20, 20);

          //---- TOMA11 ----
          TOMA11.setText("");
          TOMA11.setComponentPopupMenu(BTDA);
          TOMA11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA11.setName("TOMA11");
          xTitledPanel3ContentContainer.add(TOMA11);
          TOMA11.setBounds(95, 120, 20, 20);

          //---- TOMA12 ----
          TOMA12.setText("");
          TOMA12.setComponentPopupMenu(BTDA);
          TOMA12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA12.setName("TOMA12");
          xTitledPanel3ContentContainer.add(TOMA12);
          TOMA12.setBounds(115, 120, 20, 20);

          //---- TOMA13 ----
          TOMA13.setText("");
          TOMA13.setComponentPopupMenu(BTDA);
          TOMA13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA13.setName("TOMA13");
          xTitledPanel3ContentContainer.add(TOMA13);
          TOMA13.setBounds(135, 120, 20, 20);

          //---- TOMA14 ----
          TOMA14.setText("");
          TOMA14.setComponentPopupMenu(BTDA);
          TOMA14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA14.setName("TOMA14");
          xTitledPanel3ContentContainer.add(TOMA14);
          TOMA14.setBounds(155, 120, 20, 20);

          //---- TOMA15 ----
          TOMA15.setText("");
          TOMA15.setComponentPopupMenu(BTDA);
          TOMA15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA15.setName("TOMA15");
          xTitledPanel3ContentContainer.add(TOMA15);
          TOMA15.setBounds(175, 120, 20, 20);

          //---- TOMA21 ----
          TOMA21.setText("");
          TOMA21.setComponentPopupMenu(BTDA);
          TOMA21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA21.setName("TOMA21");
          xTitledPanel3ContentContainer.add(TOMA21);
          TOMA21.setBounds(245, 120, 20, 20);

          //---- TOMA22 ----
          TOMA22.setText("");
          TOMA22.setComponentPopupMenu(BTDA);
          TOMA22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA22.setName("TOMA22");
          xTitledPanel3ContentContainer.add(TOMA22);
          TOMA22.setBounds(265, 120, 20, 20);

          //---- TOMA23 ----
          TOMA23.setText("");
          TOMA23.setComponentPopupMenu(BTDA);
          TOMA23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA23.setName("TOMA23");
          xTitledPanel3ContentContainer.add(TOMA23);
          TOMA23.setBounds(285, 120, 20, 20);

          //---- TOMA24 ----
          TOMA24.setText("");
          TOMA24.setComponentPopupMenu(BTDA);
          TOMA24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA24.setName("TOMA24");
          xTitledPanel3ContentContainer.add(TOMA24);
          TOMA24.setBounds(305, 120, 20, 20);

          //---- TOMA25 ----
          TOMA25.setText("");
          TOMA25.setComponentPopupMenu(BTDA);
          TOMA25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOMA25.setName("TOMA25");
          xTitledPanel3ContentContainer.add(TOMA25);
          TOMA25.setBounds(325, 120, 20, 20);

          //---- TOME11 ----
          TOME11.setText("");
          TOME11.setComponentPopupMenu(BTDA);
          TOME11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME11.setName("TOME11");
          xTitledPanel3ContentContainer.add(TOME11);
          TOME11.setBounds(95, 145, 20, 20);

          //---- TOME12 ----
          TOME12.setText("");
          TOME12.setComponentPopupMenu(BTDA);
          TOME12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME12.setName("TOME12");
          xTitledPanel3ContentContainer.add(TOME12);
          TOME12.setBounds(115, 145, 20, 20);

          //---- TOME13 ----
          TOME13.setText("");
          TOME13.setComponentPopupMenu(BTDA);
          TOME13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME13.setName("TOME13");
          xTitledPanel3ContentContainer.add(TOME13);
          TOME13.setBounds(135, 145, 20, 20);

          //---- TOME14 ----
          TOME14.setText("");
          TOME14.setComponentPopupMenu(BTDA);
          TOME14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME14.setName("TOME14");
          xTitledPanel3ContentContainer.add(TOME14);
          TOME14.setBounds(155, 145, 20, 20);

          //---- TOME15 ----
          TOME15.setText("");
          TOME15.setComponentPopupMenu(BTDA);
          TOME15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME15.setName("TOME15");
          xTitledPanel3ContentContainer.add(TOME15);
          TOME15.setBounds(175, 145, 20, 20);

          //---- TOME21 ----
          TOME21.setText("");
          TOME21.setComponentPopupMenu(BTDA);
          TOME21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME21.setName("TOME21");
          xTitledPanel3ContentContainer.add(TOME21);
          TOME21.setBounds(245, 145, 20, 20);

          //---- TOME22 ----
          TOME22.setText("");
          TOME22.setComponentPopupMenu(BTDA);
          TOME22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME22.setName("TOME22");
          xTitledPanel3ContentContainer.add(TOME22);
          TOME22.setBounds(265, 145, 20, 20);

          //---- TOME23 ----
          TOME23.setText("");
          TOME23.setComponentPopupMenu(BTDA);
          TOME23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME23.setName("TOME23");
          xTitledPanel3ContentContainer.add(TOME23);
          TOME23.setBounds(285, 145, 20, 20);

          //---- TOME24 ----
          TOME24.setText("");
          TOME24.setComponentPopupMenu(BTDA);
          TOME24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME24.setName("TOME24");
          xTitledPanel3ContentContainer.add(TOME24);
          TOME24.setBounds(305, 145, 20, 20);

          //---- TOME25 ----
          TOME25.setText("");
          TOME25.setComponentPopupMenu(BTDA);
          TOME25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOME25.setName("TOME25");
          xTitledPanel3ContentContainer.add(TOME25);
          TOME25.setBounds(325, 145, 20, 20);

          //---- TOJE11 ----
          TOJE11.setText("");
          TOJE11.setComponentPopupMenu(BTDA);
          TOJE11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE11.setName("TOJE11");
          xTitledPanel3ContentContainer.add(TOJE11);
          TOJE11.setBounds(95, 170, 20, 20);

          //---- TOJE12 ----
          TOJE12.setText("");
          TOJE12.setComponentPopupMenu(BTDA);
          TOJE12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE12.setName("TOJE12");
          xTitledPanel3ContentContainer.add(TOJE12);
          TOJE12.setBounds(115, 170, 20, 20);

          //---- TOJE13 ----
          TOJE13.setText("");
          TOJE13.setComponentPopupMenu(BTDA);
          TOJE13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE13.setName("TOJE13");
          xTitledPanel3ContentContainer.add(TOJE13);
          TOJE13.setBounds(135, 170, 20, 20);

          //---- TOJE14 ----
          TOJE14.setText("");
          TOJE14.setComponentPopupMenu(BTDA);
          TOJE14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE14.setName("TOJE14");
          xTitledPanel3ContentContainer.add(TOJE14);
          TOJE14.setBounds(155, 170, 20, 20);

          //---- TOJE15 ----
          TOJE15.setText("");
          TOJE15.setComponentPopupMenu(BTDA);
          TOJE15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE15.setName("TOJE15");
          xTitledPanel3ContentContainer.add(TOJE15);
          TOJE15.setBounds(175, 170, 20, 20);

          //---- TOJE21 ----
          TOJE21.setText("");
          TOJE21.setComponentPopupMenu(BTDA);
          TOJE21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE21.setName("TOJE21");
          xTitledPanel3ContentContainer.add(TOJE21);
          TOJE21.setBounds(245, 170, 20, 20);

          //---- TOJE22 ----
          TOJE22.setText("");
          TOJE22.setComponentPopupMenu(BTDA);
          TOJE22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE22.setName("TOJE22");
          xTitledPanel3ContentContainer.add(TOJE22);
          TOJE22.setBounds(265, 170, 20, 20);

          //---- TOJE23 ----
          TOJE23.setText("");
          TOJE23.setComponentPopupMenu(BTDA);
          TOJE23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE23.setName("TOJE23");
          xTitledPanel3ContentContainer.add(TOJE23);
          TOJE23.setBounds(285, 170, 20, 20);

          //---- TOJE24 ----
          TOJE24.setText("");
          TOJE24.setComponentPopupMenu(BTDA);
          TOJE24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE24.setName("TOJE24");
          xTitledPanel3ContentContainer.add(TOJE24);
          TOJE24.setBounds(305, 170, 20, 20);

          //---- TOJE25 ----
          TOJE25.setText("");
          TOJE25.setComponentPopupMenu(BTDA);
          TOJE25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOJE25.setName("TOJE25");
          xTitledPanel3ContentContainer.add(TOJE25);
          TOJE25.setBounds(325, 170, 20, 20);

          //---- TOVE11 ----
          TOVE11.setText("");
          TOVE11.setComponentPopupMenu(BTDA);
          TOVE11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE11.setName("TOVE11");
          xTitledPanel3ContentContainer.add(TOVE11);
          TOVE11.setBounds(95, 195, 20, 20);

          //---- TOVE12 ----
          TOVE12.setText("");
          TOVE12.setComponentPopupMenu(BTDA);
          TOVE12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE12.setName("TOVE12");
          xTitledPanel3ContentContainer.add(TOVE12);
          TOVE12.setBounds(115, 195, 20, 20);

          //---- TOVE13 ----
          TOVE13.setText("");
          TOVE13.setComponentPopupMenu(BTDA);
          TOVE13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE13.setName("TOVE13");
          xTitledPanel3ContentContainer.add(TOVE13);
          TOVE13.setBounds(135, 195, 20, 20);

          //---- TOVE14 ----
          TOVE14.setText("");
          TOVE14.setComponentPopupMenu(BTDA);
          TOVE14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE14.setName("TOVE14");
          xTitledPanel3ContentContainer.add(TOVE14);
          TOVE14.setBounds(155, 195, 20, 20);

          //---- TOVE15 ----
          TOVE15.setText("");
          TOVE15.setComponentPopupMenu(BTDA);
          TOVE15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE15.setName("TOVE15");
          xTitledPanel3ContentContainer.add(TOVE15);
          TOVE15.setBounds(175, 195, 20, 20);

          //---- TOVE21 ----
          TOVE21.setText("");
          TOVE21.setComponentPopupMenu(BTDA);
          TOVE21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE21.setName("TOVE21");
          xTitledPanel3ContentContainer.add(TOVE21);
          TOVE21.setBounds(245, 195, 20, 20);

          //---- TOVE22 ----
          TOVE22.setText("");
          TOVE22.setComponentPopupMenu(BTDA);
          TOVE22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE22.setName("TOVE22");
          xTitledPanel3ContentContainer.add(TOVE22);
          TOVE22.setBounds(265, 195, 20, 20);

          //---- TOVE23 ----
          TOVE23.setText("");
          TOVE23.setComponentPopupMenu(BTDA);
          TOVE23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE23.setName("TOVE23");
          xTitledPanel3ContentContainer.add(TOVE23);
          TOVE23.setBounds(285, 195, 20, 20);

          //---- TOVE24 ----
          TOVE24.setText("");
          TOVE24.setComponentPopupMenu(BTDA);
          TOVE24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE24.setName("TOVE24");
          xTitledPanel3ContentContainer.add(TOVE24);
          TOVE24.setBounds(305, 195, 20, 20);

          //---- TOVE25 ----
          TOVE25.setText("");
          TOVE25.setComponentPopupMenu(BTDA);
          TOVE25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOVE25.setName("TOVE25");
          xTitledPanel3ContentContainer.add(TOVE25);
          TOVE25.setBounds(325, 195, 20, 20);

          //---- TOSA11 ----
          TOSA11.setText("");
          TOSA11.setComponentPopupMenu(BTDA);
          TOSA11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA11.setName("TOSA11");
          xTitledPanel3ContentContainer.add(TOSA11);
          TOSA11.setBounds(95, 220, 20, 20);

          //---- TOSA12 ----
          TOSA12.setText("");
          TOSA12.setComponentPopupMenu(BTDA);
          TOSA12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA12.setName("TOSA12");
          xTitledPanel3ContentContainer.add(TOSA12);
          TOSA12.setBounds(115, 220, 20, 20);

          //---- TOSA13 ----
          TOSA13.setText("");
          TOSA13.setComponentPopupMenu(BTDA);
          TOSA13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA13.setName("TOSA13");
          xTitledPanel3ContentContainer.add(TOSA13);
          TOSA13.setBounds(135, 220, 20, 20);

          //---- TOSA14 ----
          TOSA14.setText("");
          TOSA14.setComponentPopupMenu(BTDA);
          TOSA14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA14.setName("TOSA14");
          xTitledPanel3ContentContainer.add(TOSA14);
          TOSA14.setBounds(155, 220, 20, 20);

          //---- TOSA15 ----
          TOSA15.setText("");
          TOSA15.setComponentPopupMenu(BTDA);
          TOSA15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA15.setName("TOSA15");
          xTitledPanel3ContentContainer.add(TOSA15);
          TOSA15.setBounds(175, 220, 20, 20);

          //---- TOSA21 ----
          TOSA21.setText("");
          TOSA21.setComponentPopupMenu(BTDA);
          TOSA21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA21.setName("TOSA21");
          xTitledPanel3ContentContainer.add(TOSA21);
          TOSA21.setBounds(245, 220, 20, 20);

          //---- TOSA22 ----
          TOSA22.setText("");
          TOSA22.setComponentPopupMenu(BTDA);
          TOSA22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA22.setName("TOSA22");
          xTitledPanel3ContentContainer.add(TOSA22);
          TOSA22.setBounds(265, 220, 20, 20);

          //---- TOSA23 ----
          TOSA23.setText("");
          TOSA23.setComponentPopupMenu(BTDA);
          TOSA23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA23.setName("TOSA23");
          xTitledPanel3ContentContainer.add(TOSA23);
          TOSA23.setBounds(285, 220, 20, 20);

          //---- TOSA24 ----
          TOSA24.setText("");
          TOSA24.setComponentPopupMenu(BTDA);
          TOSA24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA24.setName("TOSA24");
          xTitledPanel3ContentContainer.add(TOSA24);
          TOSA24.setBounds(305, 220, 20, 20);

          //---- TOSA25 ----
          TOSA25.setText("");
          TOSA25.setComponentPopupMenu(BTDA);
          TOSA25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOSA25.setName("TOSA25");
          xTitledPanel3ContentContainer.add(TOSA25);
          TOSA25.setBounds(325, 220, 20, 20);

          //---- TODI11 ----
          TODI11.setText("");
          TODI11.setComponentPopupMenu(BTDA);
          TODI11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI11.setName("TODI11");
          xTitledPanel3ContentContainer.add(TODI11);
          TODI11.setBounds(95, 245, 20, 20);

          //---- TODI12 ----
          TODI12.setText("");
          TODI12.setComponentPopupMenu(BTDA);
          TODI12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI12.setName("TODI12");
          xTitledPanel3ContentContainer.add(TODI12);
          TODI12.setBounds(115, 245, 20, 20);

          //---- TODI13 ----
          TODI13.setText("");
          TODI13.setComponentPopupMenu(BTDA);
          TODI13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI13.setName("TODI13");
          xTitledPanel3ContentContainer.add(TODI13);
          TODI13.setBounds(135, 245, 20, 20);

          //---- TODI14 ----
          TODI14.setText("");
          TODI14.setComponentPopupMenu(BTDA);
          TODI14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI14.setName("TODI14");
          xTitledPanel3ContentContainer.add(TODI14);
          TODI14.setBounds(155, 245, 20, 20);

          //---- TODI15 ----
          TODI15.setText("");
          TODI15.setComponentPopupMenu(BTDA);
          TODI15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI15.setName("TODI15");
          xTitledPanel3ContentContainer.add(TODI15);
          TODI15.setBounds(175, 245, 20, 20);

          //---- TODI21 ----
          TODI21.setText("");
          TODI21.setComponentPopupMenu(BTDA);
          TODI21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI21.setName("TODI21");
          xTitledPanel3ContentContainer.add(TODI21);
          TODI21.setBounds(245, 245, 20, 20);

          //---- TODI22 ----
          TODI22.setText("");
          TODI22.setComponentPopupMenu(BTDA);
          TODI22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI22.setName("TODI22");
          xTitledPanel3ContentContainer.add(TODI22);
          TODI22.setBounds(265, 245, 20, 20);

          //---- TODI23 ----
          TODI23.setText("");
          TODI23.setComponentPopupMenu(BTDA);
          TODI23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI23.setName("TODI23");
          xTitledPanel3ContentContainer.add(TODI23);
          TODI23.setBounds(285, 245, 20, 20);

          //---- TODI24 ----
          TODI24.setText("");
          TODI24.setComponentPopupMenu(BTDA);
          TODI24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI24.setName("TODI24");
          xTitledPanel3ContentContainer.add(TODI24);
          TODI24.setBounds(305, 245, 20, 20);

          //---- TODI25 ----
          TODI25.setText("");
          TODI25.setComponentPopupMenu(BTDA);
          TODI25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TODI25.setName("TODI25");
          xTitledPanel3ContentContainer.add(TODI25);
          TODI25.setBounds(325, 245, 20, 20);

          //---- TOLU15 ----
          TOLU15.setText("");
          TOLU15.setComponentPopupMenu(BTDA);
          TOLU15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOLU15.setName("TOLU15");
          xTitledPanel3ContentContainer.add(TOLU15);
          TOLU15.setBounds(175, 95, 20, 20);

          //---- OBJ_102 ----
          OBJ_102.setText("1");
          OBJ_102.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_102.setName("OBJ_102");
          xTitledPanel3ContentContainer.add(OBJ_102);
          OBJ_102.setBounds(95, 80, 17, 14);

          //---- OBJ_103 ----
          OBJ_103.setText("2");
          OBJ_103.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_103.setName("OBJ_103");
          xTitledPanel3ContentContainer.add(OBJ_103);
          OBJ_103.setBounds(115, 80, 17, 14);

          //---- OBJ_104 ----
          OBJ_104.setText("3");
          OBJ_104.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_104.setName("OBJ_104");
          xTitledPanel3ContentContainer.add(OBJ_104);
          OBJ_104.setBounds(135, 80, 17, 14);

          //---- OBJ_105 ----
          OBJ_105.setText("4");
          OBJ_105.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_105.setName("OBJ_105");
          xTitledPanel3ContentContainer.add(OBJ_105);
          OBJ_105.setBounds(155, 80, 17, 14);

          //---- OBJ_107 ----
          OBJ_107.setText("1");
          OBJ_107.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_107.setName("OBJ_107");
          xTitledPanel3ContentContainer.add(OBJ_107);
          OBJ_107.setBounds(245, 80, 17, 14);

          //---- OBJ_108 ----
          OBJ_108.setText("2");
          OBJ_108.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_108.setName("OBJ_108");
          xTitledPanel3ContentContainer.add(OBJ_108);
          OBJ_108.setBounds(265, 80, 17, 14);

          //---- OBJ_109 ----
          OBJ_109.setText("3");
          OBJ_109.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_109.setName("OBJ_109");
          xTitledPanel3ContentContainer.add(OBJ_109);
          OBJ_109.setBounds(285, 80, 17, 14);

          //---- OBJ_110 ----
          OBJ_110.setText("4");
          OBJ_110.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_110.setName("OBJ_110");
          xTitledPanel3ContentContainer.add(OBJ_110);
          OBJ_110.setBounds(305, 80, 17, 14);

          //---- OBJ_111 ----
          OBJ_111.setText("5");
          OBJ_111.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_111.setName("OBJ_111");
          xTitledPanel3ContentContainer.add(OBJ_111);
          OBJ_111.setBounds(325, 80, 17, 14);

          //---- OBJ_106 ----
          OBJ_106.setText("5");
          OBJ_106.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_106.setName("OBJ_106");
          xTitledPanel3ContentContainer.add(OBJ_106);
          OBJ_106.setBounds(176, 80, 13, 14);
        }

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //======== panel6 ========
          {
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_204 ----
            OBJ_204.setText("Prochaine tourn\u00e9e le");
            OBJ_204.setName("OBJ_204");
            panel6.add(OBJ_204);
            OBJ_204.setBounds(10, 44, 126, 20);

            //---- OBJ_30 ----
            OBJ_30.setText("@APPE2@");
            OBJ_30.setName("OBJ_30");
            panel6.add(OBJ_30);
            OBJ_30.setBounds(10, 14, 76, 20);

            //---- CTDATX ----
            CTDATX.setComponentPopupMenu(BTDA);
            CTDATX.setName("CTDATX");
            panel6.add(CTDATX);
            CTDATX.setBounds(135, 40, 115, CTDATX.getPreferredSize().height);

            //---- OBJ_220 ----
            OBJ_220.setText("Ordre");
            OBJ_220.setName("OBJ_220");
            panel6.add(OBJ_220);
            OBJ_220.setBounds(10, 14, 42, 20);

            //---- LTORT1 ----
            LTORT1.setComponentPopupMenu(BTDA);
            LTORT1.setName("LTORT1");
            panel6.add(LTORT1);
            LTORT1.setBounds(85, 10, 50, LTORT1.getPreferredSize().height);

            //---- LTPHR ----
            LTPHR.setComponentPopupMenu(BTD);
            LTPHR.setName("LTPHR");
            panel6.add(LTPHR);
            LTPHR.setBounds(85, 10, 26, LTPHR.getPreferredSize().height);

            //---- LTPMN ----
            LTPMN.setComponentPopupMenu(BTD);
            LTPMN.setName("LTPMN");
            panel6.add(LTPMN);
            LTPMN.setBounds(135, 10, 26, LTPMN.getPreferredSize().height);

            //---- OBJ_179 ----
            OBJ_179.setText("H");
            OBJ_179.setName("OBJ_179");
            panel6.add(OBJ_179);
            OBJ_179.setBounds(125, 14, 13, 20);

            //---- OBJ_192 ----
            OBJ_192.setText("Jours exclus");
            OBJ_192.setName("OBJ_192");
            panel6.add(OBJ_192);
            OBJ_192.setBounds(10, 75, 77, 20);

            //---- LTIN1 ----
            LTIN1.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Lundi",
              "Mardi",
              "Mercredi",
              "Jeudi",
              "Vendredi",
              "Samedi",
              "Dimanche"
            }));
            LTIN1.setComponentPopupMenu(BTD);
            LTIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LTIN1.setName("LTIN1");
            panel6.add(LTIN1);
            LTIN1.setBounds(85, 72, 100, LTIN1.getPreferredSize().height);

            //---- LTIN2 ----
            LTIN2.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Lundi",
              "Mardi",
              "Mercredi",
              "Jeudi",
              "Vendredi",
              "Samedi",
              "Dimanche"
            }));
            LTIN2.setComponentPopupMenu(BTD);
            LTIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LTIN2.setName("LTIN2");
            panel6.add(LTIN2);
            LTIN2.setBounds(202, 72, 100, LTIN2.getPreferredSize().height);
          }
          xTitledPanel2ContentContainer.add(panel6);
          panel6.setBounds(10, 10, 305, 110);
        }

        GroupLayout panel3Layout = new GroupLayout(panel3);
        panel3.setLayout(panel3Layout);
        panel3Layout.setHorizontalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 425, GroupLayout.PREFERRED_SIZE))
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
              .addGap(10, 10, 10)
              .addGroup(panel3Layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 375, Short.MAX_VALUE))
              .addGap(50, 50, 50))
        );
        panel3Layout.setVerticalGroup(
          panel3Layout.createParallelGroup()
            .addGroup(panel3Layout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))))
        );
      }
      P_Centre.add(panel3, BorderLayout.CENTER);
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);

    //---- V06FO ----
    V06FO.setComponentPopupMenu(BTD);
    V06FO.setName("V06FO");

    //---- OBJ_222 ----
    OBJ_222.setText("Fermeture");
    OBJ_222.setName("OBJ_222");

    //======== panel5 ========
    {
      panel5.setName("panel5");
      panel5.setLayout(null);

      //---- LTTOU1 ----
      LTTOU1.setComponentPopupMenu(BTD);
      LTTOU1.setName("LTTOU1");
      panel5.add(LTTOU1);
      LTTOU1.setBounds(75, 0, 60, LTTOU1.getPreferredSize().height);

      //---- label1 ----
      label1.setText("Tourn\u00e9e");
      label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
      label1.setName("label1");
      panel5.add(label1);
      label1.setBounds(10, 0, 55, 28);

      //---- OBJ_224 ----
      OBJ_224.setText("@APPE@");
      OBJ_224.setName("OBJ_224");
      panel5.add(OBJ_224);
      OBJ_224.setBounds(140, 5, 200, 17);

      panel5.setPreferredSize(new Dimension(355, 30));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu CMD;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private XRiTextField INDCLI;
  private JLabel OBJ_38;
  private XRiTextField INDETB;
  private XRiTextField INDLIV;
  private XRiTextField INDMEX;
  private JLabel OBJ_37;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JButton bt_Fonctions;
  private JLabel OBJ_42;
  private JPanel P_Centre;
  private JPanel panel2;
  private JPanel P_PnlOpts;
  private JButton OBJ_86;
  private JButton OBJ_128;
  private JButton OBJ_205;
  private JPanel panel3;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField CLNOM;
  private XRiTextField CLCPL;
  private XRiTextField CLRUE;
  private XRiTextField CLLOC;
  private XRiTextField LTOBS;
  private XRiTextField CLVILR;
  private JLabel OBJ_97;
  private JLabel OBJ_127;
  private JLabel OBJ_144;
  private JLabel OBJ_156;
  private JLabel OBJ_191;
  private XRiTextField CLCDP;
  private JPanel panel4;
  private XRiCalendrier LTDD1X;
  private XRiCalendrier LTDF1X;
  private XRiCalendrier LTDD2X;
  private XRiCalendrier LTDF2X;
  private XRiTextField LTHD1H;
  private XRiTextField LTHD1M;
  private XRiTextField LTHF1H;
  private XRiTextField LTHF1M;
  private XRiTextField LTHD2H;
  private XRiTextField LTHD2M;
  private XRiTextField LTHF2H;
  private XRiTextField LTHF2M;
  private JLabel OBJ_210;
  private JLabel OBJ_214;
  private JLabel OBJ_206;
  private JLabel OBJ_207;
  private JLabel OBJ_208;
  private JLabel OBJ_209;
  private JLabel OBJ_211;
  private JLabel OBJ_213;
  private JLabel OBJ_215;
  private JLabel OBJ_217;
  private JLabel OBJ_212;
  private JLabel OBJ_216;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_193;
  private JLabel OBJ_168;
  private JLabel OBJ_145;
  private JLabel OBJ_180;
  private JLabel OBJ_129;
  private XRiTextField TODP1X;
  private XRiTextField TODP2X;
  private XRiTextField TODP3X;
  private XRiTextField TODP4X;
  private JLabel OBJ_113;
  private JLabel OBJ_115;
  private JLabel OBJ_141;
  private JLabel OBJ_143;
  private JLabel OBJ_157;
  private JLabel OBJ_116;
  private JLabel OBJ_93;
  private JLabel OBJ_95;
  private JLabel OBJ_98;
  private JLabel OBJ_100;
  private JLabel OBJ_112;
  private JLabel OBJ_114;
  private JLabel OBJ_140;
  private JLabel OBJ_142;
  private XRiCheckBox TOLU11;
  private XRiCheckBox TOLU12;
  private XRiCheckBox TOLU13;
  private XRiCheckBox TOLU14;
  private XRiCheckBox TOLU21;
  private XRiCheckBox TOLU22;
  private XRiCheckBox TOLU23;
  private XRiCheckBox TOLU24;
  private XRiCheckBox TOLU25;
  private XRiCheckBox TOMA11;
  private XRiCheckBox TOMA12;
  private XRiCheckBox TOMA13;
  private XRiCheckBox TOMA14;
  private XRiCheckBox TOMA15;
  private XRiCheckBox TOMA21;
  private XRiCheckBox TOMA22;
  private XRiCheckBox TOMA23;
  private XRiCheckBox TOMA24;
  private XRiCheckBox TOMA25;
  private XRiCheckBox TOME11;
  private XRiCheckBox TOME12;
  private XRiCheckBox TOME13;
  private XRiCheckBox TOME14;
  private XRiCheckBox TOME15;
  private XRiCheckBox TOME21;
  private XRiCheckBox TOME22;
  private XRiCheckBox TOME23;
  private XRiCheckBox TOME24;
  private XRiCheckBox TOME25;
  private XRiCheckBox TOJE11;
  private XRiCheckBox TOJE12;
  private XRiCheckBox TOJE13;
  private XRiCheckBox TOJE14;
  private XRiCheckBox TOJE15;
  private XRiCheckBox TOJE21;
  private XRiCheckBox TOJE22;
  private XRiCheckBox TOJE23;
  private XRiCheckBox TOJE24;
  private XRiCheckBox TOJE25;
  private XRiCheckBox TOVE11;
  private XRiCheckBox TOVE12;
  private XRiCheckBox TOVE13;
  private XRiCheckBox TOVE14;
  private XRiCheckBox TOVE15;
  private XRiCheckBox TOVE21;
  private XRiCheckBox TOVE22;
  private XRiCheckBox TOVE23;
  private XRiCheckBox TOVE24;
  private XRiCheckBox TOVE25;
  private XRiCheckBox TOSA11;
  private XRiCheckBox TOSA12;
  private XRiCheckBox TOSA13;
  private XRiCheckBox TOSA14;
  private XRiCheckBox TOSA15;
  private XRiCheckBox TOSA21;
  private XRiCheckBox TOSA22;
  private XRiCheckBox TOSA23;
  private XRiCheckBox TOSA24;
  private XRiCheckBox TOSA25;
  private XRiCheckBox TODI11;
  private XRiCheckBox TODI12;
  private XRiCheckBox TODI13;
  private XRiCheckBox TODI14;
  private XRiCheckBox TODI15;
  private XRiCheckBox TODI21;
  private XRiCheckBox TODI22;
  private XRiCheckBox TODI23;
  private XRiCheckBox TODI24;
  private XRiCheckBox TODI25;
  private XRiCheckBox TOLU15;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private JLabel OBJ_105;
  private JLabel OBJ_107;
  private JLabel OBJ_108;
  private JLabel OBJ_109;
  private JLabel OBJ_110;
  private JLabel OBJ_111;
  private JLabel OBJ_106;
  private JXTitledPanel xTitledPanel2;
  private JPanel panel6;
  private JLabel OBJ_204;
  private JLabel OBJ_30;
  private XRiCalendrier CTDATX;
  private JLabel OBJ_220;
  private XRiTextField LTORT1;
  private XRiTextField LTPHR;
  private XRiTextField LTPMN;
  private JLabel OBJ_179;
  private JLabel OBJ_192;
  private XRiComboBox LTIN1;
  private XRiComboBox LTIN2;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  private XRiTextField V06FO;
  private JLabel OBJ_222;
  private JPanel panel5;
  private XRiTextField LTTOU1;
  private JLabel label1;
  private JLabel OBJ_224;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
