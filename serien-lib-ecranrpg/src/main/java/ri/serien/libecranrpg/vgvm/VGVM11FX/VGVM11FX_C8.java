
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_C8 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  public boolean isChantier;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_C8(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    L1ED5.setValeursSelection("X", " ");
    L1ED4.setValeursSelection("X", " ");
    L1ED3.setValeursSelection("X", " ");
    L1ED1.setValeursSelection("X", " ");
    L1ED2.setValeursSelection("X", " ");
    
    setCloseKey("F1", "F5");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    WCOD.setVisible(lexique.isPresent("WCOD"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    OBJ_35.setVisible(lexique.HostFieldGetData("V02F").equalsIgnoreCase("ERR"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_28 = new JXTitledSeparator();
    L1LIB1 = new XRiTextField();
    L1ED2 = new XRiCheckBox();
    L1LIB3 = new XRiTextField();
    OBJ_23 = new JLabel();
    WARTT = new RiZoneSortie();
    L1LB2A = new XRiTextField();
    L1ED1 = new XRiCheckBox();
    L1ED3 = new XRiCheckBox();
    L1ED4 = new XRiCheckBox();
    P_PnlOpts = new JPanel();
    OBJ_35 = new JButton();
    L1ED5 = new XRiCheckBox();
    OBJ_25 = new JLabel();
    OBJ_17 = new JLabel();
    OBJ_18 = new JLabel();
    L1LB22 = new XRiTextField();
    WNLI = new RiZoneSortie();
    WCOD = new RiZoneSortie();
    OBJ_51 = new JLabel();
    L1IN21 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(830, 305));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Saisie code barre");
            riSousMenu_bt6.setToolTipText("Mise en/hors fonction saisie code barre");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setTitle("A \u00e9diter sur");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(20, 110, 590, 20);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          p_recup.add(L1LIB1);
          L1LIB1.setBounds(285, 38, 310, L1LIB1.getPreferredSize().height);

          //---- L1ED2 ----
          L1ED2.setText("Accus\u00e9s de R\u00e9ception de Commande");
          L1ED2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED2.setName("L1ED2");
          p_recup.add(L1ED2);
          L1ED2.setBounds(215, 145, 253, 20);

          //---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setName("L1LIB3");
          p_recup.add(L1LIB3);
          L1LIB3.setBounds(25, 210, 310, L1LIB3.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Article Epuis\u00e9");
          OBJ_23.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(145, 74, 128, 20);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setText("@WARTT@");
          WARTT.setName("WARTT");
          p_recup.add(WARTT);
          WARTT.setBounds(89, 40, 190, WARTT.getPreferredSize().height);

          //---- L1LB2A ----
          L1LB2A.setComponentPopupMenu(BTD);
          L1LB2A.setName("L1LB2A");
          p_recup.add(L1LB2A);
          L1LB2A.setBounds(285, 70, 162, L1LB2A.getPreferredSize().height);

          //---- L1ED1 ----
          L1ED1.setText("Bons de Pr\u00e9paration");
          L1ED1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED1.setName("L1ED1");
          p_recup.add(L1ED1);
          L1ED1.setBounds(25, 145, 148, 20);

          //---- L1ED3 ----
          L1ED3.setText("Bons d'Exp\u00e9dition");
          L1ED3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED3.setName("L1ED3");
          p_recup.add(L1ED3);
          L1ED3.setBounds(25, 170, 148, 20);

          //---- L1ED4 ----
          L1ED4.setText("Bons Transporteurs");
          L1ED4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED4.setName("L1ED4");
          p_recup.add(L1ED4);
          L1ED4.setBounds(215, 170, 155, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);

            //---- OBJ_35 ----
            OBJ_35.setText("");
            OBJ_35.setToolTipText("D\u00e9tail erreur");
            OBJ_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_35.setName("OBJ_35");
            OBJ_35.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_35ActionPerformed(e);
              }
            });
            P_PnlOpts.add(OBJ_35);
            OBJ_35.setBounds(5, 6, 40, 40);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1036, 15, 55, 516);

          //---- L1ED5 ----
          L1ED5.setText("Factures");
          L1ED5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED5.setName("L1ED5");
          p_recup.add(L1ED5);
          L1ED5.setBounds(410, 170, 91, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Quantit\u00e9");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(485, 74, 65, 20);

          //---- OBJ_17 ----
          OBJ_17.setText("C   N\u00b0Li");
          OBJ_17.setName("OBJ_17");
          p_recup.add(OBJ_17);
          OBJ_17.setBounds(25, 15, 45, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Article");
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(80, 15, 42, 20);

          //---- L1LB22 ----
          L1LB22.setComponentPopupMenu(BTD);
          L1LB22.setName("L1LB22");
          p_recup.add(L1LB22);
          L1LB22.setBounds(545, 70, 50, L1LB22.getPreferredSize().height);

          //---- WNLI ----
          WNLI.setText("@WNLI@");
          WNLI.setName("WNLI");
          p_recup.add(WNLI);
          WNLI.setBounds(50, 40, 34, WNLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setText("@WCOD@");
          WCOD.setName("WCOD");
          p_recup.add(WCOD);
          WCOD.setBounds(25, 40, 20, WCOD.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Regroupement");
          OBJ_51.setHorizontalAlignment(SwingConstants.LEFT);
          OBJ_51.setName("OBJ_51");
          p_recup.add(OBJ_51);
          OBJ_51.setBounds(410, 214, 100, 20);

          //---- L1IN21 ----
          L1IN21.setComponentPopupMenu(BTD);
          L1IN21.setToolTipText("Code pour regroupement de lignes");
          L1IN21.setName("L1IN21");
          p_recup.add(L1IN21);
          L1IN21.setBounds(545, 210, 24, L1IN21.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JXTitledSeparator OBJ_28;
  private XRiTextField L1LIB1;
  private XRiCheckBox L1ED2;
  private XRiTextField L1LIB3;
  private JLabel OBJ_23;
  private RiZoneSortie WARTT;
  private XRiTextField L1LB2A;
  private XRiCheckBox L1ED1;
  private XRiCheckBox L1ED3;
  private XRiCheckBox L1ED4;
  private JPanel P_PnlOpts;
  private JButton OBJ_35;
  private XRiCheckBox L1ED5;
  private JLabel OBJ_25;
  private JLabel OBJ_17;
  private JLabel OBJ_18;
  private XRiTextField L1LB22;
  private RiZoneSortie WNLI;
  private RiZoneSortie WCOD;
  private JLabel OBJ_51;
  private XRiTextField L1IN21;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
