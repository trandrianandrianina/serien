
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_TR extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TRNBC_Value = { "", "1", "2", "3", "4", "5", };
  private String[] TRMCP_Value = { "", "1", "2", };
  private String[] TRPRO_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", };
  // private TR_PALETTES pal = null;
  private ODialog dialog_PALETTES = null;
  
  public VGVM01FX_TR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TRNBC.setValeurs(TRNBC_Value, null);
    TRPRO.setValeurs(TRPRO_Value, null);
    TRMCP.setValeurs(TRMCP_Value, null);
    TRCDE.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_55.setVisible(lexique.HostFieldGetData("TRMCP").equalsIgnoreCase("2"));
    
    if (lexique.isTrue("89")) {
      OBJ_80.setVisible(true);
      OBJ_82.setVisible(true);
      TRTRP.setVisible(true);
      TRPDS.setVisible(true);
      panel2.setVisible(false);
      OBJ_70.setVisible(false);
      OBJ_66.setVisible(false);
      TRTRF.setEnabled(false);
      TRMEX.setEnabled(false);
    }
    
    if (lexique.isTrue("41")) {
      OBJ_80.setVisible(false);
      OBJ_82.setVisible(false);
      TRTRP.setVisible(false);
      TRPDS.setVisible(false);
    }
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    if (dialog_PALETTES == null) {
      dialog_PALETTES = new ODialog((Window) getTopLevelAncestor(), new TR_PALETTES(this));
    }
    dialog_PALETTES.affichePopupPerso();
  }
  
  private void TRMCPItemStateChanged(ItemEvent e) {
    OBJ_55.setVisible(TRMCP.getSelectedIndex() == 2);
    lexique.HostFieldPutData("TRMCP", 0, TRMCP_Value[TRMCP.getSelectedIndex()]);
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel5 = new JXTitledPanel();
    panel6 = new JPanel();
    OBJ_63 = new JLabel();
    TRLIB = new XRiTextField();
    OBJ_56 = new JLabel();
    TRCCON = new XRiTextField();
    OBJ_58 = new JLabel();
    TRMCP = new XRiComboBox();
    OBJ_66 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_82 = new JLabel();
    TRIDE = new XRiTextField();
    OBJ_75 = new JLabel();
    OBJ_80 = new JLabel();
    TRFRS = new XRiTextField();
    TRCLI = new XRiTextField();
    TRPDS = new XRiTextField();
    TRZGEO = new XRiTextField();
    TRPFC = new XRiTextField();
    TRMEX = new XRiTextField();
    TRTRF = new XRiTextField();
    TRTRP = new XRiTextField();
    panel2 = new JPanel();
    OBJ_62 = new JLabel();
    TRPRO = new XRiComboBox();
    OBJ_64 = new JLabel();
    TRNBC = new XRiComboBox();
    panel1 = new JPanel();
    OBJ_86 = new JLabel();
    TRCDE = new XRiCheckBox();
    panel3 = new JPanel();
    OBJ_104 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_90 = new JLabel();
    TRPDSD = new XRiTextField();
    TRPDSF = new XRiTextField();
    TRVOLD = new XRiTextField();
    TRVOLF = new XRiTextField();
    TRLNGD = new XRiTextField();
    TRLNGF = new XRiTextField();
    TRSURD = new XRiTextField();
    TRSURF = new XRiTextField();
    TRCOLD = new XRiTextField();
    TRCOLF = new XRiTextField();
    OBJ_92 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_55 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setTitle("Transporteur");
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
            xTitledPanel5ContentContainer.setLayout(null);

            //======== panel6 ========
            {
              panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- OBJ_63 ----
              OBJ_63.setText("Libell\u00e9");
              OBJ_63.setName("OBJ_63");
              panel6.add(OBJ_63);
              OBJ_63.setBounds(10, 14, 43, 20);

              //---- TRLIB ----
              TRLIB.setComponentPopupMenu(BTD);
              TRLIB.setName("TRLIB");
              panel6.add(TRLIB);
              TRLIB.setBounds(235, 10, 410, TRLIB.getPreferredSize().height);
            }
            xTitledPanel5ContentContainer.add(panel6);
            panel6.setBounds(15, 15, 775, 50);

            //---- OBJ_56 ----
            OBJ_56.setText("Coefficient conjoncturel");
            OBJ_56.setName("OBJ_56");
            xTitledPanel5ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(20, 79, 139, 20);

            //---- TRCCON ----
            TRCCON.setComponentPopupMenu(BTD);
            TRCCON.setName("TRCCON");
            xTitledPanel5ContentContainer.add(TRCCON);
            TRCCON.setBounds(250, 75, 60, TRCCON.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("Calcul du port");
            OBJ_58.setComponentPopupMenu(BTD);
            OBJ_58.setName("OBJ_58");
            xTitledPanel5ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(20, 108, 84, 20);

            //---- TRMCP ----
            TRMCP.setModel(new DefaultComboBoxModel(new String[] {
              "Par d\u00e9faut (param.EX)",
              "Exprim\u00e9 en poids",
              "Exprim\u00e9 en nbre de palettes"
            }));
            TRMCP.setComponentPopupMenu(BTD);
            TRMCP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TRMCP.setName("TRMCP");
            TRMCP.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                TRMCPItemStateChanged(e);
              }
            });
            xTitledPanel5ContentContainer.add(TRMCP);
            TRMCP.setBounds(250, 105, 190, TRMCP.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("Mode d'exp\u00e9dition (param.EX)");
            OBJ_66.setName("OBJ_66");
            xTitledPanel5ContentContainer.add(OBJ_66);
            OBJ_66.setBounds(20, 139, 165, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Zone g\u00e9ographique couverte");
            OBJ_72.setName("OBJ_72");
            xTitledPanel5ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(20, 189, 165, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("Transporteur de substitution");
            OBJ_70.setName("OBJ_70");
            xTitledPanel5ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(20, 164, 168, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("Quai ou plate-forme");
            OBJ_78.setName("OBJ_78");
            xTitledPanel5ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(20, 239, 139, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("Code identification");
            OBJ_84.setName("OBJ_84");
            xTitledPanel5ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(20, 289, 131, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Num\u00e9ro fournisseurs");
            OBJ_77.setName("OBJ_77");
            xTitledPanel5ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(455, 214, 126, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Si poids inf\u00e9rieur \u00e0");
            OBJ_82.setName("OBJ_82");
            xTitledPanel5ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(455, 264, 113, 20);

            //---- TRIDE ----
            TRIDE.setComponentPopupMenu(BTD);
            TRIDE.setName("TRIDE");
            xTitledPanel5ContentContainer.add(TRIDE);
            TRIDE.setBounds(250, 285, 110, TRIDE.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Num\u00e9ro Clients");
            OBJ_75.setName("OBJ_75");
            xTitledPanel5ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(20, 214, 94, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("Transporteur");
            OBJ_80.setName("OBJ_80");
            xTitledPanel5ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(20, 264, 81, 20);

            //---- TRFRS ----
            TRFRS.setComponentPopupMenu(BTD);
            TRFRS.setName("TRFRS");
            xTitledPanel5ContentContainer.add(TRFRS);
            TRFRS.setBounds(595, 210, 80, TRFRS.getPreferredSize().height);

            //---- TRCLI ----
            TRCLI.setComponentPopupMenu(BTD);
            TRCLI.setName("TRCLI");
            xTitledPanel5ContentContainer.add(TRCLI);
            TRCLI.setBounds(250, 210, 110, TRCLI.getPreferredSize().height);

            //---- TRPDS ----
            TRPDS.setComponentPopupMenu(BTD);
            TRPDS.setName("TRPDS");
            xTitledPanel5ContentContainer.add(TRPDS);
            TRPDS.setBounds(595, 260, 74, TRPDS.getPreferredSize().height);

            //---- TRZGEO ----
            TRZGEO.setComponentPopupMenu(BTD);
            TRZGEO.setName("TRZGEO");
            xTitledPanel5ContentContainer.add(TRZGEO);
            TRZGEO.setBounds(250, 185, 60, TRZGEO.getPreferredSize().height);

            //---- TRPFC ----
            TRPFC.setComponentPopupMenu(BTD);
            TRPFC.setName("TRPFC");
            xTitledPanel5ContentContainer.add(TRPFC);
            TRPFC.setBounds(250, 235, 40, TRPFC.getPreferredSize().height);

            //---- TRMEX ----
            TRMEX.setComponentPopupMenu(BTD);
            TRMEX.setName("TRMEX");
            xTitledPanel5ContentContainer.add(TRMEX);
            TRMEX.setBounds(250, 135, 30, TRMEX.getPreferredSize().height);

            //---- TRTRF ----
            TRTRF.setComponentPopupMenu(BTD);
            TRTRF.setName("TRTRF");
            xTitledPanel5ContentContainer.add(TRTRF);
            TRTRF.setBounds(250, 160, 30, TRTRF.getPreferredSize().height);

            //---- TRTRP ----
            TRTRP.setComponentPopupMenu(BTD);
            TRTRP.setName("TRTRP");
            xTitledPanel5ContentContainer.add(TRTRP);
            TRTRP.setBounds(250, 260, 30, TRTRP.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Constitution de la zone de transport"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_62 ----
              OBJ_62.setText("Provenance");
              OBJ_62.setName("OBJ_62");
              panel2.add(OBJ_62);
              OBJ_62.setBounds(10, 28, 76, 20);

              //---- TRPRO ----
              TRPRO.setModel(new DefaultComboBoxModel(new String[] {
                "Aucune",
                "Code postal",
                "Zone g\u00e9ographique client",
                "Fin de nom",
                "Fin de compl\u00e9ment de nom",
                "Fin de rue",
                "Fin de localit\u00e9",
                "Zone g\u00e9ographique commune",
                "Regroupement tourn\u00e9e"
              }));
              TRPRO.setComponentPopupMenu(BTD);
              TRPRO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TRPRO.setName("TRPRO");
              panel2.add(TRPRO);
              TRPRO.setBounds(145, 25, 195, TRPRO.getPreferredSize().height);

              //---- OBJ_64 ----
              OBJ_64.setText("Nombre de caract\u00e8res");
              OBJ_64.setName("OBJ_64");
              panel2.add(OBJ_64);
              OBJ_64.setBounds(10, 53, 138, 20);

              //---- TRNBC ----
              TRNBC.setModel(new DefaultComboBoxModel(new String[] {
                "",
                "1",
                "2",
                "3",
                "4",
                "5"
              }));
              TRNBC.setComponentPopupMenu(BTD);
              TRNBC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TRNBC.setName("TRNBC");
              panel2.add(TRNBC);
              TRNBC.setBounds(145, 50, 50, TRNBC.getPreferredSize().height);
            }
            xTitledPanel5ContentContainer.add(panel2);
            panel2.setBounds(445, 80, 350, 90);

            //======== panel1 ========
            {
              panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_86 ----
              OBJ_86.setText("Commande transport");
              OBJ_86.setName("OBJ_86");
              panel1.add(OBJ_86);
              OBJ_86.setBounds(5, 5, 128, 20);

              //---- TRCDE ----
              TRCDE.setText("");
              TRCDE.setComponentPopupMenu(BTD);
              TRCDE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TRCDE.setName("TRCDE");
              panel1.add(TRCDE);
              TRCDE.setBounds(145, 5, TRCDE.getPreferredSize().width, 20);
            }
            xTitledPanel5ContentContainer.add(panel1);
            panel1.setBounds(450, 170, 175, 30);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Choisir ce transporteur pour"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_104 ----
              OBJ_104.setText("Une surface au plancher comprise entre");
              OBJ_104.setName("OBJ_104");
              panel3.add(OBJ_104);
              OBJ_104.setBounds(10, 109, 235, 20);

              //---- OBJ_109 ----
              OBJ_109.setText("Un nombre de colis compris entre");
              OBJ_109.setName("OBJ_109");
              panel3.add(OBJ_109);
              OBJ_109.setBounds(10, 134, 204, 20);

              //---- OBJ_99 ----
              OBJ_99.setText("Une longueur comprise entre");
              OBJ_99.setName("OBJ_99");
              panel3.add(OBJ_99);
              OBJ_99.setBounds(10, 84, 175, 20);

              //---- OBJ_94 ----
              OBJ_94.setText("Un volume compris entre");
              OBJ_94.setName("OBJ_94");
              panel3.add(OBJ_94);
              OBJ_94.setBounds(10, 59, 151, 20);

              //---- OBJ_90 ----
              OBJ_90.setText("Un poids compris entre");
              OBJ_90.setName("OBJ_90");
              panel3.add(OBJ_90);
              OBJ_90.setBounds(10, 34, 141, 20);

              //---- TRPDSD ----
              TRPDSD.setComponentPopupMenu(BTD);
              TRPDSD.setName("TRPDSD");
              panel3.add(TRPDSD);
              TRPDSD.setBounds(240, 30, 76, TRPDSD.getPreferredSize().height);

              //---- TRPDSF ----
              TRPDSF.setComponentPopupMenu(BTD);
              TRPDSF.setName("TRPDSF");
              panel3.add(TRPDSF);
              TRPDSF.setBounds(585, 30, 76, TRPDSF.getPreferredSize().height);

              //---- TRVOLD ----
              TRVOLD.setComponentPopupMenu(BTD);
              TRVOLD.setName("TRVOLD");
              panel3.add(TRVOLD);
              TRVOLD.setBounds(240, 55, 76, TRVOLD.getPreferredSize().height);

              //---- TRVOLF ----
              TRVOLF.setComponentPopupMenu(BTD);
              TRVOLF.setName("TRVOLF");
              panel3.add(TRVOLF);
              TRVOLF.setBounds(585, 55, 76, TRVOLF.getPreferredSize().height);

              //---- TRLNGD ----
              TRLNGD.setComponentPopupMenu(BTD);
              TRLNGD.setName("TRLNGD");
              panel3.add(TRLNGD);
              TRLNGD.setBounds(240, 80, 60, TRLNGD.getPreferredSize().height);

              //---- TRLNGF ----
              TRLNGF.setComponentPopupMenu(BTD);
              TRLNGF.setName("TRLNGF");
              panel3.add(TRLNGF);
              TRLNGF.setBounds(585, 80, 60, TRLNGF.getPreferredSize().height);

              //---- TRSURD ----
              TRSURD.setComponentPopupMenu(BTD);
              TRSURD.setName("TRSURD");
              panel3.add(TRSURD);
              TRSURD.setBounds(240, 105, 60, TRSURD.getPreferredSize().height);

              //---- TRSURF ----
              TRSURF.setComponentPopupMenu(BTD);
              TRSURF.setName("TRSURF");
              panel3.add(TRSURF);
              TRSURF.setBounds(585, 105, 60, TRSURF.getPreferredSize().height);

              //---- TRCOLD ----
              TRCOLD.setComponentPopupMenu(BTD);
              TRCOLD.setName("TRCOLD");
              panel3.add(TRCOLD);
              TRCOLD.setBounds(240, 130, 44, TRCOLD.getPreferredSize().height);

              //---- TRCOLF ----
              TRCOLF.setComponentPopupMenu(BTD);
              TRCOLF.setName("TRCOLF");
              panel3.add(TRCOLF);
              TRCOLF.setBounds(585, 130, 44, TRCOLF.getPreferredSize().height);

              //---- OBJ_92 ----
              OBJ_92.setText("et");
              OBJ_92.setName("OBJ_92");
              panel3.add(OBJ_92);
              OBJ_92.setBounds(440, 34, 15, 20);

              //---- OBJ_96 ----
              OBJ_96.setText("et");
              OBJ_96.setName("OBJ_96");
              panel3.add(OBJ_96);
              OBJ_96.setBounds(440, 59, 15, 20);

              //---- OBJ_101 ----
              OBJ_101.setText("et");
              OBJ_101.setName("OBJ_101");
              panel3.add(OBJ_101);
              OBJ_101.setBounds(440, 84, 15, 20);

              //---- OBJ_106 ----
              OBJ_106.setText("et");
              OBJ_106.setName("OBJ_106");
              panel3.add(OBJ_106);
              OBJ_106.setBounds(440, 109, 15, 20);

              //---- OBJ_111 ----
              OBJ_111.setText("et");
              OBJ_111.setName("OBJ_111");
              panel3.add(OBJ_111);
              OBJ_111.setBounds(440, 134, 15, 20);
            }
            xTitledPanel5ContentContainer.add(panel3);
            panel3.setBounds(10, 315, 785, 170);

            //---- OBJ_55 ----
            OBJ_55.setText("");
            OBJ_55.setToolTipText("Forfait hayon");
            OBJ_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_55.setName("OBJ_55");
            OBJ_55.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_55ActionPerformed(e);
              }
            });
            xTitledPanel5ContentContainer.add(OBJ_55);
            OBJ_55.setBounds(105, 98, 40, 40);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel5ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 825, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel5;
  private JPanel panel6;
  private JLabel OBJ_63;
  private XRiTextField TRLIB;
  private JLabel OBJ_56;
  private XRiTextField TRCCON;
  private JLabel OBJ_58;
  private XRiComboBox TRMCP;
  private JLabel OBJ_66;
  private JLabel OBJ_72;
  private JLabel OBJ_70;
  private JLabel OBJ_78;
  private JLabel OBJ_84;
  private JLabel OBJ_77;
  private JLabel OBJ_82;
  private XRiTextField TRIDE;
  private JLabel OBJ_75;
  private JLabel OBJ_80;
  private XRiTextField TRFRS;
  private XRiTextField TRCLI;
  private XRiTextField TRPDS;
  private XRiTextField TRZGEO;
  private XRiTextField TRPFC;
  private XRiTextField TRMEX;
  private XRiTextField TRTRF;
  private XRiTextField TRTRP;
  private JPanel panel2;
  private JLabel OBJ_62;
  private XRiComboBox TRPRO;
  private JLabel OBJ_64;
  private XRiComboBox TRNBC;
  private JPanel panel1;
  private JLabel OBJ_86;
  private XRiCheckBox TRCDE;
  private JPanel panel3;
  private JLabel OBJ_104;
  private JLabel OBJ_109;
  private JLabel OBJ_99;
  private JLabel OBJ_94;
  private JLabel OBJ_90;
  private XRiTextField TRPDSD;
  private XRiTextField TRPDSF;
  private XRiTextField TRVOLD;
  private XRiTextField TRVOLF;
  private XRiTextField TRLNGD;
  private XRiTextField TRLNGF;
  private XRiTextField TRSURD;
  private XRiTextField TRSURF;
  private XRiTextField TRCOLD;
  private XRiTextField TRCOLF;
  private JLabel OBJ_92;
  private JLabel OBJ_96;
  private JLabel OBJ_101;
  private JLabel OBJ_106;
  private JLabel OBJ_111;
  private SNBoutonDetail OBJ_55;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
