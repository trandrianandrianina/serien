/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgvm.VGVM117F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM117F_B2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM117F_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    WTRG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTRG@")).trim());
    E1RDM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RDM@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG1@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG2@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG3@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG4@")).trim());
    WRAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRAR@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRG1@")).trim());
    MRG1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MRG1@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRG3@")).trim());
    MRG3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MRG3@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRG5@")).trim());
    MRG5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MRG5@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRG2@")).trim());
    MRG2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MRG2@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRG4@")).trim());
    MRG4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MRG4@")).trim());
    TOTCDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTCDE@")).trim());
    TOTEXP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTEXP@")).trim());
    LARRHE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LARRHE@")).trim());
    ARRHES.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARRHES@")).trim());
    ARGL.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@ARGL@")).trim());
    TTEXP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTEXP@")).trim());
    TARRHE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TARRHE@")).trim());
    TOTMRG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTMRG@")).trim());
    TMRGS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMRGS@")).trim());
    LARRHE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LARRHE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_40.setVisible(lexique.isPresent("TRG4"));
    OBJ_39.setVisible(lexique.isPresent("TRG2"));
    OBJ_35.setVisible(lexique.isPresent("TRG5"));
    OBJ_34.setVisible(lexique.isPresent("TRG3"));
    OBJ_33.setVisible(lexique.isPresent("TRG1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Réglement"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ANNUL", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void checkBox1ActionPerformed(ActionEvent e) {
    checkBox2.setEnabled(!checkBox1.isSelected());
    checkBox3.setEnabled(!checkBox1.isSelected());
    checkBox4.setEnabled(!checkBox1.isSelected());
    E1MRG1.setEnabled(!checkBox1.isSelected());
    lexique.HostFieldPutData("E1PC101", 0, "**");
    lexique.HostFieldPutData("E1PC102", 0, "  ");
    lexique.HostFieldPutData("E1PC103", 0, "  ");
    lexique.HostFieldPutData("E1PC104", 0, "  ");
  }
  
  private void checkBox2ActionPerformed(ActionEvent e) {
    checkBox1.setEnabled(!checkBox2.isSelected());
    checkBox3.setEnabled(!checkBox2.isSelected());
    checkBox4.setEnabled(!checkBox2.isSelected());
    E1MRG2.setEnabled(!checkBox2.isSelected());
    lexique.HostFieldPutData("E1PC101", 0, "  ");
    lexique.HostFieldPutData("E1PC102", 0, "**");
    lexique.HostFieldPutData("E1PC103", 0, "  ");
    lexique.HostFieldPutData("E1PC104", 0, "  ");
  }
  
  private void checkBox3ActionPerformed(ActionEvent e) {
    checkBox1.setEnabled(!checkBox3.isSelected());
    checkBox2.setEnabled(!checkBox3.isSelected());
    checkBox4.setEnabled(!checkBox3.isSelected());
    E1MRG3.setEnabled(!checkBox3.isSelected());
    lexique.HostFieldPutData("E1PC101", 0, "  ");
    lexique.HostFieldPutData("E1PC102", 0, "  ");
    lexique.HostFieldPutData("E1PC103", 0, "**");
    lexique.HostFieldPutData("E1PC104", 0, "  ");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void checkBox4ActionPerformed(ActionEvent e) {
    checkBox1.setEnabled(!checkBox4.isSelected());
    checkBox2.setEnabled(!checkBox4.isSelected());
    checkBox3.setEnabled(!checkBox4.isSelected());
    E1MRG4.setEnabled(!checkBox4.isSelected());
    lexique.HostFieldPutData("E1PC101", 0, "  ");
    lexique.HostFieldPutData("E1PC102", 0, "  ");
    lexique.HostFieldPutData("E1PC103", 0, "  ");
    lexique.HostFieldPutData("E1PC104", 0, "**");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    E1TTC = new RiZoneSortie();
    WTRG = new RiZoneSortie();
    E1RDM = new RiZoneSortie();
    OBJ_24 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_28 = new JLabel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    E1RG1 = new XRiTextField();
    E1MRG1 = new XRiTextField();
    checkBox1 = new JCheckBox();
    checkBox2 = new JCheckBox();
    E1MRG2 = new XRiTextField();
    E1RG2 = new XRiTextField();
    label5 = new JLabel();
    label6 = new JLabel();
    E1RG3 = new XRiTextField();
    E1MRG3 = new XRiTextField();
    checkBox3 = new JCheckBox();
    label7 = new JLabel();
    E1RG4 = new XRiTextField();
    E1MRG4 = new XRiTextField();
    checkBox4 = new JCheckBox();
    WRAR = new RiZoneSortie();
    OBJ_29 = new JLabel();
    panel2 = new JPanel();
    OBJ_33 = new JLabel();
    MRG1 = new RiZoneSortie();
    OBJ_34 = new JLabel();
    MRG3 = new RiZoneSortie();
    OBJ_35 = new JLabel();
    MRG5 = new RiZoneSortie();
    OBJ_39 = new JLabel();
    MRG2 = new RiZoneSortie();
    OBJ_40 = new JLabel();
    MRG4 = new RiZoneSortie();
    panel3 = new JPanel();
    OBJ_44 = new JLabel();
    TOTCDE = new RiZoneSortie();
    OBJ_45 = new JLabel();
    TOTEXP = new RiZoneSortie();
    LARRHE = new JLabel();
    ARRHES = new RiZoneSortie();
    ARGL = new JXTitledSeparator();
    OBJ_51 = new JLabel();
    TTEXP = new RiZoneSortie();
    OBJ_59 = new JLabel();
    TARRHE = new RiZoneSortie();
    OBJ_60 = new JLabel();
    TOTMRG = new RiZoneSortie();
    OBJ_53 = new JLabel();
    OBJ_61 = new JLabel();
    TMRGS = new RiZoneSortie();
    OBJ_54 = new JLabel();
    LARRHE2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(725, 620));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 150));
            menus_haut.setPreferredSize(new Dimension(160, 150));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Gestion des r\u00e9glements");
              riSousMenu_bt6.setToolTipText("Gestion des r\u00e9glements");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Annulation du bon");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(740, 620));
        p_contenu.setName("p_contenu");
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("R\u00e8glement"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- E1TTC ----
          E1TTC.setText("@E1TTC@");
          E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
          E1TTC.setFont(E1TTC.getFont().deriveFont(E1TTC.getFont().getStyle() | Font.BOLD));
          E1TTC.setName("E1TTC");
          panel1.add(E1TTC);
          E1TTC.setBounds(335, 185, 88, E1TTC.getPreferredSize().height);
          
          // ---- WTRG ----
          WTRG.setComponentPopupMenu(BTD);
          WTRG.setText("@WTRG@");
          WTRG.setHorizontalAlignment(SwingConstants.RIGHT);
          WTRG.setName("WTRG");
          panel1.add(WTRG);
          WTRG.setBounds(335, 210, 88, WTRG.getPreferredSize().height);
          
          // ---- E1RDM ----
          E1RDM.setText("@E1RDM@");
          E1RDM.setHorizontalAlignment(SwingConstants.RIGHT);
          E1RDM.setName("E1RDM");
          panel1.add(E1RDM);
          E1RDM.setBounds(335, 260, 88, E1RDM.getPreferredSize().height);
          
          // ---- OBJ_24 ----
          OBJ_24.setText("Total \u00e0 r\u00e8gler");
          OBJ_24.setFont(OBJ_24.getFont().deriveFont(OBJ_24.getFont().getStyle() | Font.BOLD));
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(210, 185, 120, 24);
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Montants re\u00e7us");
          OBJ_26.setFont(OBJ_26.getFont().deriveFont(OBJ_26.getFont().getStyle() | Font.BOLD));
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(210, 210, 120, 24);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("A  rendre");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(210, 260, 120, 24);
          
          // ---- label1 ----
          label1.setText("Libell\u00e9 r\u00e8glement");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 35, 130, 24);
          
          // ---- label2 ----
          label2.setText("MR");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(295, 35, 25, 24);
          
          // ---- label3 ----
          label3.setText("Montant");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(340, 35, 60, 24);
          
          // ---- label4 ----
          label4.setText("@WLRG1@");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(20, 62, 260, 24);
          
          // ---- E1RG1 ----
          E1RG1.setName("E1RG1");
          panel1.add(E1RG1);
          E1RG1.setBounds(290, 60, 40, 28);
          
          // ---- E1MRG1 ----
          E1MRG1.setHorizontalAlignment(SwingConstants.TRAILING);
          E1MRG1.setName("E1MRG1");
          panel1.add(E1MRG1);
          E1MRG1.setBounds(335, 60, 90, 28);
          
          // ---- checkBox1 ----
          checkBox1.setName("checkBox1");
          checkBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox1ActionPerformed(e);
            }
          });
          panel1.add(checkBox1);
          checkBox1.setBounds(435, 65, 18, 18);
          
          // ---- checkBox2 ----
          checkBox2.setName("checkBox2");
          checkBox2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox2ActionPerformed(e);
            }
          });
          panel1.add(checkBox2);
          checkBox2.setBounds(435, 95, 18, 18);
          
          // ---- E1MRG2 ----
          E1MRG2.setHorizontalAlignment(SwingConstants.TRAILING);
          E1MRG2.setName("E1MRG2");
          panel1.add(E1MRG2);
          E1MRG2.setBounds(335, 90, 90, 28);
          
          // ---- E1RG2 ----
          E1RG2.setName("E1RG2");
          panel1.add(E1RG2);
          E1RG2.setBounds(290, 90, 40, 28);
          
          // ---- label5 ----
          label5.setText("@WLRG2@");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(20, 92, 260, 24);
          
          // ---- label6 ----
          label6.setText("@WLRG3@");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(20, 122, 260, 24);
          
          // ---- E1RG3 ----
          E1RG3.setName("E1RG3");
          panel1.add(E1RG3);
          E1RG3.setBounds(290, 120, 40, 28);
          
          // ---- E1MRG3 ----
          E1MRG3.setHorizontalAlignment(SwingConstants.TRAILING);
          E1MRG3.setName("E1MRG3");
          panel1.add(E1MRG3);
          E1MRG3.setBounds(335, 120, 90, 28);
          
          // ---- checkBox3 ----
          checkBox3.setName("checkBox3");
          checkBox3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox3ActionPerformed(e);
            }
          });
          panel1.add(checkBox3);
          checkBox3.setBounds(435, 125, 18, 18);
          
          // ---- label7 ----
          label7.setText("@WLRG4@");
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(20, 152, 260, 24);
          
          // ---- E1RG4 ----
          E1RG4.setName("E1RG4");
          panel1.add(E1RG4);
          E1RG4.setBounds(290, 150, 40, 28);
          
          // ---- E1MRG4 ----
          E1MRG4.setHorizontalAlignment(SwingConstants.TRAILING);
          E1MRG4.setName("E1MRG4");
          panel1.add(E1MRG4);
          E1MRG4.setBounds(335, 150, 90, 28);
          
          // ---- checkBox4 ----
          checkBox4.setName("checkBox4");
          checkBox4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox4ActionPerformed(e);
            }
          });
          panel1.add(checkBox4);
          checkBox4.setBounds(435, 155, 18, 18);
          
          // ---- WRAR ----
          WRAR.setText("@WRAR@");
          WRAR.setHorizontalAlignment(SwingConstants.RIGHT);
          WRAR.setName("WRAR");
          panel1.add(WRAR);
          WRAR.setBounds(335, 235, 88, WRAR.getPreferredSize().height);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Reste \u00e0 r\u00e9gler");
          OBJ_29.setFont(OBJ_29.getFont().deriveFont(OBJ_29.getFont().getStyle() | Font.BOLD));
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(210, 235, 120, 24);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("D\u00e9j\u00e0 per\u00e7u"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- OBJ_33 ----
          OBJ_33.setText("@TRG1@");
          OBJ_33.setName("OBJ_33");
          panel2.add(OBJ_33);
          OBJ_33.setBounds(20, 37, 30, 20);
          
          // ---- MRG1 ----
          MRG1.setText("@MRG1@");
          MRG1.setHorizontalAlignment(SwingConstants.RIGHT);
          MRG1.setName("MRG1");
          panel2.add(MRG1);
          MRG1.setBounds(60, 35, 90, MRG1.getPreferredSize().height);
          
          // ---- OBJ_34 ----
          OBJ_34.setText("@TRG3@");
          OBJ_34.setName("OBJ_34");
          panel2.add(OBJ_34);
          OBJ_34.setBounds(185, 37, 30, 20);
          
          // ---- MRG3 ----
          MRG3.setText("@MRG3@");
          MRG3.setHorizontalAlignment(SwingConstants.RIGHT);
          MRG3.setName("MRG3");
          panel2.add(MRG3);
          MRG3.setBounds(225, 35, 90, MRG3.getPreferredSize().height);
          
          // ---- OBJ_35 ----
          OBJ_35.setText("@TRG5@");
          OBJ_35.setName("OBJ_35");
          panel2.add(OBJ_35);
          OBJ_35.setBounds(350, 37, 30, 20);
          
          // ---- MRG5 ----
          MRG5.setText("@MRG5@");
          MRG5.setHorizontalAlignment(SwingConstants.RIGHT);
          MRG5.setName("MRG5");
          panel2.add(MRG5);
          MRG5.setBounds(390, 35, 90, MRG5.getPreferredSize().height);
          
          // ---- OBJ_39 ----
          OBJ_39.setText("@TRG2@");
          OBJ_39.setName("OBJ_39");
          panel2.add(OBJ_39);
          OBJ_39.setBounds(20, 67, 30, 20);
          
          // ---- MRG2 ----
          MRG2.setText("@MRG2@");
          MRG2.setHorizontalAlignment(SwingConstants.RIGHT);
          MRG2.setName("MRG2");
          panel2.add(MRG2);
          MRG2.setBounds(60, 65, 90, MRG2.getPreferredSize().height);
          
          // ---- OBJ_40 ----
          OBJ_40.setText("@TRG4@");
          OBJ_40.setName("OBJ_40");
          panel2.add(OBJ_40);
          OBJ_40.setBounds(185, 67, 30, 20);
          
          // ---- MRG4 ----
          MRG4.setText("@MRG4@");
          MRG4.setHorizontalAlignment(SwingConstants.RIGHT);
          MRG4.setName("MRG4");
          panel2.add(MRG4);
          MRG4.setBounds(225, 65, 90, MRG4.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        
        // ======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Total commande");
          OBJ_44.setName("OBJ_44");
          panel3.add(OBJ_44);
          OBJ_44.setBounds(60, 15, 105, 20);
          
          // ---- TOTCDE ----
          TOTCDE.setText("@TOTCDE@");
          TOTCDE.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTCDE.setName("TOTCDE");
          panel3.add(TOTCDE);
          TOTCDE.setBounds(60, 35, 90, TOTCDE.getPreferredSize().height);
          
          // ---- OBJ_45 ----
          OBJ_45.setText("Total exp\u00e9di\u00e9");
          OBJ_45.setName("OBJ_45");
          panel3.add(OBJ_45);
          OBJ_45.setBounds(225, 15, 105, 20);
          
          // ---- TOTEXP ----
          TOTEXP.setText("@TOTEXP@");
          TOTEXP.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTEXP.setName("TOTEXP");
          panel3.add(TOTEXP);
          TOTEXP.setBounds(225, 35, 90, TOTEXP.getPreferredSize().height);
          
          // ---- LARRHE ----
          LARRHE.setText("@LARRHE@");
          LARRHE.setName("LARRHE");
          panel3.add(LARRHE);
          LARRHE.setBounds(390, 15, 105, 20);
          
          // ---- ARRHES ----
          ARRHES.setText("@ARRHES@");
          ARRHES.setHorizontalAlignment(SwingConstants.RIGHT);
          ARRHES.setName("ARRHES");
          panel3.add(ARRHES);
          ARRHES.setBounds(390, 35, 90, ARRHES.getPreferredSize().height);
          
          // ---- ARGL ----
          ARGL.setTitle("@ARGL@");
          ARGL.setName("ARGL");
          panel3.add(ARGL);
          ARGL.setBounds(20, 70, 470, ARGL.getPreferredSize().height);
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Exp\u00e9di\u00e9");
          OBJ_51.setName("OBJ_51");
          panel3.add(OBJ_51);
          OBJ_51.setBounds(60, 90, 91, 20);
          
          // ---- TTEXP ----
          TTEXP.setText("@TTEXP@");
          TTEXP.setHorizontalAlignment(SwingConstants.RIGHT);
          TTEXP.setName("TTEXP");
          panel3.add(TTEXP);
          TTEXP.setBounds(60, 110, 90, TTEXP.getPreferredSize().height);
          
          // ---- OBJ_59 ----
          OBJ_59.setText("+");
          OBJ_59.setName("OBJ_59");
          panel3.add(OBJ_59);
          OBJ_59.setBounds(158, 113, 9, 18);
          
          // ---- TARRHE ----
          TARRHE.setText("@TARRHE@");
          TARRHE.setHorizontalAlignment(SwingConstants.RIGHT);
          TARRHE.setName("TARRHE");
          panel3.add(TARRHE);
          TARRHE.setBounds(170, 110, 90, TARRHE.getPreferredSize().height);
          
          // ---- OBJ_60 ----
          OBJ_60.setText("-");
          OBJ_60.setName("OBJ_60");
          panel3.add(OBJ_60);
          OBJ_60.setBounds(267, 113, 9, 18);
          
          // ---- TOTMRG ----
          TOTMRG.setText("@TOTMRG@");
          TOTMRG.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTMRG.setName("TOTMRG");
          panel3.add(TOTMRG);
          TOTMRG.setBounds(280, 110, 90, TOTMRG.getPreferredSize().height);
          
          // ---- OBJ_53 ----
          OBJ_53.setText("R\u00e8gl\u00e9");
          OBJ_53.setName("OBJ_53");
          panel3.add(OBJ_53);
          OBJ_53.setBounds(280, 90, 91, 20);
          
          // ---- OBJ_61 ----
          OBJ_61.setText("=");
          OBJ_61.setName("OBJ_61");
          panel3.add(OBJ_61);
          OBJ_61.setBounds(377, 113, 9, 18);
          
          // ---- TMRGS ----
          TMRGS.setText("@TMRGS@");
          TMRGS.setHorizontalAlignment(SwingConstants.RIGHT);
          TMRGS.setName("TMRGS");
          panel3.add(TMRGS);
          TMRGS.setBounds(390, 110, 90, TMRGS.getPreferredSize().height);
          
          // ---- OBJ_54 ----
          OBJ_54.setText("Total");
          OBJ_54.setName("OBJ_54");
          panel3.add(OBJ_54);
          OBJ_54.setBounds(390, 90, 111, 20);
          
          // ---- LARRHE2 ----
          LARRHE2.setText("@LARRHE@");
          LARRHE2.setName("LARRHE2");
          panel3.add(LARRHE2);
          LARRHE2.setBounds(170, 90, 105, 20);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE))));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup().addGap(15, 15, 15)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 307, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 112, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie E1TTC;
  private RiZoneSortie WTRG;
  private RiZoneSortie E1RDM;
  private JLabel OBJ_24;
  private JLabel OBJ_26;
  private JLabel OBJ_28;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField E1RG1;
  private XRiTextField E1MRG1;
  private JCheckBox checkBox1;
  private JCheckBox checkBox2;
  private XRiTextField E1MRG2;
  private XRiTextField E1RG2;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField E1RG3;
  private XRiTextField E1MRG3;
  private JCheckBox checkBox3;
  private JLabel label7;
  private XRiTextField E1RG4;
  private XRiTextField E1MRG4;
  private JCheckBox checkBox4;
  private RiZoneSortie WRAR;
  private JLabel OBJ_29;
  private JPanel panel2;
  private JLabel OBJ_33;
  private RiZoneSortie MRG1;
  private JLabel OBJ_34;
  private RiZoneSortie MRG3;
  private JLabel OBJ_35;
  private RiZoneSortie MRG5;
  private JLabel OBJ_39;
  private RiZoneSortie MRG2;
  private JLabel OBJ_40;
  private RiZoneSortie MRG4;
  private JPanel panel3;
  private JLabel OBJ_44;
  private RiZoneSortie TOTCDE;
  private JLabel OBJ_45;
  private RiZoneSortie TOTEXP;
  private JLabel LARRHE;
  private RiZoneSortie ARRHES;
  private JXTitledSeparator ARGL;
  private JLabel OBJ_51;
  private RiZoneSortie TTEXP;
  private JLabel OBJ_59;
  private RiZoneSortie TARRHE;
  private JLabel OBJ_60;
  private RiZoneSortie TOTMRG;
  private JLabel OBJ_53;
  private JLabel OBJ_61;
  private RiZoneSortie TMRGS;
  private JLabel OBJ_54;
  private JLabel LARRHE2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
