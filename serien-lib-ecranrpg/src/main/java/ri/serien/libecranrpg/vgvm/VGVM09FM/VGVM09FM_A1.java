
package ri.serien.libecranrpg.vgvm.VGVM09FM;
// Nom Fichier: pop_VGVM09FM_FMTA1_FMTF1_155.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVM09FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST2_Top=null;
  private String[] _TIL01_Title = { "", "", };
  private String[][] _TIL01_Data =
      { { "TIL01", "TIPC01", }, { "TIL02", "TIPC02", }, { "TIL03", "TIPC03", }, { "TIL04", "TIPC04", }, { "TIL05", "TIPC05", },
          { "TIL06", "TIPC06", }, { "TIL07", "TIPC07", }, { "TIL08", "TIPC08", }, { "TIL09", "TIPC09", }, { "TIL10", "TIPC10", }, };
  private int[] _TIL01_Width = { 216, 48, };
  // private String[][] _LIST2_Title_Data_Brut=null;
  
  public VGVM09FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST2_Title_Data_Brut = initTable(LIST2, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIL01.setAspectTable(null, _TIL01_Title, _TIL01_Data, _TIL01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST2, _LIST2_Title_Data_Brut, _LIST2_Top);
    
    
    TITOT.setEnabled(lexique.isPresent("TITOT"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_24.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  /*
    private void TIL01MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(LIST2, _LIST2_Top, "1", "ENTER", e);
      if (TIL01.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(LIST2, _LIST2_Top, "1", "Enter");
      TIL01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel3 = new JPanel();
    panel2 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_24 = new JButton();
    panel4 = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    TIL01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_21 = new JLabel();
    TITOT = new XRiTextField();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_25 = new JLabel();

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== panel3 ========
    {
      panel3.setName("panel3");

      //======== panel2 ========
      {
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("OK");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_23ActionPerformed(e);
          }
        });
        panel2.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_24 ----
        OBJ_24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_24.setToolTipText("Retour");
        OBJ_24.setName("OBJ_24");
        OBJ_24.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_24ActionPerformed(e);
          }
        });
        panel2.add(OBJ_24);
        OBJ_24.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }

      GroupLayout panel3Layout = new GroupLayout(panel3);
      panel3.setLayout(panel3Layout);
      panel3Layout.setHorizontalGroup(
        panel3Layout.createParallelGroup()
          .addGroup(panel3Layout.createSequentialGroup()
            .addGap(275, 275, 275)
            .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
      );
      panel3Layout.setVerticalGroup(
        panel3Layout.createParallelGroup()
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
      );
    }
    add(panel3, BorderLayout.SOUTH);

    //======== panel4 ========
    {
      panel4.setName("panel4");
      panel4.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("D\u00e9tail condition"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //======== SCROLLPANE_LIST2 ========
        {
          SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

          //---- TIL01 ----
          TIL01.setName("TIL01");
          SCROLLPANE_LIST2.setViewportView(TIL01);
        }
        panel1.add(SCROLLPANE_LIST2);
        SCROLLPANE_LIST2.setBounds(25, 35, 295, 175);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        BT_PGUP.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGUPActionPerformed(e);
          }
        });
        panel1.add(BT_PGUP);
        BT_PGUP.setBounds(330, 35, 25, 85);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        BT_PGDOWN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGDOWNActionPerformed(e);
          }
        });
        panel1.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(330, 125, 25, 85);

        //---- OBJ_21 ----
        OBJ_21.setText("Total");
        OBJ_21.setName("OBJ_21");
        panel1.add(OBJ_21);
        OBJ_21.setBounds(210, 234, 52, 20);

        //---- TITOT ----
        TITOT.setComponentPopupMenu(BTD);
        TITOT.setName("TITOT");
        panel1.add(TITOT);
        TITOT.setBounds(275, 230, 58, TITOT.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel4.add(panel1);
      panel1.setBounds(5, 5, 385, 280);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel4.getComponentCount(); i++) {
          Rectangle bounds = panel4.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel4.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel4.setMinimumSize(preferredSize);
        panel4.setPreferredSize(preferredSize);
      }
    }
    add(panel4, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Exploitation");
      OBJ_9.setName("OBJ_9");
      OBJ_4.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_25 ----
    OBJ_25.setText("D\u00e9tail condition");
    OBJ_25.setName("OBJ_25");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel3;
  private JPanel panel2;
  private JButton BT_ENTER;
  private JButton OBJ_24;
  private JPanel panel4;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable TIL01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_21;
  private XRiTextField TITOT;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JLabel OBJ_25;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
