
package ri.serien.libecranrpg.vgvm.VGVM032F;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VGVM032F_COM extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private boolean isConsult = false;
  
  public VGVM032F_COM(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
    TM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM1@")).trim());
    TM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM2@")).trim());
    TM3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM3@")).trim());
    TM4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM4@")).trim());
    TM5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM5@")).trim());
    TM6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM6@")).trim());
    TM7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM7@")).trim());
    TM8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM8@")).trim());
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    isConsult = lexique.isTrue("53");
    
    // Valeurs ++++++++++++++++++++++++++++++++++++++++++++++++
    TM1.setText(lexique.HostFieldGetData("TM1").trim());
    TM2.setText(lexique.HostFieldGetData("TM2").trim());
    TM3.setText(lexique.HostFieldGetData("TM3").trim());
    TM4.setText(lexique.HostFieldGetData("TM4").trim());
    TM5.setText(lexique.HostFieldGetData("TM5").trim());
    TM6.setText(lexique.HostFieldGetData("TM6").trim());
    TM7.setText(lexique.HostFieldGetData("TM7").trim());
    TM8.setText(lexique.HostFieldGetData("TM8").trim());
    OB1.setText(lexique.HostFieldGetData("OB1").trim());
    OB2.setText(lexique.HostFieldGetData("OB2").trim());
    OB3.setText(lexique.HostFieldGetData("OB3").trim());
    OB4.setText(lexique.HostFieldGetData("OB4").trim());
    OB5.setText(lexique.HostFieldGetData("OB5").trim());
    OB6.setText(lexique.HostFieldGetData("OB6").trim());
    OB7.setText(lexique.HostFieldGetData("OB7").trim());
    OB8.setText(lexique.HostFieldGetData("OB8").trim());
    
    // consultation ou modification
    OB1.setEditable(!isConsult);
    OB2.setEditable(!isConsult);
    OB3.setEditable(!isConsult);
    OB4.setEditable(!isConsult);
    OB5.setEditable(!isConsult);
    OB6.setEditable(!isConsult);
    OB7.setEditable(!isConsult);
    OB8.setEditable(!isConsult);
    
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Adresse client"));
  }
  
  public void getData() {
    lexique.HostFieldPutData("OB1", 0, OB1.getText());
    lexique.HostFieldPutData("OB2", 0, OB2.getText());
    lexique.HostFieldPutData("OB3", 0, OB3.getText());
    lexique.HostFieldPutData("OB4", 0, OB4.getText());
    lexique.HostFieldPutData("OB5", 0, OB5.getText());
    lexique.HostFieldPutData("OB6", 0, OB6.getText());
    lexique.HostFieldPutData("OB7", 0, OB7.getText());
    lexique.HostFieldPutData("OB8", 0, OB8.getText());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    dispose();
  }
  
  public JDialog getDialog1() {
    return this;
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    this.dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    this.setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OB1 = new XRiTextField();
    TM1 = new JLabel();
    OB2 = new XRiTextField();
    TM2 = new JLabel();
    TM3 = new JLabel();
    OB3 = new XRiTextField();
    TM4 = new JLabel();
    OB4 = new XRiTextField();
    TM5 = new JLabel();
    OB5 = new XRiTextField();
    TM6 = new JLabel();
    OB6 = new XRiTextField();
    TM7 = new JLabel();
    OB7 = new XRiTextField();
    TM8 = new JLabel();
    OB8 = new XRiTextField();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1030, 350));
    setResizable(false);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Compl\u00e9ment d'adresse"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- OB1 ----
          OB1.setComponentPopupMenu(BTD);
          OB1.setName("OB1");
          panel3.add(OB1);
          OB1.setBounds(215, 41, 600, OB1.getPreferredSize().height);
          
          // ---- TM1 ----
          TM1.setText("@TM1@");
          TM1.setFont(TM1.getFont().deriveFont(TM1.getFont().getStyle() | Font.BOLD));
          TM1.setName("TM1");
          panel3.add(TM1);
          TM1.setBounds(25, 45, 185, 20);
          
          // ---- OB2 ----
          OB2.setComponentPopupMenu(BTD);
          OB2.setName("OB2");
          panel3.add(OB2);
          OB2.setBounds(215, 70, 600, OB2.getPreferredSize().height);
          
          // ---- TM2 ----
          TM2.setText("@TM2@");
          TM2.setFont(TM2.getFont().deriveFont(TM2.getFont().getStyle() | Font.BOLD));
          TM2.setName("TM2");
          panel3.add(TM2);
          TM2.setBounds(25, 74, 185, 20);
          
          // ---- TM3 ----
          TM3.setText("@TM3@");
          TM3.setFont(TM3.getFont().deriveFont(TM3.getFont().getStyle() | Font.BOLD));
          TM3.setName("TM3");
          panel3.add(TM3);
          TM3.setBounds(25, 104, 185, 20);
          
          // ---- OB3 ----
          OB3.setComponentPopupMenu(BTD);
          OB3.setName("OB3");
          panel3.add(OB3);
          OB3.setBounds(215, 100, 600, OB3.getPreferredSize().height);
          
          // ---- TM4 ----
          TM4.setText("@TM4@");
          TM4.setFont(TM4.getFont().deriveFont(TM4.getFont().getStyle() | Font.BOLD));
          TM4.setName("TM4");
          panel3.add(TM4);
          TM4.setBounds(25, 134, 185, 20);
          
          // ---- OB4 ----
          OB4.setComponentPopupMenu(BTD);
          OB4.setName("OB4");
          panel3.add(OB4);
          OB4.setBounds(215, 130, 600, OB4.getPreferredSize().height);
          
          // ---- TM5 ----
          TM5.setText("@TM5@");
          TM5.setFont(TM5.getFont().deriveFont(TM5.getFont().getStyle() | Font.BOLD));
          TM5.setName("TM5");
          panel3.add(TM5);
          TM5.setBounds(25, 164, 185, 20);
          
          // ---- OB5 ----
          OB5.setComponentPopupMenu(BTD);
          OB5.setName("OB5");
          panel3.add(OB5);
          OB5.setBounds(215, 160, 600, OB5.getPreferredSize().height);
          
          // ---- TM6 ----
          TM6.setText("@TM6@");
          TM6.setFont(TM6.getFont().deriveFont(TM6.getFont().getStyle() | Font.BOLD));
          TM6.setName("TM6");
          panel3.add(TM6);
          TM6.setBounds(25, 194, 185, 20);
          
          // ---- OB6 ----
          OB6.setComponentPopupMenu(BTD);
          OB6.setName("OB6");
          panel3.add(OB6);
          OB6.setBounds(215, 190, 600, OB6.getPreferredSize().height);
          
          // ---- TM7 ----
          TM7.setText("@TM7@");
          TM7.setFont(TM7.getFont().deriveFont(TM7.getFont().getStyle() | Font.BOLD));
          TM7.setName("TM7");
          panel3.add(TM7);
          TM7.setBounds(25, 224, 185, 20);
          
          // ---- OB7 ----
          OB7.setComponentPopupMenu(BTD);
          OB7.setName("OB7");
          panel3.add(OB7);
          OB7.setBounds(215, 220, 600, OB7.getPreferredSize().height);
          
          // ---- TM8 ----
          TM8.setText("@TM8@");
          TM8.setFont(TM8.getFont().deriveFont(TM8.getFont().getStyle() | Font.BOLD));
          TM8.setName("TM8");
          panel3.add(TM8);
          TM8.setBounds(25, 254, 185, 20);
          
          // ---- OB8 ----
          OB8.setComponentPopupMenu(BTD);
          OB8.setName("OB8");
          panel3.add(OB8);
          OB8.setBounds(215, 250, 600, OB8.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout
            .setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addContainerGap()));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel3, GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    contentPane.add(p_principal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField OB1;
  private JLabel TM1;
  private XRiTextField OB2;
  private JLabel TM2;
  private JLabel TM3;
  private XRiTextField OB3;
  private JLabel TM4;
  private XRiTextField OB4;
  private JLabel TM5;
  private XRiTextField OB5;
  private JLabel TM6;
  private XRiTextField OB6;
  private JLabel TM7;
  private XRiTextField OB7;
  private JLabel TM8;
  private XRiTextField OB8;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
