
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_CV extends SNPanelEcranRPG implements ioFrame {
  
  private String[] CVCPC_Value = { "J", "S", "D", "Q", "M", };
  private String[] CVREF_Value = { "", "1", "2", "3", "4", "5", "6", "7" };
  private String[] _HLD01_Title = { "Plan comptable", "Co", "HLD01", };
  private String[][] _HLD01_Data = { { "N° CO Vtes taxables TVA1", "CVC01", "LCG01", }, { "N° CO Vtes taxables TVA2", "CVC02", "LCG02", },
      { "N° CO Vtes taxables TVA3", "CVC03", "LCG03", }, { "N° CO Vtes taxables TVA4", "CVC04", "LCG04", },
      { "N° CO Vtes taxables TVA5", "CVC05", "LCG05", }, { "N° CO Vtes taxables TVA6", "CVC06", "LCG06", },
      { "N° CO Vtes Export C.E.E.", "CVC07", "LCG07", }, { "N° CO Ventes Export", "CVC08", "LCG08", },
      { "N° CO Vtes assim.Export", "CVC09", "LCG09", }, { "N° CO TVA collectée 1", "CVC10", "LCG10", },
      { "N° CO TVA collectée 2", "CVC11", "LCG11", }, { "N° CO TVA collectée 3", "CVC12", "LCG12", },
      { "N° CO TVA collectée 4", "CVC13", "LCG13", }, { "N° CO TVA collectée 5", "CVC14", "LCG14", },
      { "N° CO TVA collectée 6", "CVC15", "LCG15", }, { "N° CO Taxe Parafiscale", "CVC16", "LCG16", },
      { "N° CO Escomptes/Ventes", "CVC17", "LCG17", }, };
  private int[] _HLD01_Width = { 175, 15, 250, };
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.LEFT };
  
  public VGVM01FX_CV(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    CVATT.setValeursSelection("1", "");
    CVSEC.setValeursSelection("X", "");
    CVSER.setValeursSelection("X", "");
    WOPT1.setValeursSelection("X", "");
    CVCPC.setValeurs(CVCPC_Value, null);
    CVREF.setValeurs(CVREF_Value, null);
    HLD01.setAspectTable(null, _HLD01_Title, _HLD01_Data, _HLD01_Width, true, _LIST_Justification, null, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    if (CVCPC.isEnabled()) {
      CVCPA.setEnabled(true);
      OBJ_66.setEnabled(true);
    }
    else {
      CVCPA.setEnabled(false);
      OBJ_66.setEnabled(false);
    }
    
    if (lexique.isTrue("52")) {
      HLD01.setComponentPopupMenu(popupMenu1);
    }
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  /**
   * 
   * Touche de fonction associée au bouton Edition des paramètres.
   * 
   * @param e
   */
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton valider.
   * 
   * @param e
   */
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton retour.
   * 
   * @param e
   */
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton menu contextuel aide en ligne.
   * 
   * @param e
   */
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  /**
   * 
   * Touche de fonction associée au bouton menu contextuel Choix possibles.
   * 
   * @param e
   */
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton menu contextuel Choix possibles sur liste.
   * 
   * @param e
   */
  private void menuItem1ActionPerformed(ActionEvent e) {
    int ligneCourante = 4;
    ligneCourante += HLD01.getSelectedRow();
    lexique.HostCursorPut(ligneCourante, 26);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton axes analytiques.
   * 
   * @param e
   */
  private void OBJ_66ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 79);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton Comptabilisation des ventes par anticipation.
   * 
   * @param e
   */
  private void CVCPAActionPerformed() {
    lexique.HostCursorPut(7, 78);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * 
   * Touche de fonction associée au bouton Historique modifications.
   * 
   * @param e
   */
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel2 = new JPanel();
    SCROLLPANE_LISTCV = new JScrollPane();
    HLD01 = new XRiTable();
    panel3 = new JPanel();
    CVCPC = new XRiComboBox();
    OBJ_71 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_73 = new JLabel();
    CVCOL = new XRiTextField();
    CVDIF = new XRiTextField();
    CVCJV = new XRiTextField();
    CVATT = new XRiCheckBox();
    panel4 = new JPanel();
    OBJ_80 = new JLabel();
    CVSEC = new XRiCheckBox();
    OBJ_94 = new JLabel();
    CVSER = new XRiCheckBox();
    WOPT1 = new XRiCheckBox();
    OBJ_66 = new SNBoutonLeger();
    CVCPA = new SNBoutonLeger();
    CVETC = new XRiTextField();
    OBJ_76 = new JLabel();
    CVACC = new XRiTextField();
    panel5 = new JPanel();
    OBJ_96 = new JLabel();
    OBJ_106 = new JLabel();
    SAN1 = new XRiTextField();
    SAN2 = new XRiTextField();
    SAN3 = new XRiTextField();
    SAN4 = new XRiTextField();
    SAN5 = new XRiTextField();
    SAN6 = new XRiTextField();
    SAN7 = new XRiTextField();
    SAN8 = new XRiTextField();
    SAN9 = new XRiTextField();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_105 = new JLabel();
    CVREF = new XRiComboBox();
    OBJ_110 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    popupMenu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1170, 660));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE).addGap(16, 16, 16)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(6, 6, 6)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(3, 3, 3)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(970, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Comptabilisation des ventes");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);
            
            // ======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Plan comptable des ventes"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ======== SCROLLPANE_LISTCV ========
              {
                SCROLLPANE_LISTCV.setComponentPopupMenu(BTD);
                SCROLLPANE_LISTCV.setName("SCROLLPANE_LISTCV");
                
                // ---- HLD01 ----
                HLD01.setComponentPopupMenu(null);
                HLD01.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                HLD01.setName("HLD01");
                SCROLLPANE_LISTCV.setViewportView(HLD01);
              }
              panel2.add(SCROLLPANE_LISTCV);
              SCROLLPANE_LISTCV.setBounds(15, 35, 530, 302);
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(10, 15, 560, 370);
            
            // ======== panel3 ========
            {
              panel3.setBorder(new TitledBorder(""));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- CVCPC ----
              CVCPC.setModel(new DefaultComboBoxModel(new String[] { "Journ\u00e9e", "Semaine", "D\u00e9cade", "Quinzaine", "Mois" }));
              CVCPC.setComponentPopupMenu(BTD);
              CVCPC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CVCPC.setName("CVCPC");
              panel3.add(CVCPC);
              CVCPC.setBounds(240, 120, 93, CVCPC.getPreferredSize().height);
              
              // ---- OBJ_71 ----
              OBJ_71.setText("Etablissement comptable");
              OBJ_71.setName("OBJ_71");
              panel3.add(OBJ_71);
              OBJ_71.setBounds(25, 10, 156, 20);
              
              // ---- OBJ_108 ----
              OBJ_108.setText("Num\u00e9ro CO diff\u00e9rent");
              OBJ_108.setName("OBJ_108");
              panel3.add(OBJ_108);
              OBJ_108.setBounds(25, 35, 156, 20);
              
              // ---- OBJ_72 ----
              OBJ_72.setText("Code journal des ventes");
              OBJ_72.setName("OBJ_72");
              panel3.add(OBJ_72);
              OBJ_72.setBounds(25, 60, 148, 20);
              
              // ---- OBJ_75 ----
              OBJ_75.setText("Compte collectif clients");
              OBJ_75.setName("OBJ_75");
              panel3.add(OBJ_75);
              OBJ_75.setBounds(25, 150, 139, 20);
              
              // ---- OBJ_77 ----
              OBJ_77.setText("Folio en attente");
              OBJ_77.setName("OBJ_77");
              panel3.add(OBJ_77);
              OBJ_77.setBounds(25, 200, 94, 20);
              
              // ---- OBJ_73 ----
              OBJ_73.setText("P\u00e9riodicit\u00e9");
              OBJ_73.setName("OBJ_73");
              panel3.add(OBJ_73);
              OBJ_73.setBounds(25, 125, 67, 20);
              
              // ---- CVCOL ----
              CVCOL.setComponentPopupMenu(null);
              CVCOL.setName("CVCOL");
              panel3.add(CVCOL);
              CVCOL.setBounds(240, 145, 60, CVCOL.getPreferredSize().height);
              
              // ---- CVDIF ----
              CVDIF.setComponentPopupMenu(null);
              CVDIF.setName("CVDIF");
              panel3.add(CVDIF);
              CVDIF.setBounds(240, 30, 36, CVDIF.getPreferredSize().height);
              
              // ---- CVCJV ----
              CVCJV.setComponentPopupMenu(null);
              CVCJV.setName("CVCJV");
              panel3.add(CVCJV);
              CVCJV.setBounds(240, 55, 30, CVCJV.getPreferredSize().height);
              
              // ---- CVATT ----
              CVATT.setComponentPopupMenu(null);
              CVATT.setText("Folio ne attente");
              CVATT.setName("CVATT");
              panel3.add(CVATT);
              CVATT.setBounds(240, 205, 20, CVATT.getPreferredSize().height);
              
              // ======== panel4 ========
              {
                panel4.setBorder(new TitledBorder("Suspension Edition"));
                panel4.setOpaque(false);
                panel4.setName("panel4");
                panel4.setLayout(null);
                
                // ---- OBJ_80 ----
                OBJ_80.setText("Compte-rendu");
                OBJ_80.setName("OBJ_80");
                panel4.add(OBJ_80);
                OBJ_80.setBounds(15, 29, 88, 20);
                
                // ---- CVSEC ----
                CVSEC.setComponentPopupMenu(null);
                CVSEC.setName("CVSEC");
                panel4.add(CVSEC);
                CVSEC.setBounds(230, 25, 20, CVSEC.getPreferredSize().height);
                
                // ---- OBJ_94 ----
                OBJ_94.setText("Edition erreurs");
                OBJ_94.setName("OBJ_94");
                panel4.add(OBJ_94);
                OBJ_94.setBounds(15, 54, 90, 20);
                
                // ---- CVSER ----
                CVSER.setComponentPopupMenu(null);
                CVSER.setName("CVSER");
                panel4.add(CVSER);
                CVSER.setBounds(230, 50, 20, CVSER.getPreferredSize().height);
                
                // ---- WOPT1 ----
                WOPT1.setComponentPopupMenu(null);
                WOPT1.setName("WOPT1");
                panel4.add(WOPT1);
                WOPT1.setBounds(230, 85, 20, WOPT1.getPreferredSize().height);
                
                // ---- OBJ_66 ----
                OBJ_66.setText("Axes Analytiques");
                OBJ_66.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                OBJ_66.setName("OBJ_66");
                OBJ_66.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    OBJ_66ActionPerformed(e);
                  }
                });
                panel4.add(OBJ_66);
                OBJ_66.setBounds(new Rectangle(new Point(10, 80), OBJ_66.getPreferredSize()));
              }
              panel3.add(panel4);
              panel4.setBounds(10, 225, 325, 130);
              
              // ---- CVCPA ----
              CVCPA.setText("Comptabilisation des ventes par anticipation");
              CVCPA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CVCPA.setName("CVCPA");
              CVCPA.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  CVCPAActionPerformed();
                }
              });
              panel3.add(CVCPA);
              CVCPA.setBounds(20, 85, 315, CVCPA.getPreferredSize().height);
              
              // ---- CVETC ----
              CVETC.setName("CVETC");
              panel3.add(CVETC);
              CVETC.setBounds(240, 5, 40, CVETC.getPreferredSize().height);
              
              // ---- OBJ_76 ----
              OBJ_76.setText("Compte pour les acomptes");
              OBJ_76.setName("OBJ_76");
              panel3.add(OBJ_76);
              OBJ_76.setBounds(25, 175, 185, 20);
              
              // ---- CVACC ----
              CVACC.setComponentPopupMenu(null);
              CVACC.setName("CVACC");
              panel3.add(CVACC);
              CVACC.setBounds(240, 170, 60, CVACC.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(580, 20, 345, 370);
            
            // ======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("Codes sections ventes"));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ---- OBJ_96 ----
              OBJ_96.setName("OBJ_96");
              panel5.add(OBJ_96);
              OBJ_96.setBounds(15, 35, 130, OBJ_96.getPreferredSize().height);
              
              // ---- OBJ_106 ----
              OBJ_106.setText("R\u00e9f\u00e9rence");
              OBJ_106.setName("OBJ_106");
              panel5.add(OBJ_106);
              OBJ_106.setBounds(640, 54, 66, 20);
              
              // ---- SAN1 ----
              SAN1.setComponentPopupMenu(null);
              SAN1.setName("SAN1");
              panel5.add(SAN1);
              SAN1.setBounds(165, 50, 44, SAN1.getPreferredSize().height);
              
              // ---- SAN2 ----
              SAN2.setComponentPopupMenu(null);
              SAN2.setName("SAN2");
              panel5.add(SAN2);
              SAN2.setBounds(216, 50, 44, SAN2.getPreferredSize().height);
              
              // ---- SAN3 ----
              SAN3.setComponentPopupMenu(null);
              SAN3.setName("SAN3");
              panel5.add(SAN3);
              SAN3.setBounds(267, 50, 44, SAN3.getPreferredSize().height);
              
              // ---- SAN4 ----
              SAN4.setComponentPopupMenu(null);
              SAN4.setName("SAN4");
              panel5.add(SAN4);
              SAN4.setBounds(318, 50, 44, SAN4.getPreferredSize().height);
              
              // ---- SAN5 ----
              SAN5.setComponentPopupMenu(null);
              SAN5.setName("SAN5");
              panel5.add(SAN5);
              SAN5.setBounds(369, 50, 44, SAN5.getPreferredSize().height);
              
              // ---- SAN6 ----
              SAN6.setComponentPopupMenu(null);
              SAN6.setName("SAN6");
              panel5.add(SAN6);
              SAN6.setBounds(420, 50, 44, SAN6.getPreferredSize().height);
              
              // ---- SAN7 ----
              SAN7.setComponentPopupMenu(null);
              SAN7.setName("SAN7");
              panel5.add(SAN7);
              SAN7.setBounds(471, 50, 44, SAN7.getPreferredSize().height);
              
              // ---- SAN8 ----
              SAN8.setComponentPopupMenu(null);
              SAN8.setName("SAN8");
              panel5.add(SAN8);
              SAN8.setBounds(522, 50, 44, SAN8.getPreferredSize().height);
              
              // ---- SAN9 ----
              SAN9.setComponentPopupMenu(null);
              SAN9.setName("SAN9");
              panel5.add(SAN9);
              SAN9.setBounds(573, 50, 44, SAN9.getPreferredSize().height);
              
              // ---- OBJ_97 ----
              OBJ_97.setText("1");
              OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_97.setName("OBJ_97");
              panel5.add(OBJ_97);
              OBJ_97.setBounds(165, 30, 44, 19);
              
              // ---- OBJ_98 ----
              OBJ_98.setText("2");
              OBJ_98.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_98.setName("OBJ_98");
              panel5.add(OBJ_98);
              OBJ_98.setBounds(216, 30, 44, 19);
              
              // ---- OBJ_99 ----
              OBJ_99.setText("3");
              OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_99.setName("OBJ_99");
              panel5.add(OBJ_99);
              OBJ_99.setBounds(267, 30, 44, 19);
              
              // ---- OBJ_100 ----
              OBJ_100.setText("4");
              OBJ_100.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_100.setName("OBJ_100");
              panel5.add(OBJ_100);
              OBJ_100.setBounds(318, 30, 44, 19);
              
              // ---- OBJ_101 ----
              OBJ_101.setText("5");
              OBJ_101.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_101.setName("OBJ_101");
              panel5.add(OBJ_101);
              OBJ_101.setBounds(369, 30, 44, 19);
              
              // ---- OBJ_102 ----
              OBJ_102.setText("6");
              OBJ_102.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_102.setName("OBJ_102");
              panel5.add(OBJ_102);
              OBJ_102.setBounds(420, 30, 44, 19);
              
              // ---- OBJ_103 ----
              OBJ_103.setText("7");
              OBJ_103.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_103.setName("OBJ_103");
              panel5.add(OBJ_103);
              OBJ_103.setBounds(471, 30, 44, 19);
              
              // ---- OBJ_104 ----
              OBJ_104.setText("8");
              OBJ_104.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_104.setName("OBJ_104");
              panel5.add(OBJ_104);
              OBJ_104.setBounds(522, 30, 44, 19);
              
              // ---- OBJ_105 ----
              OBJ_105.setText("9");
              OBJ_105.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_105.setName("OBJ_105");
              panel5.add(OBJ_105);
              OBJ_105.setBounds(573, 30, 44, 19);
              
              // ---- CVREF ----
              CVREF.setComponentPopupMenu(null);
              CVREF.setModel(new DefaultComboBoxModel(new String[] { " ", "Premier mode de r\u00e8glement", "R\u00e9f\u00e9rence courte",
                  "Etablissement de GVM", "Code magasin", "Code repr\u00e9sentant", "Code affaire du bon", "Code contrat" }));
              CVREF.setName("CVREF");
              panel5.add(CVREF);
              CVREF.setBounds(705, 51, 195, CVREF.getPreferredSize().height);
              
              // ---- OBJ_110 ----
              OBJ_110.setName("OBJ_110");
              panel5.add(OBJ_110);
              OBJ_110.setBounds(new Rectangle(new Point(15, 56), OBJ_110.getPreferredSize()));
              
              // ---- OBJ_81 ----
              OBJ_81.setText("Ecarts de conversion");
              OBJ_81.setName("OBJ_81");
              panel5.add(OBJ_81);
              OBJ_81.setBounds(15, 32, 140, 20);
              
              // ---- OBJ_82 ----
              OBJ_82.setText("Codes sections");
              OBJ_82.setName("OBJ_82");
              panel5.add(OBJ_82);
              OBJ_82.setBounds(15, 54, 135, 20);
            }
            xTitledPanel1ContentContainer.add(panel5);
            panel5.setBounds(10, 395, 915, 85);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 940, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(16, Short.MAX_VALUE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(20, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    
    // ======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");
      
      // ---- menuItem1 ----
      menuItem1.setText("Invite");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LISTCV;
  private XRiTable HLD01;
  private JPanel panel3;
  private XRiComboBox CVCPC;
  private JLabel OBJ_71;
  private JLabel OBJ_108;
  private JLabel OBJ_72;
  private JLabel OBJ_75;
  private JLabel OBJ_77;
  private JLabel OBJ_73;
  private XRiTextField CVCOL;
  private XRiTextField CVDIF;
  private XRiTextField CVCJV;
  private XRiCheckBox CVATT;
  private JPanel panel4;
  private JLabel OBJ_80;
  private XRiCheckBox CVSEC;
  private JLabel OBJ_94;
  private XRiCheckBox CVSER;
  private XRiCheckBox WOPT1;
  private SNBoutonLeger OBJ_66;
  private SNBoutonLeger CVCPA;
  private XRiTextField CVETC;
  private JLabel OBJ_76;
  private XRiTextField CVACC;
  private JPanel panel5;
  private JLabel OBJ_96;
  private JLabel OBJ_106;
  private XRiTextField SAN1;
  private XRiTextField SAN2;
  private XRiTextField SAN3;
  private XRiTextField SAN4;
  private XRiTextField SAN5;
  private XRiTextField SAN6;
  private XRiTextField SAN7;
  private XRiTextField SAN8;
  private XRiTextField SAN9;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_101;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private JLabel OBJ_105;
  private XRiComboBox CVREF;
  private JLabel OBJ_110;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
