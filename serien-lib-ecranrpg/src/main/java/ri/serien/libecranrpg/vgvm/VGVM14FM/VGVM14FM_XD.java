
package ri.serien.libecranrpg.vgvm.VGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM14FM_XD extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LD01_Title = { "TETLD", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 543, };
  // private String[] _LIST_Top=null;
  
  public VGVM14FM_XD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim());
    ETAL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETAL@")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    P14ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P14ETB@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    CLCPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCPL@")).trim());
    CLRUE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLRUE@")).trim());
    CLLOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLLOC@")).trim());
    CLVILR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLVILR@")).trim());
    CLTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTEL@")).trim());
    CLFAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLFAX@")).trim());
    E1CLLP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLP@")).trim());
    CLCDP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCDP@")).trim());
    E1CLLS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLS@")).trim());
    E1NCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NCC@")).trim());
    WNFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNFA@")).trim());
    WDCOX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDCOX@")).trim());
    WDEXP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEXP@")).trim());
    E1MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@")).trim());
    WDHOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDHOM@")).trim());
    WDFAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDFAC@")).trim());
    STATUT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@STATUT@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    E1THTL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1THTL@")).trim());
    TE1TL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TE1TL@")).trim());
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    TOTCDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTCDE@")).trim());
    TOTEXP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTEXP@")).trim());
    ARRHES.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARRHES@")).trim());
    TOTMRG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTMRG@")).trim());
    OBJ_98.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LARRHE@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AREG@")).trim());
    TMRGS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMRGS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    
    CLCDP.setVisible(lexique.isPresent("CLCDP"));
    E1NUM.setVisible(lexique.isPresent("E1NUM"));
    TE1TL.setVisible(lexique.isPresent("TE1TL"));
    E1THTL.setVisible(lexique.isPresent("E1THTL"));
    CLFAX.setVisible(lexique.isPresent("CLFAX"));
    CLTEL.setVisible(lexique.isPresent("CLTEL"));
    CLVILR.setVisible(lexique.isPresent("CLVILR"));
    CLLOC.setVisible(lexique.isPresent("CLLOC"));
    CLRUE.setVisible(lexique.isPresent("CLRUE"));
    CLCPL.setVisible(lexique.isPresent("CLCPL"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _LIST_Top, "1", "Enter");
    LD01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_107ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(16, 89);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_106ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 122);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void LD01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _LIST_Top, "1", "Enter",e);
    if (LD01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    ETAL = new RiZoneSortie();
    OBJ_44 = new JLabel();
    E1NUM = new RiZoneSortie();
    OBJ_45 = new JLabel();
    P14ETB = new RiZoneSortie();
    OBJ_46 = new JLabel();
    E1SUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    CLNOM = new RiZoneSortie();
    CLCPL = new RiZoneSortie();
    CLRUE = new RiZoneSortie();
    CLLOC = new RiZoneSortie();
    CLVILR = new RiZoneSortie();
    CLTEL = new RiZoneSortie();
    CLFAX = new RiZoneSortie();
    OBJ_107 = new JButton();
    E1CLLP = new RiZoneSortie();
    CLCDP = new RiZoneSortie();
    OBJ_54 = new JLabel();
    E1CLLS = new RiZoneSortie();
    SCROLLPANE_LIST = new JScrollPane();
    LD01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xTitledPanel2 = new JXTitledPanel();
    E1NCC = new RiZoneSortie();
    OBJ_60 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_71 = new JLabel();
    WNFA = new RiZoneSortie();
    WDCOX = new RiZoneSortie();
    WDEXP = new RiZoneSortie();
    OBJ_51 = new JLabel();
    E1MAG = new RiZoneSortie();
    WDHOM = new RiZoneSortie();
    WDFAC = new RiZoneSortie();
    STATUT = new RiZoneSortie();
    OBJ_78 = new JLabel();
    MALIB = new RiZoneSortie();
    separator1 = new JSeparator();
    xTitledPanel3 = new JXTitledPanel();
    E1THTL = new RiZoneSortie();
    TE1TL = new RiZoneSortie();
    E1TTC = new RiZoneSortie();
    OBJ_101 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_89 = new JLabel();
    TOTCDE = new RiZoneSortie();
    OBJ_103 = new JLabel();
    TOTEXP = new RiZoneSortie();
    ARRHES = new RiZoneSortie();
    TOTMRG = new RiZoneSortie();
    OBJ_94 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_91 = new JLabel();
    TMRGS = new RiZoneSortie();
    OBJ_106 = new JButton();
    separator2 = new JSeparator();
    BTD1 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITRE@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- ETAL ----
          ETAL.setOpaque(false);
          ETAL.setText("@ETAL@");
          ETAL.setName("ETAL");

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");

          //---- E1NUM ----
          E1NUM.setOpaque(false);
          E1NUM.setText("@E1NUM@");
          E1NUM.setName("E1NUM");

          //---- OBJ_45 ----
          OBJ_45.setText("N\u00b0 Facture");
          OBJ_45.setName("OBJ_45");

          //---- P14ETB ----
          P14ETB.setOpaque(false);
          P14ETB.setText("@P14ETB@");
          P14ETB.setName("P14ETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Etat");
          OBJ_46.setName("OBJ_46");

          //---- E1SUF ----
          E1SUF.setOpaque(false);
          E1SUF.setText("@E1SUF@");
          E1SUF.setName("E1SUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(P14ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(E1NUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(ETAL, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(P14ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(E1NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(ETAL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1040, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1040, 570));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- CLNOM ----
            CLNOM.setComponentPopupMenu(BTD);
            CLNOM.setText("@CLNOM@");
            CLNOM.setName("CLNOM");
            xTitledPanel1ContentContainer.add(CLNOM);
            CLNOM.setBounds(15, 40, 310, CLNOM.getPreferredSize().height);

            //---- CLCPL ----
            CLCPL.setComponentPopupMenu(BTD);
            CLCPL.setText("@CLCPL@");
            CLCPL.setName("CLCPL");
            xTitledPanel1ContentContainer.add(CLCPL);
            CLCPL.setBounds(15, 70, 310, CLCPL.getPreferredSize().height);

            //---- CLRUE ----
            CLRUE.setComponentPopupMenu(BTD);
            CLRUE.setText("@CLRUE@");
            CLRUE.setName("CLRUE");
            xTitledPanel1ContentContainer.add(CLRUE);
            CLRUE.setBounds(15, 100, 310, CLRUE.getPreferredSize().height);

            //---- CLLOC ----
            CLLOC.setComponentPopupMenu(BTD);
            CLLOC.setText("@CLLOC@");
            CLLOC.setName("CLLOC");
            xTitledPanel1ContentContainer.add(CLLOC);
            CLLOC.setBounds(15, 130, 310, CLLOC.getPreferredSize().height);

            //---- CLVILR ----
            CLVILR.setComponentPopupMenu(BTD);
            CLVILR.setText("@CLVILR@");
            CLVILR.setName("CLVILR");
            xTitledPanel1ContentContainer.add(CLVILR);
            CLVILR.setBounds(75, 160, 250, CLVILR.getPreferredSize().height);

            //---- CLTEL ----
            CLTEL.setToolTipText("Extension de la fiche client");
            CLTEL.setComponentPopupMenu(BTD);
            CLTEL.setText("@CLTEL@");
            CLTEL.setName("CLTEL");
            xTitledPanel1ContentContainer.add(CLTEL);
            CLTEL.setBounds(15, 190, 210, CLTEL.getPreferredSize().height);

            //---- CLFAX ----
            CLFAX.setToolTipText("Num\u00e9ro de fax");
            CLFAX.setComponentPopupMenu(BTD);
            CLFAX.setText("@CLFAX@");
            CLFAX.setName("CLFAX");
            xTitledPanel1ContentContainer.add(CLFAX);
            CLFAX.setBounds(15, 220, 210, CLFAX.getPreferredSize().height);

            //---- OBJ_107 ----
            OBJ_107.setText("");
            OBJ_107.setToolTipText("Acces \u00e0 la fiche client");
            OBJ_107.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_107.setName("OBJ_107");
            OBJ_107.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_107ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(OBJ_107);
            OBJ_107.setBounds(285, 195, 40, 40);

            //---- E1CLLP ----
            E1CLLP.setText("@E1CLLP@");
            E1CLLP.setName("E1CLLP");
            xTitledPanel1ContentContainer.add(E1CLLP);
            E1CLLP.setBounds(70, 10, 90, E1CLLP.getPreferredSize().height);

            //---- CLCDP ----
            CLCDP.setComponentPopupMenu(BTD);
            CLCDP.setText("@CLCDP@");
            CLCDP.setName("CLCDP");
            xTitledPanel1ContentContainer.add(CLCDP);
            CLCDP.setBounds(15, 160, 60, CLCDP.getPreferredSize().height);

            //---- OBJ_54 ----
            OBJ_54.setText("Client");
            OBJ_54.setName("OBJ_54");
            xTitledPanel1ContentContainer.add(OBJ_54);
            OBJ_54.setBounds(17, 13, 42, 18);

            //---- E1CLLS ----
            E1CLLS.setText("@E1CLLS@");
            E1CLLS.setName("E1CLLS");
            xTitledPanel1ContentContainer.add(E1CLLS);
            E1CLLS.setBounds(165, 10, 40, E1CLLS.getPreferredSize().height);
          }

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD1);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- LD01 ----
            LD01.setComponentPopupMenu(BTD1);
            LD01.setName("LD01");
            LD01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(LD01);
          }

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Infos diverses");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- E1NCC ----
            E1NCC.setText("@E1NCC@");
            E1NCC.setName("E1NCC");
            xTitledPanel2ContentContainer.add(E1NCC);
            E1NCC.setBounds(320, 70, 90, E1NCC.getPreferredSize().height);

            //---- OBJ_60 ----
            OBJ_60.setText("Homologation");
            OBJ_60.setName("OBJ_60");
            xTitledPanel2ContentContainer.add(OBJ_60);
            OBJ_60.setBounds(15, 132, 95, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("R\u00e9f\u00e9rence client");
            OBJ_68.setName("OBJ_68");
            xTitledPanel2ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(215, 72, 105, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Exp\u00e9dition");
            OBJ_63.setName("OBJ_63");
            xTitledPanel2ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(15, 102, 95, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("Facturation");
            OBJ_65.setName("OBJ_65");
            xTitledPanel2ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(15, 162, 95, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("Cr\u00e9ation");
            OBJ_58.setName("OBJ_58");
            xTitledPanel2ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(15, 72, 95, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("N\u00b0 de facture");
            OBJ_71.setName("OBJ_71");
            xTitledPanel2ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(215, 102, 105, 20);

            //---- WNFA ----
            WNFA.setText("@WNFA@");
            WNFA.setName("WNFA");
            xTitledPanel2ContentContainer.add(WNFA);
            WNFA.setBounds(320, 100, 80, WNFA.getPreferredSize().height);

            //---- WDCOX ----
            WDCOX.setText("@WDCOX@");
            WDCOX.setName("WDCOX");
            xTitledPanel2ContentContainer.add(WDCOX);
            WDCOX.setBounds(110, 70, 65, WDCOX.getPreferredSize().height);

            //---- WDEXP ----
            WDEXP.setText("@WDEXP@");
            WDEXP.setName("WDEXP");
            xTitledPanel2ContentContainer.add(WDEXP);
            WDEXP.setBounds(110, 100, 65, WDEXP.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("Magasin");
            OBJ_51.setName("OBJ_51");
            xTitledPanel2ContentContainer.add(OBJ_51);
            OBJ_51.setBounds(15, 12, 55, 20);

            //---- E1MAG ----
            E1MAG.setText("@E1MAG@");
            E1MAG.setName("E1MAG");
            xTitledPanel2ContentContainer.add(E1MAG);
            E1MAG.setBounds(110, 10, 34, E1MAG.getPreferredSize().height);

            //---- WDHOM ----
            WDHOM.setText("@WDHOM@");
            WDHOM.setName("WDHOM");
            xTitledPanel2ContentContainer.add(WDHOM);
            WDHOM.setBounds(110, 130, 65, WDHOM.getPreferredSize().height);

            //---- WDFAC ----
            WDFAC.setText("@WDFAC@");
            WDFAC.setName("WDFAC");
            xTitledPanel2ContentContainer.add(WDFAC);
            WDFAC.setBounds(110, 160, 65, WDFAC.getPreferredSize().height);

            //---- STATUT ----
            STATUT.setText("@STATUT@");
            STATUT.setName("STATUT");
            xTitledPanel2ContentContainer.add(STATUT);
            STATUT.setBounds(320, 130, 110, STATUT.getPreferredSize().height);

            //---- OBJ_78 ----
            OBJ_78.setText("Statut");
            OBJ_78.setName("OBJ_78");
            xTitledPanel2ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(215, 132, 105, 20);

            //---- MALIB ----
            MALIB.setText("@MALIB@");
            MALIB.setName("MALIB");
            xTitledPanel2ContentContainer.add(MALIB);
            MALIB.setBounds(150, 10, 305, MALIB.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            xTitledPanel2ContentContainer.add(separator1);
            separator1.setBounds(10, 50, 445, 15);
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- E1THTL ----
            E1THTL.setComponentPopupMenu(BTD);
            E1THTL.setHorizontalAlignment(SwingConstants.RIGHT);
            E1THTL.setText("@E1THTL@");
            E1THTL.setName("E1THTL");
            xTitledPanel3ContentContainer.add(E1THTL);
            E1THTL.setBounds(120, 10, 98, E1THTL.getPreferredSize().height);

            //---- TE1TL ----
            TE1TL.setComponentPopupMenu(BTD);
            TE1TL.setHorizontalAlignment(SwingConstants.RIGHT);
            TE1TL.setText("@TE1TL@");
            TE1TL.setName("TE1TL");
            xTitledPanel3ContentContainer.add(TE1TL);
            TE1TL.setBounds(120, 40, 98, TE1TL.getPreferredSize().height);

            //---- E1TTC ----
            E1TTC.setComponentPopupMenu(BTD);
            E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TTC.setText("@E1TTC@");
            E1TTC.setName("E1TTC");
            xTitledPanel3ContentContainer.add(E1TTC);
            E1TTC.setBounds(120, 70, 98, E1TTC.getPreferredSize().height);

            //---- OBJ_101 ----
            OBJ_101.setText("Total TTC");
            OBJ_101.setName("OBJ_101");
            xTitledPanel3ContentContainer.add(OBJ_101);
            OBJ_101.setBounds(15, 72, 75, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("Total TVA");
            OBJ_92.setName("OBJ_92");
            xTitledPanel3ContentContainer.add(OBJ_92);
            OBJ_92.setBounds(15, 42, 75, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("Total H.T");
            OBJ_87.setName("OBJ_87");
            xTitledPanel3ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(15, 12, 75, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("Total commande");
            OBJ_89.setName("OBJ_89");
            xTitledPanel3ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(250, 12, 110, 20);

            //---- TOTCDE ----
            TOTCDE.setHorizontalAlignment(SwingConstants.RIGHT);
            TOTCDE.setText("@TOTCDE@");
            TOTCDE.setName("TOTCDE");
            xTitledPanel3ContentContainer.add(TOTCDE);
            TOTCDE.setBounds(365, 10, 98, TOTCDE.getPreferredSize().height);

            //---- OBJ_103 ----
            OBJ_103.setText("Montant per\u00e7u");
            OBJ_103.setName("OBJ_103");
            xTitledPanel3ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(15, 162, 100, 20);

            //---- TOTEXP ----
            TOTEXP.setHorizontalAlignment(SwingConstants.RIGHT);
            TOTEXP.setText("@TOTEXP@");
            TOTEXP.setName("TOTEXP");
            xTitledPanel3ContentContainer.add(TOTEXP);
            TOTEXP.setBounds(365, 40, 98, TOTEXP.getPreferredSize().height);

            //---- ARRHES ----
            ARRHES.setHorizontalAlignment(SwingConstants.RIGHT);
            ARRHES.setText("@ARRHES@");
            ARRHES.setName("ARRHES");
            xTitledPanel3ContentContainer.add(ARRHES);
            ARRHES.setBounds(120, 130, 98, ARRHES.getPreferredSize().height);

            //---- TOTMRG ----
            TOTMRG.setHorizontalAlignment(SwingConstants.RIGHT);
            TOTMRG.setText("@TOTMRG@");
            TOTMRG.setName("TOTMRG");
            xTitledPanel3ContentContainer.add(TOTMRG);
            TOTMRG.setBounds(120, 160, 98, TOTMRG.getPreferredSize().height);

            //---- OBJ_94 ----
            OBJ_94.setText("Total exp\u00e9di\u00e9");
            OBJ_94.setName("OBJ_94");
            xTitledPanel3ContentContainer.add(OBJ_94);
            OBJ_94.setBounds(250, 44, 110, 16);

            //---- OBJ_98 ----
            OBJ_98.setText("@LARRHE@");
            OBJ_98.setName("OBJ_98");
            xTitledPanel3ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(15, 134, 100, 16);

            //---- OBJ_91 ----
            OBJ_91.setText("@AREG@");
            OBJ_91.setName("OBJ_91");
            xTitledPanel3ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(250, 160, 110, 25);

            //---- TMRGS ----
            TMRGS.setHorizontalAlignment(SwingConstants.RIGHT);
            TMRGS.setText("@TMRGS@");
            TMRGS.setName("TMRGS");
            xTitledPanel3ContentContainer.add(TMRGS);
            TMRGS.setBounds(365, 160, 98, TMRGS.getPreferredSize().height);

            //---- OBJ_106 ----
            OBJ_106.setText("");
            OBJ_106.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_106.setName("OBJ_106");
            OBJ_106.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_106ActionPerformed(e);
              }
            });
            xTitledPanel3ContentContainer.add(OBJ_106);
            OBJ_106.setBounds(470, 152, 40, 40);

            //---- separator2 ----
            separator2.setName("separator2");
            xTitledPanel3ContentContainer.add(separator2);
            separator2.setBounds(10, 110, 500, 15);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 624, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 475, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE))))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup()
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD1 ========
    {
      BTD1.setName("BTD1");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD1.add(CHOISIR);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie ETAL;
  private JLabel OBJ_44;
  private RiZoneSortie E1NUM;
  private JLabel OBJ_45;
  private RiZoneSortie P14ETB;
  private JLabel OBJ_46;
  private RiZoneSortie E1SUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie CLNOM;
  private RiZoneSortie CLCPL;
  private RiZoneSortie CLRUE;
  private RiZoneSortie CLLOC;
  private RiZoneSortie CLVILR;
  private RiZoneSortie CLTEL;
  private RiZoneSortie CLFAX;
  private JButton OBJ_107;
  private RiZoneSortie E1CLLP;
  private RiZoneSortie CLCDP;
  private JLabel OBJ_54;
  private RiZoneSortie E1CLLS;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LD01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie E1NCC;
  private JLabel OBJ_60;
  private JLabel OBJ_68;
  private JLabel OBJ_63;
  private JLabel OBJ_65;
  private JLabel OBJ_58;
  private JLabel OBJ_71;
  private RiZoneSortie WNFA;
  private RiZoneSortie WDCOX;
  private RiZoneSortie WDEXP;
  private JLabel OBJ_51;
  private RiZoneSortie E1MAG;
  private RiZoneSortie WDHOM;
  private RiZoneSortie WDFAC;
  private RiZoneSortie STATUT;
  private JLabel OBJ_78;
  private RiZoneSortie MALIB;
  private JSeparator separator1;
  private JXTitledPanel xTitledPanel3;
  private RiZoneSortie E1THTL;
  private RiZoneSortie TE1TL;
  private RiZoneSortie E1TTC;
  private JLabel OBJ_101;
  private JLabel OBJ_92;
  private JLabel OBJ_87;
  private JLabel OBJ_89;
  private RiZoneSortie TOTCDE;
  private JLabel OBJ_103;
  private RiZoneSortie TOTEXP;
  private RiZoneSortie ARRHES;
  private RiZoneSortie TOTMRG;
  private JLabel OBJ_94;
  private JLabel OBJ_98;
  private JLabel OBJ_91;
  private RiZoneSortie TMRGS;
  private JButton OBJ_106;
  private JSeparator separator2;
  private JPopupMenu BTD1;
  private JMenuItem CHOISIR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
