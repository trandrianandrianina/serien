
package ri.serien.libecranrpg.vgvm.VGVM50FM;
// Nom Fichier: pop_VCGM05FM_FMTERR_367.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class VGVM50FM_F1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM50FM_F1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    this.setBackground(Constantes.COULEUR_F1);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V05F@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
  }
  
  @Override
  public void setData() {
    // String chaine="";
    
    super.setData();
    
    
    // chaine = lexique.HostFieldGetData("V02F").trim();
    
    // affichage du bon panel ++++++++++++++++++++
    
    // Bouton par défaut
    if (lexique.HostFieldGetData("V06F").trim().equals("NON")) {
      setDefaultButton(ANN);
    }
    else {
      setDefaultButton(OK);
    }
    this.setPreferredSize(new Dimension(340, 150));
    this.setTitle("Confirmation");
    OK.setIcon(lexique.chargerImage("images/OK_p.png", true));
    ANN.setIcon(lexique.chargerImage("images/retour_p.png", true));
    // TODO Icones
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("@V03F@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OKActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void ANNActionPerformed() {
    lexique.HostFieldPutData("V06F", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_DemandeConf = new JPanel();
    OBJ_5 = new JLabel();
    OBJ_14 = new JLabel();
    OK = new JButton();
    ANN = new JButton();

    //======== this ========
    setPreferredSize(new Dimension(340, 190));
    setBackground(new Color(90, 90, 90));
    setForeground(Color.black);
    setName("this");
    setLayout(null);

    //======== p_DemandeConf ========
    {
      p_DemandeConf.setPreferredSize(new Dimension(340, 140));
      p_DemandeConf.setBackground(new Color(90, 90, 90));
      p_DemandeConf.setOpaque(false);
      p_DemandeConf.setName("p_DemandeConf");
      p_DemandeConf.setLayout(null);

      //---- OBJ_5 ----
      OBJ_5.setText("@V05F@");
      OBJ_5.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_5.setRequestFocusEnabled(false);
      OBJ_5.setFont(new Font("sansserif", Font.PLAIN, 16));
      OBJ_5.setForeground(Color.white);
      OBJ_5.setName("OBJ_5");
      p_DemandeConf.add(OBJ_5);
      OBJ_5.setBounds(0, 30, 340, 25);

      //---- OBJ_14 ----
      OBJ_14.setText("@V03F@");
      OBJ_14.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_14.setRequestFocusEnabled(false);
      OBJ_14.setForeground(Color.white);
      OBJ_14.setFont(new Font("sansserif", Font.PLAIN, 15));
      OBJ_14.setName("OBJ_14");
      p_DemandeConf.add(OBJ_14);
      OBJ_14.setBounds(0, 5, 340, 25);

      //---- OK ----
      OK.setText("Valider");
      OK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OK.setPreferredSize(new Dimension(110, 19));
      OK.setMinimumSize(new Dimension(110, 1));
      OK.setFont(OK.getFont().deriveFont(OK.getFont().getStyle() | Font.BOLD, OK.getFont().getSize() + 2f));
      OK.setIconTextGap(25);
      OK.setHorizontalAlignment(SwingConstants.LEADING);
      OK.setName("OK");
      OK.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OKActionPerformed();
        }
      });
      p_DemandeConf.add(OK);
      OK.setBounds(75, 60, 190, 40);

      //---- ANN ----
      ANN.setText(" Retour");
      ANN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      ANN.setPreferredSize(new Dimension(120, 19));
      ANN.setMinimumSize(new Dimension(120, 1));
      ANN.setFont(new Font("sansserif", Font.BOLD, 14));
      ANN.setIconTextGap(25);
      ANN.setHorizontalAlignment(SwingConstants.LEADING);
      ANN.setName("ANN");
      ANN.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ANNActionPerformed();
        }
      });
      p_DemandeConf.add(ANN);
      ANN.setBounds(75, 100, 190, 40);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_DemandeConf.getComponentCount(); i++) {
          Rectangle bounds = p_DemandeConf.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_DemandeConf.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_DemandeConf.setMinimumSize(preferredSize);
        p_DemandeConf.setPreferredSize(preferredSize);
      }
    }
    add(p_DemandeConf);
    p_DemandeConf.setBounds(0, 0, p_DemandeConf.getPreferredSize().width, 150);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_DemandeConf;
  private JLabel OBJ_5;
  private JLabel OBJ_14;
  private JButton OK;
  private JButton ANN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
