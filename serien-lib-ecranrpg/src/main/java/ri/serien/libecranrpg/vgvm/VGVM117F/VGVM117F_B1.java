/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgvm.VGVM117F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM117F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM117F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    E1MCI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MCI@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG1@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG2@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG3@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLRG4@")).trim());
    WMTNRG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMTNRG@")).trim());
    E1RDM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RDM@")).trim());
    WRAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riSousMenu7.setVisible(lexique.isTrue("98"));
    if (lexique.isTrue("N66")) {
      OBJ_23.setText("Total à règler");
    }
    else {
      OBJ_23.setText("Total du règlement");
    }
    E1TTC.setVisible(lexique.isTrue("N66"));
    WMTNRG.setVisible(lexique.isTrue("66"));
    
    int numClientLivre = 0;
    if (!lexique.HostFieldGetData("E1CLLP").trim().equals("")) {
      numClientLivre = Integer.parseInt(lexique.HostFieldGetData("E1CLLP"));
    }
    riSousMenu1.setVisible(numClientLivre > 999900);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Réglement"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void checkBox1ActionPerformed(ActionEvent e) {
    checkBox2.setEnabled(!checkBox1.isSelected());
    checkBox3.setEnabled(!checkBox1.isSelected());
    checkBox4.setEnabled(!checkBox1.isSelected());
    E1MRG1.setEnabled(!checkBox1.isSelected());
    if (checkBox1.isSelected()) {
      E1PC1.setText("**");
      E1PC2.setText("");
      E1PC3.setText("");
      E1PC4.setText("");
      E1MRG1.setText("");
    }
  }
  
  private void checkBox2ActionPerformed(ActionEvent e) {
    checkBox1.setEnabled(!checkBox2.isSelected());
    checkBox3.setEnabled(!checkBox2.isSelected());
    checkBox4.setEnabled(!checkBox2.isSelected());
    E1MRG2.setEnabled(!checkBox2.isSelected());
    if (checkBox2.isSelected()) {
      E1PC1.setText("");
      E1PC2.setText("**");
      E1PC3.setText("");
      E1PC4.setText("");
      E1MRG2.setText("");
    }
  }
  
  private void checkBox3ActionPerformed(ActionEvent e) {
    checkBox1.setEnabled(!checkBox3.isSelected());
    checkBox2.setEnabled(!checkBox3.isSelected());
    checkBox4.setEnabled(!checkBox3.isSelected());
    E1MRG3.setEnabled(!checkBox3.isSelected());
    if (checkBox3.isSelected()) {
      E1PC1.setText("");
      E1PC1.setText("");
      E1PC3.setText("**");
      E1PC4.setText("");
      E1MRG3.setText("");
    }
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void checkBox4ActionPerformed(ActionEvent e) {
    checkBox1.setEnabled(!checkBox4.isSelected());
    checkBox2.setEnabled(!checkBox4.isSelected());
    checkBox3.setEnabled(!checkBox4.isSelected());
    E1MRG4.setEnabled(!checkBox4.isSelected());
    if (checkBox4.isSelected()) {
      E1PC1.setText("");
      E1PC1.setText("");
      E1PC3.setText("");
      E1PC4.setText("**");
      E1MRG4.setText("");
    }
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_23 = new JLabel();
    E1TTC = new RiZoneSortie();
    OBJ_25 = new JLabel();
    E1MCI = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    checkBox1 = new JCheckBox();
    checkBox2 = new JCheckBox();
    checkBox3 = new JCheckBox();
    E1MRG1 = new XRiTextField();
    E1MRG2 = new XRiTextField();
    E1MRG3 = new XRiTextField();
    E1RG1 = new XRiTextField();
    E1RG2 = new XRiTextField();
    E1RG3 = new XRiTextField();
    label4 = new RiZoneSortie();
    label5 = new RiZoneSortie();
    label6 = new RiZoneSortie();
    E1PC1 = new XRiTextField();
    E1PC2 = new XRiTextField();
    E1PC3 = new XRiTextField();
    checkBox4 = new JCheckBox();
    E1MRG4 = new XRiTextField();
    E1RG4 = new XRiTextField();
    label7 = new RiZoneSortie();
    E1PC4 = new XRiTextField();
    label8 = new JLabel();
    WMTNRG = new RiZoneSortie();
    E1RDM = new RiZoneSortie();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    WRAR = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 350));
    setFocusable(false);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setFocusable(false);
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Comptant diff\u00e9r\u00e9");
              riSousMenu_bt1.setToolTipText("Cr\u00e9ation d'une fiche client pour un client comptant diff\u00e9r\u00e9");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Gestion des r\u00e9glements");
              riSousMenu_bt6.setToolTipText("Gestion des r\u00e9glements");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("D\u00e9but de r\u00e8glement");
              riSousMenu_bt7.setToolTipText("D\u00e9but de r\u00e8glement");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setFocusable(false);
        p_contenu.setName("p_contenu");
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("R\u00e8glement"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_23 ----
          OBJ_23.setText("Total \u00e0 r\u00e8gler");
          OBJ_23.setFont(OBJ_23.getFont().deriveFont(OBJ_23.getFont().getStyle() | Font.BOLD));
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(260, 190, 115, 24);
          
          // ---- E1TTC ----
          E1TTC.setText("@E1TTC@");
          E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
          E1TTC.setFont(E1TTC.getFont().deriveFont(E1TTC.getFont().getStyle() | Font.BOLD));
          E1TTC.setName("E1TTC");
          panel1.add(E1TTC);
          E1TTC.setBounds(380, 190, 135, E1TTC.getPreferredSize().height);
          
          // ---- OBJ_25 ----
          OBJ_25.setText("Montants re\u00e7us");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD));
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(260, 219, 120, 26);
          
          // ---- E1MCI ----
          E1MCI.setComponentPopupMenu(BTD);
          E1MCI.setText("@E1MCI@");
          E1MCI.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MCI.setName("E1MCI");
          panel1.add(E1MCI);
          E1MCI.setBounds(380, 220, 135, E1MCI.getPreferredSize().height);
          
          // ---- label1 ----
          label1.setText("Libell\u00e9 R\u00e8glement");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 35, 130, 24);
          
          // ---- label2 ----
          label2.setText("MR");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(345, 35, 25, 24);
          
          // ---- label3 ----
          label3.setText("Montant");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(390, 35, 60, 24);
          
          // ---- checkBox1 ----
          checkBox1.setName("checkBox1");
          checkBox1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox1ActionPerformed(e);
            }
          });
          panel1.add(checkBox1);
          checkBox1.setBounds(new Rectangle(new Point(560, 63), checkBox1.getPreferredSize()));
          
          // ---- checkBox2 ----
          checkBox2.setName("checkBox2");
          checkBox2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox2ActionPerformed(e);
            }
          });
          panel1.add(checkBox2);
          checkBox2.setBounds(560, 93, 18, 18);
          
          // ---- checkBox3 ----
          checkBox3.setName("checkBox3");
          checkBox3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox3ActionPerformed(e);
            }
          });
          panel1.add(checkBox3);
          checkBox3.setBounds(560, 123, 18, 18);
          
          // ---- E1MRG1 ----
          E1MRG1.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG1.setName("E1MRG1");
          panel1.add(E1MRG1);
          E1MRG1.setBounds(380, 58, 135, E1MRG1.getPreferredSize().height);
          
          // ---- E1MRG2 ----
          E1MRG2.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG2.setName("E1MRG2");
          panel1.add(E1MRG2);
          E1MRG2.setBounds(380, 88, 135, 28);
          
          // ---- E1MRG3 ----
          E1MRG3.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG3.setName("E1MRG3");
          panel1.add(E1MRG3);
          E1MRG3.setBounds(380, 118, 135, 28);
          
          // ---- E1RG1 ----
          E1RG1.setName("E1RG1");
          panel1.add(E1RG1);
          E1RG1.setBounds(335, 58, 40, E1RG1.getPreferredSize().height);
          
          // ---- E1RG2 ----
          E1RG2.setName("E1RG2");
          panel1.add(E1RG2);
          E1RG2.setBounds(335, 88, 40, 28);
          
          // ---- E1RG3 ----
          E1RG3.setName("E1RG3");
          panel1.add(E1RG3);
          E1RG3.setBounds(335, 118, 40, 28);
          
          // ---- label4 ----
          label4.setText("@WLRG1@");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(15, 60, 310, label4.getPreferredSize().height);
          
          // ---- label5 ----
          label5.setText("@WLRG2@");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(15, 90, 310, label5.getPreferredSize().height);
          
          // ---- label6 ----
          label6.setText("@WLRG3@");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(15, 120, 310, label6.getPreferredSize().height);
          
          // ---- E1PC1 ----
          E1PC1.setName("E1PC1");
          panel1.add(E1PC1);
          E1PC1.setBounds(520, 58, 30, E1PC1.getPreferredSize().height);
          
          // ---- E1PC2 ----
          E1PC2.setName("E1PC2");
          panel1.add(E1PC2);
          E1PC2.setBounds(520, 88, 30, 28);
          
          // ---- E1PC3 ----
          E1PC3.setName("E1PC3");
          panel1.add(E1PC3);
          E1PC3.setBounds(520, 118, 30, 28);
          
          // ---- checkBox4 ----
          checkBox4.setName("checkBox4");
          checkBox4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              checkBox4ActionPerformed(e);
            }
          });
          panel1.add(checkBox4);
          checkBox4.setBounds(new Rectangle(new Point(560, 153), checkBox4.getPreferredSize()));
          
          // ---- E1MRG4 ----
          E1MRG4.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG4.setName("E1MRG4");
          panel1.add(E1MRG4);
          E1MRG4.setBounds(380, 148, 135, E1MRG4.getPreferredSize().height);
          
          // ---- E1RG4 ----
          E1RG4.setName("E1RG4");
          panel1.add(E1RG4);
          E1RG4.setBounds(335, 148, 40, E1RG4.getPreferredSize().height);
          
          // ---- label7 ----
          label7.setText("@WLRG4@");
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(15, 150, 310, label7.getPreferredSize().height);
          
          // ---- E1PC4 ----
          E1PC4.setName("E1PC4");
          panel1.add(E1PC4);
          E1PC4.setBounds(520, 148, 30, E1PC4.getPreferredSize().height);
          
          // ---- label8 ----
          label8.setText("%");
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(525, 35, 20, 24);
          
          // ---- WMTNRG ----
          WMTNRG.setText("@WMTNRG@");
          WMTNRG.setHorizontalAlignment(SwingConstants.RIGHT);
          WMTNRG.setFont(WMTNRG.getFont().deriveFont(WMTNRG.getFont().getStyle() | Font.BOLD));
          WMTNRG.setName("WMTNRG");
          panel1.add(WMTNRG);
          WMTNRG.setBounds(380, 190, 135, WMTNRG.getPreferredSize().height);
          
          // ---- E1RDM ----
          E1RDM.setText("@E1RDM@");
          E1RDM.setHorizontalAlignment(SwingConstants.RIGHT);
          E1RDM.setName("E1RDM");
          panel1.add(E1RDM);
          E1RDM.setBounds(380, 280, 135, E1RDM.getPreferredSize().height);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("A  rendre");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(260, 280, 120, 24);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Reste \u00e0 r\u00e9gler");
          OBJ_29.setFont(OBJ_29.getFont().deriveFont(OBJ_29.getFont().getStyle() | Font.BOLD));
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(260, 250, 120, 24);
          
          // ---- WRAR ----
          WRAR.setText("@WRAR@");
          WRAR.setHorizontalAlignment(SwingConstants.RIGHT);
          WRAR.setName("WRAR");
          panel1.add(WRAR);
          WRAR.setBounds(380, 250, 135, WRAR.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addGap(15, 15, 15).addComponent(panel1, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)));
        p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 324, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    
    // ======== riSousMenu8 ========
    {
      riSousMenu8.setName("riSousMenu8");
      
      // ---- riSousMenu_bt8 ----
      riSousMenu_bt8.setText("Changement de client");
      riSousMenu_bt8.setToolTipText("D\u00e9but de r\u00e8glement");
      riSousMenu_bt8.setName("riSousMenu_bt8");
      riSousMenu_bt8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt8ActionPerformed(e);
        }
      });
      riSousMenu8.add(riSousMenu_bt8);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_23;
  private RiZoneSortie E1TTC;
  private JLabel OBJ_25;
  private RiZoneSortie E1MCI;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JCheckBox checkBox1;
  private JCheckBox checkBox2;
  private JCheckBox checkBox3;
  private XRiTextField E1MRG1;
  private XRiTextField E1MRG2;
  private XRiTextField E1MRG3;
  private XRiTextField E1RG1;
  private XRiTextField E1RG2;
  private XRiTextField E1RG3;
  private RiZoneSortie label4;
  private RiZoneSortie label5;
  private RiZoneSortie label6;
  private XRiTextField E1PC1;
  private XRiTextField E1PC2;
  private XRiTextField E1PC3;
  private JCheckBox checkBox4;
  private XRiTextField E1MRG4;
  private XRiTextField E1RG4;
  private RiZoneSortie label7;
  private XRiTextField E1PC4;
  private JLabel label8;
  private RiZoneSortie WMTNRG;
  private RiZoneSortie E1RDM;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private RiZoneSortie WRAR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
