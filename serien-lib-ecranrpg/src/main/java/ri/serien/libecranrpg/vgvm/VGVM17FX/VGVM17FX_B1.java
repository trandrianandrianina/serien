
package ri.serien.libecranrpg.vgvm.VGVM17FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.devise.IdDevise;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente.SNConditionVenteEtClient;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM17FX_B1 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_MODIFIER = "Modifier";
  private static final String BOUTON_CONSULTER = "Consulter";
  private static final String BOUTON_BLOC_NOTES = "Bloc-notes";
  private String[] TRA_Value = { "A", "T", "F", "G", };
  private String[] Type_Value = { "", "N", "B", "T", "R", "K", "F", "+", "-", };
  private String[] TARIF_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  
  public VGVM17FX_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    TRA.setValeurs(TRA_Value, null);
    T1.setValeurs(Type_Value, null);
    T2.setValeurs(Type_Value, null);
    T3.setValeurs(Type_Value, null);
    T4.setValeurs(Type_Value, null);
    T5.setValeurs(Type_Value, null);
    T6.setValeurs(Type_Value, null);
    T7.setValeurs(Type_Value, null);
    T8.setValeurs(Type_Value, null);
    T9.setValeurs(Type_Value, null);
    T10.setValeurs(Type_Value, null);
    TA01.setValeurs(TARIF_Value, null);
    TA02.setValeurs(TARIF_Value, null);
    TA03.setValeurs(TARIF_Value, null);
    TA04.setValeurs(TARIF_Value, null);
    TA05.setValeurs(TARIF_Value, null);
    TA06.setValeurs(TARIF_Value, null);
    TA07.setValeurs(TARIF_Value, null);
    TA08.setValeurs(TARIF_Value, null);
    TA09.setValeurs(TARIF_Value, null);
    TA10.setValeurs(TARIF_Value, null);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_CONSULTER, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_BLOC_NOTES, 'b', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    scrollPane1.setBackground(SNCharteGraphique.COULEUR_FOND);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    RAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@I2RAT@")).trim());
    pnlQuantite.setTitre(lexique.TranslationTable(interpreteurD.analyseExpression("@ULIB@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    A1RTA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1RTA@")).trim());
    ULBTAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBTAR@")).trim());
    WPRVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRVA@")).trim());
    A1P11X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P11X@")).trim());
    A1K21X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K21X@")).trim());
    A1P21X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P21X@")).trim());
    A1P31X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P31X@")).trim());
    A1P41X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P41X@")).trim());
    A1P51X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P51X@")).trim());
    A1P61X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P61X@")).trim());
    A1K31X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K31X@")).trim());
    A1K41X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K41X@")).trim());
    A1K51X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K51X@")).trim());
    A1K61X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K61X@")).trim());
    A1P12X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P12X@")).trim());
    A1P22X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P22X@")).trim());
    A1P32X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P32X@")).trim());
    A1P42X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P42X@")).trim());
    A1P52X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P52X@")).trim());
    A1P62X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P62X@")).trim());
    A1K22X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K22X@")).trim());
    A1K32X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K32X@")).trim());
    A1K42X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K42X@")).trim());
    A1K52X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K52X@")).trim());
    A1K62X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K62X@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // ++++++++++++++++++++++++++++++++++++++++++
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    snBarreBouton.activerBouton(BOUTON_MODIFIER, isConsultation);
    snBarreBouton.activerBouton(BOUTON_CONSULTER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.RETOURNER_RECHERCHE, isConsultation);
    
    A1UNV.setVisible(lexique.isPresent("UNV"));
    pnlTarif.setVisible(lexique.isTrue("83"));
    pnlTarifDate.setVisible(pnlTarif.isVisible() && lexique.isTrue("N42"));
    lbUniteVente.setVisible(A1UNV.isVisible());
    
    if ((T1.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q1.setEnabled(false);
      V1.setEnabled(false);
      TA01.setEnabled(false);
      R11.setEnabled(false);
      R12.setEnabled(false);
      R13.setEnabled(false);
      R14.setEnabled(false);
      R15.setEnabled(false);
      R16.setEnabled(false);
      C1.setEnabled(false);
      FP01.setEnabled(false);
    }
    
    if ((T2.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q2.setEnabled(false);
      V2.setEnabled(false);
      TA02.setEnabled(false);
      R21.setEnabled(false);
      R22.setEnabled(false);
      R23.setEnabled(false);
      R24.setEnabled(false);
      R25.setEnabled(false);
      R26.setEnabled(false);
      C2.setEnabled(false);
      FP02.setEnabled(false);
    }
    
    if ((T3.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q3.setEnabled(false);
      V3.setEnabled(false);
      TA03.setEnabled(false);
      R31.setEnabled(false);
      R32.setEnabled(false);
      R33.setEnabled(false);
      R34.setEnabled(false);
      R35.setEnabled(false);
      R36.setEnabled(false);
      C3.setEnabled(false);
      FP03.setEnabled(false);
    }
    
    if ((T4.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q4.setEnabled(false);
      V4.setEnabled(false);
      TA04.setEnabled(false);
      R41.setEnabled(false);
      R42.setEnabled(false);
      R43.setEnabled(false);
      R44.setEnabled(false);
      R45.setEnabled(false);
      R46.setEnabled(false);
      C4.setEnabled(false);
      FP04.setEnabled(false);
    }
    
    if ((T5.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q5.setEnabled(false);
      V5.setEnabled(false);
      TA05.setEnabled(false);
      R51.setEnabled(false);
      R52.setEnabled(false);
      R53.setEnabled(false);
      R54.setEnabled(false);
      R55.setEnabled(false);
      R56.setEnabled(false);
      C5.setEnabled(false);
      FP05.setEnabled(false);
    }
    
    if ((T6.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q6.setEnabled(false);
      V6.setEnabled(false);
      TA06.setEnabled(false);
      R61.setEnabled(false);
      R62.setEnabled(false);
      R63.setEnabled(false);
      R64.setEnabled(false);
      R65.setEnabled(false);
      R66.setEnabled(false);
      C6.setEnabled(false);
      FP06.setEnabled(false);
    }
    
    if ((T7.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q7.setEnabled(false);
      V7.setEnabled(false);
      TA07.setEnabled(false);
      R71.setEnabled(false);
      R72.setEnabled(false);
      R73.setEnabled(false);
      R74.setEnabled(false);
      R75.setEnabled(false);
      R76.setEnabled(false);
      C7.setEnabled(false);
      FP07.setEnabled(false);
    }
    
    if ((T8.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q8.setEnabled(false);
      V8.setEnabled(false);
      TA08.setEnabled(false);
      R81.setEnabled(false);
      R82.setEnabled(false);
      R83.setEnabled(false);
      R84.setEnabled(false);
      R85.setEnabled(false);
      R86.setEnabled(false);
      C8.setEnabled(false);
      FP08.setEnabled(false);
    }
    
    if ((T9.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q9.setEnabled(false);
      V9.setEnabled(false);
      TA09.setEnabled(false);
      R91.setEnabled(false);
      R92.setEnabled(false);
      R93.setEnabled(false);
      R94.setEnabled(false);
      R95.setEnabled(false);
      R96.setEnabled(false);
      C9.setEnabled(false);
      FP09.setEnabled(false);
    }
    
    if ((T10.getSelectedIndex() == 0) || (lexique.getMode() == Lexical.MODE_CONSULTATION)) {
      Q10.setEnabled(false);
      V10.setEnabled(false);
      TA10.setEnabled(false);
      R101.setEnabled(false);
      R102.setEnabled(false);
      R103.setEnabled(false);
      R104.setEnabled(false);
      R105.setEnabled(false);
      R106.setEnabled(false);
      C10.setEnabled(false);
      FP10.setEnabled(false);
    }
    
    // Identifiant établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Composant Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(idEtablissement);
    snEtablissement.setEnabled(false);
    
    // Composant condition
    snConditionVente.setSession(getSession());
    snConditionVente.setIdEtablissement(idEtablissement);
    snConditionVente.setSelectionParChampRPG(lexique, "I2CNV");
    snConditionVente.setEnabled(false);
    
    // Composant plage de dates
    snPlageDate.setDateDebutParChampRPG(lexique, "WDEB1X");
    snPlageDate.setDateFinParChampRPG(lexique, "WFIN1X");
    snPlageDate.setEnabled(!isConsultation);
    
    // Composant devise
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(idEtablissement);
    snDevise.charger(false);
    if (!lexique.HostFieldGetData("WDEV").trim().isEmpty()) {
      snDevise.setIdSelection(IdDevise.getInstance(lexique.HostFieldGetData("WDEV")));
    }
    snDevise.setEnabled(!isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      snPlageDate.renseignerChampRPGDebut(lexique, "WDEB1X");
      snPlageDate.renseignerChampRPGFin(lexique, "WFIN1X");
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_BLOC_NOTES)) {
        lexique.HostScreenSendKey(this, "F22");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_CONSULTER)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void T1ActionPerformed(ActionEvent e) {
    if (T1.getSelectedIndex() == 0) {
      Q1.setEnabled(true);
      V1.setEnabled(false);
      TA01.setEnabled(false);
      R11.setEnabled(false);
      R12.setEnabled(false);
      R13.setEnabled(false);
      R14.setEnabled(false);
      R15.setEnabled(false);
      R16.setEnabled(false);
      C1.setEnabled(false);
      FP01.setEnabled(false);
    }
    else if ((Type_Value[T1.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T1.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T1.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T1.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q1.setEnabled(true);
      V1.setEnabled(true);
      TA01.setEnabled(false);
      R11.setEnabled(false);
      R12.setEnabled(false);
      R13.setEnabled(false);
      R14.setEnabled(false);
      R15.setEnabled(false);
      R16.setEnabled(false);
      C1.setEnabled(false);
      FP01.setEnabled(false);
    }
    else {
      Q1.setEnabled(true);
      V1.setEnabled(false);
      TA01.setEnabled(true);
      R11.setEnabled(true);
      R12.setEnabled(true);
      R13.setEnabled(true);
      R14.setEnabled(true);
      R15.setEnabled(true);
      R16.setEnabled(true);
      C1.setEnabled(true);
      FP01.setEnabled(true);
    }
  }
  
  private void T2ActionPerformed(ActionEvent e) {
    if (T2.getSelectedIndex() == 0) {
      Q2.setEnabled(true);
      V2.setEnabled(false);
      TA02.setEnabled(false);
      R21.setEnabled(false);
      R22.setEnabled(false);
      R23.setEnabled(false);
      R24.setEnabled(false);
      R25.setEnabled(false);
      R26.setEnabled(false);
      C2.setEnabled(false);
      FP02.setEnabled(false);
    }
    else if ((Type_Value[T2.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T2.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T2.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T2.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q2.setEnabled(true);
      V2.setEnabled(true);
      TA02.setEnabled(false);
      R21.setEnabled(false);
      R22.setEnabled(false);
      R23.setEnabled(false);
      R24.setEnabled(false);
      R25.setEnabled(false);
      R26.setEnabled(false);
      C2.setEnabled(false);
      FP02.setEnabled(false);
    }
    else {
      Q2.setEnabled(true);
      V2.setEnabled(false);
      TA02.setEnabled(true);
      R21.setEnabled(true);
      R22.setEnabled(true);
      R23.setEnabled(true);
      R24.setEnabled(true);
      R25.setEnabled(true);
      R26.setEnabled(true);
      C2.setEnabled(true);
      FP02.setEnabled(true);
    }
  }
  
  private void T3ActionPerformed(ActionEvent e) {
    if (T3.getSelectedIndex() == 0) {
      Q3.setEnabled(true);
      V3.setEnabled(false);
      TA03.setEnabled(false);
      R31.setEnabled(false);
      R32.setEnabled(false);
      R33.setEnabled(false);
      R34.setEnabled(false);
      R35.setEnabled(false);
      R36.setEnabled(false);
      C3.setEnabled(false);
      FP03.setEnabled(false);
    }
    else if ((Type_Value[T3.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T3.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T3.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T3.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q3.setEnabled(true);
      V3.setEnabled(true);
      TA03.setEnabled(false);
      R31.setEnabled(false);
      R32.setEnabled(false);
      R33.setEnabled(false);
      R34.setEnabled(false);
      R35.setEnabled(false);
      R36.setEnabled(false);
      C3.setEnabled(false);
      FP03.setEnabled(false);
    }
    else {
      Q3.setEnabled(true);
      V3.setEnabled(false);
      TA03.setEnabled(true);
      R31.setEnabled(true);
      R32.setEnabled(true);
      R33.setEnabled(true);
      R34.setEnabled(true);
      R35.setEnabled(true);
      R36.setEnabled(true);
      C3.setEnabled(true);
      FP03.setEnabled(true);
    }
  }
  
  private void T4ActionPerformed(ActionEvent e) {
    if (T4.getSelectedIndex() == 0) {
      Q4.setEnabled(true);
      V4.setEnabled(false);
      TA04.setEnabled(false);
      R41.setEnabled(false);
      R42.setEnabled(false);
      R43.setEnabled(false);
      R44.setEnabled(false);
      R45.setEnabled(false);
      R46.setEnabled(false);
      C4.setEnabled(false);
      FP04.setEnabled(false);
    }
    else if ((Type_Value[T4.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T4.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T4.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T4.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q4.setEnabled(true);
      V4.setEnabled(true);
      TA04.setEnabled(false);
      R41.setEnabled(false);
      R42.setEnabled(false);
      R43.setEnabled(false);
      R44.setEnabled(false);
      R45.setEnabled(false);
      R46.setEnabled(false);
      C4.setEnabled(false);
      FP04.setEnabled(false);
    }
    else {
      Q4.setEnabled(true);
      V4.setEnabled(false);
      TA04.setEnabled(true);
      R41.setEnabled(true);
      R42.setEnabled(true);
      R43.setEnabled(true);
      R44.setEnabled(true);
      R45.setEnabled(true);
      R46.setEnabled(true);
      C4.setEnabled(true);
      FP04.setEnabled(true);
    }
  }
  
  private void T5ActionPerformed(ActionEvent e) {
    if (T5.getSelectedIndex() == 0) {
      Q5.setEnabled(true);
      V5.setEnabled(false);
      TA05.setEnabled(false);
      R51.setEnabled(false);
      R52.setEnabled(false);
      R53.setEnabled(false);
      R54.setEnabled(false);
      R55.setEnabled(false);
      R56.setEnabled(false);
      C5.setEnabled(false);
      FP05.setEnabled(false);
    }
    else if ((Type_Value[T5.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T5.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T5.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T5.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q5.setEnabled(true);
      V5.setEnabled(true);
      TA05.setEnabled(false);
      R51.setEnabled(false);
      R52.setEnabled(false);
      R53.setEnabled(false);
      R54.setEnabled(false);
      R55.setEnabled(false);
      R56.setEnabled(false);
      C5.setEnabled(false);
      FP05.setEnabled(false);
    }
    else {
      Q5.setEnabled(true);
      V5.setEnabled(false);
      TA05.setEnabled(true);
      R51.setEnabled(true);
      R52.setEnabled(true);
      R53.setEnabled(true);
      R54.setEnabled(true);
      R55.setEnabled(true);
      R56.setEnabled(true);
      C5.setEnabled(true);
      FP05.setEnabled(true);
    }
  }
  
  private void T6ActionPerformed(ActionEvent e) {
    if (T6.getSelectedIndex() == 0) {
      Q6.setEnabled(true);
      V6.setEnabled(false);
      TA06.setEnabled(false);
      R61.setEnabled(false);
      R62.setEnabled(false);
      R63.setEnabled(false);
      R64.setEnabled(false);
      R65.setEnabled(false);
      R66.setEnabled(false);
      C6.setEnabled(false);
      FP06.setEnabled(false);
    }
    else if ((Type_Value[T6.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T6.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T6.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T6.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q6.setEnabled(true);
      V6.setEnabled(true);
      TA06.setEnabled(false);
      R61.setEnabled(false);
      R62.setEnabled(false);
      R63.setEnabled(false);
      R64.setEnabled(false);
      R65.setEnabled(false);
      R66.setEnabled(false);
      C6.setEnabled(false);
      FP06.setEnabled(false);
    }
    else {
      Q6.setEnabled(true);
      V6.setEnabled(false);
      TA06.setEnabled(true);
      R61.setEnabled(true);
      R62.setEnabled(true);
      R63.setEnabled(true);
      R64.setEnabled(true);
      R65.setEnabled(true);
      R66.setEnabled(true);
      C6.setEnabled(true);
      FP06.setEnabled(true);
    }
  }
  
  private void T7ActionPerformed(ActionEvent e) {
    if (T7.getSelectedIndex() == 0) {
      Q7.setEnabled(true);
      V7.setEnabled(false);
      TA07.setEnabled(false);
      R71.setEnabled(false);
      R72.setEnabled(false);
      R73.setEnabled(false);
      R74.setEnabled(false);
      R75.setEnabled(false);
      R76.setEnabled(false);
      C7.setEnabled(false);
      FP07.setEnabled(false);
    }
    else if ((Type_Value[T7.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T7.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T7.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T7.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q7.setEnabled(true);
      V7.setEnabled(true);
      TA07.setEnabled(false);
      R71.setEnabled(false);
      R72.setEnabled(false);
      R73.setEnabled(false);
      R74.setEnabled(false);
      R75.setEnabled(false);
      R76.setEnabled(false);
      C7.setEnabled(false);
      FP07.setEnabled(false);
    }
    else {
      Q7.setEnabled(true);
      V7.setEnabled(false);
      TA07.setEnabled(true);
      R71.setEnabled(true);
      R72.setEnabled(true);
      R73.setEnabled(true);
      R74.setEnabled(true);
      R75.setEnabled(true);
      R76.setEnabled(true);
      C7.setEnabled(true);
      FP07.setEnabled(true);
    }
  }
  
  private void T8ActionPerformed(ActionEvent e) {
    if (T8.getSelectedIndex() == 0) {
      Q8.setEnabled(true);
      V8.setEnabled(false);
      TA08.setEnabled(false);
      R81.setEnabled(false);
      R82.setEnabled(false);
      R83.setEnabled(false);
      R84.setEnabled(false);
      R85.setEnabled(false);
      R86.setEnabled(false);
      C8.setEnabled(false);
      FP08.setEnabled(false);
    }
    else if ((Type_Value[T8.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T8.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T8.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T8.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q8.setEnabled(true);
      V8.setEnabled(true);
      TA08.setEnabled(false);
      R81.setEnabled(false);
      R82.setEnabled(false);
      R83.setEnabled(false);
      R84.setEnabled(false);
      R85.setEnabled(false);
      R86.setEnabled(false);
      C8.setEnabled(false);
      FP08.setEnabled(false);
    }
    else {
      Q8.setEnabled(true);
      V8.setEnabled(false);
      TA08.setEnabled(true);
      R81.setEnabled(true);
      R82.setEnabled(true);
      R83.setEnabled(true);
      R84.setEnabled(true);
      R85.setEnabled(true);
      R86.setEnabled(true);
      C8.setEnabled(true);
      FP08.setEnabled(true);
    }
  }
  
  private void T9ActionPerformed(ActionEvent e) {
    if (T9.getSelectedIndex() == 0) {
      Q9.setEnabled(true);
      V9.setEnabled(false);
      TA09.setEnabled(false);
      R91.setEnabled(false);
      R92.setEnabled(false);
      R93.setEnabled(false);
      R94.setEnabled(false);
      R95.setEnabled(false);
      R96.setEnabled(false);
      C9.setEnabled(false);
      FP09.setEnabled(false);
    }
    else if ((Type_Value[T9.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T9.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T9.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T9.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q9.setEnabled(true);
      V9.setEnabled(true);
      TA09.setEnabled(false);
      R91.setEnabled(false);
      R92.setEnabled(false);
      R93.setEnabled(false);
      R94.setEnabled(false);
      R95.setEnabled(false);
      R96.setEnabled(false);
      C9.setEnabled(false);
      FP09.setEnabled(false);
    }
    else {
      Q9.setEnabled(true);
      V9.setEnabled(false);
      TA09.setEnabled(true);
      R91.setEnabled(true);
      R92.setEnabled(true);
      R93.setEnabled(true);
      R94.setEnabled(true);
      R95.setEnabled(true);
      R96.setEnabled(true);
      C9.setEnabled(true);
      FP09.setEnabled(true);
    }
  }
  
  private void T10ActionPerformed(ActionEvent e) {
    if (T10.getSelectedIndex() == 0) {
      Q10.setEnabled(true);
      V10.setEnabled(false);
      TA10.setEnabled(false);
      R101.setEnabled(false);
      R102.setEnabled(false);
      R103.setEnabled(false);
      R104.setEnabled(false);
      R105.setEnabled(false);
      R106.setEnabled(false);
      C10.setEnabled(false);
      FP10.setEnabled(false);
    }
    else if ((Type_Value[T10.getSelectedIndex()].equalsIgnoreCase("N")) || (Type_Value[T10.getSelectedIndex()].equalsIgnoreCase("B"))
        || (Type_Value[T10.getSelectedIndex()].equalsIgnoreCase("+")) || (Type_Value[T10.getSelectedIndex()].equalsIgnoreCase("-"))) {
      Q10.setEnabled(true);
      V10.setEnabled(true);
      TA10.setEnabled(false);
      R101.setEnabled(false);
      R102.setEnabled(false);
      R103.setEnabled(false);
      R104.setEnabled(false);
      R105.setEnabled(false);
      R106.setEnabled(false);
      C10.setEnabled(false);
      FP10.setEnabled(false);
    }
    else {
      Q10.setEnabled(true);
      V10.setEnabled(false);
      TA10.setEnabled(true);
      R101.setEnabled(true);
      R102.setEnabled(true);
      R103.setEnabled(true);
      R104.setEnabled(true);
      R105.setEnabled(true);
      R106.setEnabled(true);
      C10.setEnabled(true);
      FP10.setEnabled(true);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlInfos = new SNPanel();
    lbCondition = new SNLabelChamp();
    snConditionVente = new SNConditionVenteEtClient();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbTypeRattachement = new SNLabelChamp();
    TRA = new XRiComboBox();
    lbRattachement = new SNLabelChamp();
    RAT = new SNTexte();
    pnlGeneral = new SNPanel();
    lbReference = new SNLabelChamp();
    W1REF = new XRiTextField();
    lbPeriode = new SNLabelChamp();
    snPlageDate = new SNPlageDate();
    lbUniteVente = new SNLabelChamp();
    A1UNV = new SNTexte();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    pnlQuantite = new SNPanelTitre();
    scrollPane1 = new JScrollPane();
    pnlQuantites = new SNPanel();
    lbQuantite = new SNLabelUnite();
    lbType = new SNLabelUnite();
    lbValeur = new SNLabelUnite();
    lbTarif = new SNLabelUnite();
    lbRemises = new SNLabelUnite();
    lbCoeff = new SNLabelUnite();
    lbFormule = new SNLabelUnite();
    Q1 = new XRiTextField();
    T1 = new XRiComboBox();
    V1 = new XRiTextField();
    TA01 = new XRiComboBox();
    pnlRemises1 = new SNPanel();
    R11 = new XRiTextField();
    R12 = new XRiTextField();
    R13 = new XRiTextField();
    R14 = new XRiTextField();
    R15 = new XRiTextField();
    R16 = new XRiTextField();
    C1 = new XRiTextField();
    FP01 = new XRiTextField();
    Q2 = new XRiTextField();
    T2 = new XRiComboBox();
    V2 = new XRiTextField();
    TA02 = new XRiComboBox();
    pnlRemises2 = new SNPanel();
    R21 = new XRiTextField();
    R22 = new XRiTextField();
    R23 = new XRiTextField();
    R24 = new XRiTextField();
    R25 = new XRiTextField();
    R26 = new XRiTextField();
    C2 = new XRiTextField();
    FP02 = new XRiTextField();
    Q3 = new XRiTextField();
    T3 = new XRiComboBox();
    V3 = new XRiTextField();
    TA03 = new XRiComboBox();
    pnlRemises3 = new SNPanel();
    R31 = new XRiTextField();
    R32 = new XRiTextField();
    R33 = new XRiTextField();
    R34 = new XRiTextField();
    R35 = new XRiTextField();
    R36 = new XRiTextField();
    C3 = new XRiTextField();
    FP03 = new XRiTextField();
    Q4 = new XRiTextField();
    T4 = new XRiComboBox();
    V4 = new XRiTextField();
    TA04 = new XRiComboBox();
    pnlRemises4 = new SNPanel();
    R41 = new XRiTextField();
    R42 = new XRiTextField();
    R43 = new XRiTextField();
    R44 = new XRiTextField();
    R45 = new XRiTextField();
    R46 = new XRiTextField();
    C4 = new XRiTextField();
    FP04 = new XRiTextField();
    Q5 = new XRiTextField();
    T5 = new XRiComboBox();
    V5 = new XRiTextField();
    TA05 = new XRiComboBox();
    pnlRemises5 = new SNPanel();
    R51 = new XRiTextField();
    R52 = new XRiTextField();
    R53 = new XRiTextField();
    R54 = new XRiTextField();
    R55 = new XRiTextField();
    R56 = new XRiTextField();
    C5 = new XRiTextField();
    FP05 = new XRiTextField();
    Q6 = new XRiTextField();
    T6 = new XRiComboBox();
    V6 = new XRiTextField();
    TA06 = new XRiComboBox();
    pnlRemises6 = new SNPanel();
    R61 = new XRiTextField();
    R62 = new XRiTextField();
    R63 = new XRiTextField();
    R64 = new XRiTextField();
    R65 = new XRiTextField();
    R66 = new XRiTextField();
    C6 = new XRiTextField();
    FP06 = new XRiTextField();
    Q7 = new XRiTextField();
    T7 = new XRiComboBox();
    V7 = new XRiTextField();
    TA07 = new XRiComboBox();
    pnlRemises7 = new SNPanel();
    R71 = new XRiTextField();
    R72 = new XRiTextField();
    R73 = new XRiTextField();
    R74 = new XRiTextField();
    R75 = new XRiTextField();
    R76 = new XRiTextField();
    C7 = new XRiTextField();
    FP07 = new XRiTextField();
    Q8 = new XRiTextField();
    T8 = new XRiComboBox();
    V8 = new XRiTextField();
    TA08 = new XRiComboBox();
    pnlRemises8 = new SNPanel();
    R81 = new XRiTextField();
    R82 = new XRiTextField();
    R83 = new XRiTextField();
    R84 = new XRiTextField();
    R85 = new XRiTextField();
    R86 = new XRiTextField();
    C8 = new XRiTextField();
    FP08 = new XRiTextField();
    Q9 = new XRiTextField();
    T9 = new XRiComboBox();
    V9 = new XRiTextField();
    TA09 = new XRiComboBox();
    pnlRemises9 = new SNPanel();
    R91 = new XRiTextField();
    R92 = new XRiTextField();
    R93 = new XRiTextField();
    R94 = new XRiTextField();
    R95 = new XRiTextField();
    R96 = new XRiTextField();
    C9 = new XRiTextField();
    FP09 = new XRiTextField();
    Q10 = new XRiTextField();
    T10 = new XRiComboBox();
    V10 = new XRiTextField();
    TA10 = new XRiComboBox();
    pnlRemises10 = new SNPanel();
    R101 = new XRiTextField();
    R102 = new XRiTextField();
    R103 = new XRiTextField();
    R104 = new XRiTextField();
    R105 = new XRiTextField();
    R106 = new XRiTextField();
    C10 = new XRiTextField();
    FP10 = new XRiTextField();
    pnlTarif = new SNPanelTitre();
    pnlRefTarif = new SNPanel();
    sNLabelChamp11 = new SNLabelChamp();
    WPRVA = new SNTexte();
    lbReference2 = new SNLabelChamp();
    A1RTA = new SNTexte();
    ULBTAR = new SNTexte();
    pnlTarifEnCours = new SNPanel();
    sNLabelChamp9 = new SNLabelChamp();
    sNLabelChamp5 = new SNLabelChamp();
    A1P11X = new SNTexte();
    A1P21X = new SNTexte();
    A1P31X = new SNTexte();
    A1P41X = new SNTexte();
    A1P51X = new SNTexte();
    A1P61X = new SNTexte();
    sNLabelChamp6 = new SNLabelChamp();
    A1K21X = new SNTexte();
    A1K31X = new SNTexte();
    A1K41X = new SNTexte();
    A1K51X = new SNTexte();
    A1K61X = new SNTexte();
    pnlTarifDate = new SNPanel();
    sNLabelChamp10 = new SNLabelChamp();
    sNLabelChamp7 = new SNLabelChamp();
    A1P12X = new SNTexte();
    A1P22X = new SNTexte();
    A1P32X = new SNTexte();
    A1P42X = new SNTexte();
    A1P52X = new SNTexte();
    A1P62X = new SNTexte();
    sNLabelChamp8 = new SNLabelChamp();
    A1K22X = new SNTexte();
    A1K32X = new SNTexte();
    A1K42X = new SNTexte();
    A1K52X = new SNTexte();
    A1K62X = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Conditions quantitatives");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
      
      // ======== pnlInfos ========
      {
        pnlInfos.setName("pnlInfos");
        pnlInfos.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInfos.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInfos.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlInfos.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInfos.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCondition ----
        lbCondition.setText("Condition");
        lbCondition.setMaximumSize(new Dimension(100, 30));
        lbCondition.setMinimumSize(new Dimension(100, 30));
        lbCondition.setPreferredSize(new Dimension(100, 30));
        lbCondition.setName("lbCondition");
        pnlInfos.add(lbCondition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snConditionVente ----
        snConditionVente.setName("snConditionVente");
        pnlInfos.add(snConditionVente, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlInfos.add(lbEtablissement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setEnabled(false);
        snEtablissement.setName("snEtablissement");
        pnlInfos.add(snEtablissement, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTypeRattachement ----
        lbTypeRattachement.setText("Type de rattachement");
        lbTypeRattachement.setName("lbTypeRattachement");
        pnlInfos.add(lbTypeRattachement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- TRA ----
        TRA.setModel(
            new DefaultComboBoxModel(new String[] { "\u00e0 un article", "\u00e0 un tarif", "\u00e0 une famille", "\u00e0 un groupe" }));
        TRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TRA.setFont(new Font("sansserif", Font.PLAIN, 14));
        TRA.setBackground(Color.white);
        TRA.setEnabled(false);
        TRA.setName("TRA");
        pnlInfos.add(TRA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbRattachement ----
        lbRattachement.setText("Rattachement");
        lbRattachement.setName("lbRattachement");
        pnlInfos.add(lbRattachement, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- RAT ----
        RAT.setOpaque(false);
        RAT.setText("@I2RAT@");
        RAT.setEnabled(false);
        RAT.setName("RAT");
        pnlInfos.add(RAT, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInfos,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlGeneral ========
      {
        pnlGeneral.setName("pnlGeneral");
        pnlGeneral.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbReference ----
        lbReference.setText("R\u00e9f\u00e9rence");
        lbReference.setName("lbReference");
        pnlGeneral.add(lbReference, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- W1REF ----
        W1REF.setMinimumSize(new Dimension(310, 28));
        W1REF.setPreferredSize(new Dimension(310, 28));
        W1REF.setName("W1REF");
        pnlGeneral.add(W1REF, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPeriode ----
        lbPeriode.setText("P\u00e9riode");
        lbPeriode.setName("lbPeriode");
        pnlGeneral.add(lbPeriode, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snPlageDate ----
        snPlageDate.setName("snPlageDate");
        pnlGeneral.add(snPlageDate, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbUniteVente ----
        lbUniteVente.setText("Unit\u00e9 de vente");
        lbUniteVente.setName("lbUniteVente");
        pnlGeneral.add(lbUniteVente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- A1UNV ----
        A1UNV.setText("@A1UNV@");
        A1UNV.setEnabled(false);
        A1UNV.setName("A1UNV");
        pnlGeneral.add(A1UNV, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDevise ----
        lbDevise.setText("Devise");
        lbDevise.setName("lbDevise");
        pnlGeneral.add(lbDevise, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snDevise ----
        snDevise.setName("snDevise");
        pnlGeneral.add(snDevise, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGeneral,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlQuantite ========
      {
        pnlQuantite.setTitre("@ULIB@");
        pnlQuantite.setName("pnlQuantite");
        pnlQuantite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlQuantite.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlQuantite.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlQuantite.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setBackground(new Color(239, 239, 222));
          scrollPane1.setOpaque(true);
          scrollPane1.setName("scrollPane1");
          
          // ======== pnlQuantites ========
          {
            pnlQuantites.setBackground(new Color(239, 239, 222));
            pnlQuantites.setOpaque(true);
            pnlQuantites.setName("pnlQuantites");
            pnlQuantites.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlQuantites.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlQuantites.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlQuantites.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlQuantites.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbQuantite ----
            lbQuantite.setText("Quantit\u00e9");
            lbQuantite.setMaximumSize(new Dimension(110, 30));
            lbQuantite.setMinimumSize(new Dimension(110, 30));
            lbQuantite.setPreferredSize(new Dimension(110, 30));
            lbQuantite.setName("lbQuantite");
            pnlQuantites.add(lbQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbType ----
            lbType.setText("Type");
            lbType.setMaximumSize(new Dimension(160, 30));
            lbType.setMinimumSize(new Dimension(160, 30));
            lbType.setPreferredSize(new Dimension(160, 30));
            lbType.setName("lbType");
            pnlQuantites.add(lbType, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbValeur ----
            lbValeur.setText("Valeur");
            lbValeur.setMaximumSize(new Dimension(110, 30));
            lbValeur.setMinimumSize(new Dimension(110, 30));
            lbValeur.setPreferredSize(new Dimension(110, 30));
            lbValeur.setName("lbValeur");
            pnlQuantites.add(lbValeur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbTarif ----
            lbTarif.setText("Tarif");
            lbTarif.setMaximumSize(new Dimension(50, 30));
            lbTarif.setMinimumSize(new Dimension(50, 30));
            lbTarif.setPreferredSize(new Dimension(50, 30));
            lbTarif.setName("lbTarif");
            pnlQuantites.add(lbTarif, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbRemises ----
            lbRemises.setText("Remises");
            lbRemises.setMaximumSize(new Dimension(380, 30));
            lbRemises.setMinimumSize(new Dimension(380, 30));
            lbRemises.setPreferredSize(new Dimension(380, 30));
            lbRemises.setName("lbRemises");
            pnlQuantites.add(lbRemises, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbCoeff ----
            lbCoeff.setText("Coefficient");
            lbCoeff.setMaximumSize(new Dimension(80, 30));
            lbCoeff.setMinimumSize(new Dimension(80, 30));
            lbCoeff.setPreferredSize(new Dimension(80, 30));
            lbCoeff.setName("lbCoeff");
            pnlQuantites.add(lbCoeff, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbFormule ----
            lbFormule.setText("Formule");
            lbFormule.setMaximumSize(new Dimension(60, 30));
            lbFormule.setMinimumSize(new Dimension(60, 30));
            lbFormule.setPreferredSize(new Dimension(60, 30));
            lbFormule.setName("lbFormule");
            pnlQuantites.add(lbFormule, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q1 ----
            Q1.setHorizontalAlignment(SwingConstants.RIGHT);
            Q1.setMinimumSize(new Dimension(110, 30));
            Q1.setMaximumSize(new Dimension(110, 30));
            Q1.setPreferredSize(new Dimension(110, 30));
            Q1.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q1.setName("Q1");
            pnlQuantites.add(Q1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T1 ----
            T1.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T1.setComponentPopupMenu(null);
            T1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T1.setPreferredSize(new Dimension(160, 30));
            T1.setMinimumSize(new Dimension(160, 30));
            T1.setMaximumSize(new Dimension(160, 30));
            T1.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1.setBackground(Color.white);
            T1.setName("T1");
            T1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T1ActionPerformed(e);
              }
            });
            pnlQuantites.add(T1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V1 ----
            V1.setHorizontalAlignment(SwingConstants.RIGHT);
            V1.setMinimumSize(new Dimension(110, 30));
            V1.setMaximumSize(new Dimension(110, 30));
            V1.setPreferredSize(new Dimension(110, 30));
            V1.setFont(new Font("sansserif", Font.PLAIN, 14));
            V1.setName("V1");
            pnlQuantites.add(V1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA01 ----
            TA01.setComponentPopupMenu(null);
            TA01.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA01.setMinimumSize(new Dimension(50, 30));
            TA01.setPreferredSize(new Dimension(50, 30));
            TA01.setMaximumSize(new Dimension(50, 30));
            TA01.setBackground(Color.white);
            TA01.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA01.setName("TA01");
            pnlQuantites.add(TA01, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises1 ========
            {
              pnlRemises1.setName("pnlRemises1");
              pnlRemises1.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R11 ----
              R11.setHorizontalAlignment(SwingConstants.RIGHT);
              R11.setMinimumSize(new Dimension(60, 30));
              R11.setMaximumSize(new Dimension(60, 30));
              R11.setPreferredSize(new Dimension(60, 30));
              R11.setFont(new Font("sansserif", Font.PLAIN, 14));
              R11.setName("R11");
              pnlRemises1.add(R11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R12 ----
              R12.setHorizontalAlignment(SwingConstants.RIGHT);
              R12.setMinimumSize(new Dimension(60, 30));
              R12.setMaximumSize(new Dimension(60, 30));
              R12.setPreferredSize(new Dimension(60, 30));
              R12.setFont(new Font("sansserif", Font.PLAIN, 14));
              R12.setName("R12");
              pnlRemises1.add(R12, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R13 ----
              R13.setHorizontalAlignment(SwingConstants.RIGHT);
              R13.setMinimumSize(new Dimension(60, 30));
              R13.setMaximumSize(new Dimension(60, 30));
              R13.setPreferredSize(new Dimension(60, 30));
              R13.setFont(new Font("sansserif", Font.PLAIN, 14));
              R13.setName("R13");
              pnlRemises1.add(R13, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R14 ----
              R14.setHorizontalAlignment(SwingConstants.RIGHT);
              R14.setMinimumSize(new Dimension(60, 30));
              R14.setMaximumSize(new Dimension(60, 30));
              R14.setPreferredSize(new Dimension(60, 30));
              R14.setFont(new Font("sansserif", Font.PLAIN, 14));
              R14.setName("R14");
              pnlRemises1.add(R14, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R15 ----
              R15.setHorizontalAlignment(SwingConstants.RIGHT);
              R15.setMinimumSize(new Dimension(60, 30));
              R15.setMaximumSize(new Dimension(60, 30));
              R15.setPreferredSize(new Dimension(60, 30));
              R15.setFont(new Font("sansserif", Font.PLAIN, 14));
              R15.setName("R15");
              pnlRemises1.add(R15, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R16 ----
              R16.setHorizontalAlignment(SwingConstants.RIGHT);
              R16.setMinimumSize(new Dimension(60, 30));
              R16.setMaximumSize(new Dimension(60, 30));
              R16.setPreferredSize(new Dimension(60, 30));
              R16.setFont(new Font("sansserif", Font.PLAIN, 14));
              R16.setName("R16");
              pnlRemises1.add(R16, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises1, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C1 ----
            C1.setComponentPopupMenu(null);
            C1.setHorizontalAlignment(SwingConstants.RIGHT);
            C1.setMaximumSize(new Dimension(80, 30));
            C1.setMinimumSize(new Dimension(80, 30));
            C1.setPreferredSize(new Dimension(80, 30));
            C1.setFont(new Font("sansserif", Font.PLAIN, 14));
            C1.setName("C1");
            pnlQuantites.add(C1, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP01 ----
            FP01.setComponentPopupMenu(null);
            FP01.setHorizontalAlignment(SwingConstants.LEFT);
            FP01.setMaximumSize(new Dimension(60, 30));
            FP01.setMinimumSize(new Dimension(60, 30));
            FP01.setPreferredSize(new Dimension(60, 30));
            FP01.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP01.setName("FP01");
            pnlQuantites.add(FP01, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q2 ----
            Q2.setHorizontalAlignment(SwingConstants.RIGHT);
            Q2.setMaximumSize(new Dimension(110, 30));
            Q2.setMinimumSize(new Dimension(110, 30));
            Q2.setPreferredSize(new Dimension(110, 30));
            Q2.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q2.setName("Q2");
            pnlQuantites.add(Q2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T2 ----
            T2.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T2.setComponentPopupMenu(null);
            T2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T2.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2.setBackground(Color.white);
            T2.setName("T2");
            T2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T2ActionPerformed(e);
              }
            });
            pnlQuantites.add(T2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V2 ----
            V2.setHorizontalAlignment(SwingConstants.RIGHT);
            V2.setMaximumSize(new Dimension(110, 30));
            V2.setMinimumSize(new Dimension(110, 30));
            V2.setPreferredSize(new Dimension(110, 30));
            V2.setFont(new Font("sansserif", Font.PLAIN, 14));
            V2.setName("V2");
            pnlQuantites.add(V2, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA02 ----
            TA02.setComponentPopupMenu(null);
            TA02.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA02.setBackground(Color.white);
            TA02.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA02.setName("TA02");
            pnlQuantites.add(TA02, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises2 ========
            {
              pnlRemises2.setName("pnlRemises2");
              pnlRemises2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R21 ----
              R21.setHorizontalAlignment(SwingConstants.RIGHT);
              R21.setMinimumSize(new Dimension(60, 30));
              R21.setMaximumSize(new Dimension(60, 30));
              R21.setPreferredSize(new Dimension(60, 30));
              R21.setFont(new Font("sansserif", Font.PLAIN, 14));
              R21.setName("R21");
              pnlRemises2.add(R21, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R22 ----
              R22.setHorizontalAlignment(SwingConstants.RIGHT);
              R22.setMinimumSize(new Dimension(60, 30));
              R22.setMaximumSize(new Dimension(60, 30));
              R22.setPreferredSize(new Dimension(60, 30));
              R22.setFont(new Font("sansserif", Font.PLAIN, 14));
              R22.setName("R22");
              pnlRemises2.add(R22, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R23 ----
              R23.setHorizontalAlignment(SwingConstants.RIGHT);
              R23.setMinimumSize(new Dimension(60, 30));
              R23.setMaximumSize(new Dimension(60, 30));
              R23.setPreferredSize(new Dimension(60, 30));
              R23.setFont(new Font("sansserif", Font.PLAIN, 14));
              R23.setName("R23");
              pnlRemises2.add(R23, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R24 ----
              R24.setHorizontalAlignment(SwingConstants.RIGHT);
              R24.setMinimumSize(new Dimension(60, 30));
              R24.setMaximumSize(new Dimension(60, 30));
              R24.setPreferredSize(new Dimension(60, 30));
              R24.setFont(new Font("sansserif", Font.PLAIN, 14));
              R24.setName("R24");
              pnlRemises2.add(R24, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R25 ----
              R25.setHorizontalAlignment(SwingConstants.RIGHT);
              R25.setMinimumSize(new Dimension(60, 30));
              R25.setMaximumSize(new Dimension(60, 30));
              R25.setPreferredSize(new Dimension(60, 30));
              R25.setFont(new Font("sansserif", Font.PLAIN, 14));
              R25.setName("R25");
              pnlRemises2.add(R25, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R26 ----
              R26.setHorizontalAlignment(SwingConstants.RIGHT);
              R26.setMinimumSize(new Dimension(60, 30));
              R26.setMaximumSize(new Dimension(60, 30));
              R26.setPreferredSize(new Dimension(60, 30));
              R26.setFont(new Font("sansserif", Font.PLAIN, 14));
              R26.setName("R26");
              pnlRemises2.add(R26, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises2, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C2 ----
            C2.setComponentPopupMenu(null);
            C2.setHorizontalAlignment(SwingConstants.RIGHT);
            C2.setMinimumSize(new Dimension(80, 30));
            C2.setMaximumSize(new Dimension(80, 30));
            C2.setPreferredSize(new Dimension(80, 30));
            C2.setFont(new Font("sansserif", Font.PLAIN, 14));
            C2.setName("C2");
            pnlQuantites.add(C2, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP02 ----
            FP02.setComponentPopupMenu(null);
            FP02.setHorizontalAlignment(SwingConstants.LEFT);
            FP02.setMinimumSize(new Dimension(60, 30));
            FP02.setMaximumSize(new Dimension(60, 30));
            FP02.setPreferredSize(new Dimension(60, 30));
            FP02.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP02.setName("FP02");
            pnlQuantites.add(FP02, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q3 ----
            Q3.setHorizontalAlignment(SwingConstants.RIGHT);
            Q3.setMaximumSize(new Dimension(110, 30));
            Q3.setMinimumSize(new Dimension(110, 30));
            Q3.setPreferredSize(new Dimension(110, 30));
            Q3.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q3.setName("Q3");
            pnlQuantites.add(Q3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T3 ----
            T3.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T3.setComponentPopupMenu(null);
            T3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T3.setFont(new Font("sansserif", Font.PLAIN, 14));
            T3.setBackground(Color.white);
            T3.setName("T3");
            T3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T3ActionPerformed(e);
              }
            });
            pnlQuantites.add(T3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V3 ----
            V3.setHorizontalAlignment(SwingConstants.RIGHT);
            V3.setMaximumSize(new Dimension(110, 30));
            V3.setMinimumSize(new Dimension(110, 30));
            V3.setPreferredSize(new Dimension(110, 30));
            V3.setFont(new Font("sansserif", Font.PLAIN, 14));
            V3.setName("V3");
            pnlQuantites.add(V3, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA03 ----
            TA03.setComponentPopupMenu(null);
            TA03.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA03.setBackground(Color.white);
            TA03.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA03.setName("TA03");
            pnlQuantites.add(TA03, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises3 ========
            {
              pnlRemises3.setName("pnlRemises3");
              pnlRemises3.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises3.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R31 ----
              R31.setHorizontalAlignment(SwingConstants.RIGHT);
              R31.setMinimumSize(new Dimension(60, 30));
              R31.setMaximumSize(new Dimension(60, 30));
              R31.setPreferredSize(new Dimension(60, 30));
              R31.setFont(new Font("sansserif", Font.PLAIN, 14));
              R31.setName("R31");
              pnlRemises3.add(R31, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R32 ----
              R32.setHorizontalAlignment(SwingConstants.RIGHT);
              R32.setMinimumSize(new Dimension(60, 30));
              R32.setMaximumSize(new Dimension(60, 30));
              R32.setPreferredSize(new Dimension(60, 30));
              R32.setFont(new Font("sansserif", Font.PLAIN, 14));
              R32.setName("R32");
              pnlRemises3.add(R32, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R33 ----
              R33.setHorizontalAlignment(SwingConstants.RIGHT);
              R33.setMinimumSize(new Dimension(60, 30));
              R33.setMaximumSize(new Dimension(60, 30));
              R33.setPreferredSize(new Dimension(60, 30));
              R33.setFont(new Font("sansserif", Font.PLAIN, 14));
              R33.setName("R33");
              pnlRemises3.add(R33, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R34 ----
              R34.setHorizontalAlignment(SwingConstants.RIGHT);
              R34.setMinimumSize(new Dimension(60, 30));
              R34.setMaximumSize(new Dimension(60, 30));
              R34.setPreferredSize(new Dimension(60, 30));
              R34.setFont(new Font("sansserif", Font.PLAIN, 14));
              R34.setName("R34");
              pnlRemises3.add(R34, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R35 ----
              R35.setHorizontalAlignment(SwingConstants.RIGHT);
              R35.setMinimumSize(new Dimension(60, 30));
              R35.setMaximumSize(new Dimension(60, 30));
              R35.setPreferredSize(new Dimension(60, 30));
              R35.setFont(new Font("sansserif", Font.PLAIN, 14));
              R35.setName("R35");
              pnlRemises3.add(R35, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R36 ----
              R36.setHorizontalAlignment(SwingConstants.RIGHT);
              R36.setMinimumSize(new Dimension(60, 30));
              R36.setMaximumSize(new Dimension(60, 30));
              R36.setPreferredSize(new Dimension(60, 30));
              R36.setFont(new Font("sansserif", Font.PLAIN, 14));
              R36.setName("R36");
              pnlRemises3.add(R36, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises3, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C3 ----
            C3.setComponentPopupMenu(null);
            C3.setHorizontalAlignment(SwingConstants.RIGHT);
            C3.setMinimumSize(new Dimension(80, 30));
            C3.setMaximumSize(new Dimension(80, 30));
            C3.setPreferredSize(new Dimension(80, 30));
            C3.setFont(new Font("sansserif", Font.PLAIN, 14));
            C3.setName("C3");
            pnlQuantites.add(C3, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP03 ----
            FP03.setComponentPopupMenu(null);
            FP03.setHorizontalAlignment(SwingConstants.LEFT);
            FP03.setMinimumSize(new Dimension(60, 30));
            FP03.setMaximumSize(new Dimension(60, 30));
            FP03.setPreferredSize(new Dimension(60, 30));
            FP03.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP03.setName("FP03");
            pnlQuantites.add(FP03, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q4 ----
            Q4.setHorizontalAlignment(SwingConstants.RIGHT);
            Q4.setMaximumSize(new Dimension(110, 30));
            Q4.setMinimumSize(new Dimension(110, 30));
            Q4.setPreferredSize(new Dimension(110, 30));
            Q4.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q4.setName("Q4");
            pnlQuantites.add(Q4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T4 ----
            T4.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T4.setComponentPopupMenu(null);
            T4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T4.setFont(new Font("sansserif", Font.PLAIN, 14));
            T4.setBackground(Color.white);
            T4.setName("T4");
            T4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T4ActionPerformed(e);
              }
            });
            pnlQuantites.add(T4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V4 ----
            V4.setHorizontalAlignment(SwingConstants.RIGHT);
            V4.setMaximumSize(new Dimension(110, 30));
            V4.setMinimumSize(new Dimension(110, 30));
            V4.setPreferredSize(new Dimension(110, 30));
            V4.setFont(new Font("sansserif", Font.PLAIN, 14));
            V4.setName("V4");
            pnlQuantites.add(V4, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA04 ----
            TA04.setComponentPopupMenu(null);
            TA04.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA04.setBackground(Color.white);
            TA04.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA04.setName("TA04");
            pnlQuantites.add(TA04, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises4 ========
            {
              pnlRemises4.setName("pnlRemises4");
              pnlRemises4.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises4.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R41 ----
              R41.setHorizontalAlignment(SwingConstants.RIGHT);
              R41.setMinimumSize(new Dimension(60, 30));
              R41.setMaximumSize(new Dimension(60, 30));
              R41.setPreferredSize(new Dimension(60, 30));
              R41.setFont(new Font("sansserif", Font.PLAIN, 14));
              R41.setName("R41");
              pnlRemises4.add(R41, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R42 ----
              R42.setHorizontalAlignment(SwingConstants.RIGHT);
              R42.setMinimumSize(new Dimension(60, 30));
              R42.setMaximumSize(new Dimension(60, 30));
              R42.setPreferredSize(new Dimension(60, 30));
              R42.setFont(new Font("sansserif", Font.PLAIN, 14));
              R42.setName("R42");
              pnlRemises4.add(R42, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R43 ----
              R43.setHorizontalAlignment(SwingConstants.RIGHT);
              R43.setMinimumSize(new Dimension(60, 30));
              R43.setMaximumSize(new Dimension(60, 30));
              R43.setPreferredSize(new Dimension(60, 30));
              R43.setFont(new Font("sansserif", Font.PLAIN, 14));
              R43.setName("R43");
              pnlRemises4.add(R43, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R44 ----
              R44.setHorizontalAlignment(SwingConstants.RIGHT);
              R44.setMinimumSize(new Dimension(60, 30));
              R44.setMaximumSize(new Dimension(60, 30));
              R44.setPreferredSize(new Dimension(60, 30));
              R44.setFont(new Font("sansserif", Font.PLAIN, 14));
              R44.setName("R44");
              pnlRemises4.add(R44, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R45 ----
              R45.setHorizontalAlignment(SwingConstants.RIGHT);
              R45.setMinimumSize(new Dimension(60, 30));
              R45.setMaximumSize(new Dimension(60, 30));
              R45.setPreferredSize(new Dimension(60, 30));
              R45.setFont(new Font("sansserif", Font.PLAIN, 14));
              R45.setName("R45");
              pnlRemises4.add(R45, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R46 ----
              R46.setHorizontalAlignment(SwingConstants.RIGHT);
              R46.setMinimumSize(new Dimension(60, 30));
              R46.setMaximumSize(new Dimension(60, 30));
              R46.setPreferredSize(new Dimension(60, 30));
              R46.setFont(new Font("sansserif", Font.PLAIN, 14));
              R46.setName("R46");
              pnlRemises4.add(R46, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises4, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C4 ----
            C4.setComponentPopupMenu(null);
            C4.setHorizontalAlignment(SwingConstants.RIGHT);
            C4.setMinimumSize(new Dimension(80, 30));
            C4.setMaximumSize(new Dimension(80, 30));
            C4.setPreferredSize(new Dimension(80, 30));
            C4.setFont(new Font("sansserif", Font.PLAIN, 14));
            C4.setName("C4");
            pnlQuantites.add(C4, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP04 ----
            FP04.setComponentPopupMenu(null);
            FP04.setHorizontalAlignment(SwingConstants.LEFT);
            FP04.setMinimumSize(new Dimension(60, 30));
            FP04.setMaximumSize(new Dimension(60, 30));
            FP04.setPreferredSize(new Dimension(60, 30));
            FP04.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP04.setName("FP04");
            pnlQuantites.add(FP04, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q5 ----
            Q5.setHorizontalAlignment(SwingConstants.RIGHT);
            Q5.setMaximumSize(new Dimension(110, 30));
            Q5.setMinimumSize(new Dimension(110, 30));
            Q5.setPreferredSize(new Dimension(110, 30));
            Q5.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q5.setName("Q5");
            pnlQuantites.add(Q5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T5 ----
            T5.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T5.setComponentPopupMenu(null);
            T5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T5.setFont(new Font("sansserif", Font.PLAIN, 14));
            T5.setBackground(Color.white);
            T5.setName("T5");
            T5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T5ActionPerformed(e);
              }
            });
            pnlQuantites.add(T5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V5 ----
            V5.setHorizontalAlignment(SwingConstants.RIGHT);
            V5.setMaximumSize(new Dimension(110, 30));
            V5.setMinimumSize(new Dimension(110, 30));
            V5.setPreferredSize(new Dimension(110, 30));
            V5.setFont(new Font("sansserif", Font.PLAIN, 14));
            V5.setName("V5");
            pnlQuantites.add(V5, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA05 ----
            TA05.setComponentPopupMenu(null);
            TA05.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA05.setBackground(Color.white);
            TA05.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA05.setName("TA05");
            pnlQuantites.add(TA05, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises5 ========
            {
              pnlRemises5.setName("pnlRemises5");
              pnlRemises5.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises5.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R51 ----
              R51.setHorizontalAlignment(SwingConstants.RIGHT);
              R51.setMinimumSize(new Dimension(60, 30));
              R51.setMaximumSize(new Dimension(60, 30));
              R51.setPreferredSize(new Dimension(60, 30));
              R51.setFont(new Font("sansserif", Font.PLAIN, 14));
              R51.setName("R51");
              pnlRemises5.add(R51, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R52 ----
              R52.setHorizontalAlignment(SwingConstants.RIGHT);
              R52.setMinimumSize(new Dimension(60, 30));
              R52.setMaximumSize(new Dimension(60, 30));
              R52.setPreferredSize(new Dimension(60, 30));
              R52.setFont(new Font("sansserif", Font.PLAIN, 14));
              R52.setName("R52");
              pnlRemises5.add(R52, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R53 ----
              R53.setHorizontalAlignment(SwingConstants.RIGHT);
              R53.setMinimumSize(new Dimension(60, 30));
              R53.setMaximumSize(new Dimension(60, 30));
              R53.setPreferredSize(new Dimension(60, 30));
              R53.setFont(new Font("sansserif", Font.PLAIN, 14));
              R53.setName("R53");
              pnlRemises5.add(R53, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R54 ----
              R54.setHorizontalAlignment(SwingConstants.RIGHT);
              R54.setMinimumSize(new Dimension(60, 30));
              R54.setMaximumSize(new Dimension(60, 30));
              R54.setPreferredSize(new Dimension(60, 30));
              R54.setFont(new Font("sansserif", Font.PLAIN, 14));
              R54.setName("R54");
              pnlRemises5.add(R54, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R55 ----
              R55.setHorizontalAlignment(SwingConstants.RIGHT);
              R55.setMinimumSize(new Dimension(60, 30));
              R55.setMaximumSize(new Dimension(60, 30));
              R55.setPreferredSize(new Dimension(60, 30));
              R55.setFont(new Font("sansserif", Font.PLAIN, 14));
              R55.setName("R55");
              pnlRemises5.add(R55, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R56 ----
              R56.setHorizontalAlignment(SwingConstants.RIGHT);
              R56.setMinimumSize(new Dimension(60, 30));
              R56.setMaximumSize(new Dimension(60, 30));
              R56.setPreferredSize(new Dimension(60, 30));
              R56.setFont(new Font("sansserif", Font.PLAIN, 14));
              R56.setName("R56");
              pnlRemises5.add(R56, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises5, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C5 ----
            C5.setComponentPopupMenu(null);
            C5.setHorizontalAlignment(SwingConstants.RIGHT);
            C5.setMinimumSize(new Dimension(80, 30));
            C5.setMaximumSize(new Dimension(80, 30));
            C5.setPreferredSize(new Dimension(80, 30));
            C5.setFont(new Font("sansserif", Font.PLAIN, 14));
            C5.setName("C5");
            pnlQuantites.add(C5, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP05 ----
            FP05.setComponentPopupMenu(null);
            FP05.setHorizontalAlignment(SwingConstants.LEFT);
            FP05.setMinimumSize(new Dimension(60, 30));
            FP05.setMaximumSize(new Dimension(60, 30));
            FP05.setPreferredSize(new Dimension(60, 30));
            FP05.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP05.setName("FP05");
            pnlQuantites.add(FP05, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q6 ----
            Q6.setHorizontalAlignment(SwingConstants.RIGHT);
            Q6.setMaximumSize(new Dimension(110, 30));
            Q6.setMinimumSize(new Dimension(110, 30));
            Q6.setPreferredSize(new Dimension(110, 30));
            Q6.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q6.setName("Q6");
            pnlQuantites.add(Q6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T6 ----
            T6.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T6.setComponentPopupMenu(null);
            T6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T6.setFont(new Font("sansserif", Font.PLAIN, 14));
            T6.setBackground(Color.white);
            T6.setName("T6");
            T6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T6ActionPerformed(e);
              }
            });
            pnlQuantites.add(T6, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V6 ----
            V6.setHorizontalAlignment(SwingConstants.RIGHT);
            V6.setMaximumSize(new Dimension(110, 30));
            V6.setMinimumSize(new Dimension(110, 30));
            V6.setPreferredSize(new Dimension(110, 30));
            V6.setFont(new Font("sansserif", Font.PLAIN, 14));
            V6.setName("V6");
            pnlQuantites.add(V6, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA06 ----
            TA06.setComponentPopupMenu(null);
            TA06.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA06.setBackground(Color.white);
            TA06.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA06.setName("TA06");
            pnlQuantites.add(TA06, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises6 ========
            {
              pnlRemises6.setName("pnlRemises6");
              pnlRemises6.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises6.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises6.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises6.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R61 ----
              R61.setHorizontalAlignment(SwingConstants.RIGHT);
              R61.setMinimumSize(new Dimension(60, 30));
              R61.setMaximumSize(new Dimension(60, 30));
              R61.setPreferredSize(new Dimension(60, 30));
              R61.setFont(new Font("sansserif", Font.PLAIN, 14));
              R61.setName("R61");
              pnlRemises6.add(R61, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R62 ----
              R62.setHorizontalAlignment(SwingConstants.RIGHT);
              R62.setMinimumSize(new Dimension(60, 30));
              R62.setMaximumSize(new Dimension(60, 30));
              R62.setPreferredSize(new Dimension(60, 30));
              R62.setFont(new Font("sansserif", Font.PLAIN, 14));
              R62.setName("R62");
              pnlRemises6.add(R62, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R63 ----
              R63.setHorizontalAlignment(SwingConstants.RIGHT);
              R63.setMinimumSize(new Dimension(60, 30));
              R63.setMaximumSize(new Dimension(60, 30));
              R63.setPreferredSize(new Dimension(60, 30));
              R63.setFont(new Font("sansserif", Font.PLAIN, 14));
              R63.setName("R63");
              pnlRemises6.add(R63, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R64 ----
              R64.setHorizontalAlignment(SwingConstants.RIGHT);
              R64.setMinimumSize(new Dimension(60, 30));
              R64.setMaximumSize(new Dimension(60, 30));
              R64.setPreferredSize(new Dimension(60, 30));
              R64.setFont(new Font("sansserif", Font.PLAIN, 14));
              R64.setName("R64");
              pnlRemises6.add(R64, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R65 ----
              R65.setHorizontalAlignment(SwingConstants.RIGHT);
              R65.setMinimumSize(new Dimension(60, 30));
              R65.setMaximumSize(new Dimension(60, 30));
              R65.setPreferredSize(new Dimension(60, 30));
              R65.setFont(new Font("sansserif", Font.PLAIN, 14));
              R65.setName("R65");
              pnlRemises6.add(R65, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R66 ----
              R66.setHorizontalAlignment(SwingConstants.RIGHT);
              R66.setMinimumSize(new Dimension(60, 30));
              R66.setMaximumSize(new Dimension(60, 30));
              R66.setPreferredSize(new Dimension(60, 30));
              R66.setFont(new Font("sansserif", Font.PLAIN, 14));
              R66.setName("R66");
              pnlRemises6.add(R66, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises6, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C6 ----
            C6.setComponentPopupMenu(null);
            C6.setHorizontalAlignment(SwingConstants.RIGHT);
            C6.setMinimumSize(new Dimension(80, 30));
            C6.setMaximumSize(new Dimension(80, 30));
            C6.setPreferredSize(new Dimension(80, 30));
            C6.setFont(new Font("sansserif", Font.PLAIN, 14));
            C6.setName("C6");
            pnlQuantites.add(C6, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP06 ----
            FP06.setComponentPopupMenu(null);
            FP06.setHorizontalAlignment(SwingConstants.LEFT);
            FP06.setMinimumSize(new Dimension(60, 30));
            FP06.setMaximumSize(new Dimension(60, 30));
            FP06.setPreferredSize(new Dimension(60, 30));
            FP06.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP06.setName("FP06");
            pnlQuantites.add(FP06, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q7 ----
            Q7.setHorizontalAlignment(SwingConstants.RIGHT);
            Q7.setMaximumSize(new Dimension(110, 30));
            Q7.setMinimumSize(new Dimension(110, 30));
            Q7.setPreferredSize(new Dimension(110, 30));
            Q7.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q7.setName("Q7");
            pnlQuantites.add(Q7, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T7 ----
            T7.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T7.setComponentPopupMenu(null);
            T7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T7.setFont(new Font("sansserif", Font.PLAIN, 14));
            T7.setBackground(Color.white);
            T7.setName("T7");
            T7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T7ActionPerformed(e);
              }
            });
            pnlQuantites.add(T7, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V7 ----
            V7.setHorizontalAlignment(SwingConstants.RIGHT);
            V7.setMaximumSize(new Dimension(110, 30));
            V7.setMinimumSize(new Dimension(110, 30));
            V7.setPreferredSize(new Dimension(110, 30));
            V7.setFont(new Font("sansserif", Font.PLAIN, 14));
            V7.setName("V7");
            pnlQuantites.add(V7, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA07 ----
            TA07.setComponentPopupMenu(null);
            TA07.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA07.setBackground(Color.white);
            TA07.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA07.setName("TA07");
            pnlQuantites.add(TA07, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises7 ========
            {
              pnlRemises7.setName("pnlRemises7");
              pnlRemises7.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises7.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises7.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises7.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises7.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R71 ----
              R71.setHorizontalAlignment(SwingConstants.RIGHT);
              R71.setMinimumSize(new Dimension(60, 30));
              R71.setMaximumSize(new Dimension(60, 30));
              R71.setPreferredSize(new Dimension(60, 30));
              R71.setFont(new Font("sansserif", Font.PLAIN, 14));
              R71.setName("R71");
              pnlRemises7.add(R71, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R72 ----
              R72.setHorizontalAlignment(SwingConstants.RIGHT);
              R72.setMinimumSize(new Dimension(60, 30));
              R72.setMaximumSize(new Dimension(60, 30));
              R72.setPreferredSize(new Dimension(60, 30));
              R72.setFont(new Font("sansserif", Font.PLAIN, 14));
              R72.setName("R72");
              pnlRemises7.add(R72, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R73 ----
              R73.setHorizontalAlignment(SwingConstants.RIGHT);
              R73.setMinimumSize(new Dimension(60, 30));
              R73.setMaximumSize(new Dimension(60, 30));
              R73.setPreferredSize(new Dimension(60, 30));
              R73.setFont(new Font("sansserif", Font.PLAIN, 14));
              R73.setName("R73");
              pnlRemises7.add(R73, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R74 ----
              R74.setHorizontalAlignment(SwingConstants.RIGHT);
              R74.setMinimumSize(new Dimension(60, 30));
              R74.setMaximumSize(new Dimension(60, 30));
              R74.setPreferredSize(new Dimension(60, 30));
              R74.setFont(new Font("sansserif", Font.PLAIN, 14));
              R74.setName("R74");
              pnlRemises7.add(R74, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R75 ----
              R75.setHorizontalAlignment(SwingConstants.RIGHT);
              R75.setMinimumSize(new Dimension(60, 30));
              R75.setMaximumSize(new Dimension(60, 30));
              R75.setPreferredSize(new Dimension(60, 30));
              R75.setFont(new Font("sansserif", Font.PLAIN, 14));
              R75.setName("R75");
              pnlRemises7.add(R75, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R76 ----
              R76.setHorizontalAlignment(SwingConstants.RIGHT);
              R76.setMinimumSize(new Dimension(60, 30));
              R76.setMaximumSize(new Dimension(60, 30));
              R76.setPreferredSize(new Dimension(60, 30));
              R76.setFont(new Font("sansserif", Font.PLAIN, 14));
              R76.setName("R76");
              pnlRemises7.add(R76, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises7, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C7 ----
            C7.setComponentPopupMenu(null);
            C7.setHorizontalAlignment(SwingConstants.RIGHT);
            C7.setMinimumSize(new Dimension(80, 30));
            C7.setMaximumSize(new Dimension(80, 30));
            C7.setPreferredSize(new Dimension(80, 30));
            C7.setFont(new Font("sansserif", Font.PLAIN, 14));
            C7.setName("C7");
            pnlQuantites.add(C7, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP07 ----
            FP07.setComponentPopupMenu(null);
            FP07.setHorizontalAlignment(SwingConstants.LEFT);
            FP07.setMinimumSize(new Dimension(60, 30));
            FP07.setMaximumSize(new Dimension(60, 30));
            FP07.setPreferredSize(new Dimension(60, 30));
            FP07.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP07.setName("FP07");
            pnlQuantites.add(FP07, new GridBagConstraints(6, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q8 ----
            Q8.setHorizontalAlignment(SwingConstants.RIGHT);
            Q8.setMaximumSize(new Dimension(110, 30));
            Q8.setMinimumSize(new Dimension(110, 30));
            Q8.setPreferredSize(new Dimension(110, 30));
            Q8.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q8.setName("Q8");
            pnlQuantites.add(Q8, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T8 ----
            T8.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T8.setComponentPopupMenu(null);
            T8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T8.setFont(new Font("sansserif", Font.PLAIN, 14));
            T8.setBackground(Color.white);
            T8.setName("T8");
            T8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T8ActionPerformed(e);
              }
            });
            pnlQuantites.add(T8, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V8 ----
            V8.setHorizontalAlignment(SwingConstants.RIGHT);
            V8.setMaximumSize(new Dimension(110, 30));
            V8.setMinimumSize(new Dimension(110, 30));
            V8.setPreferredSize(new Dimension(110, 30));
            V8.setFont(new Font("sansserif", Font.PLAIN, 14));
            V8.setName("V8");
            pnlQuantites.add(V8, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA08 ----
            TA08.setComponentPopupMenu(null);
            TA08.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA08.setBackground(Color.white);
            TA08.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA08.setName("TA08");
            pnlQuantites.add(TA08, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises8 ========
            {
              pnlRemises8.setName("pnlRemises8");
              pnlRemises8.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises8.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises8.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises8.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises8.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R81 ----
              R81.setHorizontalAlignment(SwingConstants.RIGHT);
              R81.setMinimumSize(new Dimension(60, 30));
              R81.setMaximumSize(new Dimension(60, 30));
              R81.setPreferredSize(new Dimension(60, 30));
              R81.setFont(new Font("sansserif", Font.PLAIN, 14));
              R81.setName("R81");
              pnlRemises8.add(R81, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R82 ----
              R82.setHorizontalAlignment(SwingConstants.RIGHT);
              R82.setMinimumSize(new Dimension(60, 30));
              R82.setMaximumSize(new Dimension(60, 30));
              R82.setPreferredSize(new Dimension(60, 30));
              R82.setFont(new Font("sansserif", Font.PLAIN, 14));
              R82.setName("R82");
              pnlRemises8.add(R82, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R83 ----
              R83.setHorizontalAlignment(SwingConstants.RIGHT);
              R83.setMinimumSize(new Dimension(60, 30));
              R83.setMaximumSize(new Dimension(60, 30));
              R83.setPreferredSize(new Dimension(60, 30));
              R83.setFont(new Font("sansserif", Font.PLAIN, 14));
              R83.setName("R83");
              pnlRemises8.add(R83, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R84 ----
              R84.setHorizontalAlignment(SwingConstants.RIGHT);
              R84.setMinimumSize(new Dimension(60, 30));
              R84.setMaximumSize(new Dimension(60, 30));
              R84.setPreferredSize(new Dimension(60, 30));
              R84.setFont(new Font("sansserif", Font.PLAIN, 14));
              R84.setName("R84");
              pnlRemises8.add(R84, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R85 ----
              R85.setHorizontalAlignment(SwingConstants.RIGHT);
              R85.setMinimumSize(new Dimension(60, 30));
              R85.setMaximumSize(new Dimension(60, 30));
              R85.setPreferredSize(new Dimension(60, 30));
              R85.setFont(new Font("sansserif", Font.PLAIN, 14));
              R85.setName("R85");
              pnlRemises8.add(R85, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R86 ----
              R86.setHorizontalAlignment(SwingConstants.RIGHT);
              R86.setMinimumSize(new Dimension(60, 30));
              R86.setMaximumSize(new Dimension(60, 30));
              R86.setPreferredSize(new Dimension(60, 30));
              R86.setFont(new Font("sansserif", Font.PLAIN, 14));
              R86.setName("R86");
              pnlRemises8.add(R86, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises8, new GridBagConstraints(4, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C8 ----
            C8.setComponentPopupMenu(null);
            C8.setHorizontalAlignment(SwingConstants.RIGHT);
            C8.setMinimumSize(new Dimension(80, 30));
            C8.setMaximumSize(new Dimension(80, 30));
            C8.setPreferredSize(new Dimension(80, 30));
            C8.setFont(new Font("sansserif", Font.PLAIN, 14));
            C8.setName("C8");
            pnlQuantites.add(C8, new GridBagConstraints(5, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP08 ----
            FP08.setComponentPopupMenu(null);
            FP08.setHorizontalAlignment(SwingConstants.LEFT);
            FP08.setMinimumSize(new Dimension(60, 30));
            FP08.setMaximumSize(new Dimension(60, 30));
            FP08.setPreferredSize(new Dimension(60, 30));
            FP08.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP08.setName("FP08");
            pnlQuantites.add(FP08, new GridBagConstraints(6, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q9 ----
            Q9.setHorizontalAlignment(SwingConstants.RIGHT);
            Q9.setMaximumSize(new Dimension(110, 30));
            Q9.setMinimumSize(new Dimension(110, 30));
            Q9.setPreferredSize(new Dimension(110, 30));
            Q9.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q9.setName("Q9");
            pnlQuantites.add(Q9, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- T9 ----
            T9.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T9.setComponentPopupMenu(null);
            T9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T9.setFont(new Font("sansserif", Font.PLAIN, 14));
            T9.setBackground(Color.white);
            T9.setName("T9");
            T9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T9ActionPerformed(e);
              }
            });
            pnlQuantites.add(T9, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- V9 ----
            V9.setHorizontalAlignment(SwingConstants.RIGHT);
            V9.setMaximumSize(new Dimension(110, 30));
            V9.setMinimumSize(new Dimension(110, 30));
            V9.setPreferredSize(new Dimension(110, 30));
            V9.setFont(new Font("sansserif", Font.PLAIN, 14));
            V9.setName("V9");
            pnlQuantites.add(V9, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- TA09 ----
            TA09.setComponentPopupMenu(null);
            TA09.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA09.setBackground(Color.white);
            TA09.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA09.setName("TA09");
            pnlQuantites.add(TA09, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises9 ========
            {
              pnlRemises9.setName("pnlRemises9");
              pnlRemises9.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises9.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises9.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises9.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises9.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R91 ----
              R91.setHorizontalAlignment(SwingConstants.RIGHT);
              R91.setMinimumSize(new Dimension(60, 30));
              R91.setMaximumSize(new Dimension(60, 30));
              R91.setPreferredSize(new Dimension(60, 30));
              R91.setFont(new Font("sansserif", Font.PLAIN, 14));
              R91.setName("R91");
              pnlRemises9.add(R91, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R92 ----
              R92.setHorizontalAlignment(SwingConstants.RIGHT);
              R92.setMinimumSize(new Dimension(60, 30));
              R92.setMaximumSize(new Dimension(60, 30));
              R92.setPreferredSize(new Dimension(60, 30));
              R92.setFont(new Font("sansserif", Font.PLAIN, 14));
              R92.setName("R92");
              pnlRemises9.add(R92, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R93 ----
              R93.setHorizontalAlignment(SwingConstants.RIGHT);
              R93.setMinimumSize(new Dimension(60, 30));
              R93.setMaximumSize(new Dimension(60, 30));
              R93.setPreferredSize(new Dimension(60, 30));
              R93.setFont(new Font("sansserif", Font.PLAIN, 14));
              R93.setName("R93");
              pnlRemises9.add(R93, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R94 ----
              R94.setHorizontalAlignment(SwingConstants.RIGHT);
              R94.setMinimumSize(new Dimension(60, 30));
              R94.setMaximumSize(new Dimension(60, 30));
              R94.setPreferredSize(new Dimension(60, 30));
              R94.setFont(new Font("sansserif", Font.PLAIN, 14));
              R94.setName("R94");
              pnlRemises9.add(R94, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R95 ----
              R95.setHorizontalAlignment(SwingConstants.RIGHT);
              R95.setMinimumSize(new Dimension(60, 30));
              R95.setMaximumSize(new Dimension(60, 30));
              R95.setPreferredSize(new Dimension(60, 30));
              R95.setFont(new Font("sansserif", Font.PLAIN, 14));
              R95.setName("R95");
              pnlRemises9.add(R95, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R96 ----
              R96.setHorizontalAlignment(SwingConstants.RIGHT);
              R96.setMinimumSize(new Dimension(60, 30));
              R96.setMaximumSize(new Dimension(60, 30));
              R96.setPreferredSize(new Dimension(60, 30));
              R96.setFont(new Font("sansserif", Font.PLAIN, 14));
              R96.setName("R96");
              pnlRemises9.add(R96, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises9, new GridBagConstraints(4, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- C9 ----
            C9.setComponentPopupMenu(null);
            C9.setHorizontalAlignment(SwingConstants.RIGHT);
            C9.setMinimumSize(new Dimension(80, 30));
            C9.setMaximumSize(new Dimension(80, 30));
            C9.setPreferredSize(new Dimension(80, 30));
            C9.setFont(new Font("sansserif", Font.PLAIN, 14));
            C9.setName("C9");
            pnlQuantites.add(C9, new GridBagConstraints(5, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- FP09 ----
            FP09.setComponentPopupMenu(null);
            FP09.setHorizontalAlignment(SwingConstants.LEFT);
            FP09.setMinimumSize(new Dimension(60, 30));
            FP09.setMaximumSize(new Dimension(60, 30));
            FP09.setPreferredSize(new Dimension(60, 30));
            FP09.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP09.setName("FP09");
            pnlQuantites.add(FP09, new GridBagConstraints(6, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- Q10 ----
            Q10.setHorizontalAlignment(SwingConstants.RIGHT);
            Q10.setMaximumSize(new Dimension(110, 30));
            Q10.setMinimumSize(new Dimension(110, 30));
            Q10.setPreferredSize(new Dimension(110, 30));
            Q10.setFont(new Font("sansserif", Font.PLAIN, 14));
            Q10.setName("Q10");
            pnlQuantites.add(Q10, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T10 ----
            T10.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Colonne de tarif",
                "Remise en %", "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
            T10.setComponentPopupMenu(null);
            T10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T10.setFont(new Font("sansserif", Font.PLAIN, 14));
            T10.setBackground(Color.white);
            T10.setName("T10");
            T10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                T10ActionPerformed(e);
              }
            });
            pnlQuantites.add(T10, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- V10 ----
            V10.setHorizontalAlignment(SwingConstants.RIGHT);
            V10.setMaximumSize(new Dimension(110, 30));
            V10.setMinimumSize(new Dimension(110, 30));
            V10.setPreferredSize(new Dimension(110, 30));
            V10.setFont(new Font("sansserif", Font.PLAIN, 14));
            V10.setName("V10");
            pnlQuantites.add(V10, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- TA10 ----
            TA10.setComponentPopupMenu(null);
            TA10.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            TA10.setBackground(Color.white);
            TA10.setFont(new Font("sansserif", Font.PLAIN, 14));
            TA10.setName("TA10");
            pnlQuantites.add(TA10, new GridBagConstraints(3, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlRemises10 ========
            {
              pnlRemises10.setName("pnlRemises10");
              pnlRemises10.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises10.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises10.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises10.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises10.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- R101 ----
              R101.setHorizontalAlignment(SwingConstants.RIGHT);
              R101.setMinimumSize(new Dimension(60, 30));
              R101.setMaximumSize(new Dimension(60, 30));
              R101.setPreferredSize(new Dimension(60, 30));
              R101.setFont(new Font("sansserif", Font.PLAIN, 14));
              R101.setName("R101");
              pnlRemises10.add(R101, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R102 ----
              R102.setHorizontalAlignment(SwingConstants.RIGHT);
              R102.setMinimumSize(new Dimension(60, 30));
              R102.setMaximumSize(new Dimension(60, 30));
              R102.setPreferredSize(new Dimension(60, 30));
              R102.setFont(new Font("sansserif", Font.PLAIN, 14));
              R102.setName("R102");
              pnlRemises10.add(R102, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R103 ----
              R103.setHorizontalAlignment(SwingConstants.RIGHT);
              R103.setMinimumSize(new Dimension(60, 30));
              R103.setMaximumSize(new Dimension(60, 30));
              R103.setPreferredSize(new Dimension(60, 30));
              R103.setFont(new Font("sansserif", Font.PLAIN, 14));
              R103.setName("R103");
              pnlRemises10.add(R103, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R104 ----
              R104.setHorizontalAlignment(SwingConstants.RIGHT);
              R104.setMinimumSize(new Dimension(60, 30));
              R104.setMaximumSize(new Dimension(60, 30));
              R104.setPreferredSize(new Dimension(60, 30));
              R104.setFont(new Font("sansserif", Font.PLAIN, 14));
              R104.setName("R104");
              pnlRemises10.add(R104, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R105 ----
              R105.setHorizontalAlignment(SwingConstants.RIGHT);
              R105.setMinimumSize(new Dimension(60, 30));
              R105.setMaximumSize(new Dimension(60, 30));
              R105.setPreferredSize(new Dimension(60, 30));
              R105.setFont(new Font("sansserif", Font.PLAIN, 14));
              R105.setName("R105");
              pnlRemises10.add(R105, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- R106 ----
              R106.setHorizontalAlignment(SwingConstants.RIGHT);
              R106.setMinimumSize(new Dimension(60, 30));
              R106.setMaximumSize(new Dimension(60, 30));
              R106.setPreferredSize(new Dimension(60, 30));
              R106.setFont(new Font("sansserif", Font.PLAIN, 14));
              R106.setName("R106");
              pnlRemises10.add(R106, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantites.add(pnlRemises10, new GridBagConstraints(4, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- C10 ----
            C10.setComponentPopupMenu(null);
            C10.setHorizontalAlignment(SwingConstants.RIGHT);
            C10.setMinimumSize(new Dimension(80, 30));
            C10.setMaximumSize(new Dimension(80, 30));
            C10.setPreferredSize(new Dimension(80, 30));
            C10.setFont(new Font("sansserif", Font.PLAIN, 14));
            C10.setName("C10");
            pnlQuantites.add(C10, new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- FP10 ----
            FP10.setComponentPopupMenu(null);
            FP10.setHorizontalAlignment(SwingConstants.LEFT);
            FP10.setMinimumSize(new Dimension(60, 30));
            FP10.setMaximumSize(new Dimension(60, 30));
            FP10.setPreferredSize(new Dimension(60, 30));
            FP10.setFont(new Font("sansserif", Font.PLAIN, 14));
            FP10.setName("FP10");
            pnlQuantites.add(FP10, new GridBagConstraints(6, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          scrollPane1.setViewportView(pnlQuantites);
        }
        pnlQuantite.add(scrollPane1, new GridBagConstraints(0, 0, 7, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlQuantite,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTarif ========
      {
        pnlTarif.setTitre("Tarif");
        pnlTarif.setName("pnlTarif");
        pnlTarif.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTarif.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlTarif.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlTarif.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlRefTarif ========
        {
          pnlRefTarif.setName("pnlRefTarif");
          pnlRefTarif.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRefTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRefTarif.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRefTarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRefTarif.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp11 ----
          sNLabelChamp11.setText("Prix de revient");
          sNLabelChamp11.setName("sNLabelChamp11");
          pnlRefTarif.add(sNLabelChamp11, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WPRVA ----
          WPRVA.setText("@WPRVA@");
          WPRVA.setHorizontalAlignment(SwingConstants.RIGHT);
          WPRVA.setFont(WPRVA.getFont().deriveFont(WPRVA.getFont().getStyle() | Font.BOLD));
          WPRVA.setEnabled(false);
          WPRVA.setName("WPRVA");
          pnlRefTarif.add(WPRVA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbReference2 ----
          lbReference2.setText("R\u00e9f\u00e9rence tarif");
          lbReference2.setName("lbReference2");
          pnlRefTarif.add(lbReference2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1RTA ----
          A1RTA.setText("@A1RTA@");
          A1RTA.setEnabled(false);
          A1RTA.setName("A1RTA");
          pnlRefTarif.add(A1RTA, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- ULBTAR ----
          ULBTAR.setText("@ULBTAR@");
          ULBTAR.setEnabled(false);
          ULBTAR.setMinimumSize(new Dimension(450, 30));
          ULBTAR.setPreferredSize(new Dimension(450, 30));
          ULBTAR.setName("ULBTAR");
          pnlRefTarif.add(ULBTAR, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlRefTarif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTarifEnCours ========
        {
          pnlTarifEnCours.setName("pnlTarifEnCours");
          pnlTarifEnCours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarifEnCours.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTarifEnCours.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarifEnCours.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarifEnCours.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp9 ----
          sNLabelChamp9.setText("Tarif en cours");
          sNLabelChamp9.setName("sNLabelChamp9");
          pnlTarifEnCours.add(sNLabelChamp9, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNLabelChamp5 ----
          sNLabelChamp5.setText("Prix de vente");
          sNLabelChamp5.setName("sNLabelChamp5");
          pnlTarifEnCours.add(sNLabelChamp5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P11X ----
          A1P11X.setText("@A1P11X@");
          A1P11X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P11X.setEnabled(false);
          A1P11X.setName("A1P11X");
          pnlTarifEnCours.add(A1P11X, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P21X ----
          A1P21X.setText("@A1P21X@");
          A1P21X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P21X.setEnabled(false);
          A1P21X.setName("A1P21X");
          pnlTarifEnCours.add(A1P21X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P31X ----
          A1P31X.setText("@A1P31X@");
          A1P31X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P31X.setEnabled(false);
          A1P31X.setName("A1P31X");
          pnlTarifEnCours.add(A1P31X, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P41X ----
          A1P41X.setText("@A1P41X@");
          A1P41X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P41X.setEnabled(false);
          A1P41X.setName("A1P41X");
          pnlTarifEnCours.add(A1P41X, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P51X ----
          A1P51X.setText("@A1P51X@");
          A1P51X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P51X.setEnabled(false);
          A1P51X.setName("A1P51X");
          pnlTarifEnCours.add(A1P51X, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P61X ----
          A1P61X.setText("@A1P61X@");
          A1P61X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P61X.setEnabled(false);
          A1P61X.setName("A1P61X");
          pnlTarifEnCours.add(A1P61X, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- sNLabelChamp6 ----
          sNLabelChamp6.setText("Coefficient");
          sNLabelChamp6.setName("sNLabelChamp6");
          pnlTarifEnCours.add(sNLabelChamp6, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K21X ----
          A1K21X.setText("@A1K21X@");
          A1K21X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K21X.setEnabled(false);
          A1K21X.setName("A1K21X");
          pnlTarifEnCours.add(A1K21X, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K31X ----
          A1K31X.setText("@A1K31X@");
          A1K31X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K31X.setEnabled(false);
          A1K31X.setName("A1K31X");
          pnlTarifEnCours.add(A1K31X, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K41X ----
          A1K41X.setText("@A1K41X@");
          A1K41X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K41X.setEnabled(false);
          A1K41X.setName("A1K41X");
          pnlTarifEnCours.add(A1K41X, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K51X ----
          A1K51X.setText("@A1K51X@");
          A1K51X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K51X.setEnabled(false);
          A1K51X.setName("A1K51X");
          pnlTarifEnCours.add(A1K51X, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K61X ----
          A1K61X.setText("@A1K61X@");
          A1K61X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K61X.setEnabled(false);
          A1K61X.setName("A1K61X");
          pnlTarifEnCours.add(A1K61X, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlTarifEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTarifDate ========
        {
          pnlTarifDate.setName("pnlTarifDate");
          pnlTarifDate.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarifDate.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTarifDate.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarifDate.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarifDate.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp10 ----
          sNLabelChamp10.setText("Tarif au @W0DTDX@ ");
          sNLabelChamp10.setName("sNLabelChamp10");
          pnlTarifDate.add(sNLabelChamp10, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- sNLabelChamp7 ----
          sNLabelChamp7.setText("Prix de vente");
          sNLabelChamp7.setName("sNLabelChamp7");
          pnlTarifDate.add(sNLabelChamp7, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P12X ----
          A1P12X.setText("@A1P12X@");
          A1P12X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P12X.setEnabled(false);
          A1P12X.setName("A1P12X");
          pnlTarifDate.add(A1P12X, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P22X ----
          A1P22X.setText("@A1P22X@");
          A1P22X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P22X.setEnabled(false);
          A1P22X.setName("A1P22X");
          pnlTarifDate.add(A1P22X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P32X ----
          A1P32X.setText("@A1P32X@");
          A1P32X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P32X.setEnabled(false);
          A1P32X.setName("A1P32X");
          pnlTarifDate.add(A1P32X, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P42X ----
          A1P42X.setText("@A1P42X@");
          A1P42X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P42X.setEnabled(false);
          A1P42X.setName("A1P42X");
          pnlTarifDate.add(A1P42X, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P52X ----
          A1P52X.setText("@A1P52X@");
          A1P52X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P52X.setEnabled(false);
          A1P52X.setName("A1P52X");
          pnlTarifDate.add(A1P52X, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- A1P62X ----
          A1P62X.setText("@A1P62X@");
          A1P62X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1P62X.setEnabled(false);
          A1P62X.setName("A1P62X");
          pnlTarifDate.add(A1P62X, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- sNLabelChamp8 ----
          sNLabelChamp8.setText("Coefficient");
          sNLabelChamp8.setName("sNLabelChamp8");
          pnlTarifDate.add(sNLabelChamp8, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K22X ----
          A1K22X.setText("@A1K22X@");
          A1K22X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K22X.setEnabled(false);
          A1K22X.setName("A1K22X");
          pnlTarifDate.add(A1K22X, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K32X ----
          A1K32X.setText("@A1K32X@");
          A1K32X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K32X.setEnabled(false);
          A1K32X.setName("A1K32X");
          pnlTarifDate.add(A1K32X, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K42X ----
          A1K42X.setText("@A1K42X@");
          A1K42X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K42X.setEnabled(false);
          A1K42X.setName("A1K42X");
          pnlTarifDate.add(A1K42X, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K52X ----
          A1K52X.setText("@A1K52X@");
          A1K52X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K52X.setEnabled(false);
          A1K52X.setName("A1K52X");
          pnlTarifDate.add(A1K52X, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- A1K62X ----
          A1K62X.setText("@A1K62X@");
          A1K62X.setHorizontalAlignment(SwingConstants.RIGHT);
          A1K62X.setEnabled(false);
          A1K62X.setName("A1K62X");
          pnlTarifDate.add(A1K62X, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlTarifDate, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTarif,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlInfos;
  private SNLabelChamp lbCondition;
  private SNConditionVenteEtClient snConditionVente;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbTypeRattachement;
  private XRiComboBox TRA;
  private SNLabelChamp lbRattachement;
  private SNTexte RAT;
  private SNPanel pnlGeneral;
  private SNLabelChamp lbReference;
  private XRiTextField W1REF;
  private SNLabelChamp lbPeriode;
  private SNPlageDate snPlageDate;
  private SNLabelChamp lbUniteVente;
  private SNTexte A1UNV;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNPanelTitre pnlQuantite;
  private JScrollPane scrollPane1;
  private SNPanel pnlQuantites;
  private SNLabelUnite lbQuantite;
  private SNLabelUnite lbType;
  private SNLabelUnite lbValeur;
  private SNLabelUnite lbTarif;
  private SNLabelUnite lbRemises;
  private SNLabelUnite lbCoeff;
  private SNLabelUnite lbFormule;
  private XRiTextField Q1;
  private XRiComboBox T1;
  private XRiTextField V1;
  private XRiComboBox TA01;
  private SNPanel pnlRemises1;
  private XRiTextField R11;
  private XRiTextField R12;
  private XRiTextField R13;
  private XRiTextField R14;
  private XRiTextField R15;
  private XRiTextField R16;
  private XRiTextField C1;
  private XRiTextField FP01;
  private XRiTextField Q2;
  private XRiComboBox T2;
  private XRiTextField V2;
  private XRiComboBox TA02;
  private SNPanel pnlRemises2;
  private XRiTextField R21;
  private XRiTextField R22;
  private XRiTextField R23;
  private XRiTextField R24;
  private XRiTextField R25;
  private XRiTextField R26;
  private XRiTextField C2;
  private XRiTextField FP02;
  private XRiTextField Q3;
  private XRiComboBox T3;
  private XRiTextField V3;
  private XRiComboBox TA03;
  private SNPanel pnlRemises3;
  private XRiTextField R31;
  private XRiTextField R32;
  private XRiTextField R33;
  private XRiTextField R34;
  private XRiTextField R35;
  private XRiTextField R36;
  private XRiTextField C3;
  private XRiTextField FP03;
  private XRiTextField Q4;
  private XRiComboBox T4;
  private XRiTextField V4;
  private XRiComboBox TA04;
  private SNPanel pnlRemises4;
  private XRiTextField R41;
  private XRiTextField R42;
  private XRiTextField R43;
  private XRiTextField R44;
  private XRiTextField R45;
  private XRiTextField R46;
  private XRiTextField C4;
  private XRiTextField FP04;
  private XRiTextField Q5;
  private XRiComboBox T5;
  private XRiTextField V5;
  private XRiComboBox TA05;
  private SNPanel pnlRemises5;
  private XRiTextField R51;
  private XRiTextField R52;
  private XRiTextField R53;
  private XRiTextField R54;
  private XRiTextField R55;
  private XRiTextField R56;
  private XRiTextField C5;
  private XRiTextField FP05;
  private XRiTextField Q6;
  private XRiComboBox T6;
  private XRiTextField V6;
  private XRiComboBox TA06;
  private SNPanel pnlRemises6;
  private XRiTextField R61;
  private XRiTextField R62;
  private XRiTextField R63;
  private XRiTextField R64;
  private XRiTextField R65;
  private XRiTextField R66;
  private XRiTextField C6;
  private XRiTextField FP06;
  private XRiTextField Q7;
  private XRiComboBox T7;
  private XRiTextField V7;
  private XRiComboBox TA07;
  private SNPanel pnlRemises7;
  private XRiTextField R71;
  private XRiTextField R72;
  private XRiTextField R73;
  private XRiTextField R74;
  private XRiTextField R75;
  private XRiTextField R76;
  private XRiTextField C7;
  private XRiTextField FP07;
  private XRiTextField Q8;
  private XRiComboBox T8;
  private XRiTextField V8;
  private XRiComboBox TA08;
  private SNPanel pnlRemises8;
  private XRiTextField R81;
  private XRiTextField R82;
  private XRiTextField R83;
  private XRiTextField R84;
  private XRiTextField R85;
  private XRiTextField R86;
  private XRiTextField C8;
  private XRiTextField FP08;
  private XRiTextField Q9;
  private XRiComboBox T9;
  private XRiTextField V9;
  private XRiComboBox TA09;
  private SNPanel pnlRemises9;
  private XRiTextField R91;
  private XRiTextField R92;
  private XRiTextField R93;
  private XRiTextField R94;
  private XRiTextField R95;
  private XRiTextField R96;
  private XRiTextField C9;
  private XRiTextField FP09;
  private XRiTextField Q10;
  private XRiComboBox T10;
  private XRiTextField V10;
  private XRiComboBox TA10;
  private SNPanel pnlRemises10;
  private XRiTextField R101;
  private XRiTextField R102;
  private XRiTextField R103;
  private XRiTextField R104;
  private XRiTextField R105;
  private XRiTextField R106;
  private XRiTextField C10;
  private XRiTextField FP10;
  private SNPanelTitre pnlTarif;
  private SNPanel pnlRefTarif;
  private SNLabelChamp sNLabelChamp11;
  private SNTexte WPRVA;
  private SNLabelChamp lbReference2;
  private SNTexte A1RTA;
  private SNTexte ULBTAR;
  private SNPanel pnlTarifEnCours;
  private SNLabelChamp sNLabelChamp9;
  private SNLabelChamp sNLabelChamp5;
  private SNTexte A1P11X;
  private SNTexte A1P21X;
  private SNTexte A1P31X;
  private SNTexte A1P41X;
  private SNTexte A1P51X;
  private SNTexte A1P61X;
  private SNLabelChamp sNLabelChamp6;
  private SNTexte A1K21X;
  private SNTexte A1K31X;
  private SNTexte A1K41X;
  private SNTexte A1K51X;
  private SNTexte A1K61X;
  private SNPanel pnlTarifDate;
  private SNLabelChamp sNLabelChamp10;
  private SNLabelChamp sNLabelChamp7;
  private SNTexte A1P12X;
  private SNTexte A1P22X;
  private SNTexte A1P32X;
  private SNTexte A1P42X;
  private SNTexte A1P52X;
  private SNTexte A1P62X;
  private SNLabelChamp sNLabelChamp8;
  private SNTexte A1K22X;
  private SNTexte A1K32X;
  private SNTexte A1K42X;
  private SNTexte A1K52X;
  private SNTexte A1K62X;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
