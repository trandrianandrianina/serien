
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_ZG extends SNPanelEcranRPG implements ioFrame {
  
   
  private int nombreDep = 0;
  private ZG_WARNING warning = null;
  
  public VGVM01FX_ZG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    JToggleButton[] boutonsDep = { toggleButton1, toggleButton2, toggleButton3, toggleButton4, toggleButton5, toggleButton6, toggleButton7,
        toggleButton8, toggleButton9, toggleButton10, toggleButton11, toggleButton12, toggleButton13, toggleButton14, toggleButton15,
        toggleButton16, toggleButton17, toggleButton18, toggleButton19, toggleButton20, toggleButton21, toggleButton22, toggleButton23,
        toggleButton24, toggleButton25, toggleButton26, toggleButton27, toggleButton28, toggleButton29, toggleButton30, toggleButton31,
        toggleButton32, toggleButton33, toggleButton34, toggleButton35, toggleButton36, toggleButton37, toggleButton38, toggleButton39,
        toggleButton40, toggleButton41, toggleButton42, toggleButton43, toggleButton44, toggleButton45, toggleButton46, toggleButton47,
        toggleButton48, toggleButton49, toggleButton50, toggleButton51, toggleButton52, toggleButton53, toggleButton54, toggleButton55,
        toggleButton56, toggleButton57, toggleButton58, toggleButton59, toggleButton60, toggleButton61, toggleButton62, toggleButton63,
        toggleButton64, toggleButton65, toggleButton66, toggleButton67, toggleButton68, toggleButton69, toggleButton70, toggleButton71,
        toggleButton72, toggleButton73, toggleButton74, toggleButton75, toggleButton76, toggleButton77, toggleButton78, toggleButton79,
        toggleButton80, toggleButton81, toggleButton82, toggleButton83, toggleButton84, toggleButton85, toggleButton86, toggleButton87,
        toggleButton88, toggleButton89, toggleButton90, toggleButton91, toggleButton92, toggleButton93, toggleButton94, toggleButton95 };
    
    JToggleButton[] boutonsDom = { ZGD012, ZGD022, ZGD032, ZGD042, ZGD052, ZGD062, ZGD072, ZGD082, ZGD092, ZGD102 };
    
    for (int i = 0; i < 10; i++) {
      System.err
          .println("DOM n°" + (i + 1) + " : " + lexique.HostFieldGetData("ZGD" + ("" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))) + "2"));
    }
    
    
    nombreDep = 0;
    for (int i = 0; i < boutonsDep.length; i++) {
      String index = "" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1));
      boutonsDep[i].setSelected(isDepPresent(index));
      if (boutonsDep[i].isSelected()) {
        nombreDep++;
      }
    }
    
    for (int i = 0; i < boutonsDom.length; i++) {
      String index = "" + i;
      boutonsDom[i].setSelected(isDomPresent(index));
      if (boutonsDom[i].isSelected()) {
        nombreDep++;
      }
    }
    
    if (lexique.isTrue("53")) {
      for (int i = 0; i < boutonsDep.length; i++) {
        boutonsDep[i].setEnabled(false);
      }
      for (int i = 0; i < boutonsDom.length; i++) {
        boutonsDom[i].setEnabled(false);
      }
    }
    else {
      for (int i = 0; i < boutonsDep.length; i++) {
        boutonsDep[i].setEnabled(true);
      }
      for (int i = 0; i < boutonsDom.length; i++) {
        boutonsDom[i].setEnabled(true);
      }
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    for (int i = 0; i < 10; i++) {
      System.err
          .println("DOM n°" + (i + 1) + " : " + lexique.HostFieldGetData("ZGD" + ("" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))) + "2"));
    }
    
  }
  
  private void ecritDep() {
    JToggleButton[] boutonsDep = { toggleButton1, toggleButton2, toggleButton3, toggleButton4, toggleButton5, toggleButton6, toggleButton7,
        toggleButton8, toggleButton9, toggleButton10, toggleButton11, toggleButton12, toggleButton13, toggleButton14, toggleButton15,
        toggleButton16, toggleButton17, toggleButton18, toggleButton19, toggleButton20, toggleButton21, toggleButton22, toggleButton23,
        toggleButton24, toggleButton25, toggleButton26, toggleButton27, toggleButton28, toggleButton29, toggleButton30, toggleButton31,
        toggleButton32, toggleButton33, toggleButton34, toggleButton35, toggleButton36, toggleButton37, toggleButton38, toggleButton39,
        toggleButton40, toggleButton41, toggleButton42, toggleButton43, toggleButton44, toggleButton45, toggleButton46, toggleButton47,
        toggleButton48, toggleButton49, toggleButton50, toggleButton51, toggleButton52, toggleButton53, toggleButton54, toggleButton55,
        toggleButton56, toggleButton57, toggleButton58, toggleButton59, toggleButton60, toggleButton61, toggleButton62, toggleButton63,
        toggleButton64, toggleButton65, toggleButton66, toggleButton67, toggleButton68, toggleButton69, toggleButton70, toggleButton71,
        toggleButton72, toggleButton73, toggleButton74, toggleButton75, toggleButton76, toggleButton77, toggleButton78, toggleButton79,
        toggleButton80, toggleButton81, toggleButton82, toggleButton83, toggleButton84, toggleButton85, toggleButton86, toggleButton87,
        toggleButton88, toggleButton89, toggleButton90, toggleButton91, toggleButton92, toggleButton93, toggleButton94, toggleButton95 };
    // mise à blanc des zones département
    for (int i = 0; i < 40; i++) {
      lexique.HostFieldPutData("ZGD" + ("" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))), 0, "");
    }
    int cellule = 0;
    // si bouton coché on ajoute aux zones département
    for (int i = 0; i < boutonsDep.length; i++) {
      if (boutonsDep[i].isSelected()) {
        lexique.HostFieldPutData("ZGD" + ("" + ((cellule + 1) < 10 ? "0" + (cellule + 1) : (cellule + 1))), 0,
            ("" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))));
        cellule++;
      }
    }
  }
  
  private void ecritDom() {
    JToggleButton[] boutonsDom = { ZGD012, ZGD022, ZGD032, ZGD042, ZGD052, ZGD062, ZGD072, ZGD082, ZGD092, ZGD102 };
    // mise à blanc des zones département
    for (int i = 0; i < 10; i++) {
      lexique.HostFieldPutData("ZGD" + ("" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))) + "2", 0, "");
    }
    int cellule = 0;
    // si bouton coché on ajoute aux zones département
    for (int i = 0; i < boutonsDom.length; i++) {
      if (boutonsDom[i].isSelected()) {
        String dom = "000";
        switch (i) {
          case 0:
            dom = "971";
            break;
          case 1:
            dom = "972";
            break;
          case 2:
            dom = "973";
            break;
          case 3:
            dom = "974";
            break;
          case 4:
            dom = "975";
            break;
          case 5:
            dom = "976";
            break;
          case 6:
            dom = "984";
            break;
          case 7:
            dom = "986";
            break;
          case 8:
            dom = "987";
            break;
          case 9:
            dom = "988";
            break;
        }
        lexique.HostFieldPutData("ZGD" + ("" + ((cellule + 1) < 10 ? "0" + (cellule + 1) : (cellule + 1))) + "2", 0, dom);
        cellule++;
      }
    }
  }
  
  private boolean isDepPresent(String dep) {
    boolean retour = false;
    for (int i = 0; i < 40; i++) {
      if ((lexique.HostFieldGetData("ZGD" + ("" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))))).equals(dep)) {
        retour = true;
        break;
      }
    }
    return retour;
  }
  
  private boolean isDomPresent(String dep) {
    boolean retour = false;
    String dom = null;
    switch (Integer.parseInt(dep)) {
      case 0:
        dom = "971";
        break;
      case 1:
        dom = "972";
        break;
      case 2:
        dom = "973";
        break;
      case 3:
        dom = "974";
        break;
      case 4:
        dom = "975";
        break;
      case 5:
        dom = "976";
        break;
      case 6:
        dom = "984";
        break;
      case 7:
        dom = "986";
        break;
      case 8:
        dom = "987";
        break;
      case 9:
        dom = "988";
        break;
    }
    for (int i = 0; i < 10; i++) {
      if (lexique.HostFieldGetData("ZGD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)) + "2").equals(dom)) {
        retour = true;
        break;
      }
    }
    return retour;
  }
  
  private boolean plusDeQuarante() {
    if (nombreDep > 39) {
      if (warning != null) {
        warning.reveiller();
      }
      else {
        warning = new ZG_WARNING(lexique, interpreteurD);
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  private void toggleButtonAction(ActionEvent e) {
    JToggleButton bouton = (JToggleButton) e.getSource();
    
    if (bouton.isSelected()) {
      if (!plusDeQuarante()) {
        nombreDep++;
        ecritDep();
      }
      else {
        bouton.setSelected(false);
      }
    }
    else {
      nombreDep--;
      ecritDep();
    }
  }
  
  private void toggleDOMActionPerformed(ActionEvent e) {
    JToggleButton bouton = (JToggleButton) e.getSource();
    
    if (bouton.isSelected()) {
      if (!plusDeQuarante()) {
        nombreDep++;
        ecritDom();
      }
      else {
        bouton.setSelected(false);
      }
    }
    else {
      nombreDep--;
      ecritDom();
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    OBJ_52 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    panel3 = new JPanel();
    OBJ_269 = new JLabel();
    ZGLIB = new XRiTextField();
    panel4 = new JPanel();
    OBJ_276 = new JLabel();
    ZGREG = new XRiTextField();
    panel2 = new JPanel();
    toggleButton1 = new JToggleButton();
    toggleButton2 = new JToggleButton();
    toggleButton3 = new JToggleButton();
    toggleButton4 = new JToggleButton();
    toggleButton5 = new JToggleButton();
    toggleButton6 = new JToggleButton();
    toggleButton7 = new JToggleButton();
    toggleButton8 = new JToggleButton();
    toggleButton9 = new JToggleButton();
    toggleButton10 = new JToggleButton();
    toggleButton11 = new JToggleButton();
    toggleButton12 = new JToggleButton();
    toggleButton13 = new JToggleButton();
    toggleButton14 = new JToggleButton();
    toggleButton15 = new JToggleButton();
    toggleButton16 = new JToggleButton();
    toggleButton17 = new JToggleButton();
    toggleButton18 = new JToggleButton();
    toggleButton19 = new JToggleButton();
    toggleButton20 = new JToggleButton();
    toggleButton21 = new JToggleButton();
    toggleButton22 = new JToggleButton();
    toggleButton23 = new JToggleButton();
    toggleButton24 = new JToggleButton();
    toggleButton25 = new JToggleButton();
    toggleButton26 = new JToggleButton();
    toggleButton27 = new JToggleButton();
    toggleButton28 = new JToggleButton();
    toggleButton29 = new JToggleButton();
    toggleButton30 = new JToggleButton();
    toggleButton31 = new JToggleButton();
    toggleButton32 = new JToggleButton();
    toggleButton33 = new JToggleButton();
    toggleButton34 = new JToggleButton();
    toggleButton35 = new JToggleButton();
    toggleButton36 = new JToggleButton();
    toggleButton37 = new JToggleButton();
    toggleButton38 = new JToggleButton();
    toggleButton39 = new JToggleButton();
    toggleButton40 = new JToggleButton();
    toggleButton41 = new JToggleButton();
    toggleButton42 = new JToggleButton();
    toggleButton43 = new JToggleButton();
    toggleButton44 = new JToggleButton();
    toggleButton45 = new JToggleButton();
    toggleButton46 = new JToggleButton();
    toggleButton47 = new JToggleButton();
    toggleButton48 = new JToggleButton();
    toggleButton49 = new JToggleButton();
    toggleButton50 = new JToggleButton();
    toggleButton51 = new JToggleButton();
    toggleButton52 = new JToggleButton();
    toggleButton53 = new JToggleButton();
    toggleButton54 = new JToggleButton();
    toggleButton55 = new JToggleButton();
    toggleButton56 = new JToggleButton();
    toggleButton57 = new JToggleButton();
    toggleButton58 = new JToggleButton();
    toggleButton59 = new JToggleButton();
    toggleButton60 = new JToggleButton();
    toggleButton61 = new JToggleButton();
    toggleButton62 = new JToggleButton();
    toggleButton63 = new JToggleButton();
    toggleButton64 = new JToggleButton();
    toggleButton65 = new JToggleButton();
    toggleButton66 = new JToggleButton();
    toggleButton67 = new JToggleButton();
    toggleButton68 = new JToggleButton();
    toggleButton69 = new JToggleButton();
    toggleButton70 = new JToggleButton();
    toggleButton71 = new JToggleButton();
    toggleButton72 = new JToggleButton();
    toggleButton73 = new JToggleButton();
    toggleButton74 = new JToggleButton();
    toggleButton75 = new JToggleButton();
    toggleButton76 = new JToggleButton();
    toggleButton77 = new JToggleButton();
    toggleButton78 = new JToggleButton();
    toggleButton79 = new JToggleButton();
    toggleButton80 = new JToggleButton();
    toggleButton81 = new JToggleButton();
    toggleButton82 = new JToggleButton();
    toggleButton83 = new JToggleButton();
    toggleButton84 = new JToggleButton();
    toggleButton85 = new JToggleButton();
    toggleButton86 = new JToggleButton();
    toggleButton87 = new JToggleButton();
    toggleButton88 = new JToggleButton();
    toggleButton89 = new JToggleButton();
    toggleButton90 = new JToggleButton();
    toggleButton91 = new JToggleButton();
    toggleButton92 = new JToggleButton();
    toggleButton93 = new JToggleButton();
    toggleButton94 = new JToggleButton();
    toggleButton95 = new JToggleButton();
    ZGD012 = new JToggleButton();
    ZGD022 = new JToggleButton();
    ZGD032 = new JToggleButton();
    ZGD042 = new JToggleButton();
    ZGD052 = new JToggleButton();
    ZGD062 = new JToggleButton();
    ZGD072 = new JToggleButton();
    ZGD082 = new JToggleButton();
    ZGD102 = new JToggleButton();
    ZGD092 = new JToggleButton();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    xTitledPanel5 = new JXTitledPanel();
    panel5 = new JPanel();
    OBJ_270 = new JLabel();
    ZGLIB2 = new XRiTextField();
    panel6 = new JPanel();
    OBJ_277 = new JLabel();
    ZGREG2 = new XRiTextField();
    panel7 = new JPanel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_45 ----
          OBJ_45.setText("Code");
          OBJ_45.setName("OBJ_45");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          //---- OBJ_52 ----
          OBJ_52.setText("@LIBZP@");
          OBJ_52.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_52.setOpaque(false);
          OBJ_52.setName("OBJ_52");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(770, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Zone g\u00e9ographique");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_269 ----
              OBJ_269.setText("Libell\u00e9");
              OBJ_269.setName("OBJ_269");
              panel3.add(OBJ_269);
              OBJ_269.setBounds(10, 12, 43, 20);

              //---- ZGLIB ----
              ZGLIB.setComponentPopupMenu(BTD);
              ZGLIB.setName("ZGLIB");
              panel3.add(ZGLIB);
              ZGLIB.setBounds(65, 8, 310, ZGLIB.getPreferredSize().height);
            }
            xTitledPanel4ContentContainer.add(panel3);
            panel3.setBounds(20, 10, 390, 45);

            //======== panel4 ========
            {
              panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_276 ----
              OBJ_276.setText("Zone regroupement");
              OBJ_276.setName("OBJ_276");
              panel4.add(OBJ_276);
              OBJ_276.setBounds(10, 14, 121, 16);

              //---- ZGREG ----
              ZGREG.setComponentPopupMenu(BTD);
              ZGREG.setName("ZGREG");
              panel4.add(ZGREG);
              ZGREG.setBounds(125, 8, 30, ZGREG.getPreferredSize().height);
            }
            xTitledPanel4ContentContainer.add(panel4);
            panel4.setBounds(535, 10, 175, 45);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("R\u00e9gion constitu\u00e9e des d\u00e9partements"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- toggleButton1 ----
              toggleButton1.setText("01 - Ain");
              toggleButton1.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton1.setName("toggleButton1");
              toggleButton1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton1);
              toggleButton1.setBounds(10, 25, 170, 18);

              //---- toggleButton2 ----
              toggleButton2.setText("02 - Aine");
              toggleButton2.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton2.setName("toggleButton2");
              toggleButton2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton2);
              toggleButton2.setBounds(10, 40, 170, 18);

              //---- toggleButton3 ----
              toggleButton3.setText("03 - Allier");
              toggleButton3.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton3.setName("toggleButton3");
              toggleButton3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton3);
              toggleButton3.setBounds(10, 55, 170, 18);

              //---- toggleButton4 ----
              toggleButton4.setText("04 - Alpes-de-Haute-Pro.");
              toggleButton4.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton4.setName("toggleButton4");
              toggleButton4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton4);
              toggleButton4.setBounds(10, 70, 170, 18);

              //---- toggleButton5 ----
              toggleButton5.setText("05 - Hautes-Alpes");
              toggleButton5.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton5.setName("toggleButton5");
              toggleButton5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton5);
              toggleButton5.setBounds(10, 85, 170, 18);

              //---- toggleButton6 ----
              toggleButton6.setText("06 - Alpes-Maritimes");
              toggleButton6.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton6.setName("toggleButton6");
              toggleButton6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton6);
              toggleButton6.setBounds(10, 100, 170, 18);

              //---- toggleButton7 ----
              toggleButton7.setText("07 - Ard\u00e8che");
              toggleButton7.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton7.setName("toggleButton7");
              toggleButton7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton7);
              toggleButton7.setBounds(10, 115, 170, 18);

              //---- toggleButton8 ----
              toggleButton8.setText("08 - Ardennes");
              toggleButton8.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton8.setName("toggleButton8");
              toggleButton8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton8);
              toggleButton8.setBounds(10, 130, 170, 18);

              //---- toggleButton9 ----
              toggleButton9.setText("09 - Ari\u00e8ge");
              toggleButton9.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton9.setName("toggleButton9");
              toggleButton9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton9);
              toggleButton9.setBounds(10, 145, 170, 18);

              //---- toggleButton10 ----
              toggleButton10.setText("10 - Aube");
              toggleButton10.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton10.setName("toggleButton10");
              toggleButton10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton10);
              toggleButton10.setBounds(10, 160, 170, 18);

              //---- toggleButton11 ----
              toggleButton11.setText("11 - Aude");
              toggleButton11.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton11.setName("toggleButton11");
              toggleButton11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton11);
              toggleButton11.setBounds(10, 175, 170, 18);

              //---- toggleButton12 ----
              toggleButton12.setText("12 - Aveyron");
              toggleButton12.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton12.setName("toggleButton12");
              toggleButton12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton12);
              toggleButton12.setBounds(10, 190, 170, 18);

              //---- toggleButton13 ----
              toggleButton13.setText("13 - Bouches-du-Rh\u00f4ne");
              toggleButton13.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton13.setName("toggleButton13");
              toggleButton13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton13);
              toggleButton13.setBounds(10, 205, 170, 18);

              //---- toggleButton14 ----
              toggleButton14.setText("14 - Calvados");
              toggleButton14.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton14.setName("toggleButton14");
              toggleButton14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton14);
              toggleButton14.setBounds(10, 220, 170, 18);

              //---- toggleButton15 ----
              toggleButton15.setText("15 - Cantal");
              toggleButton15.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton15.setName("toggleButton15");
              toggleButton15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton15);
              toggleButton15.setBounds(10, 235, 170, 18);

              //---- toggleButton16 ----
              toggleButton16.setText("16 - Charente");
              toggleButton16.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton16.setName("toggleButton16");
              toggleButton16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton16);
              toggleButton16.setBounds(10, 250, 170, 18);

              //---- toggleButton17 ----
              toggleButton17.setText("17 - Charente-Maritime");
              toggleButton17.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton17.setName("toggleButton17");
              toggleButton17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton17);
              toggleButton17.setBounds(10, 265, 170, 18);

              //---- toggleButton18 ----
              toggleButton18.setText("18 - Cher");
              toggleButton18.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton18.setName("toggleButton18");
              toggleButton18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton18);
              toggleButton18.setBounds(10, 280, 170, 18);

              //---- toggleButton19 ----
              toggleButton19.setText("19 - Corr\u00e8ze");
              toggleButton19.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton19.setName("toggleButton19");
              toggleButton19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton19);
              toggleButton19.setBounds(10, 295, 170, 18);

              //---- toggleButton20 ----
              toggleButton20.setText("20 - Corse");
              toggleButton20.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton20.setName("toggleButton20");
              toggleButton20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton20);
              toggleButton20.setBounds(10, 310, 170, 18);

              //---- toggleButton21 ----
              toggleButton21.setText("21 - C\u00f4te-d'Or");
              toggleButton21.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton21.setName("toggleButton21");
              toggleButton21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton21);
              toggleButton21.setBounds(10, 325, 170, 18);

              //---- toggleButton22 ----
              toggleButton22.setText("22 - C\u00f4tes-d'Armor");
              toggleButton22.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton22.setName("toggleButton22");
              toggleButton22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton22);
              toggleButton22.setBounds(10, 340, 170, 18);

              //---- toggleButton23 ----
              toggleButton23.setText("23 - Creuse");
              toggleButton23.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton23.setName("toggleButton23");
              toggleButton23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton23);
              toggleButton23.setBounds(10, 355, 170, 18);

              //---- toggleButton24 ----
              toggleButton24.setText("24 - Dordogne");
              toggleButton24.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton24.setName("toggleButton24");
              toggleButton24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton24);
              toggleButton24.setBounds(10, 370, 170, 18);

              //---- toggleButton25 ----
              toggleButton25.setText("25 - Doubs");
              toggleButton25.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton25.setName("toggleButton25");
              toggleButton25.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton25);
              toggleButton25.setBounds(10, 385, 170, 18);

              //---- toggleButton26 ----
              toggleButton26.setText("26 - Dr\u00f4me");
              toggleButton26.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton26.setName("toggleButton26");
              toggleButton26.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton26);
              toggleButton26.setBounds(10, 400, 170, 18);

              //---- toggleButton27 ----
              toggleButton27.setText("27 - Eure");
              toggleButton27.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton27.setName("toggleButton27");
              toggleButton27.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton27);
              toggleButton27.setBounds(10, 415, 170, 18);

              //---- toggleButton28 ----
              toggleButton28.setText("28 - Eure et Loir");
              toggleButton28.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton28.setName("toggleButton28");
              toggleButton28.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton28);
              toggleButton28.setBounds(10, 430, 170, 18);

              //---- toggleButton29 ----
              toggleButton29.setText("29- Finist\u00e8re");
              toggleButton29.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton29.setName("toggleButton29");
              toggleButton29.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton29);
              toggleButton29.setBounds(180, 25, 170, 18);

              //---- toggleButton30 ----
              toggleButton30.setText("30 - Gard");
              toggleButton30.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton30.setName("toggleButton30");
              toggleButton30.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton30);
              toggleButton30.setBounds(180, 40, 170, 18);

              //---- toggleButton31 ----
              toggleButton31.setText("31 - Haute Garonne");
              toggleButton31.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton31.setName("toggleButton31");
              toggleButton31.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton31);
              toggleButton31.setBounds(180, 55, 170, 18);

              //---- toggleButton32 ----
              toggleButton32.setText("32 - Gers");
              toggleButton32.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton32.setName("toggleButton32");
              toggleButton32.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton32);
              toggleButton32.setBounds(180, 70, 170, 18);

              //---- toggleButton33 ----
              toggleButton33.setText("33 - Gironde");
              toggleButton33.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton33.setName("toggleButton33");
              toggleButton33.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton33);
              toggleButton33.setBounds(180, 85, 170, 18);

              //---- toggleButton34 ----
              toggleButton34.setText("34 - H\u00e9rault");
              toggleButton34.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton34.setName("toggleButton34");
              toggleButton34.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton34);
              toggleButton34.setBounds(180, 100, 170, 18);

              //---- toggleButton35 ----
              toggleButton35.setText("35 - Ille-et-Vilaine");
              toggleButton35.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton35.setName("toggleButton35");
              toggleButton35.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton35);
              toggleButton35.setBounds(180, 115, 170, 18);

              //---- toggleButton36 ----
              toggleButton36.setText("36 - Indre");
              toggleButton36.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton36.setName("toggleButton36");
              toggleButton36.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton36);
              toggleButton36.setBounds(180, 130, 170, 18);

              //---- toggleButton37 ----
              toggleButton37.setText("37 - Indre-et-Loire");
              toggleButton37.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton37.setName("toggleButton37");
              toggleButton37.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton37);
              toggleButton37.setBounds(180, 145, 170, 18);

              //---- toggleButton38 ----
              toggleButton38.setText("38 - Is\u00e8re");
              toggleButton38.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton38.setName("toggleButton38");
              toggleButton38.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton38);
              toggleButton38.setBounds(180, 160, 170, 18);

              //---- toggleButton39 ----
              toggleButton39.setText("39 - Jura");
              toggleButton39.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton39.setName("toggleButton39");
              toggleButton39.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton39);
              toggleButton39.setBounds(180, 175, 170, 18);

              //---- toggleButton40 ----
              toggleButton40.setText("40 - Landes");
              toggleButton40.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton40.setName("toggleButton40");
              toggleButton40.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton40);
              toggleButton40.setBounds(180, 190, 170, 18);

              //---- toggleButton41 ----
              toggleButton41.setText("41 - Loir et Cher");
              toggleButton41.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton41.setName("toggleButton41");
              toggleButton41.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton41);
              toggleButton41.setBounds(180, 205, 170, 18);

              //---- toggleButton42 ----
              toggleButton42.setText("42 - Loire");
              toggleButton42.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton42.setName("toggleButton42");
              toggleButton42.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton42);
              toggleButton42.setBounds(180, 220, 170, 18);

              //---- toggleButton43 ----
              toggleButton43.setText("43 - Haute Loire");
              toggleButton43.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton43.setName("toggleButton43");
              toggleButton43.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton43);
              toggleButton43.setBounds(180, 235, 170, 18);

              //---- toggleButton44 ----
              toggleButton44.setText("44 - Loire-Atlantique");
              toggleButton44.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton44.setName("toggleButton44");
              toggleButton44.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton44);
              toggleButton44.setBounds(180, 250, 170, 18);

              //---- toggleButton45 ----
              toggleButton45.setText("45 - Loiret");
              toggleButton45.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton45.setName("toggleButton45");
              toggleButton45.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton45);
              toggleButton45.setBounds(180, 265, 170, 18);

              //---- toggleButton46 ----
              toggleButton46.setText("46 - Lot");
              toggleButton46.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton46.setName("toggleButton46");
              toggleButton46.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton46);
              toggleButton46.setBounds(180, 280, 170, 18);

              //---- toggleButton47 ----
              toggleButton47.setText("47 - Lot-et-Garonne");
              toggleButton47.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton47.setName("toggleButton47");
              toggleButton47.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton47);
              toggleButton47.setBounds(180, 295, 170, 18);

              //---- toggleButton48 ----
              toggleButton48.setText("48 - Loz\u00e8re");
              toggleButton48.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton48.setName("toggleButton48");
              toggleButton48.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton48);
              toggleButton48.setBounds(180, 310, 170, 18);

              //---- toggleButton49 ----
              toggleButton49.setText("49 - Maine et Loire");
              toggleButton49.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton49.setName("toggleButton49");
              toggleButton49.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton49);
              toggleButton49.setBounds(180, 325, 170, 18);

              //---- toggleButton50 ----
              toggleButton50.setText("50 - Manche");
              toggleButton50.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton50.setName("toggleButton50");
              toggleButton50.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton50);
              toggleButton50.setBounds(180, 340, 170, 18);

              //---- toggleButton51 ----
              toggleButton51.setText("51 - Marne");
              toggleButton51.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton51.setName("toggleButton51");
              toggleButton51.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton51);
              toggleButton51.setBounds(180, 355, 170, 18);

              //---- toggleButton52 ----
              toggleButton52.setText("52 - Haute-Marne");
              toggleButton52.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton52.setName("toggleButton52");
              toggleButton52.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton52);
              toggleButton52.setBounds(180, 370, 170, 18);

              //---- toggleButton53 ----
              toggleButton53.setText("53 - Mayenne");
              toggleButton53.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton53.setName("toggleButton53");
              toggleButton53.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton53);
              toggleButton53.setBounds(180, 385, 170, 18);

              //---- toggleButton54 ----
              toggleButton54.setText("54 - Meurthe et Moselle");
              toggleButton54.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton54.setName("toggleButton54");
              toggleButton54.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton54);
              toggleButton54.setBounds(180, 400, 170, 18);

              //---- toggleButton55 ----
              toggleButton55.setText("55 - Meuse");
              toggleButton55.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton55.setName("toggleButton55");
              toggleButton55.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton55);
              toggleButton55.setBounds(180, 415, 170, 18);

              //---- toggleButton56 ----
              toggleButton56.setText("56 - Morbihan");
              toggleButton56.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton56.setName("toggleButton56");
              toggleButton56.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton56);
              toggleButton56.setBounds(180, 430, 170, 18);

              //---- toggleButton57 ----
              toggleButton57.setText("57 - Moselle");
              toggleButton57.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton57.setName("toggleButton57");
              toggleButton57.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton57);
              toggleButton57.setBounds(350, 25, 170, 18);

              //---- toggleButton58 ----
              toggleButton58.setText("58 - Ni\u00e8vre");
              toggleButton58.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton58.setName("toggleButton58");
              toggleButton58.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton58);
              toggleButton58.setBounds(350, 40, 170, 18);

              //---- toggleButton59 ----
              toggleButton59.setText("59 - Nord");
              toggleButton59.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton59.setName("toggleButton59");
              toggleButton59.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton59);
              toggleButton59.setBounds(350, 55, 170, 18);

              //---- toggleButton60 ----
              toggleButton60.setText("60 - Oise");
              toggleButton60.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton60.setName("toggleButton60");
              toggleButton60.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton60);
              toggleButton60.setBounds(350, 70, 170, 18);

              //---- toggleButton61 ----
              toggleButton61.setText("61 - Orne");
              toggleButton61.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton61.setName("toggleButton61");
              toggleButton61.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton61);
              toggleButton61.setBounds(350, 85, 170, 18);

              //---- toggleButton62 ----
              toggleButton62.setText("62 - Pas-de-Calais");
              toggleButton62.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton62.setName("toggleButton62");
              toggleButton62.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton62);
              toggleButton62.setBounds(350, 100, 170, 18);

              //---- toggleButton63 ----
              toggleButton63.setText("63 - Puy-de-D\u00f4me");
              toggleButton63.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton63.setName("toggleButton63");
              toggleButton63.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton63);
              toggleButton63.setBounds(350, 115, 170, 18);

              //---- toggleButton64 ----
              toggleButton64.setText("64 - Pyr\u00e9n\u00e9es atlantiques");
              toggleButton64.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton64.setName("toggleButton64");
              toggleButton64.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton64);
              toggleButton64.setBounds(350, 130, 170, 18);

              //---- toggleButton65 ----
              toggleButton65.setText("65 - Hautes Pyr\u00e9n\u00e9es");
              toggleButton65.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton65.setName("toggleButton65");
              toggleButton65.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton65);
              toggleButton65.setBounds(350, 145, 170, 18);

              //---- toggleButton66 ----
              toggleButton66.setText("66 - Pyr\u00e9n\u00e9es orientales");
              toggleButton66.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton66.setName("toggleButton66");
              toggleButton66.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton66);
              toggleButton66.setBounds(350, 160, 170, 18);

              //---- toggleButton67 ----
              toggleButton67.setText("67 - Bas-Rhin");
              toggleButton67.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton67.setName("toggleButton67");
              toggleButton67.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton67);
              toggleButton67.setBounds(350, 175, 170, 18);

              //---- toggleButton68 ----
              toggleButton68.setText("68 - Haut-Rhin");
              toggleButton68.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton68.setName("toggleButton68");
              toggleButton68.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton68);
              toggleButton68.setBounds(350, 190, 170, 18);

              //---- toggleButton69 ----
              toggleButton69.setText("69 - Rh\u00f4ne");
              toggleButton69.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton69.setName("toggleButton69");
              toggleButton69.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton69);
              toggleButton69.setBounds(350, 205, 170, 18);

              //---- toggleButton70 ----
              toggleButton70.setText("70 - Haute-Sa\u00f4ne");
              toggleButton70.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton70.setName("toggleButton70");
              toggleButton70.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton70);
              toggleButton70.setBounds(350, 220, 170, 18);

              //---- toggleButton71 ----
              toggleButton71.setText("71 - Sa\u00f4ne-et-Loire");
              toggleButton71.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton71.setName("toggleButton71");
              toggleButton71.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton71);
              toggleButton71.setBounds(350, 235, 170, 18);

              //---- toggleButton72 ----
              toggleButton72.setText("72 - Sarthe");
              toggleButton72.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton72.setName("toggleButton72");
              toggleButton72.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton72);
              toggleButton72.setBounds(350, 250, 170, 18);

              //---- toggleButton73 ----
              toggleButton73.setText("73 - Savoie");
              toggleButton73.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton73.setName("toggleButton73");
              toggleButton73.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton73);
              toggleButton73.setBounds(350, 265, 170, 18);

              //---- toggleButton74 ----
              toggleButton74.setText("74 - Haute-Savoie");
              toggleButton74.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton74.setName("toggleButton74");
              toggleButton74.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton74);
              toggleButton74.setBounds(350, 280, 170, 18);

              //---- toggleButton75 ----
              toggleButton75.setText("75 - Paris (Ville de)");
              toggleButton75.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton75.setName("toggleButton75");
              toggleButton75.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton75);
              toggleButton75.setBounds(350, 295, 170, 18);

              //---- toggleButton76 ----
              toggleButton76.setText("76 - Seine-Maritime");
              toggleButton76.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton76.setName("toggleButton76");
              toggleButton76.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton76);
              toggleButton76.setBounds(350, 310, 170, 18);

              //---- toggleButton77 ----
              toggleButton77.setText("77 - Seine-et-Marne");
              toggleButton77.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton77.setName("toggleButton77");
              toggleButton77.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton77);
              toggleButton77.setBounds(350, 325, 170, 18);

              //---- toggleButton78 ----
              toggleButton78.setText("78 - Yvelines");
              toggleButton78.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton78.setName("toggleButton78");
              toggleButton78.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton78);
              toggleButton78.setBounds(350, 340, 170, 18);

              //---- toggleButton79 ----
              toggleButton79.setText("79 - Deux-S\u00e8vres");
              toggleButton79.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton79.setName("toggleButton79");
              toggleButton79.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton79);
              toggleButton79.setBounds(350, 355, 170, 18);

              //---- toggleButton80 ----
              toggleButton80.setText("80 - Somme");
              toggleButton80.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton80.setName("toggleButton80");
              toggleButton80.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton80);
              toggleButton80.setBounds(350, 370, 170, 18);

              //---- toggleButton81 ----
              toggleButton81.setText("81 - Tarn");
              toggleButton81.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton81.setName("toggleButton81");
              toggleButton81.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton81);
              toggleButton81.setBounds(350, 385, 170, 18);

              //---- toggleButton82 ----
              toggleButton82.setText("82 - Tarn-et-Garonne");
              toggleButton82.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton82.setName("toggleButton82");
              toggleButton82.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton82);
              toggleButton82.setBounds(350, 400, 170, 18);

              //---- toggleButton83 ----
              toggleButton83.setText("83 -Var");
              toggleButton83.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton83.setName("toggleButton83");
              toggleButton83.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton83);
              toggleButton83.setBounds(350, 415, 170, 18);

              //---- toggleButton84 ----
              toggleButton84.setText("84 - Vaucluse");
              toggleButton84.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton84.setName("toggleButton84");
              toggleButton84.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton84);
              toggleButton84.setBounds(350, 430, 170, 18);

              //---- toggleButton85 ----
              toggleButton85.setText("85 - Vend\u00e9e");
              toggleButton85.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton85.setName("toggleButton85");
              toggleButton85.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton85);
              toggleButton85.setBounds(520, 25, 170, 18);

              //---- toggleButton86 ----
              toggleButton86.setText("86 - Vienne");
              toggleButton86.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton86.setName("toggleButton86");
              toggleButton86.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton86);
              toggleButton86.setBounds(520, 40, 170, 18);

              //---- toggleButton87 ----
              toggleButton87.setText("87 - Haute-Vienne");
              toggleButton87.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton87.setName("toggleButton87");
              toggleButton87.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton87);
              toggleButton87.setBounds(520, 55, 170, 18);

              //---- toggleButton88 ----
              toggleButton88.setText("88 - Vosges");
              toggleButton88.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton88.setName("toggleButton88");
              toggleButton88.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton88);
              toggleButton88.setBounds(520, 70, 170, 18);

              //---- toggleButton89 ----
              toggleButton89.setText("89 - Yonne");
              toggleButton89.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton89.setName("toggleButton89");
              toggleButton89.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton89);
              toggleButton89.setBounds(520, 85, 170, 18);

              //---- toggleButton90 ----
              toggleButton90.setText("90 - Territoire de Belfort");
              toggleButton90.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton90.setName("toggleButton90");
              toggleButton90.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton90);
              toggleButton90.setBounds(520, 100, 170, 18);

              //---- toggleButton91 ----
              toggleButton91.setText("91 - Essonne");
              toggleButton91.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton91.setName("toggleButton91");
              toggleButton91.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton91);
              toggleButton91.setBounds(520, 115, 170, 18);

              //---- toggleButton92 ----
              toggleButton92.setText("92 - Hauts-de-Seine");
              toggleButton92.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton92.setName("toggleButton92");
              toggleButton92.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton92);
              toggleButton92.setBounds(520, 130, 170, 18);

              //---- toggleButton93 ----
              toggleButton93.setText("93 - Seine-Saint-Denis");
              toggleButton93.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton93.setName("toggleButton93");
              toggleButton93.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton93);
              toggleButton93.setBounds(520, 145, 170, 18);

              //---- toggleButton94 ----
              toggleButton94.setText("94 - Val-de-Marne");
              toggleButton94.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton94.setName("toggleButton94");
              toggleButton94.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton94);
              toggleButton94.setBounds(520, 160, 170, 18);

              //---- toggleButton95 ----
              toggleButton95.setText("95 - Val-d'Oise");
              toggleButton95.setHorizontalAlignment(SwingConstants.LEFT);
              toggleButton95.setName("toggleButton95");
              toggleButton95.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleButtonAction(e);
                }
              });
              panel2.add(toggleButton95);
              toggleButton95.setBounds(520, 175, 170, 18);

              //---- ZGD012 ----
              ZGD012.setText("971 - Guadeloupe");
              ZGD012.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD012.setName("ZGD012");
              ZGD012.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD012);
              ZGD012.setBounds(520, 220, 170, 18);

              //---- ZGD022 ----
              ZGD022.setText("972 - Martinique");
              ZGD022.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD022.setName("ZGD022");
              ZGD022.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD022);
              ZGD022.setBounds(520, 235, 170, 18);

              //---- ZGD032 ----
              ZGD032.setText("973 - Guyane");
              ZGD032.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD032.setName("ZGD032");
              ZGD032.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD032);
              ZGD032.setBounds(520, 250, 170, 18);

              //---- ZGD042 ----
              ZGD042.setText("974 - La r\u00e9union");
              ZGD042.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD042.setName("ZGD042");
              ZGD042.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD042);
              ZGD042.setBounds(520, 265, 170, 18);

              //---- ZGD052 ----
              ZGD052.setText("975 - St-Pierre-et-Miquelon");
              ZGD052.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD052.setName("ZGD052");
              ZGD052.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD052);
              ZGD052.setBounds(520, 280, 170, 18);

              //---- ZGD062 ----
              ZGD062.setText("976 - Mayotte");
              ZGD062.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD062.setName("ZGD062");
              ZGD062.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD062);
              ZGD062.setBounds(520, 295, 170, 18);

              //---- ZGD072 ----
              ZGD072.setText("984-Terres-Australes");
              ZGD072.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD072.setName("ZGD072");
              ZGD072.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD072);
              ZGD072.setBounds(520, 310, 170, 18);

              //---- ZGD082 ----
              ZGD082.setText("986-Wallis-et-Futuna");
              ZGD082.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD082.setName("ZGD082");
              ZGD082.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD082);
              ZGD082.setBounds(520, 325, 170, 18);

              //---- ZGD102 ----
              ZGD102.setText("988-Nouvelle-Cal\u00e9donie");
              ZGD102.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD102.setName("ZGD102");
              ZGD102.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD102);
              ZGD102.setBounds(520, 355, 170, 18);

              //---- ZGD092 ----
              ZGD092.setText("987-Polyn\u00e9sie Fran\u00e7aise");
              ZGD092.setHorizontalAlignment(SwingConstants.LEFT);
              ZGD092.setName("ZGD092");
              ZGD092.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  toggleDOMActionPerformed(e);
                }
              });
              panel2.add(ZGD092);
              ZGD092.setBounds(520, 340, 170, 18);
            }
            xTitledPanel4ContentContainer.add(panel2);
            panel2.setBounds(15, 60, 700, 460);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 740, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(10, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //======== xTitledPanel5 ========
    {
      xTitledPanel5.setTitle("Zone g\u00e9ographique");
      xTitledPanel5.setBorder(new DropShadowBorder());
      xTitledPanel5.setTitleFont(new Font("sansserif", Font.BOLD, 12));
      xTitledPanel5.setName("xTitledPanel5");
      Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
      xTitledPanel5ContentContainer.setLayout(null);

      //======== panel5 ========
      {
        panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
        panel5.setOpaque(false);
        panel5.setName("panel5");
        panel5.setLayout(null);

        //---- OBJ_270 ----
        OBJ_270.setText("Libell\u00e9");
        OBJ_270.setName("OBJ_270");
        panel5.add(OBJ_270);
        OBJ_270.setBounds(10, 12, 43, 20);

        //---- ZGLIB2 ----
        ZGLIB2.setComponentPopupMenu(BTD);
        ZGLIB2.setName("ZGLIB2");
        panel5.add(ZGLIB2);
        ZGLIB2.setBounds(65, 8, 310, ZGLIB2.getPreferredSize().height);
      }
      xTitledPanel5ContentContainer.add(panel5);
      panel5.setBounds(20, 10, 390, 45);

      //======== panel6 ========
      {
        panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
        panel6.setOpaque(false);
        panel6.setName("panel6");
        panel6.setLayout(null);

        //---- OBJ_277 ----
        OBJ_277.setText("Zone regroupement");
        OBJ_277.setName("OBJ_277");
        panel6.add(OBJ_277);
        OBJ_277.setBounds(10, 14, 121, 16);

        //---- ZGREG2 ----
        ZGREG2.setComponentPopupMenu(BTD);
        ZGREG2.setName("ZGREG2");
        panel6.add(ZGREG2);
        ZGREG2.setBounds(125, 8, 30, ZGREG2.getPreferredSize().height);
      }
      xTitledPanel5ContentContainer.add(panel6);
      panel6.setBounds(535, 10, 175, 45);

      //======== panel7 ========
      {
        panel7.setBorder(new TitledBorder("R\u00e9gion constitu\u00e9e des d\u00e9partements"));
        panel7.setOpaque(false);
        panel7.setName("panel7");
        panel7.setLayout(new GridLayout(28, 4));
      }
      xTitledPanel5ContentContainer.add(panel7);
      panel7.setBounds(15, 60, 700, 460);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
          Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = xTitledPanel5ContentContainer.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
        xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
      }
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_45;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private RiZoneSortie OBJ_52;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private JPanel panel3;
  private JLabel OBJ_269;
  private XRiTextField ZGLIB;
  private JPanel panel4;
  private JLabel OBJ_276;
  private XRiTextField ZGREG;
  private JPanel panel2;
  private JToggleButton toggleButton1;
  private JToggleButton toggleButton2;
  private JToggleButton toggleButton3;
  private JToggleButton toggleButton4;
  private JToggleButton toggleButton5;
  private JToggleButton toggleButton6;
  private JToggleButton toggleButton7;
  private JToggleButton toggleButton8;
  private JToggleButton toggleButton9;
  private JToggleButton toggleButton10;
  private JToggleButton toggleButton11;
  private JToggleButton toggleButton12;
  private JToggleButton toggleButton13;
  private JToggleButton toggleButton14;
  private JToggleButton toggleButton15;
  private JToggleButton toggleButton16;
  private JToggleButton toggleButton17;
  private JToggleButton toggleButton18;
  private JToggleButton toggleButton19;
  private JToggleButton toggleButton20;
  private JToggleButton toggleButton21;
  private JToggleButton toggleButton22;
  private JToggleButton toggleButton23;
  private JToggleButton toggleButton24;
  private JToggleButton toggleButton25;
  private JToggleButton toggleButton26;
  private JToggleButton toggleButton27;
  private JToggleButton toggleButton28;
  private JToggleButton toggleButton29;
  private JToggleButton toggleButton30;
  private JToggleButton toggleButton31;
  private JToggleButton toggleButton32;
  private JToggleButton toggleButton33;
  private JToggleButton toggleButton34;
  private JToggleButton toggleButton35;
  private JToggleButton toggleButton36;
  private JToggleButton toggleButton37;
  private JToggleButton toggleButton38;
  private JToggleButton toggleButton39;
  private JToggleButton toggleButton40;
  private JToggleButton toggleButton41;
  private JToggleButton toggleButton42;
  private JToggleButton toggleButton43;
  private JToggleButton toggleButton44;
  private JToggleButton toggleButton45;
  private JToggleButton toggleButton46;
  private JToggleButton toggleButton47;
  private JToggleButton toggleButton48;
  private JToggleButton toggleButton49;
  private JToggleButton toggleButton50;
  private JToggleButton toggleButton51;
  private JToggleButton toggleButton52;
  private JToggleButton toggleButton53;
  private JToggleButton toggleButton54;
  private JToggleButton toggleButton55;
  private JToggleButton toggleButton56;
  private JToggleButton toggleButton57;
  private JToggleButton toggleButton58;
  private JToggleButton toggleButton59;
  private JToggleButton toggleButton60;
  private JToggleButton toggleButton61;
  private JToggleButton toggleButton62;
  private JToggleButton toggleButton63;
  private JToggleButton toggleButton64;
  private JToggleButton toggleButton65;
  private JToggleButton toggleButton66;
  private JToggleButton toggleButton67;
  private JToggleButton toggleButton68;
  private JToggleButton toggleButton69;
  private JToggleButton toggleButton70;
  private JToggleButton toggleButton71;
  private JToggleButton toggleButton72;
  private JToggleButton toggleButton73;
  private JToggleButton toggleButton74;
  private JToggleButton toggleButton75;
  private JToggleButton toggleButton76;
  private JToggleButton toggleButton77;
  private JToggleButton toggleButton78;
  private JToggleButton toggleButton79;
  private JToggleButton toggleButton80;
  private JToggleButton toggleButton81;
  private JToggleButton toggleButton82;
  private JToggleButton toggleButton83;
  private JToggleButton toggleButton84;
  private JToggleButton toggleButton85;
  private JToggleButton toggleButton86;
  private JToggleButton toggleButton87;
  private JToggleButton toggleButton88;
  private JToggleButton toggleButton89;
  private JToggleButton toggleButton90;
  private JToggleButton toggleButton91;
  private JToggleButton toggleButton92;
  private JToggleButton toggleButton93;
  private JToggleButton toggleButton94;
  private JToggleButton toggleButton95;
  private JToggleButton ZGD012;
  private JToggleButton ZGD022;
  private JToggleButton ZGD032;
  private JToggleButton ZGD042;
  private JToggleButton ZGD052;
  private JToggleButton ZGD062;
  private JToggleButton ZGD072;
  private JToggleButton ZGD082;
  private JToggleButton ZGD102;
  private JToggleButton ZGD092;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JXTitledPanel xTitledPanel5;
  private JPanel panel5;
  private JLabel OBJ_270;
  private XRiTextField ZGLIB2;
  private JPanel panel6;
  private JLabel OBJ_277;
  private XRiTextField ZGREG2;
  private JPanel panel7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
