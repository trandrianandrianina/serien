
package ri.serien.libecranrpg.vgvm.VGVM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM04FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 539, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VGVM04FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DSAC@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PROC@")).trim());
    OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOUR@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VALI@")).trim());
    OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    INDMEX.setEnabled(lexique.isPresent("INDMEX"));
    INDLIV.setEnabled(lexique.isPresent("INDLIV"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    WP2.setVisible(lexique.isPresent("WP2"));
    INDCLI.setEnabled(lexique.isPresent("INDCLI"));
    // OBJ_57.setVisible( lexique.isPresent("V01F"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - CLIENT/TOURNEE @LIBPG@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTDI.getInvoker().getName());
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTDI.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    // CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_56 = new JLabel();
    INDCLI = new XRiTextField();
    OBJ_52 = new JLabel();
    INDETB = new XRiTextField();
    INDLIV = new XRiTextField();
    INDMEX = new XRiTextField();
    OBJ_51 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_54 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTDI = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_8 = new JMenuItem();
    CMD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    WP2 = new XRiTextField();
    OBJ_69 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_71 = new JLabel();

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 520));
        menus_haut.setPreferredSize(new Dimension(160, 520));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu_V01F ========
        {
          riMenu_V01F.setMinimumSize(new Dimension(104, 50));
          riMenu_V01F.setPreferredSize(new Dimension(170, 50));
          riMenu_V01F.setMaximumSize(new Dimension(104, 50));
          riMenu_V01F.setName("riMenu_V01F");

          //---- riMenu_bt_V01F ----
          riMenu_bt_V01F.setText("@V01F@");
          riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
          riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
          riMenu_bt_V01F.setName("riMenu_bt_V01F");
          riMenu_V01F.add(riMenu_bt_V01F);
        }
        menus_haut.add(riMenu_V01F);

        //======== riSousMenu_consult ========
        {
          riSousMenu_consult.setName("riSousMenu_consult");

          //---- riSousMenu_bt_consult ----
          riSousMenu_bt_consult.setText("Consultation");
          riSousMenu_bt_consult.setToolTipText("Consultation");
          riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
          riSousMenu_consult.add(riSousMenu_bt_consult);
        }
        menus_haut.add(riSousMenu_consult);

        //======== riSousMenu_modif ========
        {
          riSousMenu_modif.setName("riSousMenu_modif");

          //---- riSousMenu_bt_modif ----
          riSousMenu_bt_modif.setText("Modification");
          riSousMenu_bt_modif.setToolTipText("Modification");
          riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
          riSousMenu_modif.add(riSousMenu_bt_modif);
        }
        menus_haut.add(riSousMenu_modif);

        //======== riSousMenu_crea ========
        {
          riSousMenu_crea.setName("riSousMenu_crea");

          //---- riSousMenu_bt_crea ----
          riSousMenu_bt_crea.setText("Cr\u00e9ation");
          riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
          riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
          riSousMenu_crea.add(riSousMenu_bt_crea);
        }
        menus_haut.add(riSousMenu_crea);

        //======== riSousMenu_suppr ========
        {
          riSousMenu_suppr.setName("riSousMenu_suppr");

          //---- riSousMenu_bt_suppr ----
          riSousMenu_bt_suppr.setText("Annulation");
          riSousMenu_bt_suppr.setToolTipText("Annulation");
          riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
          riSousMenu_suppr.add(riSousMenu_bt_suppr);
        }
        menus_haut.add(riSousMenu_suppr);

        //======== riSousMenuF_dupli ========
        {
          riSousMenuF_dupli.setName("riSousMenuF_dupli");

          //---- riSousMenu_bt_dupli ----
          riSousMenu_bt_dupli.setText("Duplication");
          riSousMenu_bt_dupli.setToolTipText("Duplication");
          riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
          riSousMenuF_dupli.add(riSousMenu_bt_dupli);
        }
        menus_haut.add(riSousMenuF_dupli);

        //======== riSousMenu_rappel ========
        {
          riSousMenu_rappel.setName("riSousMenu_rappel");

          //---- riSousMenu_bt_rappel ----
          riSousMenu_bt_rappel.setText("Rappel");
          riSousMenu_bt_rappel.setToolTipText("Rappel");
          riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
          riSousMenu_rappel.add(riSousMenu_bt_rappel);
        }
        menus_haut.add(riSousMenu_rappel);

        //======== riSousMenu_reac ========
        {
          riSousMenu_reac.setName("riSousMenu_reac");

          //---- riSousMenu_bt_reac ----
          riSousMenu_bt_reac.setText("R\u00e9activation");
          riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
          riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
          riSousMenu_reac.add(riSousMenu_bt_reac);
        }
        menus_haut.add(riSousMenu_reac);

        //======== riSousMenu_destr ========
        {
          riSousMenu_destr.setName("riSousMenu_destr");

          //---- riSousMenu_bt_destr ----
          riSousMenu_bt_destr.setText("Suppression");
          riSousMenu_bt_destr.setToolTipText("Suppression");
          riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
          riSousMenu_destr.add(riSousMenu_bt_destr);
        }
        menus_haut.add(riSousMenu_destr);

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt6 ----
          riSousMenu_bt6.setText("text");
          riSousMenu_bt6.setName("riSousMenu_bt6");
          riSousMenu_bt6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt6ActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt6);
        }
        menus_haut.add(riSousMenu6);

        //======== riSousMenu7 ========
        {
          riSousMenu7.setName("riSousMenu7");

          //---- riSousMenu_bt7 ----
          riSousMenu_bt7.setText("text");
          riSousMenu_bt7.setName("riSousMenu_bt7");
          riSousMenu_bt7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt7ActionPerformed(e);
            }
          });
          riSousMenu7.add(riSousMenu_bt7);
        }
        menus_haut.add(riSousMenu7);

        //======== riSousMenu8 ========
        {
          riSousMenu8.setName("riSousMenu8");

          //---- riSousMenu_bt8 ----
          riSousMenu_bt8.setText("text");
          riSousMenu_bt8.setName("riSousMenu_bt8");
          riSousMenu_bt8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt8ActionPerformed(e);
            }
          });
          riSousMenu8.add(riSousMenu_bt8);
        }
        menus_haut.add(riSousMenu8);

        //======== riSousMenu9 ========
        {
          riSousMenu9.setName("riSousMenu9");

          //---- riSousMenu_bt9 ----
          riSousMenu_bt9.setText("text");
          riSousMenu_bt9.setName("riSousMenu_bt9");
          riSousMenu_bt9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt9ActionPerformed(e);
            }
          });
          riSousMenu9.add(riSousMenu_bt9);
        }
        menus_haut.add(riSousMenu9);

        //======== riSousMenu10 ========
        {
          riSousMenu10.setName("riSousMenu10");

          //---- riSousMenu_bt10 ----
          riSousMenu_bt10.setText("text");
          riSousMenu_bt10.setName("riSousMenu_bt10");
          riSousMenu_bt10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt10ActionPerformed(e);
            }
          });
          riSousMenu10.add(riSousMenu_bt10);
        }
        menus_haut.add(riSousMenu10);

        //======== riSousMenu11 ========
        {
          riSousMenu11.setName("riSousMenu11");

          //---- riSousMenu_bt11 ----
          riSousMenu_bt11.setText("text");
          riSousMenu_bt11.setName("riSousMenu_bt11");
          riSousMenu_bt11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt11ActionPerformed(e);
            }
          });
          riSousMenu11.add(riSousMenu_bt11);
        }
        menus_haut.add(riSousMenu11);

        //======== riSousMenu12 ========
        {
          riSousMenu12.setName("riSousMenu12");

          //---- riSousMenu_bt12 ----
          riSousMenu_bt12.setText("text");
          riSousMenu_bt12.setName("riSousMenu_bt12");
          riSousMenu_bt12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt12ActionPerformed(e);
            }
          });
          riSousMenu12.add(riSousMenu_bt12);
        }
        menus_haut.add(riSousMenu12);

        //======== riSousMenu13 ========
        {
          riSousMenu13.setName("riSousMenu13");

          //---- riSousMenu_bt13 ----
          riSousMenu_bt13.setText("text");
          riSousMenu_bt13.setName("riSousMenu_bt13");
          riSousMenu_bt13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt13ActionPerformed(e);
            }
          });
          riSousMenu13.add(riSousMenu_bt13);
        }
        menus_haut.add(riSousMenu13);

        //======== riMenu3 ========
        {
          riMenu3.setName("riMenu3");

          //---- riMenu_bt3 ----
          riMenu_bt3.setText("Outils");
          riMenu_bt3.setName("riMenu_bt3");
          riMenu3.add(riMenu_bt3);
        }
        menus_haut.add(riMenu3);

        //======== riSousMenu14 ========
        {
          riSousMenu14.setName("riSousMenu14");

          //---- riSousMenu_bt14 ----
          riSousMenu_bt14.setText("text");
          riSousMenu_bt14.setName("riSousMenu_bt14");
          riSousMenu_bt14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt14ActionPerformed(e);
            }
          });
          riSousMenu14.add(riSousMenu_bt14);
        }
        menus_haut.add(riSousMenu14);

        //======== riSousMenu15 ========
        {
          riSousMenu15.setName("riSousMenu15");

          //---- riSousMenu_bt15 ----
          riSousMenu_bt15.setText("text");
          riSousMenu_bt15.setName("riSousMenu_bt15");
          riSousMenu_bt15.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt15ActionPerformed(e);
            }
          });
          riSousMenu15.add(riSousMenu_bt15);
        }
        menus_haut.add(riSousMenu15);

        //======== riSousMenu16 ========
        {
          riSousMenu16.setName("riSousMenu16");

          //---- riSousMenu_bt16 ----
          riSousMenu_bt16.setText("text");
          riSousMenu_bt16.setName("riSousMenu_bt16");
          riSousMenu_bt16.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt16ActionPerformed(e);
            }
          });
          riSousMenu16.add(riSousMenu_bt16);
        }
        menus_haut.add(riSousMenu16);

        //======== riSousMenu17 ========
        {
          riSousMenu17.setName("riSousMenu17");

          //---- riSousMenu_bt17 ----
          riSousMenu_bt17.setText("text");
          riSousMenu_bt17.setName("riSousMenu_bt17");
          riSousMenu_bt17.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt17ActionPerformed(e);
            }
          });
          riSousMenu17.add(riSousMenu_bt17);
        }
        menus_haut.add(riSousMenu17);

        //======== riMenu4 ========
        {
          riMenu4.setName("riMenu4");

          //---- riMenu_bt4 ----
          riMenu_bt4.setText("Fonctions");
          riMenu_bt4.setName("riMenu_bt4");
          riMenu4.add(riMenu_bt4);
        }
        menus_haut.add(riMenu4);

        //======== riSousMenu18 ========
        {
          riSousMenu18.setName("riSousMenu18");

          //---- riSousMenu_bt18 ----
          riSousMenu_bt18.setText("text");
          riSousMenu_bt18.setName("riSousMenu_bt18");
          riSousMenu_bt18.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt18ActionPerformed(e);
            }
          });
          riSousMenu18.add(riSousMenu_bt18);
        }
        menus_haut.add(riSousMenu18);

        //======== riSousMenu19 ========
        {
          riSousMenu19.setName("riSousMenu19");

          //---- riSousMenu_bt19 ----
          riSousMenu_bt19.setText("text");
          riSousMenu_bt19.setName("riSousMenu_bt19");
          riSousMenu_bt19.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt19ActionPerformed(e);
            }
          });
          riSousMenu19.add(riSousMenu_bt19);
        }
        menus_haut.add(riSousMenu19);

        //======== riSousMenu20 ========
        {
          riSousMenu20.setName("riSousMenu20");

          //---- riSousMenu_bt20 ----
          riSousMenu_bt20.setText("text");
          riSousMenu_bt20.setName("riSousMenu_bt20");
          riSousMenu_bt20.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt20ActionPerformed(e);
            }
          });
          riSousMenu20.add(riSousMenu_bt20);
        }
        menus_haut.add(riSousMenu20);

        //======== riSousMenu21 ========
        {
          riSousMenu21.setName("riSousMenu21");

          //---- riSousMenu_bt21 ----
          riSousMenu_bt21.setText("text");
          riSousMenu_bt21.setName("riSousMenu_bt21");
          riSousMenu_bt21.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt21ActionPerformed(e);
            }
          });
          riSousMenu21.add(riSousMenu_bt21);
        }
        menus_haut.add(riSousMenu21);
      }
      scroll_droite.setViewportView(menus_haut);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Client/tourn\u00e9e");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_56 ----
          OBJ_56.setText("@DSAC@");
          OBJ_56.setName("OBJ_56");

          //---- INDCLI ----
          INDCLI.setComponentPopupMenu(BTDA);
          INDCLI.setName("INDCLI");

          //---- OBJ_52 ----
          OBJ_52.setText("Client");
          OBJ_52.setName("OBJ_52");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTDA);
          INDETB.setName("INDETB");

          //---- INDLIV ----
          INDLIV.setComponentPopupMenu(BTDA);
          INDLIV.setName("INDLIV");

          //---- INDMEX ----
          INDMEX.setComponentPopupMenu(BTDI);
          INDMEX.setName("INDMEX");

          //---- OBJ_51 ----
          OBJ_51.setText("Etablissement");
          OBJ_51.setName("OBJ_51");

          //---- OBJ_53 ----
          OBJ_53.setText("Livraison");
          OBJ_53.setName("OBJ_53");

          //---- OBJ_54 ----
          OBJ_54.setText("Exp\u00e9dition");
          OBJ_54.setName("OBJ_54");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(90, 90, 90)
                    .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(70, 70, 70)
                    .addComponent(INDMEX, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                .addGap(40, 40, 40)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(710, 410));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 40, 555, 268);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(585, 40, 25, 122);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(585, 185, 25, 122);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 633, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 341, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(38, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTDI ========
    {
      BTDI.setName("BTDI");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTDI.add(OBJ_6);
    }

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_8 ----
      OBJ_8.setText("Aide en ligne");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_8);
    }

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_10 ----
      OBJ_10.setText("Fin de Travail");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_12 ----
      OBJ_12.setText("R\u00e9afficher");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Recherche multi-crit\u00e8res");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Annuler");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
      CMD.addSeparator();

      //---- OBJ_15 ----
      OBJ_15.setText("Cr\u00e9ation");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      CMD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Modification");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      CMD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("Interrogation");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      CMD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Annulation");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      CMD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Duplication");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      CMD.add(OBJ_19);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_23 ----
      OBJ_23.setText("Modifier");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Supprimer");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("Interrogation");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
      BTD.addSeparator();

      //---- OBJ_31 ----
      OBJ_31.setText("Choix possibles");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);

      //---- OBJ_30 ----
      OBJ_30.setText("Aide en ligne");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);
    }

    //---- WP2 ----
    WP2.setComponentPopupMenu(BTD);
    WP2.setName("WP2");

    //---- OBJ_69 ----
    OBJ_69.setText("@PROC@");
    OBJ_69.setName("OBJ_69");

    //---- OBJ_70 ----
    OBJ_70.setText("@TOUR@");
    OBJ_70.setName("OBJ_70");

    //---- OBJ_68 ----
    OBJ_68.setText("@VALI@");
    OBJ_68.setName("OBJ_68");

    //---- OBJ_71 ----
    OBJ_71.setText("@ORDR@");
    OBJ_71.setName("OBJ_71");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_56;
  private XRiTextField INDCLI;
  private JLabel OBJ_52;
  private XRiTextField INDETB;
  private XRiTextField INDLIV;
  private XRiTextField INDMEX;
  private JLabel OBJ_51;
  private JLabel OBJ_53;
  private JLabel OBJ_54;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTDI;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_8;
  private JPopupMenu CMD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_30;
  private XRiTextField WP2;
  private JLabel OBJ_69;
  private JLabel OBJ_70;
  private JLabel OBJ_68;
  private JLabel OBJ_71;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
