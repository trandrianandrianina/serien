
package ri.serien.libecranrpg.vgvm.VGVM3DFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * Boite de dialogue permettant à un responsable d'autoriser le réglement en différée d'un document.
 */
public class VGVM3DFM_B2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM3DFM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    initDiverses();
    
    HDAUT.setValeurs("OUI", buttonGroup1);
    HDAUT_N.setValeurs("NON");
    
    // Initialiser les boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    HDUSR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HDUSR@")).trim());
    HDRES.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HDRES@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    gererLesErreurs("19");
    
    // Mettre à jour l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "E1ETB");
    snEtablissement.setEnabled(false);
    // Mettre à jour le magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "E1MAG");
    snMagasin.setEnabled(false);
    // Mettre à jour le vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "E1VDE");
    snVendeur.setEnabled(false);
    
    // Pour les heures inférieures à 10 car le 0 est supprimé à cause du code d'édition
    String hdtim = lexique.HostFieldGetData("HDTIM");
    if (hdtim.length() == 5) {
      hdtim = '0' + hdtim;
    }
    lbHeure.setText(String.format("à %sh %smn %ss", hdtim.substring(0, 2), hdtim.substring(2, 4), hdtim.substring(4, 6)));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1NUM");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipall = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    pnlInformations = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbNumeroDocument = new SNLabelChamp();
    E1NUM = new RiZoneSortie();
    E1SUF = new RiZoneSortie();
    riBoutonDetail = new SNBoutonDetail();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbMontantTTC = new SNLabelChamp();
    E1TTC = new RiZoneSortie();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbDateHeure = new SNLabelChamp();
    pnlDateHeure = new SNPanel();
    HDCREX = new XRiCalendrier();
    lbHeure = new JLabel();
    lbUtilisateur = new SNLabelChamp();
    HDUSR = new RiZoneSortie();
    lbDeblocage = new SNLabelChamp();
    pnlChoixReponse = new SNPanel();
    HDAUT = new XRiRadioButton();
    HDAUT_N = new XRiRadioButton();
    lbResponsable = new SNLabelChamp();
    HDRES = new RiZoneSortie();
    lbMotif = new SNLabelChamp();
    HDMOT = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(910, 350));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlPrincipall ========
    {
      pnlPrincipall.setPreferredSize(new Dimension(910, 345));
      pnlPrincipall.setName("pnlPrincipall");
      pnlPrincipall.setLayout(new BorderLayout());

      //======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setPreferredSize(new Dimension(1000, 340));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());

        //======== pnlInformations ========
        {
          pnlInformations.setOpaque(false);
          pnlInformations.setTitre("Autoriser la demande");
          pnlInformations.setPreferredSize(new Dimension(1000, 340));
          pnlInformations.setName("pnlInformations");
          pnlInformations.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlInformations.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlInformations.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlInformations.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlInformations.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setPreferredSize(new Dimension(100, 30));
          lbEtablissement.setMinimumSize(new Dimension(100, 30));
          lbEtablissement.setName("lbEtablissement");
          pnlInformations.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setMinimumSize(new Dimension(320, 30));
          snEtablissement.setPreferredSize(new Dimension(1130, 330));
          snEtablissement.setName("snEtablissement");
          pnlInformations.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbNumeroDocument ----
          lbNumeroDocument.setText("Num\u00e9ro de bon");
          lbNumeroDocument.setPreferredSize(new Dimension(130, 30));
          lbNumeroDocument.setMinimumSize(new Dimension(130, 30));
          lbNumeroDocument.setName("lbNumeroDocument");
          pnlInformations.add(lbNumeroDocument, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- E1NUM ----
          E1NUM.setText("@E1NUM@");
          E1NUM.setFont(E1NUM.getFont().deriveFont(E1NUM.getFont().getSize() + 2f));
          E1NUM.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NUM.setMinimumSize(new Dimension(100, 30));
          E1NUM.setPreferredSize(new Dimension(100, 30));
          E1NUM.setName("E1NUM");
          pnlInformations.add(E1NUM, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- E1SUF ----
          E1SUF.setText("@E1SUF@");
          E1SUF.setPreferredSize(new Dimension(25, 30));
          E1SUF.setMinimumSize(new Dimension(25, 20));
          E1SUF.setHorizontalAlignment(SwingConstants.CENTER);
          E1SUF.setName("E1SUF");
          pnlInformations.add(E1SUF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- riBoutonDetail ----
          riBoutonDetail.setName("riBoutonDetail");
          riBoutonDetail.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          pnlInformations.add(riBoutonDetail, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setPreferredSize(new Dimension(100, 30));
          lbMagasin.setMinimumSize(new Dimension(100, 30));
          lbMagasin.setName("lbMagasin");
          pnlInformations.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snMagasin ----
          snMagasin.setName("snMagasin");
          pnlInformations.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbMontantTTC ----
          lbMontantTTC.setText("Montant TTC");
          lbMontantTTC.setPreferredSize(new Dimension(130, 30));
          lbMontantTTC.setMinimumSize(new Dimension(130, 30));
          lbMontantTTC.setName("lbMontantTTC");
          pnlInformations.add(lbMontantTTC, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- E1TTC ----
          E1TTC.setText("@E1TTC@");
          E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
          E1TTC.setFont(E1TTC.getFont().deriveFont(E1TTC.getFont().getSize() + 2f));
          E1TTC.setPreferredSize(new Dimension(100, 30));
          E1TTC.setMinimumSize(new Dimension(100, 30));
          E1TTC.setName("E1TTC");
          pnlInformations.add(E1TTC, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbVendeur ----
          lbVendeur.setText("Vendeur");
          lbVendeur.setPreferredSize(new Dimension(100, 30));
          lbVendeur.setMinimumSize(new Dimension(100, 30));
          lbVendeur.setName("lbVendeur");
          pnlInformations.add(lbVendeur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snVendeur ----
          snVendeur.setName("snVendeur");
          pnlInformations.add(snVendeur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbDateHeure ----
          lbDateHeure.setText("Date et heure");
          lbDateHeure.setPreferredSize(new Dimension(130, 30));
          lbDateHeure.setMinimumSize(new Dimension(130, 30));
          lbDateHeure.setName("lbDateHeure");
          pnlInformations.add(lbDateHeure, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlDateHeure ========
          {
            pnlDateHeure.setMinimumSize(new Dimension(290, 30));
            pnlDateHeure.setPreferredSize(new Dimension(310, 30));
            pnlDateHeure.setName("pnlDateHeure");
            pnlDateHeure.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDateHeure.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDateHeure.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlDateHeure.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlDateHeure.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- HDCREX ----
            HDCREX.setPreferredSize(new Dimension(115, 30));
            HDCREX.setFont(HDCREX.getFont().deriveFont(HDCREX.getFont().getSize() + 2f));
            HDCREX.setMinimumSize(new Dimension(150, 30));
            HDCREX.setName("HDCREX");
            pnlDateHeure.add(HDCREX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbHeure ----
            lbHeure.setText("\u00e0 XXh XXmn XXs");
            lbHeure.setFont(lbHeure.getFont().deriveFont(lbHeure.getFont().getSize() + 2f));
            lbHeure.setName("lbHeure");
            pnlDateHeure.add(lbHeure, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInformations.add(pnlDateHeure, new GridBagConstraints(3, 2, 5, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbUtilisateur ----
          lbUtilisateur.setText("Utilisateur");
          lbUtilisateur.setPreferredSize(new Dimension(100, 30));
          lbUtilisateur.setMinimumSize(new Dimension(100, 30));
          lbUtilisateur.setName("lbUtilisateur");
          pnlInformations.add(lbUtilisateur, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- HDUSR ----
          HDUSR.setText("@HDUSR@");
          HDUSR.setFont(HDUSR.getFont().deriveFont(HDUSR.getFont().getSize() + 2f));
          HDUSR.setPreferredSize(new Dimension(150, 30));
          HDUSR.setMinimumSize(new Dimension(100, 30));
          HDUSR.setName("HDUSR");
          pnlInformations.add(HDUSR, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbDeblocage ----
          lbDeblocage.setText("D\u00e9blocage");
          lbDeblocage.setPreferredSize(new Dimension(100, 30));
          lbDeblocage.setMinimumSize(new Dimension(100, 30));
          lbDeblocage.setName("lbDeblocage");
          pnlInformations.add(lbDeblocage, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlChoixReponse ========
          {
            pnlChoixReponse.setName("pnlChoixReponse");
            pnlChoixReponse.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlChoixReponse.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlChoixReponse.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlChoixReponse.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlChoixReponse.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- HDAUT ----
            HDAUT.setText("Oui");
            HDAUT.setFont(HDAUT.getFont().deriveFont(HDAUT.getFont().getSize() + 2f));
            HDAUT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            HDAUT.setName("HDAUT");
            pnlChoixReponse.add(HDAUT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- HDAUT_N ----
            HDAUT_N.setText("Non");
            HDAUT_N.setFont(HDAUT_N.getFont().deriveFont(HDAUT_N.getFont().getSize() + 2f));
            HDAUT_N.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            HDAUT_N.setName("HDAUT_N");
            pnlChoixReponse.add(HDAUT_N, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInformations.add(pnlChoixReponse, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbResponsable ----
          lbResponsable.setText("par le responsable");
          lbResponsable.setPreferredSize(new Dimension(130, 30));
          lbResponsable.setMinimumSize(new Dimension(130, 30));
          lbResponsable.setName("lbResponsable");
          pnlInformations.add(lbResponsable, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- HDRES ----
          HDRES.setText("@HDRES@");
          HDRES.setFont(HDRES.getFont().deriveFont(HDRES.getFont().getSize() + 2f));
          HDRES.setMinimumSize(new Dimension(100, 30));
          HDRES.setPreferredSize(new Dimension(100, 30));
          HDRES.setName("HDRES");
          pnlInformations.add(HDRES, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbMotif ----
          lbMotif.setText("Motif");
          lbMotif.setPreferredSize(new Dimension(100, 30));
          lbMotif.setMinimumSize(new Dimension(100, 30));
          lbMotif.setName("lbMotif");
          pnlInformations.add(lbMotif, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- HDMOT ----
          HDMOT.setFont(HDMOT.getFont().deriveFont(HDMOT.getFont().getSize() + 2f));
          HDMOT.setName("HDMOT");
          pnlInformations.add(HDMOT, new GridBagConstraints(1, 5, 7, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlInformations, BorderLayout.CENTER);
      }
      pnlPrincipall.add(pnlContenu, BorderLayout.CENTER);

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipall.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipall, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    buttonGroup1.add(HDAUT);
    buttonGroup1.add(HDAUT_N);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipall;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlInformations;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbNumeroDocument;
  private RiZoneSortie E1NUM;
  private RiZoneSortie E1SUF;
  private SNBoutonDetail riBoutonDetail;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbMontantTTC;
  private RiZoneSortie E1TTC;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbDateHeure;
  private SNPanel pnlDateHeure;
  private XRiCalendrier HDCREX;
  private JLabel lbHeure;
  private SNLabelChamp lbUtilisateur;
  private RiZoneSortie HDUSR;
  private SNLabelChamp lbDeblocage;
  private SNPanel pnlChoixReponse;
  private XRiRadioButton HDAUT;
  private XRiRadioButton HDAUT_N;
  private SNLabelChamp lbResponsable;
  private RiZoneSortie HDRES;
  private SNLabelChamp lbMotif;
  private XRiTextField HDMOT;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
