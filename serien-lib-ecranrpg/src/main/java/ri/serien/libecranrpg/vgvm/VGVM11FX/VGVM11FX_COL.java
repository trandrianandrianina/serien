
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_COL extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_COL(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    initDiverses();
    
    // Titre
    setTitle("Colis");
    
    
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    setModal(true);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WCOLC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOLC@")).trim());
    WPDSC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPDSC@")).trim());
    WVOLC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WVOLC@")).trim());
    WPALC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPALC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    WCOLC = new RiZoneSortie();
    WPDSC = new RiZoneSortie();
    WVOLC = new RiZoneSortie();
    marge = new JLabel();
    label4 = new JLabel();
    WPALC = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(500, 200));
    setPreferredSize(new Dimension(500, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Colis, poids et volume"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- label1 ----
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setText("Nombre de colis");
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(25, 32, 115, 20);

          //---- label2 ----
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setText("Poids");
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(25, 65, 115, 20);

          //---- label3 ----
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setText("Volume");
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(25, 98, 115, 20);

          //---- WCOLC ----
          WCOLC.setHorizontalAlignment(SwingConstants.RIGHT);
          WCOLC.setText("@WCOLC@");
          WCOLC.setName("WCOLC");
          panel3.add(WCOLC);
          WCOLC.setBounds(205, 30, 44, WCOLC.getPreferredSize().height);

          //---- WPDSC ----
          WPDSC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPDSC.setText("@WPDSC@");
          WPDSC.setName("WPDSC");
          panel3.add(WPDSC);
          WPDSC.setBounds(205, 63, 68, WPDSC.getPreferredSize().height);

          //---- WVOLC ----
          WVOLC.setHorizontalAlignment(SwingConstants.RIGHT);
          WVOLC.setText("@WVOLC@");
          WVOLC.setName("WVOLC");
          panel3.add(WVOLC);
          WVOLC.setBounds(205, 96, 68, WVOLC.getPreferredSize().height);

          //---- marge ----
          marge.setName("marge");
          panel3.add(marge);
          marge.setBounds(165, 27, 30, 30);

          //---- label4 ----
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setText("Nombre de palettes th\u00e9orique");
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(25, 131, 180, 20);

          //---- WPALC ----
          WPALC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPALC.setText("@WPALC@");
          WPALC.setName("WPALC");
          panel3.add(WPALC);
          WPALC.setBounds(205, 129, 68, WPALC.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private RiZoneSortie WCOLC;
  private RiZoneSortie WPDSC;
  private RiZoneSortie WVOLC;
  private JLabel marge;
  private JLabel label4;
  private RiZoneSortie WPALC;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
