
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_CH extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] E1TRC_Value = { "", "1", "N", "G" };
  private RiPanelNav riPanelNav1 = null;
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  
  public ODialog dialog_REGL = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_CH(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    p_encours.setRightDecoration(bt_encours);
    p_reglement.setRightDecoration(bt_reglement);
    p_condition.setRightDecoration(bt_condition);
    
    E2NOM.activerModeFantome("Nom du chantier");
    E2CPL.activerModeFantome("Complément de nom");
    E2RUE.activerModeFantome("Rue");
    E2LOC.activerModeFantome("Localité");
    E2CDPX.activerModeFantome("00000");
    E2VILN.activerModeFantome("Ville");
    E2PAYN.activerModeFantome("Pays");
    E2TEL.activerModeFantome("Téléphone");
    
    initDiverses();
    E1TRC.setValeurs(E1TRC_Value, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/navigation.png", true));
    
    // Navigation graphique du menu
    riPanelNav1 = new RiPanelNav();
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de devis", true, null);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du devis", false, bouton_valider);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de devis", false, button1);
    riSousMenu1.add(riPanelNav1);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    RPLI1G.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLI1G@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WVDNOM@")).trim());
    WOBS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WOBS@")).trim());
    E1CLFP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLP@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRECIV@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WREPAC@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
    WEPLF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF@")).trim());
    WEPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATN@")).trim());
    OBJ_192.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE1@")).trim());
    OBJ_193.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE2@")).trim());
    OBJ_194.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE3@")).trim());
    OBJ_195.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE4@")).trim());
    OBJ_196.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE5@")).trim());
    E1CAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CAN@")).trim());
    E1VEH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1VEH@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DPLIB@")).trim());
    LRG12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG12@")).trim());
    OBJ_123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRG1@")).trim());
    lib_Devise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    OBJ_227.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
    OBJ_225.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIBR@")).trim());
    WMFRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMFRP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    WSUF.setVisible(lexique.isPresent("WSUF"));
    E1TRP.setEnabled(lexique.isPresent("E1TRP"));
    OBJ_196.setVisible(lexique.isPresent("TIZPE5"));
    OBJ_195.setVisible(lexique.isPresent("TIZPE4"));
    OBJ_194.setVisible(lexique.isPresent("TIZPE3"));
    OBJ_193.setVisible(lexique.isPresent("TIZPE2"));
    OBJ_192.setVisible(lexique.isPresent("TIZPE1"));
    E1CTR.setText(lexique.HostFieldGetData("E1CTR"));
    E1ETB.setVisible(lexique.isPresent("E1ETB"));
    E1CAN.setVisible(lexique.isPresent("E1CAN"));
    OBJ_162.setVisible(lexique.isPresent("WEPOS"));
    WCAT.setVisible(lexique.HostFieldGetData("CENT").equalsIgnoreCase("") & lexique.isPresent("WCAT"));
    OBJ_276.setVisible(lexique.isPresent("WCNP"));
    OBJ_212.setVisible(lexique.isPresent("E1ACT"));
    OBJ_210.setVisible(lexique.isPresent("E1SAN"));
    WEDEP.setVisible(lexique.isPresent("WEDEP"));
    WEPLF.setVisible(lexique.isPresent("WEPLF"));
    WEPOS.setVisible(lexique.isPresent("WEPOS"));
    if (lexique.isTrue("79")) {
      WEPOS.setForeground(Color.RED);
    }
    WCNP.setVisible(lexique.isPresent("WCNP"));
    E1CNV.setEnabled(lexique.isPresent("E1CNV"));
    OBJ_69.setVisible(lexique.HostFieldGetData("CENT").equalsIgnoreCase(""));
    E1VEH.setVisible(lexique.isTrue("N09"));
    OBJ_272.setVisible(E1VEH.isVisible());
    riZoneSortie1.setVisible(E1VEH.isVisible());
    OBJ_157.setVisible(lexique.HostFieldGetData("WF4ENC").equalsIgnoreCase("I"));
    OBJ_178.setVisible(lexique.isPresent("WEDEP"));
    E1NCC.setEnabled(lexique.isPresent("E1NCC"));
    label4.setVisible(true);
    
    OBJ_145.setVisible(lexique.isPresent("E1CCT"));
    E1CCT.setVisible(lexique.isPresent("E1CCT"));
    OBJ_123.setVisible(lexique.isPresent("LIBRG1"));
    E2TEL.setEnabled(lexique.isPresent("E2TEL"));
    OBJ_60.setVisible(!lexique.HostFieldGetData("WATN").trim().equals(""));
    label19.setVisible(OBJ_60.isVisible());
    WOBS.setEnabled(lexique.isPresent("WOBS"));
    
    OBJ_163.setVisible(WEPLF.isVisible());
    OBJ_271.setVisible(E1ZTR.isVisible());
    
    // Panneau impayé
    if (lexique.HostFieldGetData("WF4ENC").equalsIgnoreCase("I")) {
      p_impaye.setVisible(true);
      layeredPane1.setComponentZOrder(p_impaye, 0);
    }
    else {
      p_impaye.setVisible(false);
    }
    
    // Panel des réglements
    lib_Devise.setVisible(lexique.isTrue("N86"));
    E1CHGX.setVisible(lib_Devise.isVisible());
    E1BAS.setVisible(lib_Devise.isVisible());
    label16.setVisible(lib_Devise.isVisible());
    label17.setVisible(lib_Devise.isVisible());
    OBJ_123.setVisible(lib_Devise.isVisible());
    
    if (OBJ_178.isVisible()) {
      OBJ_178.setForeground(Color.RED);
    }
    else {
      OBJ_178.setForeground(Color.BLACK);
    }
    
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 22);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1REP");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_encoursActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 80);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_reglementActionPerformed(ActionEvent e) {
    if (dialog_REGL == null) {
      dialog_REGL = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_REGL(this));
    }
    dialog_REGL.affichePopupPerso();
  }
  
  private void bt_conditionActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 42);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 10);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WNUM = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    WSUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel5 = new JPanel();
    E1RCC = new XRiTextField();
    label3 = new JLabel();
    RPLI1G = new RiZoneSortie();
    E1REP = new XRiTextField();
    OBJ_52 = new JLabel();
    E1VDE = new XRiTextField();
    label6 = new JLabel();
    E1MAG = new XRiTextField();
    label7 = new RiZoneSortie();
    label15 = new JLabel();
    riZoneSortie4 = new RiZoneSortie();
    WOBS = new RiZoneSortie();
    label5 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    E1DLSX = new XRiCalendrier();
    E1DLPX = new XRiCalendrier();
    E1DT2X = new XRiCalendrier();
    label8 = new JLabel();
    label9 = new JLabel();
    OBJ_164 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    panel2 = new JPanel();
    E2NOM = new XRiTextField();
    E2CPL = new XRiTextField();
    E2RUE = new XRiTextField();
    E2LOC = new XRiTextField();
    E2VILN = new XRiTextField();
    E2PAYN = new XRiTextField();
    E2TEL = new XRiTextField();
    E1CLFP = new RiZoneSortie();
    E2CDPX = new XRiTextField();
    E2COP = new XRiTextField();
    riZoneSortie2 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    label18 = new JLabel();
    p_encours = new JXTitledPanel();
    layeredPane1 = new JLayeredPane();
    p_paye = new JPanel();
    WEDEP = new RiZoneSortie();
    OBJ_178 = new JLabel();
    OBJ_163 = new JLabel();
    WEPLF = new RiZoneSortie();
    WEPOS = new RiZoneSortie();
    OBJ_162 = new JLabel();
    label19 = new JLabel();
    OBJ_60 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    bt_encours = new SNBoutonDetail();
    bt_reglement = new SNBoutonDetail();
    bt_condition = new SNBoutonDetail();
    panel1 = new JPanel();
    OBJ_210 = new JLabel();
    E1SAN = new XRiTextField();
    E1ACT = new XRiTextField();
    OBJ_212 = new JLabel();
    E20NAT = new XRiTextField();
    OBJ_214 = new JLabel();
    OBJ_145 = new JLabel();
    E1CCT = new XRiTextField();
    E1PRE = new XRiTextField();
    label13 = new JLabel();
    E1PFC = new XRiTextField();
    label11 = new JLabel();
    E1TP1 = new XRiTextField();
    E1TP2 = new XRiTextField();
    E1TP3 = new XRiTextField();
    E1TP4 = new XRiTextField();
    E1TP5 = new XRiTextField();
    OBJ_192 = new JLabel();
    OBJ_193 = new JLabel();
    OBJ_194 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    OBJ_84 = new JLabel();
    E1CAN = new RiZoneSortie();
    E1PDS = new XRiTextField();
    OBJ_190 = new JLabel();
    OBJ_191 = new JLabel();
    E1IN9 = new XRiTextField();
    E1DPR = new XRiTextField();
    label10 = new JLabel();
    E1VEH = new RiZoneSortie();
    E1ZTR = new XRiTextField();
    OBJ_271 = new JLabel();
    OBJ_272 = new JLabel();
    E1TRC = new XRiComboBox();
    riZoneSortie1 = new RiZoneSortie();
    p_reglement = new JXTitledPanel();
    E1RG2 = new XRiTextField();
    OBJ_265 = new JLabel();
    LRG12 = new RiZoneSortie();
    OBJ_123 = new JLabel();
    lib_Devise = new RiZoneSortie();
    E1CHGX = new XRiTextField();
    E1BAS = new XRiTextField();
    label16 = new JLabel();
    label17 = new JLabel();
    p_condition = new JXTitledPanel();
    OBJ_277 = new JLabel();
    E1CNV = new XRiTextField();
    WCNP = new XRiTextField();
    E1REM1 = new XRiTextField();
    E1REM2 = new XRiTextField();
    E1REM3 = new XRiTextField();
    E1ESCX = new XRiTextField();
    OBJ_276 = new JLabel();
    OBJ_274 = new JLabel();
    E1TAR = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    label4 = new JLabel();
    E1TRP = new XRiTextField();
    riBoutonDetail3 = new SNBoutonDetail();
    E1NCC = new XRiTextField();
    OBJ_69 = new JLabel();
    WCAT = new XRiTextField();
    OBJ_157 = new JLabel();
    p_impaye = new JPanel();
    button1 = new JButton();
    p_exped = new JXTitledPanel();
    OBJ_227 = new RiZoneSortie();
    OBJ_225 = new RiZoneSortie();
    WMFRP = new RiZoneSortie();
    OBJ_235 = new JLabel();
    E1CTR = new XRiTextField();
    E1MEX = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(990, 590));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(1100, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setText("@WNUM@");
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");

          //---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");

          //---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro");
          OBJ_49.setName("OBJ_49");

          //---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          WSUF.setName("WSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(55, 55, 55)
                    .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(335, 335, 335))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE))))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(60, 0));
          p_tete_droite.setMinimumSize(new Dimension(60, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Acc\u00e9s compte");
              riSousMenu_bt6.setToolTipText("Acc\u00e9s compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique");
              riSousMenu_bt7.setToolTipText("Historique");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Options clients");
              riSousMenu_bt8.setToolTipText("Options clients");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes du chantier");
              riSousMenu_bt14.setToolTipText("Bloc-notes du chantier");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("Navigation");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setMinimumSize(new Dimension(104, 260));
              riSousMenu1.setMaximumSize(new Dimension(104, 260));
              riSousMenu1.setPreferredSize(new Dimension(170, 260));
              riSousMenu1.setName("riSousMenu1");
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(830, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(800, 610));
          p_contenu.setName("p_contenu");

          //======== panel5 ========
          {
            panel5.setBackground(new Color(239, 239, 222));
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- E1RCC ----
            E1RCC.setComponentPopupMenu(BTD);
            E1RCC.setName("E1RCC");
            panel5.add(E1RCC);
            E1RCC.setBounds(90, 5, 306, E1RCC.getPreferredSize().height);

            //---- label3 ----
            label3.setText("Repr\u00e9sentant");
            label3.setName("label3");
            panel5.add(label3);
            label3.setBounds(10, 97, 85, 20);

            //---- RPLI1G ----
            RPLI1G.setText("@RPLI1G@");
            RPLI1G.setName("RPLI1G");
            panel5.add(RPLI1G);
            RPLI1G.setBounds(130, 95, 265, RPLI1G.getPreferredSize().height);

            //---- E1REP ----
            E1REP.setComponentPopupMenu(BTD);
            E1REP.setName("E1REP");
            panel5.add(E1REP);
            E1REP.setBounds(90, 93, 34, E1REP.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("Vendeur");
            OBJ_52.setHorizontalAlignment(SwingConstants.LEFT);
            OBJ_52.setName("OBJ_52");
            panel5.add(OBJ_52);
            OBJ_52.setBounds(10, 68, 60, 18);

            //---- E1VDE ----
            E1VDE.setComponentPopupMenu(BTD);
            E1VDE.setName("E1VDE");
            panel5.add(E1VDE);
            E1VDE.setBounds(90, 63, 40, E1VDE.getPreferredSize().height);

            //---- label6 ----
            label6.setText("Magasin");
            label6.setName("label6");
            panel5.add(label6);
            label6.setBounds(10, 37, 80, 20);

            //---- E1MAG ----
            E1MAG.setName("E1MAG");
            panel5.add(E1MAG);
            E1MAG.setBounds(90, 33, 34, E1MAG.getPreferredSize().height);

            //---- label7 ----
            label7.setText("@MALIBR@");
            label7.setName("label7");
            panel5.add(label7);
            label7.setBounds(130, 35, 265, label7.getPreferredSize().height);

            //---- label15 ----
            label15.setText("R\u00e9f\u00e9rence");
            label15.setName("label15");
            panel5.add(label15);
            label15.setBounds(10, 7, 100, 25);

            //---- riZoneSortie4 ----
            riZoneSortie4.setText("@WVDNOM@");
            riZoneSortie4.setName("riZoneSortie4");
            panel5.add(riZoneSortie4);
            riZoneSortie4.setBounds(135, 65, 260, riZoneSortie4.getPreferredSize().height);

            //---- WOBS ----
            WOBS.setComponentPopupMenu(BTD);
            WOBS.setText("@WOBS@");
            WOBS.setName("WOBS");
            panel5.add(WOBS);
            WOBS.setBounds(90, 125, 305, WOBS.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Observation");
            label5.setName("label5");
            panel5.add(label5);
            label5.setBounds(10, 127, 85, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Dates");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- E1DLSX ----
            E1DLSX.setComponentPopupMenu(BTD);
            E1DLSX.setName("E1DLSX");
            xTitledPanel4ContentContainer.add(E1DLSX);
            E1DLSX.setBounds(165, 35, 105, E1DLSX.getPreferredSize().height);

            //---- E1DLPX ----
            E1DLPX.setComponentPopupMenu(BTD);
            E1DLPX.setName("E1DLPX");
            xTitledPanel4ContentContainer.add(E1DLPX);
            E1DLPX.setBounds(165, 60, 105, E1DLPX.getPreferredSize().height);

            //---- E1DT2X ----
            E1DT2X.setComponentPopupMenu(BTD);
            E1DT2X.setName("E1DT2X");
            xTitledPanel4ContentContainer.add(E1DT2X);
            E1DT2X.setBounds(165, 10, 105, E1DT2X.getPreferredSize().height);

            //---- label8 ----
            label8.setText("Livraison souhait\u00e9e");
            label8.setName("label8");
            xTitledPanel4ContentContainer.add(label8);
            label8.setBounds(15, 38, 130, 22);

            //---- label9 ----
            label9.setText("Livraison pr\u00e9vue");
            label9.setName("label9");
            xTitledPanel4ContentContainer.add(label9);
            label9.setBounds(15, 63, 115, 22);

            //---- OBJ_164 ----
            OBJ_164.setText("Validit\u00e9 du chantier");
            OBJ_164.setName("OBJ_164");
            xTitledPanel4ContentContainer.add(OBJ_164);
            OBJ_164.setBounds(15, 14, 125, 20);
          }

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Chantier");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- E2NOM ----
              E2NOM.setComponentPopupMenu(BTD);
              E2NOM.setFont(E2NOM.getFont().deriveFont(E2NOM.getFont().getStyle() | Font.BOLD));
              E2NOM.setName("E2NOM");
              panel2.add(E2NOM);
              E2NOM.setBounds(10, 30, 310, 28);

              //---- E2CPL ----
              E2CPL.setComponentPopupMenu(BTD);
              E2CPL.setName("E2CPL");
              panel2.add(E2CPL);
              E2CPL.setBounds(10, 55, 310, 28);

              //---- E2RUE ----
              E2RUE.setComponentPopupMenu(BTD);
              E2RUE.setName("E2RUE");
              panel2.add(E2RUE);
              E2RUE.setBounds(10, 80, 310, 28);

              //---- E2LOC ----
              E2LOC.setComponentPopupMenu(BTD);
              E2LOC.setName("E2LOC");
              panel2.add(E2LOC);
              E2LOC.setBounds(10, 105, 310, 28);

              //---- E2VILN ----
              E2VILN.setComponentPopupMenu(BTD);
              E2VILN.setName("E2VILN");
              panel2.add(E2VILN);
              E2VILN.setBounds(70, 130, 250, 28);

              //---- E2PAYN ----
              E2PAYN.setComponentPopupMenu(BTD);
              E2PAYN.setName("E2PAYN");
              panel2.add(E2PAYN);
              E2PAYN.setBounds(10, 155, 270, 28);

              //---- E2TEL ----
              E2TEL.setComponentPopupMenu(BTD);
              E2TEL.setName("E2TEL");
              panel2.add(E2TEL);
              E2TEL.setBounds(170, 5, 150, 28);

              //---- E1CLFP ----
              E1CLFP.setComponentPopupMenu(BTD);
              E1CLFP.setText("@E1CLLP@");
              E1CLFP.setHorizontalAlignment(SwingConstants.RIGHT);
              E1CLFP.setName("E1CLFP");
              panel2.add(E1CLFP);
              E1CLFP.setBounds(10, 7, 70, E1CLFP.getPreferredSize().height);

              //---- E2CDPX ----
              E2CDPX.setComponentPopupMenu(BTD);
              E2CDPX.setToolTipText("Code postal");
              E2CDPX.setName("E2CDPX");
              panel2.add(E2CDPX);
              E2CDPX.setBounds(10, 130, 60, 28);

              //---- E2COP ----
              E2COP.setComponentPopupMenu(BTD);
              E2COP.setName("E2COP");
              panel2.add(E2COP);
              E2COP.setBounds(280, 155, 40, 28);

              //---- riZoneSortie2 ----
              riZoneSortie2.setText("@WRECIV@");
              riZoneSortie2.setToolTipText("Civilit\u00e9 contact");
              riZoneSortie2.setName("riZoneSortie2");
              panel2.add(riZoneSortie2);
              riZoneSortie2.setBounds(11, 182, 40, riZoneSortie2.getPreferredSize().height);

              //---- riZoneSortie3 ----
              riZoneSortie3.setText("@WREPAC@");
              riZoneSortie3.setToolTipText("Contact");
              riZoneSortie3.setName("riZoneSortie3");
              panel2.add(riZoneSortie3);
              riZoneSortie3.setBounds(54, 182, 265, riZoneSortie3.getPreferredSize().height);

              //---- label18 ----
              label18.setText("T\u00e9l");
              label18.setName("label18");
              panel2.add(label18);
              label18.setBounds(140, 7, 30, 25);
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(5, 5, 330, 220);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== p_encours ========
          {
            p_encours.setTitle("Encours");
            p_encours.setBorder(new DropShadowBorder());
            p_encours.setName("p_encours");
            Container p_encoursContentContainer = p_encours.getContentContainer();
            p_encoursContentContainer.setLayout(null);

            //======== layeredPane1 ========
            {
              layeredPane1.setName("layeredPane1");

              //======== p_paye ========
              {
                p_paye.setOpaque(false);
                p_paye.setName("p_paye");
                p_paye.setLayout(null);

                //---- WEDEP ----
                WEDEP.setComponentPopupMenu(BTD);
                WEDEP.setText("@WEDEP@");
                WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
                WEDEP.setName("WEDEP");
                p_paye.add(WEDEP);
                WEDEP.setBounds(110, 75, 60, WEDEP.getPreferredSize().height);

                //---- OBJ_178 ----
                OBJ_178.setText("D\u00e9passement");
                OBJ_178.setFont(OBJ_178.getFont().deriveFont(OBJ_178.getFont().getStyle() | Font.BOLD));
                OBJ_178.setForeground(new Color(215, 17, 17));
                OBJ_178.setName("OBJ_178");
                p_paye.add(OBJ_178);
                OBJ_178.setBounds(10, 78, 110, 18);

                //---- OBJ_163 ----
                OBJ_163.setText("Plafond");
                OBJ_163.setName("OBJ_163");
                p_paye.add(OBJ_163);
                OBJ_163.setBounds(15, 48, 55, 18);

                //---- WEPLF ----
                WEPLF.setComponentPopupMenu(BTD);
                WEPLF.setText("@WEPLF@");
                WEPLF.setHorizontalAlignment(SwingConstants.RIGHT);
                WEPLF.setName("WEPLF");
                p_paye.add(WEPLF);
                WEPLF.setBounds(100, 45, 70, WEPLF.getPreferredSize().height);

                //---- WEPOS ----
                WEPOS.setComponentPopupMenu(BTD);
                WEPOS.setText("@WEPOS@");
                WEPOS.setHorizontalAlignment(SwingConstants.RIGHT);
                WEPOS.setName("WEPOS");
                p_paye.add(WEPOS);
                WEPOS.setBounds(100, 15, 70, WEPOS.getPreferredSize().height);

                //---- OBJ_162 ----
                OBJ_162.setText("Position");
                OBJ_162.setName("OBJ_162");
                p_paye.add(OBJ_162);
                OBJ_162.setBounds(15, 16, 60, 23);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < p_paye.getComponentCount(); i++) {
                    Rectangle bounds = p_paye.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = p_paye.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  p_paye.setMinimumSize(preferredSize);
                  p_paye.setPreferredSize(preferredSize);
                }
              }
              layeredPane1.add(p_paye, JLayeredPane.DEFAULT_LAYER);
              p_paye.setBounds(0, -10, 200, 110);
            }
            p_encoursContentContainer.add(layeredPane1);
            layeredPane1.setBounds(0, 0, 205, 105);
          }

          //---- label19 ----
          label19.setText("Attention");
          label19.setName("label19");

          //---- OBJ_60 ----
          OBJ_60.setText("@WATN@");
          OBJ_60.setForeground(new Color(255, 51, 51));
          OBJ_60.setName("OBJ_60");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(p_encours, GroupLayout.PREFERRED_SIZE, 345, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 410, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(504, 504, 504)
                .addComponent(label19, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(30, 30, 30)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(p_encours, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(label19, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_60, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_12 ----
      OBJ_12.setText("M\u00e9morisation curseur");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }

    //---- bt_encours ----
    bt_encours.setToolTipText("Encours");
    bt_encours.setName("bt_encours");
    bt_encours.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_encoursActionPerformed(e);
      }
    });

    //---- bt_reglement ----
    bt_reglement.setToolTipText("R\u00e9glements");
    bt_reglement.setName("bt_reglement");
    bt_reglement.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_reglementActionPerformed(e);
      }
    });

    //---- bt_condition ----
    bt_condition.setToolTipText("Conditions");
    bt_condition.setName("bt_condition");
    bt_condition.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_conditionActionPerformed(e);
      }
    });

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_210 ----
      OBJ_210.setText("Section");
      OBJ_210.setName("OBJ_210");
      panel1.add(OBJ_210);
      OBJ_210.setBounds(20, 20, 60, 20);

      //---- E1SAN ----
      E1SAN.setComponentPopupMenu(BTD);
      E1SAN.setName("E1SAN");
      panel1.add(E1SAN);
      E1SAN.setBounds(90, 15, 50, E1SAN.getPreferredSize().height);

      //---- E1ACT ----
      E1ACT.setComponentPopupMenu(BTD);
      E1ACT.setName("E1ACT");
      panel1.add(E1ACT);
      E1ACT.setBounds(90, 40, 50, E1ACT.getPreferredSize().height);

      //---- OBJ_212 ----
      OBJ_212.setText("Affaire");
      OBJ_212.setName("OBJ_212");
      panel1.add(OBJ_212);
      OBJ_212.setBounds(20, 45, 60, 20);

      //---- E20NAT ----
      E20NAT.setComponentPopupMenu(BTD);
      E20NAT.setName("E20NAT");
      panel1.add(E20NAT);
      E20NAT.setBounds(80, 65, 60, E20NAT.getPreferredSize().height);

      //---- OBJ_214 ----
      OBJ_214.setText("Nature");
      OBJ_214.setName("OBJ_214");
      panel1.add(OBJ_214);
      OBJ_214.setBounds(20, 70, 60, 20);

      //---- OBJ_145 ----
      OBJ_145.setText("Commande initiale");
      OBJ_145.setName("OBJ_145");
      panel1.add(OBJ_145);
      OBJ_145.setBounds(150, 15, 114, 24);

      //---- E1CCT ----
      E1CCT.setComponentPopupMenu(BTD);
      E1CCT.setName("E1CCT");
      panel1.add(E1CCT);
      E1CCT.setBounds(265, 15, 110, E1CCT.getPreferredSize().height);

      //---- E1PRE ----
      E1PRE.setComponentPopupMenu(BTD);
      E1PRE.setName("E1PRE");
      panel1.add(E1PRE);
      E1PRE.setBounds(560, 40, 40, E1PRE.getPreferredSize().height);

      //---- label13 ----
      label13.setText("Niveau espoir r\u00e9alisation");
      label13.setHorizontalAlignment(SwingConstants.RIGHT);
      label13.setName("label13");
      panel1.add(label13);
      label13.setBounds(390, 45, 160, 21);

      //---- E1PFC ----
      E1PFC.setComponentPopupMenu(BTD);
      E1PFC.setName("E1PFC");
      panel1.add(E1PFC);
      E1PFC.setBounds(560, 15, 40, E1PFC.getPreferredSize().height);

      //---- label11 ----
      label11.setText("Type de devis");
      label11.setHorizontalAlignment(SwingConstants.RIGHT);
      label11.setName("label11");
      panel1.add(label11);
      label11.setBounds(470, 20, 80, 20);

      //---- E1TP1 ----
      E1TP1.setComponentPopupMenu(BTD);
      E1TP1.setName("E1TP1");
      panel1.add(E1TP1);
      E1TP1.setBounds(115, 125, 30, E1TP1.getPreferredSize().height);

      //---- E1TP2 ----
      E1TP2.setComponentPopupMenu(BTD);
      E1TP2.setName("E1TP2");
      panel1.add(E1TP2);
      E1TP2.setBounds(145, 125, 30, E1TP2.getPreferredSize().height);

      //---- E1TP3 ----
      E1TP3.setComponentPopupMenu(BTD);
      E1TP3.setName("E1TP3");
      panel1.add(E1TP3);
      E1TP3.setBounds(175, 125, 30, E1TP3.getPreferredSize().height);

      //---- E1TP4 ----
      E1TP4.setComponentPopupMenu(BTD);
      E1TP4.setName("E1TP4");
      panel1.add(E1TP4);
      E1TP4.setBounds(210, 125, 30, E1TP4.getPreferredSize().height);

      //---- E1TP5 ----
      E1TP5.setComponentPopupMenu(BTD);
      E1TP5.setName("E1TP5");
      panel1.add(E1TP5);
      E1TP5.setBounds(240, 125, 30, E1TP5.getPreferredSize().height);

      //---- OBJ_192 ----
      OBJ_192.setText("@TIZPE1@");
      OBJ_192.setName("OBJ_192");
      panel1.add(OBJ_192);
      OBJ_192.setBounds(120, 105, 25, 20);

      //---- OBJ_193 ----
      OBJ_193.setText("@TIZPE2@");
      OBJ_193.setName("OBJ_193");
      panel1.add(OBJ_193);
      OBJ_193.setBounds(150, 105, 25, 20);

      //---- OBJ_194 ----
      OBJ_194.setText("@TIZPE3@");
      OBJ_194.setName("OBJ_194");
      panel1.add(OBJ_194);
      OBJ_194.setBounds(185, 105, 25, 20);

      //---- OBJ_195 ----
      OBJ_195.setText("@TIZPE4@");
      OBJ_195.setName("OBJ_195");
      panel1.add(OBJ_195);
      OBJ_195.setBounds(215, 105, 25, 20);

      //---- OBJ_196 ----
      OBJ_196.setText("@TIZPE5@");
      OBJ_196.setName("OBJ_196");
      panel1.add(OBJ_196);
      OBJ_196.setBounds(245, 105, 25, 20);

      //---- OBJ_84 ----
      OBJ_84.setText("Canal");
      OBJ_84.setName("OBJ_84");
      panel1.add(OBJ_84);
      OBJ_84.setBounds(10, 125, 43, 20);

      //---- E1CAN ----
      E1CAN.setText("@E1CAN@");
      E1CAN.setName("E1CAN");
      panel1.add(E1CAN);
      E1CAN.setBounds(50, 125, 40, E1CAN.getPreferredSize().height);

      //---- E1PDS ----
      E1PDS.setComponentPopupMenu(BTD);
      E1PDS.setName("E1PDS");
      panel1.add(E1PDS);
      E1PDS.setBounds(310, 225, 74, E1PDS.getPreferredSize().height);

      //---- OBJ_190 ----
      OBJ_190.setText("D\u00e9lai pr\u00e9paration");
      OBJ_190.setName("OBJ_190");
      panel1.add(OBJ_190);
      OBJ_190.setBounds(220, 180, 135, 16);

      //---- OBJ_191 ----
      OBJ_191.setText("Livraison partielle");
      OBJ_191.setName("OBJ_191");
      panel1.add(OBJ_191);
      OBJ_191.setBounds(220, 205, 135, 16);

      //---- E1IN9 ----
      E1IN9.setComponentPopupMenu(BTD);
      E1IN9.setName("E1IN9");
      panel1.add(E1IN9);
      E1IN9.setBounds(365, 200, 20, E1IN9.getPreferredSize().height);

      //---- E1DPR ----
      E1DPR.setComponentPopupMenu(BTD);
      E1DPR.setName("E1DPR");
      panel1.add(E1DPR);
      E1DPR.setBounds(360, 175, 26, E1DPR.getPreferredSize().height);

      //---- label10 ----
      label10.setText("Poids");
      label10.setName("label10");
      panel1.add(label10);
      label10.setBounds(220, 225, 90, 20);

      //---- E1VEH ----
      E1VEH.setComponentPopupMenu(BTD);
      E1VEH.setText("@E1VEH@");
      E1VEH.setName("E1VEH");
      panel1.add(E1VEH);
      E1VEH.setBounds(115, 280, 60, E1VEH.getPreferredSize().height);

      //---- E1ZTR ----
      E1ZTR.setComponentPopupMenu(BTD);
      E1ZTR.setName("E1ZTR");
      panel1.add(E1ZTR);
      E1ZTR.setBounds(115, 305, 60, E1ZTR.getPreferredSize().height);

      //---- OBJ_271 ----
      OBJ_271.setText("Zone");
      OBJ_271.setName("OBJ_271");
      panel1.add(OBJ_271);
      OBJ_271.setBounds(30, 310, 85, 20);

      //---- OBJ_272 ----
      OBJ_272.setText("Devis perdu");
      OBJ_272.setName("OBJ_272");
      panel1.add(OBJ_272);
      OBJ_272.setBounds(30, 285, 85, 20);

      //---- E1TRC ----
      E1TRC.setModel(new DefaultComboBoxModel(new String[] {
        "Aucun for\u00e7age",
        "For\u00e7age zone transport",
        "Non calcul de port",
        "Franco garanti"
      }));
      E1TRC.setComponentPopupMenu(BTD);
      E1TRC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      E1TRC.setName("E1TRC");
      panel1.add(E1TRC);
      E1TRC.setBounds(180, 305, 170, E1TRC.getPreferredSize().height);

      //---- riZoneSortie1 ----
      riZoneSortie1.setText("@DPLIB@");
      riZoneSortie1.setName("riZoneSortie1");
      panel1.add(riZoneSortie1);
      riZoneSortie1.setBounds(180, 280, 230, riZoneSortie1.getPreferredSize().height);

      //======== p_reglement ========
      {
        p_reglement.setTitle("R\u00e9glements");
        p_reglement.setRightDecoration(null);
        p_reglement.setBorder(new DropShadowBorder());
        p_reglement.setName("p_reglement");
        Container p_reglementContentContainer = p_reglement.getContentContainer();
        p_reglementContentContainer.setLayout(null);

        //---- E1RG2 ----
        E1RG2.setComponentPopupMenu(BTD);
        E1RG2.setName("E1RG2");
        p_reglementContentContainer.add(E1RG2);
        E1RG2.setBounds(15, 10, 34, E1RG2.getPreferredSize().height);

        //---- OBJ_265 ----
        OBJ_265.setText("Ech\u00e9ance");
        OBJ_265.setName("OBJ_265");
        p_reglementContentContainer.add(OBJ_265);
        OBJ_265.setBounds(55, 14, 64, 20);

        //---- LRG12 ----
        LRG12.setText("@LRG12@");
        LRG12.setName("LRG12");
        p_reglementContentContainer.add(LRG12);
        LRG12.setBounds(120, 12, 281, LRG12.getPreferredSize().height);

        //---- OBJ_123 ----
        OBJ_123.setText("@LIBRG1@");
        OBJ_123.setHorizontalTextPosition(SwingConstants.LEADING);
        OBJ_123.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_123.setName("OBJ_123");
        p_reglementContentContainer.add(OBJ_123);
        OBJ_123.setBounds(15, 39, 165, 22);

        //---- lib_Devise ----
        lib_Devise.setText("@DVLIBR@");
        lib_Devise.setBackground(new Color(204, 204, 204));
        lib_Devise.setFont(lib_Devise.getFont().deriveFont(lib_Devise.getFont().getStyle() | Font.BOLD));
        lib_Devise.setForeground(Color.red);
        lib_Devise.setName("lib_Devise");
        p_reglementContentContainer.add(lib_Devise);
        lib_Devise.setBounds(189, 38, 212, lib_Devise.getPreferredSize().height);

        //---- E1CHGX ----
        E1CHGX.setName("E1CHGX");
        p_reglementContentContainer.add(E1CHGX);
        E1CHGX.setBounds(188, 63, 90, 28);

        //---- E1BAS ----
        E1BAS.setName("E1BAS");
        p_reglementContentContainer.add(E1BAS);
        E1BAS.setBounds(349, 63, 52, 28);

        //---- label16 ----
        label16.setText("Change");
        label16.setHorizontalAlignment(SwingConstants.RIGHT);
        label16.setName("label16");
        p_reglementContentContainer.add(label16);
        label16.setBounds(125, 67, 55, 20);

        //---- label17 ----
        label17.setText("Base");
        label17.setName("label17");
        p_reglementContentContainer.add(label17);
        label17.setBounds(310, 67, 40, 20);
      }
      panel1.add(p_reglement);
      p_reglement.setBounds(485, 80, 420, 130);

      //======== p_condition ========
      {
        p_condition.setTitle("Conditions");
        p_condition.setBorder(new DropShadowBorder());
        p_condition.setName("p_condition");
        Container p_conditionContentContainer = p_condition.getContentContainer();
        p_conditionContentContainer.setLayout(null);

        //---- OBJ_277 ----
        OBJ_277.setText("%Esc.");
        OBJ_277.setFont(OBJ_277.getFont().deriveFont(OBJ_277.getFont().getStyle() | Font.BOLD));
        OBJ_277.setName("OBJ_277");
        p_conditionContentContainer.add(OBJ_277);
        OBJ_277.setBounds(322, 15, 40, 17);

        //---- E1CNV ----
        E1CNV.setComponentPopupMenu(BTD);
        E1CNV.setName("E1CNV");
        p_conditionContentContainer.add(E1CNV);
        E1CNV.setBounds(191, 35, 70, E1CNV.getPreferredSize().height);

        //---- WCNP ----
        WCNP.setName("WCNP");
        p_conditionContentContainer.add(WCNP);
        WCNP.setBounds(261, 35, 60, WCNP.getPreferredSize().height);

        //---- E1REM1 ----
        E1REM1.setComponentPopupMenu(BTD);
        E1REM1.setName("E1REM1");
        p_conditionContentContainer.add(E1REM1);
        E1REM1.setBounds(15, 35, 50, E1REM1.getPreferredSize().height);

        //---- E1REM2 ----
        E1REM2.setComponentPopupMenu(BTD);
        E1REM2.setName("E1REM2");
        p_conditionContentContainer.add(E1REM2);
        E1REM2.setBounds(65, 35, 50, E1REM2.getPreferredSize().height);

        //---- E1REM3 ----
        E1REM3.setComponentPopupMenu(BTD);
        E1REM3.setName("E1REM3");
        p_conditionContentContainer.add(E1REM3);
        E1REM3.setBounds(115, 35, 50, E1REM3.getPreferredSize().height);

        //---- E1ESCX ----
        E1ESCX.setComponentPopupMenu(BTD);
        E1ESCX.setHorizontalAlignment(SwingConstants.RIGHT);
        E1ESCX.setName("E1ESCX");
        p_conditionContentContainer.add(E1ESCX);
        E1ESCX.setBounds(321, 35, 42, E1ESCX.getPreferredSize().height);

        //---- OBJ_276 ----
        OBJ_276.setText("Promo");
        OBJ_276.setFont(OBJ_276.getFont().deriveFont(OBJ_276.getFont().getStyle() | Font.BOLD));
        OBJ_276.setName("OBJ_276");
        p_conditionContentContainer.add(OBJ_276);
        OBJ_276.setBounds(270, 15, 43, 17);

        //---- OBJ_274 ----
        OBJ_274.setText("Tarif");
        OBJ_274.setFont(OBJ_274.getFont().deriveFont(OBJ_274.getFont().getStyle() | Font.BOLD));
        OBJ_274.setName("OBJ_274");
        p_conditionContentContainer.add(OBJ_274);
        OBJ_274.setBounds(163, 15, 30, 17);

        //---- E1TAR ----
        E1TAR.setComponentPopupMenu(BTD);
        E1TAR.setName("E1TAR");
        p_conditionContentContainer.add(E1TAR);
        E1TAR.setBounds(165, 35, 26, E1TAR.getPreferredSize().height);

        //---- xTitledSeparator1 ----
        xTitledSeparator1.setTitle("Remises");
        xTitledSeparator1.setName("xTitledSeparator1");
        p_conditionContentContainer.add(xTitledSeparator1);
        xTitledSeparator1.setBounds(15, 15, 145, xTitledSeparator1.getPreferredSize().height);

        //---- label4 ----
        label4.setText("Condition");
        label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
        label4.setName("label4");
        p_conditionContentContainer.add(label4);
        label4.setBounds(new Rectangle(new Point(200, 15), label4.getPreferredSize()));
      }
      panel1.add(p_condition);
      p_condition.setBounds(480, 235, 525, 130);

      //---- E1TRP ----
      E1TRP.setComponentPopupMenu(BTD);
      E1TRP.setName("E1TRP");
      panel1.add(E1TRP);
      E1TRP.setBounds(35, 190, 20, E1TRP.getPreferredSize().height);

      //---- riBoutonDetail3 ----
      riBoutonDetail3.setToolTipText("Repr\u00e9sentants");
      riBoutonDetail3.setName("riBoutonDetail3");
      riBoutonDetail3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riBoutonDetail3ActionPerformed(e);
        }
      });
      panel1.add(riBoutonDetail3);
      riBoutonDetail3.setBounds(new Rectangle(new Point(55, 195), riBoutonDetail3.getPreferredSize()));

      //---- E1NCC ----
      E1NCC.setComponentPopupMenu(BTD);
      E1NCC.setName("E1NCC");
      panel1.add(E1NCC);
      E1NCC.setBounds(10, 240, 100, E1NCC.getPreferredSize().height);

      //---- OBJ_69 ----
      OBJ_69.setText("Cat\u00e9gorie");
      OBJ_69.setName("OBJ_69");
      panel1.add(OBJ_69);
      OBJ_69.setBounds(10, 375, 58, 20);

      //---- WCAT ----
      WCAT.setComponentPopupMenu(BTD);
      WCAT.setName("WCAT");
      panel1.add(WCAT);
      WCAT.setBounds(30, 390, 40, 28);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }

    //---- OBJ_157 ----
    OBJ_157.setForeground(new Color(255, 0, 51));
    OBJ_157.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_157.setFont(OBJ_157.getFont().deriveFont(OBJ_157.getFont().getStyle() | Font.BOLD, OBJ_157.getFont().getSize() + 6f));
    OBJ_157.setName("OBJ_157");

    //======== p_impaye ========
    {
      p_impaye.setOpaque(false);
      p_impaye.setPreferredSize(new Dimension(160, 0));
      p_impaye.setName("p_impaye");
      p_impaye.setLayout(null);

      p_impaye.setPreferredSize(new Dimension(205, 65));
    }

    //---- button1 ----
    button1.setText("Pied de bon : ne pas supprimer");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });

    //======== p_exped ========
    {
      p_exped.setTitle("Exp\u00e9dition");
      p_exped.setBorder(new DropShadowBorder());
      p_exped.setName("p_exped");
      Container p_expedContentContainer = p_exped.getContentContainer();
      p_expedContentContainer.setLayout(null);

      //---- OBJ_227 ----
      OBJ_227.setText("@TRLIB@");
      OBJ_227.setName("OBJ_227");
      p_expedContentContainer.add(OBJ_227);
      OBJ_227.setBounds(135, 32, 255, OBJ_227.getPreferredSize().height);

      //---- OBJ_225 ----
      OBJ_225.setText("@EXLIBR@");
      OBJ_225.setName("OBJ_225");
      p_expedContentContainer.add(OBJ_225);
      OBJ_225.setBounds(135, 6, 255, OBJ_225.getPreferredSize().height);

      //---- WMFRP ----
      WMFRP.setComponentPopupMenu(BTD);
      WMFRP.setText("@WMFRP@");
      WMFRP.setName("WMFRP");
      p_expedContentContainer.add(WMFRP);
      WMFRP.setBounds(95, 60, 65, WMFRP.getPreferredSize().height);

      //---- OBJ_235 ----
      OBJ_235.setText("Franco");
      OBJ_235.setName("OBJ_235");
      p_expedContentContainer.add(OBJ_235);
      OBJ_235.setBounds(10, 62, 90, 20);

      //---- E1CTR ----
      E1CTR.setComponentPopupMenu(BTD);
      E1CTR.setName("E1CTR");
      p_expedContentContainer.add(E1CTR);
      E1CTR.setBounds(95, 30, 34, E1CTR.getPreferredSize().height);

      //---- E1MEX ----
      E1MEX.setComponentPopupMenu(BTD);
      E1MEX.setName("E1MEX");
      p_expedContentContainer.add(E1MEX);
      E1MEX.setBounds(95, 4, 34, E1MEX.getPreferredSize().height);

      //---- label1 ----
      label1.setText("Mode");
      label1.setName("label1");
      p_expedContentContainer.add(label1);
      label1.setBounds(10, 8, 85, 20);

      //---- label2 ----
      label2.setText("Transporteur");
      label2.setName("label2");
      p_expedContentContainer.add(label2);
      label2.setBounds(10, 34, 85, 20);

      p_expedContentContainer.setPreferredSize(new Dimension(410, 130));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WNUM;
  private RiZoneSortie E1ETB;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private RiZoneSortie WSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel5;
  private XRiTextField E1RCC;
  private JLabel label3;
  private RiZoneSortie RPLI1G;
  private XRiTextField E1REP;
  private JLabel OBJ_52;
  private XRiTextField E1VDE;
  private JLabel label6;
  private XRiTextField E1MAG;
  private RiZoneSortie label7;
  private JLabel label15;
  private RiZoneSortie riZoneSortie4;
  private RiZoneSortie WOBS;
  private JLabel label5;
  private JXTitledPanel xTitledPanel4;
  private XRiCalendrier E1DLSX;
  private XRiCalendrier E1DLPX;
  private XRiCalendrier E1DT2X;
  private JLabel label8;
  private JLabel label9;
  private JLabel OBJ_164;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel2;
  private XRiTextField E2NOM;
  private XRiTextField E2CPL;
  private XRiTextField E2RUE;
  private XRiTextField E2LOC;
  private XRiTextField E2VILN;
  private XRiTextField E2PAYN;
  private XRiTextField E2TEL;
  private RiZoneSortie E1CLFP;
  private XRiTextField E2CDPX;
  private XRiTextField E2COP;
  private RiZoneSortie riZoneSortie2;
  private RiZoneSortie riZoneSortie3;
  private JLabel label18;
  private JXTitledPanel p_encours;
  private JLayeredPane layeredPane1;
  private JPanel p_paye;
  private RiZoneSortie WEDEP;
  private JLabel OBJ_178;
  private JLabel OBJ_163;
  private RiZoneSortie WEPLF;
  private RiZoneSortie WEPOS;
  private JLabel OBJ_162;
  private JLabel label19;
  private RiZoneSortie OBJ_60;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_20;
  private SNBoutonDetail bt_encours;
  private SNBoutonDetail bt_reglement;
  private SNBoutonDetail bt_condition;
  private JPanel panel1;
  private JLabel OBJ_210;
  private XRiTextField E1SAN;
  private XRiTextField E1ACT;
  private JLabel OBJ_212;
  private XRiTextField E20NAT;
  private JLabel OBJ_214;
  private JLabel OBJ_145;
  private XRiTextField E1CCT;
  private XRiTextField E1PRE;
  private JLabel label13;
  private XRiTextField E1PFC;
  private JLabel label11;
  private XRiTextField E1TP1;
  private XRiTextField E1TP2;
  private XRiTextField E1TP3;
  private XRiTextField E1TP4;
  private XRiTextField E1TP5;
  private JLabel OBJ_192;
  private JLabel OBJ_193;
  private JLabel OBJ_194;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private JLabel OBJ_84;
  private RiZoneSortie E1CAN;
  private XRiTextField E1PDS;
  private JLabel OBJ_190;
  private JLabel OBJ_191;
  private XRiTextField E1IN9;
  private XRiTextField E1DPR;
  private JLabel label10;
  private RiZoneSortie E1VEH;
  private XRiTextField E1ZTR;
  private JLabel OBJ_271;
  private JLabel OBJ_272;
  private XRiComboBox E1TRC;
  private RiZoneSortie riZoneSortie1;
  private JXTitledPanel p_reglement;
  private XRiTextField E1RG2;
  private JLabel OBJ_265;
  private RiZoneSortie LRG12;
  private JLabel OBJ_123;
  private RiZoneSortie lib_Devise;
  private XRiTextField E1CHGX;
  private XRiTextField E1BAS;
  private JLabel label16;
  private JLabel label17;
  private JXTitledPanel p_condition;
  private JLabel OBJ_277;
  private XRiTextField E1CNV;
  private XRiTextField WCNP;
  private XRiTextField E1REM1;
  private XRiTextField E1REM2;
  private XRiTextField E1REM3;
  private XRiTextField E1ESCX;
  private JLabel OBJ_276;
  private JLabel OBJ_274;
  private XRiTextField E1TAR;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel label4;
  private XRiTextField E1TRP;
  private SNBoutonDetail riBoutonDetail3;
  private XRiTextField E1NCC;
  private JLabel OBJ_69;
  private XRiTextField WCAT;
  private JLabel OBJ_157;
  private JPanel p_impaye;
  private JButton button1;
  private JXTitledPanel p_exped;
  private RiZoneSortie OBJ_227;
  private RiZoneSortie OBJ_225;
  private RiZoneSortie WMFRP;
  private JLabel OBJ_235;
  private XRiTextField E1CTR;
  private XRiTextField E1MEX;
  private JLabel label1;
  private JLabel label2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
