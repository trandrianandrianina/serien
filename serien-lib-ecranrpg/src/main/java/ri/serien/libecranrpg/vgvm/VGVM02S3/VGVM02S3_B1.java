
package ri.serien.libecranrpg.vgvm.VGVM02S3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM02S3_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] PPER_Value = { "E", "S", "T", "M", "J", };
  private String[] PEXE_Value = { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", };
  private String[] PCHX_J_Value = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17",
      "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "99", "90", "00", };
  private String[] PCHX_J_Title = { "Premier jour", "Deuxième jour", "Troisième jour", "Quatrième jour", "Cinquième jour", "Sixième jour",
      "Septième jour", "Huitième jour", "Neuvième jour", "Dixième jour", "Onzième jour", "Douzième jour", "Treizième jour",
      "Quatorzième jour", "Quinzième jour", "Seizième jour", "Dix septième jour", "Dix huitième jour", "Dix neuvième jour",
      "Vingtième jour", "Vingt et unième jour", "Vingt deuxième jour", "Vingt troisième jour", "Vingt quatrième jour",
      "Vingt cinquième jour", "Vingt sixième jour", "Vingt septième jour", "Vingt huitième jour", "Vingt neuvième jour", "Trentième jour",
      "Trente et unième jour", "Veille", "Saisie", "Aujourd'hui" };
  private String[] PCHX_M_Value =
      { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "51", "52", "53", "99", "90", "00", "80", };
  private String[] PCHX_M_Title = { "Premier mois", "Deuxième mois", "Troisième  mois", "Quatrième mois", "Cinquième mois", "Sixième mois",
      "Septième mois", "Huitième mois", "Neuvième mois", "Dixième mois", "Onzième mois", "Douzième mois", "Mois - 1", "Mois - 2",
      "Mois - 3", "Dernier complet", "Saisie", "En cours", "Suivant" };
  private String[] PCHX_E_Value = { "00", "98", "90", "99", };
  private String[] PCHX_E_Title = { "A ce jour", "Fin de mois précédent", "Saisie", "Complet" };
  private String[] PCHX_S_Value = { "01", "02", "99", "90", "00", };
  private String[] PCHX_S_Title = { "Premier semestre", "Deuxiéme semestre", "Dernier complet", "Saisie", "En cours" };
  private String[] PCHX_T_Value = { "01", "02", "03", "04", "99", "90", "00", };
  private String[] PCHX_T_Title =
      { "Premier trimestre", "Deuxième trimestre", "Troisième trimestre", "Quatrième trimestre", "Dernier complet", "Saisie", "En cours" };
  
  public VGVM02S3_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PPER.setValeurs(PPER_Value, null);
    PEXE.setValeurs(PEXE_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    DTDEB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTDEB@")).trim());
    DTFIN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DTFIN@")).trim());
  }
  
  @Override
  public void setData() {
    // Init avant traitement du composant
    String chaine = lexique.HostFieldGetData("PPER").trim();
    if (chaine.equals("M")) {
      PCHX.setValeurs(PCHX_M_Value, PCHX_M_Title);
    }
    else if (chaine.equals("E")) {
      PCHX.setValeurs(PCHX_E_Value, PCHX_E_Title);
    }
    else if (chaine.equals("S")) {
      PCHX.setValeurs(PCHX_S_Value, PCHX_S_Title);
    }
    else if (chaine.equals("T")) {
      PCHX.setValeurs(PCHX_T_Value, PCHX_T_Title);
    }
    else if (chaine.equals("J")) {
      PCHX.setValeurs(PCHX_J_Value, PCHX_J_Title);
    }
    else {
      PCHX.setVisible(false);
    }
    
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // DTFIN.setVisible( lexique.isPresent("DTFIN"));
    // DTDEB.setVisible( lexique.isPresent("DTDEB"));
    // OBJ_43.setVisible( lexique.isPresent("V01F"));
    // OBJ_50.setVisible( lexique.isPresent("WENCX"));
    // S3LIB.setEnabled( lexique.isPresent("S3LIB"));
    
    /*
    PCHX_OBJ_63_22.setSelectedIndex(getIndice("PCHX_OBJ_63_22", PCHX_OBJ_63_22_Value));
    PCHX_OBJ_63_22.setVisible(lexique.HostFieldGetData("PPER").equalsIgnoreCase("M") &  lexique.isPresent("PCHX"));
    PCHX_OBJ_25_21.setSelectedIndex(getIndice("PCHX_OBJ_25_21", PCHX_OBJ_25_21_Value));
    PCHX_OBJ_25_21.setVisible(lexique.HostFieldGetData("PPER").equalsIgnoreCase("E") &  lexique.isPresent("PCHX"));
    PCHX_OBJ_24_20.setSelectedIndex(getIndice("PCHX_OBJ_24_20", PCHX_OBJ_24_20_Value));
    PCHX_OBJ_24_20.setVisible(lexique.HostFieldGetData("PPER").equalsIgnoreCase("S") &  lexique.isPresent("PCHX"));
    PCHX_OBJ_23_19.setSelectedIndex(getIndice("PCHX_OBJ_23_19", PCHX_OBJ_23_19_Value));
    PCHX_OBJ_23_19.setVisible(lexique.HostFieldGetData("PPER").equalsIgnoreCase("T") &  lexique.isPresent("PCHX"));
    */
    // PPER.setSelectedIndex(getIndice("PPER", PPER_Value));
    // PEXE.setSelectedIndex(getIndice("PEXE", PEXE_Value));
    
    // PCHX.setSelectedIndex(getIndice("PCHX", PCHX_Value));
    // PCHX.setVisible(lexique.HostFieldGetData("PPER").equalsIgnoreCase("J") & lexique.isPresent("PCHX"));
    
    // Titre
    // setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("PCHX_OBJ_63_22", 0, PCHX_OBJ_63_22_Value[PCHX_OBJ_63_22.getSelectedIndex()]);
    // lexique.HostFieldPutData("PCHX_OBJ_25_21", 0, PCHX_OBJ_25_21_Value[PCHX_OBJ_25_21.getSelectedIndex()]);
    // lexique.HostFieldPutData("PCHX_OBJ_24_20", 0, PCHX_OBJ_24_20_Value[PCHX_OBJ_24_20.getSelectedIndex()]);
    // lexique.HostFieldPutData("PCHX_OBJ_23_19", 0, PCHX_OBJ_23_19_Value[PCHX_OBJ_23_19.getSelectedIndex()]);
    // lexique.HostFieldPutData("PPER", 0, PPER_Value[PPER.getSelectedIndex()]);
    // lexique.HostFieldPutData("PEXE", 0, PEXE_Value[PEXE.getSelectedIndex()]);
    // lexique.HostFieldPutData("PCHX", 0, PCHX_Value[PCHX.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    OBJ_62 = new JLabel();
    OBJ_50 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    PEXE = new XRiComboBox();
    PPER = new XRiComboBox();
    S3LIB = new XRiTextField();
    OBJ_55 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_66 = new JLabel();
    DTDEB = new RiZoneSortie();
    DTFIN = new RiZoneSortie();
    PCHX = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(780, 40));
          p_tete_gauche.setMinimumSize(new Dimension(780, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_43 ----
          OBJ_43.setText("Etablissement");
          OBJ_43.setName("OBJ_43");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          //---- OBJ_62 ----
          OBJ_62.setText("P\u00e9riodes \u00e9dition statistiques");
          OBJ_62.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_62.setName("OBJ_62");

          //---- OBJ_50 ----
          OBJ_50.setText("@WENCX@");
          OBJ_50.setOpaque(false);
          OBJ_50.setName("OBJ_50");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(175, 175, 175)
                    .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 223, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(610, 230));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Personnalisation des p\u00e9riodes"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- PEXE ----
            PEXE.setModel(new DefaultComboBoxModel(new String[] {
              "Ann\u00e9e en cours",
              "Ann\u00e9e - 1",
              "Ann\u00e9e - 2",
              "Ann\u00e9e - 3",
              "Ann\u00e9e - 4",
              "Ann\u00e9e - 5",
              "Ann\u00e9e - 6",
              "Ann\u00e9e - 7",
              "Ann\u00e9e - 8",
              "Ann\u00e9e - 9",
              "Ann\u00e9e - 10"
            }));
            PEXE.setComponentPopupMenu(BTD);
            PEXE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PEXE.setName("PEXE");
            panel1.add(PEXE);
            PEXE.setBounds(125, 60, 120, PEXE.getPreferredSize().height);

            //---- PPER ----
            PPER.setModel(new DefaultComboBoxModel(new String[] {
              "Exercice",
              "Semestre",
              "Trimestre",
              "Mois",
              "Jour"
            }));
            PPER.setComponentPopupMenu(BTD);
            PPER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PPER.setName("PPER");
            panel1.add(PPER);
            PPER.setBounds(255, 60, 96, PPER.getPreferredSize().height);

            //---- S3LIB ----
            S3LIB.setComponentPopupMenu(BTD);
            S3LIB.setName("S3LIB");
            panel1.add(S3LIB);
            S3LIB.setBounds(125, 30, 370, S3LIB.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("Lib\u00e9ll\u00e9 p\u00e9riode");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(20, 34, 93, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("Code p\u00e9riode");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(20, 63, 87, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("Date d\u00e9but");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(20, 94, 76, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("\u00e0 fin");
            OBJ_66.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(205, 92, 60, 20);

            //---- DTDEB ----
            DTDEB.setText("@DTDEB@");
            DTDEB.setName("DTDEB");
            panel1.add(DTDEB);
            DTDEB.setBounds(125, 90, 70, DTDEB.getPreferredSize().height);

            //---- DTFIN ----
            DTFIN.setText("@DTFIN@");
            DTFIN.setName("DTFIN");
            panel1.add(DTFIN);
            DTFIN.setBounds(275, 90, 70, DTFIN.getPreferredSize().height);

            //---- PCHX ----
            PCHX.setComponentPopupMenu(BTD);
            PCHX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PCHX.setName("PCHX");
            panel1.add(PCHX);
            PCHX.setBounds(360, 60, 135, PCHX.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 515, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(53, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JLabel OBJ_62;
  private RiZoneSortie OBJ_50;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox PEXE;
  private XRiComboBox PPER;
  private XRiTextField S3LIB;
  private JLabel OBJ_55;
  private JLabel OBJ_59;
  private JLabel OBJ_64;
  private JLabel OBJ_66;
  private RiZoneSortie DTDEB;
  private RiZoneSortie DTFIN;
  private XRiComboBox PCHX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
