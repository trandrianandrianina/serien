
package ri.serien.libecranrpg.vgvm.VGVM55FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM55FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] WRON_Value = { " ", "2", "3", "4", "5", "6" };
  private String[] WCTVA_Value = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
  private String[] WCTVA_Title = { "@W4TVA1@%", "@W4TVA2@%", "@W4TVA3@%", "@W4TVA4@%", "@W4TVA5@%", "@W4TVA6@%", "Exon\u00e9r\u00e9",
      "Exon\u00e9r\u00e9", "Exon\u00e9r\u00e9" };
  
  private static final String BOUTON_TARIF_PRECEDENT = "Afficher tarif précédent";
  private static final String BOUTON_FUTUR_TARIF = "Afficher futur tarif";
  private static final String BOUTON_MODIFIER_PRV = "Modifier PRV";
  private static final String BOUTON_ECRAN_PRECEDENT = "Retourner écran précédent";
  
  public VGVM55FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WCTVA.setValeurs(WCTVA_Value, WCTVA_Title);
    WRON.setValeurs(WRON_Value, null);
    ckTTC.setValeursSelection("O", "N");
    
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(BOUTON_TARIF_PRECEDENT, 'P', true);
    snBarreBouton.ajouterBouton(BOUTON_FUTUR_TARIF, 'T', true);
    snBarreBouton.ajouterBouton(BOUTON_ECRAN_PRECEDENT, 'A', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_PRV, 'M', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    tfSupport.setVisible(false);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    tfIDDevise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV@")).trim());
    tfLibelleDevise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLDEV@")).trim());
    tfIDUniteVente.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNV@")).trim());
    tfLibelleUniteVente.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLUNV@")).trim());
    tfChange.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCHGX@")).trim());
    lbPrixPublic.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LTAR1@")).trim());
    P55VR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P55VR2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Gestion auto des erreurs à l'aide du bouton erreur
    gererLesErreurs("19");

    boolean isConsult = lexique.isTrue("53");
    
    boolean is21 = lexique.isTrue("21");
    boolean is20 = lexique.isTrue("20");
    boolean is25 = lexique.isTrue("25");
    boolean is26 = lexique.isTrue("26");
    boolean is27 = lexique.isTrue("27");
    boolean is28 = lexique.isTrue("28");
    boolean is29 = lexique.isTrue("29");
    boolean is47 = lexique.isTrue("47");
    boolean is64 = lexique.isTrue("64");
    boolean is67 = lexique.isTrue("67");
    boolean isPremier = lexique.isTrue("60");
    boolean isDernier = lexique.isTrue("61");
    boolean isConsultation = (lexique.getMode() == Lexical.MODE_CONSULTATION);
    boolean isModification = (lexique.getMode() == Lexical.MODE_MODIFICATION);
    boolean isP55SEC = lexique.HostFieldGetData("P55SEC").trim().equals("4");
    
    snBarreBouton.activerBouton(BOUTON_MODIFIER_PRV, isModification);
    
    if (is21 && !is20) {
      tfSupport.setText(lexique.HostFieldGetData("TAART"));
      tfSupport.setVisible(true);
      lbSupport.setVisible(true);
    }
    else {
      tfSupport.setVisible(false);
      lbSupport.setVisible(false);
    }
    if (is21 && !is20 && !isConsultation) {
      tfSupport.setEnabled(true);
      lbSupport.setEnabled(true);
    }
    else {
      tfSupport.setEnabled(false);
      lbSupport.setEnabled(false);
    }
    btGauche.setVisible(!isPremier && !isP55SEC);
    btDroite.setVisible(!isDernier && !isP55SEC);
    lbPrixPropose.setVisible(is47);
    lbPrixCorrige.setVisible(is47);
    
    // Radio bouton
    rbIndice.setEnabled(!isConsult);
    rbRemise.setEnabled(!isConsult);
    rbPrixHT.setEnabled(!isConsult);
    rbPrixTTC.setEnabled(!isConsult);
    rbMarge.setEnabled(!isConsult);
    rbIndice.setSelected(is25);
    rbRemise.setSelected(is26);
    rbPrixHT.setSelected(is27);
    rbPrixTTC.setSelected(is28);
    rbMarge.setSelected(is29);
    
    WCOE1.setEnabled(is25 && !isConsultation);
    WCOE2.setEnabled(is25 && !isConsultation);
    WCOE3.setEnabled(is25 && !isConsultation);
    WCOE4.setEnabled(is25 && !isConsultation);
    WCOE5.setEnabled(is25 && !isConsultation);
    WCOE6.setEnabled(is25 && !isConsultation);
    
    WREM2.setEnabled(is26 && !isConsultation);
    WREM3.setEnabled(is26 && !isConsultation);
    WREM4.setEnabled(is26 && !isConsultation);
    WREM5.setEnabled(is26 && !isConsultation);
    WREM6.setEnabled(is26 && !isConsultation);
    
    WHT1A.setEnabled(is27 && !isConsultation);
    WHT2A.setEnabled(is27 && !isConsultation);
    WHT3A.setEnabled(is27 && !isConsultation);
    WHT4A.setEnabled(is27 && !isConsultation);
    WHT5A.setEnabled(is27 && !isConsultation);
    WHT6A.setEnabled(is27 && !isConsultation);
    WHT1B.setEnabled(is27 && !isConsultation);
    WHT2B.setEnabled(is27 && !isConsultation);
    WHT3B.setEnabled(is27 && !isConsultation);
    WHT4B.setEnabled(is27 && !isConsultation);
    WHT5B.setEnabled(is27 && !isConsultation);
    WHT6B.setEnabled(is27 && !isConsultation);
    WHT1C.setEnabled(is27 && !isConsultation);
    WHT2C.setEnabled(is27 && !isConsultation);
    WHT3C.setEnabled(is27 && !isConsultation);
    WHT4C.setEnabled(is27 && !isConsultation);
    WHT5C.setEnabled(is27 && !isConsultation);
    WHT6C.setEnabled(is27 && !isConsultation);
    
    WTTC1A.setEnabled(is28 && !isConsultation);
    WTTC2A.setEnabled(is28 && !isConsultation);
    WTTC3A.setEnabled(is28 && !isConsultation);
    WTTC4A.setEnabled(is28 && !isConsultation);
    WTTC5A.setEnabled(is28 && !isConsultation);
    WTTC6A.setEnabled(is28 && !isConsultation);
    WTTC1B.setEnabled(is28 && !isConsultation);
    WTTC2B.setEnabled(is28 && !isConsultation);
    WTTC3B.setEnabled(is28 && !isConsultation);
    WTTC4B.setEnabled(is28 && !isConsultation);
    WTTC5B.setEnabled(is28 && !isConsultation);
    WTTC6B.setEnabled(is28 && !isConsultation);
    WTTC1C.setEnabled(is28 && !isConsultation);
    WTTC2C.setEnabled(is28 && !isConsultation);
    WTTC3C.setEnabled(is28 && !isConsultation);
    WTTC4C.setEnabled(is28 && !isConsultation);
    WTTC5C.setEnabled(is28 && !isConsultation);
    WTTC6C.setEnabled(is28 && !isConsultation);
    
    WMAR1.setEnabled(is29 && !isConsultation);
    WMAR2.setEnabled(is29 && !isConsultation);
    WMAR3.setEnabled(is29 && !isConsultation);
    WMAR4.setEnabled(is29 && !isConsultation);
    WMAR5.setEnabled(is29 && !isConsultation);
    WMAR6.setEnabled(is29 && !isConsultation);
    
    WHT1A.setVisible(!is64 && !is67);
    WHT2A.setVisible(!is64 && !is67);
    WHT3A.setVisible(!is64 && !is67);
    WHT4A.setVisible(!is64 && !is67);
    WHT5A.setVisible(!is64 && !is67);
    WHT6A.setVisible(!is64 && !is67);
    WHT1B.setVisible(is64 && !is67);
    WHT2B.setVisible(is64 && !is67);
    WHT3B.setVisible(is64 && !is67);
    WHT4B.setVisible(is64 && !is67);
    WHT5B.setVisible(is64 && !is67);
    WHT6B.setVisible(is64 && !is67);
    WHT1C.setVisible(is67);
    WHT2C.setVisible(is67);
    WHT3C.setVisible(is67);
    WHT4C.setVisible(is67);
    WHT5C.setVisible(is67);
    WHT6C.setVisible(is67);
    WTTC1A.setVisible(!is64 && !is67);
    WTTC2A.setVisible(!is64 && !is67);
    WTTC3A.setVisible(!is64 && !is67);
    WTTC4A.setVisible(!is64 && !is67);
    WTTC5A.setVisible(!is64 && !is67);
    WTTC6A.setVisible(!is64 && !is67);
    WTTC1B.setVisible(is64 && !is67);
    WTTC2B.setVisible(is64 && !is67);
    WTTC3B.setVisible(is64 && !is67);
    WTTC4B.setVisible(is64 && !is67);
    WTTC5B.setVisible(is64 && !is67);
    WTTC6B.setVisible(is64 && !is67);
    WTTC1C.setVisible(is67);
    WTTC2C.setVisible(is67);
    WTTC3C.setVisible(is67);
    WTTC4C.setVisible(is67);
    WTTC5C.setVisible(is67);
    WTTC6C.setVisible(is67);
    
    tfChange.setEnabled(!isConsult);
    tfIDDevise.setEnabled(!isConsult);
    tfLibelleDevise.setEnabled(!isConsult);
    tfLibelleUniteVente.setEnabled(!isConsult);
    tfIDUniteVente.setEnabled(!isConsult);
    ckTTC.setEnabled(!isConsult);
    
    if (WPRVA.isVisible()) {
      WPRVA.requestFocusInWindow();
    }
    else if (WPRVAS.isVisible()) {
      WPRVAS.requestFocusInWindow();
    }
    else if (WPRVC.isVisible()) {
      WPRVC.requestFocusInWindow();
    }
    else if (WPRVCS.isVisible()) {
      WPRVCS.requestFocusInWindow();
    }
    
    // Prix proposé
    if (is47 && is67) {
      tfPrixVentePropose.setText(lexique.HostFieldGetData("WHT1CP"));
      tfPrixVentePropose2.setText(lexique.HostFieldGetData("WHT2CP"));
      tfPrixVentePropose3.setText(lexique.HostFieldGetData("WHT3CP"));
      tfPrixVentePropose4.setText(lexique.HostFieldGetData("WHT4CP"));
      tfPrixVentePropose5.setText(lexique.HostFieldGetData("WHT5CP"));
      tfPrixVentePropose6.setText(lexique.HostFieldGetData("WHT6CP"));
      tfPrixVenteCorrige.setText(lexique.HostFieldGetData("WHT1CC"));
      tfPrixVenteCorrige2.setText(lexique.HostFieldGetData("WHT2CC"));
      tfPrixVenteCorrige3.setText(lexique.HostFieldGetData("WHT3CC"));
      tfPrixVenteCorrige4.setText(lexique.HostFieldGetData("WHT4CC"));
      tfPrixVenteCorrige5.setText(lexique.HostFieldGetData("WHT5CC"));
      tfPrixVenteCorrige6.setText(lexique.HostFieldGetData("WHT6CC"));
    }
    else if (is47 && is64 && !is67) {
      tfPrixVentePropose.setText(lexique.HostFieldGetData("WHT1BP"));
      tfPrixVentePropose2.setText(lexique.HostFieldGetData("WHT2BP"));
      tfPrixVentePropose3.setText(lexique.HostFieldGetData("WHT3BP"));
      tfPrixVentePropose4.setText(lexique.HostFieldGetData("WHT4BP"));
      tfPrixVentePropose5.setText(lexique.HostFieldGetData("WHT5BP"));
      tfPrixVentePropose6.setText(lexique.HostFieldGetData("WHT6BP"));
      tfPrixVenteCorrige.setText(lexique.HostFieldGetData("WHT1BC"));
      tfPrixVenteCorrige2.setText(lexique.HostFieldGetData("WHT2BC"));
      tfPrixVenteCorrige3.setText(lexique.HostFieldGetData("WHT3BC"));
      tfPrixVenteCorrige4.setText(lexique.HostFieldGetData("WHT4BC"));
      tfPrixVenteCorrige5.setText(lexique.HostFieldGetData("WHT5BC"));
      tfPrixVenteCorrige6.setText(lexique.HostFieldGetData("WHT6BC"));
    }
    else if (is47 && !is64 && !is67) {
      tfPrixVentePropose.setText(lexique.HostFieldGetData("WHT1AP"));
      tfPrixVentePropose2.setText(lexique.HostFieldGetData("WHT2AP"));
      tfPrixVentePropose3.setText(lexique.HostFieldGetData("WHT3AP"));
      tfPrixVentePropose4.setText(lexique.HostFieldGetData("WHT4AP"));
      tfPrixVentePropose5.setText(lexique.HostFieldGetData("WHT5AP"));
      tfPrixVentePropose6.setText(lexique.HostFieldGetData("WHT6AP"));
      tfPrixVenteCorrige.setText(lexique.HostFieldGetData("WHT1AC"));
      tfPrixVenteCorrige2.setText(lexique.HostFieldGetData("WHT2AC"));
      tfPrixVenteCorrige3.setText(lexique.HostFieldGetData("WHT3AC"));
      tfPrixVenteCorrige4.setText(lexique.HostFieldGetData("WHT4AC"));
      tfPrixVenteCorrige5.setText(lexique.HostFieldGetData("WHT5AC"));
      tfPrixVenteCorrige6.setText(lexique.HostFieldGetData("WHT6AC"));
    }
    
    tfChange.setVisible(!lexique.HostFieldGetData("WCHGX").trim().equals(""));
    lbChange.setVisible(tfChange.isVisible());
    lbVariationPrix.setVisible(is47);
    lbCorrectionPrix.setVisible(is47);
    pnlPourcentageVariation.setVisible(is47);
    pnlAjustementPrixVente.setVisible(is47);
    
    
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    btGauche.setIcon(lexique.chargerImage("images/arriere.gif", true));
    btDroite.setIcon(lexique.chargerImage("images/avant.gif", true));
    snBarreBouton.rafraichir(lexique);
    
    // Charger le composant article
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(idEtablissement);
    snArticle.charger(true);
    snArticle.setSelectionParChampRPG(lexique, "WART");
    
  }
  
  @Override
  public void getData() {
    super.getData();
    snArticle.renseignerChampRPG(lexique, "WART");
  }
  
  protected void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (snBarreBouton.isTraiterClickBouton(pSNBouton)) {
        return;
      }
      if (pSNBouton.isBouton(BOUTON_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_TARIF_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F7");
      }
      else if (pSNBouton.isBouton(BOUTON_FUTUR_TARIF)) {
        lexique.HostScreenSendKey(this, "F8");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_PRV)) {
        lexique.HostScreenSendKey(this, "F6");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void clicBoutonIndice(ActionEvent e) {
    lexique.HostFieldPutData("WBASE", 0, "1");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void clickBoutonRemise(ActionEvent e) {
    lexique.HostFieldPutData("WBASE", 0, "2");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void clickBoutonPixHT(ActionEvent e) {
    lexique.HostFieldPutData("WBASE", 0, "3");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void clickBoutonPrixTTC(ActionEvent e) {
    lexique.HostFieldPutData("WBASE", 0, "4");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void clickBoutonMarge(ActionEvent e) {
    lexique.HostFieldPutData("WBASE", 0, "5");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btGaucheActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void btDroiteActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new SNPanel();
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlArticle = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbDevise = new SNLabelChamp();
    pnlDevise = new SNPanel();
    tfIDDevise = new SNTexte();
    tfLibelleDevise = new SNTexte();
    lbPrixRevient = new SNLabelChamp();
    pnlPrv = new SNPanel();
    WPRVA = new XRiTextField();
    WPRVAS = new XRiTextField();
    WPRVC = new XRiTextField();
    WPRVCS = new XRiTextField();
    pnlInfoPrix = new SNPanelTitre();
    lbDateApplication = new SNLabelChamp();
    pnlDateApplication = new SNPanel();
    btGauche = new SNBouton();
    WDAPL = new XRiCalendrier();
    btDroite = new SNBouton();
    lbCreeLe = new SNLabelChamp();
    WDCRE = new XRiCalendrier();
    lbModifieLe = new SNLabelChamp();
    WDMOD = new XRiCalendrier();
    lbUniteVente = new SNLabelChamp();
    pnlUnite = new SNPanel();
    tfIDUniteVente = new SNTexte();
    tfLibelleUniteVente = new SNTexte();
    lbArrondi = new SNLabelChamp();
    WRON = new XRiComboBox();
    lbChange = new SNLabelChamp();
    tfChange = new SNTexte();
    lbSupport = new SNLabelChamp();
    tfSupport = new SNTexte();
    lbCodeTVA = new SNLabelChamp();
    WCTVA = new XRiComboBox();
    ckTTC = new XRiCheckBox();
    pnlColonneTarif = new SNPanelTitre();
    lbPrixPublic = new SNLabelChamp();
    lbPrixPublic2 = new SNLabelChamp();
    lbPrixPublic3 = new SNLabelChamp();
    lbPrixPublic4 = new SNLabelChamp();
    lbPrixPublic5 = new SNLabelChamp();
    lbPrixPublic6 = new SNLabelChamp();
    rbIndice = new SNRadioButton();
    WCOE1 = new XRiTextField();
    WCOE2 = new XRiTextField();
    WCOE3 = new XRiTextField();
    WCOE4 = new XRiTextField();
    WCOE5 = new XRiTextField();
    WCOE6 = new XRiTextField();
    rbRemise = new SNRadioButton();
    pnlRemise1 = new SNPanel();
    WREM2 = new XRiTextField();
    lbPourcentage = new SNLabelUnite();
    pnlRemise2 = new SNPanel();
    WREM3 = new XRiTextField();
    lbPourcentage2 = new SNLabelUnite();
    pnlRemise3 = new SNPanel();
    WREM4 = new XRiTextField();
    lbPourcentage3 = new SNLabelUnite();
    pnlRemise4 = new SNPanel();
    WREM5 = new XRiTextField();
    lbPourcentage4 = new SNLabelUnite();
    pnlRemise5 = new SNPanel();
    WREM6 = new XRiTextField();
    lbPourcentage5 = new SNLabelUnite();
    rbPrixHT = new SNRadioButton();
    WHT1C = new XRiTextField();
    WHT1A = new XRiTextField();
    WHT1B = new XRiTextField();
    WHT2C = new XRiTextField();
    WHT2A = new XRiTextField();
    WHT2B = new XRiTextField();
    WHT3C = new XRiTextField();
    WHT3A = new XRiTextField();
    WHT3B = new XRiTextField();
    WHT4C = new XRiTextField();
    WHT4A = new XRiTextField();
    WHT4B = new XRiTextField();
    WHT5C = new XRiTextField();
    WHT5A = new XRiTextField();
    WHT5B = new XRiTextField();
    WHT6C = new XRiTextField();
    WHT6A = new XRiTextField();
    WHT6B = new XRiTextField();
    rbPrixTTC = new SNRadioButton();
    WTTC1C = new XRiTextField();
    WTTC1A = new XRiTextField();
    WTTC1B = new XRiTextField();
    WTTC2C = new XRiTextField();
    WTTC2A = new XRiTextField();
    WTTC2B = new XRiTextField();
    WTTC3C = new XRiTextField();
    WTTC3A = new XRiTextField();
    WTTC3B = new XRiTextField();
    WTTC4C = new XRiTextField();
    WTTC4A = new XRiTextField();
    WTTC4B = new XRiTextField();
    WTTC5C = new XRiTextField();
    WTTC5A = new XRiTextField();
    WTTC5B = new XRiTextField();
    WTTC6C = new XRiTextField();
    WTTC6A = new XRiTextField();
    WTTC6B = new XRiTextField();
    rbMarge = new SNRadioButton();
    pnlMarge1 = new SNPanel();
    WMAR1 = new XRiTextField();
    lbPourcentage6 = new SNLabelUnite();
    pnlMarge2 = new SNPanel();
    WMAR2 = new XRiTextField();
    lbPourcentage7 = new SNLabelUnite();
    pnlMarge3 = new SNPanel();
    WMAR3 = new XRiTextField();
    lbPourcentage8 = new SNLabelUnite();
    pnlMarge4 = new SNPanel();
    WMAR4 = new XRiTextField();
    lbPourcentage9 = new SNLabelUnite();
    pnlMarge5 = new SNPanel();
    WMAR5 = new XRiTextField();
    lbPourcentage10 = new SNLabelUnite();
    pnlMarge6 = new SNPanel();
    WMAR6 = new XRiTextField();
    lbPourcentage11 = new SNLabelUnite();
    pnlAjustementPrixVente = new SNPanelTitre();
    lbPrixPropose = new SNLabelChamp();
    tfPrixVentePropose = new SNTexte();
    tfPrixVentePropose2 = new SNTexte();
    tfPrixVentePropose3 = new SNTexte();
    tfPrixVentePropose4 = new SNTexte();
    tfPrixVentePropose5 = new SNTexte();
    tfPrixVentePropose6 = new SNTexte();
    lbPrixCorrige = new SNLabelChamp();
    tfPrixVenteCorrige = new SNTexte();
    tfPrixVenteCorrige2 = new SNTexte();
    tfPrixVenteCorrige3 = new SNTexte();
    tfPrixVenteCorrige4 = new SNTexte();
    tfPrixVenteCorrige5 = new SNTexte();
    tfPrixVenteCorrige6 = new SNTexte();
    pnlPourcentageVariation = new SNPanel();
    lbVariationPrix = new SNLabelChamp();
    P55VR1 = new XRiTextField();
    lbCorrectionPrix = new SNLabelChamp();
    P55VR2 = new XRiTextField();
    snBarreBouton = new XRiBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    
    // ======== this ========
    setMaximumSize(new Dimension(1225, 765));
    setPreferredSize(new Dimension(1025, 715));
    setMinimumSize(new Dimension(1025, 715));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());
      
      // ---- bpPresentation ----
      bpPresentation.setText("@TITPG1@ @TITPG2@");
      bpPresentation.setName("bpPresentation");
      pnlBandeau.add(bpPresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(1225, 765));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setMinimumSize(new Dimension(1225, 765));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlArticle ========
      {
        pnlArticle.setName("pnlArticle");
        pnlArticle.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle.setPreferredSize(new Dimension(200, 30));
        lbArticle.setMinimumSize(new Dimension(200, 30));
        lbArticle.setMaximumSize(new Dimension(200, 30));
        lbArticle.setName("lbArticle");
        pnlArticle.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snArticle ----
        snArticle.setEnabled(false);
        snArticle.setFont(new Font("sansserif", Font.PLAIN, 14));
        snArticle.setName("snArticle");
        pnlArticle.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDevise ----
        lbDevise.setText("Devise");
        lbDevise.setComponentPopupMenu(null);
        lbDevise.setPreferredSize(new Dimension(100, 30));
        lbDevise.setMinimumSize(new Dimension(100, 30));
        lbDevise.setMaximumSize(new Dimension(100, 30));
        lbDevise.setName("lbDevise");
        pnlArticle.add(lbDevise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlDevise ========
        {
          pnlDevise.setName("pnlDevise");
          pnlDevise.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDevise.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDevise.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDevise.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDevise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfIDDevise ----
          tfIDDevise.setText("@WDEV@");
          tfIDDevise.setComponentPopupMenu(null);
          tfIDDevise.setMaximumSize(new Dimension(50, 28));
          tfIDDevise.setMinimumSize(new Dimension(44, 30));
          tfIDDevise.setPreferredSize(new Dimension(44, 30));
          tfIDDevise.setEditable(false);
          tfIDDevise.setName("tfIDDevise");
          pnlDevise.add(tfIDDevise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfLibelleDevise ----
          tfLibelleDevise.setText("@WLDEV@");
          tfLibelleDevise.setComponentPopupMenu(null);
          tfLibelleDevise.setMaximumSize(new Dimension(150, 30));
          tfLibelleDevise.setMinimumSize(new Dimension(150, 30));
          tfLibelleDevise.setPreferredSize(new Dimension(150, 30));
          tfLibelleDevise.setEditable(false);
          tfLibelleDevise.setName("tfLibelleDevise");
          pnlDevise.add(tfLibelleDevise, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlArticle.add(pnlDevise, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPrixRevient ----
        lbPrixRevient.setText("Prix de revient standard HT");
        lbPrixRevient.setComponentPopupMenu(null);
        lbPrixRevient.setPreferredSize(new Dimension(200, 30));
        lbPrixRevient.setMinimumSize(new Dimension(200, 30));
        lbPrixRevient.setMaximumSize(new Dimension(200, 30));
        lbPrixRevient.setName("lbPrixRevient");
        pnlArticle.add(lbPrixRevient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlPrv ========
        {
          pnlPrv.setName("pnlPrv");
          pnlPrv.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrv.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlPrv.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPrv.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrv.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WPRVA ----
          WPRVA.setPreferredSize(new Dimension(100, 30));
          WPRVA.setMinimumSize(new Dimension(100, 30));
          WPRVA.setMaximumSize(new Dimension(100, 30));
          WPRVA.setName("WPRVA");
          pnlPrv.add(WPRVA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WPRVAS ----
          WPRVAS.setPreferredSize(new Dimension(100, 30));
          WPRVAS.setMinimumSize(new Dimension(100, 30));
          WPRVAS.setMaximumSize(new Dimension(100, 30));
          WPRVAS.setName("WPRVAS");
          pnlPrv.add(WPRVAS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WPRVC ----
          WPRVC.setPreferredSize(new Dimension(100, 30));
          WPRVC.setMinimumSize(new Dimension(100, 30));
          WPRVC.setMaximumSize(new Dimension(100, 30));
          WPRVC.setName("WPRVC");
          pnlPrv.add(WPRVC, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WPRVCS ----
          WPRVCS.setPreferredSize(new Dimension(100, 30));
          WPRVCS.setMinimumSize(new Dimension(100, 30));
          WPRVCS.setMaximumSize(new Dimension(100, 30));
          WPRVCS.setName("WPRVCS");
          pnlPrv.add(WPRVCS, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlArticle.add(pnlPrv, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlArticle,
          new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlInfoPrix ========
      {
        pnlInfoPrix.setOpaque(false);
        pnlInfoPrix.setComponentPopupMenu(null);
        pnlInfoPrix.setTitre("Informations compl\u00e9mentaires");
        pnlInfoPrix.setName("pnlInfoPrix");
        pnlInfoPrix.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInfoPrix.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlInfoPrix.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInfoPrix.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlInfoPrix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDateApplication ----
        lbDateApplication.setText("Date d'application");
        lbDateApplication.setName("lbDateApplication");
        pnlInfoPrix.add(lbDateApplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlDateApplication ========
        {
          pnlDateApplication.setOpaque(false);
          pnlDateApplication.setName("pnlDateApplication");
          pnlDateApplication.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateApplication.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateApplication.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateApplication.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateApplication.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- btGauche ----
          btGauche.setToolTipText("Tarifs pr\u00e9c\u00e9dents");
          btGauche.setMaximumSize(new Dimension(28, 28));
          btGauche.setMinimumSize(new Dimension(28, 30));
          btGauche.setPreferredSize(new Dimension(28, 30));
          btGauche.setName("btGauche");
          btGauche.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btGaucheActionPerformed(e);
            }
          });
          pnlDateApplication.add(btGauche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WDAPL ----
          WDAPL.setComponentPopupMenu(null);
          WDAPL.setPreferredSize(new Dimension(110, 30));
          WDAPL.setMinimumSize(new Dimension(110, 30));
          WDAPL.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDAPL.setName("WDAPL");
          pnlDateApplication.add(WDAPL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- btDroite ----
          btDroite.setToolTipText("Futurs tarifs");
          btDroite.setPreferredSize(new Dimension(28, 30));
          btDroite.setMinimumSize(new Dimension(28, 30));
          btDroite.setMaximumSize(new Dimension(28, 28));
          btDroite.setName("btDroite");
          btDroite.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btDroiteActionPerformed(e);
            }
          });
          pnlDateApplication.add(btDroite, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInfoPrix.add(pnlDateApplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCreeLe ----
        lbCreeLe.setText("Cr\u00e9\u00e9 le");
        lbCreeLe.setComponentPopupMenu(null);
        lbCreeLe.setMaximumSize(new Dimension(100, 30));
        lbCreeLe.setMinimumSize(new Dimension(100, 30));
        lbCreeLe.setPreferredSize(new Dimension(100, 30));
        lbCreeLe.setName("lbCreeLe");
        pnlInfoPrix.add(lbCreeLe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WDCRE ----
        WDCRE.setComponentPopupMenu(null);
        WDCRE.setPreferredSize(new Dimension(110, 30));
        WDCRE.setMinimumSize(new Dimension(110, 30));
        WDCRE.setEditable(false);
        WDCRE.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDCRE.setName("WDCRE");
        pnlInfoPrix.add(WDCRE, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbModifieLe ----
        lbModifieLe.setText("Modifi\u00e9 le");
        lbModifieLe.setComponentPopupMenu(null);
        lbModifieLe.setMaximumSize(new Dimension(100, 28));
        lbModifieLe.setMinimumSize(new Dimension(100, 30));
        lbModifieLe.setPreferredSize(new Dimension(100, 30));
        lbModifieLe.setHorizontalAlignment(SwingConstants.TRAILING);
        lbModifieLe.setName("lbModifieLe");
        pnlInfoPrix.add(lbModifieLe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WDMOD ----
        WDMOD.setComponentPopupMenu(null);
        WDMOD.setPreferredSize(new Dimension(110, 30));
        WDMOD.setMinimumSize(new Dimension(110, 30));
        WDMOD.setEditable(false);
        WDMOD.setFont(new Font("sansserif", Font.PLAIN, 14));
        WDMOD.setName("WDMOD");
        pnlInfoPrix.add(WDMOD, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbUniteVente ----
        lbUniteVente.setText("Unit\u00e9 vente");
        lbUniteVente.setComponentPopupMenu(null);
        lbUniteVente.setPreferredSize(new Dimension(100, 30));
        lbUniteVente.setMinimumSize(new Dimension(100, 30));
        lbUniteVente.setMaximumSize(new Dimension(100, 30));
        lbUniteVente.setName("lbUniteVente");
        pnlInfoPrix.add(lbUniteVente, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlUnite ========
        {
          pnlUnite.setName("pnlUnite");
          pnlUnite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlUnite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlUnite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlUnite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlUnite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfIDUniteVente ----
          tfIDUniteVente.setText("@WUNV@");
          tfIDUniteVente.setComponentPopupMenu(null);
          tfIDUniteVente.setPreferredSize(new Dimension(44, 30));
          tfIDUniteVente.setMinimumSize(new Dimension(44, 30));
          tfIDUniteVente.setMaximumSize(new Dimension(50, 28));
          tfIDUniteVente.setEditable(false);
          tfIDUniteVente.setName("tfIDUniteVente");
          pnlUnite.add(tfIDUniteVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfLibelleUniteVente ----
          tfLibelleUniteVente.setText("@WLUNV@");
          tfLibelleUniteVente.setComponentPopupMenu(null);
          tfLibelleUniteVente.setPreferredSize(new Dimension(150, 30));
          tfLibelleUniteVente.setMinimumSize(new Dimension(150, 30));
          tfLibelleUniteVente.setMaximumSize(new Dimension(150, 30));
          tfLibelleUniteVente.setEditable(false);
          tfLibelleUniteVente.setName("tfLibelleUniteVente");
          pnlUnite.add(tfLibelleUniteVente, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInfoPrix.add(pnlUnite, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbArrondi ----
        lbArrondi.setText("Arrondi");
        lbArrondi.setHorizontalAlignment(SwingConstants.RIGHT);
        lbArrondi.setComponentPopupMenu(null);
        lbArrondi.setMaximumSize(new Dimension(100, 30));
        lbArrondi.setPreferredSize(new Dimension(100, 30));
        lbArrondi.setMinimumSize(new Dimension(100, 30));
        lbArrondi.setName("lbArrondi");
        pnlInfoPrix.add(lbArrondi, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WRON ----
        WRON.setModel(new DefaultComboBoxModel(new String[] { "aucun", "0,10", "0,25", "0,50", "1,00", "10,00" }));
        WRON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WRON.setComponentPopupMenu(null);
        WRON.setPreferredSize(new Dimension(100, 30));
        WRON.setMinimumSize(new Dimension(100, 30));
        WRON.setMaximumSize(new Dimension(100, 28));
        WRON.setFont(new Font("sansserif", Font.PLAIN, 14));
        WRON.setName("WRON");
        pnlInfoPrix.add(WRON, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbChange ----
        lbChange.setText("Change");
        lbChange.setComponentPopupMenu(null);
        lbChange.setPreferredSize(new Dimension(100, 30));
        lbChange.setMaximumSize(new Dimension(100, 30));
        lbChange.setMinimumSize(new Dimension(100, 30));
        lbChange.setName("lbChange");
        pnlInfoPrix.add(lbChange, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfChange ----
        tfChange.setText("@WCHGX@");
        tfChange.setHorizontalAlignment(SwingConstants.RIGHT);
        tfChange.setComponentPopupMenu(null);
        tfChange.setPreferredSize(new Dimension(104, 30));
        tfChange.setMaximumSize(new Dimension(100, 28));
        tfChange.setMinimumSize(new Dimension(104, 30));
        tfChange.setEditable(false);
        tfChange.setName("tfChange");
        pnlInfoPrix.add(tfChange, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSupport ----
        lbSupport.setText("Support");
        lbSupport.setComponentPopupMenu(null);
        lbSupport.setPreferredSize(new Dimension(50, 30));
        lbSupport.setMaximumSize(new Dimension(50, 30));
        lbSupport.setMinimumSize(new Dimension(50, 30));
        lbSupport.setName("lbSupport");
        pnlInfoPrix.add(lbSupport, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfSupport ----
        tfSupport.setEditable(false);
        tfSupport.setMaximumSize(new Dimension(75, 30));
        tfSupport.setPreferredSize(new Dimension(75, 30));
        tfSupport.setMinimumSize(new Dimension(75, 30));
        tfSupport.setName("tfSupport");
        pnlInfoPrix.add(tfSupport, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeTVA ----
        lbCodeTVA.setText("TVA");
        lbCodeTVA.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodeTVA.setComponentPopupMenu(null);
        lbCodeTVA.setMaximumSize(new Dimension(100, 30));
        lbCodeTVA.setPreferredSize(new Dimension(100, 30));
        lbCodeTVA.setMinimumSize(new Dimension(100, 30));
        lbCodeTVA.setName("lbCodeTVA");
        pnlInfoPrix.add(lbCodeTVA, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCTVA ----
        WCTVA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        WCTVA.setComponentPopupMenu(null);
        WCTVA.setMinimumSize(new Dimension(100, 30));
        WCTVA.setPreferredSize(new Dimension(100, 30));
        WCTVA.setMaximumSize(new Dimension(100, 28));
        WCTVA.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCTVA.setName("WCTVA");
        pnlInfoPrix.add(WCTVA, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- ckTTC ----
        ckTTC.setText("TVA sur TTC");
        ckTTC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckTTC.setComponentPopupMenu(null);
        ckTTC.setMaximumSize(new Dimension(100, 28));
        ckTTC.setMinimumSize(new Dimension(100, 30));
        ckTTC.setPreferredSize(new Dimension(100, 30));
        ckTTC.setFont(new Font("sansserif", Font.PLAIN, 14));
        ckTTC.setActionCommand("TVA sur TTC");
        ckTTC.setName("ckTTC");
        pnlInfoPrix.add(ckTTC, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInfoPrix,
          new GridBagConstraints(1, 1, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ======== pnlColonneTarif ========
      {
        pnlColonneTarif.setTitre("Colonnes de tarif");
        pnlColonneTarif.setName("pnlColonneTarif");
        pnlColonneTarif.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlColonneTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 28, 0, 0, 0, 0 };
        ((GridBagLayout) pnlColonneTarif.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlColonneTarif.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlColonneTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbPrixPublic ----
        lbPrixPublic.setText("@LTAR1@");
        lbPrixPublic.setComponentPopupMenu(null);
        lbPrixPublic.setPreferredSize(new Dimension(75, 30));
        lbPrixPublic.setMinimumSize(new Dimension(75, 30));
        lbPrixPublic.setMaximumSize(new Dimension(75, 30));
        lbPrixPublic.setName("lbPrixPublic");
        pnlColonneTarif.add(lbPrixPublic, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixPublic2 ----
        lbPrixPublic2.setText("Colonne 2");
        lbPrixPublic2.setMinimumSize(new Dimension(75, 30));
        lbPrixPublic2.setPreferredSize(new Dimension(75, 30));
        lbPrixPublic2.setMaximumSize(new Dimension(75, 30));
        lbPrixPublic2.setName("lbPrixPublic2");
        pnlColonneTarif.add(lbPrixPublic2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixPublic3 ----
        lbPrixPublic3.setText("Colonne 3");
        lbPrixPublic3.setComponentPopupMenu(null);
        lbPrixPublic3.setMinimumSize(new Dimension(75, 30));
        lbPrixPublic3.setPreferredSize(new Dimension(75, 30));
        lbPrixPublic3.setMaximumSize(new Dimension(75, 30));
        lbPrixPublic3.setName("lbPrixPublic3");
        pnlColonneTarif.add(lbPrixPublic3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixPublic4 ----
        lbPrixPublic4.setText("Colonne 4");
        lbPrixPublic4.setComponentPopupMenu(null);
        lbPrixPublic4.setMinimumSize(new Dimension(75, 30));
        lbPrixPublic4.setPreferredSize(new Dimension(75, 30));
        lbPrixPublic4.setMaximumSize(new Dimension(75, 30));
        lbPrixPublic4.setName("lbPrixPublic4");
        pnlColonneTarif.add(lbPrixPublic4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixPublic5 ----
        lbPrixPublic5.setText("Colonne 5");
        lbPrixPublic5.setComponentPopupMenu(null);
        lbPrixPublic5.setMinimumSize(new Dimension(75, 30));
        lbPrixPublic5.setPreferredSize(new Dimension(75, 30));
        lbPrixPublic5.setMaximumSize(new Dimension(75, 30));
        lbPrixPublic5.setName("lbPrixPublic5");
        pnlColonneTarif.add(lbPrixPublic5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixPublic6 ----
        lbPrixPublic6.setText("Colonne 6");
        lbPrixPublic6.setComponentPopupMenu(null);
        lbPrixPublic6.setMinimumSize(new Dimension(75, 30));
        lbPrixPublic6.setPreferredSize(new Dimension(75, 30));
        lbPrixPublic6.setMaximumSize(new Dimension(75, 30));
        lbPrixPublic6.setName("lbPrixPublic6");
        pnlColonneTarif.add(lbPrixPublic6, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbIndice ----
        rbIndice.setText("Indice");
        rbIndice.setToolTipText("Calcul des tarifs par coefficients");
        rbIndice.setComponentPopupMenu(null);
        rbIndice.setMaximumSize(new Dimension(150, 30));
        rbIndice.setName("rbIndice");
        rbIndice.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            clicBoutonIndice(e);
          }
        });
        pnlColonneTarif.add(rbIndice, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCOE1 ----
        WCOE1.setComponentPopupMenu(null);
        WCOE1.setPreferredSize(new Dimension(75, 30));
        WCOE1.setMinimumSize(new Dimension(75, 30));
        WCOE1.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOE1.setMaximumSize(new Dimension(75, 30));
        WCOE1.setName("WCOE1");
        pnlColonneTarif.add(WCOE1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCOE2 ----
        WCOE2.setComponentPopupMenu(null);
        WCOE2.setMinimumSize(new Dimension(75, 30));
        WCOE2.setPreferredSize(new Dimension(75, 30));
        WCOE2.setFont(new Font("sansserif", Font.BOLD, 14));
        WCOE2.setMaximumSize(new Dimension(75, 30));
        WCOE2.setName("WCOE2");
        pnlColonneTarif.add(WCOE2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCOE3 ----
        WCOE3.setComponentPopupMenu(null);
        WCOE3.setPreferredSize(new Dimension(75, 30));
        WCOE3.setMinimumSize(new Dimension(75, 30));
        WCOE3.setMaximumSize(new Dimension(75, 30));
        WCOE3.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOE3.setName("WCOE3");
        pnlColonneTarif.add(WCOE3, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCOE4 ----
        WCOE4.setComponentPopupMenu(null);
        WCOE4.setMaximumSize(new Dimension(75, 30));
        WCOE4.setMinimumSize(new Dimension(75, 30));
        WCOE4.setPreferredSize(new Dimension(75, 30));
        WCOE4.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOE4.setName("WCOE4");
        pnlColonneTarif.add(WCOE4, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCOE5 ----
        WCOE5.setComponentPopupMenu(null);
        WCOE5.setPreferredSize(new Dimension(75, 30));
        WCOE5.setMinimumSize(new Dimension(75, 30));
        WCOE5.setMaximumSize(new Dimension(75, 30));
        WCOE5.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOE5.setName("WCOE5");
        pnlColonneTarif.add(WCOE5, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WCOE6 ----
        WCOE6.setComponentPopupMenu(null);
        WCOE6.setMaximumSize(new Dimension(75, 30));
        WCOE6.setMinimumSize(new Dimension(75, 30));
        WCOE6.setPreferredSize(new Dimension(75, 30));
        WCOE6.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOE6.setName("WCOE6");
        pnlColonneTarif.add(WCOE6, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbRemise ----
        rbRemise.setText("Remise");
        rbRemise.setToolTipText("Calcul des tarifs avec remises");
        rbRemise.setComponentPopupMenu(null);
        rbRemise.setMaximumSize(new Dimension(150, 30));
        rbRemise.setName("rbRemise");
        rbRemise.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            clickBoutonRemise(e);
          }
        });
        pnlColonneTarif.add(rbRemise, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRemise1 ========
        {
          pnlRemise1.setName("pnlRemise1");
          pnlRemise1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRemise1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRemise1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRemise1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRemise1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WREM2 ----
          WREM2.setComponentPopupMenu(null);
          WREM2.setMinimumSize(new Dimension(55, 30));
          WREM2.setPreferredSize(new Dimension(55, 30));
          WREM2.setMaximumSize(new Dimension(55, 30));
          WREM2.setFont(new Font("sansserif", Font.PLAIN, 14));
          WREM2.setName("WREM2");
          pnlRemise1.add(WREM2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage ----
          lbPourcentage.setText("%");
          lbPourcentage.setPreferredSize(new Dimension(15, 30));
          lbPourcentage.setMinimumSize(new Dimension(15, 30));
          lbPourcentage.setMaximumSize(new Dimension(15, 30));
          lbPourcentage.setName("lbPourcentage");
          pnlRemise1.add(lbPourcentage, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlRemise1, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRemise2 ========
        {
          pnlRemise2.setName("pnlRemise2");
          pnlRemise2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRemise2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRemise2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRemise2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRemise2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WREM3 ----
          WREM3.setComponentPopupMenu(null);
          WREM3.setPreferredSize(new Dimension(55, 30));
          WREM3.setMinimumSize(new Dimension(55, 30));
          WREM3.setMaximumSize(new Dimension(55, 30));
          WREM3.setFont(new Font("sansserif", Font.PLAIN, 14));
          WREM3.setName("WREM3");
          pnlRemise2.add(WREM3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage2 ----
          lbPourcentage2.setText("%");
          lbPourcentage2.setPreferredSize(new Dimension(15, 30));
          lbPourcentage2.setMinimumSize(new Dimension(15, 30));
          lbPourcentage2.setMaximumSize(new Dimension(15, 30));
          lbPourcentage2.setName("lbPourcentage2");
          pnlRemise2.add(lbPourcentage2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlRemise2, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRemise3 ========
        {
          pnlRemise3.setName("pnlRemise3");
          pnlRemise3.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRemise3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRemise3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRemise3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRemise3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WREM4 ----
          WREM4.setComponentPopupMenu(null);
          WREM4.setMinimumSize(new Dimension(55, 30));
          WREM4.setPreferredSize(new Dimension(55, 30));
          WREM4.setMaximumSize(new Dimension(55, 30));
          WREM4.setFont(new Font("sansserif", Font.PLAIN, 14));
          WREM4.setName("WREM4");
          pnlRemise3.add(WREM4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage3 ----
          lbPourcentage3.setText("%");
          lbPourcentage3.setPreferredSize(new Dimension(15, 30));
          lbPourcentage3.setMinimumSize(new Dimension(15, 30));
          lbPourcentage3.setMaximumSize(new Dimension(15, 30));
          lbPourcentage3.setName("lbPourcentage3");
          pnlRemise3.add(lbPourcentage3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlRemise3, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRemise4 ========
        {
          pnlRemise4.setName("pnlRemise4");
          pnlRemise4.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRemise4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRemise4.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRemise4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRemise4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WREM5 ----
          WREM5.setComponentPopupMenu(null);
          WREM5.setMinimumSize(new Dimension(55, 30));
          WREM5.setPreferredSize(new Dimension(55, 30));
          WREM5.setMaximumSize(new Dimension(55, 30));
          WREM5.setFont(new Font("sansserif", Font.PLAIN, 14));
          WREM5.setName("WREM5");
          pnlRemise4.add(WREM5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage4 ----
          lbPourcentage4.setText("%");
          lbPourcentage4.setComponentPopupMenu(null);
          lbPourcentage4.setPreferredSize(new Dimension(15, 30));
          lbPourcentage4.setMinimumSize(new Dimension(15, 30));
          lbPourcentage4.setMaximumSize(new Dimension(15, 30));
          lbPourcentage4.setName("lbPourcentage4");
          pnlRemise4.add(lbPourcentage4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlRemise4, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRemise5 ========
        {
          pnlRemise5.setName("pnlRemise5");
          pnlRemise5.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRemise5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRemise5.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRemise5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRemise5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WREM6 ----
          WREM6.setComponentPopupMenu(null);
          WREM6.setPreferredSize(new Dimension(55, 30));
          WREM6.setMinimumSize(new Dimension(55, 30));
          WREM6.setMaximumSize(new Dimension(55, 30));
          WREM6.setFont(new Font("sansserif", Font.PLAIN, 14));
          WREM6.setName("WREM6");
          pnlRemise5.add(WREM6, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage5 ----
          lbPourcentage5.setText("%");
          lbPourcentage5.setPreferredSize(new Dimension(15, 30));
          lbPourcentage5.setMinimumSize(new Dimension(15, 30));
          lbPourcentage5.setMaximumSize(new Dimension(15, 30));
          lbPourcentage5.setName("lbPourcentage5");
          pnlRemise5.add(lbPourcentage5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlRemise5, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbPrixHT ----
        rbPrixHT.setText("Prix net H.T");
        rbPrixHT.setToolTipText("Calcul des tarifs par prix hors taxes");
        rbPrixHT.setComponentPopupMenu(null);
        rbPrixHT.setMaximumSize(new Dimension(150, 30));
        rbPrixHT.setName("rbPrixHT");
        rbPrixHT.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            clickBoutonPixHT(e);
          }
        });
        pnlColonneTarif.add(rbPrixHT, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT1C ----
        WHT1C.setComponentPopupMenu(null);
        WHT1C.setPreferredSize(new Dimension(75, 30));
        WHT1C.setMinimumSize(new Dimension(75, 30));
        WHT1C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT1C.setMaximumSize(new Dimension(75, 30));
        WHT1C.setName("WHT1C");
        pnlColonneTarif.add(WHT1C, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT1A ----
        WHT1A.setComponentPopupMenu(null);
        WHT1A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT1A.setPreferredSize(new Dimension(75, 30));
        WHT1A.setMinimumSize(new Dimension(75, 30));
        WHT1A.setMaximumSize(new Dimension(75, 30));
        WHT1A.setName("WHT1A");
        pnlColonneTarif.add(WHT1A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT1B ----
        WHT1B.setComponentPopupMenu(null);
        WHT1B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT1B.setPreferredSize(new Dimension(75, 30));
        WHT1B.setMinimumSize(new Dimension(75, 30));
        WHT1B.setMaximumSize(new Dimension(75, 30));
        WHT1B.setName("WHT1B");
        pnlColonneTarif.add(WHT1B, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT2C ----
        WHT2C.setComponentPopupMenu(null);
        WHT2C.setMinimumSize(new Dimension(75, 30));
        WHT2C.setPreferredSize(new Dimension(75, 30));
        WHT2C.setMaximumSize(new Dimension(75, 30));
        WHT2C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT2C.setName("WHT2C");
        pnlColonneTarif.add(WHT2C, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT2A ----
        WHT2A.setComponentPopupMenu(null);
        WHT2A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT2A.setMinimumSize(new Dimension(75, 30));
        WHT2A.setPreferredSize(new Dimension(75, 30));
        WHT2A.setMaximumSize(new Dimension(75, 30));
        WHT2A.setName("WHT2A");
        pnlColonneTarif.add(WHT2A, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT2B ----
        WHT2B.setComponentPopupMenu(null);
        WHT2B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT2B.setMinimumSize(new Dimension(75, 30));
        WHT2B.setPreferredSize(new Dimension(75, 30));
        WHT2B.setMaximumSize(new Dimension(75, 30));
        WHT2B.setName("WHT2B");
        pnlColonneTarif.add(WHT2B, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT3C ----
        WHT3C.setComponentPopupMenu(null);
        WHT3C.setMinimumSize(new Dimension(75, 30));
        WHT3C.setPreferredSize(new Dimension(75, 30));
        WHT3C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT3C.setMaximumSize(new Dimension(75, 30));
        WHT3C.setName("WHT3C");
        pnlColonneTarif.add(WHT3C, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT3A ----
        WHT3A.setComponentPopupMenu(null);
        WHT3A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT3A.setMinimumSize(new Dimension(75, 30));
        WHT3A.setPreferredSize(new Dimension(75, 30));
        WHT3A.setMaximumSize(new Dimension(75, 30));
        WHT3A.setName("WHT3A");
        pnlColonneTarif.add(WHT3A, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT3B ----
        WHT3B.setComponentPopupMenu(null);
        WHT3B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT3B.setMinimumSize(new Dimension(75, 30));
        WHT3B.setPreferredSize(new Dimension(75, 30));
        WHT3B.setMaximumSize(new Dimension(75, 30));
        WHT3B.setName("WHT3B");
        pnlColonneTarif.add(WHT3B, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT4C ----
        WHT4C.setComponentPopupMenu(null);
        WHT4C.setPreferredSize(new Dimension(75, 30));
        WHT4C.setMinimumSize(new Dimension(75, 30));
        WHT4C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT4C.setMaximumSize(new Dimension(75, 30));
        WHT4C.setName("WHT4C");
        pnlColonneTarif.add(WHT4C, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT4A ----
        WHT4A.setComponentPopupMenu(null);
        WHT4A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT4A.setPreferredSize(new Dimension(75, 30));
        WHT4A.setMinimumSize(new Dimension(75, 30));
        WHT4A.setMaximumSize(new Dimension(75, 30));
        WHT4A.setName("WHT4A");
        pnlColonneTarif.add(WHT4A, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT4B ----
        WHT4B.setComponentPopupMenu(null);
        WHT4B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT4B.setPreferredSize(new Dimension(75, 30));
        WHT4B.setMinimumSize(new Dimension(75, 30));
        WHT4B.setMaximumSize(new Dimension(75, 30));
        WHT4B.setName("WHT4B");
        pnlColonneTarif.add(WHT4B, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT5C ----
        WHT5C.setComponentPopupMenu(null);
        WHT5C.setMinimumSize(new Dimension(75, 30));
        WHT5C.setPreferredSize(new Dimension(75, 30));
        WHT5C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT5C.setMaximumSize(new Dimension(75, 30));
        WHT5C.setName("WHT5C");
        pnlColonneTarif.add(WHT5C, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT5A ----
        WHT5A.setComponentPopupMenu(null);
        WHT5A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT5A.setMinimumSize(new Dimension(75, 30));
        WHT5A.setPreferredSize(new Dimension(75, 30));
        WHT5A.setMaximumSize(new Dimension(75, 30));
        WHT5A.setName("WHT5A");
        pnlColonneTarif.add(WHT5A, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT5B ----
        WHT5B.setComponentPopupMenu(null);
        WHT5B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT5B.setMinimumSize(new Dimension(75, 30));
        WHT5B.setPreferredSize(new Dimension(75, 30));
        WHT5B.setMaximumSize(new Dimension(75, 30));
        WHT5B.setName("WHT5B");
        pnlColonneTarif.add(WHT5B, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WHT6C ----
        WHT6C.setComponentPopupMenu(null);
        WHT6C.setPreferredSize(new Dimension(75, 30));
        WHT6C.setMinimumSize(new Dimension(75, 30));
        WHT6C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT6C.setMaximumSize(new Dimension(75, 30));
        WHT6C.setName("WHT6C");
        pnlColonneTarif.add(WHT6C, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- WHT6A ----
        WHT6A.setComponentPopupMenu(null);
        WHT6A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT6A.setPreferredSize(new Dimension(75, 30));
        WHT6A.setMinimumSize(new Dimension(75, 30));
        WHT6A.setMaximumSize(new Dimension(75, 30));
        WHT6A.setName("WHT6A");
        pnlColonneTarif.add(WHT6A, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- WHT6B ----
        WHT6B.setComponentPopupMenu(null);
        WHT6B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT6B.setPreferredSize(new Dimension(75, 30));
        WHT6B.setMinimumSize(new Dimension(75, 30));
        WHT6B.setMaximumSize(new Dimension(75, 30));
        WHT6B.setName("WHT6B");
        pnlColonneTarif.add(WHT6B, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbPrixTTC ----
        rbPrixTTC.setText("Prix net TTC");
        rbPrixTTC.setToolTipText("Calcul des tarifs par prix toutes taxes comprises");
        rbPrixTTC.setComponentPopupMenu(null);
        rbPrixTTC.setMaximumSize(new Dimension(150, 30));
        rbPrixTTC.setName("rbPrixTTC");
        rbPrixTTC.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            clickBoutonPrixTTC(e);
          }
        });
        pnlColonneTarif.add(rbPrixTTC, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC1C ----
        WTTC1C.setComponentPopupMenu(null);
        WTTC1C.setPreferredSize(new Dimension(75, 30));
        WTTC1C.setMinimumSize(new Dimension(75, 30));
        WTTC1C.setMaximumSize(new Dimension(75, 30));
        WTTC1C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC1C.setName("WTTC1C");
        pnlColonneTarif.add(WTTC1C, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC1A ----
        WTTC1A.setComponentPopupMenu(null);
        WTTC1A.setPreferredSize(new Dimension(75, 30));
        WTTC1A.setMinimumSize(new Dimension(75, 30));
        WTTC1A.setMaximumSize(new Dimension(75, 30));
        WTTC1A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC1A.setName("WTTC1A");
        pnlColonneTarif.add(WTTC1A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC1B ----
        WTTC1B.setComponentPopupMenu(null);
        WTTC1B.setPreferredSize(new Dimension(75, 30));
        WTTC1B.setMinimumSize(new Dimension(75, 30));
        WTTC1B.setMaximumSize(new Dimension(75, 30));
        WTTC1B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC1B.setName("WTTC1B");
        pnlColonneTarif.add(WTTC1B, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC2C ----
        WTTC2C.setComponentPopupMenu(null);
        WTTC2C.setMaximumSize(new Dimension(75, 30));
        WTTC2C.setPreferredSize(new Dimension(75, 30));
        WTTC2C.setMinimumSize(new Dimension(75, 30));
        WTTC2C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC2C.setName("WTTC2C");
        pnlColonneTarif.add(WTTC2C, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC2A ----
        WTTC2A.setComponentPopupMenu(null);
        WTTC2A.setMaximumSize(new Dimension(75, 30));
        WTTC2A.setPreferredSize(new Dimension(75, 30));
        WTTC2A.setMinimumSize(new Dimension(75, 30));
        WTTC2A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC2A.setName("WTTC2A");
        pnlColonneTarif.add(WTTC2A, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC2B ----
        WTTC2B.setComponentPopupMenu(null);
        WTTC2B.setMaximumSize(new Dimension(75, 30));
        WTTC2B.setPreferredSize(new Dimension(75, 30));
        WTTC2B.setMinimumSize(new Dimension(75, 30));
        WTTC2B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC2B.setName("WTTC2B");
        pnlColonneTarif.add(WTTC2B, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC3C ----
        WTTC3C.setComponentPopupMenu(null);
        WTTC3C.setMinimumSize(new Dimension(75, 30));
        WTTC3C.setMaximumSize(new Dimension(75, 30));
        WTTC3C.setPreferredSize(new Dimension(75, 30));
        WTTC3C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC3C.setName("WTTC3C");
        pnlColonneTarif.add(WTTC3C, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC3A ----
        WTTC3A.setComponentPopupMenu(null);
        WTTC3A.setMinimumSize(new Dimension(75, 30));
        WTTC3A.setMaximumSize(new Dimension(75, 30));
        WTTC3A.setPreferredSize(new Dimension(75, 30));
        WTTC3A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC3A.setName("WTTC3A");
        pnlColonneTarif.add(WTTC3A, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC3B ----
        WTTC3B.setComponentPopupMenu(null);
        WTTC3B.setMinimumSize(new Dimension(75, 30));
        WTTC3B.setMaximumSize(new Dimension(75, 30));
        WTTC3B.setPreferredSize(new Dimension(75, 30));
        WTTC3B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC3B.setName("WTTC3B");
        pnlColonneTarif.add(WTTC3B, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC4C ----
        WTTC4C.setComponentPopupMenu(null);
        WTTC4C.setMaximumSize(new Dimension(75, 30));
        WTTC4C.setMinimumSize(new Dimension(75, 30));
        WTTC4C.setPreferredSize(new Dimension(75, 30));
        WTTC4C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC4C.setName("WTTC4C");
        pnlColonneTarif.add(WTTC4C, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC4A ----
        WTTC4A.setComponentPopupMenu(null);
        WTTC4A.setMaximumSize(new Dimension(75, 30));
        WTTC4A.setMinimumSize(new Dimension(75, 30));
        WTTC4A.setPreferredSize(new Dimension(75, 30));
        WTTC4A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC4A.setName("WTTC4A");
        pnlColonneTarif.add(WTTC4A, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC4B ----
        WTTC4B.setComponentPopupMenu(null);
        WTTC4B.setMaximumSize(new Dimension(75, 30));
        WTTC4B.setMinimumSize(new Dimension(75, 30));
        WTTC4B.setPreferredSize(new Dimension(75, 30));
        WTTC4B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC4B.setName("WTTC4B");
        pnlColonneTarif.add(WTTC4B, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC5C ----
        WTTC5C.setComponentPopupMenu(null);
        WTTC5C.setPreferredSize(new Dimension(75, 30));
        WTTC5C.setMinimumSize(new Dimension(75, 30));
        WTTC5C.setMaximumSize(new Dimension(75, 30));
        WTTC5C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC5C.setName("WTTC5C");
        pnlColonneTarif.add(WTTC5C, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC5A ----
        WTTC5A.setComponentPopupMenu(null);
        WTTC5A.setPreferredSize(new Dimension(75, 30));
        WTTC5A.setMinimumSize(new Dimension(75, 30));
        WTTC5A.setMaximumSize(new Dimension(75, 30));
        WTTC5A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC5A.setName("WTTC5A");
        pnlColonneTarif.add(WTTC5A, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC5B ----
        WTTC5B.setComponentPopupMenu(null);
        WTTC5B.setPreferredSize(new Dimension(75, 30));
        WTTC5B.setMinimumSize(new Dimension(75, 30));
        WTTC5B.setMaximumSize(new Dimension(75, 30));
        WTTC5B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC5B.setName("WTTC5B");
        pnlColonneTarif.add(WTTC5B, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WTTC6C ----
        WTTC6C.setComponentPopupMenu(null);
        WTTC6C.setMaximumSize(new Dimension(75, 30));
        WTTC6C.setMinimumSize(new Dimension(75, 30));
        WTTC6C.setPreferredSize(new Dimension(75, 30));
        WTTC6C.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC6C.setName("WTTC6C");
        pnlColonneTarif.add(WTTC6C, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- WTTC6A ----
        WTTC6A.setComponentPopupMenu(null);
        WTTC6A.setMaximumSize(new Dimension(75, 30));
        WTTC6A.setMinimumSize(new Dimension(75, 30));
        WTTC6A.setPreferredSize(new Dimension(75, 30));
        WTTC6A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC6A.setName("WTTC6A");
        pnlColonneTarif.add(WTTC6A, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- WTTC6B ----
        WTTC6B.setComponentPopupMenu(null);
        WTTC6B.setMaximumSize(new Dimension(75, 30));
        WTTC6B.setMinimumSize(new Dimension(75, 30));
        WTTC6B.setPreferredSize(new Dimension(75, 30));
        WTTC6B.setFont(new Font("sansserif", Font.PLAIN, 14));
        WTTC6B.setName("WTTC6B");
        pnlColonneTarif.add(WTTC6B, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbMarge ----
        rbMarge.setText("Marge");
        rbMarge.setToolTipText("Calcul des tarifs par marges");
        rbMarge.setComponentPopupMenu(null);
        rbMarge.setMaximumSize(new Dimension(150, 30));
        rbMarge.setName("rbMarge");
        rbMarge.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            clickBoutonMarge(e);
          }
        });
        pnlColonneTarif.add(rbMarge, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMarge1 ========
        {
          pnlMarge1.setName("pnlMarge1");
          pnlMarge1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMarge1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMarge1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMarge1.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMarge1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WMAR1 ----
          WMAR1.setComponentPopupMenu(null);
          WMAR1.setMaximumSize(new Dimension(55, 30));
          WMAR1.setMinimumSize(new Dimension(55, 30));
          WMAR1.setPreferredSize(new Dimension(55, 30));
          WMAR1.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMAR1.setName("WMAR1");
          pnlMarge1.add(WMAR1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage6 ----
          lbPourcentage6.setText("%");
          lbPourcentage6.setMaximumSize(new Dimension(15, 30));
          lbPourcentage6.setPreferredSize(new Dimension(15, 30));
          lbPourcentage6.setMinimumSize(new Dimension(15, 30));
          lbPourcentage6.setName("lbPourcentage6");
          pnlMarge1.add(lbPourcentage6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlMarge1, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMarge2 ========
        {
          pnlMarge2.setName("pnlMarge2");
          pnlMarge2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMarge2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMarge2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMarge2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMarge2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WMAR2 ----
          WMAR2.setComponentPopupMenu(null);
          WMAR2.setPreferredSize(new Dimension(55, 30));
          WMAR2.setMinimumSize(new Dimension(55, 30));
          WMAR2.setMaximumSize(new Dimension(55, 30));
          WMAR2.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMAR2.setName("WMAR2");
          pnlMarge2.add(WMAR2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage7 ----
          lbPourcentage7.setText("%");
          lbPourcentage7.setMaximumSize(new Dimension(15, 30));
          lbPourcentage7.setMinimumSize(new Dimension(15, 30));
          lbPourcentage7.setPreferredSize(new Dimension(15, 30));
          lbPourcentage7.setName("lbPourcentage7");
          pnlMarge2.add(lbPourcentage7, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlMarge2, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMarge3 ========
        {
          pnlMarge3.setName("pnlMarge3");
          pnlMarge3.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMarge3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMarge3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMarge3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMarge3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WMAR3 ----
          WMAR3.setComponentPopupMenu(null);
          WMAR3.setPreferredSize(new Dimension(55, 30));
          WMAR3.setMinimumSize(new Dimension(55, 30));
          WMAR3.setMaximumSize(new Dimension(55, 30));
          WMAR3.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMAR3.setName("WMAR3");
          pnlMarge3.add(WMAR3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage8 ----
          lbPourcentage8.setText("%");
          lbPourcentage8.setPreferredSize(new Dimension(15, 30));
          lbPourcentage8.setMinimumSize(new Dimension(15, 30));
          lbPourcentage8.setMaximumSize(new Dimension(15, 30));
          lbPourcentage8.setName("lbPourcentage8");
          pnlMarge3.add(lbPourcentage8, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlMarge3, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMarge4 ========
        {
          pnlMarge4.setName("pnlMarge4");
          pnlMarge4.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMarge4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMarge4.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMarge4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMarge4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WMAR4 ----
          WMAR4.setComponentPopupMenu(null);
          WMAR4.setPreferredSize(new Dimension(55, 30));
          WMAR4.setMinimumSize(new Dimension(55, 30));
          WMAR4.setMaximumSize(new Dimension(55, 30));
          WMAR4.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMAR4.setName("WMAR4");
          pnlMarge4.add(WMAR4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage9 ----
          lbPourcentage9.setText("%");
          lbPourcentage9.setComponentPopupMenu(null);
          lbPourcentage9.setPreferredSize(new Dimension(15, 30));
          lbPourcentage9.setMinimumSize(new Dimension(15, 30));
          lbPourcentage9.setMaximumSize(new Dimension(15, 30));
          lbPourcentage9.setName("lbPourcentage9");
          pnlMarge4.add(lbPourcentage9, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlMarge4, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMarge5 ========
        {
          pnlMarge5.setName("pnlMarge5");
          pnlMarge5.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMarge5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMarge5.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMarge5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMarge5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WMAR5 ----
          WMAR5.setComponentPopupMenu(null);
          WMAR5.setPreferredSize(new Dimension(55, 30));
          WMAR5.setMinimumSize(new Dimension(55, 30));
          WMAR5.setMaximumSize(new Dimension(55, 30));
          WMAR5.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMAR5.setName("WMAR5");
          pnlMarge5.add(WMAR5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage10 ----
          lbPourcentage10.setText("%");
          lbPourcentage10.setPreferredSize(new Dimension(15, 30));
          lbPourcentage10.setMinimumSize(new Dimension(15, 30));
          lbPourcentage10.setMaximumSize(new Dimension(15, 30));
          lbPourcentage10.setName("lbPourcentage10");
          pnlMarge5.add(lbPourcentage10, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlMarge5, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMarge6 ========
        {
          pnlMarge6.setName("pnlMarge6");
          pnlMarge6.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMarge6.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMarge6.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMarge6.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMarge6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WMAR6 ----
          WMAR6.setComponentPopupMenu(null);
          WMAR6.setPreferredSize(new Dimension(55, 30));
          WMAR6.setMinimumSize(new Dimension(55, 30));
          WMAR6.setMaximumSize(new Dimension(55, 30));
          WMAR6.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMAR6.setName("WMAR6");
          pnlMarge6.add(WMAR6, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage11 ----
          lbPourcentage11.setText("%");
          lbPourcentage11.setPreferredSize(new Dimension(15, 30));
          lbPourcentage11.setMinimumSize(new Dimension(15, 30));
          lbPourcentage11.setMaximumSize(new Dimension(15, 30));
          lbPourcentage11.setName("lbPourcentage11");
          pnlMarge6.add(lbPourcentage11, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonneTarif.add(pnlMarge6, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlColonneTarif,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlAjustementPrixVente ========
      {
        pnlAjustementPrixVente.setTitre("Ajustement du prix de vente");
        pnlAjustementPrixVente.setName("pnlAjustementPrixVente");
        pnlAjustementPrixVente.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAjustementPrixVente.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlAjustementPrixVente.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlAjustementPrixVente.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlAjustementPrixVente.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbPrixPropose ----
        lbPrixPropose.setText("Prix de vente propos\u00e9");
        lbPrixPropose.setComponentPopupMenu(null);
        lbPrixPropose.setName("lbPrixPropose");
        pnlAjustementPrixVente.add(lbPrixPropose, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVentePropose ----
        tfPrixVentePropose.setPreferredSize(new Dimension(75, 30));
        tfPrixVentePropose.setMinimumSize(new Dimension(75, 30));
        tfPrixVentePropose.setMaximumSize(new Dimension(75, 30));
        tfPrixVentePropose.setName("tfPrixVentePropose");
        pnlAjustementPrixVente.add(tfPrixVentePropose, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVentePropose2 ----
        tfPrixVentePropose2.setPreferredSize(new Dimension(80, 30));
        tfPrixVentePropose2.setMinimumSize(new Dimension(80, 30));
        tfPrixVentePropose2.setName("tfPrixVentePropose2");
        pnlAjustementPrixVente.add(tfPrixVentePropose2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVentePropose3 ----
        tfPrixVentePropose3.setMinimumSize(new Dimension(80, 30));
        tfPrixVentePropose3.setPreferredSize(new Dimension(80, 30));
        tfPrixVentePropose3.setName("tfPrixVentePropose3");
        pnlAjustementPrixVente.add(tfPrixVentePropose3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVentePropose4 ----
        tfPrixVentePropose4.setMinimumSize(new Dimension(80, 30));
        tfPrixVentePropose4.setPreferredSize(new Dimension(80, 30));
        tfPrixVentePropose4.setName("tfPrixVentePropose4");
        pnlAjustementPrixVente.add(tfPrixVentePropose4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVentePropose5 ----
        tfPrixVentePropose5.setMinimumSize(new Dimension(75, 30));
        tfPrixVentePropose5.setPreferredSize(new Dimension(75, 30));
        tfPrixVentePropose5.setMaximumSize(new Dimension(75, 30));
        tfPrixVentePropose5.setName("tfPrixVentePropose5");
        pnlAjustementPrixVente.add(tfPrixVentePropose5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVentePropose6 ----
        tfPrixVentePropose6.setMinimumSize(new Dimension(75, 30));
        tfPrixVentePropose6.setPreferredSize(new Dimension(75, 30));
        tfPrixVentePropose6.setMaximumSize(new Dimension(75, 30));
        tfPrixVentePropose6.setName("tfPrixVentePropose6");
        pnlAjustementPrixVente.add(tfPrixVentePropose6, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPrixCorrige ----
        lbPrixCorrige.setText("Prix de vente corrig\u00e9");
        lbPrixCorrige.setComponentPopupMenu(null);
        lbPrixCorrige.setName("lbPrixCorrige");
        pnlAjustementPrixVente.add(lbPrixCorrige, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVenteCorrige ----
        tfPrixVenteCorrige.setMinimumSize(new Dimension(75, 30));
        tfPrixVenteCorrige.setPreferredSize(new Dimension(75, 30));
        tfPrixVenteCorrige.setMaximumSize(new Dimension(75, 30));
        tfPrixVenteCorrige.setName("tfPrixVenteCorrige");
        pnlAjustementPrixVente.add(tfPrixVenteCorrige, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVenteCorrige2 ----
        tfPrixVenteCorrige2.setPreferredSize(new Dimension(80, 30));
        tfPrixVenteCorrige2.setMinimumSize(new Dimension(80, 30));
        tfPrixVenteCorrige2.setName("tfPrixVenteCorrige2");
        pnlAjustementPrixVente.add(tfPrixVenteCorrige2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVenteCorrige3 ----
        tfPrixVenteCorrige3.setMinimumSize(new Dimension(80, 30));
        tfPrixVenteCorrige3.setPreferredSize(new Dimension(80, 30));
        tfPrixVenteCorrige3.setName("tfPrixVenteCorrige3");
        pnlAjustementPrixVente.add(tfPrixVenteCorrige3, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVenteCorrige4 ----
        tfPrixVenteCorrige4.setPreferredSize(new Dimension(80, 30));
        tfPrixVenteCorrige4.setMinimumSize(new Dimension(80, 30));
        tfPrixVenteCorrige4.setName("tfPrixVenteCorrige4");
        pnlAjustementPrixVente.add(tfPrixVenteCorrige4, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVenteCorrige5 ----
        tfPrixVenteCorrige5.setMinimumSize(new Dimension(75, 30));
        tfPrixVenteCorrige5.setPreferredSize(new Dimension(75, 30));
        tfPrixVenteCorrige5.setMaximumSize(new Dimension(75, 30));
        tfPrixVenteCorrige5.setName("tfPrixVenteCorrige5");
        pnlAjustementPrixVente.add(tfPrixVenteCorrige5, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixVenteCorrige6 ----
        tfPrixVenteCorrige6.setPreferredSize(new Dimension(75, 30));
        tfPrixVenteCorrige6.setMinimumSize(new Dimension(75, 30));
        tfPrixVenteCorrige6.setMaximumSize(new Dimension(75, 30));
        tfPrixVenteCorrige6.setName("tfPrixVenteCorrige6");
        pnlAjustementPrixVente.add(tfPrixVenteCorrige6, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlPourcentageVariation ========
        {
          pnlPourcentageVariation.setName("pnlPourcentageVariation");
          pnlPourcentageVariation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPourcentageVariation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPourcentageVariation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPourcentageVariation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPourcentageVariation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbVariationPrix ----
          lbVariationPrix.setText("% propos\u00e9 de variation du prix de vente");
          lbVariationPrix.setMinimumSize(new Dimension(252, 30));
          lbVariationPrix.setPreferredSize(new Dimension(252, 30));
          lbVariationPrix.setMaximumSize(new Dimension(252, 30));
          lbVariationPrix.setName("lbVariationPrix");
          pnlPourcentageVariation.add(lbVariationPrix, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- P55VR1 ----
          P55VR1.setMaximumSize(new Dimension(100, 30));
          P55VR1.setFont(new Font("sansserif", Font.PLAIN, 14));
          P55VR1.setName("P55VR1");
          pnlPourcentageVariation.add(P55VR1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCorrectionPrix ----
          lbCorrectionPrix.setText("% corrig\u00e9 de variation du prix de vente");
          lbCorrectionPrix.setPreferredSize(new Dimension(244, 30));
          lbCorrectionPrix.setMinimumSize(new Dimension(244, 30));
          lbCorrectionPrix.setMaximumSize(new Dimension(244, 30));
          lbCorrectionPrix.setName("lbCorrectionPrix");
          pnlPourcentageVariation.add(lbCorrectionPrix, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- P55VR2 ----
          P55VR2.setText("@P55VR2@");
          P55VR2.setMaximumSize(new Dimension(100, 30));
          P55VR2.setFont(new Font("sansserif", Font.PLAIN, 14));
          P55VR2.setName("P55VR2");
          pnlPourcentageVariation.add(P55VR2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlAjustementPrixVente.add(pnlPourcentageVariation, new GridBagConstraints(0, 2, 7, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAjustementPrixVente,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
    }
    
    // ---- btgBoutonColonneTarif ----
    ButtonGroup btgBoutonColonneTarif = new ButtonGroup();
    btgBoutonColonneTarif.add(rbIndice);
    btgBoutonColonneTarif.add(rbRemise);
    btgBoutonColonneTarif.add(rbPrixHT);
    btgBoutonColonneTarif.add(rbPrixTTC);
    btgBoutonColonneTarif.add(rbMarge);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlBandeau;
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlArticle;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbDevise;
  private SNPanel pnlDevise;
  private SNTexte tfIDDevise;
  private SNTexte tfLibelleDevise;
  private SNLabelChamp lbPrixRevient;
  private SNPanel pnlPrv;
  private XRiTextField WPRVA;
  private XRiTextField WPRVAS;
  private XRiTextField WPRVC;
  private XRiTextField WPRVCS;
  private SNPanelTitre pnlInfoPrix;
  private SNLabelChamp lbDateApplication;
  private SNPanel pnlDateApplication;
  private SNBouton btGauche;
  private XRiCalendrier WDAPL;
  private SNBouton btDroite;
  private SNLabelChamp lbCreeLe;
  private XRiCalendrier WDCRE;
  private SNLabelChamp lbModifieLe;
  private XRiCalendrier WDMOD;
  private SNLabelChamp lbUniteVente;
  private SNPanel pnlUnite;
  private SNTexte tfIDUniteVente;
  private SNTexte tfLibelleUniteVente;
  private SNLabelChamp lbArrondi;
  private XRiComboBox WRON;
  private SNLabelChamp lbChange;
  private SNTexte tfChange;
  private SNLabelChamp lbSupport;
  private SNTexte tfSupport;
  private SNLabelChamp lbCodeTVA;
  private XRiComboBox WCTVA;
  private XRiCheckBox ckTTC;
  private SNPanelTitre pnlColonneTarif;
  private SNLabelChamp lbPrixPublic;
  private SNLabelChamp lbPrixPublic2;
  private SNLabelChamp lbPrixPublic3;
  private SNLabelChamp lbPrixPublic4;
  private SNLabelChamp lbPrixPublic5;
  private SNLabelChamp lbPrixPublic6;
  private SNRadioButton rbIndice;
  private XRiTextField WCOE1;
  private XRiTextField WCOE2;
  private XRiTextField WCOE3;
  private XRiTextField WCOE4;
  private XRiTextField WCOE5;
  private XRiTextField WCOE6;
  private SNRadioButton rbRemise;
  private SNPanel pnlRemise1;
  private XRiTextField WREM2;
  private SNLabelUnite lbPourcentage;
  private SNPanel pnlRemise2;
  private XRiTextField WREM3;
  private SNLabelUnite lbPourcentage2;
  private SNPanel pnlRemise3;
  private XRiTextField WREM4;
  private SNLabelUnite lbPourcentage3;
  private SNPanel pnlRemise4;
  private XRiTextField WREM5;
  private SNLabelUnite lbPourcentage4;
  private SNPanel pnlRemise5;
  private XRiTextField WREM6;
  private SNLabelUnite lbPourcentage5;
  private SNRadioButton rbPrixHT;
  private XRiTextField WHT1C;
  private XRiTextField WHT1A;
  private XRiTextField WHT1B;
  private XRiTextField WHT2C;
  private XRiTextField WHT2A;
  private XRiTextField WHT2B;
  private XRiTextField WHT3C;
  private XRiTextField WHT3A;
  private XRiTextField WHT3B;
  private XRiTextField WHT4C;
  private XRiTextField WHT4A;
  private XRiTextField WHT4B;
  private XRiTextField WHT5C;
  private XRiTextField WHT5A;
  private XRiTextField WHT5B;
  private XRiTextField WHT6C;
  private XRiTextField WHT6A;
  private XRiTextField WHT6B;
  private SNRadioButton rbPrixTTC;
  private XRiTextField WTTC1C;
  private XRiTextField WTTC1A;
  private XRiTextField WTTC1B;
  private XRiTextField WTTC2C;
  private XRiTextField WTTC2A;
  private XRiTextField WTTC2B;
  private XRiTextField WTTC3C;
  private XRiTextField WTTC3A;
  private XRiTextField WTTC3B;
  private XRiTextField WTTC4C;
  private XRiTextField WTTC4A;
  private XRiTextField WTTC4B;
  private XRiTextField WTTC5C;
  private XRiTextField WTTC5A;
  private XRiTextField WTTC5B;
  private XRiTextField WTTC6C;
  private XRiTextField WTTC6A;
  private XRiTextField WTTC6B;
  private SNRadioButton rbMarge;
  private SNPanel pnlMarge1;
  private XRiTextField WMAR1;
  private SNLabelUnite lbPourcentage6;
  private SNPanel pnlMarge2;
  private XRiTextField WMAR2;
  private SNLabelUnite lbPourcentage7;
  private SNPanel pnlMarge3;
  private XRiTextField WMAR3;
  private SNLabelUnite lbPourcentage8;
  private SNPanel pnlMarge4;
  private XRiTextField WMAR4;
  private SNLabelUnite lbPourcentage9;
  private SNPanel pnlMarge5;
  private XRiTextField WMAR5;
  private SNLabelUnite lbPourcentage10;
  private SNPanel pnlMarge6;
  private XRiTextField WMAR6;
  private SNLabelUnite lbPourcentage11;
  private SNPanelTitre pnlAjustementPrixVente;
  private SNLabelChamp lbPrixPropose;
  private SNTexte tfPrixVentePropose;
  private SNTexte tfPrixVentePropose2;
  private SNTexte tfPrixVentePropose3;
  private SNTexte tfPrixVentePropose4;
  private SNTexte tfPrixVentePropose5;
  private SNTexte tfPrixVentePropose6;
  private SNLabelChamp lbPrixCorrige;
  private SNTexte tfPrixVenteCorrige;
  private SNTexte tfPrixVenteCorrige2;
  private SNTexte tfPrixVenteCorrige3;
  private SNTexte tfPrixVenteCorrige4;
  private SNTexte tfPrixVenteCorrige5;
  private SNTexte tfPrixVenteCorrige6;
  private SNPanel pnlPourcentageVariation;
  private SNLabelChamp lbVariationPrix;
  private XRiTextField P55VR1;
  private SNLabelChamp lbCorrectionPrix;
  private XRiTextField P55VR2;
  private XRiBarreBouton snBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
