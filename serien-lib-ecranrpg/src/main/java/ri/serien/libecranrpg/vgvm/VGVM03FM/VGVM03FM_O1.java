
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_O1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM03FM_O1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    
    OBJ_61.setIcon(lexique.chargerImage("images/fax.png", true));
    OBJ_59.setIcon(lexique.chargerImage("images/tel.png", true));
    
    // Titre
    setTitle("Bloc-notes de la fiche client");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_16_OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM1@")).trim());
    OBJ_22_OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM2@")).trim());
    OBJ_15_OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM17@")).trim());
    OBJ_24_OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM3@")).trim());
    OBJ_26_OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM4@")).trim());
    OBJ_28_OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM5@")).trim());
    OBJ_30_OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM6@")).trim());
    OBJ_32_OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM7@")).trim());
    OBJ_34_OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM8@")).trim());
    OBJ_36_OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM9@")).trim());
    OBJ_38_OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM10@")).trim());
    OBJ_40_OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM11@")).trim());
    OBJ_42_OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM12@")).trim());
    OBJ_44_OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM13@")).trim());
    OBJ_46_OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM14@")).trim());
    OBJ_48_OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM15@")).trim());
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TM16@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean isModif = (!lexique.isTrue("53"));
    navig_valid.setVisible(isModif);
    
    OB185.setEnabled(lexique.isPresent("OB185"));
    OB184.setEnabled(lexique.isPresent("OB184"));
    OB183.setEnabled(lexique.isPresent("OB183"));
    OB182.setEnabled(lexique.isPresent("OB182"));
    OB181.setEnabled(lexique.isPresent("OB181"));
    OBJ_50_OBJ_50.setVisible(lexique.isPresent("TM16"));
    OBJ_48_OBJ_48.setVisible(lexique.isPresent("TM15"));
    OBJ_46_OBJ_46.setVisible(lexique.isPresent("TM14"));
    OBJ_44_OBJ_44.setVisible(lexique.isPresent("TM13"));
    OBJ_42_OBJ_42.setVisible(lexique.isPresent("TM12"));
    OBJ_40_OBJ_40.setVisible(lexique.isPresent("TM11"));
    OBJ_38_OBJ_38.setVisible(lexique.isPresent("TM10"));
    OBJ_36_OBJ_36.setVisible(lexique.isPresent("TM9"));
    OBJ_34_OBJ_34.setVisible(lexique.isPresent("TM8"));
    OBJ_32_OBJ_32.setVisible(lexique.isPresent("TM7"));
    OBJ_30_OBJ_30.setVisible(lexique.isPresent("TM6"));
    OBJ_28_OBJ_28.setVisible(lexique.isPresent("TM5"));
    OBJ_26_OBJ_26.setVisible(lexique.isPresent("TM4"));
    OBJ_24_OBJ_24.setVisible(lexique.isPresent("TM3"));
    OBJ_22_OBJ_22.setVisible(lexique.isPresent("TM2"));
    OBJ_16_OBJ_16.setVisible(lexique.isPresent("TM1"));
    OBJ_15_OBJ_15.setVisible(lexique.isPresent("TM17"));
    CLFAX.setVisible(lexique.isPresent("CLFAX"));
    CLTEL.setVisible(lexique.isPresent("CLTEL"));
    OB17.setEnabled(lexique.isPresent("OB17"));
    OB16.setEnabled(lexique.isPresent("OB16"));
    OB15.setEnabled(lexique.isPresent("OB15"));
    OB14.setEnabled(lexique.isPresent("OB14"));
    OB13.setEnabled(lexique.isPresent("OB13"));
    OB12.setEnabled(lexique.isPresent("OB12"));
    OB11.setEnabled(lexique.isPresent("OB11"));
    OB10.setEnabled(lexique.isPresent("OB10"));
    OB9.setEnabled(lexique.isPresent("OB9"));
    OB8.setEnabled(lexique.isPresent("OB8"));
    OB7.setEnabled(lexique.isPresent("OB7"));
    OB6.setEnabled(lexique.isPresent("OB6"));
    OB5.setEnabled(lexique.isPresent("OB5"));
    OB4.setEnabled(lexique.isPresent("OB4"));
    OB3.setEnabled(lexique.isPresent("OB3"));
    OB2.setEnabled(lexique.isPresent("OB2"));
    OB1.setEnabled(lexique.isPresent("OB1"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    // PcCommand(4, "Numeroteur.exe " +Chr(34)+@"@CLTEL@"+Chr(34)+ " "+Chr(34)+@"@CLNOM@"+Chr(34)+" "+"1 "+
    // @"@&PREFIXE@")
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    OB9 = new XRiTextField();
    OB10 = new XRiTextField();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OB13 = new XRiTextField();
    OB14 = new XRiTextField();
    OB15 = new XRiTextField();
    OB16 = new XRiTextField();
    OB17 = new XRiTextField();
    OBJ_16_OBJ_16 = new JLabel();
    OBJ_65_OBJ_65 = new JLabel();
    OBJ_22_OBJ_22 = new JLabel();
    OBJ_67_OBJ_67 = new JLabel();
    OBJ_15_OBJ_15 = new JLabel();
    OBJ_24_OBJ_24 = new JLabel();
    OBJ_26_OBJ_26 = new JLabel();
    OBJ_28_OBJ_28 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_34_OBJ_34 = new JLabel();
    OBJ_36_OBJ_36 = new JLabel();
    OBJ_38_OBJ_38 = new JLabel();
    OBJ_40_OBJ_40 = new JLabel();
    OBJ_42_OBJ_42 = new JLabel();
    OBJ_44_OBJ_44 = new JLabel();
    OBJ_46_OBJ_46 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_50_OBJ_50 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_69_OBJ_69 = new JLabel();
    OBJ_70_OBJ_70 = new JLabel();
    OBJ_71_OBJ_71 = new JLabel();
    OBJ_72_OBJ_72 = new JLabel();
    OBJ_73_OBJ_73 = new JLabel();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_76_OBJ_76 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_79_OBJ_79 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_81_OBJ_81 = new JLabel();
    OBJ_59 = new JButton();
    CLTEL = new XRiTextField();
    OBJ_61 = new JLabel();
    CLFAX = new XRiTextField();
    panel2 = new JPanel();
    OB181 = new XRiTextField();
    OB182 = new XRiTextField();
    OB183 = new XRiTextField();
    OB184 = new XRiTextField();
    OB185 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(885, 640));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 240));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Bloc-notes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- OB1 ----
          OB1.setName("OB1");

          //---- OB2 ----
          OB2.setName("OB2");

          //---- OB3 ----
          OB3.setName("OB3");

          //---- OB4 ----
          OB4.setName("OB4");

          //---- OB5 ----
          OB5.setName("OB5");

          //---- OB6 ----
          OB6.setName("OB6");

          //---- OB7 ----
          OB7.setName("OB7");

          //---- OB8 ----
          OB8.setName("OB8");

          //---- OB9 ----
          OB9.setName("OB9");

          //---- OB10 ----
          OB10.setName("OB10");

          //---- OB11 ----
          OB11.setName("OB11");

          //---- OB12 ----
          OB12.setName("OB12");

          //---- OB13 ----
          OB13.setName("OB13");

          //---- OB14 ----
          OB14.setName("OB14");

          //---- OB15 ----
          OB15.setName("OB15");

          //---- OB16 ----
          OB16.setName("OB16");

          //---- OB17 ----
          OB17.setName("OB17");

          //---- OBJ_16_OBJ_16 ----
          OBJ_16_OBJ_16.setText("@TM1@");
          OBJ_16_OBJ_16.setName("OBJ_16_OBJ_16");

          //---- OBJ_65_OBJ_65 ----
          OBJ_65_OBJ_65.setText("1");
          OBJ_65_OBJ_65.setName("OBJ_65_OBJ_65");

          //---- OBJ_22_OBJ_22 ----
          OBJ_22_OBJ_22.setText("@TM2@");
          OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");

          //---- OBJ_67_OBJ_67 ----
          OBJ_67_OBJ_67.setText("2");
          OBJ_67_OBJ_67.setName("OBJ_67_OBJ_67");

          //---- OBJ_15_OBJ_15 ----
          OBJ_15_OBJ_15.setText("@TM17@");
          OBJ_15_OBJ_15.setName("OBJ_15_OBJ_15");

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("@TM3@");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");

          //---- OBJ_26_OBJ_26 ----
          OBJ_26_OBJ_26.setText("@TM4@");
          OBJ_26_OBJ_26.setName("OBJ_26_OBJ_26");

          //---- OBJ_28_OBJ_28 ----
          OBJ_28_OBJ_28.setText("@TM5@");
          OBJ_28_OBJ_28.setName("OBJ_28_OBJ_28");

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("@TM6@");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("@TM7@");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("@TM8@");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");

          //---- OBJ_36_OBJ_36 ----
          OBJ_36_OBJ_36.setText("@TM9@");
          OBJ_36_OBJ_36.setName("OBJ_36_OBJ_36");

          //---- OBJ_38_OBJ_38 ----
          OBJ_38_OBJ_38.setText("@TM10@");
          OBJ_38_OBJ_38.setName("OBJ_38_OBJ_38");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("@TM11@");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- OBJ_42_OBJ_42 ----
          OBJ_42_OBJ_42.setText("@TM12@");
          OBJ_42_OBJ_42.setName("OBJ_42_OBJ_42");

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("@TM13@");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("@TM14@");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");

          //---- OBJ_48_OBJ_48 ----
          OBJ_48_OBJ_48.setText("@TM15@");
          OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("@TM16@");
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");

          //---- OBJ_66_OBJ_66 ----
          OBJ_66_OBJ_66.setText("17");
          OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");

          //---- OBJ_68_OBJ_68 ----
          OBJ_68_OBJ_68.setText("3");
          OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");

          //---- OBJ_69_OBJ_69 ----
          OBJ_69_OBJ_69.setText("4");
          OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");

          //---- OBJ_70_OBJ_70 ----
          OBJ_70_OBJ_70.setText("5");
          OBJ_70_OBJ_70.setName("OBJ_70_OBJ_70");

          //---- OBJ_71_OBJ_71 ----
          OBJ_71_OBJ_71.setText("6");
          OBJ_71_OBJ_71.setName("OBJ_71_OBJ_71");

          //---- OBJ_72_OBJ_72 ----
          OBJ_72_OBJ_72.setText("7");
          OBJ_72_OBJ_72.setName("OBJ_72_OBJ_72");

          //---- OBJ_73_OBJ_73 ----
          OBJ_73_OBJ_73.setText("8");
          OBJ_73_OBJ_73.setName("OBJ_73_OBJ_73");

          //---- OBJ_74_OBJ_74 ----
          OBJ_74_OBJ_74.setText("9");
          OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");

          //---- OBJ_75_OBJ_75 ----
          OBJ_75_OBJ_75.setText("10");
          OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");

          //---- OBJ_76_OBJ_76 ----
          OBJ_76_OBJ_76.setText("11");
          OBJ_76_OBJ_76.setName("OBJ_76_OBJ_76");

          //---- OBJ_77_OBJ_77 ----
          OBJ_77_OBJ_77.setText("12");
          OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");

          //---- OBJ_78_OBJ_78 ----
          OBJ_78_OBJ_78.setText("13");
          OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");

          //---- OBJ_79_OBJ_79 ----
          OBJ_79_OBJ_79.setText("14");
          OBJ_79_OBJ_79.setName("OBJ_79_OBJ_79");

          //---- OBJ_80_OBJ_80 ----
          OBJ_80_OBJ_80.setText("15");
          OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");

          //---- OBJ_81_OBJ_81 ----
          OBJ_81_OBJ_81.setText("16");
          OBJ_81_OBJ_81.setName("OBJ_81_OBJ_81");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(OBJ_16_OBJ_16, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_22_OBJ_22, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_24_OBJ_24, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_28_OBJ_28, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_34_OBJ_34, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_15_OBJ_15, GroupLayout.PREFERRED_SIZE, 146, GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_81_OBJ_81, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(OB7, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB3, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB9, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB6, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB2, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB5, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB1, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB8, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB4, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB13, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB15, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB16, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB11, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB12, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB17, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB14, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OB10, GroupLayout.PREFERRED_SIZE, 465, GroupLayout.PREFERRED_SIZE)))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(OBJ_16_OBJ_16, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_22_OBJ_22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_24_OBJ_24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_26_OBJ_26, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_28_OBJ_28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_34_OBJ_34, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_36_OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_38_OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_42_OBJ_42, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_46_OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_48_OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_50_OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_15_OBJ_15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(OBJ_65_OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_67_OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_68_OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_69_OBJ_69, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_70_OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_71_OBJ_71, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_72_OBJ_72, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_73_OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_74_OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_75_OBJ_75, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_76_OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_77_OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_78_OBJ_78, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_79_OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_80_OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_81_OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_66_OBJ_66, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(OB7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(OB3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(200, 200, 200)
                        .addComponent(OB9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(OB6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(OB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(OB5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(175, 175, 175)
                        .addComponent(OB8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(OB4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGap(22, 22, 22)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(OB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(100, 100, 100)
                        .addComponent(OB15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(125, 125, 125)
                        .addComponent(OB16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addComponent(OB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(OB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(150, 150, 150)
                        .addComponent(OB17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel1Layout.createSequentialGroup()
                        .addGap(75, 75, 75)
                        .addComponent(OB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(225, 225, 225)
                    .addComponent(OB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 55, 680, 485);

        //---- OBJ_59 ----
        OBJ_59.setText("");
        OBJ_59.setToolTipText("Num\u00e9ro de t\u00e9l\u00e9phone");
        OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_59.setName("OBJ_59");
        OBJ_59.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_59ActionPerformed(e);
          }
        });
        p_contenu.add(OBJ_59);
        OBJ_59.setBounds(15, 13, 27, 22);

        //---- CLTEL ----
        CLTEL.setToolTipText("Extension de la fiche client");
        CLTEL.setName("CLTEL");
        p_contenu.add(CLTEL);
        CLTEL.setBounds(45, 10, 165, CLTEL.getPreferredSize().height);

        //---- OBJ_61 ----
        OBJ_61.setIcon(new ImageIcon("images/fax.gif"));
        OBJ_61.setToolTipText("Num\u00e9ro de fax");
        OBJ_61.setName("OBJ_61");
        p_contenu.add(OBJ_61);
        OBJ_61.setBounds(230, 13, 27, 22);

        //---- CLFAX ----
        CLFAX.setToolTipText("Num\u00e9ro de fax");
        CLFAX.setName("CLFAX");
        p_contenu.add(CLFAX);
        CLFAX.setBounds(265, 10, 165, CLFAX.getPreferredSize().height);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Articles ou commentaires g\u00e9n\u00e9r\u00e9s en d\u00e9but de bon"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OB181 ----
          OB181.setName("OB181");
          panel2.add(OB181);
          OB181.setBounds(15, 30, 105, OB181.getPreferredSize().height);

          //---- OB182 ----
          OB182.setName("OB182");
          panel2.add(OB182);
          OB182.setBounds(126, 30, 105, OB182.getPreferredSize().height);

          //---- OB183 ----
          OB183.setName("OB183");
          panel2.add(OB183);
          OB183.setBounds(237, 30, 105, OB183.getPreferredSize().height);

          //---- OB184 ----
          OB184.setName("OB184");
          panel2.add(OB184);
          OB184.setBounds(348, 30, 105, OB184.getPreferredSize().height);

          //---- OB185 ----
          OB185.setName("OB185");
          panel2.add(OB185);
          OB185.setBounds(459, 30, 105, OB185.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(15, 550, 582, 75);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private XRiTextField OB9;
  private XRiTextField OB10;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private XRiTextField OB13;
  private XRiTextField OB14;
  private XRiTextField OB15;
  private XRiTextField OB16;
  private XRiTextField OB17;
  private JLabel OBJ_16_OBJ_16;
  private JLabel OBJ_65_OBJ_65;
  private JLabel OBJ_22_OBJ_22;
  private JLabel OBJ_67_OBJ_67;
  private JLabel OBJ_15_OBJ_15;
  private JLabel OBJ_24_OBJ_24;
  private JLabel OBJ_26_OBJ_26;
  private JLabel OBJ_28_OBJ_28;
  private JLabel OBJ_30_OBJ_30;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_34_OBJ_34;
  private JLabel OBJ_36_OBJ_36;
  private JLabel OBJ_38_OBJ_38;
  private JLabel OBJ_40_OBJ_40;
  private JLabel OBJ_42_OBJ_42;
  private JLabel OBJ_44_OBJ_44;
  private JLabel OBJ_46_OBJ_46;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_50_OBJ_50;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_69_OBJ_69;
  private JLabel OBJ_70_OBJ_70;
  private JLabel OBJ_71_OBJ_71;
  private JLabel OBJ_72_OBJ_72;
  private JLabel OBJ_73_OBJ_73;
  private JLabel OBJ_74_OBJ_74;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_76_OBJ_76;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_79_OBJ_79;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_81_OBJ_81;
  private JButton OBJ_59;
  private XRiTextField CLTEL;
  private JLabel OBJ_61;
  private XRiTextField CLFAX;
  private JPanel panel2;
  private XRiTextField OB181;
  private XRiTextField OB182;
  private XRiTextField OB183;
  private XRiTextField OB184;
  private XRiTextField OB185;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
