
package ri.serien.libecranrpg.vgvm.VGVM11XF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11XF_B1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] WTYP_Value = { "", "S", };
  
  /**
   * Constructeur.
   */
  public VGVM11XF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTYP.setValeurs(WTYP_Value, null);
    
    setTitle("Saisie ligne de vente spécial télé-vente");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WUNIT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNIT@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIB@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_32 = new JLabel();
    WTYP = new XRiComboBox();
    OBJ_19 = new JLabel();
    WSAIZ = new XRiTextField();
    WQTEX = new XRiTextField();
    L1ART = new XRiTextField();
    OBJ_30 = new JLabel();
    WUNIT = new RiZoneSortie();
    OBJ_31 = new RiZoneSortie();
    OBJ_29 = new JLabel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Saisie ligne de vente sp\u00e9cial t\u00e9l\u00e9-vente"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_32 ----
          OBJ_32.setText("Type");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(26, 40, 35, 20);

          //---- WTYP ----
          WTYP.setModel(new DefaultComboBoxModel(new String[] {
            "Saisie Quantit\u00e9 Code article",
            "Saisie Quantit\u00e9 d\u00e9signation article"
          }));
          WTYP.setComponentPopupMenu(BTD);
          WTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTYP.setName("WTYP");
          panel1.add(WTYP);
          WTYP.setBounds(80, 37, 255, WTYP.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Quantit\u00e9/ Article");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(27, 72, 96, 20);

          //---- WSAIZ ----
          WSAIZ.setComponentPopupMenu(BTD);
          WSAIZ.setName("WSAIZ");
          panel1.add(WSAIZ);
          WSAIZ.setBounds(25, 95, 310, WSAIZ.getPreferredSize().height);

          //---- WQTEX ----
          WQTEX.setComponentPopupMenu(BTD);
          WQTEX.setName("WQTEX");
          panel1.add(WQTEX);
          WQTEX.setBounds(25, 125, 90, WQTEX.getPreferredSize().height);

          //---- L1ART ----
          L1ART.setComponentPopupMenu(BTD);
          L1ART.setName("L1ART");
          panel1.add(L1ART);
          L1ART.setBounds(120, 125, 215, L1ART.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("Unit\u00e9");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(27, 159, 34, 20);

          //---- WUNIT ----
          WUNIT.setText("@WUNIT@");
          WUNIT.setName("WUNIT");
          panel1.add(WUNIT);
          WUNIT.setBounds(90, 157, 25, WUNIT.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("@UNLIB@");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(122, 157, 210, OBJ_31.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("D\u00e9signations");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(367, 72, 100, 20);

          //---- A1LIB ----
          A1LIB.setText("@A1LIB@");
          A1LIB.setName("A1LIB");
          panel1.add(A1LIB);
          A1LIB.setBounds(365, 97, 231, A1LIB.getPreferredSize().height);

          //---- A1LB1 ----
          A1LB1.setText("@A1LB1@");
          A1LB1.setName("A1LB1");
          panel1.add(A1LB1);
          A1LB1.setBounds(365, 127, 231, A1LB1.getPreferredSize().height);

          //---- A1LB2 ----
          A1LB2.setText("@A1LB2@");
          A1LB2.setName("A1LB2");
          panel1.add(A1LB2);
          A1LB2.setBounds(365, 157, 231, A1LB2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_32;
  private XRiComboBox WTYP;
  private JLabel OBJ_19;
  private XRiTextField WSAIZ;
  private XRiTextField WQTEX;
  private XRiTextField L1ART;
  private JLabel OBJ_30;
  private RiZoneSortie WUNIT;
  private RiZoneSortie OBJ_31;
  private JLabel OBJ_29;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
