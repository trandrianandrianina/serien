
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class UN_SUSPECT extends SNPanelEcranRPG implements ioFrame {
   
  
  public UN_SUSPECT(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Quantités suspectes pour alerte en saisie de vente"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    UNQSP1 = new XRiTextField();
    UNQSP2 = new XRiTextField();
    UNQSP3 = new XRiTextField();
    UNQSP4 = new XRiTextField();
    UNQSP5 = new XRiTextField();
    UNQSP6 = new XRiTextField();
    UNQSP7 = new XRiTextField();
    UNQSP8 = new XRiTextField();
    UNQSP9 = new XRiTextField();
    UNQSP10 = new XRiTextField();
    label6 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(390, 400));
    setPreferredSize(new Dimension(390, 400));
    setMaximumSize(new Dimension(390, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
    }
    add(p_menus, BorderLayout.EAST);

    //======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("quantit\u00e9s suspectes"));
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- label1 ----
        label1.setName("label1");
        panel1.add(label1);
        label1.setBounds(35, 40, 25, label1.getPreferredSize().height);

        //---- label2 ----
        label2.setName("label2");
        panel1.add(label2);
        label2.setBounds(35, 76, 25, label2.getPreferredSize().height);

        //---- label3 ----
        label3.setName("label3");
        panel1.add(label3);
        label3.setBounds(35, 111, 25, label3.getPreferredSize().height);

        //---- label4 ----
        label4.setName("label4");
        panel1.add(label4);
        label4.setBounds(35, 146, 25, label4.getPreferredSize().height);

        //---- label5 ----
        label5.setName("label5");
        panel1.add(label5);
        label5.setBounds(35, 181, 25, label5.getPreferredSize().height);

        //---- UNQSP1 ----
        UNQSP1.setName("UNQSP1");
        panel1.add(UNQSP1);
        UNQSP1.setBounds(90, 45, 60, UNQSP1.getPreferredSize().height);

        //---- UNQSP2 ----
        UNQSP2.setName("UNQSP2");
        panel1.add(UNQSP2);
        UNQSP2.setBounds(90, 75, 60, UNQSP2.getPreferredSize().height);

        //---- UNQSP3 ----
        UNQSP3.setName("UNQSP3");
        panel1.add(UNQSP3);
        UNQSP3.setBounds(90, 105, 60, UNQSP3.getPreferredSize().height);

        //---- UNQSP4 ----
        UNQSP4.setName("UNQSP4");
        panel1.add(UNQSP4);
        UNQSP4.setBounds(90, 135, 60, UNQSP4.getPreferredSize().height);

        //---- UNQSP5 ----
        UNQSP5.setName("UNQSP5");
        panel1.add(UNQSP5);
        UNQSP5.setBounds(90, 165, 60, UNQSP5.getPreferredSize().height);

        //---- UNQSP6 ----
        UNQSP6.setName("UNQSP6");
        panel1.add(UNQSP6);
        UNQSP6.setBounds(90, 195, 60, UNQSP6.getPreferredSize().height);

        //---- UNQSP7 ----
        UNQSP7.setName("UNQSP7");
        panel1.add(UNQSP7);
        UNQSP7.setBounds(90, 225, 60, UNQSP7.getPreferredSize().height);

        //---- UNQSP8 ----
        UNQSP8.setName("UNQSP8");
        panel1.add(UNQSP8);
        UNQSP8.setBounds(90, 255, 60, UNQSP8.getPreferredSize().height);

        //---- UNQSP9 ----
        UNQSP9.setName("UNQSP9");
        panel1.add(UNQSP9);
        UNQSP9.setBounds(90, 285, 60, UNQSP9.getPreferredSize().height);

        //---- UNQSP10 ----
        UNQSP10.setName("UNQSP10");
        panel1.add(UNQSP10);
        UNQSP10.setBounds(90, 315, 60, UNQSP10.getPreferredSize().height);

        //---- label6 ----
        label6.setText("maximum");
        label6.setName("label6");
        panel1.add(label6);
        label6.setBounds(20, 315, 70, 28);
      }
      P_Centre.add(panel1);
      panel1.setBounds(15, 15, 190, 370);
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel P_Centre;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private XRiTextField UNQSP1;
  private XRiTextField UNQSP2;
  private XRiTextField UNQSP3;
  private XRiTextField UNQSP4;
  private XRiTextField UNQSP5;
  private XRiTextField UNQSP6;
  private XRiTextField UNQSP7;
  private XRiTextField UNQSP8;
  private XRiTextField UNQSP9;
  private XRiTextField UNQSP10;
  private JLabel label6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
