
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snjournal.SNJournal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_C2 extends SNPanelEcranRPG implements ioFrame {
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  private boolean isConsultation = false;
  private String[] CVCPC_Value = { "J", "S", "D", "Q", "M", };
  private String[] CVREF_Value = { "", "1", "2", "3", "4", "5", "6", "7" };
  private String[] _HLD01_Title = { "Plan comptable", "Co", "HLD01", };
  private String[][] _HLD01_Data = { { "N° CO Vtes taxables", "CVC01", "LCG01", }, { "N° CO TVA collectée", "CVC10", "LCG10", } };
  private int[] _HLD01_Width = { 400, 60, 1500 };
  // private String[] _LISTCV_Top=null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.LEFT };
  
  public VGVM01FX_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    HLD01.setAspectTable(null, _HLD01_Title, _HLD01_Data, _HLD01_Width, true, _LIST_Justification, null, null, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    // Initialisation du composant journal
    snJournal.setSession(getSession());
    snJournal.setIdEtablissement(snEtablissement.getIdSelection());
    snJournal.charger(false);
    snJournal.setSelectionParChampRPG(lexique, "CVCJV");
    snJournal.setEnabled(!isConsultation);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    snJournal.renseignerChampRPG(lexique, "CVCJV");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * 
   * Visibilité des boutons.
   *
   */
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    p_sud = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    pnlOptions = new SNPanel();
    lbJournal = new SNLabelChamp();
    snJournal = new SNJournal();
    lbCompteCollectif = new SNLabelChamp();
    CVCOL = new XRiTextField();
    SCROLLPANE_LISTCV = new JScrollPane();
    HLD01 = new XRiTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1170, 660));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation
          .setText("Personnalisation de la gestion des ventes : Comptabilisation des ventes (Pays de l'Union Europ\u00e9enne)");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      p_sud.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlPersonnalisation ========
        {
          pnlPersonnalisation.setName("pnlPersonnalisation");
          pnlPersonnalisation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCodeCategorie ----
          lbCodeCategorie.setText("code");
          lbCodeCategorie.setName("lbCodeCategorie");
          pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- INDIND ----
          INDIND.setPreferredSize(new Dimension(40, 30));
          INDIND.setMinimumSize(new Dimension(40, 30));
          INDIND.setMaximumSize(new Dimension(40, 30));
          INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDIND.setName("INDIND");
          pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlPersonnalisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlOptions ========
        {
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlOptions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlOptions.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlOptions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbJournal ----
          lbJournal.setText("Code journal des ventes");
          lbJournal.setMaximumSize(new Dimension(200, 30));
          lbJournal.setMinimumSize(new Dimension(200, 30));
          lbJournal.setPreferredSize(new Dimension(200, 30));
          lbJournal.setName("lbJournal");
          pnlOptions.add(lbJournal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snJournal ----
          snJournal.setMaximumSize(new Dimension(400, 30));
          snJournal.setMinimumSize(new Dimension(400, 30));
          snJournal.setPreferredSize(new Dimension(400, 30));
          snJournal.setName("snJournal");
          pnlOptions.add(snJournal, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCompteCollectif ----
          lbCompteCollectif.setText("Compte collectif clients");
          lbCompteCollectif.setName("lbCompteCollectif");
          pnlOptions.add(lbCompteCollectif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CVCOL ----
          CVCOL.setComponentPopupMenu(null);
          CVCOL.setFont(new Font("sansserif", Font.PLAIN, 14));
          CVCOL.setMaximumSize(new Dimension(70, 30));
          CVCOL.setMinimumSize(new Dimension(70, 30));
          CVCOL.setPreferredSize(new Dimension(70, 30));
          CVCOL.setName("CVCOL");
          pnlOptions.add(CVCOL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlOptions, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== SCROLLPANE_LISTCV ========
        {
          SCROLLPANE_LISTCV.setMinimumSize(new Dimension(150, 60));
          SCROLLPANE_LISTCV.setPreferredSize(new Dimension(150, 60));
          SCROLLPANE_LISTCV.setMaximumSize(new Dimension(150, 80));
          SCROLLPANE_LISTCV.setName("SCROLLPANE_LISTCV");
          
          // ---- HLD01 ----
          HLD01.setComponentPopupMenu(null);
          HLD01.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          HLD01.setName("HLD01");
          SCROLLPANE_LISTCV.setViewportView(HLD01);
        }
        pnlContenu.add(SCROLLPANE_LISTCV, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel p_sud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNPanel pnlOptions;
  private SNLabelChamp lbJournal;
  private SNJournal snJournal;
  private SNLabelChamp lbCompteCollectif;
  private XRiTextField CVCOL;
  private JScrollPane SCROLLPANE_LISTCV;
  private XRiTable HLD01;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
