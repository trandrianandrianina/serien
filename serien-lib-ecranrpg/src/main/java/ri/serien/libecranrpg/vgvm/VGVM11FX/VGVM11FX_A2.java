
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JLayeredPane;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.ModeleListeDocumentStocke;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.VueDocumentStocke;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_A2 extends SNPanelEcranRPG implements ioFrame {
  // Constantes
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  
  // Variables
  private String[] E1TRC_Value = { "", "1", "N", "G", };
  private String[] E1PGC_Value = { "1", "", "2", "3", };
  private String[] E1IN9_Value = { "", "1", "2" };
  private String[] E1TAR_Value = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  
  private boolean isConsult = false;
  private int valeurGBA = 0;
  private RiPanelNav riPanelNav1 = null;
  public boolean isChantier;
  public boolean isEnModeRegl = false;
  private ODialog dialog_REGL = null;
  public ODialog dialog_GA = null;
  private Icon bloc_couleur = null;
  private String libelleDirectOuInterne = "";
  
  /**
   * Constructeur.
   */
  public VGVM11FX_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    p_encours.setRightDecoration(bt_encours);
    p_reglement.setRightDecoration(bt_reglement);
    p_condition.setRightDecoration(bt_condition);
    
    E2NOM.activerModeFantome("Nom ou raison sociale");
    E2CPL.activerModeFantome("Complément de nom");
    E2RUE.activerModeFantome("Rue");
    E2LOC.activerModeFantome("Localité");
    E2CDPX.activerModeFantome("00000");
    E2VILN.activerModeFantome("Ville");
    E2PAYN.activerModeFantome("Pays");
    E2TEL.activerModeFantome("Téléphone");
    
    E3NOM.activerModeFantome("Nom ou raison sociale");
    E3CPL.activerModeFantome("Complément de nom");
    E3RUE.activerModeFantome("Rue");
    E3LOC.activerModeFantome("Localité");
    E3CDPX.activerModeFantome("00000");
    E3VILN.activerModeFantome("Ville");
    E3PAYN.activerModeFantome("Pays");
    E3TEL.activerModeFantome("Téléphone");
    
    initDiverses();
    E1PGC.setValeurs(E1PGC_Value, null);
    E1IN9.setValeurs(E1IN9_Value, null);
    E1TRC.setValeurs(E1TRC_Value, null);
    E1TAR.setValeurs(E1TAR_Value); // Tarifs
    TE1IN17.setValeursSelection("X", "");
    
    // Navigation graphique
    riPanelNav1 = new RiPanelNav();
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de bon", true, null);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du bon", false, bouton_valider);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de bon", false, button1);
    riSousMenu18.add(riPanelNav1);
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/reduire.png", true));
    OBJ_153.setIcon(lexique.chargerImage("images/aeuro.gif", true));
    OBJ_157.setIcon(lexique.chargerImage("images/impaye_2.png", true));
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    button2.setIcon(bloc_couleur);
    button3.setIcon(bloc_couleur);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@ facturation @TFLIB@   @WLIEN@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_192.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE1@")).trim());
    OBJ_193.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE2@")).trim());
    OBJ_194.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE3@")).trim());
    OBJ_195.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE4@")).trim());
    OBJ_196.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE5@")).trim());
    E1CAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CAN@")).trim());
    RPLI1G.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLI1G@")).trim());
    WOBS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WOBS@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    WEPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS@")).trim());
    WEPLF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
    OBJ_169.setToolTipText(
        lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Choix d'une date grâce<BR>@&L000285@</HTML>")).trim());
    E1HRE1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HRE1@")).trim());
    E1HRE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HRE2@")).trim());
    OBJ_245.setToolTipText(
        lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Choix d'une date grâce<BR>@&L000285@</HTML>")).trim());
    OBJ_227.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
    OBJ_225.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIBR@")).trim());
    WMFRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMFRP@")).trim());
    E1IN7.setToolTipText(
        lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Caractère de sélection du bon<BR>@&L000985@</HTML>")).trim());
    TOLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOLIB@")).trim());
    WCNP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCNP@")).trim());
    LRG12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRG12@")).trim());
    OBJ_123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRG1@")).trim());
    lib_Devise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    E1CLFP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLFP@")).trim());
    WCAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCAT@")).trim());
    E1CLFS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLFS@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WREPAC@")).trim());
    E1CLLP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLP@")).trim());
    E1CLLS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLS@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATN@")).trim());
    panel1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    LMAIL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPAC@")).trim());
    LMAIL2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    LFAX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LTEL@")).trim());
    LFAX2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    zs_LAVOIR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAVOIR@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPE@")).trim());
    OBJ_117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BONN@")).trim());
    OBJ_164.setToolTipText(
        lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Choix d'une date grâce<BR>@&L000285@</HTML>")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isChantier = lexique.HostFieldGetData("EXISTCH").trim().equals("1");
    boolean isConsult = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    
    riSousMenu10.setEnabled(lexique.isTrue("46"));
    riSousMenu19.setEnabled(isChantier);
    
    p_encours.setVisible(lexique.isTrue("(N38) AND (N39)"));
    
    // Modes d'affichages consult et modif
    riBoutonDetail1.setVisible(!isConsult);
    riBoutonDetail3.setVisible(!isConsult && !WNUM.getText().trim().isEmpty() && lexique.HostFieldGetData("GBAX").trim().equals("G"));
    riBoutonDetail2.setVisible(!isConsult);
    riBoutonDetail6.setVisible(!isConsult);
    bt_condition.setEnabled(!isConsult);
    OBJ_240.setEnabled(!isConsult);
    E1PGC.setEnabled(!isConsult);
    OBJ_21.setVisible(!isConsult);
    OBJ_12.setVisible(!isConsult);
    riSousMenu9.setEnabled(!isConsult);
    riSousMenu11.setEnabled(!isConsult);
    riSousMenu12.setEnabled(!isConsult);
    riSousMenu3.setEnabled(!isConsult);
    riSousMenu1.setEnabled(!isConsult);
    OBJ_272.setVisible(E1VEH.isVisible());
    OBJ_271.setVisible(E1ZTR.isVisible());
    riSousMenu2.setEnabled(!isCreation);
    riSousMenu_bt3.setVisible(!isCreation);
    
    // Barre entête
    E1NFA.setVisible(!lexique.HostFieldGetData("E1NFA").trim().isEmpty());
    OBJ_50.setVisible(E1NFA.isVisible());
    
    // Bloc client
    WCAT_.setText(WCAT.getText());
    
    // Zones personnalisées
    E1TP1.setVisible(!lexique.HostFieldGetData("TIZPE1").trim().isEmpty());
    E1TP2.setVisible(!lexique.HostFieldGetData("TIZPE2").trim().isEmpty());
    E1TP3.setVisible(!lexique.HostFieldGetData("TIZPE3").trim().isEmpty());
    E1TP4.setVisible(!lexique.HostFieldGetData("TIZPE4").trim().isEmpty());
    E1TP5.setVisible(!lexique.HostFieldGetData("TIZPE5").trim().isEmpty());
    
    // Bloc encours
    bt_encours.setEnabled(!lexique.isTrue("46"));
    label4.setVisible(true);
    
    WEDEP.setVisible(lexique.isTrue("(N07) AND (N38)"));
    
    // Dépassement ou disponible (changement de la couleur, le libellé ne clignote plus comme auparavant)
    if (lexique.isTrue("(N08)")) {
      lb_depassement.setForeground(Color.GRAY);
    }
    else {
      lb_depassement.setForeground(Color.BLACK);
      lb_depassement.setText("Disponible");
    }
    
    // Bloc réglements
    lib_Devise.setVisible(lexique.isTrue("N86"));
    E1CHGX.setVisible(lib_Devise.isVisible());
    E1BAS.setVisible(lib_Devise.isVisible());
    label16.setVisible(lib_Devise.isVisible());
    label17.setVisible(lib_Devise.isVisible());
    OBJ_123.setVisible(lib_Devise.isVisible());
    
    // Panneau impayé
    if (lexique.HostFieldGetData("WF4ENC").equalsIgnoreCase("I")) {
      p_impaye.setVisible(true);
      layeredPane1.setComponentZOrder(p_impaye, 0);
    }
    else {
      p_impaye.setVisible(false);
    }
    
    // Label état du bon
    riSousMenu3.setEnabled(false);
    riSousMenu6.setEnabled(true);
    riSousMenu7.setEnabled(true);
    riSousMenu10.setEnabled(true);
    riSousMenu13.setEnabled(true);
    bouton_retour.setToolTipText("Mettre ce bon en attente");
    String etatBon = lexique.HostFieldGetData("ETABON").trim().toUpperCase();
    if (etatBon.isEmpty()) {
      OBJ_129.setVisible(false);
    }
    else {
      OBJ_129.setVisible(true);
      OBJ_129.setText("");
      
      if (etatBon.equals("N")) {
        OBJ_129.setText("Annulé");
        // Si bon annulé, ne pas afficher certains boutons
        riSousMenu6.setEnabled(false);
        riSousMenu7.setEnabled(false);
        riSousMenu10.setEnabled(false);
        riSousMenu13.setEnabled(false);
      }
      if (etatBon.equals("C")) {
        OBJ_129.setText("Comptabilisé");
      }
      else if (etatBon.equals("T")) {
        OBJ_129.setText("Commande type");
      }
      else if (etatBon.equals("P")) {
        OBJ_129.setText("Positionnement");
      }
      else if (etatBon.equals("A")) {
        OBJ_129.setText("Attente");
        riSousMenu3.setEnabled(true);
      }
      else if (etatBon.equals("E")) {
        OBJ_129.setText("Expédié");
      }
      else if (etatBon.equals("H")) {
        OBJ_129.setText("Validé");
      }
      else if (etatBon.equals("R")) {
        OBJ_129.setText("Réservé");
      }
      else if (etatBon.equals("F")) {
        OBJ_129.setText("Facturé");
        bouton_retour.setToolTipText("Mettre cette facture en attente");
      }
    }
    
    // Bouton de génération et son label de livraison
    String texteBouton = "Aucune";
    String texteLabel = "";
    String texteLibelle = "Génération d'achat:";
    
    String e1gba = lexique.HostFieldGetData("E1GBA").trim();
    if (!e1gba.isEmpty()) {
      valeurGBA = Integer.parseInt(e1gba);
      OBJ_240.setEnabled(true);
      switch (valeurGBA) {
        case 1:
          texteBouton = "Différée";
          break;
        case 2:
          texteBouton = "Différée";
          texteLabel = "Livraison directe assurée";
          break;
        case 3:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 4:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 5:
          texteBouton = "Immédiate";
          break;
        case 6:
          texteBouton = "Immédiate";
          texteLabel = "Livraison directe assurée";
          break;
        case 7:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 8:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 9:
          texteBouton = "Par ligne";
          texteLabel = "";
          OBJ_240.setEnabled(false);
          texteLibelle = "";
          break;
        default:
          texteBouton = "Aucune";
          break;
      }
    }
    OBJ_240.setText(texteBouton);
    label11.setText(texteLabel);
    label12.setText(texteLibelle);
    
    zs_LAVOIR.setVisible(!lexique.HostFieldGetData("LAVOIR").trim().isEmpty());
    
    riBoutonDetail7.setVisible(lexique.isPresent("WREPAC"));
    OBJ_145.setVisible(lexique.isPresent("E1CCT"));
    
    OBJ_240.setVisible(lexique.HostFieldGetData("GBA").isEmpty());
    WMFRP.setVisible(true);
    E1CCT.setVisible(lexique.isPresent("E1CCT"));
    E2TEL.setEnabled(lexique.isPresent("E2TEL"));
    E3TEL.setEnabled(lexique.isPresent("E3TEL"));
    OBJ_116.setVisible(lexique.HostFieldGetData("WFCHB").equals("1"));
    
    riBoutonDetail5.setVisible(lexique.HostFieldGetData("WDLPM").equals("1"));
    riBoutonDetail4.setVisible(lexique.HostFieldGetData("WDLPM").equals("1"));
    
    OBJ_225.setVisible(lexique.isPresent("EXLIBR"));
    OBJ_60.setVisible(!lexique.HostFieldGetData("WATN").trim().isEmpty());
    OBJ_227.setVisible(lexique.isPresent("TRLIB"));
    OBJ_102.setVisible(lexique.isPresent("WREPAC"));
    WOBS.setEnabled(lexique.isPresent("WOBS"));
    E2VILN.setVisible(lexique.isPresent("E2VILN"));
    E3VILN.setEnabled(lexique.isPresent("E3VILN"));
    E2LOC_doublon_35.setVisible(lexique.isPresent("E1PFC"));
    LIVRE.setVisible(lexique.isPresent("E2NOM"));
    OBJ_163.setVisible(WEPLF.isVisible());
    OBJ_271.setVisible(E1ZTR.isVisible());
    
    // Tournées
    if (lexique.HostFieldGetData("TOPCRT").equals("1")) {
      label18.setVisible(true);
      E1CRT.setVisible(true);
    }
    else {
      label18.setVisible(false);
      E1CRT.setVisible(false);
    }
    
    int numClientLivre = 0;
    String e1cllp = lexique.HostFieldGetData("E1CLLP").trim();
    if (!e1cllp.isEmpty()) {
      numClientLivre = Integer.parseInt(e1cllp);
    }
    
    if (numClientLivre > 999900) {
      riSousMenu_bt2.setToolTipText("Rechercher ou créer un client en compte");
      riSousMenu_bt2.setText("Client en compte");
    }
    else {
      riSousMenu_bt2.setToolTipText("Changement de client");
      riSousMenu_bt2.setText("Changement de client");
    }
    
    // Problème de rafraichissement des onglets
    OBJ_65.setSelectedComponent(LIVRE);
    OBJ_65.setSelectedComponent(FACTUR);
    // Gestion des onglets mémorisés
    if (lexique.getValeurVariableGlobale("ongletEnCours_VGVM11FX_A2") != null) {
      OBJ_65.setSelectedIndex((Integer) lexique.getValeurVariableGlobale("ongletEnCours_VGVM11FX_A2"));
    }
    
    String wchant = Constantes.normerTexte(lexique.HostFieldGetData("WCHANT"));
    panelChantier.setVisible(!wchant.isEmpty() && !wchant.equals("0"));
    
    OBJ_86.setVisible(isChantier);
    riBoutonDetailListe1.setVisible(isChantier);
    
    // Bandeau du panel
    // Capitalisation du contenu de la variable typ11 (à voir plus tard - il existe déjà une méthode dans Lexical)
    String chaineMaj = null;
    String typ11 = lexique.HostFieldGetData("TYP11");
    if (typ11 != null && !typ11.trim().isEmpty()) {
      chaineMaj = typ11.trim().toLowerCase().replaceFirst(".", (typ11.charAt(0) + "").toUpperCase());
    }
    if (isChantier) {
      p_bpresentation.setText(
          typ11 + " " + lexique.HostFieldGetData("WLIEN").trim() + " (facturation " + lexique.HostFieldGetData("TFLIB").trim() + ")");
    }
    else {
      p_bpresentation.setText(
          chaineMaj + " " + lexique.HostFieldGetData("WLIEN").trim() + " (facturation " + lexique.HostFieldGetData("TFLIB").trim() + ")");
    }
    String e1in18 = lexique.HostFieldGetData("E1IN18").trim();
    if (e1in18.equals("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (e1in18.equals("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText().replaceAll(":", "").trim() + libelleDirectOuInterne);
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    lexique.addVariableGlobale("ongletEnCours_VGVM11FX_A2", OBJ_65.getSelectedIndex());
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_240ActionPerformed(ActionEvent e) {
    if (dialog_GA == null) {
      dialog_GA = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_GA(this));
    }
    dialog_GA.affichePopupPerso();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 33);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06F4", 0, "MAJ");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    /*lexique.HostFieldPutData("V06F4", 0, "CC");
    lexique.HostScreenSendKey(this, "ENTER");*/
    lexique.HostCursorPut("E1CC1");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WCSC");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    // lexique.HostCursorPut("LMAIL");
    lexique.HostCursorPut(24, 10);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    // lexique.HostCursorPut("E3NOM");
    lexique.HostCursorPut(4, 2);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    // lexique.HostCursorPut("E2NOM");
    lexique.HostCursorPut(4, 33);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("GBAX", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetail4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WDLSM", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetail5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WDLPM", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_encoursActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 80);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail6ActionPerformed(ActionEvent e) {
    if (dialog_REGL == null) {
      dialog_REGL = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_REGL(this));
    }
    dialog_REGL.affichePopupPerso();
  }
  
  private void bt_conditionActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 42);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riBoutonDet6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("OADLIV", 0, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetail7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 56);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 10);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E3NOM");
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riBoutonDetail8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1CLFP");
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 43);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 43);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1RG2");
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E2NOM");
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 1);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void afficheDocumentActionPerformed(ActionEvent e) {
    CritereDocumentStocke critere = new CritereDocumentStocke();
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    critere.setIdEtablissement(idEtablissement);
    if (lexique.HostFieldGetData("E1NFA").trim().isEmpty()) {
      throw new MessageErreurException("Veuillez créer la commande de vente avant d'afficher les documents stockés.");
    }
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstancePourFacture(idEtablissement, Integer.parseInt(lexique.HostFieldGetData("E1NFA")));
    critere.setIdDocumentVente(idDocumentVente);
    ModeleListeDocumentStocke modele = new ModeleListeDocumentStocke(getSession(), critere);
    VueDocumentStocke vue = new VueDocumentStocke(modele);
    vue.afficher();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    E1NFA = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    WSUF = new RiZoneSortie();
    WNUM = new RiZoneSortie();
    OBJ_54 = new RiZoneSortie();
    label15 = new JLabel();
    E1VDE = new XRiTextField();
    OBJ_61 = new JLabel();
    E1PRE = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_50 = new JLabel();
    p_tete_droite = new JPanel();
    bt_ecran = new JLabel();
    OBJ_129 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    E1RCC = new XRiTextField();
    E1CCT = new XRiTextField();
    OBJ_145 = new JLabel();
    E1NCC = new XRiTextField();
    E1TP1 = new XRiTextField();
    E1TP2 = new XRiTextField();
    E1TP3 = new XRiTextField();
    E1TP4 = new XRiTextField();
    E1TP5 = new XRiTextField();
    OBJ_192 = new JLabel();
    OBJ_193 = new JLabel();
    OBJ_194 = new JLabel();
    OBJ_195 = new JLabel();
    OBJ_196 = new JLabel();
    OBJ_210 = new JLabel();
    E1SAN = new XRiTextField();
    E1ACT = new XRiTextField();
    OBJ_212 = new JLabel();
    E20NAT = new XRiTextField();
    OBJ_214 = new JLabel();
    OBJ_84 = new JLabel();
    E1CAN = new RiZoneSortie();
    label3 = new JLabel();
    RPLI1G = new RiZoneSortie();
    E1REP = new XRiTextField();
    E1TRP = new XRiTextField();
    label5 = new JLabel();
    WOBS = new RiZoneSortie();
    OBJ_257 = new JLabel();
    E1NEX = new XRiTextField();
    label6 = new JLabel();
    E1MAG = new XRiTextField();
    label7 = new RiZoneSortie();
    riBoutonDetail2 = new SNBoutonDetail();
    panelChantier = new JPanel();
    label14 = new JLabel();
    WCHANT = new XRiTextField();
    lbrefCourte = new JLabel();
    lbrefLongue = new JLabel();
    p_encours = new JXTitledPanel();
    layeredPane1 = new JLayeredPane();
    p_impaye = new JPanel();
    OBJ_157 = new JLabel();
    p_paye = new JPanel();
    lb_depassement = new SNLabelChamp();
    WEPOS = new RiZoneSortie();
    WEPLF = new RiZoneSortie();
    WEDEP = new RiZoneSortie();
    OBJ_162 = new JLabel();
    OBJ_163 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    E1DLSX = new XRiCalendrier();
    E1DLPX = new XRiCalendrier();
    OBJ_169 = new JLabel();
    OBJ_161 = new JLabel();
    E1DCOX = new XRiCalendrier();
    E1HRE1 = new RiZoneSortie();
    E1HRE2 = new RiZoneSortie();
    OBJ_159 = new JLabel();
    OBJ_245 = new JLabel();
    E1DT1X = new XRiCalendrier();
    label8 = new JLabel();
    label9 = new JLabel();
    riBoutonDetail4 = new SNBoutonDetail();
    riBoutonDetail5 = new SNBoutonDetail();
    p_exped = new JXTitledPanel();
    OBJ_227 = new RiZoneSortie();
    OBJ_225 = new RiZoneSortie();
    E1VEH = new XRiTextField();
    E1PDS = new XRiTextField();
    E1ZTR = new XRiTextField();
    WMFRP = new RiZoneSortie();
    OBJ_235 = new JLabel();
    E1CTR = new XRiTextField();
    E1MEX = new XRiTextField();
    OBJ_271 = new JLabel();
    OBJ_190 = new JLabel();
    OBJ_191 = new JLabel();
    E1DPR = new XRiTextField();
    OBJ_203 = new JLabel();
    E1IN7 = new XRiTextField();
    OBJ_272 = new JLabel();
    label1 = new JLabel();
    label2 = new JLabel();
    OBJ_240 = new SNBoutonLeger();
    E1TRC = new XRiComboBox();
    label10 = new JLabel();
    label11 = new JLabel();
    riBoutonDetail3 = new SNBoutonDetail();
    label12 = new JLabel();
    E1COL = new XRiTextField();
    label13 = new JLabel();
    E1IN9 = new XRiComboBox();
    E1CRT = new XRiTextField();
    label18 = new JLabel();
    TOLIB = new RiZoneSortie();
    p_condition = new JXTitledPanel();
    OBJ_277 = new JLabel();
    E1CNV = new XRiTextField();
    WCNP = new RiZoneSortie();
    E1REM1 = new XRiTextField();
    E1REM2 = new XRiTextField();
    E1REM3 = new XRiTextField();
    E1ESCX = new XRiTextField();
    OBJ_276 = new JLabel();
    OBJ_274 = new JLabel();
    E1TAR = new XRiSpinner();
    E1PGC = new XRiComboBox();
    xTitledSeparator1 = new JXTitledSeparator();
    label4 = new JLabel();
    p_reglement = new JXTitledPanel();
    E1RG2 = new XRiTextField();
    OBJ_265 = new JLabel();
    LRG12 = new RiZoneSortie();
    OBJ_123 = new JLabel();
    lib_Devise = new RiZoneSortie();
    E1CHGX = new XRiTextField();
    E1BAS = new XRiTextField();
    label16 = new JLabel();
    label17 = new JLabel();
    panel2 = new JPanel();
    OBJ_65 = new JTabbedPane();
    FACTUR = new JPanel();
    E3NOM = new XRiTextField();
    E3CPL = new XRiTextField();
    E3RUE = new XRiTextField();
    E3LOC = new XRiTextField();
    E3VILN = new XRiTextField();
    E3PAYN = new XRiTextField();
    E3TEL = new XRiTextField();
    OBJ_69 = new JLabel();
    E1CLFP = new RiZoneSortie();
    E3CDPX = new XRiTextField();
    WCAT = new RiZoneSortie();
    E3COP = new XRiTextField();
    E1CLFS = new RiZoneSortie();
    button2 = new JButton();
    riBoutonDetail8 = new SNBoutonDetail();
    LIVRE = new JPanel();
    E2NOM = new XRiTextField();
    E2LOC = new XRiTextField();
    E2VILN = new XRiTextField();
    OBJ_102 = new JLabel();
    E2PAYN = new XRiTextField();
    E2TEL = new XRiTextField();
    OBJ_91 = new JLabel();
    E1CLLP = new RiZoneSortie();
    E2RUE = new XRiTextField();
    E2CDPX = new XRiTextField();
    WCAT_ = new RiZoneSortie();
    E2COP = new XRiTextField();
    E1CLLS = new RiZoneSortie();
    E2CPL = new XRiTextField();
    riBoutonDetail6 = new SNBoutonDetail();
    riBoutonDetail7 = new SNBoutonDetail();
    button3 = new JButton();
    OBJ_60 = new RiZoneSortie();
    OBJ_85 = new JLabel();
    E1IN17 = new XRiTextField();
    TE1IN17 = new XRiCheckBox();
    panel1 = new JPanel();
    riBoutonDetail9 = new SNBoutonDetail();
    label22 = new JLabel();
    LMAIL2 = new RiZoneSortie();
    label21 = new JLabel();
    LFAX2 = new RiZoneSortie();
    OBJ_86 = new JLabel();
    riBoutonDetailListe1 = new SNBoutonDetail();
    zs_LAVOIR = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_153 = new JLabel();
    E2LOC_doublon_35 = new XRiTextField();
    E1PFC = new XRiTextField();
    OBJ_113 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_164 = new JLabel();
    E1DT2X = new XRiCalendrier();
    OBJ_232 = new JLabel();
    bt_encours = new SNBoutonDetail();
    bt_reglement = new SNBoutonDetail();
    bt_condition = new SNBoutonDetail();
    OBJ_5 = new JMenuItem();
    riBoutonDetail1 = new SNBoutonDetail();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    button1 = new JButton();
    E1IN9_ = new XRiTextField();
    BTD2 = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 700));
    setPreferredSize(new Dimension(1205, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@ facturation @TFLIB@   @WLIEN@ (facturation @TFLIB@)");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setMaximumSize(new Dimension(900, 29));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- E1NFA ----
          E1NFA.setOpaque(false);
          E1NFA.setText("@E1NFA@");
          E1NFA.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NFA.setName("E1NFA");
          
          // ---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          
          // ---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");
          
          // ---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro");
          OBJ_49.setName("OBJ_49");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(null);
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          WSUF.setName("WSUF");
          
          // ---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setText("@WNUM@");
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");
          
          // ---- OBJ_54 ----
          OBJ_54.setText("@WDATEX@");
          OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getStyle() & ~Font.BOLD));
          OBJ_54.setBorder(new BevelBorder(BevelBorder.LOWERED));
          OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_54.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_54.setOpaque(false);
          OBJ_54.setName("OBJ_54");
          
          // ---- label15 ----
          label15.setText("Date de traitement");
          label15.setName("label15");
          
          // ---- E1VDE ----
          E1VDE.setComponentPopupMenu(BTD);
          E1VDE.setName("E1VDE");
          
          // ---- OBJ_61 ----
          OBJ_61.setText("Pr\u00e9parateur");
          OBJ_61.setName("OBJ_61");
          
          // ---- E1PRE ----
          E1PRE.setComponentPopupMenu(BTD);
          E1PRE.setName("E1PRE");
          
          // ---- OBJ_31 ----
          OBJ_31.setText("Vendeur");
          OBJ_31.setName("OBJ_31");
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Facture");
          OBJ_50.setName("OBJ_50");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1NFA, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1PRE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(label15, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1ETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1NFA, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 25,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(E1PRE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(label15, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- bt_ecran ----
          bt_ecran.setBorder(BorderFactory.createEmptyBorder());
          bt_ecran.setToolTipText("Passage \u00e0 l'affichage simplifi\u00e9");
          bt_ecran.setPreferredSize(new Dimension(30, 30));
          bt_ecran.setName("bt_ecran");
          bt_ecran.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              bt_ecranMouseClicked(e);
            }
          });
          p_tete_droite.add(bt_ecran);
          
          // ---- OBJ_129 ----
          OBJ_129.setOpaque(false);
          OBJ_129.setBorder(null);
          OBJ_129.setText("obj_129");
          OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getStyle() | Font.BOLD, OBJ_129.getFont().getSize() + 3f));
          OBJ_129.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_129.setName("OBJ_129");
          p_tete_droite.add(OBJ_129);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Acc\u00e8s lignes");
            bouton_valider.setToolTipText("Acc\u00e8s lignes");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Sortir");
            bouton_retour.setToolTipText("Sortir");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Acc\u00e9s compte");
              riSousMenu_bt6.setToolTipText("Acc\u00e9s compte");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique bon");
              riSousMenu_bt7.setToolTipText("Historique bon");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed(e));
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Options clients");
              riSousMenu_bt8.setToolTipText("Options clients");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(e -> riSousMenu_bt8ActionPerformed(e));
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Adresse de livraison");
              riSousMenu_bt9.setToolTipText("Recherche adresse de livraison");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(e -> riSousMenu_bt9ActionPerformed(e));
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Modif. zones d'ent\u00eate");
              riSousMenu_bt10.setToolTipText("Modification de certaines zones d'ent\u00eate");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(e -> riSousMenu_bt10ActionPerformed(e));
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Centrale");
              riSousMenu_bt11.setToolTipText("Centrale");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(e -> riSousMenu_bt11ActionPerformed(e));
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Emballage");
              riSousMenu_bt12.setToolTipText("Emballage");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(e -> riSousMenu_bt12ActionPerformed(e));
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Client en compte");
              riSousMenu_bt2.setToolTipText("Rechercher ou cr\u00e9er un client en compte");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(e -> riSousMenu_bt2ActionPerformed(e));
              riSousMenu3.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu3);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Recherche d'adresses");
              riSousMenu_bt13.setToolTipText("Recherche d'adresses");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(e -> riSousMenu_bt13ActionPerformed(e));
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Valider le chantier");
              riSousMenu_bt19.setToolTipText("Valider le chantier");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(e -> riSousMenu_bt19ActionPerformed(e));
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Documents li\u00e9s");
              riSousMenu_bt17.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(e -> riSousMenu_bt17ActionPerformed(e));
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riSousMenu5 ========
            {
              riSousMenu5.setName("riSousMenu5");
              
              // ---- riSousMenu_bt5 ----
              riSousMenu_bt5.setText("M\u00e9mo");
              riSousMenu_bt5.setToolTipText("M\u00e9mo");
              riSousMenu_bt5.setName("riSousMenu_bt5");
              riSousMenu_bt5.addActionListener(e -> riSousMenu_bt5ActionPerformed(e));
              riSousMenu5.add(riSousMenu_bt5);
            }
            menus_haut.add(riSousMenu5);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes du bon");
              riSousMenu_bt14.setToolTipText("Bloc-notes du bon");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(e -> riSousMenu_bt14ActionPerformed(e));
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes client factur\u00e9");
              riSousMenu_bt15.setToolTipText("bloc-notes du client factur\u00e9");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(e -> riSousMenu_bt15ActionPerformed(e));
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Bloc-notes client livr\u00e9");
              riSousMenu_bt16.setToolTipText("Bloc-notes client livr\u00e9");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(e -> riSousMenu_bt16ActionPerformed(e));
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Afficher les documents");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(e -> afficheDocumentActionPerformed(e));
              riSousMenu2.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Navigation ");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setPreferredSize(new Dimension(170, 260));
              riSousMenu18.setMinimumSize(new Dimension(170, 260));
              riSousMenu18.setMaximumSize(new Dimension(170, 260));
              riSousMenu18.setMargin(new Insets(0, -2, 0, 0));
              riSousMenu18.setName("riSousMenu18");
            }
            menus_haut.add(riSousMenu18);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setAutoscrolls(true);
        p_centrage.setMinimumSize(new Dimension(750, 570));
        p_centrage.setPreferredSize(new Dimension(750, 570));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1005, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1005, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);
          
          // ======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);
            
            // ---- E1RCC ----
            E1RCC.setComponentPopupMenu(BTD2);
            E1RCC.setName("E1RCC");
            panel4.add(E1RCC);
            E1RCC.setBounds(125, 25, 275, E1RCC.getPreferredSize().height);
            
            // ---- E1CCT ----
            E1CCT.setComponentPopupMenu(null);
            E1CCT.setName("E1CCT");
            panel4.add(E1CCT);
            E1CCT.setBounds(290, 50, 110, E1CCT.getPreferredSize().height);
            
            // ---- OBJ_145 ----
            OBJ_145.setText("Commande initiale");
            OBJ_145.setName("OBJ_145");
            panel4.add(OBJ_145);
            OBJ_145.setBounds(155, 55, 114, 19);
            
            // ---- E1NCC ----
            E1NCC.setComponentPopupMenu(null);
            E1NCC.setName("E1NCC");
            panel4.add(E1NCC);
            E1NCC.setBounds(125, 0, 115, E1NCC.getPreferredSize().height);
            
            // ---- E1TP1 ----
            E1TP1.setComponentPopupMenu(BTD);
            E1TP1.setName("E1TP1");
            panel4.add(E1TP1);
            E1TP1.setBounds(245, 100, 30, E1TP1.getPreferredSize().height);
            
            // ---- E1TP2 ----
            E1TP2.setComponentPopupMenu(BTD);
            E1TP2.setName("E1TP2");
            panel4.add(E1TP2);
            E1TP2.setBounds(276, 100, 30, E1TP2.getPreferredSize().height);
            
            // ---- E1TP3 ----
            E1TP3.setComponentPopupMenu(BTD);
            E1TP3.setName("E1TP3");
            panel4.add(E1TP3);
            E1TP3.setBounds(307, 100, 30, E1TP3.getPreferredSize().height);
            
            // ---- E1TP4 ----
            E1TP4.setComponentPopupMenu(BTD);
            E1TP4.setName("E1TP4");
            panel4.add(E1TP4);
            E1TP4.setBounds(338, 100, 30, E1TP4.getPreferredSize().height);
            
            // ---- E1TP5 ----
            E1TP5.setComponentPopupMenu(BTD);
            E1TP5.setName("E1TP5");
            panel4.add(E1TP5);
            E1TP5.setBounds(369, 100, 30, E1TP5.getPreferredSize().height);
            
            // ---- OBJ_192 ----
            OBJ_192.setText("@TIZPE1@");
            OBJ_192.setName("OBJ_192");
            panel4.add(OBJ_192);
            OBJ_192.setBounds(250, 80, 25, 20);
            
            // ---- OBJ_193 ----
            OBJ_193.setText("@TIZPE2@");
            OBJ_193.setName("OBJ_193");
            panel4.add(OBJ_193);
            OBJ_193.setBounds(280, 80, 25, 20);
            
            // ---- OBJ_194 ----
            OBJ_194.setText("@TIZPE3@");
            OBJ_194.setName("OBJ_194");
            panel4.add(OBJ_194);
            OBJ_194.setBounds(315, 80, 25, 20);
            
            // ---- OBJ_195 ----
            OBJ_195.setText("@TIZPE4@");
            OBJ_195.setName("OBJ_195");
            panel4.add(OBJ_195);
            OBJ_195.setBounds(345, 80, 25, 20);
            
            // ---- OBJ_196 ----
            OBJ_196.setText("@TIZPE5@");
            OBJ_196.setName("OBJ_196");
            panel4.add(OBJ_196);
            OBJ_196.setBounds(375, 80, 25, 20);
            
            // ---- OBJ_210 ----
            OBJ_210.setText("Section");
            OBJ_210.setName("OBJ_210");
            panel4.add(OBJ_210);
            OBJ_210.setBounds(10, 54, 46, 20);
            
            // ---- E1SAN ----
            E1SAN.setComponentPopupMenu(BTD);
            E1SAN.setName("E1SAN");
            panel4.add(E1SAN);
            E1SAN.setBounds(75, 50, 50, E1SAN.getPreferredSize().height);
            
            // ---- E1ACT ----
            E1ACT.setComponentPopupMenu(BTD);
            E1ACT.setName("E1ACT");
            panel4.add(E1ACT);
            E1ACT.setBounds(75, 75, 50, E1ACT.getPreferredSize().height);
            
            // ---- OBJ_212 ----
            OBJ_212.setText("Affaire");
            OBJ_212.setName("OBJ_212");
            panel4.add(OBJ_212);
            OBJ_212.setBounds(10, 79, 39, 20);
            
            // ---- E20NAT ----
            E20NAT.setComponentPopupMenu(BTD);
            E20NAT.setName("E20NAT");
            panel4.add(E20NAT);
            E20NAT.setBounds(65, 100, 60, E20NAT.getPreferredSize().height);
            
            // ---- OBJ_214 ----
            OBJ_214.setText("Nature");
            OBJ_214.setName("OBJ_214");
            panel4.add(OBJ_214);
            OBJ_214.setBounds(10, 104, 43, 20);
            
            // ---- OBJ_84 ----
            OBJ_84.setText("Canal");
            OBJ_84.setName("OBJ_84");
            panel4.add(OBJ_84);
            OBJ_84.setBounds(155, 104, 50, 20);
            
            // ---- E1CAN ----
            E1CAN.setText("@E1CAN@");
            E1CAN.setName("E1CAN");
            panel4.add(E1CAN);
            E1CAN.setBounds(200, 102, 40, E1CAN.getPreferredSize().height);
            
            // ---- label3 ----
            label3.setText("Repr\u00e9sentant");
            label3.setName("label3");
            panel4.add(label3);
            label3.setBounds(10, 129, 80, 20);
            
            // ---- RPLI1G ----
            RPLI1G.setText("@RPLI1G@");
            RPLI1G.setName("RPLI1G");
            panel4.add(RPLI1G);
            RPLI1G.setBounds(142, 127, 233, RPLI1G.getPreferredSize().height);
            
            // ---- E1REP ----
            E1REP.setComponentPopupMenu(BTD);
            E1REP.setName("E1REP");
            panel4.add(E1REP);
            E1REP.setBounds(91, 125, 34, E1REP.getPreferredSize().height);
            
            // ---- E1TRP ----
            E1TRP.setComponentPopupMenu(BTD);
            E1TRP.setToolTipText("Type de commisionnement");
            E1TRP.setName("E1TRP");
            panel4.add(E1TRP);
            E1TRP.setBounds(123, 125, 20, E1TRP.getPreferredSize().height);
            
            // ---- label5 ----
            label5.setText("Observation");
            label5.setName("label5");
            panel4.add(label5);
            label5.setBounds(10, 154, 80, 20);
            
            // ---- WOBS ----
            WOBS.setComponentPopupMenu(null);
            WOBS.setText("@WOBS@");
            WOBS.setName("WOBS");
            panel4.add(WOBS);
            WOBS.setBounds(91, 152, 284, WOBS.getPreferredSize().height);
            
            // ---- OBJ_257 ----
            OBJ_257.setText("Nombre exemplaires facture");
            OBJ_257.setName("OBJ_257");
            panel4.add(OBJ_257);
            OBJ_257.setBounds(10, 179, 165, 20);
            
            // ---- E1NEX ----
            E1NEX.setComponentPopupMenu(null);
            E1NEX.setName("E1NEX");
            panel4.add(E1NEX);
            E1NEX.setBounds(173, 175, 20, E1NEX.getPreferredSize().height);
            
            // ---- label6 ----
            label6.setText("Magasin");
            label6.setName("label6");
            panel4.add(label6);
            label6.setBounds(10, 203, 65, 20);
            
            // ---- E1MAG ----
            E1MAG.setComponentPopupMenu(BTD);
            E1MAG.setName("E1MAG");
            panel4.add(E1MAG);
            E1MAG.setBounds(91, 199, 34, E1MAG.getPreferredSize().height);
            
            // ---- label7 ----
            label7.setText("@MALIBR@");
            label7.setName("label7");
            panel4.add(label7);
            label7.setBounds(130, 202, 244, label7.getPreferredSize().height);
            
            // ---- riBoutonDetail2 ----
            riBoutonDetail2.setToolTipText("Repr\u00e9sentants");
            riBoutonDetail2.setName("riBoutonDetail2");
            riBoutonDetail2.addActionListener(e -> riBoutonDetail2ActionPerformed(e));
            panel4.add(riBoutonDetail2);
            riBoutonDetail2.setBounds(new Rectangle(new Point(379, 130), riBoutonDetail2.getPreferredSize()));
            
            // ======== panelChantier ========
            {
              panelChantier.setOpaque(false);
              panelChantier.setName("panelChantier");
              panelChantier.setLayout(null);
              
              // ---- label14 ----
              label14.setText("Chantier");
              label14.setName("label14");
              panelChantier.add(label14);
              label14.setBounds(5, 2, 60, 25);
              
              // ---- WCHANT ----
              WCHANT.setComponentPopupMenu(BTD);
              WCHANT.setName("WCHANT");
              panelChantier.add(WCHANT);
              WCHANT.setBounds(67, 0, 74, WCHANT.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panelChantier.getComponentCount(); i++) {
                  Rectangle bounds = panelChantier.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panelChantier.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panelChantier.setMinimumSize(preferredSize);
                panelChantier.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panelChantier);
            panelChantier.setBounds(235, 175, 145, 30);
            
            // ---- lbrefCourte ----
            lbrefCourte.setText("Ref\u00e9rence courte");
            lbrefCourte.setName("lbrefCourte");
            panel4.add(lbrefCourte);
            lbrefCourte.setBounds(10, 2, 115, 25);
            
            // ---- lbrefLongue ----
            lbrefLongue.setText("Ref\u00e9rence longue");
            lbrefLongue.setName("lbrefLongue");
            panel4.add(lbrefLongue);
            lbrefLongue.setBounds(10, 27, 115, 25);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(365, 26, 408, 230);
          
          // ======== p_encours ========
          {
            p_encours.setTitle("Encours");
            p_encours.setBorder(new DropShadowBorder());
            p_encours.setName("p_encours");
            Container p_encoursContentContainer = p_encours.getContentContainer();
            p_encoursContentContainer.setLayout(null);
            
            // ======== layeredPane1 ========
            {
              layeredPane1.setName("layeredPane1");
              
              // ======== p_impaye ========
              {
                p_impaye.setOpaque(false);
                p_impaye.setPreferredSize(new Dimension(160, 0));
                p_impaye.setName("p_impaye");
                p_impaye.setLayout(null);
                
                // ---- OBJ_157 ----
                OBJ_157.setForeground(new Color(255, 0, 51));
                OBJ_157.setHorizontalAlignment(SwingConstants.CENTER);
                OBJ_157.setFont(OBJ_157.getFont().deriveFont(OBJ_157.getFont().getStyle() | Font.BOLD, OBJ_157.getFont().getSize() + 6f));
                OBJ_157.setName("OBJ_157");
                p_impaye.add(OBJ_157);
                OBJ_157.setBounds(40, 5, 130, 80);
              }
              layeredPane1.add(p_impaye, JLayeredPane.DEFAULT_LAYER);
              p_impaye.setBounds(0, 0, 190, 95);
              
              // ======== p_paye ========
              {
                p_paye.setOpaque(false);
                p_paye.setName("p_paye");
                p_paye.setLayout(null);
                
                // ---- lb_depassement ----
                lb_depassement.setText("D\u00e9passement");
                lb_depassement.setForeground(Color.black);
                lb_depassement.setFont(new Font("sansserif", Font.PLAIN, 12));
                lb_depassement.setMaximumSize(new Dimension(170, 30));
                lb_depassement.setMinimumSize(new Dimension(170, 30));
                lb_depassement.setPreferredSize(new Dimension(170, 30));
                lb_depassement.setHorizontalAlignment(SwingConstants.LEFT);
                lb_depassement.setName("lb_depassement");
                p_paye.add(lb_depassement);
                lb_depassement.setBounds(15, 58, 90, 18);
                
                // ---- WEPOS ----
                WEPOS.setComponentPopupMenu(BTD);
                WEPOS.setText("@WEPOS@");
                WEPOS.setHorizontalAlignment(SwingConstants.RIGHT);
                WEPOS.setName("WEPOS");
                p_paye.add(WEPOS);
                WEPOS.setBounds(107, 5, 68, WEPOS.getPreferredSize().height);
                
                // ---- WEPLF ----
                WEPLF.setComponentPopupMenu(BTD);
                WEPLF.setText("@WEPLF@");
                WEPLF.setHorizontalAlignment(SwingConstants.RIGHT);
                WEPLF.setName("WEPLF");
                p_paye.add(WEPLF);
                WEPLF.setBounds(107, 30, 68, WEPLF.getPreferredSize().height);
                
                // ---- WEDEP ----
                WEDEP.setComponentPopupMenu(BTD);
                WEDEP.setText("@WEDEP@");
                WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
                WEDEP.setForeground(Color.red);
                WEDEP.setName("WEDEP");
                p_paye.add(WEDEP);
                WEDEP.setBounds(107, 55, 68, WEDEP.getPreferredSize().height);
                
                // ---- OBJ_162 ----
                OBJ_162.setText("Position");
                OBJ_162.setName("OBJ_162");
                p_paye.add(OBJ_162);
                OBJ_162.setBounds(15, 10, 51, 14);
                
                // ---- OBJ_163 ----
                OBJ_163.setText("Plafond");
                OBJ_163.setName("OBJ_163");
                p_paye.add(OBJ_163);
                OBJ_163.setBounds(15, 35, 51, 14);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < p_paye.getComponentCount(); i++) {
                    Rectangle bounds = p_paye.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = p_paye.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  p_paye.setMinimumSize(preferredSize);
                  p_paye.setPreferredSize(preferredSize);
                }
              }
              layeredPane1.add(p_paye, JLayeredPane.DEFAULT_LAYER);
              p_paye.setBounds(0, 0, 190, 100);
            }
            p_encoursContentContainer.add(layeredPane1);
            layeredPane1.setBounds(0, 0, 190, 95);
          }
          p_contenu.add(p_encours);
          p_encours.setBounds(779, 129, 205, 132);
          
          // ======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Dates");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);
            
            // ---- E1DLSX ----
            E1DLSX.setComponentPopupMenu(BTD);
            E1DLSX.setName("E1DLSX");
            xTitledPanel4ContentContainer.add(E1DLSX);
            E1DLSX.setBounds(195, 55, 105, E1DLSX.getPreferredSize().height);
            
            // ---- E1DLPX ----
            E1DLPX.setComponentPopupMenu(BTD);
            E1DLPX.setName("E1DLPX");
            xTitledPanel4ContentContainer.add(E1DLPX);
            E1DLPX.setBounds(195, 80, 105, E1DLPX.getPreferredSize().height);
            
            // ---- OBJ_169 ----
            OBJ_169.setText("Commande");
            OBJ_169.setToolTipText("<HTML>Choix d'une date gr\u00e2ce<BR>@&L000285@</HTML>");
            OBJ_169.setName("OBJ_169");
            xTitledPanel4ContentContainer.add(OBJ_169);
            OBJ_169.setBounds(15, 36, 145, 16);
            
            // ---- OBJ_161 ----
            OBJ_161.setText("Heure commande");
            OBJ_161.setName("OBJ_161");
            xTitledPanel4ContentContainer.add(OBJ_161);
            OBJ_161.setBounds(15, 10, 145, 14);
            
            // ---- E1DCOX ----
            E1DCOX.setComponentPopupMenu(BTD);
            E1DCOX.setName("E1DCOX");
            xTitledPanel4ContentContainer.add(E1DCOX);
            E1DCOX.setBounds(195, 30, 105, E1DCOX.getPreferredSize().height);
            
            // ---- E1HRE1 ----
            E1HRE1.setComponentPopupMenu(null);
            E1HRE1.setText("@E1HRE1@");
            E1HRE1.setHorizontalAlignment(SwingConstants.CENTER);
            E1HRE1.setName("E1HRE1");
            xTitledPanel4ContentContainer.add(E1HRE1);
            E1HRE1.setBounds(197, 5, 30, E1HRE1.getPreferredSize().height);
            
            // ---- E1HRE2 ----
            E1HRE2.setComponentPopupMenu(null);
            E1HRE2.setText("@E1HRE2@");
            E1HRE2.setHorizontalAlignment(SwingConstants.CENTER);
            E1HRE2.setName("E1HRE2");
            xTitledPanel4ContentContainer.add(E1HRE2);
            E1HRE2.setBounds(269, 5, 30, E1HRE2.getPreferredSize().height);
            
            // ---- OBJ_159 ----
            OBJ_159.setText("H");
            OBJ_159.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_159.setName("OBJ_159");
            xTitledPanel4ContentContainer.add(OBJ_159);
            OBJ_159.setBounds(240, 8, 12, 18);
            
            // ---- OBJ_245 ----
            OBJ_245.setText("Date facturation mini");
            OBJ_245.setToolTipText("<HTML>Choix d'une date gr\u00e2ce<BR>@&L000285@</HTML>");
            OBJ_245.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_245.setName("OBJ_245");
            xTitledPanel4ContentContainer.add(OBJ_245);
            OBJ_245.setBounds(15, 107, 145, 24);
            
            // ---- E1DT1X ----
            E1DT1X.setComponentPopupMenu(BTD);
            E1DT1X.setName("E1DT1X");
            xTitledPanel4ContentContainer.add(E1DT1X);
            E1DT1X.setBounds(195, 105, 105, E1DT1X.getPreferredSize().height);
            
            // ---- label8 ----
            label8.setText("Livraison souhait\u00e9e");
            label8.setName("label8");
            xTitledPanel4ContentContainer.add(label8);
            label8.setBounds(15, 58, 145, 22);
            
            // ---- label9 ----
            label9.setText("Livraison pr\u00e9vue");
            label9.setName("label9");
            xTitledPanel4ContentContainer.add(label9);
            label9.setBounds(15, 83, 145, 22);
            
            // ---- riBoutonDetail4 ----
            riBoutonDetail4.setToolTipText("Livraison souhait\u00e9e");
            riBoutonDetail4.setName("riBoutonDetail4");
            riBoutonDetail4.addActionListener(e -> riBoutonDetail4ActionPerformed(e));
            xTitledPanel4ContentContainer.add(riBoutonDetail4);
            riBoutonDetail4.setBounds(new Rectangle(new Point(135, 60), riBoutonDetail4.getPreferredSize()));
            
            // ---- riBoutonDetail5 ----
            riBoutonDetail5.setToolTipText("Livraison pr\u00e9vue");
            riBoutonDetail5.setName("riBoutonDetail5");
            riBoutonDetail5.addActionListener(e -> riBoutonDetail5ActionPerformed(e));
            xTitledPanel4ContentContainer.add(riBoutonDetail5);
            riBoutonDetail5.setBounds(new Rectangle(new Point(135, 85), riBoutonDetail5.getPreferredSize()));
          }
          p_contenu.add(xTitledPanel4);
          xTitledPanel4.setBounds(25, 266, 335, 189);
          
          // ======== p_exped ========
          {
            p_exped.setTitle("Mode de r\u00e9cup\u00e9ration");
            p_exped.setBorder(new DropShadowBorder());
            p_exped.setName("p_exped");
            Container p_expedContentContainer = p_exped.getContentContainer();
            p_expedContentContainer.setLayout(null);
            
            // ---- OBJ_227 ----
            OBJ_227.setText("@TRLIB@");
            OBJ_227.setName("OBJ_227");
            p_expedContentContainer.add(OBJ_227);
            OBJ_227.setBounds(130, 32, 243, OBJ_227.getPreferredSize().height);
            
            // ---- OBJ_225 ----
            OBJ_225.setText("@EXLIBR@");
            OBJ_225.setName("OBJ_225");
            p_expedContentContainer.add(OBJ_225);
            OBJ_225.setBounds(130, 7, 243, OBJ_225.getPreferredSize().height);
            
            // ---- E1VEH ----
            E1VEH.setComponentPopupMenu(null);
            E1VEH.setName("E1VEH");
            p_expedContentContainer.add(E1VEH);
            E1VEH.setBounds(91, 55, 110, E1VEH.getPreferredSize().height);
            
            // ---- E1PDS ----
            E1PDS.setComponentPopupMenu(null);
            E1PDS.setName("E1PDS");
            p_expedContentContainer.add(E1PDS);
            E1PDS.setBounds(91, 105, 74, E1PDS.getPreferredSize().height);
            
            // ---- E1ZTR ----
            E1ZTR.setComponentPopupMenu(null);
            E1ZTR.setToolTipText("zone g\u00e9ographique");
            E1ZTR.setName("E1ZTR");
            p_expedContentContainer.add(E1ZTR);
            E1ZTR.setBounds(91, 80, 60, E1ZTR.getPreferredSize().height);
            
            // ---- WMFRP ----
            WMFRP.setComponentPopupMenu(null);
            WMFRP.setText("@WMFRP@");
            WMFRP.setHorizontalAlignment(SwingConstants.RIGHT);
            WMFRP.setName("WMFRP");
            p_expedContentContainer.add(WMFRP);
            WMFRP.setBounds(525, 7, 66, WMFRP.getPreferredSize().height);
            
            // ---- OBJ_235 ----
            OBJ_235.setText("Franco");
            OBJ_235.setName("OBJ_235");
            p_expedContentContainer.add(OBJ_235);
            OBJ_235.setBounds(430, 9, 65, 20);
            
            // ---- E1CTR ----
            E1CTR.setComponentPopupMenu(BTD);
            E1CTR.setName("E1CTR");
            p_expedContentContainer.add(E1CTR);
            E1CTR.setBounds(91, 30, 34, E1CTR.getPreferredSize().height);
            
            // ---- E1MEX ----
            E1MEX.setComponentPopupMenu(BTD);
            E1MEX.setName("E1MEX");
            p_expedContentContainer.add(E1MEX);
            E1MEX.setBounds(91, 5, 34, E1MEX.getPreferredSize().height);
            
            // ---- OBJ_271 ----
            OBJ_271.setText("Zone g\u00e9o.");
            OBJ_271.setName("OBJ_271");
            p_expedContentContainer.add(OBJ_271);
            OBJ_271.setBounds(10, 80, 75, 28);
            
            // ---- OBJ_190 ----
            OBJ_190.setText("D\u00e9lai pr\u00e9paration");
            OBJ_190.setName("OBJ_190");
            p_expedContentContainer.add(OBJ_190);
            OBJ_190.setBounds(430, 36, 120, 16);
            
            // ---- OBJ_191 ----
            OBJ_191.setText("Livraison partielle");
            OBJ_191.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_191.setName("OBJ_191");
            p_expedContentContainer.add(OBJ_191);
            OBJ_191.setBounds(255, 59, 120, 20);
            
            // ---- E1DPR ----
            E1DPR.setComponentPopupMenu(null);
            E1DPR.setToolTipText("D\u00e9lai en nombre de semaines");
            E1DPR.setName("E1DPR");
            p_expedContentContainer.add(E1DPR);
            E1DPR.setBounds(563, 30, 28, E1DPR.getPreferredSize().height);
            
            // ---- OBJ_203 ----
            OBJ_203.setText("S\u00e9lection");
            OBJ_203.setName("OBJ_203");
            p_expedContentContainer.add(OBJ_203);
            OBJ_203.setBounds(430, 85, 110, 18);
            
            // ---- E1IN7 ----
            E1IN7.setToolTipText("<HTML>Caract\u00e8re de s\u00e9lection du bon<BR>@&L000985@</HTML>");
            E1IN7.setComponentPopupMenu(null);
            E1IN7.setName("E1IN7");
            p_expedContentContainer.add(E1IN7);
            E1IN7.setBounds(567, 80, 24, E1IN7.getPreferredSize().height);
            
            // ---- OBJ_272 ----
            OBJ_272.setText("V\u00e9hicule");
            OBJ_272.setName("OBJ_272");
            p_expedContentContainer.add(OBJ_272);
            OBJ_272.setBounds(10, 55, 75, 28);
            
            // ---- label1 ----
            label1.setText("Mode");
            label1.setName("label1");
            p_expedContentContainer.add(label1);
            label1.setBounds(10, 5, 75, 28);
            
            // ---- label2 ----
            label2.setText("Transporteur");
            label2.setName("label2");
            p_expedContentContainer.add(label2);
            label2.setBounds(10, 30, 75, 28);
            
            // ---- OBJ_240 ----
            OBJ_240.setText("Aucune");
            OBJ_240.setToolTipText("Aucune g\u00e9n\u00e9ration de bon d'achat");
            OBJ_240.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_240.setName("OBJ_240");
            OBJ_240.addActionListener(e -> OBJ_240ActionPerformed(e));
            p_expedContentContainer.add(OBJ_240);
            OBJ_240.setBounds(430, 108, 160, 22);
            
            // ---- E1TRC ----
            E1TRC.setModel(new DefaultComboBoxModel<>(
                new String[] { "Aucun for\u00e7age", "For\u00e7age zone transport", "Non calcul de port", "Franco garanti" }));
            E1TRC.setComponentPopupMenu(null);
            E1TRC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E1TRC.setName("E1TRC");
            p_expedContentContainer.add(E1TRC);
            E1TRC.setBounds(185, 81, 190, E1TRC.getPreferredSize().height);
            
            // ---- label10 ----
            label10.setText("Poids");
            label10.setName("label10");
            p_expedContentContainer.add(label10);
            label10.setBounds(10, 105, 75, 28);
            
            // ---- label11 ----
            label11.setHorizontalAlignment(SwingConstants.CENTER);
            label11.setForeground(new Color(102, 102, 102));
            label11.setName("label11");
            p_expedContentContainer.add(label11);
            label11.setBounds(430, 132, 160, 18);
            
            // ---- riBoutonDetail3 ----
            riBoutonDetail3.setToolTipText("Affichage bon d'achat g\u00e9n\u00e9r\u00e9");
            riBoutonDetail3.setName("riBoutonDetail3");
            riBoutonDetail3.addActionListener(e -> riBoutonDetail3ActionPerformed(e));
            p_expedContentContainer.add(riBoutonDetail3);
            riBoutonDetail3.setBounds(590, 110, 25, riBoutonDetail3.getPreferredSize().height);
            
            // ---- label12 ----
            label12.setText("G\u00e9n\u00e9ration d'achat:");
            label12.setHorizontalAlignment(SwingConstants.RIGHT);
            label12.setName("label12");
            p_expedContentContainer.add(label12);
            label12.setBounds(280, 109, 140, 20);
            
            // ---- E1COL ----
            E1COL.setComponentPopupMenu(BTD);
            E1COL.setName("E1COL");
            p_expedContentContainer.add(E1COL);
            E1COL.setBounds(230, 105, 42, E1COL.getPreferredSize().height);
            
            // ---- label13 ----
            label13.setText("Colis");
            label13.setName("label13");
            p_expedContentContainer.add(label13);
            label13.setBounds(187, 109, 38, 20);
            
            // ---- E1IN9 ----
            E1IN9.setModel(new DefaultComboBoxModel<>(
                new String[] { "Autoris\u00e9e", "Interdite sans r\u00e9servation", "Interdite avec r\u00e9servation" }));
            E1IN9.setName("E1IN9");
            p_expedContentContainer.add(E1IN9);
            E1IN9.setBounds(385, 56, 205, E1IN9.getPreferredSize().height);
            
            // ---- E1CRT ----
            E1CRT.setComponentPopupMenu(BTD);
            E1CRT.setName("E1CRT");
            p_expedContentContainer.add(E1CRT);
            E1CRT.setBounds(91, 130, 60, E1CRT.getPreferredSize().height);
            
            // ---- label18 ----
            label18.setText("Tourn\u00e9e");
            label18.setName("label18");
            p_expedContentContainer.add(label18);
            label18.setBounds(10, 130, 75, 28);
            
            // ---- TOLIB ----
            TOLIB.setText("@TOLIB@");
            TOLIB.setName("TOLIB");
            p_expedContentContainer.add(TOLIB);
            TOLIB.setBounds(155, 132, 243, TOLIB.getPreferredSize().height);
          }
          p_contenu.add(p_exped);
          p_exped.setBounds(365, 266, 620, 189);
          
          // ======== p_condition ========
          {
            p_condition.setTitle("Conditions");
            p_condition.setBorder(new DropShadowBorder());
            p_condition.setName("p_condition");
            Container p_conditionContentContainer = p_condition.getContentContainer();
            p_conditionContentContainer.setLayout(null);
            
            // ---- OBJ_277 ----
            OBJ_277.setText("%Esc.");
            OBJ_277.setFont(OBJ_277.getFont().deriveFont(OBJ_277.getFont().getStyle() | Font.BOLD));
            OBJ_277.setName("OBJ_277");
            p_conditionContentContainer.add(OBJ_277);
            OBJ_277.setBounds(340, 5, 40, 17);
            
            // ---- E1CNV ----
            E1CNV.setComponentPopupMenu(BTD);
            E1CNV.setName("E1CNV");
            p_conditionContentContainer.add(E1CNV);
            E1CNV.setBounds(205, 25, 70, E1CNV.getPreferredSize().height);
            
            // ---- WCNP ----
            WCNP.setText("@WCNP@");
            WCNP.setName("WCNP");
            p_conditionContentContainer.add(WCNP);
            WCNP.setBounds(275, 27, 60, WCNP.getPreferredSize().height);
            
            // ---- E1REM1 ----
            E1REM1.setComponentPopupMenu(BTD);
            E1REM1.setName("E1REM1");
            p_conditionContentContainer.add(E1REM1);
            E1REM1.setBounds(5, 25, 50, E1REM1.getPreferredSize().height);
            
            // ---- E1REM2 ----
            E1REM2.setComponentPopupMenu(BTD);
            E1REM2.setName("E1REM2");
            p_conditionContentContainer.add(E1REM2);
            E1REM2.setBounds(60, 25, 50, E1REM2.getPreferredSize().height);
            
            // ---- E1REM3 ----
            E1REM3.setComponentPopupMenu(BTD);
            E1REM3.setName("E1REM3");
            p_conditionContentContainer.add(E1REM3);
            E1REM3.setBounds(110, 25, 50, E1REM3.getPreferredSize().height);
            
            // ---- E1ESCX ----
            E1ESCX.setComponentPopupMenu(BTD);
            E1ESCX.setHorizontalAlignment(SwingConstants.RIGHT);
            E1ESCX.setName("E1ESCX");
            p_conditionContentContainer.add(E1ESCX);
            E1ESCX.setBounds(340, 25, 42, E1ESCX.getPreferredSize().height);
            
            // ---- OBJ_276 ----
            OBJ_276.setText("Promo");
            OBJ_276.setFont(OBJ_276.getFont().deriveFont(OBJ_276.getFont().getStyle() | Font.BOLD));
            OBJ_276.setName("OBJ_276");
            p_conditionContentContainer.add(OBJ_276);
            OBJ_276.setBounds(280, 5, 43, 17);
            
            // ---- OBJ_274 ----
            OBJ_274.setText("Tarif");
            OBJ_274.setFont(OBJ_274.getFont().deriveFont(OBJ_274.getFont().getStyle() | Font.BOLD));
            OBJ_274.setName("OBJ_274");
            p_conditionContentContainer.add(OBJ_274);
            OBJ_274.setBounds(165, 5, 30, 17);
            
            // ---- E1TAR ----
            E1TAR.setComponentPopupMenu(BTD);
            E1TAR.setName("E1TAR");
            p_conditionContentContainer.add(E1TAR);
            E1TAR.setBounds(160, 25, 45, E1TAR.getPreferredSize().height);
            
            // ---- E1PGC ----
            E1PGC.setModel(
                new DefaultComboBoxModel<>(new String[] { "Prix garanti", "Prix non garanti", "Ancien prix", "Prix garanti/devis" }));
            E1PGC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            E1PGC.setName("E1PGC");
            p_conditionContentContainer.add(E1PGC);
            E1PGC.setBounds(385, 26, 130, E1PGC.getPreferredSize().height);
            
            // ---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("Remises");
            xTitledSeparator1.setName("xTitledSeparator1");
            p_conditionContentContainer.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(10, 5, 145, xTitledSeparator1.getPreferredSize().height);
            
            // ---- label4 ----
            label4.setText("Condition");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            p_conditionContentContainer.add(label4);
            label4.setBounds(210, 3, 63, 20);
          }
          p_contenu.add(p_condition);
          p_condition.setBounds(455, 461, 530, 89);
          
          // ======== p_reglement ========
          {
            p_reglement.setTitle("R\u00e9glements");
            p_reglement.setRightDecoration(null);
            p_reglement.setBorder(new DropShadowBorder());
            p_reglement.setName("p_reglement");
            Container p_reglementContentContainer = p_reglement.getContentContainer();
            p_reglementContentContainer.setLayout(null);
            
            // ---- E1RG2 ----
            E1RG2.setComponentPopupMenu(BTD);
            E1RG2.setName("E1RG2");
            p_reglementContentContainer.add(E1RG2);
            E1RG2.setBounds(15, 10, 34, E1RG2.getPreferredSize().height);
            
            // ---- OBJ_265 ----
            OBJ_265.setText("Ech\u00e9ance");
            OBJ_265.setName("OBJ_265");
            p_reglementContentContainer.add(OBJ_265);
            OBJ_265.setBounds(52, 14, 65, 20);
            
            // ---- LRG12 ----
            LRG12.setText("@LRG12@");
            LRG12.setName("LRG12");
            p_reglementContentContainer.add(LRG12);
            LRG12.setBounds(115, 12, 291, LRG12.getPreferredSize().height);
            
            // ---- OBJ_123 ----
            OBJ_123.setText("@LIBRG1@");
            OBJ_123.setName("OBJ_123");
            p_reglementContentContainer.add(OBJ_123);
            OBJ_123.setBounds(16, 38, 170, 23);
            
            // ---- lib_Devise ----
            lib_Devise.setText("@DVLIBR@");
            lib_Devise.setBackground(new Color(204, 204, 204));
            lib_Devise.setFont(lib_Devise.getFont().deriveFont(lib_Devise.getFont().getStyle() | Font.BOLD));
            lib_Devise.setName("lib_Devise");
            p_reglementContentContainer.add(lib_Devise);
            lib_Devise.setBounds(198, 37, 208, lib_Devise.getPreferredSize().height);
            
            // ---- E1CHGX ----
            E1CHGX.setName("E1CHGX");
            p_reglementContentContainer.add(E1CHGX);
            E1CHGX.setBounds(198, 60, 90, E1CHGX.getPreferredSize().height);
            
            // ---- E1BAS ----
            E1BAS.setName("E1BAS");
            p_reglementContentContainer.add(E1BAS);
            E1BAS.setBounds(354, 60, 52, E1BAS.getPreferredSize().height);
            
            // ---- label16 ----
            label16.setText("Change");
            label16.setHorizontalAlignment(SwingConstants.RIGHT);
            label16.setName("label16");
            p_reglementContentContainer.add(label16);
            label16.setBounds(131, 64, 55, 20);
            
            // ---- label17 ----
            label17.setText("Base");
            label17.setName("label17");
            p_reglementContentContainer.add(label17);
            label17.setBounds(312, 64, 40, 20);
          }
          p_contenu.add(p_reglement);
          p_reglement.setBounds(25, 461, 425, 128);
          
          // ======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ======== OBJ_65 ========
            {
              OBJ_65.setDoubleBuffered(true);
              OBJ_65.setFocusCycleRoot(true);
              OBJ_65.setFocusTraversalPolicyProvider(true);
              OBJ_65.setName("OBJ_65");
              
              // ======== FACTUR ========
              {
                FACTUR.setFocusCycleRoot(true);
                FACTUR.setFocusTraversalPolicyProvider(true);
                FACTUR.setOpaque(false);
                FACTUR.setName("FACTUR");
                FACTUR.setLayout(null);
                
                // ---- E3NOM ----
                E3NOM.setComponentPopupMenu(BTD);
                E3NOM.setFont(E3NOM.getFont().deriveFont(E3NOM.getFont().getStyle() | Font.BOLD));
                E3NOM.setName("E3NOM");
                FACTUR.add(E3NOM);
                E3NOM.setBounds(10, 30, 310, E3NOM.getPreferredSize().height);
                
                // ---- E3CPL ----
                E3CPL.setComponentPopupMenu(BTD);
                E3CPL.setName("E3CPL");
                FACTUR.add(E3CPL);
                E3CPL.setBounds(10, 55, 310, E3CPL.getPreferredSize().height);
                
                // ---- E3RUE ----
                E3RUE.setComponentPopupMenu(BTD);
                E3RUE.setName("E3RUE");
                FACTUR.add(E3RUE);
                E3RUE.setBounds(10, 80, 310, E3RUE.getPreferredSize().height);
                
                // ---- E3LOC ----
                E3LOC.setComponentPopupMenu(BTD);
                E3LOC.setName("E3LOC");
                FACTUR.add(E3LOC);
                E3LOC.setBounds(10, 105, 310, E3LOC.getPreferredSize().height);
                
                // ---- E3VILN ----
                E3VILN.setComponentPopupMenu(BTD);
                E3VILN.setName("E3VILN");
                FACTUR.add(E3VILN);
                E3VILN.setBounds(70, 130, 250, E3VILN.getPreferredSize().height);
                
                // ---- E3PAYN ----
                E3PAYN.setComponentPopupMenu(BTD);
                E3PAYN.setName("E3PAYN");
                FACTUR.add(E3PAYN);
                E3PAYN.setBounds(10, 155, 270, E3PAYN.getPreferredSize().height);
                
                // ---- E3TEL ----
                E3TEL.setComponentPopupMenu(BTD);
                E3TEL.setName("E3TEL");
                FACTUR.add(E3TEL);
                E3TEL.setBounds(10, 180, 210, E3TEL.getPreferredSize().height);
                
                // ---- OBJ_69 ----
                OBJ_69.setText("Cat\u00e9gorie");
                OBJ_69.setName("OBJ_69");
                FACTUR.add(OBJ_69);
                OBJ_69.setBounds(210, 7, 58, 20);
                
                // ---- E1CLFP ----
                E1CLFP.setComponentPopupMenu(BTD);
                E1CLFP.setText("@E1CLFP@");
                E1CLFP.setName("E1CLFP");
                FACTUR.add(E1CLFP);
                E1CLFP.setBounds(12, 5, 70, E1CLFP.getPreferredSize().height);
                
                // ---- E3CDPX ----
                E3CDPX.setComponentPopupMenu(BTD);
                E3CDPX.setToolTipText("Code postal");
                E3CDPX.setName("E3CDPX");
                FACTUR.add(E3CDPX);
                E3CDPX.setBounds(10, 130, 60, E3CDPX.getPreferredSize().height);
                
                // ---- WCAT ----
                WCAT.setComponentPopupMenu(BTD);
                WCAT.setText("@WCAT@");
                WCAT.setName("WCAT");
                FACTUR.add(WCAT);
                WCAT.setBounds(278, 5, 40, WCAT.getPreferredSize().height);
                
                // ---- E3COP ----
                E3COP.setComponentPopupMenu(BTD);
                E3COP.setName("E3COP");
                FACTUR.add(E3COP);
                E3COP.setBounds(280, 155, 40, E3COP.getPreferredSize().height);
                
                // ---- E1CLFS ----
                E1CLFS.setComponentPopupMenu(BTD);
                E1CLFS.setText("@E1CLFS@");
                E1CLFS.setName("E1CLFS");
                FACTUR.add(E1CLFS);
                E1CLFS.setBounds(87, 5, 40, E1CLFS.getPreferredSize().height);
                
                // ---- button2 ----
                button2.setContentAreaFilled(false);
                button2.setToolTipText("Le bloc-notes contient des informations");
                button2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                button2.setName("button2");
                button2.addActionListener(e -> button2ActionPerformed(e));
                FACTUR.add(button2);
                button2.setBounds(290, 185, 27, 27);
                
                // ---- riBoutonDetail8 ----
                riBoutonDetail8.setToolTipText("Modification du client sur ce bon");
                riBoutonDetail8.setName("riBoutonDetail8");
                riBoutonDetail8.addActionListener(e -> riBoutonDetail8ActionPerformed(e));
                FACTUR.add(riBoutonDetail8);
                riBoutonDetail8.setBounds(new Rectangle(new Point(135, 8), riBoutonDetail8.getPreferredSize()));
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < FACTUR.getComponentCount(); i++) {
                    Rectangle bounds = FACTUR.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = FACTUR.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  FACTUR.setMinimumSize(preferredSize);
                  FACTUR.setPreferredSize(preferredSize);
                }
              }
              OBJ_65.addTab("Client factur\u00e9", FACTUR);
              
              // ======== LIVRE ========
              {
                LIVRE.setFocusCycleRoot(true);
                LIVRE.setFocusTraversalPolicyProvider(true);
                LIVRE.setOpaque(false);
                LIVRE.setName("LIVRE");
                LIVRE.setLayout(null);
                
                // ---- E2NOM ----
                E2NOM.setComponentPopupMenu(BTD);
                E2NOM.setName("E2NOM");
                LIVRE.add(E2NOM);
                E2NOM.setBounds(10, 30, 310, E2NOM.getPreferredSize().height);
                
                // ---- E2LOC ----
                E2LOC.setComponentPopupMenu(BTD);
                E2LOC.setName("E2LOC");
                LIVRE.add(E2LOC);
                E2LOC.setBounds(10, 105, 310, E2LOC.getPreferredSize().height);
                
                // ---- E2VILN ----
                E2VILN.setComponentPopupMenu(BTD);
                E2VILN.setName("E2VILN");
                LIVRE.add(E2VILN);
                E2VILN.setBounds(70, 130, 250, E2VILN.getPreferredSize().height);
                
                // ---- OBJ_102 ----
                OBJ_102.setText("@WREPAC@");
                OBJ_102.setName("OBJ_102");
                LIVRE.add(OBJ_102);
                OBJ_102.setBounds(59, 105, 210, 20);
                
                // ---- E2PAYN ----
                E2PAYN.setComponentPopupMenu(BTD);
                E2PAYN.setName("E2PAYN");
                LIVRE.add(E2PAYN);
                E2PAYN.setBounds(10, 155, 270, E2PAYN.getPreferredSize().height);
                
                // ---- E2TEL ----
                E2TEL.setComponentPopupMenu(BTD);
                E2TEL.setName("E2TEL");
                LIVRE.add(E2TEL);
                E2TEL.setBounds(10, 180, 210, E2TEL.getPreferredSize().height);
                
                // ---- OBJ_91 ----
                OBJ_91.setText("Cat\u00e9gorie");
                OBJ_91.setName("OBJ_91");
                LIVRE.add(OBJ_91);
                OBJ_91.setBounds(210, 7, 63, 20);
                
                // ---- E1CLLP ----
                E1CLLP.setComponentPopupMenu(BTD);
                E1CLLP.setText("@E1CLLP@");
                E1CLLP.setName("E1CLLP");
                LIVRE.add(E1CLLP);
                E1CLLP.setBounds(12, 5, 70, E1CLLP.getPreferredSize().height);
                
                // ---- E2RUE ----
                E2RUE.setComponentPopupMenu(BTD);
                E2RUE.setName("E2RUE");
                LIVRE.add(E2RUE);
                E2RUE.setBounds(10, 80, 310, E2RUE.getPreferredSize().height);
                
                // ---- E2CDPX ----
                E2CDPX.setComponentPopupMenu(BTD);
                E2CDPX.setToolTipText("Code postal");
                E2CDPX.setName("E2CDPX");
                LIVRE.add(E2CDPX);
                E2CDPX.setBounds(10, 130, 60, E2CDPX.getPreferredSize().height);
                
                // ---- WCAT_ ----
                WCAT_.setComponentPopupMenu(BTD);
                WCAT_.setName("WCAT_");
                LIVRE.add(WCAT_);
                WCAT_.setBounds(278, 5, 40, WCAT_.getPreferredSize().height);
                
                // ---- E2COP ----
                E2COP.setComponentPopupMenu(BTD);
                E2COP.setName("E2COP");
                LIVRE.add(E2COP);
                E2COP.setBounds(280, 155, 40, E2COP.getPreferredSize().height);
                
                // ---- E1CLLS ----
                E1CLLS.setComponentPopupMenu(BTD);
                E1CLLS.setText("@E1CLLS@");
                E1CLLS.setName("E1CLLS");
                LIVRE.add(E1CLLS);
                E1CLLS.setBounds(87, 5, 40, E1CLLS.getPreferredSize().height);
                
                // ---- E2CPL ----
                E2CPL.setComponentPopupMenu(BTD);
                E2CPL.setName("E2CPL");
                LIVRE.add(E2CPL);
                E2CPL.setBounds(10, 55, 310, E2CPL.getPreferredSize().height);
                
                // ---- riBoutonDetail6 ----
                riBoutonDetail6.setToolTipText("D\u00e9tails client livr\u00e9");
                riBoutonDetail6.setName("riBoutonDetail6");
                riBoutonDetail6.addActionListener(e -> riBoutonDet6ActionPerformed(e));
                LIVRE.add(riBoutonDetail6);
                riBoutonDetail6.setBounds(new Rectangle(new Point(135, 8), riBoutonDetail6.getPreferredSize()));
                
                // ---- riBoutonDetail7 ----
                riBoutonDetail7.setName("riBoutonDetail7");
                riBoutonDetail7.addActionListener(e -> riBoutonDetail7ActionPerformed(e));
                LIVRE.add(riBoutonDetail7);
                riBoutonDetail7.setBounds(new Rectangle(new Point(60, 61), riBoutonDetail7.getPreferredSize()));
                
                // ---- button3 ----
                button3.setContentAreaFilled(false);
                button3.setToolTipText("Le bloc-notes contient des informations");
                button3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                button3.setName("button3");
                button3.addActionListener(e -> button3ActionPerformed(e));
                LIVRE.add(button3);
                button3.setBounds(290, 181, 27, 27);
                
                { // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for (int i = 0; i < LIVRE.getComponentCount(); i++) {
                    Rectangle bounds = LIVRE.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = LIVRE.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  LIVRE.setMinimumSize(preferredSize);
                  LIVRE.setPreferredSize(preferredSize);
                }
              }
              OBJ_65.addTab("Client livr\u00e9", LIVRE);
            }
            panel2.add(OBJ_65);
            OBJ_65.setBounds(0, 1, 330, 245);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(25, 15, 335, panel2.getPreferredSize().height);
          
          // ---- OBJ_60 ----
          OBJ_60.setText("@WATN@");
          OBJ_60.setForeground(Color.red);
          OBJ_60.setOpaque(false);
          OBJ_60.setName("OBJ_60");
          p_contenu.add(OBJ_60);
          OBJ_60.setBounds(779, 53, 185, OBJ_60.getPreferredSize().height);
          
          // ---- OBJ_85 ----
          OBJ_85.setText("Type de vente");
          OBJ_85.setName("OBJ_85");
          p_contenu.add(OBJ_85);
          OBJ_85.setBounds(780, 82, 90, 20);
          
          // ---- E1IN17 ----
          E1IN17.setComponentPopupMenu(BTD);
          E1IN17.setToolTipText("Type de vente");
          E1IN17.setName("E1IN17");
          p_contenu.add(E1IN17);
          E1IN17.setBounds(870, 78, 24, E1IN17.getPreferredSize().height);
          
          // ---- TE1IN17 ----
          TE1IN17.setText("R\u00e9percussion type sur lignes");
          TE1IN17.setToolTipText("R\u00e9percussion du type de vente sur les lignes existantes");
          TE1IN17.setName("TE1IN17");
          p_contenu.add(TE1IN17);
          TE1IN17.setBounds(780, 105, 200, 25);
          
          // ======== panel1 ========
          {
            panel1.setBorder(new DropShadowBorder());
            panel1.setOpaque(false);
            panel1.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- riBoutonDetail9 ----
            riBoutonDetail9.setToolTipText("Choix des coordonn\u00e9es d'envoi");
            riBoutonDetail9.setName("riBoutonDetail9");
            riBoutonDetail9.addActionListener(e -> riBoutonDetail1ActionPerformed(e));
            panel1.add(riBoutonDetail9);
            riBoutonDetail9.setBounds(495, 3, 31, 28);
            
            // ---- label22 ----
            label22.setText("Contact");
            label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
            label22.setName("label22");
            panel1.add(label22);
            label22.setBounds(10, 5, 60, 24);
            
            // ---- LMAIL2 ----
            LMAIL2.setText("@LPAC@");
            LMAIL2.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
            LMAIL2.setName("LMAIL2");
            panel1.add(LMAIL2);
            LMAIL2.setBounds(60, 5, 237, LMAIL2.getPreferredSize().height);
            
            // ---- label21 ----
            label21.setText("T\u00e9l\u00e9phone");
            label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
            label21.setName("label21");
            panel1.add(label21);
            label21.setBounds(300, 5, 80, 24);
            
            // ---- LFAX2 ----
            LFAX2.setText("@LTEL@");
            LFAX2.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
            LFAX2.setName("LFAX2");
            panel1.add(LFAX2);
            LFAX2.setBounds(365, 5, 130, LFAX2.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(455, 550, 530, 39);
          
          // ---- OBJ_86 ----
          OBJ_86.setText("Chantier");
          OBJ_86.setName("OBJ_86");
          p_contenu.add(OBJ_86);
          OBJ_86.setBounds(780, 30, 90, 20);
          
          // ---- riBoutonDetailListe1 ----
          riBoutonDetailListe1.setName("riBoutonDetailListe1");
          riBoutonDetailListe1.addActionListener(e -> riBoutonDetailListe1ActionPerformed(e));
          p_contenu.add(riBoutonDetailListe1);
          riBoutonDetailListe1.setBounds(870, 25, 25, 28);
          
          // ---- zs_LAVOIR ----
          zs_LAVOIR.setText("@LAVOIR@");
          zs_LAVOIR.setName("zs_LAVOIR");
          p_contenu.add(zs_LAVOIR);
          zs_LAVOIR.setBounds(375, 5, 400, zs_LAVOIR.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(e -> OBJ_21ActionPerformed(e));
      BTD.add(OBJ_21);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("M\u00e9morisation curseur");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(e -> OBJ_12ActionPerformed(e));
      BTD.add(OBJ_12);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(e -> OBJ_20ActionPerformed(e));
      BTD.add(OBJ_20);
    }
    
    // ---- OBJ_153 ----
    OBJ_153.setIcon(new ImageIcon("images/aeuro.gif"));
    OBJ_153.setToolTipText("Vous travaillez en euros...");
    OBJ_153.setName("OBJ_153");
    
    // ---- E2LOC_doublon_35 ----
    E2LOC_doublon_35.setComponentPopupMenu(BTD);
    E2LOC_doublon_35.setName("E2LOC_doublon_35");
    
    // ---- E1PFC ----
    E1PFC.setComponentPopupMenu(BTD);
    E1PFC.setName("E1PFC");
    
    // ---- OBJ_113 ----
    OBJ_113.setText("@TYPE@");
    OBJ_113.setName("OBJ_113");
    
    // ---- OBJ_117 ----
    OBJ_117.setText("@BONN@");
    OBJ_117.setName("OBJ_117");
    
    // ---- OBJ_116 ----
    OBJ_116.setText("Traitement de fin diff\u00e9r\u00e9");
    OBJ_116.setName("OBJ_116");
    
    // ---- OBJ_164 ----
    OBJ_164.setText("Validit\u00e9");
    OBJ_164.setToolTipText("<HTML>Choix d'une date gr\u00e2ce<BR>@&L000285@</HTML>");
    OBJ_164.setName("OBJ_164");
    
    // ---- E1DT2X ----
    E1DT2X.setComponentPopupMenu(BTD);
    E1DT2X.setName("E1DT2X");
    
    // ---- OBJ_232 ----
    OBJ_232.setText("Colis");
    OBJ_232.setName("OBJ_232");
    
    // ---- bt_encours ----
    bt_encours.setToolTipText("Encours");
    bt_encours.setName("bt_encours");
    bt_encours.addActionListener(e -> bt_encoursActionPerformed(e));
    
    // ---- bt_reglement ----
    bt_reglement.setToolTipText("R\u00e9glements");
    bt_reglement.setName("bt_reglement");
    bt_reglement.addActionListener(e -> riBoutonDetail6ActionPerformed(e));
    
    // ---- bt_condition ----
    bt_condition.setToolTipText("Conditions");
    bt_condition.setName("bt_condition");
    bt_condition.addActionListener(e -> bt_conditionActionPerformed(e));
    
    // ---- OBJ_5 ----
    OBJ_5.setText("Chiffrage/Traitements diff\u00e9r\u00e9s");
    OBJ_5.setName("OBJ_5");
    OBJ_5.addActionListener(e -> OBJ_5ActionPerformed(e));
    
    // ---- riBoutonDetail1 ----
    riBoutonDetail1.setToolTipText("Poids");
    riBoutonDetail1.setName("riBoutonDetail1");
    riBoutonDetail1.addActionListener(e -> riBoutonDetail1ActionPerformed(e));
    
    // ======== riSousMenu4 ========
    {
      riSousMenu4.setName("riSousMenu4");
      
      // ---- riSousMenu_bt4 ----
      riSousMenu_bt4.setText("Extensions d'ent\u00eate");
      riSousMenu_bt4.setToolTipText("Extensions d'ent\u00eate");
      riSousMenu_bt4.setName("riSousMenu_bt4");
      riSousMenu_bt4.addActionListener(e -> riSousMenu_bt4ActionPerformed(e));
      riSousMenu4.add(riSousMenu_bt4);
    }
    
    // ---- button1 ----
    button1.setText("pied de bons : ne pas supprimer");
    button1.setName("button1");
    button1.addActionListener(e -> button1ActionPerformed(e));
    
    // ---- E1IN9_ ----
    E1IN9_.setComponentPopupMenu(null);
    E1IN9_.setName("E1IN9_");
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Choix du chantier");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(e -> OBJ_22ActionPerformed(e));
      BTD2.add(OBJ_22);
    }
    
    // ======== riSousMenu1 ========
    {
      riSousMenu1.setName("riSousMenu1");
      
      // ---- riSousMenu_bt1 ----
      riSousMenu_bt1.setText("Recherche de bons");
      riSousMenu_bt1.setToolTipText("Fonction recherche de bons");
      riSousMenu_bt1.setName("riSousMenu_bt1");
      riSousMenu_bt1.addActionListener(e -> riSousMenu_bt1ActionPerformed(e));
      riSousMenu1.add(riSousMenu_bt1);
    }
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie E1NFA;
  private RiZoneSortie E1ETB;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private RiZoneSortie WSUF;
  private RiZoneSortie WNUM;
  private RiZoneSortie OBJ_54;
  private JLabel label15;
  private XRiTextField E1VDE;
  private JLabel OBJ_61;
  private XRiTextField E1PRE;
  private JLabel OBJ_31;
  private JLabel OBJ_50;
  private JPanel p_tete_droite;
  private JLabel bt_ecran;
  private RiZoneSortie OBJ_129;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private XRiTextField E1RCC;
  private XRiTextField E1CCT;
  private JLabel OBJ_145;
  private XRiTextField E1NCC;
  private XRiTextField E1TP1;
  private XRiTextField E1TP2;
  private XRiTextField E1TP3;
  private XRiTextField E1TP4;
  private XRiTextField E1TP5;
  private JLabel OBJ_192;
  private JLabel OBJ_193;
  private JLabel OBJ_194;
  private JLabel OBJ_195;
  private JLabel OBJ_196;
  private JLabel OBJ_210;
  private XRiTextField E1SAN;
  private XRiTextField E1ACT;
  private JLabel OBJ_212;
  private XRiTextField E20NAT;
  private JLabel OBJ_214;
  private JLabel OBJ_84;
  private RiZoneSortie E1CAN;
  private JLabel label3;
  private RiZoneSortie RPLI1G;
  private XRiTextField E1REP;
  private XRiTextField E1TRP;
  private JLabel label5;
  private RiZoneSortie WOBS;
  private JLabel OBJ_257;
  private XRiTextField E1NEX;
  private JLabel label6;
  private XRiTextField E1MAG;
  private RiZoneSortie label7;
  private SNBoutonDetail riBoutonDetail2;
  private JPanel panelChantier;
  private JLabel label14;
  private XRiTextField WCHANT;
  private JLabel lbrefCourte;
  private JLabel lbrefLongue;
  private JXTitledPanel p_encours;
  private JLayeredPane layeredPane1;
  private JPanel p_impaye;
  private JLabel OBJ_157;
  private JPanel p_paye;
  private SNLabelChamp lb_depassement;
  private RiZoneSortie WEPOS;
  private RiZoneSortie WEPLF;
  private RiZoneSortie WEDEP;
  private JLabel OBJ_162;
  private JLabel OBJ_163;
  private JXTitledPanel xTitledPanel4;
  private XRiCalendrier E1DLSX;
  private XRiCalendrier E1DLPX;
  private JLabel OBJ_169;
  private JLabel OBJ_161;
  private XRiCalendrier E1DCOX;
  private RiZoneSortie E1HRE1;
  private RiZoneSortie E1HRE2;
  private JLabel OBJ_159;
  private JLabel OBJ_245;
  private XRiCalendrier E1DT1X;
  private JLabel label8;
  private JLabel label9;
  private SNBoutonDetail riBoutonDetail4;
  private SNBoutonDetail riBoutonDetail5;
  private JXTitledPanel p_exped;
  private RiZoneSortie OBJ_227;
  private RiZoneSortie OBJ_225;
  private XRiTextField E1VEH;
  private XRiTextField E1PDS;
  private XRiTextField E1ZTR;
  private RiZoneSortie WMFRP;
  private JLabel OBJ_235;
  private XRiTextField E1CTR;
  private XRiTextField E1MEX;
  private JLabel OBJ_271;
  private JLabel OBJ_190;
  private JLabel OBJ_191;
  private XRiTextField E1DPR;
  private JLabel OBJ_203;
  private XRiTextField E1IN7;
  private JLabel OBJ_272;
  private JLabel label1;
  private JLabel label2;
  private SNBoutonLeger OBJ_240;
  private XRiComboBox E1TRC;
  private JLabel label10;
  private JLabel label11;
  private SNBoutonDetail riBoutonDetail3;
  private JLabel label12;
  private XRiTextField E1COL;
  private JLabel label13;
  private XRiComboBox E1IN9;
  private XRiTextField E1CRT;
  private JLabel label18;
  private RiZoneSortie TOLIB;
  private JXTitledPanel p_condition;
  private JLabel OBJ_277;
  private XRiTextField E1CNV;
  private RiZoneSortie WCNP;
  private XRiTextField E1REM1;
  private XRiTextField E1REM2;
  private XRiTextField E1REM3;
  private XRiTextField E1ESCX;
  private JLabel OBJ_276;
  private JLabel OBJ_274;
  private XRiSpinner E1TAR;
  private XRiComboBox E1PGC;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel label4;
  private JXTitledPanel p_reglement;
  private XRiTextField E1RG2;
  private JLabel OBJ_265;
  private RiZoneSortie LRG12;
  private JLabel OBJ_123;
  private RiZoneSortie lib_Devise;
  private XRiTextField E1CHGX;
  private XRiTextField E1BAS;
  private JLabel label16;
  private JLabel label17;
  private JPanel panel2;
  private JTabbedPane OBJ_65;
  private JPanel FACTUR;
  private XRiTextField E3NOM;
  private XRiTextField E3CPL;
  private XRiTextField E3RUE;
  private XRiTextField E3LOC;
  private XRiTextField E3VILN;
  private XRiTextField E3PAYN;
  private XRiTextField E3TEL;
  private JLabel OBJ_69;
  private RiZoneSortie E1CLFP;
  private XRiTextField E3CDPX;
  private RiZoneSortie WCAT;
  private XRiTextField E3COP;
  private RiZoneSortie E1CLFS;
  private JButton button2;
  private SNBoutonDetail riBoutonDetail8;
  private JPanel LIVRE;
  private XRiTextField E2NOM;
  private XRiTextField E2LOC;
  private XRiTextField E2VILN;
  private JLabel OBJ_102;
  private XRiTextField E2PAYN;
  private XRiTextField E2TEL;
  private JLabel OBJ_91;
  private RiZoneSortie E1CLLP;
  private XRiTextField E2RUE;
  private XRiTextField E2CDPX;
  private RiZoneSortie WCAT_;
  private XRiTextField E2COP;
  private RiZoneSortie E1CLLS;
  private XRiTextField E2CPL;
  private SNBoutonDetail riBoutonDetail6;
  private SNBoutonDetail riBoutonDetail7;
  private JButton button3;
  private RiZoneSortie OBJ_60;
  private JLabel OBJ_85;
  private XRiTextField E1IN17;
  private XRiCheckBox TE1IN17;
  private JPanel panel1;
  private SNBoutonDetail riBoutonDetail9;
  private JLabel label22;
  private RiZoneSortie LMAIL2;
  private JLabel label21;
  private RiZoneSortie LFAX2;
  private JLabel OBJ_86;
  private SNBoutonDetail riBoutonDetailListe1;
  private RiZoneSortie zs_LAVOIR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_20;
  private JLabel OBJ_153;
  private XRiTextField E2LOC_doublon_35;
  private XRiTextField E1PFC;
  private JLabel OBJ_113;
  private JLabel OBJ_117;
  private JLabel OBJ_116;
  private JLabel OBJ_164;
  private XRiCalendrier E1DT2X;
  private JLabel OBJ_232;
  private SNBoutonDetail bt_encours;
  private SNBoutonDetail bt_reglement;
  private SNBoutonDetail bt_condition;
  private JMenuItem OBJ_5;
  private SNBoutonDetail riBoutonDetail1;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private JButton button1;
  private XRiTextField E1IN9_;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_22;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
