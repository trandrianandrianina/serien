
package ri.serien.libecranrpg.vgvm.VGVM01CA;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01CA_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01CA_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    article1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCOA1@")).trim());
    article2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCOA2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    CVJOA2.setEnabled(lexique.isPresent("CVJOA2"));
    CVJOA1.setEnabled(lexique.isPresent("CVJOA1"));
    CVCOA2.setEnabled(lexique.isPresent("CVCOA2"));
    CVCOA1.setEnabled(lexique.isPresent("CVCOA1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("PERSONNALISATION"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_20 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_24 = new JLabel();
    CVCOA1 = new XRiTextField();
    CVJOA1 = new XRiTextField();
    article1 = new RiZoneSortie();
    panel3 = new JPanel();
    OBJ_31 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_28 = new JLabel();
    CVCOA2 = new XRiTextField();
    CVJOA2 = new XRiTextField();
    article2 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(850, 275));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setTitle("Comptabilisation des ventes par anticipation");
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
          xTitledPanel3ContentContainer.setLayout(null);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Bons Homologu\u00e9s"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_20 ----
            OBJ_20.setText("Libell\u00e9");
            OBJ_20.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_20.setName("OBJ_20");
            panel2.add(OBJ_20);
            OBJ_20.setBounds(175, 30, 61, 18);

            //---- OBJ_37 ----
            OBJ_37.setText("Compte");
            OBJ_37.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_37.setName("OBJ_37");
            panel2.add(OBJ_37);
            OBJ_37.setBounds(125, 30, 61, 18);

            //---- OBJ_21 ----
            OBJ_21.setText("N\u00b0CO");
            OBJ_21.setName("OBJ_21");
            panel2.add(OBJ_21);
            OBJ_21.setBounds(15, 55, 36, 20);

            //---- OBJ_24 ----
            OBJ_24.setText("Code Journal");
            OBJ_24.setName("OBJ_24");
            panel2.add(OBJ_24);
            OBJ_24.setBounds(495, 55, 84, 20);

            //---- CVCOA1 ----
            CVCOA1.setComponentPopupMenu(BTD);
            CVCOA1.setName("CVCOA1");
            panel2.add(CVCOA1);
            CVCOA1.setBounds(55, 51, 36, CVCOA1.getPreferredSize().height);

            //---- CVJOA1 ----
            CVJOA1.setComponentPopupMenu(BTD);
            CVJOA1.setName("CVJOA1");
            panel2.add(CVJOA1);
            CVJOA1.setBounds(580, 51, 30, CVJOA1.getPreferredSize().height);

            //---- article1 ----
            article1.setText("@LCOA1@");
            article1.setFont(new Font("Courier New", Font.PLAIN, 12));
            article1.setBackground(new Color(235, 230, 235));
            article1.setOpaque(false);
            article1.setVerticalAlignment(SwingConstants.BOTTOM);
            article1.setName("article1");
            panel2.add(article1);
            article1.setBounds(125, 51, 324, article1.getPreferredSize().height);
          }
          xTitledPanel3ContentContainer.add(panel2);
          panel2.setBounds(10, 10, 630, 95);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Bons Exp\u00e9di\u00e9s"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_31 ----
            OBJ_31.setText("Code Journal");
            OBJ_31.setName("OBJ_31");
            panel3.add(OBJ_31);
            OBJ_31.setBounds(495, 55, 84, 20);

            //---- OBJ_27 ----
            OBJ_27.setText("Libell\u00e9");
            OBJ_27.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_27.setName("OBJ_27");
            panel3.add(OBJ_27);
            OBJ_27.setBounds(180, 30, 61, 18);

            //---- OBJ_38 ----
            OBJ_38.setText("Compte");
            OBJ_38.setFont(new Font("Courier New", Font.BOLD, 12));
            OBJ_38.setName("OBJ_38");
            panel3.add(OBJ_38);
            OBJ_38.setBounds(130, 30, 61, 18);

            //---- OBJ_28 ----
            OBJ_28.setText("N\u00b0CO");
            OBJ_28.setName("OBJ_28");
            panel3.add(OBJ_28);
            OBJ_28.setBounds(15, 55, 36, 20);

            //---- CVCOA2 ----
            CVCOA2.setComponentPopupMenu(BTD);
            CVCOA2.setName("CVCOA2");
            panel3.add(CVCOA2);
            CVCOA2.setBounds(55, 51, 36, CVCOA2.getPreferredSize().height);

            //---- CVJOA2 ----
            CVJOA2.setComponentPopupMenu(BTD);
            CVJOA2.setName("CVJOA2");
            panel3.add(CVJOA2);
            CVJOA2.setBounds(580, 51, 30, CVJOA2.getPreferredSize().height);

            //---- article2 ----
            article2.setText("@LCOA2@");
            article2.setFont(new Font("Courier New", Font.PLAIN, 12));
            article2.setBackground(new Color(235, 230, 235));
            article2.setOpaque(false);
            article2.setVerticalAlignment(SwingConstants.BOTTOM);
            article2.setName("article2");
            panel3.add(article2);
            article2.setBounds(130, 51, 324, article2.getPreferredSize().height);
          }
          xTitledPanel3ContentContainer.add(panel3);
          panel3.setBounds(10, 110, 630, 95);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel3ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(15, 15, 650, 245);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private JPanel panel2;
  private JLabel OBJ_20;
  private JLabel OBJ_37;
  private JLabel OBJ_21;
  private JLabel OBJ_24;
  private XRiTextField CVCOA1;
  private XRiTextField CVJOA1;
  private RiZoneSortie article1;
  private JPanel panel3;
  private JLabel OBJ_31;
  private JLabel OBJ_27;
  private JLabel OBJ_38;
  private JLabel OBJ_28;
  private XRiTextField CVCOA2;
  private XRiTextField CVJOA2;
  private RiZoneSortie article2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
