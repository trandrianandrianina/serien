
package ri.serien.libecranrpg.vgvm.VGVM141F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM141F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM141F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    EDTCOM.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    RGR0.setEnabled(lexique.isPresent("RGR0"));
    ECNLI.setEnabled(lexique.isPresent("ECNLI"));
    FONLF.setEnabled(lexique.isPresent("FONLF"));
    FONLI.setEnabled(lexique.isPresent("FONLI"));
    ECMTT.setEnabled(lexique.isPresent("ECMTT"));
    FOMTT.setEnabled(lexique.isPresent("FOMTT"));
    TOTMHT.setEnabled(lexique.isPresent("TOTMHT"));
    FOART.setEnabled(lexique.isPresent("FOART"));
    ECART.setEnabled(lexique.isPresent("ECART"));
    // EDTCOM.setEnabled( lexique.isPresent("EDTCOM"));
    // EDTCOM.setSelected(lexique.HostFieldGetData("EDTCOM").equalsIgnoreCase("1"));
    ECLB4.setEnabled(lexique.isPresent("ECLB4"));
    ECLB3.setEnabled(lexique.isPresent("ECLB3"));
    ECLB2.setEnabled(lexique.isPresent("ECLB2"));
    ECLB1.setEnabled(lexique.isPresent("ECLB1"));
    FOLB4.setEnabled(lexique.isPresent("FOLB4"));
    FOLB3.setEnabled(lexique.isPresent("FOLB3"));
    FOLB2.setEnabled(lexique.isPresent("FOLB2"));
    FOLB1.setEnabled(lexique.isPresent("FOLB1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion d'un forfait commentaire avec ligne d'écart"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (EDTCOM.isSelected())
    // lexique.HostFieldPutData("EDTCOM", 0, "1");
    // else
    // lexique.HostFieldPutData("EDTCOM", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    FOLB1 = new XRiTextField();
    FOLB2 = new XRiTextField();
    FOLB3 = new XRiTextField();
    FOLB4 = new XRiTextField();
    EDTCOM = new XRiCheckBox();
    FOART = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_29 = new JLabel();
    FONLI = new XRiTextField();
    FONLF = new XRiTextField();
    OBJ_24 = new JLabel();
    RGR0 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_26 = new JLabel();
    TOTMHT = new XRiTextField();
    FOMTT = new XRiTextField();
    OBJ_28 = new JLabel();
    panel3 = new JPanel();
    ECLB1 = new XRiTextField();
    ECLB2 = new XRiTextField();
    ECLB3 = new XRiTextField();
    ECLB4 = new XRiTextField();
    ECART = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    ECMTT = new XRiTextField();
    OBJ_51 = new JLabel();
    ECNLI = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Ligne forfait"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- FOLB1 ----
          FOLB1.setComponentPopupMenu(BTD);
          FOLB1.setName("FOLB1");
          panel1.add(FOLB1);
          FOLB1.setBounds(220, 65, 310, FOLB1.getPreferredSize().height);

          //---- FOLB2 ----
          FOLB2.setComponentPopupMenu(BTD);
          FOLB2.setName("FOLB2");
          panel1.add(FOLB2);
          FOLB2.setBounds(220, 95, 310, FOLB2.getPreferredSize().height);

          //---- FOLB3 ----
          FOLB3.setComponentPopupMenu(BTD);
          FOLB3.setName("FOLB3");
          panel1.add(FOLB3);
          FOLB3.setBounds(220, 125, 310, FOLB3.getPreferredSize().height);

          //---- FOLB4 ----
          FOLB4.setComponentPopupMenu(BTD);
          FOLB4.setName("FOLB4");
          panel1.add(FOLB4);
          FOLB4.setBounds(220, 155, 310, FOLB4.getPreferredSize().height);

          //---- EDTCOM ----
          EDTCOM.setText("Edition des commentaires");
          EDTCOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTCOM.setName("EDTCOM");
          panel1.add(EDTCOM);
          EDTCOM.setBounds(220, 185, 208, 20);

          //---- FOART ----
          FOART.setComponentPopupMenu(BTD);
          FOART.setName("FOART");
          panel1.add(FOART);
          FOART.setBounds(300, 35, 110, FOART.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Ligne forfait");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(30, 39, 80, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("Code article");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(220, 39, 84, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Ligne de fin");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(30, 69, 82, 20);

          //---- FONLI ----
          FONLI.setComponentPopupMenu(BTD);
          FONLI.setName("FONLI");
          panel1.add(FONLI);
          FONLI.setBounds(110, 35, 39, FONLI.getPreferredSize().height);

          //---- FONLF ----
          FONLF.setComponentPopupMenu(BTD);
          FONLF.setName("FONLF");
          panel1.add(FONLF);
          FONLF.setBounds(110, 65, 42, FONLF.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("Rgr");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(470, 39, 25, 20);

          //---- RGR0 ----
          RGR0.setComponentPopupMenu(BTD);
          RGR0.setName("RGR0");
          panel1.add(RGR0);
          RGR0.setBounds(510, 35, 20, RGR0.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Montant Total H.T"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_26 ----
            OBJ_26.setText("R\u00e9el");
            OBJ_26.setName("OBJ_26");
            panel2.add(OBJ_26);
            OBJ_26.setBounds(15, 34, 58, 20);

            //---- TOTMHT ----
            TOTMHT.setComponentPopupMenu(BTD);
            TOTMHT.setName("TOTMHT");
            panel2.add(TOTMHT);
            TOTMHT.setBounds(95, 30, 73, TOTMHT.getPreferredSize().height);

            //---- FOMTT ----
            FOMTT.setComponentPopupMenu(BTD);
            FOMTT.setName("FOMTT");
            panel2.add(FOMTT);
            FOMTT.setBounds(95, 60, 90, FOMTT.getPreferredSize().height);

            //---- OBJ_28 ----
            OBJ_28.setText("Forfait");
            OBJ_28.setName("OBJ_28");
            panel2.add(OBJ_28);
            OBJ_28.setBounds(15, 64, 58, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(15, 95, 200, 100);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Ligne d'\u00e9cart"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- ECLB1 ----
          ECLB1.setComponentPopupMenu(BTD);
          ECLB1.setName("ECLB1");
          panel3.add(ECLB1);
          ECLB1.setBounds(220, 65, 310, ECLB1.getPreferredSize().height);

          //---- ECLB2 ----
          ECLB2.setComponentPopupMenu(BTD);
          ECLB2.setName("ECLB2");
          panel3.add(ECLB2);
          ECLB2.setBounds(220, 95, 310, ECLB2.getPreferredSize().height);

          //---- ECLB3 ----
          ECLB3.setComponentPopupMenu(BTD);
          ECLB3.setName("ECLB3");
          panel3.add(ECLB3);
          ECLB3.setBounds(220, 125, 310, ECLB3.getPreferredSize().height);

          //---- ECLB4 ----
          ECLB4.setComponentPopupMenu(BTD);
          ECLB4.setName("ECLB4");
          panel3.add(ECLB4);
          ECLB4.setBounds(220, 155, 310, ECLB4.getPreferredSize().height);

          //---- ECART ----
          ECART.setComponentPopupMenu(BTD);
          ECART.setName("ECART");
          panel3.add(ECART);
          ECART.setBounds(300, 35, 110, ECART.getPreferredSize().height);

          //---- OBJ_48 ----
          OBJ_48.setText("Code Article");
          OBJ_48.setName("OBJ_48");
          panel3.add(OBJ_48);
          OBJ_48.setBounds(220, 39, 85, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("Ligne d'\u00e9cart");
          OBJ_49.setName("OBJ_49");
          panel3.add(OBJ_49);
          OBJ_49.setBounds(30, 39, 85, 20);

          //---- ECMTT ----
          ECMTT.setComponentPopupMenu(BTD);
          ECMTT.setName("ECMTT");
          panel3.add(ECMTT);
          ECMTT.setBounds(110, 65, 73, ECMTT.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Ecart");
          OBJ_51.setName("OBJ_51");
          panel3.add(OBJ_51);
          OBJ_51.setBounds(30, 69, 52, 20);

          //---- ECNLI ----
          ECNLI.setComponentPopupMenu(BTD);
          ECNLI.setName("ECNLI");
          panel3.add(ECNLI);
          ECNLI.setBounds(110, 35, 39, ECNLI.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField FOLB1;
  private XRiTextField FOLB2;
  private XRiTextField FOLB3;
  private XRiTextField FOLB4;
  private XRiCheckBox EDTCOM;
  private XRiTextField FOART;
  private JLabel OBJ_21;
  private JLabel OBJ_23;
  private JLabel OBJ_29;
  private XRiTextField FONLI;
  private XRiTextField FONLF;
  private JLabel OBJ_24;
  private XRiTextField RGR0;
  private JPanel panel2;
  private JLabel OBJ_26;
  private XRiTextField TOTMHT;
  private XRiTextField FOMTT;
  private JLabel OBJ_28;
  private JPanel panel3;
  private XRiTextField ECLB1;
  private XRiTextField ECLB2;
  private XRiTextField ECLB3;
  private XRiTextField ECLB4;
  private XRiTextField ECART;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private XRiTextField ECMTT;
  private JLabel OBJ_51;
  private XRiTextField ECNLI;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
