
package ri.serien.libecranrpg.vgvm.VGVM04FM;
// Nom Fichier: pop_VGVM04FM_FMTA3_1062.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM04FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM04FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
    
    // Bouton par défaut
    setDefaultButton(OBJ_23);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ARG2.setEnabled(lexique.isPresent("ARG2"));
    ARG1.setEnabled(lexique.isPresent("ARG1"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX2.setVisible( lexique.isPresent("RB"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    
    // TODO Icones
    OBJ_23.setIcon(lexique.chargerImage("images/retour.png", true));
    OBJ_22.setIcon(lexique.chargerImage("images/OK.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    TIDX2 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    ARG1 = new XRiTextField();
    ARG2 = new XRiTextField();
    panel1 = new JPanel();
    OBJ_23 = new JButton();
    OBJ_22 = new JButton();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- TIDX2 ----
      TIDX2.setText("Code Alphab\u00e9tique");
      TIDX2.setComponentPopupMenu(BTD);
      TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX2.setName("TIDX2");
      panel2.add(TIDX2);
      TIDX2.setBounds(20, 39, 137, 20);

      //---- TIDX1 ----
      TIDX1.setText("Num\u00e9ro Client");
      TIDX1.setComponentPopupMenu(BTD);
      TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX1.setName("TIDX1");
      panel2.add(TIDX1);
      TIDX1.setBounds(20, 69, 105, 20);

      //---- ARG1 ----
      ARG1.setComponentPopupMenu(BTD);
      ARG1.setName("ARG1");
      panel2.add(ARG1);
      ARG1.setBounds(180, 65, 58, ARG1.getPreferredSize().height);

      //---- ARG2 ----
      ARG2.setComponentPopupMenu(BTD);
      ARG2.setName("ARG2");
      panel2.add(ARG2);
      ARG2.setBounds(180, 35, 30, ARG2.getPreferredSize().height);
    }
    add(panel2);
    panel2.setBounds(15, 10, 255, 115);

    //======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_23 ----
      OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_23.setName("OBJ_23");
      panel1.add(OBJ_23);
      OBJ_23.setBounds(70, 0, 56, 40);

      //---- OBJ_22 ----
      OBJ_22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      panel1.add(OBJ_22);
      OBJ_22.setBounds(10, 0, 56, 40);
    }
    add(panel1);
    panel1.setBounds(145, 130, 125, 45);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX1;
  private XRiTextField ARG1;
  private XRiTextField ARG2;
  private JPanel panel1;
  private JButton OBJ_23;
  private JButton OBJ_22;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
