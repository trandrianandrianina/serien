
package ri.serien.libecranrpg.vgvm.VGVM03FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.JXTitledSeparator;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class ZP_PANEL extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public ZP_PANEL(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(OBJ_10);
    
    initDiverses();
    
    
    OBJ_10.setIcon(lexique.chargerImage("images/OK_p.png", true));
    
    // Titre
    setTitle("Zones personnalisées");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    CLTOP1.forceVisibility();
    CLTOP2.forceVisibility();
    CLTOP3.forceVisibility();
    CLTOP4.forceVisibility();
    CLTOP5.forceVisibility();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    p_principal = new JPanel();
    OBJ_13 = new JXTitledSeparator();
    label1 = new JLabel();
    label4 = new JLabel();
    label3 = new JLabel();
    label2 = new JLabel();
    label5 = new JLabel();
    CLTOP1 = new XRiTextField();
    CLTOP2 = new XRiTextField();
    CLTOP3 = new XRiTextField();
    CLTOP4 = new XRiTextField();
    CLTOP5 = new XRiTextField();
    OBJ_10 = new JButton();

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);
    }

    //======== this ========
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setBackground(new Color(90, 90, 90));
      p_principal.setName("p_principal");
      p_principal.setLayout(null);

      //---- OBJ_13 ----
      OBJ_13.setTitle("Zones personnalis\u00e9es");
      OBJ_13.setForeground(Color.white);
      OBJ_13.setName("OBJ_13");
      p_principal.add(OBJ_13);
      OBJ_13.setBounds(15, 10, 295, 20);

      //---- label1 ----
      label1.setText("@WTI1@");
      label1.setForeground(Color.white);
      label1.setName("label1");
      p_principal.add(label1);
      label1.setBounds(40, 70, 25, 20);

      //---- label4 ----
      label4.setText("@WTI4@");
      label4.setForeground(Color.white);
      label4.setName("label4");
      p_principal.add(label4);
      label4.setBounds(40, 105, 25, 20);

      //---- label3 ----
      label3.setText("@WTI3@");
      label3.setForeground(Color.white);
      label3.setName("label3");
      p_principal.add(label3);
      label3.setBounds(235, 70, 25, 20);

      //---- label2 ----
      label2.setText("@WTI2@");
      label2.setForeground(Color.white);
      label2.setName("label2");
      p_principal.add(label2);
      label2.setBounds(130, 70, 25, 20);

      //---- label5 ----
      label5.setText("@WTI5@");
      label5.setForeground(Color.white);
      label5.setName("label5");
      p_principal.add(label5);
      label5.setBounds(130, 105, 25, 20);

      //---- CLTOP1 ----
      CLTOP1.setComponentPopupMenu(BTD);
      CLTOP1.setName("CLTOP1");
      p_principal.add(CLTOP1);
      CLTOP1.setBounds(68, 66, 36, CLTOP1.getPreferredSize().height);

      //---- CLTOP2 ----
      CLTOP2.setComponentPopupMenu(BTD);
      CLTOP2.setName("CLTOP2");
      p_principal.add(CLTOP2);
      CLTOP2.setBounds(160, 66, 36, CLTOP2.getPreferredSize().height);

      //---- CLTOP3 ----
      CLTOP3.setComponentPopupMenu(BTD);
      CLTOP3.setName("CLTOP3");
      p_principal.add(CLTOP3);
      CLTOP3.setBounds(260, 66, 36, CLTOP3.getPreferredSize().height);

      //---- CLTOP4 ----
      CLTOP4.setComponentPopupMenu(BTD);
      CLTOP4.setName("CLTOP4");
      p_principal.add(CLTOP4);
      CLTOP4.setBounds(68, 101, 36, CLTOP4.getPreferredSize().height);

      //---- CLTOP5 ----
      CLTOP5.setComponentPopupMenu(BTD);
      CLTOP5.setName("CLTOP5");
      p_principal.add(CLTOP5);
      CLTOP5.setBounds(160, 101, 36, CLTOP5.getPreferredSize().height);

      //---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setText("Valider");
      OBJ_10.setFont(new Font("sansserif", Font.BOLD, 14));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      p_principal.add(OBJ_10);
      OBJ_10.setBounds(92, 170, 140, 40);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < p_principal.getComponentCount(); i++) {
          Rectangle bounds = p_principal.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = p_principal.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        p_principal.setMinimumSize(preferredSize);
        p_principal.setPreferredSize(preferredSize);
      }
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JPanel p_principal;
  private JXTitledSeparator OBJ_13;
  private JLabel label1;
  private JLabel label4;
  private JLabel label3;
  private JLabel label2;
  private JLabel label5;
  private XRiTextField CLTOP1;
  private XRiTextField CLTOP2;
  private XRiTextField CLTOP3;
  private XRiTextField CLTOP4;
  private XRiTextField CLTOP5;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
