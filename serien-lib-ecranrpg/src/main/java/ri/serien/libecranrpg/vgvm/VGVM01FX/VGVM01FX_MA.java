
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_MA extends SNPanelEcranRPG implements ioFrame {
  
  private String[] MAGES_Value = { "", "1", "2", };
  private String[] MAGBA_Value = { "", "1", "2", "5", "6" };
  private String[] MAPCST_Value = { " ", "R", "M", };
  
  public VGVM01FX_MA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    MAGES.setValeurs(MAGES_Value, null);
    MADEP.setValeursSelection("1", " ");
    MARES.setValeursSelection("1", " ");
    MAINV.setValeursSelection("1", " ");
    MATPF.setValeursSelection("2", " ");
    MAFAC.setValeursSelection("1", " ");
    MADBC.setValeursSelection("1", " ");
    MAELV.setValeursSelection("1", " ");
    MAEAS.setValeursSelection("1", " ");
    MAREA.setValeursSelection("1", " ");
    MADAS.setValeursSelection("1", " ");
    MAGBA.setValeurs(MAGBA_Value, null);
    MAPCST.setValeurs(MAPCST_Value, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_97.setVisible(lexique.isPresent("MAASTK"));
    OBJ_56.setVisible(lexique.isPresent("MAPCS"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    chargerComposantRepresentant();
  }
  
  @Override
  public void getData() {
    super.getData();
    snRepresentant.renseignerChampRPG(lexique, "MAREP");
  }
  
  /**
   * Charge le composant representant
   */
  private void chargerComposantRepresentant() {
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB")));
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "MAREP");
    snRepresentant.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    OBJ_80 = new JLabel();
    MAGES = new XRiComboBox();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    xTitledPanel1 = new JXTitledPanel();
    panel6 = new SNPanel();
    MALIB = new XRiTextField();
    OBJ_64 = new SNLabelChamp();
    OBJ_60 = new SNLabelChamp();
    OBJ_54 = new SNLabelChamp();
    MASAN = new XRiTextField();
    OBJ_66 = new SNLabelUnite();
    MADPR = new XRiTextField();
    MATYP = new XRiTextField();
    OBJ_59 = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    OBJ_58 = new SNLabelChamp();
    OBJ_61 = new SNLabelChamp();
    MANCG = new XRiTextField();
    OBJ_62 = new SNLabelChamp();
    MACODE = new XRiTextField();
    OBJ_67 = new SNLabelChamp();
    MALCH = new XRiTextField();
    OBJ_69 = new SNLabelUnite();
    MALCM = new XRiTextField();
    lbDelaiFabrication = new SNLabelChamp();
    MADFAB = new XRiTextField();
    OBJ_78 = new SNLabelChamp();
    OBJ_65 = new SNLabelChamp();
    MADSPR = new XRiTextField();
    MADSAT = new XRiTextField();
    OBJ_82 = new SNLabelChamp();
    MANBP = new XRiTextField();
    MAA1 = new XRiTextField();
    MAA2 = new XRiTextField();
    MAA3 = new XRiTextField();
    MAA4 = new XRiTextField();
    sNPanel1 = new SNPanel();
    OBJ_56 = new SNLabelChamp();
    MAPCS = new XRiTextField();
    OBJ_57 = new SNLabelChamp();
    MAPCSB = new XRiTextField();
    MAPCST = new XRiComboBox();
    panel2 = new JPanel();
    MADAS = new XRiCheckBox();
    MAREA = new XRiCheckBox();
    MAEAS = new XRiCheckBox();
    MAELV = new XRiCheckBox();
    MADBC = new XRiCheckBox();
    MAFAC = new XRiCheckBox();
    MATPF = new XRiCheckBox();
    MAINV = new XRiCheckBox();
    MARES = new XRiCheckBox();
    MADEP = new XRiCheckBox();
    panel5 = new SNPanel();
    panel7 = new SNPanel();
    OBJ_94 = new SNLabelChamp();
    OBJ_96 = new SNLabelUnite();
    MASMA = new XRiTextField();
    OBJ_99 = new SNLabelChamp();
    MAAMA = new XRiTextField();
    OBJ_101 = new SNLabelUnite();
    OBJ_102 = new SNLabelChamp();
    MAINT = new XRiTextField();
    OBJ_104 = new SNLabelUnite();
    panel8 = new SNPanel();
    OBJ_97 = new SNLabelChamp();
    MAASTK = new XRiTextField();
    OBJ_105 = new SNLabelChamp();
    MAADS = new XRiTextField();
    OBJ_63 = new SNLabelChamp();
    MAMSTY = new XRiTextField();
    panel3 = new JPanel();
    OBJ_109 = new SNLabelChamp();
    MANSA = new XRiTextField();
    OBJ_111 = new SNLabelChamp();
    MANBV = new XRiTextField();
    panel4 = new JPanel();
    OBJ_116 = new SNLabelChamp();
    MAJIT = new XRiTextField();
    OBJ_120 = new SNLabelChamp();
    MAJIR = new XRiTextField();
    panel1 = new JPanel();
    MAGBA = new XRiComboBox();
    MAGBMA = new XRiTextField();
    OBJ_95 = new SNLabelChamp();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(750, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");
          
          // ---- OBJ_80 ----
          OBJ_80.setText("Type gestion magasin");
          OBJ_80.setName("OBJ_80");
          
          // ---- MAGES ----
          MAGES.setModel(new DefaultComboBoxModel<>(new String[] { "Magasin ordinaire ou TTC", "Magasin sous douane", "Magasin HTGI" }));
          MAGES.setComponentPopupMenu(BTD);
          MAGES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MAGES.setName("MAGES");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(29, 29, 29)
                  .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 137, GroupLayout.PREFERRED_SIZE).addGap(8, 8, 8)
                  .addComponent(MAGES, GroupLayout.PREFERRED_SIZE, 187, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1)
                          .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
                              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                              .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                              .addComponent(MAGES, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(OBJ_42,
                          GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
                  .addContainerGap()));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed(e));
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());
        
        // ======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Code magasin");
          xTitledPanel1.setBorder(new CompoundBorder(new DropShadowBorder(), new EmptyBorder(10, 10, 10, 10)));
          xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
          xTitledPanel1.setOpaque(true);
          xTitledPanel1.setBackground(new Color(239, 239, 222));
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(new GridBagLayout());
          ((GridBagLayout) xTitledPanel1ContentContainer.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel1ContentContainer.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel1ContentContainer.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) xTitledPanel1ContentContainer.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== panel6 ========
          {
            panel6.setName("panel6");
            panel6.setLayout(new GridBagLayout());
            ((GridBagLayout) panel6.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel6.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel6.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) panel6.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- MALIB ----
            MALIB.setComponentPopupMenu(BTD);
            MALIB.setPreferredSize(new Dimension(310, 28));
            MALIB.setMinimumSize(new Dimension(310, 28));
            MALIB.setName("MALIB");
            panel6.add(MALIB, new GridBagConstraints(1, 1, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_64 ----
            OBJ_64.setText("D\u00e9lai de pr\u00e9paration des commandes");
            OBJ_64.setName("OBJ_64");
            panel6.add(OBJ_64, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_60 ----
            OBJ_60.setText("Code analytique");
            OBJ_60.setName("OBJ_60");
            panel6.add(OBJ_60, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_54 ----
            OBJ_54.setText("Type");
            OBJ_54.setPreferredSize(new Dimension(250, 30));
            OBJ_54.setMinimumSize(new Dimension(250, 30));
            OBJ_54.setName("OBJ_54");
            panel6.add(OBJ_54, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MASAN ----
            MASAN.setComponentPopupMenu(BTD);
            MASAN.setMinimumSize(new Dimension(50, 28));
            MASAN.setPreferredSize(new Dimension(50, 28));
            MASAN.setName("MASAN");
            panel6.add(MASAN, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_66 ----
            OBJ_66.setText("jours");
            OBJ_66.setName("OBJ_66");
            panel6.add(OBJ_66, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MADPR ----
            MADPR.setComponentPopupMenu(BTD);
            MADPR.setPreferredSize(new Dimension(28, 28));
            MADPR.setMinimumSize(new Dimension(28, 28));
            MADPR.setName("MADPR");
            panel6.add(MADPR, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MATYP ----
            MATYP.setComponentPopupMenu(BTD);
            MATYP.setMinimumSize(new Dimension(30, 28));
            MATYP.setPreferredSize(new Dimension(30, 28));
            MATYP.setName("MATYP");
            panel6.add(MATYP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_59 ----
            OBJ_59.setText("Repr\u00e9sentant");
            OBJ_59.setName("OBJ_59");
            panel6.add(OBJ_59, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snRepresentant ----
            snRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
            snRepresentant.setName("snRepresentant");
            panel6.add(snRepresentant, new GridBagConstraints(5, 0, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_58 ----
            OBJ_58.setText("Libell\u00e9");
            OBJ_58.setName("OBJ_58");
            panel6.add(OBJ_58, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_61 ----
            OBJ_61.setText("N\u00b0 Tiers");
            OBJ_61.setPreferredSize(new Dimension(80, 30));
            OBJ_61.setMinimumSize(new Dimension(80, 30));
            OBJ_61.setName("OBJ_61");
            panel6.add(OBJ_61, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MANCG ----
            MANCG.setComponentPopupMenu(BTD);
            MANCG.setPreferredSize(new Dimension(60, 28));
            MANCG.setMinimumSize(new Dimension(60, 28));
            MANCG.setName("MANCG");
            panel6.add(MANCG, new GridBagConstraints(4, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_62 ----
            OBJ_62.setText("Code externe");
            OBJ_62.setName("OBJ_62");
            panel6.add(OBJ_62, new GridBagConstraints(6, 2, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MACODE ----
            MACODE.setComponentPopupMenu(BTD);
            MACODE.setMinimumSize(new Dimension(60, 28));
            MACODE.setPreferredSize(new Dimension(60, 28));
            MACODE.setName("MACODE");
            panel6.add(MACODE, new GridBagConstraints(10, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Heure limite de commande");
            OBJ_67.setName("OBJ_67");
            panel6.add(OBJ_67, new GridBagConstraints(3, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MALCH ----
            MALCH.setComponentPopupMenu(BTD);
            MALCH.setMinimumSize(new Dimension(30, 28));
            MALCH.setPreferredSize(new Dimension(30, 28));
            MALCH.setName("MALCH");
            panel6.add(MALCH, new GridBagConstraints(7, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_69 ----
            OBJ_69.setText("h");
            OBJ_69.setMaximumSize(new Dimension(30, 30));
            OBJ_69.setMinimumSize(new Dimension(20, 30));
            OBJ_69.setPreferredSize(new Dimension(20, 30));
            OBJ_69.setName("OBJ_69");
            panel6.add(OBJ_69, new GridBagConstraints(8, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MALCM ----
            MALCM.setComponentPopupMenu(BTD);
            MALCM.setMinimumSize(new Dimension(30, 28));
            MALCM.setPreferredSize(new Dimension(30, 28));
            MALCM.setName("MALCM");
            panel6.add(MALCM, new GridBagConstraints(9, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDelaiFabrication ----
            lbDelaiFabrication.setText("D\u00e9lai de fabrication (jours)");
            lbDelaiFabrication.setMaximumSize(new Dimension(200, 30));
            lbDelaiFabrication.setMinimumSize(new Dimension(200, 30));
            lbDelaiFabrication.setPreferredSize(new Dimension(200, 30));
            lbDelaiFabrication.setName("lbDelaiFabrication");
            panel6.add(lbDelaiFabrication, new GridBagConstraints(10, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MADFAB ----
            MADFAB.setComponentPopupMenu(BTD);
            MADFAB.setPreferredSize(new Dimension(28, 28));
            MADFAB.setMinimumSize(new Dimension(28, 28));
            MADFAB.setName("MADFAB");
            panel6.add(MADFAB, new GridBagConstraints(11, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_78 ----
            OBJ_78.setText("Codes articles pied de bon");
            OBJ_78.setName("OBJ_78");
            panel6.add(OBJ_78, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_65 ----
            OBJ_65.setText("D\u00e9lai de s\u00e9curit\u00e9 : pr\u00e9paration de commandes / attendu fournisseur (en jours)");
            OBJ_65.setName("OBJ_65");
            panel6.add(OBJ_65, new GridBagConstraints(0, 4, 7, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MADSPR ----
            MADSPR.setComponentPopupMenu(BTD);
            MADSPR.setMinimumSize(new Dimension(30, 28));
            MADSPR.setPreferredSize(new Dimension(30, 28));
            MADSPR.setName("MADSPR");
            panel6.add(MADSPR, new GridBagConstraints(7, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MADSAT ----
            MADSAT.setComponentPopupMenu(BTD);
            MADSAT.setMinimumSize(new Dimension(30, 28));
            MADSAT.setPreferredSize(new Dimension(30, 28));
            MADSAT.setName("MADSAT");
            panel6.add(MADSAT, new GridBagConstraints(9, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- OBJ_82 ----
            OBJ_82.setText("Nbr exemplaire brds. pr\u00e9paration");
            OBJ_82.setName("OBJ_82");
            panel6.add(OBJ_82, new GridBagConstraints(10, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MANBP ----
            MANBP.setComponentPopupMenu(BTD);
            MANBP.setMinimumSize(new Dimension(28, 28));
            MANBP.setPreferredSize(new Dimension(28, 28));
            MANBP.setName("MANBP");
            panel6.add(MANBP, new GridBagConstraints(11, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- MAA1 ----
            MAA1.setComponentPopupMenu(BTD);
            MAA1.setPreferredSize(new Dimension(110, 28));
            MAA1.setMinimumSize(new Dimension(110, 28));
            MAA1.setName("MAA1");
            panel6.add(MAA1, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MAA2 ----
            MAA2.setComponentPopupMenu(BTD);
            MAA2.setPreferredSize(new Dimension(110, 28));
            MAA2.setMinimumSize(new Dimension(110, 28));
            MAA2.setName("MAA2");
            panel6.add(MAA2, new GridBagConstraints(3, 5, 2, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MAA3 ----
            MAA3.setComponentPopupMenu(BTD);
            MAA3.setPreferredSize(new Dimension(110, 28));
            MAA3.setMinimumSize(new Dimension(110, 28));
            MAA3.setName("MAA3");
            panel6.add(MAA3, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MAA4 ----
            MAA4.setComponentPopupMenu(BTD);
            MAA4.setPreferredSize(new Dimension(110, 28));
            MAA4.setMinimumSize(new Dimension(110, 28));
            MAA4.setName("MAA4");
            panel6.add(MAA4, new GridBagConstraints(6, 5, 4, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- OBJ_56 ----
              OBJ_56.setText("Pourcentage du stock pour alerte sur cmde");
              OBJ_56.setPreferredSize(new Dimension(300, 30));
              OBJ_56.setMinimumSize(new Dimension(300, 30));
              OBJ_56.setName("OBJ_56");
              sNPanel1.add(OBJ_56, new GridBagConstraints(0, 0, 8, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAPCS ----
              MAPCS.setToolTipText(
                  "<HTML>Dans le cas d'une commande client sup\u00e9rieure \u00e0 la qt\u00e9 en stock r\u00e9el du si\u00e8ge<BR>multipli\u00e9e par ce pourcentage,   d\u00e9clenchement d'une alerte.</HTML>");
              MAPCS.setComponentPopupMenu(BTD);
              MAPCS.setPreferredSize(new Dimension(35, 30));
              MAPCS.setMinimumSize(new Dimension(35, 30));
              MAPCS.setName("MAPCS");
              sNPanel1.add(MAPCS, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_57 ----
              OBJ_57.setText("Pourcentage maximum de cmde sur stock");
              OBJ_57.setPreferredSize(new Dimension(300, 30));
              OBJ_57.setMinimumSize(new Dimension(300, 30));
              OBJ_57.setName("OBJ_57");
              sNPanel1.add(OBJ_57, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAPCSB ----
              MAPCSB.setToolTipText(
                  "<HTML>Dans le cas d'une commande client sup\u00e9rieure \u00e0 la qt\u00e9 en stock r\u00e9el du si\u00e8ge<BR>multipli\u00e9e par ce pourcentage,   d\u00e9clenchement d'une alerte.</HTML>");
              MAPCSB.setComponentPopupMenu(BTD);
              MAPCSB.setPreferredSize(new Dimension(35, 30));
              MAPCSB.setMinimumSize(new Dimension(35, 30));
              MAPCSB.setName("MAPCSB");
              sNPanel1.add(MAPCSB, new GridBagConstraints(10, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAPCST ----
              MAPCST.setModel(new DefaultComboBoxModel<>(new String[] { "Stock si\u00e8ge", "Stock r\u00e9el", "Stock minimum" }));
              MAPCST.setMinimumSize(new Dimension(150, 30));
              MAPCST.setPreferredSize(new Dimension(150, 30));
              MAPCST.setName("MAPCST");
              sNPanel1.add(MAPCST, new GridBagConstraints(11, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel6.add(sNPanel1, new GridBagConstraints(0, 6, 12, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          xTitledPanel1ContentContainer.add(panel6, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(10, 0, 5, 0), 0, 0));
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setMinimumSize(new Dimension(750, 115));
            panel2.setPreferredSize(new Dimension(750, 115));
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- MADAS ----
            MADAS.setText("Magasin non g\u00e9r\u00e9 par adresse de stockage");
            MADAS.setComponentPopupMenu(BTD);
            MADAS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MADAS.setFont(new Font("sansserif", Font.PLAIN, 14));
            MADAS.setName("MADAS");
            panel2.add(MADAS);
            MADAS.setBounds(415, 85, 330, 20);
            
            // ---- MAREA ----
            MAREA.setText("R\u00e9approvisionnement propos\u00e9 sur facture");
            MAREA.setComponentPopupMenu(BTD);
            MAREA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAREA.setFont(new Font("sansserif", Font.PLAIN, 14));
            MAREA.setName("MAREA");
            panel2.add(MAREA);
            MAREA.setBounds(10, 65, 350, 20);
            
            // ---- MAEAS ----
            MAEAS.setText("Tri bordereaux stock sur adresse stock");
            MAEAS.setComponentPopupMenu(BTD);
            MAEAS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAEAS.setFont(new Font("sansserif", Font.PLAIN, 14));
            MAEAS.setName("MAEAS");
            panel2.add(MAEAS);
            MAEAS.setBounds(415, 5, 330, 20);
            
            // ---- MAELV ----
            MAELV.setText("Edition du lieu de livraison sur bon");
            MAELV.setComponentPopupMenu(BTD);
            MAELV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAELV.setFont(new Font("sansserif", Font.PLAIN, 14));
            MAELV.setName("MAELV");
            panel2.add(MAELV);
            MAELV.setBounds(10, 5, 350, 20);
            
            // ---- MADBC ----
            MADBC.setText("D\u00e9coupage bon de commande");
            MADBC.setComponentPopupMenu(BTD);
            MADBC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MADBC.setFont(new Font("sansserif", Font.PLAIN, 14));
            MADBC.setName("MADBC");
            panel2.add(MADBC);
            MADBC.setBounds(415, 45, 330, 20);
            
            // ---- MAFAC ----
            MAFAC.setText("Interdit sur bons et factures");
            MAFAC.setComponentPopupMenu(BTD);
            MAFAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAFAC.setFont(new Font("sansserif", Font.PLAIN, 14));
            MAFAC.setName("MAFAC");
            panel2.add(MAFAC);
            MAFAC.setBounds(415, 25, 330, 20);
            
            // ---- MATPF ----
            MATPF.setText("Taxe parafiscale");
            MATPF.setComponentPopupMenu(BTD);
            MATPF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MATPF.setFont(new Font("sansserif", Font.PLAIN, 14));
            MATPF.setName("MATPF");
            panel2.add(MATPF);
            MATPF.setBounds(415, 65, 330, 20);
            
            // ---- MAINV ----
            MAINV.setText("Inventaire interdit");
            MAINV.setComponentPopupMenu(BTD);
            MAINV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MAINV.setFont(new Font("sansserif", Font.PLAIN, 14));
            MAINV.setName("MAINV");
            panel2.add(MAINV);
            MAINV.setBounds(10, 45, 350, 20);
            
            // ---- MARES ----
            MARES.setText("Magasin r\u00e9serv\u00e9");
            MARES.setComponentPopupMenu(BTD);
            MARES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MARES.setFont(new Font("sansserif", Font.PLAIN, 14));
            MARES.setName("MARES");
            panel2.add(MARES);
            MARES.setBounds(10, 85, 350, 20);
            
            // ---- MADEP ----
            MADEP.setText("D\u00e9p\u00f4t client");
            MADEP.setComponentPopupMenu(BTD);
            MADEP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            MADEP.setFont(new Font("sansserif", Font.PLAIN, 14));
            MADEP.setName("MADEP");
            panel2.add(MADEP);
            MADEP.setBounds(10, 25, 350, 20);
          }
          xTitledPanel1ContentContainer.add(panel2, new GridBagConstraints(0, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel5 ========
          {
            panel5.setName("panel5");
            panel5.setLayout(new BoxLayout(panel5, BoxLayout.X_AXIS));
            
            // ======== panel7 ========
            {
              panel7.setName("panel7");
              panel7.setLayout(new GridBagLayout());
              ((GridBagLayout) panel7.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) panel7.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) panel7.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) panel7.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- OBJ_94 ----
              OBJ_94.setText("Magasin de regroupement");
              OBJ_94.setMinimumSize(new Dimension(200, 30));
              OBJ_94.setPreferredSize(new Dimension(200, 30));
              OBJ_94.setName("OBJ_94");
              panel7.add(OBJ_94, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OBJ_96 ----
              OBJ_96.setText("(site g\u00e9ographique)");
              OBJ_96.setMinimumSize(new Dimension(200, 30));
              OBJ_96.setPreferredSize(new Dimension(200, 30));
              OBJ_96.setName("OBJ_96");
              panel7.add(OBJ_96, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- MASMA ----
              MASMA.setComponentPopupMenu(BTD);
              MASMA.setMinimumSize(new Dimension(30, 28));
              MASMA.setPreferredSize(new Dimension(30, 28));
              MASMA.setName("MASMA");
              panel7.add(MASMA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OBJ_99 ----
              OBJ_99.setText("Magasin de regroupement");
              OBJ_99.setName("OBJ_99");
              panel7.add(OBJ_99, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- MAAMA ----
              MAAMA.setComponentPopupMenu(BTD);
              MAAMA.setMinimumSize(new Dimension(30, 28));
              MAAMA.setPreferredSize(new Dimension(30, 28));
              MAAMA.setName("MAAMA");
              panel7.add(MAAMA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OBJ_101 ----
              OBJ_101.setText("(activit\u00e9)");
              OBJ_101.setName("OBJ_101");
              panel7.add(OBJ_101, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_102 ----
              OBJ_102.setText("Magasin interm\u00e9diaire");
              OBJ_102.setName("OBJ_102");
              panel7.add(OBJ_102, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAINT ----
              MAINT.setComponentPopupMenu(BTD);
              MAINT.setMinimumSize(new Dimension(30, 28));
              MAINT.setPreferredSize(new Dimension(30, 28));
              MAINT.setName("MAINT");
              panel7.add(MAINT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_104 ----
              OBJ_104.setText("(transfert)");
              OBJ_104.setName("OBJ_104");
              panel7.add(OBJ_104, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel5.add(panel7);
            
            // ======== panel8 ========
            {
              panel8.setPreferredSize(new Dimension(455, 100));
              panel8.setMinimumSize(new Dimension(455, 100));
              panel8.setName("panel8");
              panel8.setLayout(new GridBagLayout());
              ((GridBagLayout) panel8.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) panel8.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) panel8.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) panel8.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- OBJ_97 ----
              OBJ_97.setText("Magasin adresse stockage");
              OBJ_97.setPreferredSize(new Dimension(200, 30));
              OBJ_97.setMinimumSize(new Dimension(200, 30));
              OBJ_97.setName("OBJ_97");
              panel8.add(OBJ_97, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- MAASTK ----
              MAASTK.setComponentPopupMenu(BTD);
              MAASTK.setMinimumSize(new Dimension(30, 28));
              MAASTK.setPreferredSize(new Dimension(30, 28));
              MAASTK.setName("MAASTK");
              panel8.add(MAASTK, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_105 ----
              OBJ_105.setText("Type d'adresse \u00e0 \u00e9diter en priorit\u00e9");
              OBJ_105.setMinimumSize(new Dimension(250, 30));
              OBJ_105.setPreferredSize(new Dimension(250, 30));
              OBJ_105.setName("OBJ_105");
              panel8.add(OBJ_105, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- MAADS ----
              MAADS.setComponentPopupMenu(BTD);
              MAADS.setMinimumSize(new Dimension(28, 28));
              MAADS.setPreferredSize(new Dimension(28, 28));
              MAADS.setName("MAADS");
              panel8.add(MAADS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_63 ----
              OBJ_63.setText("Type de stockage");
              OBJ_63.setName("OBJ_63");
              panel8.add(OBJ_63, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- MAMSTY ----
              MAMSTY.setComponentPopupMenu(BTD);
              MAMSTY.setPreferredSize(new Dimension(60, 28));
              MAMSTY.setMinimumSize(new Dimension(60, 28));
              MAMSTY.setName("MAMSTY");
              panel8.add(MAMSTY, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            panel5.add(panel8);
          }
          xTitledPanel1ContentContainer.add(panel5, new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Calcul stock mini"));
            panel3.setOpaque(false);
            panel3.setPreferredSize(new Dimension(270, 100));
            panel3.setMinimumSize(new Dimension(270, 100));
            panel3.setName("panel3");
            panel3.setLayout(new GridBagLayout());
            ((GridBagLayout) panel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel3.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) panel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- OBJ_109 ----
            OBJ_109.setText("P\u00e9riode d'analyse");
            OBJ_109.setName("OBJ_109");
            panel3.add(OBJ_109, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MANSA ----
            MANSA.setComponentPopupMenu(BTD);
            MANSA.setPreferredSize(new Dimension(30, 28));
            MANSA.setMinimumSize(new Dimension(30, 28));
            MANSA.setName("MANSA");
            panel3.add(MANSA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_111 ----
            OBJ_111.setText("Nombre de ventes mini");
            OBJ_111.setName("OBJ_111");
            panel3.add(OBJ_111, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- MANBV ----
            MANBV.setComponentPopupMenu(BTD);
            MANBV.setPreferredSize(new Dimension(30, 28));
            MANBV.setMinimumSize(new Dimension(30, 28));
            MANBV.setName("MANBV");
            panel3.add(MANBV, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          xTitledPanel1ContentContainer.add(panel3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Inventaires tournants"));
            panel4.setOpaque(false);
            panel4.setMinimumSize(new Dimension(330, 100));
            panel4.setPreferredSize(new Dimension(330, 100));
            panel4.setName("panel4");
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout) panel4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel4.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel4.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) panel4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- OBJ_116 ----
            OBJ_116.setText("Nombre d'inventaires tournants par an");
            OBJ_116.setMaximumSize(new Dimension(200, 30));
            OBJ_116.setMinimumSize(new Dimension(200, 30));
            OBJ_116.setPreferredSize(new Dimension(200, 30));
            OBJ_116.setName("OBJ_116");
            panel4.add(OBJ_116, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- MAJIT ----
            MAJIT.setComponentPopupMenu(BTD);
            MAJIT.setMinimumSize(new Dimension(30, 28));
            MAJIT.setPreferredSize(new Dimension(30, 28));
            MAJIT.setName("MAJIT");
            panel4.add(MAJIT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_120 ----
            OBJ_120.setText("Nombre d'inventaires d\u00e9j\u00e0 effectu\u00e9s");
            OBJ_120.setName("OBJ_120");
            panel4.add(OBJ_120, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- MAJIR ----
            MAJIR.setComponentPopupMenu(BTD);
            MAJIR.setMinimumSize(new Dimension(30, 28));
            MAJIR.setPreferredSize(new Dimension(30, 28));
            MAJIR.setName("MAJIR");
            panel4.add(MAJIR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          xTitledPanel1ContentContainer.add(panel4, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("GBA"));
            panel1.setOpaque(false);
            panel1.setPreferredSize(new Dimension(280, 100));
            panel1.setMinimumSize(new Dimension(280, 100));
            panel1.setName("panel1");
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- MAGBA ----
            MAGBA.setModel(new DefaultComboBoxModel<>(
                new String[] { "Pas de GBA", "GBA diff\u00e9r\u00e9 livraison par nos soins", "GBA diff\u00e9r\u00e9 livraison directe",
                    "GBA imm\u00e9diat livraison par nos soins", "GBA imm\u00e9diat livraison directe" }));
            MAGBA.setName("MAGBA");
            panel1.add(MAGBA, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- MAGBMA ----
            MAGBMA.setComponentPopupMenu(BTD);
            MAGBMA.setPreferredSize(new Dimension(30, 28));
            MAGBMA.setMinimumSize(new Dimension(30, 28));
            MAGBMA.setName("MAGBMA");
            panel1.add(MAGBMA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- OBJ_95 ----
            OBJ_95.setText("Magasin seuil GBA");
            OBJ_95.setName("OBJ_95");
            panel1.add(OBJ_95, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          xTitledPanel1ContentContainer.add(panel1, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        p_centrage.add(xTitledPanel1, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(e -> OBJ_18ActionPerformed(e));
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(e -> OBJ_19ActionPerformed(e));
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JLabel OBJ_80;
  private XRiComboBox MAGES;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JXTitledPanel xTitledPanel1;
  private SNPanel panel6;
  private XRiTextField MALIB;
  private SNLabelChamp OBJ_64;
  private SNLabelChamp OBJ_60;
  private SNLabelChamp OBJ_54;
  private XRiTextField MASAN;
  private SNLabelUnite OBJ_66;
  private XRiTextField MADPR;
  private XRiTextField MATYP;
  private SNLabelChamp OBJ_59;
  private SNRepresentant snRepresentant;
  private SNLabelChamp OBJ_58;
  private SNLabelChamp OBJ_61;
  private XRiTextField MANCG;
  private SNLabelChamp OBJ_62;
  private XRiTextField MACODE;
  private SNLabelChamp OBJ_67;
  private XRiTextField MALCH;
  private SNLabelUnite OBJ_69;
  private XRiTextField MALCM;
  private SNLabelChamp lbDelaiFabrication;
  private XRiTextField MADFAB;
  private SNLabelChamp OBJ_78;
  private SNLabelChamp OBJ_65;
  private XRiTextField MADSPR;
  private XRiTextField MADSAT;
  private SNLabelChamp OBJ_82;
  private XRiTextField MANBP;
  private XRiTextField MAA1;
  private XRiTextField MAA2;
  private XRiTextField MAA3;
  private XRiTextField MAA4;
  private SNPanel sNPanel1;
  private SNLabelChamp OBJ_56;
  private XRiTextField MAPCS;
  private SNLabelChamp OBJ_57;
  private XRiTextField MAPCSB;
  private XRiComboBox MAPCST;
  private JPanel panel2;
  private XRiCheckBox MADAS;
  private XRiCheckBox MAREA;
  private XRiCheckBox MAEAS;
  private XRiCheckBox MAELV;
  private XRiCheckBox MADBC;
  private XRiCheckBox MAFAC;
  private XRiCheckBox MATPF;
  private XRiCheckBox MAINV;
  private XRiCheckBox MARES;
  private XRiCheckBox MADEP;
  private SNPanel panel5;
  private SNPanel panel7;
  private SNLabelChamp OBJ_94;
  private SNLabelUnite OBJ_96;
  private XRiTextField MASMA;
  private SNLabelChamp OBJ_99;
  private XRiTextField MAAMA;
  private SNLabelUnite OBJ_101;
  private SNLabelChamp OBJ_102;
  private XRiTextField MAINT;
  private SNLabelUnite OBJ_104;
  private SNPanel panel8;
  private SNLabelChamp OBJ_97;
  private XRiTextField MAASTK;
  private SNLabelChamp OBJ_105;
  private XRiTextField MAADS;
  private SNLabelChamp OBJ_63;
  private XRiTextField MAMSTY;
  private JPanel panel3;
  private SNLabelChamp OBJ_109;
  private XRiTextField MANSA;
  private SNLabelChamp OBJ_111;
  private XRiTextField MANBV;
  private JPanel panel4;
  private SNLabelChamp OBJ_116;
  private XRiTextField MAJIT;
  private SNLabelChamp OBJ_120;
  private XRiTextField MAJIR;
  private JPanel panel1;
  private XRiComboBox MAGBA;
  private XRiTextField MAGBMA;
  private SNLabelChamp OBJ_95;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
