
package ri.serien.libecranrpg.vgvm.VGVM119F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM119F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM119F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    // Ajout
    initDiverses();
    E2PAYX.setValeursSelection("X", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ME2R@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    E2CDP.setEnabled(lexique.isPresent("E2CDP"));
    E2TEL.setEnabled(lexique.isPresent("E2TEL"));
    E2VILR.setEnabled(lexique.isPresent("E2VILR"));
    E2PAYX.setVisible(!lexique.HostFieldGetData("ME2R").trim().equals(""));
    LOC1.setEnabled(lexique.isPresent("LOC1"));
    E2RUE.setEnabled(lexique.isPresent("E2RUE"));
    E2CPL.setEnabled(lexique.isPresent("E2CPL"));
    E2NOM.setEnabled(lexique.isPresent("E2NOM"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Client livré"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_19 = new JLabel();
    E2NOM = new XRiTextField();
    E2CPL = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_23 = new JLabel();
    E2RUE = new XRiTextField();
    OBJ_26 = new JLabel();
    E2CDP = new XRiTextField();
    E2VILR = new XRiTextField();
    OBJ_31 = new JLabel();
    E2TEL = new XRiTextField();
    OBJ_32 = new JLabel();
    LOC1 = new XRiTextField();
    E2PAYX = new XRiCheckBox();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(730, 310));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_19 ----
          OBJ_19.setText("Nom");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(25, 19, 115, 20);
          
          // ---- E2NOM ----
          E2NOM.setComponentPopupMenu(BTD);
          E2NOM.setName("E2NOM");
          panel1.add(E2NOM);
          E2NOM.setBounds(150, 15, 310, E2NOM.getPreferredSize().height);
          
          // ---- E2CPL ----
          E2CPL.setComponentPopupMenu(BTD);
          E2CPL.setName("E2CPL");
          panel1.add(E2CPL);
          E2CPL.setBounds(150, 45, 310, E2CPL.getPreferredSize().height);
          
          // ---- OBJ_21 ----
          OBJ_21.setText("Compl\u00e9ment");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(25, 49, 115, 20);
          
          // ---- OBJ_23 ----
          OBJ_23.setText("Rue");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(25, 79, 115, 20);
          
          // ---- E2RUE ----
          E2RUE.setComponentPopupMenu(BTD);
          E2RUE.setName("E2RUE");
          panel1.add(E2RUE);
          E2RUE.setBounds(150, 75, 310, E2RUE.getPreferredSize().height);
          
          // ---- OBJ_26 ----
          OBJ_26.setText("Code postal / ville");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(25, 109, 115, 20);
          
          // ---- E2CDP ----
          E2CDP.setComponentPopupMenu(BTD);
          E2CDP.setName("E2CDP");
          panel1.add(E2CDP);
          E2CDP.setBounds(150, 105, 60, E2CDP.getPreferredSize().height);
          
          // ---- E2VILR ----
          E2VILR.setComponentPopupMenu(BTD);
          E2VILR.setName("E2VILR");
          panel1.add(E2VILR);
          E2VILR.setBounds(210, 105, 250, E2VILR.getPreferredSize().height);
          
          // ---- OBJ_31 ----
          OBJ_31.setText("T\u00e9l\u00e9phone");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(25, 139, 115, 20);
          
          // ---- E2TEL ----
          E2TEL.setComponentPopupMenu(BTD);
          E2TEL.setName("E2TEL");
          panel1.add(E2TEL);
          E2TEL.setBounds(150, 135, 180, E2TEL.getPreferredSize().height);
          
          // ---- OBJ_32 ----
          OBJ_32.setText("Remarque");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(25, 169, 115, 20);
          
          // ---- LOC1 ----
          LOC1.setComponentPopupMenu(BTD);
          LOC1.setName("LOC1");
          panel1.add(LOC1);
          LOC1.setBounds(150, 165, 310, LOC1.getPreferredSize().height);
          
          // ---- E2PAYX ----
          E2PAYX.setComponentPopupMenu(BTD);
          E2PAYX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          E2PAYX.setName("E2PAYX");
          panel1.add(E2PAYX);
          E2PAYX.setBounds(150, 199, 20, 20);
          
          // ---- label1 ----
          label1.setText("@ME2R@");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(25, 199, 115, 20);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addGap(35, 35, 35).addComponent(panel1, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE)));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addGap(35, 35, 35).addComponent(panel1, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_19;
  private XRiTextField E2NOM;
  private XRiTextField E2CPL;
  private JLabel OBJ_21;
  private JLabel OBJ_23;
  private XRiTextField E2RUE;
  private JLabel OBJ_26;
  private XRiTextField E2CDP;
  private XRiTextField E2VILR;
  private JLabel OBJ_31;
  private XRiTextField E2TEL;
  private JLabel OBJ_32;
  private XRiTextField LOC1;
  private XRiCheckBox E2PAYX;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
