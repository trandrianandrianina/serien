
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_D3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] DSCDP_Value = { "P", "R", "E", };
  
  public VGVM01FX_D3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DSCDP.setValeurs(DSCDP_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_83 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_82 = new JLabel();
    DSPE1 = new XRiTextField();
    DSPE2 = new XRiTextField();
    panel3 = new JPanel();
    OBJ_55 = new JLabel();
    DSLIB = new XRiTextField();
    panel1 = new JPanel();
    DSS1 = new XRiTextField();
    DSI1 = new XRiTextField();
    DSK01 = new XRiTextField();
    DSS2 = new XRiTextField();
    DSI2 = new XRiTextField();
    DSK02 = new XRiTextField();
    DSS3 = new XRiTextField();
    DSI3 = new XRiTextField();
    DSK03 = new XRiTextField();
    DSS4 = new XRiTextField();
    DSI4 = new XRiTextField();
    DSK04 = new XRiTextField();
    OBJ_85 = new JLabel();
    DSCDP = new XRiComboBox();
    panel2 = new JPanel();
    DST1 = new XRiTextField();
    DSJ1 = new XRiTextField();
    DSK06 = new XRiTextField();
    DST2 = new XRiTextField();
    DSJ2 = new XRiTextField();
    DSK07 = new XRiTextField();
    DST3 = new XRiTextField();
    DSJ3 = new XRiTextField();
    DSK08 = new XRiTextField();
    DST4 = new XRiTextField();
    DSJ4 = new XRiTextField();
    DSK09 = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_88 = new JLabel();
    DST6 = new XRiTextField();
    DSJ6 = new XRiTextField();
    DSK11 = new XRiTextField();
    DST7 = new XRiTextField();
    DSJ7 = new XRiTextField();
    DSK12 = new XRiTextField();
    OBJ_87 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(5, 3, 93, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(95, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(160, 4, 40, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(200, 1, 34, INDTYP.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(254, 4, 39, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(294, 1, 60, INDIND.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(860, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("D\u00e9pr\u00e9ciation de stock");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_83 ----
            OBJ_83.setText("D\u00e9lai de carence (mois)");
            OBJ_83.setName("OBJ_83");
            xTitledPanel1ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(215, 84, 162, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Mode 1");
            OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getStyle() | Font.BOLD));
            OBJ_81.setName("OBJ_81");
            xTitledPanel1ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(396, 60, 48, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Mode 2");
            OBJ_82.setFont(OBJ_82.getFont().deriveFont(OBJ_82.getFont().getStyle() | Font.BOLD));
            OBJ_82.setName("OBJ_82");
            xTitledPanel1ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(486, 60, 48, 20);

            //---- DSPE1 ----
            DSPE1.setComponentPopupMenu(null);
            DSPE1.setName("DSPE1");
            xTitledPanel1ContentContainer.add(DSPE1);
            DSPE1.setBounds(405, 80, 30, DSPE1.getPreferredSize().height);

            //---- DSPE2 ----
            DSPE2.setComponentPopupMenu(null);
            DSPE2.setName("DSPE2");
            xTitledPanel1ContentContainer.add(DSPE2);
            DSPE2.setBounds(495, 80, 30, DSPE2.getPreferredSize().height);

            //======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_55 ----
              OBJ_55.setText("Libell\u00e9");
              OBJ_55.setName("OBJ_55");
              panel3.add(OBJ_55);
              OBJ_55.setBounds(15, 12, 61, 20);

              //---- DSLIB ----
              DSLIB.setComponentPopupMenu(null);
              DSLIB.setName("DSLIB");
              panel3.add(DSLIB);
              DSLIB.setBounds(165, 8, 315, DSLIB.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(45, 5, 510, 45);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("A- \u00e9coulement jusqu'\u00e0 (mois)"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- DSS1 ----
              DSS1.setComponentPopupMenu(null);
              DSS1.setName("DSS1");
              panel1.add(DSS1);
              DSS1.setBounds(165, 25, 30, DSS1.getPreferredSize().height);

              //---- DSI1 ----
              DSI1.setComponentPopupMenu(null);
              DSI1.setName("DSI1");
              panel1.add(DSI1);
              DSI1.setBounds(360, 25, 30, DSI1.getPreferredSize().height);

              //---- DSK01 ----
              DSK01.setComponentPopupMenu(null);
              DSK01.setName("DSK01");
              panel1.add(DSK01);
              DSK01.setBounds(450, 25, 30, DSK01.getPreferredSize().height);

              //---- DSS2 ----
              DSS2.setComponentPopupMenu(null);
              DSS2.setName("DSS2");
              panel1.add(DSS2);
              DSS2.setBounds(165, 50, 30, DSS2.getPreferredSize().height);

              //---- DSI2 ----
              DSI2.setComponentPopupMenu(null);
              DSI2.setName("DSI2");
              panel1.add(DSI2);
              DSI2.setBounds(360, 50, 30, DSI2.getPreferredSize().height);

              //---- DSK02 ----
              DSK02.setComponentPopupMenu(null);
              DSK02.setName("DSK02");
              panel1.add(DSK02);
              DSK02.setBounds(450, 50, 30, DSK02.getPreferredSize().height);

              //---- DSS3 ----
              DSS3.setComponentPopupMenu(null);
              DSS3.setName("DSS3");
              panel1.add(DSS3);
              DSS3.setBounds(165, 75, 30, DSS3.getPreferredSize().height);

              //---- DSI3 ----
              DSI3.setComponentPopupMenu(null);
              DSI3.setName("DSI3");
              panel1.add(DSI3);
              DSI3.setBounds(360, 75, 30, DSI3.getPreferredSize().height);

              //---- DSK03 ----
              DSK03.setComponentPopupMenu(null);
              DSK03.setName("DSK03");
              panel1.add(DSK03);
              DSK03.setBounds(450, 75, 30, DSK03.getPreferredSize().height);

              //---- DSS4 ----
              DSS4.setComponentPopupMenu(null);
              DSS4.setName("DSS4");
              panel1.add(DSS4);
              DSS4.setBounds(165, 100, 30, DSS4.getPreferredSize().height);

              //---- DSI4 ----
              DSI4.setComponentPopupMenu(null);
              DSI4.setName("DSI4");
              panel1.add(DSI4);
              DSI4.setBounds(360, 100, 30, DSI4.getPreferredSize().height);

              //---- DSK04 ----
              DSK04.setComponentPopupMenu(null);
              DSK04.setName("DSK04");
              panel1.add(DSK04);
              DSK04.setBounds(450, 100, 30, DSK04.getPreferredSize().height);

              //---- OBJ_85 ----
              OBJ_85.setText("% de d\u00e9pr\u00e9ciation sur");
              OBJ_85.setName("OBJ_85");
              panel1.add(OBJ_85);
              OBJ_85.setBounds(505, 29, 140, 20);

              //---- DSCDP ----
              DSCDP.setModel(new DefaultComboBoxModel(new String[] {
                "P.U.M.P",
                "Prix de revient",
                "Derni\u00e8re entr\u00e9e"
              }));
              DSCDP.setComponentPopupMenu(null);
              DSCDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DSCDP.setName("DSCDP");
              panel1.add(DSCDP);
              DSCDP.setBounds(505, 51, 140, DSCDP.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(45, 110, 675, 145);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("B- au del\u00e0:"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- DST1 ----
              DST1.setComponentPopupMenu(null);
              DST1.setName("DST1");
              panel2.add(DST1);
              DST1.setBounds(165, 25, 30, DST1.getPreferredSize().height);

              //---- DSJ1 ----
              DSJ1.setComponentPopupMenu(null);
              DSJ1.setName("DSJ1");
              panel2.add(DSJ1);
              DSJ1.setBounds(360, 25, 30, DSJ1.getPreferredSize().height);

              //---- DSK06 ----
              DSK06.setComponentPopupMenu(null);
              DSK06.setName("DSK06");
              panel2.add(DSK06);
              DSK06.setBounds(450, 25, 30, DSK06.getPreferredSize().height);

              //---- DST2 ----
              DST2.setComponentPopupMenu(null);
              DST2.setName("DST2");
              panel2.add(DST2);
              DST2.setBounds(165, 50, 30, DST2.getPreferredSize().height);

              //---- DSJ2 ----
              DSJ2.setComponentPopupMenu(null);
              DSJ2.setName("DSJ2");
              panel2.add(DSJ2);
              DSJ2.setBounds(360, 50, 30, DSJ2.getPreferredSize().height);

              //---- DSK07 ----
              DSK07.setComponentPopupMenu(null);
              DSK07.setName("DSK07");
              panel2.add(DSK07);
              DSK07.setBounds(450, 50, 30, DSK07.getPreferredSize().height);

              //---- DST3 ----
              DST3.setComponentPopupMenu(null);
              DST3.setName("DST3");
              panel2.add(DST3);
              DST3.setBounds(165, 75, 30, DST3.getPreferredSize().height);

              //---- DSJ3 ----
              DSJ3.setComponentPopupMenu(null);
              DSJ3.setName("DSJ3");
              panel2.add(DSJ3);
              DSJ3.setBounds(360, 75, 30, DSJ3.getPreferredSize().height);

              //---- DSK08 ----
              DSK08.setComponentPopupMenu(null);
              DSK08.setName("DSK08");
              panel2.add(DSK08);
              DSK08.setBounds(450, 75, 30, DSK08.getPreferredSize().height);

              //---- DST4 ----
              DST4.setComponentPopupMenu(null);
              DST4.setName("DST4");
              panel2.add(DST4);
              DST4.setBounds(165, 100, 30, DST4.getPreferredSize().height);

              //---- DSJ4 ----
              DSJ4.setComponentPopupMenu(null);
              DSJ4.setName("DSJ4");
              panel2.add(DSJ4);
              DSJ4.setBounds(360, 100, 30, DSJ4.getPreferredSize().height);

              //---- DSK09 ----
              DSK09.setComponentPopupMenu(null);
              DSK09.setName("DSK09");
              panel2.add(DSK09);
              DSK09.setBounds(450, 100, 30, DSK09.getPreferredSize().height);

              //---- OBJ_89 ----
              OBJ_89.setText("date cr\u00e9ation inf\u00e9rieure");
              OBJ_89.setFont(OBJ_89.getFont().deriveFont(OBJ_89.getFont().getStyle() | Font.BOLD));
              OBJ_89.setName("OBJ_89");
              panel2.add(OBJ_89);
              OBJ_89.setBounds(15, 174, 140, 20);

              //---- OBJ_88 ----
              OBJ_88.setText("- aucune vente");
              OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getStyle() | Font.BOLD));
              OBJ_88.setName("OBJ_88");
              panel2.add(OBJ_88);
              OBJ_88.setBounds(15, 149, 108, 20);

              //---- DST6 ----
              DST6.setComponentPopupMenu(null);
              DST6.setName("DST6");
              panel2.add(DST6);
              DST6.setBounds(165, 145, 30, DST6.getPreferredSize().height);

              //---- DSJ6 ----
              DSJ6.setComponentPopupMenu(null);
              DSJ6.setName("DSJ6");
              panel2.add(DSJ6);
              DSJ6.setBounds(360, 145, 30, DSJ6.getPreferredSize().height);

              //---- DSK11 ----
              DSK11.setComponentPopupMenu(null);
              DSK11.setName("DSK11");
              panel2.add(DSK11);
              DSK11.setBounds(450, 145, 30, DSK11.getPreferredSize().height);

              //---- DST7 ----
              DST7.setComponentPopupMenu(null);
              DST7.setName("DST7");
              panel2.add(DST7);
              DST7.setBounds(165, 170, 30, DST7.getPreferredSize().height);

              //---- DSJ7 ----
              DSJ7.setComponentPopupMenu(null);
              DSJ7.setName("DSJ7");
              panel2.add(DSJ7);
              DSJ7.setBounds(360, 170, 30, DSJ7.getPreferredSize().height);

              //---- DSK12 ----
              DSK12.setComponentPopupMenu(null);
              DSK12.setName("DSK12");
              panel2.add(DSK12);
              DSK12.setBounds(450, 170, 30, DSK12.getPreferredSize().height);

              //---- OBJ_87 ----
              OBJ_87.setText("- DDV inf\u00e9rieure \u00e0 (mois)");
              OBJ_87.setFont(OBJ_87.getFont().deriveFont(OBJ_87.getFont().getStyle() | Font.BOLD));
              OBJ_87.setName("OBJ_87");
              panel2.add(OBJ_87);
              OBJ_87.setBounds(15, 29, 150, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(45, 260, 675, 215);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 773, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(48, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(28, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_83;
  private JLabel OBJ_81;
  private JLabel OBJ_82;
  private XRiTextField DSPE1;
  private XRiTextField DSPE2;
  private JPanel panel3;
  private JLabel OBJ_55;
  private XRiTextField DSLIB;
  private JPanel panel1;
  private XRiTextField DSS1;
  private XRiTextField DSI1;
  private XRiTextField DSK01;
  private XRiTextField DSS2;
  private XRiTextField DSI2;
  private XRiTextField DSK02;
  private XRiTextField DSS3;
  private XRiTextField DSI3;
  private XRiTextField DSK03;
  private XRiTextField DSS4;
  private XRiTextField DSI4;
  private XRiTextField DSK04;
  private JLabel OBJ_85;
  private XRiComboBox DSCDP;
  private JPanel panel2;
  private XRiTextField DST1;
  private XRiTextField DSJ1;
  private XRiTextField DSK06;
  private XRiTextField DST2;
  private XRiTextField DSJ2;
  private XRiTextField DSK07;
  private XRiTextField DST3;
  private XRiTextField DSJ3;
  private XRiTextField DSK08;
  private XRiTextField DST4;
  private XRiTextField DSJ4;
  private XRiTextField DSK09;
  private JLabel OBJ_89;
  private JLabel OBJ_88;
  private XRiTextField DST6;
  private XRiTextField DSJ6;
  private XRiTextField DSK11;
  private XRiTextField DST7;
  private XRiTextField DSJ7;
  private XRiTextField DSK12;
  private JLabel OBJ_87;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
