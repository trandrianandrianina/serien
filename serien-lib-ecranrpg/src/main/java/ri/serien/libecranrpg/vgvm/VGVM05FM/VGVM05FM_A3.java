
package ri.serien.libecranrpg.vgvm.VGVM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM05FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] WTCD_Value={"N","B","K","R","S","+","-","",};
  
  public VGVM05FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LD01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01@ ")).trim());
    LD02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02@")).trim());
    LD03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03@")).trim());
    LD04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04@")).trim());
    LD05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05@")).trim());
    LD06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06@")).trim());
    LD07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07@")).trim());
    LD08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08@")).trim());
    LD09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09@")).trim());
    LD10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10@")).trim());
    LD11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11@")).trim());
    LD12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12@")).trim());
    LD13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13@")).trim());
    LD14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14@")).trim());
    LD15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTIT@")).trim());
    PL01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL01@")).trim());
    PL02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL02@")).trim());
    PL03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL03@")).trim());
    PL04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL04@")).trim());
    PL05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL05@")).trim());
    PL06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL06@")).trim());
    PL07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL07@")).trim());
    PL08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL08@")).trim());
    PL09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL09@")).trim());
    PL10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL10@")).trim());
    PL11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL11@")).trim());
    PL12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL12@")).trim());
    PL13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL13@")).trim());
    PL14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL14@")).trim());
    PL15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_82.setVisible(lexique.isPresent("DTDEBX"));
    OBJ_78.setVisible(lexique.isPresent("DTDEBX"));
    // DTFINX.setVisible( lexique.isPresent("DTFINX"));
    // DTDEBX.setVisible( lexique.isPresent("DTDEBX"));
    OBJ_81.setVisible(lexique.isPresent("DTDEBX"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void DoubleMouseClicked(MouseEvent e) {
    if (e.getClickCount() == 2) {
      String nomZone = e.getComponent().getName();
      int ligne;
      if (nomZone.length() == 5) {
        ligne = Integer.parseInt(nomZone.substring(3, 5));
      }
      else {
        if (nomZone.startsWith("Z")) {
          ligne = Integer.parseInt(nomZone.substring(3, 4));
        }
        else {
          ligne = Integer.parseInt(nomZone.substring(2, 4));
        }
      }
      String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
      lexique.HostFieldPutData(top, 0, "1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WETB = new XRiTextField();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    BT_ChgSoc = new SNBoutonDetail();
    WCNV = new XRiTextField();
    CLNOM = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    p_TCI1 = new JXTitledPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    LD01 = new RiZoneSortie();
    LD02 = new RiZoneSortie();
    LD03 = new RiZoneSortie();
    LD04 = new RiZoneSortie();
    LD05 = new RiZoneSortie();
    LD06 = new RiZoneSortie();
    LD07 = new RiZoneSortie();
    LD08 = new RiZoneSortie();
    LD09 = new RiZoneSortie();
    LD10 = new RiZoneSortie();
    LD11 = new RiZoneSortie();
    LD12 = new RiZoneSortie();
    LD13 = new RiZoneSortie();
    LD14 = new RiZoneSortie();
    LD15 = new RiZoneSortie();
    ZL21 = new XRiTextField();
    ZL22 = new XRiTextField();
    ZL23 = new XRiTextField();
    ZL24 = new XRiTextField();
    ZL25 = new XRiTextField();
    ZL26 = new XRiTextField();
    ZL27 = new XRiTextField();
    ZL28 = new XRiTextField();
    ZL29 = new XRiTextField();
    ZL210 = new XRiTextField();
    ZL211 = new XRiTextField();
    ZL212 = new XRiTextField();
    ZL213 = new XRiTextField();
    ZL214 = new XRiTextField();
    ZL215 = new XRiTextField();
    label2 = new JLabel();
    label4 = new JLabel();
    PL01 = new JLabel();
    PL02 = new JLabel();
    PL03 = new JLabel();
    PL04 = new JLabel();
    PL05 = new JLabel();
    PL06 = new JLabel();
    PL07 = new JLabel();
    PL08 = new JLabel();
    PL09 = new JLabel();
    PL10 = new JLabel();
    PL11 = new JLabel();
    PL12 = new JLabel();
    PL13 = new JLabel();
    PL14 = new JLabel();
    PL15 = new JLabel();
    panel2 = new JPanel();
    OBJ_81 = new JLabel();
    DTDEBX = new XRiCalendrier();
    DTFINX = new XRiCalendrier();
    OBJ_78 = new JLabel();
    OBJ_82 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Conditions de ventes articles");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WETB ----
          WETB.setName("WETB");

          //---- OBJ_27 ----
          OBJ_27.setText("Etablissement");
          OBJ_27.setName("OBJ_27");

          //---- OBJ_28 ----
          OBJ_28.setText("Code CN");
          OBJ_28.setName("OBJ_28");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- WCNV ----
          WCNV.setName("WCNV");

          //---- CLNOM ----
          CLNOM.setText("@CLNOM@");
          CLNOM.setOpaque(false);
          CLNOM.setName("CLNOM");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(38, 38, 38)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(65, 65, 65)
                    .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Mode saisie pleine page");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Mode de visualisation");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Recherche client");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("ON/OFF articles d\u00e9sact.");
              riSousMenu_bt9.setToolTipText("Mise ON/OFF de l'affichage des articles d\u00e9sactiv\u00e9s");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autre affichage");
              riSousMenu_bt10.setToolTipText("Autre affichage en recherche articles");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(640, 601));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(640, 601));
          p_contenu.setName("p_contenu");

          //======== p_TCI1 ========
          {
            p_TCI1.setTitle("R\u00e9sultat de la recherche");
            p_TCI1.setBorder(new DropShadowBorder());
            p_TCI1.setTitleFont(p_TCI1.getTitleFont().deriveFont(p_TCI1.getTitleFont().getStyle() | Font.BOLD));
            p_TCI1.setMinimumSize(new Dimension(168, 500));
            p_TCI1.setPreferredSize(new Dimension(168, 520));
            p_TCI1.setName("p_TCI1");
            Container p_TCI1ContentContainer = p_TCI1.getContentContainer();
            p_TCI1ContentContainer.setLayout(null);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder(""));
              panel1.setOpaque(false);
              panel1.setMinimumSize(new Dimension(516, 400));
              panel1.setPreferredSize(new Dimension(516, 410));
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- BT_PGUP ----
              BT_PGUP.setText("");
              BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
              BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGUP.setName("BT_PGUP");
              panel1.add(BT_PGUP);
              BT_PGUP.setBounds(485, 30, 25, 175);

              //---- BT_PGDOWN ----
              BT_PGDOWN.setText("");
              BT_PGDOWN.setToolTipText("Page suivante");
              BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BT_PGDOWN.setName("BT_PGDOWN");
              panel1.add(BT_PGDOWN);
              BT_PGDOWN.setBounds(485, 235, 25, 175);

              //---- LD01 ----
              LD01.setText("@LD01@ ");
              LD01.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD01.setName("LD01");
              LD01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD01);
              LD01.setBounds(26, 34, 370, LD01.getPreferredSize().height);

              //---- LD02 ----
              LD02.setText("@LD02@");
              LD02.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD02.setName("LD02");
              LD02.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD02);
              LD02.setBounds(26, 59, 370, LD02.getPreferredSize().height);

              //---- LD03 ----
              LD03.setText("@LD03@");
              LD03.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD03.setName("LD03");
              LD03.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD03);
              LD03.setBounds(26, 84, 370, LD03.getPreferredSize().height);

              //---- LD04 ----
              LD04.setText("@LD04@");
              LD04.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD04.setName("LD04");
              LD04.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD04);
              LD04.setBounds(26, 109, 370, LD04.getPreferredSize().height);

              //---- LD05 ----
              LD05.setText("@LD05@");
              LD05.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD05.setName("LD05");
              LD05.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD05);
              LD05.setBounds(26, 134, 370, LD05.getPreferredSize().height);

              //---- LD06 ----
              LD06.setText("@LD06@");
              LD06.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD06.setName("LD06");
              LD06.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD06);
              LD06.setBounds(26, 159, 370, LD06.getPreferredSize().height);

              //---- LD07 ----
              LD07.setText("@LD07@");
              LD07.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD07.setName("LD07");
              LD07.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD07);
              LD07.setBounds(26, 184, 370, LD07.getPreferredSize().height);

              //---- LD08 ----
              LD08.setText("@LD08@");
              LD08.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD08.setName("LD08");
              LD08.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD08);
              LD08.setBounds(26, 209, 370, LD08.getPreferredSize().height);

              //---- LD09 ----
              LD09.setText("@LD09@");
              LD09.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD09.setName("LD09");
              LD09.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD09);
              LD09.setBounds(26, 234, 370, LD09.getPreferredSize().height);

              //---- LD10 ----
              LD10.setText("@LD10@");
              LD10.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD10.setName("LD10");
              LD10.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD10);
              LD10.setBounds(26, 259, 370, LD10.getPreferredSize().height);

              //---- LD11 ----
              LD11.setText("@LD11@");
              LD11.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD11.setName("LD11");
              LD11.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD11);
              LD11.setBounds(26, 284, 370, LD11.getPreferredSize().height);

              //---- LD12 ----
              LD12.setText("@LD12@");
              LD12.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD12.setName("LD12");
              LD12.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD12);
              LD12.setBounds(26, 309, 370, LD12.getPreferredSize().height);

              //---- LD13 ----
              LD13.setText("@LD13@");
              LD13.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD13.setName("LD13");
              LD13.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD13);
              LD13.setBounds(26, 334, 370, LD13.getPreferredSize().height);

              //---- LD14 ----
              LD14.setText("@LD14@");
              LD14.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD14.setName("LD14");
              LD14.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD14);
              LD14.setBounds(26, 359, 370, LD14.getPreferredSize().height);

              //---- LD15 ----
              LD15.setText("@LD15@");
              LD15.setFont(new Font("Courier New", Font.PLAIN, 14));
              LD15.setName("LD15");
              LD15.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(LD15);
              LD15.setBounds(26, 384, 370, LD15.getPreferredSize().height);

              //---- ZL21 ----
              ZL21.setName("ZL21");
              ZL21.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL21);
              ZL21.setBounds(410, 32, 34, ZL21.getPreferredSize().height);

              //---- ZL22 ----
              ZL22.setName("ZL22");
              ZL22.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL22);
              ZL22.setBounds(410, 57, 34, ZL22.getPreferredSize().height);

              //---- ZL23 ----
              ZL23.setName("ZL23");
              ZL23.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL23);
              ZL23.setBounds(410, 82, 34, ZL23.getPreferredSize().height);

              //---- ZL24 ----
              ZL24.setName("ZL24");
              ZL24.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL24);
              ZL24.setBounds(410, 107, 34, ZL24.getPreferredSize().height);

              //---- ZL25 ----
              ZL25.setName("ZL25");
              ZL25.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL25);
              ZL25.setBounds(410, 132, 34, ZL25.getPreferredSize().height);

              //---- ZL26 ----
              ZL26.setName("ZL26");
              ZL26.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL26);
              ZL26.setBounds(410, 157, 34, ZL26.getPreferredSize().height);

              //---- ZL27 ----
              ZL27.setName("ZL27");
              ZL27.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL27);
              ZL27.setBounds(410, 182, 34, ZL27.getPreferredSize().height);

              //---- ZL28 ----
              ZL28.setName("ZL28");
              ZL28.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL28);
              ZL28.setBounds(410, 207, 34, ZL28.getPreferredSize().height);

              //---- ZL29 ----
              ZL29.setName("ZL29");
              ZL29.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL29);
              ZL29.setBounds(410, 232, 34, ZL29.getPreferredSize().height);

              //---- ZL210 ----
              ZL210.setName("ZL210");
              ZL210.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL210);
              ZL210.setBounds(410, 257, 34, ZL210.getPreferredSize().height);

              //---- ZL211 ----
              ZL211.setName("ZL211");
              ZL211.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL211);
              ZL211.setBounds(410, 282, 34, ZL211.getPreferredSize().height);

              //---- ZL212 ----
              ZL212.setName("ZL212");
              ZL212.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL212);
              ZL212.setBounds(410, 307, 34, ZL212.getPreferredSize().height);

              //---- ZL213 ----
              ZL213.setName("ZL213");
              ZL213.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL213);
              ZL213.setBounds(410, 332, 34, ZL213.getPreferredSize().height);

              //---- ZL214 ----
              ZL214.setName("ZL214");
              ZL214.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL214);
              ZL214.setBounds(410, 357, 34, ZL214.getPreferredSize().height);

              //---- ZL215 ----
              ZL215.setName("ZL215");
              ZL215.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  DoubleMouseClicked(e);
                }
              });
              panel1.add(ZL215);
              ZL215.setBounds(410, 382, 34, ZL215.getPreferredSize().height);

              //---- label2 ----
              label2.setText("@UTIT@");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");
              panel1.add(label2);
              label2.setBounds(26, 10, 370, 20);

              //---- label4 ----
              label4.setText("Colonne");
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
              label4.setName("label4");
              panel1.add(label4);
              label4.setBounds(410, 10, 100, 20);

              //---- PL01 ----
              PL01.setText("@PL01@");
              PL01.setForeground(new Color(153, 0, 0));
              PL01.setHorizontalAlignment(SwingConstants.CENTER);
              PL01.setName("PL01");
              panel1.add(PL01);
              PL01.setBounds(450, 32, 20, 28);

              //---- PL02 ----
              PL02.setText("@PL02@");
              PL02.setForeground(new Color(153, 0, 0));
              PL02.setHorizontalAlignment(SwingConstants.CENTER);
              PL02.setName("PL02");
              panel1.add(PL02);
              PL02.setBounds(450, 57, 20, 28);

              //---- PL03 ----
              PL03.setText("@PL03@");
              PL03.setForeground(new Color(153, 0, 0));
              PL03.setHorizontalAlignment(SwingConstants.CENTER);
              PL03.setName("PL03");
              panel1.add(PL03);
              PL03.setBounds(450, 82, 20, 28);

              //---- PL04 ----
              PL04.setText("@PL04@");
              PL04.setForeground(new Color(153, 0, 0));
              PL04.setHorizontalAlignment(SwingConstants.CENTER);
              PL04.setName("PL04");
              panel1.add(PL04);
              PL04.setBounds(450, 107, 20, 28);

              //---- PL05 ----
              PL05.setText("@PL05@");
              PL05.setForeground(new Color(153, 0, 0));
              PL05.setHorizontalAlignment(SwingConstants.CENTER);
              PL05.setName("PL05");
              panel1.add(PL05);
              PL05.setBounds(450, 132, 20, 28);

              //---- PL06 ----
              PL06.setText("@PL06@");
              PL06.setForeground(new Color(153, 0, 0));
              PL06.setHorizontalAlignment(SwingConstants.CENTER);
              PL06.setName("PL06");
              panel1.add(PL06);
              PL06.setBounds(450, 157, 20, 28);

              //---- PL07 ----
              PL07.setText("@PL07@");
              PL07.setForeground(new Color(153, 0, 0));
              PL07.setHorizontalAlignment(SwingConstants.CENTER);
              PL07.setName("PL07");
              panel1.add(PL07);
              PL07.setBounds(450, 182, 20, 28);

              //---- PL08 ----
              PL08.setText("@PL08@");
              PL08.setForeground(new Color(153, 0, 0));
              PL08.setHorizontalAlignment(SwingConstants.CENTER);
              PL08.setName("PL08");
              panel1.add(PL08);
              PL08.setBounds(450, 207, 20, 28);

              //---- PL09 ----
              PL09.setText("@PL09@");
              PL09.setForeground(new Color(153, 0, 0));
              PL09.setHorizontalAlignment(SwingConstants.CENTER);
              PL09.setName("PL09");
              panel1.add(PL09);
              PL09.setBounds(450, 232, 20, 28);

              //---- PL10 ----
              PL10.setText("@PL10@");
              PL10.setForeground(new Color(153, 0, 0));
              PL10.setHorizontalAlignment(SwingConstants.CENTER);
              PL10.setName("PL10");
              panel1.add(PL10);
              PL10.setBounds(450, 257, 20, 28);

              //---- PL11 ----
              PL11.setText("@PL11@");
              PL11.setForeground(new Color(153, 0, 0));
              PL11.setHorizontalAlignment(SwingConstants.CENTER);
              PL11.setName("PL11");
              panel1.add(PL11);
              PL11.setBounds(450, 282, 20, 28);

              //---- PL12 ----
              PL12.setText("@PL12@");
              PL12.setForeground(new Color(153, 0, 0));
              PL12.setHorizontalAlignment(SwingConstants.CENTER);
              PL12.setName("PL12");
              panel1.add(PL12);
              PL12.setBounds(450, 307, 20, 28);

              //---- PL13 ----
              PL13.setText("@PL13@");
              PL13.setForeground(new Color(153, 0, 0));
              PL13.setHorizontalAlignment(SwingConstants.CENTER);
              PL13.setName("PL13");
              panel1.add(PL13);
              PL13.setBounds(450, 332, 20, 28);

              //---- PL14 ----
              PL14.setText("@PL14@");
              PL14.setForeground(new Color(153, 0, 0));
              PL14.setHorizontalAlignment(SwingConstants.CENTER);
              PL14.setName("PL14");
              panel1.add(PL14);
              PL14.setBounds(450, 357, 20, 28);

              //---- PL15 ----
              PL15.setText("@PL15@");
              PL15.setForeground(new Color(153, 0, 0));
              PL15.setHorizontalAlignment(SwingConstants.CENTER);
              PL15.setName("PL15");
              panel1.add(PL15);
              PL15.setBounds(450, 382, 20, 28);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            p_TCI1ContentContainer.add(panel1);
            panel1.setBounds(15, 85, 550, 440);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_81 ----
              OBJ_81.setText("Dates d'application");
              OBJ_81.setName("OBJ_81");
              panel2.add(OBJ_81);
              OBJ_81.setBounds(25, 24, 119, 20);

              //---- DTDEBX ----
              DTDEBX.setName("DTDEBX");
              panel2.add(DTDEBX);
              DTDEBX.setBounds(180, 20, 105, DTDEBX.getPreferredSize().height);

              //---- DTFINX ----
              DTFINX.setName("DTFINX");
              panel2.add(DTFINX);
              DTFINX.setBounds(340, 20, 105, DTFINX.getPreferredSize().height);

              //---- OBJ_78 ----
              OBJ_78.setText("au");
              OBJ_78.setName("OBJ_78");
              panel2.add(OBJ_78);
              OBJ_78.setBounds(305, 24, 18, 20);

              //---- OBJ_82 ----
              OBJ_82.setText("du");
              OBJ_82.setName("OBJ_82");
              panel2.add(OBJ_82);
              OBJ_82.setBounds(150, 24, 25, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            p_TCI1ContentContainer.add(panel2);
            panel2.setBounds(15, 10, 550, 70);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(p_TCI1, GroupLayout.PREFERRED_SIZE, 585, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(p_TCI1, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField WETB;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private SNBoutonDetail BT_ChgSoc;
  private XRiTextField WCNV;
  private RiZoneSortie CLNOM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel p_TCI1;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie LD01;
  private RiZoneSortie LD02;
  private RiZoneSortie LD03;
  private RiZoneSortie LD04;
  private RiZoneSortie LD05;
  private RiZoneSortie LD06;
  private RiZoneSortie LD07;
  private RiZoneSortie LD08;
  private RiZoneSortie LD09;
  private RiZoneSortie LD10;
  private RiZoneSortie LD11;
  private RiZoneSortie LD12;
  private RiZoneSortie LD13;
  private RiZoneSortie LD14;
  private RiZoneSortie LD15;
  private XRiTextField ZL21;
  private XRiTextField ZL22;
  private XRiTextField ZL23;
  private XRiTextField ZL24;
  private XRiTextField ZL25;
  private XRiTextField ZL26;
  private XRiTextField ZL27;
  private XRiTextField ZL28;
  private XRiTextField ZL29;
  private XRiTextField ZL210;
  private XRiTextField ZL211;
  private XRiTextField ZL212;
  private XRiTextField ZL213;
  private XRiTextField ZL214;
  private XRiTextField ZL215;
  private JLabel label2;
  private JLabel label4;
  private JLabel PL01;
  private JLabel PL02;
  private JLabel PL03;
  private JLabel PL04;
  private JLabel PL05;
  private JLabel PL06;
  private JLabel PL07;
  private JLabel PL08;
  private JLabel PL09;
  private JLabel PL10;
  private JLabel PL11;
  private JLabel PL12;
  private JLabel PL13;
  private JLabel PL14;
  private JLabel PL15;
  private JPanel panel2;
  private JLabel OBJ_81;
  private XRiCalendrier DTDEBX;
  private XRiCalendrier DTFINX;
  private JLabel OBJ_78;
  private JLabel OBJ_82;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
