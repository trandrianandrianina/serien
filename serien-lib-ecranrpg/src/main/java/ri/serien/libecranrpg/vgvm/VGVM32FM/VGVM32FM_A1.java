
package ri.serien.libecranrpg.vgvm.VGVM32FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVM32FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] TOPAUT_Value = { "0", "1", };
  private String[] T2TCD_Value = { "0", "1", };
  
  public VGVM32FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    setDialog(true);
    // Titre de la fenêtre
    setTitle("Autorisation de vente");
    
    // Ajout
    initDiverses();
    T2TCD.setValeurs(T2TCD_Value, null);
    TOPAUT.setValeurs(TOPAUT_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    boolean isConsultation = lexique.isTrue("11");
    
    // Fonctions diverses après initioalisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Permet de gérer les modes d'affichage
    String typeArgument = lexique.HostFieldGetData("LIBARG");
    
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    if (typeArgument.startsWith("Client")) {
      lbArgument.setText("Client");
      
      // Visibilité des composants
      snClient.setVisible(true);
      snClient.setEnabled(!isConsultation);
      snPays.setVisible(false);
      snZoneGeographique.setVisible(false);
      snCategorieClient.setVisible(false);
      snCanalDeVente.setVisible(false);
      
      chargerComposantClient();
    }
    else if (typeArgument.startsWith("Categorie")) {
      lbArgument.setText("Catégorie client");
      
      // Visibilité des composants
      snClient.setVisible(false);
      snPays.setVisible(false);
      snZoneGeographique.setVisible(false);
      snCategorieClient.setVisible(true);
      snCategorieClient.setEnabled(!isConsultation);
      snCanalDeVente.setVisible(false);
      
      chargerComposantCategorieClient();
    }
    else if (typeArgument.startsWith("Pays")) {
      lbArgument.setText("Pays");
      
      // Visibilité des composants
      snClient.setVisible(false);
      snPays.setVisible(true);
      snPays.setEnabled(!isConsultation);
      snZoneGeographique.setVisible(false);
      snCategorieClient.setVisible(false);
      snCanalDeVente.setVisible(false);
      
      chargerComposantPays();
    }
    else if (typeArgument.startsWith("Zone")) {
      lbArgument.setText("Zone géographique");
      
      // Visibilité des composants
      snClient.setVisible(false);
      snPays.setVisible(false);
      snZoneGeographique.setVisible(true);
      snZoneGeographique.setEnabled(!isConsultation);
      snCategorieClient.setVisible(false);
      snCanalDeVente.setVisible(false);
      
      chargerComposantZoneGeographique();
    }
    else if (typeArgument.startsWith("Canal")) {
      lbArgument.setText("Canal de vente");
      
      // Visibilité des composants
      snClient.setVisible(false);
      snPays.setVisible(false);
      snZoneGeographique.setVisible(false);
      snCategorieClient.setVisible(false);
      snCanalDeVente.setVisible(true);
      snCanalDeVente.setEnabled(!isConsultation);
      
      chargerComposantCanalDeVente();
    }
    else {
      lbArgument.setVisible(false);
    }
    
    // Visibilité
    
    String etatBlocage = String.valueOf(TOPAUT.getSelectedItem());
    if (etatBlocage instanceof String) {
      pnlBlocage.setVisible(etatBlocage.contentEquals("Interdite"));
    }
    
    // Charger composant snPlageDate
    snPlageValidite.setDateDebutParChampRPG(lexique, "T1DTDX");
    snPlageValidite.setDateFinParChampRPG(lexique, "T1DTFX");
    snPlageValidite.setEnabled(!lexique.isTrue("11"));
    
    // Charger composant article
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(idEtablissement);
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "T1RAT");
    snArticle.setEnabled(!lexique.isTrue("11"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Composant snPlageDate
    snPlageValidite.renseignerChampRPGDebut(lexique, "T1DTDX");
    snPlageValidite.renseignerChampRPGFin(lexique, "T1DTFX");
    
    // Composant snArticle
    snArticle.renseignerChampRPG(lexique, "T1RAT");
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void TOPAUTItemStateChanged(ItemEvent e) {
    String etatBlocage = String.valueOf(TOPAUT.getSelectedItem());
    if (etatBlocage instanceof String) {
      pnlBlocage.setVisible(etatBlocage.contentEquals("Interdite"));
    }
  }
  
  private void chargerComposantClient() {
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    // Charger composant
    snClient.setSession(getSession());
    snClient.setIdEtablissement(idEtablissement);
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "T1CNV");
  }
  
  private void chargerComposantCategorieClient() {
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    // Charger composant
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(idEtablissement);
    snCategorieClient.charger(false);
    snCategorieClient.setSelectionParChampRPG(lexique, "T1CNV");
  }
  
  private void chargerComposantPays() {
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    // Charger composant
    snPays.setSession(getSession());
    snPays.setIdEtablissement(idEtablissement);
    snPays.charger(false);
    snPays.setSelectionParChampRPG(lexique, "T1CNV");
  }
  
  private void chargerComposantZoneGeographique() {
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    // Charger composant
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(idEtablissement);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "T1CNV");
  }
  
  private void chargerComposantCanalDeVente() {
    // Récupérer l'idEtablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("T1ETB"));
    
    // Charger composant
    snCanalDeVente.setSession(getSession());
    snCanalDeVente.setIdEtablissement(idEtablissement);
    snCanalDeVente.charger(false);
    snCanalDeVente.setSelectionParChampRPG(lexique, "T1CNV");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlGestionAutorisationVente = new SNPanelTitre();
    pnlChampsHaut = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbArgument = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    snCanalDeVente = new SNCanalDeVente();
    snCategorieClient = new SNCategorieClient();
    snClient = new SNClientPrincipal();
    snPays = new SNPays();
    snZoneGeographique = new SNZoneGeographique();
    lbAutorisation = new SNLabelChamp();
    TOPAUT = new XRiComboBox();
    pnlBlocage = new SNPanel();
    lbBlocage = new SNLabelChamp();
    T2TCD = new XRiComboBox();
    pnlPeriodeDeValidite = new SNPanel();
    lbPeriodeDeValidite = new SNLabelChamp();
    snPlageValidite = new SNPlageDate();
    snBarreBouton = new SNBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(750, 400));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(750, 500));
      pnlContenu.setMinimumSize(new Dimension(750, 500));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlGestionAutorisationVente ========
      {
        pnlGestionAutorisationVente.setTitre("Gestion autorisation de vente");
        pnlGestionAutorisationVente.setName("pnlGestionAutorisationVente");
        pnlGestionAutorisationVente.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGestionAutorisationVente.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGestionAutorisationVente.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlGestionAutorisationVente.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGestionAutorisationVente.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlChampsHaut ========
        {
          pnlChampsHaut.setName("pnlChampsHaut");
          pnlChampsHaut.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlChampsHaut.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlChampsHaut.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlChampsHaut.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlChampsHaut.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setName("lbArticle");
          pnlChampsHaut.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snArticle ----
          snArticle.setName("snArticle");
          pnlChampsHaut.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbArgument ----
          lbArgument.setText("labelTypeArgument");
          lbArgument.setName("lbArgument");
          pnlChampsHaut.add(lbArgument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- snCanalDeVente ----
            snCanalDeVente.setName("snCanalDeVente");
            sNPanel1.add(snCanalDeVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snCategorieClient ----
            snCategorieClient.setName("snCategorieClient");
            sNPanel1.add(snCategorieClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snClient ----
            snClient.setName("snClient");
            sNPanel1.add(snClient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snPays ----
            snPays.setName("snPays");
            sNPanel1.add(snPays, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snZoneGeographique ----
            snZoneGeographique.setName("snZoneGeographique");
            sNPanel1.add(snZoneGeographique, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlChampsHaut.add(sNPanel1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbAutorisation ----
          lbAutorisation.setText("Autorisation de vente");
          lbAutorisation.setName("lbAutorisation");
          pnlChampsHaut.add(lbAutorisation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- TOPAUT ----
          TOPAUT.setModel(new DefaultComboBoxModel(new String[] {
            "Autoris\u00e9e",
            "Interdite"
          }));
          TOPAUT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOPAUT.setFont(new Font("sansserif", Font.PLAIN, 14));
          TOPAUT.setMinimumSize(new Dimension(100, 30));
          TOPAUT.setPreferredSize(new Dimension(100, 30));
          TOPAUT.setMaximumSize(new Dimension(100, 30));
          TOPAUT.setName("TOPAUT");
          TOPAUT.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              TOPAUTItemStateChanged(e);
            }
          });
          pnlChampsHaut.add(TOPAUT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGestionAutorisationVente.add(pnlChampsHaut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlBlocage ========
        {
          pnlBlocage.setName("pnlBlocage");
          pnlBlocage.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlBlocage.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlBlocage.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlBlocage.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlBlocage.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbBlocage ----
          lbBlocage.setText("En cas d'interdiction");
          lbBlocage.setName("lbBlocage");
          pnlBlocage.add(lbBlocage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- T2TCD ----
          T2TCD.setModel(new DefaultComboBoxModel(new String[] {
            "Blocage",
            "Message"
          }));
          T2TCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T2TCD.setFont(new Font("sansserif", Font.PLAIN, 14));
          T2TCD.setMinimumSize(new Dimension(100, 30));
          T2TCD.setPreferredSize(new Dimension(100, 30));
          T2TCD.setMaximumSize(new Dimension(100, 30));
          T2TCD.setName("T2TCD");
          pnlBlocage.add(T2TCD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGestionAutorisationVente.add(pnlBlocage, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlPeriodeDeValidite ========
        {
          pnlPeriodeDeValidite.setName("pnlPeriodeDeValidite");
          pnlPeriodeDeValidite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPeriodeDeValidite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPeriodeDeValidite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlPeriodeDeValidite.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlPeriodeDeValidite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbPeriodeDeValidite ----
          lbPeriodeDeValidite.setText("P\u00e9riode de validit\u00e9 optionnelle");
          lbPeriodeDeValidite.setPreferredSize(new Dimension(200, 30));
          lbPeriodeDeValidite.setMinimumSize(new Dimension(200, 30));
          lbPeriodeDeValidite.setMaximumSize(new Dimension(200, 30));
          lbPeriodeDeValidite.setName("lbPeriodeDeValidite");
          pnlPeriodeDeValidite.add(lbPeriodeDeValidite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snPlageValidite ----
          snPlageValidite.setMaximumSize(new Dimension(400, 30));
          snPlageValidite.setMinimumSize(new Dimension(400, 30));
          snPlageValidite.setPreferredSize(new Dimension(400, 30));
          snPlageValidite.setName("snPlageValidite");
          pnlPeriodeDeValidite.add(snPlageValidite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGestionAutorisationVente.add(pnlPeriodeDeValidite, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGestionAutorisationVente);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlGestionAutorisationVente;
  private SNPanel pnlChampsHaut;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbArgument;
  private SNPanel sNPanel1;
  private SNCanalDeVente snCanalDeVente;
  private SNCategorieClient snCategorieClient;
  private SNClientPrincipal snClient;
  private SNPays snPays;
  private SNZoneGeographique snZoneGeographique;
  private SNLabelChamp lbAutorisation;
  private XRiComboBox TOPAUT;
  private SNPanel pnlBlocage;
  private SNLabelChamp lbBlocage;
  private XRiComboBox T2TCD;
  private SNPanel pnlPeriodeDeValidite;
  private SNLabelChamp lbPeriodeDeValidite;
  private SNPlageDate snPlageValidite;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
