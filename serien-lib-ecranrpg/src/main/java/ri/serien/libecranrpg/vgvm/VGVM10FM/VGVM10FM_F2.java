
package ri.serien.libecranrpg.vgvm.VGVM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM10FM_F2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String optionCliquee = null;
  
  public VGVM10FM_F2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WMCB.setValeursSelection("1", " ");
    WOATT.setValeurs("P", WOATT_GRP);
    WOATT_NOTE.setValeurs("N");
    setCloseKey("ENTER", "F3", "F12");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    riBouton1.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" @OPT1@ - @LOPT1@")).trim());
    riBouton2.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" @OPT2@ - @LOPT2@")).trim());
    riBouton3.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" @OPT3@ - @LOPT3@")).trim());
    riBouton4.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" @OPT4@ - @LOPT4@")).trim());
    riBouton5.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" @OPT5@ - @LOPT5@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOPT6@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // WMCB.setSelected(!lexique.HostFieldGetData("WMCB").trim().equalsIgnoreCase(""));
    WMCB.setVisible(lexique.isTrue("79"));
    WTP6.setEnabled(lexique.isPresent("WTP6"));
    // OBJ_21.setVisible( lexique.isPresent("WOATT"));
    // OBJ_21.setSelected(lexique.HostFieldGetData("WOATT").equalsIgnoreCase("N"));
    E1NUM.setEnabled(lexique.isPresent("E1NUM"));
    // OBJ_30.setVisible( lexique.isPresent("WOATT"));
    // OBJ_30.setSelected(lexique.HostFieldGetData("WOATT").equalsIgnoreCase("P"));
    OBJ_22.setVisible(lexique.isPresent("LOPT6"));
    riBouton1.setVisible(!lexique.HostFieldGetData("OPT1").trim().equalsIgnoreCase(""));
    riBouton2.setVisible(!lexique.HostFieldGetData("OPT2").trim().equalsIgnoreCase(""));
    riBouton3.setVisible(!lexique.HostFieldGetData("OPT3").trim().equalsIgnoreCase(""));
    riBouton4.setVisible(!lexique.HostFieldGetData("OPT4").trim().equalsIgnoreCase(""));
    riBouton5.setVisible(!lexique.HostFieldGetData("OPT5").trim().equalsIgnoreCase(""));
    
    if (!lexique.HostFieldGetData("WTP1").trim().equals("")) {
      riBouton1.doClick();
    }
    if (!lexique.HostFieldGetData("WTP2").trim().equals("")) {
      riBouton2.doClick();
    }
    if (!lexique.HostFieldGetData("WTP3").trim().equals("")) {
      riBouton3.doClick();
    }
    if (!lexique.HostFieldGetData("WTP4").trim().equals("")) {
      riBouton4.doClick();
    }
    if (!lexique.HostFieldGetData("WTP5").trim().equals("")) {
      riBouton5.doClick();
    }
    if (!lexique.HostFieldGetData("WTP6").trim().equals("")) {
      riBouton6.doClick();
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Options de fin de traitement"));
    
    riMenu_bt1.setIcon(lexique.chargerImage("images/fin_p.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (OBJ_21.isSelected())
    // lexique.HostFieldPutData("WOATT", 0, "N");
    // if (OBJ_30.isSelected())
    // lexique.HostFieldPutData("WOATT", 0, "P");
    
    // if (WMCB.isSelected())
    // lexique.HostFieldPutData("WMCB", 0, "1");
    // else
    // lexique.HostFieldPutData("WMCB", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP1", 0, " ");
    lexique.HostFieldPutData("WTP2", 0, " ");
    lexique.HostFieldPutData("WTP3", 0, " ");
    lexique.HostFieldPutData("WTP4", 0, " ");
    lexique.HostFieldPutData("WTP5", 0, " ");
    lexique.HostFieldPutData("WTP6", 0, " ");
    
    if (optionCliquee != null) {
      lexique.HostFieldPutData(optionCliquee, 0, "X");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riBouton1ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP1";
    option.setText(lexique.HostFieldGetData("OPT1").trim());
  }
  
  private void riBouton2ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP2";
    option.setText(lexique.HostFieldGetData("OPT2").trim());
  }
  
  private void riBouton3ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP3";
    option.setText(lexique.HostFieldGetData("OPT3").trim());
  }
  
  private void riBouton4ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP4";
    option.setText(lexique.HostFieldGetData("OPT4").trim());
  }
  
  private void riBouton5ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP5";
    option.setText(lexique.HostFieldGetData("OPT5").trim());
  }
  
  private void riBouton6ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP6";
    option.setText(lexique.HostFieldGetData("OPT6").trim());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_19 = new JLabel();
    E1NUM = new RiZoneSortie();
    panel1 = new JPanel();
    riBouton1 = new SNBoutonLeger();
    riBouton2 = new SNBoutonLeger();
    riBouton3 = new SNBoutonLeger();
    riBouton4 = new SNBoutonLeger();
    riBouton5 = new SNBoutonLeger();
    label1 = new JLabel();
    option = new RiZoneSortie();
    riBouton6 = new SNBoutonLeger();
    WMCB = new XRiCheckBox();
    panel5 = new JPanel();
    WTP6 = new XRiTextField();
    OBJ_22 = new RiZoneSortie();
    WOATT = new XRiRadioButton();
    WOATT_NOTE = new XRiRadioButton();
    WOATT_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(695, 430));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);

          //======== riMenu1 ========
          {
            riMenu1.setName("riMenu1");

            //---- riMenu_bt1 ----
            riMenu_bt1.setText("Abandonner");
            riMenu_bt1.setToolTipText("Abandonner");
            riMenu_bt1.setName("riMenu_bt1");
            riMenu_bt1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_bt1ActionPerformed(e);
              }
            });
            riMenu1.add(riMenu_bt1);
          }
          menus_bas.add(riMenu1);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Sur le bon d'origine"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_19 ----
          OBJ_19.setText("Bon d'origine");
          OBJ_19.setName("OBJ_19");
          panel3.add(OBJ_19);
          OBJ_19.setBounds(22, 42, 88, 20);

          //---- E1NUM ----
          E1NUM.setBorder(new BevelBorder(BevelBorder.LOWERED));
          E1NUM.setText("@E1NUM@");
          E1NUM.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NUM.setName("E1NUM");
          panel3.add(E1NUM);
          E1NUM.setBounds(110, 40, 65, E1NUM.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(new VerticalLayout());

            //---- riBouton1 ----
            riBouton1.setText(" @OPT1@ - @LOPT1@");
            riBouton1.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton1.setMaximumSize(new Dimension(200, 26));
            riBouton1.setMinimumSize(new Dimension(20, 26));
            riBouton1.setPreferredSize(new Dimension(20, 26));
            riBouton1.setName("riBouton1");
            riBouton1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton1ActionPerformed(e);
              }
            });
            panel1.add(riBouton1);

            //---- riBouton2 ----
            riBouton2.setText(" @OPT2@ - @LOPT2@");
            riBouton2.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton2.setMaximumSize(new Dimension(200, 26));
            riBouton2.setMinimumSize(new Dimension(20, 26));
            riBouton2.setPreferredSize(new Dimension(20, 26));
            riBouton2.setName("riBouton2");
            riBouton2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton2ActionPerformed(e);
              }
            });
            panel1.add(riBouton2);

            //---- riBouton3 ----
            riBouton3.setText(" @OPT3@ - @LOPT3@");
            riBouton3.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton3.setMaximumSize(new Dimension(200, 26));
            riBouton3.setMinimumSize(new Dimension(20, 26));
            riBouton3.setPreferredSize(new Dimension(20, 26));
            riBouton3.setName("riBouton3");
            riBouton3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton3ActionPerformed(e);
              }
            });
            panel1.add(riBouton3);

            //---- riBouton4 ----
            riBouton4.setText(" @OPT4@ - @LOPT4@");
            riBouton4.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton4.setMaximumSize(new Dimension(200, 26));
            riBouton4.setMinimumSize(new Dimension(20, 26));
            riBouton4.setPreferredSize(new Dimension(20, 26));
            riBouton4.setName("riBouton4");
            riBouton4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton4ActionPerformed(e);
              }
            });
            panel1.add(riBouton4);

            //---- riBouton5 ----
            riBouton5.setText(" @OPT5@ - @LOPT5@");
            riBouton5.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton5.setMaximumSize(new Dimension(200, 26));
            riBouton5.setMinimumSize(new Dimension(20, 26));
            riBouton5.setPreferredSize(new Dimension(20, 26));
            riBouton5.setName("riBouton5");
            riBouton5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton5ActionPerformed(e);
              }
            });
            panel1.add(riBouton5);
          }
          panel3.add(panel1);
          panel1.setBounds(20, 75, 435, 160);

          //---- label1 ----
          label1.setText("Option");
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(20, 240, 55, 25);

          //---- option ----
          option.setName("option");
          panel3.add(option);
          option.setBounds(110, 240, 40, option.getPreferredSize().height);

          //---- riBouton6 ----
          riBouton6.setText(" COR - Modification du bon avant option de fin");
          riBouton6.setHorizontalAlignment(SwingConstants.LEFT);
          riBouton6.setMaximumSize(new Dimension(200, 26));
          riBouton6.setMinimumSize(new Dimension(20, 26));
          riBouton6.setPreferredSize(new Dimension(20, 26));
          riBouton6.setName("riBouton6");
          riBouton6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton6ActionPerformed(e);
            }
          });
          panel3.add(riBouton6);
          riBouton6.setBounds(20, 205, 435, riBouton6.getPreferredSize().height);

          //---- WMCB ----
          WMCB.setText("Modification du client sur le bon");
          WMCB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WMCB.setName("WMCB");
          panel3.add(WMCB);
          WMCB.setBounds(20, 275, 395, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(25, 15, 475, 310);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("Options d'\u00e9dition"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- WTP6 ----
          WTP6.setName("WTP6");
          panel5.add(WTP6);
          WTP6.setBounds(25, 40, 24, WTP6.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("@LOPT6@");
          OBJ_22.setName("OBJ_22");
          panel5.add(OBJ_22);
          OBJ_22.setBounds(55, 42, 395, OBJ_22.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel5);
        panel5.setBounds(25, 335, 475, 80);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- WOATT ----
    WOATT.setText("Positionnement");
    WOATT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    WOATT.setName("WOATT");

    //---- WOATT_NOTE ----
    WOATT_NOTE.setText("Not\u00e9");
    WOATT_NOTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    WOATT_NOTE.setName("WOATT_NOTE");

    //---- WOATT_GRP ----
    WOATT_GRP.add(WOATT);
    WOATT_GRP.add(WOATT_NOTE);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel OBJ_19;
  private RiZoneSortie E1NUM;
  private JPanel panel1;
  private SNBoutonLeger riBouton1;
  private SNBoutonLeger riBouton2;
  private SNBoutonLeger riBouton3;
  private SNBoutonLeger riBouton4;
  private SNBoutonLeger riBouton5;
  private JLabel label1;
  private RiZoneSortie option;
  private SNBoutonLeger riBouton6;
  private XRiCheckBox WMCB;
  private JPanel panel5;
  private XRiTextField WTP6;
  private RiZoneSortie OBJ_22;
  private XRiRadioButton WOATT;
  private XRiRadioButton WOATT_NOTE;
  private ButtonGroup WOATT_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
