
package ri.serien.libecranrpg.vgvm.VGVM01CV;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01CV_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01CV_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG17@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG18@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG19@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG20@")).trim());
    riZoneSortie5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG21@")).trim());
    riZoneSortie6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG22@")).trim());
    riZoneSortie7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG23@")).trim());
    riZoneSortie8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG24@")).trim());
    riZoneSortie9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG25@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("PERSONNALISATION"));
    
    

    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    menus_haut2 = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CV17 = new XRiTextField();
    CV18 = new XRiTextField();
    CV19 = new XRiTextField();
    CV20 = new XRiTextField();
    CV21 = new XRiTextField();
    CV22 = new XRiTextField();
    CV23 = new XRiTextField();
    CV24 = new XRiTextField();
    CV25 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    riZoneSortie4 = new RiZoneSortie();
    riZoneSortie5 = new RiZoneSortie();
    riZoneSortie6 = new RiZoneSortie();
    riZoneSortie7 = new RiZoneSortie();
    riZoneSortie8 = new RiZoneSortie();
    riZoneSortie9 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== menus_haut2 ========
            {
              menus_haut2.setMinimumSize(new Dimension(160, 520));
              menus_haut2.setPreferredSize(new Dimension(160, 520));
              menus_haut2.setBackground(new Color(238, 239, 241));
              menus_haut2.setAutoscrolls(true);
              menus_haut2.setName("menus_haut2");
              menus_haut2.setLayout(new VerticalLayout());
            }
            menus_haut.add(menus_haut2);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Plan comptable des ventes"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- CV17 ----
          CV17.setName("CV17");

          //---- CV18 ----
          CV18.setName("CV18");

          //---- CV19 ----
          CV19.setName("CV19");

          //---- CV20 ----
          CV20.setName("CV20");

          //---- CV21 ----
          CV21.setName("CV21");

          //---- CV22 ----
          CV22.setName("CV22");

          //---- CV23 ----
          CV23.setName("CV23");

          //---- CV24 ----
          CV24.setName("CV24");

          //---- CV25 ----
          CV25.setName("CV25");

          //---- label1 ----
          label1.setText("Num\u00e9ro CO escomptes TVA1");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("Num\u00e9ro CO escomptes TVA2");
          label2.setName("label2");

          //---- label3 ----
          label3.setText("Num\u00e9ro CO escomptes TVA3");
          label3.setName("label3");

          //---- label4 ----
          label4.setText("Num\u00e9ro CO escomptes TVA4");
          label4.setName("label4");

          //---- label5 ----
          label5.setText("Num\u00e9ro CO escomptes TVA5");
          label5.setName("label5");

          //---- label6 ----
          label6.setText("Num\u00e9ro CO escomptes TVA6");
          label6.setName("label6");

          //---- label7 ----
          label7.setText("Num\u00e9ro CO escomptes export CEE");
          label7.setName("label7");

          //---- label8 ----
          label8.setText("Num\u00e9ro CO escomptes export");
          label8.setName("label8");

          //---- label9 ----
          label9.setText("Num\u00e9ro CO escomptes assimil\u00e9es export");
          label9.setName("label9");

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@LCG17@");
          riZoneSortie1.setName("riZoneSortie1");

          //---- riZoneSortie2 ----
          riZoneSortie2.setText("@LCG18@");
          riZoneSortie2.setName("riZoneSortie2");

          //---- riZoneSortie3 ----
          riZoneSortie3.setText("@LCG19@");
          riZoneSortie3.setName("riZoneSortie3");

          //---- riZoneSortie4 ----
          riZoneSortie4.setText("@LCG20@");
          riZoneSortie4.setName("riZoneSortie4");

          //---- riZoneSortie5 ----
          riZoneSortie5.setText("@LCG21@");
          riZoneSortie5.setName("riZoneSortie5");

          //---- riZoneSortie6 ----
          riZoneSortie6.setText("@LCG22@");
          riZoneSortie6.setName("riZoneSortie6");

          //---- riZoneSortie7 ----
          riZoneSortie7.setText("@LCG23@");
          riZoneSortie7.setName("riZoneSortie7");

          //---- riZoneSortie8 ----
          riZoneSortie8.setText("@LCG24@");
          riZoneSortie8.setName("riZoneSortie8");

          //---- riZoneSortie9 ----
          riZoneSortie9.setText("@LCG25@");
          riZoneSortie9.setName("riZoneSortie9");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV17, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV18, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV19, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie3, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV20, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie4, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV21, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie5, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV22, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie6, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV23, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie7, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(15, 15, 15)
                    .addComponent(CV24, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie8, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label9, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                    .addComponent(CV25, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(riZoneSortie9, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label1))
                  .addComponent(CV17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label2))
                  .addComponent(CV18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label3))
                  .addComponent(CV19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label4))
                  .addComponent(CV20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label5))
                  .addComponent(CV21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label6))
                  .addComponent(CV22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label7))
                  .addComponent(CV23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label8))
                  .addComponent(CV24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(6, 6, 6)
                    .addComponent(label9))
                  .addComponent(CV25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(riZoneSortie9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 610, 365);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel menus_haut2;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField CV17;
  private XRiTextField CV18;
  private XRiTextField CV19;
  private XRiTextField CV20;
  private XRiTextField CV21;
  private XRiTextField CV22;
  private XRiTextField CV23;
  private XRiTextField CV24;
  private XRiTextField CV25;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private RiZoneSortie riZoneSortie3;
  private RiZoneSortie riZoneSortie4;
  private RiZoneSortie riZoneSortie5;
  private RiZoneSortie riZoneSortie6;
  private RiZoneSortie riZoneSortie7;
  private RiZoneSortie riZoneSortie8;
  private RiZoneSortie riZoneSortie9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
