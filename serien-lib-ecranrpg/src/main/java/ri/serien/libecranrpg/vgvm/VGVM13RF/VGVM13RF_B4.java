
package ri.serien.libecranrpg.vgvm.VGVM13RF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVM13RF_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM13RF_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    TIDX1.setValeurs("1", "TIDX1");
    TIDX20.setValeurs("1", "TIDX20");
    TIDX8.setValeurs("1", "TIDX8");
    TIDX19.setValeurs("1", "TIDX19");
    TIDX18.setValeurs("1", "TIDX18");
    TIDX7.setValeurs("1", "TIDX7");
    TIDX13.setValeurs("1", "TIDX13");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@&&G_TITRE@"));
    
    

    p_bpresentation.setCodeEtablissement(WETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    WETB = new XRiTextField();
    WNTO = new XRiTextField();
    OBJ_35 = new JLabel();
    R13SEL = new XRiTextField();
    OBJ_90 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX20 = new XRiRadioButton();
    TIDX19 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX18 = new XRiRadioButton();
    TIDX13 = new XRiRadioButton();
    NFADEB = new XRiTextField();
    NFAFIN = new XRiTextField();
    VDEDEB = new XRiTextField();
    VDEFIN = new XRiTextField();
    CTRDEB = new XRiTextField();
    CTRFIN = new XRiTextField();
    MRGDEB = new XRiTextField();
    MRGFIN = new XRiTextField();
    MEXDEB = new XRiTextField();
    MEXFIN = new XRiTextField();
    MAGDEB = new XRiTextField();
    MAGFIN = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_89 = new JLabel();
    TIDX1 = new XRiRadioButton();
    OBJ_43 = new JLabel();
    DATDEB = new XRiCalendrier();
    OBJ_45 = new JLabel();
    DATFIN = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    buttonGroup2 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("R\u00e8glements pr\u00e9parations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(5, 0, 93, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 0, 40, WETB.getPreferredSize().height);

          //---- WNTO ----
          WNTO.setComponentPopupMenu(BTD);
          WNTO.setName("WNTO");
          p_tete_gauche.add(WNTO);
          WNTO.setBounds(345, 0, 50, WNTO.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Num\u00e9ro de r\u00e8glement");
          OBJ_35.setName("OBJ_35");
          p_tete_gauche.add(OBJ_35);
          OBJ_35.setBounds(210, 0, 135, 28);

          //---- R13SEL ----
          R13SEL.setComponentPopupMenu(BTD);
          R13SEL.setName("R13SEL");
          p_tete_gauche.add(R13SEL);
          R13SEL.setBounds(490, 0, 60, R13SEL.getPreferredSize().height);

          //---- OBJ_90 ----
          OBJ_90.setText("Etats");
          OBJ_90.setName("OBJ_90");
          p_tete_gauche.add(OBJ_90);
          OBJ_90.setBounds(440, 0, 50, 28);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("S\u00e9lection des commandes"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX20 ----
            TIDX20.setText("Code vendeur");
            TIDX20.setName("TIDX20");
            panel1.add(TIDX20);
            TIDX20.setBounds(65, 105, 170, 20);

            //---- TIDX19 ----
            TIDX19.setText("Mode r\u00e8glement");
            TIDX19.setName("TIDX19");
            panel1.add(TIDX19);
            TIDX19.setBounds(65, 173, 170, 20);

            //---- TIDX7 ----
            TIDX7.setText("Mode d'exp\u00e9dition");
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(65, 241, 170, 20);

            //---- TIDX8 ----
            TIDX8.setText("Code transporteur");
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(65, 139, 170, 20);

            //---- TIDX18 ----
            TIDX18.setText("Num\u00e9ro facture");
            TIDX18.setName("TIDX18");
            panel1.add(TIDX18);
            TIDX18.setBounds(65, 207, 170, 20);

            //---- TIDX13 ----
            TIDX13.setText("Magasin");
            TIDX13.setName("TIDX13");
            panel1.add(TIDX13);
            TIDX13.setBounds(65, 275, 170, 20);

            //---- NFADEB ----
            NFADEB.setComponentPopupMenu(BTD);
            NFADEB.setName("NFADEB");
            panel1.add(NFADEB);
            NFADEB.setBounds(265, 200, 66, NFADEB.getPreferredSize().height);

            //---- NFAFIN ----
            NFAFIN.setComponentPopupMenu(BTD);
            NFAFIN.setName("NFAFIN");
            panel1.add(NFAFIN);
            NFAFIN.setBounds(425, 200, 66, NFAFIN.getPreferredSize().height);

            //---- VDEDEB ----
            VDEDEB.setComponentPopupMenu(BTD);
            VDEDEB.setName("VDEDEB");
            panel1.add(VDEDEB);
            VDEDEB.setBounds(265, 95, 40, VDEDEB.getPreferredSize().height);

            //---- VDEFIN ----
            VDEFIN.setComponentPopupMenu(BTD);
            VDEFIN.setName("VDEFIN");
            panel1.add(VDEFIN);
            VDEFIN.setBounds(425, 95, 40, VDEFIN.getPreferredSize().height);

            //---- CTRDEB ----
            CTRDEB.setComponentPopupMenu(BTD);
            CTRDEB.setName("CTRDEB");
            panel1.add(CTRDEB);
            CTRDEB.setBounds(265, 130, 30, CTRDEB.getPreferredSize().height);

            //---- CTRFIN ----
            CTRFIN.setComponentPopupMenu(BTD);
            CTRFIN.setName("CTRFIN");
            panel1.add(CTRFIN);
            CTRFIN.setBounds(425, 130, 30, CTRFIN.getPreferredSize().height);

            //---- MRGDEB ----
            MRGDEB.setComponentPopupMenu(BTD);
            MRGDEB.setName("MRGDEB");
            panel1.add(MRGDEB);
            MRGDEB.setBounds(265, 165, 30, MRGDEB.getPreferredSize().height);

            //---- MRGFIN ----
            MRGFIN.setComponentPopupMenu(BTD);
            MRGFIN.setName("MRGFIN");
            panel1.add(MRGFIN);
            MRGFIN.setBounds(425, 165, 30, MRGFIN.getPreferredSize().height);

            //---- MEXDEB ----
            MEXDEB.setComponentPopupMenu(BTD);
            MEXDEB.setName("MEXDEB");
            panel1.add(MEXDEB);
            MEXDEB.setBounds(265, 235, 30, MEXDEB.getPreferredSize().height);

            //---- MEXFIN ----
            MEXFIN.setComponentPopupMenu(BTD);
            MEXFIN.setName("MEXFIN");
            panel1.add(MEXFIN);
            MEXFIN.setBounds(425, 235, 30, MEXFIN.getPreferredSize().height);

            //---- MAGDEB ----
            MAGDEB.setComponentPopupMenu(BTD);
            MAGDEB.setName("MAGDEB");
            panel1.add(MAGDEB);
            MAGDEB.setBounds(265, 270, 30, MAGDEB.getPreferredSize().height);

            //---- MAGFIN ----
            MAGFIN.setComponentPopupMenu(BTD);
            MAGFIN.setName("MAGFIN");
            panel1.add(MAGFIN);
            MAGFIN.setBounds(425, 270, 30, MAGFIN.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("de");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(240, 205, 18, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("de");
            OBJ_82.setName("OBJ_82");
            panel1.add(OBJ_82);
            OBJ_82.setBounds(240, 135, 18, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("de");
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(240, 240, 18, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("de");
            OBJ_88.setName("OBJ_88");
            panel1.add(OBJ_88);
            OBJ_88.setBounds(240, 275, 18, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("du");
            OBJ_79.setName("OBJ_79");
            panel1.add(OBJ_79);
            OBJ_79.setBounds(240, 170, 18, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("du");
            OBJ_92.setName("OBJ_92");
            panel1.add(OBJ_92);
            OBJ_92.setBounds(240, 100, 18, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("au");
            OBJ_93.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(350, 100, 70, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("\u00e0");
            OBJ_77.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(350, 205, 70, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("\u00e0");
            OBJ_80.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_80.setName("OBJ_80");
            panel1.add(OBJ_80);
            OBJ_80.setBounds(350, 170, 70, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("\u00e0");
            OBJ_83.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(350, 135, 70, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("\u00e0");
            OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(350, 240, 70, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("\u00e0");
            OBJ_89.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_89.setName("OBJ_89");
            panel1.add(OBJ_89);
            OBJ_89.setBounds(350, 275, 70, 20);

            //---- TIDX1 ----
            TIDX1.setText("Dates de facturation");
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(65, 65, 170, 26);

            //---- OBJ_43 ----
            OBJ_43.setText("du");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(240, 65, 18, 20);

            //---- DATDEB ----
            DATDEB.setComponentPopupMenu(BTD);
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(265, 60, 105, DATDEB.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("au");
            OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(350, 65, 70, 20);

            //---- DATFIN ----
            DATFIN.setComponentPopupMenu(BTD);
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(425, 60, 105, DATFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 674, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- buttonGroup2 ----
    buttonGroup2.add(TIDX20);
    buttonGroup2.add(TIDX19);
    buttonGroup2.add(TIDX7);
    buttonGroup2.add(TIDX8);
    buttonGroup2.add(TIDX18);
    buttonGroup2.add(TIDX13);
    buttonGroup2.add(TIDX1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private XRiTextField WETB;
  private XRiTextField WNTO;
  private JLabel OBJ_35;
  private XRiTextField R13SEL;
  private JLabel OBJ_90;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX20;
  private XRiRadioButton TIDX19;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX18;
  private XRiRadioButton TIDX13;
  private XRiTextField NFADEB;
  private XRiTextField NFAFIN;
  private XRiTextField VDEDEB;
  private XRiTextField VDEFIN;
  private XRiTextField CTRDEB;
  private XRiTextField CTRFIN;
  private XRiTextField MRGDEB;
  private XRiTextField MRGFIN;
  private XRiTextField MEXDEB;
  private XRiTextField MEXFIN;
  private XRiTextField MAGDEB;
  private XRiTextField MAGFIN;
  private JLabel OBJ_76;
  private JLabel OBJ_82;
  private JLabel OBJ_85;
  private JLabel OBJ_88;
  private JLabel OBJ_79;
  private JLabel OBJ_92;
  private JLabel OBJ_93;
  private JLabel OBJ_77;
  private JLabel OBJ_80;
  private JLabel OBJ_83;
  private JLabel OBJ_86;
  private JLabel OBJ_89;
  private XRiRadioButton TIDX1;
  private JLabel OBJ_43;
  private XRiCalendrier DATDEB;
  private JLabel OBJ_45;
  private XRiCalendrier DATFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private ButtonGroup buttonGroup2;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
