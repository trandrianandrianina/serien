
package ri.serien.libecranrpg.vgvm.VGVM33FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM33FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _TOP1_Top = { "TOP1", "TOP2", "TOP3", "TOP4", "TOP5", "TOP6", "TOP7", "TOP8", "TOP9", };
  private String[] _TOP1_Title = { "HLD01", };
  private String[][] _TOP1_Data =
      { { "LE01", }, { "LE02", }, { "LE03", }, { "LE04", }, { "LE05", }, { "LE06", }, { "LE07", }, { "LE08", }, { "LE09", }, };
  private int[] _TOP1_Width = { 91, };
  
  public VGVM33FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TOP1.setAspectTable(_TOP1_Top, _TOP1_Title, _TOP1_Data, _TOP1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    P90ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P90ETB@")).trim());
    P90NCA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P90NCA@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENOM@")).trim());
    WEPOS2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS2@")).trim());
    WEPLF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF2@")).trim());
    WEPCO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPCO@")).trim());
    WTOTCG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTOTCG@")).trim());
    WEAFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEAFA@")).trim());
    WEFNEC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEFNEC@")).trim());
    WEFAC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEFAC2@")).trim());
    WEEXP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEEXP2@")).trim());
    WECDE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WECDE2@")).trim());
    WEVAE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEVAE2@")).trim());
    WEBON2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEBON2@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MESBLO@")).trim());
    MTTECH.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTECH@")).trim());
    P90IN5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P90IN5@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
    WESPF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WESPF2@")).trim());
    WEDPL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDPL@")).trim());
    WEDAT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDAT3@")).trim());
    WPAICP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAICP@")).trim());
    WACC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WACC@")).trim());
    WPOR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPOR@")).trim());
    WENC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENC@")).trim());
    WESC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WESC@")).trim());
    WTIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTIMP@")).trim());
    WLIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIMP@")).trim());
    WDIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIMP@")).trim());
    WNIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNIMP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _TOP1_Top);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_119.setVisible(lexique.isPresent("WDIMP"));
    OBJ_117.setVisible(lexique.isPresent("WLIMP"));
    OBJ_116.setVisible(lexique.isPresent("WTIMP"));
    OBJ_115.setVisible(lexique.isPresent("WNIMP"));
    OBJ_51.setVisible(lexique.isPresent("ASS2"));
    OBJ_40.setVisible(!lexique.HostFieldGetData("WEDEP").trim().equalsIgnoreCase(""));
    OBJ_53.setVisible(WEDEP.isVisible());
    OBJ_47.setVisible(lexique.isPresent("WENOM"));
    OBJ_84.setVisible(lexique.isPresent("MTTECH"));
    OBJ_86.setVisible(lexique.isPresent("MESBLO"));
    label5.setVisible(MTTECH.isVisible());
    OBJ_51.setVisible(WEVAE2.isVisible());
    OBJ_62.setVisible(WEPLF2.isVisible());
    OBJ_77.setVisible(WEDAT3.isVisible());
    
    WEBON2.setVisible(!lexique.HostFieldGetData("WEBON2").trim().equals(""));
    label1.setVisible(WEBON2.isVisible());
    
    xTitledPanel3.setVisible(lexique.isTrue("42"));
    
    int valeur_P90IN7 =
        (Integer.parseInt(!lexique.HostFieldGetData("P90IN7").trim().equals("") ? lexique.HostFieldGetData("P90IN7") : "0"));
    
    switch (valeur_P90IN7) {
      case 9:
        label9.setText("Client désactivé");
        break;
      case 6:
        label9.setText("Paiement à la commande");
        break;
      case 5:
        label9.setText("Acompte obligatoire");
        break;
      case 4:
        label9.setText("Attente si dépassement");
        break;
      case 3:
        label9.setText("Livraison interdite si dépassement");
        break;
      case 2:
        label9.setText("Client interdit");
        break;
      case 1:
        label9.setText("Zone en rouge");
        break;
      default:
        label9.setText("");
    }
    WESPF2.setVisible(lexique.isTrue("(20) AND (40)"));
    
    P90IN5.setVisible(!lexique.HostFieldGetData("P90IN5").trim().equals(""));
    OBJ_76.setVisible(P90IN5.isVisible());
    
    // TODO Icones
    l_plus.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_egal.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@P33GRP/+1/@ - @TITRE@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TOP1_Top, "1", "Enter");
    TOP1.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _TOP1_Top, "5", "Enter");
    TOP1.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_84ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void TOP1MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _TOP1_Top, "1", "ENTER", e);
    if (TOP1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(menu1.getInvoker().getName());
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(menu1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FactureActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 28);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_ExpedieActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 38);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_CommandeActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 47);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_CompteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 10);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    P90ETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    P90NCA = new RiZoneSortie();
    OBJ_47 = new RiZoneSortie();
    label9 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_62 = new JLabel();
    OBJ_61 = new JLabel();
    WEPOS2 = new RiZoneSortie();
    WEPLF2 = new RiZoneSortie();
    panel3 = new JPanel();
    AFFA = new JLabel();
    OBJ_55 = new JLabel();
    WEPCO = new RiZoneSortie();
    WTOTCG = new RiZoneSortie();
    WEAFA = new RiZoneSortie();
    WEFNEC = new RiZoneSortie();
    bt_Compte = new SNBoutonLeger();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    panel4 = new JPanel();
    WEFAC2 = new RiZoneSortie();
    WEEXP2 = new RiZoneSortie();
    WECDE2 = new RiZoneSortie();
    bt_Facture = new SNBoutonLeger();
    bt_Expedie = new SNBoutonLeger();
    bt_Commande = new SNBoutonLeger();
    WEVAE2 = new RiZoneSortie();
    OBJ_51 = new JLabel();
    label1 = new JLabel();
    WEBON2 = new RiZoneSortie();
    OBJ_86 = new RiZoneSortie();
    MTTECH = new RiZoneSortie();
    label5 = new JLabel();
    OBJ_84 = new SNBoutonDetail();
    OBJ_76 = new JLabel();
    P90IN5 = new RiZoneSortie();
    panel5 = new JPanel();
    OBJ_53 = new JLabel();
    WEDEP = new RiZoneSortie();
    panel6 = new JPanel();
    WESPF2 = new RiZoneSortie();
    WEDPL = new RiZoneSortie();
    l_plus = new JLabel();
    l_egal = new JLabel();
    WEDAT3 = new RiZoneSortie();
    OBJ_77 = new JLabel();
    OBJ_78 = new JLabel();
    WPAICP = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    SCROLLPANE_LIST = new JScrollPane();
    TOP1 = new XRiTable();
    OBJ_92 = new JLabel();
    WACC = new RiZoneSortie();
    WPOR = new RiZoneSortie();
    WENC = new RiZoneSortie();
    WESC = new RiZoneSortie();
    OBJ_96 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_94 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_115 = new JLabel();
    WTIMP = new RiZoneSortie();
    WLIMP = new RiZoneSortie();
    WDIMP = new RiZoneSortie();
    OBJ_116 = new JLabel();
    OBJ_117 = new JLabel();
    WNIMP = new RiZoneSortie();
    OBJ_119 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    CHOISIR = new JMenuItem();
    menu1 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    menuItem2 = new JMenuItem();
    OBJ_40 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1190, 710));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Encours comptable");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 34));
          p_tete_gauche.setMinimumSize(new Dimension(800, 34));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(5, 4, 90, 20);

          //---- P90ETB ----
          P90ETB.setOpaque(false);
          P90ETB.setText("@P90ETB@");
          P90ETB.setName("P90ETB");
          p_tete_gauche.add(P90ETB);
          P90ETB.setBounds(95, 2, 40, P90ETB.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Client");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(155, 4, 50, 20);

          //---- P90NCA ----
          P90NCA.setOpaque(false);
          P90NCA.setText("@P90NCA@");
          P90NCA.setHorizontalAlignment(SwingConstants.RIGHT);
          P90NCA.setName("P90NCA");
          p_tete_gauche.add(P90NCA);
          P90NCA.setBounds(205, 2, 60, P90NCA.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("@WENOM@");
          OBJ_47.setOpaque(false);
          OBJ_47.setFont(OBJ_47.getFont().deriveFont(OBJ_47.getFont().getStyle() | Font.BOLD));
          OBJ_47.setName("OBJ_47");
          p_tete_gauche.add(OBJ_47);
          OBJ_47.setBounds(280, 2, 223, OBJ_47.getPreferredSize().height);

          //---- label9 ----
          label9.setText("P90IN7");
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setOpaque(false);
          label9.setName("label9");
          p_tete_gauche.add(label9);
          label9.setBounds(525, 2, 220, label9.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Fiche r\u00e9gl. et relances");
              riSousMenu_bt6.setToolTipText("Fiche r\u00e9glements et relances");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Affacturage");
              riSousMenu_bt7.setToolTipText("Affacturage");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Impay\u00e9s");
              riSousMenu_bt8.setToolTipText("Impay\u00e9s");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Compte");
              riSousMenu_bt9.setToolTipText("Compte");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("R\u00e8glements par ch\u00e8que");
              riSousMenu_bt1.setToolTipText("R\u00e8glements par ch\u00e8que");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);

            //======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");

              //---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("R\u00e8glements par carte");
              riSousMenu_bt2.setToolTipText("R\u00e8glements par carte");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);

            //======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");

              //---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("R\u00e9glements par esp\u00e8ce");
              riSousMenu_bt3.setToolTipText("R\u00e9glements par esp\u00e8ce");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(990, 610));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(990, 610));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Analyse de l'encours client");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_62 ----
            OBJ_62.setText("Plafonds");
            OBJ_62.setFont(OBJ_62.getFont().deriveFont(OBJ_62.getFont().getStyle() | Font.BOLD));
            OBJ_62.setName("OBJ_62");
            xTitledPanel1ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(875, 15, 65, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Position");
            OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
            OBJ_61.setName("OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(755, 15, 60, 20);

            //---- WEPOS2 ----
            WEPOS2.setText("@WEPOS2@");
            WEPOS2.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPOS2.setName("WEPOS2");
            xTitledPanel1ContentContainer.add(WEPOS2);
            WEPOS2.setBounds(755, 37, 74, WEPOS2.getPreferredSize().height);

            //---- WEPLF2 ----
            WEPLF2.setText("@WEPLF2@");
            WEPLF2.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPLF2.setName("WEPLF2");
            xTitledPanel1ContentContainer.add(WEPLF2);
            WEPLF2.setBounds(875, 37, 74, WEPLF2.getPreferredSize().height);

            //======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- AFFA ----
              AFFA.setText("Affacturage");
              AFFA.setName("AFFA");
              panel3.add(AFFA);
              AFFA.setBounds(210, 5, 67, 20);

              //---- OBJ_55 ----
              OBJ_55.setText("Effet non \u00e9chu");
              OBJ_55.setName("OBJ_55");
              panel3.add(OBJ_55);
              OBJ_55.setBounds(110, 5, 80, 20);

              //---- WEPCO ----
              WEPCO.setToolTipText("Position comptable");
              WEPCO.setComponentPopupMenu(menu1);
              WEPCO.setText("@WEPCO@");
              WEPCO.setHorizontalAlignment(SwingConstants.RIGHT);
              WEPCO.setName("WEPCO");
              panel3.add(WEPCO);
              WEPCO.setBounds(10, 27, 74, WEPCO.getPreferredSize().height);

              //---- WTOTCG ----
              WTOTCG.setText("@WTOTCG@");
              WTOTCG.setHorizontalAlignment(SwingConstants.RIGHT);
              WTOTCG.setName("WTOTCG");
              panel3.add(WTOTCG);
              WTOTCG.setBounds(307, 27, 74, WTOTCG.getPreferredSize().height);

              //---- WEAFA ----
              WEAFA.setText("@WEAFA@");
              WEAFA.setHorizontalAlignment(SwingConstants.RIGHT);
              WEAFA.setName("WEAFA");
              panel3.add(WEAFA);
              WEAFA.setBounds(208, 27, 74, WEAFA.getPreferredSize().height);

              //---- WEFNEC ----
              WEFNEC.setToolTipText("Effets non \u00e9chus");
              WEFNEC.setText("@WEFNEC@");
              WEFNEC.setHorizontalAlignment(SwingConstants.RIGHT);
              WEFNEC.setName("WEFNEC");
              panel3.add(WEFNEC);
              WEFNEC.setBounds(109, 27, 74, WEFNEC.getPreferredSize().height);

              //---- bt_Compte ----
              bt_Compte.setText("Compte");
              bt_Compte.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              bt_Compte.setName("bt_Compte");
              bt_Compte.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_CompteActionPerformed(e);
                }
              });
              panel3.add(bt_Compte);
              bt_Compte.setBounds(10, 5, 80, 20);

              //---- label6 ----
              label6.setText("+");
              label6.setHorizontalAlignment(SwingConstants.CENTER);
              label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD, label6.getFont().getSize() + 1f));
              label6.setName("label6");
              panel3.add(label6);
              label6.setBounds(89, 30, 15, 18);

              //---- label7 ----
              label7.setText("+");
              label7.setHorizontalAlignment(SwingConstants.CENTER);
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD, label7.getFont().getSize() + 1f));
              label7.setName("label7");
              panel3.add(label7);
              label7.setBounds(188, 30, 15, 18);

              //---- label8 ----
              label8.setText("=");
              label8.setHorizontalAlignment(SwingConstants.CENTER);
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD, label8.getFont().getSize() + 1f));
              label8.setName("label8");
              panel3.add(label8);
              label8.setBounds(287, 30, 15, 18);
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(15, 10, 390, 65);

            //======== panel4 ========
            {
              panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- WEFAC2 ----
              WEFAC2.setComponentPopupMenu(menu1);
              WEFAC2.setText("@WEFAC2@");
              WEFAC2.setName("WEFAC2");
              panel4.add(WEFAC2);
              WEFAC2.setBounds(10, 27, 74, WEFAC2.getPreferredSize().height);

              //---- WEEXP2 ----
              WEEXP2.setComponentPopupMenu(menu1);
              WEEXP2.setText("@WEEXP2@");
              WEEXP2.setName("WEEXP2");
              panel4.add(WEEXP2);
              WEEXP2.setBounds(110, 27, 78, WEEXP2.getPreferredSize().height);

              //---- WECDE2 ----
              WECDE2.setComponentPopupMenu(menu1);
              WECDE2.setText("@WECDE2@");
              WECDE2.setName("WECDE2");
              panel4.add(WECDE2);
              WECDE2.setBounds(210, 27, 74, WECDE2.getPreferredSize().height);

              //---- bt_Facture ----
              bt_Facture.setText("Factur\u00e9");
              bt_Facture.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              bt_Facture.setName("bt_Facture");
              bt_Facture.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_FactureActionPerformed(e);
                }
              });
              panel4.add(bt_Facture);
              bt_Facture.setBounds(5, 5, 100, 20);

              //---- bt_Expedie ----
              bt_Expedie.setText("Exp\u00e9di\u00e9");
              bt_Expedie.setName("bt_Expedie");
              bt_Expedie.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_ExpedieActionPerformed(e);
                }
              });
              panel4.add(bt_Expedie);
              bt_Expedie.setBounds(100, 5, 100, 20);

              //---- bt_Commande ----
              bt_Commande.setText("Command\u00e9");
              bt_Commande.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              bt_Commande.setName("bt_Commande");
              bt_Commande.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_CommandeActionPerformed(e);
                }
              });
              panel4.add(bt_Commande);
              bt_Commande.setBounds(195, 5, 100, 20);

              //---- WEVAE2 ----
              WEVAE2.setText("@WEVAE2@");
              WEVAE2.setName("WEVAE2");
              panel4.add(WEVAE2);
              WEVAE2.setBounds(210, 57, 74, WEVAE2.getPreferredSize().height);

              //---- OBJ_51 ----
              OBJ_51.setText("Ventes assimil\u00e9es export");
              OBJ_51.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_51.setName("OBJ_51");
              panel4.add(OBJ_51);
              OBJ_51.setBounds(15, 60, 169, 18);

              //---- label1 ----
              label1.setText("Bon en cours");
              label1.setHorizontalAlignment(SwingConstants.RIGHT);
              label1.setName("label1");
              panel4.add(label1);
              label1.setBounds(29, 92, 155, 21);

              //---- WEBON2 ----
              WEBON2.setText("@WEBON2@");
              WEBON2.setName("WEBON2");
              panel4.add(WEBON2);
              WEBON2.setBounds(210, 90, 74, 24);

              //---- OBJ_86 ----
              OBJ_86.setText("@MESBLO@");
              OBJ_86.setForeground(new Color(153, 0, 51));
              OBJ_86.setName("OBJ_86");
              panel4.add(OBJ_86);
              OBJ_86.setBounds(10, 120, 275, 20);
            }
            xTitledPanel1ContentContainer.add(panel4);
            panel4.setBounds(430, 10, 300, 150);

            //---- MTTECH ----
            MTTECH.setText("@MTTECH@");
            MTTECH.setName("MTTECH");
            xTitledPanel1ContentContainer.add(MTTECH);
            MTTECH.setBounds(225, 130, 92, MTTECH.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Ech\u00e9ances d\u00e9pass\u00e9es");
            label5.setName("label5");
            xTitledPanel1ContentContainer.add(label5);
            label5.setBounds(30, 132, 160, 20);

            //---- OBJ_84 ----
            OBJ_84.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_84.setName("OBJ_84");
            OBJ_84.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_84ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(315, 127, 30, 30);

            //---- OBJ_76 ----
            OBJ_76.setText("Niveau de relance maxi");
            OBJ_76.setName("OBJ_76");
            xTitledPanel1ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(30, 102, 160, 20);

            //---- P90IN5 ----
            P90IN5.setText("@P90IN5@");
            P90IN5.setName("P90IN5");
            xTitledPanel1ContentContainer.add(P90IN5);
            P90IN5.setBounds(225, 100, 20, P90IN5.getPreferredSize().height);

            //======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- OBJ_53 ----
              OBJ_53.setText("D\u00e9passement");
              OBJ_53.setFont(OBJ_53.getFont().deriveFont(OBJ_53.getFont().getStyle() | Font.BOLD));
              OBJ_53.setName("OBJ_53");
              panel5.add(OBJ_53);
              OBJ_53.setBounds(5, 8, 105, 18);

              //---- WEDEP ----
              WEDEP.setForeground(Color.red);
              WEDEP.setText("@WEDEP@");
              WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
              WEDEP.setName("WEDEP");
              panel5.add(WEDEP);
              WEDEP.setBounds(125, 5, 74, WEDEP.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(panel5);
            panel5.setBounds(750, 90, 205, 35);

            //======== panel6 ========
            {
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- WESPF2 ----
              WESPF2.setText("@WESPF2@");
              WESPF2.setHorizontalAlignment(SwingConstants.RIGHT);
              WESPF2.setName("WESPF2");
              panel6.add(WESPF2);
              WESPF2.setBounds(125, 0, 74, 28);

              //---- WEDPL ----
              WEDPL.setText("@WEDPL@");
              WEDPL.setHorizontalAlignment(SwingConstants.RIGHT);
              WEDPL.setName("WEDPL");
              panel6.add(WEDPL);
              WEDPL.setBounds(5, 2, 74, WEDPL.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(panel6);
            panel6.setBounds(750, 65, 205, 30);

            //---- l_plus ----
            l_plus.setName("l_plus");
            xTitledPanel1ContentContainer.add(l_plus);
            l_plus.setBounds(410, 50, 16, 16);

            //---- l_egal ----
            l_egal.setName("l_egal");
            xTitledPanel1ContentContainer.add(l_egal);
            l_egal.setBounds(732, 50, 20, 16);

            //---- WEDAT3 ----
            WEDAT3.setText("@WEDAT3@");
            WEDAT3.setHorizontalAlignment(SwingConstants.RIGHT);
            WEDAT3.setName("WEDAT3");
            xTitledPanel1ContentContainer.add(WEDAT3);
            WEDAT3.setBounds(875, 120, 74, WEDAT3.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("Plafond maxi d\u00e9blocage");
            OBJ_77.setName("OBJ_77");
            xTitledPanel1ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(740, 122, 135, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("R\u00e8glements non comptabilis\u00e9s");
            OBJ_78.setName("OBJ_78");
            xTitledPanel1ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(30, 80, 180, 20);

            //---- WPAICP ----
            WPAICP.setText("@WPAICP@");
            WPAICP.setName("WPAICP");
            xTitledPanel1ContentContainer.add(WPAICP);
            WPAICP.setBounds(225, 75, 92, WPAICP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Effet non \u00e9chu");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- TOP1 ----
              TOP1.setComponentPopupMenu(BTD);
              TOP1.setName("TOP1");
              TOP1.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  TOP1MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(TOP1);
            }
            xTitledPanel2ContentContainer.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(45, 51, 840, 173);

            //---- OBJ_92 ----
            OBJ_92.setText("A l'acceptation");
            OBJ_92.setName("OBJ_92");
            xTitledPanel2ContentContainer.add(OBJ_92);
            OBJ_92.setBounds(45, 23, 90, 20);

            //---- WACC ----
            WACC.setText("@WACC@");
            WACC.setHorizontalAlignment(SwingConstants.RIGHT);
            WACC.setName("WACC");
            xTitledPanel2ContentContainer.add(WACC);
            WACC.setBounds(135, 21, 90, WACC.getPreferredSize().height);

            //---- WPOR ----
            WPOR.setText("@WPOR@");
            WPOR.setHorizontalAlignment(SwingConstants.RIGHT);
            WPOR.setName("WPOR");
            xTitledPanel2ContentContainer.add(WPOR);
            WPOR.setBounds(335, 21, 90, WPOR.getPreferredSize().height);

            //---- WENC ----
            WENC.setText("@WENC@");
            WENC.setHorizontalAlignment(SwingConstants.RIGHT);
            WENC.setName("WENC");
            xTitledPanel2ContentContainer.add(WENC);
            WENC.setBounds(580, 21, 90, WENC.getPreferredSize().height);

            //---- WESC ----
            WESC.setText("@WESC@");
            WESC.setHorizontalAlignment(SwingConstants.RIGHT);
            WESC.setName("WESC");
            xTitledPanel2ContentContainer.add(WESC);
            WESC.setBounds(795, 21, 90, WESC.getPreferredSize().height);

            //---- OBJ_96 ----
            OBJ_96.setText("Remise encaissement");
            OBJ_96.setName("OBJ_96");
            xTitledPanel2ContentContainer.add(OBJ_96);
            OBJ_96.setBounds(445, 23, 135, 20);

            //---- OBJ_98 ----
            OBJ_98.setText("Remise escompte");
            OBJ_98.setName("OBJ_98");
            xTitledPanel2ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(685, 23, 110, 20);

            //---- OBJ_94 ----
            OBJ_94.setText("En portefeuille");
            OBJ_94.setName("OBJ_94");
            xTitledPanel2ContentContainer.add(OBJ_94);
            OBJ_94.setBounds(245, 23, 90, 20);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            xTitledPanel2ContentContainer.add(BT_PGUP);
            BT_PGUP.setBounds(890, 51, 25, 80);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            xTitledPanel2ContentContainer.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(890, 146, 25, 80);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Impay\u00e9s");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();

            //---- OBJ_115 ----
            OBJ_115.setText("Nombre");
            OBJ_115.setName("OBJ_115");

            //---- WTIMP ----
            WTIMP.setText("@WTIMP@");
            WTIMP.setHorizontalAlignment(SwingConstants.RIGHT);
            WTIMP.setName("WTIMP");

            //---- WLIMP ----
            WLIMP.setText("@WLIMP@");
            WLIMP.setHorizontalAlignment(SwingConstants.RIGHT);
            WLIMP.setName("WLIMP");

            //---- WDIMP ----
            WDIMP.setText("@WDIMP@");
            WDIMP.setHorizontalAlignment(SwingConstants.RIGHT);
            WDIMP.setName("WDIMP");

            //---- OBJ_116 ----
            OBJ_116.setText("Total");
            OBJ_116.setFont(OBJ_116.getFont().deriveFont(OBJ_116.getFont().getStyle() | Font.BOLD));
            OBJ_116.setName("OBJ_116");

            //---- OBJ_117 ----
            OBJ_117.setText("Dont");
            OBJ_117.setName("OBJ_117");

            //---- WNIMP ----
            WNIMP.setText("@WNIMP@");
            WNIMP.setHorizontalAlignment(SwingConstants.RIGHT);
            WNIMP.setName("WNIMP");

            //---- OBJ_119 ----
            OBJ_119.setText("Du");
            OBJ_119.setName("OBJ_119");

            GroupLayout xTitledPanel3ContentContainerLayout = new GroupLayout(xTitledPanel3ContentContainer);
            xTitledPanel3ContentContainer.setLayout(xTitledPanel3ContentContainerLayout);
            xTitledPanel3ContentContainerLayout.setHorizontalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(50, 50, 50)
                  .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                  .addGap(28, 28, 28)
                  .addComponent(WNIMP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(45, 45, 45)
                  .addComponent(OBJ_116, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                  .addGap(11, 11, 11)
                  .addComponent(WTIMP, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(44, 44, 44)
                  .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 31, GroupLayout.PREFERRED_SIZE)
                  .addGap(9, 9, 9)
                  .addComponent(WLIMP, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                  .addGap(54, 54, 54)
                  .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                  .addGap(14, 14, 14)
                  .addComponent(WDIMP, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
            );
            xTitledPanel3ContentContainerLayout.setVerticalGroup(
              xTitledPanel3ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(16, 16, 16)
                  .addComponent(WNIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_116, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(16, 16, 16)
                  .addComponent(WTIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(16, 16, 16)
                  .addComponent(WLIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel3ContentContainerLayout.createSequentialGroup()
                  .addGap(16, 16, 16)
                  .addComponent(WDIMP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                  .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Affichage \u00e0 partir de");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- CHOISIR ----
      CHOISIR.setText("Interrogation");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }

    //======== menu1 ========
    {
      menu1.setName("menu1");

      //---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      menu1.add(menuItem1);

      //---- menuItem2 ----
      menuItem2.setText("Aide en ligne");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      menu1.add(menuItem2);
    }

    //---- OBJ_40 ----
    OBJ_40.setIcon(new ImageIcon("images/avert.gif"));
    OBJ_40.setName("OBJ_40");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie P90ETB;
  private JLabel OBJ_44;
  private RiZoneSortie P90NCA;
  private RiZoneSortie OBJ_47;
  private RiZoneSortie label9;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_62;
  private JLabel OBJ_61;
  private RiZoneSortie WEPOS2;
  private RiZoneSortie WEPLF2;
  private JPanel panel3;
  private JLabel AFFA;
  private JLabel OBJ_55;
  private RiZoneSortie WEPCO;
  private RiZoneSortie WTOTCG;
  private RiZoneSortie WEAFA;
  private RiZoneSortie WEFNEC;
  private SNBoutonLeger bt_Compte;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JPanel panel4;
  private RiZoneSortie WEFAC2;
  private RiZoneSortie WEEXP2;
  private RiZoneSortie WECDE2;
  private SNBoutonLeger bt_Facture;
  private SNBoutonLeger bt_Expedie;
  private SNBoutonLeger bt_Commande;
  private RiZoneSortie WEVAE2;
  private JLabel OBJ_51;
  private JLabel label1;
  private RiZoneSortie WEBON2;
  private RiZoneSortie OBJ_86;
  private RiZoneSortie MTTECH;
  private JLabel label5;
  private SNBoutonDetail OBJ_84;
  private JLabel OBJ_76;
  private RiZoneSortie P90IN5;
  private JPanel panel5;
  private JLabel OBJ_53;
  private RiZoneSortie WEDEP;
  private JPanel panel6;
  private RiZoneSortie WESPF2;
  private RiZoneSortie WEDPL;
  private JLabel l_plus;
  private JLabel l_egal;
  private RiZoneSortie WEDAT3;
  private JLabel OBJ_77;
  private JLabel OBJ_78;
  private RiZoneSortie WPAICP;
  private JXTitledPanel xTitledPanel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable TOP1;
  private JLabel OBJ_92;
  private RiZoneSortie WACC;
  private RiZoneSortie WPOR;
  private RiZoneSortie WENC;
  private RiZoneSortie WESC;
  private JLabel OBJ_96;
  private JLabel OBJ_98;
  private JLabel OBJ_94;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_115;
  private RiZoneSortie WTIMP;
  private RiZoneSortie WLIMP;
  private RiZoneSortie WDIMP;
  private JLabel OBJ_116;
  private JLabel OBJ_117;
  private RiZoneSortie WNIMP;
  private JLabel OBJ_119;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem CHOISIR;
  private JPopupMenu menu1;
  private JMenuItem menuItem1;
  private JMenuItem menuItem2;
  private JLabel OBJ_40;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
