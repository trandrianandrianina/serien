
package ri.serien.libecranrpg.vgvm.VGVM111F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM111F_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM111F_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    P13IN5.setValeursSelection("1", " ");
    P13TNC.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLB1@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BASE@")).trim());
    P13IN5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB1@ @LIB2@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTTC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    P13REP.setEnabled(!lexique.HostFieldGetData("P13TNC").trim().equalsIgnoreCase("1") & lexique.isPresent("P13REP"));
    P13COM.setEnabled(!lexique.HostFieldGetData("P13TNC").trim().equalsIgnoreCase("1") & lexique.isPresent("P13COM"));
    P13BAS.setEnabled(lexique.isPresent("P13BAS"));
    P13IN5.setEnabled(!lexique.HostFieldGetData("P13TNC").trim().equalsIgnoreCase("1"));
    // P13IN5.setSelected(lexique.HostFieldGetData("P13IN5").equalsIgnoreCase("1"));
    // P13TNC.setVisible( lexique.isPresent("P13TNC"));
    // P13TNC.setEnabled( lexique.isPresent("P13TNC"));
    // P13TNC.setSelected(lexique.HostFieldGetData("P13TNC").equalsIgnoreCase("1"));
    OBJ_26.setVisible(lexique.isPresent("RPLB1"));
    OBJ_29.setVisible(!lexique.isTrue("60"));
    OBJ_30.setVisible(lexique.isTrue("60"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion des bons"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    P13TNC = new XRiCheckBox();
    panel2 = new JPanel();
    OBJ_24 = new JLabel();
    P13REP = new XRiTextField();
    OBJ_26 = new RiZoneSortie();
    OBJ_29 = new JLabel();
    P13BAS = new XRiTextField();
    OBJ_25 = new JLabel();
    P13COM = new XRiTextField();
    P13IN5 = new XRiCheckBox();
    OBJ_30 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(865, 185));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- P13TNC ----
          P13TNC.setText("Ligne non commissionn\u00e9e");
          P13TNC.setComponentPopupMenu(BTD);
          P13TNC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P13TNC.setName("P13TNC");
          panel1.add(P13TNC);
          P13TNC.setBounds(25, 10, 181, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_24 ----
          OBJ_24.setText("Repr\u00e9sentant");
          OBJ_24.setName("OBJ_24");
          panel2.add(OBJ_24);
          OBJ_24.setBounds(25, 15, 84, 20);

          //---- P13REP ----
          P13REP.setComponentPopupMenu(BTD);
          P13REP.setName("P13REP");
          panel2.add(P13REP);
          P13REP.setBounds(25, 40, 34, P13REP.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("@RPLB1@");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(62, 42, 234, OBJ_26.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("@BASE@");
          OBJ_29.setName("OBJ_29");
          panel2.add(OBJ_29);
          OBJ_29.setBounds(300, 15, 81, 20);

          //---- P13BAS ----
          P13BAS.setComponentPopupMenu(BTD);
          P13BAS.setName("P13BAS");
          panel2.add(P13BAS);
          P13BAS.setBounds(300, 40, 82, P13BAS.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("Commission");
          OBJ_25.setName("OBJ_25");
          panel2.add(OBJ_25);
          OBJ_25.setBounds(385, 15, 78, 20);

          //---- P13COM ----
          P13COM.setComponentPopupMenu(BTD);
          P13COM.setName("P13COM");
          panel2.add(P13COM);
          P13COM.setBounds(385, 40, 50, P13COM.getPreferredSize().height);

          //---- P13IN5 ----
          P13IN5.setText("@LIB1@ @LIB2@");
          P13IN5.setComponentPopupMenu(BTD);
          P13IN5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          P13IN5.setName("P13IN5");
          panel2.add(P13IN5);
          P13IN5.setBounds(445, 44, 175, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("@MTTC@");
          OBJ_30.setName("OBJ_30");
          panel2.add(OBJ_30);
          OBJ_30.setBounds(300, 15, 70, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox P13TNC;
  private JPanel panel2;
  private JLabel OBJ_24;
  private XRiTextField P13REP;
  private RiZoneSortie OBJ_26;
  private JLabel OBJ_29;
  private XRiTextField P13BAS;
  private JLabel OBJ_25;
  private XRiTextField P13COM;
  private XRiCheckBox P13IN5;
  private JLabel OBJ_30;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
