
package ri.serien.libecranrpg.vgvm.VGVM114F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM114F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM114F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WCAF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCAF@")).trim());
    WPRV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRV@")).trim());
    WMAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAR@")).trim());
    WMARP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMARP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    L1ED5.setEnabled(lexique.isPresent("L1ED5"));
    L1ED4.setEnabled(lexique.isPresent("L1ED4"));
    L1ED3.setEnabled(lexique.isPresent("L1ED3"));
    L1ED2.setEnabled(lexique.isPresent("L1ED2"));
    L1ED1.setEnabled(lexique.isPresent("L1ED1"));
    L1IN2.setEnabled(lexique.isPresent("L1IN2"));
    L1IN3.setEnabled(lexique.isPresent("L1IN3"));
    L1NLI.setVisible(lexique.isPresent("L1NLI"));
    L1LIB4.setEnabled(lexique.isPresent("L1LIB4"));
    L1LIB3.setEnabled(lexique.isPresent("L1LIB3"));
    L1LIB2.setEnabled(lexique.isPresent("L1LIB2"));
    L1LIB1.setEnabled(lexique.isPresent("L1LIB1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm114"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    L1LIB3 = new XRiTextField();
    L1LIB4 = new XRiTextField();
    OBJ_47 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_18 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    WCAF = new RiZoneSortie();
    WPRV = new RiZoneSortie();
    WMAR = new RiZoneSortie();
    OBJ_44 = new JLabel();
    WMARP = new RiZoneSortie();
    L1NLI = new XRiTextField();
    L1IN3 = new XRiTextField();
    L1IN2 = new XRiTextField();
    L1ED1 = new XRiTextField();
    L1ED2 = new XRiTextField();
    L1ED3 = new XRiTextField();
    L1ED4 = new XRiTextField();
    L1ED5 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(805, 245));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          p_recup.add(L1LIB1);
          L1LIB1.setBounds(20, 45, 310, L1LIB1.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setName("L1LIB2");
          p_recup.add(L1LIB2);
          L1LIB2.setBounds(20, 75, 310, L1LIB2.getPreferredSize().height);

          //---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setName("L1LIB3");
          p_recup.add(L1LIB3);
          L1LIB3.setBounds(20, 105, 310, L1LIB3.getPreferredSize().height);

          //---- L1LIB4 ----
          L1LIB4.setComponentPopupMenu(BTD);
          L1LIB4.setName("L1LIB4");
          p_recup.add(L1LIB4);
          L1LIB4.setBounds(20, 135, 310, L1LIB4.getPreferredSize().height);

          //---- OBJ_47 ----
          OBJ_47.setText("Ligne commentaire num\u00e9ro");
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(100, 19, 185, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Prix de revient");
          OBJ_42.setName("OBJ_42");
          p_recup.add(OBJ_42);
          OBJ_42.setBounds(360, 109, 93, 20);

          //---- OBJ_43 ----
          OBJ_43.setText("Regroupement");
          OBJ_43.setName("OBJ_43");
          p_recup.add(OBJ_43);
          OBJ_43.setBounds(20, 169, 91, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Edition sur");
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(360, 19, 84, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Chiffre d'affaire r\u00e9el");
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(360, 79, 125, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("Marge r\u00e9elle");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(360, 139, 79, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Marge en %");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(360, 169, 75, 20);

          //---- WCAF ----
          WCAF.setComponentPopupMenu(BTD);
          WCAF.setText("@WCAF@");
          WCAF.setHorizontalAlignment(SwingConstants.RIGHT);
          WCAF.setName("WCAF");
          p_recup.add(WCAF);
          WCAF.setBounds(485, 77, 90, WCAF.getPreferredSize().height);

          //---- WPRV ----
          WPRV.setText("@WPRV@");
          WPRV.setHorizontalAlignment(SwingConstants.RIGHT);
          WPRV.setName("WPRV");
          p_recup.add(WPRV);
          WPRV.setBounds(485, 107, 90, WPRV.getPreferredSize().height);

          //---- WMAR ----
          WMAR.setComponentPopupMenu(BTD);
          WMAR.setText("@WMAR@");
          WMAR.setHorizontalAlignment(SwingConstants.RIGHT);
          WMAR.setName("WMAR");
          p_recup.add(WMAR);
          WMAR.setBounds(485, 137, 90, WMAR.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Totalisation");
          OBJ_44.setName("OBJ_44");
          p_recup.add(OBJ_44);
          OBJ_44.setBounds(230, 169, 70, 20);

          //---- WMARP ----
          WMARP.setComponentPopupMenu(BTD);
          WMARP.setText("@WMARP@");
          WMARP.setHorizontalAlignment(SwingConstants.RIGHT);
          WMARP.setName("WMARP");
          p_recup.add(WMARP);
          WMARP.setBounds(485, 167, 58, WMARP.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setComponentPopupMenu(BTD);
          L1NLI.setName("L1NLI");
          p_recup.add(L1NLI);
          L1NLI.setBounds(286, 15, 44, L1NLI.getPreferredSize().height);

          //---- L1IN3 ----
          L1IN3.setComponentPopupMenu(BTD);
          L1IN3.setName("L1IN3");
          p_recup.add(L1IN3);
          L1IN3.setBounds(132, 165, 20, L1IN3.getPreferredSize().height);

          //---- L1IN2 ----
          L1IN2.setComponentPopupMenu(BTD);
          L1IN2.setName("L1IN2");
          p_recup.add(L1IN2);
          L1IN2.setBounds(310, 165, 20, L1IN2.getPreferredSize().height);

          //---- L1ED1 ----
          L1ED1.setComponentPopupMenu(BTD);
          L1ED1.setName("L1ED1");
          p_recup.add(L1ED1);
          L1ED1.setBounds(375, 45, 20, L1ED1.getPreferredSize().height);

          //---- L1ED2 ----
          L1ED2.setComponentPopupMenu(BTD);
          L1ED2.setName("L1ED2");
          p_recup.add(L1ED2);
          L1ED2.setBounds(436, 45, 20, L1ED2.getPreferredSize().height);

          //---- L1ED3 ----
          L1ED3.setComponentPopupMenu(BTD);
          L1ED3.setName("L1ED3");
          p_recup.add(L1ED3);
          L1ED3.setBounds(485, 45, 20, L1ED3.getPreferredSize().height);

          //---- L1ED4 ----
          L1ED4.setComponentPopupMenu(BTD);
          L1ED4.setName("L1ED4");
          p_recup.add(L1ED4);
          L1ED4.setBounds(531, 45, 20, L1ED4.getPreferredSize().height);

          //---- L1ED5 ----
          L1ED5.setComponentPopupMenu(BTD);
          L1ED5.setName("L1ED5");
          p_recup.add(L1ED5);
          L1ED5.setBounds(577, 45, 20, L1ED5.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("P");
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(360, 49, 12, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("A");
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(419, 49, 12, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("E");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(469, 49, 12, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("T");
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(515, 49, 12, 20);

          //---- OBJ_40 ----
          OBJ_40.setText("F");
          OBJ_40.setName("OBJ_40");
          p_recup.add(OBJ_40);
          OBJ_40.setBounds(561, 49, 12, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 217, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private XRiTextField L1LIB3;
  private XRiTextField L1LIB4;
  private JLabel OBJ_47;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private JLabel OBJ_18;
  private JLabel OBJ_41;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private RiZoneSortie WCAF;
  private RiZoneSortie WPRV;
  private RiZoneSortie WMAR;
  private JLabel OBJ_44;
  private RiZoneSortie WMARP;
  private XRiTextField L1NLI;
  private XRiTextField L1IN3;
  private XRiTextField L1IN2;
  private XRiTextField L1ED1;
  private XRiTextField L1ED2;
  private XRiTextField L1ED3;
  private XRiTextField L1ED4;
  private XRiTextField L1ED5;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
