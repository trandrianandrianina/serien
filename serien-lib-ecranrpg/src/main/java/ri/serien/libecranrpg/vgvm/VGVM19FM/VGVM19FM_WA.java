
package ri.serien.libecranrpg.vgvm.VGVM19FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVM19FM_WA extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _TALB1_Title = { "", "", "", "", "", "", "", "", };
  private String[][] _TALB1_Data = { { "1", "TALB1", "W1K11", "W1P11", "2", "TALB2", "W1K21", "W1P21", },
      { "3", "TALB3", "W1K31", "W1P31", "4", "TALB4", "W1K41", "W1P41", },
      { "5", "TALB5", "W1K51", "W1P51", "6", "TALB6", "W1K61", "W1P61", },
      { "7", "TALB7", "W1K71", "W1P71", "8", "TALB8", "W1K81", "W1P81", },
      { "9", "TALB9", "W1K91", "W1P91", "10", "TALB10", "W1K01", "W1P01", }, };
  private int[] _TALB1_Width = { 15, 75, 48, 76, 22, 78, 51, 79, };
  
  public VGVM19FM_WA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TALB1.setAspectTable(null, _TALB1_Title, _TALB1_Data, _TALB1_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TARI@")).trim());
    OBJ_14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TARI2@")).trim());
    ATDAPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDAPX@")).trim());
    WPUMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPUMP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), null);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    WPUMP.setVisible(lexique.isPresent("WPUMP"));
    ATDAPX.setVisible(lexique.isPresent("ATDAPX"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@ULBTAR@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    TALB1 = new XRiTable();
    OBJ_15 = new JLabel();
    OBJ_14 = new JLabel();
    P_PnlOpts = new JPanel();
    ATDAPX = new RiZoneSortie();
    WPUMP = new RiZoneSortie();
    OBJ_17 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(775, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- TALB1 ----
            TALB1.setName("TALB1");
            SCROLLPANE_LIST.setViewportView(TALB1);
          }
          p_recup.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(25, 60, 503, 92);

          //---- OBJ_15 ----
          OBJ_15.setText("@TARI@");
          OBJ_15.setName("OBJ_15");
          p_recup.add(OBJ_15);
          OBJ_15.setBounds(26, 27, 124, 20);

          //---- OBJ_14 ----
          OBJ_14.setText("@TARI2@");
          OBJ_14.setName("OBJ_14");
          p_recup.add(OBJ_14);
          OBJ_14.setBounds(26, 27, 103, 20);

          //======== P_PnlOpts ========
          {
            P_PnlOpts.setName("P_PnlOpts");
            P_PnlOpts.setLayout(null);
          }
          p_recup.add(P_PnlOpts);
          P_PnlOpts.setBounds(1033, 15, 55, 516);

          //---- ATDAPX ----
          ATDAPX.setText("@ATDAPX@");
          ATDAPX.setName("ATDAPX");
          p_recup.add(ATDAPX);
          ATDAPX.setBounds(155, 25, 72, ATDAPX.getPreferredSize().height);

          //---- WPUMP ----
          WPUMP.setText("@WPUMP@");
          WPUMP.setName("WPUMP");
          p_recup.add(WPUMP);
          WPUMP.setBounds(450, 25, 76, WPUMP.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("P.U.MP");
          OBJ_17.setName("OBJ_17");
          p_recup.add(OBJ_17);
          OBJ_17.setBounds(385, 27, 59, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 555, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable TALB1;
  private JLabel OBJ_15;
  private JLabel OBJ_14;
  private JPanel P_PnlOpts;
  private RiZoneSortie ATDAPX;
  private RiZoneSortie WPUMP;
  private JLabel OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
