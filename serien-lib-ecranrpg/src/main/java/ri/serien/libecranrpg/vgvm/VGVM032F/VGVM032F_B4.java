
package ri.serien.libecranrpg.vgvm.VGVM032F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM032F_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM032F_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLIB@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTNS@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDLIV@")).trim());
    OBJ_140.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAILER@")).trim());
    CLDAT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLDAT2@")).trim());
    OBJ_131.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIBR@")).trim());
    OBJ_142.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MAILER@")).trim());
    OBJ_124.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    OBJ_125.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    OBJ_126.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    OBJ_127.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    OBJ_129.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_140.setVisible(lexique.isPresent("MAILER"));
    CLTFA.setEnabled(lexique.isPresent("CLTFA"));
    CLCTR.setEnabled(lexique.isPresent("CLCTR"));
    WCTRF.setEnabled(lexique.isPresent("WCTRF"));
    CLMEX.setEnabled(lexique.isPresent("CLMEX"));
    OBJ_129.setVisible(lexique.isPresent("WTI1"));
    OBJ_127.setVisible(lexique.isPresent("WTI5"));
    OBJ_126.setVisible(lexique.isPresent("WTI4"));
    OBJ_125.setVisible(lexique.isPresent("WTI3"));
    OBJ_124.setVisible(lexique.isPresent("WTI2"));
    CLTOP5.setEnabled(lexique.isPresent("CLTOP5"));
    CLTOP4.setEnabled(lexique.isPresent("CLTOP4"));
    CLTOP3.setEnabled(lexique.isPresent("CLTOP3"));
    CLTOP2.setEnabled(lexique.isPresent("CLTOP2"));
    CLTOP1.setEnabled(lexique.isPresent("CLTOP1"));
    WLIVL.setEnabled(lexique.isPresent("WLIVL"));
    CLCAT.setEnabled(lexique.isPresent("CLCAT"));
    WCIVF.setVisible(lexique.isPresent("WCIVF"));
    WCIV.setVisible(lexique.isPresent("WCIV"));
    RECAT.setEnabled(lexique.isPresent("RECAT"));
    OBJ_105.setVisible(lexique.isPresent("INDLIV"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    WCLIL.setEnabled(lexique.isPresent("WCLIL"));
    REPOS.setEnabled(lexique.isPresent("REPOS"));
    CLCOP.setEnabled(lexique.isPresent("CLCOP"));
    WCOPF.setEnabled(lexique.isPresent("WCOPF"));
    WCDPF.setEnabled(lexique.isPresent("WCDPF"));
    CLCDP.setEnabled(lexique.isPresent("CLCDP"));
    INDCLI.setEnabled(lexique.isPresent("INDCLI"));
    WDFAC.setEnabled(lexique.isPresent("WDFAC"));
    TFLIBR.setEnabled(lexique.isPresent("TFLIBR"));
    GCDPRL.setEnabled(lexique.isPresent("GCDPRL"));
    OBJ_47.setVisible(lexique.isPresent("LIBTNS"));
    CLOBS.setEnabled(lexique.isPresent("CLOBS"));
    CLFAX.setEnabled(lexique.isPresent("CLFAX"));
    RETEL2.setEnabled(lexique.isPresent("RETEL2"));
    CLTEL.setEnabled(lexique.isPresent("CLTEL"));
    CLCLK.setEnabled(lexique.isPresent("CLCLK"));
    WCLAF.setEnabled(lexique.isPresent("WCLAF"));
    OBJ_131.setVisible(lexique.isPresent("EXLIBR"));
    CLPAY.setEnabled(lexique.isPresent("CLPAY"));
    WPAYF.setEnabled(lexique.isPresent("WPAYF"));
    CLVILR.setEnabled(lexique.isPresent("CLVILR"));
    WVILF.setEnabled(lexique.isPresent("WVILF"));
    OBJ_38.setVisible(lexique.isPresent("CCLIB"));
    WNOMR.setVisible(lexique.isPresent("WNOMR"));
    WNOMFR.setVisible(lexique.isPresent("WNOMFR"));
    CLLOC.setEnabled(lexique.isPresent("CLLOC"));
    CLRUE.setEnabled(lexique.isPresent("CLRUE"));
    CLCPL.setEnabled(lexique.isPresent("CLCPL"));
    CLNOM.setVisible(lexique.isPresent("CLNOM"));
    WLOCF.setEnabled(lexique.isPresent("WLOCF"));
    WRUEF.setEnabled(lexique.isPresent("WRUEF"));
    WCPLF.setEnabled(lexique.isPresent("WCPLF"));
    WNOMF1.setVisible(lexique.isPresent("WNOMF1"));
    RENET1.setEnabled(lexique.isPresent("RENET1"));
    OBJ_63.setVisible(lexique.isTrue("66"));
    CLDAT2.setVisible(lexique.isTrue("66"));
    riSousMenu12.setVisible(lexique.isTrue("N53"));
    riSousMenu13.setVisible(lexique.isTrue("N53"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "2");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "CBF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "CBL");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "4");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "RBF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "3");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "RBL");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "6");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "FCF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "5");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "FCL");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TRTS", 0, "DAF");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TRTS", 0, "DAL");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "8");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "PIF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "7");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "BNF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "9");
    }
    else {
      lexique.HostFieldPutData("TRTS", 0, "SNF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_36 = new JLabel();
    CLCAT = new XRiTextField();
    OBJ_38 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    OBJ_47 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_59 = new JLabel();
    INDCLI = new XRiTextField();
    OBJ_105 = new RiZoneSortie();
    WNOMF1 = new XRiTextField();
    WCPLF = new XRiTextField();
    WRUEF = new XRiTextField();
    WLOCF = new XRiTextField();
    WNOMFR = new XRiTextField();
    WVILF = new XRiTextField();
    WPAYF = new XRiTextField();
    WCLAF = new XRiTextField();
    OBJ_122 = new JLabel();
    WCDPF = new XRiTextField();
    WCIVF = new XRiTextField();
    WCTRF = new XRiTextField();
    WCOPF = new XRiTextField();
    panel2 = new JPanel();
    OBJ_106 = new JLabel();
    WCLIL = new XRiTextField();
    WLIVL = new XRiTextField();
    CLNOM = new XRiTextField();
    CLCPL = new XRiTextField();
    CLRUE = new XRiTextField();
    CLLOC = new XRiTextField();
    WNOMR = new XRiTextField();
    CLVILR = new XRiTextField();
    CLPAY = new XRiTextField();
    CLCLK = new XRiTextField();
    OBJ_123 = new JLabel();
    CLCDP = new XRiTextField();
    CLCOP = new XRiTextField();
    WCIV = new XRiTextField();
    CLCTR = new XRiTextField();
    panel3 = new JPanel();
    OBJ_140 = new JLabel();
    RENET1 = new XRiTextField();
    panel4 = new JPanel();
    CLTEL = new XRiTextField();
    RETEL2 = new XRiTextField();
    CLFAX = new XRiTextField();
    OBJ_128 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_138 = new JLabel();
    RECAT = new XRiTextField();
    OBJ_134 = new JLabel();
    OBJ_137 = new JLabel();
    REPOS = new XRiTextField();
    OBJ_63 = new JLabel();
    CLDAT2 = new RiZoneSortie();
    panel5 = new JPanel();
    OBJ_131 = new RiZoneSortie();
    CLOBS = new XRiTextField();
    GCDPRL = new XRiTextField();
    OBJ_133 = new JLabel();
    OBJ_135 = new JLabel();
    TFLIBR = new XRiTextField();
    OBJ_130 = new JLabel();
    WDFAC = new XRiTextField();
    OBJ_143 = new JLabel();
    CLMEX = new XRiTextField();
    CLTFA = new XRiTextField();
    OBJ_136 = new JLabel();
    panel6 = new JPanel();
    OBJ_142 = new JLabel();
    CLTOP1 = new XRiTextField();
    CLTOP2 = new XRiTextField();
    CLTOP3 = new XRiTextField();
    CLTOP4 = new XRiTextField();
    CLTOP5 = new XRiTextField();
    OBJ_124 = new RiZoneSortie();
    OBJ_125 = new RiZoneSortie();
    OBJ_126 = new RiZoneSortie();
    OBJ_127 = new RiZoneSortie();
    OBJ_129 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Client");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement");
          OBJ_34.setName("OBJ_34");

          //---- INDETB ----
          INDETB.setText("@INDETB@");
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          //---- OBJ_36 ----
          OBJ_36.setText("Cat\u00e9gorie");
          OBJ_36.setName("OBJ_36");

          //---- CLCAT ----
          CLCAT.setComponentPopupMenu(BTD);
          CLCAT.setName("CLCAT");

          //---- OBJ_38 ----
          OBJ_38.setText("@CCLIB@");
          OBJ_38.setOpaque(false);
          OBJ_38.setName("OBJ_38");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 89, GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(CLCAT, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
              .addComponent(CLCAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- OBJ_47 ----
          OBJ_47.setText("@LIBTNS@");
          OBJ_47.setName("OBJ_47");
          p_tete_droite.add(OBJ_47);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Bon sur client factur\u00e9");
              riSousMenu_bt6.setToolTipText("Cr\u00e9ation d'un bon pour le client factur\u00e9");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Bon sur client livr\u00e9");
              riSousMenu_bt7.setToolTipText("Cr\u00e9ation d'un bon pour le client livr\u00e9");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Recherche bon cli. fac.");
              riSousMenu_bt8.setToolTipText("Recherche de bon sur le client factur\u00e9");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Recherche bon cli. liv.");
              riSousMenu_bt9.setToolTipText("Recherche de bon sur le client livr\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Fiche du client factur\u00e9");
              riSousMenu_bt10.setToolTipText("Fiche du client factur\u00e9");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Fiche du client livr\u00e9");
              riSousMenu_bt11.setToolTipText("Fiche du client livr\u00e9");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Dup. adresse facturation");
              riSousMenu_bt12.setToolTipText("Duplication de l'adresse de facturation vers l'adresse de livraison");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Dup. adresse livraison");
              riSousMenu_bt13.setToolTipText("Duplication de l'adresse de livraison vers l'adresse de facturation");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("M\u00e9mo");
              riSousMenu_bt14.setToolTipText("M\u00e9mo");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc notes");
              riSousMenu_bt15.setToolTipText("Bloc notes");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Statistiques client");
              riSousMenu_bt16.setToolTipText("Statistiques client");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBackground(new Color(214, 217, 223));
            panel1.setBorder(new TitledBorder("Client factur\u00e9"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_59 ----
            OBJ_59.setText("Num\u00e9ro");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(35, 30, 112, 20);

            //---- INDCLI ----
            INDCLI.setComponentPopupMenu(BTD);
            INDCLI.setName("INDCLI");
            panel1.add(INDCLI);
            INDCLI.setBounds(245, 25, 60, INDCLI.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("@INDLIV@");
            OBJ_105.setName("OBJ_105");
            panel1.add(OBJ_105);
            OBJ_105.setBounds(310, 27, 31, OBJ_105.getPreferredSize().height);

            //---- WNOMF1 ----
            WNOMF1.setComponentPopupMenu(BTD);
            WNOMF1.setName("WNOMF1");
            panel1.add(WNOMF1);
            WNOMF1.setBounds(35, 55, 310, WNOMF1.getPreferredSize().height);

            //---- WCPLF ----
            WCPLF.setComponentPopupMenu(BTD);
            WCPLF.setName("WCPLF");
            panel1.add(WCPLF);
            WCPLF.setBounds(35, 84, 310, WCPLF.getPreferredSize().height);

            //---- WRUEF ----
            WRUEF.setComponentPopupMenu(BTD);
            WRUEF.setName("WRUEF");
            panel1.add(WRUEF);
            WRUEF.setBounds(35, 113, 310, WRUEF.getPreferredSize().height);

            //---- WLOCF ----
            WLOCF.setComponentPopupMenu(BTD);
            WLOCF.setName("WLOCF");
            panel1.add(WLOCF);
            WLOCF.setBounds(35, 142, 310, WLOCF.getPreferredSize().height);

            //---- WNOMFR ----
            WNOMFR.setComponentPopupMenu(BTD);
            WNOMFR.setName("WNOMFR");
            panel1.add(WNOMFR);
            WNOMFR.setBounds(75, 55, 270, WNOMFR.getPreferredSize().height);

            //---- WVILF ----
            WVILF.setComponentPopupMenu(BTD);
            WVILF.setName("WVILF");
            panel1.add(WVILF);
            WVILF.setBounds(95, 171, 250, WVILF.getPreferredSize().height);

            //---- WPAYF ----
            WPAYF.setComponentPopupMenu(BTD);
            WPAYF.setName("WPAYF");
            panel1.add(WPAYF);
            WPAYF.setBounds(35, 200, 270, WPAYF.getPreferredSize().height);

            //---- WCLAF ----
            WCLAF.setComponentPopupMenu(BTD);
            WCLAF.setName("WCLAF");
            panel1.add(WCLAF);
            WCLAF.setBounds(150, 229, 160, WCLAF.getPreferredSize().height);

            //---- OBJ_122 ----
            OBJ_122.setText("Classement/Trp");
            OBJ_122.setName("OBJ_122");
            panel1.add(OBJ_122);
            OBJ_122.setBounds(35, 234, 115, 20);

            //---- WCDPF ----
            WCDPF.setComponentPopupMenu(BTD);
            WCDPF.setName("WCDPF");
            panel1.add(WCDPF);
            WCDPF.setBounds(35, 171, 60, WCDPF.getPreferredSize().height);

            //---- WCIVF ----
            WCIVF.setComponentPopupMenu(BTD);
            WCIVF.setName("WCIVF");
            panel1.add(WCIVF);
            WCIVF.setBounds(35, 55, 40, WCIVF.getPreferredSize().height);

            //---- WCTRF ----
            WCTRF.setComponentPopupMenu(BTD);
            WCTRF.setName("WCTRF");
            panel1.add(WCTRF);
            WCTRF.setBounds(315, 229, 30, WCTRF.getPreferredSize().height);

            //---- WCOPF ----
            WCOPF.setComponentPopupMenu(BTD);
            WCOPF.setName("WCOPF");
            panel1.add(WCOPF);
            WCOPF.setBounds(305, 200, 40, WCOPF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBackground(new Color(214, 217, 223));
            panel2.setBorder(new TitledBorder("Client livr\u00e9"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_106 ----
            OBJ_106.setText("Num\u00e9ro");
            OBJ_106.setName("OBJ_106");
            panel2.add(OBJ_106);
            OBJ_106.setBounds(35, 29, 82, 17);

            //---- WCLIL ----
            WCLIL.setComponentPopupMenu(BTD);
            WCLIL.setName("WCLIL");
            panel2.add(WCLIL);
            WCLIL.setBounds(245, 23, 60, WCLIL.getPreferredSize().height);

            //---- WLIVL ----
            WLIVL.setComponentPopupMenu(BTD);
            WLIVL.setName("WLIVL");
            panel2.add(WLIVL);
            WLIVL.setBounds(310, 23, 31, WLIVL.getPreferredSize().height);

            //---- CLNOM ----
            CLNOM.setComponentPopupMenu(BTD);
            CLNOM.setName("CLNOM");
            panel2.add(CLNOM);
            CLNOM.setBounds(35, 55, 310, CLNOM.getPreferredSize().height);

            //---- CLCPL ----
            CLCPL.setComponentPopupMenu(BTD);
            CLCPL.setName("CLCPL");
            panel2.add(CLCPL);
            CLCPL.setBounds(35, 84, 310, CLCPL.getPreferredSize().height);

            //---- CLRUE ----
            CLRUE.setComponentPopupMenu(BTD);
            CLRUE.setName("CLRUE");
            panel2.add(CLRUE);
            CLRUE.setBounds(35, 113, 310, CLRUE.getPreferredSize().height);

            //---- CLLOC ----
            CLLOC.setComponentPopupMenu(BTD);
            CLLOC.setName("CLLOC");
            panel2.add(CLLOC);
            CLLOC.setBounds(35, 142, 310, CLLOC.getPreferredSize().height);

            //---- WNOMR ----
            WNOMR.setComponentPopupMenu(BTD);
            WNOMR.setName("WNOMR");
            panel2.add(WNOMR);
            WNOMR.setBounds(75, 55, 270, WNOMR.getPreferredSize().height);

            //---- CLVILR ----
            CLVILR.setComponentPopupMenu(BTD);
            CLVILR.setName("CLVILR");
            panel2.add(CLVILR);
            CLVILR.setBounds(95, 171, 250, CLVILR.getPreferredSize().height);

            //---- CLPAY ----
            CLPAY.setComponentPopupMenu(BTD);
            CLPAY.setName("CLPAY");
            panel2.add(CLPAY);
            CLPAY.setBounds(35, 200, 270, CLPAY.getPreferredSize().height);

            //---- CLCLK ----
            CLCLK.setComponentPopupMenu(BTD);
            CLCLK.setName("CLCLK");
            panel2.add(CLCLK);
            CLCLK.setBounds(150, 229, 160, CLCLK.getPreferredSize().height);

            //---- OBJ_123 ----
            OBJ_123.setText("Classement/Trp");
            OBJ_123.setName("OBJ_123");
            panel2.add(OBJ_123);
            OBJ_123.setBounds(35, 233, 99, 20);

            //---- CLCDP ----
            CLCDP.setComponentPopupMenu(BTD);
            CLCDP.setName("CLCDP");
            panel2.add(CLCDP);
            CLCDP.setBounds(35, 171, 60, CLCDP.getPreferredSize().height);

            //---- CLCOP ----
            CLCOP.setComponentPopupMenu(BTD);
            CLCOP.setName("CLCOP");
            panel2.add(CLCOP);
            CLCOP.setBounds(305, 200, 40, CLCOP.getPreferredSize().height);

            //---- WCIV ----
            WCIV.setComponentPopupMenu(BTD);
            WCIV.setName("WCIV");
            panel2.add(WCIV);
            WCIV.setBounds(35, 55, 40, WCIV.getPreferredSize().height);

            //---- CLCTR ----
            CLCTR.setComponentPopupMenu(BTD);
            CLCTR.setName("CLCTR");
            panel2.add(CLCTR);
            CLCTR.setBounds(315, 229, 30, CLCTR.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Email"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_140 ----
            OBJ_140.setText("@MAILER@");
            OBJ_140.setName("OBJ_140");
            panel3.add(OBJ_140);
            OBJ_140.setBounds(720, 35, 14, 20);

            //---- RENET1 ----
            RENET1.setComponentPopupMenu(BTD);
            RENET1.setName("RENET1");
            panel3.add(RENET1);
            RENET1.setBounds(35, 30, 670, RENET1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");

            //---- CLTEL ----
            CLTEL.setComponentPopupMenu(BTD);
            CLTEL.setName("CLTEL");

            //---- RETEL2 ----
            RETEL2.setComponentPopupMenu(BTD);
            RETEL2.setName("RETEL2");

            //---- CLFAX ----
            CLFAX.setComponentPopupMenu(BTD);
            CLFAX.setName("CLFAX");

            //---- OBJ_128 ----
            OBJ_128.setText("T\u00e9l\u00e9phone");
            OBJ_128.setName("OBJ_128");

            //---- OBJ_132 ----
            OBJ_132.setText("Mobile");
            OBJ_132.setName("OBJ_132");

            //---- OBJ_138 ----
            OBJ_138.setText("Poste");
            OBJ_138.setName("OBJ_138");

            //---- RECAT ----
            RECAT.setComponentPopupMenu(BTD);
            RECAT.setName("RECAT");

            //---- OBJ_134 ----
            OBJ_134.setText("Fax");
            OBJ_134.setName("OBJ_134");

            //---- OBJ_137 ----
            OBJ_137.setText("Fonction");
            OBJ_137.setName("OBJ_137");

            //---- REPOS ----
            REPOS.setComponentPopupMenu(BTD);
            REPOS.setName("REPOS");

            //---- OBJ_63 ----
            OBJ_63.setText("Identification export/import");
            OBJ_63.setName("OBJ_63");

            //---- CLDAT2 ----
            CLDAT2.setText("@CLDAT2@");
            CLDAT2.setName("CLDAT2");

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_132, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_134, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_137, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
                      .addGap(6, 6, 6)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(CLFAX, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(85, 85, 85)
                          .addComponent(OBJ_138, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CLTEL, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                        .addComponent(RETEL2, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                        .addComponent(RECAT, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(REPOS, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                      .addGap(20, 20, 20)
                      .addComponent(CLDAT2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_132)
                      .addGap(11, 11, 11)
                      .addComponent(OBJ_134)
                      .addGap(9, 9, 9)
                      .addComponent(OBJ_137, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(54, 54, 54)
                      .addComponent(CLFAX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(3, 3, 3)
                      .addComponent(OBJ_138, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CLTEL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(27, 27, 27)
                      .addComponent(RETEL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(81, 81, 81)
                      .addComponent(RECAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(81, 81, 81)
                      .addComponent(REPOS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(1, 1, 1)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(CLDAT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");

            //---- OBJ_131 ----
            OBJ_131.setText("@EXLIBR@");
            OBJ_131.setName("OBJ_131");

            //---- CLOBS ----
            CLOBS.setComponentPopupMenu(BTD);
            CLOBS.setName("CLOBS");

            //---- GCDPRL ----
            GCDPRL.setComponentPopupMenu(BTD);
            GCDPRL.setName("GCDPRL");

            //---- OBJ_133 ----
            OBJ_133.setText("Observation");
            OBJ_133.setName("OBJ_133");

            //---- OBJ_135 ----
            OBJ_135.setText("Dern. facture");
            OBJ_135.setName("OBJ_135");

            //---- TFLIBR ----
            TFLIBR.setComponentPopupMenu(BTD);
            TFLIBR.setName("TFLIBR");

            //---- OBJ_130 ----
            OBJ_130.setText("Mode exp.");
            OBJ_130.setName("OBJ_130");

            //---- WDFAC ----
            WDFAC.setComponentPopupMenu(BTD);
            WDFAC.setName("WDFAC");

            //---- OBJ_143 ----
            OBJ_143.setText("Gencod");
            OBJ_143.setName("OBJ_143");

            //---- CLMEX ----
            CLMEX.setComponentPopupMenu(BTD);
            CLMEX.setName("CLMEX");

            //---- CLTFA ----
            CLTFA.setComponentPopupMenu(BTD);
            CLTFA.setName("CLTFA");

            //---- OBJ_136 ----
            OBJ_136.setText("TF");
            OBJ_136.setName("OBJ_136");

            GroupLayout panel5Layout = new GroupLayout(panel5);
            panel5.setLayout(panel5Layout);
            panel5Layout.setHorizontalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(110, 110, 110)
                  .addComponent(CLMEX, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_131, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(110, 110, 110)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addComponent(WDFAC, GroupLayout.PREFERRED_SIZE, 56, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(90, 90, 90)
                      .addComponent(OBJ_136, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                    .addComponent(GCDPRL, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_143, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(255, 255, 255)
                  .addComponent(TFLIBR, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(230, 230, 230)
                  .addComponent(CLTFA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(110, 110, 110)
                  .addComponent(CLOBS, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE))
            );
            panel5Layout.setVerticalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addComponent(CLMEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(2, 2, 2)
                      .addComponent(OBJ_131, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(3, 3, 3)
                  .addComponent(OBJ_133, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addGap(3, 3, 3)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addComponent(WDFAC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_136, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(27, 27, 27)
                      .addComponent(GCDPRL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(90, 90, 90)
                  .addComponent(OBJ_143, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(59, 59, 59)
                  .addComponent(TFLIBR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(59, 59, 59)
                  .addComponent(CLTFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_130, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(32, 32, 32)
                  .addComponent(CLOBS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(63, 63, 63)
                  .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            );
          }

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder("Zones personnalis\u00e9es"));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_142 ----
            OBJ_142.setText("@MAILER@");
            OBJ_142.setName("OBJ_142");
            panel6.add(OBJ_142);
            OBJ_142.setBounds(715, 30, 14, 20);

            //---- CLTOP1 ----
            CLTOP1.setComponentPopupMenu(BTD);
            CLTOP1.setName("CLTOP1");
            panel6.add(CLTOP1);
            CLTOP1.setBounds(145, 25, 30, CLTOP1.getPreferredSize().height);

            //---- CLTOP2 ----
            CLTOP2.setComponentPopupMenu(BTD);
            CLTOP2.setName("CLTOP2");
            panel6.add(CLTOP2);
            CLTOP2.setBounds(255, 25, 30, CLTOP2.getPreferredSize().height);

            //---- CLTOP3 ----
            CLTOP3.setComponentPopupMenu(BTD);
            CLTOP3.setName("CLTOP3");
            panel6.add(CLTOP3);
            CLTOP3.setBounds(365, 25, 30, CLTOP3.getPreferredSize().height);

            //---- CLTOP4 ----
            CLTOP4.setComponentPopupMenu(BTD);
            CLTOP4.setName("CLTOP4");
            panel6.add(CLTOP4);
            CLTOP4.setBounds(475, 25, 30, CLTOP4.getPreferredSize().height);

            //---- CLTOP5 ----
            CLTOP5.setComponentPopupMenu(BTD);
            CLTOP5.setName("CLTOP5");
            panel6.add(CLTOP5);
            CLTOP5.setBounds(585, 25, 30, CLTOP5.getPreferredSize().height);

            //---- OBJ_124 ----
            OBJ_124.setText("@WTI2@");
            OBJ_124.setName("OBJ_124");
            panel6.add(OBJ_124);
            OBJ_124.setBounds(220, 27, 30, OBJ_124.getPreferredSize().height);

            //---- OBJ_125 ----
            OBJ_125.setText("@WTI3@");
            OBJ_125.setName("OBJ_125");
            panel6.add(OBJ_125);
            OBJ_125.setBounds(330, 27, 30, OBJ_125.getPreferredSize().height);

            //---- OBJ_126 ----
            OBJ_126.setText("@WTI4@");
            OBJ_126.setName("OBJ_126");
            panel6.add(OBJ_126);
            OBJ_126.setBounds(440, 27, 30, OBJ_126.getPreferredSize().height);

            //---- OBJ_127 ----
            OBJ_127.setText("@WTI5@");
            OBJ_127.setName("OBJ_127");
            panel6.add(OBJ_127);
            OBJ_127.setBounds(550, 27, 30, OBJ_127.getPreferredSize().height);

            //---- OBJ_129 ----
            OBJ_129.setText("@WTI1@");
            OBJ_129.setName("OBJ_129");
            panel6.add(OBJ_129);
            OBJ_129.setBounds(110, 27, 30, OBJ_129.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel6, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                      .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)
                      .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(18, 18, 18)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                      .addComponent(panel2, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)))
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(40, 40, 40))
          );
          p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {panel1, panel2});
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel5, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private RiZoneSortie INDETB;
  private JLabel OBJ_36;
  private XRiTextField CLCAT;
  private RiZoneSortie OBJ_38;
  private JPanel p_tete_droite;
  private JLabel OBJ_47;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_59;
  private XRiTextField INDCLI;
  private RiZoneSortie OBJ_105;
  private XRiTextField WNOMF1;
  private XRiTextField WCPLF;
  private XRiTextField WRUEF;
  private XRiTextField WLOCF;
  private XRiTextField WNOMFR;
  private XRiTextField WVILF;
  private XRiTextField WPAYF;
  private XRiTextField WCLAF;
  private JLabel OBJ_122;
  private XRiTextField WCDPF;
  private XRiTextField WCIVF;
  private XRiTextField WCTRF;
  private XRiTextField WCOPF;
  private JPanel panel2;
  private JLabel OBJ_106;
  private XRiTextField WCLIL;
  private XRiTextField WLIVL;
  private XRiTextField CLNOM;
  private XRiTextField CLCPL;
  private XRiTextField CLRUE;
  private XRiTextField CLLOC;
  private XRiTextField WNOMR;
  private XRiTextField CLVILR;
  private XRiTextField CLPAY;
  private XRiTextField CLCLK;
  private JLabel OBJ_123;
  private XRiTextField CLCDP;
  private XRiTextField CLCOP;
  private XRiTextField WCIV;
  private XRiTextField CLCTR;
  private JPanel panel3;
  private JLabel OBJ_140;
  private XRiTextField RENET1;
  private JPanel panel4;
  private XRiTextField CLTEL;
  private XRiTextField RETEL2;
  private XRiTextField CLFAX;
  private JLabel OBJ_128;
  private JLabel OBJ_132;
  private JLabel OBJ_138;
  private XRiTextField RECAT;
  private JLabel OBJ_134;
  private JLabel OBJ_137;
  private XRiTextField REPOS;
  private JLabel OBJ_63;
  private RiZoneSortie CLDAT2;
  private JPanel panel5;
  private RiZoneSortie OBJ_131;
  private XRiTextField CLOBS;
  private XRiTextField GCDPRL;
  private JLabel OBJ_133;
  private JLabel OBJ_135;
  private XRiTextField TFLIBR;
  private JLabel OBJ_130;
  private XRiTextField WDFAC;
  private JLabel OBJ_143;
  private XRiTextField CLMEX;
  private XRiTextField CLTFA;
  private JLabel OBJ_136;
  private JPanel panel6;
  private JLabel OBJ_142;
  private XRiTextField CLTOP1;
  private XRiTextField CLTOP2;
  private XRiTextField CLTOP3;
  private XRiTextField CLTOP4;
  private XRiTextField CLTOP5;
  private RiZoneSortie OBJ_124;
  private RiZoneSortie OBJ_125;
  private RiZoneSortie OBJ_126;
  private RiZoneSortie OBJ_127;
  private RiZoneSortie OBJ_129;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
