
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_C4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM03FM_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    EBIN04.setValeursSelection("1", "");
    EBIN06.setValeursSelection("1", "");
    EBIN07.setValeursSelection("1", "");
    
    // Titre
    setTitle("Réglement");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGLIB@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBECH@")).trim());
    UCLEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEX@")).trim());
    UCLEY.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UCLEY@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean isModif = (!lexique.isTrue("53"));
    
    UCLEX.setVisible(!lexique.HostFieldGetData("UCLEX").trim().isEmpty());
    UCLEY.setVisible(!lexique.HostFieldGetData("UCLEY").trim().isEmpty());
    coffre.setEnabled(isModif);
    navig_valid.setVisible(isModif);
    
    // découpe de l'IBAN pour présentation standard
    String zoneIban = lexique.HostFieldGetData("WRIE");
    if (zoneIban.length() >= 0) {
      if (zoneIban.length() <= 4) {
        WIBAN1.setText(zoneIban.substring(0, zoneIban.length()));
      }
      else {
        WIBAN1.setText(zoneIban.substring(0, 4));
      }
    }
    if (zoneIban.length() >= 4) {
      if (zoneIban.length() <= 8) {
        WIBAN2.setText(zoneIban.substring(4, zoneIban.length()));
      }
      else {
        WIBAN2.setText(zoneIban.substring(4, 8));
      }
    }
    if (zoneIban.length() >= 8) {
      if (zoneIban.length() <= 12) {
        WIBAN3.setText(zoneIban.substring(8, zoneIban.length()));
      }
      else {
        WIBAN3.setText(zoneIban.substring(8, 12));
      }
    }
    if (zoneIban.length() >= 12) {
      if (zoneIban.length() <= 16) {
        WIBAN4.setText(zoneIban.substring(12, zoneIban.length()));
      }
      else {
        WIBAN4.setText(zoneIban.substring(12, 16));
      }
    }
    if (zoneIban.length() >= 16) {
      if (zoneIban.length() <= 20) {
        WIBAN5.setText(zoneIban.substring(16, zoneIban.length()));
      }
      else {
        WIBAN5.setText(zoneIban.substring(16, 20));
      }
    }
    if (zoneIban.length() >= 20) {
      if (zoneIban.length() <= 24) {
        WIBAN6.setText(zoneIban.substring(20, zoneIban.length()));
      }
      else {
        WIBAN6.setText(zoneIban.substring(20, 24));
      }
    }
    if (zoneIban.length() >= 24) {
      if (zoneIban.length() <= 28) {
        WIBAN7.setText(zoneIban.substring(24, zoneIban.length()));
      }
      else {
        WIBAN7.setText(zoneIban.substring(24, 28));
      }
    }
    if (zoneIban.length() >= 28) {
      WIBAN8.setText(zoneIban.substring(28, zoneIban.length()));
    }
    
    WIBAN1.setEnabled(isModif);
    WIBAN2.setEnabled(isModif);
    WIBAN3.setEnabled(isModif);
    WIBAN4.setEnabled(isModif);
    WIBAN5.setEnabled(isModif);
    WIBAN6.setEnabled(isModif);
    WIBAN7.setEnabled(isModif);
    WIBAN8.setEnabled(isModif);
    
    WIBAN1.setLongueurSaisie(4);
    WIBAN2.setLongueurSaisie(4);
    WIBAN3.setLongueurSaisie(4);
    WIBAN4.setLongueurSaisie(4);
    WIBAN5.setLongueurSaisie(4);
    WIBAN6.setLongueurSaisie(4);
    WIBAN7.setLongueurSaisie(4);
    WIBAN8.setLongueurSaisie(2);
    
    riBoutonDetail1.setEnabled(isModif);
    riBoutonDetail2.setEnabled(isModif);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    WIBAN1.setText(WIBAN1.getText().toUpperCase());
    WIBAN2.setText(WIBAN2.getText().toUpperCase());
    WIBAN3.setText(WIBAN3.getText().toUpperCase());
    WIBAN4.setText(WIBAN4.getText().toUpperCase());
    WIBAN5.setText(WIBAN5.getText().toUpperCase());
    WIBAN6.setText(WIBAN6.getText().toUpperCase());
    WIBAN7.setText(WIBAN7.getText().toUpperCase());
    WIBAN8.setText(WIBAN8.getText().toUpperCase());
    
    lexique.HostFieldPutData("WRIE", 0, WIBAN1.getText() + WIBAN2.getText() + WIBAN3.getText() + WIBAN4.getText() + WIBAN5.getText()
        + WIBAN6.getText() + WIBAN7.getText() + WIBAN8.getText());
  }
  
  private void coffreActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("CLRGL");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("CLECH");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_27 = new JPanel();
    OBJ_30 = new RiZoneSortie();
    OBJ_28 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_38 = new JLabel();
    CLAFA = new XRiTextField();
    CLCLP = new XRiTextField();
    CLRGL = new XRiTextField();
    CLECH = new XRiTextField();
    OBJ_33 = new RiZoneSortie();
    ULBRGL = new XRiTextField();
    riBoutonDetail1 = new SNBoutonDetail();
    riBoutonDetail2 = new SNBoutonDetail();
    EBIN04 = new XRiCheckBox();
    EBIN06 = new XRiCheckBox();
    EBIN07 = new XRiCheckBox();
    OBJ_42 = new JPanel();
    panel2 = new JPanel();
    WDO1 = new XRiTextField();
    OBJ_22_OBJ_22 = new JLabel();
    WBQE = new XRiTextField();
    WGUI = new XRiTextField();
    WCPT = new XRiTextField();
    WRIB = new XRiTextField();
    UCLEX = new RiZoneSortie();
    OBJ_22_OBJ_24 = new JLabel();
    OBJ_22_OBJ_25 = new JLabel();
    OBJ_22_OBJ_26 = new JLabel();
    OBJ_22_OBJ_27 = new JLabel();
    OBJ_22_OBJ_28 = new JLabel();
    coffre = new SNBoutonDetail();
    panel3 = new JPanel();
    WDO2 = new XRiTextField();
    OBJ_40_OBJ_40 = new JLabel();
    WIBAN1 = new XRiTextField();
    WIBAN2 = new XRiTextField();
    WIBAN3 = new XRiTextField();
    WIBAN4 = new XRiTextField();
    WIBAN5 = new XRiTextField();
    WIBAN6 = new XRiTextField();
    WIBAN7 = new XRiTextField();
    WIBAN8 = new XRiTextField();
    UCLEY = new RiZoneSortie();
    OBJ_22_OBJ_23 = new JLabel();
    BTD = new JPopupMenu();
    menuItem1 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1070, 380));
    setPreferredSize(new Dimension(1070, 380));
    setMaximumSize(new Dimension(1070, 380));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== OBJ_27 ========
        {
          OBJ_27.setBorder(new TitledBorder("Divers"));
          OBJ_27.setOpaque(false);
          OBJ_27.setName("OBJ_27");
          OBJ_27.setLayout(null);
          
          // ---- OBJ_30 ----
          OBJ_30.setText("@RGLIB@");
          OBJ_30.setName("OBJ_30");
          OBJ_27.add(OBJ_30);
          OBJ_30.setBounds(175, 35, 272, 28);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("Mode");
          OBJ_28.setName("OBJ_28");
          OBJ_27.add(OBJ_28);
          OBJ_28.setBounds(35, 40, 95, 20);
          
          // ---- OBJ_31 ----
          OBJ_31.setText("Ech\u00e9ance");
          OBJ_31.setName("OBJ_31");
          OBJ_27.add(OBJ_31);
          OBJ_31.setBounds(35, 71, 95, 20);
          
          // ---- OBJ_34 ----
          OBJ_34.setText("Libell\u00e9 \u00e9dit\u00e9");
          OBJ_34.setName("OBJ_34");
          OBJ_27.add(OBJ_34);
          OBJ_34.setBounds(35, 102, 95, 20);
          
          // ---- OBJ_36 ----
          OBJ_36.setText("Client payeur");
          OBJ_36.setName("OBJ_36");
          OBJ_27.add(OBJ_36);
          OBJ_36.setBounds(35, 133, 90, 20);
          
          // ---- OBJ_38 ----
          OBJ_38.setText("Code affacturage");
          OBJ_38.setName("OBJ_38");
          OBJ_27.add(OBJ_38);
          OBJ_38.setBounds(620, 133, 115, 20);
          
          // ---- CLAFA ----
          CLAFA.setComponentPopupMenu(null);
          CLAFA.setName("CLAFA");
          OBJ_27.add(CLAFA);
          CLAFA.setBounds(750, 129, 80, CLAFA.getPreferredSize().height);
          
          // ---- CLCLP ----
          CLCLP.setComponentPopupMenu(null);
          CLCLP.setName("CLCLP");
          OBJ_27.add(CLCLP);
          CLCLP.setBounds(135, 129, 60, CLCLP.getPreferredSize().height);
          
          // ---- CLRGL ----
          CLRGL.setComponentPopupMenu(BTD);
          CLRGL.setName("CLRGL");
          OBJ_27.add(CLRGL);
          CLRGL.setBounds(135, 35, 34, CLRGL.getPreferredSize().height);
          
          // ---- CLECH ----
          CLECH.setComponentPopupMenu(BTD);
          CLECH.setName("CLECH");
          OBJ_27.add(CLECH);
          CLECH.setBounds(135, 67, 34, CLECH.getPreferredSize().height);
          
          // ---- OBJ_33 ----
          OBJ_33.setText("@ULBECH@");
          OBJ_33.setName("OBJ_33");
          OBJ_27.add(OBJ_33);
          OBJ_33.setBounds(175, 67, 272, 28);
          
          // ---- ULBRGL ----
          ULBRGL.setName("ULBRGL");
          OBJ_27.add(ULBRGL);
          ULBRGL.setBounds(135, 98, 307, ULBRGL.getPreferredSize().height);
          
          // ---- riBoutonDetail1 ----
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          OBJ_27.add(riBoutonDetail1);
          riBoutonDetail1.setBounds(450, 40, 28, riBoutonDetail1.getPreferredSize().height);
          
          // ---- riBoutonDetail2 ----
          riBoutonDetail2.setName("riBoutonDetail2");
          riBoutonDetail2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail2ActionPerformed(e);
            }
          });
          OBJ_27.add(riBoutonDetail2);
          riBoutonDetail2.setBounds(450, 72, 28, riBoutonDetail2.getPreferredSize().height);
          
          // ---- EBIN04 ----
          EBIN04.setText("Adresse de facturation");
          EBIN04.setToolTipText(
              "Cette option permet de proposer l'adresse du client payeur comme adresse de facturation en cr\u00e9ation de bon ou facture de vente");
          EBIN04.setName("EBIN04");
          OBJ_27.add(EBIN04);
          EBIN04.setBounds(200, 131, 160, 25);
          
          // ---- EBIN06 ----
          EBIN06.setText("R\u00e8glement comptant obligatoire");
          EBIN06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBIN06.setName("EBIN06");
          OBJ_27.add(EBIN06);
          EBIN06.setBounds(620, 34, 220, 30);
          
          // ---- EBIN07 ----
          EBIN07.setText("R\u00e8glement par ch\u00e8que interdit");
          EBIN07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBIN07.setName("EBIN07");
          OBJ_27.add(EBIN07);
          EBIN07.setBounds(620, 66, 220, 30);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < OBJ_27.getComponentCount(); i++) {
              Rectangle bounds = OBJ_27.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_27.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_27.setMinimumSize(preferredSize);
            OBJ_27.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_27);
        OBJ_27.setBounds(20, 20, 875, 175);
        
        // ======== OBJ_42 ========
        {
          OBJ_42.setBorder(new TitledBorder("Domiciliation bancaire principale"));
          OBJ_42.setOpaque(false);
          OBJ_42.setName("OBJ_42");
          OBJ_42.setLayout(null);
          
          // ======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- WDO1 ----
            WDO1.setComponentPopupMenu(BTD);
            WDO1.setName("WDO1");
            panel2.add(WDO1);
            WDO1.setBounds(114, 25, 220, WDO1.getPreferredSize().height);
            
            // ---- OBJ_22_OBJ_22 ----
            OBJ_22_OBJ_22.setText("RIB");
            OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
            panel2.add(OBJ_22_OBJ_22);
            OBJ_22_OBJ_22.setBounds(355, 29, 55, 20);
            
            // ---- WBQE ----
            WBQE.setComponentPopupMenu(BTD);
            WBQE.setToolTipText("num\u00e9ro de banque");
            WBQE.setName("WBQE");
            panel2.add(WBQE);
            WBQE.setBounds(410, 25, 50, WBQE.getPreferredSize().height);
            
            // ---- WGUI ----
            WGUI.setComponentPopupMenu(BTD);
            WGUI.setToolTipText("num\u00e9ro de guichet");
            WGUI.setName("WGUI");
            panel2.add(WGUI);
            WGUI.setBounds(462, 25, 50, WGUI.getPreferredSize().height);
            
            // ---- WCPT ----
            WCPT.setComponentPopupMenu(BTD);
            WCPT.setToolTipText("num\u00e9ro de compte");
            WCPT.setName("WCPT");
            panel2.add(WCPT);
            WCPT.setBounds(514, 25, 121, WCPT.getPreferredSize().height);
            
            // ---- WRIB ----
            WRIB.setComponentPopupMenu(BTD);
            WRIB.setToolTipText("Cl\u00e9");
            WRIB.setName("WRIB");
            panel2.add(WRIB);
            WRIB.setBounds(670, 25, 28, WRIB.getPreferredSize().height);
            
            // ---- UCLEX ----
            UCLEX.setText("@UCLEX@");
            UCLEX.setForeground(Color.red);
            UCLEX.setToolTipText("Cl\u00e9 r\u00e9elle");
            UCLEX.setName("UCLEX");
            panel2.add(UCLEX);
            UCLEX.setBounds(805, 27, 25, UCLEX.getPreferredSize().height);
            
            // ---- OBJ_22_OBJ_24 ----
            OBJ_22_OBJ_24.setText("Banque");
            OBJ_22_OBJ_24.setName("OBJ_22_OBJ_24");
            panel2.add(OBJ_22_OBJ_24);
            OBJ_22_OBJ_24.setBounds(5, 29, 110, 20);
            
            // ---- OBJ_22_OBJ_25 ----
            OBJ_22_OBJ_25.setText("Banque");
            OBJ_22_OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_25.setFont(OBJ_22_OBJ_25.getFont().deriveFont(OBJ_22_OBJ_25.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_25.setName("OBJ_22_OBJ_25");
            panel2.add(OBJ_22_OBJ_25);
            OBJ_22_OBJ_25.setBounds(410, 5, 50, 20);
            
            // ---- OBJ_22_OBJ_26 ----
            OBJ_22_OBJ_26.setText("Guichet");
            OBJ_22_OBJ_26.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_26.setFont(OBJ_22_OBJ_26.getFont().deriveFont(OBJ_22_OBJ_26.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_26.setName("OBJ_22_OBJ_26");
            panel2.add(OBJ_22_OBJ_26);
            OBJ_22_OBJ_26.setBounds(462, 5, 50, 20);
            
            // ---- OBJ_22_OBJ_27 ----
            OBJ_22_OBJ_27.setText("Num\u00e9ro de compte");
            OBJ_22_OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_27.setFont(OBJ_22_OBJ_27.getFont().deriveFont(OBJ_22_OBJ_27.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_27.setName("OBJ_22_OBJ_27");
            panel2.add(OBJ_22_OBJ_27);
            OBJ_22_OBJ_27.setBounds(514, 5, 121, 20);
            
            // ---- OBJ_22_OBJ_28 ----
            OBJ_22_OBJ_28.setText("Cl\u00e9");
            OBJ_22_OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_22_OBJ_28.setFont(OBJ_22_OBJ_28.getFont().deriveFont(OBJ_22_OBJ_28.getFont().getStyle() | Font.BOLD));
            OBJ_22_OBJ_28.setName("OBJ_22_OBJ_28");
            panel2.add(OBJ_22_OBJ_28);
            OBJ_22_OBJ_28.setBounds(670, 5, 28, 20);
            
            // ---- coffre ----
            coffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            coffre.setName("coffre");
            coffre.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                coffreActionPerformed(e);
              }
            });
            panel2.add(coffre);
            coffre.setBounds(830, 23, 32, 32);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          OBJ_42.add(panel2);
          panel2.setBounds(5, 25, 865, 60);
          
          // ======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- WDO2 ----
            WDO2.setComponentPopupMenu(BTD);
            WDO2.setName("WDO2");
            panel3.add(WDO2);
            WDO2.setBounds(114, 15, 220, WDO2.getPreferredSize().height);
            
            // ---- OBJ_40_OBJ_40 ----
            OBJ_40_OBJ_40.setText("IBAN");
            OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");
            panel3.add(OBJ_40_OBJ_40);
            OBJ_40_OBJ_40.setBounds(355, 19, 56, 20);
            
            // ---- WIBAN1 ----
            WIBAN1.setComponentPopupMenu(BTD);
            WIBAN1.setNextFocusableComponent(WIBAN2);
            WIBAN1.setName("WIBAN1");
            panel3.add(WIBAN1);
            WIBAN1.setBounds(410, 15, 54, WIBAN1.getPreferredSize().height);
            
            // ---- WIBAN2 ----
            WIBAN2.setComponentPopupMenu(BTD);
            WIBAN2.setNextFocusableComponent(WIBAN3);
            WIBAN2.setName("WIBAN2");
            panel3.add(WIBAN2);
            WIBAN2.setBounds(462, 15, 54, WIBAN2.getPreferredSize().height);
            
            // ---- WIBAN3 ----
            WIBAN3.setComponentPopupMenu(BTD);
            WIBAN3.setNextFocusableComponent(WIBAN4);
            WIBAN3.setName("WIBAN3");
            panel3.add(WIBAN3);
            WIBAN3.setBounds(514, 15, 54, WIBAN3.getPreferredSize().height);
            
            // ---- WIBAN4 ----
            WIBAN4.setComponentPopupMenu(BTD);
            WIBAN4.setNextFocusableComponent(WIBAN5);
            WIBAN4.setName("WIBAN4");
            panel3.add(WIBAN4);
            WIBAN4.setBounds(566, 15, 54, WIBAN4.getPreferredSize().height);
            
            // ---- WIBAN5 ----
            WIBAN5.setComponentPopupMenu(BTD);
            WIBAN5.setNextFocusableComponent(WIBAN6);
            WIBAN5.setName("WIBAN5");
            panel3.add(WIBAN5);
            WIBAN5.setBounds(618, 15, 54, WIBAN5.getPreferredSize().height);
            
            // ---- WIBAN6 ----
            WIBAN6.setComponentPopupMenu(BTD);
            WIBAN6.setNextFocusableComponent(WIBAN7);
            WIBAN6.setName("WIBAN6");
            panel3.add(WIBAN6);
            WIBAN6.setBounds(670, 15, 54, WIBAN6.getPreferredSize().height);
            
            // ---- WIBAN7 ----
            WIBAN7.setComponentPopupMenu(BTD);
            WIBAN7.setNextFocusableComponent(WIBAN8);
            WIBAN7.setName("WIBAN7");
            panel3.add(WIBAN7);
            WIBAN7.setBounds(722, 15, 54, WIBAN7.getPreferredSize().height);
            
            // ---- WIBAN8 ----
            WIBAN8.setComponentPopupMenu(BTD);
            WIBAN8.setName("WIBAN8");
            panel3.add(WIBAN8);
            WIBAN8.setBounds(775, 15, 28, WIBAN8.getPreferredSize().height);
            
            // ---- UCLEY ----
            UCLEY.setText("@UCLEY@");
            UCLEY.setForeground(Color.red);
            UCLEY.setName("UCLEY");
            panel3.add(UCLEY);
            UCLEY.setBounds(805, 17, 25, UCLEY.getPreferredSize().height);
            
            // ---- OBJ_22_OBJ_23 ----
            OBJ_22_OBJ_23.setText("Guichet ou SWIFT");
            OBJ_22_OBJ_23.setName("OBJ_22_OBJ_23");
            panel3.add(OBJ_22_OBJ_23);
            OBJ_22_OBJ_23.setBounds(5, 19, 110, 20);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          OBJ_42.add(panel3);
          panel3.setBounds(5, 90, 865, 55);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < OBJ_42.getComponentCount(); i++) {
              Rectangle bounds = OBJ_42.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_42.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_42.setMinimumSize(preferredSize);
            OBJ_42.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_42);
        OBJ_42.setBounds(15, 210, 875, 155);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD.add(menuItem1);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel OBJ_27;
  private RiZoneSortie OBJ_30;
  private JLabel OBJ_28;
  private JLabel OBJ_31;
  private JLabel OBJ_34;
  private JLabel OBJ_36;
  private JLabel OBJ_38;
  private XRiTextField CLAFA;
  private XRiTextField CLCLP;
  private XRiTextField CLRGL;
  private XRiTextField CLECH;
  private RiZoneSortie OBJ_33;
  private XRiTextField ULBRGL;
  private SNBoutonDetail riBoutonDetail1;
  private SNBoutonDetail riBoutonDetail2;
  private XRiCheckBox EBIN04;
  private XRiCheckBox EBIN06;
  private XRiCheckBox EBIN07;
  private JPanel OBJ_42;
  private JPanel panel2;
  private XRiTextField WDO1;
  private JLabel OBJ_22_OBJ_22;
  private XRiTextField WBQE;
  private XRiTextField WGUI;
  private XRiTextField WCPT;
  private XRiTextField WRIB;
  private RiZoneSortie UCLEX;
  private JLabel OBJ_22_OBJ_24;
  private JLabel OBJ_22_OBJ_25;
  private JLabel OBJ_22_OBJ_26;
  private JLabel OBJ_22_OBJ_27;
  private JLabel OBJ_22_OBJ_28;
  private SNBoutonDetail coffre;
  private JPanel panel3;
  private XRiTextField WDO2;
  private JLabel OBJ_40_OBJ_40;
  private XRiTextField WIBAN1;
  private XRiTextField WIBAN2;
  private XRiTextField WIBAN3;
  private XRiTextField WIBAN4;
  private XRiTextField WIBAN5;
  private XRiTextField WIBAN6;
  private XRiTextField WIBAN7;
  private XRiTextField WIBAN8;
  private RiZoneSortie UCLEY;
  private JLabel OBJ_22_OBJ_23;
  private JPopupMenu BTD;
  private JMenuItem menuItem1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
