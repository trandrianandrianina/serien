
package ri.serien.libecranrpg.vgvm.VGVM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM27FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM27FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ERR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR1@")).trim());
    ERR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR2@")).trim());
    ERR3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR3@")).trim());
    ERR4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR4@")).trim());
    ERR5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR5@")).trim());
    ERR6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR6@")).trim());
    ERR7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR7@")).trim());
    ERR8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR8@")).trim());
    ERR9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR9@")).trim());
    ERR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR10@")).trim());
    ERR11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR11@")).trim());
    ERR12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR12@")).trim());
    ERR13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR13@")).trim());
    ERR14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR14@")).trim());
    ERR15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // WFIN2X.setEnabled( lexique.isPresent("WFIN2X"));
    // WDEB2X.setEnabled( lexique.isPresent("WDEB2X"));
    // WFIN1X.setEnabled( lexique.isPresent("WFIN1X"));
    // WDEB1X.setEnabled( lexique.isPresent("WDEB1X"));
    BRLIB.setEnabled(lexique.isPresent("BRLIB"));
    if (lexique.isTrue("92")) {
      p_bpresentation.setText("Barèmes de bonifications");
    }
    else {
      p_bpresentation.setText("Barèmes de remises");
    }
    
    OBJ_19.setVisible(!lexique.isTrue("53"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @WTYP@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    WETB = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    OBJ_50 = new JLabel();
    WCNV = new XRiTextField();
    BRLIB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panelgen = new JPanel();
    panel2 = new JPanel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    WDEB1X = new XRiCalendrier();
    WFIN1X = new XRiCalendrier();
    C2A101 = new XRiTextField();
    C2A102 = new XRiTextField();
    C2A103 = new XRiTextField();
    C2A104 = new XRiTextField();
    C2A105 = new XRiTextField();
    C2A106 = new XRiTextField();
    C2A107 = new XRiTextField();
    C2A108 = new XRiTextField();
    C2A109 = new XRiTextField();
    C2A110 = new XRiTextField();
    C2A111 = new XRiTextField();
    C2A112 = new XRiTextField();
    C2A113 = new XRiTextField();
    C2A114 = new XRiTextField();
    C2A115 = new XRiTextField();
    C2Q101 = new XRiTextField();
    C2Q102 = new XRiTextField();
    C2Q103 = new XRiTextField();
    C2Q104 = new XRiTextField();
    C2Q105 = new XRiTextField();
    C2Q106 = new XRiTextField();
    C2Q107 = new XRiTextField();
    C2Q108 = new XRiTextField();
    C2Q109 = new XRiTextField();
    C2Q110 = new XRiTextField();
    C2Q111 = new XRiTextField();
    C2Q112 = new XRiTextField();
    C2Q113 = new XRiTextField();
    C2Q114 = new XRiTextField();
    C2Q115 = new XRiTextField();
    separator2 = compFactory.createSeparator("Article n\u00b01");
    separator5 = compFactory.createSeparator("Qt\u00e9");
    panel4 = new JPanel();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    WDEB2X = new XRiCalendrier();
    WFIN2X = new XRiCalendrier();
    separator3 = compFactory.createSeparator("Article n\u00b02");
    C2A201 = new XRiTextField();
    C2A202 = new XRiTextField();
    C2A203 = new XRiTextField();
    C2A204 = new XRiTextField();
    C2A205 = new XRiTextField();
    C2A206 = new XRiTextField();
    C2A207 = new XRiTextField();
    C2A208 = new XRiTextField();
    C2A209 = new XRiTextField();
    C2A210 = new XRiTextField();
    C2A211 = new XRiTextField();
    C2A212 = new XRiTextField();
    C2A213 = new XRiTextField();
    C2A214 = new XRiTextField();
    C2A215 = new XRiTextField();
    C2Q201 = new XRiTextField();
    C2Q202 = new XRiTextField();
    C2Q203 = new XRiTextField();
    C2Q204 = new XRiTextField();
    C2Q205 = new XRiTextField();
    C2Q206 = new XRiTextField();
    C2Q207 = new XRiTextField();
    C2Q208 = new XRiTextField();
    C2Q209 = new XRiTextField();
    C2Q210 = new XRiTextField();
    C2Q211 = new XRiTextField();
    C2Q212 = new XRiTextField();
    C2Q213 = new XRiTextField();
    C2Q214 = new XRiTextField();
    C2Q215 = new XRiTextField();
    separator6 = compFactory.createSeparator("Qt\u00e9");
    Q101 = new XRiTextField();
    Q102 = new XRiTextField();
    Q103 = new XRiTextField();
    Q104 = new XRiTextField();
    Q105 = new XRiTextField();
    Q106 = new XRiTextField();
    Q107 = new XRiTextField();
    Q108 = new XRiTextField();
    Q109 = new XRiTextField();
    Q110 = new XRiTextField();
    Q111 = new XRiTextField();
    Q112 = new XRiTextField();
    Q113 = new XRiTextField();
    Q114 = new XRiTextField();
    Q115 = new XRiTextField();
    separator1 = compFactory.createSeparator("Montant mini");
    ERR1 = new JLabel();
    ERR2 = new JLabel();
    ERR3 = new JLabel();
    ERR4 = new JLabel();
    ERR5 = new JLabel();
    ERR6 = new JLabel();
    ERR7 = new JLabel();
    ERR8 = new JLabel();
    ERR9 = new JLabel();
    ERR10 = new JLabel();
    ERR11 = new JLabel();
    ERR12 = new JLabel();
    ERR13 = new JLabel();
    ERR14 = new JLabel();
    ERR15 = new JLabel();
    C2G01 = new XRiTextField();
    C2G2 = new XRiTextField();
    C2G3 = new XRiTextField();
    C2G4 = new XRiTextField();
    C2G5 = new XRiTextField();
    C2G6 = new XRiTextField();
    C2G7 = new XRiTextField();
    C2G8 = new XRiTextField();
    C2G9 = new XRiTextField();
    C2G10 = new XRiTextField();
    C2G11 = new XRiTextField();
    C2G12 = new XRiTextField();
    C2G13 = new XRiTextField();
    C2G14 = new XRiTextField();
    C2G15 = new XRiTextField();
    separator4 = compFactory.createSeparator("Gratuit");
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bar\u00e8mes de remises");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- OBJ_50 ----
          OBJ_50.setText("Code");
          OBJ_50.setName("OBJ_50");

          //---- WCNV ----
          WCNV.setComponentPopupMenu(null);
          WCNV.setName("WCNV");

          //---- BRLIB ----
          BRLIB.setComponentPopupMenu(null);
          BRLIB.setName("BRLIB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BRLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
              .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BRLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Saisie remises");
              riSousMenu_bt6.setToolTipText("Saisie de remises");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 700));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panelgen ========
          {
            panelgen.setBorder(new TitledBorder("P\u00e9riode n\u00b02"));
            panelgen.setOpaque(false);
            panelgen.setName("panelgen");
            panelgen.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("P\u00e9riode n\u00b01"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_52 ----
              OBJ_52.setText("du");
              OBJ_52.setName("OBJ_52");
              panel2.add(OBJ_52);
              OBJ_52.setBounds(10, 34, 18, 20);

              //---- OBJ_54 ----
              OBJ_54.setText("au");
              OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_54.setName("OBJ_54");
              panel2.add(OBJ_54);
              OBJ_54.setBounds(170, 34, 55, 20);

              //---- WDEB1X ----
              WDEB1X.setName("WDEB1X");
              panel2.add(WDEB1X);
              WDEB1X.setBounds(65, 30, 105, WDEB1X.getPreferredSize().height);

              //---- WFIN1X ----
              WFIN1X.setName("WFIN1X");
              panel2.add(WFIN1X);
              WFIN1X.setBounds(225, 30, 105, WFIN1X.getPreferredSize().height);

              //---- C2A101 ----
              C2A101.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A101.setComponentPopupMenu(BTD);
              C2A101.setName("C2A101");
              panel2.add(C2A101);
              C2A101.setBounds(10, 80, 240, C2A101.getPreferredSize().height);

              //---- C2A102 ----
              C2A102.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A102.setComponentPopupMenu(BTD);
              C2A102.setName("C2A102");
              panel2.add(C2A102);
              C2A102.setBounds(10, 105, 240, C2A102.getPreferredSize().height);

              //---- C2A103 ----
              C2A103.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A103.setComponentPopupMenu(BTD);
              C2A103.setName("C2A103");
              panel2.add(C2A103);
              C2A103.setBounds(10, 130, 240, C2A103.getPreferredSize().height);

              //---- C2A104 ----
              C2A104.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A104.setComponentPopupMenu(BTD);
              C2A104.setName("C2A104");
              panel2.add(C2A104);
              C2A104.setBounds(10, 155, 240, C2A104.getPreferredSize().height);

              //---- C2A105 ----
              C2A105.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A105.setComponentPopupMenu(BTD);
              C2A105.setName("C2A105");
              panel2.add(C2A105);
              C2A105.setBounds(10, 180, 240, C2A105.getPreferredSize().height);

              //---- C2A106 ----
              C2A106.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A106.setComponentPopupMenu(BTD);
              C2A106.setName("C2A106");
              panel2.add(C2A106);
              C2A106.setBounds(10, 205, 240, C2A106.getPreferredSize().height);

              //---- C2A107 ----
              C2A107.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A107.setComponentPopupMenu(BTD);
              C2A107.setName("C2A107");
              panel2.add(C2A107);
              C2A107.setBounds(10, 230, 240, C2A107.getPreferredSize().height);

              //---- C2A108 ----
              C2A108.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A108.setComponentPopupMenu(BTD);
              C2A108.setName("C2A108");
              panel2.add(C2A108);
              C2A108.setBounds(10, 255, 240, C2A108.getPreferredSize().height);

              //---- C2A109 ----
              C2A109.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A109.setComponentPopupMenu(BTD);
              C2A109.setName("C2A109");
              panel2.add(C2A109);
              C2A109.setBounds(10, 280, 240, C2A109.getPreferredSize().height);

              //---- C2A110 ----
              C2A110.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A110.setComponentPopupMenu(BTD);
              C2A110.setName("C2A110");
              panel2.add(C2A110);
              C2A110.setBounds(10, 305, 240, C2A110.getPreferredSize().height);

              //---- C2A111 ----
              C2A111.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A111.setComponentPopupMenu(BTD);
              C2A111.setName("C2A111");
              panel2.add(C2A111);
              C2A111.setBounds(10, 330, 240, C2A111.getPreferredSize().height);

              //---- C2A112 ----
              C2A112.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A112.setComponentPopupMenu(BTD);
              C2A112.setName("C2A112");
              panel2.add(C2A112);
              C2A112.setBounds(10, 355, 240, C2A112.getPreferredSize().height);

              //---- C2A113 ----
              C2A113.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A113.setComponentPopupMenu(BTD);
              C2A113.setName("C2A113");
              panel2.add(C2A113);
              C2A113.setBounds(10, 380, 240, C2A113.getPreferredSize().height);

              //---- C2A114 ----
              C2A114.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A114.setComponentPopupMenu(BTD);
              C2A114.setName("C2A114");
              panel2.add(C2A114);
              C2A114.setBounds(10, 405, 240, C2A114.getPreferredSize().height);

              //---- C2A115 ----
              C2A115.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A115.setComponentPopupMenu(BTD);
              C2A115.setName("C2A115");
              panel2.add(C2A115);
              C2A115.setBounds(10, 430, 240, C2A115.getPreferredSize().height);

              //---- C2Q101 ----
              C2Q101.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q101.setName("C2Q101");
              panel2.add(C2Q101);
              C2Q101.setBounds(250, 80, 80, C2Q101.getPreferredSize().height);

              //---- C2Q102 ----
              C2Q102.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q102.setName("C2Q102");
              panel2.add(C2Q102);
              C2Q102.setBounds(250, 105, 80, C2Q102.getPreferredSize().height);

              //---- C2Q103 ----
              C2Q103.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q103.setName("C2Q103");
              panel2.add(C2Q103);
              C2Q103.setBounds(250, 130, 80, C2Q103.getPreferredSize().height);

              //---- C2Q104 ----
              C2Q104.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q104.setName("C2Q104");
              panel2.add(C2Q104);
              C2Q104.setBounds(250, 155, 80, C2Q104.getPreferredSize().height);

              //---- C2Q105 ----
              C2Q105.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q105.setName("C2Q105");
              panel2.add(C2Q105);
              C2Q105.setBounds(250, 180, 80, C2Q105.getPreferredSize().height);

              //---- C2Q106 ----
              C2Q106.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q106.setName("C2Q106");
              panel2.add(C2Q106);
              C2Q106.setBounds(250, 205, 80, C2Q106.getPreferredSize().height);

              //---- C2Q107 ----
              C2Q107.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q107.setName("C2Q107");
              panel2.add(C2Q107);
              C2Q107.setBounds(250, 230, 80, C2Q107.getPreferredSize().height);

              //---- C2Q108 ----
              C2Q108.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q108.setName("C2Q108");
              panel2.add(C2Q108);
              C2Q108.setBounds(250, 255, 80, C2Q108.getPreferredSize().height);

              //---- C2Q109 ----
              C2Q109.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q109.setName("C2Q109");
              panel2.add(C2Q109);
              C2Q109.setBounds(250, 280, 80, C2Q109.getPreferredSize().height);

              //---- C2Q110 ----
              C2Q110.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q110.setName("C2Q110");
              panel2.add(C2Q110);
              C2Q110.setBounds(250, 305, 80, C2Q110.getPreferredSize().height);

              //---- C2Q111 ----
              C2Q111.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q111.setName("C2Q111");
              panel2.add(C2Q111);
              C2Q111.setBounds(250, 330, 80, C2Q111.getPreferredSize().height);

              //---- C2Q112 ----
              C2Q112.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q112.setName("C2Q112");
              panel2.add(C2Q112);
              C2Q112.setBounds(250, 355, 80, C2Q112.getPreferredSize().height);

              //---- C2Q113 ----
              C2Q113.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q113.setName("C2Q113");
              panel2.add(C2Q113);
              C2Q113.setBounds(250, 380, 80, C2Q113.getPreferredSize().height);

              //---- C2Q114 ----
              C2Q114.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q114.setName("C2Q114");
              panel2.add(C2Q114);
              C2Q114.setBounds(250, 405, 80, C2Q114.getPreferredSize().height);

              //---- C2Q115 ----
              C2Q115.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q115.setName("C2Q115");
              panel2.add(C2Q115);
              C2Q115.setBounds(250, 430, 80, C2Q115.getPreferredSize().height);

              //---- separator2 ----
              separator2.setName("separator2");
              panel2.add(separator2);
              separator2.setBounds(10, 65, 235, 15);

              //---- separator5 ----
              separator5.setName("separator5");
              panel2.add(separator5);
              separator5.setBounds(250, 65, 80, 15);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panelgen.add(panel2);
            panel2.setBounds(105, 45, 340, 470);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("P\u00e9riode n\u00b02"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_56 ----
              OBJ_56.setText("du");
              OBJ_56.setName("OBJ_56");
              panel4.add(OBJ_56);
              OBJ_56.setBounds(10, 34, 18, 20);

              //---- OBJ_58 ----
              OBJ_58.setText("au");
              OBJ_58.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_58.setName("OBJ_58");
              panel4.add(OBJ_58);
              OBJ_58.setBounds(170, 34, 53, 20);

              //---- WDEB2X ----
              WDEB2X.setName("WDEB2X");
              panel4.add(WDEB2X);
              WDEB2X.setBounds(65, 30, 105, WDEB2X.getPreferredSize().height);

              //---- WFIN2X ----
              WFIN2X.setName("WFIN2X");
              panel4.add(WFIN2X);
              WFIN2X.setBounds(225, 30, 105, WFIN2X.getPreferredSize().height);

              //---- separator3 ----
              separator3.setName("separator3");
              panel4.add(separator3);
              separator3.setBounds(10, 65, 235, 15);

              //---- C2A201 ----
              C2A201.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A201.setComponentPopupMenu(BTD);
              C2A201.setName("C2A201");
              panel4.add(C2A201);
              C2A201.setBounds(10, 80, 240, C2A201.getPreferredSize().height);

              //---- C2A202 ----
              C2A202.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A202.setComponentPopupMenu(BTD);
              C2A202.setName("C2A202");
              panel4.add(C2A202);
              C2A202.setBounds(10, 105, 240, C2A202.getPreferredSize().height);

              //---- C2A203 ----
              C2A203.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A203.setComponentPopupMenu(BTD);
              C2A203.setName("C2A203");
              panel4.add(C2A203);
              C2A203.setBounds(10, 130, 240, C2A203.getPreferredSize().height);

              //---- C2A204 ----
              C2A204.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A204.setComponentPopupMenu(BTD);
              C2A204.setName("C2A204");
              panel4.add(C2A204);
              C2A204.setBounds(10, 155, 240, C2A204.getPreferredSize().height);

              //---- C2A205 ----
              C2A205.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A205.setComponentPopupMenu(BTD);
              C2A205.setName("C2A205");
              panel4.add(C2A205);
              C2A205.setBounds(10, 180, 240, C2A205.getPreferredSize().height);

              //---- C2A206 ----
              C2A206.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A206.setComponentPopupMenu(BTD);
              C2A206.setName("C2A206");
              panel4.add(C2A206);
              C2A206.setBounds(10, 205, 240, C2A206.getPreferredSize().height);

              //---- C2A207 ----
              C2A207.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A207.setComponentPopupMenu(BTD);
              C2A207.setName("C2A207");
              panel4.add(C2A207);
              C2A207.setBounds(10, 230, 240, C2A207.getPreferredSize().height);

              //---- C2A208 ----
              C2A208.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A208.setComponentPopupMenu(BTD);
              C2A208.setName("C2A208");
              panel4.add(C2A208);
              C2A208.setBounds(10, 255, 240, C2A208.getPreferredSize().height);

              //---- C2A209 ----
              C2A209.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A209.setComponentPopupMenu(BTD);
              C2A209.setName("C2A209");
              panel4.add(C2A209);
              C2A209.setBounds(10, 280, 240, C2A209.getPreferredSize().height);

              //---- C2A210 ----
              C2A210.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A210.setComponentPopupMenu(BTD);
              C2A210.setName("C2A210");
              panel4.add(C2A210);
              C2A210.setBounds(10, 305, 240, C2A210.getPreferredSize().height);

              //---- C2A211 ----
              C2A211.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A211.setComponentPopupMenu(BTD);
              C2A211.setName("C2A211");
              panel4.add(C2A211);
              C2A211.setBounds(10, 330, 240, C2A211.getPreferredSize().height);

              //---- C2A212 ----
              C2A212.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A212.setComponentPopupMenu(BTD);
              C2A212.setName("C2A212");
              panel4.add(C2A212);
              C2A212.setBounds(10, 355, 240, C2A212.getPreferredSize().height);

              //---- C2A213 ----
              C2A213.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A213.setComponentPopupMenu(BTD);
              C2A213.setName("C2A213");
              panel4.add(C2A213);
              C2A213.setBounds(10, 380, 240, C2A213.getPreferredSize().height);

              //---- C2A214 ----
              C2A214.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A214.setComponentPopupMenu(BTD);
              C2A214.setName("C2A214");
              panel4.add(C2A214);
              C2A214.setBounds(10, 405, 240, C2A214.getPreferredSize().height);

              //---- C2A215 ----
              C2A215.setHorizontalAlignment(SwingConstants.RIGHT);
              C2A215.setComponentPopupMenu(BTD);
              C2A215.setName("C2A215");
              panel4.add(C2A215);
              C2A215.setBounds(10, 430, 240, C2A215.getPreferredSize().height);

              //---- C2Q201 ----
              C2Q201.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q201.setName("C2Q201");
              panel4.add(C2Q201);
              C2Q201.setBounds(250, 80, 80, C2Q201.getPreferredSize().height);

              //---- C2Q202 ----
              C2Q202.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q202.setName("C2Q202");
              panel4.add(C2Q202);
              C2Q202.setBounds(250, 105, 80, C2Q202.getPreferredSize().height);

              //---- C2Q203 ----
              C2Q203.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q203.setName("C2Q203");
              panel4.add(C2Q203);
              C2Q203.setBounds(250, 130, 80, C2Q203.getPreferredSize().height);

              //---- C2Q204 ----
              C2Q204.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q204.setName("C2Q204");
              panel4.add(C2Q204);
              C2Q204.setBounds(250, 155, 80, C2Q204.getPreferredSize().height);

              //---- C2Q205 ----
              C2Q205.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q205.setName("C2Q205");
              panel4.add(C2Q205);
              C2Q205.setBounds(250, 180, 80, C2Q205.getPreferredSize().height);

              //---- C2Q206 ----
              C2Q206.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q206.setName("C2Q206");
              panel4.add(C2Q206);
              C2Q206.setBounds(250, 205, 80, C2Q206.getPreferredSize().height);

              //---- C2Q207 ----
              C2Q207.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q207.setName("C2Q207");
              panel4.add(C2Q207);
              C2Q207.setBounds(250, 230, 80, C2Q207.getPreferredSize().height);

              //---- C2Q208 ----
              C2Q208.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q208.setName("C2Q208");
              panel4.add(C2Q208);
              C2Q208.setBounds(250, 255, 80, C2Q208.getPreferredSize().height);

              //---- C2Q209 ----
              C2Q209.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q209.setName("C2Q209");
              panel4.add(C2Q209);
              C2Q209.setBounds(250, 280, 80, C2Q209.getPreferredSize().height);

              //---- C2Q210 ----
              C2Q210.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q210.setName("C2Q210");
              panel4.add(C2Q210);
              C2Q210.setBounds(250, 305, 80, C2Q210.getPreferredSize().height);

              //---- C2Q211 ----
              C2Q211.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q211.setName("C2Q211");
              panel4.add(C2Q211);
              C2Q211.setBounds(250, 330, 80, C2Q211.getPreferredSize().height);

              //---- C2Q212 ----
              C2Q212.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q212.setName("C2Q212");
              panel4.add(C2Q212);
              C2Q212.setBounds(250, 355, 80, C2Q212.getPreferredSize().height);

              //---- C2Q213 ----
              C2Q213.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q213.setName("C2Q213");
              panel4.add(C2Q213);
              C2Q213.setBounds(250, 380, 80, C2Q213.getPreferredSize().height);

              //---- C2Q214 ----
              C2Q214.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q214.setName("C2Q214");
              panel4.add(C2Q214);
              C2Q214.setBounds(250, 405, 80, C2Q214.getPreferredSize().height);

              //---- C2Q215 ----
              C2Q215.setHorizontalAlignment(SwingConstants.RIGHT);
              C2Q215.setName("C2Q215");
              panel4.add(C2Q215);
              C2Q215.setBounds(250, 430, 80, C2Q215.getPreferredSize().height);

              //---- separator6 ----
              separator6.setName("separator6");
              panel4.add(separator6);
              separator6.setBounds(250, 65, 80, 15);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panelgen.add(panel4);
            panel4.setBounds(455, 45, 340, 470);

            //---- Q101 ----
            Q101.setHorizontalAlignment(SwingConstants.RIGHT);
            Q101.setName("Q101");
            panelgen.add(Q101);
            Q101.setBounds(15, 125, 80, Q101.getPreferredSize().height);

            //---- Q102 ----
            Q102.setHorizontalAlignment(SwingConstants.RIGHT);
            Q102.setName("Q102");
            panelgen.add(Q102);
            Q102.setBounds(15, 150, 80, Q102.getPreferredSize().height);

            //---- Q103 ----
            Q103.setHorizontalAlignment(SwingConstants.RIGHT);
            Q103.setName("Q103");
            panelgen.add(Q103);
            Q103.setBounds(15, 175, 80, Q103.getPreferredSize().height);

            //---- Q104 ----
            Q104.setHorizontalAlignment(SwingConstants.RIGHT);
            Q104.setName("Q104");
            panelgen.add(Q104);
            Q104.setBounds(15, 200, 80, Q104.getPreferredSize().height);

            //---- Q105 ----
            Q105.setHorizontalAlignment(SwingConstants.RIGHT);
            Q105.setName("Q105");
            panelgen.add(Q105);
            Q105.setBounds(15, 225, 80, Q105.getPreferredSize().height);

            //---- Q106 ----
            Q106.setHorizontalAlignment(SwingConstants.RIGHT);
            Q106.setName("Q106");
            panelgen.add(Q106);
            Q106.setBounds(15, 250, 80, Q106.getPreferredSize().height);

            //---- Q107 ----
            Q107.setHorizontalAlignment(SwingConstants.RIGHT);
            Q107.setName("Q107");
            panelgen.add(Q107);
            Q107.setBounds(15, 275, 80, Q107.getPreferredSize().height);

            //---- Q108 ----
            Q108.setHorizontalAlignment(SwingConstants.RIGHT);
            Q108.setName("Q108");
            panelgen.add(Q108);
            Q108.setBounds(15, 300, 80, Q108.getPreferredSize().height);

            //---- Q109 ----
            Q109.setHorizontalAlignment(SwingConstants.RIGHT);
            Q109.setName("Q109");
            panelgen.add(Q109);
            Q109.setBounds(15, 325, 80, Q109.getPreferredSize().height);

            //---- Q110 ----
            Q110.setHorizontalAlignment(SwingConstants.RIGHT);
            Q110.setName("Q110");
            panelgen.add(Q110);
            Q110.setBounds(15, 350, 80, Q110.getPreferredSize().height);

            //---- Q111 ----
            Q111.setHorizontalAlignment(SwingConstants.RIGHT);
            Q111.setName("Q111");
            panelgen.add(Q111);
            Q111.setBounds(15, 375, 80, Q111.getPreferredSize().height);

            //---- Q112 ----
            Q112.setHorizontalAlignment(SwingConstants.RIGHT);
            Q112.setName("Q112");
            panelgen.add(Q112);
            Q112.setBounds(15, 400, 80, Q112.getPreferredSize().height);

            //---- Q113 ----
            Q113.setHorizontalAlignment(SwingConstants.RIGHT);
            Q113.setName("Q113");
            panelgen.add(Q113);
            Q113.setBounds(15, 425, 80, Q113.getPreferredSize().height);

            //---- Q114 ----
            Q114.setHorizontalAlignment(SwingConstants.RIGHT);
            Q114.setName("Q114");
            panelgen.add(Q114);
            Q114.setBounds(15, 450, 80, Q114.getPreferredSize().height);

            //---- Q115 ----
            Q115.setHorizontalAlignment(SwingConstants.RIGHT);
            Q115.setName("Q115");
            panelgen.add(Q115);
            Q115.setBounds(15, 475, 80, Q115.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panelgen.add(separator1);
            separator1.setBounds(15, 110, 80, separator1.getPreferredSize().height);

            //---- ERR1 ----
            ERR1.setText("@ERR1@");
            ERR1.setHorizontalAlignment(SwingConstants.CENTER);
            ERR1.setForeground(new Color(153, 0, 0));
            ERR1.setFont(ERR1.getFont().deriveFont(ERR1.getFont().getStyle() | Font.BOLD));
            ERR1.setName("ERR1");
            panelgen.add(ERR1);
            ERR1.setBounds(845, 125, 50, 28);

            //---- ERR2 ----
            ERR2.setText("@ERR2@");
            ERR2.setHorizontalAlignment(SwingConstants.CENTER);
            ERR2.setForeground(new Color(153, 0, 0));
            ERR2.setFont(ERR2.getFont().deriveFont(ERR2.getFont().getStyle() | Font.BOLD));
            ERR2.setName("ERR2");
            panelgen.add(ERR2);
            ERR2.setBounds(845, 150, 50, 28);

            //---- ERR3 ----
            ERR3.setText("@ERR3@");
            ERR3.setHorizontalAlignment(SwingConstants.CENTER);
            ERR3.setForeground(new Color(153, 0, 0));
            ERR3.setFont(ERR3.getFont().deriveFont(ERR3.getFont().getStyle() | Font.BOLD));
            ERR3.setName("ERR3");
            panelgen.add(ERR3);
            ERR3.setBounds(845, 175, 50, 28);

            //---- ERR4 ----
            ERR4.setText("@ERR4@");
            ERR4.setHorizontalAlignment(SwingConstants.CENTER);
            ERR4.setForeground(new Color(153, 0, 0));
            ERR4.setFont(ERR4.getFont().deriveFont(ERR4.getFont().getStyle() | Font.BOLD));
            ERR4.setName("ERR4");
            panelgen.add(ERR4);
            ERR4.setBounds(845, 200, 50, 28);

            //---- ERR5 ----
            ERR5.setText("@ERR5@");
            ERR5.setHorizontalAlignment(SwingConstants.CENTER);
            ERR5.setForeground(new Color(153, 0, 0));
            ERR5.setFont(ERR5.getFont().deriveFont(ERR5.getFont().getStyle() | Font.BOLD));
            ERR5.setName("ERR5");
            panelgen.add(ERR5);
            ERR5.setBounds(845, 225, 50, 28);

            //---- ERR6 ----
            ERR6.setText("@ERR6@");
            ERR6.setHorizontalAlignment(SwingConstants.CENTER);
            ERR6.setForeground(new Color(153, 0, 0));
            ERR6.setFont(ERR6.getFont().deriveFont(ERR6.getFont().getStyle() | Font.BOLD));
            ERR6.setName("ERR6");
            panelgen.add(ERR6);
            ERR6.setBounds(845, 250, 50, 28);

            //---- ERR7 ----
            ERR7.setText("@ERR7@");
            ERR7.setHorizontalAlignment(SwingConstants.CENTER);
            ERR7.setForeground(new Color(153, 0, 0));
            ERR7.setFont(ERR7.getFont().deriveFont(ERR7.getFont().getStyle() | Font.BOLD));
            ERR7.setName("ERR7");
            panelgen.add(ERR7);
            ERR7.setBounds(845, 275, 50, 28);

            //---- ERR8 ----
            ERR8.setText("@ERR8@");
            ERR8.setHorizontalAlignment(SwingConstants.CENTER);
            ERR8.setForeground(new Color(153, 0, 0));
            ERR8.setFont(ERR8.getFont().deriveFont(ERR8.getFont().getStyle() | Font.BOLD));
            ERR8.setName("ERR8");
            panelgen.add(ERR8);
            ERR8.setBounds(845, 300, 50, 28);

            //---- ERR9 ----
            ERR9.setText("@ERR9@");
            ERR9.setHorizontalAlignment(SwingConstants.CENTER);
            ERR9.setForeground(new Color(153, 0, 0));
            ERR9.setFont(ERR9.getFont().deriveFont(ERR9.getFont().getStyle() | Font.BOLD));
            ERR9.setName("ERR9");
            panelgen.add(ERR9);
            ERR9.setBounds(845, 325, 50, 28);

            //---- ERR10 ----
            ERR10.setText("@ERR10@");
            ERR10.setHorizontalAlignment(SwingConstants.CENTER);
            ERR10.setForeground(new Color(153, 0, 0));
            ERR10.setFont(ERR10.getFont().deriveFont(ERR10.getFont().getStyle() | Font.BOLD));
            ERR10.setName("ERR10");
            panelgen.add(ERR10);
            ERR10.setBounds(845, 350, 50, 28);

            //---- ERR11 ----
            ERR11.setText("@ERR11@");
            ERR11.setHorizontalAlignment(SwingConstants.CENTER);
            ERR11.setForeground(new Color(153, 0, 0));
            ERR11.setFont(ERR11.getFont().deriveFont(ERR11.getFont().getStyle() | Font.BOLD));
            ERR11.setName("ERR11");
            panelgen.add(ERR11);
            ERR11.setBounds(845, 375, 50, 28);

            //---- ERR12 ----
            ERR12.setText("@ERR12@");
            ERR12.setHorizontalAlignment(SwingConstants.CENTER);
            ERR12.setForeground(new Color(153, 0, 0));
            ERR12.setFont(ERR12.getFont().deriveFont(ERR12.getFont().getStyle() | Font.BOLD));
            ERR12.setName("ERR12");
            panelgen.add(ERR12);
            ERR12.setBounds(845, 400, 50, 28);

            //---- ERR13 ----
            ERR13.setText("@ERR13@");
            ERR13.setHorizontalAlignment(SwingConstants.CENTER);
            ERR13.setForeground(new Color(153, 0, 0));
            ERR13.setFont(ERR13.getFont().deriveFont(ERR13.getFont().getStyle() | Font.BOLD));
            ERR13.setName("ERR13");
            panelgen.add(ERR13);
            ERR13.setBounds(845, 425, 50, 28);

            //---- ERR14 ----
            ERR14.setText("@ERR14@");
            ERR14.setHorizontalAlignment(SwingConstants.CENTER);
            ERR14.setForeground(new Color(153, 0, 0));
            ERR14.setFont(ERR14.getFont().deriveFont(ERR14.getFont().getStyle() | Font.BOLD));
            ERR14.setName("ERR14");
            panelgen.add(ERR14);
            ERR14.setBounds(845, 450, 50, 28);

            //---- ERR15 ----
            ERR15.setText("@ERR15@");
            ERR15.setHorizontalAlignment(SwingConstants.CENTER);
            ERR15.setForeground(new Color(153, 0, 0));
            ERR15.setFont(ERR15.getFont().deriveFont(ERR15.getFont().getStyle() | Font.BOLD));
            ERR15.setName("ERR15");
            panelgen.add(ERR15);
            ERR15.setBounds(845, 475, 50, 28);

            //---- C2G01 ----
            C2G01.setComponentPopupMenu(BTD);
            C2G01.setName("C2G01");
            panelgen.add(C2G01);
            C2G01.setBounds(810, 125, 24, C2G01.getPreferredSize().height);

            //---- C2G2 ----
            C2G2.setComponentPopupMenu(BTD);
            C2G2.setName("C2G2");
            panelgen.add(C2G2);
            C2G2.setBounds(810, 150, 24, C2G2.getPreferredSize().height);

            //---- C2G3 ----
            C2G3.setComponentPopupMenu(BTD);
            C2G3.setName("C2G3");
            panelgen.add(C2G3);
            C2G3.setBounds(810, 175, 24, C2G3.getPreferredSize().height);

            //---- C2G4 ----
            C2G4.setComponentPopupMenu(BTD);
            C2G4.setName("C2G4");
            panelgen.add(C2G4);
            C2G4.setBounds(810, 200, 24, C2G4.getPreferredSize().height);

            //---- C2G5 ----
            C2G5.setComponentPopupMenu(BTD);
            C2G5.setName("C2G5");
            panelgen.add(C2G5);
            C2G5.setBounds(810, 225, 24, C2G5.getPreferredSize().height);

            //---- C2G6 ----
            C2G6.setComponentPopupMenu(BTD);
            C2G6.setName("C2G6");
            panelgen.add(C2G6);
            C2G6.setBounds(810, 250, 24, C2G6.getPreferredSize().height);

            //---- C2G7 ----
            C2G7.setComponentPopupMenu(BTD);
            C2G7.setName("C2G7");
            panelgen.add(C2G7);
            C2G7.setBounds(810, 275, 24, C2G7.getPreferredSize().height);

            //---- C2G8 ----
            C2G8.setComponentPopupMenu(BTD);
            C2G8.setName("C2G8");
            panelgen.add(C2G8);
            C2G8.setBounds(810, 300, 24, C2G8.getPreferredSize().height);

            //---- C2G9 ----
            C2G9.setComponentPopupMenu(BTD);
            C2G9.setName("C2G9");
            panelgen.add(C2G9);
            C2G9.setBounds(810, 325, 24, C2G9.getPreferredSize().height);

            //---- C2G10 ----
            C2G10.setComponentPopupMenu(BTD);
            C2G10.setName("C2G10");
            panelgen.add(C2G10);
            C2G10.setBounds(810, 350, 24, C2G10.getPreferredSize().height);

            //---- C2G11 ----
            C2G11.setComponentPopupMenu(BTD);
            C2G11.setName("C2G11");
            panelgen.add(C2G11);
            C2G11.setBounds(810, 375, 24, C2G11.getPreferredSize().height);

            //---- C2G12 ----
            C2G12.setComponentPopupMenu(BTD);
            C2G12.setName("C2G12");
            panelgen.add(C2G12);
            C2G12.setBounds(810, 400, 24, C2G12.getPreferredSize().height);

            //---- C2G13 ----
            C2G13.setComponentPopupMenu(BTD);
            C2G13.setName("C2G13");
            panelgen.add(C2G13);
            C2G13.setBounds(810, 425, 24, C2G13.getPreferredSize().height);

            //---- C2G14 ----
            C2G14.setComponentPopupMenu(BTD);
            C2G14.setName("C2G14");
            panelgen.add(C2G14);
            C2G14.setBounds(810, 450, 24, C2G14.getPreferredSize().height);

            //---- C2G15 ----
            C2G15.setComponentPopupMenu(BTD);
            C2G15.setName("C2G15");
            panelgen.add(C2G15);
            C2G15.setBounds(810, 475, 24, C2G15.getPreferredSize().height);

            //---- separator4 ----
            separator4.setName("separator4");
            panelgen.add(separator4);
            separator4.setBounds(800, 110, 45, separator4.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panelgen.getComponentCount(); i++) {
                Rectangle bounds = panelgen.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panelgen.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panelgen.setMinimumSize(preferredSize);
              panelgen.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panelgen, GroupLayout.PREFERRED_SIZE, 910, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelgen, GroupLayout.PREFERRED_SIZE, 539, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private XRiTextField WETB;
  private SNBoutonRecherche BT_ChgSoc;
  private JLabel OBJ_50;
  private XRiTextField WCNV;
  private XRiTextField BRLIB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panelgen;
  private JPanel panel2;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private XRiCalendrier WDEB1X;
  private XRiCalendrier WFIN1X;
  private XRiTextField C2A101;
  private XRiTextField C2A102;
  private XRiTextField C2A103;
  private XRiTextField C2A104;
  private XRiTextField C2A105;
  private XRiTextField C2A106;
  private XRiTextField C2A107;
  private XRiTextField C2A108;
  private XRiTextField C2A109;
  private XRiTextField C2A110;
  private XRiTextField C2A111;
  private XRiTextField C2A112;
  private XRiTextField C2A113;
  private XRiTextField C2A114;
  private XRiTextField C2A115;
  private XRiTextField C2Q101;
  private XRiTextField C2Q102;
  private XRiTextField C2Q103;
  private XRiTextField C2Q104;
  private XRiTextField C2Q105;
  private XRiTextField C2Q106;
  private XRiTextField C2Q107;
  private XRiTextField C2Q108;
  private XRiTextField C2Q109;
  private XRiTextField C2Q110;
  private XRiTextField C2Q111;
  private XRiTextField C2Q112;
  private XRiTextField C2Q113;
  private XRiTextField C2Q114;
  private XRiTextField C2Q115;
  private JComponent separator2;
  private JComponent separator5;
  private JPanel panel4;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private XRiCalendrier WDEB2X;
  private XRiCalendrier WFIN2X;
  private JComponent separator3;
  private XRiTextField C2A201;
  private XRiTextField C2A202;
  private XRiTextField C2A203;
  private XRiTextField C2A204;
  private XRiTextField C2A205;
  private XRiTextField C2A206;
  private XRiTextField C2A207;
  private XRiTextField C2A208;
  private XRiTextField C2A209;
  private XRiTextField C2A210;
  private XRiTextField C2A211;
  private XRiTextField C2A212;
  private XRiTextField C2A213;
  private XRiTextField C2A214;
  private XRiTextField C2A215;
  private XRiTextField C2Q201;
  private XRiTextField C2Q202;
  private XRiTextField C2Q203;
  private XRiTextField C2Q204;
  private XRiTextField C2Q205;
  private XRiTextField C2Q206;
  private XRiTextField C2Q207;
  private XRiTextField C2Q208;
  private XRiTextField C2Q209;
  private XRiTextField C2Q210;
  private XRiTextField C2Q211;
  private XRiTextField C2Q212;
  private XRiTextField C2Q213;
  private XRiTextField C2Q214;
  private XRiTextField C2Q215;
  private JComponent separator6;
  private XRiTextField Q101;
  private XRiTextField Q102;
  private XRiTextField Q103;
  private XRiTextField Q104;
  private XRiTextField Q105;
  private XRiTextField Q106;
  private XRiTextField Q107;
  private XRiTextField Q108;
  private XRiTextField Q109;
  private XRiTextField Q110;
  private XRiTextField Q111;
  private XRiTextField Q112;
  private XRiTextField Q113;
  private XRiTextField Q114;
  private XRiTextField Q115;
  private JComponent separator1;
  private JLabel ERR1;
  private JLabel ERR2;
  private JLabel ERR3;
  private JLabel ERR4;
  private JLabel ERR5;
  private JLabel ERR6;
  private JLabel ERR7;
  private JLabel ERR8;
  private JLabel ERR9;
  private JLabel ERR10;
  private JLabel ERR11;
  private JLabel ERR12;
  private JLabel ERR13;
  private JLabel ERR14;
  private JLabel ERR15;
  private XRiTextField C2G01;
  private XRiTextField C2G2;
  private XRiTextField C2G3;
  private XRiTextField C2G4;
  private XRiTextField C2G5;
  private XRiTextField C2G6;
  private XRiTextField C2G7;
  private XRiTextField C2G8;
  private XRiTextField C2G9;
  private XRiTextField C2G10;
  private XRiTextField C2G11;
  private XRiTextField C2G12;
  private XRiTextField C2G13;
  private XRiTextField C2G14;
  private XRiTextField C2G15;
  private JComponent separator4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
