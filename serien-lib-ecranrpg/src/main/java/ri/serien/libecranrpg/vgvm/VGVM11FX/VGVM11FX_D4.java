
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_D4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_D4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    setCloseKey("F23");
    
    // Titre
    setTitle("Bons de commande");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    OBJ_55.setIcon(lexique.chargerImage("images/suivant1.gif", true));
    l_plus.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_plus2.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_plus3.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_plus4.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    l_egal.setIcon(lexique.chargerImage("images/egal_petit.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1RDMX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RDMX@")).trim());
    E1MPAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MPAX@")).trim());
    E1TTC_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENCO@")).trim());
    E1CLC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLC@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
    WEPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS@")).trim());
    WEPLF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF@")).trim());
    WEPLF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF2@")).trim());
    WEPCO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPCO@")).trim());
    WEFAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEFAC@")).trim());
    WEEXP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEEXP@")).trim());
    WECDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WECDE@")).trim());
    WEVAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEVAR@")).trim());
    LRGA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGA@")).trim());
    LRGA.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGA@")).trim());
    LRGA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGB@")).trim());
    LRGA2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGB@")).trim());
    LRGA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGC@")).trim());
    LRGA3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGC@")).trim());
    LRGA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGD@")).trim());
    LRGA4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRGD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_55.setVisible(lexique.isPresent("WEPLF"));
    OBJ_34.setVisible(lexique.isPresent("WEDEP"));
    OBJ_42.setVisible(lexique.isPresent("WEPLF"));
    OBJ_32.setVisible(lexique.isPresent("CLNOM"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel5 = new JPanel();
    E1RDMX = new RiZoneSortie();
    E1MPAX = new RiZoneSortie();
    OBJ_20 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_22 = new JLabel();
    E1TTC_1 = new RiZoneSortie();
    panel6 = new JPanel();
    OBJ_26 = new JLabel();
    E1IN1 = new XRiTextField();
    panel7 = new JPanel();
    OBJ_30 = new JLabel();
    E1CLC = new RiZoneSortie();
    OBJ_32 = new RiZoneSortie();
    OBJ_34 = new JLabel();
    WEDEP = new RiZoneSortie();
    WEPOS = new RiZoneSortie();
    OBJ_41 = new JLabel();
    WEPLF = new RiZoneSortie();
    OBJ_42 = new JLabel();
    OBJ_55 = new JLabel();
    WEPLF2 = new RiZoneSortie();
    l_egal = new JLabel();
    panel2 = new JPanel();
    OBJ_39 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_40 = new JLabel();
    WEPCO = new RiZoneSortie();
    WEFAC = new RiZoneSortie();
    WEEXP = new RiZoneSortie();
    WECDE = new RiZoneSortie();
    WEVAR = new RiZoneSortie();
    OBJ_38 = new JLabel();
    OBJ_37 = new JLabel();
    l_plus = new JLabel();
    l_plus2 = new JLabel();
    l_plus3 = new JLabel();
    l_plus4 = new JLabel();
    E1RG1 = new XRiTextField();
    E1RG2 = new XRiTextField();
    E1RG3 = new XRiTextField();
    E1RG4 = new XRiTextField();
    BQE1 = new XRiTextField();
    BQE2 = new XRiTextField();
    BQE3 = new XRiTextField();
    BQE4 = new XRiTextField();
    CHQ1 = new XRiTextField();
    CHQ2 = new XRiTextField();
    CHQ3 = new XRiTextField();
    CHQ4 = new XRiTextField();
    HPL1 = new XRiTextField();
    HPL2 = new XRiTextField();
    HPL3 = new XRiTextField();
    HPL4 = new XRiTextField();
    E1DTE1 = new XRiTextField();
    E1DTE2 = new XRiTextField();
    E1DTE3 = new XRiTextField();
    E1DTE4 = new XRiTextField();
    E1MRG1 = new XRiTextField();
    E1MRG2 = new XRiTextField();
    E1MRG3 = new XRiTextField();
    E1MRG4 = new XRiTextField();
    E1EC1 = new XRiTextField();
    E1EC2 = new XRiTextField();
    E1EC3 = new XRiTextField();
    E1EC4 = new XRiTextField();
    E1PC1 = new XRiTextField();
    E1PC2 = new XRiTextField();
    E1PC3 = new XRiTextField();
    E1PC4 = new XRiTextField();
    LRGA = new RiZoneSortie();
    LRGA2 = new RiZoneSortie();
    LRGA3 = new RiZoneSortie();
    LRGA4 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(910, 590));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Paiement effectif");
              riSousMenu_bt6.setToolTipText("Paiement effectif");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Gestion des r\u00e8glements");
              riSousMenu_bt1.setToolTipText("Gestion des r\u00e8glements");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("R\u00e9glement"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- E1RDMX ----
          E1RDMX.setComponentPopupMenu(BTD);
          E1RDMX.setText("@E1RDMX@");
          E1RDMX.setHorizontalAlignment(SwingConstants.RIGHT);
          E1RDMX.setName("E1RDMX");
          panel5.add(E1RDMX);
          E1RDMX.setBounds(520, 42, 90, E1RDMX.getPreferredSize().height);

          //---- E1MPAX ----
          E1MPAX.setComponentPopupMenu(BTD);
          E1MPAX.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MPAX.setText("@E1MPAX@");
          E1MPAX.setName("E1MPAX");
          panel5.add(E1MPAX);
          E1MPAX.setBounds(340, 42, 90, E1MPAX.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Total \u00e0 R\u00e8gler");
          OBJ_20.setFont(OBJ_20.getFont().deriveFont(OBJ_20.getFont().getStyle() | Font.BOLD));
          OBJ_20.setName("OBJ_20");
          panel5.add(OBJ_20);
          OBJ_20.setBounds(30, 44, 90, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("Montant pay\u00e9");
          OBJ_21.setName("OBJ_21");
          panel5.add(OBJ_21);
          OBJ_21.setBounds(255, 44, 85, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Rendu");
          OBJ_22.setName("OBJ_22");
          panel5.add(OBJ_22);
          OBJ_22.setBounds(470, 44, 45, 20);

          //---- E1TTC_1 ----
          E1TTC_1.setText("@E1TTC@");
          E1TTC_1.setHorizontalAlignment(SwingConstants.RIGHT);
          E1TTC_1.setFont(E1TTC_1.getFont().deriveFont(E1TTC_1.getFont().getStyle() | Font.BOLD));
          E1TTC_1.setName("E1TTC_1");
          panel5.add(E1TTC_1);
          E1TTC_1.setBounds(new Rectangle(new Point(120, 42), E1TTC_1.getPreferredSize()));
        }
        p_contenu.add(panel5);
        panel5.setBounds(15, 5, 710, 95);

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Ventilation r\u00e9glements"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- OBJ_26 ----
          OBJ_26.setText("Niveau de blocage traitement");
          OBJ_26.setName("OBJ_26");
          panel6.add(OBJ_26);
          OBJ_26.setBounds(460, 174, 180, 20);

          //---- E1IN1 ----
          E1IN1.setComponentPopupMenu(BTD);
          E1IN1.setName("E1IN1");
          panel6.add(E1IN1);
          E1IN1.setBounds(665, 170, 20, E1IN1.getPreferredSize().height);

          //======== panel7 ========
          {
            panel7.setBorder(new TitledBorder("Encours client"));
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- OBJ_30 ----
            OBJ_30.setText("@ENCO@");
            OBJ_30.setName("OBJ_30");
            panel7.add(OBJ_30);
            OBJ_30.setBounds(125, 0, 117, 20);

            //---- E1CLC ----
            E1CLC.setText("@E1CLC@");
            E1CLC.setName("E1CLC");
            panel7.add(E1CLC);
            E1CLC.setBounds(25, 40, 58, E1CLC.getPreferredSize().height);

            //---- OBJ_32 ----
            OBJ_32.setText("@CLNOM@");
            OBJ_32.setName("OBJ_32");
            panel7.add(OBJ_32);
            OBJ_32.setBounds(90, 40, 310, OBJ_32.getPreferredSize().height);

            //---- OBJ_34 ----
            OBJ_34.setText("D\u00e9passement");
            OBJ_34.setFont(new Font("sansserif", Font.BOLD, 12));
            OBJ_34.setName("OBJ_34");
            panel7.add(OBJ_34);
            OBJ_34.setBounds(425, 42, 95, 20);

            //---- WEDEP ----
            WEDEP.setComponentPopupMenu(BTD);
            WEDEP.setText("@WEDEP@");
            WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
            WEDEP.setName("WEDEP");
            panel7.add(WEDEP);
            WEDEP.setBounds(520, 40, 82, WEDEP.getPreferredSize().height);

            //---- WEPOS ----
            WEPOS.setComponentPopupMenu(BTD);
            WEPOS.setText("@WEPOS@");
            WEPOS.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPOS.setName("WEPOS");
            panel7.add(WEPOS);
            WEPOS.setBounds(520, 110, 82, WEPOS.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("Position");
            OBJ_41.setFont(new Font("sansserif", Font.BOLD, 12));
            OBJ_41.setName("OBJ_41");
            panel7.add(OBJ_41);
            OBJ_41.setBounds(520, 85, 58, 20);

            //---- WEPLF ----
            WEPLF.setComponentPopupMenu(BTD);
            WEPLF.setText("@WEPLF@");
            WEPLF.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPLF.setName("WEPLF");
            panel7.add(WEPLF);
            WEPLF.setBounds(520, 160, 82, WEPLF.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Plafond");
            OBJ_42.setFont(new Font("sansserif", Font.BOLD, 12));
            OBJ_42.setName("OBJ_42");
            panel7.add(OBJ_42);
            OBJ_42.setBounds(520, 140, 67, 20);

            //---- OBJ_55 ----
            OBJ_55.setName("OBJ_55");
            panel7.add(OBJ_55);
            OBJ_55.setBounds(485, 165, 25, 29);

            //---- WEPLF2 ----
            WEPLF2.setComponentPopupMenu(BTD);
            WEPLF2.setText("@WEPLF2@");
            WEPLF2.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPLF2.setName("WEPLF2");
            panel7.add(WEPLF2);
            WEPLF2.setBounds(520, 190, 82, WEPLF2.getPreferredSize().height);

            //---- l_egal ----
            l_egal.setName("l_egal");
            panel7.add(l_egal);
            l_egal.setBounds(489, 116, 16, 16);

            //======== panel2 ========
            {
              panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_39 ----
              OBJ_39.setText("Command\u00e9");
              OBJ_39.setName("OBJ_39");
              panel2.add(OBJ_39);
              OBJ_39.setBounds(305, 10, 73, 20);

              //---- OBJ_36 ----
              OBJ_36.setText("Compta");
              OBJ_36.setToolTipText("(Compta = Position Comptable y compris les Effets non \u00e9chus)");
              OBJ_36.setName("OBJ_36");
              panel2.add(OBJ_36);
              OBJ_36.setBounds(10, 10, 69, 20);

              //---- OBJ_40 ----
              OBJ_40.setText("Var./Bon");
              OBJ_40.setName("OBJ_40");
              panel2.add(OBJ_40);
              OBJ_40.setBounds(395, 10, 63, 20);

              //---- WEPCO ----
              WEPCO.setToolTipText("(Compta = Position Comptable y compris les Effets non \u00e9chus)");
              WEPCO.setComponentPopupMenu(BTD);
              WEPCO.setText("@WEPCO@");
              WEPCO.setHorizontalAlignment(SwingConstants.RIGHT);
              WEPCO.setName("WEPCO");
              panel2.add(WEPCO);
              WEPCO.setBounds(10, 35, 74, WEPCO.getPreferredSize().height);

              //---- WEFAC ----
              WEFAC.setComponentPopupMenu(BTD);
              WEFAC.setText("@WEFAC@");
              WEFAC.setHorizontalAlignment(SwingConstants.RIGHT);
              WEFAC.setName("WEFAC");
              panel2.add(WEFAC);
              WEFAC.setBounds(120, 35, 60, WEFAC.getPreferredSize().height);

              //---- WEEXP ----
              WEEXP.setComponentPopupMenu(BTD);
              WEEXP.setText("@WEEXP@");
              WEEXP.setHorizontalAlignment(SwingConstants.RIGHT);
              WEEXP.setName("WEEXP");
              panel2.add(WEEXP);
              WEEXP.setBounds(210, 35, 60, WEEXP.getPreferredSize().height);

              //---- WECDE ----
              WECDE.setComponentPopupMenu(BTD);
              WECDE.setText("@WECDE@");
              WECDE.setHorizontalAlignment(SwingConstants.RIGHT);
              WECDE.setName("WECDE");
              panel2.add(WECDE);
              WECDE.setBounds(305, 35, 60, WECDE.getPreferredSize().height);

              //---- WEVAR ----
              WEVAR.setComponentPopupMenu(BTD);
              WEVAR.setText("@WEVAR@");
              WEVAR.setHorizontalAlignment(SwingConstants.RIGHT);
              WEVAR.setName("WEVAR");
              panel2.add(WEVAR);
              WEVAR.setBounds(395, 35, 60, WEVAR.getPreferredSize().height);

              //---- OBJ_38 ----
              OBJ_38.setText("Exp\u00e9di\u00e9");
              OBJ_38.setName("OBJ_38");
              panel2.add(OBJ_38);
              OBJ_38.setBounds(210, 10, 54, 20);

              //---- OBJ_37 ----
              OBJ_37.setText("Factur\u00e9");
              OBJ_37.setName("OBJ_37");
              panel2.add(OBJ_37);
              OBJ_37.setBounds(120, 10, 52, 20);

              //---- l_plus ----
              l_plus.setName("l_plus");
              panel2.add(l_plus);
              l_plus.setBounds(95, 40, 16, 16);

              //---- l_plus2 ----
              l_plus2.setName("l_plus2");
              panel2.add(l_plus2);
              l_plus2.setBounds(185, 40, 16, 16);

              //---- l_plus3 ----
              l_plus3.setName("l_plus3");
              panel2.add(l_plus3);
              l_plus3.setBounds(280, 40, 16, 16);

              //---- l_plus4 ----
              l_plus4.setName("l_plus4");
              panel2.add(l_plus4);
              l_plus4.setBounds(370, 40, 16, 16);
            }
            panel7.add(panel2);
            panel2.setBounds(20, 75, 465, 75);
          }
          panel6.add(panel7);
          panel7.setBounds(15, 220, 680, 235);

          //---- E1RG1 ----
          E1RG1.setName("E1RG1");
          panel6.add(E1RG1);
          E1RG1.setBounds(25, 60, 30, E1RG1.getPreferredSize().height);

          //---- E1RG2 ----
          E1RG2.setName("E1RG2");
          panel6.add(E1RG2);
          E1RG2.setBounds(25, 85, 30, 28);

          //---- E1RG3 ----
          E1RG3.setName("E1RG3");
          panel6.add(E1RG3);
          E1RG3.setBounds(25, 110, 30, 28);

          //---- E1RG4 ----
          E1RG4.setName("E1RG4");
          panel6.add(E1RG4);
          E1RG4.setBounds(25, 135, 30, 28);

          //---- BQE1 ----
          BQE1.setName("BQE1");
          panel6.add(BQE1);
          BQE1.setBounds(55, 60, 80, BQE1.getPreferredSize().height);

          //---- BQE2 ----
          BQE2.setName("BQE2");
          panel6.add(BQE2);
          BQE2.setBounds(55, 85, 80, 28);

          //---- BQE3 ----
          BQE3.setName("BQE3");
          panel6.add(BQE3);
          BQE3.setBounds(55, 110, 80, 28);

          //---- BQE4 ----
          BQE4.setName("BQE4");
          panel6.add(BQE4);
          BQE4.setBounds(55, 135, 80, 28);

          //---- CHQ1 ----
          CHQ1.setName("CHQ1");
          panel6.add(CHQ1);
          CHQ1.setBounds(135, 60, 80, 28);

          //---- CHQ2 ----
          CHQ2.setName("CHQ2");
          panel6.add(CHQ2);
          CHQ2.setBounds(135, 85, 80, 28);

          //---- CHQ3 ----
          CHQ3.setName("CHQ3");
          panel6.add(CHQ3);
          CHQ3.setBounds(135, 110, 80, 28);

          //---- CHQ4 ----
          CHQ4.setName("CHQ4");
          panel6.add(CHQ4);
          CHQ4.setBounds(135, 135, 80, 28);

          //---- HPL1 ----
          HPL1.setName("HPL1");
          panel6.add(HPL1);
          HPL1.setBounds(215, 60, 20, HPL1.getPreferredSize().height);

          //---- HPL2 ----
          HPL2.setName("HPL2");
          panel6.add(HPL2);
          HPL2.setBounds(215, 85, 20, 28);

          //---- HPL3 ----
          HPL3.setName("HPL3");
          panel6.add(HPL3);
          HPL3.setBounds(215, 110, 20, 28);

          //---- HPL4 ----
          HPL4.setName("HPL4");
          panel6.add(HPL4);
          HPL4.setBounds(215, 135, 20, 28);

          //---- E1DTE1 ----
          E1DTE1.setName("E1DTE1");
          panel6.add(E1DTE1);
          E1DTE1.setBounds(235, 60, 65, E1DTE1.getPreferredSize().height);

          //---- E1DTE2 ----
          E1DTE2.setName("E1DTE2");
          panel6.add(E1DTE2);
          E1DTE2.setBounds(235, 85, 65, 28);

          //---- E1DTE3 ----
          E1DTE3.setName("E1DTE3");
          panel6.add(E1DTE3);
          E1DTE3.setBounds(235, 110, 65, 28);

          //---- E1DTE4 ----
          E1DTE4.setName("E1DTE4");
          panel6.add(E1DTE4);
          E1DTE4.setBounds(235, 135, 65, 28);

          //---- E1MRG1 ----
          E1MRG1.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG1.setName("E1MRG1");
          panel6.add(E1MRG1);
          E1MRG1.setBounds(300, 60, 95, E1MRG1.getPreferredSize().height);

          //---- E1MRG2 ----
          E1MRG2.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG2.setName("E1MRG2");
          panel6.add(E1MRG2);
          E1MRG2.setBounds(300, 85, 95, 28);

          //---- E1MRG3 ----
          E1MRG3.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG3.setName("E1MRG3");
          panel6.add(E1MRG3);
          E1MRG3.setBounds(300, 110, 95, 28);

          //---- E1MRG4 ----
          E1MRG4.setHorizontalAlignment(SwingConstants.RIGHT);
          E1MRG4.setName("E1MRG4");
          panel6.add(E1MRG4);
          E1MRG4.setBounds(300, 135, 95, 28);

          //---- E1EC1 ----
          E1EC1.setName("E1EC1");
          panel6.add(E1EC1);
          E1EC1.setBounds(395, 60, 30, E1EC1.getPreferredSize().height);

          //---- E1EC2 ----
          E1EC2.setName("E1EC2");
          panel6.add(E1EC2);
          E1EC2.setBounds(395, 85, 30, 28);

          //---- E1EC3 ----
          E1EC3.setName("E1EC3");
          panel6.add(E1EC3);
          E1EC3.setBounds(395, 110, 30, 28);

          //---- E1EC4 ----
          E1EC4.setName("E1EC4");
          panel6.add(E1EC4);
          E1EC4.setBounds(395, 135, 30, 28);

          //---- E1PC1 ----
          E1PC1.setName("E1PC1");
          panel6.add(E1PC1);
          E1PC1.setBounds(425, 60, 30, E1PC1.getPreferredSize().height);

          //---- E1PC2 ----
          E1PC2.setName("E1PC2");
          panel6.add(E1PC2);
          E1PC2.setBounds(425, 85, 30, 28);

          //---- E1PC3 ----
          E1PC3.setName("E1PC3");
          panel6.add(E1PC3);
          E1PC3.setBounds(425, 110, 30, 28);

          //---- E1PC4 ----
          E1PC4.setName("E1PC4");
          panel6.add(E1PC4);
          E1PC4.setBounds(425, 135, 30, 28);

          //---- LRGA ----
          LRGA.setText("@LRGA@");
          LRGA.setToolTipText("@LRGA@");
          LRGA.setName("LRGA");
          panel6.add(LRGA);
          LRGA.setBounds(460, 62, 225, LRGA.getPreferredSize().height);

          //---- LRGA2 ----
          LRGA2.setText("@LRGB@");
          LRGA2.setToolTipText("@LRGB@");
          LRGA2.setName("LRGA2");
          panel6.add(LRGA2);
          LRGA2.setBounds(460, 87, 225, 24);

          //---- LRGA3 ----
          LRGA3.setText("@LRGC@");
          LRGA3.setToolTipText("@LRGC@");
          LRGA3.setName("LRGA3");
          panel6.add(LRGA3);
          LRGA3.setBounds(460, 112, 225, 24);

          //---- LRGA4 ----
          LRGA4.setText("@LRGD@");
          LRGA4.setToolTipText("@LRGD@");
          LRGA4.setName("LRGA4");
          panel6.add(LRGA4);
          LRGA4.setBounds(460, 137, 225, 24);

          //---- label1 ----
          label1.setText("MR");
          label1.setName("label1");
          panel6.add(label1);
          label1.setBounds(31, 37, label1.getPreferredSize().width, 21);

          //---- label2 ----
          label2.setText("Banque");
          label2.setName("label2");
          panel6.add(label2);
          label2.setBounds(59, 37, 72, 21);

          //---- label3 ----
          label3.setText("N\u00b0 ch\u00e8que");
          label3.setName("label3");
          panel6.add(label3);
          label3.setBounds(140, 37, 70, 20);

          //---- label4 ----
          label4.setText("H");
          label4.setName("label4");
          panel6.add(label4);
          label4.setBounds(218, 39, 15, label4.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Date");
          label5.setName("label5");
          panel6.add(label5);
          label5.setBounds(241, 37, 53, 20);

          //---- label6 ----
          label6.setText("Montant");
          label6.setName("label6");
          panel6.add(label6);
          label6.setBounds(307, 37, 80, 20);

          //---- label7 ----
          label7.setText("CE");
          label7.setName("label7");
          panel6.add(label7);
          label7.setBounds(398, 37, 25, 20);

          //---- label8 ----
          label8.setText("%");
          label8.setName("label8");
          panel6.add(label8);
          label8.setBounds(430, 37, 20, 20);

          //---- label9 ----
          label9.setText("Libell\u00e9 r\u00e9glement");
          label9.setName("label9");
          panel6.add(label9);
          label9.setBounds(465, 37, 185, 20);
        }
        p_contenu.add(panel6);
        panel6.setBounds(15, 105, 710, 475);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private JPanel p_contenu;
  private JPanel panel5;
  private RiZoneSortie E1RDMX;
  private RiZoneSortie E1MPAX;
  private JLabel OBJ_20;
  private JLabel OBJ_21;
  private JLabel OBJ_22;
  private RiZoneSortie E1TTC_1;
  private JPanel panel6;
  private JLabel OBJ_26;
  private XRiTextField E1IN1;
  private JPanel panel7;
  private JLabel OBJ_30;
  private RiZoneSortie E1CLC;
  private RiZoneSortie OBJ_32;
  private JLabel OBJ_34;
  private RiZoneSortie WEDEP;
  private RiZoneSortie WEPOS;
  private JLabel OBJ_41;
  private RiZoneSortie WEPLF;
  private JLabel OBJ_42;
  private JLabel OBJ_55;
  private RiZoneSortie WEPLF2;
  private JLabel l_egal;
  private JPanel panel2;
  private JLabel OBJ_39;
  private JLabel OBJ_36;
  private JLabel OBJ_40;
  private RiZoneSortie WEPCO;
  private RiZoneSortie WEFAC;
  private RiZoneSortie WEEXP;
  private RiZoneSortie WECDE;
  private RiZoneSortie WEVAR;
  private JLabel OBJ_38;
  private JLabel OBJ_37;
  private JLabel l_plus;
  private JLabel l_plus2;
  private JLabel l_plus3;
  private JLabel l_plus4;
  private XRiTextField E1RG1;
  private XRiTextField E1RG2;
  private XRiTextField E1RG3;
  private XRiTextField E1RG4;
  private XRiTextField BQE1;
  private XRiTextField BQE2;
  private XRiTextField BQE3;
  private XRiTextField BQE4;
  private XRiTextField CHQ1;
  private XRiTextField CHQ2;
  private XRiTextField CHQ3;
  private XRiTextField CHQ4;
  private XRiTextField HPL1;
  private XRiTextField HPL2;
  private XRiTextField HPL3;
  private XRiTextField HPL4;
  private XRiTextField E1DTE1;
  private XRiTextField E1DTE2;
  private XRiTextField E1DTE3;
  private XRiTextField E1DTE4;
  private XRiTextField E1MRG1;
  private XRiTextField E1MRG2;
  private XRiTextField E1MRG3;
  private XRiTextField E1MRG4;
  private XRiTextField E1EC1;
  private XRiTextField E1EC2;
  private XRiTextField E1EC3;
  private XRiTextField E1EC4;
  private XRiTextField E1PC1;
  private XRiTextField E1PC2;
  private XRiTextField E1PC3;
  private XRiTextField E1PC4;
  private RiZoneSortie LRGA;
  private RiZoneSortie LRGA2;
  private RiZoneSortie LRGA3;
  private RiZoneSortie LRGA4;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
