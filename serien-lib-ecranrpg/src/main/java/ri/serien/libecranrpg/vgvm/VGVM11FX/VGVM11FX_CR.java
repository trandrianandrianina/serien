/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_CR extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  public boolean isChantier;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_CR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    
    setCloseKey("F1", "F5");
    
    // Titre
    setTitle("Remise de pied");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
    OBJ_153.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCPL@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    L1LIB1 = new XRiTextField();
    WARTT = new RiZoneSortie();
    OBJ_17 = new JLabel();
    OBJ_18 = new JLabel();
    WNLI = new RiZoneSortie();
    WCOD = new RiZoneSortie();
    panel1 = new JPanel();
    OBJ_27 = new JLabel();
    L1PVBX = new XRiTextField();
    OBJ_29 = new JLabel();
    L1PVNX = new XRiTextField();
    panel2 = new JPanel();
    OBJ_30 = new JLabel();
    WREM1 = new XRiTextField();
    WREM2 = new XRiTextField();
    WREM3 = new XRiTextField();
    OBJ_31 = new JLabel();
    L1COEX = new XRiTextField();
    label15 = new JLabel();
    OBJ_153 = new JLabel();
    L1CPL = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_161 = new JLabel();
    L1IN2 = new XRiTextField();
    OBJ_115 = new JLabel();
    L1QTLX = new XRiTextField();
    WSER = new XRiCheckBox();
    WF4REM = new XRiCheckBox();
    OBJ_26 = new JLabel();
    L1REP = new XRiTextField();
    label14 = new JLabel();
    L1QTEX = new XRiTextField();
    OBJ_118 = new JLabel();
    L1MAG = new XRiTextField();
    BTAR = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(1040, 210));
    setPreferredSize(new Dimension(1040, 210));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
          L1LIB1.setName("L1LIB1");
          p_recup.add(L1LIB1);
          L1LIB1.setBounds(285, 38, 310, L1LIB1.getPreferredSize().height);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setText("@WARTT@");
          WARTT.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
          WARTT.setName("WARTT");
          p_recup.add(WARTT);
          WARTT.setBounds(89, 40, 190, WARTT.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("C   N\u00b0Li");
          OBJ_17.setName("OBJ_17");
          p_recup.add(OBJ_17);
          OBJ_17.setBounds(25, 15, 45, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("Article");
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(80, 15, 42, 20);

          //---- WNLI ----
          WNLI.setText("@WNLI@");
          WNLI.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
          WNLI.setComponentPopupMenu(BTD);
          WNLI.setName("WNLI");
          p_recup.add(WNLI);
          WNLI.setBounds(50, 40, 34, WNLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setText("@WCOD@");
          WCOD.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
          WCOD.setName("WCOD");
          p_recup.add(WCOD);
          WCOD.setBounds(25, 40, 20, WCOD.getPreferredSize().height);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_27 ----
            OBJ_27.setText("Remise sur la base de");
            OBJ_27.setName("OBJ_27");
            panel1.add(OBJ_27);
            OBJ_27.setBounds(25, 12, 205, 25);

            //---- L1PVBX ----
            L1PVBX.setComponentPopupMenu(BTD);
            L1PVBX.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            L1PVBX.setHorizontalAlignment(SwingConstants.RIGHT);
            L1PVBX.setName("L1PVBX");
            panel1.add(L1PVBX);
            L1PVBX.setBounds(255, 10, 92, L1PVBX.getPreferredSize().height);

            //---- OBJ_29 ----
            OBJ_29.setText("Remise en montant");
            OBJ_29.setName("OBJ_29");
            panel1.add(OBJ_29);
            OBJ_29.setBounds(25, 42, 205, 25);

            //---- L1PVNX ----
            L1PVNX.setComponentPopupMenu(BTD);
            L1PVNX.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            L1PVNX.setHorizontalAlignment(SwingConstants.RIGHT);
            L1PVNX.setName("L1PVNX");
            panel1.add(L1PVNX);
            L1PVNX.setBounds(255, 40, 92, L1PVNX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel1);
          panel1.setBounds(25, 85, 385, 80);

          //======== panel2 ========
          {
            panel2.setBackground(new Color(214, 217, 223));
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_30 ----
            OBJ_30.setText("Remises en pourcentage");
            OBJ_30.setName("OBJ_30");
            panel2.add(OBJ_30);
            OBJ_30.setBounds(25, 12, 165, 25);

            //---- WREM1 ----
            WREM1.setComponentPopupMenu(BTD);
            WREM1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            WREM1.setHorizontalAlignment(SwingConstants.RIGHT);
            WREM1.setName("WREM1");
            panel2.add(WREM1);
            WREM1.setBounds(190, 10, 52, WREM1.getPreferredSize().height);

            //---- WREM2 ----
            WREM2.setComponentPopupMenu(BTD);
            WREM2.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            WREM2.setHorizontalAlignment(SwingConstants.RIGHT);
            WREM2.setName("WREM2");
            panel2.add(WREM2);
            WREM2.setBounds(245, 10, 52, WREM2.getPreferredSize().height);

            //---- WREM3 ----
            WREM3.setComponentPopupMenu(BTD);
            WREM3.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            WREM3.setHorizontalAlignment(SwingConstants.RIGHT);
            WREM3.setName("WREM3");
            panel2.add(WREM3);
            WREM3.setBounds(300, 10, 52, WREM3.getPreferredSize().height);

            //---- OBJ_31 ----
            OBJ_31.setText("Coefficient");
            OBJ_31.setName("OBJ_31");
            panel2.add(OBJ_31);
            OBJ_31.setBounds(25, 42, 165, 25);

            //---- L1COEX ----
            L1COEX.setComponentPopupMenu(BTD);
            L1COEX.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            L1COEX.setHorizontalAlignment(SwingConstants.RIGHT);
            L1COEX.setName("L1COEX");
            panel2.add(L1COEX);
            L1COEX.setBounds(190, 40, 92, L1COEX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel2);
          panel2.setBounds(440, 85, 385, 80);

          //---- label15 ----
          label15.setText("OU");
          label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setName("label15");
          p_recup.add(label15);
          label15.setBounds(410, 85, 30, 80);

          //---- OBJ_153 ----
          OBJ_153.setText("@LIBCPL@");
          OBJ_153.setName("OBJ_153");
          p_recup.add(OBJ_153);
          OBJ_153.setBounds(620, 15, 145, 20);

          //---- L1CPL ----
          L1CPL.setComponentPopupMenu(BTD);
          L1CPL.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
          L1CPL.setName("L1CPL");
          p_recup.add(L1CPL);
          L1CPL.setBounds(620, 38, 92, L1CPL.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 846, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 184, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_161 ----
    OBJ_161.setText("ou type de gratuit");
    OBJ_161.setName("OBJ_161");

    //---- L1IN2 ----
    L1IN2.setComponentPopupMenu(BTD);
    L1IN2.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    L1IN2.setName("L1IN2");

    //---- OBJ_115 ----
    OBJ_115.setText("@WLCA@");
    OBJ_115.setHorizontalAlignment(SwingConstants.RIGHT);
    OBJ_115.setName("OBJ_115");

    //---- L1QTLX ----
    L1QTLX.setComponentPopupMenu(BTD);
    L1QTLX.setHorizontalAlignment(SwingConstants.RIGHT);
    L1QTLX.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    L1QTLX.setName("L1QTLX");

    //---- WSER ----
    WSER.setText("WSER");
    WSER.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    WSER.setHorizontalAlignment(SwingConstants.RIGHT);
    WSER.setComponentPopupMenu(BTD);
    WSER.setName("WSER");

    //---- WF4REM ----
    WF4REM.setText("WF4REM");
    WF4REM.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    WF4REM.setHorizontalAlignment(SwingConstants.RIGHT);
    WF4REM.setComponentPopupMenu(BTD);
    WF4REM.setName("WF4REM");

    //---- OBJ_26 ----
    OBJ_26.setText("Repr\u00e9sentant");
    OBJ_26.setName("OBJ_26");

    //---- L1REP ----
    L1REP.setComponentPopupMenu(BTD);
    L1REP.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    L1REP.setName("L1REP");

    //---- label14 ----
    label14.setText("Quantit\u00e9");
    label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
    label14.setName("label14");

    //---- L1QTEX ----
    L1QTEX.setComponentPopupMenu(BTD);
    L1QTEX.setHorizontalAlignment(SwingConstants.RIGHT);
    L1QTEX.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    L1QTEX.setName("L1QTEX");

    //---- OBJ_118 ----
    OBJ_118.setText("Magasin");
    OBJ_118.setName("OBJ_118");

    //---- L1MAG ----
    L1MAG.setComponentPopupMenu(BTD);
    L1MAG.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    L1MAG.setName("L1MAG");

    //---- BTAR ----
    BTAR.setComponentPopupMenu(BTD);
    BTAR.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    BTAR.setName("BTAR");
    // //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel p_recup;
  private XRiTextField L1LIB1;
  private RiZoneSortie WARTT;
  private JLabel OBJ_17;
  private JLabel OBJ_18;
  private RiZoneSortie WNLI;
  private RiZoneSortie WCOD;
  private JPanel panel1;
  private JLabel OBJ_27;
  private XRiTextField L1PVBX;
  private JLabel OBJ_29;
  private XRiTextField L1PVNX;
  private JPanel panel2;
  private JLabel OBJ_30;
  private XRiTextField WREM1;
  private XRiTextField WREM2;
  private XRiTextField WREM3;
  private JLabel OBJ_31;
  private XRiTextField L1COEX;
  private JLabel label15;
  private JLabel OBJ_153;
  private XRiTextField L1CPL;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JLabel OBJ_161;
  private XRiTextField L1IN2;
  private JLabel OBJ_115;
  private XRiTextField L1QTLX;
  private XRiCheckBox WSER;
  private XRiCheckBox WF4REM;
  private JLabel OBJ_26;
  private XRiTextField L1REP;
  private JLabel label14;
  private XRiTextField L1QTEX;
  private JLabel OBJ_118;
  private XRiTextField L1MAG;
  private XRiTextField BTAR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
