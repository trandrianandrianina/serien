
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.documentvente.snparticipationfrais.SNParticipationFraisExpedition;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_EX extends SNPanelEcranRPG implements ioFrame {
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  private boolean isConsultation = false;
  
  private String[] EXCPR_Value = { "1", "2", "3", };
  private String[] EXMCP_Value = { "1", "2", "3", };
  
  public VGVM01FX_EX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    EXCPR.setValeurs(EXCPR_Value, null);
    EXMCP.setValeurs(EXMCP_Value, null);
    EXPRF.setValeursSelection("1", " ");
    EXENL.setValeursSelection("1", " ");
    EXURG.setValeursSelection("9", " ");
    EXETQ.setValeursSelection("1", " ");
    EXFGC.setValeursSelection("1", " ");
    EXRTR.setValeursSelection("1", " ");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(),
        ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(false);
    
    IdEtablissement idEtablissement = ManagerSessionClient.getInstance().getIdEtablissement(getIdSession());
    
    // composant participation frais d'expédition
    snParticipationFraisExpedition.setSession(getSession());
    snParticipationFraisExpedition.setIdEtablissement(idEtablissement);
    snParticipationFraisExpedition.charger(true);
    snParticipationFraisExpedition.setSelectionParChampRPG(lexique, "EXPE");
    snParticipationFraisExpedition.setEnabled(!isConsultation);
    lbParticipation.setVisible(EXCPR.getSelectedIndex() == 2);
    snParticipationFraisExpedition.setVisible(EXCPR.getSelectedIndex() == 2);
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    

    if (snEtablissement.getIdSelection() != null) {
      p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snParticipationFraisExpedition.renseignerChampRPG(lexique, "EXPE");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void EXCPRItemStateChanged(ItemEvent e) {
    try {
      lbParticipation.setVisible(EXCPR.getSelectedIndex() == 2);
      snParticipationFraisExpedition.setVisible(EXCPR.getSelectedIndex() == 2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlbandeau = new SNPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    lbLibelle = new SNLabelChamp();
    EXLIB = new XRiTextField();
    pnlGeneral = new SNPanel();
    EXENL = new XRiCheckBox();
    EXURG = new XRiCheckBox();
    EXRTR = new XRiCheckBox();
    EXETQ = new XRiCheckBox();
    pnlCalcul = new SNPanelTitre();
    EXPRF = new XRiCheckBox();
    lbCalculPort = new SNLabelChamp();
    pnlCalculPort = new SNPanel();
    EXCPR = new XRiComboBox();
    lbParticipation = new SNLabelChamp();
    snParticipationFraisExpedition = new SNParticipationFraisExpedition();
    lbModeCalcul = new SNLabelChamp();
    EXMCP = new XRiComboBox();
    lbFrancoAPartirDe = new SNLabelChamp();
    EXFRP = new XRiTextField();
    lbFrancoMaintenuPendant = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    EXFRM = new XRiTextField();
    lbSemaine = new SNLabelUnite();
    EXFGC = new XRiCheckBox();
    sNPanel2 = new SNPanel();
    lbNumeroContrat = new SNLabelChamp();
    EXNCO = new XRiTextField();
    lbNumeroContrat2 = new SNLabelChamp();
    sNPanel3 = new SNPanel();
    EXIHH = new XRiTextField();
    EXIHM = new XRiTextField();
    lbNumeroContrat3 = new SNLabelChamp();
    EXCOM = new XRiTextField();
    lbNumeroContrat4 = new SNLabelChamp();
    EXOPT = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlbandeau ========
    {
      pnlbandeau.setName("pnlbandeau");
      pnlbandeau.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes : Mode d'exp\u00e9dition");
      p_bpresentation.setName("p_bpresentation");
      pnlbandeau.add(p_bpresentation);
    }
    add(pnlbandeau, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeCategorie ----
        lbCodeCategorie.setText("code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- INDIND ----
        INDIND.setPreferredSize(new Dimension(40, 30));
        INDIND.setMinimumSize(new Dimension(40, 30));
        INDIND.setMaximumSize(new Dimension(40, 30));
        INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDIND.setName("INDIND");
        pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setMinimumSize(new Dimension(80, 30));
        lbLibelle.setMaximumSize(new Dimension(80, 30));
        lbLibelle.setPreferredSize(new Dimension(80, 30));
        lbLibelle.setName("lbLibelle");
        pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- EXLIB ----
        EXLIB.setPreferredSize(new Dimension(400, 30));
        EXLIB.setMinimumSize(new Dimension(400, 30));
        EXLIB.setMaximumSize(new Dimension(400, 30));
        EXLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXLIB.setName("EXLIB");
        pnlPersonnalisation.add(EXLIB, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlPersonnalisation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlGeneral ========
      {
        pnlGeneral.setName("pnlGeneral");
        pnlGeneral.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- EXENL ----
        EXENL.setText("Mode d'exp\u00e9dition de type enl\u00e8vement");
        EXENL.setComponentPopupMenu(null);
        EXENL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXENL.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXENL.setPreferredSize(new Dimension(250, 30));
        EXENL.setMinimumSize(new Dimension(250, 30));
        EXENL.setMaximumSize(new Dimension(250, 30));
        EXENL.setName("EXENL");
        pnlGeneral.add(EXENL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- EXURG ----
        EXURG.setText("Crit\u00e8re d'urgence");
        EXURG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXURG.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXURG.setPreferredSize(new Dimension(250, 30));
        EXURG.setMinimumSize(new Dimension(250, 30));
        EXURG.setMaximumSize(new Dimension(250, 30));
        EXURG.setName("EXURG");
        pnlGeneral.add(EXURG, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- EXRTR ----
        EXRTR.setText("Recherche du code transporteur  \u00e0 la commande");
        EXRTR.setComponentPopupMenu(null);
        EXRTR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXRTR.setMinimumSize(new Dimension(250, 30));
        EXRTR.setMaximumSize(new Dimension(250, 30));
        EXRTR.setPreferredSize(new Dimension(250, 30));
        EXRTR.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXRTR.setName("EXRTR");
        pnlGeneral.add(EXRTR, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- EXETQ ----
        EXETQ.setText("Etiquette colis non \u00e9dit\u00e9e");
        EXETQ.setComponentPopupMenu(null);
        EXETQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXETQ.setMinimumSize(new Dimension(250, 30));
        EXETQ.setMaximumSize(new Dimension(250, 30));
        EXETQ.setPreferredSize(new Dimension(250, 30));
        EXETQ.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXETQ.setName("EXETQ");
        pnlGeneral.add(EXETQ, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlGeneral,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCalcul ========
      {
        pnlCalcul.setTitre("Calcul du port et franco");
        pnlCalcul.setName("pnlCalcul");
        pnlCalcul.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCalcul.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCalcul.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCalcul.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCalcul.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- EXPRF ----
        EXPRF.setText("Port factur\u00e9");
        EXPRF.setComponentPopupMenu(null);
        EXPRF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXPRF.setMinimumSize(new Dimension(250, 30));
        EXPRF.setMaximumSize(new Dimension(250, 30));
        EXPRF.setPreferredSize(new Dimension(250, 30));
        EXPRF.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXPRF.setName("EXPRF");
        pnlCalcul.add(EXPRF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbCalculPort ----
        lbCalculPort.setText("Calcul du port");
        lbCalculPort.setMaximumSize(new Dimension(250, 30));
        lbCalculPort.setMinimumSize(new Dimension(250, 30));
        lbCalculPort.setPreferredSize(new Dimension(250, 30));
        lbCalculPort.setName("lbCalculPort");
        pnlCalcul.add(lbCalculPort, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlCalculPort ========
        {
          pnlCalculPort.setName("pnlCalculPort");
          pnlCalculPort.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCalculPort.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlCalculPort.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCalculPort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCalculPort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- EXCPR ----
          EXCPR.setModel(new DefaultComboBoxModel(
              new String[] { "Exprim\u00e9 en poids", "Exprim\u00e9 en nombre de palettes", "Par une personnalisation PE" }));
          EXCPR.setComponentPopupMenu(null);
          EXCPR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EXCPR.setFont(new Font("sansserif", Font.PLAIN, 14));
          EXCPR.setBackground(Color.white);
          EXCPR.setName("EXCPR");
          EXCPR.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              EXCPRItemStateChanged(e);
            }
          });
          pnlCalculPort.add(EXCPR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbParticipation ----
          lbParticipation.setText("Participation aux frais d'exp\u00e9dition");
          lbParticipation.setMinimumSize(new Dimension(300, 30));
          lbParticipation.setMaximumSize(new Dimension(350, 30));
          lbParticipation.setPreferredSize(new Dimension(300, 30));
          lbParticipation.setName("lbParticipation");
          pnlCalculPort.add(lbParticipation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snParticipationFraisExpedition ----
          snParticipationFraisExpedition.setName("snParticipationFraisExpedition");
          pnlCalculPort.add(snParticipationFraisExpedition, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCalcul.add(pnlCalculPort, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbModeCalcul ----
        lbModeCalcul.setText("Mode de calcul du port");
        lbModeCalcul.setName("lbModeCalcul");
        pnlCalcul.add(lbModeCalcul, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- EXMCP ----
        EXMCP.setModel(new DefaultComboBoxModel(
            new String[] { "Au prorata des livraisons", "Global sur la premi\u00e8re livraison", "Global sur le reliquat" }));
        EXMCP.setComponentPopupMenu(null);
        EXMCP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXMCP.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXMCP.setBackground(Color.white);
        EXMCP.setName("EXMCP");
        pnlCalcul.add(EXMCP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFrancoAPartirDe ----
        lbFrancoAPartirDe.setText("Franco \u00e0 partir de");
        lbFrancoAPartirDe.setName("lbFrancoAPartirDe");
        pnlCalcul.add(lbFrancoAPartirDe, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- EXFRP ----
        EXFRP.setComponentPopupMenu(null);
        EXFRP.setMinimumSize(new Dimension(100, 30));
        EXFRP.setMaximumSize(new Dimension(100, 30));
        EXFRP.setPreferredSize(new Dimension(100, 30));
        EXFRP.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXFRP.setName("EXFRP");
        pnlCalcul.add(EXFRP, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFrancoMaintenuPendant ----
        lbFrancoMaintenuPendant.setText("Franco maintenu pendant");
        lbFrancoMaintenuPendant.setName("lbFrancoMaintenuPendant");
        pnlCalcul.add(lbFrancoMaintenuPendant, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- EXFRM ----
          EXFRM.setComponentPopupMenu(null);
          EXFRM.setPreferredSize(new Dimension(50, 30));
          EXFRM.setMinimumSize(new Dimension(50, 30));
          EXFRM.setMaximumSize(new Dimension(50, 30));
          EXFRM.setFont(new Font("sansserif", Font.PLAIN, 14));
          EXFRM.setName("EXFRM");
          sNPanel1.add(EXFRM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSemaine ----
          lbSemaine.setText("semaines");
          lbSemaine.setName("lbSemaine");
          sNPanel1.add(lbSemaine, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCalcul.add(sNPanel1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- EXFGC ----
        EXFGC.setText("Franco garanti \u00e0 la commande");
        EXFGC.setComponentPopupMenu(null);
        EXFGC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        EXFGC.setMinimumSize(new Dimension(250, 30));
        EXFGC.setMaximumSize(new Dimension(250, 30));
        EXFGC.setPreferredSize(new Dimension(250, 30));
        EXFGC.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXFGC.setName("EXFGC");
        pnlCalcul.add(EXFGC, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelContenu1.add(pnlCalcul,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanel2 ========
      {
        sNPanel2.setName("sNPanel2");
        sNPanel2.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNumeroContrat ----
        lbNumeroContrat.setText("Num\u00e9ro de contrat");
        lbNumeroContrat.setMaximumSize(new Dimension(250, 30));
        lbNumeroContrat.setMinimumSize(new Dimension(250, 30));
        lbNumeroContrat.setPreferredSize(new Dimension(250, 30));
        lbNumeroContrat.setName("lbNumeroContrat");
        sNPanel2.add(lbNumeroContrat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- EXNCO ----
        EXNCO.setComponentPopupMenu(null);
        EXNCO.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXNCO.setMinimumSize(new Dimension(150, 30));
        EXNCO.setMaximumSize(new Dimension(150, 30));
        EXNCO.setPreferredSize(new Dimension(150, 30));
        EXNCO.setName("EXNCO");
        sNPanel2.add(EXNCO, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroContrat2 ----
        lbNumeroContrat2.setText("Heure imp\u00e9rative");
        lbNumeroContrat2.setMaximumSize(new Dimension(250, 30));
        lbNumeroContrat2.setMinimumSize(new Dimension(250, 30));
        lbNumeroContrat2.setPreferredSize(new Dimension(250, 30));
        lbNumeroContrat2.setName("lbNumeroContrat2");
        sNPanel2.add(lbNumeroContrat2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== sNPanel3 ========
        {
          sNPanel3.setName("sNPanel3");
          sNPanel3.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- EXIHH ----
          EXIHH.setToolTipText("sous la forme HH");
          EXIHH.setComponentPopupMenu(null);
          EXIHH.setPreferredSize(new Dimension(50, 30));
          EXIHH.setMinimumSize(new Dimension(50, 30));
          EXIHH.setMaximumSize(new Dimension(50, 30));
          EXIHH.setFont(new Font("sansserif", Font.PLAIN, 14));
          EXIHH.setName("EXIHH");
          sNPanel3.add(EXIHH, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- EXIHM ----
          EXIHM.setToolTipText("sous la forme MM");
          EXIHM.setComponentPopupMenu(null);
          EXIHM.setPreferredSize(new Dimension(50, 30));
          EXIHM.setMinimumSize(new Dimension(50, 30));
          EXIHM.setMaximumSize(new Dimension(50, 30));
          EXIHM.setFont(new Font("sansserif", Font.PLAIN, 14));
          EXIHM.setName("EXIHM");
          sNPanel3.add(EXIHM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanel2.add(sNPanel3, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroContrat3 ----
        lbNumeroContrat3.setText("Commentaire");
        lbNumeroContrat3.setMaximumSize(new Dimension(250, 30));
        lbNumeroContrat3.setMinimumSize(new Dimension(250, 30));
        lbNumeroContrat3.setPreferredSize(new Dimension(250, 30));
        lbNumeroContrat3.setName("lbNumeroContrat3");
        sNPanel2.add(lbNumeroContrat3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- EXCOM ----
        EXCOM.setToolTipText("voir documentation");
        EXCOM.setComponentPopupMenu(null);
        EXCOM.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXCOM.setMaximumSize(new Dimension(150, 30));
        EXCOM.setMinimumSize(new Dimension(150, 30));
        EXCOM.setPreferredSize(new Dimension(150, 30));
        EXCOM.setName("EXCOM");
        sNPanel2.add(EXCOM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroContrat4 ----
        lbNumeroContrat4.setText("Option de fin de bon");
        lbNumeroContrat4.setMaximumSize(new Dimension(250, 30));
        lbNumeroContrat4.setMinimumSize(new Dimension(250, 30));
        lbNumeroContrat4.setPreferredSize(new Dimension(250, 30));
        lbNumeroContrat4.setName("lbNumeroContrat4");
        sNPanel2.add(lbNumeroContrat4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- EXOPT ----
        EXOPT.setToolTipText("voir documentation");
        EXOPT.setComponentPopupMenu(null);
        EXOPT.setMinimumSize(new Dimension(50, 30));
        EXOPT.setMaximumSize(new Dimension(50, 30));
        EXOPT.setPreferredSize(new Dimension(50, 30));
        EXOPT.setFont(new Font("sansserif", Font.PLAIN, 14));
        EXOPT.setName("EXOPT");
        sNPanel2.add(EXOPT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanel2,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlbandeau;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNLabelChamp lbLibelle;
  private XRiTextField EXLIB;
  private SNPanel pnlGeneral;
  private XRiCheckBox EXENL;
  private XRiCheckBox EXURG;
  private XRiCheckBox EXRTR;
  private XRiCheckBox EXETQ;
  private SNPanelTitre pnlCalcul;
  private XRiCheckBox EXPRF;
  private SNLabelChamp lbCalculPort;
  private SNPanel pnlCalculPort;
  private XRiComboBox EXCPR;
  private SNLabelChamp lbParticipation;
  private SNParticipationFraisExpedition snParticipationFraisExpedition;
  private SNLabelChamp lbModeCalcul;
  private XRiComboBox EXMCP;
  private SNLabelChamp lbFrancoAPartirDe;
  private XRiTextField EXFRP;
  private SNLabelChamp lbFrancoMaintenuPendant;
  private SNPanel sNPanel1;
  private XRiTextField EXFRM;
  private SNLabelUnite lbSemaine;
  private XRiCheckBox EXFGC;
  private SNPanel sNPanel2;
  private SNLabelChamp lbNumeroContrat;
  private XRiTextField EXNCO;
  private SNLabelChamp lbNumeroContrat2;
  private SNPanel sNPanel3;
  private XRiTextField EXIHH;
  private XRiTextField EXIHM;
  private SNLabelChamp lbNumeroContrat3;
  private XRiTextField EXCOM;
  private SNLabelChamp lbNumeroContrat4;
  private XRiTextField EXOPT;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
