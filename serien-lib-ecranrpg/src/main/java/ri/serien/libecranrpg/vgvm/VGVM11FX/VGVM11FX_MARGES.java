
package ri.serien.libecranrpg.vgvm.VGVM11FX;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_MARGES extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private int etatMarge;
  private ImageIcon margePlus = null;
  private ImageIcon margeMoins = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_MARGES(SNPanelEcranRPG parent) {
    super(parent);
    setVisible(true);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    initDiverses();
    
    // Titre
    setTitle("Les Marges");
    
    
    margePlus = lexique.chargerImage("images/marge_moins.png", true);
    margeMoins = lexique.chargerImage("images/marge_plus.png", true);
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    setModal(true);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WMMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMR@")).trim());
    WMMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMT@")).trim());
    WMAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAC@")).trim());
    WPMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMR@")).trim());
    WPMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMT@")).trim());
    WPAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAC@")).trim());
    WTPRL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPRL@")).trim());
    L1MHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1MHT@")).trim());
    WTPVL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPVL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    etatMarge = Integer.parseInt(lexique.HostFieldGetData("MARGU"));
    if (etatMarge == 2) {
      panel3.setBorder(new TitledBorder("Marge totale"));
    }
    else if (etatMarge == 1) {
      panel3.setBorder(new TitledBorder("Marge unitaire"));
    }
    
    if (lexique.HostFieldGetData("TYPM").equalsIgnoreCase("MP")) {
      marge.setIcon(margePlus);
    }
    else {
      marge.setIcon(margeMoins);
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("MARGU", 0, "0"); // Pour qu'il reste fermé lors des prochains appel du C2
    etatMarge = 0;
    closePopupLinkWithBuffer(true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    WMMR = new RiZoneSortie();
    WMMT = new RiZoneSortie();
    WMAC = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    WPMR = new RiZoneSortie();
    WPMT = new RiZoneSortie();
    WPAC = new RiZoneSortie();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    WTPRL = new RiZoneSortie();
    L1MHT = new RiZoneSortie();
    WTPVL = new RiZoneSortie();
    marge = new JLabel();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(980, 200));
    setPreferredSize(new Dimension(970, 190));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Marge de la ligne"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- label1 ----
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setText("Marge r\u00e9elle");
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(15, 35, 120, 24);

          //---- label2 ----
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setText("Marge th\u00e9orique");
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(15, 75, 120, 24);

          //---- label3 ----
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setText("Action commerciale");
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(15, 115, 120, 24);

          //---- WMMR ----
          WMMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMR.setText("@WMMR@");
          WMMR.setName("WMMR");
          panel3.add(WMMR);
          WMMR.setBounds(140, 35, 90, WMMR.getPreferredSize().height);

          //---- WMMT ----
          WMMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMT.setText("@WMMT@");
          WMMT.setName("WMMT");
          panel3.add(WMMT);
          WMMT.setBounds(140, 75, 90, WMMT.getPreferredSize().height);

          //---- WMAC ----
          WMAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WMAC.setText("@WMAC@");
          WMAC.setName("WMAC");
          panel3.add(WMAC);
          WMAC.setBounds(140, 115, 90, WMAC.getPreferredSize().height);

          //---- label4 ----
          label4.setText("% Chiffre d'affaires r\u00e9el");
          label4.setForeground(Color.black);
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(255, 35, 170, 24);

          //---- label5 ----
          label5.setText("% Chiffre d'affaires r\u00e9el");
          label5.setForeground(Color.black);
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(255, 75, 170, 24);

          //---- label6 ----
          label6.setText("% Chiffre d'affaires th\u00e9orique");
          label6.setForeground(Color.black);
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel3.add(label6);
          label6.setBounds(255, 115, 170, 24);

          //---- WPMR ----
          WPMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMR.setText("@WPMR@");
          WPMR.setName("WPMR");
          panel3.add(WPMR);
          WPMR.setBounds(425, 35, 70, WPMR.getPreferredSize().height);

          //---- WPMT ----
          WPMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMT.setText("@WPMT@");
          WPMT.setName("WPMT");
          panel3.add(WPMT);
          WPMT.setBounds(425, 75, 70, WPMT.getPreferredSize().height);

          //---- WPAC ----
          WPAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPAC.setText("@WPAC@");
          WPAC.setName("WPAC");
          panel3.add(WPAC);
          WPAC.setBounds(425, 115, 70, WPAC.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Prix de revient");
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setForeground(Color.black);
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(520, 35, 165, 24);

          //---- label8 ----
          label8.setText("Chiffre d'affaires r\u00e9el");
          label8.setForeground(Color.black);
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(520, 75, 165, 24);

          //---- label9 ----
          label9.setText("Chiffre d'affaires th\u00e9orique");
          label9.setForeground(Color.black);
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(520, 115, 165, 24);

          //---- WTPRL ----
          WTPRL.setHorizontalAlignment(SwingConstants.RIGHT);
          WTPRL.setText("@WTPRL@");
          WTPRL.setName("WTPRL");
          panel3.add(WTPRL);
          WTPRL.setBounds(685, 35, 75, WTPRL.getPreferredSize().height);

          //---- L1MHT ----
          L1MHT.setHorizontalAlignment(SwingConstants.RIGHT);
          L1MHT.setText("@L1MHT@");
          L1MHT.setName("L1MHT");
          panel3.add(L1MHT);
          L1MHT.setBounds(685, 75, 75, L1MHT.getPreferredSize().height);

          //---- WTPVL ----
          WTPVL.setHorizontalAlignment(SwingConstants.RIGHT);
          WTPVL.setText("@WTPVL@");
          WTPVL.setName("WTPVL");
          panel3.add(WTPVL);
          WTPVL.setBounds(685, 115, 75, WTPVL.getPreferredSize().height);

          //---- marge ----
          marge.setName("marge");
          panel3.add(marge);
          marge.setBounds(110, 32, 30, 30);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 776, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");

      //======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 100));
        menus_haut.setPreferredSize(new Dimension(160, 100));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());

        //======== riMenu2 ========
        {
          riMenu2.setName("riMenu2");

          //---- riMenu_bt2 ----
          riMenu_bt2.setText("Options");
          riMenu_bt2.setName("riMenu_bt2");
          riMenu2.add(riMenu_bt2);
        }
        menus_haut.add(riMenu2);

        //======== riSousMenu6 ========
        {
          riSousMenu6.setName("riSousMenu6");

          //---- riSousMenu_bt6 ----
          riSousMenu_bt6.setName("riSousMenu_bt6");
          riSousMenu_bt6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt6ActionPerformed(e);
            }
          });
          riSousMenu6.add(riSousMenu_bt6);
        }
        menus_haut.add(riSousMenu6);
      }
      scroll_droite.setViewportView(menus_haut);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private RiZoneSortie WMMR;
  private RiZoneSortie WMMT;
  private RiZoneSortie WMAC;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private RiZoneSortie WPMR;
  private RiZoneSortie WPMT;
  private RiZoneSortie WPAC;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private RiZoneSortie WTPRL;
  private RiZoneSortie L1MHT;
  private RiZoneSortie WTPVL;
  private JLabel marge;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
