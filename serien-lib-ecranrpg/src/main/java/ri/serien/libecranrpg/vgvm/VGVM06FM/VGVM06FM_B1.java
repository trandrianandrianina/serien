
package ri.serien.libecranrpg.vgvm.VGVM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM06FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM06FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    MULTC.setValeursSelection("*", " ");
    MULTP.setValeursSelection("*", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB1@")).trim());
    LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB2@")).trim());
    LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB3@")).trim());
    LB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB4@")).trim());
    LB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB5@")).trim());
    LB6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB6@")).trim());
    LB7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB7@")).trim());
    LB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB8@")).trim());
    LB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB9@")).trim());
    LB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    MULTP = new XRiCheckBox();
    MULTC = new XRiCheckBox();
    lbPrix01 = new JLabel();
    lbPrix2 = new JLabel();
    lbPrix3 = new JLabel();
    lbPrix4 = new JLabel();
    lbPrix5 = new JLabel();
    lbPrix6 = new JLabel();
    lbPrix7 = new JLabel();
    lbPrix8 = new JLabel();
    lbPrix9 = new JLabel();
    lbPrix10 = new JLabel();
    LB1 = new RiZoneSortie();
    LB2 = new RiZoneSortie();
    LB3 = new RiZoneSortie();
    LB4 = new RiZoneSortie();
    LB5 = new RiZoneSortie();
    LB6 = new RiZoneSortie();
    LB7 = new RiZoneSortie();
    LB8 = new RiZoneSortie();
    LB9 = new RiZoneSortie();
    LB10 = new RiZoneSortie();
    CO1 = new XRiTextField();
    CO2 = new XRiTextField();
    CO3 = new XRiTextField();
    CO4 = new XRiTextField();
    CO5 = new XRiTextField();
    CO6 = new XRiTextField();
    CO7 = new XRiTextField();
    CO8 = new XRiTextField();
    CO9 = new XRiTextField();
    CO10 = new XRiTextField();
    lbTitre1 = new JLabel();
    lbTitre2 = new JLabel();
    lbTitre3 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(590, 380));
    setPreferredSize(new Dimension(590, 380));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Prix ou coefficient par multiplication"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- MULTP ----
          MULTP.setText("Prix saisis uniquement");
          MULTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MULTP.setName("MULTP");
          panel2.add(MULTP);
          MULTP.setBounds(30, 320, 180, 20);

          //---- MULTC ----
          MULTC.setText("Coefficients");
          MULTC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          MULTC.setName("MULTC");
          panel2.add(MULTC);
          MULTC.setBounds(255, 320, 117, 20);

          //---- lbPrix01 ----
          lbPrix01.setText("Prix num\u00e9ro 1");
          lbPrix01.setName("lbPrix01");
          panel2.add(lbPrix01);
          lbPrix01.setBounds(20, 53, 95, 21);

          //---- lbPrix2 ----
          lbPrix2.setText("Prix num\u00e9ro 2");
          lbPrix2.setName("lbPrix2");
          panel2.add(lbPrix2);
          lbPrix2.setBounds(20, 79, 95, 21);

          //---- lbPrix3 ----
          lbPrix3.setText("Prix num\u00e9ro 3");
          lbPrix3.setName("lbPrix3");
          panel2.add(lbPrix3);
          lbPrix3.setBounds(20, 105, 95, 21);

          //---- lbPrix4 ----
          lbPrix4.setText("Prix num\u00e9ro 4");
          lbPrix4.setName("lbPrix4");
          panel2.add(lbPrix4);
          lbPrix4.setBounds(20, 131, 95, 21);

          //---- lbPrix5 ----
          lbPrix5.setText("Prix num\u00e9ro 5");
          lbPrix5.setName("lbPrix5");
          panel2.add(lbPrix5);
          lbPrix5.setBounds(20, 157, 95, 21);

          //---- lbPrix6 ----
          lbPrix6.setText("Prix num\u00e9ro 6");
          lbPrix6.setName("lbPrix6");
          panel2.add(lbPrix6);
          lbPrix6.setBounds(20, 183, 95, 21);

          //---- lbPrix7 ----
          lbPrix7.setText("Prix num\u00e9ro 7");
          lbPrix7.setName("lbPrix7");
          panel2.add(lbPrix7);
          lbPrix7.setBounds(20, 209, 95, 21);

          //---- lbPrix8 ----
          lbPrix8.setText("Prix num\u00e9ro 8");
          lbPrix8.setName("lbPrix8");
          panel2.add(lbPrix8);
          lbPrix8.setBounds(20, 235, 95, 21);

          //---- lbPrix9 ----
          lbPrix9.setText("Prix num\u00e9ro 9");
          lbPrix9.setName("lbPrix9");
          panel2.add(lbPrix9);
          lbPrix9.setBounds(20, 261, 95, 21);

          //---- lbPrix10 ----
          lbPrix10.setText("Prix num\u00e9ro 10");
          lbPrix10.setName("lbPrix10");
          panel2.add(lbPrix10);
          lbPrix10.setBounds(20, 287, 95, 21);

          //---- LB1 ----
          LB1.setText("@LB1@");
          LB1.setName("LB1");
          panel2.add(LB1);
          LB1.setBounds(140, 51, 110, LB1.getPreferredSize().height);

          //---- LB2 ----
          LB2.setText("@LB2@");
          LB2.setName("LB2");
          panel2.add(LB2);
          LB2.setBounds(140, 77, 110, LB2.getPreferredSize().height);

          //---- LB3 ----
          LB3.setText("@LB3@");
          LB3.setName("LB3");
          panel2.add(LB3);
          LB3.setBounds(140, 103, 110, LB3.getPreferredSize().height);

          //---- LB4 ----
          LB4.setText("@LB4@");
          LB4.setName("LB4");
          panel2.add(LB4);
          LB4.setBounds(140, 129, 110, LB4.getPreferredSize().height);

          //---- LB5 ----
          LB5.setText("@LB5@");
          LB5.setName("LB5");
          panel2.add(LB5);
          LB5.setBounds(140, 155, 110, LB5.getPreferredSize().height);

          //---- LB6 ----
          LB6.setText("@LB6@");
          LB6.setName("LB6");
          panel2.add(LB6);
          LB6.setBounds(140, 181, 110, LB6.getPreferredSize().height);

          //---- LB7 ----
          LB7.setText("@LB7@");
          LB7.setName("LB7");
          panel2.add(LB7);
          LB7.setBounds(140, 207, 110, LB7.getPreferredSize().height);

          //---- LB8 ----
          LB8.setText("@LB8@");
          LB8.setName("LB8");
          panel2.add(LB8);
          LB8.setBounds(140, 233, 110, LB8.getPreferredSize().height);

          //---- LB9 ----
          LB9.setText("@LB9@");
          LB9.setName("LB9");
          panel2.add(LB9);
          LB9.setBounds(140, 259, 110, LB9.getPreferredSize().height);

          //---- LB10 ----
          LB10.setText("@LB10@");
          LB10.setName("LB10");
          panel2.add(LB10);
          LB10.setBounds(140, 285, 110, LB10.getPreferredSize().height);

          //---- CO1 ----
          CO1.setName("CO1");
          panel2.add(CO1);
          CO1.setBounds(295, 49, 60, CO1.getPreferredSize().height);

          //---- CO2 ----
          CO2.setName("CO2");
          panel2.add(CO2);
          CO2.setBounds(295, 75, 60, CO2.getPreferredSize().height);

          //---- CO3 ----
          CO3.setName("CO3");
          panel2.add(CO3);
          CO3.setBounds(295, 101, 60, CO3.getPreferredSize().height);

          //---- CO4 ----
          CO4.setName("CO4");
          panel2.add(CO4);
          CO4.setBounds(295, 127, 60, CO4.getPreferredSize().height);

          //---- CO5 ----
          CO5.setName("CO5");
          panel2.add(CO5);
          CO5.setBounds(295, 153, 60, CO5.getPreferredSize().height);

          //---- CO6 ----
          CO6.setName("CO6");
          panel2.add(CO6);
          CO6.setBounds(295, 179, 60, CO6.getPreferredSize().height);

          //---- CO7 ----
          CO7.setName("CO7");
          panel2.add(CO7);
          CO7.setBounds(295, 205, 60, CO7.getPreferredSize().height);

          //---- CO8 ----
          CO8.setName("CO8");
          panel2.add(CO8);
          CO8.setBounds(295, 231, 60, CO8.getPreferredSize().height);

          //---- CO9 ----
          CO9.setName("CO9");
          panel2.add(CO9);
          CO9.setBounds(295, 257, 60, CO9.getPreferredSize().height);

          //---- CO10 ----
          CO10.setName("CO10");
          panel2.add(CO10);
          CO10.setBounds(295, 283, 60, CO10.getPreferredSize().height);

          //---- lbTitre1 ----
          lbTitre1.setText("Prix");
          lbTitre1.setFont(lbTitre1.getFont().deriveFont(lbTitre1.getFont().getStyle() | Font.BOLD));
          lbTitre1.setName("lbTitre1");
          panel2.add(lbTitre1);
          lbTitre1.setBounds(20, 30, 95, 21);

          //---- lbTitre2 ----
          lbTitre2.setText("Libell\u00e9");
          lbTitre2.setFont(lbTitre2.getFont().deriveFont(lbTitre2.getFont().getStyle() | Font.BOLD));
          lbTitre2.setName("lbTitre2");
          panel2.add(lbTitre2);
          lbTitre2.setBounds(140, 30, 110, 21);

          //---- lbTitre3 ----
          lbTitre3.setText("Co\u00e9fficient");
          lbTitre3.setFont(lbTitre3.getFont().deriveFont(lbTitre3.getFont().getStyle() | Font.BOLD));
          lbTitre3.setHorizontalAlignment(SwingConstants.CENTER);
          lbTitre3.setName("lbTitre3");
          panel2.add(lbTitre3);
          lbTitre3.setBounds(280, 30, 95, 21);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 396, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 354, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiCheckBox MULTP;
  private XRiCheckBox MULTC;
  private JLabel lbPrix01;
  private JLabel lbPrix2;
  private JLabel lbPrix3;
  private JLabel lbPrix4;
  private JLabel lbPrix5;
  private JLabel lbPrix6;
  private JLabel lbPrix7;
  private JLabel lbPrix8;
  private JLabel lbPrix9;
  private JLabel lbPrix10;
  private RiZoneSortie LB1;
  private RiZoneSortie LB2;
  private RiZoneSortie LB3;
  private RiZoneSortie LB4;
  private RiZoneSortie LB5;
  private RiZoneSortie LB6;
  private RiZoneSortie LB7;
  private RiZoneSortie LB8;
  private RiZoneSortie LB9;
  private RiZoneSortie LB10;
  private XRiTextField CO1;
  private XRiTextField CO2;
  private XRiTextField CO3;
  private XRiTextField CO4;
  private XRiTextField CO5;
  private XRiTextField CO6;
  private XRiTextField CO7;
  private XRiTextField CO8;
  private XRiTextField CO9;
  private XRiTextField CO10;
  private JLabel lbTitre1;
  private JLabel lbTitre2;
  private JLabel lbTitre3;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
