
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerListModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleCommentaire;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_CC extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] CCARTC_Value = { "", "O", "D", };
  private String[] CCTXA_Value = { "", "O", };
  private String[] CCGLA_Value = { "", "1", "2", };
  private String[] CCGSP_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8", };
  private String[] CCPRP_Value = { "", "1", "2", };
  private String[] CCEREM_Value = { "", "1", };
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  // is modif ?
  private boolean isConsultation = false;
  
  public VGVM01FX_CC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CCARTC.setValeurs(CCARTC_Value, null);
    CCTXA.setValeurs(CCTXA_Value, null);
    CCGLA.setValeurs(CCGLA_Value, null);
    CCGSP.setValeurs(CCGSP_Value, null);
    CCEREM.setValeurs(CCEREM_Value, null);
    CCARTI.setValeursSelection("1", " ");
    CCGCD.setValeursSelection("1", " ");
    CCDEVC.setValeursSelection("O", " ");
    CCMRP.setValeursSelection("1", "");
    CCPRP.setValeurs(CCPRP_Value, null);
    CCREFC.setValeursSelection("1", "");
    CCVEH.setValeursSelection("1", "");
    CCCHAN.setValeursSelection("1", "");
    CCACO.setValeursSelection("1", "");
    CCEXN.setValeursSelection("1", "");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    // Initialisation des composants snArticle ****************************
    
    // article frais
    snArticleCCAFC.setSession(getSession());
    snArticleCCAFC.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCAFC.charger(false);
    snArticleCCAFC.setSelectionParChampRPG(lexique, "CCAFC");
    snArticleCCAFC.setEnabled(!isConsultation);
    
    // article compte épargne
    snArticleCCART.setSession(getSession());
    snArticleCCART.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCART.charger(false);
    snArticleCCART.setSelectionParChampRPG(lexique, "CCART");
    snArticleCCART.setEnabled(!isConsultation);
    
    // Articles générés sur bon
    snArticleCCART1.setSession(getSession());
    snArticleCCART1.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCART1.setFiltreArticleCommentaire(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES);
    snArticleCCART1.charger(false);
    snArticleCCART1.setSelectionParChampRPG(lexique, "CCART1");
    snArticleCCART1.setEnabled(!isConsultation);
    
    snArticleCCART2.setSession(getSession());
    snArticleCCART2.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCART2.setFiltreArticleCommentaire(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES);
    snArticleCCART2.charger(false);
    snArticleCCART2.setSelectionParChampRPG(lexique, "CCART2");
    snArticleCCART2.setEnabled(!isConsultation);
    
    snArticleCCART3.setSession(getSession());
    snArticleCCART3.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCART3.setFiltreArticleCommentaire(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES);
    snArticleCCART3.charger(false);
    snArticleCCART3.setSelectionParChampRPG(lexique, "CCART3");
    snArticleCCART3.setEnabled(!isConsultation);
    
    snArticleCCART4.setSession(getSession());
    snArticleCCART4.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCART4.setFiltreArticleCommentaire(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES);
    snArticleCCART4.charger(false);
    snArticleCCART4.setSelectionParChampRPG(lexique, "CCART4");
    snArticleCCART4.setEnabled(!isConsultation);
    
    snArticleCCART5.setSession(getSession());
    snArticleCCART5.setIdEtablissement(snEtablissement.getIdSelection());
    snArticleCCART5.setFiltreArticleCommentaire(EnumFiltreArticleCommentaire.OPTION_TOUS_LES_ARTICLES);
    snArticleCCART5.charger(false);
    snArticleCCART5.setSelectionParChampRPG(lexique, "CCART5");
    snArticleCCART5.setEnabled(!isConsultation);
    
    // Initialisation composant section analytique
    snSectionAnalytiqueCCSAN.setSession(getSession());
    snSectionAnalytiqueCCSAN.setIdEtablissement(snEtablissement.getIdSelection());
    snSectionAnalytiqueCCSAN.charger(false);
    snSectionAnalytiqueCCSAN.setSelectionParChampRPG(lexique, "CCSAN");
    snSectionAnalytiqueCCSAN.setEnabled(!isConsultation);
    
    // Initialisation composant client de référence en création
    snClientCCCLR.setSession(getSession());
    snClientCCCLR.setIdEtablissement(snEtablissement.getIdSelection());
    snClientCCCLR.charger(false);
    snClientCCCLR.setSelectionParChampRPG(lexique, "CCCLR");
    snClientCCCLR.setEnabled(!isConsultation);
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    if (snEtablissement.getIdSelection() != null) {
      p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snArticleCCAFC.renseignerChampRPG(lexique, "CCAFC");
    snArticleCCART.renseignerChampRPG(lexique, "CCART");
    snArticleCCART1.renseignerChampRPG(lexique, "CCART1");
    snArticleCCART2.renseignerChampRPG(lexique, "CCART2");
    snArticleCCART3.renseignerChampRPG(lexique, "CCART3");
    snArticleCCART4.renseignerChampRPG(lexique, "CCART4");
    snArticleCCART5.renseignerChampRPG(lexique, "CCART5");
    snSectionAnalytiqueCCSAN.renseignerChampRPG(lexique, "CCSAN");
    snClientCCCLR.renseignerChampRPG(lexique, "CCCLR");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new SNPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanel();
    pnlContenu = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    lbLibelle = new SNLabelChamp();
    CCLIB = new XRiTextField();
    tpDonnees = new JTabbedPane();
    pnlOptions = new SNPanelContenu();
    pnlOptionCocher = new SNPanel();
    pnlOptionGauche = new SNPanel();
    CCREFC = new XRiCheckBox();
    CCCHAN = new XRiCheckBox();
    CCVEH = new XRiCheckBox();
    pnlOptionCentre = new SNPanel();
    CCEXN = new XRiCheckBox();
    CCDEVC = new XRiCheckBox();
    pnlOptionDroite = new SNPanel();
    CCGCD = new XRiCheckBox();
    CCMRP = new XRiCheckBox();
    lbClientReference = new SNLabelChamp();
    snClientCCCLR = new SNClientPrincipal();
    lbPrisPar = new SNLabelChamp();
    CCPRP = new XRiComboBox();
    lbAlerteLigne = new SNLabelChamp();
    CCARTC = new XRiComboBox();
    lbTraitementSpecifique = new SNLabelChamp();
    CCGSP = new XRiComboBox();
    lbFormatPersonnalise = new SNLabelChamp();
    CCFMT = new XRiTextField();
    lbScoring = new SNLabelChamp();
    CCSCO = new XRiTextField();
    pnlFinance = new SNPanelContenu();
    lbMargeMini = new SNLabelChamp();
    pnlMarge = new SNPanel();
    CCMMI = new XRiTextField();
    lbPourcentage2 = new SNLabelUnite();
    lbMargeMaxi = new SNLabelChamp();
    CCMMX = new XRiTextField();
    lbPourcentage3 = new SNLabelUnite();
    lbMontantMini = new SNLabelChamp();
    CCMIC = new XRiTextField();
    lbArticleFrais = new SNLabelChamp();
    snArticleCCAFC = new SNArticle();
    lbMontantInferieur = new SNLabelChamp();
    CCMFC = new XRiTextField();
    lbPlafond = new SNLabelChamp();
    CCPLE = new XRiTextField();
    lbAcompte = new SNLabelChamp();
    pnlAcompte = new SNPanel();
    CCRGL = new XRiTextField();
    lbPourcentage = new SNLabelUnite();
    CCACO = new XRiCheckBox();
    lbAcompte2 = new SNLabelChamp();
    pnlAcompte2 = new SNPanel();
    CCRGLS = new XRiTextField();
    lbPourcentage4 = new SNLabelUnite();
    CCACOS = new XRiCheckBox();
    lbBlocageExpedition = new SNLabelChamp();
    CCNRM = new XRiSpinner();
    lbNonEditionAffacturage = new SNLabelChamp();
    CCTXA = new XRiComboBox();
    lbConditionVente = new SNLabelChamp();
    CCCNV = new XRiTextField();
    lbFormulePRV = new SNLabelChamp();
    CCFPR = new XRiTextField();
    lbSection = new SNLabelChamp();
    snSectionAnalytiqueCCSAN = new SNSectionAnalytique();
    lbEditionRemises = new SNLabelChamp();
    CCEREM = new XRiComboBox();
    lbArticleCompteEpargne = new SNLabelChamp();
    snArticleCCART = new SNArticle();
    pnlGenerationBon = new SNPanelContenu();
    CCARTI = new XRiCheckBox();
    lbArticleGenere1 = new SNLabelChamp();
    snArticleCCART1 = new SNArticle();
    lbArticleGenere2 = new SNLabelChamp();
    snArticleCCART2 = new SNArticle();
    lbArticleGenere3 = new SNLabelChamp();
    snArticleCCART3 = new SNArticle();
    lbArticleGenere4 = new SNLabelChamp();
    snArticleCCART4 = new SNArticle();
    lbArticleGenere5 = new SNLabelChamp();
    snArticleCCART5 = new SNArticle();
    lbGenerationCommentaire = new SNLabelChamp();
    CCGLA = new XRiComboBox();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1200, 650));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      pnlBandeau.add(p_bpresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlPersonnalisation ========
        {
          pnlPersonnalisation.setName("pnlPersonnalisation");
          pnlPersonnalisation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlPersonnalisation.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlPersonnalisation.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlPersonnalisation.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlPersonnalisation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbCodeCategorie ----
          lbCodeCategorie.setText("code");
          lbCodeCategorie.setName("lbCodeCategorie");
          pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- INDIND ----
          INDIND.setPreferredSize(new Dimension(40, 30));
          INDIND.setMinimumSize(new Dimension(40, 30));
          INDIND.setMaximumSize(new Dimension(40, 30));
          INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDIND.setName("INDIND");
          pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbLibelle ----
          lbLibelle.setText("Libell\u00e9");
          lbLibelle.setMinimumSize(new Dimension(80, 30));
          lbLibelle.setMaximumSize(new Dimension(80, 30));
          lbLibelle.setPreferredSize(new Dimension(80, 30));
          lbLibelle.setName("lbLibelle");
          pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- CCLIB ----
          CCLIB.setPreferredSize(new Dimension(400, 30));
          CCLIB.setMinimumSize(new Dimension(400, 30));
          CCLIB.setMaximumSize(new Dimension(400, 30));
          CCLIB.setFont(new Font("sansserif", Font.PLAIN, 14));
          CCLIB.setName("CCLIB");
          pnlPersonnalisation.add(CCLIB, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlPersonnalisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== tpDonnees ========
        {
          tpDonnees.setFont(new Font("sansserif", Font.PLAIN, 14));
          tpDonnees.setName("tpDonnees");

          //======== pnlOptions ========
          {
            pnlOptions.setName("pnlOptions");
            pnlOptions.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlOptions.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlOptions.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlOptions.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlOptions.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //======== pnlOptionCocher ========
            {
              pnlOptionCocher.setName("pnlOptionCocher");
              pnlOptionCocher.setLayout(new GridLayout(1, 3, 5, 5));

              //======== pnlOptionGauche ========
              {
                pnlOptionGauche.setName("pnlOptionGauche");
                pnlOptionGauche.setLayout(new GridBagLayout());
                ((GridBagLayout)pnlOptionGauche.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)pnlOptionGauche.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
                ((GridBagLayout)pnlOptionGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)pnlOptionGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

                //---- CCREFC ----
                CCREFC.setText("R\u00e9f\u00e9rence de commande obligatoire");
                CCREFC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCREFC.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCREFC.setPreferredSize(new Dimension(250, 30));
                CCREFC.setMinimumSize(new Dimension(250, 30));
                CCREFC.setMaximumSize(new Dimension(250, 30));
                CCREFC.setName("CCREFC");
                pnlOptionGauche.add(CCREFC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));

                //---- CCCHAN ----
                CCCHAN.setText("Chantier obligatoire");
                CCCHAN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCCHAN.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCCHAN.setPreferredSize(new Dimension(250, 30));
                CCCHAN.setMinimumSize(new Dimension(250, 30));
                CCCHAN.setMaximumSize(new Dimension(250, 30));
                CCCHAN.setName("CCCHAN");
                pnlOptionGauche.add(CCCHAN, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));

                //---- CCVEH ----
                CCVEH.setText("V\u00e9hicule obligatoire");
                CCVEH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCVEH.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCVEH.setPreferredSize(new Dimension(250, 30));
                CCVEH.setMinimumSize(new Dimension(250, 30));
                CCVEH.setMaximumSize(new Dimension(250, 30));
                CCVEH.setName("CCVEH");
                pnlOptionGauche.add(CCVEH, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlOptionCocher.add(pnlOptionGauche);

              //======== pnlOptionCentre ========
              {
                pnlOptionCentre.setName("pnlOptionCentre");
                pnlOptionCentre.setLayout(new GridBagLayout());
                ((GridBagLayout)pnlOptionCentre.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)pnlOptionCentre.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
                ((GridBagLayout)pnlOptionCentre.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)pnlOptionCentre.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

                //---- CCEXN ----
                CCEXN.setText("Exp\u00e9ditions non facturables");
                CCEXN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCEXN.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCEXN.setPreferredSize(new Dimension(250, 30));
                CCEXN.setMinimumSize(new Dimension(250, 30));
                CCEXN.setMaximumSize(new Dimension(250, 30));
                CCEXN.setName("CCEXN");
                pnlOptionCentre.add(CCEXN, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));

                //---- CCDEVC ----
                CCDEVC.setText("Alerte sur bon : devis en cours");
                CCDEVC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCDEVC.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCDEVC.setPreferredSize(new Dimension(250, 30));
                CCDEVC.setMinimumSize(new Dimension(250, 30));
                CCDEVC.setMaximumSize(new Dimension(250, 30));
                CCDEVC.setName("CCDEVC");
                pnlOptionCentre.add(CCDEVC, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              }
              pnlOptionCocher.add(pnlOptionCentre);

              //======== pnlOptionDroite ========
              {
                pnlOptionDroite.setName("pnlOptionDroite");
                pnlOptionDroite.setLayout(new GridBagLayout());
                ((GridBagLayout)pnlOptionDroite.getLayout()).columnWidths = new int[] {0, 0};
                ((GridBagLayout)pnlOptionDroite.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
                ((GridBagLayout)pnlOptionDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
                ((GridBagLayout)pnlOptionDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

                //---- CCGCD ----
                CCGCD.setText("Non \u00e9dition du gencod");
                CCGCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCGCD.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCGCD.setPreferredSize(new Dimension(250, 30));
                CCGCD.setMinimumSize(new Dimension(250, 30));
                CCGCD.setMaximumSize(new Dimension(250, 30));
                CCGCD.setName("CCGCD");
                pnlOptionDroite.add(CCGCD, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));

                //---- CCMRP ----
                CCMRP.setText("Regroupement des pr\u00e9visions MRP");
                CCMRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                CCMRP.setFont(new Font("sansserif", Font.PLAIN, 14));
                CCMRP.setPreferredSize(new Dimension(250, 30));
                CCMRP.setMinimumSize(new Dimension(250, 30));
                CCMRP.setMaximumSize(new Dimension(250, 30));
                CCMRP.setName("CCMRP");
                pnlOptionDroite.add(CCMRP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              }
              pnlOptionCocher.add(pnlOptionDroite);
            }
            pnlOptions.add(pnlOptionCocher, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbClientReference ----
            lbClientReference.setText("Client de r\u00e9f\u00e9rence en cr\u00e9ation");
            lbClientReference.setName("lbClientReference");
            pnlOptions.add(lbClientReference, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snClientCCCLR ----
            snClientCCCLR.setComponentPopupMenu(null);
            snClientCCCLR.setFont(new Font("sansserif", Font.PLAIN, 14));
            snClientCCCLR.setName("snClientCCCLR");
            pnlOptions.add(snClientCCCLR, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbPrisPar ----
            lbPrisPar.setText("\"Pris par\"");
            lbPrisPar.setMinimumSize(new Dimension(250, 30));
            lbPrisPar.setMaximumSize(new Dimension(250, 30));
            lbPrisPar.setPreferredSize(new Dimension(250, 30));
            lbPrisPar.setName("lbPrisPar");
            pnlOptions.add(lbPrisPar, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCPRP ----
            CCPRP.setModel(new DefaultComboBoxModel(new String[] {
              "Non obligatoire",
              "Obligatoire avec Gencod",
              "Obligatoire avec contact"
            }));
            CCPRP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCPRP.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCPRP.setMaximumSize(new Dimension(250, 30));
            CCPRP.setMinimumSize(new Dimension(250, 30));
            CCPRP.setPreferredSize(new Dimension(250, 30));
            CCPRP.setName("CCPRP");
            pnlOptions.add(CCPRP, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbAlerteLigne ----
            lbAlerteLigne.setText("Alertes sur lignes");
            lbAlerteLigne.setMinimumSize(new Dimension(250, 30));
            lbAlerteLigne.setMaximumSize(new Dimension(250, 30));
            lbAlerteLigne.setPreferredSize(new Dimension(250, 30));
            lbAlerteLigne.setName("lbAlerteLigne");
            pnlOptions.add(lbAlerteLigne, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCARTC ----
            CCARTC.setModel(new DefaultComboBoxModel(new String[] {
              "Aucune",
              "Article d\u00e9j\u00e0 en commande",
              "Article en commande ou sur devis"
            }));
            CCARTC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCARTC.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCARTC.setMaximumSize(new Dimension(250, 30));
            CCARTC.setMinimumSize(new Dimension(250, 30));
            CCARTC.setPreferredSize(new Dimension(250, 30));
            CCARTC.setName("CCARTC");
            pnlOptions.add(CCARTC, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbTraitementSpecifique ----
            lbTraitementSpecifique.setText("Traitement par programme sp\u00e9cifique");
            lbTraitementSpecifique.setMinimumSize(new Dimension(250, 30));
            lbTraitementSpecifique.setMaximumSize(new Dimension(250, 30));
            lbTraitementSpecifique.setPreferredSize(new Dimension(250, 30));
            lbTraitementSpecifique.setName("lbTraitementSpecifique");
            pnlOptions.add(lbTraitementSpecifique, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCGSP ----
            CCGSP.setModel(new DefaultComboBoxModel(new String[] {
              "Aucun",
              "Ent\u00eate bon de vente",
              "Pied bon de vente",
              "Ent\u00eate et pied",
              "Fiche client",
              "Ent\u00eate bon de vente + fiche client",
              "Pied bon de vente + fiche client",
              "Ent\u00eate + pied + fiche client",
              "Avant \u00e9dition bon"
            }));
            CCGSP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCGSP.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCGSP.setMaximumSize(new Dimension(250, 30));
            CCGSP.setMinimumSize(new Dimension(250, 30));
            CCGSP.setPreferredSize(new Dimension(250, 30));
            CCGSP.setName("CCGSP");
            pnlOptions.add(CCGSP, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbFormatPersonnalise ----
            lbFormatPersonnalise.setText("Format personnalis\u00e9");
            lbFormatPersonnalise.setMinimumSize(new Dimension(250, 30));
            lbFormatPersonnalise.setMaximumSize(new Dimension(250, 30));
            lbFormatPersonnalise.setPreferredSize(new Dimension(250, 30));
            lbFormatPersonnalise.setName("lbFormatPersonnalise");
            pnlOptions.add(lbFormatPersonnalise, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCFMT ----
            CCFMT.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCFMT.setMinimumSize(new Dimension(60, 30));
            CCFMT.setMaximumSize(new Dimension(60, 30));
            CCFMT.setPreferredSize(new Dimension(60, 30));
            CCFMT.setName("CCFMT");
            pnlOptions.add(CCFMT, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbScoring ----
            lbScoring.setText("Scoring client");
            lbScoring.setName("lbScoring");
            pnlOptions.add(lbScoring, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCSCO ----
            CCSCO.setComponentPopupMenu(null);
            CCSCO.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCSCO.setMaximumSize(new Dimension(60, 30));
            CCSCO.setMinimumSize(new Dimension(60, 30));
            CCSCO.setPreferredSize(new Dimension(60, 30));
            CCSCO.setName("CCSCO");
            pnlOptions.add(CCSCO, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          }
          tpDonnees.addTab("G\u00e9n\u00e9ral", pnlOptions);

          //======== pnlFinance ========
          {
            pnlFinance.setName("pnlFinance");
            pnlFinance.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlFinance.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)pnlFinance.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlFinance.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlFinance.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- lbMargeMini ----
            lbMargeMini.setText("Marge minimum autoris\u00e9e");
            lbMargeMini.setName("lbMargeMini");
            pnlFinance.add(lbMargeMini, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== pnlMarge ========
            {
              pnlMarge.setName("pnlMarge");
              pnlMarge.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlMarge.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0};
              ((GridBagLayout)pnlMarge.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlMarge.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlMarge.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- CCMMI ----
              CCMMI.setComponentPopupMenu(null);
              CCMMI.setPreferredSize(new Dimension(70, 30));
              CCMMI.setMinimumSize(new Dimension(70, 30));
              CCMMI.setMaximumSize(new Dimension(70, 30));
              CCMMI.setFont(new Font("sansserif", Font.PLAIN, 14));
              CCMMI.setName("CCMMI");
              pnlMarge.add(CCMMI, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbPourcentage2 ----
              lbPourcentage2.setText("%");
              lbPourcentage2.setName("lbPourcentage2");
              pnlMarge.add(lbPourcentage2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbMargeMaxi ----
              lbMargeMaxi.setText("maximum");
              lbMargeMaxi.setPreferredSize(new Dimension(70, 30));
              lbMargeMaxi.setMinimumSize(new Dimension(70, 30));
              lbMargeMaxi.setMaximumSize(new Dimension(70, 30));
              lbMargeMaxi.setName("lbMargeMaxi");
              pnlMarge.add(lbMargeMaxi, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CCMMX ----
              CCMMX.setComponentPopupMenu(null);
              CCMMX.setPreferredSize(new Dimension(70, 30));
              CCMMX.setMinimumSize(new Dimension(70, 30));
              CCMMX.setMaximumSize(new Dimension(70, 30));
              CCMMX.setFont(new Font("sansserif", Font.PLAIN, 14));
              CCMMX.setName("CCMMX");
              pnlMarge.add(CCMMX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbPourcentage3 ----
              lbPourcentage3.setText("%");
              lbPourcentage3.setMinimumSize(new Dimension(20, 30));
              lbPourcentage3.setMaximumSize(new Dimension(20, 30));
              lbPourcentage3.setPreferredSize(new Dimension(20, 30));
              lbPourcentage3.setName("lbPourcentage3");
              pnlMarge.add(lbPourcentage3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFinance.add(pnlMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbMontantMini ----
            lbMontantMini.setText("Montant minimum obligatoire pour une commande");
            lbMontantMini.setMinimumSize(new Dimension(300, 30));
            lbMontantMini.setMaximumSize(new Dimension(300, 30));
            lbMontantMini.setPreferredSize(new Dimension(300, 30));
            lbMontantMini.setName("lbMontantMini");
            pnlFinance.add(lbMontantMini, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCMIC ----
            CCMIC.setMaximumSize(new Dimension(60, 30));
            CCMIC.setMinimumSize(new Dimension(60, 30));
            CCMIC.setPreferredSize(new Dimension(60, 30));
            CCMIC.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCMIC.setName("CCMIC");
            pnlFinance.add(CCMIC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbArticleFrais ----
            lbArticleFrais.setText("Article frais");
            lbArticleFrais.setMinimumSize(new Dimension(350, 30));
            lbArticleFrais.setMaximumSize(new Dimension(350, 30));
            lbArticleFrais.setPreferredSize(new Dimension(350, 30));
            lbArticleFrais.setName("lbArticleFrais");
            pnlFinance.add(lbArticleFrais, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticleCCAFC ----
            snArticleCCAFC.setName("snArticleCCAFC");
            pnlFinance.add(snArticleCCAFC, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbMontantInferieur ----
            lbMontantInferieur.setText("si montant inf\u00e9rieur \u00e0");
            lbMontantInferieur.setName("lbMontantInferieur");
            pnlFinance.add(lbMontantInferieur, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCMFC ----
            CCMFC.setMaximumSize(new Dimension(60, 30));
            CCMFC.setMinimumSize(new Dimension(60, 30));
            CCMFC.setPreferredSize(new Dimension(60, 30));
            CCMFC.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCMFC.setName("CCMFC");
            pnlFinance.add(CCMFC, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbPlafond ----
            lbPlafond.setText("Plafond d'encours par d\u00e9faut");
            lbPlafond.setName("lbPlafond");
            pnlFinance.add(lbPlafond, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCPLE ----
            CCPLE.setComponentPopupMenu(null);
            CCPLE.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCPLE.setPreferredSize(new Dimension(80, 30));
            CCPLE.setMinimumSize(new Dimension(80, 30));
            CCPLE.setMaximumSize(new Dimension(80, 30));
            CCPLE.setName("CCPLE");
            pnlFinance.add(CCPLE, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbAcompte ----
            lbAcompte.setText("Acompte");
            lbAcompte.setMinimumSize(new Dimension(250, 30));
            lbAcompte.setMaximumSize(new Dimension(250, 30));
            lbAcompte.setPreferredSize(new Dimension(250, 30));
            lbAcompte.setName("lbAcompte");
            pnlFinance.add(lbAcompte, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== pnlAcompte ========
            {
              pnlAcompte.setName("pnlAcompte");
              pnlAcompte.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlAcompte.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
              ((GridBagLayout)pnlAcompte.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlAcompte.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlAcompte.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- CCRGL ----
              CCRGL.setComponentPopupMenu(null);
              CCRGL.setPreferredSize(new Dimension(40, 30));
              CCRGL.setMinimumSize(new Dimension(40, 30));
              CCRGL.setMaximumSize(new Dimension(40, 30));
              CCRGL.setFont(new Font("sansserif", Font.PLAIN, 14));
              CCRGL.setName("CCRGL");
              pnlAcompte.add(CCRGL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbPourcentage ----
              lbPourcentage.setText("%");
              lbPourcentage.setName("lbPourcentage");
              pnlAcompte.add(lbPourcentage, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CCACO ----
              CCACO.setText("Acompte obligatoire");
              CCACO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CCACO.setFont(new Font("sansserif", Font.PLAIN, 14));
              CCACO.setName("CCACO");
              pnlAcompte.add(CCACO, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFinance.add(pnlAcompte, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbAcompte2 ----
            lbAcompte2.setText("Acompte documents de ventes avec articles sp\u00e9ciaux");
            lbAcompte2.setMinimumSize(new Dimension(250, 30));
            lbAcompte2.setMaximumSize(new Dimension(250, 30));
            lbAcompte2.setPreferredSize(new Dimension(250, 30));
            lbAcompte2.setName("lbAcompte2");
            pnlFinance.add(lbAcompte2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //======== pnlAcompte2 ========
            {
              pnlAcompte2.setName("pnlAcompte2");
              pnlAcompte2.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlAcompte2.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
              ((GridBagLayout)pnlAcompte2.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlAcompte2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlAcompte2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- CCRGLS ----
              CCRGLS.setComponentPopupMenu(null);
              CCRGLS.setPreferredSize(new Dimension(40, 30));
              CCRGLS.setMinimumSize(new Dimension(40, 30));
              CCRGLS.setMaximumSize(new Dimension(40, 30));
              CCRGLS.setFont(new Font("sansserif", Font.PLAIN, 14));
              CCRGLS.setName("CCRGLS");
              pnlAcompte2.add(CCRGLS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbPourcentage4 ----
              lbPourcentage4.setText("%");
              lbPourcentage4.setName("lbPourcentage4");
              pnlAcompte2.add(lbPourcentage4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- CCACOS ----
              CCACOS.setText("Acompte obligatoire");
              CCACOS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CCACOS.setFont(new Font("sansserif", Font.PLAIN, 14));
              CCACOS.setName("CCACOS");
              pnlAcompte2.add(CCACOS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFinance.add(pnlAcompte2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbBlocageExpedition ----
            lbBlocageExpedition.setText("Exp\u00e9ditions bloqu\u00e9es \u00e0 partir du niveau de relance");
            lbBlocageExpedition.setMinimumSize(new Dimension(300, 30));
            lbBlocageExpedition.setMaximumSize(new Dimension(300, 30));
            lbBlocageExpedition.setPreferredSize(new Dimension(300, 30));
            lbBlocageExpedition.setName("lbBlocageExpedition");
            pnlFinance.add(lbBlocageExpedition, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCNRM ----
            CCNRM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCNRM.setModel(new SpinnerListModel(new String[] {"", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
            CCNRM.setPreferredSize(new Dimension(50, 30));
            CCNRM.setMinimumSize(new Dimension(50, 30));
            CCNRM.setMaximumSize(new Dimension(50, 30));
            CCNRM.setName("CCNRM");
            pnlFinance.add(CCNRM, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbNonEditionAffacturage ----
            lbNonEditionAffacturage.setText("Edition du libell\u00e9 d'affacturage");
            lbNonEditionAffacturage.setMinimumSize(new Dimension(250, 30));
            lbNonEditionAffacturage.setMaximumSize(new Dimension(250, 30));
            lbNonEditionAffacturage.setPreferredSize(new Dimension(250, 30));
            lbNonEditionAffacturage.setName("lbNonEditionAffacturage");
            pnlFinance.add(lbNonEditionAffacturage, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCTXA ----
            CCTXA.setModel(new DefaultComboBoxModel(new String[] {
              "Edition texte affacturage",
              "Non \u00e9dition texte affacturage"
            }));
            CCTXA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCTXA.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCTXA.setPreferredSize(new Dimension(250, 30));
            CCTXA.setMinimumSize(new Dimension(250, 30));
            CCTXA.setMaximumSize(new Dimension(250, 30));
            CCTXA.setName("CCTXA");
            pnlFinance.add(CCTXA, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbConditionVente ----
            lbConditionVente.setText("Condition de vente");
            lbConditionVente.setName("lbConditionVente");
            pnlFinance.add(lbConditionVente, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCCNV ----
            CCCNV.setComponentPopupMenu(BTD);
            CCCNV.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCCNV.setMinimumSize(new Dimension(60, 30));
            CCCNV.setMaximumSize(new Dimension(60, 30));
            CCCNV.setPreferredSize(new Dimension(60, 30));
            CCCNV.setName("CCCNV");
            pnlFinance.add(CCCNV, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbFormulePRV ----
            lbFormulePRV.setText("Formule de calcul du prix de revient");
            lbFormulePRV.setName("lbFormulePRV");
            pnlFinance.add(lbFormulePRV, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCFPR ----
            CCFPR.setComponentPopupMenu(null);
            CCFPR.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCFPR.setPreferredSize(new Dimension(50, 30));
            CCFPR.setMinimumSize(new Dimension(50, 30));
            CCFPR.setName("CCFPR");
            pnlFinance.add(CCFPR, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbSection ----
            lbSection.setText("Section analytique");
            lbSection.setName("lbSection");
            pnlFinance.add(lbSection, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snSectionAnalytiqueCCSAN ----
            snSectionAnalytiqueCCSAN.setComponentPopupMenu(null);
            snSectionAnalytiqueCCSAN.setFont(new Font("sansserif", Font.PLAIN, 14));
            snSectionAnalytiqueCCSAN.setName("snSectionAnalytiqueCCSAN");
            pnlFinance.add(snSectionAnalytiqueCCSAN, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbEditionRemises ----
            lbEditionRemises.setText("Edition des remises");
            lbEditionRemises.setMinimumSize(new Dimension(250, 30));
            lbEditionRemises.setMaximumSize(new Dimension(250, 30));
            lbEditionRemises.setPreferredSize(new Dimension(250, 30));
            lbEditionRemises.setName("lbEditionRemises");
            pnlFinance.add(lbEditionRemises, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- CCEREM ----
            CCEREM.setModel(new DefaultComboBoxModel(new String[] {
              "Edition \u00e9ventuelle",
              "Pas d'\u00e9dition"
            }));
            CCEREM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCEREM.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCEREM.setMaximumSize(new Dimension(250, 30));
            CCEREM.setMinimumSize(new Dimension(250, 30));
            CCEREM.setPreferredSize(new Dimension(250, 30));
            CCEREM.setName("CCEREM");
            pnlFinance.add(CCEREM, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbArticleCompteEpargne ----
            lbArticleCompteEpargne.setText("Article pour compte \u00e9pargne");
            lbArticleCompteEpargne.setMinimumSize(new Dimension(250, 30));
            lbArticleCompteEpargne.setMaximumSize(new Dimension(250, 30));
            lbArticleCompteEpargne.setPreferredSize(new Dimension(250, 30));
            lbArticleCompteEpargne.setName("lbArticleCompteEpargne");
            pnlFinance.add(lbArticleCompteEpargne, new GridBagConstraints(0, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- snArticleCCART ----
            snArticleCCART.setName("snArticleCCART");
            pnlFinance.add(snArticleCCART, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          tpDonnees.addTab("Finance", pnlFinance);

          //======== pnlGenerationBon ========
          {
            pnlGenerationBon.setName("pnlGenerationBon");
            pnlGenerationBon.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlGenerationBon.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlGenerationBon.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlGenerationBon.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlGenerationBon.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- CCARTI ----
            CCARTI.setText("Articles r\u00e9percut\u00e9s sur le bloc-notes client");
            CCARTI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCARTI.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCARTI.setPreferredSize(new Dimension(250, 30));
            CCARTI.setMinimumSize(new Dimension(250, 30));
            CCARTI.setMaximumSize(new Dimension(250, 30));
            CCARTI.setName("CCARTI");
            pnlGenerationBon.add(CCARTI, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbArticleGenere1 ----
            lbArticleGenere1.setText("Article g\u00e9n\u00e9r\u00e9 1");
            lbArticleGenere1.setPreferredSize(new Dimension(250, 30));
            lbArticleGenere1.setMinimumSize(new Dimension(250, 30));
            lbArticleGenere1.setMaximumSize(new Dimension(250, 30));
            lbArticleGenere1.setName("lbArticleGenere1");
            pnlGenerationBon.add(lbArticleGenere1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticleCCART1 ----
            snArticleCCART1.setName("snArticleCCART1");
            pnlGenerationBon.add(snArticleCCART1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbArticleGenere2 ----
            lbArticleGenere2.setText("Article g\u00e9n\u00e9r\u00e9 2");
            lbArticleGenere2.setPreferredSize(new Dimension(250, 30));
            lbArticleGenere2.setMinimumSize(new Dimension(250, 30));
            lbArticleGenere2.setMaximumSize(new Dimension(250, 30));
            lbArticleGenere2.setName("lbArticleGenere2");
            pnlGenerationBon.add(lbArticleGenere2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticleCCART2 ----
            snArticleCCART2.setName("snArticleCCART2");
            pnlGenerationBon.add(snArticleCCART2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbArticleGenere3 ----
            lbArticleGenere3.setText("Article g\u00e9n\u00e9r\u00e9 3");
            lbArticleGenere3.setPreferredSize(new Dimension(250, 30));
            lbArticleGenere3.setMinimumSize(new Dimension(250, 30));
            lbArticleGenere3.setMaximumSize(new Dimension(250, 30));
            lbArticleGenere3.setName("lbArticleGenere3");
            pnlGenerationBon.add(lbArticleGenere3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticleCCART3 ----
            snArticleCCART3.setName("snArticleCCART3");
            pnlGenerationBon.add(snArticleCCART3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbArticleGenere4 ----
            lbArticleGenere4.setText("Article g\u00e9n\u00e9r\u00e9 4");
            lbArticleGenere4.setPreferredSize(new Dimension(250, 30));
            lbArticleGenere4.setMinimumSize(new Dimension(250, 30));
            lbArticleGenere4.setMaximumSize(new Dimension(250, 30));
            lbArticleGenere4.setName("lbArticleGenere4");
            pnlGenerationBon.add(lbArticleGenere4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticleCCART4 ----
            snArticleCCART4.setName("snArticleCCART4");
            pnlGenerationBon.add(snArticleCCART4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbArticleGenere5 ----
            lbArticleGenere5.setText("Article g\u00e9n\u00e9r\u00e9 5");
            lbArticleGenere5.setPreferredSize(new Dimension(250, 30));
            lbArticleGenere5.setMinimumSize(new Dimension(250, 30));
            lbArticleGenere5.setMaximumSize(new Dimension(250, 30));
            lbArticleGenere5.setName("lbArticleGenere5");
            pnlGenerationBon.add(lbArticleGenere5, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snArticleCCART5 ----
            snArticleCCART5.setName("snArticleCCART5");
            pnlGenerationBon.add(snArticleCCART5, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbGenerationCommentaire ----
            lbGenerationCommentaire.setText("G\u00e9n\u00e9ration de ligne commentaire");
            lbGenerationCommentaire.setMinimumSize(new Dimension(250, 30));
            lbGenerationCommentaire.setMaximumSize(new Dimension(250, 30));
            lbGenerationCommentaire.setPreferredSize(new Dimension(250, 30));
            lbGenerationCommentaire.setName("lbGenerationCommentaire");
            pnlGenerationBon.add(lbGenerationCommentaire, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- CCGLA ----
            CCGLA.setModel(new DefaultComboBoxModel(new String[] {
              "Aucune ",
              "G\u00e9n\u00e9ration de ligne de bon depuis les observations client",
              "G\u00e9n\u00e9ration de ligne de bon depuis le bloc notes client"
            }));
            CCGLA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CCGLA.setFont(new Font("sansserif", Font.PLAIN, 14));
            CCGLA.setMinimumSize(new Dimension(400, 30));
            CCGLA.setMaximumSize(new Dimension(400, 30));
            CCGLA.setPreferredSize(new Dimension(400, 30));
            CCGLA.setName("CCGLA");
            pnlGenerationBon.add(CCGLA, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          tpDonnees.addTab("G\u00e9n\u00e9ration sur bons", pnlGenerationBon);
        }
        pnlContenu.add(tpDonnees, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlBandeau;
  private SNBandeauTitre p_bpresentation;
  private SNPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNLabelChamp lbLibelle;
  private XRiTextField CCLIB;
  private JTabbedPane tpDonnees;
  private SNPanelContenu pnlOptions;
  private SNPanel pnlOptionCocher;
  private SNPanel pnlOptionGauche;
  private XRiCheckBox CCREFC;
  private XRiCheckBox CCCHAN;
  private XRiCheckBox CCVEH;
  private SNPanel pnlOptionCentre;
  private XRiCheckBox CCEXN;
  private XRiCheckBox CCDEVC;
  private SNPanel pnlOptionDroite;
  private XRiCheckBox CCGCD;
  private XRiCheckBox CCMRP;
  private SNLabelChamp lbClientReference;
  private SNClientPrincipal snClientCCCLR;
  private SNLabelChamp lbPrisPar;
  private XRiComboBox CCPRP;
  private SNLabelChamp lbAlerteLigne;
  private XRiComboBox CCARTC;
  private SNLabelChamp lbTraitementSpecifique;
  private XRiComboBox CCGSP;
  private SNLabelChamp lbFormatPersonnalise;
  private XRiTextField CCFMT;
  private SNLabelChamp lbScoring;
  private XRiTextField CCSCO;
  private SNPanelContenu pnlFinance;
  private SNLabelChamp lbMargeMini;
  private SNPanel pnlMarge;
  private XRiTextField CCMMI;
  private SNLabelUnite lbPourcentage2;
  private SNLabelChamp lbMargeMaxi;
  private XRiTextField CCMMX;
  private SNLabelUnite lbPourcentage3;
  private SNLabelChamp lbMontantMini;
  private XRiTextField CCMIC;
  private SNLabelChamp lbArticleFrais;
  private SNArticle snArticleCCAFC;
  private SNLabelChamp lbMontantInferieur;
  private XRiTextField CCMFC;
  private SNLabelChamp lbPlafond;
  private XRiTextField CCPLE;
  private SNLabelChamp lbAcompte;
  private SNPanel pnlAcompte;
  private XRiTextField CCRGL;
  private SNLabelUnite lbPourcentage;
  private XRiCheckBox CCACO;
  private SNLabelChamp lbAcompte2;
  private SNPanel pnlAcompte2;
  private XRiTextField CCRGLS;
  private SNLabelUnite lbPourcentage4;
  private XRiCheckBox CCACOS;
  private SNLabelChamp lbBlocageExpedition;
  private XRiSpinner CCNRM;
  private SNLabelChamp lbNonEditionAffacturage;
  private XRiComboBox CCTXA;
  private SNLabelChamp lbConditionVente;
  private XRiTextField CCCNV;
  private SNLabelChamp lbFormulePRV;
  private XRiTextField CCFPR;
  private SNLabelChamp lbSection;
  private SNSectionAnalytique snSectionAnalytiqueCCSAN;
  private SNLabelChamp lbEditionRemises;
  private XRiComboBox CCEREM;
  private SNLabelChamp lbArticleCompteEpargne;
  private SNArticle snArticleCCART;
  private SNPanelContenu pnlGenerationBon;
  private XRiCheckBox CCARTI;
  private SNLabelChamp lbArticleGenere1;
  private SNArticle snArticleCCART1;
  private SNLabelChamp lbArticleGenere2;
  private SNArticle snArticleCCART2;
  private SNLabelChamp lbArticleGenere3;
  private SNArticle snArticleCCART3;
  private SNLabelChamp lbArticleGenere4;
  private SNArticle snArticleCCART4;
  private SNLabelChamp lbArticleGenere5;
  private SNArticle snArticleCCART5;
  private SNLabelChamp lbGenerationCommentaire;
  private XRiComboBox CCGLA;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
