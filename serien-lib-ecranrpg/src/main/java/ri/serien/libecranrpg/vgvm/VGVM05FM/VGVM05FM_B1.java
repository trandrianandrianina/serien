
package ri.serien.libecranrpg.vgvm.VGVM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM05FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] T1TCD_Value = { "", "N", "B", "R", "K", "F", "+", "-", };
  private String[] T2TCD_Value = { "", "N", "B", "R", "K", "F", "+", "-", };
  
  public VGVM05FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T1TCD.setValeurs(T1TCD_Value, null);
    T2TCD.setValeurs(T2TCD_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@COND@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // T2TCD.setSelectedIndex(getIndice("T2TCD", T2TCD_Value));
    // T1TCD.setSelectedIndex(getIndice("T1TCD", T1TCD_Value));
    
    
    // T2TCD.setEnabled( lexique.isPresent("T2TCD"));
    // T1TCD.setEnabled( lexique.isPresent("T1TCD"));
    T2REM6.setEnabled(lexique.isPresent("T2REM6"));
    T2REM5.setEnabled(lexique.isPresent("T2REM5"));
    T2REM4.setEnabled(lexique.isPresent("T2REM4"));
    T2REM3.setEnabled(lexique.isPresent("T2REM3"));
    T2REM2.setEnabled(lexique.isPresent("T2REM2"));
    T2REM1.setEnabled(lexique.isPresent("T2REM1"));
    T1REM6.setEnabled(lexique.isPresent("T1REM6"));
    T1REM5.setEnabled(lexique.isPresent("T1REM5"));
    T1REM4.setEnabled(lexique.isPresent("T1REM4"));
    T1REM3.setEnabled(lexique.isPresent("T1REM3"));
    T1REM2.setEnabled(lexique.isPresent("T1REM2"));
    T1REM1.setEnabled(lexique.isPresent("T1REM1"));
    T2COE.setEnabled(lexique.isPresent("T2COE"));
    T1COE.setEnabled(lexique.isPresent("T1COE"));
    // T2DTFX.setEnabled( lexique.isPresent("T2DTFX"));
    // T2DTDX.setEnabled( lexique.isPresent("T2DTDX"));
    // T1DTFX.setEnabled( lexique.isPresent("T1DTFX"));
    // T1DTDX.setEnabled( lexique.isPresent("T1DTDX"));
    T2VAL.setEnabled(lexique.isPresent("T2VAL"));
    T1VAL.setEnabled(lexique.isPresent("T1VAL"));
    OBJ_20.setVisible(lexique.isPresent("COND"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // lexique.HostFieldPutData("T2TCD", 0, T2TCD_Value[T2TCD.getSelectedIndex()]);
    // lexique.HostFieldPutData("T1TCD", 0, T1TCD_Value[T1TCD.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    T2DTDX = new XRiCalendrier();
    T2DTFX = new XRiCalendrier();
    OBJ_36 = new JLabel();
    OBJ_38 = new JLabel();
    T2TCD = new XRiComboBox();
    OBJ_107 = new JLabel();
    panel3 = new JPanel();
    T2VAL = new XRiTextField();
    T2COE = new XRiTextField();
    T2REM1 = new XRiTextField();
    T2REM2 = new XRiTextField();
    T2REM3 = new XRiTextField();
    T2REM4 = new XRiTextField();
    T2REM5 = new XRiTextField();
    T2REM6 = new XRiTextField();
    OBJ_109 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_111 = new JLabel();
    panel1 = new JPanel();
    T1DTDX = new XRiCalendrier();
    T1DTFX = new XRiCalendrier();
    OBJ_22 = new JLabel();
    OBJ_24 = new JLabel();
    T1TCD = new XRiComboBox();
    OBJ_108 = new JLabel();
    OBJ_20 = new JLabel();
    panel4 = new JPanel();
    T1VAL = new XRiTextField();
    T1COE = new XRiTextField();
    T1REM1 = new XRiTextField();
    T1REM2 = new XRiTextField();
    T1REM3 = new XRiTextField();
    T1REM4 = new XRiTextField();
    T1REM5 = new XRiTextField();
    T1REM6 = new XRiTextField();
    OBJ_110 = new JLabel();
    OBJ_136 = new JLabel();
    OBJ_112 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1055, 305));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("P\u00e9riode N\u00b02"));
          panel2.setOpaque(false);
          panel2.setName("panel2");

          //---- T2DTDX ----
          T2DTDX.setComponentPopupMenu(BTD);
          T2DTDX.setName("T2DTDX");

          //---- T2DTFX ----
          T2DTFX.setComponentPopupMenu(BTD);
          T2DTFX.setName("T2DTFX");

          //---- OBJ_36 ----
          OBJ_36.setText("du");
          OBJ_36.setName("OBJ_36");

          //---- OBJ_38 ----
          OBJ_38.setText("au");
          OBJ_38.setName("OBJ_38");

          //---- T2TCD ----
          T2TCD.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Prix net",
            "Prix de base",
            "Remise en %",
            "Coefficient",
            "Formule (FP)",
            "Ajout en valeur",
            "Remise en valeur"
          }));
          T2TCD.setComponentPopupMenu(null);
          T2TCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T2TCD.setName("T2TCD");

          //---- OBJ_107 ----
          OBJ_107.setText("Type");
          OBJ_107.setComponentPopupMenu(null);
          OBJ_107.setName("OBJ_107");

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- T2VAL ----
            T2VAL.setComponentPopupMenu(BTD);
            T2VAL.setName("T2VAL");

            //---- T2COE ----
            T2COE.setComponentPopupMenu(BTD);
            T2COE.setName("T2COE");

            //---- T2REM1 ----
            T2REM1.setComponentPopupMenu(BTD);
            T2REM1.setName("T2REM1");

            //---- T2REM2 ----
            T2REM2.setComponentPopupMenu(BTD);
            T2REM2.setName("T2REM2");

            //---- T2REM3 ----
            T2REM3.setComponentPopupMenu(BTD);
            T2REM3.setName("T2REM3");

            //---- T2REM4 ----
            T2REM4.setComponentPopupMenu(BTD);
            T2REM4.setName("T2REM4");

            //---- T2REM5 ----
            T2REM5.setComponentPopupMenu(BTD);
            T2REM5.setName("T2REM5");

            //---- T2REM6 ----
            T2REM6.setComponentPopupMenu(BTD);
            T2REM6.setName("T2REM6");

            //---- OBJ_109 ----
            OBJ_109.setText("Valeur");
            OBJ_109.setComponentPopupMenu(null);
            OBJ_109.setName("OBJ_109");

            //---- OBJ_135 ----
            OBJ_135.setText("Remises");
            OBJ_135.setComponentPopupMenu(null);
            OBJ_135.setName("OBJ_135");

            //---- OBJ_111 ----
            OBJ_111.setText("Coefficient");
            OBJ_111.setComponentPopupMenu(null);
            OBJ_111.setName("OBJ_111");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_109, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                  .addGap(58, 58, 58)
                  .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(102, 102, 102)
                  .addComponent(OBJ_111, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(T2VAL, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T2REM1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T2REM2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T2REM3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(T2COE, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(110, 110, 110)
                  .addComponent(T2REM4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T2REM5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T2REM6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(OBJ_109)
                    .addComponent(OBJ_135)
                    .addComponent(OBJ_111))
                  .addGap(4, 4, 4)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(T2VAL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T2REM1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T2REM2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T2REM3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T2COE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(T2REM4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T2REM5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T2REM6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(T2DTDX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(T2DTFX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addGroup(panel2Layout.createParallelGroup()
                  .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addComponent(T2TCD, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
          );
          panel2Layout.setVerticalGroup(
            panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(T2DTDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(T2DTFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_107)
                .addGap(5, 5, 5)
                .addComponent(T2TCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(panel2);
        panel2.setBounds(15, 155, 855, 135);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("P\u00e9riode N\u00b01"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- T1DTDX ----
          T1DTDX.setComponentPopupMenu(BTD);
          T1DTDX.setName("T1DTDX");

          //---- T1DTFX ----
          T1DTFX.setComponentPopupMenu(BTD);
          T1DTFX.setName("T1DTFX");

          //---- OBJ_22 ----
          OBJ_22.setText("du");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_24 ----
          OBJ_24.setText("au");
          OBJ_24.setName("OBJ_24");

          //---- T1TCD ----
          T1TCD.setModel(new DefaultComboBoxModel(new String[] {
            "",
            "Prix net",
            "Prix de base",
            "Remise en %",
            "Coefficient",
            "Formule (FP)",
            "Ajout en valeur",
            "Remise en valeur"
          }));
          T1TCD.setComponentPopupMenu(null);
          T1TCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T1TCD.setName("T1TCD");

          //---- OBJ_108 ----
          OBJ_108.setText("Type");
          OBJ_108.setComponentPopupMenu(null);
          OBJ_108.setName("OBJ_108");

          //---- OBJ_20 ----
          OBJ_20.setText("@COND@");
          OBJ_20.setName("OBJ_20");

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");

            //---- T1VAL ----
            T1VAL.setComponentPopupMenu(BTD);
            T1VAL.setName("T1VAL");

            //---- T1COE ----
            T1COE.setComponentPopupMenu(BTD);
            T1COE.setName("T1COE");

            //---- T1REM1 ----
            T1REM1.setComponentPopupMenu(BTD);
            T1REM1.setName("T1REM1");

            //---- T1REM2 ----
            T1REM2.setComponentPopupMenu(BTD);
            T1REM2.setName("T1REM2");

            //---- T1REM3 ----
            T1REM3.setComponentPopupMenu(BTD);
            T1REM3.setName("T1REM3");

            //---- T1REM4 ----
            T1REM4.setComponentPopupMenu(BTD);
            T1REM4.setName("T1REM4");

            //---- T1REM5 ----
            T1REM5.setComponentPopupMenu(BTD);
            T1REM5.setName("T1REM5");

            //---- T1REM6 ----
            T1REM6.setComponentPopupMenu(BTD);
            T1REM6.setName("T1REM6");

            //---- OBJ_110 ----
            OBJ_110.setText("Valeur");
            OBJ_110.setComponentPopupMenu(null);
            OBJ_110.setName("OBJ_110");

            //---- OBJ_136 ----
            OBJ_136.setText("Remises");
            OBJ_136.setComponentPopupMenu(null);
            OBJ_136.setName("OBJ_136");

            //---- OBJ_112 ----
            OBJ_112.setText("Coefficient");
            OBJ_112.setComponentPopupMenu(null);
            OBJ_112.setName("OBJ_112");

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_110, GroupLayout.PREFERRED_SIZE, 42, GroupLayout.PREFERRED_SIZE)
                  .addGap(58, 58, 58)
                  .addComponent(OBJ_136, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGap(102, 102, 102)
                  .addComponent(OBJ_112, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(T1VAL, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T1REM1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T1REM2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T1REM3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(6, 6, 6)
                  .addComponent(T1COE, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(110, 110, 110)
                  .addComponent(T1REM4, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T1REM5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGap(2, 2, 2)
                  .addComponent(T1REM6, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addComponent(OBJ_110)
                    .addComponent(OBJ_136)
                    .addComponent(OBJ_112))
                  .addGap(4, 4, 4)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addComponent(T1VAL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T1REM1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T1REM2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T1REM3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T1COE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(2, 2, 2)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addComponent(T1REM4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T1REM5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(T1REM6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(T1DTDX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                    .addGap(2, 2, 2)
                    .addComponent(T1DTFX, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 271, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(OBJ_108, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(T1TCD, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)))
                .addGap(7, 7, 7)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 380, GroupLayout.PREFERRED_SIZE))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T1DTDX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(T1DTFX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_108)
                .addGap(5, 5, 5)
                .addComponent(T1TCD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 15, 855, 135);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiCalendrier T2DTDX;
  private XRiCalendrier T2DTFX;
  private JLabel OBJ_36;
  private JLabel OBJ_38;
  private XRiComboBox T2TCD;
  private JLabel OBJ_107;
  private JPanel panel3;
  private XRiTextField T2VAL;
  private XRiTextField T2COE;
  private XRiTextField T2REM1;
  private XRiTextField T2REM2;
  private XRiTextField T2REM3;
  private XRiTextField T2REM4;
  private XRiTextField T2REM5;
  private XRiTextField T2REM6;
  private JLabel OBJ_109;
  private JLabel OBJ_135;
  private JLabel OBJ_111;
  private JPanel panel1;
  private XRiCalendrier T1DTDX;
  private XRiCalendrier T1DTFX;
  private JLabel OBJ_22;
  private JLabel OBJ_24;
  private XRiComboBox T1TCD;
  private JLabel OBJ_108;
  private JLabel OBJ_20;
  private JPanel panel4;
  private XRiTextField T1VAL;
  private XRiTextField T1COE;
  private XRiTextField T1REM1;
  private XRiTextField T1REM2;
  private XRiTextField T1REM3;
  private XRiTextField T1REM4;
  private XRiTextField T1REM5;
  private XRiTextField T1REM6;
  private JLabel OBJ_110;
  private JLabel OBJ_136;
  private JLabel OBJ_112;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
