
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_C4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_C4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    L1IN2.setValeursSelection("T", " ");
    L1ED5.setValeursSelection("X", " ");
    L1ED1.setValeursSelection("X", " ");
    L1ED4.setValeursSelection("X", " ");
    L1ED3.setValeursSelection("X", " ");
    L1ED2.setValeursSelection("X", " ");
    
    L1LIB1.setSelectAll(false);
    L1LIB2.setSelectAll(false);
    L1LIB3.setSelectAll(false);
    L1LIB4.setSelectAll(false);
    
    setCloseKey("F5", "F19");
    
    // Titre
    setTitle("Ligne commentaire");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCPL@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    L1IN3.setEnabled(lexique.isPresent("L1IN3"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    L1CPL.setVisible(!lexique.HostFieldGetData("LIBCPL").trim().equalsIgnoreCase("") & lexique.isPresent("L1CPL"));
    L1ED1_OBJ_51_24.setVisible(lexique.isPresent("L1ED1"));
    L1ED1_OBJ_51_24.setSelected(lexique.HostFieldGetData("L1ED1").equalsIgnoreCase("9"));
    OBJ_49.setVisible(lexique.isPresent("LIBCPL"));
    WARTT.setVisible(lexique.isPresent("WARTT"));
    L1LIB4.setEnabled(lexique.isPresent("L1LIB4"));
    L1LIB3.setEnabled(lexique.isPresent("L1LIB3"));
    L1LIB2.setEnabled(lexique.isPresent("L1LIB2"));
    L1LIB1.setEnabled(lexique.isPresent("L1LIB1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    if (L1ED1_OBJ_51_24.isSelected()) {
      lexique.HostFieldPutData("L1ED1", 0, "9");
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("L1IN1", 0, "1");
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("L1IN1", 0, "2");
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    L1LIB3 = new XRiTextField();
    L1LIB4 = new XRiTextField();
    WARTT = new RiZoneSortie();
    OBJ_49 = new RiZoneSortie();
    OBJ_19 = new JLabel();
    L1CPL = new XRiTextField();
    OBJ_18 = new JLabel();
    WNLI = new RiZoneSortie();
    WCOD = new RiZoneSortie();
    OBJ_23 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_35 = new JLabel();
    panel1 = new JPanel();
    L1ED1_OBJ_51_24 = new JCheckBox();
    L1ED2 = new XRiCheckBox();
    L1ED3 = new XRiCheckBox();
    L1ED4 = new XRiCheckBox();
    L1ED1 = new XRiCheckBox();
    L1ED5 = new XRiCheckBox();
    OBJ_50 = new JLabel();
    L1IN3 = new XRiTextField();
    L1IN2 = new XRiCheckBox();
    OBJ_51 = new JLabel();
    L1IN21 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(825, 420));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Saisie");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Pleine page 30 car.");
              riSousMenu_bt6.setToolTipText("Saisie pleine page");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Pleine page 60 car.");
              riSousMenu_bt7.setToolTipText("Saisie pleine page");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          panel3.add(L1LIB1);
          L1LIB1.setBounds(35, 70, 310, L1LIB1.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setName("L1LIB2");
          panel3.add(L1LIB2);
          L1LIB2.setBounds(35, 100, 310, L1LIB2.getPreferredSize().height);

          //---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setName("L1LIB3");
          panel3.add(L1LIB3);
          L1LIB3.setBounds(35, 130, 310, L1LIB3.getPreferredSize().height);

          //---- L1LIB4 ----
          L1LIB4.setComponentPopupMenu(BTD);
          L1LIB4.setName("L1LIB4");
          panel3.add(L1LIB4);
          L1LIB4.setBounds(35, 160, 310, L1LIB4.getPreferredSize().height);

          //---- WARTT ----
          WARTT.setComponentPopupMenu(BTD);
          WARTT.setText("@WARTT@");
          WARTT.setName("WARTT");
          panel3.add(WARTT);
          WARTT.setBounds(105, 40, 186, WARTT.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("@LIBCPL@");
          OBJ_49.setName("OBJ_49");
          panel3.add(OBJ_49);
          OBJ_49.setBounds(295, 40, 183, OBJ_49.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Commentaire");
          OBJ_19.setName("OBJ_19");
          panel3.add(OBJ_19);
          OBJ_19.setBounds(105, 15, 102, 20);

          //---- L1CPL ----
          L1CPL.setComponentPopupMenu(BTD);
          L1CPL.setToolTipText("Compl\u00e9ment de libell\u00e9");
          L1CPL.setName("L1CPL");
          panel3.add(L1CPL);
          L1CPL.setBounds(485, 38, 74, L1CPL.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("C     N\u00b0Li");
          OBJ_18.setName("OBJ_18");
          panel3.add(OBJ_18);
          OBJ_18.setBounds(40, 15, 57, 20);

          //---- WNLI ----
          WNLI.setText("@WNLI@");
          WNLI.setHorizontalAlignment(SwingConstants.RIGHT);
          WNLI.setName("WNLI");
          panel3.add(WNLI);
          WNLI.setBounds(60, 40, 44, WNLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setText("@WCOD@");
          WCOD.setName("WCOD");
          panel3.add(WCOD);
          WCOD.setBounds(35, 40, 24, WCOD.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("1");
          OBJ_23.setName("OBJ_23");
          panel3.add(OBJ_23);
          OBJ_23.setBounds(15, 75, 12, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("2");
          OBJ_32.setName("OBJ_32");
          panel3.add(OBJ_32);
          OBJ_32.setBounds(15, 105, 12, 20);

          //---- OBJ_34 ----
          OBJ_34.setText("3");
          OBJ_34.setName("OBJ_34");
          panel3.add(OBJ_34);
          OBJ_34.setBounds(15, 135, 12, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("4");
          OBJ_35.setName("OBJ_35");
          panel3.add(OBJ_35);
          OBJ_35.setBounds(15, 165, 12, 20);
        }
        p_contenu.add(panel3);
        panel3.setBounds(15, 15, 620, 220);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("\u00e0 \u00e9diter sur"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- L1ED1_OBJ_51_24 ----
          L1ED1_OBJ_51_24.setText("Aucune \u00e9dition");
          L1ED1_OBJ_51_24.setComponentPopupMenu(BTD);
          L1ED1_OBJ_51_24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED1_OBJ_51_24.setName("L1ED1_OBJ_51_24");
          panel1.add(L1ED1_OBJ_51_24);
          L1ED1_OBJ_51_24.setBounds(485, -1, 119, 18);

          //---- L1ED2 ----
          L1ED2.setText("Accus\u00e9s de r\u00e9ception de commande");
          L1ED2.setComponentPopupMenu(BTD);
          L1ED2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED2.setName("L1ED2");
          panel1.add(L1ED2);
          L1ED2.setBounds(240, 40, 253, 20);

          //---- L1ED3 ----
          L1ED3.setText("Bons d'exp\u00e9dition");
          L1ED3.setComponentPopupMenu(BTD);
          L1ED3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED3.setName("L1ED3");
          panel1.add(L1ED3);
          L1ED3.setBounds(25, 70, 148, 20);

          //---- L1ED4 ----
          L1ED4.setText("Bons transporteurs");
          L1ED4.setComponentPopupMenu(BTD);
          L1ED4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED4.setName("L1ED4");
          panel1.add(L1ED4);
          L1ED4.setBounds(240, 70, 148, 20);

          //---- L1ED1 ----
          L1ED1.setText("Bons de pr\u00e9paration");
          L1ED1.setComponentPopupMenu(BTD);
          L1ED1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED1.setName("L1ED1");
          panel1.add(L1ED1);
          L1ED1.setBounds(25, 40, 148, 20);

          //---- L1ED5 ----
          L1ED5.setText("Factures");
          L1ED5.setComponentPopupMenu(BTD);
          L1ED5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED5.setName("L1ED5");
          panel1.add(L1ED5);
          L1ED5.setBounds(485, 70, 78, 20);
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 245, 620, 115);

        //---- OBJ_50 ----
        OBJ_50.setText("Specif,Taxe,Kit...");
        OBJ_50.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_50.setName("OBJ_50");
        p_contenu.add(OBJ_50);
        OBJ_50.setBounds(40, 374, 105, 20);

        //---- L1IN3 ----
        L1IN3.setComponentPopupMenu(BTD);
        L1IN3.setToolTipText("Code pour regroupement de lignes");
        L1IN3.setName("L1IN3");
        p_contenu.add(L1IN3);
        L1IN3.setBounds(145, 370, 24, L1IN3.getPreferredSize().height);

        //---- L1IN2 ----
        L1IN2.setText("Totalisation");
        L1IN2.setToolTipText("Toatalisation");
        L1IN2.setName("L1IN2");
        p_contenu.add(L1IN2);
        L1IN2.setBounds(255, 372, 135, 25);

        //---- OBJ_51 ----
        OBJ_51.setText("Regroupement");
        OBJ_51.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_51.setName("OBJ_51");
        p_contenu.add(OBJ_51);
        OBJ_51.setBounds(395, 374, 100, 20);

        //---- L1IN21 ----
        L1IN21.setComponentPopupMenu(BTD);
        L1IN21.setToolTipText("Code pour regroupement de lignes");
        L1IN21.setName("L1IN21");
        p_contenu.add(L1IN21);
        L1IN21.setBounds(500, 370, 24, L1IN21.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== riSousMenu9 ========
    {
      riSousMenu9.setName("riSousMenu9");

      //---- riSousMenu_bt9 ----
      riSousMenu_bt9.setText("Recherche bon, facture");
      riSousMenu_bt9.setToolTipText("Recherche bon, facture, devis");
      riSousMenu_bt9.setName("riSousMenu_bt9");
      riSousMenu_bt9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt9ActionPerformed(e);
        }
      });
      riSousMenu9.add(riSousMenu_bt9);
    }

    //======== riSousMenu8 ========
    {
      riSousMenu8.setName("riSousMenu8");

      //---- riSousMenu_bt8 ----
      riSousMenu_bt8.setText("Saisie code barre");
      riSousMenu_bt8.setToolTipText("Mise en fonction saisie code barre");
      riSousMenu_bt8.setName("riSousMenu_bt8");
      riSousMenu_bt8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt8ActionPerformed(e);
        }
      });
      riSousMenu8.add(riSousMenu_bt8);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private XRiTextField L1LIB3;
  private XRiTextField L1LIB4;
  private RiZoneSortie WARTT;
  private RiZoneSortie OBJ_49;
  private JLabel OBJ_19;
  private XRiTextField L1CPL;
  private JLabel OBJ_18;
  private RiZoneSortie WNLI;
  private RiZoneSortie WCOD;
  private JLabel OBJ_23;
  private JLabel OBJ_32;
  private JLabel OBJ_34;
  private JLabel OBJ_35;
  private JPanel panel1;
  private JCheckBox L1ED1_OBJ_51_24;
  private XRiCheckBox L1ED2;
  private XRiCheckBox L1ED3;
  private XRiCheckBox L1ED4;
  private XRiCheckBox L1ED1;
  private XRiCheckBox L1ED5;
  private JLabel OBJ_50;
  private XRiTextField L1IN3;
  private XRiCheckBox L1IN2;
  private JLabel OBJ_51;
  private XRiTextField L1IN21;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
