
package ri.serien.libecranrpg.vgvm.VGVM06FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.sngroupe.SNGroupe;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM651] Gestion des ventes -> Modification des tarifs de ventes (Mise à Jour de prix).
 * Option "Saisie de coefficient"
 */
public class VGVM06FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM06FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    
    // Ajout
    initDiverses();
    WTD1.setValeursSelection("*", " ");
    WTD2.setValeursSelection("*", " ");
    
    // Lier les composants groupe, famille et sous-famille début
    snFamilleDebut.lierComposantGroupe(snGroupeDebut);
    snSousFamilleDebut.lierComposantGroupe(snGroupeDebut);
    
    // Lier les composants groupe, famille et sous-famille fin
    snFamilleFin.lierComposantGroupe(snGroupeFin);
    snSousFamilleFin.lierComposantGroupe(snGroupeFin);
    
    // Lier les composants pour les plages début et fin
    snGroupeDebut.lierComposantFin(snGroupeFin);
    snFamilleDebut.lierComposantFin(snFamilleFin);
    snSousFamilleDebut.lierComposantFin(snSousFamilleFin);
    
    // Ajouter la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    zscodeTarifDebut.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTAD@")).trim());
    zscodeTarifFin.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTAF@")).trim());
    zsLibelleZP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP1@")).trim());
    zsLibelleZP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP2@")).trim());
    zsLibelleZP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP3@")).trim());
    zsLibelleZP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP4@")).trim());
    zsLibelleZP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), IdEtablissement.getInstance(lexique.HostFieldGetData("WETB")));
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Initialise les composants
    rafraichirGroupeDebut();
    rafraichirGroupeFin();
    rafraichirFamilleDebut();
    rafraichirFamilleFin();
    rafraichirSousFamilleDebut();
    rafraichirSousFamilleFin();
    rafraichirFournisseur();
    rafraichirDevises();
    
    // Plage de dates
    snPlageDate.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDate.setDateDebutParChampRPG(lexique, "WDATDX");
    snPlageDate.setDateFinParChampRPG(lexique, "WDATFX");
    
    // OBJ_43.setVisible(lexique.isPresent("OPTLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@  :  @SELE@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snGroupeDebut.renseignerChampRPG(lexique, "WGRPD");
    snGroupeFin.renseignerChampRPG(lexique, "WGRPF");
    snFamilleDebut.renseignerChampRPG(lexique, "WFAMD");
    snFamilleFin.renseignerChampRPG(lexique, "WFAMF");
    snSousFamilleDebut.renseignerChampRPG(lexique, "WSFAD");
    snSousFamilleFin.renseignerChampRPG(lexique, "WSFAF");
    snDeviseTarif.renseignerChampRPG(lexique, "WDEV");
    snDeviseAchat.renseignerChampRPG(lexique, "WDEVA");
    snFournisseur.renseignerChampRPG(lexique, "WCOL", "WFRS");
    
    if (lexique.HostFieldGetData("WGRPD").trim().isEmpty() && lexique.HostFieldGetData("WGRPDF").trim().isEmpty()) {
      lexique.HostFieldPutData("WGRPD", 0, "*");
    }
    
    if (lexique.HostFieldGetData("WFAMD").trim().isEmpty() && lexique.HostFieldGetData("WFAMF").trim().isEmpty()) {
      lexique.HostFieldPutData("WFAMD", 0, "***");
    }
    
    if (lexique.HostFieldGetData("WSFAD").trim().isEmpty() && lexique.HostFieldGetData("WSFAF").trim().isEmpty()) {
      lexique.HostFieldPutData("WSFAD", 0, "*****");
    }
    
    snPlageDate.renseignerChampRPGDebut(lexique, "WDATDX");
    snPlageDate.renseignerChampRPGFin(lexique, "WDATFX");
  }
  
  /**
   * Rafraîchir la liste du groupe début.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirGroupeDebut() {
    snGroupeDebut.setSession(getSession());
    snGroupeDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snGroupeDebut.setTousAutorise(true);
    snGroupeDebut.charger(false);
    snGroupeDebut.setSelectionParChampRPG(lexique, "WGRPD");
  }
  
  /**
   * Rafraîchir la liste du groupe fin.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirGroupeFin() {
    snGroupeFin.setSession(getSession());
    snGroupeFin.setIdEtablissement(snEtablissement.getIdSelection());
    snGroupeFin.setTousAutorise(true);
    snGroupeFin.charger(false);
    snGroupeFin.setSelectionParChampRPG(lexique, "WGRPF");
  }
  
  /**
   * Rafraîchir la liste de la famille début.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirFamilleDebut() {
    snFamilleDebut.setSession(getSession());
    snFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleDebut.setTousAutorise(true);
    snFamilleDebut.charger(false);
    snFamilleDebut.setSelectionParChampRPG(lexique, "WFAMD");
  }
  
  /**
   * Rafraîchir la liste de la famille fin.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirFamilleFin() {
    snFamilleFin.setSession(getSession());
    snFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFamilleFin.setTousAutorise(true);
    snFamilleFin.charger(false);
    snFamilleFin.setSelectionParChampRPG(lexique, "WFAMF");
  }
  
  /**
   * Rafraîchir la liste de la sous-famille début.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirSousFamilleDebut() {
    snSousFamilleDebut.setSession(getSession());
    snSousFamilleDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleDebut.setTousAutorise(true);
    snSousFamilleDebut.charger(false);
    snSousFamilleDebut.setSelectionParChampRPG(lexique, "WSFAD");
  }
  
  /**
   * Rafraîchir la liste de la sous-famille fin.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirSousFamilleFin() {
    snSousFamilleFin.setSession(getSession());
    snSousFamilleFin.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamilleFin.setTousAutorise(true);
    snSousFamilleFin.charger(false);
    snSousFamilleFin.setSelectionParChampRPG(lexique, "WSFAF");
  }
  
  /**
   * Rafraichir la liste des fournisseur.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirFournisseur() {
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "WCOL", "WFRS");
  }
  
  /**
   * Rafraichir la liste des devises.
   * Lors du changement d'établissement, la liste est automatiquement rechargée.
   */
  private void rafraichirDevises() {
    snDeviseTarif.setSession(getSession());
    snDeviseTarif.setIdEtablissement(snEtablissement.getIdSelection());
    snDeviseTarif.charger(false);
    snDeviseTarif.setSelectionParChampRPG(lexique, "WDEV");
    
    snDeviseAchat.setSession(getSession());
    snDeviseAchat.setIdEtablissement(snEtablissement.getIdSelection());
    snDeviseAchat.charger(false);
    snDeviseAchat.setSelectionParChampRPG(lexique, "WDEVA");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    snPanelContenu = new SNPanelContenu();
    sNPanelTitre5 = new SNPanelTitre();
    lbetablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    sNPanelTitre2 = new SNPanelTitre();
    lbCodeTarif = new SNLabelChamp();
    sNPanelTitre1 = new SNPanel();
    TADEB = new XRiTextField();
    zscodeTarifDebut = new RiZoneSortie();
    lbCodeTarif3 = new SNLabelChamp();
    TAFIN = new XRiTextField();
    zscodeTarifFin = new RiZoneSortie();
    lbCodeTarif2 = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    WTD1 = new XRiCheckBox();
    WTD2 = new XRiCheckBox();
    snPlageDate = new SNPlageDate();
    sNPanelTitre3 = new SNPanelTitre();
    lbGroupe = new SNLabelChamp();
    pnGroupe = new SNPanel();
    snGroupeDebut = new SNGroupe();
    lbGroupe2 = new SNLabelChamp();
    snGroupeFin = new SNGroupe();
    lbFamille = new SNLabelChamp();
    pnlFamille = new SNPanel();
    snFamilleDebut = new SNFamille();
    lbFamille2 = new SNLabelChamp();
    snFamilleFin = new SNFamille();
    lbSousFamille = new SNLabelChamp();
    pnlsousFamille = new SNPanel();
    snSousFamilleDebut = new SNSousFamille();
    lbSousFamille2 = new SNLabelChamp();
    snSousFamilleFin = new SNSousFamille();
    lbZonesPersonnalisées = new SNLabelChamp();
    pnlZP = new SNPanel();
    zsLibelleZP1 = new RiZoneSortie();
    WZP1 = new XRiTextField();
    zsLibelleZP2 = new RiZoneSortie();
    WZP2 = new XRiTextField();
    zsLibelleZP3 = new RiZoneSortie();
    WZP3 = new XRiTextField();
    zsLibelleZP4 = new RiZoneSortie();
    WZP4 = new XRiTextField();
    zsLibelleZP5 = new RiZoneSortie();
    WZP5 = new XRiTextField();
    sNPanelTitre4 = new SNPanelTitre();
    lbRattachementCNV = new SNLabelChamp();
    pnlRattachementCNV = new SNPanel();
    WCNV = new XRiTextField();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbDeviseTarif = new SNLabelChamp();
    snDeviseTarif = new SNDevise();
    lbDeviseAchat = new SNLabelChamp();
    snDeviseAchat = new SNDevise();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("MODIFICATION TARIFS DE VENTE : SELECTION A TRAITER");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== snPanelContenu ========
    {
      snPanelContenu.setName("snPanelContenu");
      snPanelContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) snPanelContenu.getLayout()).columnWidths = new int[] { 962, 0 };
      ((GridBagLayout) snPanelContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) snPanelContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) snPanelContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== sNPanelTitre5 ========
      {
        sNPanelTitre5.setName("sNPanelTitre5");
        sNPanelTitre5.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre5.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) sNPanelTitre5.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbetablissement ----
        lbetablissement.setText("Etablissement");
        lbetablissement.setName("lbetablissement");
        sNPanelTitre5.add(lbetablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setMaximumSize(new Dimension(500, 30));
        snEtablissement.setMinimumSize(new Dimension(500, 30));
        snEtablissement.setPreferredSize(new Dimension(500, 30));
        snEtablissement.setName("snEtablissement");
        sNPanelTitre5.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      snPanelContenu.add(sNPanelTitre5,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanelTitre2 ========
      {
        sNPanelTitre2.setName("sNPanelTitre2");
        sNPanelTitre2.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCodeTarif ----
        lbCodeTarif.setText("Tarif de");
        lbCodeTarif.setName("lbCodeTarif");
        sNPanelTitre2.add(lbCodeTarif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- TADEB ----
          TADEB.setPreferredSize(new Dimension(75, 30));
          TADEB.setMaximumSize(new Dimension(75, 30));
          TADEB.setMinimumSize(new Dimension(75, 30));
          TADEB.setName("TADEB");
          sNPanelTitre1.add(TADEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zscodeTarifDebut ----
          zscodeTarifDebut.setText("@LIBTAD@");
          zscodeTarifDebut.setPreferredSize(new Dimension(372, 24));
          zscodeTarifDebut.setMinimumSize(new Dimension(372, 24));
          zscodeTarifDebut.setMaximumSize(new Dimension(372, 24));
          zscodeTarifDebut.setName("zscodeTarifDebut");
          sNPanelTitre1.add(zscodeTarifDebut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbCodeTarif3 ----
          lbCodeTarif3.setText("\u00e0");
          lbCodeTarif3.setMaximumSize(new Dimension(20, 30));
          lbCodeTarif3.setMinimumSize(new Dimension(20, 30));
          lbCodeTarif3.setPreferredSize(new Dimension(20, 30));
          lbCodeTarif3.setHorizontalAlignment(SwingConstants.CENTER);
          lbCodeTarif3.setName("lbCodeTarif3");
          sNPanelTitre1.add(lbCodeTarif3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- TAFIN ----
          TAFIN.setPreferredSize(new Dimension(75, 30));
          TAFIN.setMaximumSize(new Dimension(75, 30));
          TAFIN.setMinimumSize(new Dimension(75, 30));
          TAFIN.setName("TAFIN");
          sNPanelTitre1.add(TAFIN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zscodeTarifFin ----
          zscodeTarifFin.setText("@LIBTAF@");
          zscodeTarifFin.setPreferredSize(new Dimension(372, 24));
          zscodeTarifFin.setMinimumSize(new Dimension(372, 24));
          zscodeTarifFin.setMaximumSize(new Dimension(372, 24));
          zscodeTarifFin.setName("zscodeTarifFin");
          sNPanelTitre1.add(zscodeTarifFin, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre2.add(sNPanelTitre1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeTarif2 ----
        lbCodeTarif2.setText("Date d'application");
        lbCodeTarif2.setName("lbCodeTarif2");
        sNPanelTitre2.add(lbCodeTarif2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- WTD1 ----
          WTD1.setText("En cours");
          WTD1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTD1.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTD1.setName("WTD1");
          sNPanel1.add(WTD1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WTD2 ----
          WTD2.setText("En pr\u00e9paration");
          WTD2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTD2.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTD2.setName("WTD2");
          sNPanel1.add(WTD2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snPlageDate ----
          snPlageDate.setName("snPlageDate");
          sNPanel1.add(snPlageDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre2.add(sNPanel1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      snPanelContenu.add(sNPanelTitre2,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanelTitre3 ========
      {
        sNPanelTitre3.setName("sNPanelTitre3");
        sNPanelTitre3.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbGroupe ----
        lbGroupe.setText("Groupe de");
        lbGroupe.setName("lbGroupe");
        sNPanelTitre3.add(lbGroupe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnGroupe ========
        {
          pnGroupe.setName("pnGroupe");
          pnGroupe.setLayout(new GridBagLayout());
          ((GridBagLayout) pnGroupe.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnGroupe.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnGroupe.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnGroupe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- snGroupeDebut ----
          snGroupeDebut.setMaximumSize(new Dimension(400, 30));
          snGroupeDebut.setMinimumSize(new Dimension(400, 30));
          snGroupeDebut.setPreferredSize(new Dimension(400, 30));
          snGroupeDebut.setName("snGroupeDebut");
          pnGroupe.add(snGroupeDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbGroupe2 ----
          lbGroupe2.setText("\u00e0");
          lbGroupe2.setMaximumSize(new Dimension(20, 30));
          lbGroupe2.setMinimumSize(new Dimension(20, 30));
          lbGroupe2.setPreferredSize(new Dimension(20, 30));
          lbGroupe2.setHorizontalAlignment(SwingConstants.CENTER);
          lbGroupe2.setName("lbGroupe2");
          pnGroupe.add(lbGroupe2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snGroupeFin ----
          snGroupeFin.setMaximumSize(new Dimension(400, 30));
          snGroupeFin.setMinimumSize(new Dimension(400, 30));
          snGroupeFin.setPreferredSize(new Dimension(400, 30));
          snGroupeFin.setName("snGroupeFin");
          pnGroupe.add(snGroupeFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre3.add(pnGroupe, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFamille ----
        lbFamille.setText("Famille de");
        lbFamille.setName("lbFamille");
        sNPanelTitre3.add(lbFamille, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlFamille ========
        {
          pnlFamille.setName("pnlFamille");
          pnlFamille.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFamille.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlFamille.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlFamille.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlFamille.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- snFamilleDebut ----
          snFamilleDebut.setMaximumSize(new Dimension(400, 30));
          snFamilleDebut.setMinimumSize(new Dimension(400, 30));
          snFamilleDebut.setPreferredSize(new Dimension(400, 30));
          snFamilleDebut.setName("snFamilleDebut");
          pnlFamille.add(snFamilleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbFamille2 ----
          lbFamille2.setText("\u00e0");
          lbFamille2.setMaximumSize(new Dimension(20, 30));
          lbFamille2.setMinimumSize(new Dimension(20, 30));
          lbFamille2.setPreferredSize(new Dimension(20, 30));
          lbFamille2.setHorizontalAlignment(SwingConstants.CENTER);
          lbFamille2.setName("lbFamille2");
          pnlFamille.add(lbFamille2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snFamilleFin ----
          snFamilleFin.setMaximumSize(new Dimension(400, 30));
          snFamilleFin.setMinimumSize(new Dimension(400, 30));
          snFamilleFin.setPreferredSize(new Dimension(400, 30));
          snFamilleFin.setName("snFamilleFin");
          pnlFamille.add(snFamilleFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre3.add(pnlFamille, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSousFamille ----
        lbSousFamille.setText("Sous-famille de");
        lbSousFamille.setName("lbSousFamille");
        sNPanelTitre3.add(lbSousFamille, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlsousFamille ========
        {
          pnlsousFamille.setName("pnlsousFamille");
          pnlsousFamille.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlsousFamille.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlsousFamille.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlsousFamille.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlsousFamille.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- snSousFamilleDebut ----
          snSousFamilleDebut.setMaximumSize(new Dimension(400, 30));
          snSousFamilleDebut.setMinimumSize(new Dimension(400, 30));
          snSousFamilleDebut.setPreferredSize(new Dimension(400, 30));
          snSousFamilleDebut.setName("snSousFamilleDebut");
          pnlsousFamille.add(snSousFamilleDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSousFamille2 ----
          lbSousFamille2.setText("\u00e0");
          lbSousFamille2.setMaximumSize(new Dimension(20, 30));
          lbSousFamille2.setMinimumSize(new Dimension(20, 30));
          lbSousFamille2.setPreferredSize(new Dimension(20, 30));
          lbSousFamille2.setHorizontalAlignment(SwingConstants.CENTER);
          lbSousFamille2.setName("lbSousFamille2");
          pnlsousFamille.add(lbSousFamille2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snSousFamilleFin ----
          snSousFamilleFin.setMaximumSize(new Dimension(400, 30));
          snSousFamilleFin.setMinimumSize(new Dimension(400, 30));
          snSousFamilleFin.setPreferredSize(new Dimension(400, 30));
          snSousFamilleFin.setName("snSousFamilleFin");
          pnlsousFamille.add(snSousFamilleFin, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre3.add(pnlsousFamille, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbZonesPersonnalisées ----
        lbZonesPersonnalisées.setText("Zone personnalis\u00e9e");
        lbZonesPersonnalisées.setName("lbZonesPersonnalis\u00e9es");
        sNPanelTitre3.add(lbZonesPersonnalisées, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlZP ========
        {
          pnlZP.setName("pnlZP");
          pnlZP.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlZP.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlZP.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlZP.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlZP.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- zsLibelleZP1 ----
          zsLibelleZP1.setText("@LIBZP1@");
          zsLibelleZP1.setPreferredSize(new Dimension(36, 24));
          zsLibelleZP1.setMinimumSize(new Dimension(36, 24));
          zsLibelleZP1.setMaximumSize(new Dimension(36, 24));
          zsLibelleZP1.setName("zsLibelleZP1");
          pnlZP.add(zsLibelleZP1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WZP1 ----
          WZP1.setPreferredSize(new Dimension(36, 28));
          WZP1.setMaximumSize(new Dimension(36, 28));
          WZP1.setMinimumSize(new Dimension(36, 28));
          WZP1.setName("WZP1");
          pnlZP.add(WZP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsLibelleZP2 ----
          zsLibelleZP2.setText("@LIBZP2@");
          zsLibelleZP2.setPreferredSize(new Dimension(36, 24));
          zsLibelleZP2.setMinimumSize(new Dimension(36, 24));
          zsLibelleZP2.setMaximumSize(new Dimension(36, 24));
          zsLibelleZP2.setName("zsLibelleZP2");
          pnlZP.add(zsLibelleZP2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WZP2 ----
          WZP2.setPreferredSize(new Dimension(36, 28));
          WZP2.setMaximumSize(new Dimension(36, 28));
          WZP2.setMinimumSize(new Dimension(36, 28));
          WZP2.setName("WZP2");
          pnlZP.add(WZP2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsLibelleZP3 ----
          zsLibelleZP3.setText("@LIBZP3@");
          zsLibelleZP3.setPreferredSize(new Dimension(36, 24));
          zsLibelleZP3.setMinimumSize(new Dimension(36, 24));
          zsLibelleZP3.setMaximumSize(new Dimension(36, 24));
          zsLibelleZP3.setName("zsLibelleZP3");
          pnlZP.add(zsLibelleZP3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WZP3 ----
          WZP3.setPreferredSize(new Dimension(36, 28));
          WZP3.setMaximumSize(new Dimension(36, 28));
          WZP3.setMinimumSize(new Dimension(36, 28));
          WZP3.setName("WZP3");
          pnlZP.add(WZP3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsLibelleZP4 ----
          zsLibelleZP4.setText("@LIBZP4@");
          zsLibelleZP4.setPreferredSize(new Dimension(36, 24));
          zsLibelleZP4.setMinimumSize(new Dimension(36, 24));
          zsLibelleZP4.setMaximumSize(new Dimension(36, 24));
          zsLibelleZP4.setName("zsLibelleZP4");
          pnlZP.add(zsLibelleZP4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WZP4 ----
          WZP4.setPreferredSize(new Dimension(36, 28));
          WZP4.setMaximumSize(new Dimension(36, 28));
          WZP4.setMinimumSize(new Dimension(36, 28));
          WZP4.setName("WZP4");
          pnlZP.add(WZP4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsLibelleZP5 ----
          zsLibelleZP5.setText("@LIBZP5@");
          zsLibelleZP5.setPreferredSize(new Dimension(36, 24));
          zsLibelleZP5.setMinimumSize(new Dimension(36, 24));
          zsLibelleZP5.setMaximumSize(new Dimension(36, 24));
          zsLibelleZP5.setName("zsLibelleZP5");
          pnlZP.add(zsLibelleZP5, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WZP5 ----
          WZP5.setPreferredSize(new Dimension(36, 28));
          WZP5.setMaximumSize(new Dimension(36, 28));
          WZP5.setMinimumSize(new Dimension(36, 28));
          WZP5.setName("WZP5");
          pnlZP.add(WZP5, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre3.add(pnlZP, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      snPanelContenu.add(sNPanelTitre3,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanelTitre4 ========
      {
        sNPanelTitre4.setName("sNPanelTitre4");
        sNPanelTitre4.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanelTitre4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanelTitre4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) sNPanelTitre4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbRattachementCNV ----
        lbRattachementCNV.setText("Rattachement CNV");
        lbRattachementCNV.setName("lbRattachementCNV");
        sNPanelTitre4.add(lbRattachementCNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlRattachementCNV ========
        {
          pnlRattachementCNV.setName("pnlRattachementCNV");
          pnlRattachementCNV.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRattachementCNV.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlRattachementCNV.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRattachementCNV.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRattachementCNV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- WCNV ----
          WCNV.setPreferredSize(new Dimension(74, 28));
          WCNV.setMaximumSize(new Dimension(74, 28));
          WCNV.setMinimumSize(new Dimension(74, 28));
          WCNV.setName("WCNV");
          pnlRattachementCNV.add(WCNV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        sNPanelTitre4.add(pnlRattachementCNV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur principal");
        lbFournisseur.setName("lbFournisseur");
        sNPanelTitre4.add(lbFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snFournisseur ----
        snFournisseur.setMaximumSize(new Dimension(400, 30));
        snFournisseur.setMinimumSize(new Dimension(400, 30));
        snFournisseur.setPreferredSize(new Dimension(400, 30));
        snFournisseur.setName("snFournisseur");
        sNPanelTitre4.add(snFournisseur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDeviseTarif ----
        lbDeviseTarif.setText("Devise tarif");
        lbDeviseTarif.setName("lbDeviseTarif");
        sNPanelTitre4.add(lbDeviseTarif, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDeviseTarif ----
        snDeviseTarif.setName("snDeviseTarif");
        sNPanelTitre4.add(snDeviseTarif, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDeviseAchat ----
        lbDeviseAchat.setText("Devise d'achat");
        lbDeviseAchat.setName("lbDeviseAchat");
        sNPanelTitre4.add(lbDeviseAchat, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDeviseAchat ----
        snDeviseAchat.setName("snDeviseAchat");
        sNPanelTitre4.add(snDeviseAchat, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
      }
      snPanelContenu.add(sNPanelTitre4,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(snPanelContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu snPanelContenu;
  private SNPanelTitre sNPanelTitre5;
  private SNLabelChamp lbetablissement;
  private SNEtablissement snEtablissement;
  private SNPanelTitre sNPanelTitre2;
  private SNLabelChamp lbCodeTarif;
  private SNPanel sNPanelTitre1;
  private XRiTextField TADEB;
  private RiZoneSortie zscodeTarifDebut;
  private SNLabelChamp lbCodeTarif3;
  private XRiTextField TAFIN;
  private RiZoneSortie zscodeTarifFin;
  private SNLabelChamp lbCodeTarif2;
  private SNPanel sNPanel1;
  private XRiCheckBox WTD1;
  private XRiCheckBox WTD2;
  private SNPlageDate snPlageDate;
  private SNPanelTitre sNPanelTitre3;
  private SNLabelChamp lbGroupe;
  private SNPanel pnGroupe;
  private SNGroupe snGroupeDebut;
  private SNLabelChamp lbGroupe2;
  private SNGroupe snGroupeFin;
  private SNLabelChamp lbFamille;
  private SNPanel pnlFamille;
  private SNFamille snFamilleDebut;
  private SNLabelChamp lbFamille2;
  private SNFamille snFamilleFin;
  private SNLabelChamp lbSousFamille;
  private SNPanel pnlsousFamille;
  private SNSousFamille snSousFamilleDebut;
  private SNLabelChamp lbSousFamille2;
  private SNSousFamille snSousFamilleFin;
  private SNLabelChamp lbZonesPersonnalisées;
  private SNPanel pnlZP;
  private RiZoneSortie zsLibelleZP1;
  private XRiTextField WZP1;
  private RiZoneSortie zsLibelleZP2;
  private XRiTextField WZP2;
  private RiZoneSortie zsLibelleZP3;
  private XRiTextField WZP3;
  private RiZoneSortie zsLibelleZP4;
  private XRiTextField WZP4;
  private RiZoneSortie zsLibelleZP5;
  private XRiTextField WZP5;
  private SNPanelTitre sNPanelTitre4;
  private SNLabelChamp lbRattachementCNV;
  private SNPanel pnlRattachementCNV;
  private XRiTextField WCNV;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbDeviseTarif;
  private SNDevise snDeviseTarif;
  private SNLabelChamp lbDeviseAchat;
  private SNDevise snDeviseAchat;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
