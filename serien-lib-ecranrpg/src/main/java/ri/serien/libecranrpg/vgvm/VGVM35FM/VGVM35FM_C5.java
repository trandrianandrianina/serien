//$$david$$ ££07/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_C5 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_OBJECTIF = "Objectifs";
  private static final String BOUTON_DEVIS = "Devis";
  private static final String BOUTON_COMMANDE = "Commandes";
  private static final String BOUTON_FACTURES = "Factures";
  
  public VGVM35FM_C5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(BOUTON_OBJECTIF, 'o', true);
    snBarreBouton.ajouterBouton(BOUTON_DEVIS, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_COMMANDE, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_FACTURES, 'f', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FICHE REPRESENTANT"));
    
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_OBJECTIF)) {
        lexique.HostScreenSendKey(this, "F2");
      }
      else if (pSNBouton.isBouton(BOUTON_DEVIS)) {
        lexique.HostCursorPut(8, 47);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_COMMANDE)) {
        lexique.HostCursorPut(8, 28);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_FACTURES)) {
        lexique.HostCursorPut(8, 39);
        lexique.HostScreenSendKey(this, "F4");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlObjectif = new SNPanel();
    pnlPlafonds = new SNPanelTitre();
    sNPanel1 = new SNPanel();
    pnlMois1 = new SNPanel();
    lbMois = new SNLabelChamp();
    sNLabelChamp1 = new SNLabelChamp();
    lbMois1 = new SNLabelChamp();
    RPO01 = new XRiTextField();
    lbMois2 = new SNLabelChamp();
    RPO02 = new XRiTextField();
    lbMois3 = new SNLabelChamp();
    RPO03 = new XRiTextField();
    pnlMois2 = new SNPanel();
    lbMois14 = new SNLabelChamp();
    sNLabelChamp3 = new SNLabelChamp();
    lbMois4 = new SNLabelChamp();
    RPO04 = new XRiTextField();
    lbMois5 = new SNLabelChamp();
    RPO05 = new XRiTextField();
    lbMois6 = new SNLabelChamp();
    RPO06 = new XRiTextField();
    pnlMois3 = new SNPanel();
    lbMois15 = new SNLabelChamp();
    sNLabelChamp4 = new SNLabelChamp();
    lbMois7 = new SNLabelChamp();
    RPO07 = new XRiTextField();
    lbMois8 = new SNLabelChamp();
    RPO08 = new XRiTextField();
    lbMois9 = new SNLabelChamp();
    RPO09 = new XRiTextField();
    pnlMois4 = new SNPanel();
    lbMois16 = new SNLabelChamp();
    sNLabelChamp5 = new SNLabelChamp();
    lbMois10 = new SNLabelChamp();
    RPO10 = new XRiTextField();
    lbMois11 = new SNLabelChamp();
    RPO11 = new XRiTextField();
    lbMois12 = new SNLabelChamp();
    RPO12 = new XRiTextField();
    pnlTauxPlafond = new SNPanel();
    lbTauxPlafond = new SNLabelChamp();
    RPCO2 = new XRiTextField();
    sNLabelUnite1 = new SNLabelUnite();
    lbMontantDroite = new SNLabelChamp();
    lbMontantGauche = new SNLabelChamp();
    
    // ======== this ========
    setMinimumSize(new Dimension(1015, 330));
    setPreferredSize(new Dimension(1015, 330));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlObjectif ========
      {
        pnlObjectif.setName("pnlObjectif");
        pnlObjectif.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlObjectif.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlObjectif.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlObjectif.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlObjectif.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== pnlPlafonds ========
        {
          pnlPlafonds.setTitre("Objectifs : plafonds de chiffre d'affaires pour taux de base");
          pnlPlafonds.setName("pnlPlafonds");
          pnlPlafonds.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPlafonds.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlPlafonds.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPlafonds.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPlafonds.getLayout()).rowWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          
          // ======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlMois1 ========
            {
              pnlMois1.setName("pnlMois1");
              pnlMois1.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMois1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMois1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMois1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMois1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMois ----
              lbMois.setText("Mois");
              lbMois.setName("lbMois");
              pnlMois1.add(lbMois, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNLabelChamp1 ----
              sNLabelChamp1.setText("Montant");
              sNLabelChamp1.setMinimumSize(new Dimension(80, 30));
              sNLabelChamp1.setPreferredSize(new Dimension(80, 30));
              sNLabelChamp1.setName("sNLabelChamp1");
              pnlMois1.add(sNLabelChamp1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois1 ----
              lbMois1.setText("Janvier");
              lbMois1.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois1.setName("lbMois1");
              pnlMois1.add(lbMois1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO01 ----
              RPO01.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO01.setMaximumSize(new Dimension(80, 30));
              RPO01.setMinimumSize(new Dimension(80, 30));
              RPO01.setPreferredSize(new Dimension(80, 30));
              RPO01.setName("RPO01");
              pnlMois1.add(RPO01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois2 ----
              lbMois2.setText("F\u00e9vrier");
              lbMois2.setPreferredSize(new Dimension(40, 30));
              lbMois2.setMinimumSize(new Dimension(40, 30));
              lbMois2.setMaximumSize(new Dimension(40, 30));
              lbMois2.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois2.setName("lbMois2");
              pnlMois1.add(lbMois2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO02 ----
              RPO02.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO02.setMaximumSize(new Dimension(80, 30));
              RPO02.setMinimumSize(new Dimension(80, 30));
              RPO02.setPreferredSize(new Dimension(80, 30));
              RPO02.setName("RPO02");
              pnlMois1.add(RPO02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois3 ----
              lbMois3.setText("Mars");
              lbMois3.setPreferredSize(new Dimension(40, 30));
              lbMois3.setMinimumSize(new Dimension(40, 30));
              lbMois3.setMaximumSize(new Dimension(40, 30));
              lbMois3.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois3.setName("lbMois3");
              pnlMois1.add(lbMois3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- RPO03 ----
              RPO03.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO03.setMaximumSize(new Dimension(80, 30));
              RPO03.setMinimumSize(new Dimension(80, 30));
              RPO03.setPreferredSize(new Dimension(80, 30));
              RPO03.setName("RPO03");
              pnlMois1.add(RPO03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel1.add(pnlMois1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlMois2 ========
            {
              pnlMois2.setName("pnlMois2");
              pnlMois2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMois2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMois2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMois2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMois2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMois14 ----
              lbMois14.setText("Mois");
              lbMois14.setName("lbMois14");
              pnlMois2.add(lbMois14, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNLabelChamp3 ----
              sNLabelChamp3.setText("Montant");
              sNLabelChamp3.setMinimumSize(new Dimension(80, 30));
              sNLabelChamp3.setPreferredSize(new Dimension(80, 30));
              sNLabelChamp3.setName("sNLabelChamp3");
              pnlMois2.add(sNLabelChamp3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois4 ----
              lbMois4.setText("Avril");
              lbMois4.setPreferredSize(new Dimension(40, 30));
              lbMois4.setMinimumSize(new Dimension(40, 30));
              lbMois4.setMaximumSize(new Dimension(40, 30));
              lbMois4.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois4.setName("lbMois4");
              pnlMois2.add(lbMois4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO04 ----
              RPO04.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO04.setMaximumSize(new Dimension(80, 30));
              RPO04.setMinimumSize(new Dimension(80, 30));
              RPO04.setPreferredSize(new Dimension(80, 30));
              RPO04.setName("RPO04");
              pnlMois2.add(RPO04, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois5 ----
              lbMois5.setText("Mai");
              lbMois5.setPreferredSize(new Dimension(40, 30));
              lbMois5.setMinimumSize(new Dimension(40, 30));
              lbMois5.setMaximumSize(new Dimension(40, 30));
              lbMois5.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois5.setName("lbMois5");
              pnlMois2.add(lbMois5, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO05 ----
              RPO05.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO05.setMaximumSize(new Dimension(80, 30));
              RPO05.setMinimumSize(new Dimension(80, 30));
              RPO05.setPreferredSize(new Dimension(80, 30));
              RPO05.setName("RPO05");
              pnlMois2.add(RPO05, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois6 ----
              lbMois6.setText("Juin");
              lbMois6.setPreferredSize(new Dimension(40, 30));
              lbMois6.setMinimumSize(new Dimension(40, 30));
              lbMois6.setMaximumSize(new Dimension(40, 30));
              lbMois6.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois6.setName("lbMois6");
              pnlMois2.add(lbMois6, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- RPO06 ----
              RPO06.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO06.setMaximumSize(new Dimension(80, 30));
              RPO06.setMinimumSize(new Dimension(80, 30));
              RPO06.setPreferredSize(new Dimension(80, 30));
              RPO06.setName("RPO06");
              pnlMois2.add(RPO06, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel1.add(pnlMois2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlMois3 ========
            {
              pnlMois3.setName("pnlMois3");
              pnlMois3.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMois3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMois3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMois3.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMois3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMois15 ----
              lbMois15.setText("Mois");
              lbMois15.setName("lbMois15");
              pnlMois3.add(lbMois15, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNLabelChamp4 ----
              sNLabelChamp4.setText("Montant");
              sNLabelChamp4.setMinimumSize(new Dimension(80, 30));
              sNLabelChamp4.setPreferredSize(new Dimension(80, 30));
              sNLabelChamp4.setName("sNLabelChamp4");
              pnlMois3.add(sNLabelChamp4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois7 ----
              lbMois7.setText("Juillet");
              lbMois7.setPreferredSize(new Dimension(40, 30));
              lbMois7.setMinimumSize(new Dimension(40, 30));
              lbMois7.setMaximumSize(new Dimension(40, 30));
              lbMois7.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois7.setName("lbMois7");
              pnlMois3.add(lbMois7, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO07 ----
              RPO07.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO07.setMaximumSize(new Dimension(80, 30));
              RPO07.setMinimumSize(new Dimension(80, 30));
              RPO07.setPreferredSize(new Dimension(80, 30));
              RPO07.setName("RPO07");
              pnlMois3.add(RPO07, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois8 ----
              lbMois8.setText("Ao\u00fbt");
              lbMois8.setPreferredSize(new Dimension(40, 30));
              lbMois8.setMinimumSize(new Dimension(40, 30));
              lbMois8.setMaximumSize(new Dimension(40, 30));
              lbMois8.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois8.setName("lbMois8");
              pnlMois3.add(lbMois8, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO08 ----
              RPO08.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO08.setMaximumSize(new Dimension(80, 30));
              RPO08.setMinimumSize(new Dimension(80, 30));
              RPO08.setPreferredSize(new Dimension(80, 30));
              RPO08.setName("RPO08");
              pnlMois3.add(RPO08, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois9 ----
              lbMois9.setText("Septembre");
              lbMois9.setPreferredSize(new Dimension(40, 30));
              lbMois9.setMinimumSize(new Dimension(40, 30));
              lbMois9.setMaximumSize(new Dimension(40, 30));
              lbMois9.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois9.setName("lbMois9");
              pnlMois3.add(lbMois9, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- RPO09 ----
              RPO09.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO09.setMaximumSize(new Dimension(80, 30));
              RPO09.setMinimumSize(new Dimension(80, 30));
              RPO09.setPreferredSize(new Dimension(80, 30));
              RPO09.setName("RPO09");
              pnlMois3.add(RPO09, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel1.add(pnlMois3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlMois4 ========
            {
              pnlMois4.setName("pnlMois4");
              pnlMois4.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlMois4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlMois4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlMois4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlMois4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMois16 ----
              lbMois16.setText("Mois");
              lbMois16.setName("lbMois16");
              pnlMois4.add(lbMois16, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNLabelChamp5 ----
              sNLabelChamp5.setText("Montant");
              sNLabelChamp5.setMinimumSize(new Dimension(80, 30));
              sNLabelChamp5.setPreferredSize(new Dimension(80, 30));
              sNLabelChamp5.setName("sNLabelChamp5");
              pnlMois4.add(sNLabelChamp5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois10 ----
              lbMois10.setText("Octobre");
              lbMois10.setPreferredSize(new Dimension(40, 30));
              lbMois10.setMinimumSize(new Dimension(40, 30));
              lbMois10.setMaximumSize(new Dimension(40, 30));
              lbMois10.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois10.setName("lbMois10");
              pnlMois4.add(lbMois10, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO10 ----
              RPO10.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO10.setMaximumSize(new Dimension(80, 30));
              RPO10.setMinimumSize(new Dimension(80, 30));
              RPO10.setPreferredSize(new Dimension(80, 30));
              RPO10.setName("RPO10");
              pnlMois4.add(RPO10, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois11 ----
              lbMois11.setText("Novembre");
              lbMois11.setPreferredSize(new Dimension(40, 30));
              lbMois11.setMinimumSize(new Dimension(40, 30));
              lbMois11.setMaximumSize(new Dimension(40, 30));
              lbMois11.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois11.setName("lbMois11");
              pnlMois4.add(lbMois11, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- RPO11 ----
              RPO11.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO11.setMaximumSize(new Dimension(80, 30));
              RPO11.setMinimumSize(new Dimension(80, 30));
              RPO11.setPreferredSize(new Dimension(80, 30));
              RPO11.setName("RPO11");
              pnlMois4.add(RPO11, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMois12 ----
              lbMois12.setText("D\u00e9cembre");
              lbMois12.setPreferredSize(new Dimension(40, 30));
              lbMois12.setMinimumSize(new Dimension(40, 30));
              lbMois12.setMaximumSize(new Dimension(40, 30));
              lbMois12.setHorizontalTextPosition(SwingConstants.CENTER);
              lbMois12.setName("lbMois12");
              pnlMois4.add(lbMois12, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- RPO12 ----
              RPO12.setFont(new Font("sansserif", Font.PLAIN, 14));
              RPO12.setMaximumSize(new Dimension(80, 30));
              RPO12.setMinimumSize(new Dimension(80, 30));
              RPO12.setPreferredSize(new Dimension(80, 30));
              RPO12.setName("RPO12");
              pnlMois4.add(RPO12, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel1.add(pnlMois4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlafonds.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTauxPlafond ========
          {
            pnlTauxPlafond.setName("pnlTauxPlafond");
            pnlTauxPlafond.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTauxPlafond.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlTauxPlafond.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTauxPlafond.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTauxPlafond.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbTauxPlafond ----
            lbTauxPlafond.setText("Taux de plafond atteint");
            lbTauxPlafond.setName("lbTauxPlafond");
            pnlTauxPlafond.add(lbTauxPlafond, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RPCO2 ----
            RPCO2.setPreferredSize(new Dimension(72, 30));
            RPCO2.setMinimumSize(new Dimension(72, 30));
            RPCO2.setMaximumSize(new Dimension(72, 30));
            RPCO2.setName("RPCO2");
            pnlTauxPlafond.add(RPCO2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- sNLabelUnite1 ----
            sNLabelUnite1.setText("%");
            sNLabelUnite1.setName("sNLabelUnite1");
            pnlTauxPlafond.add(sNLabelUnite1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPlafonds.add(pnlTauxPlafond, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlObjectif.add(pnlPlafonds, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlObjectif);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- lbMontantDroite ----
    lbMontantDroite.setText("Montant");
    lbMontantDroite.setHorizontalAlignment(SwingConstants.LEADING);
    lbMontantDroite.setHorizontalTextPosition(SwingConstants.CENTER);
    lbMontantDroite.setName("lbMontantDroite");
    
    // ---- lbMontantGauche ----
    lbMontantGauche.setText("Montant");
    lbMontantGauche.setHorizontalAlignment(SwingConstants.LEADING);
    lbMontantGauche.setHorizontalTextPosition(SwingConstants.CENTER);
    lbMontantGauche.setName("lbMontantGauche");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlObjectif;
  private SNPanelTitre pnlPlafonds;
  private SNPanel sNPanel1;
  private SNPanel pnlMois1;
  private SNLabelChamp lbMois;
  private SNLabelChamp sNLabelChamp1;
  private SNLabelChamp lbMois1;
  private XRiTextField RPO01;
  private SNLabelChamp lbMois2;
  private XRiTextField RPO02;
  private SNLabelChamp lbMois3;
  private XRiTextField RPO03;
  private SNPanel pnlMois2;
  private SNLabelChamp lbMois14;
  private SNLabelChamp sNLabelChamp3;
  private SNLabelChamp lbMois4;
  private XRiTextField RPO04;
  private SNLabelChamp lbMois5;
  private XRiTextField RPO05;
  private SNLabelChamp lbMois6;
  private XRiTextField RPO06;
  private SNPanel pnlMois3;
  private SNLabelChamp lbMois15;
  private SNLabelChamp sNLabelChamp4;
  private SNLabelChamp lbMois7;
  private XRiTextField RPO07;
  private SNLabelChamp lbMois8;
  private XRiTextField RPO08;
  private SNLabelChamp lbMois9;
  private XRiTextField RPO09;
  private SNPanel pnlMois4;
  private SNLabelChamp lbMois16;
  private SNLabelChamp sNLabelChamp5;
  private SNLabelChamp lbMois10;
  private XRiTextField RPO10;
  private SNLabelChamp lbMois11;
  private XRiTextField RPO11;
  private SNLabelChamp lbMois12;
  private XRiTextField RPO12;
  private SNPanel pnlTauxPlafond;
  private SNLabelChamp lbTauxPlafond;
  private XRiTextField RPCO2;
  private SNLabelUnite sNLabelUnite1;
  private SNLabelChamp lbMontantDroite;
  private SNLabelChamp lbMontantGauche;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
