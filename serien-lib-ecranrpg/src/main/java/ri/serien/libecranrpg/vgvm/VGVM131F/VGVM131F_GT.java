
package ri.serien.libecranrpg.vgvm.VGVM131F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVM131F_GT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] EDTBON_Value = { "", "1", "R", "G", "C", "T", };
  
  public VGVM131F_GT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    EDTBON.setValeurs(EDTBON_Value, null);
    EDTETQ.setValeurs("E", group_EDTETQ);
    EDTETQ_1.setValeurs("O");
    ETQTRI.setValeursSelection("X", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("Caractéristiques de la préparation numéro @NTNUM@")).trim()));
    EXLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB@")).trim());
    TRLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle("Gestion d'une préparation");
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    EDTBON = new XRiComboBox();
    NTLIB = new XRiTextField();
    EXLIB = new RiZoneSortie();
    TRLIB = new RiZoneSortie();
    ETQTRI = new XRiCheckBox();
    OBJ_31 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_36 = new JLabel();
    EDTETQ = new XRiRadioButton();
    EDTETQ_1 = new XRiRadioButton();
    NTDT1X = new XRiCalendrier();
    NTMEX = new XRiTextField();
    NTCTR = new XRiTextField();
    NTPRE = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    group_EDTETQ = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(770, 325));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Caract\u00e9ristiques de la pr\u00e9paration num\u00e9ro @NTNUM@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- EDTBON ----
          EDTBON.setModel(new DefaultComboBoxModel(new String[] {
            "Aucune",
            "Edition des bons",
            "R\u00e9\u00e9dition des bons",
            "Edition de la pr\u00e9paration",
            "Contr\u00f4le de l'exp\u00e9dition sur canal",
            "Bordereau de chargement"
          }));
          EDTBON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTBON.setName("EDTBON");
          panel1.add(EDTBON);
          EDTBON.setBounds(185, 181, 187, EDTBON.getPreferredSize().height);

          //---- NTLIB ----
          NTLIB.setComponentPopupMenu(BTD);
          NTLIB.setName("NTLIB");
          panel1.add(NTLIB);
          NTLIB.setBounds(185, 245, 310, NTLIB.getPreferredSize().height);

          //---- EXLIB ----
          EXLIB.setText("@EXLIB@");
          EXLIB.setName("EXLIB");
          panel1.add(EXLIB);
          EXLIB.setBounds(225, 81, 220, EXLIB.getPreferredSize().height);

          //---- TRLIB ----
          TRLIB.setText("@TRLIB@");
          TRLIB.setName("TRLIB");
          panel1.add(TRLIB);
          TRLIB.setBounds(225, 115, 220, TRLIB.getPreferredSize().height);

          //---- ETQTRI ----
          ETQTRI.setText("non tri\u00e9es par commande");
          ETQTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ETQTRI.setName("ETQTRI");
          panel1.add(ETQTRI);
          ETQTRI.setBounds(370, 209, 195, 28);

          //---- OBJ_31 ----
          OBJ_31.setText("Mode d'exp\u00e9dition");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(25, 79, 155, 28);

          //---- OBJ_33 ----
          OBJ_33.setText("Code transporteur");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(25, 113, 155, 28);

          //---- OBJ_35 ----
          OBJ_35.setText("Edition");
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(25, 180, 155, 28);

          //---- OBJ_39 ----
          OBJ_39.setText("Edition d'\u00e9tiquettes");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(25, 209, 155, 28);

          //---- OBJ_37 ----
          OBJ_37.setText("Pr\u00e9parateur");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(25, 147, 155, 28);

          //---- OBJ_20 ----
          OBJ_20.setText("Livraison le");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(25, 45, 155, 28);

          //---- OBJ_36 ----
          OBJ_36.setText("Remarque");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(25, 245, 155, 28);

          //---- EDTETQ ----
          EDTETQ.setText("Clients");
          EDTETQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTETQ.setName("EDTETQ");
          panel1.add(EDTETQ);
          EDTETQ.setBounds(185, 209, 80, 28);

          //---- EDTETQ_1 ----
          EDTETQ_1.setText("Articles");
          EDTETQ_1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EDTETQ_1.setName("EDTETQ_1");
          panel1.add(EDTETQ_1);
          EDTETQ_1.setBounds(275, 209, 80, 28);

          //---- NTDT1X ----
          NTDT1X.setComponentPopupMenu(BTD);
          NTDT1X.setName("NTDT1X");
          panel1.add(NTDT1X);
          NTDT1X.setBounds(185, 45, 105, NTDT1X.getPreferredSize().height);

          //---- NTMEX ----
          NTMEX.setComponentPopupMenu(BTD);
          NTMEX.setName("NTMEX");
          panel1.add(NTMEX);
          NTMEX.setBounds(185, 79, 34, NTMEX.getPreferredSize().height);

          //---- NTCTR ----
          NTCTR.setComponentPopupMenu(BTD);
          NTCTR.setName("NTCTR");
          panel1.add(NTCTR);
          NTCTR.setBounds(185, 113, 34, NTCTR.getPreferredSize().height);

          //---- NTPRE ----
          NTPRE.setComponentPopupMenu(BTD);
          NTPRE.setName("NTPRE");
          panel1.add(NTPRE);
          NTPRE.setBounds(185, 147, 44, NTPRE.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 303, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- group_EDTETQ ----
    group_EDTETQ.add(EDTETQ);
    group_EDTETQ.add(EDTETQ_1);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox EDTBON;
  private XRiTextField NTLIB;
  private RiZoneSortie EXLIB;
  private RiZoneSortie TRLIB;
  private XRiCheckBox ETQTRI;
  private JLabel OBJ_31;
  private JLabel OBJ_33;
  private JLabel OBJ_35;
  private JLabel OBJ_39;
  private JLabel OBJ_37;
  private JLabel OBJ_20;
  private JLabel OBJ_36;
  private XRiRadioButton EDTETQ;
  private XRiRadioButton EDTETQ_1;
  private XRiCalendrier NTDT1X;
  private XRiTextField NTMEX;
  private XRiTextField NTCTR;
  private XRiTextField NTPRE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private ButtonGroup group_EDTETQ;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
