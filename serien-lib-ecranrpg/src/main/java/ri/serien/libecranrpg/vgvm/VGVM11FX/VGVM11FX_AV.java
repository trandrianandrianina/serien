
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_AV extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Construteur.
   */
  public VGVM11FX_AV(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    initDiverses();
    CHXDUP.setValeurs("", buttonGroup1);
    CHXDUP_1.setValeurs("1");
    CHXDUP_2.setValeurs("2");
    CHXDUP_3.setValeurs("3");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererAffichageMenus(null);
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    CHXDUP_1 = new XRiRadioButton();
    CHXDUP_2 = new XRiRadioButton();
    CHXDUP_3 = new XRiRadioButton();
    CHXDUP = new XRiRadioButton();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(665, 220));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Duplication d'adresse dans le bon en cours"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- CHXDUP_1 ----
          CHXDUP_1.setText("Duplication dans l'adresse de livraison");
          CHXDUP_1.setName("CHXDUP_1");
          panel1.add(CHXDUP_1);
          CHXDUP_1.setBounds(35, 40, 330, 25);

          //---- CHXDUP_2 ----
          CHXDUP_2.setText("Duplication dans l'adresse de facturation");
          CHXDUP_2.setName("CHXDUP_2");
          panel1.add(CHXDUP_2);
          CHXDUP_2.setBounds(35, 75, 330, 25);

          //---- CHXDUP_3 ----
          CHXDUP_3.setText("Duplication dans les deux adresses");
          CHXDUP_3.setName("CHXDUP_3");
          panel1.add(CHXDUP_3);
          CHXDUP_3.setBounds(35, 110, 330, 25);

          //---- CHXDUP ----
          CHXDUP.setText("Pas de duplication");
          CHXDUP.setName("CHXDUP");
          panel1.add(CHXDUP);
          CHXDUP.setBounds(35, 145, 330, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 475, 200);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    buttonGroup1.add(CHXDUP_1);
    buttonGroup1.add(CHXDUP_2);
    buttonGroup1.add(CHXDUP_3);
    buttonGroup1.add(CHXDUP);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton CHXDUP_1;
  private XRiRadioButton CHXDUP_2;
  private XRiRadioButton CHXDUP_3;
  private XRiRadioButton CHXDUP;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
