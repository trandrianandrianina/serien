
package ri.serien.libecranrpg.vgvm.VGVM07FX;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM07FX_BR extends SNPanelEcranRPG implements ioFrame {
  
  private Font font_bold = null;
  private Font font_plain = null;
  private String[] hostvalues = { "00", "01", "02", "03", "04", "05", "06" };
  private String[] hosttexts = null;
  private final static String BOUTON_CONSULTATION = "Consulter";
  private final static String BOUTON_CREATION = "Créer";
  private final static String BOUTON_MODIFICATION = "Modifier";
  private final static String BOUTON_SUPPRESSION = "Supprimer";
  private final static String BOUTON_CALCUL = "Calculer";
  private final static String BOUTON_TRANSFORMATION_EN_CONDITION = "Transformer en condition";
  
  public VGVM07FX_BR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    initDiverses();
    font_bold = new Font(Font.SANS_SERIF, Font.BOLD, 14);
    font_plain = new Font(Font.SANS_SERIF, Font.PLAIN, 14);
    W0TAR2.setValeurs(hostvalues, hosttexts);
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CONSULTATION, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 'e', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRESSION, 's', true);
    snBarreBouton.ajouterBouton(BOUTON_CALCUL, 'x', true);
    snBarreBouton.ajouterBouton(BOUTON_TRANSFORMATION_EN_CONDITION, 't', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    T1CNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1CNV@")).trim());
    WNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    T1RAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1RAT@")).trim());
    ULIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULIB@")).trim());
    tfQuantite.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTE@")).trim());
    tfUniteVente.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNV@")).trim());
    tfDevise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1DEV@")).trim());
    tfDebut.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W0DTDX@")).trim());
    tfFin.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W0DTFX@")).trim());
    tfPrixRevient.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRVA@")).trim());
    tfCoef.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOE1@")).trim());
    tfMarge.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAR1@")).trim());
    tfRemise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WREM1@")).trim());
    tfTarif.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W0TAR1@")).trim());
    tfFaitLe.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUDAT@")).trim());
    tfA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUHEU@")).trim());
    tfPar.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUSER@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererLesErreurs("19");
    int isConsult = Lexical.MODE_CONSULTATION;
    boolean isFlash = lexique.isTrue("74");
    boolean isNegociation = lexique.isTrue("(74) AND (75)");
    
    boolean isCondition = lexique.isTrue("(74) AND (75) AND (N51)");
    boolean isConsultation = (lexique.getMode() == Lexical.MODE_CONSULTATION);
    boolean isTTC = lexique.isTrue("69");
    
    if (lexique.getMode() == isConsult) {
      radioButtonremise.setSelected(false);
      radioButtonremise.setEnabled(false);
      radioButtonprix.setSelected(false);
      radioButtonprix.setEnabled(false);
      radioButtoncoef.setSelected(false);
      radioButtoncoef.setEnabled(false);
      radioButtonmarge.setSelected(false);
      radioButtonmarge.setEnabled(false);
      radioButtontarif.setSelected(false);
      radioButtontarif.setEnabled(false);
    }
    else {
      radioButtonremise.setEnabled(true);
      radioButtonprix.setEnabled(true);
      radioButtoncoef.setEnabled(true);
      radioButtonmarge.setEnabled(true);
      radioButtontarif.setEnabled(true);
    }
    
    hosttexts = new String[] { "Sur tarif actuel", "Colonne 1 : " + lexique.HostFieldGetData("ATPX1").trim(),
        "Colonne 2 : " + lexique.HostFieldGetData("ATPX2").trim(), "Colonne 3 : " + lexique.HostFieldGetData("ATPX3").trim(),
        "Colonne 4 : " + lexique.HostFieldGetData("ATPX4").trim(), "Colonne 5 : " + lexique.HostFieldGetData("ATPX5").trim(),
        "Colonne 6 : " + lexique.HostFieldGetData("ATPX6").trim() };
    if (lexique.isTrue("(N87) AND (N64) AND (N69)")) { // zones A
      tfPrixFlashObtenu.setText(lexique.HostFieldGetData("WHT2A"));
    }
    if (lexique.isTrue("(N87) AND (64) AND (N69)")) { // zones B
      tfPrixFlashObtenu.setText(lexique.HostFieldGetData("WHT2B"));
    }
    if (lexique.isTrue("(87) AND (N69)")) { // zones C
      tfPrixFlashObtenu.setText(lexique.HostFieldGetData("WHT2C"));
    }
    
    if (lexique.isTrue("(N87) AND (N64) AND (69)")) { // zones A
      tfPrixFlashObtenu.setText(lexique.HostFieldGetData("WTTC2A"));
    }
    if (lexique.isTrue("(N87) AND (64) AND (69)")) { // zones B
      tfPrixFlashObtenu.setText(lexique.HostFieldGetData("WTTC2B"));
    }
    if (lexique.isTrue("(87) AND (69)")) { // zones C
      tfPrixFlashObtenu.setText(lexique.HostFieldGetData("WTTC2C"));
    }
    W0TAR2.setModel(new DefaultComboBoxModel(hosttexts));
    if (lexique.getMode() != isConsult) {
      WREM2.setEnabled(radioButtonremise.isSelected());
      WHT2A.setEnabled(radioButtonprix.isSelected());
      WHT2B.setEnabled(radioButtonprix.isSelected());
      WHT2C.setEnabled(radioButtonprix.isSelected());
      WTTC2A.setEnabled(radioButtonprix.isSelected());
      WTTC2B.setEnabled(radioButtonprix.isSelected());
      WTTC2C.setEnabled(radioButtonprix.isSelected());
      WCOE2.setEnabled(radioButtoncoef.isSelected());
      WMAR2.setEnabled(radioButtonmarge.isSelected());
      W0TAR2.setEnabled(radioButtontarif.isSelected());
    }
    // Mettre en gras la colonne qui a été modifiée
    if (lexique.isTrue("42")) {
      WREM2.setFont(font_bold);
    }
    else {
      WREM2.setFont(font_plain);
    }
    if (lexique.isTrue("43")) {
      WHT2A.setFont(font_bold);
      WHT2B.setFont(font_bold);
      WHT2C.setFont(font_bold);
      WTTC2A.setFont(font_bold);
      WTTC2B.setFont(font_bold);
      WTTC2C.setFont(font_bold);
    }
    else {
      WHT2A.setFont(font_plain);
      WHT2B.setFont(font_plain);
      WHT2C.setFont(font_plain);
      WTTC2A.setFont(font_plain);
      WTTC2B.setFont(font_plain);
      WTTC2C.setFont(font_plain);
    }
    if (lexique.isTrue("44")) {
      WHT2A.setFont(font_bold);
      WHT2B.setFont(font_bold);
      WHT2C.setFont(font_bold);
      WTTC2A.setFont(font_bold);
      WTTC2B.setFont(font_bold);
      WTTC2C.setFont(font_bold);
    }
    else {
      WHT2A.setFont(font_plain);
      WHT2B.setFont(font_plain);
      WHT2C.setFont(font_plain);
      WTTC2A.setFont(font_plain);
      WTTC2B.setFont(font_plain);
      WTTC2C.setFont(font_plain);
    }
    if (lexique.isTrue("45")) {
      WCOE2.setFont(font_bold);
    }
    else {
      WCOE2.setFont(font_plain);
    }
    if (lexique.isTrue("46")) {
      WMAR2.setFont(font_bold);
    }
    else {
      WMAR2.setFont(font_plain);
    }
    // Visiblité des tarifs avec ou sans décimale, patati patata
    if (lexique.isTrue("(N87) AND (N64)")) {
      tfPrxiBase.setText(lexique.HostFieldGetData("WBASA"));
      tfPrixAppliqueHT.setText(lexique.HostFieldGetData("WHT1A"));
      tfPrixAppliqueTTC.setText(lexique.HostFieldGetData("WTTC1A"));
    }
    else if (lexique.isTrue("(N87) AND (64)")) {
      tfPrxiBase.setText(lexique.HostFieldGetData("WBASB"));
      tfPrixAppliqueHT.setText(lexique.HostFieldGetData("WHT1B"));
      tfPrixAppliqueTTC.setText(lexique.HostFieldGetData("WTTC1B"));
    }
    else if (lexique.isTrue("87")) {
      tfPrxiBase.setText(lexique.HostFieldGetData("WBASC"));
      tfPrixAppliqueHT.setText(lexique.HostFieldGetData("WHT1C"));
      tfPrixAppliqueTTC.setText(lexique.HostFieldGetData("WTTC1C"));
    }
    tfDevise.setVisible(!lexique.HostFieldGetData("T1DEV").trim().equals(""));
    lbDevise.setVisible(tfDevise.isVisible());
    // Prix saisis avec ou sans taxe, pour moi se sera sans, merci
    WHT2A.setVisible(lexique.isTrue("(N87) AND (N64) AND (N69)"));
    WHT2B.setVisible(lexique.isTrue("(N87) AND (64) AND (N69)"));
    WHT2C.setVisible(lexique.isTrue("(87) AND (N69)"));
    WTTC2A.setVisible(lexique.isTrue("(N87) AND (N64) AND (69)"));
    WTTC2B.setVisible(lexique.isTrue("(N87) AND (64) AND (69)"));
    WTTC2C.setVisible(lexique.isTrue("(87) AND (69)"));
    
    if (isFlash) {
      setTitle("Prix flash");
      if (isTTC) {
        radioButtonprix.setText("Prix saisi TTC");
        radioButtontarif.setText("Choix de tarif TTC");
        lbPrixFlash.setText("Prix Flash TTC obtenu");
      }
      else {
        radioButtonprix.setText("Prix saisi HT");
        radioButtontarif.setText("Choix de tarif HT");
        lbPrixFlash.setText("Prix Flash HT obtenu");
      }
    }
    
    if (isCondition) {
      lbPrixFlash.setText("Condition obtenue");
    }
    
    if (isNegociation) {
      lbPrixFlash.setText("Prix négocié");
    }
    
    lbPeriodeValidite.setVisible(T1DTDX.isVisible());
    lbAu.setVisible(T1DTFX.isVisible());
    
    tfTarif.setEnabled(!isConsultation);
    tfPrxiBase.setEnabled(!isConsultation);
    tfRemise.setEnabled(!isConsultation);
    tfCoef.setEnabled(!isConsultation);
    tfMarge.setEnabled(!isConsultation);
    tfPrixAppliqueHT.setEnabled(!isConsultation);
    tfPrixAppliqueTTC.setEnabled(!isConsultation);
    tfDebut.setEnabled(!isConsultation);
    tfFin.setEnabled(!isConsultation);
    tfPrixRevient.setEnabled(!isConsultation);
    W0TAR2.setEnabled(!isConsultation);
    WREM2.setEnabled(!isConsultation);
    WREM3.setEnabled(!isConsultation);
    WREM4.setEnabled(!isConsultation);
    WHT2A.setEnabled(!isConsultation);
    WCOE2.setEnabled(!isConsultation);
    WMAR2.setEnabled(!isConsultation);
    T1DTDX.setEnabled(!isConsultation);
    T1DTFX.setEnabled(!isConsultation);
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
    // Visibilité des boutons
    rafraichirBoutons();
    initRadioButton();
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_CONSULTATION)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRESSION)) {
        lexique.HostScreenSendKey(this, "F16");
      }
      else if (pSNBouton.isBouton(BOUTON_CALCUL)) {
        lexique.HostScreenSendKey(this, "F6");
      }
      else if (pSNBouton.isBouton(BOUTON_TRANSFORMATION_EN_CONDITION)) {
        lexique.HostScreenSendKey(this, "F20");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Visibilité des boutons
   */
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_CONSULTATION, lexique.isTrue("(52)"));
    snBarreBouton.activerBouton(BOUTON_CREATION, lexique.isTrue("(54)"));
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, lexique.isTrue("(53)"));
    snBarreBouton.activerBouton(BOUTON_CALCUL, lexique.isTrue("(51) or (52)"));
    snBarreBouton.activerBouton(BOUTON_TRANSFORMATION_EN_CONDITION, lexique.isTrue("(N54)"));
    snBarreBouton.activerBouton(BOUTON_SUPPRESSION, lexique.isTrue("(52) or (53)"));
  }
  
  private void radioButtonremiseActionPerformed(ActionEvent e) {
    WREM2.setEnabled(radioButtonremise.isSelected());
    WREM2.setEnabled(radioButtonremise.isSelected());
    WHT2A.setEnabled(radioButtonprix.isSelected());
    WHT2B.setEnabled(radioButtonprix.isSelected());
    WHT2C.setEnabled(radioButtonprix.isSelected());
    WTTC2A.setEnabled(radioButtonprix.isSelected());
    WTTC2B.setEnabled(radioButtonprix.isSelected());
    WTTC2C.setEnabled(radioButtonprix.isSelected());
    WCOE2.setEnabled(radioButtoncoef.isSelected());
    WMAR2.setEnabled(radioButtonmarge.isSelected());
    W0TAR2.setEnabled(radioButtontarif.isSelected());
  }
  
  private void radioButtontarifActionPerformed(ActionEvent e) {
    WREM2.setEnabled(radioButtonremise.isSelected());
    WHT2A.setEnabled(radioButtonprix.isSelected());
    WHT2B.setEnabled(radioButtonprix.isSelected());
    WHT2C.setEnabled(radioButtonprix.isSelected());
    WTTC2A.setEnabled(radioButtonprix.isSelected());
    WTTC2B.setEnabled(radioButtonprix.isSelected());
    WTTC2C.setEnabled(radioButtonprix.isSelected());
    WCOE2.setEnabled(radioButtoncoef.isSelected());
    WMAR2.setEnabled(radioButtonmarge.isSelected());
    W0TAR2.setEnabled(radioButtontarif.isSelected());
  }
  
  private void initRadioButton() {
    radioButtonremise.setSelected(lexique.HostFieldGetData("T1TCD").equals("R"));
    radioButtonprix.setSelected(lexique.HostFieldGetData("T1TCD").equals("N"));
    radioButtontarif.setSelected(lexique.HostFieldGetData("T1TCD").equals("B"));
    radioButtoncoef.setSelected(lexique.HostFieldGetData("T1TCD").equals("K"));
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    pnlBandeau = new SNPanel();
    lbClient = new SNLabelChamp();
    T1CNV = new SNTexte();
    WNOM = new SNTexte();
    lbArticle = new SNLabelChamp();
    T1RAT = new SNTexte();
    ULIB = new SNTexte();
    pnlTarifNormal = new SNPanelTitre();
    tfQuantite = new SNTexte();
    lbQuantite = new SNLabelChamp();
    lbUniteVente = new SNLabelChamp();
    tfUniteVente = new SNTexte();
    lbDevise = new SNLabelChamp();
    tfDevise = new SNTexte();
    pnlDateValidite = new SNPanelTitre();
    tfDebut = new SNTexte();
    lbDebut = new SNLabelChamp();
    tfFin = new SNTexte();
    lbFin = new SNLabelChamp();
    tfPrixRevient = new SNTexte();
    lbPrixRevient = new SNLabelChamp();
    pnlPrixApplique = new SNPanelTitre();
    tfPrixAppliqueHT = new SNTexte();
    lbPrixHT = new SNLabelChamp();
    lbPrixTTC = new SNLabelChamp();
    tfPrixAppliqueTTC = new SNTexte();
    pnlConditions = new SNPanelTitre();
    lbRemise = new SNLabelChamp();
    label1 = new SNLabelChamp();
    tfCoef = new SNTexte();
    tfMarge = new SNTexte();
    label5 = new SNLabelChamp();
    lbCoef = new SNLabelChamp();
    lbMarge = new SNLabelChamp();
    tfRemise = new SNTexte();
    pnlBase = new SNPanelTitre();
    tfPrxiBase = new SNTexte();
    tfTarif = new SNTexte();
    lbTarif = new SNLabelChamp();
    lbPrixBase = new SNLabelChamp();
    pnlNegociations = new SNPanelTitre();
    WREM2 = new XRiTextField();
    WHT2A = new XRiTextField();
    WHT2B = new XRiTextField();
    WHT2C = new XRiTextField();
    WCOE2 = new XRiTextField();
    WMAR2 = new XRiTextField();
    W0TAR2 = new XRiComboBox();
    radioButtonremise = new JRadioButton();
    radioButtontarif = new JRadioButton();
    lbPourcentage = new SNLabelChamp();
    T1DTDX = new XRiCalendrier();
    T1DTFX = new XRiCalendrier();
    lbPeriodeValidite = new SNLabelChamp();
    lbAu = new SNLabelChamp();
    lbPourcentage4 = new SNLabelChamp();
    radioButtonprix = new JRadioButton();
    radioButtoncoef = new JRadioButton();
    radioButtonmarge = new JRadioButton();
    WREM3 = new XRiTextField();
    lbPourcentage2 = new SNLabelChamp();
    WREM4 = new XRiTextField();
    lbPourcentage3 = new SNLabelChamp();
    WTTC2A = new XRiTextField();
    WTTC2C = new XRiTextField();
    WTTC2B = new XRiTextField();
    pnlPrixFlash = new SNPanelTitre();
    lbPrixFlash = new SNLabelChamp();
    tfPrixFlashObtenu = new SNTexte();
    pnlInfo = new SNPanelTitre();
    lbFaitLe = new SNLabelChamp();
    lbA = new SNLabelChamp();
    lbPar = new SNLabelChamp();
    tfFaitLe = new SNTexte();
    tfA = new SNTexte();
    tfPar = new SNTexte();
    
    // ======== this ========
    setMinimumSize(new Dimension(1360, 655));
    setPreferredSize(new Dimension(1320, 655));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanelPrincipal1 ========
    {
      sNPanelPrincipal1.setName("sNPanelPrincipal1");
      sNPanelPrincipal1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlBandeau ========
      {
        pnlBandeau.setName("pnlBandeau");
        pnlBandeau.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlBandeau.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlBandeau.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlBandeau.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlBandeau.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbClient ----
        lbClient.setText("Client");
        lbClient.setMinimumSize(new Dimension(50, 30));
        lbClient.setPreferredSize(new Dimension(50, 30));
        lbClient.setName("lbClient");
        pnlBandeau.add(lbClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- T1CNV ----
        T1CNV.setToolTipText("Code condition");
        T1CNV.setOpaque(false);
        T1CNV.setText("@T1CNV@");
        T1CNV.setEditable(false);
        T1CNV.setEnabled(false);
        T1CNV.setMinimumSize(new Dimension(80, 30));
        T1CNV.setPreferredSize(new Dimension(80, 30));
        T1CNV.setName("T1CNV");
        pnlBandeau.add(T1CNV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WNOM ----
        WNOM.setText("@WNOM@");
        WNOM.setOpaque(false);
        WNOM.setEditable(false);
        WNOM.setEnabled(false);
        WNOM.setPreferredSize(new Dimension(368, 30));
        WNOM.setMinimumSize(new Dimension(368, 30));
        WNOM.setName("WNOM");
        pnlBandeau.add(WNOM, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle.setPreferredSize(new Dimension(100, 30));
        lbArticle.setMinimumSize(new Dimension(100, 30));
        lbArticle.setName("lbArticle");
        pnlBandeau.add(lbArticle, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- T1RAT ----
        T1RAT.setOpaque(false);
        T1RAT.setText("@T1RAT@");
        T1RAT.setEditable(false);
        T1RAT.setEnabled(false);
        T1RAT.setMinimumSize(new Dimension(260, 30));
        T1RAT.setPreferredSize(new Dimension(260, 30));
        T1RAT.setName("T1RAT");
        pnlBandeau.add(T1RAT, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- ULIB ----
        ULIB.setText("@ULIB@");
        ULIB.setOpaque(false);
        ULIB.setEnabled(false);
        ULIB.setPreferredSize(new Dimension(368, 30));
        ULIB.setMinimumSize(new Dimension(368, 30));
        ULIB.setName("ULIB");
        pnlBandeau.add(ULIB, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(pnlBandeau,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTarifNormal ========
      {
        pnlTarifNormal.setOpaque(false);
        pnlTarifNormal.setTitre("Tarif normal du client");
        pnlTarifNormal.setName("pnlTarifNormal");
        pnlTarifNormal.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTarifNormal.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlTarifNormal.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlTarifNormal.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlTarifNormal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- tfQuantite ----
        tfQuantite.setText("@WQTE@");
        tfQuantite.setHorizontalAlignment(SwingConstants.LEFT);
        tfQuantite.setPreferredSize(new Dimension(80, 30));
        tfQuantite.setMinimumSize(new Dimension(80, 30));
        tfQuantite.setEditable(false);
        tfQuantite.setEnabled(false);
        tfQuantite.setName("tfQuantite");
        pnlTarifNormal.add(tfQuantite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbQuantite ----
        lbQuantite.setText("Quantit\u00e9");
        lbQuantite.setComponentPopupMenu(null);
        lbQuantite.setHorizontalAlignment(SwingConstants.LEFT);
        lbQuantite.setMinimumSize(new Dimension(55, 30));
        lbQuantite.setPreferredSize(new Dimension(55, 30));
        lbQuantite.setName("lbQuantite");
        pnlTarifNormal.add(lbQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbUniteVente ----
        lbUniteVente.setText("Unit\u00e9 de vente");
        lbUniteVente.setName("lbUniteVente");
        pnlTarifNormal.add(lbUniteVente, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfUniteVente ----
        tfUniteVente.setText("@WUNV@");
        tfUniteVente.setMinimumSize(new Dimension(36, 30));
        tfUniteVente.setPreferredSize(new Dimension(36, 30));
        tfUniteVente.setEditable(false);
        tfUniteVente.setEnabled(false);
        tfUniteVente.setName("tfUniteVente");
        pnlTarifNormal.add(tfUniteVente, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDevise ----
        lbDevise.setText("Devise ");
        lbDevise.setMinimumSize(new Dimension(100, 30));
        lbDevise.setPreferredSize(new Dimension(100, 30));
        lbDevise.setName("lbDevise");
        pnlTarifNormal.add(lbDevise, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfDevise ----
        tfDevise.setText("@T1DEV@");
        tfDevise.setPreferredSize(new Dimension(44, 30));
        tfDevise.setMinimumSize(new Dimension(44, 30));
        tfDevise.setEditable(false);
        tfDevise.setEnabled(false);
        tfDevise.setName("tfDevise");
        pnlTarifNormal.add(tfDevise, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDateValidite ========
        {
          pnlDateValidite.setTitre("Dates de validit\u00e9");
          pnlDateValidite.setName("pnlDateValidite");
          pnlDateValidite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateValidite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateValidite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDateValidite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateValidite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfDebut ----
          tfDebut.setComponentPopupMenu(null);
          tfDebut.setText("@W0DTDX@");
          tfDebut.setHorizontalAlignment(SwingConstants.CENTER);
          tfDebut.setMinimumSize(new Dimension(104, 30));
          tfDebut.setPreferredSize(new Dimension(104, 30));
          tfDebut.setEditable(false);
          tfDebut.setEnabled(false);
          tfDebut.setName("tfDebut");
          pnlDateValidite.add(tfDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDebut ----
          lbDebut.setText("D\u00e9but");
          lbDebut.setComponentPopupMenu(null);
          lbDebut.setHorizontalAlignment(SwingConstants.LEFT);
          lbDebut.setPreferredSize(new Dimension(104, 30));
          lbDebut.setMinimumSize(new Dimension(104, 30));
          lbDebut.setName("lbDebut");
          pnlDateValidite.add(lbDebut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfFin ----
          tfFin.setComponentPopupMenu(null);
          tfFin.setText("@W0DTFX@");
          tfFin.setHorizontalAlignment(SwingConstants.CENTER);
          tfFin.setMinimumSize(new Dimension(104, 30));
          tfFin.setPreferredSize(new Dimension(104, 30));
          tfFin.setEditable(false);
          tfFin.setEnabled(false);
          tfFin.setName("tfFin");
          pnlDateValidite.add(tfFin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbFin ----
          lbFin.setText("Fin");
          lbFin.setComponentPopupMenu(null);
          lbFin.setHorizontalAlignment(SwingConstants.LEFT);
          lbFin.setPreferredSize(new Dimension(104, 30));
          lbFin.setMinimumSize(new Dimension(104, 30));
          lbFin.setName("lbFin");
          pnlDateValidite.add(lbFin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixRevient ----
          tfPrixRevient.setText("@WPRVA@");
          tfPrixRevient.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixRevient.setEditable(false);
          tfPrixRevient.setEnabled(false);
          tfPrixRevient.setName("tfPrixRevient");
          pnlDateValidite.add(tfPrixRevient, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- lbPrixRevient ----
          lbPrixRevient.setText("Prix de revient");
          lbPrixRevient.setComponentPopupMenu(null);
          lbPrixRevient.setPreferredSize(new Dimension(100, 30));
          lbPrixRevient.setMinimumSize(new Dimension(100, 30));
          lbPrixRevient.setHorizontalAlignment(SwingConstants.LEFT);
          lbPrixRevient.setName("lbPrixRevient");
          pnlDateValidite.add(lbPrixRevient, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlTarifNormal.add(pnlDateValidite, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== pnlPrixApplique ========
        {
          pnlPrixApplique.setTitre("Prix appliqu\u00e9");
          pnlPrixApplique.setName("pnlPrixApplique");
          pnlPrixApplique.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixApplique.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixApplique.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixApplique.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixApplique.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfPrixAppliqueHT ----
          tfPrixAppliqueHT.setEditable(false);
          tfPrixAppliqueHT.setPreferredSize(new Dimension(120, 30));
          tfPrixAppliqueHT.setMinimumSize(new Dimension(120, 30));
          tfPrixAppliqueHT.setEnabled(false);
          tfPrixAppliqueHT.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixAppliqueHT.setName("tfPrixAppliqueHT");
          pnlPrixApplique.add(tfPrixAppliqueHT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPrixHT ----
          lbPrixHT.setText("Prix H.T.");
          lbPrixHT.setComponentPopupMenu(null);
          lbPrixHT.setHorizontalAlignment(SwingConstants.LEFT);
          lbPrixHT.setPreferredSize(new Dimension(120, 30));
          lbPrixHT.setMinimumSize(new Dimension(120, 30));
          lbPrixHT.setName("lbPrixHT");
          pnlPrixApplique.add(lbPrixHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixTTC ----
          lbPrixTTC.setText("Prix TTC");
          lbPrixTTC.setComponentPopupMenu(null);
          lbPrixTTC.setHorizontalAlignment(SwingConstants.LEFT);
          lbPrixTTC.setPreferredSize(new Dimension(120, 19));
          lbPrixTTC.setMinimumSize(new Dimension(120, 19));
          lbPrixTTC.setName("lbPrixTTC");
          pnlPrixApplique.add(lbPrixTTC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfPrixAppliqueTTC ----
          tfPrixAppliqueTTC.setEditable(false);
          tfPrixAppliqueTTC.setMinimumSize(new Dimension(120, 30));
          tfPrixAppliqueTTC.setPreferredSize(new Dimension(120, 30));
          tfPrixAppliqueTTC.setEnabled(false);
          tfPrixAppliqueTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixAppliqueTTC.setName("tfPrixAppliqueTTC");
          pnlPrixApplique.add(tfPrixAppliqueTTC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifNormal.add(pnlPrixApplique, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlConditions ========
        {
          pnlConditions.setTitre("Conditions");
          pnlConditions.setName("pnlConditions");
          pnlConditions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlConditions.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlConditions.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlConditions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlConditions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbRemise ----
          lbRemise.setText("Remise");
          lbRemise.setComponentPopupMenu(null);
          lbRemise.setMinimumSize(new Dimension(100, 30));
          lbRemise.setPreferredSize(new Dimension(100, 30));
          lbRemise.setHorizontalAlignment(SwingConstants.LEFT);
          lbRemise.setName("lbRemise");
          pnlConditions.add(lbRemise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- label1 ----
          label1.setText("%");
          label1.setPreferredSize(new Dimension(20, 30));
          label1.setMinimumSize(new Dimension(20, 30));
          label1.setName("label1");
          pnlConditions.add(label1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoef ----
          tfCoef.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          tfCoef.setComponentPopupMenu(null);
          tfCoef.setText("@WCOE1@");
          tfCoef.setPreferredSize(new Dimension(60, 30));
          tfCoef.setMinimumSize(new Dimension(60, 30));
          tfCoef.setEditable(false);
          tfCoef.setEnabled(false);
          tfCoef.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoef.setName("tfCoef");
          pnlConditions.add(tfCoef, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfMarge ----
          tfMarge.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
          tfMarge.setComponentPopupMenu(null);
          tfMarge.setText("@WMAR1@");
          tfMarge.setHorizontalAlignment(SwingConstants.RIGHT);
          tfMarge.setPreferredSize(new Dimension(40, 30));
          tfMarge.setMinimumSize(new Dimension(40, 30));
          tfMarge.setEditable(false);
          tfMarge.setEnabled(false);
          tfMarge.setName("tfMarge");
          pnlConditions.add(tfMarge, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- label5 ----
          label5.setText("%");
          label5.setMinimumSize(new Dimension(20, 30));
          label5.setPreferredSize(new Dimension(20, 30));
          label5.setName("label5");
          pnlConditions.add(label5, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- lbCoef ----
          lbCoef.setText("Coefficient");
          lbCoef.setComponentPopupMenu(null);
          lbCoef.setHorizontalAlignment(SwingConstants.LEFT);
          lbCoef.setMinimumSize(new Dimension(100, 30));
          lbCoef.setPreferredSize(new Dimension(100, 30));
          lbCoef.setName("lbCoef");
          pnlConditions.add(lbCoef, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMarge ----
          lbMarge.setText("Marge");
          lbMarge.setComponentPopupMenu(null);
          lbMarge.setHorizontalAlignment(SwingConstants.LEFT);
          lbMarge.setPreferredSize(new Dimension(100, 30));
          lbMarge.setMinimumSize(new Dimension(100, 30));
          lbMarge.setName("lbMarge");
          pnlConditions.add(lbMarge, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfRemise ----
          tfRemise.setText("@WREM1@");
          tfRemise.setHorizontalAlignment(SwingConstants.RIGHT);
          tfRemise.setPreferredSize(new Dimension(50, 30));
          tfRemise.setMinimumSize(new Dimension(50, 30));
          tfRemise.setEditable(false);
          tfRemise.setEnabled(false);
          tfRemise.setName("tfRemise");
          pnlConditions.add(tfRemise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlTarifNormal.add(pnlConditions, new GridBagConstraints(2, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlBase ========
        {
          pnlBase.setTitre("Base");
          pnlBase.setName("pnlBase");
          pnlBase.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlBase.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlBase.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlBase.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlBase.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfPrxiBase ----
          tfPrxiBase.setEditable(false);
          tfPrxiBase.setMinimumSize(new Dimension(120, 30));
          tfPrxiBase.setPreferredSize(new Dimension(120, 30));
          tfPrxiBase.setEnabled(false);
          tfPrxiBase.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrxiBase.setName("tfPrxiBase");
          pnlBase.add(tfPrxiBase, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- tfTarif ----
          tfTarif.setText("@W0TAR1@");
          tfTarif.setHorizontalAlignment(SwingConstants.RIGHT);
          tfTarif.setMinimumSize(new Dimension(36, 30));
          tfTarif.setPreferredSize(new Dimension(36, 30));
          tfTarif.setEditable(false);
          tfTarif.setEnabled(false);
          tfTarif.setName("tfTarif");
          pnlBase.add(tfTarif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbTarif ----
          lbTarif.setText("Tarif");
          lbTarif.setComponentPopupMenu(null);
          lbTarif.setPreferredSize(new Dimension(100, 30));
          lbTarif.setMinimumSize(new Dimension(100, 30));
          lbTarif.setHorizontalAlignment(SwingConstants.LEFT);
          lbTarif.setName("lbTarif");
          pnlBase.add(lbTarif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixBase ----
          lbPrixBase.setText("Prix de base");
          lbPrixBase.setComponentPopupMenu(null);
          lbPrixBase.setPreferredSize(new Dimension(100, 30));
          lbPrixBase.setMinimumSize(new Dimension(100, 30));
          lbPrixBase.setHorizontalAlignment(SwingConstants.LEFT);
          lbPrixBase.setName("lbPrixBase");
          pnlBase.add(lbPrixBase, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlTarifNormal.add(pnlBase, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      sNPanelPrincipal1.add(pnlTarifNormal,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlNegociations ========
      {
        pnlNegociations.setOpaque(false);
        pnlNegociations.setTitre("N\u00e9gociation (les calculs ci-dessous s'effectuent toujours sur le prix de base)");
        pnlNegociations.setName("pnlNegociations");
        pnlNegociations.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlNegociations.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlNegociations.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlNegociations.getLayout()).columnWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlNegociations.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- WREM2 ----
        WREM2.setMinimumSize(new Dimension(50, 30));
        WREM2.setPreferredSize(new Dimension(50, 30));
        WREM2.setFont(new Font("sansserif", Font.PLAIN, 14));
        WREM2.setName("WREM2");
        pnlNegociations.add(WREM2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WHT2A ----
        WHT2A.setMinimumSize(new Dimension(100, 30));
        WHT2A.setPreferredSize(new Dimension(100, 30));
        WHT2A.setFont(new Font("sansserif", Font.PLAIN, 14));
        WHT2A.setName("WHT2A");
        pnlNegociations.add(WHT2A, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WHT2B ----
        WHT2B.setMinimumSize(new Dimension(120, 28));
        WHT2B.setPreferredSize(new Dimension(120, 28));
        WHT2B.setName("WHT2B");
        pnlNegociations.add(WHT2B, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WHT2C ----
        WHT2C.setPreferredSize(new Dimension(80, 28));
        WHT2C.setMinimumSize(new Dimension(80, 28));
        WHT2C.setName("WHT2C");
        pnlNegociations.add(WHT2C, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WCOE2 ----
        WCOE2.setPreferredSize(new Dimension(60, 30));
        WCOE2.setMinimumSize(new Dimension(60, 30));
        WCOE2.setFont(new Font("sansserif", Font.PLAIN, 14));
        WCOE2.setName("WCOE2");
        pnlNegociations.add(WCOE2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WMAR2 ----
        WMAR2.setPreferredSize(new Dimension(40, 30));
        WMAR2.setMinimumSize(new Dimension(40, 30));
        WMAR2.setFont(new Font("sansserif", Font.PLAIN, 14));
        WMAR2.setName("WMAR2");
        pnlNegociations.add(WMAR2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- W0TAR2 ----
        W0TAR2.setMinimumSize(new Dimension(63, 30));
        W0TAR2.setPreferredSize(new Dimension(63, 30));
        W0TAR2.setFont(new Font("sansserif", Font.PLAIN, 14));
        W0TAR2.setName("W0TAR2");
        pnlNegociations.add(W0TAR2, new GridBagConstraints(1, 0, 7, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- radioButtonremise ----
        radioButtonremise.setText("Remise sur prix tarif");
        radioButtonremise.setFont(new Font("sansserif", Font.PLAIN, 14));
        radioButtonremise.setMinimumSize(new Dimension(149, 30));
        radioButtonremise.setPreferredSize(new Dimension(149, 30));
        radioButtonremise.setName("radioButtonremise");
        radioButtonremise.addActionListener(e -> radioButtonremiseActionPerformed(e));
        pnlNegociations.add(radioButtonremise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- radioButtontarif ----
        radioButtontarif.setText("Choix de tarif");
        radioButtontarif.setFont(new Font("sansserif", Font.PLAIN, 14));
        radioButtontarif.setMinimumSize(new Dimension(106, 30));
        radioButtontarif.setPreferredSize(new Dimension(106, 30));
        radioButtontarif.setName("radioButtontarif");
        radioButtontarif.addActionListener(e -> radioButtontarifActionPerformed(e));
        pnlNegociations.add(radioButtontarif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbPourcentage ----
        lbPourcentage.setText("% ");
        lbPourcentage.setHorizontalAlignment(SwingConstants.LEFT);
        lbPourcentage.setPreferredSize(new Dimension(20, 30));
        lbPourcentage.setMinimumSize(new Dimension(20, 30));
        lbPourcentage.setName("lbPourcentage");
        pnlNegociations.add(lbPourcentage, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- T1DTDX ----
        T1DTDX.setPreferredSize(new Dimension(120, 30));
        T1DTDX.setMinimumSize(new Dimension(120, 30));
        T1DTDX.setFont(new Font("sansserif", Font.PLAIN, 14));
        T1DTDX.setName("T1DTDX");
        pnlNegociations.add(T1DTDX, new GridBagConstraints(1, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 10), 0, 0));
        
        // ---- T1DTFX ----
        T1DTFX.setMinimumSize(new Dimension(120, 30));
        T1DTFX.setPreferredSize(new Dimension(120, 30));
        T1DTFX.setFont(new Font("sansserif", Font.PLAIN, 14));
        T1DTFX.setName("T1DTFX");
        pnlNegociations.add(T1DTFX, new GridBagConstraints(4, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 10), 0, 0));
        
        // ---- lbPeriodeValidite ----
        lbPeriodeValidite.setText("P\u00e9riode de validit\u00e9 de ce prix, du");
        lbPeriodeValidite.setPreferredSize(new Dimension(100, 30));
        lbPeriodeValidite.setMinimumSize(new Dimension(100, 30));
        lbPeriodeValidite.setHorizontalAlignment(SwingConstants.LEFT);
        lbPeriodeValidite.setName("lbPeriodeValidite");
        pnlNegociations.add(lbPeriodeValidite, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
        
        // ---- lbAu ----
        lbAu.setText("au");
        lbAu.setHorizontalAlignment(SwingConstants.CENTER);
        lbAu.setPreferredSize(new Dimension(20, 30));
        lbAu.setMinimumSize(new Dimension(20, 30));
        lbAu.setName("lbAu");
        pnlNegociations.add(lbAu, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 10), 0, 0));
        
        // ---- lbPourcentage4 ----
        lbPourcentage4.setText("%");
        lbPourcentage4.setHorizontalAlignment(SwingConstants.LEFT);
        lbPourcentage4.setMinimumSize(new Dimension(20, 16));
        lbPourcentage4.setPreferredSize(new Dimension(20, 16));
        lbPourcentage4.setName("lbPourcentage4");
        pnlNegociations.add(lbPourcentage4, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- radioButtonprix ----
        radioButtonprix.setText("Prix saisi");
        radioButtonprix.setFont(new Font("sansserif", Font.PLAIN, 14));
        radioButtonprix.setPreferredSize(new Dimension(78, 30));
        radioButtonprix.setMinimumSize(new Dimension(78, 30));
        radioButtonprix.setName("radioButtonprix");
        radioButtonprix.addActionListener(e -> radioButtonremiseActionPerformed(e));
        pnlNegociations.add(radioButtonprix, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- radioButtoncoef ----
        radioButtoncoef.setText("Coefficient sur prix de revient");
        radioButtoncoef.setFont(new Font("sansserif", Font.PLAIN, 14));
        radioButtoncoef.setMinimumSize(new Dimension(207, 30));
        radioButtoncoef.setPreferredSize(new Dimension(207, 30));
        radioButtoncoef.setName("radioButtoncoef");
        radioButtoncoef.addActionListener(e -> radioButtonremiseActionPerformed(e));
        pnlNegociations.add(radioButtoncoef, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- radioButtonmarge ----
        radioButtonmarge.setText("Marge par rapport au prix de revient");
        radioButtonmarge.setFont(new Font("sansserif", Font.PLAIN, 14));
        radioButtonmarge.setPreferredSize(new Dimension(252, 30));
        radioButtonmarge.setMinimumSize(new Dimension(252, 30));
        radioButtonmarge.setName("radioButtonmarge");
        radioButtonmarge.addActionListener(e -> radioButtonremiseActionPerformed(e));
        pnlNegociations.add(radioButtonmarge, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WREM3 ----
        WREM3.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        WREM3.setComponentPopupMenu(null);
        WREM3.setMinimumSize(new Dimension(50, 30));
        WREM3.setPreferredSize(new Dimension(50, 30));
        WREM3.setFont(new Font("sansserif", Font.PLAIN, 14));
        WREM3.setName("WREM3");
        pnlNegociations.add(WREM3, new GridBagConstraints(3, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbPourcentage2 ----
        lbPourcentage2.setText("%");
        lbPourcentage2.setMinimumSize(new Dimension(20, 30));
        lbPourcentage2.setPreferredSize(new Dimension(20, 30));
        lbPourcentage2.setName("lbPourcentage2");
        pnlNegociations.add(lbPourcentage2, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WREM4 ----
        WREM4.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        WREM4.setComponentPopupMenu(null);
        WREM4.setPreferredSize(new Dimension(50, 30));
        WREM4.setMinimumSize(new Dimension(50, 30));
        WREM4.setFont(new Font("sansserif", Font.PLAIN, 14));
        WREM4.setName("WREM4");
        pnlNegociations.add(WREM4, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbPourcentage3 ----
        lbPourcentage3.setText("%");
        lbPourcentage3.setPreferredSize(new Dimension(20, 30));
        lbPourcentage3.setMinimumSize(new Dimension(20, 30));
        lbPourcentage3.setName("lbPourcentage3");
        pnlNegociations.add(lbPourcentage3, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WTTC2A ----
        WTTC2A.setName("WTTC2A");
        pnlNegociations.add(WTTC2A, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WTTC2C ----
        WTTC2C.setName("WTTC2C");
        pnlNegociations.add(WTTC2C, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- WTTC2B ----
        WTTC2B.setName("WTTC2B");
        pnlNegociations.add(WTTC2B, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ======== pnlPrixFlash ========
        {
          pnlPrixFlash.setOpaque(false);
          pnlPrixFlash.setName("pnlPrixFlash");
          pnlPrixFlash.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixFlash.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixFlash.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPrixFlash.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixFlash.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbPrixFlash ----
          lbPrixFlash.setText("Prix flash obtenu");
          lbPrixFlash.setFont(new Font("sansserif", Font.BOLD, 14));
          lbPrixFlash.setName("lbPrixFlash");
          pnlPrixFlash.add(lbPrixFlash, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPrixFlashObtenu ----
          tfPrixFlashObtenu.setText("text");
          tfPrixFlashObtenu.setFont(new Font("sansserif", Font.BOLD, 14));
          tfPrixFlashObtenu.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixFlashObtenu.setMinimumSize(new Dimension(120, 30));
          tfPrixFlashObtenu.setPreferredSize(new Dimension(120, 30));
          tfPrixFlashObtenu.setEditable(false);
          tfPrixFlashObtenu.setEnabled(false);
          tfPrixFlashObtenu.setName("tfPrixFlashObtenu");
          pnlPrixFlash.add(tfPrixFlashObtenu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlNegociations.add(pnlPrixFlash, new GridBagConstraints(8, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(pnlNegociations,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlInfo ========
      {
        pnlInfo.setOpaque(false);
        pnlInfo.setName("pnlInfo");
        pnlInfo.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInfo.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInfo.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlInfo.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlInfo.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbFaitLe ----
        lbFaitLe.setText("Fait le ");
        lbFaitLe.setComponentPopupMenu(null);
        lbFaitLe.setMinimumSize(new Dimension(100, 30));
        lbFaitLe.setPreferredSize(new Dimension(50, 30));
        lbFaitLe.setName("lbFaitLe");
        pnlInfo.add(lbFaitLe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbA ----
        lbA.setText("\u00e0");
        lbA.setComponentPopupMenu(null);
        lbA.setHorizontalAlignment(SwingConstants.CENTER);
        lbA.setPreferredSize(new Dimension(20, 30));
        lbA.setName("lbA");
        pnlInfo.add(lbA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbPar ----
        lbPar.setText("par");
        lbPar.setComponentPopupMenu(null);
        lbPar.setHorizontalAlignment(SwingConstants.CENTER);
        lbPar.setPreferredSize(new Dimension(50, 30));
        lbPar.setName("lbPar");
        pnlInfo.add(lbPar, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfFaitLe ----
        tfFaitLe.setText("@WUDAT@");
        tfFaitLe.setHorizontalAlignment(SwingConstants.CENTER);
        tfFaitLe.setPreferredSize(new Dimension(104, 30));
        tfFaitLe.setMinimumSize(new Dimension(104, 30));
        tfFaitLe.setEditable(false);
        tfFaitLe.setEnabled(false);
        tfFaitLe.setName("tfFaitLe");
        pnlInfo.add(tfFaitLe, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfA ----
        tfA.setText("@WUHEU@");
        tfA.setMinimumSize(new Dimension(50, 30));
        tfA.setPreferredSize(new Dimension(50, 30));
        tfA.setEditable(false);
        tfA.setEnabled(false);
        tfA.setName("tfA");
        pnlInfo.add(tfA, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfPar ----
        tfPar.setText("@WUSER@");
        tfPar.setPreferredSize(new Dimension(368, 30));
        tfPar.setMinimumSize(new Dimension(368, 30));
        tfPar.setEditable(false);
        tfPar.setEnabled(false);
        tfPar.setName("tfPar");
        pnlInfo.add(tfPar, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelPrincipal1.add(pnlInfo,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelPrincipal1, BorderLayout.CENTER);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(radioButtonremise);
    buttonGroup1.add(radioButtontarif);
    buttonGroup1.add(radioButtonprix);
    buttonGroup1.add(radioButtoncoef);
    buttonGroup1.add(radioButtonmarge);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private SNPanel pnlBandeau;
  private SNLabelChamp lbClient;
  private SNTexte T1CNV;
  private SNTexte WNOM;
  private SNLabelChamp lbArticle;
  private SNTexte T1RAT;
  private SNTexte ULIB;
  private SNPanelTitre pnlTarifNormal;
  private SNTexte tfQuantite;
  private SNLabelChamp lbQuantite;
  private SNLabelChamp lbUniteVente;
  private SNTexte tfUniteVente;
  private SNLabelChamp lbDevise;
  private SNTexte tfDevise;
  private SNPanelTitre pnlDateValidite;
  private SNTexte tfDebut;
  private SNLabelChamp lbDebut;
  private SNTexte tfFin;
  private SNLabelChamp lbFin;
  private SNTexte tfPrixRevient;
  private SNLabelChamp lbPrixRevient;
  private SNPanelTitre pnlPrixApplique;
  private SNTexte tfPrixAppliqueHT;
  private SNLabelChamp lbPrixHT;
  private SNLabelChamp lbPrixTTC;
  private SNTexte tfPrixAppliqueTTC;
  private SNPanelTitre pnlConditions;
  private SNLabelChamp lbRemise;
  private SNLabelChamp label1;
  private SNTexte tfCoef;
  private SNTexte tfMarge;
  private SNLabelChamp label5;
  private SNLabelChamp lbCoef;
  private SNLabelChamp lbMarge;
  private SNTexte tfRemise;
  private SNPanelTitre pnlBase;
  private SNTexte tfPrxiBase;
  private SNTexte tfTarif;
  private SNLabelChamp lbTarif;
  private SNLabelChamp lbPrixBase;
  private SNPanelTitre pnlNegociations;
  private XRiTextField WREM2;
  private XRiTextField WHT2A;
  private XRiTextField WHT2B;
  private XRiTextField WHT2C;
  private XRiTextField WCOE2;
  private XRiTextField WMAR2;
  private XRiComboBox W0TAR2;
  private JRadioButton radioButtonremise;
  private JRadioButton radioButtontarif;
  private SNLabelChamp lbPourcentage;
  private XRiCalendrier T1DTDX;
  private XRiCalendrier T1DTFX;
  private SNLabelChamp lbPeriodeValidite;
  private SNLabelChamp lbAu;
  private SNLabelChamp lbPourcentage4;
  private JRadioButton radioButtonprix;
  private JRadioButton radioButtoncoef;
  private JRadioButton radioButtonmarge;
  private XRiTextField WREM3;
  private SNLabelChamp lbPourcentage2;
  private XRiTextField WREM4;
  private SNLabelChamp lbPourcentage3;
  private XRiTextField WTTC2A;
  private XRiTextField WTTC2C;
  private XRiTextField WTTC2B;
  private SNPanelTitre pnlPrixFlash;
  private SNLabelChamp lbPrixFlash;
  private SNTexte tfPrixFlashObtenu;
  private SNPanelTitre pnlInfo;
  private SNLabelChamp lbFaitLe;
  private SNLabelChamp lbA;
  private SNLabelChamp lbPar;
  private SNTexte tfFaitLe;
  private SNTexte tfA;
  private SNTexte tfPar;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
