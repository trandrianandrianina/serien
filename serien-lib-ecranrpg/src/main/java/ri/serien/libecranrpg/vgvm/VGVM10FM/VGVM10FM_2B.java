
package ri.serien.libecranrpg.vgvm.VGVM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM10FM_2B extends SNPanelEcranRPG implements ioFrame {
  
  private String optionCliquee = null;
  
  public VGVM10FM_2B(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    // WOATT.setValeursSelection("N", " ");
    WOANN.setValeursSelection("1", " ");
    WOEXN.setValeursSelection("1", " ");
    setCloseKey("ENTER", "F3", "F12");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riBouton1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT1@ - @LOPT1@")).trim());
    riBouton2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT2@ - @LOPT2@")).trim());
    riBouton3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT3@ - @LOPT3@")).trim());
    riBouton4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT4@ - @LOPT4@")).trim());
    riBouton5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPT5@ - @LOPT5@")).trim());
    LOPT62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOPT62@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    label7.setVisible(WTP62.isVisible());
    
    riBouton1.setVisible(!lexique.HostFieldGetData("OPT1").trim().equalsIgnoreCase(""));
    riBouton2.setVisible(!lexique.HostFieldGetData("OPT2").trim().equalsIgnoreCase(""));
    riBouton3.setVisible(!lexique.HostFieldGetData("OPT3").trim().equalsIgnoreCase(""));
    riBouton4.setVisible(!lexique.HostFieldGetData("OPT4").trim().equalsIgnoreCase(""));
    riBouton5.setVisible(!lexique.HostFieldGetData("OPT5").trim().equalsIgnoreCase(""));
    
    if (!lexique.HostFieldGetData("WTP1").trim().equals("")) {
      riBouton1.doClick();
    }
    if (!lexique.HostFieldGetData("WTP2").trim().equals("")) {
      riBouton2.doClick();
    }
    if (!lexique.HostFieldGetData("WTP3").trim().equals("")) {
      riBouton3.doClick();
    }
    if (!lexique.HostFieldGetData("WTP4").trim().equals("")) {
      riBouton4.doClick();
    }
    if (!lexique.HostFieldGetData("WTP5").trim().equals("")) {
      riBouton5.doClick();
    }
    if (!lexique.HostFieldGetData("WTPCOR").trim().equals("")) {
      riBouton6.doClick();
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Options de fin de traitement"));
    
    riMenu_bt1.setIcon(lexique.chargerImage("images/fin_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    
    lexique.HostFieldPutData("WTP1", 0, " ");
    lexique.HostFieldPutData("WTP2", 0, " ");
    lexique.HostFieldPutData("WTP3", 0, " ");
    lexique.HostFieldPutData("WTP4", 0, " ");
    lexique.HostFieldPutData("WTP5", 0, " ");
    lexique.HostFieldPutData("WTPCOR", 0, " ");
    
    if (optionCliquee != null) {
      lexique.HostFieldPutData(optionCliquee, 0, "X");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riBouton1ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP1";
    option.setText(lexique.HostFieldGetData("OPT1").trim());
  }
  
  private void riBouton2ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP2";
    option.setText(lexique.HostFieldGetData("OPT2").trim());
  }
  
  private void riBouton3ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP3";
    option.setText(lexique.HostFieldGetData("OPT3").trim());
  }
  
  private void riBouton4ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP4";
    option.setText(lexique.HostFieldGetData("OPT4").trim());
  }
  
  private void riBouton5ActionPerformed(ActionEvent e) {
    optionCliquee = "WTP5";
    option.setText(lexique.HostFieldGetData("OPT5").trim());
  }
  
  private void riBouton6ActionPerformed(ActionEvent e) {
    optionCliquee = "WTPCOR";
    option.setText("COR");
  }
  
  private void riMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    panel4 = new JPanel();
    riBouton1 = new SNBoutonLeger();
    riBouton2 = new SNBoutonLeger();
    riBouton3 = new SNBoutonLeger();
    riBouton4 = new SNBoutonLeger();
    riBouton5 = new SNBoutonLeger();
    riBouton6 = new SNBoutonLeger();
    WTP62 = new XRiTextField();
    label7 = new JLabel();
    LOPT62 = new RiZoneSortie();
    label8 = new JLabel();
    option = new RiZoneSortie();
    WOANN = new XRiCheckBox();
    WOEXN = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(660, 310));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);

          //======== riMenu1 ========
          {
            riMenu1.setName("riMenu1");

            //---- riMenu_bt1 ----
            riMenu_bt1.setText("Abandonner");
            riMenu_bt1.setToolTipText("Abandonner");
            riMenu_bt1.setName("riMenu_bt1");
            riMenu_bt1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_bt1ActionPerformed(e);
              }
            });
            riMenu1.add(riMenu_bt1);
          }
          menus_bas.add(riMenu1);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Sur le bon g\u00e9n\u00e9r\u00e9"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(new VerticalLayout());

            //---- riBouton1 ----
            riBouton1.setText("@OPT1@ - @LOPT1@");
            riBouton1.setPreferredSize(new Dimension(20, 26));
            riBouton1.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton1.setMinimumSize(new Dimension(20, 26));
            riBouton1.setMaximumSize(new Dimension(20, 26));
            riBouton1.setName("riBouton1");
            riBouton1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton1ActionPerformed(e);
              }
            });
            panel4.add(riBouton1);

            //---- riBouton2 ----
            riBouton2.setText("@OPT2@ - @LOPT2@");
            riBouton2.setMinimumSize(new Dimension(20, 26));
            riBouton2.setMaximumSize(new Dimension(20, 26));
            riBouton2.setPreferredSize(new Dimension(20, 26));
            riBouton2.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton2.setName("riBouton2");
            riBouton2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton2ActionPerformed(e);
              }
            });
            panel4.add(riBouton2);

            //---- riBouton3 ----
            riBouton3.setText("@OPT3@ - @LOPT3@");
            riBouton3.setMinimumSize(new Dimension(20, 26));
            riBouton3.setMaximumSize(new Dimension(20, 26));
            riBouton3.setPreferredSize(new Dimension(20, 26));
            riBouton3.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton3.setName("riBouton3");
            riBouton3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton3ActionPerformed(e);
              }
            });
            panel4.add(riBouton3);

            //---- riBouton4 ----
            riBouton4.setText("@OPT4@ - @LOPT4@");
            riBouton4.setMinimumSize(new Dimension(20, 26));
            riBouton4.setMaximumSize(new Dimension(20, 26));
            riBouton4.setPreferredSize(new Dimension(20, 26));
            riBouton4.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton4.setName("riBouton4");
            riBouton4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton4ActionPerformed(e);
              }
            });
            panel4.add(riBouton4);

            //---- riBouton5 ----
            riBouton5.setMinimumSize(new Dimension(20, 26));
            riBouton5.setMaximumSize(new Dimension(20, 26));
            riBouton5.setPreferredSize(new Dimension(20, 26));
            riBouton5.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton5.setText("@OPT5@ - @LOPT5@");
            riBouton5.setName("riBouton5");
            riBouton5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton5ActionPerformed(e);
              }
            });
            panel4.add(riBouton5);

            //---- riBouton6 ----
            riBouton6.setText("COR - Modification d'un bon avant traitement");
            riBouton6.setPreferredSize(new Dimension(20, 26));
            riBouton6.setHorizontalAlignment(SwingConstants.LEFT);
            riBouton6.setName("riBouton6");
            riBouton6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBouton6ActionPerformed(e);
              }
            });
            panel4.add(riBouton6);
          }
          panel3.add(panel4);
          panel4.setBounds(25, 35, 395, 160);

          //---- WTP62 ----
          WTP62.setName("WTP62");
          panel3.add(WTP62);
          WTP62.setBounds(80, 230, 24, WTP62.getPreferredSize().height);

          //---- label7 ----
          label7.setText("Edition");
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(25, 234, 45, 20);

          //---- LOPT62 ----
          LOPT62.setText("@LOPT62@");
          LOPT62.setName("LOPT62");
          panel3.add(LOPT62);
          LOPT62.setBounds(110, 232, 310, LOPT62.getPreferredSize().height);

          //---- label8 ----
          label8.setText("Option");
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(25, 202, 45, 20);

          //---- option ----
          option.setName("option");
          panel3.add(option);
          option.setBounds(80, 200, 40, option.getPreferredSize().height);

          //---- WOANN ----
          WOANN.setText("Pas de reliquat");
          WOANN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOANN.setName("WOANN");
          panel3.add(WOANN);
          WOANN.setBounds(25, 270, 140, WOANN.getPreferredSize().height);

          //---- WOEXN ----
          WOEXN.setText("Exp\u00e9dition non facturable");
          WOEXN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WOEXN.setName("WOEXN");
          panel3.add(WOEXN);
          WOEXN.setBounds(260, 270, 185, WOEXN.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 309, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private JPanel p_contenu;
  private JPanel panel3;
  private JPanel panel4;
  private SNBoutonLeger riBouton1;
  private SNBoutonLeger riBouton2;
  private SNBoutonLeger riBouton3;
  private SNBoutonLeger riBouton4;
  private SNBoutonLeger riBouton5;
  private SNBoutonLeger riBouton6;
  private XRiTextField WTP62;
  private JLabel label7;
  private RiZoneSortie LOPT62;
  private JLabel label8;
  private RiZoneSortie option;
  private XRiCheckBox WOANN;
  private XRiCheckBox WOEXN;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
