
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.ModeleListeDocumentStocke;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.VueDocumentStocke;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_AR extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private boolean isConsult = false;
  private Icon bloc_couleur = null;
  private Icon bloc_neutre = null;
  public ODialog dialog_GA = null;
  private boolean isChantier = false;
  private String libelleDirectOuInterne = "";
  
  /**
   * Constructeur.
   */
  public VGVM11FX_AR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    xTitledPanel2.setRightDecoration(bt_encours);
    
    E3NOM.activerModeFantome("Nom ou raison sociale");
    E3CPL.activerModeFantome("Complément de nom");
    E3RUE.activerModeFantome("Rue");
    E3LOC.activerModeFantome("Localité");
    E3CDPX.activerModeFantome("00000");
    E3VILN.activerModeFantome("Ville");
    E3PAYN.activerModeFantome("Pays");
    E3TEL.activerModeFantome("Téléphone");
    E1GBA.setValeursSelection("6", "");
    
    initDiverses();
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/agrandir.png", true));
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    bloc_neutre = lexique.chargerImage("images/bloc_notes_r.png", true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
    E1CAN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CAN@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE1@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE2@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE3@")).trim());
    OBJ_117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE4@")).trim());
    OBJ_118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE5@")).trim());
    E1HRE1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HRE1@")).trim());
    E1HRE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HRE2@")).trim());
    WEPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS@")).trim());
    WEPLF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
    z_devise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    EXLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIBR@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATN@")).trim());
    zs_LAVOIR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAVOIR@")).trim());
    panel2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    LMAIL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPAC@")).trim());
    LMAIL.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    LFAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LTEL@")).trim());
    LFAX.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isConsult = lexique.isTrue("46");
    isChantier = lexique.HostFieldGetData("EXISTCH").trim().equals("1");
    
    bt_encours.setEnabled(!isConsult);
    riBoutonDetail1.setVisible(!isConsult);
    riSousMenu1.setEnabled(!isConsult);
    
    xTitledPanel2.setVisible(lexique.isTrue("(N38) AND (N39)"));
    
    panelChantier.setVisible(isChantier);
    
    // Bloc encours
    WEDEP.setVisible(lexique.isTrue("(N07) AND (N38)"));
    // Dépassement ou disponible (clignoté à l'origine)
    if (WEDEP.isVisible()) {
      lb_depassement.setVisible(true);
      if (lexique.isTrue("(N08)")) {
        lb_depassement.setForeground(Color.GRAY);
      }
      else {
        lb_depassement.setText("Disponible");
        lb_depassement.setForeground(Color.BLACK);
      }
    }
    else {
      lb_depassement.setVisible(false);
    }
    
    zs_LAVOIR.setVisible(!lexique.HostFieldGetData("LAVOIR").trim().isEmpty());
    
    // Devises
    z_devise.setVisible(lexique.isTrue("N86"));
    label_devise.setVisible(z_devise.isVisible());
    
    // Zones personnalisées
    E1TP1.setVisible(!lexique.HostFieldGetData("TIZPE1").trim().isEmpty());
    E1TP2.setVisible(!lexique.HostFieldGetData("TIZPE2").trim().isEmpty());
    E1TP3.setVisible(!lexique.HostFieldGetData("TIZPE3").trim().isEmpty());
    E1TP4.setVisible(!lexique.HostFieldGetData("TIZPE4").trim().isEmpty());
    E1TP5.setVisible(!lexique.HostFieldGetData("TIZPE5").trim().isEmpty());
    
    OBJ_58.setVisible(lexique.HostFieldGetData("WF4ENC").equalsIgnoreCase("I"));
    
    // Label état du bon
    bouton_retour.setToolTipText("Mettre ce bon en attente");
    String etatBon = lexique.HostFieldGetData("ETABON").trim().toUpperCase();
    if (!etatBon.isEmpty()) {
      OBJ_129.setVisible(true);
      OBJ_129.setText("");
      if (etatBon.equals("C")) {
        OBJ_129.setText("Comptabilisé");
      }
      else if (etatBon.equals("T")) {
        OBJ_129.setText("Commande type");
      }
      else if (etatBon.equals("P")) {
        OBJ_129.setText("Positionnement");
      }
      else if (etatBon.equals("A")) {
        OBJ_129.setText("Attente");
      }
      else if (etatBon.equals("E")) {
        OBJ_129.setText("Expédié");
      }
      else if (etatBon.equals("H")) {
        OBJ_129.setText("Validé");
      }
      else if (etatBon.equals("R")) {
        OBJ_129.setText("Réservé");
      }
      else if (etatBon.equals("F")) {
        OBJ_129.setText("Facturé");
        bouton_retour.setToolTipText("Mettre cette facture en attente");
      }
    }
    else {
      OBJ_129.setVisible(false);
    }
    
    if (lexique.HostFieldGetData("WOBST").trim().equals("+")) {
      button1.setIcon(bloc_couleur);
      button1.setToolTipText("Le bloc-notes contient des informations");
    }
    else {
      button1.setIcon(bloc_neutre);
      button1.setToolTipText("Le bloc-notes est vide");
    }
    
    OBJ_61.setVisible(!lexique.HostFieldGetData("WATN").trim().isEmpty());
    label19.setVisible(OBJ_61.isVisible());
    
    // GBA
    int valeurGBA = 0;
    String texteBouton = "";
    String texteLabel = "";
    String e1gba = lexique.HostFieldGetData("E1GBA").trim();
    if (!e1gba.isEmpty()) {
      valeurGBA = Integer.parseInt(e1gba);
    }
    E1GBA.setVisible(valeurGBA == 6 || valeurGBA == 0);
    label_gba.setVisible(valeurGBA != 6 && valeurGBA != 0);
    GBAX.setVisible(valeurGBA != 6 && valeurGBA != 0);
    labelCompGba.setVisible(valeurGBA != 6 && valeurGBA != 0);
    
    switch (valeurGBA) {
      case 1:
        texteBouton = "Différée";
        break;
      case 2:
        texteBouton = "Différée";
        texteLabel = "Livraison directe assurée";
        break;
      case 3:
        texteBouton = "Bon d'achat généré";
        texteLabel = "";
        break;
      case 4:
        texteBouton = "Bon d'achat généré";
        texteLabel = "";
        break;
      case 5:
        texteBouton = "Immédiate";
        break;
      case 6:
        texteBouton = "Immédiate";
        texteLabel = "Livraison directe assurée";
        break;
      case 7:
        texteBouton = "Bon d'achat généré";
        texteLabel = "";
        break;
      case 8:
        texteBouton = "Bon d'achat généré";
        texteLabel = "";
        break;
      case 9:
        texteBouton = "Par ligne";
        texteLabel = "";
        break;
      
      default:
        texteBouton = "Aucune";
        break;
    }
    
    GBAX.setText(texteBouton);
    labelCompGba.setText(texteLabel);
    
    // Barre tête
    E1NFA.setVisible(!lexique.HostFieldGetData("E1NFA").trim().isEmpty());
    OBJ_50.setVisible(E1NFA.isVisible());
    
    if (lexique.HostFieldGetData("E1IN18").trim().equalsIgnoreCase("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (lexique.HostFieldGetData("E1IN18").trim().equalsIgnoreCase("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText().replaceAll(":", "").trim() + " (facturation "
        + lexique.HostFieldGetData("TFLIB").trim() + ")" + libelleDirectOuInterne);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Mode Test Calcul prix en mode débug uniquement
    if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
      try {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(E1ETB.getText());
        IdMagasin idMagasin = IdMagasin.getInstance(idEtablissement, E1MAG.getText());
        lexique.addVariableGlobaleFromSession("idMagasin", idMagasin);
      }
      catch (Exception e) {
        Trace.erreur(e, "Problème avec l'initialisation du client pour le calcul prix de vente");
      }
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 80);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E3NOM");
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 38);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void OBJ_240ActionPerformed(ActionEvent e) {
    if (dialog_GA == null) {
      dialog_GA = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_GA(this));
    }
    dialog_GA.affichePopupPerso();
  }
  
  private void riBoutonDetailMailActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 43);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void visualiserDocumentActionPerformed(ActionEvent e) {
    CritereDocumentStocke critere = new CritereDocumentStocke();
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    critere.setIdEtablissement(idEtablissement);
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstancePourFacture(idEtablissement, Integer.parseInt(lexique.HostFieldGetData("E1NFA")));
    critere.setIdDocumentVente(idDocumentVente);
    ModeleListeDocumentStocke modele = new ModeleListeDocumentStocke(getSession(), critere);
    VueDocumentStocke vue = new VueDocumentStocke(modele);
    vue.afficher();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_28 = new JLabel();
    E1ETB = new RiZoneSortie();
    OBJ_29 = new JLabel();
    E1NFA = new RiZoneSortie();
    WNUM = new RiZoneSortie();
    WSUF = new RiZoneSortie();
    OBJ_31 = new JLabel();
    E1VDE = new XRiTextField();
    WDATEX = new RiZoneSortie();
    label2 = new JLabel();
    OBJ_50 = new JLabel();
    p_tete_droite = new JPanel();
    bt_ecran = new JLabel();
    OBJ_129 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_46 = new RiZoneSortie();
    E3NOM = new XRiTextField();
    E3CPL = new XRiTextField();
    E3RUE = new XRiTextField();
    E3LOC = new XRiTextField();
    WOBS = new XRiTextField();
    E3PAYN = new XRiTextField();
    E3VILN = new XRiTextField();
    E3TEL = new XRiTextField();
    OBJ_121 = new JLabel();
    E3CDPX = new XRiTextField();
    OBJ_119 = new JLabel();
    E3COP = new XRiTextField();
    E1CAN = new RiZoneSortie();
    E1TP1 = new XRiTextField();
    E1TP2 = new XRiTextField();
    E1TP3 = new XRiTextField();
    E1TP4 = new XRiTextField();
    E1TP5 = new XRiTextField();
    E1CTR = new XRiTextField();
    OBJ_114 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_118 = new JLabel();
    panel1 = new JPanel();
    OBJ_43 = new JLabel();
    E1HRE1 = new RiZoneSortie();
    OBJ_44 = new JLabel();
    E1HRE2 = new RiZoneSortie();
    xTitledPanel2 = new JXTitledPanel();
    WEPOS = new RiZoneSortie();
    WEPLF = new RiZoneSortie();
    WEDEP = new RiZoneSortie();
    OBJ_60 = new JLabel();
    lb_depassement = new SNLabelChamp();
    OBJ_62 = new JLabel();
    OBJ_58 = new RiZoneSortie();
    label_devise = new JLabel();
    z_devise = new RiZoneSortie();
    button1 = new JButton();
    E1RCC = new XRiTextField();
    E1NCC = new XRiTextField();
    panelChantier = new JPanel();
    label14 = new JLabel();
    WCHANT = new XRiTextField();
    riBoutonDetailListe1 = new SNBoutonDetail();
    OBJ_30 = new JLabel();
    E1MAG = new XRiTextField();
    OBJ_32 = new RiZoneSortie();
    OBJ_122 = new JLabel();
    E1MEX = new XRiTextField();
    EXLIBR = new RiZoneSortie();
    label19 = new JLabel();
    OBJ_61 = new RiZoneSortie();
    E1GBA = new XRiCheckBox();
    label_gba = new JLabel();
    GBAX = new SNBoutonLeger();
    labelCompGba = new JLabel();
    xTitledSeparator2 = new JXTitledSeparator();
    zs_LAVOIR = new RiZoneSortie();
    panel2 = new JPanel();
    LMAIL = new RiZoneSortie();
    label20 = new JLabel();
    LFAX = new RiZoneSortie();
    riBoutonDetailMail = new SNBoutonDetail();
    label22 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    E1PDS = new XRiTextField();
    OBJ_66 = new JLabel();
    OBJ_110 = new JLabel();
    E1COL = new XRiTextField();
    bt_encours = new SNBoutonDetail();
    riBoutonDetail1 = new SNBoutonDetail();
    BTD2 = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1060, 560));
    setPreferredSize(new Dimension(1060, 560));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(920, 32));
          p_tete_gauche.setMinimumSize(new Dimension(920, 32));
          p_tete_gauche.setMaximumSize(new Dimension(920, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_28 ----
          OBJ_28.setText("Etablissement");
          OBJ_28.setName("OBJ_28");
          
          // ---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Bon");
          OBJ_29.setName("OBJ_29");
          
          // ---- E1NFA ----
          E1NFA.setOpaque(false);
          E1NFA.setText("@E1NFA@");
          E1NFA.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NFA.setName("E1NFA");
          
          // ---- WNUM ----
          WNUM.setText("@WNUM@");
          WNUM.setOpaque(false);
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setName("WSUF");
          
          // ---- OBJ_31 ----
          OBJ_31.setText("Vendeur");
          OBJ_31.setName("OBJ_31");
          
          // ---- E1VDE ----
          E1VDE.setComponentPopupMenu(BTD);
          E1VDE.setName("E1VDE");
          
          // ---- WDATEX ----
          WDATEX.setComponentPopupMenu(BTD);
          WDATEX.setOpaque(false);
          WDATEX.setText("@WDATEX@");
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setName("WDATEX");
          
          // ---- label2 ----
          label2.setText("Date de traitement");
          label2.setName("label2");
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Facture");
          OBJ_50.setName("OBJ_50");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(10, 10, 10)
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1NFA, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE).addGap(47, 47, 47)
                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(50, 50, 50)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1ETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1NFA, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 25,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(label2, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WDATEX, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(50, 0));
          p_tete_droite.setMinimumSize(new Dimension(50, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- bt_ecran ----
          bt_ecran.setBorder(BorderFactory.createEmptyBorder());
          bt_ecran.setToolTipText("Passage \u00e0 l'affichage complet");
          bt_ecran.setPreferredSize(new Dimension(30, 30));
          bt_ecran.setName("bt_ecran");
          bt_ecran.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              bt_ecranMouseClicked(e);
            }
          });
          p_tete_droite.add(bt_ecran);
          
          // ---- OBJ_129 ----
          OBJ_129.setOpaque(false);
          OBJ_129.setBorder(null);
          OBJ_129.setText("obj_129");
          OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getStyle() | Font.BOLD, OBJ_129.getFont().getSize() + 3f));
          OBJ_129.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_129.setName("OBJ_129");
          p_tete_droite.add(OBJ_129);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Acc\u00e8s lignes");
            bouton_valider.setToolTipText("Acc\u00e8s lignes");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Sortir");
            bouton_retour.setToolTipText("Sortir");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Bloc-notes");
              riSousMenu_bt3.setToolTipText("Bloc-notes");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Recherche de bons");
              riSousMenu_bt1.setToolTipText("Fonction recherche de bons");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");
              
              // ---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Recherche d'adresses");
              riSousMenu_bt4.setToolTipText("Recherche d'adresses");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Visualiser les documents");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  visualiserDocumentActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu3);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout) p_centrage.getLayout()).rowHeights = new int[] { 585, 0 };
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(750, 570));
          p_contenu.setName("p_contenu");
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitle("Client factur\u00e9");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);
            
            // ---- OBJ_46 ----
            OBJ_46.setText("@TRLIB@");
            OBJ_46.setName("OBJ_46");
            xTitledPanel1ContentContainer.add(OBJ_46);
            OBJ_46.setBounds(485, 344, 210, OBJ_46.getPreferredSize().height);
            
            // ---- E3NOM ----
            E3NOM.setComponentPopupMenu(BTD);
            E3NOM.setName("E3NOM");
            xTitledPanel1ContentContainer.add(E3NOM);
            E3NOM.setBounds(20, 20, 310, E3NOM.getPreferredSize().height);
            
            // ---- E3CPL ----
            E3CPL.setComponentPopupMenu(BTD);
            E3CPL.setName("E3CPL");
            xTitledPanel1ContentContainer.add(E3CPL);
            E3CPL.setBounds(20, 50, 310, E3CPL.getPreferredSize().height);
            
            // ---- E3RUE ----
            E3RUE.setComponentPopupMenu(BTD);
            E3RUE.setName("E3RUE");
            xTitledPanel1ContentContainer.add(E3RUE);
            E3RUE.setBounds(20, 80, 310, E3RUE.getPreferredSize().height);
            
            // ---- E3LOC ----
            E3LOC.setComponentPopupMenu(BTD);
            E3LOC.setName("E3LOC");
            xTitledPanel1ContentContainer.add(E3LOC);
            E3LOC.setBounds(20, 110, 310, E3LOC.getPreferredSize().height);
            
            // ---- WOBS ----
            WOBS.setComponentPopupMenu(BTD);
            WOBS.setName("WOBS");
            xTitledPanel1ContentContainer.add(WOBS);
            WOBS.setBounds(20, 200, 210, WOBS.getPreferredSize().height);
            
            // ---- E3PAYN ----
            E3PAYN.setComponentPopupMenu(BTD);
            E3PAYN.setName("E3PAYN");
            xTitledPanel1ContentContainer.add(E3PAYN);
            E3PAYN.setBounds(20, 170, 270, E3PAYN.getPreferredSize().height);
            
            // ---- E3VILN ----
            E3VILN.setComponentPopupMenu(BTD);
            E3VILN.setName("E3VILN");
            xTitledPanel1ContentContainer.add(E3VILN);
            E3VILN.setBounds(70, 140, 260, E3VILN.getPreferredSize().height);
            
            // ---- E3TEL ----
            E3TEL.setComponentPopupMenu(BTD);
            E3TEL.setName("E3TEL");
            xTitledPanel1ContentContainer.add(E3TEL);
            E3TEL.setBounds(20, 230, 210, E3TEL.getPreferredSize().height);
            
            // ---- OBJ_121 ----
            OBJ_121.setText("Transporteur");
            OBJ_121.setName("OBJ_121");
            xTitledPanel1ContentContainer.add(OBJ_121);
            OBJ_121.setBounds(365, 346, 85, 20);
            
            // ---- E3CDPX ----
            E3CDPX.setComponentPopupMenu(BTD);
            E3CDPX.setToolTipText("Code postal");
            E3CDPX.setName("E3CDPX");
            xTitledPanel1ContentContainer.add(E3CDPX);
            E3CDPX.setBounds(20, 140, 50, E3CDPX.getPreferredSize().height);
            
            // ---- OBJ_119 ----
            OBJ_119.setText("Canal");
            OBJ_119.setName("OBJ_119");
            xTitledPanel1ContentContainer.add(OBJ_119);
            OBJ_119.setBounds(245, 234, 40, 20);
            
            // ---- E3COP ----
            E3COP.setComponentPopupMenu(BTD);
            E3COP.setName("E3COP");
            xTitledPanel1ContentContainer.add(E3COP);
            E3COP.setBounds(290, 170, 40, E3COP.getPreferredSize().height);
            
            // ---- E1CAN ----
            E1CAN.setText("@E1CAN@");
            E1CAN.setName("E1CAN");
            xTitledPanel1ContentContainer.add(E1CAN);
            E1CAN.setBounds(290, 232, 40, E1CAN.getPreferredSize().height);
            
            // ---- E1TP1 ----
            E1TP1.setComponentPopupMenu(BTD);
            E1TP1.setName("E1TP1");
            xTitledPanel1ContentContainer.add(E1TP1);
            E1TP1.setBounds(190, 490, 30, E1TP1.getPreferredSize().height);
            
            // ---- E1TP2 ----
            E1TP2.setComponentPopupMenu(BTD);
            E1TP2.setName("E1TP2");
            xTitledPanel1ContentContainer.add(E1TP2);
            E1TP2.setBounds(303, 490, 30, 28);
            
            // ---- E1TP3 ----
            E1TP3.setComponentPopupMenu(BTD);
            E1TP3.setName("E1TP3");
            xTitledPanel1ContentContainer.add(E1TP3);
            E1TP3.setBounds(416, 490, 30, 28);
            
            // ---- E1TP4 ----
            E1TP4.setComponentPopupMenu(BTD);
            E1TP4.setName("E1TP4");
            xTitledPanel1ContentContainer.add(E1TP4);
            E1TP4.setBounds(529, 490, 30, 28);
            
            // ---- E1TP5 ----
            E1TP5.setComponentPopupMenu(BTD);
            E1TP5.setName("E1TP5");
            xTitledPanel1ContentContainer.add(E1TP5);
            E1TP5.setBounds(642, 490, 30, 28);
            
            // ---- E1CTR ----
            E1CTR.setComponentPopupMenu(BTD);
            E1CTR.setName("E1CTR");
            xTitledPanel1ContentContainer.add(E1CTR);
            E1CTR.setBounds(450, 342, 34, E1CTR.getPreferredSize().height);
            
            // ---- OBJ_114 ----
            OBJ_114.setText("@TIZPE1@");
            OBJ_114.setName("OBJ_114");
            xTitledPanel1ContentContainer.add(OBJ_114);
            OBJ_114.setBounds(155, 490, 25, 28);
            
            // ---- OBJ_115 ----
            OBJ_115.setText("@TIZPE2@");
            OBJ_115.setName("OBJ_115");
            xTitledPanel1ContentContainer.add(OBJ_115);
            OBJ_115.setBounds(268, 490, 25, 28);
            
            // ---- OBJ_116 ----
            OBJ_116.setText("@TIZPE3@");
            OBJ_116.setName("OBJ_116");
            xTitledPanel1ContentContainer.add(OBJ_116);
            OBJ_116.setBounds(381, 490, 25, 28);
            
            // ---- OBJ_117 ----
            OBJ_117.setText("@TIZPE4@");
            OBJ_117.setName("OBJ_117");
            xTitledPanel1ContentContainer.add(OBJ_117);
            OBJ_117.setBounds(494, 490, 25, 28);
            
            // ---- OBJ_118 ----
            OBJ_118.setText("@TIZPE5@");
            OBJ_118.setName("OBJ_118");
            xTitledPanel1ContentContainer.add(OBJ_118);
            OBJ_118.setBounds(607, 490, 25, 28);
            
            // ======== panel1 ========
            {
              panel1.setBorder(null);
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ---- OBJ_43 ----
              OBJ_43.setText("Heure de commande");
              OBJ_43.setName("OBJ_43");
              panel1.add(OBJ_43);
              OBJ_43.setBounds(10, 5, 130, 24);
              
              // ---- E1HRE1 ----
              E1HRE1.setComponentPopupMenu(null);
              E1HRE1.setText("@E1HRE1@");
              E1HRE1.setHorizontalAlignment(SwingConstants.RIGHT);
              E1HRE1.setName("E1HRE1");
              panel1.add(E1HRE1);
              E1HRE1.setBounds(145, 5, 26, E1HRE1.getPreferredSize().height);
              
              // ---- OBJ_44 ----
              OBJ_44.setText("H");
              OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_44.setName("OBJ_44");
              panel1.add(OBJ_44);
              OBJ_44.setBounds(175, 5, 13, 24);
              
              // ---- E1HRE2 ----
              E1HRE2.setComponentPopupMenu(null);
              E1HRE2.setText("@E1HRE2@");
              E1HRE2.setHorizontalAlignment(SwingConstants.RIGHT);
              E1HRE2.setName("E1HRE2");
              panel1.add(E1HRE2);
              E1HRE2.setBounds(190, 5, 26, E1HRE2.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(475, 260, 230, 35);
            
            // ======== xTitledPanel2 ========
            {
              xTitledPanel2.setTitle("Encours");
              xTitledPanel2.setBorder(new DropShadowBorder());
              xTitledPanel2.setName("xTitledPanel2");
              Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
              xTitledPanel2ContentContainer.setLayout(null);
              
              // ---- WEPOS ----
              WEPOS.setComponentPopupMenu(BTD);
              WEPOS.setText("@WEPOS@");
              WEPOS.setHorizontalAlignment(SwingConstants.RIGHT);
              WEPOS.setName("WEPOS");
              xTitledPanel2ContentContainer.add(WEPOS);
              WEPOS.setBounds(105, 10, 60, WEPOS.getPreferredSize().height);
              
              // ---- WEPLF ----
              WEPLF.setComponentPopupMenu(BTD);
              WEPLF.setText("@WEPLF@");
              WEPLF.setHorizontalAlignment(SwingConstants.RIGHT);
              WEPLF.setName("WEPLF");
              xTitledPanel2ContentContainer.add(WEPLF);
              WEPLF.setBounds(250, 10, 60, WEPLF.getPreferredSize().height);
              
              // ---- WEDEP ----
              WEDEP.setComponentPopupMenu(BTD);
              WEDEP.setText("@WEDEP@");
              WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
              WEDEP.setForeground(Color.red);
              WEDEP.setName("WEDEP");
              xTitledPanel2ContentContainer.add(WEDEP);
              WEDEP.setBounds(105, 43, 60, WEDEP.getPreferredSize().height);
              
              // ---- OBJ_60 ----
              OBJ_60.setText("Position");
              OBJ_60.setName("OBJ_60");
              xTitledPanel2ContentContainer.add(OBJ_60);
              OBJ_60.setBounds(15, 12, 65, 20);
              
              // ---- lb_depassement ----
              lb_depassement.setText("D\u00e9passement");
              lb_depassement.setFont(new Font("sansserif", Font.PLAIN, 12));
              lb_depassement.setHorizontalAlignment(SwingConstants.LEFT);
              lb_depassement.setName("lb_depassement");
              xTitledPanel2ContentContainer.add(lb_depassement);
              lb_depassement.setBounds(15, 45, 95, 20);
              
              // ---- OBJ_62 ----
              OBJ_62.setText("Plafond");
              OBJ_62.setName("OBJ_62");
              xTitledPanel2ContentContainer.add(OBJ_62);
              OBJ_62.setBounds(185, 12, 65, 20);
              
              // ---- OBJ_58 ----
              OBJ_58.setText("Impay\u00e9");
              OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD, OBJ_58.getFont().getSize() + 3f));
              OBJ_58.setForeground(Color.red);
              OBJ_58.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_58.setName("OBJ_58");
              xTitledPanel2ContentContainer.add(OBJ_58);
              OBJ_58.setBounds(190, 43, 120, OBJ_58.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(xTitledPanel2);
            xTitledPanel2.setBounds(375, 20, 335, 110);
            
            // ---- label_devise ----
            label_devise.setText("Devise");
            label_devise.setName("label_devise");
            xTitledPanel1ContentContainer.add(label_devise);
            label_devise.setBounds(20, 400, 65, 24);
            
            // ---- z_devise ----
            z_devise.setText("@DVLIBR@");
            z_devise.setBackground(new Color(220, 220, 220));
            z_devise.setFont(z_devise.getFont().deriveFont(z_devise.getFont().getStyle() | Font.BOLD));
            z_devise.setForeground(Color.red);
            z_devise.setOpaque(false);
            z_devise.setName("z_devise");
            xTitledPanel1ContentContainer.add(z_devise);
            z_devise.setBounds(155, 400, 282, z_devise.getPreferredSize().height);
            
            // ---- button1 ----
            button1.setContentAreaFilled(false);
            button1.setToolTipText("Le bloc-notes contient des informations");
            button1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            button1.setName("button1");
            button1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                button1ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(button1);
            button1.setBounds(335, 21, 27, 27);
            
            // ---- E1RCC ----
            E1RCC.setComponentPopupMenu(BTD2);
            E1RCC.setName("E1RCC");
            xTitledPanel1ContentContainer.add(E1RCC);
            E1RCC.setBounds(155, 310, 275, E1RCC.getPreferredSize().height);
            
            // ---- E1NCC ----
            E1NCC.setComponentPopupMenu(null);
            E1NCC.setName("E1NCC");
            xTitledPanel1ContentContainer.add(E1NCC);
            E1NCC.setBounds(20, 310, 115, E1NCC.getPreferredSize().height);
            
            // ======== panelChantier ========
            {
              panelChantier.setOpaque(false);
              panelChantier.setName("panelChantier");
              panelChantier.setLayout(null);
              
              // ---- label14 ----
              label14.setText("Chantier");
              label14.setName("label14");
              panelChantier.add(label14);
              label14.setBounds(5, 2, 60, 25);
              
              // ---- WCHANT ----
              WCHANT.setComponentPopupMenu(BTD);
              WCHANT.setName("WCHANT");
              panelChantier.add(WCHANT);
              WCHANT.setBounds(67, 0, 74, WCHANT.getPreferredSize().height);
              
              // ---- riBoutonDetailListe1 ----
              riBoutonDetailListe1.setName("riBoutonDetailListe1");
              riBoutonDetailListe1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe1ActionPerformed(e);
                }
              });
              panelChantier.add(riBoutonDetailListe1);
              riBoutonDetailListe1.setBounds(150, 0, 25, 28);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panelChantier.getComponentCount(); i++) {
                  Rectangle bounds = panelChantier.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panelChantier.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panelChantier.setMinimumSize(preferredSize);
                panelChantier.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panelChantier);
            panelChantier.setBounds(480, 309, 225, 30);
            
            // ---- OBJ_30 ----
            OBJ_30.setText("Magasin");
            OBJ_30.setName("OBJ_30");
            xTitledPanel1ContentContainer.add(OBJ_30);
            OBJ_30.setBounds(20, 263, 55, 28);
            
            // ---- E1MAG ----
            E1MAG.setComponentPopupMenu(BTD);
            E1MAG.setName("E1MAG");
            xTitledPanel1ContentContainer.add(E1MAG);
            E1MAG.setBounds(101, 263, 34, E1MAG.getPreferredSize().height);
            
            // ---- OBJ_32 ----
            OBJ_32.setText("@MALIBR@");
            OBJ_32.setOpaque(false);
            OBJ_32.setName("OBJ_32");
            xTitledPanel1ContentContainer.add(OBJ_32);
            OBJ_32.setBounds(155, 265, 200, OBJ_32.getPreferredSize().height);
            
            // ---- OBJ_122 ----
            OBJ_122.setText("Mode de r\u00e9cup\u00e9ration");
            OBJ_122.setName("OBJ_122");
            xTitledPanel1ContentContainer.add(OBJ_122);
            OBJ_122.setBounds(20, 346, 130, 20);
            
            // ---- E1MEX ----
            E1MEX.setComponentPopupMenu(BTD);
            E1MEX.setName("E1MEX");
            xTitledPanel1ContentContainer.add(E1MEX);
            E1MEX.setBounds(155, 342, 34, E1MEX.getPreferredSize().height);
            
            // ---- EXLIBR ----
            EXLIBR.setText("@EXLIBR@");
            EXLIBR.setName("EXLIBR");
            xTitledPanel1ContentContainer.add(EXLIBR);
            EXLIBR.setBounds(200, 344, 150, EXLIBR.getPreferredSize().height);
            
            // ---- label19 ----
            label19.setText("Attention");
            label19.setName("label19");
            xTitledPanel1ContentContainer.add(label19);
            label19.setBounds(20, 428, 75, 25);
            
            // ---- OBJ_61 ----
            OBJ_61.setText("@WATN@");
            OBJ_61.setForeground(new Color(255, 51, 51));
            OBJ_61.setName("OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(155, 430, 210, OBJ_61.getPreferredSize().height);
            
            // ---- E1GBA ----
            E1GBA.setText("G\u00e9n\u00e9ration d'achat");
            E1GBA.setName("E1GBA");
            xTitledPanel1ContentContainer.add(E1GBA);
            E1GBA.setBounds(20, 371, 135, 28);
            
            // ---- label_gba ----
            label_gba.setText("G\u00e9n\u00e9ration d'achat");
            label_gba.setName("label_gba");
            xTitledPanel1ContentContainer.add(label_gba);
            label_gba.setBounds(20, 373, 115, 24);
            
            // ---- GBAX ----
            GBAX.setText("Aucune");
            GBAX.setToolTipText("Aucune g\u00e9n\u00e9ration de bon d'achat");
            GBAX.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GBAX.setName("GBAX");
            GBAX.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_240ActionPerformed(e);
              }
            });
            xTitledPanel1ContentContainer.add(GBAX);
            GBAX.setBounds(155, 374, 160, 22);
            
            // ---- labelCompGba ----
            labelCompGba.setHorizontalAlignment(SwingConstants.CENTER);
            labelCompGba.setForeground(new Color(102, 102, 102));
            labelCompGba.setName("labelCompGba");
            xTitledPanel1ContentContainer.add(labelCompGba);
            labelCompGba.setBounds(320, 375, 160, 18);
            
            // ---- xTitledSeparator2 ----
            xTitledSeparator2.setTitle("R\u00e9f\u00e9rences client");
            xTitledSeparator2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
            xTitledSeparator2.setName("xTitledSeparator2");
            xTitledPanel1ContentContainer.add(xTitledSeparator2);
            xTitledSeparator2.setBounds(20, 290, 410, 25);
            
            // ---- zs_LAVOIR ----
            zs_LAVOIR.setText("@LAVOIR@");
            zs_LAVOIR.setName("zs_LAVOIR");
            xTitledPanel1ContentContainer.add(zs_LAVOIR);
            zs_LAVOIR.setBounds(155, 460, 400, zs_LAVOIR.getPreferredSize().height);
            
            // ======== panel2 ========
            {
              panel2.setBorder(new DropShadowBorder());
              panel2.setOpaque(false);
              panel2.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
              panel2.setName("panel2");
              panel2.setLayout(null);
              
              // ---- LMAIL ----
              LMAIL.setText("@LPAC@");
              LMAIL.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
              LMAIL.setName("LMAIL");
              panel2.add(LMAIL);
              LMAIL.setBounds(85, 5, 237, LMAIL.getPreferredSize().height);
              
              // ---- label20 ----
              label20.setText("T\u00e9l\u00e9phone");
              label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
              label20.setName("label20");
              panel2.add(label20);
              label20.setBounds(15, 37, 80, 24);
              
              // ---- LFAX ----
              LFAX.setText("@LTEL@");
              LFAX.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
              LFAX.setName("LFAX");
              panel2.add(LFAX);
              LFAX.setBounds(85, 37, 130, LFAX.getPreferredSize().height);
              
              // ---- riBoutonDetailMail ----
              riBoutonDetailMail.setToolTipText("Choix des coordonn\u00e9es d'envoi");
              riBoutonDetailMail.setName("riBoutonDetailMail");
              riBoutonDetailMail.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailMailActionPerformed(e);
                }
              });
              panel2.add(riBoutonDetailMail);
              riBoutonDetailMail.setBounds(291, 35, 31, 28);
              
              // ---- label22 ----
              label22.setText("Contact");
              label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
              label22.setName("label22");
              panel2.add(label22);
              label22.setBounds(15, 5, 60, 24);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel2);
            panel2.setBounds(375, 135, 335, 80);
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 565, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Choix possibles");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      
      // ---- OBJ_8 ----
      OBJ_8.setText("M\u00e9morisation position curseur");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      BTD.add(OBJ_8);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    
    // ---- E1PDS ----
    E1PDS.setComponentPopupMenu(BTD);
    E1PDS.setName("E1PDS");
    
    // ---- OBJ_66 ----
    OBJ_66.setText("Poids total");
    OBJ_66.setName("OBJ_66");
    
    // ---- OBJ_110 ----
    OBJ_110.setText("Nombre de colis");
    OBJ_110.setName("OBJ_110");
    
    // ---- E1COL ----
    E1COL.setComponentPopupMenu(BTD);
    E1COL.setName("E1COL");
    
    // ---- bt_encours ----
    bt_encours.setToolTipText("En cours");
    bt_encours.setName("bt_encours");
    bt_encours.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riBoutonDetail2ActionPerformed(e);
      }
    });
    
    // ---- riBoutonDetail1 ----
    riBoutonDetail1.setToolTipText("Bloc_notes");
    riBoutonDetail1.setName("riBoutonDetail1");
    riBoutonDetail1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riBoutonDetail1ActionPerformed(e);
      }
    });
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Choix du chantier");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_28;
  private RiZoneSortie E1ETB;
  private JLabel OBJ_29;
  private RiZoneSortie E1NFA;
  private RiZoneSortie WNUM;
  private RiZoneSortie WSUF;
  private JLabel OBJ_31;
  private XRiTextField E1VDE;
  private RiZoneSortie WDATEX;
  private JLabel label2;
  private JLabel OBJ_50;
  private JPanel p_tete_droite;
  private JLabel bt_ecran;
  private RiZoneSortie OBJ_129;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt2;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie OBJ_46;
  private XRiTextField E3NOM;
  private XRiTextField E3CPL;
  private XRiTextField E3RUE;
  private XRiTextField E3LOC;
  private XRiTextField WOBS;
  private XRiTextField E3PAYN;
  private XRiTextField E3VILN;
  private XRiTextField E3TEL;
  private JLabel OBJ_121;
  private XRiTextField E3CDPX;
  private JLabel OBJ_119;
  private XRiTextField E3COP;
  private RiZoneSortie E1CAN;
  private XRiTextField E1TP1;
  private XRiTextField E1TP2;
  private XRiTextField E1TP3;
  private XRiTextField E1TP4;
  private XRiTextField E1TP5;
  private XRiTextField E1CTR;
  private JLabel OBJ_114;
  private JLabel OBJ_115;
  private JLabel OBJ_116;
  private JLabel OBJ_117;
  private JLabel OBJ_118;
  private JPanel panel1;
  private JLabel OBJ_43;
  private RiZoneSortie E1HRE1;
  private JLabel OBJ_44;
  private RiZoneSortie E1HRE2;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie WEPOS;
  private RiZoneSortie WEPLF;
  private RiZoneSortie WEDEP;
  private JLabel OBJ_60;
  private SNLabelChamp lb_depassement;
  private JLabel OBJ_62;
  private RiZoneSortie OBJ_58;
  private JLabel label_devise;
  private RiZoneSortie z_devise;
  private JButton button1;
  private XRiTextField E1RCC;
  private XRiTextField E1NCC;
  private JPanel panelChantier;
  private JLabel label14;
  private XRiTextField WCHANT;
  private SNBoutonDetail riBoutonDetailListe1;
  private JLabel OBJ_30;
  private XRiTextField E1MAG;
  private RiZoneSortie OBJ_32;
  private JLabel OBJ_122;
  private XRiTextField E1MEX;
  private RiZoneSortie EXLIBR;
  private JLabel label19;
  private RiZoneSortie OBJ_61;
  private XRiCheckBox E1GBA;
  private JLabel label_gba;
  private SNBoutonLeger GBAX;
  private JLabel labelCompGba;
  private JXTitledSeparator xTitledSeparator2;
  private RiZoneSortie zs_LAVOIR;
  private JPanel panel2;
  private RiZoneSortie LMAIL;
  private JLabel label20;
  private RiZoneSortie LFAX;
  private SNBoutonDetail riBoutonDetailMail;
  private JLabel label22;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_15;
  private XRiTextField E1PDS;
  private JLabel OBJ_66;
  private JLabel OBJ_110;
  private XRiTextField E1COL;
  private SNBoutonDetail bt_encours;
  private SNBoutonDetail riBoutonDetail1;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
