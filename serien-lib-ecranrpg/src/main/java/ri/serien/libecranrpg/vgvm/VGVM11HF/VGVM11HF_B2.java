
package ri.serien.libecranrpg.vgvm.VGVM11HF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.table.DefaultTableModel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11HF_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] tableHeader = { "  ", "Date", "Utilisateur", "Type", "Option", "Imprimante", "Réédit°" };
  private String[] _table1_Title = { "A", "B", "C", "D", "E", "F", "G", };
  private String[][] _table1_Data = { { null, null, null, null, null, null, null, }, { null, null, null, null, null, null, null, },
      { null, null, null, null, null, null, null, }, { null, null, null, null, null, null, null, },
      { null, null, null, null, null, null, null, }, { null, null, null, null, null, null, null, },
      { null, null, null, null, null, null, null, }, { null, null, null, null, null, null, null, },
      { null, null, null, null, null, null, null, }, { null, null, null, null, null, null, null, },
      { null, null, null, null, null, null, null, }, };
  private int[] _table1_Width = { 75, 75, 75, 75, 75, 75, 75, };
  // private String[] _LIST_Top=null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.LEFT,
      SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.LEFT };
  
  public VGVM11HF_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    table1.setAspectTable(null, _table1_Title, _table1_Data, _table1_Width, false, _LIST_Justification, null, null, null);
    
    setTitle("Suite de l'historique");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NCC@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RCC@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    E1VDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1VDE@")).trim());
    E1MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1VDE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(table1, table1.get_LIST_Title_Data_Brut(), _LIST_Top, _LIST_Justification);
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    E1SUF.setVisible(lexique.isPresent("E1SUF"));
    E1MAG.setVisible(lexique.isPresent("E1MAG"));
    E1VDE.setVisible(lexique.isPresent("E1VDE"));
    E1ETB.setVisible(lexique.isPresent("E1ETB"));
    E1NUM.setVisible(lexique.isPresent("E1NUM"));
    OBJ_32.setVisible(lexique.isPresent("E1NCC"));
    OBJ_33.setVisible(lexique.isPresent("E1RCC"));
    OBJ_21.setVisible(lexique.isPresent("WNOM"));
    
    // Création de la liste
    table1.setModel(new DefaultTableModel(new Object[][] { { null, null, null, null, null, null, null },
        { "Commande", lexique.HostFieldGetData("DAT1"), lexique.HostFieldGetData("USR1"), lexique.HostFieldGetData("TYP1"),
            lexique.HostFieldGetData("OPT1"), lexique.HostFieldGetData("DEV1"), lexique.HostFieldGetData("RED1") },
        { null, null, null, null, null, null, null },
        { "Préparation", lexique.HostFieldGetData("DAT2"), lexique.HostFieldGetData("USR2"), lexique.HostFieldGetData("TYP2"),
            lexique.HostFieldGetData("OPT2"), lexique.HostFieldGetData("DEV2"), lexique.HostFieldGetData("RED2") },
        { null, null, null, null, null, null, null },
        { "Expédition", lexique.HostFieldGetData("DAT3"), lexique.HostFieldGetData("USR3"), lexique.HostFieldGetData("TYP3"),
            lexique.HostFieldGetData("OPT3"), lexique.HostFieldGetData("DEV3"), lexique.HostFieldGetData("RED3") },
        { null, null, null, null, null, null, null },
        { "Facture", lexique.HostFieldGetData("DAT4"), lexique.HostFieldGetData("USR4"), lexique.HostFieldGetData("TYP4"),
            lexique.HostFieldGetData("OPT4"), lexique.HostFieldGetData("DEV4"), lexique.HostFieldGetData("RED4") },
        { null, null, null, null, null, null, null },
        { "Proforma", lexique.HostFieldGetData("DAT5"), lexique.HostFieldGetData("USR5"), lexique.HostFieldGetData("TYP5"),
            lexique.HostFieldGetData("OPT5"), lexique.HostFieldGetData("DEV5"), lexique.HostFieldGetData("RED5") },
        { null, null, null, null, null, null, null } }, tableHeader) {
      
      boolean[] columnEditable = new boolean[] { false, false, false, false, false, false, false };
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnEditable[columnIndex];
      }
    });
    table1.getColumnModel().getColumn(0).setPreferredWidth(110);
    table1.getColumnModel().getColumn(1).setPreferredWidth(80);
    table1.getColumnModel().getColumn(2).setPreferredWidth(90);
    table1.getColumnModel().getColumn(3).setPreferredWidth(50);
    table1.getColumnModel().getColumn(4).setPreferredWidth(50);
    table1.getColumnModel().getColumn(5).setPreferredWidth(90);
    table1.getColumnModel().getColumn(6).setPreferredWidth(60);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    OBJ_32 = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    scrollPane1 = new JScrollPane();
    table1 = new XRiTable();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    E1ETB = new RiZoneSortie();
    E1NUM = new RiZoneSortie();
    E1SUF = new RiZoneSortie();
    OBJ_21 = new RiZoneSortie();
    OBJ_22 = new JLabel();
    E1VDE = new RiZoneSortie();
    OBJ_24 = new JLabel();
    E1MAG = new RiZoneSortie();
    OBJ_44_OBJ_44 = new JLabel();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 330));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- OBJ_32 ----
        OBJ_32.setText("@E1NCC@");
        OBJ_32.setName("OBJ_32");

        //---- OBJ_33 ----
        OBJ_33.setText("@E1RCC@");
        OBJ_33.setName("OBJ_33");

        //======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");

          //---- table1 ----
          table1.setRowSelectionAllowed(false);
          table1.setName("table1");
          scrollPane1.setViewportView(table1);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 229, GroupLayout.PREFERRED_SIZE))
                .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 600, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(30, 30, 30)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(OBJ_32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGap(11, 11, 11)
              .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 207, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 32));
      barre_tete.setName("barre_tete");

      //======== panel2 ========
      {
        panel2.setBorder(null);
        panel2.setPreferredSize(new Dimension(790, 35));
        panel2.setOpaque(false);
        panel2.setMaximumSize(new Dimension(790, 28));
        panel2.setMinimumSize(new Dimension(790, 28));
        panel2.setName("panel2");

        //---- E1ETB ----
        E1ETB.setOpaque(false);
        E1ETB.setText("@E1ETB@");
        E1ETB.setName("E1ETB");

        //---- E1NUM ----
        E1NUM.setOpaque(false);
        E1NUM.setText("@E1NUM@");
        E1NUM.setName("E1NUM");

        //---- E1SUF ----
        E1SUF.setOpaque(false);
        E1SUF.setText("@E1SUF@");
        E1SUF.setName("E1SUF");

        //---- OBJ_21 ----
        OBJ_21.setText("@WNOM@");
        OBJ_21.setOpaque(false);
        OBJ_21.setName("OBJ_21");

        //---- OBJ_22 ----
        OBJ_22.setText("Vendeur");
        OBJ_22.setName("OBJ_22");

        //---- E1VDE ----
        E1VDE.setOpaque(false);
        E1VDE.setText("@E1VDE@");
        E1VDE.setName("E1VDE");

        //---- OBJ_24 ----
        OBJ_24.setText("Magasin");
        OBJ_24.setName("OBJ_24");

        //---- E1MAG ----
        E1MAG.setOpaque(false);
        E1MAG.setText("@E1VDE@");
        E1MAG.setName("E1MAG");

        //---- OBJ_44_OBJ_44 ----
        OBJ_44_OBJ_44.setText("Etablissement");
        OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");

        //---- label1 ----
        label1.setText("Num\u00e9ro");
        label1.setName("label1");

        GroupLayout panel2Layout = new GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
              .addGap(2, 2, 2)
              .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(15, 15, 15)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(E1NUM, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
              .addGap(6, 6, 6)
              .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(20, 20, 20)
              .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(E1MAG, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
        );
        panel2Layout.setVerticalGroup(
          panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(OBJ_44_OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(4, 4, 4)
              .addComponent(label1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(E1NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(E1SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(OBJ_21, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
            .addGroup(panel2Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(E1MAG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
        );
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_33;
  private JScrollPane scrollPane1;
  private XRiTable table1;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private RiZoneSortie E1ETB;
  private RiZoneSortie E1NUM;
  private RiZoneSortie E1SUF;
  private RiZoneSortie OBJ_21;
  private JLabel OBJ_22;
  private RiZoneSortie E1VDE;
  private JLabel OBJ_24;
  private RiZoneSortie E1MAG;
  private JLabel OBJ_44_OBJ_44;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
