
package ri.serien.libecranrpg.vgvm.VGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM14FM_C1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM14FM_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TOP2.setValeursSelection("X", " ");
    TOP1.setValeursSelection("X", " ");
    TOP3.setValeursSelection("X", " ");
    TOP4.setValeursSelection("X", " ");
    TGT1.setValeursSelection("X", " ");
    RGR1.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    RGR0.setVisible(lexique.isPresent("RGR0"));
    PAS.setEnabled(lexique.isPresent("PAS"));
    NLIF.setEnabled(lexique.isPresent("NLIF"));
    NLID.setEnabled(lexique.isPresent("NLID"));
    NLIA.setEnabled(lexique.isPresent("NLIA"));
    TOP2.setEnabled(!lexique.HostFieldGetData("TOP1").trim().equalsIgnoreCase("X"));
    // TOP2.setSelected(lexique.HostFieldGetData("TOP2").equalsIgnoreCase("X"));
    TOP1.setEnabled(!lexique.HostFieldGetData("TOP2").trim().equalsIgnoreCase("X"));
    // TOP1.setSelected(lexique.HostFieldGetData("TOP1").equalsIgnoreCase("X"));
    // TOP3.setVisible( lexique.isPresent("TOP3"));
    // TOP3.setSelected(lexique.HostFieldGetData("TOP3").equalsIgnoreCase("X"));
    // TOP4.setVisible( lexique.isPresent("TOP4"));
    // TOP4.setSelected(lexique.HostFieldGetData("TOP4").equalsIgnoreCase("X"));
    // TGT1.setVisible( lexique.isPresent("TGT1"));
    // TGT1.setSelected(lexique.HostFieldGetData("TGT1").equalsIgnoreCase("X"));
    // RGR1.setVisible( lexique.isPresent("RGR1"));
    // RGR1.setSelected(lexique.HostFieldGetData("RGR1").equalsIgnoreCase("X"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TOP2.isSelected())
    // lexique.HostFieldPutData("TOP2", 0, "X");
    // else
    // lexique.HostFieldPutData("TOP2", 0, " ");
    // if (TOP1.isSelected())
    // lexique.HostFieldPutData("TOP1", 0, "X");
    // else
    // lexique.HostFieldPutData("TOP1", 0, " ");
    // if (TOP3.isSelected())
    // lexique.HostFieldPutData("TOP3", 0, "X");
    // else
    // lexique.HostFieldPutData("TOP3", 0, " ");
    // if (TOP4.isSelected())
    // lexique.HostFieldPutData("TOP4", 0, "X");
    // else
    // lexique.HostFieldPutData("TOP4", 0, " ");
    // if (TGT1.isSelected())
    // lexique.HostFieldPutData("TGT1", 0, "X");
    // else
    // lexique.HostFieldPutData("TGT1", 0, " ");
    // if (RGR1.isSelected())
    // lexique.HostFieldPutData("RGR1", 0, "X");
    // else
    // lexique.HostFieldPutData("RGR1", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm14"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_11 = new JLabel();
    RGR1 = new XRiCheckBox();
    TGT1 = new XRiCheckBox();
    TOP4 = new XRiCheckBox();
    TOP3 = new XRiCheckBox();
    OBJ_15 = new JLabel();
    TOP1 = new XRiCheckBox();
    OBJ_24 = new JLabel();
    OBJ_20 = new JLabel();
    TOP2 = new XRiCheckBox();
    NLIA = new XRiTextField();
    NLID = new XRiTextField();
    NLIF = new XRiTextField();
    PAS = new XRiTextField();
    RGR0 = new XRiTextField();
    OBJ_13 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 220));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_11 ----
          OBJ_11.setText("Traitement groupe de lignes");
          OBJ_11.setName("OBJ_11");
          panel2.add(OBJ_11);
          OBJ_11.setBounds(20, 24, 211, 20);

          //---- RGR1 ----
          RGR1.setText("Nouveau regroupement");
          RGR1.setComponentPopupMenu(BTD);
          RGR1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RGR1.setName("RGR1");
          panel2.add(RGR1);
          RGR1.setBounds(20, 144, 190, 20);

          //---- TGT1 ----
          TGT1.setText("Nouveau type de gratuit");
          TGT1.setComponentPopupMenu(BTD);
          TGT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TGT1.setName("TGT1");
          panel2.add(TGT1);
          TGT1.setBounds(245, 144, 166, 20);

          //---- TOP4 ----
          TOP4.setText("Renum\u00e9rotation");
          TOP4.setComponentPopupMenu(BTD);
          TOP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOP4.setName("TOP4");
          panel2.add(TOP4);
          TOP4.setBounds(20, 84, 135, 20);

          //---- TOP3 ----
          TOP3.setText("Suppression");
          TOP3.setComponentPopupMenu(BTD);
          TOP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOP3.setName("TOP3");
          panel2.add(TOP3);
          TOP3.setBounds(20, 114, 135, 20);

          //---- OBJ_15 ----
          OBJ_15.setText("Regroupement");
          OBJ_15.setName("OBJ_15");
          panel2.add(OBJ_15);
          OBJ_15.setBounds(400, 24, 100, 20);

          //---- TOP1 ----
          TOP1.setText("D\u00e9placement");
          TOP1.setComponentPopupMenu(BTD);
          TOP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOP1.setName("TOP1");
          panel2.add(TOP1);
          TOP1.setBounds(20, 54, 135, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("avec pas de");
          OBJ_24.setName("OBJ_24");
          panel2.add(OBJ_24);
          OBJ_24.setBounds(160, 84, 78, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("Apr\u00e9s ligne");
          OBJ_20.setName("OBJ_20");
          panel2.add(OBJ_20);
          OBJ_20.setBounds(400, 54, 72, 20);

          //---- TOP2 ----
          TOP2.setText("Copie");
          TOP2.setComponentPopupMenu(BTD);
          TOP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TOP2.setName("TOP2");
          panel2.add(TOP2);
          TOP2.setBounds(245, 54, 90, 20);

          //---- NLIA ----
          NLIA.setName("NLIA");
          panel2.add(NLIA);
          NLIA.setBounds(500, 50, 42, NLIA.getPreferredSize().height);

          //---- NLID ----
          NLID.setComponentPopupMenu(BTD);
          NLID.setName("NLID");
          panel2.add(NLID);
          NLID.setBounds(245, 20, 44, NLID.getPreferredSize().height);

          //---- NLIF ----
          NLIF.setComponentPopupMenu(BTD);
          NLIF.setName("NLIF");
          panel2.add(NLIF);
          NLIF.setBounds(320, 20, 44, NLIF.getPreferredSize().height);

          //---- PAS ----
          PAS.setComponentPopupMenu(BTD);
          PAS.setName("PAS");
          panel2.add(PAS);
          PAS.setBounds(245, 80, 28, PAS.getPreferredSize().height);

          //---- RGR0 ----
          RGR0.setComponentPopupMenu(BTD);
          RGR0.setName("RGR0");
          panel2.add(RGR0);
          RGR0.setBounds(500, 20, 20, RGR0.getPreferredSize().height);

          //---- OBJ_13 ----
          OBJ_13.setText("\u00e0");
          OBJ_13.setName("OBJ_13");
          panel2.add(OBJ_13);
          OBJ_13.setBounds(300, 24, 12, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_11;
  private XRiCheckBox RGR1;
  private XRiCheckBox TGT1;
  private XRiCheckBox TOP4;
  private XRiCheckBox TOP3;
  private JLabel OBJ_15;
  private XRiCheckBox TOP1;
  private JLabel OBJ_24;
  private JLabel OBJ_20;
  private XRiCheckBox TOP2;
  private XRiTextField NLIA;
  private XRiTextField NLID;
  private XRiTextField NLIF;
  private XRiTextField PAS;
  private XRiTextField RGR0;
  private JLabel OBJ_13;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
