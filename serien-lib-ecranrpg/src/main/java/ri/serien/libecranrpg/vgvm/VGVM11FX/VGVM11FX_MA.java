
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_MA extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private ImageIcon margePlus = null;
  private ImageIcon margeMoins = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_MA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    initDiverses();
    
    setCloseKey("F4");
    
    // Titre
    setTitle("Marges et actions commerciales globales");
    
    
    margePlus = lexique.chargerImage("images/marge_moins.png", true);
    margeMoins = lexique.chargerImage("images/marge_plus.png", true);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WTHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTHT@")).trim());
    WMMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMR@")).trim());
    W1TPRL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1TPRL@")).trim());
    E1TPVL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TPVL@")).trim());
    WMMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMT@")).trim());
    WMAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAC@")).trim());
    WPMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMR@")).trim());
    WPMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMT@")).trim());
    WPAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAC@")).trim());
    CCMMI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCMMI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    if (lexique.HostFieldGetData("WMMR").contains("-")) {
      marge.setIcon(margePlus);
    }
    else {
      marge.setIcon(margeMoins);
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_36 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_39 = new JLabel();
    WTHT = new RiZoneSortie();
    WMMR = new RiZoneSortie();
    W1TPRL = new RiZoneSortie();
    E1TPVL = new RiZoneSortie();
    WMMT = new RiZoneSortie();
    OBJ_33 = new JLabel();
    WMAC = new RiZoneSortie();
    WPMR = new RiZoneSortie();
    WPMT = new RiZoneSortie();
    WPAC = new RiZoneSortie();
    CCMMI = new RiZoneSortie();
    marge = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1010, 220));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(1010, 260));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_36 ----
          OBJ_36.setText("% Chiffre d'affaires th\u00e9orique");
          OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
          OBJ_36.setName("OBJ_36");
          p_recup.add(OBJ_36);
          OBJ_36.setBounds(285, 85, 175, 24);

          //---- OBJ_47 ----
          OBJ_47.setText("% Chiffre d'affaires r\u00e9el");
          OBJ_47.setFont(OBJ_47.getFont().deriveFont(OBJ_47.getFont().getStyle() | Font.BOLD));
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(285, 115, 175, 24);

          //---- OBJ_35 ----
          OBJ_35.setText("Action commerciale");
          OBJ_35.setFont(OBJ_35.getFont().deriveFont(OBJ_35.getFont().getStyle() | Font.BOLD));
          OBJ_35.setName("OBJ_35");
          p_recup.add(OBJ_35);
          OBJ_35.setBounds(25, 85, 132, 24);

          //---- OBJ_41 ----
          OBJ_41.setText("% Chiffre d'affaires r\u00e9el");
          OBJ_41.setFont(OBJ_41.getFont().deriveFont(OBJ_41.getFont().getStyle() | Font.BOLD));
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(285, 25, 175, 24);

          //---- OBJ_42 ----
          OBJ_42.setText("% Chiffre d'affaires r\u00e9el");
          OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
          OBJ_42.setName("OBJ_42");
          p_recup.add(OBJ_42);
          OBJ_42.setBounds(285, 55, 175, 24);

          //---- OBJ_38 ----
          OBJ_38.setText("Chiffre d'affaires th\u00e9orique");
          OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD));
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(545, 85, 155, 24);

          //---- OBJ_37 ----
          OBJ_37.setText("Prix de revient");
          OBJ_37.setFont(OBJ_37.getFont().deriveFont(OBJ_37.getFont().getStyle() | Font.BOLD));
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(545, 25, 115, 24);

          //---- OBJ_34 ----
          OBJ_34.setText("Marge th\u00e9orique");
          OBJ_34.setFont(OBJ_34.getFont().deriveFont(OBJ_34.getFont().getStyle() | Font.BOLD));
          OBJ_34.setName("OBJ_34");
          p_recup.add(OBJ_34);
          OBJ_34.setBounds(25, 55, 132, 24);

          //---- OBJ_39 ----
          OBJ_39.setText("Chiffre d'affaires r\u00e9el");
          OBJ_39.setFont(OBJ_39.getFont().deriveFont(OBJ_39.getFont().getStyle() | Font.BOLD));
          OBJ_39.setName("OBJ_39");
          p_recup.add(OBJ_39);
          OBJ_39.setBounds(545, 55, 120, 24);

          //---- WTHT ----
          WTHT.setText("@WTHT@");
          WTHT.setHorizontalAlignment(SwingConstants.RIGHT);
          WTHT.setName("WTHT");
          p_recup.add(WTHT);
          WTHT.setBounds(700, 55, 98, WTHT.getPreferredSize().height);

          //---- WMMR ----
          WMMR.setText("@WMMR@");
          WMMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMR.setName("WMMR");
          p_recup.add(WMMR);
          WMMR.setBounds(160, 25, 98, WMMR.getPreferredSize().height);

          //---- W1TPRL ----
          W1TPRL.setText("@W1TPRL@");
          W1TPRL.setHorizontalAlignment(SwingConstants.RIGHT);
          W1TPRL.setName("W1TPRL");
          p_recup.add(W1TPRL);
          W1TPRL.setBounds(700, 25, 98, W1TPRL.getPreferredSize().height);

          //---- E1TPVL ----
          E1TPVL.setText("@E1TPVL@");
          E1TPVL.setHorizontalAlignment(SwingConstants.RIGHT);
          E1TPVL.setName("E1TPVL");
          p_recup.add(E1TPVL);
          E1TPVL.setBounds(700, 85, 98, E1TPVL.getPreferredSize().height);

          //---- WMMT ----
          WMMT.setText("@WMMT@");
          WMMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMT.setName("WMMT");
          p_recup.add(WMMT);
          WMMT.setBounds(160, 55, 98, WMMT.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("Marge r\u00e9elle");
          OBJ_33.setFont(OBJ_33.getFont().deriveFont(OBJ_33.getFont().getStyle() | Font.BOLD));
          OBJ_33.setName("OBJ_33");
          p_recup.add(OBJ_33);
          OBJ_33.setBounds(25, 25, 100, 24);

          //---- WMAC ----
          WMAC.setText("@WMAC@");
          WMAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WMAC.setName("WMAC");
          p_recup.add(WMAC);
          WMAC.setBounds(160, 85, 98, WMAC.getPreferredSize().height);

          //---- WPMR ----
          WPMR.setText("@WPMR@");
          WPMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMR.setName("WPMR");
          p_recup.add(WPMR);
          WPMR.setBounds(460, 25, 66, WPMR.getPreferredSize().height);

          //---- WPMT ----
          WPMT.setText("@WPMT@");
          WPMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMT.setName("WPMT");
          p_recup.add(WPMT);
          WPMT.setBounds(460, 55, 66, WPMT.getPreferredSize().height);

          //---- WPAC ----
          WPAC.setText("@WPAC@");
          WPAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPAC.setName("WPAC");
          p_recup.add(WPAC);
          WPAC.setBounds(460, 85, 66, WPAC.getPreferredSize().height);

          //---- CCMMI ----
          CCMMI.setText("@CCMMI@");
          CCMMI.setHorizontalAlignment(SwingConstants.RIGHT);
          CCMMI.setName("CCMMI");
          p_recup.add(CCMMI);
          CCMMI.setBounds(460, 115, 66, CCMMI.getPreferredSize().height);

          //---- marge ----
          marge.setName("marge");
          p_recup.add(marge);
          marge.setBounds(130, 22, 30, 30);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 813, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(27, 27, 27)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(28, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_36;
  private JLabel OBJ_47;
  private JLabel OBJ_35;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JLabel OBJ_38;
  private JLabel OBJ_37;
  private JLabel OBJ_34;
  private JLabel OBJ_39;
  private RiZoneSortie WTHT;
  private RiZoneSortie WMMR;
  private RiZoneSortie W1TPRL;
  private RiZoneSortie E1TPVL;
  private RiZoneSortie WMMT;
  private JLabel OBJ_33;
  private RiZoneSortie WMAC;
  private RiZoneSortie WPMR;
  private RiZoneSortie WPMT;
  private RiZoneSortie WPAC;
  private RiZoneSortie CCMMI;
  private JLabel marge;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
