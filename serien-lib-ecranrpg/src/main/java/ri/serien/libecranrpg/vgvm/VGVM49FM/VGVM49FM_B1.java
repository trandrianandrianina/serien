
package ri.serien.libecranrpg.vgvm.VGVM49FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;

/**
 * @author Stéphane Vénéri
 */
public class VGVM49FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM49FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    WCODG.setValeursSelection("H", " ");
    
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // Etablissement client
    IdEtablissement idEtablissementClient = IdEtablissement.getInstance(lexique.HostFieldGetData("CLETB"));
    
    // Client
    snClient.setSession(getSession());
    snClient.setIdEtablissement(idEtablissementClient);
    snClient.charger(true);
    snClient.setSelectionParChampRPG(lexique, "CLCLI", "CLLIV");
    snClient.setEnabled(false);
    
    snFournisseur.setEnabled(false);
    snMagasin.setEnabled(false);
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setEnabled(true);
    snEtablissement.setSelectionParChampRPG(lexique, "WETBA");
    
    // fournisseur et magasin
    if (snEtablissement.getSelection() != null) {
      snFournisseur.setSession(getSession());
      snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
      snFournisseur.charger(false);
      snFournisseur.setEnabled(true);
      snFournisseur.setSelectionParChampRPG(lexique, "WCOFA", "WNUFA");
      
      snMagasin.setSession(getSession());
      snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      snMagasin.charger(false);
      snMagasin.setEnabled(true);
      snMagasin.setSelectionParChampRPG(lexique, "WMAGA");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Lien client/fournisseur"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETBA");
    snFournisseur.renseignerChampRPG(lexique, "WCOFA", "WNUFA");
    snMagasin.renseignerChampRPG(lexique, "WMAGA");
  }
  
  protected void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      // fournisseur et magasin
      snFournisseur.setSelection(null);
      snMagasin.setSelection(null);
      snFournisseur.setEnabled(false);
      snMagasin.setEnabled(false);
      
      if (snEtablissement.getSelection() != null) {
        snFournisseur.setSession(getSession());
        snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
        snFournisseur.charger(true);
        snFournisseur.setEnabled(true);
        
        snMagasin.setSession(getSession());
        snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
        snMagasin.charger(true);
        snMagasin.setEnabled(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    sNLabelChamp1 = new SNLabelChamp();
    snClient = new SNClient();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    WCODG = new XRiCheckBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(800, 260));
    setPreferredSize(new Dimension(800, 260));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ---- sNLabelChamp1 ----
      sNLabelChamp1.setText("Client");
      sNLabelChamp1.setName("sNLabelChamp1");
      pnlContenu.add(sNLabelChamp1,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snClient ----
      snClient.setName("snClient");
      pnlContenu.add(snClient,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbEtablissement ----
      lbEtablissement.setText("Etablissement du fournisseur");
      lbEtablissement.setMaximumSize(new Dimension(200, 30));
      lbEtablissement.setMinimumSize(new Dimension(200, 30));
      lbEtablissement.setPreferredSize(new Dimension(200, 30));
      lbEtablissement.setName("lbEtablissement");
      pnlContenu.add(lbEtablissement,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snEtablissement ----
      snEtablissement.setName("snEtablissement");
      snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
        @Override
        public void valueChanged(SNComposantEvent e) {
          snEtablissementValueChanged(e);
        }
      });
      pnlContenu.add(snEtablissement,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbFournisseur ----
      lbFournisseur.setText("Fournisseur");
      lbFournisseur.setName("lbFournisseur");
      pnlContenu.add(lbFournisseur,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snFournisseur ----
      snFournisseur.setName("snFournisseur");
      pnlContenu.add(snFournisseur,
          new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbMagasin ----
      lbMagasin.setText("Magasin");
      lbMagasin.setName("lbMagasin");
      pnlContenu.add(lbMagasin,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snMagasin ----
      snMagasin.setName("snMagasin");
      pnlContenu.add(snMagasin,
          new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- WCODG ----
      WCODG.setText("G\u00e9n\u00e9ration des commandes d'achats lors de la validation de la commande de ventes");
      WCODG.setFont(new Font("sansserif", Font.PLAIN, 14));
      WCODG.setPreferredSize(new Dimension(530, 30));
      WCODG.setMinimumSize(new Dimension(530, 30));
      WCODG.setMaximumSize(new Dimension(530, 30));
      WCODG.setName("WCODG");
      pnlContenu.add(WCODG,
          new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp sNLabelChamp1;
  private SNClient snClient;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private XRiCheckBox WCODG;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
