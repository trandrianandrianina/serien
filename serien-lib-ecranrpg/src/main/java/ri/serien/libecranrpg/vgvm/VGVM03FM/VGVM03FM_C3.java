
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerListModel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_C3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] CLPRL_Value = { "", "J", "S", "D", "Q", "M" };
  private String[] CLIN16_Value = { "", "1", "2" };
  private String[] CLPFA_Value = { "", "J", "S", "D", "Q", "M" };
  private String[] CLTTC_Value = { "", "1", "2" };
  private String[] CLIN15_Value = { "", "1", "2" };
  private String[] CLRBF_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8" };
  private String[] CLIN4_Value = { "0", "1", "2", "3", "4" };
  private boolean top[] = { false, false, false, false };
  private String[] CLIN19_Value = { "", "1", "2" };
  private String[] CLIN26_Value = { "", "1" };
  private String[] CLIN3_Value = { "", "1", "2" };
  
  /**
   * Constructeur.
   */
  public VGVM03FM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CLPRL.setValeurs(CLPRL_Value, null);
    CLPFA.setValeurs(CLPFA_Value, null);
    CLRBF.setValeurs(CLRBF_Value, null);
    CLIN4.setValeurs(CLIN4_Value, null);
    CLIN16.setValeurs(CLIN16_Value, null);
    CLIN15.setValeurs(CLIN15_Value, null);
    CLTTC.setValeurs(CLTTC_Value, null);
    CLIN12.setValeursSelection("1", "");
    CLIN28.setValeursSelection("1", "");
    CLIN27.setValeursSelection("1", "");
    CLIN26.setValeurs(CLIN26_Value, null);
    CLIN8.setValeursSelection("1", "");
    CLIN11.setValeursSelection("1", "");
    CLIN3.setValeurs(CLIN3_Value, null);
    CLIN10.setValeursSelection("1", "");
    CLIN17.setValeursSelection("X", "");
    CLIN13.setValeursSelection("1", "");
    CLIN19.setValeurs(CLIN19_Value, null);
    
    WTEDI.setValeursSelection("1", "");
    CLIN20.setValeursSelection("A", " ");
    EBIN01.setValeursSelection("1", "");
    EBIN02.setValeursSelection("1", "");
    EBIN03.setValeursSelection("1", "");
    EBIN05.setValeursSelection("1", "");
    
    EBIN09.setValeurs("", EBIN09_GRP);
    EBIN09_1.setValeurs("1");
    EBIN09_2.setValeurs("2");
    
    setCloseKey("F7");
    
    // Titre
    setTitle("Facturation");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LBCLIF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBCLIF@")).trim());
    LBVILF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBVILF@")).trim());
    DVLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    TFLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TFLIBR@")).trim());
    LBIN22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBIN22@")).trim());
    LIBC1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBC1@")).trim());
    LIBC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBC2@")).trim());
    LIBC3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBC3@")).trim());
    LIBFI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBFI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean in53 = lexique.isTrue("53");
    boolean isPS74 = true;
    boolean isConsultation = (lexique.getMode() == Lexical.MODE_CONSULTATION);
    
    navig_valid.setVisible(!in53);
    
    // On va sur l'onglet en cours
    Integer indice = (Integer) lexique.getValeurVariableGlobale("ongletEnCours_VGVM03FM_C3");
    if (indice != null) {
      OBJ_26.setSelectedIndex(indice.intValue());
    }
    
    // if (in53) CLFIR.setVisible(true);
    CLCC1.forceVisibility();
    CLCC2.forceVisibility();
    CLCC3.forceVisibility();
    CLFIR.forceVisibility();
    
    // CLTFA.setEnabled( lexique.isPresent("CLTFA"));
    
    // Initialisation de la table top
    // Constitue 4 tops SGM avec 1 zone SDA
    for (int i = 0; i < 4; i++) {
      top[i] = false;
      if (lexique.HostFieldGetData("CLNLA").charAt(i) == '*') {
        top[i] = true;
      }
    }
    
    CLNLA_1_CHK.setSelected(top[0]);
    CLNLA_2_CHK.setSelected(top[1]);
    CLNLA_3_CHK.setSelected(top[2]);
    CLNLA_4_CHK.setSelected(top[3]);
    
    OBJ_30.setVisible(CLTTC.isVisible());
    OBJ_37.setVisible(CLCLFP.isVisible());
    OBJ_42.setVisible(CLDEV.isVisible());
    OBJ_45.setVisible(CLCNC.isVisible());
    OBJ_47.setVisible(CLADH.isVisible());
    
    if (!isPS74) {
      CLIN26.removeAllItems();
      CLIN26.addItem("Edition des remises eventuelles");
      CLIN26.addItem("Non-edition des remises");
      CLIN26.setSelectedIndex(Integer.parseInt(lexique.HostFieldGetData("CLIN26")));
    }
    
    // Si l'article est 'géré' ou 'non visible', il ne peut passer à 'non géré' (choix grisé)
    if (!lexique.HostFieldGetData("EBIN09").trim().equalsIgnoreCase("")) {
      EBIN09.setEnabled(false);
    }
    EBIN09_1.setEnabled(!isConsultation);
    EBIN09_2.setEnabled(!isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // On stocke l'onlet en cours
    lexique.addVariableGlobale("ongletEnCours_VGVM03FM_C3", OBJ_26.getSelectedIndex());
    
    // constitue 1 zone SDA avec 4 tops SGM
    StringBuffer topReconstitue = new StringBuffer("    ");
    
    if (CLNLA_1_CHK.isSelected()) {
      topReconstitue.setCharAt(0, '*');
    }
    if (CLNLA_2_CHK.isSelected()) {
      topReconstitue.setCharAt(1, '*');
    }
    if (CLNLA_3_CHK.isSelected()) {
      topReconstitue.setCharAt(2, '*');
    }
    if (CLNLA_4_CHK.isSelected()) {
      topReconstitue.setCharAt(3, '*');
    }
    
    lexique.HostFieldPutData("CLNLA", 0, topReconstitue.toString());
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void OBJ_61ActionPerformed(ActionEvent e) {
    if (lexique.isTrue("52")) {
      lexique.HostCursorPut(14, 35);
      lexique.HostScreenSendKey(this, "F4");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "&");
    }
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_26 = new JTabbedPane();
    OBJ_27 = new JPanel();
    OBJ_28 = new JPanel();
    LBCLIF = new RiZoneSortie();
    LBVILF = new RiZoneSortie();
    DVLIBR = new RiZoneSortie();
    TFLIBR = new RiZoneSortie();
    CLIN13 = new XRiCheckBox();
    CLADH = new XRiTextField();
    LBIN22 = new RiZoneSortie();
    OBJ_29 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_49 = new JLabel();
    CLCLFP = new XRiTextField();
    CLCNC = new XRiTextField();
    CLDEV = new XRiTextField();
    CLCLFS = new XRiTextField();
    CLTFA = new XRiTextField();
    CLIN22 = new XRiTextField();
    CLTTC = new XRiComboBox();
    OBJ_30 = new JLabel();
    CLESC = new XRiTextField();
    OBJ_32 = new JLabel();
    EBIN05 = new XRiCheckBox();
    lbVendeur = new JLabel();
    EBVDE = new XRiTextField();
    panelGestionSurWeb = new JPanel();
    EBIN09 = new XRiRadioButton();
    EBIN09_1 = new XRiRadioButton();
    EBIN09_2 = new XRiRadioButton();
    OBJ_52 = new JPanel();
    OBJ_70 = new JPanel();
    OBJ_71 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_81 = new JLabel();
    CLREM1 = new XRiTextField();
    CLREM2 = new XRiTextField();
    CLREM3 = new XRiTextField();
    CLCNB = new XRiTextField();
    CLRP1 = new XRiTextField();
    CLRP2 = new XRiTextField();
    CLRP3 = new XRiTextField();
    ZPC15R = new XRiTextField();
    WTAR = new XRiSpinner();
    WTA1 = new XRiSpinner();
    OBJ_53 = new JPanel();
    CLIN15 = new XRiComboBox();
    CLIN16 = new XRiComboBox();
    OBJ_55 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_60 = new SNBoutonDetail();
    OBJ_61 = new SNBoutonDetail();
    CLCNV = new XRiTextField();
    CLCNP = new XRiTextField();
    CLCNR = new XRiTextField();
    CLDAT1 = new XRiTextField();
    OBJ_88 = new JPanel();
    CLIN17 = new XRiCheckBox();
    CLIN10 = new XRiCheckBox();
    OBJ_91 = new JLabel();
    CLIN4 = new XRiComboBox();
    OBJ_103 = new JPanel();
    OBJ_112 = new JPanel();
    OBJ_113 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_120 = new JLabel();
    CLNEX1 = new XRiSpinner();
    CLNEX2 = new XRiSpinner();
    CLNEX3 = new XRiSpinner();
    CLIN21 = new XRiSpinner();
    OBJ_121 = new JPanel();
    CLIN11 = new XRiCheckBox();
    CLIN8 = new XRiCheckBox();
    CLIN27 = new XRiCheckBox();
    CLIN28 = new XRiCheckBox();
    WTEDI = new XRiCheckBox();
    CLIN20 = new XRiCheckBox();
    lbCCEREM = new JLabel();
    CLIN26 = new XRiComboBox();
    OBJ_106 = new JLabel();
    CLIN3 = new XRiComboBox();
    OBJ_104 = new JPanel();
    OBJ_107 = new JLabel();
    OBJ_105 = new JLabel();
    CLLAN = new XRiTextField();
    CLNLA_1_CHK = new JCheckBox();
    CLNLA_2_CHK = new JCheckBox();
    CLNLA_3_CHK = new JCheckBox();
    CLNLA_4_CHK = new JCheckBox();
    OBJ_114 = new JPanel();
    EBIN01 = new XRiCheckBox();
    EBIN02 = new XRiCheckBox();
    EBIN03 = new XRiCheckBox();
    MAIFAC = new XRiTextField();
    OBJ_125 = new JPanel();
    OBJ_128 = new JPanel();
    LIBC1 = new RiZoneSortie();
    LIBC2 = new RiZoneSortie();
    LIBC3 = new RiZoneSortie();
    LIBFI = new RiZoneSortie();
    OBJ_129 = new JLabel();
    OBJ_132 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_138 = new JLabel();
    CLCC1 = new XRiTextField();
    CLCC2 = new XRiTextField();
    CLCC3 = new XRiTextField();
    CLFIR = new XRiTextField();
    OBJ_94 = new JPanel();
    OBJ_95 = new JPanel();
    CLRBF = new XRiComboBox();
    CLPFA = new XRiComboBox();
    CLPRL = new XRiComboBox();
    CLIN12 = new XRiCheckBox();
    OBJ_96 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_100 = new JLabel();
    CLIN19 = new XRiComboBox();
    OBJ_99 = new JLabel();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();
    EBIN09_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(830, 530));
    setPreferredSize(new Dimension(830, 530));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Conditions de vente");
              riSousMenu_bt6.setToolTipText("Conditions de vente");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Conditions sur quantit\u00e9s");
              riSousMenu_bt7.setToolTipText("Conditions sur quantit\u00e9s");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_26 ========
        {
          OBJ_26.setPreferredSize(new Dimension(640, 500));
          OBJ_26.setName("OBJ_26");

          //======== OBJ_27 ========
          {
            OBJ_27.setPreferredSize(new Dimension(350, 350));
            OBJ_27.setOpaque(false);
            OBJ_27.setName("OBJ_27");

            //======== OBJ_28 ========
            {
              OBJ_28.setBorder(new TitledBorder(""));
              OBJ_28.setOpaque(false);
              OBJ_28.setName("OBJ_28");
              OBJ_28.setLayout(null);

              //---- LBCLIF ----
              LBCLIF.setText("@LBCLIF@");
              LBCLIF.setName("LBCLIF");
              OBJ_28.add(LBCLIF);
              LBCLIF.setBounds(300, 168, 272, LBCLIF.getPreferredSize().height);

              //---- LBVILF ----
              LBVILF.setText("@LBVILF@");
              LBVILF.setName("LBVILF");
              OBJ_28.add(LBVILF);
              LBVILF.setBounds(300, 195, 272, LBVILF.getPreferredSize().height);

              //---- DVLIBR ----
              DVLIBR.setText("@DVLIBR@");
              DVLIBR.setName("DVLIBR");
              OBJ_28.add(DVLIBR);
              DVLIBR.setBounds(225, 232, 244, DVLIBR.getPreferredSize().height);

              //---- TFLIBR ----
              TFLIBR.setText("@TFLIBR@");
              TFLIBR.setName("TFLIBR");
              OBJ_28.add(TFLIBR);
              TFLIBR.setBounds(210, 27, 225, TFLIBR.getPreferredSize().height);

              //---- CLIN13 ----
              CLIN13.setText("Non soumis \u00e0 la taxe additionnelle");
              CLIN13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CLIN13.setName("CLIN13");
              OBJ_28.add(CLIN13);
              CLIN13.setBounds(175, 100, 235, 20);

              //---- CLADH ----
              CLADH.setComponentPopupMenu(null);
              CLADH.setName("CLADH");
              OBJ_28.add(CLADH);
              CLADH.setBounds(175, 294, 180, CLADH.getPreferredSize().height);

              //---- LBIN22 ----
              LBIN22.setText("@LBIN22@");
              LBIN22.setName("LBIN22");
              OBJ_28.add(LBIN22);
              LBIN22.setBounds(210, 328, 131, LBIN22.getPreferredSize().height);

              //---- OBJ_29 ----
              OBJ_29.setText("Type facture TVA");
              OBJ_29.setName("OBJ_29");
              OBJ_28.add(OBJ_29);
              OBJ_29.setBounds(25, 29, 130, 20);

              //---- OBJ_37 ----
              OBJ_37.setText("Client factur\u00e9");
              OBJ_37.setName("OBJ_37");
              OBJ_28.add(OBJ_37);
              OBJ_37.setBounds(25, 170, 130, 20);

              //---- OBJ_42 ----
              OBJ_42.setText("Devise");
              OBJ_42.setName("OBJ_42");
              OBJ_28.add(OBJ_42);
              OBJ_42.setBounds(25, 234, 130, 20);

              //---- OBJ_45 ----
              OBJ_45.setText("Commissionnement");
              OBJ_45.setName("OBJ_45");
              OBJ_28.add(OBJ_45);
              OBJ_45.setBounds(25, 266, 130, 20);

              //---- OBJ_47 ----
              OBJ_47.setText("Adh\u00e9rent");
              OBJ_47.setName("OBJ_47");
              OBJ_28.add(OBJ_47);
              OBJ_47.setBounds(25, 298, 130, 20);

              //---- OBJ_49 ----
              OBJ_49.setText("Type de vente");
              OBJ_49.setName("OBJ_49");
              OBJ_28.add(OBJ_49);
              OBJ_49.setBounds(25, 330, 130, 20);

              //---- CLCLFP ----
              CLCLFP.setComponentPopupMenu(null);
              CLCLFP.setName("CLCLFP");
              OBJ_28.add(CLCLFP);
              CLCLFP.setBounds(175, 166, 70, CLCLFP.getPreferredSize().height);

              //---- CLCNC ----
              CLCNC.setComponentPopupMenu(BTD);
              CLCNC.setName("CLCNC");
              OBJ_28.add(CLCNC);
              CLCNC.setBounds(175, 262, 50, CLCNC.getPreferredSize().height);

              //---- CLDEV ----
              CLDEV.setComponentPopupMenu(BTD);
              CLDEV.setName("CLDEV");
              OBJ_28.add(CLDEV);
              CLDEV.setBounds(175, 230, 40, CLDEV.getPreferredSize().height);

              //---- CLCLFS ----
              CLCLFS.setComponentPopupMenu(null);
              CLCLFS.setName("CLCLFS");
              OBJ_28.add(CLCLFS);
              CLCLFS.setBounds(255, 166, 40, CLCLFS.getPreferredSize().height);

              //---- CLTFA ----
              CLTFA.setComponentPopupMenu(BTD);
              CLTFA.setName("CLTFA");
              OBJ_28.add(CLTFA);
              CLTFA.setBounds(175, 25, 26, CLTFA.getPreferredSize().height);

              //---- CLIN22 ----
              CLIN22.setComponentPopupMenu(BTD);
              CLIN22.setName("CLIN22");
              OBJ_28.add(CLIN22);
              CLIN22.setBounds(175, 326, 26, CLIN22.getPreferredSize().height);

              //---- CLTTC ----
              CLTTC.setModel(new DefaultComboBoxModel(new String[] {
                "Calcul HT et \u00e9dition HT ",
                "Calcul TTC et \u00e9dition TTC",
                "Calcul TTC et \u00e9dition  HT"
              }));
              CLTTC.setName("CLTTC");
              OBJ_28.add(CLTTC);
              CLTTC.setBounds(175, 73, 200, CLTTC.getPreferredSize().height);

              //---- OBJ_30 ----
              OBJ_30.setText("Mode HT / TTC");
              OBJ_30.setName("OBJ_30");
              OBJ_28.add(OBJ_30);
              OBJ_30.setBounds(25, 76, 130, 20);

              //---- CLESC ----
              CLESC.setName("CLESC");
              OBJ_28.add(CLESC);
              CLESC.setBounds(175, 135, 39, CLESC.getPreferredSize().height);

              //---- OBJ_32 ----
              OBJ_32.setText("Pourcentage d'escompte");
              OBJ_32.setName("OBJ_32");
              OBJ_28.add(OBJ_32);
              OBJ_32.setBounds(25, 139, 145, 20);

              //---- EBIN05 ----
              EBIN05.setText("Client comptant");
              EBIN05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EBIN05.setName("EBIN05");
              OBJ_28.add(EBIN05);
              EBIN05.setBounds(405, 77, 166, EBIN05.getPreferredSize().height);

              //---- lbVendeur ----
              lbVendeur.setText("Vendeur");
              lbVendeur.setName("lbVendeur");
              OBJ_28.add(lbVendeur);
              lbVendeur.setBounds(27, 370, 130, 20);

              //---- EBVDE ----
              EBVDE.setComponentPopupMenu(BTD);
              EBVDE.setName("EBVDE");
              OBJ_28.add(EBVDE);
              EBVDE.setBounds(176, 365, 40, EBVDE.getPreferredSize().height);

              //======== panelGestionSurWeb ========
              {
                panelGestionSurWeb.setBorder(new TitledBorder(""));
                panelGestionSurWeb.setOpaque(false);
                panelGestionSurWeb.setName("panelGestionSurWeb");
                panelGestionSurWeb.setLayout(null);

                //---- EBIN09 ----
                EBIN09.setText("Non g\u00e9r\u00e9 sur le Web");
                EBIN09.setName("EBIN09");
                panelGestionSurWeb.add(EBIN09);
                EBIN09.setBounds(10, 5, 225, 18);

                //---- EBIN09_1 ----
                EBIN09_1.setText("G\u00e9r\u00e9 sur le Web");
                EBIN09_1.setName("EBIN09_1");
                panelGestionSurWeb.add(EBIN09_1);
                EBIN09_1.setBounds(10, 23, 225, 18);

                //---- EBIN09_2 ----
                EBIN09_2.setText("G\u00e9r\u00e9 mais non actif");
                EBIN09_2.setName("EBIN09_2");
                panelGestionSurWeb.add(EBIN09_2);
                EBIN09_2.setBounds(10, 41, 225, 18);
              }
              OBJ_28.add(panelGestionSurWeb);
              panelGestionSurWeb.setBounds(385, 370, 190, 70);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_28.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_28.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_28.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_28.setMinimumSize(preferredSize);
                OBJ_28.setPreferredSize(preferredSize);
              }
            }

            GroupLayout OBJ_27Layout = new GroupLayout(OBJ_27);
            OBJ_27.setLayout(OBJ_27Layout);
            OBJ_27Layout.setHorizontalGroup(
              OBJ_27Layout.createParallelGroup()
                .addGroup(OBJ_27Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(30, Short.MAX_VALUE))
            );
            OBJ_27Layout.setVerticalGroup(
              OBJ_27Layout.createParallelGroup()
                .addGroup(OBJ_27Layout.createSequentialGroup()
                  .addComponent(OBJ_28, GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                  .addContainerGap())
            );
          }
          OBJ_26.addTab("Divers", OBJ_27);

          //======== OBJ_52 ========
          {
            OBJ_52.setPreferredSize(new Dimension(635, 510));
            OBJ_52.setOpaque(false);
            OBJ_52.setName("OBJ_52");
            OBJ_52.setLayout(null);

            //======== OBJ_70 ========
            {
              OBJ_70.setBorder(new TitledBorder("Remises"));
              OBJ_70.setOpaque(false);
              OBJ_70.setName("OBJ_70");
              OBJ_70.setLayout(null);

              //---- OBJ_71 ----
              OBJ_71.setText("Ligne");
              OBJ_71.setName("OBJ_71");
              OBJ_70.add(OBJ_71);
              OBJ_71.setBounds(23, 40, 156, 20);

              //---- OBJ_77 ----
              OBJ_77.setText("Bon");
              OBJ_77.setName("OBJ_77");
              OBJ_70.add(OBJ_77);
              OBJ_77.setBounds(23, 70, 156, 20);

              //---- OBJ_83 ----
              OBJ_83.setText("Colonnes tarif");
              OBJ_83.setName("OBJ_83");
              OBJ_70.add(OBJ_83);
              OBJ_83.setBounds(23, 100, 156, 20);

              //---- OBJ_75 ----
              OBJ_75.setText("Remise de pied");
              OBJ_75.setName("OBJ_75");
              OBJ_70.add(OBJ_75);
              OBJ_75.setBounds(375, 39, 132, 20);

              //---- OBJ_81 ----
              OBJ_81.setText("Pourcentage \u00e9pargne");
              OBJ_81.setName("OBJ_81");
              OBJ_70.add(OBJ_81);
              OBJ_81.setBounds(375, 70, 140, 20);

              //---- CLREM1 ----
              CLREM1.setComponentPopupMenu(null);
              CLREM1.setName("CLREM1");
              OBJ_70.add(CLREM1);
              CLREM1.setBounds(180, 35, 60, CLREM1.getPreferredSize().height);

              //---- CLREM2 ----
              CLREM2.setName("CLREM2");
              OBJ_70.add(CLREM2);
              CLREM2.setBounds(240, 35, 60, CLREM2.getPreferredSize().height);

              //---- CLREM3 ----
              CLREM3.setName("CLREM3");
              OBJ_70.add(CLREM3);
              CLREM3.setBounds(300, 35, 60, CLREM3.getPreferredSize().height);

              //---- CLCNB ----
              CLCNB.setComponentPopupMenu(BTD);
              CLCNB.setName("CLCNB");
              OBJ_70.add(CLCNB);
              CLCNB.setBounds(513, 35, 60, CLCNB.getPreferredSize().height);

              //---- CLRP1 ----
              CLRP1.setComponentPopupMenu(BTD);
              CLRP1.setName("CLRP1");
              OBJ_70.add(CLRP1);
              CLRP1.setBounds(180, 65, 60, CLRP1.getPreferredSize().height);

              //---- CLRP2 ----
              CLRP2.setName("CLRP2");
              OBJ_70.add(CLRP2);
              CLRP2.setBounds(240, 65, 60, CLRP2.getPreferredSize().height);

              //---- CLRP3 ----
              CLRP3.setName("CLRP3");
              OBJ_70.add(CLRP3);
              CLRP3.setBounds(300, 65, 60, CLRP3.getPreferredSize().height);

              //---- ZPC15R ----
              ZPC15R.setName("ZPC15R");
              OBJ_70.add(ZPC15R);
              ZPC15R.setBounds(513, 66, 60, ZPC15R.getPreferredSize().height);

              //---- WTAR ----
              WTAR.setModel(new SpinnerListModel(new String[] {"", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
              WTAR.setName("WTAR");
              OBJ_70.add(WTAR);
              WTAR.setBounds(180, 95, 50, WTAR.getPreferredSize().height);

              //---- WTA1 ----
              WTA1.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"}));
              WTA1.setName("WTA1");
              OBJ_70.add(WTA1);
              WTA1.setBounds(240, 95, 50, WTA1.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_70.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_70.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_70.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_70.setMinimumSize(preferredSize);
                OBJ_70.setPreferredSize(preferredSize);
              }
            }
            OBJ_52.add(OBJ_70);
            OBJ_70.setBounds(15, 170, 609, 145);

            //======== OBJ_53 ========
            {
              OBJ_53.setBorder(new TitledBorder(""));
              OBJ_53.setOpaque(false);
              OBJ_53.setName("OBJ_53");
              OBJ_53.setLayout(null);

              //---- CLIN15 ----
              CLIN15.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de g\u00e9n\u00e9ration",
                "\u00e0 la facturation",
                "En cr\u00e9ation / modification de ligne"
              }));
              CLIN15.setName("CLIN15");
              OBJ_53.add(CLIN15);
              CLIN15.setBounds(323, 49, 274, CLIN15.getPreferredSize().height);

              //---- CLIN16 ----
              CLIN16.setModel(new DefaultComboBoxModel(new String[] {
                "Syst\u00e9matique",
                "Au 1\u00b0 bon du mois",
                "Pas de frais fixe"
              }));
              CLIN16.setName("CLIN16");
              OBJ_53.add(CLIN16);
              CLIN16.setBounds(463, 105, 134, CLIN16.getPreferredSize().height);

              //---- OBJ_55 ----
              OBJ_55.setText("Standard");
              OBJ_55.setName("OBJ_55");
              OBJ_53.add(OBJ_55);
              OBJ_55.setBounds(23, 24, 157, 20);

              //---- OBJ_63 ----
              OBJ_63.setText("Promotion");
              OBJ_63.setName("OBJ_63");
              OBJ_53.add(OBJ_63);
              OBJ_63.setBounds(23, 52, 157, 20);

              //---- OBJ_65 ----
              OBJ_65.setText("Ristourne sur p\u00e9riode");
              OBJ_65.setName("OBJ_65");
              OBJ_53.add(OBJ_65);
              OBJ_65.setBounds(23, 80, 157, 20);

              //---- OBJ_54 ----
              OBJ_54.setText("G\u00e9n\u00e9ration automatique de CNV");
              OBJ_54.setName("OBJ_54");
              OBJ_53.add(OBJ_54);
              OBJ_54.setBounds(323, 24, 264, 20);

              //---- OBJ_66 ----
              OBJ_66.setText("Application frais fixes");
              OBJ_66.setName("OBJ_66");
              OBJ_53.add(OBJ_66);
              OBJ_66.setBounds(323, 108, 128, 20);

              //---- OBJ_93 ----
              OBJ_93.setText("CA pr\u00e9visionnel");
              OBJ_93.setName("OBJ_93");
              OBJ_53.add(OBJ_93);
              OBJ_93.setBounds(23, 108, 115, 20);

              //---- OBJ_60 ----
              OBJ_60.setText("");
              OBJ_60.setMargin(new Insets(0, 0, 0, 0));
              OBJ_60.setName("OBJ_60");
              OBJ_60.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_60ActionPerformed(e);
                }
              });
              OBJ_53.add(OBJ_60);
              OBJ_60.setBounds(250, 18, 25, 32);

              //---- OBJ_61 ----
              OBJ_61.setText("");
              OBJ_61.setMargin(new Insets(0, 0, 0, 0));
              OBJ_61.setName("OBJ_61");
              OBJ_61.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_61ActionPerformed(e);
                }
              });
              OBJ_53.add(OBJ_61);
              OBJ_61.setBounds(250, 74, 25, 32);

              //---- CLCNV ----
              CLCNV.setComponentPopupMenu(BTD);
              CLCNV.setName("CLCNV");
              OBJ_53.add(CLCNV);
              CLCNV.setBounds(180, 20, 70, CLCNV.getPreferredSize().height);

              //---- CLCNP ----
              CLCNP.setComponentPopupMenu(BTD);
              CLCNP.setName("CLCNP");
              OBJ_53.add(CLCNP);
              CLCNP.setBounds(180, 48, 70, CLCNP.getPreferredSize().height);

              //---- CLCNR ----
              CLCNR.setComponentPopupMenu(BTD);
              CLCNR.setName("CLCNR");
              OBJ_53.add(CLCNR);
              CLCNR.setBounds(180, 76, 70, CLCNR.getPreferredSize().height);

              //---- CLDAT1 ----
              CLDAT1.setComponentPopupMenu(BTD);
              CLDAT1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
              CLDAT1.setName("CLDAT1");
              OBJ_53.add(CLDAT1);
              CLDAT1.setBounds(180, 104, 70, CLDAT1.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_53.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_53.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_53.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_53.setMinimumSize(preferredSize);
                OBJ_53.setPreferredSize(preferredSize);
              }
            }
            OBJ_52.add(OBJ_53);
            OBJ_53.setBounds(15, 10, 609, 155);

            //======== OBJ_88 ========
            {
              OBJ_88.setBorder(new TitledBorder("Exclusion"));
              OBJ_88.setOpaque(false);
              OBJ_88.setName("OBJ_88");
              OBJ_88.setLayout(null);

              //---- CLIN17 ----
              CLIN17.setText("Remise de pied si une condition de vente d\u00e9j\u00e0 appliqu\u00e9e");
              CLIN17.setName("CLIN17");
              OBJ_88.add(CLIN17);
              CLIN17.setBounds(248, 45, 365, 20);

              //---- CLIN10 ----
              CLIN10.setText("Condition de vente sur la quantit\u00e9");
              CLIN10.setName("CLIN10");
              OBJ_88.add(CLIN10);
              CLIN10.setBounds(23, 45, 221, 20);

              //---- OBJ_91 ----
              OBJ_91.setText("Condition g\u00e9n\u00e9rale sur la DG");
              OBJ_91.setName("OBJ_91");
              OBJ_88.add(OBJ_91);
              OBJ_91.setBounds(23, 73, 204, 20);

              //---- CLIN4 ----
              CLIN4.setModel(new DefaultComboBoxModel(new String[] {
                "Pas d'exclusion",
                "Exclusion condition de vente g\u00e9n\u00e9rale (Param DG)",
                "Exclusion condition de vente sur les articles li\u00e9s",
                "Exclusion condition de vente g\u00e9n\u00e9rale et les articles li\u00e9s",
                "Si une condition client existe, elle prime"
              }));
              CLIN4.setName("CLIN4");
              OBJ_88.add(CLIN4);
              CLIN4.setBounds(248, 70, 337, CLIN4.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_88.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_88.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_88.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_88.setMinimumSize(preferredSize);
                OBJ_88.setPreferredSize(preferredSize);
              }
            }
            OBJ_52.add(OBJ_88);
            OBJ_88.setBounds(15, 315, 609, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < OBJ_52.getComponentCount(); i++) {
                Rectangle bounds = OBJ_52.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_52.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_52.setMinimumSize(preferredSize);
              OBJ_52.setPreferredSize(preferredSize);
            }
          }
          OBJ_26.addTab("Conditions de vente", OBJ_52);

          //======== OBJ_103 ========
          {
            OBJ_103.setOpaque(false);
            OBJ_103.setName("OBJ_103");
            OBJ_103.setLayout(null);

            //======== OBJ_112 ========
            {
              OBJ_112.setBorder(new TitledBorder(""));
              OBJ_112.setOpaque(false);
              OBJ_112.setName("OBJ_112");
              OBJ_112.setLayout(null);

              //---- OBJ_113 ----
              OBJ_113.setText("Nombre d'exemplaires du bon de commande");
              OBJ_113.setName("OBJ_113");
              OBJ_112.add(OBJ_113);
              OBJ_113.setBounds(25, 15, 304, 20);

              //---- OBJ_115 ----
              OBJ_115.setText("Nombre d'exemplaires du bon d'exp\u00e9dition");
              OBJ_115.setName("OBJ_115");
              OBJ_112.add(OBJ_115);
              OBJ_115.setBounds(25, 48, 303, 20);

              //---- OBJ_117 ----
              OBJ_117.setText("Nombre d'exemplaires de la facture");
              OBJ_117.setName("OBJ_117");
              OBJ_112.add(OBJ_117);
              OBJ_117.setBounds(25, 81, 303, 20);

              //---- OBJ_120 ----
              OBJ_120.setText("Nombre d'exemplaires d'avoir");
              OBJ_120.setName("OBJ_120");
              OBJ_112.add(OBJ_120);
              OBJ_120.setBounds(25, 114, 303, 20);

              //---- CLNEX1 ----
              CLNEX1.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
              CLNEX1.setName("CLNEX1");
              OBJ_112.add(CLNEX1);
              CLNEX1.setBounds(335, 11, 41, CLNEX1.getPreferredSize().height);

              //---- CLNEX2 ----
              CLNEX2.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
              CLNEX2.setName("CLNEX2");
              OBJ_112.add(CLNEX2);
              CLNEX2.setBounds(335, 44, 41, CLNEX2.getPreferredSize().height);

              //---- CLNEX3 ----
              CLNEX3.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
              CLNEX3.setName("CLNEX3");
              OBJ_112.add(CLNEX3);
              CLNEX3.setBounds(335, 77, 41, CLNEX3.getPreferredSize().height);

              //---- CLIN21 ----
              CLIN21.setModel(new SpinnerListModel(new String[] {" ", "1", "2", "3", "4", "5", "6", "7", "8", "9"}));
              CLIN21.setName("CLIN21");
              OBJ_112.add(CLIN21);
              CLIN21.setBounds(335, 110, 41, CLIN21.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_112.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_112.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_112.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_112.setMinimumSize(preferredSize);
                OBJ_112.setPreferredSize(preferredSize);
              }
            }
            OBJ_103.add(OBJ_112);
            OBJ_112.setBounds(15, 95, 395, 155);

            //======== OBJ_121 ========
            {
              OBJ_121.setBorder(new TitledBorder(""));
              OBJ_121.setOpaque(false);
              OBJ_121.setName("OBJ_121");
              OBJ_121.setLayout(null);

              //---- CLIN11 ----
              CLIN11.setText("Etiquette colis par bon");
              CLIN11.setName("CLIN11");
              OBJ_121.add(CLIN11);
              CLIN11.setBounds(25, 53, 280, 20);

              //---- CLIN8 ----
              CLIN8.setText("Facture et bon d'exp\u00e9dition encha\u00een\u00e9s");
              CLIN8.setName("CLIN8");
              OBJ_121.add(CLIN8);
              CLIN8.setBounds(25, 91, 280, 20);

              //---- CLIN27 ----
              CLIN27.setText("Non \u00e9dition du prix de base");
              CLIN27.setName("CLIN27");
              OBJ_121.add(CLIN27);
              CLIN27.setBounds(335, 15, 255, 20);

              //---- CLIN28 ----
              CLIN28.setText("Non \u00e9dition du num\u00e9ro de s\u00e9rie");
              CLIN28.setName("CLIN28");
              OBJ_121.add(CLIN28);
              CLIN28.setBounds(335, 54, 255, CLIN28.getPreferredSize().height);

              //---- WTEDI ----
              WTEDI.setText("Edition de la facture par EDI");
              WTEDI.setToolTipText("Si vous cochez EDI (\u00e9change de donn\u00e9es informatis\u00e9), la facture ne sera pas imprim\u00e9e avec le flot des autres factures");
              WTEDI.setName("WTEDI");
              OBJ_121.add(WTEDI);
              WTEDI.setBounds(335, 92, 255, WTEDI.getPreferredSize().height);

              //---- CLIN20 ----
              CLIN20.setText("Edition pi\u00e8ce suppl\u00e9mentaire");
              CLIN20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CLIN20.setName("CLIN20");
              OBJ_121.add(CLIN20);
              CLIN20.setBounds(335, 129, 240, 20);

              //---- lbCCEREM ----
              lbCCEREM.setText("Edition des remises");
              lbCCEREM.setName("lbCCEREM");
              OBJ_121.add(lbCCEREM);
              lbCCEREM.setBounds(25, 125, 125, 28);

              //---- CLIN26 ----
              CLIN26.setModel(new DefaultComboBoxModel(new String[] {
                "Edition \u00e9ventuelle",
                "Pas d'\u00e9dition"
              }));
              CLIN26.setComponentPopupMenu(null);
              CLIN26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CLIN26.setName("CLIN26");
              OBJ_121.add(CLIN26);
              CLIN26.setBounds(140, 126, 176, CLIN26.getPreferredSize().height);

              //---- OBJ_106 ----
              OBJ_106.setText("Chiffrage bons");
              OBJ_106.setName("OBJ_106");
              OBJ_121.add(OBJ_106);
              OBJ_106.setBounds(25, 15, 96, 20);

              //---- CLIN3 ----
              CLIN3.setModel(new DefaultComboBoxModel(new String[] {
                "Edition chiffr\u00e9e",
                "Edition non chiffr\u00e9e",
                "Edition sans totaux"
              }));
              CLIN3.setComponentPopupMenu(null);
              CLIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CLIN3.setName("CLIN3");
              OBJ_121.add(CLIN3);
              CLIN3.setBounds(140, 12, 176, CLIN3.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_121.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_121.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_121.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_121.setMinimumSize(preferredSize);
                OBJ_121.setPreferredSize(preferredSize);
              }
            }
            OBJ_103.add(OBJ_121);
            OBJ_121.setBounds(15, 260, 600, 175);

            //======== OBJ_104 ========
            {
              OBJ_104.setBorder(new TitledBorder(""));
              OBJ_104.setOpaque(false);
              OBJ_104.setName("OBJ_104");

              //---- OBJ_107 ----
              OBJ_107.setText("Choix des libell\u00e9s d'article \u00e0 \u00e9diter");
              OBJ_107.setName("OBJ_107");

              //---- OBJ_105 ----
              OBJ_105.setText("Code langue");
              OBJ_105.setName("OBJ_105");

              //---- CLLAN ----
              CLLAN.setName("CLLAN");

              //---- CLNLA_1_CHK ----
              CLNLA_1_CHK.setText("1");
              CLNLA_1_CHK.setName("CLNLA_1_CHK");

              //---- CLNLA_2_CHK ----
              CLNLA_2_CHK.setText("2");
              CLNLA_2_CHK.setName("CLNLA_2_CHK");

              //---- CLNLA_3_CHK ----
              CLNLA_3_CHK.setText("3");
              CLNLA_3_CHK.setName("CLNLA_3_CHK");

              //---- CLNLA_4_CHK ----
              CLNLA_4_CHK.setText("4");
              CLNLA_4_CHK.setName("CLNLA_4_CHK");

              GroupLayout OBJ_104Layout = new GroupLayout(OBJ_104);
              OBJ_104.setLayout(OBJ_104Layout);
              OBJ_104Layout.setHorizontalGroup(
                OBJ_104Layout.createParallelGroup()
                  .addGroup(OBJ_104Layout.createSequentialGroup()
                    .addGap(12, 12, 12)
                    .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                    .addGap(1, 1, 1)
                    .addComponent(CLLAN, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                    .addGap(38, 38, 38)
                    .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
                    .addGroup(OBJ_104Layout.createParallelGroup()
                      .addGroup(OBJ_104Layout.createSequentialGroup()
                        .addGap(129, 129, 129)
                        .addComponent(CLNLA_4_CHK, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                      .addGroup(OBJ_104Layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addComponent(CLNLA_2_CHK, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                      .addComponent(CLNLA_1_CHK, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE)
                      .addGroup(OBJ_104Layout.createSequentialGroup()
                        .addGap(86, 86, 86)
                        .addComponent(CLNLA_3_CHK, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))))
              );
              OBJ_104Layout.setVerticalGroup(
                OBJ_104Layout.createParallelGroup()
                  .addGroup(OBJ_104Layout.createSequentialGroup()
                    .addGap(18, 18, 18)
                    .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_104Layout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addComponent(CLLAN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_104Layout.createSequentialGroup()
                    .addGap(18, 18, 18)
                    .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(OBJ_104Layout.createSequentialGroup()
                    .addGap(18, 18, 18)
                    .addGroup(OBJ_104Layout.createParallelGroup()
                      .addComponent(CLNLA_4_CHK, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CLNLA_2_CHK, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CLNLA_1_CHK, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(CLNLA_3_CHK, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
              );
            }
            OBJ_103.add(OBJ_104);
            OBJ_104.setBounds(15, 10, 600, 75);

            //======== OBJ_114 ========
            {
              OBJ_114.setBorder(new TitledBorder(""));
              OBJ_114.setOpaque(false);
              OBJ_114.setName("OBJ_114");
              OBJ_114.setLayout(null);

              //---- EBIN01 ----
              EBIN01.setText("Commande EDI");
              EBIN01.setToolTipText("Si vous cochez EDI (\u00e9change de donn\u00e9es informatis\u00e9), la facture ne sera pas imprim\u00e9e avec le flot des autres factures");
              EBIN01.setName("EBIN01");
              OBJ_114.add(EBIN01);
              EBIN01.setBounds(25, 20, 130, 30);

              //---- EBIN02 ----
              EBIN02.setText("Exp\u00e9dition EDI");
              EBIN02.setToolTipText("Si vous cochez EDI (\u00e9change de donn\u00e9es informatis\u00e9), la facture ne sera pas imprim\u00e9e avec le flot des autres factures");
              EBIN02.setName("EBIN02");
              OBJ_114.add(EBIN02);
              EBIN02.setBounds(25, 60, 130, 30);

              //---- EBIN03 ----
              EBIN03.setText("Facture EDI");
              EBIN03.setToolTipText("Si vous cochez EDI (\u00e9change de donn\u00e9es informatis\u00e9), la facture ne sera pas imprim\u00e9e avec le flot des autres factures");
              EBIN03.setName("EBIN03");
              OBJ_114.add(EBIN03);
              EBIN03.setBounds(25, 100, 130, 30);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_114.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_114.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_114.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_114.setMinimumSize(preferredSize);
                OBJ_114.setPreferredSize(preferredSize);
              }
            }
            OBJ_103.add(OBJ_114);
            OBJ_114.setBounds(420, 95, 195, 155);

            //---- MAIFAC ----
            MAIFAC.setPreferredSize(new Dimension(600, 25));
            MAIFAC.setMaximumSize(new Dimension(600, 25));
            MAIFAC.setMinimumSize(new Dimension(600, 25));
            MAIFAC.setName("MAIFAC");
            OBJ_103.add(MAIFAC);
            MAIFAC.setBounds(15, 435, MAIFAC.getPreferredSize().width, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < OBJ_103.getComponentCount(); i++) {
                Rectangle bounds = OBJ_103.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_103.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_103.setMinimumSize(preferredSize);
              OBJ_103.setPreferredSize(preferredSize);
            }
          }
          OBJ_26.addTab("Edition bons et factures", OBJ_103);

          //======== OBJ_125 ========
          {
            OBJ_125.setOpaque(false);
            OBJ_125.setName("OBJ_125");

            //======== OBJ_128 ========
            {
              OBJ_128.setBorder(new TitledBorder("Centrales de rattachement du client"));
              OBJ_128.setOpaque(false);
              OBJ_128.setName("OBJ_128");
              OBJ_128.setLayout(null);

              //---- LIBC1 ----
              LIBC1.setText("@LIBC1@");
              LIBC1.setName("LIBC1");
              OBJ_128.add(LIBC1);
              LIBC1.setBounds(210, 48, 310, LIBC1.getPreferredSize().height);

              //---- LIBC2 ----
              LIBC2.setText("@LIBC2@");
              LIBC2.setName("LIBC2");
              OBJ_128.add(LIBC2);
              LIBC2.setBounds(210, 86, 310, LIBC2.getPreferredSize().height);

              //---- LIBC3 ----
              LIBC3.setText("@LIBC3@");
              LIBC3.setName("LIBC3");
              OBJ_128.add(LIBC3);
              LIBC3.setBounds(210, 124, 310, LIBC3.getPreferredSize().height);

              //---- LIBFI ----
              LIBFI.setText("@LIBFI@");
              LIBFI.setName("LIBFI");
              OBJ_128.add(LIBFI);
              LIBFI.setBounds(210, 162, 310, LIBFI.getPreferredSize().height);

              //---- OBJ_129 ----
              OBJ_129.setText("Centrale 1");
              OBJ_129.setName("OBJ_129");
              OBJ_128.add(OBJ_129);
              OBJ_129.setBounds(33, 50, 82, 20);

              //---- OBJ_132 ----
              OBJ_132.setText("Centrale 2");
              OBJ_132.setName("OBJ_132");
              OBJ_128.add(OBJ_132);
              OBJ_132.setBounds(33, 88, 82, 20);

              //---- OBJ_135 ----
              OBJ_135.setText("Centrale 3");
              OBJ_135.setName("OBJ_135");
              OBJ_128.add(OBJ_135);
              OBJ_135.setBounds(33, 126, 82, 20);

              //---- OBJ_138 ----
              OBJ_138.setText("Firme");
              OBJ_138.setName("OBJ_138");
              OBJ_128.add(OBJ_138);
              OBJ_138.setBounds(33, 164, 82, 20);

              //---- CLCC1 ----
              CLCC1.setComponentPopupMenu(BTD);
              CLCC1.setName("CLCC1");
              OBJ_128.add(CLCC1);
              CLCC1.setBounds(120, 46, 70, CLCC1.getPreferredSize().height);

              //---- CLCC2 ----
              CLCC2.setComponentPopupMenu(BTD);
              CLCC2.setName("CLCC2");
              OBJ_128.add(CLCC2);
              CLCC2.setBounds(120, 84, 70, CLCC2.getPreferredSize().height);

              //---- CLCC3 ----
              CLCC3.setComponentPopupMenu(BTD);
              CLCC3.setName("CLCC3");
              OBJ_128.add(CLCC3);
              CLCC3.setBounds(120, 122, 70, CLCC3.getPreferredSize().height);

              //---- CLFIR ----
              CLFIR.setComponentPopupMenu(BTD);
              CLFIR.setName("CLFIR");
              OBJ_128.add(CLFIR);
              CLFIR.setBounds(120, 160, 70, CLFIR.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_128.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_128.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_128.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_128.setMinimumSize(preferredSize);
                OBJ_128.setPreferredSize(preferredSize);
              }
            }

            GroupLayout OBJ_125Layout = new GroupLayout(OBJ_125);
            OBJ_125.setLayout(OBJ_125Layout);
            OBJ_125Layout.setHorizontalGroup(
              OBJ_125Layout.createParallelGroup()
                .addGroup(OBJ_125Layout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 570, GroupLayout.PREFERRED_SIZE))
            );
            OBJ_125Layout.setVerticalGroup(
              OBJ_125Layout.createParallelGroup()
                .addGroup(OBJ_125Layout.createSequentialGroup()
                  .addGap(30, 30, 30)
                  .addComponent(OBJ_128, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE))
            );
          }
          OBJ_26.addTab("Centrales", OBJ_125);

          //======== OBJ_94 ========
          {
            OBJ_94.setOpaque(false);
            OBJ_94.setName("OBJ_94");
            OBJ_94.setLayout(null);

            //======== OBJ_95 ========
            {
              OBJ_95.setBorder(new TitledBorder(""));
              OBJ_95.setOpaque(false);
              OBJ_95.setName("OBJ_95");
              OBJ_95.setLayout(null);

              //---- CLRBF ----
              CLRBF.setModel(new DefaultComboBoxModel(new String[] {
                "Pas de regroupement",
                "Regroupement",
                "Regroup. d'un m\u00eame client livr\u00e9",
                "Regroup. sur m\u00eame r\u00e9f\u00e9rence commande",
                "Regroup. sur m\u00eame commande initiale",
                "Regroup. et ZP n\u00b01 du bon",
                "Regroup. sur la m\u00eame date d'\u00e9ch\u00e9ance",
                "Regroup.sur r\u00e9f\u00e9rence longue (chantier)",
                "Regroup. par chantier"
              }));
              CLRBF.setName("CLRBF");
              OBJ_95.add(CLRBF);
              CLRBF.setBounds(220, 103, 345, CLRBF.getPreferredSize().height);

              //---- CLPFA ----
              CLPFA.setModel(new DefaultComboBoxModel(new String[] {
                "Sans p\u00e9riodicit\u00e9",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CLPFA.setName("CLPFA");
              OBJ_95.add(CLPFA);
              CLPFA.setBounds(220, 25, 140, CLPFA.getPreferredSize().height);

              //---- CLPRL ----
              CLPRL.setModel(new DefaultComboBoxModel(new String[] {
                "Inactif",
                "Journ\u00e9e",
                "Semaine",
                "D\u00e9cade",
                "Quinzaine",
                "Mois"
              }));
              CLPRL.setName("CLPRL");
              OBJ_95.add(CLPRL);
              CLPRL.setBounds(220, 64, 140, CLPRL.getPreferredSize().height);

              //---- CLIN12 ----
              CLIN12.setText("Commande sold\u00e9e");
              CLIN12.setName("CLIN12");
              OBJ_95.add(CLIN12);
              CLIN12.setBounds(44, 145, 255, 20);

              //---- OBJ_96 ----
              OBJ_96.setText("Facturation");
              OBJ_96.setName("OBJ_96");
              OBJ_95.add(OBJ_96);
              OBJ_96.setBounds(44, 28, 176, 20);

              //---- OBJ_98 ----
              OBJ_98.setText("Relev\u00e9");
              OBJ_98.setName("OBJ_98");
              OBJ_95.add(OBJ_98);
              OBJ_98.setBounds(44, 67, 176, 20);

              //---- OBJ_100 ----
              OBJ_100.setText("Regroupement bons/facture");
              OBJ_100.setName("OBJ_100");
              OBJ_95.add(OBJ_100);
              OBJ_100.setBounds(44, 106, 176, 20);

              //---- CLIN19 ----
              CLIN19.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "Edition cumul\u00e9e sur factures",
                "R\u00e9capitulatif bons en fin de facture"
              }));
              CLIN19.setName("CLIN19");
              OBJ_95.add(CLIN19);
              CLIN19.setBounds(220, 182, 250, CLIN19.getPreferredSize().height);

              //---- OBJ_99 ----
              OBJ_99.setText("Edition factures");
              OBJ_99.setName("OBJ_99");
              OBJ_95.add(OBJ_99);
              OBJ_99.setBounds(44, 185, 176, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_95.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_95.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_95.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_95.setMinimumSize(preferredSize);
                OBJ_95.setPreferredSize(preferredSize);
              }
            }
            OBJ_94.add(OBJ_95);
            OBJ_95.setBounds(15, 15, 590, 235);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < OBJ_94.getComponentCount(); i++) {
                Rectangle bounds = OBJ_94.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = OBJ_94.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              OBJ_94.setMinimumSize(preferredSize);
              OBJ_94.setPreferredSize(preferredSize);
            }
          }
          OBJ_26.addTab("P\u00e9riodique", OBJ_94);
        }
        p_contenu.add(OBJ_26);
        OBJ_26.setBounds(25, 25, 635, 495);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);

      //---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }

    //---- EBIN09_GRP ----
    EBIN09_GRP.add(EBIN09);
    EBIN09_GRP.add(EBIN09_1);
    EBIN09_GRP.add(EBIN09_2);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JTabbedPane OBJ_26;
  private JPanel OBJ_27;
  private JPanel OBJ_28;
  private RiZoneSortie LBCLIF;
  private RiZoneSortie LBVILF;
  private RiZoneSortie DVLIBR;
  private RiZoneSortie TFLIBR;
  private XRiCheckBox CLIN13;
  private XRiTextField CLADH;
  private RiZoneSortie LBIN22;
  private JLabel OBJ_29;
  private JLabel OBJ_37;
  private JLabel OBJ_42;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private JLabel OBJ_49;
  private XRiTextField CLCLFP;
  private XRiTextField CLCNC;
  private XRiTextField CLDEV;
  private XRiTextField CLCLFS;
  private XRiTextField CLTFA;
  private XRiTextField CLIN22;
  private XRiComboBox CLTTC;
  private JLabel OBJ_30;
  private XRiTextField CLESC;
  private JLabel OBJ_32;
  private XRiCheckBox EBIN05;
  private JLabel lbVendeur;
  private XRiTextField EBVDE;
  private JPanel panelGestionSurWeb;
  private XRiRadioButton EBIN09;
  private XRiRadioButton EBIN09_1;
  private XRiRadioButton EBIN09_2;
  private JPanel OBJ_52;
  private JPanel OBJ_70;
  private JLabel OBJ_71;
  private JLabel OBJ_77;
  private JLabel OBJ_83;
  private JLabel OBJ_75;
  private JLabel OBJ_81;
  private XRiTextField CLREM1;
  private XRiTextField CLREM2;
  private XRiTextField CLREM3;
  private XRiTextField CLCNB;
  private XRiTextField CLRP1;
  private XRiTextField CLRP2;
  private XRiTextField CLRP3;
  private XRiTextField ZPC15R;
  private XRiSpinner WTAR;
  private XRiSpinner WTA1;
  private JPanel OBJ_53;
  private XRiComboBox CLIN15;
  private XRiComboBox CLIN16;
  private JLabel OBJ_55;
  private JLabel OBJ_63;
  private JLabel OBJ_65;
  private JLabel OBJ_54;
  private JLabel OBJ_66;
  private JLabel OBJ_93;
  private SNBoutonDetail OBJ_60;
  private SNBoutonDetail OBJ_61;
  private XRiTextField CLCNV;
  private XRiTextField CLCNP;
  private XRiTextField CLCNR;
  private XRiTextField CLDAT1;
  private JPanel OBJ_88;
  private XRiCheckBox CLIN17;
  private XRiCheckBox CLIN10;
  private JLabel OBJ_91;
  private XRiComboBox CLIN4;
  private JPanel OBJ_103;
  private JPanel OBJ_112;
  private JLabel OBJ_113;
  private JLabel OBJ_115;
  private JLabel OBJ_117;
  private JLabel OBJ_120;
  private XRiSpinner CLNEX1;
  private XRiSpinner CLNEX2;
  private XRiSpinner CLNEX3;
  private XRiSpinner CLIN21;
  private JPanel OBJ_121;
  private XRiCheckBox CLIN11;
  private XRiCheckBox CLIN8;
  private XRiCheckBox CLIN27;
  private XRiCheckBox CLIN28;
  private XRiCheckBox WTEDI;
  private XRiCheckBox CLIN20;
  private JLabel lbCCEREM;
  private XRiComboBox CLIN26;
  private JLabel OBJ_106;
  private XRiComboBox CLIN3;
  private JPanel OBJ_104;
  private JLabel OBJ_107;
  private JLabel OBJ_105;
  private XRiTextField CLLAN;
  private JCheckBox CLNLA_1_CHK;
  private JCheckBox CLNLA_2_CHK;
  private JCheckBox CLNLA_3_CHK;
  private JCheckBox CLNLA_4_CHK;
  private JPanel OBJ_114;
  private XRiCheckBox EBIN01;
  private XRiCheckBox EBIN02;
  private XRiCheckBox EBIN03;
  private XRiTextField MAIFAC;
  private JPanel OBJ_125;
  private JPanel OBJ_128;
  private RiZoneSortie LIBC1;
  private RiZoneSortie LIBC2;
  private RiZoneSortie LIBC3;
  private RiZoneSortie LIBFI;
  private JLabel OBJ_129;
  private JLabel OBJ_132;
  private JLabel OBJ_135;
  private JLabel OBJ_138;
  private XRiTextField CLCC1;
  private XRiTextField CLCC2;
  private XRiTextField CLCC3;
  private XRiTextField CLFIR;
  private JPanel OBJ_94;
  private JPanel OBJ_95;
  private XRiComboBox CLRBF;
  private XRiComboBox CLPFA;
  private XRiComboBox CLPRL;
  private XRiCheckBox CLIN12;
  private JLabel OBJ_96;
  private JLabel OBJ_98;
  private JLabel OBJ_100;
  private XRiComboBox CLIN19;
  private JLabel OBJ_99;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  private ButtonGroup EBIN09_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
