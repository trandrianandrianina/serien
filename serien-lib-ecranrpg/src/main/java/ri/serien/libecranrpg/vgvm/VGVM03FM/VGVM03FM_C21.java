
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_C21 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] CLIN20_Value = { "", "1", "2" };
  
  /**
   * Constructeur.
   */
  public VGVM03FM_C21(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    CLIN20.setValeurs(CLIN20_Value, null);
    CLIN3.setValeursSelection("1", " ");
    CLIN2.setValeursSelection("1", " ");
    CLIN1.setValeursSelection("1", " ");
    
    setCloseKey("ENTER", "F12");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZGLIB@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@KVLIB@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL16R@")).trim());
    OBJ_67.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRPKG@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL10R@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL11R@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL12R@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPC101@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPC111@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    OBJ_39.setVisible(CLCAN.isVisible());
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    boolean isModif = (!lexique.isTrue("53"));
    
    navig_valid.setVisible(isModif);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_44ActionPerformed() {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_32 = new JPanel();
    OBJ_35 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_42 = new JLabel();
    CLGEO = new XRiTextField();
    CLMEX = new XRiTextField();
    CLCAN = new XRiTextField();
    OBJ_44 = new JButton();
    OBJ_48 = new JPanel();
    OBJ_60 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_51 = new JLabel();
    ZPC16R = new XRiTextField();
    CLTRA = new XRiTextField();
    CLCRT = new XRiTextField();
    CLOTO = new XRiTextField();
    CLCTR = new XRiTextField();
    CLMAG = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_65 = new JLabel();
    CLFRP = new XRiTextField();
    OBJ_68 = new JLabel();
    CLIN14 = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_28 = new JPanel();
    CLIN20 = new XRiComboBox();
    CLIN1 = new XRiCheckBox();
    CLIN2 = new XRiCheckBox();
    label1 = new JLabel();
    CLIN3 = new XRiCheckBox();
    alcool = new JPanel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    ZPC10R = new XRiTextField();
    ZPC11R = new XRiTextField();
    ZPC12R = new XRiTextField();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(905, 605));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_32 ========
        {
          OBJ_32.setBorder(new TitledBorder("Divers"));
          OBJ_32.setOpaque(false);
          OBJ_32.setName("OBJ_32");
          OBJ_32.setLayout(null);

          //---- OBJ_35 ----
          OBJ_35.setText("@ZGLIB@");
          OBJ_35.setName("OBJ_35");
          OBJ_32.add(OBJ_35);
          OBJ_35.setBounds(235, 34, 232, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("@EXLIB@");
          OBJ_38.setName("OBJ_38");
          OBJ_32.add(OBJ_38);
          OBJ_38.setBounds(235, 59, 170, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("@KVLIB@");
          OBJ_41.setName("OBJ_41");
          OBJ_32.add(OBJ_41);
          OBJ_41.setBounds(235, 84, 232, 20);

          //---- OBJ_33 ----
          OBJ_33.setText("Zone g\u00e9ographique");
          OBJ_33.setName("OBJ_33");
          OBJ_32.add(OBJ_33);
          OBJ_33.setBounds(20, 34, 125, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Mode d'Exp\u00e9dition");
          OBJ_36.setName("OBJ_36");
          OBJ_32.add(OBJ_36);
          OBJ_36.setBounds(20, 59, 115, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Canal de vente");
          OBJ_39.setName("OBJ_39");
          OBJ_32.add(OBJ_39);
          OBJ_39.setBounds(20, 84, 95, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("G\u00e9n\u00e9ration d'achat");
          OBJ_42.setName("OBJ_42");
          OBJ_32.add(OBJ_42);
          OBJ_42.setBounds(450, 60, 130, 20);

          //---- CLGEO ----
          CLGEO.setName("CLGEO");
          OBJ_32.add(CLGEO);
          CLGEO.setBounds(165, 30, 60, CLGEO.getPreferredSize().height);

          //---- CLMEX ----
          CLMEX.setName("CLMEX");
          OBJ_32.add(CLMEX);
          CLMEX.setBounds(165, 55, 30, CLMEX.getPreferredSize().height);

          //---- CLCAN ----
          CLCAN.setName("CLCAN");
          OBJ_32.add(CLCAN);
          CLCAN.setBounds(165, 80, 40, CLCAN.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("");
          OBJ_44.setName("OBJ_44");
          OBJ_44.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_44ActionPerformed();
            }
          });
          OBJ_32.add(OBJ_44);
          OBJ_44.setBounds(570, 50, 40, 40);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_32.getComponentCount(); i++) {
              Rectangle bounds = OBJ_32.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_32.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_32.setMinimumSize(preferredSize);
            OBJ_32.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_32);
        OBJ_32.setBounds(15, 15, 705, 125);

        //======== OBJ_48 ========
        {
          OBJ_48.setBorder(new TitledBorder("Transport"));
          OBJ_48.setOpaque(false);
          OBJ_48.setName("OBJ_48");
          OBJ_48.setLayout(null);

          //---- OBJ_60 ----
          OBJ_60.setText("@MALIB@");
          OBJ_60.setName("OBJ_60");
          OBJ_48.add(OBJ_60);
          OBJ_60.setBounds(235, 109, 205, 20);

          //---- OBJ_55 ----
          OBJ_55.setText("@TRLIB@");
          OBJ_55.setName("OBJ_55");
          OBJ_48.add(OBJ_55);
          OBJ_55.setBounds(235, 59, 195, 20);

          //---- OBJ_61 ----
          OBJ_61.setText("Client transitaire");
          OBJ_61.setName("OBJ_61");
          OBJ_48.add(OBJ_61);
          OBJ_61.setBounds(20, 134, 120, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("Regroupement transport");
          OBJ_49.setName("OBJ_49");
          OBJ_48.add(OBJ_49);
          OBJ_49.setBounds(20, 34, 145, 20);

          //---- OBJ_53 ----
          OBJ_53.setText("Transporteur habituel");
          OBJ_53.setName("OBJ_53");
          OBJ_48.add(OBJ_53);
          OBJ_53.setBounds(20, 59, 130, 20);

          //---- OBJ_56 ----
          OBJ_56.setText("@ZPL16R@");
          OBJ_56.setName("OBJ_56");
          OBJ_48.add(OBJ_56);
          OBJ_56.setBounds(20, 84, 140, 20);

          //---- OBJ_58 ----
          OBJ_58.setText("Magasin");
          OBJ_58.setName("OBJ_58");
          OBJ_48.add(OBJ_58);
          OBJ_58.setBounds(20, 109, 90, 20);

          //---- OBJ_51 ----
          OBJ_51.setText("Ordre Tourn\u00e9e");
          OBJ_51.setName("OBJ_51");
          OBJ_48.add(OBJ_51);
          OBJ_51.setBounds(450, 34, 100, 20);

          //---- ZPC16R ----
          ZPC16R.setName("ZPC16R");
          OBJ_48.add(ZPC16R);
          ZPC16R.setBounds(165, 80, 45, ZPC16R.getPreferredSize().height);

          //---- CLTRA ----
          CLTRA.setName("CLTRA");
          OBJ_48.add(CLTRA);
          CLTRA.setBounds(165, 130, 60, CLTRA.getPreferredSize().height);

          //---- CLCRT ----
          CLCRT.setName("CLCRT");
          OBJ_48.add(CLCRT);
          CLCRT.setBounds(165, 30, 40, CLCRT.getPreferredSize().height);

          //---- CLOTO ----
          CLOTO.setName("CLOTO");
          OBJ_48.add(CLOTO);
          CLOTO.setBounds(620, 30, 52, CLOTO.getPreferredSize().height);

          //---- CLCTR ----
          CLCTR.setName("CLCTR");
          OBJ_48.add(CLCTR);
          CLCTR.setBounds(165, 55, 30, CLCTR.getPreferredSize().height);

          //---- CLMAG ----
          CLMAG.setName("CLMAG");
          OBJ_48.add(CLMAG);
          CLMAG.setBounds(165, 105, 30, CLMAG.getPreferredSize().height);

          //---- OBJ_76 ----
          OBJ_76.setText("Pr\u00e9 commande");
          OBJ_76.setName("OBJ_76");
          OBJ_48.add(OBJ_76);
          OBJ_76.setBounds(368, 353, 145, 20);

          //---- OBJ_65 ----
          OBJ_65.setText("Commande mini. pour Franco");
          OBJ_65.setName("OBJ_65");
          OBJ_48.add(OBJ_65);
          OBJ_65.setBounds(450, 59, 178, 20);

          //---- CLFRP ----
          CLFRP.setName("CLFRP");
          OBJ_48.add(CLFRP);
          CLFRP.setBounds(620, 55, 68, CLFRP.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Frais d'exp\u00e9dition");
          OBJ_68.setName("OBJ_68");
          OBJ_48.add(OBJ_68);
          OBJ_68.setBounds(450, 85, 117, 20);

          //---- CLIN14 ----
          CLIN14.setName("CLIN14");
          OBJ_48.add(CLIN14);
          CLIN14.setBounds(620, 80, 20, CLIN14.getPreferredSize().height);

          //---- OBJ_67 ----
          OBJ_67.setText("@FRPKG@");
          OBJ_67.setName("OBJ_67");
          OBJ_48.add(OBJ_67);
          OBJ_67.setBounds(450, 110, 151, 18);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_48.getComponentCount(); i++) {
              Rectangle bounds = OBJ_48.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_48.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_48.setMinimumSize(preferredSize);
            OBJ_48.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_48);
        OBJ_48.setBounds(15, 140, 705, 175);

        //======== OBJ_28 ========
        {
          OBJ_28.setBorder(new TitledBorder(""));
          OBJ_28.setOpaque(false);
          OBJ_28.setName("OBJ_28");
          OBJ_28.setLayout(null);

          //---- CLIN20 ----
          CLIN20.setModel(new DefaultComboBoxModel(new String[] {
            "Interdire les deux",
            "R\u00e9impression not\u00e9 interdit",
            "A paraitre not\u00e9 interdit",
            ""
          }));
          CLIN20.setName("CLIN20");
          OBJ_28.add(CLIN20);
          CLIN20.setBounds(130, 105, 201, CLIN20.getPreferredSize().height);

          //---- CLIN1 ----
          CLIN1.setText("Ce client n'accepte pas les livraisons partielles (la livraison est report\u00e9e jusqu'\u00e0 disponibilit\u00e9 de la commande)");
          CLIN1.setToolTipText("Ce client n'accepte pas les livraisons partielles (la livraison est report\u00e9e jusqu'\u00e0 disponibilit\u00e9 compl\u00e8te de la commande)");
          CLIN1.setName("CLIN1");
          OBJ_28.add(CLIN1);
          CLIN1.setBounds(20, 45, 670, 20);

          //---- CLIN2 ----
          CLIN2.setText("Ce client n'accepte pas de reliquat (les articles non disponibles seront annul\u00e9s de sa commande)");
          CLIN2.setToolTipText("Ce client n'accepte pas de reliquat (les articles non disponibles seront annul\u00e9s de sa commande)");
          CLIN2.setName("CLIN2");
          OBJ_28.add(CLIN2);
          CLIN2.setBounds(20, 15, 645, 20);

          //---- label1 ----
          label1.setText("Pr\u00e9 commande");
          label1.setName("label1");
          OBJ_28.add(label1);
          label1.setBounds(20, 110, 105, label1.getPreferredSize().height);

          //---- CLIN3 ----
          CLIN3.setText("Soumis au droit de pr\u00eat");
          CLIN3.setName("CLIN3");
          OBJ_28.add(CLIN3);
          CLIN3.setBounds(20, 75, 645, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_28.getComponentCount(); i++) {
              Rectangle bounds = OBJ_28.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_28.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_28.setMinimumSize(preferredSize);
            OBJ_28.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_28);
        OBJ_28.setBounds(15, 320, 705, 145);

        //======== alcool ========
        {
          alcool.setBorder(new TitledBorder("Alcool"));
          alcool.setOpaque(false);
          alcool.setName("alcool");
          alcool.setLayout(null);

          //---- label2 ----
          label2.setName("label2");
          alcool.add(label2);
          label2.setBounds(25, 35, 95, label2.getPreferredSize().height);

          //---- label3 ----
          label3.setName("label3");
          alcool.add(label3);
          label3.setBounds(25, 60, 95, label3.getPreferredSize().height);

          //---- label4 ----
          label4.setName("label4");
          alcool.add(label4);
          label4.setBounds(new Rectangle(new Point(25, 85), label4.getPreferredSize()));

          //---- ZPC10R ----
          ZPC10R.setName("ZPC10R");
          alcool.add(ZPC10R);
          ZPC10R.setBounds(165, 30, 20, ZPC10R.getPreferredSize().height);

          //---- ZPC11R ----
          ZPC11R.setName("ZPC11R");
          alcool.add(ZPC11R);
          ZPC11R.setBounds(165, 55, 20, ZPC11R.getPreferredSize().height);

          //---- ZPC12R ----
          ZPC12R.setName("ZPC12R");
          alcool.add(ZPC12R);
          ZPC12R.setBounds(165, 80, 160, ZPC12R.getPreferredSize().height);

          //---- label5 ----
          label5.setName("label5");
          alcool.add(label5);
          label5.setBounds(235, 36, 290, label5.getPreferredSize().height);

          //---- label6 ----
          label6.setName("label6");
          alcool.add(label6);
          label6.setBounds(235, 61, 290, label6.getPreferredSize().height);

          //---- label7 ----
          label7.setText("@ZPL10R@");
          label7.setName("label7");
          alcool.add(label7);
          label7.setBounds(10, 36, 145, label7.getPreferredSize().height);

          //---- label8 ----
          label8.setText("@ZPL11R@");
          label8.setName("label8");
          alcool.add(label8);
          label8.setBounds(10, 61, 145, 16);

          //---- label9 ----
          label9.setText("@ZPL12R@");
          label9.setName("label9");
          alcool.add(label9);
          label9.setBounds(10, 86, 145, 16);

          //---- label10 ----
          label10.setText("@ZPC101@");
          label10.setName("label10");
          alcool.add(label10);
          label10.setBounds(195, 35, 145, 16);

          //---- label11 ----
          label11.setText("@ZPC111@");
          label11.setName("label11");
          alcool.add(label11);
          label11.setBounds(195, 60, 145, 16);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < alcool.getComponentCount(); i++) {
              Rectangle bounds = alcool.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = alcool.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            alcool.setMinimumSize(preferredSize);
            alcool.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(alcool);
        alcool.setBounds(15, 470, 705, 125);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel OBJ_32;
  private JLabel OBJ_35;
  private JLabel OBJ_38;
  private JLabel OBJ_41;
  private JLabel OBJ_33;
  private JLabel OBJ_36;
  private JLabel OBJ_39;
  private JLabel OBJ_42;
  private XRiTextField CLGEO;
  private XRiTextField CLMEX;
  private XRiTextField CLCAN;
  private JButton OBJ_44;
  private JPanel OBJ_48;
  private JLabel OBJ_60;
  private JLabel OBJ_55;
  private JLabel OBJ_61;
  private JLabel OBJ_49;
  private JLabel OBJ_53;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private JLabel OBJ_51;
  private XRiTextField ZPC16R;
  private XRiTextField CLTRA;
  private XRiTextField CLCRT;
  private XRiTextField CLOTO;
  private XRiTextField CLCTR;
  private XRiTextField CLMAG;
  private JLabel OBJ_76;
  private JLabel OBJ_65;
  private XRiTextField CLFRP;
  private JLabel OBJ_68;
  private XRiTextField CLIN14;
  private JLabel OBJ_67;
  private JPanel OBJ_28;
  private XRiComboBox CLIN20;
  private XRiCheckBox CLIN1;
  private XRiCheckBox CLIN2;
  private JLabel label1;
  private XRiCheckBox CLIN3;
  private JPanel alcool;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField ZPC10R;
  private XRiTextField ZPC11R;
  private XRiTextField ZPC12R;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
