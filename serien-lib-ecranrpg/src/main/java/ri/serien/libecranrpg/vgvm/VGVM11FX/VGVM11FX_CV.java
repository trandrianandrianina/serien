
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_CV extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_CV(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    L1ED5.setValeursSelection("X", " ");
    L1ED4.setValeursSelection("X", " ");
    L1ED3.setValeursSelection("X", " ");
    L1ED1.setValeursSelection("X", " ");
    L1ED2.setValeursSelection("X", " ");
    
    setCloseKey("F4");
    
    // Titre
    setTitle("Variantes");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    L1ARTS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ARTS@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    WNLI.setVisible(lexique.isPresent("WNLI"));
    L1ART.setVisible(lexique.isPresent("L1ART"));
    L1ARTS.setVisible(lexique.isPresent("L1ARTS"));
    L1LIB4.setEnabled(lexique.isPresent("L1LIB4"));
    L1LIB3.setEnabled(lexique.isPresent("L1LIB3"));
    L1LIB2.setEnabled(lexique.isPresent("L1LIB2"));
    L1LIB1.setEnabled(lexique.isPresent("L1LIB1"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    L1LIB3 = new XRiTextField();
    L1LIB4 = new XRiTextField();
    L1ARTS = new RiZoneSortie();
    L1ART = new XRiTextField();
    OBJ_22 = new JLabel();
    OBJ_16 = new JLabel();
    WNLI = new RiZoneSortie();
    WCOD = new RiZoneSortie();
    OBJ_20 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_28 = new JLabel();
    panel2 = new JPanel();
    L1ED2 = new XRiCheckBox();
    L1ED1 = new XRiCheckBox();
    L1ED3 = new XRiCheckBox();
    L1ED4 = new XRiCheckBox();
    L1ED5 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(845, 355));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setName("L1LIB1");
          panel1.add(L1LIB1);
          L1LIB1.setBounds(330, 35, 310, L1LIB1.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setName("L1LIB2");
          panel1.add(L1LIB2);
          L1LIB2.setBounds(330, 65, 310, L1LIB2.getPreferredSize().height);

          //---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setName("L1LIB3");
          panel1.add(L1LIB3);
          L1LIB3.setBounds(330, 95, 310, L1LIB3.getPreferredSize().height);

          //---- L1LIB4 ----
          L1LIB4.setComponentPopupMenu(BTD);
          L1LIB4.setName("L1LIB4");
          panel1.add(L1LIB4);
          L1LIB4.setBounds(330, 125, 310, L1LIB4.getPreferredSize().height);

          //---- L1ARTS ----
          L1ARTS.setText("@L1ARTS@");
          L1ARTS.setName("L1ARTS");
          panel1.add(L1ARTS);
          L1ARTS.setBounds(85, 37, 210, L1ARTS.getPreferredSize().height);

          //---- L1ART ----
          L1ART.setName("L1ART");
          panel1.add(L1ART);
          L1ART.setBounds(85, 95, 210, L1ART.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("Article");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(90, 69, 170, 20);

          //---- OBJ_16 ----
          OBJ_16.setText("C   N\u00b0Li");
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(20, 15, 45, 20);

          //---- WNLI ----
          WNLI.setText("@WNLI@");
          WNLI.setName("WNLI");
          panel1.add(WNLI);
          WNLI.setBounds(35, 37, 50, WNLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setText("@WCOD@");
          WCOD.setName("WCOD");
          panel1.add(WCOD);
          WCOD.setBounds(15, 37, 20, WCOD.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("1");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(310, 39, 20, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("2");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(310, 69, 20, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("3");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(310, 99, 20, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("4");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(310, 129, 20, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("\u00e0 \u00e9diter sur"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- L1ED2 ----
          L1ED2.setText("Accus\u00e9s de r\u00e9ception de commande");
          L1ED2.setComponentPopupMenu(BTD);
          L1ED2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED2.setName("L1ED2");
          panel2.add(L1ED2);
          L1ED2.setBounds(190, 40, 253, 20);

          //---- L1ED1 ----
          L1ED1.setText("Bons de pr\u00e9paration");
          L1ED1.setComponentPopupMenu(BTD);
          L1ED1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED1.setName("L1ED1");
          panel2.add(L1ED1);
          L1ED1.setBounds(20, 40, 148, 20);

          //---- L1ED3 ----
          L1ED3.setText("Bons d'exp\u00e9dition");
          L1ED3.setComponentPopupMenu(BTD);
          L1ED3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED3.setName("L1ED3");
          panel2.add(L1ED3);
          L1ED3.setBounds(20, 70, 148, 20);

          //---- L1ED4 ----
          L1ED4.setText("Bons transporteurs");
          L1ED4.setComponentPopupMenu(BTD);
          L1ED4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED4.setName("L1ED4");
          panel2.add(L1ED4);
          L1ED4.setBounds(190, 70, 148, 20);

          //---- L1ED5 ----
          L1ED5.setText("Factures");
          L1ED5.setComponentPopupMenu(BTD);
          L1ED5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1ED5.setName("L1ED5");
          panel2.add(L1ED5);
          L1ED5.setBounds(355, 70, 78, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 655, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 655, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private XRiTextField L1LIB3;
  private XRiTextField L1LIB4;
  private RiZoneSortie L1ARTS;
  private XRiTextField L1ART;
  private JLabel OBJ_22;
  private JLabel OBJ_16;
  private RiZoneSortie WNLI;
  private RiZoneSortie WCOD;
  private JLabel OBJ_20;
  private JLabel OBJ_23;
  private JLabel OBJ_26;
  private JLabel OBJ_28;
  private JPanel panel2;
  private XRiCheckBox L1ED2;
  private XRiCheckBox L1ED1;
  private XRiCheckBox L1ED3;
  private XRiCheckBox L1ED4;
  private XRiCheckBox L1ED5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
