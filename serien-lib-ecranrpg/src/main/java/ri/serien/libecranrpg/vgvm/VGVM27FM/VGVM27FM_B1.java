
package ri.serien.libecranrpg.vgvm.VGVM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM27FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM27FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ERR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR1@")).trim());
    ERR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR2@")).trim());
    ERR3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR3@")).trim());
    ERR4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR4@")).trim());
    ERR5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR5@")).trim());
    ERR6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR6@")).trim());
    ERR7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR7@")).trim());
    ERR8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR8@")).trim());
    ERR9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR9@")).trim());
    ERR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR10@")).trim());
    ERR11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR11@")).trim());
    ERR12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR12@")).trim());
    ERR13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR13@")).trim());
    ERR14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR14@")).trim());
    ERR15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    // WFIN2X.setEnabled( lexique.isPresent("WFIN2X"));
    // WDEB2X.setEnabled( lexique.isPresent("WDEB2X"));
    // WFIN1X.setEnabled( lexique.isPresent("WFIN1X"));
    // WDEB1X.setEnabled( lexique.isPresent("WDEB1X"));
    BRART2.setEnabled(lexique.isPresent("BRART2"));
    BRLIB.setEnabled(lexique.isPresent("BRLIB"));
    if (lexique.isTrue("92")) {
      p_bpresentation.setText("Barèmes de bonifications");
    }
    else {
      p_bpresentation.setText("Barèmes de remises");
    }
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @WTYP@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    WETB = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    OBJ_50 = new JLabel();
    WCNV = new XRiTextField();
    BRLIB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_47 = new JLabel();
    BRART2 = new XRiTextField();
    panel4 = new JPanel();
    panel2 = new JPanel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    WDEB1X = new XRiCalendrier();
    WFIN1X = new XRiCalendrier();
    R1011 = new XRiTextField();
    R1021 = new XRiTextField();
    R1031 = new XRiTextField();
    R1041 = new XRiTextField();
    R1051 = new XRiTextField();
    R1061 = new XRiTextField();
    R1071 = new XRiTextField();
    R1081 = new XRiTextField();
    R1091 = new XRiTextField();
    R1101 = new XRiTextField();
    R1111 = new XRiTextField();
    R1121 = new XRiTextField();
    R1131 = new XRiTextField();
    R1141 = new XRiTextField();
    R11511 = new XRiTextField();
    R1012 = new XRiTextField();
    R1022 = new XRiTextField();
    R1032 = new XRiTextField();
    R1042 = new XRiTextField();
    R1052 = new XRiTextField();
    R1062 = new XRiTextField();
    R1072 = new XRiTextField();
    R1082 = new XRiTextField();
    R1092 = new XRiTextField();
    R1102 = new XRiTextField();
    R1112 = new XRiTextField();
    R1122 = new XRiTextField();
    R1132 = new XRiTextField();
    R1142 = new XRiTextField();
    R1152 = new XRiTextField();
    R1013 = new XRiTextField();
    R1023 = new XRiTextField();
    R1033 = new XRiTextField();
    R1043 = new XRiTextField();
    R1053 = new XRiTextField();
    R1063 = new XRiTextField();
    R1073 = new XRiTextField();
    R1083 = new XRiTextField();
    R1093 = new XRiTextField();
    R1103 = new XRiTextField();
    R1113 = new XRiTextField();
    R1123 = new XRiTextField();
    R1133 = new XRiTextField();
    R1143 = new XRiTextField();
    R1153 = new XRiTextField();
    R1014 = new XRiTextField();
    R1024 = new XRiTextField();
    R1034 = new XRiTextField();
    R1044 = new XRiTextField();
    R1054 = new XRiTextField();
    R1064 = new XRiTextField();
    R1074 = new XRiTextField();
    R1084 = new XRiTextField();
    R1094 = new XRiTextField();
    R1104 = new XRiTextField();
    R1114 = new XRiTextField();
    R1124 = new XRiTextField();
    R1134 = new XRiTextField();
    R1144 = new XRiTextField();
    R1154 = new XRiTextField();
    separator2 = compFactory.createSeparator("Remise", SwingConstants.CENTER);
    panel3 = new JPanel();
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    WDEB2X = new XRiCalendrier();
    WFIN2X = new XRiCalendrier();
    R2011 = new XRiTextField();
    R2021 = new XRiTextField();
    R2031 = new XRiTextField();
    R2041 = new XRiTextField();
    R2051 = new XRiTextField();
    R2061 = new XRiTextField();
    R2071 = new XRiTextField();
    R2081 = new XRiTextField();
    R2091 = new XRiTextField();
    R2101 = new XRiTextField();
    R2111 = new XRiTextField();
    R2121 = new XRiTextField();
    R2131 = new XRiTextField();
    R2141 = new XRiTextField();
    R2151 = new XRiTextField();
    R2012 = new XRiTextField();
    R2022 = new XRiTextField();
    R2032 = new XRiTextField();
    R2042 = new XRiTextField();
    R2052 = new XRiTextField();
    R2062 = new XRiTextField();
    R2072 = new XRiTextField();
    R2082 = new XRiTextField();
    R2092 = new XRiTextField();
    R2102 = new XRiTextField();
    R2112 = new XRiTextField();
    R2122 = new XRiTextField();
    R2132 = new XRiTextField();
    R2142 = new XRiTextField();
    R2152 = new XRiTextField();
    R2013 = new XRiTextField();
    R2023 = new XRiTextField();
    R2033 = new XRiTextField();
    R2043 = new XRiTextField();
    R2053 = new XRiTextField();
    R2063 = new XRiTextField();
    R2073 = new XRiTextField();
    R2083 = new XRiTextField();
    R2093 = new XRiTextField();
    R2103 = new XRiTextField();
    R2113 = new XRiTextField();
    R2123 = new XRiTextField();
    R2133 = new XRiTextField();
    R2143 = new XRiTextField();
    R2153 = new XRiTextField();
    R2014 = new XRiTextField();
    R2024 = new XRiTextField();
    R2034 = new XRiTextField();
    R2044 = new XRiTextField();
    R2054 = new XRiTextField();
    R2064 = new XRiTextField();
    R2074 = new XRiTextField();
    R2084 = new XRiTextField();
    R2094 = new XRiTextField();
    R2104 = new XRiTextField();
    R2114 = new XRiTextField();
    R2124 = new XRiTextField();
    R2134 = new XRiTextField();
    R2144 = new XRiTextField();
    R2154 = new XRiTextField();
    separator3 = compFactory.createSeparator("Remise", SwingConstants.CENTER);
    Q101 = new XRiTextField();
    Q102 = new XRiTextField();
    Q103 = new XRiTextField();
    Q104 = new XRiTextField();
    Q105 = new XRiTextField();
    Q106 = new XRiTextField();
    Q107 = new XRiTextField();
    Q108 = new XRiTextField();
    Q109 = new XRiTextField();
    Q110 = new XRiTextField();
    Q111 = new XRiTextField();
    Q112 = new XRiTextField();
    Q113 = new XRiTextField();
    Q114 = new XRiTextField();
    Q115 = new XRiTextField();
    ERR1 = new JLabel();
    ERR2 = new JLabel();
    ERR3 = new JLabel();
    ERR4 = new JLabel();
    ERR5 = new JLabel();
    ERR6 = new JLabel();
    ERR7 = new JLabel();
    ERR8 = new JLabel();
    ERR9 = new JLabel();
    ERR10 = new JLabel();
    ERR11 = new JLabel();
    ERR12 = new JLabel();
    ERR13 = new JLabel();
    ERR14 = new JLabel();
    ERR15 = new JLabel();
    separator1 = compFactory.createSeparator("Montant mini");
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bar\u00e8mes de remises");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");
          p_tete_gauche.add(OBJ_48);
          OBJ_48.setBounds(5, 4, 100, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(110, 0, 40, WETB.getPreferredSize().height);

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });
          p_tete_gauche.add(BT_ChgSoc);
          BT_ChgSoc.setBounds(155, 0, 32, 29);

          //---- OBJ_50 ----
          OBJ_50.setText("Code");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(215, 4, 36, 20);

          //---- WCNV ----
          WCNV.setComponentPopupMenu(null);
          WCNV.setName("WCNV");
          p_tete_gauche.add(WCNV);
          WCNV.setBounds(255, 0, 60, WCNV.getPreferredSize().height);

          //---- BRLIB ----
          BRLIB.setComponentPopupMenu(null);
          BRLIB.setName("BRLIB");
          p_tete_gauche.add(BRLIB);
          BRLIB.setBounds(320, 0, 310, BRLIB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Conditions type cadeau");
              riSousMenu_bt6.setToolTipText("Saisie conditions de type cadeau");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 700));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_47 ----
            OBJ_47.setText("Article support");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(15, 19, 108, 20);

            //---- BRART2 ----
            BRART2.setComponentPopupMenu(BTD2);
            BRART2.setName("BRART2");
            panel1.add(BRART2);
            BRART2.setBounds(170, 15, 270, BRART2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("P\u00e9riode n\u00b01"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_52 ----
              OBJ_52.setText("du");
              OBJ_52.setName("OBJ_52");
              panel2.add(OBJ_52);
              OBJ_52.setBounds(10, 34, 18, 20);

              //---- OBJ_54 ----
              OBJ_54.setText("au");
              OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_54.setName("OBJ_54");
              panel2.add(OBJ_54);
              OBJ_54.setBounds(170, 34, 55, 20);

              //---- WDEB1X ----
              WDEB1X.setName("WDEB1X");
              panel2.add(WDEB1X);
              WDEB1X.setBounds(65, 30, 105, WDEB1X.getPreferredSize().height);

              //---- WFIN1X ----
              WFIN1X.setName("WFIN1X");
              panel2.add(WFIN1X);
              WFIN1X.setBounds(225, 30, 105, WFIN1X.getPreferredSize().height);

              //---- R1011 ----
              R1011.setHorizontalAlignment(SwingConstants.RIGHT);
              R1011.setName("R1011");
              panel2.add(R1011);
              R1011.setBounds(10, 80, 80, R1011.getPreferredSize().height);

              //---- R1021 ----
              R1021.setHorizontalAlignment(SwingConstants.RIGHT);
              R1021.setName("R1021");
              panel2.add(R1021);
              R1021.setBounds(10, 105, 80, R1021.getPreferredSize().height);

              //---- R1031 ----
              R1031.setHorizontalAlignment(SwingConstants.RIGHT);
              R1031.setName("R1031");
              panel2.add(R1031);
              R1031.setBounds(10, 130, 80, R1031.getPreferredSize().height);

              //---- R1041 ----
              R1041.setHorizontalAlignment(SwingConstants.RIGHT);
              R1041.setName("R1041");
              panel2.add(R1041);
              R1041.setBounds(10, 155, 80, R1041.getPreferredSize().height);

              //---- R1051 ----
              R1051.setHorizontalAlignment(SwingConstants.RIGHT);
              R1051.setName("R1051");
              panel2.add(R1051);
              R1051.setBounds(10, 180, 80, R1051.getPreferredSize().height);

              //---- R1061 ----
              R1061.setHorizontalAlignment(SwingConstants.RIGHT);
              R1061.setName("R1061");
              panel2.add(R1061);
              R1061.setBounds(10, 205, 80, R1061.getPreferredSize().height);

              //---- R1071 ----
              R1071.setHorizontalAlignment(SwingConstants.RIGHT);
              R1071.setName("R1071");
              panel2.add(R1071);
              R1071.setBounds(10, 230, 80, R1071.getPreferredSize().height);

              //---- R1081 ----
              R1081.setHorizontalAlignment(SwingConstants.RIGHT);
              R1081.setName("R1081");
              panel2.add(R1081);
              R1081.setBounds(10, 255, 80, R1081.getPreferredSize().height);

              //---- R1091 ----
              R1091.setHorizontalAlignment(SwingConstants.RIGHT);
              R1091.setName("R1091");
              panel2.add(R1091);
              R1091.setBounds(10, 280, 80, R1091.getPreferredSize().height);

              //---- R1101 ----
              R1101.setHorizontalAlignment(SwingConstants.RIGHT);
              R1101.setName("R1101");
              panel2.add(R1101);
              R1101.setBounds(10, 305, 80, R1101.getPreferredSize().height);

              //---- R1111 ----
              R1111.setHorizontalAlignment(SwingConstants.RIGHT);
              R1111.setName("R1111");
              panel2.add(R1111);
              R1111.setBounds(10, 330, 80, R1111.getPreferredSize().height);

              //---- R1121 ----
              R1121.setHorizontalAlignment(SwingConstants.RIGHT);
              R1121.setName("R1121");
              panel2.add(R1121);
              R1121.setBounds(10, 355, 80, R1121.getPreferredSize().height);

              //---- R1131 ----
              R1131.setHorizontalAlignment(SwingConstants.RIGHT);
              R1131.setName("R1131");
              panel2.add(R1131);
              R1131.setBounds(10, 380, 80, R1131.getPreferredSize().height);

              //---- R1141 ----
              R1141.setHorizontalAlignment(SwingConstants.RIGHT);
              R1141.setName("R1141");
              panel2.add(R1141);
              R1141.setBounds(10, 405, 80, R1141.getPreferredSize().height);

              //---- R11511 ----
              R11511.setHorizontalAlignment(SwingConstants.RIGHT);
              R11511.setName("R11511");
              panel2.add(R11511);
              R11511.setBounds(10, 430, 80, R11511.getPreferredSize().height);

              //---- R1012 ----
              R1012.setHorizontalAlignment(SwingConstants.RIGHT);
              R1012.setName("R1012");
              panel2.add(R1012);
              R1012.setBounds(90, 80, 80, R1012.getPreferredSize().height);

              //---- R1022 ----
              R1022.setHorizontalAlignment(SwingConstants.RIGHT);
              R1022.setName("R1022");
              panel2.add(R1022);
              R1022.setBounds(90, 105, 80, R1022.getPreferredSize().height);

              //---- R1032 ----
              R1032.setHorizontalAlignment(SwingConstants.RIGHT);
              R1032.setName("R1032");
              panel2.add(R1032);
              R1032.setBounds(90, 130, 80, R1032.getPreferredSize().height);

              //---- R1042 ----
              R1042.setHorizontalAlignment(SwingConstants.RIGHT);
              R1042.setName("R1042");
              panel2.add(R1042);
              R1042.setBounds(90, 155, 80, R1042.getPreferredSize().height);

              //---- R1052 ----
              R1052.setHorizontalAlignment(SwingConstants.RIGHT);
              R1052.setName("R1052");
              panel2.add(R1052);
              R1052.setBounds(90, 180, 80, R1052.getPreferredSize().height);

              //---- R1062 ----
              R1062.setHorizontalAlignment(SwingConstants.RIGHT);
              R1062.setName("R1062");
              panel2.add(R1062);
              R1062.setBounds(90, 205, 80, R1062.getPreferredSize().height);

              //---- R1072 ----
              R1072.setHorizontalAlignment(SwingConstants.RIGHT);
              R1072.setName("R1072");
              panel2.add(R1072);
              R1072.setBounds(90, 230, 80, R1072.getPreferredSize().height);

              //---- R1082 ----
              R1082.setHorizontalAlignment(SwingConstants.RIGHT);
              R1082.setName("R1082");
              panel2.add(R1082);
              R1082.setBounds(90, 255, 80, R1082.getPreferredSize().height);

              //---- R1092 ----
              R1092.setHorizontalAlignment(SwingConstants.RIGHT);
              R1092.setName("R1092");
              panel2.add(R1092);
              R1092.setBounds(90, 280, 80, R1092.getPreferredSize().height);

              //---- R1102 ----
              R1102.setHorizontalAlignment(SwingConstants.RIGHT);
              R1102.setName("R1102");
              panel2.add(R1102);
              R1102.setBounds(90, 305, 80, R1102.getPreferredSize().height);

              //---- R1112 ----
              R1112.setHorizontalAlignment(SwingConstants.RIGHT);
              R1112.setName("R1112");
              panel2.add(R1112);
              R1112.setBounds(90, 330, 80, R1112.getPreferredSize().height);

              //---- R1122 ----
              R1122.setHorizontalAlignment(SwingConstants.RIGHT);
              R1122.setName("R1122");
              panel2.add(R1122);
              R1122.setBounds(90, 355, 80, R1122.getPreferredSize().height);

              //---- R1132 ----
              R1132.setHorizontalAlignment(SwingConstants.RIGHT);
              R1132.setName("R1132");
              panel2.add(R1132);
              R1132.setBounds(90, 380, 80, R1132.getPreferredSize().height);

              //---- R1142 ----
              R1142.setHorizontalAlignment(SwingConstants.RIGHT);
              R1142.setName("R1142");
              panel2.add(R1142);
              R1142.setBounds(90, 405, 80, R1142.getPreferredSize().height);

              //---- R1152 ----
              R1152.setHorizontalAlignment(SwingConstants.RIGHT);
              R1152.setName("R1152");
              panel2.add(R1152);
              R1152.setBounds(90, 430, 80, R1152.getPreferredSize().height);

              //---- R1013 ----
              R1013.setHorizontalAlignment(SwingConstants.RIGHT);
              R1013.setName("R1013");
              panel2.add(R1013);
              R1013.setBounds(170, 80, 80, R1013.getPreferredSize().height);

              //---- R1023 ----
              R1023.setHorizontalAlignment(SwingConstants.RIGHT);
              R1023.setName("R1023");
              panel2.add(R1023);
              R1023.setBounds(170, 105, 80, R1023.getPreferredSize().height);

              //---- R1033 ----
              R1033.setHorizontalAlignment(SwingConstants.RIGHT);
              R1033.setName("R1033");
              panel2.add(R1033);
              R1033.setBounds(170, 130, 80, R1033.getPreferredSize().height);

              //---- R1043 ----
              R1043.setHorizontalAlignment(SwingConstants.RIGHT);
              R1043.setName("R1043");
              panel2.add(R1043);
              R1043.setBounds(170, 155, 80, R1043.getPreferredSize().height);

              //---- R1053 ----
              R1053.setHorizontalAlignment(SwingConstants.RIGHT);
              R1053.setName("R1053");
              panel2.add(R1053);
              R1053.setBounds(170, 180, 80, R1053.getPreferredSize().height);

              //---- R1063 ----
              R1063.setHorizontalAlignment(SwingConstants.RIGHT);
              R1063.setName("R1063");
              panel2.add(R1063);
              R1063.setBounds(170, 205, 80, R1063.getPreferredSize().height);

              //---- R1073 ----
              R1073.setHorizontalAlignment(SwingConstants.RIGHT);
              R1073.setName("R1073");
              panel2.add(R1073);
              R1073.setBounds(170, 230, 80, R1073.getPreferredSize().height);

              //---- R1083 ----
              R1083.setHorizontalAlignment(SwingConstants.RIGHT);
              R1083.setName("R1083");
              panel2.add(R1083);
              R1083.setBounds(170, 255, 80, R1083.getPreferredSize().height);

              //---- R1093 ----
              R1093.setHorizontalAlignment(SwingConstants.RIGHT);
              R1093.setName("R1093");
              panel2.add(R1093);
              R1093.setBounds(170, 280, 80, R1093.getPreferredSize().height);

              //---- R1103 ----
              R1103.setHorizontalAlignment(SwingConstants.RIGHT);
              R1103.setName("R1103");
              panel2.add(R1103);
              R1103.setBounds(170, 305, 80, R1103.getPreferredSize().height);

              //---- R1113 ----
              R1113.setHorizontalAlignment(SwingConstants.RIGHT);
              R1113.setName("R1113");
              panel2.add(R1113);
              R1113.setBounds(170, 330, 80, R1113.getPreferredSize().height);

              //---- R1123 ----
              R1123.setHorizontalAlignment(SwingConstants.RIGHT);
              R1123.setName("R1123");
              panel2.add(R1123);
              R1123.setBounds(170, 355, 80, R1123.getPreferredSize().height);

              //---- R1133 ----
              R1133.setHorizontalAlignment(SwingConstants.RIGHT);
              R1133.setName("R1133");
              panel2.add(R1133);
              R1133.setBounds(170, 380, 80, R1133.getPreferredSize().height);

              //---- R1143 ----
              R1143.setHorizontalAlignment(SwingConstants.RIGHT);
              R1143.setName("R1143");
              panel2.add(R1143);
              R1143.setBounds(170, 405, 80, R1143.getPreferredSize().height);

              //---- R1153 ----
              R1153.setHorizontalAlignment(SwingConstants.RIGHT);
              R1153.setName("R1153");
              panel2.add(R1153);
              R1153.setBounds(170, 430, 80, R1153.getPreferredSize().height);

              //---- R1014 ----
              R1014.setHorizontalAlignment(SwingConstants.RIGHT);
              R1014.setName("R1014");
              panel2.add(R1014);
              R1014.setBounds(250, 80, 80, R1014.getPreferredSize().height);

              //---- R1024 ----
              R1024.setHorizontalAlignment(SwingConstants.RIGHT);
              R1024.setName("R1024");
              panel2.add(R1024);
              R1024.setBounds(250, 105, 80, R1024.getPreferredSize().height);

              //---- R1034 ----
              R1034.setHorizontalAlignment(SwingConstants.RIGHT);
              R1034.setName("R1034");
              panel2.add(R1034);
              R1034.setBounds(250, 130, 80, R1034.getPreferredSize().height);

              //---- R1044 ----
              R1044.setHorizontalAlignment(SwingConstants.RIGHT);
              R1044.setName("R1044");
              panel2.add(R1044);
              R1044.setBounds(250, 155, 80, R1044.getPreferredSize().height);

              //---- R1054 ----
              R1054.setHorizontalAlignment(SwingConstants.RIGHT);
              R1054.setName("R1054");
              panel2.add(R1054);
              R1054.setBounds(250, 180, 80, R1054.getPreferredSize().height);

              //---- R1064 ----
              R1064.setHorizontalAlignment(SwingConstants.RIGHT);
              R1064.setName("R1064");
              panel2.add(R1064);
              R1064.setBounds(250, 205, 80, R1064.getPreferredSize().height);

              //---- R1074 ----
              R1074.setHorizontalAlignment(SwingConstants.RIGHT);
              R1074.setName("R1074");
              panel2.add(R1074);
              R1074.setBounds(250, 230, 80, R1074.getPreferredSize().height);

              //---- R1084 ----
              R1084.setHorizontalAlignment(SwingConstants.RIGHT);
              R1084.setName("R1084");
              panel2.add(R1084);
              R1084.setBounds(250, 255, 80, R1084.getPreferredSize().height);

              //---- R1094 ----
              R1094.setHorizontalAlignment(SwingConstants.RIGHT);
              R1094.setName("R1094");
              panel2.add(R1094);
              R1094.setBounds(250, 280, 80, R1094.getPreferredSize().height);

              //---- R1104 ----
              R1104.setHorizontalAlignment(SwingConstants.RIGHT);
              R1104.setName("R1104");
              panel2.add(R1104);
              R1104.setBounds(250, 305, 80, R1104.getPreferredSize().height);

              //---- R1114 ----
              R1114.setHorizontalAlignment(SwingConstants.RIGHT);
              R1114.setName("R1114");
              panel2.add(R1114);
              R1114.setBounds(250, 330, 80, R1114.getPreferredSize().height);

              //---- R1124 ----
              R1124.setHorizontalAlignment(SwingConstants.RIGHT);
              R1124.setName("R1124");
              panel2.add(R1124);
              R1124.setBounds(250, 355, 80, R1124.getPreferredSize().height);

              //---- R1134 ----
              R1134.setHorizontalAlignment(SwingConstants.RIGHT);
              R1134.setName("R1134");
              panel2.add(R1134);
              R1134.setBounds(250, 380, 80, R1134.getPreferredSize().height);

              //---- R1144 ----
              R1144.setHorizontalAlignment(SwingConstants.RIGHT);
              R1144.setName("R1144");
              panel2.add(R1144);
              R1144.setBounds(250, 405, 80, R1144.getPreferredSize().height);

              //---- R1154 ----
              R1154.setHorizontalAlignment(SwingConstants.RIGHT);
              R1154.setName("R1154");
              panel2.add(R1154);
              R1154.setBounds(250, 430, 80, R1154.getPreferredSize().height);

              //---- separator2 ----
              separator2.setName("separator2");
              panel2.add(separator2);
              separator2.setBounds(10, 65, 320, 15);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel2);
            panel2.setBounds(105, 10, 340, 470);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("P\u00e9riode n\u00b02"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_56 ----
              OBJ_56.setText("du");
              OBJ_56.setName("OBJ_56");
              panel3.add(OBJ_56);
              OBJ_56.setBounds(10, 34, 18, 20);

              //---- OBJ_58 ----
              OBJ_58.setText("au");
              OBJ_58.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_58.setName("OBJ_58");
              panel3.add(OBJ_58);
              OBJ_58.setBounds(170, 34, 53, 20);

              //---- WDEB2X ----
              WDEB2X.setName("WDEB2X");
              panel3.add(WDEB2X);
              WDEB2X.setBounds(65, 30, 105, WDEB2X.getPreferredSize().height);

              //---- WFIN2X ----
              WFIN2X.setName("WFIN2X");
              panel3.add(WFIN2X);
              WFIN2X.setBounds(225, 30, 105, WFIN2X.getPreferredSize().height);

              //---- R2011 ----
              R2011.setHorizontalAlignment(SwingConstants.RIGHT);
              R2011.setName("R2011");
              panel3.add(R2011);
              R2011.setBounds(10, 80, 80, R2011.getPreferredSize().height);

              //---- R2021 ----
              R2021.setHorizontalAlignment(SwingConstants.RIGHT);
              R2021.setName("R2021");
              panel3.add(R2021);
              R2021.setBounds(10, 105, 80, R2021.getPreferredSize().height);

              //---- R2031 ----
              R2031.setHorizontalAlignment(SwingConstants.RIGHT);
              R2031.setName("R2031");
              panel3.add(R2031);
              R2031.setBounds(10, 130, 80, R2031.getPreferredSize().height);

              //---- R2041 ----
              R2041.setHorizontalAlignment(SwingConstants.RIGHT);
              R2041.setName("R2041");
              panel3.add(R2041);
              R2041.setBounds(10, 155, 80, R2041.getPreferredSize().height);

              //---- R2051 ----
              R2051.setHorizontalAlignment(SwingConstants.RIGHT);
              R2051.setName("R2051");
              panel3.add(R2051);
              R2051.setBounds(10, 180, 80, R2051.getPreferredSize().height);

              //---- R2061 ----
              R2061.setHorizontalAlignment(SwingConstants.RIGHT);
              R2061.setName("R2061");
              panel3.add(R2061);
              R2061.setBounds(10, 205, 80, R2061.getPreferredSize().height);

              //---- R2071 ----
              R2071.setHorizontalAlignment(SwingConstants.RIGHT);
              R2071.setName("R2071");
              panel3.add(R2071);
              R2071.setBounds(10, 230, 80, R2071.getPreferredSize().height);

              //---- R2081 ----
              R2081.setHorizontalAlignment(SwingConstants.RIGHT);
              R2081.setName("R2081");
              panel3.add(R2081);
              R2081.setBounds(10, 255, 80, R2081.getPreferredSize().height);

              //---- R2091 ----
              R2091.setHorizontalAlignment(SwingConstants.RIGHT);
              R2091.setName("R2091");
              panel3.add(R2091);
              R2091.setBounds(10, 280, 80, R2091.getPreferredSize().height);

              //---- R2101 ----
              R2101.setHorizontalAlignment(SwingConstants.RIGHT);
              R2101.setName("R2101");
              panel3.add(R2101);
              R2101.setBounds(10, 305, 80, R2101.getPreferredSize().height);

              //---- R2111 ----
              R2111.setHorizontalAlignment(SwingConstants.RIGHT);
              R2111.setName("R2111");
              panel3.add(R2111);
              R2111.setBounds(10, 330, 80, R2111.getPreferredSize().height);

              //---- R2121 ----
              R2121.setHorizontalAlignment(SwingConstants.RIGHT);
              R2121.setName("R2121");
              panel3.add(R2121);
              R2121.setBounds(10, 355, 80, R2121.getPreferredSize().height);

              //---- R2131 ----
              R2131.setHorizontalAlignment(SwingConstants.RIGHT);
              R2131.setName("R2131");
              panel3.add(R2131);
              R2131.setBounds(10, 380, 80, R2131.getPreferredSize().height);

              //---- R2141 ----
              R2141.setHorizontalAlignment(SwingConstants.RIGHT);
              R2141.setName("R2141");
              panel3.add(R2141);
              R2141.setBounds(10, 405, 80, R2141.getPreferredSize().height);

              //---- R2151 ----
              R2151.setHorizontalAlignment(SwingConstants.RIGHT);
              R2151.setName("R2151");
              panel3.add(R2151);
              R2151.setBounds(10, 430, 80, R2151.getPreferredSize().height);

              //---- R2012 ----
              R2012.setHorizontalAlignment(SwingConstants.RIGHT);
              R2012.setName("R2012");
              panel3.add(R2012);
              R2012.setBounds(90, 80, 80, R2012.getPreferredSize().height);

              //---- R2022 ----
              R2022.setHorizontalAlignment(SwingConstants.RIGHT);
              R2022.setName("R2022");
              panel3.add(R2022);
              R2022.setBounds(90, 105, 80, R2022.getPreferredSize().height);

              //---- R2032 ----
              R2032.setHorizontalAlignment(SwingConstants.RIGHT);
              R2032.setName("R2032");
              panel3.add(R2032);
              R2032.setBounds(90, 130, 80, R2032.getPreferredSize().height);

              //---- R2042 ----
              R2042.setHorizontalAlignment(SwingConstants.RIGHT);
              R2042.setName("R2042");
              panel3.add(R2042);
              R2042.setBounds(90, 155, 80, R2042.getPreferredSize().height);

              //---- R2052 ----
              R2052.setHorizontalAlignment(SwingConstants.RIGHT);
              R2052.setName("R2052");
              panel3.add(R2052);
              R2052.setBounds(90, 180, 80, R2052.getPreferredSize().height);

              //---- R2062 ----
              R2062.setHorizontalAlignment(SwingConstants.RIGHT);
              R2062.setName("R2062");
              panel3.add(R2062);
              R2062.setBounds(90, 205, 80, R2062.getPreferredSize().height);

              //---- R2072 ----
              R2072.setHorizontalAlignment(SwingConstants.RIGHT);
              R2072.setName("R2072");
              panel3.add(R2072);
              R2072.setBounds(90, 230, 80, R2072.getPreferredSize().height);

              //---- R2082 ----
              R2082.setHorizontalAlignment(SwingConstants.RIGHT);
              R2082.setName("R2082");
              panel3.add(R2082);
              R2082.setBounds(90, 255, 80, R2082.getPreferredSize().height);

              //---- R2092 ----
              R2092.setHorizontalAlignment(SwingConstants.RIGHT);
              R2092.setName("R2092");
              panel3.add(R2092);
              R2092.setBounds(90, 280, 80, R2092.getPreferredSize().height);

              //---- R2102 ----
              R2102.setHorizontalAlignment(SwingConstants.RIGHT);
              R2102.setName("R2102");
              panel3.add(R2102);
              R2102.setBounds(90, 305, 80, R2102.getPreferredSize().height);

              //---- R2112 ----
              R2112.setHorizontalAlignment(SwingConstants.RIGHT);
              R2112.setName("R2112");
              panel3.add(R2112);
              R2112.setBounds(90, 330, 80, R2112.getPreferredSize().height);

              //---- R2122 ----
              R2122.setHorizontalAlignment(SwingConstants.RIGHT);
              R2122.setName("R2122");
              panel3.add(R2122);
              R2122.setBounds(90, 355, 80, R2122.getPreferredSize().height);

              //---- R2132 ----
              R2132.setHorizontalAlignment(SwingConstants.RIGHT);
              R2132.setName("R2132");
              panel3.add(R2132);
              R2132.setBounds(90, 380, 80, R2132.getPreferredSize().height);

              //---- R2142 ----
              R2142.setHorizontalAlignment(SwingConstants.RIGHT);
              R2142.setName("R2142");
              panel3.add(R2142);
              R2142.setBounds(90, 405, 80, R2142.getPreferredSize().height);

              //---- R2152 ----
              R2152.setHorizontalAlignment(SwingConstants.RIGHT);
              R2152.setName("R2152");
              panel3.add(R2152);
              R2152.setBounds(90, 430, 80, R2152.getPreferredSize().height);

              //---- R2013 ----
              R2013.setHorizontalAlignment(SwingConstants.RIGHT);
              R2013.setName("R2013");
              panel3.add(R2013);
              R2013.setBounds(170, 80, 80, R2013.getPreferredSize().height);

              //---- R2023 ----
              R2023.setHorizontalAlignment(SwingConstants.RIGHT);
              R2023.setName("R2023");
              panel3.add(R2023);
              R2023.setBounds(170, 105, 80, R2023.getPreferredSize().height);

              //---- R2033 ----
              R2033.setHorizontalAlignment(SwingConstants.RIGHT);
              R2033.setName("R2033");
              panel3.add(R2033);
              R2033.setBounds(170, 130, 80, R2033.getPreferredSize().height);

              //---- R2043 ----
              R2043.setHorizontalAlignment(SwingConstants.RIGHT);
              R2043.setName("R2043");
              panel3.add(R2043);
              R2043.setBounds(170, 155, 80, R2043.getPreferredSize().height);

              //---- R2053 ----
              R2053.setHorizontalAlignment(SwingConstants.RIGHT);
              R2053.setName("R2053");
              panel3.add(R2053);
              R2053.setBounds(170, 180, 80, R2053.getPreferredSize().height);

              //---- R2063 ----
              R2063.setHorizontalAlignment(SwingConstants.RIGHT);
              R2063.setName("R2063");
              panel3.add(R2063);
              R2063.setBounds(170, 205, 80, R2063.getPreferredSize().height);

              //---- R2073 ----
              R2073.setHorizontalAlignment(SwingConstants.RIGHT);
              R2073.setName("R2073");
              panel3.add(R2073);
              R2073.setBounds(170, 230, 80, R2073.getPreferredSize().height);

              //---- R2083 ----
              R2083.setHorizontalAlignment(SwingConstants.RIGHT);
              R2083.setName("R2083");
              panel3.add(R2083);
              R2083.setBounds(170, 255, 80, R2083.getPreferredSize().height);

              //---- R2093 ----
              R2093.setHorizontalAlignment(SwingConstants.RIGHT);
              R2093.setName("R2093");
              panel3.add(R2093);
              R2093.setBounds(170, 280, 80, R2093.getPreferredSize().height);

              //---- R2103 ----
              R2103.setHorizontalAlignment(SwingConstants.RIGHT);
              R2103.setName("R2103");
              panel3.add(R2103);
              R2103.setBounds(170, 305, 80, R2103.getPreferredSize().height);

              //---- R2113 ----
              R2113.setHorizontalAlignment(SwingConstants.RIGHT);
              R2113.setName("R2113");
              panel3.add(R2113);
              R2113.setBounds(170, 330, 80, R2113.getPreferredSize().height);

              //---- R2123 ----
              R2123.setHorizontalAlignment(SwingConstants.RIGHT);
              R2123.setName("R2123");
              panel3.add(R2123);
              R2123.setBounds(170, 355, 80, R2123.getPreferredSize().height);

              //---- R2133 ----
              R2133.setHorizontalAlignment(SwingConstants.RIGHT);
              R2133.setName("R2133");
              panel3.add(R2133);
              R2133.setBounds(170, 380, 80, R2133.getPreferredSize().height);

              //---- R2143 ----
              R2143.setHorizontalAlignment(SwingConstants.RIGHT);
              R2143.setName("R2143");
              panel3.add(R2143);
              R2143.setBounds(170, 405, 80, R2143.getPreferredSize().height);

              //---- R2153 ----
              R2153.setHorizontalAlignment(SwingConstants.RIGHT);
              R2153.setName("R2153");
              panel3.add(R2153);
              R2153.setBounds(170, 430, 80, R2153.getPreferredSize().height);

              //---- R2014 ----
              R2014.setHorizontalAlignment(SwingConstants.RIGHT);
              R2014.setName("R2014");
              panel3.add(R2014);
              R2014.setBounds(250, 80, 80, R2014.getPreferredSize().height);

              //---- R2024 ----
              R2024.setHorizontalAlignment(SwingConstants.RIGHT);
              R2024.setName("R2024");
              panel3.add(R2024);
              R2024.setBounds(250, 105, 80, R2024.getPreferredSize().height);

              //---- R2034 ----
              R2034.setHorizontalAlignment(SwingConstants.RIGHT);
              R2034.setName("R2034");
              panel3.add(R2034);
              R2034.setBounds(250, 130, 80, R2034.getPreferredSize().height);

              //---- R2044 ----
              R2044.setHorizontalAlignment(SwingConstants.RIGHT);
              R2044.setName("R2044");
              panel3.add(R2044);
              R2044.setBounds(250, 155, 80, R2044.getPreferredSize().height);

              //---- R2054 ----
              R2054.setHorizontalAlignment(SwingConstants.RIGHT);
              R2054.setName("R2054");
              panel3.add(R2054);
              R2054.setBounds(250, 180, 80, R2054.getPreferredSize().height);

              //---- R2064 ----
              R2064.setHorizontalAlignment(SwingConstants.RIGHT);
              R2064.setName("R2064");
              panel3.add(R2064);
              R2064.setBounds(250, 205, 80, R2064.getPreferredSize().height);

              //---- R2074 ----
              R2074.setHorizontalAlignment(SwingConstants.RIGHT);
              R2074.setName("R2074");
              panel3.add(R2074);
              R2074.setBounds(250, 230, 80, R2074.getPreferredSize().height);

              //---- R2084 ----
              R2084.setHorizontalAlignment(SwingConstants.RIGHT);
              R2084.setName("R2084");
              panel3.add(R2084);
              R2084.setBounds(250, 255, 80, R2084.getPreferredSize().height);

              //---- R2094 ----
              R2094.setHorizontalAlignment(SwingConstants.RIGHT);
              R2094.setName("R2094");
              panel3.add(R2094);
              R2094.setBounds(250, 280, 80, R2094.getPreferredSize().height);

              //---- R2104 ----
              R2104.setHorizontalAlignment(SwingConstants.RIGHT);
              R2104.setName("R2104");
              panel3.add(R2104);
              R2104.setBounds(250, 305, 80, R2104.getPreferredSize().height);

              //---- R2114 ----
              R2114.setHorizontalAlignment(SwingConstants.RIGHT);
              R2114.setName("R2114");
              panel3.add(R2114);
              R2114.setBounds(250, 330, 80, R2114.getPreferredSize().height);

              //---- R2124 ----
              R2124.setHorizontalAlignment(SwingConstants.RIGHT);
              R2124.setName("R2124");
              panel3.add(R2124);
              R2124.setBounds(250, 355, 80, R2124.getPreferredSize().height);

              //---- R2134 ----
              R2134.setHorizontalAlignment(SwingConstants.RIGHT);
              R2134.setName("R2134");
              panel3.add(R2134);
              R2134.setBounds(250, 380, 80, R2134.getPreferredSize().height);

              //---- R2144 ----
              R2144.setHorizontalAlignment(SwingConstants.RIGHT);
              R2144.setName("R2144");
              panel3.add(R2144);
              R2144.setBounds(250, 405, 80, R2144.getPreferredSize().height);

              //---- R2154 ----
              R2154.setHorizontalAlignment(SwingConstants.RIGHT);
              R2154.setName("R2154");
              panel3.add(R2154);
              R2154.setBounds(250, 430, 80, R2154.getPreferredSize().height);

              //---- separator3 ----
              separator3.setName("separator3");
              panel3.add(separator3);
              separator3.setBounds(10, 65, 320, 15);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel4.add(panel3);
            panel3.setBounds(540, 10, 340, 470);

            //---- Q101 ----
            Q101.setHorizontalAlignment(SwingConstants.RIGHT);
            Q101.setName("Q101");
            panel4.add(Q101);
            Q101.setBounds(15, 90, 80, Q101.getPreferredSize().height);

            //---- Q102 ----
            Q102.setHorizontalAlignment(SwingConstants.RIGHT);
            Q102.setName("Q102");
            panel4.add(Q102);
            Q102.setBounds(15, 115, 80, Q102.getPreferredSize().height);

            //---- Q103 ----
            Q103.setHorizontalAlignment(SwingConstants.RIGHT);
            Q103.setName("Q103");
            panel4.add(Q103);
            Q103.setBounds(15, 140, 80, Q103.getPreferredSize().height);

            //---- Q104 ----
            Q104.setHorizontalAlignment(SwingConstants.RIGHT);
            Q104.setName("Q104");
            panel4.add(Q104);
            Q104.setBounds(15, 165, 80, Q104.getPreferredSize().height);

            //---- Q105 ----
            Q105.setHorizontalAlignment(SwingConstants.RIGHT);
            Q105.setName("Q105");
            panel4.add(Q105);
            Q105.setBounds(15, 190, 80, Q105.getPreferredSize().height);

            //---- Q106 ----
            Q106.setHorizontalAlignment(SwingConstants.RIGHT);
            Q106.setName("Q106");
            panel4.add(Q106);
            Q106.setBounds(15, 215, 80, Q106.getPreferredSize().height);

            //---- Q107 ----
            Q107.setHorizontalAlignment(SwingConstants.RIGHT);
            Q107.setName("Q107");
            panel4.add(Q107);
            Q107.setBounds(15, 240, 80, Q107.getPreferredSize().height);

            //---- Q108 ----
            Q108.setHorizontalAlignment(SwingConstants.RIGHT);
            Q108.setName("Q108");
            panel4.add(Q108);
            Q108.setBounds(15, 265, 80, Q108.getPreferredSize().height);

            //---- Q109 ----
            Q109.setHorizontalAlignment(SwingConstants.RIGHT);
            Q109.setName("Q109");
            panel4.add(Q109);
            Q109.setBounds(15, 290, 80, Q109.getPreferredSize().height);

            //---- Q110 ----
            Q110.setHorizontalAlignment(SwingConstants.RIGHT);
            Q110.setName("Q110");
            panel4.add(Q110);
            Q110.setBounds(15, 315, 80, Q110.getPreferredSize().height);

            //---- Q111 ----
            Q111.setHorizontalAlignment(SwingConstants.RIGHT);
            Q111.setName("Q111");
            panel4.add(Q111);
            Q111.setBounds(15, 340, 80, Q111.getPreferredSize().height);

            //---- Q112 ----
            Q112.setHorizontalAlignment(SwingConstants.RIGHT);
            Q112.setName("Q112");
            panel4.add(Q112);
            Q112.setBounds(15, 365, 80, Q112.getPreferredSize().height);

            //---- Q113 ----
            Q113.setHorizontalAlignment(SwingConstants.RIGHT);
            Q113.setName("Q113");
            panel4.add(Q113);
            Q113.setBounds(15, 390, 80, Q113.getPreferredSize().height);

            //---- Q114 ----
            Q114.setHorizontalAlignment(SwingConstants.RIGHT);
            Q114.setName("Q114");
            panel4.add(Q114);
            Q114.setBounds(15, 415, 80, Q114.getPreferredSize().height);

            //---- Q115 ----
            Q115.setHorizontalAlignment(SwingConstants.RIGHT);
            Q115.setName("Q115");
            panel4.add(Q115);
            Q115.setBounds(15, 440, 80, Q115.getPreferredSize().height);

            //---- ERR1 ----
            ERR1.setText("@ERR1@");
            ERR1.setHorizontalAlignment(SwingConstants.CENTER);
            ERR1.setForeground(new Color(153, 0, 0));
            ERR1.setFont(ERR1.getFont().deriveFont(ERR1.getFont().getStyle() | Font.BOLD));
            ERR1.setName("ERR1");
            panel4.add(ERR1);
            ERR1.setBounds(445, 90, 90, 28);

            //---- ERR2 ----
            ERR2.setText("@ERR2@");
            ERR2.setHorizontalAlignment(SwingConstants.CENTER);
            ERR2.setForeground(new Color(153, 0, 0));
            ERR2.setFont(ERR2.getFont().deriveFont(ERR2.getFont().getStyle() | Font.BOLD));
            ERR2.setName("ERR2");
            panel4.add(ERR2);
            ERR2.setBounds(445, 115, 90, 28);

            //---- ERR3 ----
            ERR3.setText("@ERR3@");
            ERR3.setHorizontalAlignment(SwingConstants.CENTER);
            ERR3.setForeground(new Color(153, 0, 0));
            ERR3.setFont(ERR3.getFont().deriveFont(ERR3.getFont().getStyle() | Font.BOLD));
            ERR3.setName("ERR3");
            panel4.add(ERR3);
            ERR3.setBounds(445, 140, 90, 28);

            //---- ERR4 ----
            ERR4.setText("@ERR4@");
            ERR4.setHorizontalAlignment(SwingConstants.CENTER);
            ERR4.setForeground(new Color(153, 0, 0));
            ERR4.setFont(ERR4.getFont().deriveFont(ERR4.getFont().getStyle() | Font.BOLD));
            ERR4.setName("ERR4");
            panel4.add(ERR4);
            ERR4.setBounds(445, 165, 90, 28);

            //---- ERR5 ----
            ERR5.setText("@ERR5@");
            ERR5.setHorizontalAlignment(SwingConstants.CENTER);
            ERR5.setForeground(new Color(153, 0, 0));
            ERR5.setFont(ERR5.getFont().deriveFont(ERR5.getFont().getStyle() | Font.BOLD));
            ERR5.setName("ERR5");
            panel4.add(ERR5);
            ERR5.setBounds(445, 190, 90, 28);

            //---- ERR6 ----
            ERR6.setText("@ERR6@");
            ERR6.setHorizontalAlignment(SwingConstants.CENTER);
            ERR6.setForeground(new Color(153, 0, 0));
            ERR6.setFont(ERR6.getFont().deriveFont(ERR6.getFont().getStyle() | Font.BOLD));
            ERR6.setName("ERR6");
            panel4.add(ERR6);
            ERR6.setBounds(445, 215, 90, 28);

            //---- ERR7 ----
            ERR7.setText("@ERR7@");
            ERR7.setHorizontalAlignment(SwingConstants.CENTER);
            ERR7.setForeground(new Color(153, 0, 0));
            ERR7.setFont(ERR7.getFont().deriveFont(ERR7.getFont().getStyle() | Font.BOLD));
            ERR7.setName("ERR7");
            panel4.add(ERR7);
            ERR7.setBounds(445, 240, 90, 28);

            //---- ERR8 ----
            ERR8.setText("@ERR8@");
            ERR8.setHorizontalAlignment(SwingConstants.CENTER);
            ERR8.setForeground(new Color(153, 0, 0));
            ERR8.setFont(ERR8.getFont().deriveFont(ERR8.getFont().getStyle() | Font.BOLD));
            ERR8.setName("ERR8");
            panel4.add(ERR8);
            ERR8.setBounds(445, 265, 90, 28);

            //---- ERR9 ----
            ERR9.setText("@ERR9@");
            ERR9.setHorizontalAlignment(SwingConstants.CENTER);
            ERR9.setForeground(new Color(153, 0, 0));
            ERR9.setFont(ERR9.getFont().deriveFont(ERR9.getFont().getStyle() | Font.BOLD));
            ERR9.setName("ERR9");
            panel4.add(ERR9);
            ERR9.setBounds(445, 290, 90, 28);

            //---- ERR10 ----
            ERR10.setText("@ERR10@");
            ERR10.setHorizontalAlignment(SwingConstants.CENTER);
            ERR10.setForeground(new Color(153, 0, 0));
            ERR10.setFont(ERR10.getFont().deriveFont(ERR10.getFont().getStyle() | Font.BOLD));
            ERR10.setName("ERR10");
            panel4.add(ERR10);
            ERR10.setBounds(445, 315, 90, 28);

            //---- ERR11 ----
            ERR11.setText("@ERR11@");
            ERR11.setHorizontalAlignment(SwingConstants.CENTER);
            ERR11.setForeground(new Color(153, 0, 0));
            ERR11.setFont(ERR11.getFont().deriveFont(ERR11.getFont().getStyle() | Font.BOLD));
            ERR11.setName("ERR11");
            panel4.add(ERR11);
            ERR11.setBounds(445, 340, 90, 28);

            //---- ERR12 ----
            ERR12.setText("@ERR12@");
            ERR12.setHorizontalAlignment(SwingConstants.CENTER);
            ERR12.setForeground(new Color(153, 0, 0));
            ERR12.setFont(ERR12.getFont().deriveFont(ERR12.getFont().getStyle() | Font.BOLD));
            ERR12.setName("ERR12");
            panel4.add(ERR12);
            ERR12.setBounds(445, 365, 90, 28);

            //---- ERR13 ----
            ERR13.setText("@ERR13@");
            ERR13.setHorizontalAlignment(SwingConstants.CENTER);
            ERR13.setForeground(new Color(153, 0, 0));
            ERR13.setFont(ERR13.getFont().deriveFont(ERR13.getFont().getStyle() | Font.BOLD));
            ERR13.setName("ERR13");
            panel4.add(ERR13);
            ERR13.setBounds(445, 390, 90, 28);

            //---- ERR14 ----
            ERR14.setText("@ERR14@");
            ERR14.setHorizontalAlignment(SwingConstants.CENTER);
            ERR14.setForeground(new Color(153, 0, 0));
            ERR14.setFont(ERR14.getFont().deriveFont(ERR14.getFont().getStyle() | Font.BOLD));
            ERR14.setName("ERR14");
            panel4.add(ERR14);
            ERR14.setBounds(445, 415, 90, 28);

            //---- ERR15 ----
            ERR15.setText("@ERR15@");
            ERR15.setHorizontalAlignment(SwingConstants.CENTER);
            ERR15.setForeground(new Color(153, 0, 0));
            ERR15.setFont(ERR15.getFont().deriveFont(ERR15.getFont().getStyle() | Font.BOLD));
            ERR15.setName("ERR15");
            panel4.add(ERR15);
            ERR15.setBounds(445, 440, 90, 28);

            //---- separator1 ----
            separator1.setName("separator1");
            panel4.add(separator1);
            separator1.setBounds(15, 75, 80, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 898, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 887, GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
          );
          p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] {panel1, panel4});
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(13, 13, 13)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 496, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private XRiTextField WETB;
  private SNBoutonRecherche BT_ChgSoc;
  private JLabel OBJ_50;
  private XRiTextField WCNV;
  private XRiTextField BRLIB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_47;
  private XRiTextField BRART2;
  private JPanel panel4;
  private JPanel panel2;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private XRiCalendrier WDEB1X;
  private XRiCalendrier WFIN1X;
  private XRiTextField R1011;
  private XRiTextField R1021;
  private XRiTextField R1031;
  private XRiTextField R1041;
  private XRiTextField R1051;
  private XRiTextField R1061;
  private XRiTextField R1071;
  private XRiTextField R1081;
  private XRiTextField R1091;
  private XRiTextField R1101;
  private XRiTextField R1111;
  private XRiTextField R1121;
  private XRiTextField R1131;
  private XRiTextField R1141;
  private XRiTextField R11511;
  private XRiTextField R1012;
  private XRiTextField R1022;
  private XRiTextField R1032;
  private XRiTextField R1042;
  private XRiTextField R1052;
  private XRiTextField R1062;
  private XRiTextField R1072;
  private XRiTextField R1082;
  private XRiTextField R1092;
  private XRiTextField R1102;
  private XRiTextField R1112;
  private XRiTextField R1122;
  private XRiTextField R1132;
  private XRiTextField R1142;
  private XRiTextField R1152;
  private XRiTextField R1013;
  private XRiTextField R1023;
  private XRiTextField R1033;
  private XRiTextField R1043;
  private XRiTextField R1053;
  private XRiTextField R1063;
  private XRiTextField R1073;
  private XRiTextField R1083;
  private XRiTextField R1093;
  private XRiTextField R1103;
  private XRiTextField R1113;
  private XRiTextField R1123;
  private XRiTextField R1133;
  private XRiTextField R1143;
  private XRiTextField R1153;
  private XRiTextField R1014;
  private XRiTextField R1024;
  private XRiTextField R1034;
  private XRiTextField R1044;
  private XRiTextField R1054;
  private XRiTextField R1064;
  private XRiTextField R1074;
  private XRiTextField R1084;
  private XRiTextField R1094;
  private XRiTextField R1104;
  private XRiTextField R1114;
  private XRiTextField R1124;
  private XRiTextField R1134;
  private XRiTextField R1144;
  private XRiTextField R1154;
  private JComponent separator2;
  private JPanel panel3;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private XRiCalendrier WDEB2X;
  private XRiCalendrier WFIN2X;
  private XRiTextField R2011;
  private XRiTextField R2021;
  private XRiTextField R2031;
  private XRiTextField R2041;
  private XRiTextField R2051;
  private XRiTextField R2061;
  private XRiTextField R2071;
  private XRiTextField R2081;
  private XRiTextField R2091;
  private XRiTextField R2101;
  private XRiTextField R2111;
  private XRiTextField R2121;
  private XRiTextField R2131;
  private XRiTextField R2141;
  private XRiTextField R2151;
  private XRiTextField R2012;
  private XRiTextField R2022;
  private XRiTextField R2032;
  private XRiTextField R2042;
  private XRiTextField R2052;
  private XRiTextField R2062;
  private XRiTextField R2072;
  private XRiTextField R2082;
  private XRiTextField R2092;
  private XRiTextField R2102;
  private XRiTextField R2112;
  private XRiTextField R2122;
  private XRiTextField R2132;
  private XRiTextField R2142;
  private XRiTextField R2152;
  private XRiTextField R2013;
  private XRiTextField R2023;
  private XRiTextField R2033;
  private XRiTextField R2043;
  private XRiTextField R2053;
  private XRiTextField R2063;
  private XRiTextField R2073;
  private XRiTextField R2083;
  private XRiTextField R2093;
  private XRiTextField R2103;
  private XRiTextField R2113;
  private XRiTextField R2123;
  private XRiTextField R2133;
  private XRiTextField R2143;
  private XRiTextField R2153;
  private XRiTextField R2014;
  private XRiTextField R2024;
  private XRiTextField R2034;
  private XRiTextField R2044;
  private XRiTextField R2054;
  private XRiTextField R2064;
  private XRiTextField R2074;
  private XRiTextField R2084;
  private XRiTextField R2094;
  private XRiTextField R2104;
  private XRiTextField R2114;
  private XRiTextField R2124;
  private XRiTextField R2134;
  private XRiTextField R2144;
  private XRiTextField R2154;
  private JComponent separator3;
  private XRiTextField Q101;
  private XRiTextField Q102;
  private XRiTextField Q103;
  private XRiTextField Q104;
  private XRiTextField Q105;
  private XRiTextField Q106;
  private XRiTextField Q107;
  private XRiTextField Q108;
  private XRiTextField Q109;
  private XRiTextField Q110;
  private XRiTextField Q111;
  private XRiTextField Q112;
  private XRiTextField Q113;
  private XRiTextField Q114;
  private XRiTextField Q115;
  private JLabel ERR1;
  private JLabel ERR2;
  private JLabel ERR3;
  private JLabel ERR4;
  private JLabel ERR5;
  private JLabel ERR6;
  private JLabel ERR7;
  private JLabel ERR8;
  private JLabel ERR9;
  private JLabel ERR10;
  private JLabel ERR11;
  private JLabel ERR12;
  private JLabel ERR13;
  private JLabel ERR14;
  private JLabel ERR15;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
