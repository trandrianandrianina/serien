
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "CHAP", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 558, };
  private static final String BOUTON_CONSULTATION = "Consulter";
  private static final String BOUTON_CREATION = "Créer";
  private static final String BOUTON_DESACTIVATION = "Désactiver";
  private static final String BOUTON_REACTIVATION = "Réactiver";
  private static final String BOUTON_SUPPRESSION = "Supprimer";
  private boolean isCreation = false;
  private boolean isParametreDG = false;
  
  public VGVM01FX_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CONSULTATION, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 'e', true);
    snBarreBouton.ajouterBouton(BOUTON_DESACTIVATION, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_REACTIVATION, 'r', true);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRESSION, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbListePersonnalisations.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Choisir @PG01LI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    isParametreDG = lexique.HostFieldGetData("INDTYP").trim().equalsIgnoreCase("DG");
    
    if (isCreation && isParametreDG) {
      lbEtablissement.setText("Nouvel établissement");
      INDETB.setVisible(true);
      INDETB.setText("");
      snEtablissement.setVisible(false);
      lbCodeCategorie.setVisible(false);
      pnlCode.setVisible(false);
    }
    else {
      lbEtablissement.setText("Etablissement");
      INDETB.setVisible(false);
      snEtablissement.setVisible(true);
      lbCodeCategorie.setVisible(true);
      pnlCode.setVisible(true);
      // Initialisation de l'établissement
      snEtablissement.setSession(getSession());
      snEtablissement.setEtablissementBlanc(true);
      snEtablissement.charger(false);
      snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    }
    
    // Message de mise en forme autorisée pour le code personnalisation
    UTMZ.setVisible(!lexique.HostFieldGetData("UTMZ").trim().isEmpty() || (lexique.isTrue("51")));
    if (UTMZ.isVisible()) {
      if (lexique.HostFieldGetData("UTMZ").trim().isEmpty()) {
        UTMZ.setMessage(Message.getMessageMoyen("Saisissez le code"));
      }
      else {
        UTMZ.setMessage(Message.getMessageMoyen("Saisissez le code sous la forme : " + lexique.HostFieldGetData("UTMZ")));
      }
    }
    
    // Titre liste
    String typePersonnalisation = lexique.HostFieldGetData("PG01LI");
    if (lexique.isTrue("53")) {
      lbListePersonnalisations.setText("Consulter " + typePersonnalisation);
    }
    else if (lexique.isTrue("51")) {
      lbListePersonnalisations.setText("Créer " + typePersonnalisation);
    }
    else if (lexique.isTrue("54")) {
      lbListePersonnalisations.setText("Désactiver " + typePersonnalisation);
    }
    else if (lexique.isTrue("55")) {
      lbListePersonnalisations.setText("Résactiver " + typePersonnalisation);
    }
    else if (lexique.isTrue("42")) {
      lbListePersonnalisations.setText("Supprimer " + typePersonnalisation);
    }
    else {
      lexique.HostScreenSendKey(this, "F15");
    }
    WTP01.setEnabled(lexique.isTrue("N51"));
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    if (snEtablissement.getIdSelection() != null) {
      p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    if (!isCreation && !isParametreDG) {
      snEtablissement.renseignerChampRPG(lexique, "INDETB");
    }
    
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_CONSULTATION)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_DESACTIVATION)) {
        lexique.HostScreenSendKey(this, "F16");
      }
      else if (pSNBouton.isBouton(BOUTON_REACTIVATION)) {
        lexique.HostScreenSendKey(this, "F17");
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRESSION)) {
        lexique.HostScreenSendKey(this, "F19");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Visibilité des boutons
   */
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_CONSULTATION, lexique.isTrue("N53"));
    snBarreBouton.activerBouton(BOUTON_CREATION, lexique.isTrue("(N51) and (N54) and (N55) and (N42))"));
    snBarreBouton.activerBouton(BOUTON_DESACTIVATION, lexique.isTrue("(N51) and (N54) and (N55) and (N42)"));
    snBarreBouton.activerBouton(BOUTON_REACTIVATION, lexique.isTrue("(N51) and (N54) and (N55) and (N42)"));
    snBarreBouton.activerBouton(BOUTON_SUPPRESSION, lexique.isTrue("(N51) and (N54) and (N55) and (N42)"));
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void miHistoriqueActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbEtablissement = new SNLabelChamp();
    pnlEtablissement = new SNPanel();
    INDETB = new XRiTextField();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    pnlCode = new SNPanel();
    sNIndicateur = new SNPanel();
    INDIND = new XRiTextField();
    IND1 = new XRiTextField();
    UTMZ = new SNMessage();
    lbListePersonnalisations = new SNLabelTitre();
    sNPanel2 = new SNPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlScroll = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    separator1 = new JSeparator();
    miHistorique = new JMenuItem();
    OBJ_25 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlContenu.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- INDETB ----
          INDETB.setPreferredSize(new Dimension(50, 30));
          INDETB.setMinimumSize(new Dimension(50, 30));
          INDETB.setMaximumSize(new Dimension(50, 30));
          INDETB.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDETB.setName("INDETB");
          pnlEtablissement.add(INDETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeCategorie ----
        lbCodeCategorie.setText("Code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlContenu.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlCode ========
        {
          pnlCode.setName("pnlCode");
          pnlCode.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCode.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCode.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCode.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCode.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== sNIndicateur ========
          {
            sNIndicateur.setName("sNIndicateur");
            sNIndicateur.setLayout(new GridBagLayout());
            ((GridBagLayout) sNIndicateur.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNIndicateur.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNIndicateur.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) sNIndicateur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- INDIND ----
            INDIND.setPreferredSize(new Dimension(70, 30));
            INDIND.setMinimumSize(new Dimension(70, 30));
            INDIND.setMaximumSize(new Dimension(70, 30));
            INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
            INDIND.setName("INDIND");
            sNIndicateur.add(INDIND, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- IND1 ----
            IND1.setPreferredSize(new Dimension(70, 30));
            IND1.setMinimumSize(new Dimension(70, 30));
            IND1.setMaximumSize(new Dimension(70, 30));
            IND1.setFont(new Font("sansserif", Font.PLAIN, 14));
            IND1.setName("IND1");
            sNIndicateur.add(IND1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCode.add(sNIndicateur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- UTMZ ----
          UTMZ.setText("message");
          UTMZ.setForeground(new Color(153, 0, 0));
          UTMZ.setName("UTMZ");
          pnlCode.add(UTMZ, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlCode, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbListePersonnalisations ----
        lbListePersonnalisations.setText("Choisir @PG01LI@");
        lbListePersonnalisations.setName("lbListePersonnalisations");
        pnlContenu.add(lbListePersonnalisations, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel2 ========
        {
          sNPanel2.setName("sNPanel2");
          sNPanel2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setPreferredSize(new Dimension(500, 270));
            SCROLLPANE_LIST.setMinimumSize(new Dimension(500, 270));
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
            
            // ---- WTP01 ----
            WTP01.setMaximumSize(new Dimension(2147483647, 238));
            WTP01.setPreferredSize(new Dimension(150, 240));
            WTP01.setPreferredScrollableViewportSize(new Dimension(450, 238));
            WTP01.setRequestFocusEnabled(false);
            WTP01.setMinimumSize(new Dimension(150, 240));
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          sNPanel2.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlScroll ========
          {
            pnlScroll.setMinimumSize(new Dimension(28, 100));
            pnlScroll.setPreferredSize(new Dimension(28, 100));
            pnlScroll.setMaximumSize(new Dimension(28, 100));
            pnlScroll.setName("pnlScroll");
            pnlScroll.setLayout(new GridLayout(2, 1));
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMaximumSize(new Dimension(28, 100));
            BT_PGUP.setMinimumSize(new Dimension(28, 100));
            BT_PGUP.setPreferredSize(new Dimension(28, 100));
            BT_PGUP.setName("BT_PGUP");
            pnlScroll.add(BT_PGUP);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMaximumSize(new Dimension(28, 50));
            BT_PGDOWN.setMinimumSize(new Dimension(28, 50));
            BT_PGDOWN.setPreferredSize(new Dimension(28, 50));
            BT_PGDOWN.setName("BT_PGDOWN");
            pnlScroll.add(BT_PGDOWN);
          }
          sNPanel2.add(pnlScroll, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(sNPanel2, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Modifier");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Annuler");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Interroger");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- separator1 ----
      separator1.setName("separator1");
      BTD.add(separator1);
      
      // ---- miHistorique ----
      miHistorique.setText("Historique des modifications");
      miHistorique.setName("miHistorique");
      miHistorique.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miHistoriqueActionPerformed(e);
        }
      });
      BTD.add(miHistorique);
      
      // ---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbEtablissement;
  private SNPanel pnlEtablissement;
  private XRiTextField INDETB;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private SNPanel pnlCode;
  private SNPanel sNIndicateur;
  private XRiTextField INDIND;
  private XRiTextField IND1;
  private SNMessage UTMZ;
  private SNLabelTitre lbListePersonnalisations;
  private SNPanel sNPanel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel pnlScroll;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JSeparator separator1;
  private JMenuItem miHistorique;
  private JMenuItem OBJ_25;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
