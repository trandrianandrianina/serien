
package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNAdresseMail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_C9 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVM35FM_C9(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    RPFAX.setEnabled(lexique.isPresent("RPFAX"));
    RPTEL.setEnabled(lexique.isPresent("RPTEL"));
    riSousMenu15.setEnabled(!lexique.HostFieldGetData("RPNET1").trim().equals(""));
    
    setTitle(interpreteurD.analyseExpression("FICHE REPRESENTANT"));
    
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 70);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    if (Desktop.isDesktopSupported()) {
      if (Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
        try {
          if (!lexique.HostFieldGetData("RPNET1").trim().equals("")) {
            Desktop.getDesktop().mail(new URI("mailto:" + lexique.HostFieldGetData("RPNET1").trim()));
          }
        }
        catch (IOException e1) {
          e1.printStackTrace();
        }
        catch (URISyntaxException e2) {
          // TODO Auto-generated catch block
          e2.printStackTrace();
        }
      }
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlCoordonnees = new SNPanelTitre();
    lbTelephone = new SNLabelChamp();
    RPTEL = new XRiTextField();
    lbFax = new SNLabelChamp();
    RPFAX = new XRiTextField();
    lbMail = new SNLabelChamp();
    snAdresseMail = new SNAdresseMail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(720, 205));
    setName("this");
    setLayout(new BorderLayout());

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());

      //======== pnlCoordonnees ========
      {
        pnlCoordonnees.setTitre("Coordonn\u00e9es");
        pnlCoordonnees.setName("pnlCoordonnees");
        pnlCoordonnees.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCoordonnees.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlCoordonnees.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlCoordonnees.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlCoordonnees.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

        //---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setName("lbTelephone");
        pnlCoordonnees.add(lbTelephone, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- RPTEL ----
        RPTEL.setComponentPopupMenu(BTD);
        RPTEL.setName("RPTEL");
        pnlCoordonnees.add(RPTEL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbFax ----
        lbFax.setText("Fax");
        lbFax.setName("lbFax");
        pnlCoordonnees.add(lbFax, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- RPFAX ----
        RPFAX.setComponentPopupMenu(BTD);
        RPFAX.setName("RPFAX");
        pnlCoordonnees.add(RPFAX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMail ----
        lbMail.setText("Adresse e-mail");
        lbMail.setName("lbMail");
        pnlCoordonnees.add(lbMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- snAdresseMail ----
        snAdresseMail.setName("snAdresseMail");
        pnlCoordonnees.add(snAdresseMail, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCoordonnees);
    }
    add(pnlContenu, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_erreurs ========
        {
          navig_erreurs.setName("navig_erreurs");

          //---- bouton_erreurs ----
          bouton_erreurs.setText("Erreurs");
          bouton_erreurs.setToolTipText("Erreurs");
          bouton_erreurs.setName("bouton_erreurs");
          navig_erreurs.add(bouton_erreurs);
        }
        menus_bas.add(navig_erreurs);

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setToolTipText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);

      //======== scroll_droite ========
      {
        scroll_droite.setBackground(new Color(238, 239, 241));
        scroll_droite.setPreferredSize(new Dimension(16, 520));
        scroll_droite.setBorder(null);
        scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll_droite.setName("scroll_droite");

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Param\u00e9trage  web");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);

          //======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");

            //---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Composer un email");
            riSousMenu_bt15.setToolTipText("Composer un email");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        scroll_droite.setViewportView(menus_haut);
      }
      p_menus.add(scroll_droite, BorderLayout.NORTH);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCoordonnees;
  private SNLabelChamp lbTelephone;
  private XRiTextField RPTEL;
  private SNLabelChamp lbFax;
  private XRiTextField RPFAX;
  private SNLabelChamp lbMail;
  private SNAdresseMail snAdresseMail;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
