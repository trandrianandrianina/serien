
package ri.serien.libecranrpg.vgvm.VGVM11HF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11HF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVM11HF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    // setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    setTitle("Historique du bon de vente");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1DPR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DPR@")).trim());
    E1DLSX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DLSX@")).trim());
    E1DLPI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DLPI@")).trim());
    E1DLPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DLPX@")).trim());
    E1HOMX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1HOMX@")).trim());
    E1AFFX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1AFFX@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    E1FACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1FACX@")).trim());
    L16RLR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L16RLR@")).trim());
    L16PAL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L16PAL@")).trim());
    L16VOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L16VOL@")).trim());
    L14PAS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DATPLA@")).trim());
    DATPLA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L14PAS@")).trim());
    DATPRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DATPRE@")).trim());
    E1EXPX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1EXPX@")).trim());
    E1DT3X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DT3X@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NCC@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RCC@")).trim());
    E1DLSX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DLSX2@")).trim());
    E1CREX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CREX@")).trim());
    DATMOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DATMOD@")).trim());
    DATANN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DATANN@")).trim());
    USRCRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@USRCRE@")).trim());
    USRMOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@USRMOD@")).trim());
    USRANN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@USRANN@")).trim());
    OPTCRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPTCRE@")).trim());
    OPTCRE2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPTMOD@")).trim());
    TTCCRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTCCRE@")).trim());
    TTCMOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTCMOD@")).trim());
    TTCANN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTCANN@")).trim());
    NLICRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NLICRE@")).trim());
    NLIMOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NLIMOD@")).trim());
    NLIANN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NLIANN@")).trim());
    ETACRE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETACRE@")).trim());
    ETAMOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETACRE@")).trim());
    ETAANN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETAANN@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    E1VDE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1VDE@")).trim());
    E1MAG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@")).trim());
    L15PAS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L15PAS@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CPT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_74.setVisible(lexique.isPresent("E1NFA"));
    OBJ_45.setVisible(lexique.isPresent("E1EXPX"));
    riSousMenu7.setEnabled(!lexique.HostFieldGetData("DATANN").trim().equalsIgnoreCase(""));
    OBJ_21.setVisible(lexique.isPresent("WNOM"));
    
    /*L16RLR.setVisible(!lexique.isTrue("27"));
    OBJ_59.setVisible(L16RLR.isVisible());
    L16VOL.setVisible(!lexique.isTrue("27"));
    OBJ_64.setVisible(L16VOL.isVisible());
    L16PAL.setVisible(!lexique.isTrue("27"));
    OBJ_61.setVisible(L16PAL.isVisible());*/
    
    // ********** Visibilité sur ligne annulation
    String testAnnulation1 = "";
    String testAnnulation2 = "";
    String testAnnulation3 = "";
    String testAnnulation4 = "";
    String testAnnulation5 = "";
    String testAnnulation6 = "";
    
    if (!lexique.isTrue("22")) {
      testAnnulation1 = "Annulation";
      testAnnulation2 = lexique.HostFieldGetData("DATANN");
      testAnnulation3 = lexique.HostFieldGetData("USRANN");
      testAnnulation4 = lexique.HostFieldGetData("TTCANN");
      testAnnulation5 = lexique.HostFieldGetData("NLIANN");
      testAnnulation6 = lexique.HostFieldGetData("ETAANN");
    }
    // *************
    
    // Création de la liste
    /*
    table1.setModel(new DefaultTableModel(new Object[][]{
        {null, null, null, null, null, null, null},
        {"Création", lexique.HostFieldGetData("E1CREX"), lexique.HostFieldGetData("USRCRE"), lexique.HostFieldGetData("OPTCRE"), lexique.HostFieldGetData("TTCCRE"), lexique.HostFieldGetData("NLICRE"), lexique.HostFieldGetData("ETACRE")},
        {null, null, null, null, null, null, null},
        {"Modification", lexique.HostFieldGetData("DATMOD"), lexique.HostFieldGetData("USRMOD"), lexique.HostFieldGetData("OPTMOD"), lexique.HostFieldGetData("TTCMOD"), lexique.HostFieldGetData("NLIMOD"), lexique.HostFieldGetData("ETAMOD")},
        {null, null, null, null, null, null, null},
        {testAnnulation1, testAnnulation2,testAnnulation3, null, testAnnulation4,testAnnulation5,testAnnulation6},
        {null, null, null, null, null, null, null}
    },tableHeader){
      
      boolean[] columnEditable = new boolean[] {
          false, false, false, false, false, false, false
        };
        
        public boolean isCellEditable(int rowIndex, int columnIndex) {
          return columnEditable[columnIndex];
        }
    });
    table1.getColumnModel().getColumn(0).setPreferredWidth(110);
    table1.getColumnModel().getColumn(1).setPreferredWidth(80);
    table1.getColumnModel().getColumn(2).setPreferredWidth(90);
    table1.getColumnModel().getColumn(3).setPreferredWidth(50);
    table1.getColumnModel().getColumn(4).setPreferredWidth(90);
    table1.getColumnModel().getColumn(5).setPreferredWidth(50);
    table1.getColumnModel().getColumn(6).setPreferredWidth(40);
    */
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    setTitle("Historique");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", true);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_54 = new JLabel();
    E1DPR = new RiZoneSortie();
    OBJ_55 = new JLabel();
    E1DLSX = new RiZoneSortie();
    E1DLPI = new RiZoneSortie();
    E1DLPX = new RiZoneSortie();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_67 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_71 = new JLabel();
    E1HOMX = new RiZoneSortie();
    E1AFFX = new RiZoneSortie();
    E1NFA = new RiZoneSortie();
    E1FACX = new RiZoneSortie();
    OBJ_74 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_59 = new JLabel();
    L16RLR = new RiZoneSortie();
    OBJ_61 = new JLabel();
    L16PAL = new RiZoneSortie();
    OBJ_64 = new JLabel();
    L16VOL = new RiZoneSortie();
    panel6 = new JPanel();
    OBJ_43 = new JLabel();
    L14PAS = new RiZoneSortie();
    DATPLA = new RiZoneSortie();
    panel7 = new JPanel();
    OBJ_44 = new JLabel();
    DATPRE = new RiZoneSortie();
    panel8 = new JPanel();
    OBJ_45 = new JLabel();
    E1EXPX = new RiZoneSortie();
    E1DT3X = new RiZoneSortie();
    OBJ_27 = new RiZoneSortie();
    OBJ_28 = new RiZoneSortie();
    xTitledPanel4 = new JXTitledPanel();
    E1DLSX2 = new RiZoneSortie();
    panel1 = new JPanel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    E1CREX = new RiZoneSortie();
    DATMOD = new RiZoneSortie();
    DATANN = new RiZoneSortie();
    USRCRE = new RiZoneSortie();
    USRMOD = new RiZoneSortie();
    USRANN = new RiZoneSortie();
    OPTCRE = new RiZoneSortie();
    OPTCRE2 = new RiZoneSortie();
    TTCCRE = new RiZoneSortie();
    TTCMOD = new RiZoneSortie();
    TTCANN = new RiZoneSortie();
    NLICRE = new RiZoneSortie();
    NLIMOD = new RiZoneSortie();
    NLIANN = new RiZoneSortie();
    ETACRE = new RiZoneSortie();
    ETAMOD = new RiZoneSortie();
    ETAANN = new RiZoneSortie();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    E1ETB = new RiZoneSortie();
    E1NUM = new RiZoneSortie();
    E1SUF = new RiZoneSortie();
    OBJ_21 = new RiZoneSortie();
    OBJ_22 = new JLabel();
    E1VDE = new RiZoneSortie();
    OBJ_24 = new JLabel();
    E1MAG = new RiZoneSortie();
    OBJ_44_OBJ_44 = new JLabel();
    label1 = new JLabel();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    L15PAS = new RiZoneSortie();
    OBJ_73 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(975, 530));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affichage \u00e9tendu");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Historique \u00e9dition");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("R\u00e9cup\u00e9ration");
              riSousMenu_bt7.setToolTipText("R\u00e9cup\u00e9ration (duplication)");
              riSousMenu_bt7.setFocusCycleRoot(true);
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setTitle("Livraison");
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_41 ----
          OBJ_41.setText("Souhait\u00e9e");
          OBJ_41.setName("OBJ_41");
          xTitledPanel1ContentContainer.add(OBJ_41);
          OBJ_41.setBounds(20, 7, 85, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("Pr\u00e9vue le");
          OBJ_42.setName("OBJ_42");
          xTitledPanel1ContentContainer.add(OBJ_42);
          OBJ_42.setBounds(185, 7, 70, 20);

          //---- OBJ_54 ----
          OBJ_54.setText("Modifi\u00e9 le");
          OBJ_54.setName("OBJ_54");
          xTitledPanel1ContentContainer.add(OBJ_54);
          OBJ_54.setBounds(20, 37, 85, 20);

          //---- E1DPR ----
          E1DPR.setText("@E1DPR@");
          E1DPR.setName("E1DPR");
          xTitledPanel1ContentContainer.add(E1DPR);
          E1DPR.setBounds(245, 35, 28, E1DPR.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("D\u00e9lai");
          OBJ_55.setName("OBJ_55");
          xTitledPanel1ContentContainer.add(OBJ_55);
          OBJ_55.setBounds(185, 37, 70, 20);

          //---- E1DLSX ----
          E1DLSX.setText("@E1DLSX@");
          E1DLSX.setHorizontalAlignment(SwingConstants.CENTER);
          E1DLSX.setName("E1DLSX");
          xTitledPanel1ContentContainer.add(E1DLSX);
          E1DLSX.setBounds(105, 5, 65, E1DLSX.getPreferredSize().height);

          //---- E1DLPI ----
          E1DLPI.setText("@E1DLPI@");
          E1DLPI.setHorizontalAlignment(SwingConstants.CENTER);
          E1DLPI.setName("E1DLPI");
          xTitledPanel1ContentContainer.add(E1DLPI);
          E1DLPI.setBounds(245, 5, 65, E1DLPI.getPreferredSize().height);

          //---- E1DLPX ----
          E1DLPX.setText("@E1DLPX@");
          E1DLPX.setHorizontalAlignment(SwingConstants.CENTER);
          E1DLPX.setName("E1DLPX");
          xTitledPanel1ContentContainer.add(E1DLPX);
          E1DLPX.setBounds(105, 35, 65, E1DLPX.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel1);
        xTitledPanel1.setBounds(15, 210, 335, 95);

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setTitle("Facturation");
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
          xTitledPanel3ContentContainer.setLayout(null);

          //---- OBJ_67 ----
          OBJ_67.setText("Valid\u00e9");
          OBJ_67.setName("OBJ_67");
          xTitledPanel3ContentContainer.add(OBJ_67);
          OBJ_67.setBounds(20, 17, 80, 20);

          //---- OBJ_69 ----
          OBJ_69.setText("R\u00e9serv\u00e9 le");
          OBJ_69.setName("OBJ_69");
          xTitledPanel3ContentContainer.add(OBJ_69);
          OBJ_69.setBounds(195, 17, 70, 20);

          //---- OBJ_71 ----
          OBJ_71.setText("Factur\u00e9 le");
          OBJ_71.setName("OBJ_71");
          xTitledPanel3ContentContainer.add(OBJ_71);
          OBJ_71.setBounds(365, 17, 68, 20);

          //---- E1HOMX ----
          E1HOMX.setText("@E1HOMX@");
          E1HOMX.setHorizontalAlignment(SwingConstants.CENTER);
          E1HOMX.setName("E1HOMX");
          xTitledPanel3ContentContainer.add(E1HOMX);
          E1HOMX.setBounds(105, 15, 65, E1HOMX.getPreferredSize().height);

          //---- E1AFFX ----
          E1AFFX.setText("@E1AFFX@");
          E1AFFX.setHorizontalAlignment(SwingConstants.CENTER);
          E1AFFX.setName("E1AFFX");
          xTitledPanel3ContentContainer.add(E1AFFX);
          E1AFFX.setBounds(270, 15, 65, E1AFFX.getPreferredSize().height);

          //---- E1NFA ----
          E1NFA.setText("@E1NFA@");
          E1NFA.setName("E1NFA");
          xTitledPanel3ContentContainer.add(E1NFA);
          E1NFA.setBounds(660, 15, 60, E1NFA.getPreferredSize().height);

          //---- E1FACX ----
          E1FACX.setText("@E1FACX@");
          E1FACX.setHorizontalAlignment(SwingConstants.CENTER);
          E1FACX.setName("E1FACX");
          xTitledPanel3ContentContainer.add(E1FACX);
          E1FACX.setBounds(430, 15, 65, E1FACX.getPreferredSize().height);

          //---- OBJ_74 ----
          OBJ_74.setText("Num\u00e9ro de facture");
          OBJ_74.setName("OBJ_74");
          xTitledPanel3ContentContainer.add(OBJ_74);
          OBJ_74.setBounds(545, 17, 115, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel3ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel3);
        xTitledPanel3.setBounds(15, 405, 760, 95);

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setTitle("Logistique");
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- OBJ_59 ----
          OBJ_59.setText("Logisticien");
          OBJ_59.setName("OBJ_59");
          xTitledPanel2ContentContainer.add(OBJ_59);
          OBJ_59.setBounds(30, 86, 140, 23);

          //---- L16RLR ----
          L16RLR.setText("@L16RLR@");
          L16RLR.setName("L16RLR");
          xTitledPanel2ContentContainer.add(L16RLR);
          L16RLR.setBounds(172, 85, 213, L16RLR.getPreferredSize().height);

          //---- OBJ_61 ----
          OBJ_61.setText("Nombre de palettes");
          OBJ_61.setName("OBJ_61");
          xTitledPanel2ContentContainer.add(OBJ_61);
          OBJ_61.setBounds(30, 115, 140, 22);

          //---- L16PAL ----
          L16PAL.setText("@L16PAL@");
          L16PAL.setName("L16PAL");
          xTitledPanel2ContentContainer.add(L16PAL);
          L16PAL.setBounds(172, 114, 34, L16PAL.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Volume");
          OBJ_64.setName("OBJ_64");
          xTitledPanel2ContentContainer.add(OBJ_64);
          OBJ_64.setBounds(265, 116, 60, 20);

          //---- L16VOL ----
          L16VOL.setText("@L16VOL@");
          L16VOL.setName("L16VOL");
          xTitledPanel2ContentContainer.add(L16VOL);
          L16VOL.setBounds(327, 114, 58, L16VOL.getPreferredSize().height);

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_43 ----
            OBJ_43.setText("Planification");
            OBJ_43.setName("OBJ_43");
            panel6.add(OBJ_43);
            OBJ_43.setBounds(25, 5, 95, 20);

            //---- L14PAS ----
            L14PAS.setText("@DATPLA@");
            L14PAS.setName("L14PAS");
            panel6.add(L14PAS);
            L14PAS.setBounds(25, 25, 60, L14PAS.getPreferredSize().height);

            //---- DATPLA ----
            DATPLA.setText("@L14PAS@");
            DATPLA.setName("DATPLA");
            panel6.add(DATPLA);
            DATPLA.setBounds(25, 55, 60, DATPLA.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel2ContentContainer.add(panel6);
          panel6.setBounds(5, 0, 130, 90);

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- OBJ_44 ----
            OBJ_44.setText("Pr\u00e9paration");
            OBJ_44.setName("OBJ_44");
            panel7.add(OBJ_44);
            OBJ_44.setBounds(25, 5, 95, 20);

            //---- DATPRE ----
            DATPRE.setText("@DATPRE@");
            DATPRE.setName("DATPRE");
            panel7.add(DATPRE);
            DATPRE.setBounds(25, 25, 60, DATPRE.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel2ContentContainer.add(panel7);
          panel7.setBounds(147, 0, 130, 90);

          //======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(null);

            //---- OBJ_45 ----
            OBJ_45.setText("Exp\u00e9dition");
            OBJ_45.setName("OBJ_45");
            panel8.add(OBJ_45);
            OBJ_45.setBounds(35, 5, 85, 20);

            //---- E1EXPX ----
            E1EXPX.setText("@E1EXPX@");
            E1EXPX.setName("E1EXPX");
            panel8.add(E1EXPX);
            E1EXPX.setBounds(35, 25, 60, E1EXPX.getPreferredSize().height);

            //---- E1DT3X ----
            E1DT3X.setText("@E1DT3X@");
            E1DT3X.setName("E1DT3X");
            panel8.add(E1DT3X);
            E1DT3X.setBounds(35, 55, 60, E1DT3X.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }
          xTitledPanel2ContentContainer.add(panel8);
          panel8.setBounds(289, 0, 116, 90);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel2);
        xTitledPanel2.setBounds(360, 210, 415, 180);

        //---- OBJ_27 ----
        OBJ_27.setText("@E1NCC@");
        OBJ_27.setName("OBJ_27");
        p_contenu.add(OBJ_27);
        OBJ_27.setBounds(35, 20, 81, OBJ_27.getPreferredSize().height);

        //---- OBJ_28 ----
        OBJ_28.setText("@E1RCC@");
        OBJ_28.setName("OBJ_28");
        p_contenu.add(OBJ_28);
        OBJ_28.setBounds(120, 20, 229, OBJ_28.getPreferredSize().height);

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setBorder(new DropShadowBorder());
          xTitledPanel4.setTitle("Motif d'annulation");
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
          xTitledPanel4ContentContainer.setLayout(null);

          //---- E1DLSX2 ----
          E1DLSX2.setText("@E1DLSX2@");
          E1DLSX2.setName("E1DLSX2");
          xTitledPanel4ContentContainer.add(E1DLSX2);
          E1DLSX2.setBounds(20, 10, 295, E1DLSX2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(xTitledPanel4);
        xTitledPanel4.setBounds(15, 315, 335, 75);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label2 ----
          label2.setText("Cr\u00e9ation");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(20, 37, 70, 20);

          //---- label3 ----
          label3.setText("Date");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(100, 10, 60, 25);

          //---- label4 ----
          label4.setText("Utilisateur");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(185, 10, 70, 25);

          //---- label5 ----
          label5.setText("Option");
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(295, 10, 45, 25);

          //---- label6 ----
          label6.setText("Montant TTC");
          label6.setName("label6");
          panel1.add(label6);
          label6.setBounds(385, 10, 85, 25);

          //---- label7 ----
          label7.setText("Nb lign.");
          label7.setName("label7");
          panel1.add(label7);
          label7.setBounds(495, 10, 45, 25);

          //---- label8 ----
          label8.setText("Etat");
          label8.setName("label8");
          panel1.add(label8);
          label8.setBounds(555, 10, 45, 25);

          //---- label9 ----
          label9.setText("Modification");
          label9.setName("label9");
          panel1.add(label9);
          label9.setBounds(20, 67, 80, 20);

          //---- label10 ----
          label10.setText("Annulation");
          label10.setName("label10");
          panel1.add(label10);
          label10.setBounds(20, 100, 70, 20);

          //---- E1CREX ----
          E1CREX.setText("@E1CREX@");
          E1CREX.setHorizontalAlignment(SwingConstants.CENTER);
          E1CREX.setName("E1CREX");
          panel1.add(E1CREX);
          E1CREX.setBounds(100, 35, 65, E1CREX.getPreferredSize().height);

          //---- DATMOD ----
          DATMOD.setText("@DATMOD@");
          DATMOD.setHorizontalAlignment(SwingConstants.CENTER);
          DATMOD.setName("DATMOD");
          panel1.add(DATMOD);
          DATMOD.setBounds(100, 65, 65, 24);

          //---- DATANN ----
          DATANN.setText("@DATANN@");
          DATANN.setHorizontalAlignment(SwingConstants.CENTER);
          DATANN.setName("DATANN");
          panel1.add(DATANN);
          DATANN.setBounds(100, 95, 65, 24);

          //---- USRCRE ----
          USRCRE.setText("@USRCRE@");
          USRCRE.setName("USRCRE");
          panel1.add(USRCRE);
          USRCRE.setBounds(185, 35, 110, USRCRE.getPreferredSize().height);

          //---- USRMOD ----
          USRMOD.setText("@USRMOD@");
          USRMOD.setName("USRMOD");
          panel1.add(USRMOD);
          USRMOD.setBounds(185, 65, 110, 24);

          //---- USRANN ----
          USRANN.setText("@USRANN@");
          USRANN.setName("USRANN");
          panel1.add(USRANN);
          USRANN.setBounds(185, 95, 110, 24);

          //---- OPTCRE ----
          OPTCRE.setText("@OPTCRE@");
          OPTCRE.setName("OPTCRE");
          panel1.add(OPTCRE);
          OPTCRE.setBounds(315, 35, 50, OPTCRE.getPreferredSize().height);

          //---- OPTCRE2 ----
          OPTCRE2.setText("@OPTMOD@");
          OPTCRE2.setName("OPTCRE2");
          panel1.add(OPTCRE2);
          OPTCRE2.setBounds(315, 65, 50, 24);

          //---- TTCCRE ----
          TTCCRE.setText("@TTCCRE@");
          TTCCRE.setHorizontalAlignment(SwingConstants.RIGHT);
          TTCCRE.setName("TTCCRE");
          panel1.add(TTCCRE);
          TTCCRE.setBounds(385, 35, 90, TTCCRE.getPreferredSize().height);

          //---- TTCMOD ----
          TTCMOD.setText("@TTCMOD@");
          TTCMOD.setHorizontalAlignment(SwingConstants.RIGHT);
          TTCMOD.setName("TTCMOD");
          panel1.add(TTCMOD);
          TTCMOD.setBounds(385, 65, 90, 24);

          //---- TTCANN ----
          TTCANN.setText("@TTCANN@");
          TTCANN.setHorizontalAlignment(SwingConstants.RIGHT);
          TTCANN.setName("TTCANN");
          panel1.add(TTCANN);
          TTCANN.setBounds(385, 95, 90, 24);

          //---- NLICRE ----
          NLICRE.setText("@NLICRE@");
          NLICRE.setHorizontalAlignment(SwingConstants.RIGHT);
          NLICRE.setName("NLICRE");
          panel1.add(NLICRE);
          NLICRE.setBounds(495, 35, 40, NLICRE.getPreferredSize().height);

          //---- NLIMOD ----
          NLIMOD.setText("@NLIMOD@");
          NLIMOD.setHorizontalAlignment(SwingConstants.RIGHT);
          NLIMOD.setName("NLIMOD");
          panel1.add(NLIMOD);
          NLIMOD.setBounds(495, 65, 40, 24);

          //---- NLIANN ----
          NLIANN.setText("@NLIANN@");
          NLIANN.setHorizontalAlignment(SwingConstants.RIGHT);
          NLIANN.setName("NLIANN");
          panel1.add(NLIANN);
          NLIANN.setBounds(495, 95, 40, 24);

          //---- ETACRE ----
          ETACRE.setText("@ETACRE@");
          ETACRE.setName("ETACRE");
          panel1.add(ETACRE);
          ETACRE.setBounds(555, 35, 20, ETACRE.getPreferredSize().height);

          //---- ETAMOD ----
          ETAMOD.setText("@ETACRE@");
          ETAMOD.setName("ETAMOD");
          panel1.add(ETAMOD);
          ETAMOD.setBounds(555, 65, 20, 24);

          //---- ETAANN ----
          ETAANN.setText("@ETAANN@");
          ETAANN.setName("ETAANN");
          panel1.add(ETAANN);
          ETAANN.setBounds(555, 95, 20, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 50, 755, 150);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 32));
      barre_tete.setName("barre_tete");

      //======== panel2 ========
      {
        panel2.setBorder(null);
        panel2.setPreferredSize(new Dimension(800, 35));
        panel2.setOpaque(false);
        panel2.setMaximumSize(new Dimension(800, 28));
        panel2.setMinimumSize(new Dimension(800, 28));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- E1ETB ----
        E1ETB.setOpaque(false);
        E1ETB.setText("@E1ETB@");
        E1ETB.setName("E1ETB");
        panel2.add(E1ETB);
        E1ETB.setBounds(100, 2, 40, E1ETB.getPreferredSize().height);

        //---- E1NUM ----
        E1NUM.setOpaque(false);
        E1NUM.setText("@E1NUM@");
        E1NUM.setName("E1NUM");
        panel2.add(E1NUM);
        E1NUM.setBounds(205, 2, 60, E1NUM.getPreferredSize().height);

        //---- E1SUF ----
        E1SUF.setOpaque(false);
        E1SUF.setText("@E1SUF@");
        E1SUF.setName("E1SUF");
        panel2.add(E1SUF);
        E1SUF.setBounds(270, 2, 20, E1SUF.getPreferredSize().height);

        //---- OBJ_21 ----
        OBJ_21.setText("@WNOM@");
        OBJ_21.setOpaque(false);
        OBJ_21.setName("OBJ_21");
        panel2.add(OBJ_21);
        OBJ_21.setBounds(300, 2, 265, OBJ_21.getPreferredSize().height);

        //---- OBJ_22 ----
        OBJ_22.setText("Vendeur");
        OBJ_22.setName("OBJ_22");
        panel2.add(OBJ_22);
        OBJ_22.setBounds(585, 5, 54, 18);

        //---- E1VDE ----
        E1VDE.setOpaque(false);
        E1VDE.setText("@E1VDE@");
        E1VDE.setName("E1VDE");
        panel2.add(E1VDE);
        E1VDE.setBounds(640, 2, 40, E1VDE.getPreferredSize().height);

        //---- OBJ_24 ----
        OBJ_24.setText("Magasin");
        OBJ_24.setName("OBJ_24");
        panel2.add(OBJ_24);
        OBJ_24.setBounds(700, 5, 55, 18);

        //---- E1MAG ----
        E1MAG.setOpaque(false);
        E1MAG.setText("@E1MAG@");
        E1MAG.setName("E1MAG");
        panel2.add(E1MAG);
        E1MAG.setBounds(760, 2, 34, E1MAG.getPreferredSize().height);

        //---- OBJ_44_OBJ_44 ----
        OBJ_44_OBJ_44.setText("Etablissement");
        OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
        panel2.add(OBJ_44_OBJ_44);
        OBJ_44_OBJ_44.setBounds(5, 5, 93, 18);

        //---- label1 ----
        label1.setText("Num\u00e9ro");
        label1.setName("label1");
        panel2.add(label1);
        label1.setBounds(150, 5, 55, 18);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== riSousMenu8 ========
    {
      riSousMenu8.setName("riSousMenu8");

      //---- riSousMenu_bt8 ----
      riSousMenu_bt8.setText("Situation transporteur");
      riSousMenu_bt8.setName("riSousMenu_bt8");
      riSousMenu_bt8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt8ActionPerformed(e);
        }
      });
      riSousMenu8.add(riSousMenu_bt8);
    }

    //---- L15PAS ----
    L15PAS.setText("@L15PAS@");
    L15PAS.setName("L15PAS");

    //---- OBJ_73 ----
    OBJ_73.setText("@CPT@");
    OBJ_73.setName("OBJ_73");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JLabel OBJ_54;
  private RiZoneSortie E1DPR;
  private JLabel OBJ_55;
  private RiZoneSortie E1DLSX;
  private RiZoneSortie E1DLPI;
  private RiZoneSortie E1DLPX;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_67;
  private JLabel OBJ_69;
  private JLabel OBJ_71;
  private RiZoneSortie E1HOMX;
  private RiZoneSortie E1AFFX;
  private RiZoneSortie E1NFA;
  private RiZoneSortie E1FACX;
  private JLabel OBJ_74;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_59;
  private RiZoneSortie L16RLR;
  private JLabel OBJ_61;
  private RiZoneSortie L16PAL;
  private JLabel OBJ_64;
  private RiZoneSortie L16VOL;
  private JPanel panel6;
  private JLabel OBJ_43;
  private RiZoneSortie L14PAS;
  private RiZoneSortie DATPLA;
  private JPanel panel7;
  private JLabel OBJ_44;
  private RiZoneSortie DATPRE;
  private JPanel panel8;
  private JLabel OBJ_45;
  private RiZoneSortie E1EXPX;
  private RiZoneSortie E1DT3X;
  private RiZoneSortie OBJ_27;
  private RiZoneSortie OBJ_28;
  private JXTitledPanel xTitledPanel4;
  private RiZoneSortie E1DLSX2;
  private JPanel panel1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private RiZoneSortie E1CREX;
  private RiZoneSortie DATMOD;
  private RiZoneSortie DATANN;
  private RiZoneSortie USRCRE;
  private RiZoneSortie USRMOD;
  private RiZoneSortie USRANN;
  private RiZoneSortie OPTCRE;
  private RiZoneSortie OPTCRE2;
  private RiZoneSortie TTCCRE;
  private RiZoneSortie TTCMOD;
  private RiZoneSortie TTCANN;
  private RiZoneSortie NLICRE;
  private RiZoneSortie NLIMOD;
  private RiZoneSortie NLIANN;
  private RiZoneSortie ETACRE;
  private RiZoneSortie ETAMOD;
  private RiZoneSortie ETAANN;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private RiZoneSortie E1ETB;
  private RiZoneSortie E1NUM;
  private RiZoneSortie E1SUF;
  private RiZoneSortie OBJ_21;
  private JLabel OBJ_22;
  private RiZoneSortie E1VDE;
  private JLabel OBJ_24;
  private RiZoneSortie E1MAG;
  private JLabel OBJ_44_OBJ_44;
  private JLabel label1;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiZoneSortie L15PAS;
  private RiZoneSortie OBJ_73;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
