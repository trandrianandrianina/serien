
package ri.serien.libecranrpg.vgvm.VGVM09FM;
// Nom Fichier: pop_VGVM09FM_FMTPA_154.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVM09FM_PA extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST_Top=null;
  private String[] _L01_Title = { "", };
  private String[][] _L01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", }, };
  private int[] _L01_Width = { 259, };
  // private String[][] _LIST_Title_Data_Brut=null;
  
  public VGVM09FM_PA(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST_Title_Data_Brut = initTable(LIST, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    setDialog(true);
    
    // Ajout
    initDiverses();
    L01.setAspectTable(null, _L01_Title, _L01_Data, _L01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, _LIST_Title_Data_Brut, _LIST_Top);
    
    
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Paramètres"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void VALActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  /*
    private void L01MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(LIST, _LIST_Top, "1", "ENTER", e);
      if (L01.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(LIST, _LIST_Top, "1", "Enter");
      L01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel3 = new JPanel();
    panel1 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_8 = new JButton();
    panel4 = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    L01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_10 = new JLabel();

    //======== this ========
    setPreferredSize(new Dimension(360, 260));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel3 ========
    {
      panel3.setName("panel3");

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("Ok");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            VALActionPerformed(e);
          }
        });
        panel1.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_8 ----
        OBJ_8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_8.setToolTipText("Retour");
        OBJ_8.setName("OBJ_8");
        OBJ_8.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_8ActionPerformed(e);
          }
        });
        panel1.add(OBJ_8);
        OBJ_8.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }

      GroupLayout panel3Layout = new GroupLayout(panel3);
      panel3.setLayout(panel3Layout);
      panel3Layout.setHorizontalGroup(
        panel3Layout.createParallelGroup()
          .addGroup(panel3Layout.createSequentialGroup()
            .addGap(240, 240, 240)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
      );
      panel3Layout.setVerticalGroup(
        panel3Layout.createParallelGroup()
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
      );
    }
    add(panel3, BorderLayout.SOUTH);

    //======== panel4 ========
    {
      panel4.setName("panel4");
      panel4.setLayout(null);

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder("Choix possibles"));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- L01 ----
          L01.setName("L01");
          SCROLLPANE_LIST.setViewportView(L01);
        }
        panel2.add(SCROLLPANE_LIST);
        SCROLLPANE_LIST.setBounds(20, 35, 277, 143);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        BT_PGUP.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGUPActionPerformed(e);
          }
        });
        panel2.add(BT_PGUP);
        BT_PGUP.setBounds(305, 35, 25, 65);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setToolTipText("Page suivante");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        BT_PGDOWN.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_PGDOWNActionPerformed(e);
          }
        });
        panel2.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(305, 113, 25, 65);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel4.add(panel2);
      panel2.setBounds(5, 5, 350, 200);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel4.getComponentCount(); i++) {
          Rectangle bounds = panel4.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel4.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel4.setMinimumSize(preferredSize);
        panel4.setPreferredSize(preferredSize);
      }
    }
    add(panel4, BorderLayout.CENTER);

    //---- OBJ_10 ----
    OBJ_10.setText("Choix possibles");
    OBJ_10.setName("OBJ_10");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel3;
  private JPanel panel1;
  private JButton BT_ENTER;
  private JButton OBJ_8;
  private JPanel panel4;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable L01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
