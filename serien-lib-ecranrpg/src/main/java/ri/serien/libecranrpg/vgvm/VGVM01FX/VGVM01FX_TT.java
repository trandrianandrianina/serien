
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_TT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TTTDS2_Value = { "", "3", "1", "2", };
  private String[] TTTDS1_Value = { "", "3", "1", "2", };
  
  public VGVM01FX_TT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TTTDS2.setValeurs(TTTDS2_Value, null);
    TTTDS1.setValeurs(TTTDS1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel8 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_76 = new JLabel();
    TTABC1 = new XRiTextField();
    TTABC2 = new XRiTextField();
    TTABC3 = new XRiTextField();
    TTABC4 = new XRiTextField();
    TTABC5 = new XRiTextField();
    xTitledPanel7 = new JXTitledPanel();
    TTTDS1 = new XRiComboBox();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_77 = new JLabel();
    TTOP11 = new XRiTextField();
    TTOP12 = new XRiTextField();
    OBJ_78 = new JLabel();
    TTPDL1 = new XRiTextField();
    TTTCP = new XRiTextField();
    xTitledPanel9 = new JXTitledPanel();
    TTTDS2 = new XRiComboBox();
    OBJ_86 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_91 = new JLabel();
    TTOP21 = new XRiTextField();
    TTOP22 = new XRiTextField();
    OBJ_93 = new JLabel();
    TTPDL2 = new XRiTextField();
    xTitledPanel6 = new JXTitledPanel();
    OBJ_87 = new JLabel();
    TTNJR = new XRiTextField();
    OBJ_88 = new JLabel();
    TTPDL3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel8 ========
          {
            xTitledPanel8.setTitle("Traitement des commandes");
            xTitledPanel8.setBorder(new DropShadowBorder());
            xTitledPanel8.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel8.setName("xTitledPanel8");
            Container xTitledPanel8ContentContainer = xTitledPanel8.getContentContainer();
            xTitledPanel8ContentContainer.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("D\u00e9clenchement de la livraison sur le pourcentage d'articles disponibles"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_76 ----
              OBJ_76.setText("Code ABC des articles concern\u00e9s");
              OBJ_76.setName("OBJ_76");
              panel2.add(OBJ_76);
              OBJ_76.setBounds(15, 29, 204, 20);

              //---- TTABC1 ----
              TTABC1.setComponentPopupMenu(BTD);
              TTABC1.setName("TTABC1");
              panel2.add(TTABC1);
              TTABC1.setBounds(225, 25, 20, TTABC1.getPreferredSize().height);

              //---- TTABC2 ----
              TTABC2.setComponentPopupMenu(BTD);
              TTABC2.setName("TTABC2");
              panel2.add(TTABC2);
              TTABC2.setBounds(250, 25, 20, TTABC2.getPreferredSize().height);

              //---- TTABC3 ----
              TTABC3.setComponentPopupMenu(BTD);
              TTABC3.setName("TTABC3");
              panel2.add(TTABC3);
              TTABC3.setBounds(275, 25, 20, TTABC3.getPreferredSize().height);

              //---- TTABC4 ----
              TTABC4.setComponentPopupMenu(BTD);
              TTABC4.setName("TTABC4");
              panel2.add(TTABC4);
              TTABC4.setBounds(300, 25, 20, TTABC4.getPreferredSize().height);

              //---- TTABC5 ----
              TTABC5.setComponentPopupMenu(BTD);
              TTABC5.setName("TTABC5");
              panel2.add(TTABC5);
              TTABC5.setBounds(325, 25, 20, TTABC5.getPreferredSize().height);
            }
            xTitledPanel8ContentContainer.add(panel2);
            panel2.setBounds(10, 5, 655, 65);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel8ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel8ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel8ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel8ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel8ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel7 ========
          {
            xTitledPanel7.setTitle("Traitement fin de bon");
            xTitledPanel7.setBorder(new DropShadowBorder());
            xTitledPanel7.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel7.setName("xTitledPanel7");
            Container xTitledPanel7ContentContainer = xTitledPanel7.getContentContainer();
            xTitledPanel7ContentContainer.setLayout(null);

            //---- TTTDS1 ----
            TTTDS1.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Stock r\u00e9el",
              "Stock r\u00e9el - R\u00e9serv\u00e9",
              "Stock r\u00e9el - Command\u00e9 global"
            }));
            TTTDS1.setComponentPopupMenu(BTD);
            TTTDS1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTTDS1.setName("TTTDS1");
            xTitledPanel7ContentContainer.add(TTTDS1);
            TTTDS1.setBounds(225, 35, 205, TTTDS1.getPreferredSize().height);

            //---- OBJ_79 ----
            OBJ_79.setText("Options  de traitement");
            OBJ_79.setName("OBJ_79");
            xTitledPanel7ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(495, 10, 132, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("% sur stock type");
            OBJ_80.setName("OBJ_80");
            xTitledPanel7ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(115, 38, 100, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Seuil non atteint");
            OBJ_82.setName("OBJ_82");
            xTitledPanel7ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(495, 74, 96, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("Seuil atteint");
            OBJ_81.setName("OBJ_81");
            xTitledPanel7ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(495, 38, 72, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("Priorit\u00e9");
            OBJ_77.setName("OBJ_77");
            xTitledPanel7ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(15, 10, 45, 20);

            //---- TTOP11 ----
            TTOP11.setComponentPopupMenu(BTD);
            TTOP11.setName("TTOP11");
            xTitledPanel7ContentContainer.add(TTOP11);
            TTOP11.setBounds(610, 34, 50, TTOP11.getPreferredSize().height);

            //---- TTOP12 ----
            TTOP12.setComponentPopupMenu(BTD);
            TTOP12.setName("TTOP12");
            xTitledPanel7ContentContainer.add(TTOP12);
            TTOP12.setBounds(610, 70, 50, TTOP12.getPreferredSize().height);

            //---- OBJ_78 ----
            OBJ_78.setText("Seuil");
            OBJ_78.setName("OBJ_78");
            xTitledPanel7ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(80, 10, 33, 20);

            //---- TTPDL1 ----
            TTPDL1.setComponentPopupMenu(BTD);
            TTPDL1.setName("TTPDL1");
            xTitledPanel7ContentContainer.add(TTPDL1);
            TTPDL1.setBounds(80, 34, 28, TTPDL1.getPreferredSize().height);

            //---- TTTCP ----
            TTTCP.setComponentPopupMenu(BTD);
            TTTCP.setName("TTTCP");
            xTitledPanel7ContentContainer.add(TTTCP);
            TTTCP.setBounds(20, 34, 20, TTTCP.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel7ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel7ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel7ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel7ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel7ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel9 ========
          {
            xTitledPanel9.setTitle("Pr\u00e9paration des exp\u00e9ditions");
            xTitledPanel9.setBorder(new DropShadowBorder());
            xTitledPanel9.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel9.setName("xTitledPanel9");
            Container xTitledPanel9ContentContainer = xTitledPanel9.getContentContainer();
            xTitledPanel9ContentContainer.setLayout(null);

            //---- TTTDS2 ----
            TTTDS2.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Stock r\u00e9el",
              "Stock r\u00e9el - R\u00e9serv\u00e9",
              "Stock r\u00e9el - Command\u00e9 global"
            }));
            TTTDS2.setComponentPopupMenu(BTD);
            TTTDS2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TTTDS2.setName("TTTDS2");
            xTitledPanel9ContentContainer.add(TTTDS2);
            TTTDS2.setBounds(225, 35, 205, TTTDS2.getPreferredSize().height);

            //---- OBJ_86 ----
            OBJ_86.setText("Options  de traitement");
            OBJ_86.setName("OBJ_86");
            xTitledPanel9ContentContainer.add(OBJ_86);
            OBJ_86.setBounds(495, 10, 132, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("% sur stock type");
            OBJ_89.setName("OBJ_89");
            xTitledPanel9ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(115, 38, 100, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Seuil non atteint");
            OBJ_90.setName("OBJ_90");
            xTitledPanel9ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(495, 74, 96, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("Seuil atteint");
            OBJ_91.setName("OBJ_91");
            xTitledPanel9ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(495, 38, 72, 20);

            //---- TTOP21 ----
            TTOP21.setComponentPopupMenu(BTD);
            TTOP21.setName("TTOP21");
            xTitledPanel9ContentContainer.add(TTOP21);
            TTOP21.setBounds(610, 34, 50, TTOP21.getPreferredSize().height);

            //---- TTOP22 ----
            TTOP22.setComponentPopupMenu(BTD);
            TTOP22.setName("TTOP22");
            xTitledPanel9ContentContainer.add(TTOP22);
            TTOP22.setBounds(610, 70, 50, TTOP22.getPreferredSize().height);

            //---- OBJ_93 ----
            OBJ_93.setText("Seuil");
            OBJ_93.setName("OBJ_93");
            xTitledPanel9ContentContainer.add(OBJ_93);
            OBJ_93.setBounds(80, 10, 33, 20);

            //---- TTPDL2 ----
            TTPDL2.setComponentPopupMenu(BTD);
            TTPDL2.setName("TTPDL2");
            xTitledPanel9ContentContainer.add(TTPDL2);
            TTPDL2.setBounds(80, 34, 28, TTPDL2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel9ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel9ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel9ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel9ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel9ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel6 ========
          {
            xTitledPanel6.setTitle("Edition de reliquats");
            xTitledPanel6.setBorder(new DropShadowBorder());
            xTitledPanel6.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel6.setName("xTitledPanel6");
            Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();
            xTitledPanel6ContentContainer.setLayout(null);

            //---- OBJ_87 ----
            OBJ_87.setText("Nombre de jours de retard");
            OBJ_87.setName("OBJ_87");
            xTitledPanel6ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(15, 29, 160, 20);

            //---- TTNJR ----
            TTNJR.setComponentPopupMenu(BTD);
            TTNJR.setName("TTNJR");
            xTitledPanel6ContentContainer.add(TTNJR);
            TTNJR.setBounds(225, 25, 28, TTNJR.getPreferredSize().height);

            //---- OBJ_88 ----
            OBJ_88.setText("Seuil");
            OBJ_88.setName("OBJ_88");
            xTitledPanel6ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(495, 29, 33, 20);

            //---- TTPDL3 ----
            TTPDL3.setComponentPopupMenu(BTD);
            TTPDL3.setName("TTPDL3");
            xTitledPanel6ContentContainer.add(TTPDL3);
            TTPDL3.setBounds(610, 25, 28, TTPDL3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel6ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel6ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel6ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel6ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel6ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel8, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel7, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel9, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel6, GroupLayout.PREFERRED_SIZE, 720, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(39, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel8, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                .addGap(14, 14, 14)
                .addComponent(xTitledPanel7, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel9, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel6, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel8;
  private JPanel panel2;
  private JLabel OBJ_76;
  private XRiTextField TTABC1;
  private XRiTextField TTABC2;
  private XRiTextField TTABC3;
  private XRiTextField TTABC4;
  private XRiTextField TTABC5;
  private JXTitledPanel xTitledPanel7;
  private XRiComboBox TTTDS1;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private JLabel OBJ_82;
  private JLabel OBJ_81;
  private JLabel OBJ_77;
  private XRiTextField TTOP11;
  private XRiTextField TTOP12;
  private JLabel OBJ_78;
  private XRiTextField TTPDL1;
  private XRiTextField TTTCP;
  private JXTitledPanel xTitledPanel9;
  private XRiComboBox TTTDS2;
  private JLabel OBJ_86;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private JLabel OBJ_91;
  private XRiTextField TTOP21;
  private XRiTextField TTOP22;
  private JLabel OBJ_93;
  private XRiTextField TTPDL2;
  private JXTitledPanel xTitledPanel6;
  private JLabel OBJ_87;
  private XRiTextField TTNJR;
  private JLabel OBJ_88;
  private XRiTextField TTPDL3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
