
package ri.serien.libecranrpg.vgvm.VGVM111F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM111F_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] P12TRL_Value = { "C", "A" };
  private String[] P12TRE_Value = { "C", "A" };
  private String[] P12BRL_Value = { "M", "P" };
  private String[] P12PGC_Value = { "1", "", "2", "3", };
  
  public VGVM111F_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    P12PGC.setValeurs(P12PGC_Value, null);
    P12TRE.setValeurs(P12TRE_Value, null);
    P12BRL.setValeurs(P12BRL_Value, null);
    P12TRL.setValeurs(P12TRL_Value, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIBCNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCNV@")).trim());
    P12QCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P12QCC@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P12RCC@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P12MTT@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MONT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    LIBCNV.setVisible(lexique.isPresent("LIBCNV"));
    panel3.setVisible(lexique.isTrue("80"));
    panel2.setVisible(lexique.isTrue("79"));
    
    // Titre
    setTitle("Conditions");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_10 = new JLabel();
    P12CNV = new XRiTextField();
    LIBCNV = new RiZoneSortie();
    OBJ_38 = new JLabel();
    E1REM1 = new XRiTextField();
    E1REM2 = new XRiTextField();
    E1REM3 = new XRiTextField();
    E1REM4 = new XRiTextField();
    E1REM5 = new XRiTextField();
    E1REM6 = new XRiTextField();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_39 = new JLabel();
    E1RP1 = new XRiTextField();
    E1RP2 = new XRiTextField();
    E1RP3 = new XRiTextField();
    E1RP4 = new XRiTextField();
    E1RP5 = new XRiTextField();
    E1RP6 = new XRiTextField();
    OBJ_48 = new JLabel();
    P12TAR = new XRiTextField();
    panel2 = new JPanel();
    OBJ_40 = new JLabel();
    P12QCI = new XRiTextField();
    OBJ_41 = new JLabel();
    P12QCC = new RiZoneSortie();
    label1 = new JLabel();
    P12FP1 = new XRiTextField();
    P12FP2 = new XRiTextField();
    P12FP3 = new XRiTextField();
    panel3 = new JPanel();
    OBJ_43 = new JLabel();
    P12RCI = new XRiTextField();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_46 = new JLabel();
    P12TRL = new XRiComboBox();
    P12BRL = new XRiComboBox();
    P12TRE = new XRiComboBox();
    P12PGC = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(870, 310));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 280));
            menus_haut.setPreferredSize(new Dimension(160, 280));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Conditions sur quantit\u00e9");
              riSousMenu_bt6.setToolTipText("Conditions sur quantit\u00e9");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Conditions de vente");
              riSousMenu_bt7.setToolTipText("Conditions de vente");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- OBJ_10 ----
          OBJ_10.setText("Code condition");
          OBJ_10.setName("OBJ_10");
          panel1.add(OBJ_10);
          OBJ_10.setBounds(20, 29, 110, 20);
          
          // ---- P12CNV ----
          P12CNV.setComponentPopupMenu(BTD);
          P12CNV.setName("P12CNV");
          panel1.add(P12CNV);
          P12CNV.setBounds(160, 25, 60, P12CNV.getPreferredSize().height);
          
          // ---- LIBCNV ----
          LIBCNV.setText("@LIBCNV@");
          LIBCNV.setName("LIBCNV");
          panel1.add(LIBCNV);
          LIBCNV.setBounds(225, 27, 338, LIBCNV.getPreferredSize().height);
          
          // ---- OBJ_38 ----
          OBJ_38.setText("Remises / ligne (%)");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(20, 90, 140, 20);
          
          // ---- E1REM1 ----
          E1REM1.setComponentPopupMenu(null);
          E1REM1.setName("E1REM1");
          panel1.add(E1REM1);
          E1REM1.setBounds(160, 85, 52, E1REM1.getPreferredSize().height);
          
          // ---- E1REM2 ----
          E1REM2.setComponentPopupMenu(null);
          E1REM2.setName("E1REM2");
          panel1.add(E1REM2);
          E1REM2.setBounds(213, 85, 52, E1REM2.getPreferredSize().height);
          
          // ---- E1REM3 ----
          E1REM3.setComponentPopupMenu(null);
          E1REM3.setName("E1REM3");
          panel1.add(E1REM3);
          E1REM3.setBounds(266, 85, 52, E1REM3.getPreferredSize().height);
          
          // ---- E1REM4 ----
          E1REM4.setComponentPopupMenu(null);
          E1REM4.setName("E1REM4");
          panel1.add(E1REM4);
          E1REM4.setBounds(319, 85, 52, E1REM4.getPreferredSize().height);
          
          // ---- E1REM5 ----
          E1REM5.setComponentPopupMenu(null);
          E1REM5.setName("E1REM5");
          panel1.add(E1REM5);
          E1REM5.setBounds(372, 85, 52, E1REM5.getPreferredSize().height);
          
          // ---- E1REM6 ----
          E1REM6.setComponentPopupMenu(null);
          E1REM6.setName("E1REM6");
          panel1.add(E1REM6);
          E1REM6.setBounds(425, 85, 52, E1REM6.getPreferredSize().height);
          
          // ---- OBJ_36 ----
          OBJ_36.setText("Type");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(490, 60, 36, 20);
          
          // ---- OBJ_37 ----
          OBJ_37.setText("Base");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(580, 60, 36, 20);
          
          // ---- OBJ_39 ----
          OBJ_39.setText("Remises / pied (%)");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(20, 119, 140, 20);
          
          // ---- E1RP1 ----
          E1RP1.setComponentPopupMenu(null);
          E1RP1.setName("E1RP1");
          panel1.add(E1RP1);
          E1RP1.setBounds(160, 115, 52, E1RP1.getPreferredSize().height);
          
          // ---- E1RP2 ----
          E1RP2.setComponentPopupMenu(null);
          E1RP2.setName("E1RP2");
          panel1.add(E1RP2);
          E1RP2.setBounds(213, 115, 52, E1RP2.getPreferredSize().height);
          
          // ---- E1RP3 ----
          E1RP3.setComponentPopupMenu(null);
          E1RP3.setName("E1RP3");
          panel1.add(E1RP3);
          E1RP3.setBounds(266, 115, 52, E1RP3.getPreferredSize().height);
          
          // ---- E1RP4 ----
          E1RP4.setComponentPopupMenu(null);
          E1RP4.setName("E1RP4");
          panel1.add(E1RP4);
          E1RP4.setBounds(319, 115, 52, E1RP4.getPreferredSize().height);
          
          // ---- E1RP5 ----
          E1RP5.setComponentPopupMenu(null);
          E1RP5.setName("E1RP5");
          panel1.add(E1RP5);
          E1RP5.setBounds(372, 115, 52, E1RP5.getPreferredSize().height);
          
          // ---- E1RP6 ----
          E1RP6.setComponentPopupMenu(null);
          E1RP6.setName("E1RP6");
          panel1.add(E1RP6);
          E1RP6.setBounds(425, 115, 52, E1RP6.getPreferredSize().height);
          
          // ---- OBJ_48 ----
          OBJ_48.setText("Colonne tarif");
          OBJ_48.setName("OBJ_48");
          panel1.add(OBJ_48);
          OBJ_48.setBounds(18, 209, 78, 20);
          
          // ---- P12TAR ----
          P12TAR.setName("P12TAR");
          panel1.add(P12TAR);
          P12TAR.setBounds(160, 205, 26, P12TAR.getPreferredSize().height);
          
          // ======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- OBJ_40 ----
            OBJ_40.setText("Quantit\u00e9 impos\u00e9e pour le calcul de conditions");
            OBJ_40.setName("OBJ_40");
            panel2.add(OBJ_40);
            OBJ_40.setBounds(0, 4, 273, 20);
            
            // ---- P12QCI ----
            P12QCI.setName("P12QCI");
            panel2.add(P12QCI);
            P12QCI.setBounds(280, 0, 42, P12QCI.getPreferredSize().height);
            
            // ---- OBJ_41 ----
            OBJ_41.setText("Calcul\u00e9e");
            OBJ_41.setName("OBJ_41");
            panel2.add(OBJ_41);
            OBJ_41.setBounds(415, 4, 57, 20);
            
            // ---- P12QCC ----
            P12QCC.setText("@P12QCC@");
            P12QCC.setName("P12QCC");
            panel2.add(P12QCC);
            P12QCC.setBounds(485, 2, 42, P12QCC.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(20, 175, 552, 30);
          
          // ---- label1 ----
          label1.setText("Frais de facturation (%)");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 147, 140, 25);
          
          // ---- P12FP1 ----
          P12FP1.setName("P12FP1");
          panel1.add(P12FP1);
          P12FP1.setBounds(160, 145, 52, P12FP1.getPreferredSize().height);
          
          // ---- P12FP2 ----
          P12FP2.setName("P12FP2");
          panel1.add(P12FP2);
          P12FP2.setBounds(213, 145, 52, P12FP2.getPreferredSize().height);
          
          // ---- P12FP3 ----
          P12FP3.setName("P12FP3");
          panel1.add(P12FP3);
          P12FP3.setBounds(266, 145, 52, 28);
          
          // ======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- OBJ_43 ----
            OBJ_43.setText("Rg bar\u00e8me impos\u00e9");
            OBJ_43.setName("OBJ_43");
            panel3.add(OBJ_43);
            OBJ_43.setBounds(0, 4, 120, 20);
            
            // ---- P12RCI ----
            P12RCI.setComponentPopupMenu(BTD);
            P12RCI.setName("P12RCI");
            panel3.add(P12RCI);
            P12RCI.setBounds(120, 0, 26, P12RCI.getPreferredSize().height);
            
            // ---- OBJ_44 ----
            OBJ_44.setText("Rang calcul\u00e9");
            OBJ_44.setName("OBJ_44");
            panel3.add(OBJ_44);
            OBJ_44.setBounds(175, 4, 82, 20);
            
            // ---- OBJ_45 ----
            OBJ_45.setText("@P12RCC@");
            OBJ_45.setName("OBJ_45");
            panel3.add(OBJ_45);
            OBJ_45.setBounds(260, 2, 102, 24);
            
            // ---- OBJ_47 ----
            OBJ_47.setText("@P12MTT@");
            OBJ_47.setName("OBJ_47");
            panel3.add(OBJ_47);
            OBJ_47.setBounds(465, 5, 84, 18);
            
            // ---- OBJ_46 ----
            OBJ_46.setText("@MONT@");
            OBJ_46.setName("OBJ_46");
            panel3.add(OBJ_46);
            OBJ_46.setBounds(370, 4, 84, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel3);
          panel3.setBounds(40, 175, 610, 30);
          
          // ---- P12TRL ----
          P12TRL.setModel(new DefaultComboBoxModel(new String[] { "Cascade", "Ajout" }));
          P12TRL.setName("P12TRL");
          panel1.add(P12TRL);
          P12TRL.setBounds(478, 85, 85, P12TRL.getPreferredSize().height);
          
          // ---- P12BRL ----
          P12BRL.setModel(new DefaultComboBoxModel(new String[] { "Montant", "Prix" }));
          P12BRL.setName("P12BRL");
          panel1.add(P12BRL);
          P12BRL.setBounds(564, 85, 80, P12BRL.getPreferredSize().height);
          
          // ---- P12TRE ----
          P12TRE.setModel(new DefaultComboBoxModel(new String[] { "Cascade", "Ajout" }));
          P12TRE.setName("P12TRE");
          panel1.add(P12TRE);
          P12TRE.setBounds(478, 116, 85, 26);
          
          // ---- P12PGC ----
          P12PGC.setModel(
              new DefaultComboBoxModel(new String[] { "Prix garanti", "Prix non garanti", "Ancien prix", "Prix garanti/devis" }));
          P12PGC.setName("P12PGC");
          panel1.add(P12PGC);
          P12PGC.setBounds(20, 245, 300, P12PGC.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        
        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 676, Short.MAX_VALUE).addContainerGap()));
        p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
            .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE).addContainerGap()));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_10;
  private XRiTextField P12CNV;
  private RiZoneSortie LIBCNV;
  private JLabel OBJ_38;
  private XRiTextField E1REM1;
  private XRiTextField E1REM2;
  private XRiTextField E1REM3;
  private XRiTextField E1REM4;
  private XRiTextField E1REM5;
  private XRiTextField E1REM6;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_39;
  private XRiTextField E1RP1;
  private XRiTextField E1RP2;
  private XRiTextField E1RP3;
  private XRiTextField E1RP4;
  private XRiTextField E1RP5;
  private XRiTextField E1RP6;
  private JLabel OBJ_48;
  private XRiTextField P12TAR;
  private JPanel panel2;
  private JLabel OBJ_40;
  private XRiTextField P12QCI;
  private JLabel OBJ_41;
  private RiZoneSortie P12QCC;
  private JLabel label1;
  private XRiTextField P12FP1;
  private XRiTextField P12FP2;
  private XRiTextField P12FP3;
  private JPanel panel3;
  private JLabel OBJ_43;
  private XRiTextField P12RCI;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private JLabel OBJ_46;
  private XRiComboBox P12TRL;
  private XRiComboBox P12BRL;
  private XRiComboBox P12TRE;
  private XRiComboBox P12PGC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
