
package ri.serien.libecranrpg.vgvm.VGVM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM50FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] ATRON_Value = { "", "1", "2", "3", "4", "5", "6" };
  private String[] ATRON2_Value = { "", "1", "2", "3", "4", "5", "6" };
  
  public VGVM50FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    ATRON.setValeurs(ATRON_Value, null);
    ATRON2.setValeurs(ATRON2_Value, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBENC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBENC2@")).trim());
    ATDT2X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDT2X@")).trim());
    ATIVA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATIVA2@")).trim());
    ATCNV2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATCNV2@")).trim());
    LBDAT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBDAT2@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAART@")).trim());
    OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RTLIBR@")).trim());
    WRTTAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRTTAR@")).trim());
    W2K01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K01@")).trim());
    W2K02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K02@")).trim());
    W2K03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K03@")).trim());
    W2K04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K04@")).trim());
    W2K05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K05@")).trim());
    W2K06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K06@")).trim());
    W2K07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K07@")).trim());
    W2K08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K08@")).trim());
    W2K09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K09@")).trim());
    W2K10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K10@")).trim());
    label29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS1@")).trim());
    W2P01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P01@")).trim());
    label31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS2@")).trim());
    label32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS3@")).trim());
    label33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS4@")).trim());
    label34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS5@")).trim());
    label35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS6@")).trim());
    label36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS7@")).trim());
    label37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS8@")).trim());
    label38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS9@")).trim());
    label39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALS10@")).trim());
    W2P02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P02@")).trim());
    W2P03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P03@")).trim());
    W2P04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P04@")).trim());
    W2P05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P05@")).trim());
    W2P06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P06@")).trim());
    W2P07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P07@")).trim());
    W2P08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P08@")).trim());
    W2P09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P09@")).trim());
    W2P10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P10@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDEV2@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIB2@")).trim());
    ATDT1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDT1X@")).trim());
    ATIVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATIVA@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBENC1@")).trim());
    LBDAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBDAT@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BASE@")).trim());
    W1K01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1K01@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB1@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB2@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB3@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB4@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB5@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB6@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB7@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB8@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB9@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TALB10@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    LIBDEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    ATCNV.setEnabled(lexique.isPresent("ATCNV"));
    
    OBJ_73.setVisible(lexique.isPresent("TAART"));
    label3.setVisible(lexique.isPresent("ATDEV2"));
    label1.setVisible(ATDEV.isVisible());
    label4.setVisible(lexique.isPresent("ATDEV2"));
    label5.setVisible(lexique.isPresent("ATDEV2"));
    label1.setVisible(!lexique.HostFieldGetData("ATDEV").trim().equals(""));
    label2.setVisible(!lexique.HostFieldGetData("ATDEV").trim().equals(""));
    ATCNV2.setVisible(lexique.isPresent("ATCNV2"));
    OBJ_75.setVisible(lexique.isPresent("WCHGX"));
    
    // ATRON.setSelectedIndex(getIndice("ATRON", ATRON_Value));
    // ATRON2.setSelectedIndex(getIndice("ATRON2", ATRON2_Value));
    
    ATRON.setEnabled(!lexique.isTrue("53"));
    
    // en création panel choix de devise à gauche
    if (lexique.isTrue("51")) {
      panel6.setVisible(false);
      panel7.setVisible(true);
      ATDEV.setVisible(false);
      DEV.setVisible(true);
      LIBDEV.setVisible(true);
      riBoutonDetail1.setVisible(true);
      OBJ_51.setText("Choix de la devise");
      OBJ_52.setVisible(false);
    }
    else {
      
      panel6.setVisible(true);
      panel7.setVisible(false);
      // message "aucun tarif précédent" (94 mais pas 51)
      if (lexique.isTrue("94")) {
        panel6.setVisible(false);
        panel7.setVisible(true);
        DEV.setVisible(false);
        LIBDEV.setVisible(false);
        riBoutonDetail1.setVisible(false);
        OBJ_51.setText("Aucun tarif précédent");
        OBJ_52.setText("sur cette devise");
      }
    }
    
    DEV.setText(lexique.HostFieldGetData("ATDEV"));
    LIBDEV.setText(lexique.HostFieldGetData("DVLIBR"));
    
    // Titre
    Object objet = lexique.getValeurVariableGlobale("CODE_ARTICLE_COURS");
    String codeArticle = "";
    if (objet != null) {
      codeArticle = (String) objet;
    }
    
    // Gestion deux ou quatre décimales
    boolean is4decimales = lexique.isTrue("50");
    T1P01.setVisible(!W1P01.isVisible() && is4decimales);
    T1P02.setVisible(!W1P02.isVisible() && is4decimales);
    T1P03.setVisible(!W1P03.isVisible() && is4decimales);
    T1P04.setVisible(!W1P04.isVisible() && is4decimales);
    T1P05.setVisible(!W1P05.isVisible() && is4decimales);
    T1P06.setVisible(!W1P06.isVisible() && is4decimales);
    T1P07.setVisible(!W1P07.isVisible() && is4decimales);
    T1P08.setVisible(!W1P08.isVisible() && is4decimales);
    T1P09.setVisible(!W1P09.isVisible() && is4decimales);
    T1P10.setVisible(!W1P10.isVisible() && is4decimales);
    
    if (is4decimales) {
      W2P01.setText(lexique.HostFieldGetData("T2P01"));
      W2P02.setText(lexique.HostFieldGetData("T2P02"));
      W2P03.setText(lexique.HostFieldGetData("T2P03"));
      W2P04.setText(lexique.HostFieldGetData("T2P04"));
      W2P05.setText(lexique.HostFieldGetData("T2P05"));
      W2P06.setText(lexique.HostFieldGetData("T2P06"));
      W2P07.setText(lexique.HostFieldGetData("T2P07"));
      W2P08.setText(lexique.HostFieldGetData("T2P08"));
      W2P09.setText(lexique.HostFieldGetData("T2P09"));
      W2P10.setText(lexique.HostFieldGetData("T2P10"));
    }
    
    // Gestion des dates
    ATDT2X.setVisible(lexique.isTrue("66") && lexique.isTrue("68"));
    ATIVA2.setVisible(ATDT2X.isVisible());
    ATDT1X.setVisible(ATDT2X.isVisible());
    ATIVA.setVisible(ATDT2X.isVisible());
    OBJ_31.setVisible(ATDT2X.isVisible());
    OBJ_33.setVisible(ATIVA2.isVisible());
    OBJ_35.setVisible(ATDT1X.isVisible());
    OBJ_37.setVisible(ATIVA.isVisible());
    
    if (lexique.isTrue("91")) {
      bouton_fin.setText("Fiche article");
    }
    else {
      bouton_fin.setText("Retour");
    }
    
    setTitle(interpreteurD.analyseExpression("Gestion des tarifs articles et devises: " + codeArticle));
    
    button1.setIcon(lexique.chargerImage("images/fleche_pre.png", true));
    button2.setIcon(lexique.chargerImage("images/fleche_sui.png", true));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    bouton_fin.setIcon(lexique.chargerImage("images/fin_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("ATRON", 0, ATRON_Value[ATRON.getSelectedIndex()]);
    // lexique.HostFieldPutData("AR", 0, ATRON2_Value[ATRON2.getSelectedIndex()]);
    
    if (DEV.isVisible() && !DEV.getText().trim().equals("")) {
      lexique.HostFieldPutData("ATDEV", 1, DEV.getText());
    }
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt8ActionPerformed() {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void button1ActionPerformed() {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void button2ActionPerformed() {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("ATDEV");
    lexique.HostScreenSendKey(this, "F4", true);
    // DEV.setText(lexique.HostFieldGetData("ATDEV"));
    // LIBDEV.setText(lexique.HostFieldGetData("DVLIBR"));
  }
  
  private void DEVKeyReleased(KeyEvent e) {
    DEV.setText(DEV.getText().trim().toUpperCase());
  }
  
  private void DEVKeyPressed(KeyEvent e) {
    DEV.setText(DEV.getText().trim().toUpperCase());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    navig_fin = new RiMenu();
    bouton_fin = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel6 = new JPanel();
    ATDAX2 = new XRiCalendrier();
    OBJ_31 = new JLabel();
    LIBENC2 = new JLabel();
    ATDT2X = new RiZoneSortie();
    ATIVA2 = new RiZoneSortie();
    OBJ_33 = new JLabel();
    panel1 = new JPanel();
    OBJ_56 = new JLabel();
    ATCNV2 = new RiZoneSortie();
    OBJ_58 = new JLabel();
    ATRON2 = new XRiComboBox();
    LBDAT2 = new RiZoneSortie();
    OBJ_69 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_74 = new JLabel();
    WRTTAR = new RiZoneSortie();
    panel9 = new JPanel();
    W2K01 = new RiZoneSortie();
    W2K02 = new RiZoneSortie();
    W2K03 = new RiZoneSortie();
    W2K04 = new RiZoneSortie();
    W2K05 = new RiZoneSortie();
    W2K06 = new RiZoneSortie();
    W2K07 = new RiZoneSortie();
    W2K08 = new RiZoneSortie();
    W2K09 = new RiZoneSortie();
    W2K10 = new RiZoneSortie();
    label29 = new RiZoneSortie();
    W2P01 = new RiZoneSortie();
    label30 = new JLabel();
    label31 = new RiZoneSortie();
    label32 = new RiZoneSortie();
    label33 = new RiZoneSortie();
    label34 = new RiZoneSortie();
    label35 = new RiZoneSortie();
    label36 = new RiZoneSortie();
    label37 = new RiZoneSortie();
    label38 = new RiZoneSortie();
    label39 = new RiZoneSortie();
    W2P02 = new RiZoneSortie();
    W2P03 = new RiZoneSortie();
    W2P04 = new RiZoneSortie();
    W2P05 = new RiZoneSortie();
    W2P06 = new RiZoneSortie();
    W2P07 = new RiZoneSortie();
    W2P08 = new RiZoneSortie();
    W2P09 = new RiZoneSortie();
    W2P10 = new RiZoneSortie();
    label40 = new JLabel();
    label41 = new JLabel();
    label42 = new JLabel();
    label43 = new JLabel();
    label44 = new JLabel();
    label45 = new JLabel();
    label46 = new JLabel();
    label47 = new JLabel();
    label48 = new JLabel();
    label49 = new JLabel();
    label50 = new JLabel();
    label51 = new JLabel();
    panel4 = new JPanel();
    label3 = new JLabel();
    label4 = new RiZoneSortie();
    label5 = new RiZoneSortie();
    P_Centre = new JPanel();
    button1 = new JButton();
    button2 = new JButton();
    panel5 = new JPanel();
    ATDT1X = new RiZoneSortie();
    OBJ_35 = new JLabel();
    ATIVA = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_30 = new JLabel();
    ATDAPX = new XRiCalendrier();
    panel2 = new JPanel();
    OBJ_60 = new JLabel();
    ATCNV = new XRiTextField();
    OBJ_62 = new JLabel();
    ATRON = new XRiComboBox();
    LBDAT = new RiZoneSortie();
    OBJ_75 = new JLabel();
    WCHGX = new XRiTextField();
    OBJ_76 = new JLabel();
    WBAS = new XRiTextField();
    panel8 = new JPanel();
    W1K01 = new RiZoneSortie();
    W1K02 = new XRiTextField();
    W1K03 = new XRiTextField();
    W1K04 = new XRiTextField();
    W1K05 = new XRiTextField();
    W1K06 = new XRiTextField();
    W1K07 = new XRiTextField();
    W1K08 = new XRiTextField();
    W1K09 = new XRiTextField();
    W1K10 = new XRiTextField();
    label6 = new RiZoneSortie();
    W1P01 = new XRiTextField();
    label7 = new JLabel();
    label8 = new RiZoneSortie();
    label9 = new RiZoneSortie();
    label10 = new RiZoneSortie();
    label11 = new RiZoneSortie();
    label12 = new RiZoneSortie();
    label13 = new RiZoneSortie();
    label14 = new RiZoneSortie();
    label15 = new RiZoneSortie();
    label16 = new RiZoneSortie();
    W1P02 = new XRiTextField();
    W1P03 = new XRiTextField();
    W1P04 = new XRiTextField();
    W1P05 = new XRiTextField();
    W1P06 = new XRiTextField();
    W1P07 = new XRiTextField();
    W1P08 = new XRiTextField();
    W1P09 = new XRiTextField();
    W1P10 = new XRiTextField();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    label23 = new JLabel();
    label24 = new JLabel();
    label25 = new JLabel();
    label26 = new JLabel();
    label27 = new JLabel();
    label28 = new JLabel();
    T1P01 = new XRiTextField();
    T1P02 = new XRiTextField();
    T1P03 = new XRiTextField();
    T1P04 = new XRiTextField();
    T1P05 = new XRiTextField();
    T1P06 = new XRiTextField();
    T1P07 = new XRiTextField();
    T1P08 = new XRiTextField();
    T1P09 = new XRiTextField();
    T1P10 = new XRiTextField();
    panel3 = new JPanel();
    label1 = new JLabel();
    ATDEV = new XRiTextField();
    label2 = new RiZoneSortie();
    panel7 = new JPanel();
    riBoutonDetail1 = new SNBoutonDetail();
    DEV = new XRiTextField();
    LIBDEV = new RiZoneSortie();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(890, 635));
    setPreferredSize(new Dimension(890, 575));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Historique tarifs");
            bouton_retour.setToolTipText("Historique tarifs");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
          
          // ======== navig_fin ========
          {
            navig_fin.setName("navig_fin");
            
            // ---- bouton_fin ----
            bouton_fin.setText("Retour");
            bouton_fin.setToolTipText("Retour sur l'article");
            bouton_fin.setName("bouton_fin");
            bouton_fin.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_bt3ActionPerformed(e);
              }
            });
            navig_fin.add(bouton_fin);
          }
          menus_bas.add(navig_fin);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 350));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("conditions de ventes");
              riSousMenu_bt6.setToolTipText("conditions de ventes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed();
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Coefficient sur tarif");
              riSousMenu_bt8.setToolTipText("Application du coefficient sur ce tarif");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed();
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel6 ========
        {
          panel6.setBorder(new TitledBorder(""));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- ATDAX2 ----
          ATDAX2.setName("ATDAX2");
          panel6.add(ATDAX2);
          ATDAX2.setBounds(190, 15, 105, ATDAX2.getPreferredSize().height);
          
          // ---- OBJ_31 ----
          OBJ_31.setText("Date validation");
          OBJ_31.setName("OBJ_31");
          panel6.add(OBJ_31);
          OBJ_31.setBounds(15, 332, 100, 20);
          
          // ---- LIBENC2 ----
          LIBENC2.setText("@LBENC2@");
          LIBENC2.setHorizontalAlignment(SwingConstants.RIGHT);
          LIBENC2.setName("LIBENC2");
          panel6.add(LIBENC2);
          LIBENC2.setBounds(35, 19, 145, 20);
          
          // ---- ATDT2X ----
          ATDT2X.setComponentPopupMenu(null);
          ATDT2X.setText("@ATDT2X@");
          ATDT2X.setHorizontalAlignment(SwingConstants.CENTER);
          ATDT2X.setName("ATDT2X");
          panel6.add(ATDT2X);
          ATDT2X.setBounds(110, 330, 70, ATDT2X.getPreferredSize().height);
          
          // ---- ATIVA2 ----
          ATIVA2.setComponentPopupMenu(null);
          ATIVA2.setText("@ATIVA2@");
          ATIVA2.setName("ATIVA2");
          panel6.add(ATIVA2);
          ATIVA2.setBounds(260, 330, 40, ATIVA2.getPreferredSize().height);
          
          // ---- OBJ_33 ----
          OBJ_33.setText("par");
          OBJ_33.setName("OBJ_33");
          panel6.add(OBJ_33);
          OBJ_33.setBounds(220, 332, 35, 20);
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- OBJ_56 ----
            OBJ_56.setText("Regroupement");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(5, 5, 93, 20);
            
            // ---- ATCNV2 ----
            ATCNV2.setComponentPopupMenu(null);
            ATCNV2.setText("@ATCNV2@");
            ATCNV2.setToolTipText("Regroupement CNV");
            ATCNV2.setName("ATCNV2");
            panel1.add(ATCNV2);
            ATCNV2.setBounds(100, 3, 60, ATCNV2.getPreferredSize().height);
            
            // ---- OBJ_58 ----
            OBJ_58.setText("Arrondi");
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58);
            OBJ_58.setBounds(170, 5, 55, 20);
            
            // ---- ATRON2 ----
            ATRON2.setModel(new DefaultComboBoxModel(new String[] { "aucun", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00" }));
            ATRON2.setComponentPopupMenu(BTD);
            ATRON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ATRON2.setName("ATRON2");
            panel1.add(ATRON2);
            ATRON2.setBounds(225, 2, 70, ATRON2.getPreferredSize().height);
            
            // ---- LBDAT2 ----
            LBDAT2.setText("@LBDAT2@");
            LBDAT2.setComponentPopupMenu(null);
            LBDAT2.setName("LBDAT2");
            panel1.add(LBDAT2);
            LBDAT2.setBounds(5, 35, 290, LBDAT2.getPreferredSize().height);
            
            // ---- OBJ_69 ----
            OBJ_69.setText("Rattachement");
            OBJ_69.setName("OBJ_69");
            panel1.add(OBJ_69);
            OBJ_69.setBounds(5, 67, 90, 20);
            
            // ---- OBJ_73 ----
            OBJ_73.setText("@TAART@");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(100, 67, 118, 20);
            
            // ---- OBJ_74 ----
            OBJ_74.setText("@RTLIBR@");
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(5, 100, 98, 20);
            
            // ---- WRTTAR ----
            WRTTAR.setComponentPopupMenu(null);
            WRTTAR.setText("@WRTTAR@");
            WRTTAR.setName("WRTTAR");
            panel1.add(WRTTAR);
            WRTTAR.setBounds(100, 65, 60, WRTTAR.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          panel6.add(panel1);
          panel1.setBounds(10, 355, 305, 120);
          
          // ======== panel9 ========
          {
            panel9.setOpaque(false);
            panel9.setName("panel9");
            panel9.setLayout(null);
            
            // ---- W2K01 ----
            W2K01.setFont(W2K01.getFont().deriveFont(W2K01.getFont().getStyle() | Font.BOLD));
            W2K01.setText("@W2K01@");
            W2K01.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K01.setName("W2K01");
            panel9.add(W2K01);
            W2K01.setBounds(145, 25, 54, W2K01.getPreferredSize().height);
            
            // ---- W2K02 ----
            W2K02.setText("@W2K02@");
            W2K02.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K02.setName("W2K02");
            panel9.add(W2K02);
            W2K02.setBounds(145, 50, 54, W2K02.getPreferredSize().height);
            
            // ---- W2K03 ----
            W2K03.setText("@W2K03@");
            W2K03.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K03.setName("W2K03");
            panel9.add(W2K03);
            W2K03.setBounds(145, 75, 54, W2K03.getPreferredSize().height);
            
            // ---- W2K04 ----
            W2K04.setText("@W2K04@");
            W2K04.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K04.setName("W2K04");
            panel9.add(W2K04);
            W2K04.setBounds(145, 100, 54, W2K04.getPreferredSize().height);
            
            // ---- W2K05 ----
            W2K05.setText("@W2K05@");
            W2K05.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K05.setName("W2K05");
            panel9.add(W2K05);
            W2K05.setBounds(145, 125, 54, W2K05.getPreferredSize().height);
            
            // ---- W2K06 ----
            W2K06.setText("@W2K06@");
            W2K06.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K06.setName("W2K06");
            panel9.add(W2K06);
            W2K06.setBounds(145, 150, 54, W2K06.getPreferredSize().height);
            
            // ---- W2K07 ----
            W2K07.setText("@W2K07@");
            W2K07.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K07.setName("W2K07");
            panel9.add(W2K07);
            W2K07.setBounds(145, 175, 54, W2K07.getPreferredSize().height);
            
            // ---- W2K08 ----
            W2K08.setText("@W2K08@");
            W2K08.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K08.setName("W2K08");
            panel9.add(W2K08);
            W2K08.setBounds(145, 200, 54, W2K08.getPreferredSize().height);
            
            // ---- W2K09 ----
            W2K09.setText("@W2K09@");
            W2K09.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K09.setName("W2K09");
            panel9.add(W2K09);
            W2K09.setBounds(145, 225, 54, W2K09.getPreferredSize().height);
            
            // ---- W2K10 ----
            W2K10.setText("@W2K10@");
            W2K10.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K10.setName("W2K10");
            panel9.add(W2K10);
            W2K10.setBounds(145, 250, 54, W2K10.getPreferredSize().height);
            
            // ---- label29 ----
            label29.setText("@TALS1@");
            label29.setName("label29");
            panel9.add(label29);
            label29.setBounds(30, 25, 114, label29.getPreferredSize().height);
            
            // ---- W2P01 ----
            W2P01.setText("@W2P01@");
            W2P01.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P01.setName("W2P01");
            panel9.add(W2P01);
            W2P01.setBounds(205, 25, 92, W2P01.getPreferredSize().height);
            
            // ---- label30 ----
            label30.setText("1");
            label30.setFont(label30.getFont().deriveFont(label30.getFont().getStyle() | Font.BOLD, label30.getFont().getSize() + 2f));
            label30.setHorizontalAlignment(SwingConstants.CENTER);
            label30.setName("label30");
            panel9.add(label30);
            label30.setBounds(5, 20, 20, 28);
            
            // ---- label31 ----
            label31.setText("@TALS2@");
            label31.setName("label31");
            panel9.add(label31);
            label31.setBounds(30, 50, 114, label31.getPreferredSize().height);
            
            // ---- label32 ----
            label32.setText("@TALS3@");
            label32.setName("label32");
            panel9.add(label32);
            label32.setBounds(30, 75, 114, label32.getPreferredSize().height);
            
            // ---- label33 ----
            label33.setText("@TALS4@");
            label33.setName("label33");
            panel9.add(label33);
            label33.setBounds(30, 100, 114, label33.getPreferredSize().height);
            
            // ---- label34 ----
            label34.setText("@TALS5@");
            label34.setName("label34");
            panel9.add(label34);
            label34.setBounds(30, 125, 114, label34.getPreferredSize().height);
            
            // ---- label35 ----
            label35.setText("@TALS6@");
            label35.setName("label35");
            panel9.add(label35);
            label35.setBounds(30, 150, 114, label35.getPreferredSize().height);
            
            // ---- label36 ----
            label36.setText("@TALS7@");
            label36.setName("label36");
            panel9.add(label36);
            label36.setBounds(30, 175, 114, label36.getPreferredSize().height);
            
            // ---- label37 ----
            label37.setText("@TALS8@");
            label37.setName("label37");
            panel9.add(label37);
            label37.setBounds(30, 200, 114, label37.getPreferredSize().height);
            
            // ---- label38 ----
            label38.setText("@TALS9@");
            label38.setName("label38");
            panel9.add(label38);
            label38.setBounds(30, 225, 114, label38.getPreferredSize().height);
            
            // ---- label39 ----
            label39.setText("@TALS10@");
            label39.setName("label39");
            panel9.add(label39);
            label39.setBounds(30, 250, 114, label39.getPreferredSize().height);
            
            // ---- W2P02 ----
            W2P02.setText("@W2P02@");
            W2P02.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P02.setName("W2P02");
            panel9.add(W2P02);
            W2P02.setBounds(205, 50, 92, W2P02.getPreferredSize().height);
            
            // ---- W2P03 ----
            W2P03.setText("@W2P03@");
            W2P03.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P03.setName("W2P03");
            panel9.add(W2P03);
            W2P03.setBounds(205, 75, 92, W2P03.getPreferredSize().height);
            
            // ---- W2P04 ----
            W2P04.setText("@W2P04@");
            W2P04.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P04.setName("W2P04");
            panel9.add(W2P04);
            W2P04.setBounds(205, 100, 92, W2P04.getPreferredSize().height);
            
            // ---- W2P05 ----
            W2P05.setText("@W2P05@");
            W2P05.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P05.setName("W2P05");
            panel9.add(W2P05);
            W2P05.setBounds(205, 125, 92, W2P05.getPreferredSize().height);
            
            // ---- W2P06 ----
            W2P06.setText("@W2P06@");
            W2P06.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P06.setName("W2P06");
            panel9.add(W2P06);
            W2P06.setBounds(205, 150, 92, W2P06.getPreferredSize().height);
            
            // ---- W2P07 ----
            W2P07.setText("@W2P07@");
            W2P07.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P07.setName("W2P07");
            panel9.add(W2P07);
            W2P07.setBounds(205, 175, 92, W2P07.getPreferredSize().height);
            
            // ---- W2P08 ----
            W2P08.setText("@W2P08@");
            W2P08.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P08.setName("W2P08");
            panel9.add(W2P08);
            W2P08.setBounds(205, 200, 92, W2P08.getPreferredSize().height);
            
            // ---- W2P09 ----
            W2P09.setText("@W2P09@");
            W2P09.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P09.setName("W2P09");
            panel9.add(W2P09);
            W2P09.setBounds(205, 225, 92, W2P09.getPreferredSize().height);
            
            // ---- W2P10 ----
            W2P10.setText("@W2P10@");
            W2P10.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P10.setName("W2P10");
            panel9.add(W2P10);
            W2P10.setBounds(205, 250, 92, W2P10.getPreferredSize().height);
            
            // ---- label40 ----
            label40.setText("2");
            label40.setFont(label40.getFont().deriveFont(label40.getFont().getStyle() | Font.BOLD, label40.getFont().getSize() + 2f));
            label40.setHorizontalAlignment(SwingConstants.CENTER);
            label40.setName("label40");
            panel9.add(label40);
            label40.setBounds(5, 45, 20, 28);
            
            // ---- label41 ----
            label41.setText("3");
            label41.setFont(label41.getFont().deriveFont(label41.getFont().getStyle() | Font.BOLD, label41.getFont().getSize() + 2f));
            label41.setHorizontalAlignment(SwingConstants.CENTER);
            label41.setName("label41");
            panel9.add(label41);
            label41.setBounds(5, 70, 20, 28);
            
            // ---- label42 ----
            label42.setText("4");
            label42.setFont(label42.getFont().deriveFont(label42.getFont().getStyle() | Font.BOLD, label42.getFont().getSize() + 2f));
            label42.setHorizontalAlignment(SwingConstants.CENTER);
            label42.setName("label42");
            panel9.add(label42);
            label42.setBounds(5, 95, 20, 28);
            
            // ---- label43 ----
            label43.setText("5");
            label43.setFont(label43.getFont().deriveFont(label43.getFont().getStyle() | Font.BOLD, label43.getFont().getSize() + 2f));
            label43.setHorizontalAlignment(SwingConstants.CENTER);
            label43.setName("label43");
            panel9.add(label43);
            label43.setBounds(5, 120, 20, 28);
            
            // ---- label44 ----
            label44.setText("6");
            label44.setFont(label44.getFont().deriveFont(label44.getFont().getStyle() | Font.BOLD, label44.getFont().getSize() + 2f));
            label44.setHorizontalAlignment(SwingConstants.CENTER);
            label44.setName("label44");
            panel9.add(label44);
            label44.setBounds(5, 145, 20, 28);
            
            // ---- label45 ----
            label45.setText("7");
            label45.setFont(label45.getFont().deriveFont(label45.getFont().getStyle() | Font.BOLD, label45.getFont().getSize() + 2f));
            label45.setHorizontalAlignment(SwingConstants.CENTER);
            label45.setName("label45");
            panel9.add(label45);
            label45.setBounds(5, 170, 20, 28);
            
            // ---- label46 ----
            label46.setText("8");
            label46.setFont(label46.getFont().deriveFont(label46.getFont().getStyle() | Font.BOLD, label46.getFont().getSize() + 2f));
            label46.setHorizontalAlignment(SwingConstants.CENTER);
            label46.setName("label46");
            panel9.add(label46);
            label46.setBounds(5, 195, 20, 28);
            
            // ---- label47 ----
            label47.setText("9");
            label47.setFont(label47.getFont().deriveFont(label47.getFont().getStyle() | Font.BOLD, label47.getFont().getSize() + 2f));
            label47.setHorizontalAlignment(SwingConstants.CENTER);
            label47.setName("label47");
            panel9.add(label47);
            label47.setBounds(5, 220, 20, 28);
            
            // ---- label48 ----
            label48.setText("10");
            label48.setFont(label48.getFont().deriveFont(label48.getFont().getStyle() | Font.BOLD, label48.getFont().getSize() + 2f));
            label48.setHorizontalAlignment(SwingConstants.CENTER);
            label48.setName("label48");
            panel9.add(label48);
            label48.setBounds(5, 245, 20, 28);
            
            // ---- label49 ----
            label49.setText("Libell\u00e9");
            label49.setFont(label49.getFont().deriveFont(label49.getFont().getStyle() | Font.BOLD));
            label49.setName("label49");
            panel9.add(label49);
            label49.setBounds(30, 0, 114, 25);
            
            // ---- label50 ----
            label50.setText("Coeff.");
            label50.setFont(label50.getFont().deriveFont(label50.getFont().getStyle() | Font.BOLD));
            label50.setName("label50");
            panel9.add(label50);
            label50.setBounds(145, 0, 53, 25);
            
            // ---- label51 ----
            label51.setText("Prix");
            label51.setFont(label51.getFont().deriveFont(label51.getFont().getStyle() | Font.BOLD));
            label51.setName("label51");
            panel9.add(label51);
            label51.setBounds(205, 0, 92, 25);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel9.getComponentCount(); i++) {
                Rectangle bounds = panel9.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel9.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel9.setMinimumSize(preferredSize);
              panel9.setPreferredSize(preferredSize);
            }
          }
          panel6.add(panel9);
          panel9.setBounds(5, 45, 300, 280);
          
          // ======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);
            
            // ---- label3 ----
            label3.setText("Devise");
            label3.setName("label3");
            panel4.add(label3);
            label3.setBounds(0, 15, 50, 24);
            
            // ---- label4 ----
            label4.setText("@ATDEV2@");
            label4.setComponentPopupMenu(null);
            label4.setName("label4");
            panel4.add(label4);
            label4.setBounds(52, 15, 40, label4.getPreferredSize().height);
            
            // ---- label5 ----
            label5.setText("@DVLIB2@");
            label5.setComponentPopupMenu(null);
            label5.setName("label5");
            panel4.add(label5);
            label5.setBounds(102, 15, 188, label5.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          panel6.add(panel4);
          panel4.setBounds(15, 475, 295, 50);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel6);
        panel6.setBounds(35, 15, 320, 540);
        
        // ======== P_Centre ========
        {
          P_Centre.setOpaque(false);
          P_Centre.setName("P_Centre");
          P_Centre.setLayout(null);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < P_Centre.getComponentCount(); i++) {
              Rectangle bounds = P_Centre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = P_Centre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            P_Centre.setMinimumSize(preferredSize);
            P_Centre.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(P_Centre);
        P_Centre.setBounds(new Rectangle(new Point(35, 15), P_Centre.getPreferredSize()));
        
        // ---- button1 ----
        button1.setToolTipText("Tarifs pr\u00e9c\u00e9dents");
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button1ActionPerformed();
          }
        });
        p_contenu.add(button1);
        button1.setBounds(5, 15, 25, 535);
        
        // ---- button2 ----
        button2.setToolTipText("Tarifs suivants");
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button2ActionPerformed();
          }
        });
        p_contenu.add(button2);
        button2.setBounds(690, 15, 25, 535);
        
        // ======== panel5 ========
        {
          panel5.setBorder(new TitledBorder(""));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);
          
          // ---- ATDT1X ----
          ATDT1X.setComponentPopupMenu(null);
          ATDT1X.setText("@ATDT1X@");
          ATDT1X.setHorizontalAlignment(SwingConstants.CENTER);
          ATDT1X.setName("ATDT1X");
          panel5.add(ATDT1X);
          ATDT1X.setBounds(110, 330, 70, ATDT1X.getPreferredSize().height);
          
          // ---- OBJ_35 ----
          OBJ_35.setText("Date validation");
          OBJ_35.setName("OBJ_35");
          panel5.add(OBJ_35);
          OBJ_35.setBounds(15, 332, 100, 20);
          
          // ---- ATIVA ----
          ATIVA.setComponentPopupMenu(null);
          ATIVA.setText("@ATIVA@");
          ATIVA.setName("ATIVA");
          panel5.add(ATIVA);
          ATIVA.setBounds(255, 330, 40, ATIVA.getPreferredSize().height);
          
          // ---- OBJ_37 ----
          OBJ_37.setText("par");
          OBJ_37.setName("OBJ_37");
          panel5.add(OBJ_37);
          OBJ_37.setBounds(220, 330, 40, 25);
          
          // ---- OBJ_30 ----
          OBJ_30.setText("@LBENC1@");
          OBJ_30.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_30.setName("OBJ_30");
          panel5.add(OBJ_30);
          OBJ_30.setBounds(35, 19, 145, 20);
          
          // ---- ATDAPX ----
          ATDAPX.setName("ATDAPX");
          panel5.add(ATDAPX);
          ATDAPX.setBounds(190, 15, 105, ATDAPX.getPreferredSize().height);
          
          // ======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- OBJ_60 ----
            OBJ_60.setText("Regroupement");
            OBJ_60.setName("OBJ_60");
            panel2.add(OBJ_60);
            OBJ_60.setBounds(5, 5, 93, 20);
            
            // ---- ATCNV ----
            ATCNV.setComponentPopupMenu(null);
            ATCNV.setToolTipText("Regroupement CNV");
            ATCNV.setName("ATCNV");
            panel2.add(ATCNV);
            ATCNV.setBounds(100, 1, 60, ATCNV.getPreferredSize().height);
            
            // ---- OBJ_62 ----
            OBJ_62.setText("Arrondi");
            OBJ_62.setName("OBJ_62");
            panel2.add(OBJ_62);
            OBJ_62.setBounds(170, 5, 53, 20);
            
            // ---- ATRON ----
            ATRON.setModel(new DefaultComboBoxModel(new String[] { "aucun", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00" }));
            ATRON.setComponentPopupMenu(BTD);
            ATRON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ATRON.setName("ATRON");
            panel2.add(ATRON);
            ATRON.setBounds(225, 2, 70, ATRON.getPreferredSize().height);
            
            // ---- LBDAT ----
            LBDAT.setText("@LBDAT@");
            LBDAT.setComponentPopupMenu(null);
            LBDAT.setName("LBDAT");
            panel2.add(LBDAT);
            LBDAT.setBounds(5, 35, 285, LBDAT.getPreferredSize().height);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("Change");
            OBJ_75.setName("OBJ_75");
            panel2.add(OBJ_75);
            OBJ_75.setBounds(5, 70, 60, 18);
            
            // ---- WCHGX ----
            WCHGX.setComponentPopupMenu(BTD);
            WCHGX.setHorizontalAlignment(SwingConstants.RIGHT);
            WCHGX.setName("WCHGX");
            panel2.add(WCHGX);
            WCHGX.setBounds(100, 65, 90, WCHGX.getPreferredSize().height);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("@BASE@");
            OBJ_76.setName("OBJ_76");
            panel2.add(OBJ_76);
            OBJ_76.setBounds(5, 100, 85, 18);
            
            // ---- WBAS ----
            WBAS.setComponentPopupMenu(BTD);
            WBAS.setHorizontalAlignment(SwingConstants.RIGHT);
            WBAS.setName("WBAS");
            panel2.add(WBAS);
            WBAS.setBounds(100, 95, 60, WBAS.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel5.add(panel2);
          panel2.setBounds(10, 355, 296, panel2.getPreferredSize().height);
          
          // ======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(null);
            
            // ---- W1K01 ----
            W1K01.setFont(W1K01.getFont().deriveFont(W1K01.getFont().getStyle() | Font.BOLD));
            W1K01.setText("@W1K01@");
            W1K01.setHorizontalAlignment(SwingConstants.RIGHT);
            W1K01.setName("W1K01");
            panel8.add(W1K01);
            W1K01.setBounds(140, 27, 54, W1K01.getPreferredSize().height);
            
            // ---- W1K02 ----
            W1K02.setName("W1K02");
            panel8.add(W1K02);
            W1K02.setBounds(140, 50, 54, W1K02.getPreferredSize().height);
            
            // ---- W1K03 ----
            W1K03.setName("W1K03");
            panel8.add(W1K03);
            W1K03.setBounds(140, 75, 54, W1K03.getPreferredSize().height);
            
            // ---- W1K04 ----
            W1K04.setName("W1K04");
            panel8.add(W1K04);
            W1K04.setBounds(140, 100, 54, W1K04.getPreferredSize().height);
            
            // ---- W1K05 ----
            W1K05.setName("W1K05");
            panel8.add(W1K05);
            W1K05.setBounds(140, 125, 54, W1K05.getPreferredSize().height);
            
            // ---- W1K06 ----
            W1K06.setName("W1K06");
            panel8.add(W1K06);
            W1K06.setBounds(140, 150, 54, W1K06.getPreferredSize().height);
            
            // ---- W1K07 ----
            W1K07.setName("W1K07");
            panel8.add(W1K07);
            W1K07.setBounds(140, 175, 54, W1K07.getPreferredSize().height);
            
            // ---- W1K08 ----
            W1K08.setName("W1K08");
            panel8.add(W1K08);
            W1K08.setBounds(140, 200, 54, W1K08.getPreferredSize().height);
            
            // ---- W1K09 ----
            W1K09.setName("W1K09");
            panel8.add(W1K09);
            W1K09.setBounds(140, 225, 54, W1K09.getPreferredSize().height);
            
            // ---- W1K10 ----
            W1K10.setName("W1K10");
            panel8.add(W1K10);
            W1K10.setBounds(140, 250, 54, W1K10.getPreferredSize().height);
            
            // ---- label6 ----
            label6.setText("@TALB1@");
            label6.setName("label6");
            panel8.add(label6);
            label6.setBounds(25, 27, 114, label6.getPreferredSize().height);
            
            // ---- W1P01 ----
            W1P01.setName("W1P01");
            panel8.add(W1P01);
            W1P01.setBounds(195, 25, 92, W1P01.getPreferredSize().height);
            
            // ---- label7 ----
            label7.setText("1");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD, label7.getFont().getSize() + 2f));
            label7.setHorizontalAlignment(SwingConstants.CENTER);
            label7.setName("label7");
            panel8.add(label7);
            label7.setBounds(0, 25, 20, 28);
            
            // ---- label8 ----
            label8.setText("@TALB2@");
            label8.setName("label8");
            panel8.add(label8);
            label8.setBounds(25, 52, 114, label8.getPreferredSize().height);
            
            // ---- label9 ----
            label9.setText("@TALB3@");
            label9.setName("label9");
            panel8.add(label9);
            label9.setBounds(25, 77, 114, label9.getPreferredSize().height);
            
            // ---- label10 ----
            label10.setText("@TALB4@");
            label10.setName("label10");
            panel8.add(label10);
            label10.setBounds(25, 102, 114, label10.getPreferredSize().height);
            
            // ---- label11 ----
            label11.setText("@TALB5@");
            label11.setName("label11");
            panel8.add(label11);
            label11.setBounds(25, 127, 114, label11.getPreferredSize().height);
            
            // ---- label12 ----
            label12.setText("@TALB6@");
            label12.setName("label12");
            panel8.add(label12);
            label12.setBounds(25, 152, 114, label12.getPreferredSize().height);
            
            // ---- label13 ----
            label13.setText("@TALB7@");
            label13.setName("label13");
            panel8.add(label13);
            label13.setBounds(25, 177, 114, label13.getPreferredSize().height);
            
            // ---- label14 ----
            label14.setText("@TALB8@");
            label14.setName("label14");
            panel8.add(label14);
            label14.setBounds(25, 202, 114, label14.getPreferredSize().height);
            
            // ---- label15 ----
            label15.setText("@TALB9@");
            label15.setName("label15");
            panel8.add(label15);
            label15.setBounds(25, 227, 114, label15.getPreferredSize().height);
            
            // ---- label16 ----
            label16.setText("@TALB10@");
            label16.setName("label16");
            panel8.add(label16);
            label16.setBounds(25, 252, 114, label16.getPreferredSize().height);
            
            // ---- W1P02 ----
            W1P02.setName("W1P02");
            panel8.add(W1P02);
            W1P02.setBounds(195, 50, 92, W1P02.getPreferredSize().height);
            
            // ---- W1P03 ----
            W1P03.setName("W1P03");
            panel8.add(W1P03);
            W1P03.setBounds(195, 75, 92, W1P03.getPreferredSize().height);
            
            // ---- W1P04 ----
            W1P04.setName("W1P04");
            panel8.add(W1P04);
            W1P04.setBounds(195, 100, 92, W1P04.getPreferredSize().height);
            
            // ---- W1P05 ----
            W1P05.setName("W1P05");
            panel8.add(W1P05);
            W1P05.setBounds(195, 125, 92, W1P05.getPreferredSize().height);
            
            // ---- W1P06 ----
            W1P06.setName("W1P06");
            panel8.add(W1P06);
            W1P06.setBounds(195, 150, 92, W1P06.getPreferredSize().height);
            
            // ---- W1P07 ----
            W1P07.setName("W1P07");
            panel8.add(W1P07);
            W1P07.setBounds(195, 175, 92, W1P07.getPreferredSize().height);
            
            // ---- W1P08 ----
            W1P08.setName("W1P08");
            panel8.add(W1P08);
            W1P08.setBounds(195, 200, 92, W1P08.getPreferredSize().height);
            
            // ---- W1P09 ----
            W1P09.setName("W1P09");
            panel8.add(W1P09);
            W1P09.setBounds(195, 225, 92, W1P09.getPreferredSize().height);
            
            // ---- W1P10 ----
            W1P10.setName("W1P10");
            panel8.add(W1P10);
            W1P10.setBounds(195, 250, 92, W1P10.getPreferredSize().height);
            
            // ---- label17 ----
            label17.setText("2");
            label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD, label17.getFont().getSize() + 2f));
            label17.setHorizontalAlignment(SwingConstants.CENTER);
            label17.setName("label17");
            panel8.add(label17);
            label17.setBounds(0, 50, 20, 28);
            
            // ---- label18 ----
            label18.setText("3");
            label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD, label18.getFont().getSize() + 2f));
            label18.setHorizontalAlignment(SwingConstants.CENTER);
            label18.setName("label18");
            panel8.add(label18);
            label18.setBounds(0, 75, 20, 28);
            
            // ---- label19 ----
            label19.setText("4");
            label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD, label19.getFont().getSize() + 2f));
            label19.setHorizontalAlignment(SwingConstants.CENTER);
            label19.setName("label19");
            panel8.add(label19);
            label19.setBounds(0, 100, 20, 28);
            
            // ---- label20 ----
            label20.setText("5");
            label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD, label20.getFont().getSize() + 2f));
            label20.setHorizontalAlignment(SwingConstants.CENTER);
            label20.setName("label20");
            panel8.add(label20);
            label20.setBounds(0, 125, 20, 28);
            
            // ---- label21 ----
            label21.setText("6");
            label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD, label21.getFont().getSize() + 2f));
            label21.setHorizontalAlignment(SwingConstants.CENTER);
            label21.setName("label21");
            panel8.add(label21);
            label21.setBounds(0, 150, 20, 28);
            
            // ---- label22 ----
            label22.setText("7");
            label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD, label22.getFont().getSize() + 2f));
            label22.setHorizontalAlignment(SwingConstants.CENTER);
            label22.setName("label22");
            panel8.add(label22);
            label22.setBounds(0, 175, 20, 28);
            
            // ---- label23 ----
            label23.setText("8");
            label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD, label23.getFont().getSize() + 2f));
            label23.setHorizontalAlignment(SwingConstants.CENTER);
            label23.setName("label23");
            panel8.add(label23);
            label23.setBounds(0, 200, 20, 28);
            
            // ---- label24 ----
            label24.setText("9");
            label24.setFont(label24.getFont().deriveFont(label24.getFont().getStyle() | Font.BOLD, label24.getFont().getSize() + 2f));
            label24.setHorizontalAlignment(SwingConstants.CENTER);
            label24.setName("label24");
            panel8.add(label24);
            label24.setBounds(0, 225, 20, 28);
            
            // ---- label25 ----
            label25.setText("10");
            label25.setFont(label25.getFont().deriveFont(label25.getFont().getStyle() | Font.BOLD, label25.getFont().getSize() + 2f));
            label25.setHorizontalAlignment(SwingConstants.CENTER);
            label25.setName("label25");
            panel8.add(label25);
            label25.setBounds(0, 250, 20, 28);
            
            // ---- label26 ----
            label26.setText("Libell\u00e9");
            label26.setFont(label26.getFont().deriveFont(label26.getFont().getStyle() | Font.BOLD));
            label26.setName("label26");
            panel8.add(label26);
            label26.setBounds(25, 5, 114, 20);
            
            // ---- label27 ----
            label27.setText("Coeff.");
            label27.setFont(label27.getFont().deriveFont(label27.getFont().getStyle() | Font.BOLD));
            label27.setName("label27");
            panel8.add(label27);
            label27.setBounds(140, 5, 53, 20);
            
            // ---- label28 ----
            label28.setText("Prix");
            label28.setFont(label28.getFont().deriveFont(label28.getFont().getStyle() | Font.BOLD));
            label28.setName("label28");
            panel8.add(label28);
            label28.setBounds(200, 5, 85, 20);
            
            // ---- T1P01 ----
            T1P01.setName("T1P01");
            panel8.add(T1P01);
            T1P01.setBounds(195, 25, 92, 28);
            
            // ---- T1P02 ----
            T1P02.setName("T1P02");
            panel8.add(T1P02);
            T1P02.setBounds(195, 50, 92, 28);
            
            // ---- T1P03 ----
            T1P03.setName("T1P03");
            panel8.add(T1P03);
            T1P03.setBounds(195, 75, 92, 28);
            
            // ---- T1P04 ----
            T1P04.setName("T1P04");
            panel8.add(T1P04);
            T1P04.setBounds(195, 100, 92, 28);
            
            // ---- T1P05 ----
            T1P05.setName("T1P05");
            panel8.add(T1P05);
            T1P05.setBounds(195, 125, 92, 28);
            
            // ---- T1P06 ----
            T1P06.setName("T1P06");
            panel8.add(T1P06);
            T1P06.setBounds(195, 150, 92, 28);
            
            // ---- T1P07 ----
            T1P07.setName("T1P07");
            panel8.add(T1P07);
            T1P07.setBounds(195, 175, 92, 28);
            
            // ---- T1P08 ----
            T1P08.setName("T1P08");
            panel8.add(T1P08);
            T1P08.setBounds(195, 200, 92, 28);
            
            // ---- T1P09 ----
            T1P09.setName("T1P09");
            panel8.add(T1P09);
            T1P09.setBounds(195, 225, 92, 28);
            
            // ---- T1P10 ----
            T1P10.setName("T1P10");
            panel8.add(T1P10);
            T1P10.setBounds(195, 250, 92, 28);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }
          panel5.add(panel8);
          panel8.setBounds(10, 44, 295, 280);
          
          // ======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- label1 ----
            label1.setText("Devise");
            label1.setName("label1");
            panel3.add(label1);
            label1.setBounds(0, 13, 50, 28);
            
            // ---- ATDEV ----
            ATDEV.setName("ATDEV");
            panel3.add(ATDEV);
            ATDEV.setBounds(48, 13, 40, ATDEV.getPreferredSize().height);
            
            // ---- label2 ----
            label2.setText("@DVLIBR@");
            label2.setComponentPopupMenu(null);
            label2.setName("label2");
            panel3.add(label2);
            label2.setBounds(95, 15, 195, label2.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          panel5.add(panel3);
          panel3.setBounds(15, 475, 295, 50);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel5);
        panel5.setBounds(365, 15, 320, 540);
        
        // ======== panel7 ========
        {
          panel7.setBorder(new TitledBorder(""));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- riBoutonDetail1 ----
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          panel7.add(riBoutonDetail1);
          riBoutonDetail1.setBounds(70, 234, 25, 30);
          
          // ---- DEV ----
          DEV.setComponentPopupMenu(null);
          DEV.setName("DEV");
          DEV.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
              DEVKeyPressed(e);
            }
            
            @Override
            public void keyReleased(KeyEvent e) {
              DEVKeyReleased(e);
            }
          });
          panel7.add(DEV);
          DEV.setBounds(30, 235, 44, DEV.getPreferredSize().height);
          
          // ---- LIBDEV ----
          LIBDEV.setText("@LIBDEV@");
          LIBDEV.setName("LIBDEV");
          panel7.add(LIBDEV);
          LIBDEV.setBounds(95, 235, 180, LIBDEV.getPreferredSize().height);
          
          // ---- OBJ_51 ----
          OBJ_51.setText("Aucun tarif pr\u00e9c\u00e9dent");
          OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_51.setFont(OBJ_51.getFont().deriveFont(OBJ_51.getFont().getStyle() | Font.BOLD, OBJ_51.getFont().getSize() + 4f));
          OBJ_51.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_51.setName("OBJ_51");
          panel7.add(OBJ_51);
          OBJ_51.setBounds(25, 155, 255, 49);
          
          // ---- OBJ_52 ----
          OBJ_52.setText("sur cette devise");
          OBJ_52.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_52.setFont(OBJ_52.getFont().deriveFont(OBJ_52.getFont().getStyle() | Font.BOLD, OBJ_52.getFont().getSize() + 4f));
          OBJ_52.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_52.setName("OBJ_52");
          panel7.add(OBJ_52);
          OBJ_52.setBounds(25, 220, 255, 49);
        }
        p_contenu.add(panel7);
        panel7.setBounds(35, 15, 320, 540);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu navig_fin;
  private RiMenu_bt bouton_fin;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel panel6;
  private XRiCalendrier ATDAX2;
  private JLabel OBJ_31;
  private JLabel LIBENC2;
  private RiZoneSortie ATDT2X;
  private RiZoneSortie ATIVA2;
  private JLabel OBJ_33;
  private JPanel panel1;
  private JLabel OBJ_56;
  private RiZoneSortie ATCNV2;
  private JLabel OBJ_58;
  private XRiComboBox ATRON2;
  private RiZoneSortie LBDAT2;
  private JLabel OBJ_69;
  private JLabel OBJ_73;
  private JLabel OBJ_74;
  private RiZoneSortie WRTTAR;
  private JPanel panel9;
  private RiZoneSortie W2K01;
  private RiZoneSortie W2K02;
  private RiZoneSortie W2K03;
  private RiZoneSortie W2K04;
  private RiZoneSortie W2K05;
  private RiZoneSortie W2K06;
  private RiZoneSortie W2K07;
  private RiZoneSortie W2K08;
  private RiZoneSortie W2K09;
  private RiZoneSortie W2K10;
  private RiZoneSortie label29;
  private RiZoneSortie W2P01;
  private JLabel label30;
  private RiZoneSortie label31;
  private RiZoneSortie label32;
  private RiZoneSortie label33;
  private RiZoneSortie label34;
  private RiZoneSortie label35;
  private RiZoneSortie label36;
  private RiZoneSortie label37;
  private RiZoneSortie label38;
  private RiZoneSortie label39;
  private RiZoneSortie W2P02;
  private RiZoneSortie W2P03;
  private RiZoneSortie W2P04;
  private RiZoneSortie W2P05;
  private RiZoneSortie W2P06;
  private RiZoneSortie W2P07;
  private RiZoneSortie W2P08;
  private RiZoneSortie W2P09;
  private RiZoneSortie W2P10;
  private JLabel label40;
  private JLabel label41;
  private JLabel label42;
  private JLabel label43;
  private JLabel label44;
  private JLabel label45;
  private JLabel label46;
  private JLabel label47;
  private JLabel label48;
  private JLabel label49;
  private JLabel label50;
  private JLabel label51;
  private JPanel panel4;
  private JLabel label3;
  private RiZoneSortie label4;
  private RiZoneSortie label5;
  private JPanel P_Centre;
  private JButton button1;
  private JButton button2;
  private JPanel panel5;
  private RiZoneSortie ATDT1X;
  private JLabel OBJ_35;
  private RiZoneSortie ATIVA;
  private JLabel OBJ_37;
  private JLabel OBJ_30;
  private XRiCalendrier ATDAPX;
  private JPanel panel2;
  private JLabel OBJ_60;
  private XRiTextField ATCNV;
  private JLabel OBJ_62;
  private XRiComboBox ATRON;
  private RiZoneSortie LBDAT;
  private JLabel OBJ_75;
  private XRiTextField WCHGX;
  private JLabel OBJ_76;
  private XRiTextField WBAS;
  private JPanel panel8;
  private RiZoneSortie W1K01;
  private XRiTextField W1K02;
  private XRiTextField W1K03;
  private XRiTextField W1K04;
  private XRiTextField W1K05;
  private XRiTextField W1K06;
  private XRiTextField W1K07;
  private XRiTextField W1K08;
  private XRiTextField W1K09;
  private XRiTextField W1K10;
  private RiZoneSortie label6;
  private XRiTextField W1P01;
  private JLabel label7;
  private RiZoneSortie label8;
  private RiZoneSortie label9;
  private RiZoneSortie label10;
  private RiZoneSortie label11;
  private RiZoneSortie label12;
  private RiZoneSortie label13;
  private RiZoneSortie label14;
  private RiZoneSortie label15;
  private RiZoneSortie label16;
  private XRiTextField W1P02;
  private XRiTextField W1P03;
  private XRiTextField W1P04;
  private XRiTextField W1P05;
  private XRiTextField W1P06;
  private XRiTextField W1P07;
  private XRiTextField W1P08;
  private XRiTextField W1P09;
  private XRiTextField W1P10;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private JLabel label23;
  private JLabel label24;
  private JLabel label25;
  private JLabel label26;
  private JLabel label27;
  private JLabel label28;
  private XRiTextField T1P01;
  private XRiTextField T1P02;
  private XRiTextField T1P03;
  private XRiTextField T1P04;
  private XRiTextField T1P05;
  private XRiTextField T1P06;
  private XRiTextField T1P07;
  private XRiTextField T1P08;
  private XRiTextField T1P09;
  private XRiTextField T1P10;
  private JPanel panel3;
  private JLabel label1;
  private XRiTextField ATDEV;
  private RiZoneSortie label2;
  private JPanel panel7;
  private SNBoutonDetail riBoutonDetail1;
  private XRiTextField DEV;
  private RiZoneSortie LIBDEV;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
