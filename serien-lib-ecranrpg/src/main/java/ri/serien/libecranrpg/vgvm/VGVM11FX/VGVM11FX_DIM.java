
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_DIM extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_DIM(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    
    // Icone
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    
    // Titre
    setTitle("Les dimensions");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_135.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DIME@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    p_contenu = new JPanel();
    panel10 = new JPanel();
    L2QT1X = new XRiTextField();
    L2QT2X = new XRiTextField();
    L2QT3X = new XRiTextField();
    OBJ_135 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(535, 190));
    setPreferredSize(new Dimension(510, 150));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel10 ========
        {
          panel10.setBorder(new TitledBorder("Dimensions"));
          panel10.setOpaque(false);
          panel10.setName("panel10");

          //---- L2QT1X ----
          L2QT1X.setComponentPopupMenu(null);
          L2QT1X.setName("L2QT1X");

          //---- L2QT2X ----
          L2QT2X.setComponentPopupMenu(null);
          L2QT2X.setName("L2QT2X");

          //---- L2QT3X ----
          L2QT3X.setComponentPopupMenu(null);
          L2QT3X.setName("L2QT3X");

          //---- OBJ_135 ----
          OBJ_135.setText("@DIME@");
          OBJ_135.setName("OBJ_135");

          GroupLayout panel10Layout = new GroupLayout(panel10);
          panel10.setLayout(panel10Layout);
          panel10Layout.setHorizontalGroup(
            panel10Layout.createParallelGroup()
              .addGroup(panel10Layout.createSequentialGroup()
                .addComponent(L2QT1X, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(L2QT2X, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(L2QT3X, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel10Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
          );
          panel10Layout.setVerticalGroup(
            panel10Layout.createParallelGroup()
              .addGroup(panel10Layout.createSequentialGroup()
                .addGroup(panel10Layout.createParallelGroup()
                  .addComponent(L2QT1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L2QT2X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(L2QT3X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(7, 7, 7)
                .addComponent(OBJ_135, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_contenu.add(panel10);
        panel10.setBounds(30, 20, 275, 105);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JPanel p_contenu;
  private JPanel panel10;
  private XRiTextField L2QT1X;
  private XRiTextField L2QT2X;
  private XRiTextField L2QT3X;
  private RiZoneSortie OBJ_135;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
