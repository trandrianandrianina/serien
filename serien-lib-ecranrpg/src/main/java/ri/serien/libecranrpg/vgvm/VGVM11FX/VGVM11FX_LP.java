
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_LP extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_LP(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    ART1.activerModeFantome("Article");
    ART2.activerModeFantome("Article");
    ART3.activerModeFantome("Article");
    ART4.activerModeFantome("Article");
    
    LIB1.activerModeFantome("Désignation");
    LIB2.activerModeFantome("Désignation");
    LIB3.activerModeFantome("Désignation");
    LIB4.activerModeFantome("Désignation");
    
    PVN1.activerModeFantome("Prix net");
    PVN2.activerModeFantome("Prix net");
    PVN3.activerModeFantome("Prix net");
    PVN4.activerModeFantome("Prix net");
    
    initDiverses();
    E1IN22.setValeursSelection("1", " ");
    
    setCloseKey("F4", "F5", "F20");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle("Lignes de pied");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" Err @LDPE1@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" Err @LDPE1@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" Err @LDPE1@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression(" Err @LDPE1@")).trim());
    WCOLC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOLC@")).trim());
    WPDSC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPDSC@")).trim());
    WVOLC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WVOLC@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB1@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    E1CTR.setEnabled(lexique.isPresent("E1CTR"));
    E1MEX.setEnabled(lexique.isPresent("E1MEX"));
    WMTRDV.setEnabled(lexique.isPresent("WMTRDV"));
    WHTRDV.setEnabled(lexique.isPresent("WHTRDV"));
    WNBPAL.setEnabled(lexique.isPresent("WNBPAL"));
    WVOLC.setVisible(lexique.isPresent("WVOLC"));
    WPDSC.setVisible(lexique.isPresent("WPDSC"));
    E1PDS.setEnabled(lexique.isPresent("E1PDS"));
    WCOLC.setVisible(lexique.isPresent("WCOLC"));
    E1COL.setEnabled(lexique.isPresent("E1COL"));
    W29OBS.setEnabled(lexique.isPresent("W29OBS"));
    E1VEH.setEnabled(lexique.isPresent("E1VEH"));
    OBJ_54.setVisible(lexique.isPresent("TRLIBR"));
    OBJ_52.setVisible(lexique.isPresent("EXLIB1"));
    
    label1.setVisible(!lexique.HostFieldGetData("LDPE1").trim().equals(""));
    label2.setVisible(!lexique.HostFieldGetData("LDPE2").trim().equals(""));
    label3.setVisible(!lexique.HostFieldGetData("LDPE3").trim().equals(""));
    label4.setVisible(!lexique.HostFieldGetData("LDPE4").trim().equals(""));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel6 = new JPanel();
    ART1 = new XRiTextField();
    ART2 = new XRiTextField();
    ART3 = new XRiTextField();
    ART4 = new XRiTextField();
    LIB1 = new XRiTextField();
    LIB2 = new XRiTextField();
    LIB3 = new XRiTextField();
    LIB4 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    PVN1 = new XRiTextField();
    PVN2 = new XRiTextField();
    PVN3 = new XRiTextField();
    PVN4 = new XRiTextField();
    panel1 = new JPanel();
    OBJ_30 = new JLabel();
    E1COL = new XRiTextField();
    WCOLC = new RiZoneSortie();
    OBJ_33 = new JLabel();
    WNBPAL = new XRiTextField();
    OBJ_35 = new JLabel();
    E1PDS = new XRiTextField();
    WPDSC = new RiZoneSortie();
    OBJ_38 = new JLabel();
    WVOLC = new RiZoneSortie();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    panel2 = new JPanel();
    OBJ_42 = new JLabel();
    E1MEX = new XRiTextField();
    OBJ_52 = new RiZoneSortie();
    OBJ_54 = new RiZoneSortie();
    E1CTR = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_55 = new JLabel();
    E1VEH = new XRiTextField();
    E1IN22 = new XRiCheckBox();
    OBJ_61 = new JLabel();
    OBJ_56 = new JLabel();
    WHTRDV = new XRiTextField();
    OBJ_57 = new JLabel();
    WMTRDV = new XRiTextField();
    W29OBS = new XRiTextField();
    OBJ_58 = new JLabel();
    E1DT3X = new XRiCalendrier();
    WDTRDV = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(760, 590));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Saisie code barre");
              riSousMenu_bt6.setToolTipText("Mise en fonction saisie code barre");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("LIbell\u00e9(s) sur ligne article");
              riSousMenu_bt7.setToolTipText("Saisie ou Modification de Libell\u00e9(s) sur Ligne Article");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Recherche de bons");
              riSousMenu_bt8.setToolTipText("Fonction recherche de bons");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Interruption d'une ligne");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(760, 590));
        p_contenu.setName("p_contenu");

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Articles de pied de bon"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- ART1 ----
          ART1.setFont(ART1.getFont().deriveFont(ART1.getFont().getSize() - 1f));
          ART1.setComponentPopupMenu(BTD);
          ART1.setName("ART1");
          panel6.add(ART1);
          ART1.setBounds(25, 35, 95, 25);

          //---- ART2 ----
          ART2.setFont(ART2.getFont().deriveFont(ART2.getFont().getSize() - 1f));
          ART2.setComponentPopupMenu(BTD);
          ART2.setName("ART2");
          panel6.add(ART2);
          ART2.setBounds(25, 60, 95, 25);

          //---- ART3 ----
          ART3.setFont(ART3.getFont().deriveFont(ART3.getFont().getSize() - 1f));
          ART3.setComponentPopupMenu(BTD);
          ART3.setName("ART3");
          panel6.add(ART3);
          ART3.setBounds(25, 85, 95, 25);

          //---- ART4 ----
          ART4.setFont(ART4.getFont().deriveFont(ART4.getFont().getSize() - 1f));
          ART4.setComponentPopupMenu(BTD);
          ART4.setName("ART4");
          panel6.add(ART4);
          ART4.setBounds(25, 110, 95, 25);

          //---- LIB1 ----
          LIB1.setFont(LIB1.getFont().deriveFont(LIB1.getFont().getSize() - 1f));
          LIB1.setName("LIB1");
          panel6.add(LIB1);
          LIB1.setBounds(120, 35, 270, 25);

          //---- LIB2 ----
          LIB2.setFont(LIB2.getFont().deriveFont(LIB2.getFont().getSize() - 1f));
          LIB2.setName("LIB2");
          panel6.add(LIB2);
          LIB2.setBounds(120, 60, 270, 25);

          //---- LIB3 ----
          LIB3.setFont(LIB3.getFont().deriveFont(LIB3.getFont().getSize() - 1f));
          LIB3.setName("LIB3");
          panel6.add(LIB3);
          LIB3.setBounds(120, 85, 270, 25);

          //---- LIB4 ----
          LIB4.setFont(LIB4.getFont().deriveFont(LIB4.getFont().getSize() - 1f));
          LIB4.setName("LIB4");
          panel6.add(LIB4);
          LIB4.setBounds(120, 110, 270, 25);

          //---- label1 ----
          label1.setText(" Err @LDPE1@");
          label1.setForeground(Color.red);
          label1.setName("label1");
          panel6.add(label1);
          label1.setBounds(485, 39, 70, label1.getPreferredSize().height);

          //---- label2 ----
          label2.setText(" Err @LDPE1@");
          label2.setForeground(Color.red);
          label2.setName("label2");
          panel6.add(label2);
          label2.setBounds(485, 64, 70, 16);

          //---- label3 ----
          label3.setText(" Err @LDPE1@");
          label3.setForeground(Color.red);
          label3.setName("label3");
          panel6.add(label3);
          label3.setBounds(485, 89, 70, 16);

          //---- label4 ----
          label4.setText(" Err @LDPE1@");
          label4.setForeground(Color.red);
          label4.setName("label4");
          panel6.add(label4);
          label4.setBounds(485, 114, 70, 16);

          //---- PVN1 ----
          PVN1.setName("PVN1");
          panel6.add(PVN1);
          PVN1.setBounds(390, 35, 90, 25);

          //---- PVN2 ----
          PVN2.setHorizontalAlignment(SwingConstants.RIGHT);
          PVN2.setName("PVN2");
          panel6.add(PVN2);
          PVN2.setBounds(390, 60, 90, 25);

          //---- PVN3 ----
          PVN3.setHorizontalAlignment(SwingConstants.RIGHT);
          PVN3.setName("PVN3");
          panel6.add(PVN3);
          PVN3.setBounds(390, 85, 90, 25);

          //---- PVN4 ----
          PVN4.setHorizontalAlignment(SwingConstants.RIGHT);
          PVN4.setName("PVN4");
          panel6.add(PVN4);
          PVN4.setBounds(390, 110, 90, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Colisage"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_30 ----
          OBJ_30.setText("Colis");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(20, 64, 47, 20);

          //---- E1COL ----
          E1COL.setComponentPopupMenu(BTD);
          E1COL.setName("E1COL");
          panel1.add(E1COL);
          E1COL.setBounds(73, 60, 74, E1COL.getPreferredSize().height);

          //---- WCOLC ----
          WCOLC.setComponentPopupMenu(BTD);
          WCOLC.setText("@WCOLC@");
          WCOLC.setName("WCOLC");
          panel1.add(WCOLC);
          WCOLC.setBounds(150, 62, 74, WCOLC.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("Nombre palettes");
          OBJ_33.setName("OBJ_33");
          panel1.add(OBJ_33);
          OBJ_33.setBounds(290, 64, 110, 20);

          //---- WNBPAL ----
          WNBPAL.setComponentPopupMenu(BTD);
          WNBPAL.setName("WNBPAL");
          panel1.add(WNBPAL);
          WNBPAL.setBounds(403, 60, 26, WNBPAL.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Poids");
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(20, 94, 47, 20);

          //---- E1PDS ----
          E1PDS.setComponentPopupMenu(BTD);
          E1PDS.setName("E1PDS");
          panel1.add(E1PDS);
          E1PDS.setBounds(73, 90, 74, E1PDS.getPreferredSize().height);

          //---- WPDSC ----
          WPDSC.setComponentPopupMenu(BTD);
          WPDSC.setText("@WPDSC@");
          WPDSC.setName("WPDSC");
          panel1.add(WPDSC);
          WPDSC.setBounds(150, 92, 74, WPDSC.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Volume");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(290, 94, 55, 20);

          //---- WVOLC ----
          WVOLC.setComponentPopupMenu(BTD);
          WVOLC.setText("@WVOLC@");
          WVOLC.setName("WVOLC");
          panel1.add(WVOLC);
          WVOLC.setBounds(353, 92, 74, WVOLC.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("Saisi");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(75, 40, 55, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Calcul\u00e9");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(150, 40, 60, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Exp\u00e9dition"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Mode");
          OBJ_42.setName("OBJ_42");
          panel2.add(OBJ_42);
          OBJ_42.setBounds(20, 45, 177, 20);

          //---- E1MEX ----
          E1MEX.setComponentPopupMenu(BTD);
          E1MEX.setName("E1MEX");
          panel2.add(E1MEX);
          E1MEX.setBounds(218, 41, 34, E1MEX.getPreferredSize().height);

          //---- OBJ_52 ----
          OBJ_52.setText("@EXLIB1@");
          OBJ_52.setName("OBJ_52");
          panel2.add(OBJ_52);
          OBJ_52.setBounds(258, 43, 202, OBJ_52.getPreferredSize().height);

          //---- OBJ_54 ----
          OBJ_54.setText("@TRLIBR@");
          OBJ_54.setName("OBJ_54");
          panel2.add(OBJ_54);
          OBJ_54.setBounds(258, 72, 202, OBJ_54.getPreferredSize().height);

          //---- E1CTR ----
          E1CTR.setComponentPopupMenu(BTD);
          E1CTR.setName("E1CTR");
          panel2.add(E1CTR);
          E1CTR.setBounds(218, 70, 34, E1CTR.getPreferredSize().height);

          //---- OBJ_53 ----
          OBJ_53.setText("Transporteur");
          OBJ_53.setName("OBJ_53");
          panel2.add(OBJ_53);
          OBJ_53.setBounds(20, 74, 177, 20);

          //---- OBJ_55 ----
          OBJ_55.setText("V\u00e9hicule");
          OBJ_55.setName("OBJ_55");
          panel2.add(OBJ_55);
          OBJ_55.setBounds(20, 103, 177, 20);

          //---- E1VEH ----
          E1VEH.setComponentPopupMenu(BTD);
          E1VEH.setName("E1VEH");
          panel2.add(E1VEH);
          E1VEH.setBounds(218, 99, 110, E1VEH.getPreferredSize().height);

          //---- E1IN22 ----
          E1IN22.setText("Hayon");
          E1IN22.setToolTipText("Forfait hayon");
          E1IN22.setComponentPopupMenu(BTD);
          E1IN22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          E1IN22.setHorizontalAlignment(SwingConstants.RIGHT);
          E1IN22.setName("E1IN22");
          panel2.add(E1IN22);
          E1IN22.setBounds(361, 103, 65, 20);

          //---- OBJ_61 ----
          OBJ_61.setText("Date d'exp\u00e9dition effective");
          OBJ_61.setName("OBJ_61");
          panel2.add(OBJ_61);
          OBJ_61.setBounds(20, 132, 177, 20);

          //---- OBJ_56 ----
          OBJ_56.setText("Rendez vous transporteur");
          OBJ_56.setName("OBJ_56");
          panel2.add(OBJ_56);
          OBJ_56.setBounds(20, 161, 177, 20);

          //---- WHTRDV ----
          WHTRDV.setComponentPopupMenu(BTD);
          WHTRDV.setName("WHTRDV");
          panel2.add(WHTRDV);
          WHTRDV.setBounds(345, 157, 30, WHTRDV.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("H");
          OBJ_57.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_57.setName("OBJ_57");
          panel2.add(OBJ_57);
          OBJ_57.setBounds(375, 161, 25, 20);

          //---- WMTRDV ----
          WMTRDV.setComponentPopupMenu(BTD);
          WMTRDV.setName("WMTRDV");
          panel2.add(WMTRDV);
          WMTRDV.setBounds(400, 157, 30, WMTRDV.getPreferredSize().height);

          //---- W29OBS ----
          W29OBS.setComponentPopupMenu(BTD);
          W29OBS.setName("W29OBS");
          panel2.add(W29OBS);
          W29OBS.setBounds(218, 185, 110, W29OBS.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("Observation");
          OBJ_58.setName("OBJ_58");
          panel2.add(OBJ_58);
          OBJ_58.setBounds(20, 189, 177, 20);

          //---- E1DT3X ----
          E1DT3X.setName("E1DT3X");
          panel2.add(E1DT3X);
          E1DT3X.setBounds(218, 128, 105, E1DT3X.getPreferredSize().height);

          //---- WDTRDV ----
          WDTRDV.setName("WDTRDV");
          panel2.add(WDTRDV);
          WDTRDV.setBounds(218, 157, 105, WDTRDV.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 560, GroupLayout.PREFERRED_SIZE)))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
              .addGap(5, 5, 5)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 241, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JPanel panel6;
  private XRiTextField ART1;
  private XRiTextField ART2;
  private XRiTextField ART3;
  private XRiTextField ART4;
  private XRiTextField LIB1;
  private XRiTextField LIB2;
  private XRiTextField LIB3;
  private XRiTextField LIB4;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private XRiTextField PVN1;
  private XRiTextField PVN2;
  private XRiTextField PVN3;
  private XRiTextField PVN4;
  private JPanel panel1;
  private JLabel OBJ_30;
  private XRiTextField E1COL;
  private RiZoneSortie WCOLC;
  private JLabel OBJ_33;
  private XRiTextField WNBPAL;
  private JLabel OBJ_35;
  private XRiTextField E1PDS;
  private RiZoneSortie WPDSC;
  private JLabel OBJ_38;
  private RiZoneSortie WVOLC;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JPanel panel2;
  private JLabel OBJ_42;
  private XRiTextField E1MEX;
  private RiZoneSortie OBJ_52;
  private RiZoneSortie OBJ_54;
  private XRiTextField E1CTR;
  private JLabel OBJ_53;
  private JLabel OBJ_55;
  private XRiTextField E1VEH;
  private XRiCheckBox E1IN22;
  private JLabel OBJ_61;
  private JLabel OBJ_56;
  private XRiTextField WHTRDV;
  private JLabel OBJ_57;
  private XRiTextField WMTRDV;
  private XRiTextField W29OBS;
  private JLabel OBJ_58;
  private XRiCalendrier E1DT3X;
  private XRiCalendrier WDTRDV;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
