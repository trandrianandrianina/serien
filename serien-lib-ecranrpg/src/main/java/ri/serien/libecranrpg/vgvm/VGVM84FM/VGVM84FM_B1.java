
package ri.serien.libecranrpg.vgvm.VGVM84FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM84FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM84FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CODE@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCND@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNPA@")).trim());
    OBJ_22.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTEP@")).trim());
    OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_18.setVisible(lexique.isPresent("WUNL"));
    OBJ_22.setVisible(lexique.isPresent("WQTEP"));
    WQTE.setEnabled(lexique.isPresent("WQTE"));
    OBJ_11.setVisible(lexique.isPresent("WNPA"));
    OBJ_20.setVisible(lexique.isPresent("WCND"));
    WART.setEnabled(lexique.isPresent("WART"));
    OBJ_15.setVisible(lexique.isPresent("A1LIB"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_15 = new RiZoneSortie();
    WART = new XRiTextField();
    OBJ_16 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_13 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_20 = new RiZoneSortie();
    OBJ_10 = new JLabel();
    OBJ_11 = new RiZoneSortie();
    WQTE = new XRiTextField();
    OBJ_22 = new RiZoneSortie();
    OBJ_18 = new RiZoneSortie();
    OBJ_19 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(535, 190));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_15 ----
          OBJ_15.setText("@A1LIB@");
          OBJ_15.setName("OBJ_15");
          p_recup.add(OBJ_15);
          OBJ_15.setBounds(15, 75, 315, OBJ_15.getPreferredSize().height);

          //---- WART ----
          WART.setComponentPopupMenu(BTD);
          WART.setName("WART");
          p_recup.add(WART);
          WART.setBounds(118, 45, 214, WART.getPreferredSize().height);

          //---- OBJ_16 ----
          OBJ_16.setText("Qt\u00e9 pr\u00e9par\u00e9e");
          OBJ_16.setName("OBJ_16");
          p_recup.add(OBJ_16);
          OBJ_16.setBounds(15, 109, 101, 20);

          //---- OBJ_12 ----
          OBJ_12.setText("Code Article");
          OBJ_12.setName("OBJ_12");
          p_recup.add(OBJ_12);
          OBJ_12.setBounds(15, 49, 94, 20);

          //---- OBJ_13 ----
          OBJ_13.setText("@CODE@");
          OBJ_13.setName("OBJ_13");
          p_recup.add(OBJ_13);
          OBJ_13.setBounds(15, 50, 89, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("Qt\u00e9 pr\u00e9vue");
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(15, 140, 101, 20);

          //---- OBJ_20 ----
          OBJ_20.setText("@WCND@");
          OBJ_20.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_20.setName("OBJ_20");
          p_recup.add(OBJ_20);
          OBJ_20.setBounds(252, 107, 78, OBJ_20.getPreferredSize().height);

          //---- OBJ_10 ----
          OBJ_10.setText("Palette");
          OBJ_10.setName("OBJ_10");
          p_recup.add(OBJ_10);
          OBJ_10.setBounds(15, 17, 101, 20);

          //---- OBJ_11 ----
          OBJ_11.setText("@WNPA@");
          OBJ_11.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_11.setName("OBJ_11");
          p_recup.add(OBJ_11);
          OBJ_11.setBounds(120, 15, 62, OBJ_11.getPreferredSize().height);

          //---- WQTE ----
          WQTE.setComponentPopupMenu(BTD);
          WQTE.setName("WQTE");
          p_recup.add(WQTE);
          WQTE.setBounds(118, 105, 66, WQTE.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("@WQTEP@");
          OBJ_22.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(120, 135, 60, OBJ_22.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("@WUNL@");
          OBJ_18.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_18.setName("OBJ_18");
          p_recup.add(OBJ_18);
          OBJ_18.setBounds(195, 107, 30, OBJ_18.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("x");
          OBJ_19.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_19.setName("OBJ_19");
          p_recup.add(OBJ_19);
          OBJ_19.setBounds(230, 109, 16, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie OBJ_15;
  private XRiTextField WART;
  private JLabel OBJ_16;
  private JLabel OBJ_12;
  private JLabel OBJ_13;
  private JLabel OBJ_21;
  private RiZoneSortie OBJ_20;
  private JLabel OBJ_10;
  private RiZoneSortie OBJ_11;
  private XRiTextField WQTE;
  private RiZoneSortie OBJ_22;
  private RiZoneSortie OBJ_18;
  private JLabel OBJ_19;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
