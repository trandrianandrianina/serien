
package ri.serien.libecranrpg.vgvm.VGVM13RF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGVM13RF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM13RF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX1.setValeursSelection("1", "");
    TIDX2.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MES19@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("RECHERCHE"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_45 = new JLabel();
    TIDX2 = new XRiCheckBox();
    TIDX1 = new XRiCheckBox();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    MTTDEB = new XRiTextField();
    MTTFIN = new XRiTextField();
    CC1 = new XRiTextField();
    OBJ_36 = new JLabel();
    OPTDAT = new XRiTextField();
    OPTMTT = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_42 = new JLabel();
    panel2 = new JPanel();
    OBJ_38 = new JLabel();
    ART = new XRiTextField();
    OBJ_39 = new JLabel();
    OBJ_41 = new JLabel();
    GP = new XRiTextField();
    FAM = new XRiTextField();
    OBJ_40 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(695, 320));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("S\u00e9lection suppl\u00e9mentaire et tri"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_45 ----
          OBJ_45.setText("Centrale ou donneur d'ordre");
          OBJ_45.setName("OBJ_45");
          panel1.add(OBJ_45);
          OBJ_45.setBounds(25, 245, 171, 20);

          //---- TIDX2 ----
          TIDX2.setText("Tri par montants");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");
          panel1.add(TIDX2);
          TIDX2.setBounds(30, 80, 135, 20);

          //---- TIDX1 ----
          TIDX1.setText("Tri par dates");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");
          panel1.add(TIDX1);
          TIDX1.setBounds(30, 45, 135, 20);

          //---- DATDEB ----
          DATDEB.setToolTipText("Date d\u00e9but");
          DATDEB.setComponentPopupMenu(BTD);
          DATDEB.setName("DATDEB");
          panel1.add(DATDEB);
          DATDEB.setBounds(200, 41, 105, DATDEB.getPreferredSize().height);

          //---- DATFIN ----
          DATFIN.setToolTipText("Date fin");
          DATFIN.setComponentPopupMenu(BTD);
          DATFIN.setName("DATFIN");
          panel1.add(DATFIN);
          DATFIN.setBounds(355, 41, 105, DATFIN.getPreferredSize().height);

          //---- MTTDEB ----
          MTTDEB.setToolTipText("Montant r\u00e9f\u00e9rence d\u00e9but");
          MTTDEB.setComponentPopupMenu(BTD);
          MTTDEB.setName("MTTDEB");
          panel1.add(MTTDEB);
          MTTDEB.setBounds(200, 76, 80, MTTDEB.getPreferredSize().height);

          //---- MTTFIN ----
          MTTFIN.setToolTipText("Montant r\u00e9f\u00e9rence fin");
          MTTFIN.setComponentPopupMenu(BTD);
          MTTFIN.setName("MTTFIN");
          panel1.add(MTTFIN);
          MTTFIN.setBounds(355, 76, 80, MTTFIN.getPreferredSize().height);

          //---- CC1 ----
          CC1.setComponentPopupMenu(BTD);
          CC1.setName("CC1");
          panel1.add(CC1);
          CC1.setBounds(200, 240, 58, CC1.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("de");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(175, 80, 30, 20);

          //---- OPTDAT ----
          OPTDAT.setToolTipText("Type de date \u00e0 traiter");
          OPTDAT.setComponentPopupMenu(BTD);
          OPTDAT.setName("OPTDAT");
          panel1.add(OPTDAT);
          OPTDAT.setBounds(465, 41, 24, OPTDAT.getPreferredSize().height);

          //---- OPTMTT ----
          OPTMTT.setToolTipText("Type de Montants");
          OPTMTT.setComponentPopupMenu(BTD);
          OPTMTT.setName("OPTMTT");
          panel1.add(OPTMTT);
          OPTMTT.setBounds(465, 76, 24, OPTMTT.getPreferredSize().height);

          //---- OBJ_34 ----
          OBJ_34.setText("du");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(175, 45, 30, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("au");
          OBJ_35.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(290, 45, 65, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("\u00e0");
          OBJ_37.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(280, 80, 75, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("@MES19@");
          OBJ_42.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_42.setForeground(new Color(204, 0, 0));
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(15, 270, 470, 20);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_38 ----
            OBJ_38.setText("Code article");
            OBJ_38.setName("OBJ_38");
            panel2.add(OBJ_38);
            OBJ_38.setBounds(15, 15, 76, 20);

            //---- ART ----
            ART.setComponentPopupMenu(BTD);
            ART.setName("ART");
            panel2.add(ART);
            ART.setBounds(185, 10, 210, ART.getPreferredSize().height);

            //---- OBJ_39 ----
            OBJ_39.setText("OU");
            OBJ_39.setFont(OBJ_39.getFont().deriveFont(OBJ_39.getFont().getStyle() | Font.BOLD));
            OBJ_39.setName("OBJ_39");
            panel2.add(OBJ_39);
            OBJ_39.setBounds(185, 41, 18, 20);

            //---- OBJ_41 ----
            OBJ_41.setText("Gestionnaire produit");
            OBJ_41.setName("OBJ_41");
            panel2.add(OBJ_41);
            OBJ_41.setBounds(240, 69, 123, 20);

            //---- GP ----
            GP.setComponentPopupMenu(BTD);
            GP.setName("GP");
            panel2.add(GP);
            GP.setBounds(375, 65, 20, GP.getPreferredSize().height);

            //---- FAM ----
            FAM.setComponentPopupMenu(BTD);
            FAM.setName("FAM");
            panel2.add(FAM);
            FAM.setBounds(185, 64, 40, FAM.getPreferredSize().height);

            //---- OBJ_40 ----
            OBJ_40.setText("Code famille");
            OBJ_40.setName("OBJ_40");
            panel2.add(OBJ_40);
            OBJ_40.setBounds(15, 69, 78, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel1.add(panel2);
          panel2.setBounds(15, 120, 470, 110);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 505, 300);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_45;
  private XRiCheckBox TIDX2;
  private XRiCheckBox TIDX1;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiTextField MTTDEB;
  private XRiTextField MTTFIN;
  private XRiTextField CC1;
  private JLabel OBJ_36;
  private XRiTextField OPTDAT;
  private XRiTextField OPTMTT;
  private JLabel OBJ_34;
  private JLabel OBJ_35;
  private JLabel OBJ_37;
  private JLabel OBJ_42;
  private JPanel panel2;
  private JLabel OBJ_38;
  private XRiTextField ART;
  private JLabel OBJ_39;
  private JLabel OBJ_41;
  private XRiTextField GP;
  private XRiTextField FAM;
  private JLabel OBJ_40;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
