
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_TF extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TFCP6_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP5_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP4_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP3_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP2_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  private String[] TFCP1_Value = { "", "1", "3", "5", "4", "6", "8", "9", };
  
  public VGVM01FX_TF(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TFCP6.setValeurs(TFCP6_Value, null);
    TFCP5.setValeurs(TFCP5_Value, null);
    TFCP4.setValeurs(TFCP4_Value, null);
    TFCP3.setValeurs(TFCP3_Value, null);
    TFCP2.setValeurs(TFCP2_Value, null);
    TFCP1.setValeurs(TFCP1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAUX@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAXE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    panel2.setVisible(false);
    
    if (lexique.isTrue("27")) {
      panel2.setVisible(true);
    }
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    TFCP1 = new XRiComboBox();
    TFCP2 = new XRiComboBox();
    TFCP3 = new XRiComboBox();
    TFCP4 = new XRiComboBox();
    TFCP5 = new XRiComboBox();
    TFCP6 = new XRiComboBox();
    OBJ_56 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_121 = new JLabel();
    TFLI1 = new XRiTextField();
    TFLI2 = new XRiTextField();
    TFLI3 = new XRiTextField();
    TFLI4 = new XRiTextField();
    TFLI5 = new XRiTextField();
    TFLI6 = new XRiTextField();
    TFTV1 = new XRiTextField();
    TFTV2 = new XRiTextField();
    TFTV3 = new XRiTextField();
    TFTV4 = new XRiTextField();
    TFTV5 = new XRiTextField();
    TFTV6 = new XRiTextField();
    TFTF1 = new XRiTextField();
    TFTP1 = new XRiTextField();
    TFTE1 = new XRiTextField();
    TFTF2 = new XRiTextField();
    TFTP2 = new XRiTextField();
    TFTE2 = new XRiTextField();
    TFTF3 = new XRiTextField();
    TFTP3 = new XRiTextField();
    TFTE3 = new XRiTextField();
    TFTF4 = new XRiTextField();
    TFTP4 = new XRiTextField();
    TFTE4 = new XRiTextField();
    TFTF5 = new XRiTextField();
    TFTP5 = new XRiTextField();
    TFTE5 = new XRiTextField();
    TFTF6 = new XRiTextField();
    TFTP6 = new XRiTextField();
    TFTE6 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_101 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_111 = new JLabel();
    TFCX1 = new XRiTextField();
    TFCX2 = new XRiTextField();
    TFCX3 = new XRiTextField();
    TFCX4 = new XRiTextField();
    TFCX5 = new XRiTextField();
    TFCX6 = new XRiTextField();
    TFTXPX = new XRiTextField();
    OBJ_95 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_109 = new JLabel();
    TFRBP = new XRiTextField();
    OBJ_117 = new JLabel();
    panel3 = new JPanel();
    OBJ_116 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_119 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    riSousMenu7 = new RiSousMenu();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Historique modifications");
            riSousMenu_bt7.setToolTipText("Historique des modifications");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            menus_haut.add(riSousMenu_bt7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Table des types de facturation");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- TFCP1 ----
            TFCP1.setModel(new DefaultComboBoxModel(new String[] {
              "Pas d'\u00e9dition",
              "sur ARC",
              "sur EXP",
              "sur FAC",
              "sur ARC + EXP",
              "sur ARC + FAC",
              "sur EXP + FAC",
              "sur ARC + EXP + FAC"
            }));
            TFCP1.setComponentPopupMenu(BTD);
            TFCP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TFCP1.setName("TFCP1");
            xTitledPanel4ContentContainer.add(TFCP1);
            TFCP1.setBounds(495, 76, 154, TFCP1.getPreferredSize().height);

            //---- TFCP2 ----
            TFCP2.setModel(new DefaultComboBoxModel(new String[] {
              "Pas d'\u00e9dition",
              "sur ARC",
              "sur EXP",
              "sur FAC",
              "sur ARC + EXP",
              "sur ARC + FAC",
              "sur EXP + FAC",
              "sur ARC + EXP + FAC"
            }));
            TFCP2.setComponentPopupMenu(BTD);
            TFCP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TFCP2.setName("TFCP2");
            xTitledPanel4ContentContainer.add(TFCP2);
            TFCP2.setBounds(495, 106, 154, TFCP2.getPreferredSize().height);

            //---- TFCP3 ----
            TFCP3.setModel(new DefaultComboBoxModel(new String[] {
              "Pas d'\u00e9dition",
              "sur ARC",
              "sur EXP",
              "sur FAC",
              "sur ARC + EXP",
              "sur ARC + FAC",
              "sur EXP + FAC",
              "sur ARC + EXP + FAC"
            }));
            TFCP3.setComponentPopupMenu(BTD);
            TFCP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TFCP3.setName("TFCP3");
            xTitledPanel4ContentContainer.add(TFCP3);
            TFCP3.setBounds(495, 136, 154, TFCP3.getPreferredSize().height);

            //---- TFCP4 ----
            TFCP4.setModel(new DefaultComboBoxModel(new String[] {
              "Pas d'\u00e9dition",
              "sur ARC",
              "sur EXP",
              "sur FAC",
              "sur ARC + EXP",
              "sur ARC + FAC",
              "sur EXP + FAC",
              "sur ARC + EXP + FAC"
            }));
            TFCP4.setComponentPopupMenu(BTD);
            TFCP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TFCP4.setName("TFCP4");
            xTitledPanel4ContentContainer.add(TFCP4);
            TFCP4.setBounds(495, 165, 154, TFCP4.getPreferredSize().height);

            //---- TFCP5 ----
            TFCP5.setModel(new DefaultComboBoxModel(new String[] {
              "Pas d'\u00e9dition",
              "sur ARC",
              "sur EXP",
              "sur FAC",
              "sur ARC + EXP",
              "sur ARC + FAC",
              "sur EXP + FAC",
              "sur ARC + EXP + FAC"
            }));
            TFCP5.setComponentPopupMenu(BTD);
            TFCP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TFCP5.setName("TFCP5");
            xTitledPanel4ContentContainer.add(TFCP5);
            TFCP5.setBounds(495, 196, 154, TFCP5.getPreferredSize().height);

            //---- TFCP6 ----
            TFCP6.setModel(new DefaultComboBoxModel(new String[] {
              "Pas d'\u00e9dition",
              "sur ARC",
              "sur EXP",
              "sur FAC",
              "sur ARC + EXP",
              "sur ARC + FAC",
              "sur EXP + FAC",
              "sur ARC + EXP + FAC"
            }));
            TFCP6.setComponentPopupMenu(BTD);
            TFCP6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TFCP6.setName("TFCP6");
            xTitledPanel4ContentContainer.add(TFCP6);
            TFCP6.setBounds(495, 226, 154, TFCP6.getPreferredSize().height);

            //---- OBJ_56 ----
            OBJ_56.setText("Codes facturation");
            OBJ_56.setName("OBJ_56");
            xTitledPanel4ContentContainer.add(OBJ_56);
            OBJ_56.setBounds(20, 79, 130, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Type de facturation");
            OBJ_53.setName("OBJ_53");
            xTitledPanel4ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(190, 45, 118, 19);

            //---- OBJ_121 ----
            OBJ_121.setText("Edition code pays");
            OBJ_121.setName("OBJ_121");
            xTitledPanel4ContentContainer.add(OBJ_121);
            OBJ_121.setBounds(520, 45, 111, 19);

            //---- TFLI1 ----
            TFLI1.setComponentPopupMenu(BTD);
            TFLI1.setName("TFLI1");
            xTitledPanel4ContentContainer.add(TFLI1);
            TFLI1.setBounds(190, 75, 110, TFLI1.getPreferredSize().height);

            //---- TFLI2 ----
            TFLI2.setToolTipText("libell\u00e9");
            TFLI2.setComponentPopupMenu(BTD);
            TFLI2.setName("TFLI2");
            xTitledPanel4ContentContainer.add(TFLI2);
            TFLI2.setBounds(190, 105, 110, TFLI2.getPreferredSize().height);

            //---- TFLI3 ----
            TFLI3.setToolTipText("libell\u00e9");
            TFLI3.setComponentPopupMenu(BTD);
            TFLI3.setName("TFLI3");
            xTitledPanel4ContentContainer.add(TFLI3);
            TFLI3.setBounds(190, 135, 110, TFLI3.getPreferredSize().height);

            //---- TFLI4 ----
            TFLI4.setToolTipText("libell\u00e9");
            TFLI4.setComponentPopupMenu(BTD);
            TFLI4.setName("TFLI4");
            xTitledPanel4ContentContainer.add(TFLI4);
            TFLI4.setBounds(190, 165, 110, TFLI4.getPreferredSize().height);

            //---- TFLI5 ----
            TFLI5.setToolTipText("libell\u00e9");
            TFLI5.setComponentPopupMenu(BTD);
            TFLI5.setName("TFLI5");
            xTitledPanel4ContentContainer.add(TFLI5);
            TFLI5.setBounds(190, 195, 110, TFLI5.getPreferredSize().height);

            //---- TFLI6 ----
            TFLI6.setToolTipText("libell\u00e9");
            TFLI6.setComponentPopupMenu(BTD);
            TFLI6.setName("TFLI6");
            xTitledPanel4ContentContainer.add(TFLI6);
            TFLI6.setBounds(190, 225, 110, TFLI6.getPreferredSize().height);

            //---- TFTV1 ----
            TFTV1.setComponentPopupMenu(BTD);
            TFTV1.setName("TFTV1");
            xTitledPanel4ContentContainer.add(TFTV1);
            TFTV1.setBounds(350, 75, 36, TFTV1.getPreferredSize().height);

            //---- TFTV2 ----
            TFTV2.setToolTipText("sous la forme \"123\"");
            TFTV2.setComponentPopupMenu(BTD);
            TFTV2.setName("TFTV2");
            xTitledPanel4ContentContainer.add(TFTV2);
            TFTV2.setBounds(350, 105, 36, TFTV2.getPreferredSize().height);

            //---- TFTV3 ----
            TFTV3.setToolTipText("sous la forme \"123\"");
            TFTV3.setComponentPopupMenu(BTD);
            TFTV3.setName("TFTV3");
            xTitledPanel4ContentContainer.add(TFTV3);
            TFTV3.setBounds(350, 135, 36, TFTV3.getPreferredSize().height);

            //---- TFTV4 ----
            TFTV4.setToolTipText("sous la forme \"123\"");
            TFTV4.setComponentPopupMenu(BTD);
            TFTV4.setName("TFTV4");
            xTitledPanel4ContentContainer.add(TFTV4);
            TFTV4.setBounds(350, 165, 36, TFTV4.getPreferredSize().height);

            //---- TFTV5 ----
            TFTV5.setToolTipText("sous la forme \"123\"");
            TFTV5.setComponentPopupMenu(BTD);
            TFTV5.setName("TFTV5");
            xTitledPanel4ContentContainer.add(TFTV5);
            TFTV5.setBounds(350, 195, 36, TFTV5.getPreferredSize().height);

            //---- TFTV6 ----
            TFTV6.setToolTipText("sous la forme \"123\"");
            TFTV6.setComponentPopupMenu(BTD);
            TFTV6.setName("TFTV6");
            xTitledPanel4ContentContainer.add(TFTV6);
            TFTV6.setBounds(350, 225, 36, TFTV6.getPreferredSize().height);

            //---- TFTF1 ----
            TFTF1.setComponentPopupMenu(BTD);
            TFTF1.setName("TFTF1");
            xTitledPanel4ContentContainer.add(TFTF1);
            TFTF1.setBounds(165, 75, 20, TFTF1.getPreferredSize().height);

            //---- TFTP1 ----
            TFTP1.setToolTipText("taxe parafiscale");
            TFTP1.setComponentPopupMenu(BTD);
            TFTP1.setName("TFTP1");
            xTitledPanel4ContentContainer.add(TFTP1);
            TFTP1.setBounds(410, 75, 20, TFTP1.getPreferredSize().height);

            //---- TFTE1 ----
            TFTE1.setComponentPopupMenu(BTD);
            TFTE1.setName("TFTE1");
            xTitledPanel4ContentContainer.add(TFTE1);
            TFTE1.setBounds(450, 75, 20, TFTE1.getPreferredSize().height);

            //---- TFTF2 ----
            TFTF2.setComponentPopupMenu(BTD);
            TFTF2.setName("TFTF2");
            xTitledPanel4ContentContainer.add(TFTF2);
            TFTF2.setBounds(165, 105, 20, TFTF2.getPreferredSize().height);

            //---- TFTP2 ----
            TFTP2.setToolTipText("taxe parafiscale");
            TFTP2.setComponentPopupMenu(BTD);
            TFTP2.setName("TFTP2");
            xTitledPanel4ContentContainer.add(TFTP2);
            TFTP2.setBounds(410, 105, 20, TFTP2.getPreferredSize().height);

            //---- TFTE2 ----
            TFTE2.setComponentPopupMenu(BTD);
            TFTE2.setName("TFTE2");
            xTitledPanel4ContentContainer.add(TFTE2);
            TFTE2.setBounds(450, 105, 20, TFTE2.getPreferredSize().height);

            //---- TFTF3 ----
            TFTF3.setComponentPopupMenu(BTD);
            TFTF3.setName("TFTF3");
            xTitledPanel4ContentContainer.add(TFTF3);
            TFTF3.setBounds(165, 135, 20, TFTF3.getPreferredSize().height);

            //---- TFTP3 ----
            TFTP3.setToolTipText("taxe parafiscale");
            TFTP3.setComponentPopupMenu(BTD);
            TFTP3.setName("TFTP3");
            xTitledPanel4ContentContainer.add(TFTP3);
            TFTP3.setBounds(410, 135, 20, TFTP3.getPreferredSize().height);

            //---- TFTE3 ----
            TFTE3.setComponentPopupMenu(BTD);
            TFTE3.setName("TFTE3");
            xTitledPanel4ContentContainer.add(TFTE3);
            TFTE3.setBounds(450, 135, 20, TFTE3.getPreferredSize().height);

            //---- TFTF4 ----
            TFTF4.setComponentPopupMenu(BTD);
            TFTF4.setName("TFTF4");
            xTitledPanel4ContentContainer.add(TFTF4);
            TFTF4.setBounds(165, 165, 20, TFTF4.getPreferredSize().height);

            //---- TFTP4 ----
            TFTP4.setToolTipText("taxe parafiscale");
            TFTP4.setComponentPopupMenu(BTD);
            TFTP4.setName("TFTP4");
            xTitledPanel4ContentContainer.add(TFTP4);
            TFTP4.setBounds(410, 165, 20, TFTP4.getPreferredSize().height);

            //---- TFTE4 ----
            TFTE4.setComponentPopupMenu(BTD);
            TFTE4.setName("TFTE4");
            xTitledPanel4ContentContainer.add(TFTE4);
            TFTE4.setBounds(450, 164, 20, TFTE4.getPreferredSize().height);

            //---- TFTF5 ----
            TFTF5.setComponentPopupMenu(BTD);
            TFTF5.setName("TFTF5");
            xTitledPanel4ContentContainer.add(TFTF5);
            TFTF5.setBounds(165, 195, 20, TFTF5.getPreferredSize().height);

            //---- TFTP5 ----
            TFTP5.setToolTipText("taxe parafiscale");
            TFTP5.setComponentPopupMenu(BTD);
            TFTP5.setName("TFTP5");
            xTitledPanel4ContentContainer.add(TFTP5);
            TFTP5.setBounds(410, 195, 20, TFTP5.getPreferredSize().height);

            //---- TFTE5 ----
            TFTE5.setComponentPopupMenu(BTD);
            TFTE5.setName("TFTE5");
            xTitledPanel4ContentContainer.add(TFTE5);
            TFTE5.setBounds(450, 195, 20, TFTE5.getPreferredSize().height);

            //---- TFTF6 ----
            TFTF6.setComponentPopupMenu(BTD);
            TFTF6.setName("TFTF6");
            xTitledPanel4ContentContainer.add(TFTF6);
            TFTF6.setBounds(165, 225, 20, TFTF6.getPreferredSize().height);

            //---- TFTP6 ----
            TFTP6.setToolTipText("taxe parafiscale");
            TFTP6.setComponentPopupMenu(BTD);
            TFTP6.setName("TFTP6");
            xTitledPanel4ContentContainer.add(TFTP6);
            TFTP6.setBounds(410, 225, 20, TFTP6.getPreferredSize().height);

            //---- TFTE6 ----
            TFTE6.setComponentPopupMenu(BTD);
            TFTE6.setName("TFTE6");
            xTitledPanel4ContentContainer.add(TFTE6);
            TFTE6.setBounds(450, 225, 20, TFTE6.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_101 ----
              OBJ_101.setText("@TAUX@");
              OBJ_101.setName("OBJ_101");
              panel2.add(OBJ_101);
              OBJ_101.setBounds(20, 39, 130, 20);

              //---- OBJ_108 ----
              OBJ_108.setText("@TAXE@");
              OBJ_108.setName("OBJ_108");
              panel2.add(OBJ_108);
              OBJ_108.setBounds(20, 74, 130, 20);

              //---- OBJ_111 ----
              OBJ_111.setText("Base %");
              OBJ_111.setName("OBJ_111");
              panel2.add(OBJ_111);
              OBJ_111.setBounds(355, 74, 51, 20);

              //---- TFCX1 ----
              TFCX1.setComponentPopupMenu(BTD);
              TFCX1.setName("TFCX1");
              panel2.add(TFCX1);
              TFCX1.setBounds(160, 35, 58, TFCX1.getPreferredSize().height);

              //---- TFCX2 ----
              TFCX2.setComponentPopupMenu(BTD);
              TFCX2.setName("TFCX2");
              panel2.add(TFCX2);
              TFCX2.setBounds(225, 35, 58, TFCX2.getPreferredSize().height);

              //---- TFCX3 ----
              TFCX3.setComponentPopupMenu(BTD);
              TFCX3.setName("TFCX3");
              panel2.add(TFCX3);
              TFCX3.setBounds(290, 35, 58, TFCX3.getPreferredSize().height);

              //---- TFCX4 ----
              TFCX4.setComponentPopupMenu(BTD);
              TFCX4.setName("TFCX4");
              panel2.add(TFCX4);
              TFCX4.setBounds(355, 35, 58, TFCX4.getPreferredSize().height);

              //---- TFCX5 ----
              TFCX5.setComponentPopupMenu(BTD);
              TFCX5.setName("TFCX5");
              panel2.add(TFCX5);
              TFCX5.setBounds(420, 35, 58, TFCX5.getPreferredSize().height);

              //---- TFCX6 ----
              TFCX6.setComponentPopupMenu(BTD);
              TFCX6.setName("TFCX6");
              panel2.add(TFCX6);
              TFCX6.setBounds(485, 35, 58, TFCX6.getPreferredSize().height);

              //---- TFTXPX ----
              TFTXPX.setComponentPopupMenu(BTD);
              TFTXPX.setName("TFTXPX");
              panel2.add(TFTXPX);
              TFTXPX.setBounds(225, 70, 58, TFTXPX.getPreferredSize().height);

              //---- OBJ_95 ----
              OBJ_95.setText("Code1");
              OBJ_95.setName("OBJ_95");
              panel2.add(OBJ_95);
              OBJ_95.setBounds(160, 10, 42, 16);

              //---- OBJ_96 ----
              OBJ_96.setText("Code2");
              OBJ_96.setName("OBJ_96");
              panel2.add(OBJ_96);
              OBJ_96.setBounds(225, 10, 42, 16);

              //---- OBJ_97 ----
              OBJ_97.setText("Code3");
              OBJ_97.setName("OBJ_97");
              panel2.add(OBJ_97);
              OBJ_97.setBounds(290, 10, 42, 16);

              //---- OBJ_98 ----
              OBJ_98.setText("Code4");
              OBJ_98.setName("OBJ_98");
              panel2.add(OBJ_98);
              OBJ_98.setBounds(355, 10, 42, 16);

              //---- OBJ_99 ----
              OBJ_99.setText("Code5");
              OBJ_99.setName("OBJ_99");
              panel2.add(OBJ_99);
              OBJ_99.setBounds(420, 10, 42, 16);

              //---- OBJ_100 ----
              OBJ_100.setText("Code6");
              OBJ_100.setName("OBJ_100");
              panel2.add(OBJ_100);
              OBJ_100.setBounds(485, 10, 42, 16);

              //---- OBJ_109 ----
              OBJ_109.setText("Taux");
              OBJ_109.setName("OBJ_109");
              panel2.add(OBJ_109);
              OBJ_109.setBounds(160, 74, 33, 20);

              //---- TFRBP ----
              TFRBP.setComponentPopupMenu(BTD);
              TFRBP.setName("TFRBP");
              panel2.add(TFRBP);
              TFRBP.setBounds(420, 70, 26, TFRBP.getPreferredSize().height);
            }
            xTitledPanel4ContentContainer.add(panel2);
            panel2.setBounds(10, 275, 640, 115);

            //---- OBJ_117 ----
            OBJ_117.setText("Codes");
            OBJ_117.setName("OBJ_117");
            xTitledPanel4ContentContainer.add(OBJ_117);
            OBJ_117.setBounds(392, 10, 40, 20);

            //======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_116 ----
              OBJ_116.setText("TVA");
              OBJ_116.setName("OBJ_116");
              panel3.add(OBJ_116);
              OBJ_116.setBounds(10, 5, 35, 20);

              //---- OBJ_118 ----
              OBJ_118.setText("TPF");
              OBJ_118.setName("OBJ_118");
              panel3.add(OBJ_118);
              OBJ_118.setBounds(70, 8, 23, 14);

              //---- OBJ_119 ----
              OBJ_119.setText("TEC");
              OBJ_119.setName("OBJ_119");
              panel3.add(OBJ_119);
              OBJ_119.setBounds(110, 8, 30, 14);
            }
            xTitledPanel4ContentContainer.add(panel3);
            panel3.setBounds(340, 35, 145, 30);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 443, GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //======== riSousMenu7 ========
    {
      riSousMenu7.setName("riSousMenu7");
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private XRiComboBox TFCP1;
  private XRiComboBox TFCP2;
  private XRiComboBox TFCP3;
  private XRiComboBox TFCP4;
  private XRiComboBox TFCP5;
  private XRiComboBox TFCP6;
  private JLabel OBJ_56;
  private JLabel OBJ_53;
  private JLabel OBJ_121;
  private XRiTextField TFLI1;
  private XRiTextField TFLI2;
  private XRiTextField TFLI3;
  private XRiTextField TFLI4;
  private XRiTextField TFLI5;
  private XRiTextField TFLI6;
  private XRiTextField TFTV1;
  private XRiTextField TFTV2;
  private XRiTextField TFTV3;
  private XRiTextField TFTV4;
  private XRiTextField TFTV5;
  private XRiTextField TFTV6;
  private XRiTextField TFTF1;
  private XRiTextField TFTP1;
  private XRiTextField TFTE1;
  private XRiTextField TFTF2;
  private XRiTextField TFTP2;
  private XRiTextField TFTE2;
  private XRiTextField TFTF3;
  private XRiTextField TFTP3;
  private XRiTextField TFTE3;
  private XRiTextField TFTF4;
  private XRiTextField TFTP4;
  private XRiTextField TFTE4;
  private XRiTextField TFTF5;
  private XRiTextField TFTP5;
  private XRiTextField TFTE5;
  private XRiTextField TFTF6;
  private XRiTextField TFTP6;
  private XRiTextField TFTE6;
  private JPanel panel2;
  private JLabel OBJ_101;
  private JLabel OBJ_108;
  private JLabel OBJ_111;
  private XRiTextField TFCX1;
  private XRiTextField TFCX2;
  private XRiTextField TFCX3;
  private XRiTextField TFCX4;
  private XRiTextField TFCX5;
  private XRiTextField TFCX6;
  private XRiTextField TFTXPX;
  private JLabel OBJ_95;
  private JLabel OBJ_96;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_109;
  private XRiTextField TFRBP;
  private JLabel OBJ_117;
  private JPanel panel3;
  private JLabel OBJ_116;
  private JLabel OBJ_118;
  private JLabel OBJ_119;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private RiSousMenu riSousMenu7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
