
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_ID extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] IDCG4_Value = { "T", "C", "O", "", };
  private String[] IDCG3_Value = { "T", "C", "O", "", };
  private String[] IDCG2_Value = { "T", "C", "O", "", };
  private String[] IDCG1_Value = { "T", "C", "O", "", };
  
  private boolean isConsultation = false;
  
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  public VGVM01FX_ID(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    IDCG4.setValeurs(IDCG4_Value, null);
    IDCG3.setValeurs(IDCG3_Value, null);
    IDCG2.setValeurs(IDCG2_Value, null);
    IDCG1.setValeurs(IDCG1_Value, null);
    IDCPTR.setValeursSelection("X", "");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    


    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    // Commune
    snCodePostalCommune.setSession(getSession());
    snCodePostalCommune.setIdEtablissement(snEtablissement.getIdSelection());
    snCodePostalCommune.charger(false);
    snCodePostalCommune.setSelectionParChampRPG(lexique, "IDCDP", "IDVILR");
    snCodePostalCommune.setEnabled(!isConsultation);
    
    // Clients grand public
    snClient1.setSession(getSession());
    snClient1.setIdEtablissement(snEtablissement.getIdSelection());
    snClient1.charger(false);
    snClient1.setSelectionParChampRPG(lexique, "IDCGP");
    snClient1.setEnabled(!isConsultation);
    
    snClient2.setSession(getSession());
    snClient2.setIdEtablissement(snEtablissement.getIdSelection());
    snClient2.charger(false);
    snClient2.setSelectionParChampRPG(lexique, "IDCGP2");
    snClient2.setEnabled(!isConsultation);
    
    snClient3.setSession(getSession());
    snClient3.setIdEtablissement(snEtablissement.getIdSelection());
    snClient3.charger(false);
    snClient3.setSelectionParChampRPG(lexique, "IDCGP3");
    snClient3.setEnabled(!isConsultation);
    
    snClient4.setSession(getSession());
    snClient4.setIdEtablissement(snEtablissement.getIdSelection());
    snClient4.charger(false);
    snClient4.setSelectionParChampRPG(lexique, "IDCGP4");
    snClient4.setEnabled(!isConsultation);
    
    rafraichirBoutons();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snCodePostalCommune.renseignerChampRPG(lexique, "IDCDP", "IDVILR", false);
    snClient1.renseignerChampRPG(lexique, "IDCGP");
    snClient2.renseignerChampRPG(lexique, "IDCGP2");
    snClient3.renseignerChampRPG(lexique, "IDCGP3");
    snClient4.renseignerChampRPG(lexique, "IDCGP4");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    tpnFamille = new JTabbedPane();
    pnlDescription = new SNPanel();
    pnlGauche = new SNPanelContenu();
    pnlCoordonnees = new SNPanelTitre();
    lbRue = new SNLabelChamp();
    IDRUE = new XRiTextField();
    lbLocalite = new SNLabelChamp();
    IDLOC = new XRiTextField();
    lbCommune = new SNLabelChamp();
    snCodePostalCommune = new SNCodePostalCommune();
    lbTelephone = new SNLabelChamp();
    IDTEL = new XRiTextField();
    lbFax = new SNLabelChamp();
    IDFAX = new XRiTextField();
    pnlCompte = new SNPanel();
    lbCompteArrondis = new SNLabelChamp();
    IDNCG = new XRiTextField();
    pnlDroite = new SNPanelContenu();
    sNPanel5 = new SNPanel();
    lbStatut = new SNLabelChamp();
    IDSTA = new XRiTextField();
    lbSIRET = new SNLabelChamp();
    IDSIR = new XRiTextField();
    lbAPE = new SNLabelChamp();
    IDAPE = new XRiTextField();
    lbRCS = new SNLabelChamp();
    scrollPane1 = new JScrollPane();
    IDRCS = new RiTextArea();
    lbTVA = new SNLabelChamp();
    IDNII = new XRiTextField();
    sNPanel6 = new SNPanel();
    lbCNUF = new SNLabelChamp();
    IDCNU = new XRiTextField();
    lbAdherent = new SNLabelChamp();
    IDADH = new XRiTextField();
    pnlStocks = new SNPanel();
    pnlGauche2 = new SNPanelContenu();
    sNPanel1 = new SNPanel();
    lbClient = new SNLabelChamp();
    snClient1 = new SNClientPrincipal();
    lbAffectation = new SNLabelChamp();
    IDCG1 = new XRiComboBox();
    sNPanel2 = new SNPanel();
    lbClient2 = new SNLabelChamp();
    snClient2 = new SNClientPrincipal();
    lbAffectation2 = new SNLabelChamp();
    IDCG2 = new XRiComboBox();
    sNPanel3 = new SNPanel();
    lbClient3 = new SNLabelChamp();
    snClient3 = new SNClientPrincipal();
    lbAffectation3 = new SNLabelChamp();
    IDCG3 = new XRiComboBox();
    sNPanel4 = new SNPanel();
    lbClient4 = new SNLabelChamp();
    snClient4 = new SNClientPrincipal();
    lbAffectation4 = new SNLabelChamp();
    IDCG4 = new XRiComboBox();
    IDCPTR = new XRiCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes : El\u00e9ments d'identification");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

      //======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlPersonnalisation.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlPersonnalisation.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlPersonnalisation.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlPersonnalisation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbCodeCategorie ----
        lbCodeCategorie.setText("code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- INDIND ----
        INDIND.setPreferredSize(new Dimension(80, 30));
        INDIND.setMinimumSize(new Dimension(80, 30));
        INDIND.setMaximumSize(new Dimension(80, 30));
        INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDIND.setName("INDIND");
        pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlPersonnalisation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== tpnFamille ========
      {
        tpnFamille.setMinimumSize(new Dimension(890, 450));
        tpnFamille.setPreferredSize(new Dimension(890, 450));
        tpnFamille.setBackground(new Color(239, 239, 222));
        tpnFamille.setOpaque(true);
        tpnFamille.setFont(new Font("sansserif", Font.PLAIN, 14));
        tpnFamille.setName("tpnFamille");

        //======== pnlDescription ========
        {
          pnlDescription.setOpaque(false);
          pnlDescription.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlDescription.setName("pnlDescription");
          pnlDescription.setLayout(new GridLayout(1, 2));

          //======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //======== pnlCoordonnees ========
            {
              pnlCoordonnees.setTitre("Coordonn\u00e9es");
              pnlCoordonnees.setName("pnlCoordonnees");
              pnlCoordonnees.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlCoordonnees.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)pnlCoordonnees.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
              ((GridBagLayout)pnlCoordonnees.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
              ((GridBagLayout)pnlCoordonnees.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

              //---- lbRue ----
              lbRue.setText("Rue");
              lbRue.setName("lbRue");
              pnlCoordonnees.add(lbRue, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDRUE ----
              IDRUE.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDRUE.setName("IDRUE");
              pnlCoordonnees.add(IDRUE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbLocalite ----
              lbLocalite.setText("Localit\u00e9");
              lbLocalite.setName("lbLocalite");
              pnlCoordonnees.add(lbLocalite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDLOC ----
              IDLOC.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDLOC.setName("IDLOC");
              pnlCoordonnees.add(IDLOC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbCommune ----
              lbCommune.setText("Commune");
              lbCommune.setName("lbCommune");
              pnlCoordonnees.add(lbCommune, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- snCodePostalCommune ----
              snCodePostalCommune.setName("snCodePostalCommune");
              pnlCoordonnees.add(snCodePostalCommune, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbTelephone ----
              lbTelephone.setText("T\u00e9l\u00e9phone");
              lbTelephone.setName("lbTelephone");
              pnlCoordonnees.add(lbTelephone, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDTEL ----
              IDTEL.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDTEL.setName("IDTEL");
              pnlCoordonnees.add(IDTEL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbFax ----
              lbFax.setText("Fax");
              lbFax.setName("lbFax");
              pnlCoordonnees.add(lbFax, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDFAX ----
              IDFAX.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDFAX.setName("IDFAX");
              pnlCoordonnees.add(IDFAX, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlCoordonnees, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== pnlCompte ========
            {
              pnlCompte.setName("pnlCompte");
              pnlCompte.setLayout(new GridBagLayout());
              ((GridBagLayout)pnlCompte.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)pnlCompte.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)pnlCompte.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
              ((GridBagLayout)pnlCompte.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbCompteArrondis ----
              lbCompteArrondis.setText("Num\u00e9ro de compte pour comptabilisation des arrondis de caisse");
              lbCompteArrondis.setMinimumSize(new Dimension(420, 30));
              lbCompteArrondis.setPreferredSize(new Dimension(420, 30));
              lbCompteArrondis.setMaximumSize(new Dimension(450, 30));
              lbCompteArrondis.setName("lbCompteArrondis");
              pnlCompte.add(lbCompteArrondis, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDNCG ----
              IDNCG.setMinimumSize(new Dimension(100, 30));
              IDNCG.setPreferredSize(new Dimension(100, 30));
              IDNCG.setMaximumSize(new Dimension(100, 30));
              IDNCG.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDNCG.setName("IDNCG");
              pnlCompte.add(IDNCG, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlCompte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDescription.add(pnlGauche);

          //======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //======== sNPanel5 ========
            {
              sNPanel5.setName("sNPanel5");
              sNPanel5.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanel5.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)sNPanel5.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
              ((GridBagLayout)sNPanel5.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
              ((GridBagLayout)sNPanel5.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

              //---- lbStatut ----
              lbStatut.setText("Statut juridique");
              lbStatut.setMaximumSize(new Dimension(250, 30));
              lbStatut.setMinimumSize(new Dimension(200, 30));
              lbStatut.setPreferredSize(new Dimension(200, 30));
              lbStatut.setName("lbStatut");
              sNPanel5.add(lbStatut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDSTA ----
              IDSTA.setMinimumSize(new Dimension(100, 30));
              IDSTA.setMaximumSize(new Dimension(100, 30));
              IDSTA.setPreferredSize(new Dimension(100, 30));
              IDSTA.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDSTA.setName("IDSTA");
              sNPanel5.add(IDSTA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbSIRET ----
              lbSIRET.setText("Num\u00e9ro SIRET");
              lbSIRET.setName("lbSIRET");
              sNPanel5.add(lbSIRET, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDSIR ----
              IDSIR.setPreferredSize(new Dimension(200, 30));
              IDSIR.setMinimumSize(new Dimension(200, 30));
              IDSIR.setMaximumSize(new Dimension(200, 30));
              IDSIR.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDSIR.setName("IDSIR");
              sNPanel5.add(IDSIR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbAPE ----
              lbAPE.setText("Code APE");
              lbAPE.setName("lbAPE");
              sNPanel5.add(lbAPE, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDAPE ----
              IDAPE.setMinimumSize(new Dimension(100, 30));
              IDAPE.setMaximumSize(new Dimension(100, 30));
              IDAPE.setPreferredSize(new Dimension(100, 30));
              IDAPE.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDAPE.setName("IDAPE");
              sNPanel5.add(IDAPE, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbRCS ----
              lbRCS.setText("Registre du commerce (RCS)");
              lbRCS.setName("lbRCS");
              sNPanel5.add(lbRCS, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //======== scrollPane1 ========
              {
                scrollPane1.setName("scrollPane1");

                //---- IDRCS ----
                IDRCS.setWrapStyleWord(true);
                IDRCS.setLineWrap(true);
                IDRCS.setMinimumSize(new Dimension(350, 40));
                IDRCS.setPreferredSize(new Dimension(350, 40));
                IDRCS.setMaximumSize(new Dimension(350, 60));
                IDRCS.setFont(new Font("sansserif", Font.PLAIN, 14));
                IDRCS.setName("IDRCS");
                scrollPane1.setViewportView(IDRCS);
              }
              sNPanel5.add(scrollPane1, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbTVA ----
              lbTVA.setText("Num\u00e9ro d'identification T.V.A.");
              lbTVA.setName("lbTVA");
              sNPanel5.add(lbTVA, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDNII ----
              IDNII.setPreferredSize(new Dimension(200, 30));
              IDNII.setMinimumSize(new Dimension(200, 30));
              IDNII.setMaximumSize(new Dimension(200, 30));
              IDNII.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDNII.setName("IDNII");
              sNPanel5.add(IDNII, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite.add(sNPanel5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== sNPanel6 ========
            {
              sNPanel6.setName("sNPanel6");
              sNPanel6.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanel6.getLayout()).columnWidths = new int[] {0, 0, 0};
              ((GridBagLayout)sNPanel6.getLayout()).rowHeights = new int[] {0, 0, 0};
              ((GridBagLayout)sNPanel6.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
              ((GridBagLayout)sNPanel6.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

              //---- lbCNUF ----
              lbCNUF.setText("Code CNUF");
              lbCNUF.setPreferredSize(new Dimension(200, 30));
              lbCNUF.setMinimumSize(new Dimension(200, 30));
              lbCNUF.setMaximumSize(new Dimension(250, 30));
              lbCNUF.setName("lbCNUF");
              sNPanel6.add(lbCNUF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- IDCNU ----
              IDCNU.setMinimumSize(new Dimension(100, 30));
              IDCNU.setMaximumSize(new Dimension(100, 30));
              IDCNU.setPreferredSize(new Dimension(100, 30));
              IDCNU.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDCNU.setName("IDCNU");
              sNPanel6.add(IDCNU, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- lbAdherent ----
              lbAdherent.setText("Code adh\u00e9rent");
              lbAdherent.setName("lbAdherent");
              sNPanel6.add(lbAdherent, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDADH ----
              IDADH.setMinimumSize(new Dimension(150, 30));
              IDADH.setMaximumSize(new Dimension(150, 30));
              IDADH.setPreferredSize(new Dimension(150, 30));
              IDADH.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDADH.setName("IDADH");
              sNPanel6.add(IDADH, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite.add(sNPanel6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDescription.add(pnlDroite);
        }
        tpnFamille.addTab("Identification", pnlDescription);

        //======== pnlStocks ========
        {
          pnlStocks.setOpaque(false);
          pnlStocks.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlStocks.setName("pnlStocks");
          pnlStocks.setLayout(new GridLayout(1, 2));

          //======== pnlGauche2 ========
          {
            pnlGauche2.setName("pnlGauche2");
            pnlGauche2.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlGauche2.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlGauche2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlGauche2.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlGauche2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanel1.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
              ((GridBagLayout)sNPanel1.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)sNPanel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 1.0E-4};
              ((GridBagLayout)sNPanel1.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbClient ----
              lbClient.setText("Clients grand public 1");
              lbClient.setMaximumSize(new Dimension(250, 30));
              lbClient.setMinimumSize(new Dimension(200, 30));
              lbClient.setPreferredSize(new Dimension(200, 30));
              lbClient.setName("lbClient");
              sNPanel1.add(lbClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- snClient1 ----
              snClient1.setName("snClient1");
              sNPanel1.add(snClient1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbAffectation ----
              lbAffectation.setText("Affectation");
              lbAffectation.setMaximumSize(new Dimension(100, 30));
              lbAffectation.setMinimumSize(new Dimension(100, 30));
              lbAffectation.setPreferredSize(new Dimension(100, 30));
              lbAffectation.setName("lbAffectation");
              sNPanel1.add(lbAffectation, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDCG1 ----
              IDCG1.setModel(new DefaultComboBoxModel(new String[] {
                "Ticket de caisse",
                "Commande grand public",
                "Vente comptoir",
                "Aucune"
              }));
              IDCG1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              IDCG1.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDCG1.setBackground(Color.white);
              IDCG1.setName("IDCG1");
              sNPanel1.add(IDCG1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche2.add(sNPanel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== sNPanel2 ========
            {
              sNPanel2.setName("sNPanel2");
              sNPanel2.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
              ((GridBagLayout)sNPanel2.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)sNPanel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 1.0E-4};
              ((GridBagLayout)sNPanel2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbClient2 ----
              lbClient2.setText("Clients grand public 2");
              lbClient2.setMaximumSize(new Dimension(250, 30));
              lbClient2.setMinimumSize(new Dimension(200, 30));
              lbClient2.setPreferredSize(new Dimension(200, 30));
              lbClient2.setName("lbClient2");
              sNPanel2.add(lbClient2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- snClient2 ----
              snClient2.setName("snClient2");
              sNPanel2.add(snClient2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbAffectation2 ----
              lbAffectation2.setText("Affectation");
              lbAffectation2.setMaximumSize(new Dimension(100, 30));
              lbAffectation2.setMinimumSize(new Dimension(100, 30));
              lbAffectation2.setPreferredSize(new Dimension(100, 30));
              lbAffectation2.setName("lbAffectation2");
              sNPanel2.add(lbAffectation2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDCG2 ----
              IDCG2.setModel(new DefaultComboBoxModel(new String[] {
                "Ticket de caisse",
                "Commande grand public",
                "Vente comptoir",
                "Aucune"
              }));
              IDCG2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              IDCG2.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDCG2.setBackground(Color.white);
              IDCG2.setName("IDCG2");
              sNPanel2.add(IDCG2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche2.add(sNPanel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== sNPanel3 ========
            {
              sNPanel3.setName("sNPanel3");
              sNPanel3.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanel3.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
              ((GridBagLayout)sNPanel3.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)sNPanel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 1.0E-4};
              ((GridBagLayout)sNPanel3.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbClient3 ----
              lbClient3.setText("Clients grand public 3");
              lbClient3.setMaximumSize(new Dimension(250, 30));
              lbClient3.setMinimumSize(new Dimension(200, 30));
              lbClient3.setPreferredSize(new Dimension(200, 30));
              lbClient3.setName("lbClient3");
              sNPanel3.add(lbClient3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- snClient3 ----
              snClient3.setName("snClient3");
              sNPanel3.add(snClient3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbAffectation3 ----
              lbAffectation3.setText("Affectation");
              lbAffectation3.setMaximumSize(new Dimension(100, 30));
              lbAffectation3.setMinimumSize(new Dimension(100, 30));
              lbAffectation3.setPreferredSize(new Dimension(100, 30));
              lbAffectation3.setName("lbAffectation3");
              sNPanel3.add(lbAffectation3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDCG3 ----
              IDCG3.setModel(new DefaultComboBoxModel(new String[] {
                "Ticket de caisse",
                "Commande grand public",
                "Vente comptoir",
                "Aucune"
              }));
              IDCG3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              IDCG3.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDCG3.setBackground(Color.white);
              IDCG3.setName("IDCG3");
              sNPanel3.add(IDCG3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche2.add(sNPanel3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== sNPanel4 ========
            {
              sNPanel4.setName("sNPanel4");
              sNPanel4.setLayout(new GridBagLayout());
              ((GridBagLayout)sNPanel4.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
              ((GridBagLayout)sNPanel4.getLayout()).rowHeights = new int[] {0, 0};
              ((GridBagLayout)sNPanel4.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0, 1.0E-4};
              ((GridBagLayout)sNPanel4.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

              //---- lbClient4 ----
              lbClient4.setText("Clients grand public 4");
              lbClient4.setMaximumSize(new Dimension(250, 30));
              lbClient4.setMinimumSize(new Dimension(200, 30));
              lbClient4.setPreferredSize(new Dimension(200, 30));
              lbClient4.setName("lbClient4");
              sNPanel4.add(lbClient4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- snClient4 ----
              snClient4.setName("snClient4");
              sNPanel4.add(snClient4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- lbAffectation4 ----
              lbAffectation4.setText("Affectation");
              lbAffectation4.setMaximumSize(new Dimension(100, 30));
              lbAffectation4.setMinimumSize(new Dimension(100, 30));
              lbAffectation4.setPreferredSize(new Dimension(100, 30));
              lbAffectation4.setName("lbAffectation4");
              sNPanel4.add(lbAffectation4, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- IDCG4 ----
              IDCG4.setModel(new DefaultComboBoxModel(new String[] {
                "Ticket de caisse",
                "Commande grand public",
                "Vente comptoir",
                "Aucune"
              }));
              IDCG4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              IDCG4.setFont(new Font("sansserif", Font.PLAIN, 14));
              IDCG4.setBackground(Color.white);
              IDCG4.setName("IDCG4");
              sNPanel4.add(IDCG4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche2.add(sNPanel4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- IDCPTR ----
            IDCPTR.setText("Adresse obligatoire au comptoir");
            IDCPTR.setFont(new Font("sansserif", Font.PLAIN, 14));
            IDCPTR.setPreferredSize(new Dimension(223, 30));
            IDCPTR.setMinimumSize(new Dimension(223, 30));
            IDCPTR.setMaximumSize(new Dimension(225, 30));
            IDCPTR.setName("IDCPTR");
            pnlGauche2.add(IDCPTR, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlStocks.add(pnlGauche2);
        }
        tpnFamille.addTab("Clients grand public", pnlStocks);
      }
      pnlContenu.add(tpnFamille, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private JTabbedPane tpnFamille;
  private SNPanel pnlDescription;
  private SNPanelContenu pnlGauche;
  private SNPanelTitre pnlCoordonnees;
  private SNLabelChamp lbRue;
  private XRiTextField IDRUE;
  private SNLabelChamp lbLocalite;
  private XRiTextField IDLOC;
  private SNLabelChamp lbCommune;
  private SNCodePostalCommune snCodePostalCommune;
  private SNLabelChamp lbTelephone;
  private XRiTextField IDTEL;
  private SNLabelChamp lbFax;
  private XRiTextField IDFAX;
  private SNPanel pnlCompte;
  private SNLabelChamp lbCompteArrondis;
  private XRiTextField IDNCG;
  private SNPanelContenu pnlDroite;
  private SNPanel sNPanel5;
  private SNLabelChamp lbStatut;
  private XRiTextField IDSTA;
  private SNLabelChamp lbSIRET;
  private XRiTextField IDSIR;
  private SNLabelChamp lbAPE;
  private XRiTextField IDAPE;
  private SNLabelChamp lbRCS;
  private JScrollPane scrollPane1;
  private RiTextArea IDRCS;
  private SNLabelChamp lbTVA;
  private XRiTextField IDNII;
  private SNPanel sNPanel6;
  private SNLabelChamp lbCNUF;
  private XRiTextField IDCNU;
  private SNLabelChamp lbAdherent;
  private XRiTextField IDADH;
  private SNPanel pnlStocks;
  private SNPanelContenu pnlGauche2;
  private SNPanel sNPanel1;
  private SNLabelChamp lbClient;
  private SNClientPrincipal snClient1;
  private SNLabelChamp lbAffectation;
  private XRiComboBox IDCG1;
  private SNPanel sNPanel2;
  private SNLabelChamp lbClient2;
  private SNClientPrincipal snClient2;
  private SNLabelChamp lbAffectation2;
  private XRiComboBox IDCG2;
  private SNPanel sNPanel3;
  private SNLabelChamp lbClient3;
  private SNClientPrincipal snClient3;
  private SNLabelChamp lbAffectation3;
  private XRiComboBox IDCG3;
  private SNPanel sNPanel4;
  private SNLabelChamp lbClient4;
  private SNClientPrincipal snClient4;
  private SNLabelChamp lbAffectation4;
  private XRiComboBox IDCG4;
  private XRiCheckBox IDCPTR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
