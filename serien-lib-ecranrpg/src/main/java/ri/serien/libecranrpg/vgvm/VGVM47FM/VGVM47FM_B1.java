
package ri.serien.libecranrpg.vgvm.VGVM47FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM47FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM47FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EXLIB@")).trim());
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TRLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    E1CTR.setEnabled(lexique.isPresent("E1CTR"));
    E1MEX.setEnabled(lexique.isPresent("E1MEX"));
    E1PRE.setEnabled(lexique.isPresent("E1PRE"));
    E1VEH.setEnabled(lexique.isPresent("E1VEH"));
    OBJ_24.setVisible(lexique.isPresent("TRLIB"));
    OBJ_21.setVisible(lexique.isPresent("EXLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm47"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_21 = new RiZoneSortie();
    OBJ_24 = new RiZoneSortie();
    OBJ_19 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_17 = new JLabel();
    E1VEH = new XRiTextField();
    E1PRE = new XRiTextField();
    E1MEX = new XRiTextField();
    E1CTR = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Informations sur le pr\u00e9parateur"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_21 ----
          OBJ_21.setText("@EXLIB@");
          OBJ_21.setName("OBJ_21");
          panel2.add(OBJ_21);
          OBJ_21.setBounds(210, 67, 195, OBJ_21.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("@TRLIB@");
          OBJ_24.setName("OBJ_24");
          panel2.add(OBJ_24);
          OBJ_24.setBounds(210, 97, 161, OBJ_24.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Mode d'exp\u00e9dition");
          OBJ_19.setName("OBJ_19");
          panel2.add(OBJ_19);
          OBJ_19.setBounds(30, 69, 132, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Code transporteur");
          OBJ_22.setName("OBJ_22");
          panel2.add(OBJ_22);
          OBJ_22.setBounds(30, 99, 128, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Code V\u00e9hicule");
          OBJ_25.setName("OBJ_25");
          panel2.add(OBJ_25);
          OBJ_25.setBounds(30, 129, 109, 20);

          //---- OBJ_17 ----
          OBJ_17.setText("Pr\u00e9parateur");
          OBJ_17.setName("OBJ_17");
          panel2.add(OBJ_17);
          OBJ_17.setBounds(30, 39, 92, 20);

          //---- E1VEH ----
          E1VEH.setComponentPopupMenu(BTD);
          E1VEH.setName("E1VEH");
          panel2.add(E1VEH);
          E1VEH.setBounds(165, 125, 110, E1VEH.getPreferredSize().height);

          //---- E1PRE ----
          E1PRE.setComponentPopupMenu(BTD);
          E1PRE.setName("E1PRE");
          panel2.add(E1PRE);
          E1PRE.setBounds(165, 35, 40, E1PRE.getPreferredSize().height);

          //---- E1MEX ----
          E1MEX.setComponentPopupMenu(BTD);
          E1MEX.setName("E1MEX");
          panel2.add(E1MEX);
          E1MEX.setBounds(165, 65, 34, E1MEX.getPreferredSize().height);

          //---- E1CTR ----
          E1CTR.setComponentPopupMenu(BTD);
          E1CTR.setName("E1CTR");
          panel2.add(E1CTR);
          E1CTR.setBounds(165, 95, 34, E1CTR.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie OBJ_21;
  private RiZoneSortie OBJ_24;
  private JLabel OBJ_19;
  private JLabel OBJ_22;
  private JLabel OBJ_25;
  private JLabel OBJ_17;
  private XRiTextField E1VEH;
  private XRiTextField E1PRE;
  private XRiTextField E1MEX;
  private XRiTextField E1CTR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
