
package ri.serien.libecranrpg.vgvm.VGVM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM50FM_P2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVM50FM_P2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LIBENC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBENC2@")).trim());
    ATDT2X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDT2X@")).trim());
    ATIVA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATIVA2@")).trim());
    LBDAT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBDAT2@")).trim());
    W2K02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2K02@")).trim());
    W2P01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P01@")).trim());
    W2P02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W2P02@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDEV2@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIB2@")).trim());
    ATDT1X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATDT1X@")).trim());
    ATIVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATIVA@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBENC1@")).trim());
    LBDAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBDAT@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BASE@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    LIBDEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    label3.setVisible(lexique.isPresent("ATDEV2"));
    label1.setVisible(ATDEV.isVisible());
    label4.setVisible(lexique.isPresent("ATDEV2"));
    label5.setVisible(lexique.isPresent("ATDEV2"));
    label1.setVisible(!lexique.HostFieldGetData("ATDEV").trim().equals(""));
    label2.setVisible(!lexique.HostFieldGetData("ATDEV").trim().equals(""));
    OBJ_75.setVisible(lexique.isPresent("WCHGX"));
    
    // en création panel choix de devise à gauche
    if (lexique.isTrue("51")) {
      panel6.setVisible(false);
      panel7.setVisible(true);
      ATDEV.setVisible(false);
      DEV.setVisible(true);
      LIBDEV.setVisible(true);
      riBoutonDetail1.setVisible(true);
      OBJ_51.setText("Choix de la devise");
      OBJ_52.setVisible(false);
    }
    else {
      
      panel6.setVisible(true);
      panel7.setVisible(false);
      // message "aucun tarif précédent" (94 mais pas 51)
      if (lexique.isTrue("94")) {
        panel6.setVisible(false);
        panel7.setVisible(true);
        DEV.setVisible(false);
        LIBDEV.setVisible(false);
        riBoutonDetail1.setVisible(false);
        OBJ_51.setText("Aucun tarif précédent");
        OBJ_52.setText("sur cette devise");
      }
    }
    
    DEV.setText(lexique.HostFieldGetData("ATDEV"));
    LIBDEV.setText(lexique.HostFieldGetData("DVLIBR"));
    
    // Titre
    Object objet = lexique.getValeurVariableGlobale("CODE_ARTICLE_COURS");
    String codeArticle = "";
    if (objet != null) {
      codeArticle = (String) objet;
    }
    
    // Gestion deux ou quatre décimales
    boolean is4decimales = lexique.isTrue("50");
    T1P01.setVisible(!W1P01.isVisible() && is4decimales);
    T1P02.setVisible(!W1P02.isVisible() && is4decimales);
    
    if (is4decimales) {
      W2P01.setText(lexique.HostFieldGetData("T2P01"));
      W2P02.setText(lexique.HostFieldGetData("T2P02"));
      
    }
    
    // Gestion des dates
    ATDT2X.setVisible(lexique.isTrue("66") && lexique.isTrue("68"));
    ATIVA2.setVisible(ATDT2X.isVisible());
    ATDT1X.setVisible(ATDT2X.isVisible());
    ATIVA.setVisible(ATDT2X.isVisible());
    OBJ_31.setVisible(ATDT2X.isVisible());
    OBJ_33.setVisible(ATIVA2.isVisible());
    OBJ_35.setVisible(ATDT1X.isVisible());
    OBJ_37.setVisible(ATIVA.isVisible());
    
    if (lexique.isTrue("91")) {
      bouton_fin.setText("Fiche article");
    }
    else {
      bouton_fin.setText("Retour");
    }
    
    String titre = "";
    // Transport
    if (lexique.isTrue("76")) {
      titre = "Gestion des tarifs articles transport";
      // Panel tarif en cours
      label8.setVisible(false);
      label9.setText("Prix HT");
      label27.setVisible(false);
      label28.setVisible(false);
      // Panel tarif précedent
      label6.setText("Prix HT");
      label7.setVisible(false);
      W2K02.setVisible(false);
      W2P02.setVisible(false);
      label50.setVisible(false);
      label51.setVisible(false);
    }
    // Palette
    else {
      titre = "Gestion des tarifs articles sur-conditionnement (palettes) : " + codeArticle;
      // Panel tarif en cours
      label8.setVisible(true);
      label9.setText("Consignation");
      label27.setVisible(true);
      label27.setVisible(true);
      // Panel tarif précedent
      label6.setText("Consignation");
      label7.setVisible(true);
      W2K02.setVisible(true);
      W2P02.setVisible(true);
      label50.setVisible(true);
      label51.setVisible(true);
    }
    setTitle(titre);
    
    button1.setIcon(lexique.chargerImage("images/fleche_pre.png", true));
    button2.setIcon(lexique.chargerImage("images/fleche_sui.png", true));
    
    bouton_fin.setIcon(lexique.chargerImage("images/fin_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("ATRON", 0, ATRON_Value[ATRON.getSelectedIndex()]);
    // lexique.HostFieldPutData("AR", 0, ATRON2_Value[ATRON2.getSelectedIndex()]);
    
    if (DEV.isVisible() && !DEV.getText().trim().equals("")) {
      lexique.HostFieldPutData("ATDEV", 1, DEV.getText());
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void button1ActionPerformed() {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void button2ActionPerformed() {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("ATDEV");
    lexique.HostScreenSendKey(this, "F4");
    // DEV.setText(lexique.HostFieldGetData("ATDEV"));
    // LIBDEV.setText(lexique.HostFieldGetData("DVLIBR"));
  }
  
  private void DEVKeyReleased(KeyEvent e) {
    DEV.setText(DEV.getText().trim().toUpperCase());
  }
  
  private void DEVKeyPressed(KeyEvent e) {
    DEV.setText(DEV.getText().trim().toUpperCase());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    navig_fin = new RiMenu();
    bouton_fin = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel6 = new JPanel();
    ATDAX2 = new XRiCalendrier();
    OBJ_31 = new JLabel();
    LIBENC2 = new JLabel();
    ATDT2X = new RiZoneSortie();
    ATIVA2 = new RiZoneSortie();
    OBJ_33 = new JLabel();
    panel1 = new JPanel();
    LBDAT2 = new RiZoneSortie();
    panel9 = new JPanel();
    W2K02 = new RiZoneSortie();
    W2P01 = new RiZoneSortie();
    W2P02 = new RiZoneSortie();
    label50 = new JLabel();
    label51 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label3 = new JLabel();
    label4 = new RiZoneSortie();
    label5 = new RiZoneSortie();
    P_Centre = new JPanel();
    button1 = new JButton();
    button2 = new JButton();
    panel5 = new JPanel();
    ATDT1X = new RiZoneSortie();
    OBJ_35 = new JLabel();
    ATIVA = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_30 = new JLabel();
    ATDAPX = new XRiCalendrier();
    panel2 = new JPanel();
    LBDAT = new RiZoneSortie();
    OBJ_75 = new JLabel();
    WCHGX = new XRiTextField();
    OBJ_76 = new JLabel();
    WBAS = new XRiTextField();
    panel8 = new JPanel();
    W1K02 = new XRiTextField();
    W1P01 = new XRiTextField();
    W1P02 = new XRiTextField();
    label27 = new JLabel();
    label28 = new JLabel();
    T1P01 = new XRiTextField();
    T1P02 = new XRiTextField();
    label8 = new JLabel();
    label9 = new JLabel();
    label1 = new JLabel();
    ATDEV = new XRiTextField();
    label2 = new RiZoneSortie();
    panel7 = new JPanel();
    riBoutonDetail1 = new SNBoutonDetail();
    DEV = new XRiTextField();
    LIBDEV = new RiZoneSortie();
    OBJ_51 = new JLabel();
    OBJ_52 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(890, 430));
    setPreferredSize(new Dimension(890, 430));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Historique tarifs");
            bouton_retour.setToolTipText("Historique tarifs");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);

          //======== navig_fin ========
          {
            navig_fin.setName("navig_fin");

            //---- bouton_fin ----
            bouton_fin.setText("Retour");
            bouton_fin.setToolTipText("Retour sur l'article");
            bouton_fin.setName("bouton_fin");
            bouton_fin.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_bt3ActionPerformed(e);
              }
            });
            navig_fin.add(bouton_fin);
          }
          menus_bas.add(navig_fin);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 350));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel6 ========
        {
          panel6.setBorder(new TitledBorder(""));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);

          //---- ATDAX2 ----
          ATDAX2.setName("ATDAX2");
          panel6.add(ATDAX2);
          ATDAX2.setBounds(190, 15, 105, ATDAX2.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("Date validation");
          OBJ_31.setName("OBJ_31");
          panel6.add(OBJ_31);
          OBJ_31.setBounds(10, 200, 100, 20);

          //---- LIBENC2 ----
          LIBENC2.setText("@LBENC2@");
          LIBENC2.setHorizontalAlignment(SwingConstants.RIGHT);
          LIBENC2.setName("LIBENC2");
          panel6.add(LIBENC2);
          LIBENC2.setBounds(35, 19, 145, 20);

          //---- ATDT2X ----
          ATDT2X.setComponentPopupMenu(null);
          ATDT2X.setText("@ATDT2X@");
          ATDT2X.setHorizontalAlignment(SwingConstants.CENTER);
          ATDT2X.setName("ATDT2X");
          panel6.add(ATDT2X);
          ATDT2X.setBounds(105, 198, 70, ATDT2X.getPreferredSize().height);

          //---- ATIVA2 ----
          ATIVA2.setComponentPopupMenu(null);
          ATIVA2.setText("@ATIVA2@");
          ATIVA2.setName("ATIVA2");
          panel6.add(ATIVA2);
          ATIVA2.setBounds(255, 198, 40, ATIVA2.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("par");
          OBJ_33.setName("OBJ_33");
          panel6.add(OBJ_33);
          OBJ_33.setBounds(215, 200, 35, 20);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- LBDAT2 ----
            LBDAT2.setText("@LBDAT2@");
            LBDAT2.setComponentPopupMenu(null);
            LBDAT2.setName("LBDAT2");
            panel1.add(LBDAT2);
            LBDAT2.setBounds(5, 10, 290, LBDAT2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          panel6.add(panel1);
          panel1.setBounds(5, 230, 305, 45);

          //======== panel9 ========
          {
            panel9.setOpaque(false);
            panel9.setName("panel9");
            panel9.setLayout(null);

            //---- W2K02 ----
            W2K02.setText("@W2K02@");
            W2K02.setHorizontalAlignment(SwingConstants.RIGHT);
            W2K02.setName("W2K02");
            panel9.add(W2K02);
            W2K02.setBounds(145, 65, 54, W2K02.getPreferredSize().height);

            //---- W2P01 ----
            W2P01.setText("@W2P01@");
            W2P01.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P01.setName("W2P01");
            panel9.add(W2P01);
            W2P01.setBounds(205, 30, 92, W2P01.getPreferredSize().height);

            //---- W2P02 ----
            W2P02.setText("@W2P02@");
            W2P02.setHorizontalAlignment(SwingConstants.RIGHT);
            W2P02.setName("W2P02");
            panel9.add(W2P02);
            W2P02.setBounds(205, 65, 92, W2P02.getPreferredSize().height);

            //---- label50 ----
            label50.setText("Coeff.");
            label50.setFont(label50.getFont().deriveFont(label50.getFont().getStyle() | Font.BOLD));
            label50.setName("label50");
            panel9.add(label50);
            label50.setBounds(145, 0, 53, 25);

            //---- label51 ----
            label51.setText("Prix");
            label51.setFont(label51.getFont().deriveFont(label51.getFont().getStyle() | Font.BOLD));
            label51.setName("label51");
            panel9.add(label51);
            label51.setBounds(205, 0, 92, 25);

            //---- label6 ----
            label6.setText("Consignation");
            label6.setName("label6");
            panel9.add(label6);
            label6.setBounds(20, 30, 125, 24);

            //---- label7 ----
            label7.setText("D\u00e9consignation");
            label7.setName("label7");
            panel9.add(label7);
            label7.setBounds(20, 65, 125, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel9.getComponentCount(); i++) {
                Rectangle bounds = panel9.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel9.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel9.setMinimumSize(preferredSize);
              panel9.setPreferredSize(preferredSize);
            }
          }
          panel6.add(panel9);
          panel9.setBounds(5, 85, 300, 110);

          //---- label3 ----
          label3.setText("Devise");
          label3.setName("label3");
          panel6.add(label3);
          label3.setBounds(10, 285, 50, 24);

          //---- label4 ----
          label4.setText("@ATDEV2@");
          label4.setComponentPopupMenu(null);
          label4.setName("label4");
          panel6.add(label4);
          label4.setBounds(60, 285, 40, label4.getPreferredSize().height);

          //---- label5 ----
          label5.setText("@DVLIB2@");
          label5.setComponentPopupMenu(null);
          label5.setName("label5");
          panel6.add(label5);
          label5.setBounds(110, 285, 188, label5.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel6);
        panel6.setBounds(35, 15, 320, 400);

        //======== P_Centre ========
        {
          P_Centre.setOpaque(false);
          P_Centre.setName("P_Centre");
          P_Centre.setLayout(null);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < P_Centre.getComponentCount(); i++) {
              Rectangle bounds = P_Centre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = P_Centre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            P_Centre.setMinimumSize(preferredSize);
            P_Centre.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(P_Centre);
        P_Centre.setBounds(new Rectangle(new Point(35, 15), P_Centre.getPreferredSize()));

        //---- button1 ----
        button1.setToolTipText("Tarifs pr\u00e9c\u00e9dents");
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button1ActionPerformed();
          }
        });
        p_contenu.add(button1);
        button1.setBounds(5, 15, 25, 400);

        //---- button2 ----
        button2.setToolTipText("Tarifs suivants");
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button2ActionPerformed();
          }
        });
        p_contenu.add(button2);
        button2.setBounds(690, 15, 25, 400);

        //======== panel5 ========
        {
          panel5.setBorder(new TitledBorder(""));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);

          //---- ATDT1X ----
          ATDT1X.setComponentPopupMenu(null);
          ATDT1X.setText("@ATDT1X@");
          ATDT1X.setHorizontalAlignment(SwingConstants.CENTER);
          ATDT1X.setName("ATDT1X");
          panel5.add(ATDT1X);
          ATDT1X.setBounds(105, 198, 70, ATDT1X.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Date validation");
          OBJ_35.setName("OBJ_35");
          panel5.add(OBJ_35);
          OBJ_35.setBounds(10, 200, 100, 20);

          //---- ATIVA ----
          ATIVA.setComponentPopupMenu(null);
          ATIVA.setText("@ATIVA@");
          ATIVA.setName("ATIVA");
          panel5.add(ATIVA);
          ATIVA.setBounds(250, 198, 40, ATIVA.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("par");
          OBJ_37.setName("OBJ_37");
          panel5.add(OBJ_37);
          OBJ_37.setBounds(215, 198, 40, 25);

          //---- OBJ_30 ----
          OBJ_30.setText("@LBENC1@");
          OBJ_30.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_30.setName("OBJ_30");
          panel5.add(OBJ_30);
          OBJ_30.setBounds(35, 19, 145, 20);

          //---- ATDAPX ----
          ATDAPX.setName("ATDAPX");
          panel5.add(ATDAPX);
          ATDAPX.setBounds(190, 15, 105, ATDAPX.getPreferredSize().height);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- LBDAT ----
            LBDAT.setText("@LBDAT@");
            LBDAT.setComponentPopupMenu(null);
            LBDAT.setName("LBDAT");
            panel2.add(LBDAT);
            LBDAT.setBounds(5, 10, 285, LBDAT.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Change");
            OBJ_75.setName("OBJ_75");
            panel2.add(OBJ_75);
            OBJ_75.setBounds(5, 45, 60, 18);

            //---- WCHGX ----
            WCHGX.setComponentPopupMenu(BTD);
            WCHGX.setHorizontalAlignment(SwingConstants.RIGHT);
            WCHGX.setName("WCHGX");
            panel2.add(WCHGX);
            WCHGX.setBounds(100, 40, 90, WCHGX.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("@BASE@");
            OBJ_76.setName("OBJ_76");
            panel2.add(OBJ_76);
            OBJ_76.setBounds(5, 75, 85, 18);

            //---- WBAS ----
            WBAS.setComponentPopupMenu(BTD);
            WBAS.setHorizontalAlignment(SwingConstants.RIGHT);
            WBAS.setName("WBAS");
            panel2.add(WBAS);
            WBAS.setBounds(100, 70, 60, WBAS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          panel5.add(panel2);
          panel2.setBounds(5, 230, 296, panel2.getPreferredSize().height);

          //======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(null);

            //---- W1K02 ----
            W1K02.setName("W1K02");
            panel8.add(W1K02);
            W1K02.setBounds(140, 63, 54, W1K02.getPreferredSize().height);

            //---- W1P01 ----
            W1P01.setName("W1P01");
            panel8.add(W1P01);
            W1P01.setBounds(195, 28, 92, W1P01.getPreferredSize().height);

            //---- W1P02 ----
            W1P02.setName("W1P02");
            panel8.add(W1P02);
            W1P02.setBounds(195, 63, 92, W1P02.getPreferredSize().height);

            //---- label27 ----
            label27.setText("Coeff.");
            label27.setFont(label27.getFont().deriveFont(label27.getFont().getStyle() | Font.BOLD));
            label27.setName("label27");
            panel8.add(label27);
            label27.setBounds(140, 5, 53, 20);

            //---- label28 ----
            label28.setText("Prix");
            label28.setFont(label28.getFont().deriveFont(label28.getFont().getStyle() | Font.BOLD));
            label28.setName("label28");
            panel8.add(label28);
            label28.setBounds(200, 5, 85, 20);

            //---- T1P01 ----
            T1P01.setName("T1P01");
            panel8.add(T1P01);
            T1P01.setBounds(195, 28, 92, 28);

            //---- T1P02 ----
            T1P02.setName("T1P02");
            panel8.add(T1P02);
            T1P02.setBounds(195, 63, 92, 28);

            //---- label8 ----
            label8.setText("D\u00e9consignation");
            label8.setName("label8");
            panel8.add(label8);
            label8.setBounds(15, 65, 125, 24);

            //---- label9 ----
            label9.setText("Consignation");
            label9.setName("label9");
            panel8.add(label9);
            label9.setBounds(15, 30, 125, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }
          panel5.add(panel8);
          panel8.setBounds(10, 85, 295, 111);

          //---- label1 ----
          label1.setText("Devise");
          label1.setName("label1");
          panel5.add(label1);
          label1.setBounds(10, 335, 50, 28);

          //---- ATDEV ----
          ATDEV.setName("ATDEV");
          panel5.add(ATDEV);
          ATDEV.setBounds(60, 335, 40, ATDEV.getPreferredSize().height);

          //---- label2 ----
          label2.setText("@DVLIBR@");
          label2.setComponentPopupMenu(null);
          label2.setName("label2");
          panel5.add(label2);
          label2.setBounds(105, 337, 195, label2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel5);
        panel5.setBounds(365, 15, 320, 400);

        //======== panel7 ========
        {
          panel7.setBorder(new TitledBorder(""));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);

          //---- riBoutonDetail1 ----
          riBoutonDetail1.setName("riBoutonDetail1");
          riBoutonDetail1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail1ActionPerformed(e);
            }
          });
          panel7.add(riBoutonDetail1);
          riBoutonDetail1.setBounds(70, 234, 25, 30);

          //---- DEV ----
          DEV.setComponentPopupMenu(null);
          DEV.setName("DEV");
          DEV.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
              DEVKeyPressed(e);
            }
            @Override
            public void keyReleased(KeyEvent e) {
              DEVKeyReleased(e);
            }
          });
          panel7.add(DEV);
          DEV.setBounds(30, 235, 44, DEV.getPreferredSize().height);

          //---- LIBDEV ----
          LIBDEV.setText("@LIBDEV@");
          LIBDEV.setName("LIBDEV");
          panel7.add(LIBDEV);
          LIBDEV.setBounds(95, 235, 180, LIBDEV.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Aucun tarif pr\u00e9c\u00e9dent");
          OBJ_51.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_51.setFont(OBJ_51.getFont().deriveFont(OBJ_51.getFont().getStyle() | Font.BOLD, OBJ_51.getFont().getSize() + 4f));
          OBJ_51.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_51.setName("OBJ_51");
          panel7.add(OBJ_51);
          OBJ_51.setBounds(25, 155, 255, 49);

          //---- OBJ_52 ----
          OBJ_52.setText("sur cette devise");
          OBJ_52.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_52.setFont(OBJ_52.getFont().deriveFont(OBJ_52.getFont().getStyle() | Font.BOLD, OBJ_52.getFont().getSize() + 4f));
          OBJ_52.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_52.setName("OBJ_52");
          panel7.add(OBJ_52);
          OBJ_52.setBounds(25, 220, 255, 49);
        }
        p_contenu.add(panel7);
        panel7.setBounds(35, 15, 320, 400);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu navig_fin;
  private RiMenu_bt bouton_fin;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel6;
  private XRiCalendrier ATDAX2;
  private JLabel OBJ_31;
  private JLabel LIBENC2;
  private RiZoneSortie ATDT2X;
  private RiZoneSortie ATIVA2;
  private JLabel OBJ_33;
  private JPanel panel1;
  private RiZoneSortie LBDAT2;
  private JPanel panel9;
  private RiZoneSortie W2K02;
  private RiZoneSortie W2P01;
  private RiZoneSortie W2P02;
  private JLabel label50;
  private JLabel label51;
  private JLabel label6;
  private JLabel label7;
  private JLabel label3;
  private RiZoneSortie label4;
  private RiZoneSortie label5;
  private JPanel P_Centre;
  private JButton button1;
  private JButton button2;
  private JPanel panel5;
  private RiZoneSortie ATDT1X;
  private JLabel OBJ_35;
  private RiZoneSortie ATIVA;
  private JLabel OBJ_37;
  private JLabel OBJ_30;
  private XRiCalendrier ATDAPX;
  private JPanel panel2;
  private RiZoneSortie LBDAT;
  private JLabel OBJ_75;
  private XRiTextField WCHGX;
  private JLabel OBJ_76;
  private XRiTextField WBAS;
  private JPanel panel8;
  private XRiTextField W1K02;
  private XRiTextField W1P01;
  private XRiTextField W1P02;
  private JLabel label27;
  private JLabel label28;
  private XRiTextField T1P01;
  private XRiTextField T1P02;
  private JLabel label8;
  private JLabel label9;
  private JLabel label1;
  private XRiTextField ATDEV;
  private RiZoneSortie label2;
  private JPanel panel7;
  private SNBoutonDetail riBoutonDetail1;
  private XRiTextField DEV;
  private RiZoneSortie LIBDEV;
  private JLabel OBJ_51;
  private JLabel OBJ_52;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
