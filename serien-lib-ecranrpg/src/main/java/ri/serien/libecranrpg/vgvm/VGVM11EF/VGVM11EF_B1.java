
package ri.serien.libecranrpg.vgvm.VGVM11EF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11EF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM11EF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    E1CLLP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLP@")).trim());
    E1CLLS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLLS@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L32GCD@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LREP2@")).trim());
    OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LREP1@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP01@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP02@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP03@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP04@")).trim());
    OBJ_90.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TZP05@")).trim());
    WNBRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBRA@")).trim());
    E1NUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NUM@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    E1SUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1SUF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    E1SUF.setVisible(lexique.isPresent("E1SUF"));
    
    OBJ_69.setVisible(lexique.isPresent("WREP"));
    OBJ_33.setVisible(lexique.isPresent("L32GCD"));
    OBJ_96.setVisible(lexique.isPresent("WNBRA"));
    OBJ_76.setVisible(lexique.isPresent("LREP2"));
    OBJ_75.setVisible(lexique.isPresent("LREP1"));
    OBJ_91.setVisible(lexique.isTrue("94"));
    OBJ_92.setVisible(lexique.isTrue("94"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@AFFI@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    E3NOM = new XRiTextField();
    E3CPL = new XRiTextField();
    E3RUE = new XRiTextField();
    E3LOC = new XRiTextField();
    E3CDP = new XRiTextField();
    E3VILR = new XRiTextField();
    E3PAY = new XRiTextField();
    xTitledPanel5 = new JXTitledPanel();
    E2NOM = new XRiTextField();
    E2CPL = new XRiTextField();
    E2RUE = new XRiTextField();
    E2LOC = new XRiTextField();
    E2VILR = new XRiTextField();
    E2CDP = new XRiTextField();
    E2PAY = new XRiTextField();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_30 = new JLabel();
    E1CLLP = new RiZoneSortie();
    E1CLLS = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    OBJ_73 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_77 = new JLabel();
    E1ACT = new XRiTextField();
    OBJ_78 = new JLabel();
    E1SAN = new XRiTextField();
    E1RCC = new XRiTextField();
    E1NCC = new XRiTextField();
    OBJ_74 = new JLabel();
    E1REP = new XRiTextField();
    E1TRP = new XRiTextField();
    E1REP2 = new XRiTextField();
    E1TRP2 = new XRiTextField();
    OBJ_76 = new RiZoneSortie();
    OBJ_75 = new RiZoneSortie();
    label1 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_79 = new JLabel();
    OBJ_80 = new JLabel();
    E1ZTR = new XRiTextField();
    OBJ_83 = new JLabel();
    E1CTR = new XRiTextField();
    OBJ_82 = new JLabel();
    E1MEX = new XRiTextField();
    OBJ_81 = new JLabel();
    OBJ_86 = new JLabel();
    E1TP1 = new XRiTextField();
    OBJ_87 = new JLabel();
    E1TP2 = new XRiTextField();
    OBJ_88 = new JLabel();
    E1TP3 = new XRiTextField();
    OBJ_89 = new JLabel();
    E1TP4 = new XRiTextField();
    OBJ_90 = new JLabel();
    E1TP5 = new XRiTextField();
    E1DLPX = new XRiCalendrier();
    E1DLSX = new XRiCalendrier();
    E1DT3X = new XRiCalendrier();
    E1DT1X = new XRiCalendrier();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_84 = new JLabel();
    E1COL = new XRiTextField();
    OBJ_85 = new JLabel();
    E1PDS = new XRiTextField();
    OBJ_96 = new JLabel();
    WNBRA = new RiZoneSortie();
    barre_tete = new JMenuBar();
    panel6 = new JPanel();
    lib_numero = new JLabel();
    E1NUM = new RiZoneSortie();
    E1NFA = new RiZoneSortie();
    E1SUF = new RiZoneSortie();
    OBJ_69 = new JLabel();
    WREP = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1095, 465));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(150, 0));
        p_menus.setMinimumSize(new Dimension(150, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== xTitledPanel3 ========
        {
          xTitledPanel3.setBorder(new DropShadowBorder());
          xTitledPanel3.setTitle("Adresse de facturation");
          xTitledPanel3.setName("xTitledPanel3");
          Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
          xTitledPanel3ContentContainer.setLayout(null);

          //---- E3NOM ----
          E3NOM.setComponentPopupMenu(BTD);
          E3NOM.setName("E3NOM");
          xTitledPanel3ContentContainer.add(E3NOM);
          E3NOM.setBounds(5, 10, 310, E3NOM.getPreferredSize().height);

          //---- E3CPL ----
          E3CPL.setComponentPopupMenu(BTD);
          E3CPL.setName("E3CPL");
          xTitledPanel3ContentContainer.add(E3CPL);
          E3CPL.setBounds(5, 40, 310, E3CPL.getPreferredSize().height);

          //---- E3RUE ----
          E3RUE.setComponentPopupMenu(BTD);
          E3RUE.setName("E3RUE");
          xTitledPanel3ContentContainer.add(E3RUE);
          E3RUE.setBounds(5, 70, 310, E3RUE.getPreferredSize().height);

          //---- E3LOC ----
          E3LOC.setComponentPopupMenu(BTD);
          E3LOC.setName("E3LOC");
          xTitledPanel3ContentContainer.add(E3LOC);
          E3LOC.setBounds(5, 100, 310, E3LOC.getPreferredSize().height);

          //---- E3CDP ----
          E3CDP.setComponentPopupMenu(BTD);
          E3CDP.setName("E3CDP");
          xTitledPanel3ContentContainer.add(E3CDP);
          E3CDP.setBounds(5, 130, 52, E3CDP.getPreferredSize().height);

          //---- E3VILR ----
          E3VILR.setComponentPopupMenu(BTD);
          E3VILR.setName("E3VILR");
          xTitledPanel3ContentContainer.add(E3VILR);
          E3VILR.setBounds(65, 130, 250, E3VILR.getPreferredSize().height);

          //---- E3PAY ----
          E3PAY.setComponentPopupMenu(BTD);
          E3PAY.setName("E3PAY");
          xTitledPanel3ContentContainer.add(E3PAY);
          E3PAY.setBounds(5, 160, 310, E3PAY.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel3ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel5 ========
        {
          xTitledPanel5.setBorder(new DropShadowBorder());
          xTitledPanel5.setTitle("Adresse de livraison");
          xTitledPanel5.setName("xTitledPanel5");
          Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
          xTitledPanel5ContentContainer.setLayout(null);

          //---- E2NOM ----
          E2NOM.setComponentPopupMenu(BTD);
          E2NOM.setName("E2NOM");
          xTitledPanel5ContentContainer.add(E2NOM);
          E2NOM.setBounds(5, 10, 310, E2NOM.getPreferredSize().height);

          //---- E2CPL ----
          E2CPL.setComponentPopupMenu(BTD);
          E2CPL.setName("E2CPL");
          xTitledPanel5ContentContainer.add(E2CPL);
          E2CPL.setBounds(5, 40, 310, E2CPL.getPreferredSize().height);

          //---- E2RUE ----
          E2RUE.setComponentPopupMenu(BTD);
          E2RUE.setName("E2RUE");
          xTitledPanel5ContentContainer.add(E2RUE);
          E2RUE.setBounds(5, 70, 310, E2RUE.getPreferredSize().height);

          //---- E2LOC ----
          E2LOC.setComponentPopupMenu(BTD);
          E2LOC.setName("E2LOC");
          xTitledPanel5ContentContainer.add(E2LOC);
          E2LOC.setBounds(5, 100, 310, E2LOC.getPreferredSize().height);

          //---- E2VILR ----
          E2VILR.setComponentPopupMenu(BTD);
          E2VILR.setName("E2VILR");
          xTitledPanel5ContentContainer.add(E2VILR);
          E2VILR.setBounds(65, 130, 250, E2VILR.getPreferredSize().height);

          //---- E2CDP ----
          E2CDP.setComponentPopupMenu(BTD);
          E2CDP.setName("E2CDP");
          xTitledPanel5ContentContainer.add(E2CDP);
          E2CDP.setBounds(5, 130, 52, E2CDP.getPreferredSize().height);

          //---- E2PAY ----
          E2PAY.setComponentPopupMenu(BTD);
          E2PAY.setName("E2PAY");
          xTitledPanel5ContentContainer.add(E2PAY);
          E2PAY.setBounds(5, 160, 310, E2PAY.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel5ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel4 ========
        {
          xTitledPanel4.setBorder(new DropShadowBorder());
          xTitledPanel4.setTitle("Client");
          xTitledPanel4.setName("xTitledPanel4");
          Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
          xTitledPanel4ContentContainer.setLayout(null);

          //---- OBJ_30 ----
          OBJ_30.setText("Num\u00e9ro client");
          OBJ_30.setName("OBJ_30");
          xTitledPanel4ContentContainer.add(OBJ_30);
          OBJ_30.setBounds(10, 14, 90, 20);

          //---- E1CLLP ----
          E1CLLP.setText("@E1CLLP@");
          E1CLLP.setName("E1CLLP");
          xTitledPanel4ContentContainer.add(E1CLLP);
          E1CLLP.setBounds(115, 12, 60, E1CLLP.getPreferredSize().height);

          //---- E1CLLS ----
          E1CLLS.setText("@E1CLLS@");
          E1CLLS.setName("E1CLLS");
          xTitledPanel4ContentContainer.add(E1CLLS);
          E1CLLS.setBounds(180, 12, 30, E1CLLS.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("@L32GCD@");
          OBJ_33.setComponentPopupMenu(BTD);
          OBJ_33.setName("OBJ_33");
          xTitledPanel4ContentContainer.add(OBJ_33);
          OBJ_33.setBounds(365, 12, 105, OBJ_33.getPreferredSize().height);

          //---- OBJ_73 ----
          OBJ_73.setText("Gencod");
          OBJ_73.setName("OBJ_73");
          xTitledPanel4ContentContainer.add(OBJ_73);
          OBJ_73.setBounds(305, 14, 51, 20);

          //---- OBJ_98 ----
          OBJ_98.setText("Affaire");
          OBJ_98.setName("OBJ_98");
          xTitledPanel4ContentContainer.add(OBJ_98);
          OBJ_98.setBounds(490, 14, 41, 20);

          //---- OBJ_77 ----
          OBJ_77.setText("R\u00e9f\u00e9rence Client");
          OBJ_77.setName("OBJ_77");
          xTitledPanel4ContentContainer.add(OBJ_77);
          OBJ_77.setBounds(10, 42, 102, 25);

          //---- E1ACT ----
          E1ACT.setComponentPopupMenu(BTD);
          E1ACT.setName("E1ACT");
          xTitledPanel4ContentContainer.add(E1ACT);
          E1ACT.setBounds(535, 10, 50, E1ACT.getPreferredSize().height);

          //---- OBJ_78 ----
          OBJ_78.setText("Section");
          OBJ_78.setName("OBJ_78");
          xTitledPanel4ContentContainer.add(OBJ_78);
          OBJ_78.setBounds(485, 44, 48, 20);

          //---- E1SAN ----
          E1SAN.setComponentPopupMenu(BTD);
          E1SAN.setName("E1SAN");
          xTitledPanel4ContentContainer.add(E1SAN);
          E1SAN.setBounds(535, 40, 50, E1SAN.getPreferredSize().height);

          //---- E1RCC ----
          E1RCC.setComponentPopupMenu(BTD);
          E1RCC.setName("E1RCC");
          xTitledPanel4ContentContainer.add(E1RCC);
          E1RCC.setBounds(210, 40, 262, E1RCC.getPreferredSize().height);

          //---- E1NCC ----
          E1NCC.setComponentPopupMenu(BTD);
          E1NCC.setName("E1NCC");
          xTitledPanel4ContentContainer.add(E1NCC);
          E1NCC.setBounds(115, 40, 90, E1NCC.getPreferredSize().height);

          //---- OBJ_74 ----
          OBJ_74.setText("Repr\u00e9sentant 1");
          OBJ_74.setName("OBJ_74");
          xTitledPanel4ContentContainer.add(OBJ_74);
          OBJ_74.setBounds(10, 74, 95, 20);

          //---- E1REP ----
          E1REP.setComponentPopupMenu(BTD);
          E1REP.setName("E1REP");
          xTitledPanel4ContentContainer.add(E1REP);
          E1REP.setBounds(115, 74, 34, E1REP.getPreferredSize().height);

          //---- E1TRP ----
          E1TRP.setComponentPopupMenu(BTD);
          E1TRP.setName("E1TRP");
          xTitledPanel4ContentContainer.add(E1TRP);
          E1TRP.setBounds(150, 74, 20, E1TRP.getPreferredSize().height);

          //---- E1REP2 ----
          E1REP2.setComponentPopupMenu(BTD);
          E1REP2.setName("E1REP2");
          xTitledPanel4ContentContainer.add(E1REP2);
          E1REP2.setBounds(115, 104, 34, E1REP2.getPreferredSize().height);

          //---- E1TRP2 ----
          E1TRP2.setComponentPopupMenu(BTD);
          E1TRP2.setName("E1TRP2");
          xTitledPanel4ContentContainer.add(E1TRP2);
          E1TRP2.setBounds(150, 104, 20, E1TRP2.getPreferredSize().height);

          //---- OBJ_76 ----
          OBJ_76.setText("@LREP2@");
          OBJ_76.setName("OBJ_76");
          xTitledPanel4ContentContainer.add(OBJ_76);
          OBJ_76.setBounds(175, 102, 410, OBJ_76.getPreferredSize().height);

          //---- OBJ_75 ----
          OBJ_75.setText("@LREP1@");
          OBJ_75.setName("OBJ_75");
          xTitledPanel4ContentContainer.add(OBJ_75);
          OBJ_75.setBounds(175, 72, 410, OBJ_75.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Repr\u00e9sentant 2");
          label1.setName("label1");
          xTitledPanel4ContentContainer.add(label1);
          label1.setBounds(10, 104, 100, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setBorder(new DropShadowBorder());
          xTitledPanel2.setTitle("Livraison");
          xTitledPanel2.setName("xTitledPanel2");
          Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
          xTitledPanel2ContentContainer.setLayout(null);

          //---- OBJ_79 ----
          OBJ_79.setText("Livraison pr\u00e9vue");
          OBJ_79.setName("OBJ_79");
          xTitledPanel2ContentContainer.add(OBJ_79);
          OBJ_79.setBounds(10, 14, 116, 20);

          //---- OBJ_80 ----
          OBJ_80.setText("Livraison souhait\u00e9e ");
          OBJ_80.setName("OBJ_80");
          xTitledPanel2ContentContainer.add(OBJ_80);
          OBJ_80.setBounds(10, 44, 115, 20);

          //---- E1ZTR ----
          E1ZTR.setComponentPopupMenu(BTD);
          E1ZTR.setName("E1ZTR");
          xTitledPanel2ContentContainer.add(E1ZTR);
          E1ZTR.setBounds(125, 172, 60, E1ZTR.getPreferredSize().height);

          //---- OBJ_83 ----
          OBJ_83.setText("Zone");
          OBJ_83.setName("OBJ_83");
          xTitledPanel2ContentContainer.add(OBJ_83);
          OBJ_83.setBounds(10, 180, 34, 20);

          //---- E1CTR ----
          E1CTR.setComponentPopupMenu(BTD);
          E1CTR.setName("E1CTR");
          xTitledPanel2ContentContainer.add(E1CTR);
          E1CTR.setBounds(125, 145, 34, E1CTR.getPreferredSize().height);

          //---- OBJ_82 ----
          OBJ_82.setText("Transporteur");
          OBJ_82.setName("OBJ_82");
          xTitledPanel2ContentContainer.add(OBJ_82);
          OBJ_82.setBounds(10, 150, 80, 20);

          //---- E1MEX ----
          E1MEX.setComponentPopupMenu(BTD);
          E1MEX.setName("E1MEX");
          xTitledPanel2ContentContainer.add(E1MEX);
          E1MEX.setBounds(125, 118, 34, E1MEX.getPreferredSize().height);

          //---- OBJ_81 ----
          OBJ_81.setText("Exp\u00e9dition");
          OBJ_81.setName("OBJ_81");
          xTitledPanel2ContentContainer.add(OBJ_81);
          OBJ_81.setBounds(10, 120, 64, 20);

          //---- OBJ_86 ----
          OBJ_86.setText("@TZP01@");
          OBJ_86.setName("OBJ_86");
          xTitledPanel2ContentContainer.add(OBJ_86);
          OBJ_86.setBounds(10, 205, 30, 20);

          //---- E1TP1 ----
          E1TP1.setComponentPopupMenu(BTD);
          E1TP1.setName("E1TP1");
          xTitledPanel2ContentContainer.add(E1TP1);
          E1TP1.setBounds(10, 225, 35, 30);

          //---- OBJ_87 ----
          OBJ_87.setText("@TZP02@");
          OBJ_87.setName("OBJ_87");
          xTitledPanel2ContentContainer.add(OBJ_87);
          OBJ_87.setBounds(60, 200, 33, 29);

          //---- E1TP2 ----
          E1TP2.setComponentPopupMenu(BTD);
          E1TP2.setName("E1TP2");
          xTitledPanel2ContentContainer.add(E1TP2);
          E1TP2.setBounds(60, 225, 38, 30);

          //---- OBJ_88 ----
          OBJ_88.setText("@TZP03@");
          OBJ_88.setName("OBJ_88");
          xTitledPanel2ContentContainer.add(OBJ_88);
          OBJ_88.setBounds(105, 200, 31, 29);

          //---- E1TP3 ----
          E1TP3.setComponentPopupMenu(BTD);
          E1TP3.setName("E1TP3");
          xTitledPanel2ContentContainer.add(E1TP3);
          E1TP3.setBounds(105, 225, 36, 30);

          //---- OBJ_89 ----
          OBJ_89.setText("@TZP04@");
          OBJ_89.setName("OBJ_89");
          xTitledPanel2ContentContainer.add(OBJ_89);
          OBJ_89.setBounds(150, 200, 34, 29);

          //---- E1TP4 ----
          E1TP4.setComponentPopupMenu(BTD);
          E1TP4.setName("E1TP4");
          xTitledPanel2ContentContainer.add(E1TP4);
          E1TP4.setBounds(150, 225, 34, 30);

          //---- OBJ_90 ----
          OBJ_90.setText("@TZP05@");
          OBJ_90.setName("OBJ_90");
          xTitledPanel2ContentContainer.add(OBJ_90);
          OBJ_90.setBounds(200, 200, 32, 29);

          //---- E1TP5 ----
          E1TP5.setComponentPopupMenu(BTD);
          E1TP5.setName("E1TP5");
          xTitledPanel2ContentContainer.add(E1TP5);
          E1TP5.setBounds(200, 225, 37, 30);

          //---- E1DLPX ----
          E1DLPX.setName("E1DLPX");
          xTitledPanel2ContentContainer.add(E1DLPX);
          E1DLPX.setBounds(125, 10, 105, E1DLPX.getPreferredSize().height);

          //---- E1DLSX ----
          E1DLSX.setName("E1DLSX");
          xTitledPanel2ContentContainer.add(E1DLSX);
          E1DLSX.setBounds(125, 37, 105, E1DLSX.getPreferredSize().height);

          //---- E1DT3X ----
          E1DT3X.setName("E1DT3X");
          xTitledPanel2ContentContainer.add(E1DT3X);
          E1DT3X.setBounds(125, 91, 105, 20);

          //---- E1DT1X ----
          E1DT1X.setName("E1DT1X");
          xTitledPanel2ContentContainer.add(E1DT1X);
          E1DT1X.setBounds(125, 64, 105, E1DT1X.getPreferredSize().height);

          //---- OBJ_91 ----
          OBJ_91.setText("Exp\u00e9dition effective");
          OBJ_91.setName("OBJ_91");
          xTitledPanel2ContentContainer.add(OBJ_91);
          OBJ_91.setBounds(10, 90, 115, 20);

          //---- OBJ_92 ----
          OBJ_92.setText("Date de facture");
          OBJ_92.setName("OBJ_92");
          xTitledPanel2ContentContainer.add(OBJ_92);
          OBJ_92.setBounds(10, 65, 116, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
          }
        }

        //======== xTitledPanel1 ========
        {
          xTitledPanel1.setBorder(new DropShadowBorder());
          xTitledPanel1.setTitle("Divers");
          xTitledPanel1.setName("xTitledPanel1");
          Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
          xTitledPanel1ContentContainer.setLayout(null);

          //---- OBJ_84 ----
          OBJ_84.setText("Nombre de colis");
          OBJ_84.setName("OBJ_84");
          xTitledPanel1ContentContainer.add(OBJ_84);
          OBJ_84.setBounds(10, 5, 102, 20);

          //---- E1COL ----
          E1COL.setComponentPopupMenu(BTD);
          E1COL.setName("E1COL");
          xTitledPanel1ContentContainer.add(E1COL);
          E1COL.setBounds(125, 5, 42, E1COL.getPreferredSize().height);

          //---- OBJ_85 ----
          OBJ_85.setText("Poids");
          OBJ_85.setName("OBJ_85");
          xTitledPanel1ContentContainer.add(OBJ_85);
          OBJ_85.setBounds(10, 30, 38, 20);

          //---- E1PDS ----
          E1PDS.setComponentPopupMenu(BTD);
          E1PDS.setName("E1PDS");
          xTitledPanel1ContentContainer.add(E1PDS);
          E1PDS.setBounds(125, 30, 74, E1PDS.getPreferredSize().height);

          //---- OBJ_96 ----
          OBJ_96.setText("Nombre d'articles");
          OBJ_96.setName("OBJ_96");
          xTitledPanel1ContentContainer.add(OBJ_96);
          OBJ_96.setBounds(10, 57, 112, 20);

          //---- WNBRA ----
          WNBRA.setText("@WNBRA@");
          WNBRA.setName("WNBRA");
          xTitledPanel1ContentContainer.add(WNBRA);
          WNBRA.setBounds(125, 55, 44, WNBRA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel1ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
            xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 665, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE))))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap(308, Short.MAX_VALUE)
              .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
              .addContainerGap())
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addGroup(p_contenuLayout.createParallelGroup()
                    .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE)
                    .addComponent(xTitledPanel5, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE))))
              .addGap(15, 15, 15))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel6 ========
      {
        panel6.setOpaque(false);
        panel6.setPreferredSize(new Dimension(0, 30));
        panel6.setMinimumSize(new Dimension(0, 30));
        panel6.setName("panel6");
        panel6.setLayout(null);

        //---- lib_numero ----
        lib_numero.setText("Num\u00e9ro");
        lib_numero.setName("lib_numero");
        panel6.add(lib_numero);
        lib_numero.setBounds(10, 3, 55, 20);

        //---- E1NUM ----
        E1NUM.setOpaque(false);
        E1NUM.setText("@E1NUM@");
        E1NUM.setName("E1NUM");
        panel6.add(E1NUM);
        E1NUM.setBounds(75, 1, 60, 24);

        //---- E1NFA ----
        E1NFA.setOpaque(false);
        E1NFA.setText("@E1NFA@");
        E1NFA.setName("E1NFA");
        panel6.add(E1NFA);
        E1NFA.setBounds(75, 1, 60, 24);

        //---- E1SUF ----
        E1SUF.setOpaque(false);
        E1SUF.setText("@E1SUF@");
        E1SUF.setName("E1SUF");
        panel6.add(E1SUF);
        E1SUF.setBounds(138, 1, 20, 24);

        //---- OBJ_69 ----
        OBJ_69.setText("Option de traitement");
        OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_69.setName("OBJ_69");
        panel6.add(OBJ_69);
        OBJ_69.setBounds(655, 4, 125, 19);

        //---- WREP ----
        WREP.setComponentPopupMenu(BTD);
        WREP.setName("WREP");
        panel6.add(WREP);
        WREP.setBounds(800, 0, 50, 26);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel6.getComponentCount(); i++) {
            Rectangle bounds = panel6.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel6.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel6.setMinimumSize(preferredSize);
          panel6.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel6);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField E3NOM;
  private XRiTextField E3CPL;
  private XRiTextField E3RUE;
  private XRiTextField E3LOC;
  private XRiTextField E3CDP;
  private XRiTextField E3VILR;
  private XRiTextField E3PAY;
  private JXTitledPanel xTitledPanel5;
  private XRiTextField E2NOM;
  private XRiTextField E2CPL;
  private XRiTextField E2RUE;
  private XRiTextField E2LOC;
  private XRiTextField E2VILR;
  private XRiTextField E2CDP;
  private XRiTextField E2PAY;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_30;
  private RiZoneSortie E1CLLP;
  private RiZoneSortie E1CLLS;
  private RiZoneSortie OBJ_33;
  private JLabel OBJ_73;
  private JLabel OBJ_98;
  private JLabel OBJ_77;
  private XRiTextField E1ACT;
  private JLabel OBJ_78;
  private XRiTextField E1SAN;
  private XRiTextField E1RCC;
  private XRiTextField E1NCC;
  private JLabel OBJ_74;
  private XRiTextField E1REP;
  private XRiTextField E1TRP;
  private XRiTextField E1REP2;
  private XRiTextField E1TRP2;
  private RiZoneSortie OBJ_76;
  private RiZoneSortie OBJ_75;
  private JLabel label1;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_79;
  private JLabel OBJ_80;
  private XRiTextField E1ZTR;
  private JLabel OBJ_83;
  private XRiTextField E1CTR;
  private JLabel OBJ_82;
  private XRiTextField E1MEX;
  private JLabel OBJ_81;
  private JLabel OBJ_86;
  private XRiTextField E1TP1;
  private JLabel OBJ_87;
  private XRiTextField E1TP2;
  private JLabel OBJ_88;
  private XRiTextField E1TP3;
  private JLabel OBJ_89;
  private XRiTextField E1TP4;
  private JLabel OBJ_90;
  private XRiTextField E1TP5;
  private XRiCalendrier E1DLPX;
  private XRiCalendrier E1DLSX;
  private XRiCalendrier E1DT3X;
  private XRiCalendrier E1DT1X;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_84;
  private XRiTextField E1COL;
  private JLabel OBJ_85;
  private XRiTextField E1PDS;
  private JLabel OBJ_96;
  private RiZoneSortie WNBRA;
  private JMenuBar barre_tete;
  private JPanel panel6;
  private JLabel lib_numero;
  private RiZoneSortie E1NUM;
  private RiZoneSortie E1NFA;
  private RiZoneSortie E1SUF;
  private JLabel OBJ_69;
  private XRiTextField WREP;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
