
package ri.serien.libecranrpg.vgvm.VGVM11TF;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11TF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private final static String BOUTON_ACCES_PARAMETRE = "Modifier personnalisation";
  //
  private String[] _T01_Top = { "T01", "T02", "T03", "T04", "T05", "T06", "T07", "T08", };
  private String[] _T01_Title = { "UTIT", };
  private String[][] _T01_Data = { { "L01", }, { "L02", }, { "L03", }, { "L04", }, { "L05", }, { "L06", }, { "L07", }, { "L08", }, };
  private int[] _T01_Width = { 450, };
  
  public VGVM11TF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    T01.setAspectTable(_T01_Top, _T01_Title, _T01_Data, _T01_Width, false, null, null, null, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBouton(BOUTON_ACCES_PARAMETRE, 'e', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Visibilité
    pnlUniteTransport.setVisible(!lexique.HostFieldGetData("UNTQT1").trim().isEmpty());
    
    // Code Préprarateur
    snPreparateur.setSession(getSession());
    snPreparateur.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("P11ETB")));
    snPreparateur.charger(false);
    snPreparateur.setSelectionParChampRPG(lexique, "WPRE");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Choix du transporteur adapté à la commande"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Code préparateur
    snPreparateur.renseignerChampRPG(lexique, "WPRE");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_ACCES_PARAMETRE)) {
        lexique.HostScreenSendKey(this, "F2");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void miChoixActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miAideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void T01MouseClicked(MouseEvent e) {
    try {
      if (T01.doubleClicSelection(e)) {
        T01.setValeurTop("1");
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlInfoCommande = new SNPanelTitre();
    lbDateLivraison = new SNLabelChamp();
    E1DLPX = new XRiCalendrier();
    lbQuai = new SNLabelChamp();
    WPFC = new XRiTextField();
    lbPreparateur = new SNLabelChamp();
    snPreparateur = new SNVendeur();
    pnlColis = new SNPanelTitre();
    lbVolume = new SNLabelChamp();
    E1VOL = new XRiTextField();
    lbLongueur = new SNLabelChamp();
    E1LGM = new XRiTextField();
    lbPoids = new SNLabelChamp();
    E1PDS = new XRiTextField();
    lbPlancher = new SNLabelChamp();
    E1M2P = new XRiTextField();
    pnlUniteTransport = new SNPanelTitre();
    UNTQ1 = new XRiTextField();
    UNTL1 = new XRiTextField();
    UNTQ2 = new XRiTextField();
    UNTL2 = new XRiTextField();
    UNTQ3 = new XRiTextField();
    UNTL3 = new XRiTextField();
    UNTQ4 = new XRiTextField();
    UNTL4 = new XRiTextField();
    pnlChoixtransporteur = new SNPanelTitre();
    scrollPane1 = new JScrollPane();
    T01 = new XRiTable();
    pnlScroll = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    miChoix = new JMenuItem();
    miAide = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(700, 580));
    setPreferredSize(new Dimension(700, 580));
    setMaximumSize(new Dimension(700, 580));
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);

      //======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //======== pnlInfoCommande ========
        {
          pnlInfoCommande.setTitre("Commande");
          pnlInfoCommande.setMaximumSize(new Dimension(680, 195));
          pnlInfoCommande.setName("pnlInfoCommande");
          pnlInfoCommande.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlInfoCommande.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInfoCommande.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlInfoCommande.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlInfoCommande.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbDateLivraison ----
          lbDateLivraison.setText("Livraison le ");
          lbDateLivraison.setMaximumSize(new Dimension(123, 19));
          lbDateLivraison.setName("lbDateLivraison");
          pnlInfoCommande.add(lbDateLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- E1DLPX ----
          E1DLPX.setMaximumSize(new Dimension(110, 30));
          E1DLPX.setMinimumSize(new Dimension(110, 30));
          E1DLPX.setPreferredSize(new Dimension(110, 30));
          E1DLPX.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1DLPX.setName("E1DLPX");
          pnlInfoCommande.add(E1DLPX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbQuai ----
          lbQuai.setText("Quai");
          lbQuai.setMaximumSize(new Dimension(123, 19));
          lbQuai.setName("lbQuai");
          pnlInfoCommande.add(lbQuai, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WPFC ----
          WPFC.setMaximumSize(new Dimension(50, 30));
          WPFC.setMinimumSize(new Dimension(50, 30));
          WPFC.setPreferredSize(new Dimension(50, 30));
          WPFC.setFont(new Font("sansserif", Font.PLAIN, 14));
          WPFC.setName("WPFC");
          pnlInfoCommande.add(WPFC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPreparateur ----
          lbPreparateur.setText("Pr\u00e9parateur");
          lbPreparateur.setMaximumSize(new Dimension(123, 19));
          lbPreparateur.setName("lbPreparateur");
          pnlInfoCommande.add(lbPreparateur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snPreparateur ----
          snPreparateur.setName("snPreparateur");
          pnlInfoCommande.add(snPreparateur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlInfoCommande, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlColis ========
        {
          pnlColis.setTitre("Colis");
          pnlColis.setName("pnlColis");
          pnlColis.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlColis.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlColis.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlColis.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlColis.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbVolume ----
          lbVolume.setText("Volume");
          lbVolume.setName("lbVolume");
          pnlColis.add(lbVolume, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- E1VOL ----
          E1VOL.setMaximumSize(new Dimension(80, 30));
          E1VOL.setMinimumSize(new Dimension(80, 30));
          E1VOL.setPreferredSize(new Dimension(80, 30));
          E1VOL.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1VOL.setName("E1VOL");
          pnlColis.add(E1VOL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbLongueur ----
          lbLongueur.setText("Longueur maximum");
          lbLongueur.setName("lbLongueur");
          pnlColis.add(lbLongueur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- E1LGM ----
          E1LGM.setMaximumSize(new Dimension(50, 30));
          E1LGM.setMinimumSize(new Dimension(50, 30));
          E1LGM.setPreferredSize(new Dimension(50, 30));
          E1LGM.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1LGM.setName("E1LGM");
          pnlColis.add(E1LGM, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbPoids ----
          lbPoids.setText("Poids");
          lbPoids.setMaximumSize(new Dimension(123, 19));
          lbPoids.setName("lbPoids");
          pnlColis.add(lbPoids, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- E1PDS ----
          E1PDS.setMaximumSize(new Dimension(80, 30));
          E1PDS.setMinimumSize(new Dimension(80, 30));
          E1PDS.setPreferredSize(new Dimension(80, 30));
          E1PDS.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1PDS.setName("E1PDS");
          pnlColis.add(E1PDS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- lbPlancher ----
          lbPlancher.setText("M\u00b2 au plancher");
          lbPlancher.setMaximumSize(new Dimension(123, 19));
          lbPlancher.setName("lbPlancher");
          pnlColis.add(lbPlancher, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- E1M2P ----
          E1M2P.setMaximumSize(new Dimension(50, 30));
          E1M2P.setMinimumSize(new Dimension(50, 30));
          E1M2P.setPreferredSize(new Dimension(50, 30));
          E1M2P.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1M2P.setName("E1M2P");
          pnlColis.add(E1M2P, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlColis, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlUniteTransport ========
        {
          pnlUniteTransport.setTitre("Unit\u00e9s de transport");
          pnlUniteTransport.setMaximumSize(new Dimension(550, 110));
          pnlUniteTransport.setMinimumSize(new Dimension(550, 110));
          pnlUniteTransport.setPreferredSize(new Dimension(550, 110));
          pnlUniteTransport.setName("pnlUniteTransport");
          pnlUniteTransport.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlUniteTransport.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlUniteTransport.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlUniteTransport.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlUniteTransport.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- UNTQ1 ----
          UNTQ1.setMaximumSize(new Dimension(40, 30));
          UNTQ1.setMinimumSize(new Dimension(40, 30));
          UNTQ1.setPreferredSize(new Dimension(40, 30));
          UNTQ1.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTQ1.setName("UNTQ1");
          pnlUniteTransport.add(UNTQ1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- UNTL1 ----
          UNTL1.setMaximumSize(new Dimension(214, 29));
          UNTL1.setMinimumSize(new Dimension(215, 30));
          UNTL1.setPreferredSize(new Dimension(215, 30));
          UNTL1.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTL1.setName("UNTL1");
          pnlUniteTransport.add(UNTL1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- UNTQ2 ----
          UNTQ2.setMaximumSize(new Dimension(40, 30));
          UNTQ2.setMinimumSize(new Dimension(40, 30));
          UNTQ2.setPreferredSize(new Dimension(40, 30));
          UNTQ2.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTQ2.setName("UNTQ2");
          pnlUniteTransport.add(UNTQ2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- UNTL2 ----
          UNTL2.setMaximumSize(new Dimension(214, 29));
          UNTL2.setMinimumSize(new Dimension(215, 30));
          UNTL2.setPreferredSize(new Dimension(215, 30));
          UNTL2.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTL2.setName("UNTL2");
          pnlUniteTransport.add(UNTL2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- UNTQ3 ----
          UNTQ3.setMaximumSize(new Dimension(40, 30));
          UNTQ3.setMinimumSize(new Dimension(40, 30));
          UNTQ3.setPreferredSize(new Dimension(40, 30));
          UNTQ3.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTQ3.setName("UNTQ3");
          pnlUniteTransport.add(UNTQ3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- UNTL3 ----
          UNTL3.setMaximumSize(new Dimension(214, 29));
          UNTL3.setMinimumSize(new Dimension(215, 30));
          UNTL3.setPreferredSize(new Dimension(215, 30));
          UNTL3.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTL3.setName("UNTL3");
          pnlUniteTransport.add(UNTL3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- UNTQ4 ----
          UNTQ4.setMaximumSize(new Dimension(40, 30));
          UNTQ4.setMinimumSize(new Dimension(40, 30));
          UNTQ4.setPreferredSize(new Dimension(40, 30));
          UNTQ4.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTQ4.setName("UNTQ4");
          pnlUniteTransport.add(UNTQ4, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- UNTL4 ----
          UNTL4.setMaximumSize(new Dimension(214, 29));
          UNTL4.setMinimumSize(new Dimension(215, 30));
          UNTL4.setPreferredSize(new Dimension(215, 30));
          UNTL4.setFont(new Font("sansserif", Font.PLAIN, 14));
          UNTL4.setName("UNTL4");
          pnlUniteTransport.add(UNTL4, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlUniteTransport, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlChoixtransporteur ========
        {
          pnlChoixtransporteur.setTitre(" Choix du transporteur");
          pnlChoixtransporteur.setMaximumSize(new Dimension(510, 260));
          pnlChoixtransporteur.setDoubleBuffered(false);
          pnlChoixtransporteur.setFocusable(false);
          pnlChoixtransporteur.setName("pnlChoixtransporteur");
          pnlChoixtransporteur.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlChoixtransporteur.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlChoixtransporteur.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlChoixtransporteur.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlChoixtransporteur.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //======== scrollPane1 ========
          {
            scrollPane1.setMinimumSize(new Dimension(400, 165));
            scrollPane1.setMaximumSize(new Dimension(400, 165));
            scrollPane1.setPreferredSize(new Dimension(400, 165));
            scrollPane1.setName("scrollPane1");

            //---- T01 ----
            T01.setMaximumSize(new Dimension(400, 160));
            T01.setMinimumSize(new Dimension(400, 130));
            T01.setPreferredSize(new Dimension(400, 130));
            T01.setPreferredScrollableViewportSize(null);
            T01.setName("T01");
            T01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                T01MouseClicked(e);
              }
            });
            scrollPane1.setViewportView(T01);
          }
          pnlChoixtransporteur.add(scrollPane1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //======== pnlScroll ========
          {
            pnlScroll.setName("pnlScroll");
            pnlScroll.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlScroll.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlScroll.getLayout()).rowHeights = new int[] {0, 0, 0};
            ((GridBagLayout)pnlScroll.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
            ((GridBagLayout)pnlScroll.getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0E-4};

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMaximumSize(new Dimension(25, 50));
            BT_PGUP.setMinimumSize(new Dimension(25, 60));
            BT_PGUP.setPreferredSize(new Dimension(25, 60));
            BT_PGUP.setName("BT_PGUP");
            pnlScroll.add(BT_PGUP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMaximumSize(new Dimension(25, 50));
            BT_PGDOWN.setMinimumSize(new Dimension(25, 60));
            BT_PGDOWN.setPreferredSize(new Dimension(25, 60));
            BT_PGDOWN.setName("BT_PGDOWN");
            pnlScroll.add(BT_PGDOWN, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlChoixtransporteur.add(pnlScroll, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlChoixtransporteur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- miChoix ----
      miChoix.setText("Choix possibles");
      miChoix.setName("miChoix");
      miChoix.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixActionPerformed(e);
        }
      });
      BTD.add(miChoix);

      //---- miAide ----
      miAide.setText("Aide en ligne");
      miAide.setName("miAide");
      miAide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideActionPerformed(e);
        }
      });
      BTD.add(miAide);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlInfoCommande;
  private SNLabelChamp lbDateLivraison;
  private XRiCalendrier E1DLPX;
  private SNLabelChamp lbQuai;
  private XRiTextField WPFC;
  private SNLabelChamp lbPreparateur;
  private SNVendeur snPreparateur;
  private SNPanelTitre pnlColis;
  private SNLabelChamp lbVolume;
  private XRiTextField E1VOL;
  private SNLabelChamp lbLongueur;
  private XRiTextField E1LGM;
  private SNLabelChamp lbPoids;
  private XRiTextField E1PDS;
  private SNLabelChamp lbPlancher;
  private XRiTextField E1M2P;
  private SNPanelTitre pnlUniteTransport;
  private XRiTextField UNTQ1;
  private XRiTextField UNTL1;
  private XRiTextField UNTQ2;
  private XRiTextField UNTL2;
  private XRiTextField UNTQ3;
  private XRiTextField UNTL3;
  private XRiTextField UNTQ4;
  private XRiTextField UNTL4;
  private SNPanelTitre pnlChoixtransporteur;
  private JScrollPane scrollPane1;
  private XRiTable T01;
  private SNPanel pnlScroll;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem miChoix;
  private JMenuItem miAide;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
