
package ri.serien.libecranrpg.vgvm.VGVM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM71FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] ETADAT_Value = { "", "C", "H", "R", "E", "P", "S", "F", };
  private String[] _WY501_Title = { "Type", "Date", };
  private String[][] _WY501_Data =
      { { "WY501", "WP501", }, { "WY502", "WP502", }, { "WY503", "WP503", }, { "WY504", "WP504", }, { "WY505", "WP505", },
          { "WY506", "WP506", }, { "WY507", "WP507", }, { "WY508", "WP508", }, { "WY509", "WP509", }, { "WY510", "WP510", },
          { "WY511", "WP511", }, { "WY512", "WP512", }, { "WY513", "WP513", }, { "WY514", "WP514", }, { "WY515", "WP515", }, };
  private int[] _WY501_Width = { 37, 59, };
  private String[] _WY201_Title = { "Type", "Date", };
  private String[][] _WY201_Data =
      { { "WY201", "WP201", }, { "WY202", "WP202", }, { "WY203", "WP203", }, { "WY204", "WP204", }, { "WY205", "WP205", },
          { "WY206", "WP206", }, { "WY207", "WP207", }, { "WY208", "WP208", }, { "WY209", "WP209", }, { "WY210", "WP210", },
          { "WY211", "WP211", }, { "WY212", "WP212", }, { "WY213", "WP213", }, { "WY214", "WP214", }, { "WY215", "WP215", }, };
  private int[] _WY201_Width = { 30, 63, };
  private String[] _WY401_Title = { "Type", "Date", };
  private String[][] _WY401_Data =
      { { "WY401", "WP401", }, { "WY402", "WP402", }, { "WY403", "WP403", }, { "WY404", "WP404", }, { "WY405", "WP405", },
          { "WY406", "WP406", }, { "WY407", "WP407", }, { "WY408", "WP408", }, { "WY409", "WP409", }, { "WY410", "WP410", },
          { "WY411", "WP411", }, { "WY412", "WP412", }, { "WY413", "WP413", }, { "WY414", "WP414", }, { "WY415", "WP415", }, };
  private int[] _WY401_Width = { 30, 63, };
  private String[] _WY301_Title = { "Type", "Date", };
  private String[][] _WY301_Data =
      { { "WY301", "WP301", }, { "WY302", "WP302", }, { "WY303", "WP303", }, { "WY304", "WP304", }, { "WY305", "WP305", },
          { "WY306", "WP306", }, { "WY307", "WP307", }, { "WY308", "WP308", }, { "WY309", "WP309", }, { "WY310", "WP310", },
          { "WY311", "WP311", }, { "WY312", "WP312", }, { "WY313", "WP313", }, { "WY314", "WP314", }, { "WY315", "WP315", }, };
  private int[] _WY301_Width = { 30, 63, };
  private String[] _WT101_Top = { "WT101", "WT102", "WT103", "WT104", "WT105", "WT106", "WT107", "WT108", "WT109", "WT110", "WT111",
      "WT112", "WT113", "WT114", "WT115", };
  private String[] _WT101_Title = { "Mois/an", "Montant", "E", };
  private String[][] _WT101_Data = { { "WP101", "WM101", "WEB01", }, { "WP102", "WM102", "WEB02", }, { "WP103", "WM103", "WEB03", },
      { "WP104", "WM104", "WEB04", }, { "WP105", "WM105", "WEB05", }, { "WP106", "WM106", "WEB06", }, { "WP107", "WM107", "WEB07", },
      { "WP108", "WM108", "WEB08", }, { "WP109", "WM109", "WEB09", }, { "WP110", "WM110", "WEB10", }, { "WP111", "WM111", "WEB11", },
      { "WP112", "WM112", "WEB12", }, { "WP113", "WM113", "WEB13", }, { "WP114", "WM114", "WEB14", }, { "WP115", "WM115", "WEB15", }, };
  private int[] _WT101_Width = { 52, 63, 15, };
  // private String[] _WY201_Top = {"WT201", "WT202", "WT203", "WT204", "WT205", "WT206", "WT207", "WT208", "WT209",
  // "WT210", "WT211", "WT212", "WT213", "WT214", "WT215"};
  private String[] _WY301_Top = { "WT301", "WT302", "WT303", "WT304", "WT305", "WT306", "WT307", "WT308", "WT309", "WT310", "WT311",
      "WT312", "WT313", "WT314", "WT315" };
  // private String[] _WY401_Top = {"WT401", "WT402", "WT403", "WT404", "WT405", "WT406", "WT407", "WT408", "WT409",
  // "WT410", "WT411", "WT412", "WT413", "WT414", "WT415"};
  // private String[] _WY501_Top = {"WT501", "WT502", "WT503", "WT504", "WT505", "WT506", "WT507", "WT508", "WT509",
  // "WT510", "WT511", "WT512", "WT513", "WT514", "WT515"};
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.LEFT };
  private int[] _LIST_Justification2 = { SwingConstants.LEFT, SwingConstants.RIGHT };
  private int choixLIST = -1;
  private int choixLIST2 = -1;
  private int choixLIST3 = -1;
  private int choixLIST4 = -1;
  private int choixLIST5 = -1;
  
  public VGVM71FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ETADAT.setValeurs(ETADAT_Value, null);
    WFAC.setValeursSelection("F", " ");
    WRES.setValeursSelection("R", " ");
    WCPT.setValeursSelection("C", " ");
    WEXP.setValeursSelection("E", " ");
    WHOM.setValeursSelection("H", " ");
    WT101.setAspectTable(_WT101_Top, _WT101_Title, _WT101_Data, _WT101_Width, false, _LIST_Justification, null, null, null);
    WY501.setAspectTable(null, _WY501_Title, _WY501_Data, _WY501_Width, false, _LIST_Justification2, null, null, null);
    WY201.setAspectTable(null, _WY201_Title, _WY201_Data, _WY201_Width, false, _LIST_Justification2, null, null, null);
    WY401.setAspectTable(null, _WY401_Title, _WY401_Data, _WY401_Width, false, _LIST_Justification2, null, null, null);
    WY301.setAspectTable(_WY301_Top, _WY301_Title, _WY301_Data, _WY301_Width, false, _LIST_Justification2, null, null, null);
    WDEV.setValeurs("D", WDEV_GRP);
    WDEV_BONS.setValeurs("B");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_50_OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOMR@ - @CLVILR@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    // OBJ_104.setIcon(lexique.getImage("images/"retour@&langue@"", true));
    up.setIcon(lexique.chargerImage("images/pgup20.png", true));
    down.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    up2.setIcon(lexique.chargerImage("images/pgup20.png", true));
    down2.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    up3.setIcon(lexique.chargerImage("images/pgup20.png", true));
    down3.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    up4.setIcon(lexique.chargerImage("images/pgup20.png", true));
    down4.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    up5.setIcon(lexique.chargerImage("images/pgup20.png", true));
    down5.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private int majLIST(XRiTable liste, int old_choix) {
    int new_choix = liste.getSelectedRow();
    
    if (old_choix == new_choix) {
      new_choix = -1;
      liste.clearSelection();
    }
    
    return new_choix;
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    if (BTD.getInvoker().getName().equals("WT101")) {
      // lexique.validSelection(LIST, _WT101_Top, "1", "Enter");
      WT101.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
    else if (BTD.getInvoker().getName().equals("WY201")) {
      // lexique.validSelection(LIST2, _WT101_Top2, "1", "Enter");
      WY201.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
    else if (BTD.getInvoker().getName().equals("WY301")) {
      // lexique.validSelection(LIST3, _WT101_Top3, "1", "Enter");
      WY301.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
    else if (BTD.getInvoker().getName().equals("WY401")) {
      // lexique.validSelection(LIST4, _WT101_Top4, "1", "Enter");
      WY401.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
    else if (BTD.getInvoker().getName().equals("WY501")) {
      // lexique.validSelection(LIST5, _WT101_Top5, "1", "Enter");
      WY501.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    if (WDEV_BONS.isSelected()) {
      p_bons.setVisible(true);
    }
    else {
      p_bons.setVisible(false);
      WHOM.setSelected(false);
      WRES.setSelected(false);
      WEXP.setSelected(false);
      WFAC.setSelected(false);
      WCPT.setSelected(false);
    }
  }
  
  private void WT101MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WT101_Top, "1", "ENTER", e);
    if (WT101.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    choixLIST = majLIST(WT101, choixLIST);
  }
  
  private void upActionPerformed(ActionEvent e) {
    // paginerMultiList("UP",7,2);
    lexique.HostCursorPut(7, 2);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void downActionPerformed(ActionEvent e) {
    // paginerMultiList("DOWN",7,2);
    lexique.HostCursorPut(7, 2);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void WY201MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST2, _WT101_Top2, "2", "ENTER", e);
    if (WY201.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    choixLIST2 = majLIST(WY201, choixLIST2);
  }
  
  private void up2ActionPerformed(ActionEvent e) {
    // paginerMultiList("UP",7,21);
    lexique.HostCursorPut(7, 21);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void down2ActionPerformed(ActionEvent e) {
    // paginerMultiList("DOWN",7,21);
    lexique.HostCursorPut(7, 21);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void WY301MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST3, _WT101_Top3, "3", "ENTER", e);
    if (WY301.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    choixLIST3 = majLIST(WY301, choixLIST3);
  }
  
  private void up3ActionPerformed(ActionEvent e) {
    // paginerMultiList("UP",7,36);
    lexique.HostCursorPut(7, 36);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void down3ActionPerformed(ActionEvent e) {
    // paginerMultiList("DOWN",7,36);
    lexique.HostCursorPut(7, 36);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void WY401MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST4, _WT101_Top4, "4", "ENTER", e);
    if (WY401.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    choixLIST4 = majLIST(WY401, choixLIST4);
  }
  
  private void up4ActionPerformed(ActionEvent e) {
    // paginerMultiList("UP",7,51);
    lexique.HostCursorPut(7, 51);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void down4ActionPerformed(ActionEvent e) {
    // paginerMultiList("DOWN",7,51);
    lexique.HostCursorPut(7, 51);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void WY501MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST5, _WT101_Top5, "5", "ENTER", e);
    if (WY501.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    choixLIST5 = majLIST(WY501, choixLIST5);
  }
  
  private void up5ActionPerformed(ActionEvent e) {
    // paginerMultiList("UP",7,65);
    lexique.HostCursorPut(7, 65);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void down5ActionPerformed(ActionEvent e) {
    // paginerMultiList("DOWN",7,65);
    lexique.HostCursorPut(7, 65);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44_OBJ_44 = new JLabel();
    WETB = new XRiTextField();
    OBJ_45_OBJ_45 = new JLabel();
    WCLI = new XRiTextField();
    OBJ_46_OBJ_46 = new JLabel();
    WLIV = new XRiTextField();
    OBJ_50_OBJ_50 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WCAT = new XRiTextField();
    CLDVEX = new XRiCalendrier();
    panel2 = new JPanel();
    WDEV = new XRiRadioButton();
    WDEV_BONS = new XRiRadioButton();
    p_bons = new JPanel();
    WHOM = new XRiCheckBox();
    WEXP = new XRiCheckBox();
    WCPT = new XRiCheckBox();
    WRES = new XRiCheckBox();
    WFAC = new XRiCheckBox();
    ETADAT = new XRiComboBox();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_59_OBJ_59 = new JLabel();
    OBJ_53_OBJ_53 = new JLabel();
    OBJ_66_OBJ_66 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    WPERD = new XRiCalendrier();
    WPERF = new XRiCalendrier();
    label1 = new RiZoneSortie();
    WREP1 = new XRiTextField();
    WREP2 = new XRiTextField();
    panel3 = new JPanel();
    SCROLLPANE_OBJ_22 = new JScrollPane();
    WT101 = new XRiTable();
    up = new JButton();
    down = new JButton();
    panel4 = new JPanel();
    SCROLLPANE_OBJ_84 = new JScrollPane();
    WY201 = new XRiTable();
    up2 = new JButton();
    down2 = new JButton();
    panel5 = new JPanel();
    SCROLLPANE_OBJ_87 = new JScrollPane();
    WY301 = new XRiTable();
    up3 = new JButton();
    down3 = new JButton();
    panel6 = new JPanel();
    SCROLLPANE_OBJ_90 = new JScrollPane();
    WY401 = new XRiTable();
    up4 = new JButton();
    down4 = new JButton();
    panel7 = new JPanel();
    SCROLLPANE_OBJ_93 = new JScrollPane();
    WY501 = new XRiTable();
    up5 = new JButton();
    down5 = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    WDEV_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("TABLEAU DE SYNTHESE");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 26));
          p_tete_gauche.setMinimumSize(new Dimension(700, 26));
          p_tete_gauche.setAlignmentY(2.5F);
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_44_OBJ_44 ----
          OBJ_44_OBJ_44.setText("Etablissement");
          OBJ_44_OBJ_44.setName("OBJ_44_OBJ_44");
          p_tete_gauche.add(OBJ_44_OBJ_44);
          OBJ_44_OBJ_44.setBounds(5, 5, 93, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(95, 1, 40, WETB.getPreferredSize().height);

          //---- OBJ_45_OBJ_45 ----
          OBJ_45_OBJ_45.setText("Client");
          OBJ_45_OBJ_45.setName("OBJ_45_OBJ_45");
          p_tete_gauche.add(OBJ_45_OBJ_45);
          OBJ_45_OBJ_45.setBounds(155, 5, 42, 20);

          //---- WCLI ----
          WCLI.setComponentPopupMenu(BTD);
          WCLI.setName("WCLI");
          p_tete_gauche.add(WCLI);
          WCLI.setBounds(195, 1, 60, WCLI.getPreferredSize().height);

          //---- OBJ_46_OBJ_46 ----
          OBJ_46_OBJ_46.setText("Livr\u00e9");
          OBJ_46_OBJ_46.setName("OBJ_46_OBJ_46");
          p_tete_gauche.add(OBJ_46_OBJ_46);
          OBJ_46_OBJ_46.setBounds(280, 5, 30, 20);

          //---- WLIV ----
          WLIV.setComponentPopupMenu(BTD);
          WLIV.setName("WLIV");
          p_tete_gauche.add(WLIV);
          WLIV.setBounds(315, 1, 36, WLIV.getPreferredSize().height);

          //---- OBJ_50_OBJ_50 ----
          OBJ_50_OBJ_50.setText("@CLNOMR@ - @CLVILR@");
          OBJ_50_OBJ_50.setOpaque(false);
          OBJ_50_OBJ_50.setName("OBJ_50_OBJ_50");
          p_tete_gauche.add(OBJ_50_OBJ_50);
          OBJ_50_OBJ_50.setBounds(370, 3, 480, OBJ_50_OBJ_50.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Recherche de bons");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Actions commerciales");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Interventions/R\u00e9parations");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");

            //---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("M\u00e9mos");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(999, 470));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WCAT ----
            WCAT.setName("WCAT");
            panel1.add(WCAT);
            WCAT.setBounds(120, 46, 40, WCAT.getPreferredSize().height);

            //---- CLDVEX ----
            CLDVEX.setName("CLDVEX");
            panel1.add(CLDVEX);
            CLDVEX.setBounds(120, 76, 105, CLDVEX.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- WDEV ----
              WDEV.setText("Devis");
              WDEV.setComponentPopupMenu(BTD);
              WDEV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WDEV.setName("WDEV");
              panel2.add(WDEV);
              WDEV.setBounds(15, 14, 60, 20);

              //---- WDEV_BONS ----
              WDEV_BONS.setText("Bons");
              WDEV_BONS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              WDEV_BONS.setName("WDEV_BONS");
              WDEV_BONS.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_64ActionPerformed(e);
                }
              });
              panel2.add(WDEV_BONS);
              WDEV_BONS.setBounds(15, 44, 56, 20);

              //======== p_bons ========
              {
                p_bons.setOpaque(false);
                p_bons.setName("p_bons");

                //---- WHOM ----
                WHOM.setText("Valid\u00e9");
                WHOM.setComponentPopupMenu(BTD);
                WHOM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WHOM.setName("WHOM");

                //---- WEXP ----
                WEXP.setText("Exp\u00e9di\u00e9");
                WEXP.setComponentPopupMenu(BTD);
                WEXP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WEXP.setName("WEXP");

                //---- WCPT ----
                WCPT.setText("Comptabilis\u00e9");
                WCPT.setComponentPopupMenu(BTD);
                WCPT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WCPT.setName("WCPT");

                //---- WRES ----
                WRES.setText("R\u00e9serv\u00e9");
                WRES.setComponentPopupMenu(BTD);
                WRES.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WRES.setName("WRES");

                //---- WFAC ----
                WFAC.setText("Factur\u00e9");
                WFAC.setComponentPopupMenu(BTD);
                WFAC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WFAC.setName("WFAC");

                GroupLayout p_bonsLayout = new GroupLayout(p_bons);
                p_bons.setLayout(p_bonsLayout);
                p_bonsLayout.setHorizontalGroup(
                  p_bonsLayout.createParallelGroup()
                    .addGroup(p_bonsLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(p_bonsLayout.createParallelGroup()
                        .addGroup(p_bonsLayout.createSequentialGroup()
                          .addComponent(WHOM, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                          .addGap(4, 4, 4)
                          .addComponent(WRES, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                        .addGroup(p_bonsLayout.createSequentialGroup()
                          .addComponent(WEXP, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
                          .addGap(4, 4, 4)
                          .addComponent(WFAC, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                        .addComponent(WCPT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                );
                p_bonsLayout.setVerticalGroup(
                  p_bonsLayout.createParallelGroup()
                    .addGroup(p_bonsLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addGroup(p_bonsLayout.createParallelGroup()
                        .addComponent(WHOM, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WRES, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(p_bonsLayout.createParallelGroup()
                        .addComponent(WEXP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WFAC, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addComponent(WCPT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                );
              }
              panel2.add(p_bons);
              p_bons.setBounds(80, 39, 190, 90);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(425, 14, 275, 135);

            //---- ETADAT ----
            ETADAT.setModel(new DefaultComboBoxModel(new String[] {
              " ",
              "Cr\u00e9ation",
              "Valid\u00e9",
              "R\u00e9serv\u00e9",
              "Exp\u00e9di\u00e9",
              "Date de livraison pr\u00e9vue",
              "Date de livraison souhait\u00e9e",
              "Factur\u00e9"
            }));
            ETADAT.setComponentPopupMenu(BTD);
            ETADAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ETADAT.setName("ETADAT");
            panel1.add(ETADAT);
            ETADAT.setBounds(230, 77, 161, ETADAT.getPreferredSize().height);

            //---- OBJ_57_OBJ_57 ----
            OBJ_57_OBJ_57.setText("Repr\u00e9sentant 1");
            OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
            panel1.add(OBJ_57_OBJ_57);
            OBJ_57_OBJ_57.setBounds(25, 20, 100, 20);

            //---- OBJ_61_OBJ_61 ----
            OBJ_61_OBJ_61.setText("Repr\u00e9sentant 2");
            OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
            panel1.add(OBJ_61_OBJ_61);
            OBJ_61_OBJ_61.setBounds(230, 20, 110, 20);

            //---- OBJ_59_OBJ_59 ----
            OBJ_59_OBJ_59.setText("Derni\u00e8re vente");
            OBJ_59_OBJ_59.setName("OBJ_59_OBJ_59");
            panel1.add(OBJ_59_OBJ_59);
            OBJ_59_OBJ_59.setBounds(25, 80, 100, 20);

            //---- OBJ_53_OBJ_53 ----
            OBJ_53_OBJ_53.setText("Cat\u00e9gorie");
            OBJ_53_OBJ_53.setName("OBJ_53_OBJ_53");
            panel1.add(OBJ_53_OBJ_53);
            OBJ_53_OBJ_53.setBounds(25, 50, 95, 20);

            //---- OBJ_66_OBJ_66 ----
            OBJ_66_OBJ_66.setText("Dates");
            OBJ_66_OBJ_66.setName("OBJ_66_OBJ_66");
            panel1.add(OBJ_66_OBJ_66);
            OBJ_66_OBJ_66.setBounds(25, 110, 90, 20);

            //---- OBJ_68_OBJ_68 ----
            OBJ_68_OBJ_68.setText("\u00e0");
            OBJ_68_OBJ_68.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
            panel1.add(OBJ_68_OBJ_68);
            OBJ_68_OBJ_68.setBounds(250, 110, 25, 20);

            //---- WPERD ----
            WPERD.setName("WPERD");
            panel1.add(WPERD);
            WPERD.setBounds(120, 106, 105, WPERD.getPreferredSize().height);

            //---- WPERF ----
            WPERF.setName("WPERF");
            panel1.add(WPERF);
            WPERF.setBounds(286, 106, 105, WPERF.getPreferredSize().height);

            //---- label1 ----
            label1.setText("@CCLIBR@");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(165, 48, 224, label1.getPreferredSize().height);

            //---- WREP1 ----
            WREP1.setName("WREP1");
            panel1.add(WREP1);
            WREP1.setBounds(120, 16, 34, WREP1.getPreferredSize().height);

            //---- WREP2 ----
            WREP2.setName("WREP2");
            panel1.add(WREP2);
            WREP2.setBounds(355, 16, 34, WREP2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Facturation"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //======== SCROLLPANE_OBJ_22 ========
            {
              SCROLLPANE_OBJ_22.setComponentPopupMenu(BTD);
              SCROLLPANE_OBJ_22.setName("SCROLLPANE_OBJ_22");

              //---- WT101 ----
              WT101.setComponentPopupMenu(BTD);
              WT101.setName("WT101");
              WT101.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WT101MouseClicked(e);
                }
              });
              SCROLLPANE_OBJ_22.setViewportView(WT101);
            }
            panel3.add(SCROLLPANE_OBJ_22);
            SCROLLPANE_OBJ_22.setBounds(15, 32, 170, 273);

            //---- up ----
            up.setText("");
            up.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            up.setName("up");
            up.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                upActionPerformed(e);
              }
            });
            panel3.add(up);
            up.setBounds(190, 30, 20, 120);

            //---- down ----
            down.setText("");
            down.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            down.setName("down");
            down.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                downActionPerformed(e);
              }
            });
            panel3.add(down);
            down.setBounds(190, 180, 20, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Actions commerciales"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== SCROLLPANE_OBJ_84 ========
            {
              SCROLLPANE_OBJ_84.setComponentPopupMenu(BTD);
              SCROLLPANE_OBJ_84.setName("SCROLLPANE_OBJ_84");

              //---- WY201 ----
              WY201.setFont(new Font("Courier New", Font.PLAIN, 12));
              WY201.setComponentPopupMenu(BTD);
              WY201.setName("WY201");
              WY201.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WY201MouseClicked(e);
                }
              });
              SCROLLPANE_OBJ_84.setViewportView(WY201);
            }
            panel4.add(SCROLLPANE_OBJ_84);
            SCROLLPANE_OBJ_84.setBounds(15, 32, 125, 273);

            //---- up2 ----
            up2.setText("");
            up2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            up2.setName("up2");
            up2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                up2ActionPerformed(e);
              }
            });
            panel4.add(up2);
            up2.setBounds(145, 30, 20, 120);

            //---- down2 ----
            down2.setText("");
            down2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            down2.setName("down2");
            down2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                down2ActionPerformed(e);
              }
            });
            panel4.add(down2);
            down2.setBounds(145, 180, 20, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder("M\u00e9mos"));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //======== SCROLLPANE_OBJ_87 ========
            {
              SCROLLPANE_OBJ_87.setComponentPopupMenu(BTD);
              SCROLLPANE_OBJ_87.setName("SCROLLPANE_OBJ_87");

              //---- WY301 ----
              WY301.setComponentPopupMenu(BTD);
              WY301.setName("WY301");
              WY301.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WY301MouseClicked(e);
                }
              });
              SCROLLPANE_OBJ_87.setViewportView(WY301);
            }
            panel5.add(SCROLLPANE_OBJ_87);
            SCROLLPANE_OBJ_87.setBounds(15, 32, 125, 273);

            //---- up3 ----
            up3.setText("");
            up3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            up3.setName("up3");
            up3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                up3ActionPerformed(e);
              }
            });
            panel5.add(up3);
            up3.setBounds(145, 30, 20, 120);

            //---- down3 ----
            down3.setText("");
            down3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            down3.setName("down3");
            down3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                down3ActionPerformed(e);
              }
            });
            panel5.add(down3);
            down3.setBounds(145, 180, 20, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder("Appels t\u00e9l\u00e9phoniques"));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //======== SCROLLPANE_OBJ_90 ========
            {
              SCROLLPANE_OBJ_90.setComponentPopupMenu(BTD);
              SCROLLPANE_OBJ_90.setName("SCROLLPANE_OBJ_90");

              //---- WY401 ----
              WY401.setComponentPopupMenu(BTD);
              WY401.setName("WY401");
              WY401.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WY401MouseClicked(e);
                }
              });
              SCROLLPANE_OBJ_90.setViewportView(WY401);
            }
            panel6.add(SCROLLPANE_OBJ_90);
            SCROLLPANE_OBJ_90.setBounds(15, 32, 125, 273);

            //---- up4 ----
            up4.setText("");
            up4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            up4.setName("up4");
            up4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                up4ActionPerformed(e);
              }
            });
            panel6.add(up4);
            up4.setBounds(145, 30, 20, 120);

            //---- down4 ----
            down4.setText("");
            down4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            down4.setName("down4");
            down4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                down4ActionPerformed(e);
              }
            });
            panel6.add(down4);
            down4.setBounds(145, 180, 20, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }

          //======== panel7 ========
          {
            panel7.setBorder(new TitledBorder("Intervention/r\u00e9partition"));
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //======== SCROLLPANE_OBJ_93 ========
            {
              SCROLLPANE_OBJ_93.setComponentPopupMenu(BTD);
              SCROLLPANE_OBJ_93.setName("SCROLLPANE_OBJ_93");

              //---- WY501 ----
              WY501.setComponentPopupMenu(BTD);
              WY501.setPreferredScrollableViewportSize(new Dimension(450, 393));
              WY501.setName("WY501");
              WY501.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WY501MouseClicked(e);
                }
              });
              SCROLLPANE_OBJ_93.setViewportView(WY501);
            }
            panel7.add(SCROLLPANE_OBJ_93);
            SCROLLPANE_OBJ_93.setBounds(15, 32, 130, 273);

            //---- up5 ----
            up5.setText("");
            up5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            up5.setName("up5");
            up5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                up5ActionPerformed(e);
              }
            });
            panel7.add(up5);
            up5.setBounds(150, 32, 20, 120);

            //---- down5 ----
            down5.setText("");
            down5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            down5.setName("down5");
            down5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                down5ActionPerformed(e);
              }
            });
            panel7.add(down5);
            down5.setBounds(150, 180, 20, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(412, 412, 412)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(597, 597, 597)
                    .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(232, 232, 232)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE))
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(159, 159, 159)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE)))))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(179, 179, 179)
                .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 330, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }

    //---- WDEV_GRP ----
    WDEV_GRP.add(WDEV);
    WDEV_GRP.add(WDEV_BONS);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44_OBJ_44;
  private XRiTextField WETB;
  private JLabel OBJ_45_OBJ_45;
  private XRiTextField WCLI;
  private JLabel OBJ_46_OBJ_46;
  private XRiTextField WLIV;
  private RiZoneSortie OBJ_50_OBJ_50;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WCAT;
  private XRiCalendrier CLDVEX;
  private JPanel panel2;
  private XRiRadioButton WDEV;
  private XRiRadioButton WDEV_BONS;
  private JPanel p_bons;
  private XRiCheckBox WHOM;
  private XRiCheckBox WEXP;
  private XRiCheckBox WCPT;
  private XRiCheckBox WRES;
  private XRiCheckBox WFAC;
  private XRiComboBox ETADAT;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_59_OBJ_59;
  private JLabel OBJ_53_OBJ_53;
  private JLabel OBJ_66_OBJ_66;
  private JLabel OBJ_68_OBJ_68;
  private XRiCalendrier WPERD;
  private XRiCalendrier WPERF;
  private RiZoneSortie label1;
  private XRiTextField WREP1;
  private XRiTextField WREP2;
  private JPanel panel3;
  private JScrollPane SCROLLPANE_OBJ_22;
  private XRiTable WT101;
  private JButton up;
  private JButton down;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_OBJ_84;
  private XRiTable WY201;
  private JButton up2;
  private JButton down2;
  private JPanel panel5;
  private JScrollPane SCROLLPANE_OBJ_87;
  private XRiTable WY301;
  private JButton up3;
  private JButton down3;
  private JPanel panel6;
  private JScrollPane SCROLLPANE_OBJ_90;
  private XRiTable WY401;
  private JButton up4;
  private JButton down4;
  private JPanel panel7;
  private JScrollPane SCROLLPANE_OBJ_93;
  private XRiTable WY501;
  private JButton up5;
  private JButton down5;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private ButtonGroup WDEV_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
