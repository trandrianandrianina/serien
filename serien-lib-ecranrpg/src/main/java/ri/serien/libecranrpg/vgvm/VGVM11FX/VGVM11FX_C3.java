
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.EnumTypePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.DialogueDetailPrixVente;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.Calculatrice;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_C3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] L1IN18_Value = { "1", "0", "2", "3" };
  private String[] UTAR_Value = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  private String[] L1IN20_Value = { "", "1" };
  
  private boolean isLigneValeur = false;
  private boolean isDevis;
  
  private ODialog dialog_GA = null;
  private ODialog dialog_MARGES = null;
  
  public boolean isChantier;
  public boolean isSec55;
  private Calculatrice calculatrice = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    initDiverses();
    L1IN18.setValeurs(L1IN18_Value, null);
    L1IN20.setValeurs(L1IN20_Value, null);
    WSER.setValeursSelection("L", " ");
    UTAR.setValeurs(UTAR_Value);
    L1TE1.setValeursSelection("X", "");
    L1TE2.setValeursSelection("X", "");
    L1TE3.setValeursSelection("X", "");
    
    setCloseKey("ENTER", "F4", "F14", "F21", "F22", "F23");
    
    // Titre
    setTitle("Ligne de saisie");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/fonctions.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/agrandir.png", true));
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    L1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNV@")).trim());
    A1UNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    L1CNDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1CNDX@")).trim());
    WSTKX_3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WRESX_3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WAFFX_3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    WDISX_3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WATTX_3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WSTKX_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WAFFX_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    WDISX_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WCNRX_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCNRX@")).trim());
    WSTTX_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTTX@")).trim());
    WATTX_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WDISX_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WRESX_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WATTX_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WAFFX_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    OBJ_144.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDELX@")).trim());
    bt_GA
        .setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Aucune génération<BR>@&L000999@</HTML>")).trim());
    WPVCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPVCX@")).trim());
    cnvchantier.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    L1IN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1IN2@")).trim());
    WMMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMR@")).trim());
    WMMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMT@")).trim());
    WMAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAC@")).trim());
    WPMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMR@")).trim());
    WPMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMT@")).trim());
    WPAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAC@")).trim());
    WTPRL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPRL@")).trim());
    L1MHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1MHT@")).trim());
    WTPVL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPVL@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCA@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPS@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPM@")).trim());
    riBoutonDetail1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    gererAffichageMenus(riMenu_bt1);
    gererLesErreurs("19");
    
    isLigneValeur = lexique.isTrue("N04");
    isDevis = lexique.HostFieldGetData("ISDEVIS").trim().equals("1");
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    isSec55 = lexique.HostFieldGetData("SEC055").trim().equals("0");
    
    // Bouton prix flash
    if (!isChantier) {
      flash.setVisible(true);
      String heure = "";
      String wuheu = lexique.HostFieldGetData("WUHEU");
      if (wuheu.length() > 0) {
        if (wuheu.length() == 4) {
          heure = " à " + lexique.HostFieldGetNumericString(wuheu, 0).substring(0, 2) + " heure "
              + lexique.HostFieldGetNumericString(wuheu, 0).substring(2, 4);
        }
        else {
          heure = " à 0" + lexique.HostFieldGetNumericString(wuheu, 0).substring(0, 1) + " heure "
              + lexique.HostFieldGetNumericString(wuheu, 0).substring(1, 3);
        }
      }
      String wpvf = lexique.HostFieldGetData("WPVF").trim();
      if (!wpvf.isEmpty()) {
        flash.setToolTipText(
            "Prix négocié à " + wpvf + " par " + lexique.HostFieldGetData("WUSER") + " le " + lexique.HostFieldGetData("WUDAT") + heure);
        flash.setForeground(Color.BLUE);
      }
      else {
        flash.setToolTipText("Pas de prix flash pour cet article");
        flash.setForeground(Color.BLACK);
      }
    }
    else {
      flash.setVisible(false);
    }
    
    // Visibilité devis
    bt_GA.setVisible(!isLigneValeur && !isDevis);
    label11.setVisible(bt_GA.isVisible());
    label8.setVisible(bt_GA.isVisible());
    
    // Bloc des stocks
    String typs = lexique.HostFieldGetData("TYPS");
    p_s3.setVisible(typs.equalsIgnoreCase("S3"));
    p_s6.setVisible(typs.equalsIgnoreCase("S6"));
    p_s8.setVisible(typs.equalsIgnoreCase("S8"));
    
    OBJ_102.setVisible(lexique.isPresent("TYPS"));
    OBJ_103.setVisible(lexique.isPresent("TYPM"));
    OBJ_144.setVisible(lexique.isPresent("WDELX"));
    OBJ_88.setVisible(lexique.isPresent("WLCA"));
    // Si gratuit pas de notion de prix garanti
    L1IN2.setVisible(lexique.isTrue("02"));
    OBJ_146.setVisible(L1IN2.isVisible());
    L1IN18.setVisible(!isLigneValeur && L1IN2.getText().trim().isEmpty());
    OBJ_136.setVisible(lexique.isTrue("40"));
    L1CPL.setVisible(lexique.isTrue("40"));
    
    // Etat de l'article
    String wnpu = lexique.HostFieldGetData("WNPU");
    if (wnpu.equalsIgnoreCase("D")) {
      etat.setText("Déprécié");
    }
    else if (wnpu.equals("6")) {
      etat.setText("Fin de série");
    }
    else if (wnpu.equals("5")) {
      etat.setText("Pré-'fin de série'");
    }
    else if (wnpu.equals("2")) {
      etat.setText("Epuisé");
    }
    else {
      etat.setText("");
    }
    
    L1QTEX.setEnabled(lexique.isPresent("L1QTEX"));
    WARTT.setVisible(lexique.isPresent("WARTT"));
    L1LIB1.setEnabled(lexique.isPresent("L1LIB1"));
    OBJ_100.setVisible(L1REP.isVisible());
    
    // Traitement propre aux lignes de valeur
    p_stock.setVisible(!isLigneValeur);
    WSER.setVisible(!isLigneValeur && !isDevis);
    OBJ_102.setVisible(!isLigneValeur);
    OBJ_103.setVisible(!isLigneValeur);
    riBoutonDetail1.setVisible(!isLigneValeur);
    riSousMenu7.setEnabled(!isLigneValeur);
    riSousMenu9.setEnabled(!isLigneValeur);
    riSousMenu11.setEnabled(!isLigneValeur);
    riMenu3.setVisible(!isLigneValeur);
    riSousMenu14.setEnabled(!isLigneValeur);
    riSousMenu15.setEnabled(!isLigneValeur);
    riSousMenu13.setEnabled(!isLigneValeur);
    riSousMenu2.setVisible(isChantier);
    L1UNV.setVisible(lexique.isTrue("N15"));
    A1UNL.setVisible(!L1UNV.isVisible());
    L1COEX.setVisible(!isLigneValeur);
    
    // WSER
    if (!lexique.HostFieldGetData("A1IN2").trim().isEmpty()) {
      WSER.setText("Lots");
      WSER.setValeursSelection("L", " ");
      WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("L"));
    }
    else if (lexique.HostFieldGetData("A1TSP").trim().equals("S")) {
      WSER.setText("Série");
      WSER.setValeursSelection("S", " ");
      WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("L"));
    }
    else {
      WSER.setVisible(false);
    }
    
    // Visibilité des labels
    OBJ_129.setVisible(L1COEX.isVisible());
    OBJ_109.setVisible(UTAR.isVisible());
    
    L1CNDX.setVisible(lexique.isTrue("15"));
    label15.setVisible(L1CNDX.isVisible());
    
    // Chantier
    String l1in20 = lexique.HostFieldGetData("L1IN20").trim();
    L1IN20.setVisible(isChantier);
    cnvchantier.setVisible(isChantier);
    if (isChantier && l1in20.equals(" ")) {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    else {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    
    // Mettre à jour les bons textes du bouton génération d'achat et de son label de livraison
    String texteBouton = "Aucune";
    String texteLabel = "";
    String texteLibelle = "Génération d'achat:";
    String l1gba = lexique.HostFieldGetData("L1GBA").trim();
    if (!l1gba.isEmpty()) {
      int valeurN = Integer.parseInt(l1gba);
      switch (valeurN) {
        case 1:
          texteBouton = "Différée";
          break;
        case 2:
          texteBouton = "Différée";
          texteLabel = "Livraison directe assurée";
          break;
        case 3:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 4:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 5:
          texteBouton = "Immédiate";
          break;
        case 6:
          texteBouton = "Immédiate";
          texteLabel = "Livraison directe assurée";
          break;
        case 7:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        case 8:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          texteLibelle = "";
          break;
        default:
          texteBouton = "Aucune";
          break;
      }
    }
    bt_GA.setText(texteBouton);
    label11.setText(texteLabel);
    label8.setText(texteLibelle);
    
    boolean typm = lexique.HostFieldGetData("TYPM").startsWith("M");
    p_stock.setVisible(!isChantier && !typm);
    p_marge.setVisible((isChantier || typm) && isSec55);
    riSousMenu9.setEnabled(!isChantier);
    
    if (!p_stock.isVisible() && !p_marge.isVisible()) {
      this.setPreferredSize(new Dimension(1050, 345));
    }
    else {
      this.setPreferredSize(new Dimension(1050, 475));
    }
    
    riBoutonDetail2
        .setVisible((!lexique.HostFieldGetData("WREM2").trim().isEmpty() || !lexique.HostFieldGetData("WREM3").trim().isEmpty())
            && !lexique.HostFieldGetData("WREM1").trim().isEmpty());
    
    // Marge
    int etatMarge = Integer.parseInt(lexique.HostFieldGetData("MARGU"));
    if (etatMarge > 0) {
      if (dialog_MARGES == null) {
        dialog_MARGES = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_MARGES(this));
      }
      dialog_MARGES.affichePopupPerso();
    }
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Afficher la boîte de dialogue de détail du calcul du montant.
   */
  private void afficherDetailCalculMontant(EnumTypePrixVente pTypePrixVente) {
    ParametreChargement parametreChargement = new ParametreChargement();
    
    // Récupérer l'identifiant du document de vente
    IdDocumentVente idDocumentVente = (IdDocumentVente) lexique.getValeurVariableGlobaleFromSession("idDocumentVente");
    parametreChargement.setIdDocumentVente(idDocumentVente);
    
    // Récupérer l'identifiant de la ligne de vente
    int numeroLigne = Constantes.convertirTexteEnInteger(WNLI.getText());
    IdLigneVente idLigneVente = IdLigneVente.getInstance(idDocumentVente.getIdEtablissement(), idDocumentVente.getCodeEntete(),
        idDocumentVente.getNumero(), idDocumentVente.getSuffixe(), numeroLigne);
    parametreChargement.setIdLigneDocumentVente(idLigneVente);
    
    // Récupérer l'identifiant du client (inutile car récupéré dans l'entête du document)
    // IdClient idClient = (IdClient) lexique.getValeurVariableGlobaleFromSession("idClient");
    // parametreChargement.setIdClientFacture(idClient);
    
    // Récupérer l'identifiant du magasin
    // IdMagasin idMagasin = (IdMagasin) lexique.getValeurVariableGlobaleFromSession("idMagasin");
    if (!Constantes.normerTexte(L1MAG.getText()).isEmpty()) {
      IdMagasin idMagasin = IdMagasin.getInstance(idDocumentVente.getIdEtablissement(), L1MAG.getText());
      parametreChargement.setIdMagasin(idMagasin);
    }
    
    // Renseigner l'identifiant de l'article
    if (WARTT.getText() != null) {
      IdArticle idArticle = IdArticle.getInstance(idDocumentVente.getIdEtablissement(), WARTT.getText());
      parametreChargement.setIdEtablissement(idArticle.getIdEtablissement());
      parametreChargement.setIdArticle(idArticle);
    }
    
    // Afficher la boite de dialogue du détail du calcul d'un prix de vente
    ModeleDialogueDetailPrixVente modele = new ModeleDialogueDetailPrixVente(getSession(), parametreChargement, pTypePrixVente);
    DialogueDetailPrixVente vue = new DialogueDetailPrixVente(modele);
    vue.afficher();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 31);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
    
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("BTAR", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(22, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 01);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 51);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void L1UNVActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WLDLP");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_127ActionPerformed(ActionEvent e) {
    if (dialog_GA == null) {
      dialog_GA = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_GA(this));
    }
    dialog_GA.affichePopupPerso();
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(FCT1.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(FCT1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  public void rechargerEcran() {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("L1QTEX");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("BCNQ", 0, "2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WREM1");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("COLPAL", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSPR", 0, "N");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    String folder =
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim();
    File directory = new File(folder);
    try {
      if (directory.exists()) {
        Desktop.getDesktop().open(directory);
      }
      else {
        JOptionPane.showMessageDialog(null, "Le dossier spécifié pour les documents liés n'existe pas");
      }
    }
    catch (IOException e1) {
      JOptionPane.showMessageDialog(null, e1.toString());
    }
  }
  
  private void flashActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSPR", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void appliquerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("L1PVNX", 0, lexique.HostFieldGetData("WPVF"));
    L1PVNX.setText(lexique.HostFieldGetData("WPVF"));
  }
  
  private void negoActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSPR", 0, "N");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    if (calculatrice == null) {
      calculatrice = new Calculatrice(BTD5.getInvoker());
    }
    else {
      calculatrice.reveiller(BTD5.getInvoker());
    }
  }
  
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riBoutonDetail3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(22, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void L1IN20ItemStateChanged(ItemEvent e) {
    if (isChantier && L1IN20.getSelectedIndex() == 1) {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    else {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    cnvchantier.repaint();
  }
  
  private void WPVCXMouseClicked(MouseEvent e) {
    try {
      // Pour que l'action soit exécutée il faut un CTRL + clic souris dans le composant
      if (!e.isControlDown()) {
        return;
      }
      
      // Déterminer le prix à metttre en évidence
      Boolean affichageTTC = (Boolean) lexique.getValeurVariableGlobaleFromSession("affichageTTC");
      EnumTypePrixVente typePrixVente = EnumTypePrixVente.PRIX_NET_HT;
      if (affichageTTC != null && affichageTTC.booleanValue()) {
        typePrixVente = EnumTypePrixVente.PRIX_NET_TTC;
      }
      
      // Afficher la boîte de dialogue
      afficherDetailCalculMontant(typePrixVente);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_principal = new JPanel();
    p_contenu = new JPanel();
    panel7 = new JPanel();
    L1QTEX = new XRiTextField();
    OBJ_106 = new JLabel();
    L1UNV = new RiZoneSortie();
    A1UNL = new RiZoneSortie();
    L1CNDX = new RiZoneSortie();
    label15 = new JLabel();
    OBJ_136 = new JLabel();
    L1CPL = new XRiTextField();
    p_stock = new JPanel();
    riBoutonDetailListe1 = new SNBoutonDetail();
    p_s3 = new JPanel();
    WSTKX_3 = new RiZoneSortie();
    OBJ_94 = new JLabel();
    OBJ_444 = new JLabel();
    WRESX_3 = new RiZoneSortie();
    WAFFX_3 = new RiZoneSortie();
    OBJ_96 = new JLabel();
    OBJ_97 = new JLabel();
    WDISX_3 = new RiZoneSortie();
    WATTX_3 = new RiZoneSortie();
    commande_88 = new JLabel();
    OBJ_605 = new JLabel();
    disp_11 = new JLabel();
    att_11 = new JLabel();
    p_s6 = new JPanel();
    WSTKX_6 = new RiZoneSortie();
    OBJ_95 = new JLabel();
    OBJ_494 = new JLabel();
    WAFFX_6 = new RiZoneSortie();
    label1 = new JLabel();
    OBJ_99 = new JLabel();
    WDISX_6 = new RiZoneSortie();
    label2 = new JLabel();
    WCNRX_6 = new RiZoneSortie();
    OBJ_101 = new JLabel();
    label3 = new JLabel();
    WSTTX_6 = new RiZoneSortie();
    label4 = new JLabel();
    OBJ_104 = new JLabel();
    WATTX_6 = new RiZoneSortie();
    label5 = new JLabel();
    p_s8 = new JPanel();
    WDISX_8 = new RiZoneSortie();
    disp_22 = new JLabel();
    WRESX_8 = new RiZoneSortie();
    commande_9 = new JLabel();
    WATTX_8 = new RiZoneSortie();
    att_12 = new JLabel();
    WAFFX_7 = new RiZoneSortie();
    att_13 = new JLabel();
    L1MAG = new XRiTextField();
    OBJ_98 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_144 = new RiZoneSortie();
    label8 = new JLabel();
    bt_GA = new SNBoutonLeger();
    label11 = new JLabel();
    WSER = new XRiCheckBox();
    label9 = new JLabel();
    panel2 = new JPanel();
    L1PVNX = new XRiTextField();
    OBJ_107 = new JLabel();
    label7 = new JLabel();
    WREM1 = new XRiTextField();
    riBoutonDetail2 = new SNBoutonDetail();
    label10 = new JLabel();
    WPVCX = new RiZoneSortie();
    L1PVBX = new XRiTextField();
    label12 = new JLabel();
    UTAR = new XRiSpinner();
    nego = new SNBoutonDetail();
    flash = new SNBoutonDetail();
    L1IN20 = new XRiComboBox();
    cnvchantier = new RiZoneSortie();
    OBJ_146 = new JLabel();
    L1IN18 = new XRiComboBox();
    L1IN2 = new RiZoneSortie();
    p_marge = new JPanel();
    label13 = new JLabel();
    label14 = new JLabel();
    label16 = new JLabel();
    WMMR = new RiZoneSortie();
    WMMT = new RiZoneSortie();
    WMAC = new RiZoneSortie();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    WPMR = new RiZoneSortie();
    WPMT = new RiZoneSortie();
    WPAC = new RiZoneSortie();
    label20 = new JLabel();
    label21 = new JLabel();
    label22 = new JLabel();
    WTPRL = new RiZoneSortie();
    L1MHT = new RiZoneSortie();
    WTPVL = new RiZoneSortie();
    marge = new JLabel();
    panel16 = new JPanel();
    L1TE1 = new XRiCheckBox();
    L1TE2 = new XRiCheckBox();
    L1TE3 = new XRiCheckBox();
    barre_tete = new JMenuBar();
    P_Infos = new JPanel();
    WNLI = new RiZoneSortie();
    WARTT = new RiZoneSortie();
    OBJ_87 = new JLabel();
    OBJ_88 = new JLabel();
    etat = new JLabel();
    L1LIB1 = new XRiTextField();
    bt_ecran = new JLabel();
    riBoutonDetail3 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    FCT1 = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    riSousMenu3 = new RiSousMenu();
    panel1 = new JPanel();
    label6 = new JLabel();
    WCOD = new RiZoneSortie();
    OBJ_100 = new JLabel();
    L1REP = new XRiTextField();
    OBJ_112 = new JLabel();
    L20NAT = new XRiTextField();
    OBJ_102 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    OBJ_129 = new JLabel();
    L1COEX = new XRiTextField();
    WREM2 = new XRiTextField();
    WREM3 = new XRiTextField();
    riBoutonDetail1 = new SNBoutonDetail();
    OBJ_109 = new JLabel();
    L1UNV_ = new SNBoutonDetail();
    BTD2 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    BTD3 = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    BTD4 = new JPopupMenu();
    appliquer = new JMenuItem();
    BTD5 = new JPopupMenu();
    menuItem2 = new JMenuItem();
    menuItem3 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu_bt3 = new RiSousMenu_bt();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1050, 475));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());
      
      // ======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());
        
        // ======== navig_erreurs ========
        {
          navig_erreurs.setName("navig_erreurs");
          
          // ---- bouton_erreurs ----
          bouton_erreurs.setText("Erreurs");
          bouton_erreurs.setToolTipText("Erreurs");
          bouton_erreurs.setName("bouton_erreurs");
          navig_erreurs.add(bouton_erreurs);
        }
        menus_bas.add(navig_erreurs);
        
        // ======== navig_valid ========
        {
          navig_valid.setName("navig_valid");
          
          // ---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setToolTipText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);
        
        // ======== navig_retour ========
        {
          navig_retour.setName("navig_retour");
          
          // ---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setToolTipText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
      
      // ======== scroll_droite ========
      {
        scroll_droite.setBackground(new Color(238, 239, 241));
        scroll_droite.setPreferredSize(new Dimension(16, 550));
        scroll_droite.setBorder(null);
        scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scroll_droite.setAutoscrolls(true);
        scroll_droite.setName("scroll_droite");
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 320));
          menus_haut.setPreferredSize(new Dimension(160, 550));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setMaximumSize(new Dimension(11, 11));
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setToolTipText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riMenu1 ========
          {
            riMenu1.setName("riMenu1");
            
            // ---- riMenu_bt1 ----
            riMenu_bt1.setText("Options");
            riMenu_bt1.setName("riMenu_bt1");
            riMenu1.add(riMenu_bt1);
          }
          menus_haut.add(riMenu1);
          
          // ======== riSousMenu5 ========
          {
            riSousMenu5.setName("riSousMenu5");
            
            // ---- riSousMenu_bt5 ----
            riSousMenu_bt5.setText("N\u00e9gocier");
            riSousMenu_bt5.setToolTipText("N\u00e9gocier");
            riSousMenu_bt5.setName("riSousMenu_bt5");
            riSousMenu_bt5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt5ActionPerformed(e);
              }
            });
            riSousMenu5.add(riSousMenu_bt5);
          }
          menus_haut.add(riSousMenu5);
          
          // ======== riSousMenu14 ========
          {
            riSousMenu14.setName("riSousMenu14");
            
            // ---- riSousMenu_bt14 ----
            riSousMenu_bt14.setText("Position en stock");
            riSousMenu_bt14.setToolTipText("Position en Stock");
            riSousMenu_bt14.setName("riSousMenu_bt14");
            riSousMenu_bt14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt14ActionPerformed(e);
              }
            });
            riSousMenu14.add(riSousMenu_bt14);
          }
          menus_haut.add(riSousMenu14);
          
          // ======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");
            
            // ---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Historique vente/article");
            riSousMenu_bt7.setToolTipText("Historique Vente/Article");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
          
          // ======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");
            
            // ---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Affichage des marges");
            riSousMenu_bt9.setToolTipText("Affichage des marges");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
          
          // ======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");
            
            // ---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Documents li\u00e9s");
            riSousMenu_bt8.setToolTipText("Gestion des documents li\u00e9s \u00e0 cette ligne de bon");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Compl\u00e9ments");
            riMenu_bt2.setToolTipText("Compl\u00e9ments");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu2 ========
          {
            riSousMenu2.setName("riSousMenu2");
            
            // ---- riSousMenu_bt2 ----
            riSousMenu_bt2.setText("Conditions quantitatives");
            riSousMenu_bt2.setToolTipText("Conditions quantitatives");
            riSousMenu_bt2.setName("riSousMenu_bt2");
            riSousMenu_bt2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt2ActionPerformed(e);
              }
            });
            riSousMenu2.add(riSousMenu_bt2);
          }
          menus_haut.add(riSousMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Options article");
            riSousMenu_bt6.setToolTipText("Options article");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
          
          // ======== riSousMenu4 ========
          {
            riSousMenu4.setName("riSousMenu4");
            
            // ---- riSousMenu_bt4 ----
            riSousMenu_bt4.setText("Lien internet");
            riSousMenu_bt4.setToolTipText("Lien internet");
            riSousMenu_bt4.setName("riSousMenu_bt4");
            riSousMenu_bt4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt4ActionPerformed(e);
              }
            });
            riSousMenu4.add(riSousMenu_bt4);
          }
          menus_haut.add(riSousMenu4);
          
          // ======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");
            
            // ---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Acc\u00e9s au tarif");
            riSousMenu_bt10.setToolTipText("Acc\u00e9s au tarif");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);
          
          // ======== riSousMenu13 ========
          {
            riSousMenu13.setName("riSousMenu13");
            
            // ---- riSousMenu_bt13 ----
            riSousMenu_bt13.setText("Gestion extension ligne");
            riSousMenu_bt13.setToolTipText("Gestion extension ligne");
            riSousMenu_bt13.setName("riSousMenu_bt13");
            riSousMenu_bt13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt13ActionPerformed(e);
              }
            });
            riSousMenu13.add(riSousMenu_bt13);
          }
          menus_haut.add(riSousMenu13);
          
          // ======== riSousMenu12 ========
          {
            riSousMenu12.setName("riSousMenu12");
            
            // ---- riSousMenu_bt12 ----
            riSousMenu_bt12.setText("Extensions de libell\u00e9s");
            riSousMenu_bt12.setToolTipText("Saisie ou modification des extentions de libell\u00e9(s) pour l'article");
            riSousMenu_bt12.setName("riSousMenu_bt12");
            riSousMenu_bt12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt12ActionPerformed(e);
              }
            });
            riSousMenu12.add(riSousMenu_bt12);
          }
          menus_haut.add(riSousMenu12);
          
          // ======== riSousMenu11 ========
          {
            riSousMenu11.setName("riSousMenu11");
            
            // ---- riSousMenu_bt11 ----
            riSousMenu_bt11.setText("Articles \u00e9quivalents");
            riSousMenu_bt11.setToolTipText("Articles \u00e9quivalents");
            riSousMenu_bt11.setName("riSousMenu_bt11");
            riSousMenu_bt11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt11ActionPerformed(e);
              }
            });
            riSousMenu11.add(riSousMenu_bt11);
          }
          menus_haut.add(riSousMenu11);
          
          // ======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");
            
            // ---- riMenu_bt3 ----
            riMenu_bt3.setText("Stocks");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);
          
          // ======== riSousMenu15 ========
          {
            riSousMenu15.setName("riSousMenu15");
            
            // ---- riSousMenu_bt15 ----
            riSousMenu_bt15.setText("Attendu / command\u00e9");
            riSousMenu_bt15.setToolTipText("Attendu / command\u00e9");
            riSousMenu_bt15.setName("riSousMenu_bt15");
            riSousMenu_bt15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt15ActionPerformed(e);
              }
            });
            riSousMenu15.add(riSousMenu_bt15);
          }
          menus_haut.add(riSousMenu15);
        }
        scroll_droite.setViewportView(menus_haut);
      }
      p_menus.add(scroll_droite, BorderLayout.NORTH);
    }
    add(p_menus, BorderLayout.EAST);
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel7 ========
        {
          panel7.setBorder(new TitledBorder(""));
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- L1QTEX ----
          L1QTEX.setComponentPopupMenu(BTD5);
          L1QTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTEX.setName("L1QTEX");
          panel7.add(L1QTEX);
          L1QTEX.setBounds(80, 25, 74, L1QTEX.getPreferredSize().height);
          
          // ---- OBJ_106 ----
          OBJ_106.setText("Quantit\u00e9");
          OBJ_106.setFont(OBJ_106.getFont().deriveFont(OBJ_106.getFont().getStyle() | Font.BOLD));
          OBJ_106.setName("OBJ_106");
          panel7.add(OBJ_106);
          OBJ_106.setBounds(20, 29, 58, 20);
          
          // ---- L1UNV ----
          L1UNV.setText("@L1UNV@");
          L1UNV.setName("L1UNV");
          panel7.add(L1UNV);
          L1UNV.setBounds(160, 27, 34, L1UNV.getPreferredSize().height);
          
          // ---- A1UNL ----
          A1UNL.setText("@A1UNL@");
          A1UNL.setName("A1UNL");
          panel7.add(A1UNL);
          A1UNL.setBounds(160, 27, 34, 24);
          
          // ---- L1CNDX ----
          L1CNDX.setText("@L1CNDX@");
          L1CNDX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1CNDX.setName("L1CNDX");
          panel7.add(L1CNDX);
          L1CNDX.setBounds(230, 27, 78, L1CNDX.getPreferredSize().height);
          
          // ---- label15 ----
          label15.setText("X");
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setName("label15");
          panel7.add(label15);
          label15.setBounds(190, 29, 35, 20);
          
          // ---- OBJ_136 ----
          OBJ_136.setText("pi\u00e8ces");
          OBJ_136.setName("OBJ_136");
          panel7.add(OBJ_136);
          OBJ_136.setBounds(330, 25, 53, 28);
          
          // ---- L1CPL ----
          L1CPL.setComponentPopupMenu(BTD);
          L1CPL.setHorizontalAlignment(SwingConstants.RIGHT);
          L1CPL.setName("L1CPL");
          panel7.add(L1CPL);
          L1CPL.setBounds(390, 25, 74, L1CPL.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel7);
        panel7.setBounds(15, 10, 850, 75);
        
        // ======== p_stock ========
        {
          p_stock.setBorder(new TitledBorder("Stocks"));
          p_stock.setOpaque(false);
          p_stock.setName("p_stock");
          p_stock.setLayout(null);
          
          // ---- riBoutonDetailListe1 ----
          riBoutonDetailListe1.setToolTipText("position de stock");
          riBoutonDetailListe1.setName("riBoutonDetailListe1");
          riBoutonDetailListe1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe1ActionPerformed(e);
            }
          });
          p_stock.add(riBoutonDetailListe1);
          riBoutonDetailListe1.setBounds(60, -5, 30, 30);
          
          // ======== p_s3 ========
          {
            p_s3.setOpaque(false);
            p_s3.setName("p_s3");
            p_s3.setLayout(null);
            
            // ---- WSTKX_3 ----
            WSTKX_3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WSTKX_3.setText("@WSTKX@");
            WSTKX_3.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTKX_3.setName("WSTKX_3");
            p_s3.add(WSTKX_3);
            WSTKX_3.setBounds(15, 30, 98, 24);
            
            // ---- OBJ_94 ----
            OBJ_94.setText("-");
            OBJ_94.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_94.setFont(OBJ_94.getFont().deriveFont(OBJ_94.getFont().getStyle() | Font.BOLD, OBJ_94.getFont().getSize() + 3f));
            OBJ_94.setName("OBJ_94");
            p_s3.add(OBJ_94);
            OBJ_94.setBounds(120, 32, 19, 20);
            
            // ---- OBJ_444 ----
            OBJ_444.setText("En stock");
            OBJ_444.setName("OBJ_444");
            p_s3.add(OBJ_444);
            OBJ_444.setBounds(15, 5, 69, 21);
            
            // ---- WRESX_3 ----
            WRESX_3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WRESX_3.setText("@WRESX@");
            WRESX_3.setHorizontalAlignment(SwingConstants.RIGHT);
            WRESX_3.setName("WRESX_3");
            p_s3.add(WRESX_3);
            WRESX_3.setBounds(145, 30, 98, WRESX_3.getPreferredSize().height);
            
            // ---- WAFFX_3 ----
            WAFFX_3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WAFFX_3.setText("@WAFFX@");
            WAFFX_3.setHorizontalAlignment(SwingConstants.RIGHT);
            WAFFX_3.setName("WAFFX_3");
            p_s3.add(WAFFX_3);
            WAFFX_3.setBounds(275, 30, 98, WAFFX_3.getPreferredSize().height);
            
            // ---- OBJ_96 ----
            OBJ_96.setText("+");
            OBJ_96.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_96.setFont(OBJ_96.getFont().deriveFont(OBJ_96.getFont().getStyle() | Font.BOLD, OBJ_96.getFont().getSize() + 1f));
            OBJ_96.setName("OBJ_96");
            p_s3.add(OBJ_96);
            OBJ_96.setBounds(250, 32, 19, 20);
            
            // ---- OBJ_97 ----
            OBJ_97.setText("=");
            OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_97.setFont(OBJ_97.getFont().deriveFont(OBJ_97.getFont().getStyle() | Font.BOLD, OBJ_97.getFont().getSize() + 1f));
            OBJ_97.setName("OBJ_97");
            p_s3.add(OBJ_97);
            OBJ_97.setBounds(380, 32, 19, 20);
            
            // ---- WDISX_3 ----
            WDISX_3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WDISX_3.setText("@WDISX@");
            WDISX_3.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX_3.setName("WDISX_3");
            p_s3.add(WDISX_3);
            WDISX_3.setBounds(405, 30, 98, WDISX_3.getPreferredSize().height);
            
            // ---- WATTX_3 ----
            WATTX_3.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WATTX_3.setText("@WATTX@");
            WATTX_3.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX_3.setName("WATTX_3");
            p_s3.add(WATTX_3);
            WATTX_3.setBounds(535, 30, 98, WATTX_3.getPreferredSize().height);
            
            // ---- commande_88 ----
            commande_88.setText("Commandes clients");
            commande_88.setName("commande_88");
            p_s3.add(commande_88);
            commande_88.setBounds(145, 5, 121, 20);
            
            // ---- OBJ_605 ----
            OBJ_605.setText("Dont r\u00e9serv\u00e9es");
            OBJ_605.setName("OBJ_605");
            p_s3.add(OBJ_605);
            OBJ_605.setBounds(275, 5, 90, 20);
            
            // ---- disp_11 ----
            disp_11.setText("Disponible");
            disp_11.setName("disp_11");
            p_s3.add(disp_11);
            disp_11.setBounds(405, 5, 69, 20);
            
            // ---- att_11 ----
            att_11.setText("Attendu");
            att_11.setName("att_11");
            p_s3.add(att_11);
            att_11.setBounds(535, 5, 60, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < p_s3.getComponentCount(); i++) {
                Rectangle bounds = p_s3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_s3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_s3.setMinimumSize(preferredSize);
              p_s3.setPreferredSize(preferredSize);
            }
          }
          p_stock.add(p_s3);
          p_s3.setBounds(10, 65, 680, 65);
          
          // ======== p_s6 ========
          {
            p_s6.setOpaque(false);
            p_s6.setName("p_s6");
            p_s6.setLayout(null);
            
            // ---- WSTKX_6 ----
            WSTKX_6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WSTKX_6.setText("@WSTKX@");
            WSTKX_6.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTKX_6.setName("WSTKX_6");
            p_s6.add(WSTKX_6);
            WSTKX_6.setBounds(15, 30, 98, 24);
            
            // ---- OBJ_95 ----
            OBJ_95.setText("-");
            OBJ_95.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD, OBJ_95.getFont().getSize() + 3f));
            OBJ_95.setName("OBJ_95");
            p_s6.add(OBJ_95);
            OBJ_95.setBounds(120, 30, 19, 20);
            
            // ---- OBJ_494 ----
            OBJ_494.setText("En stock");
            OBJ_494.setName("OBJ_494");
            p_s6.add(OBJ_494);
            OBJ_494.setBounds(15, 5, 69, 21);
            
            // ---- WAFFX_6 ----
            WAFFX_6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WAFFX_6.setText("@WAFFX@");
            WAFFX_6.setHorizontalAlignment(SwingConstants.RIGHT);
            WAFFX_6.setName("WAFFX_6");
            p_s6.add(WAFFX_6);
            WAFFX_6.setBounds(145, 30, 98, WAFFX_6.getPreferredSize().height);
            
            // ---- label1 ----
            label1.setText("R\u00e9serv\u00e9");
            label1.setName("label1");
            p_s6.add(label1);
            label1.setBounds(145, 5, 85, 21);
            
            // ---- OBJ_99 ----
            OBJ_99.setText("=");
            OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getStyle() | Font.BOLD, OBJ_99.getFont().getSize() + 1f));
            OBJ_99.setName("OBJ_99");
            p_s6.add(OBJ_99);
            OBJ_99.setBounds(250, 30, 19, 20);
            
            // ---- WDISX_6 ----
            WDISX_6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WDISX_6.setText("@WDISX@");
            WDISX_6.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX_6.setName("WDISX_6");
            p_s6.add(WDISX_6);
            WDISX_6.setBounds(275, 30, 98, WDISX_6.getPreferredSize().height);
            
            // ---- label2 ----
            label2.setText("Disponible");
            label2.setName("label2");
            p_s6.add(label2);
            label2.setBounds(275, 5, label2.getPreferredSize().width, 21);
            
            // ---- WCNRX_6 ----
            WCNRX_6.setText("@WCNRX@");
            WCNRX_6.setHorizontalAlignment(SwingConstants.RIGHT);
            WCNRX_6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WCNRX_6.setName("WCNRX_6");
            p_s6.add(WCNRX_6);
            WCNRX_6.setBounds(new Rectangle(new Point(405, 30), WCNRX_6.getPreferredSize()));
            
            // ---- OBJ_101 ----
            OBJ_101.setText("-");
            OBJ_101.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_101.setFont(OBJ_101.getFont().deriveFont(OBJ_101.getFont().getStyle() | Font.BOLD, OBJ_101.getFont().getSize() + 3f));
            OBJ_101.setName("OBJ_101");
            p_s6.add(OBJ_101);
            OBJ_101.setBounds(380, 30, 19, OBJ_101.getPreferredSize().height);
            
            // ---- label3 ----
            label3.setText("Command\u00e9");
            label3.setName("label3");
            p_s6.add(label3);
            label3.setBounds(405, 5, 100, 21);
            
            // ---- WSTTX_6 ----
            WSTTX_6.setText("@WSTTX@");
            WSTTX_6.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTTX_6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WSTTX_6.setName("WSTTX_6");
            p_s6.add(WSTTX_6);
            WSTTX_6.setBounds(535, 30, 100, WSTTX_6.getPreferredSize().height);
            
            // ---- label4 ----
            label4.setText("Th\u00e9orique");
            label4.setName("label4");
            p_s6.add(label4);
            label4.setBounds(535, 5, label4.getPreferredSize().width, 21);
            
            // ---- OBJ_104 ----
            OBJ_104.setText("=");
            OBJ_104.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_104.setFont(OBJ_104.getFont().deriveFont(OBJ_104.getFont().getStyle() | Font.BOLD, OBJ_104.getFont().getSize() + 1f));
            OBJ_104.setName("OBJ_104");
            p_s6.add(OBJ_104);
            OBJ_104.setBounds(510, 30, 19, 20);
            
            // ---- WATTX_6 ----
            WATTX_6.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WATTX_6.setText("@WATTX@");
            WATTX_6.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX_6.setName("WATTX_6");
            p_s6.add(WATTX_6);
            WATTX_6.setBounds(650, 30, 98, WATTX_6.getPreferredSize().height);
            
            // ---- label5 ----
            label5.setText("Attendu");
            label5.setName("label5");
            p_s6.add(label5);
            label5.setBounds(650, 5, 56, 21);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < p_s6.getComponentCount(); i++) {
                Rectangle bounds = p_s6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_s6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_s6.setMinimumSize(preferredSize);
              p_s6.setPreferredSize(preferredSize);
            }
          }
          p_stock.add(p_s6);
          p_s6.setBounds(10, 65, 755, 65);
          
          // ======== p_s8 ========
          {
            p_s8.setOpaque(false);
            p_s8.setName("p_s8");
            p_s8.setLayout(null);
            
            // ---- WDISX_8 ----
            WDISX_8.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WDISX_8.setText("@WDISX@");
            WDISX_8.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX_8.setName("WDISX_8");
            p_s8.add(WDISX_8);
            WDISX_8.setBounds(15, 30, 98, WDISX_8.getPreferredSize().height);
            
            // ---- disp_22 ----
            disp_22.setText("Disponible");
            disp_22.setName("disp_22");
            p_s8.add(disp_22);
            disp_22.setBounds(15, 5, 69, 20);
            
            // ---- WRESX_8 ----
            WRESX_8.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WRESX_8.setText("@WRESX@");
            WRESX_8.setHorizontalAlignment(SwingConstants.RIGHT);
            WRESX_8.setName("WRESX_8");
            p_s8.add(WRESX_8);
            WRESX_8.setBounds(145, 30, 98, WRESX_8.getPreferredSize().height);
            
            // ---- commande_9 ----
            commande_9.setText("Commandes clients");
            commande_9.setName("commande_9");
            p_s8.add(commande_9);
            commande_9.setBounds(145, 5, 121, 20);
            
            // ---- WATTX_8 ----
            WATTX_8.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WATTX_8.setText("@WATTX@");
            WATTX_8.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX_8.setName("WATTX_8");
            p_s8.add(WATTX_8);
            WATTX_8.setBounds(275, 30, 98, WATTX_8.getPreferredSize().height);
            
            // ---- att_12 ----
            att_12.setText("Attendu");
            att_12.setName("att_12");
            p_s8.add(att_12);
            att_12.setBounds(275, 5, 60, 20);
            
            // ---- WAFFX_7 ----
            WAFFX_7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WAFFX_7.setText("@WAFFX@");
            WAFFX_7.setHorizontalAlignment(SwingConstants.RIGHT);
            WAFFX_7.setName("WAFFX_7");
            p_s8.add(WAFFX_7);
            WAFFX_7.setBounds(405, 30, 98, WAFFX_7.getPreferredSize().height);
            
            // ---- att_13 ----
            att_13.setText("Attendu disponible");
            att_13.setName("att_13");
            p_s8.add(att_13);
            att_13.setBounds(405, 5, 110, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < p_s8.getComponentCount(); i++) {
                Rectangle bounds = p_s8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_s8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_s8.setMinimumSize(preferredSize);
              p_s8.setPreferredSize(preferredSize);
            }
          }
          p_stock.add(p_s8);
          p_s8.setBounds(10, 65, 540, 65);
          
          // ---- L1MAG ----
          L1MAG.setComponentPopupMenu(BTD);
          L1MAG.setName("L1MAG");
          p_stock.add(L1MAG);
          L1MAG.setBounds(80, 35, 34, L1MAG.getPreferredSize().height);
          
          // ---- OBJ_98 ----
          OBJ_98.setText("Magasin");
          OBJ_98.setName("OBJ_98");
          p_stock.add(OBJ_98);
          OBJ_98.setBounds(25, 39, 55, 20);
          
          // ---- OBJ_110 ----
          OBJ_110.setText("D\u00e9lai");
          OBJ_110.setName("OBJ_110");
          p_stock.add(OBJ_110);
          OBJ_110.setBounds(130, 39, 45, 20);
          
          // ---- OBJ_144 ----
          OBJ_144.setText("@WDELX@");
          OBJ_144.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_144.setName("OBJ_144");
          p_stock.add(OBJ_144);
          OBJ_144.setBounds(170, 37, 26, OBJ_144.getPreferredSize().height);
          
          // ---- label8 ----
          label8.setText("G\u00e9n\u00e9ration d'achat:");
          label8.setName("label8");
          p_stock.add(label8);
          label8.setBounds(275, 39, 115, 20);
          
          // ---- bt_GA ----
          bt_GA.setText("Aucune");
          bt_GA.setToolTipText("<HTML>Aucune g\u00e9n\u00e9ration<BR>@&L000999@</HTML>");
          bt_GA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_GA.setName("bt_GA");
          bt_GA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_127ActionPerformed(e);
            }
          });
          p_stock.add(bt_GA);
          bt_GA.setBounds(390, 37, 120, 25);
          
          // ---- label11 ----
          label11.setText("text");
          label11.setHorizontalAlignment(SwingConstants.LEFT);
          label11.setForeground(new Color(102, 102, 102));
          label11.setName("label11");
          p_stock.add(label11);
          label11.setBounds(520, 40, 165, 18);
          
          // ---- WSER ----
          WSER.setText("WSER");
          WSER.setComponentPopupMenu(null);
          WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER.setHorizontalAlignment(SwingConstants.RIGHT);
          WSER.setName("WSER");
          p_stock.add(WSER);
          WSER.setBounds(685, 39, 70, 20);
          
          // ---- label9 ----
          label9.setText("semaines");
          label9.setName("label9");
          p_stock.add(label9);
          label9.setBounds(205, 39, 65, 21);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_stock.getComponentCount(); i++) {
              Rectangle bounds = p_stock.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_stock.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_stock.setMinimumSize(preferredSize);
            p_stock.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_stock);
        p_stock.setBounds(15, 205, 850, 150);
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Condition"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- L1PVNX ----
          L1PVNX.setComponentPopupMenu(BTD5);
          L1PVNX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVNX.setName("L1PVNX");
          panel2.add(L1PVNX);
          L1PVNX.setBounds(640, 40, 90, L1PVNX.getPreferredSize().height);
          
          // ---- OBJ_107 ----
          OBJ_107.setText("Tarif");
          OBJ_107.setName("OBJ_107");
          panel2.add(OBJ_107);
          OBJ_107.setBounds(25, 44, 40, 20);
          
          // ---- label7 ----
          label7.setText("Remise");
          label7.setName("label7");
          panel2.add(label7);
          label7.setBounds(235, 45, 55, 20);
          
          // ---- WREM1 ----
          WREM1.setComponentPopupMenu(BTD3);
          WREM1.setHorizontalAlignment(SwingConstants.RIGHT);
          WREM1.setName("WREM1");
          panel2.add(WREM1);
          WREM1.setBounds(285, 40, 50, WREM1.getPreferredSize().height);
          
          // ---- riBoutonDetail2 ----
          riBoutonDetail2.setToolTipText("Autres remises");
          riBoutonDetail2.setName("riBoutonDetail2");
          riBoutonDetail2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetail2ActionPerformed(e);
            }
          });
          panel2.add(riBoutonDetail2);
          riBoutonDetail2.setBounds(335, 45, 20, riBoutonDetail2.getPreferredSize().height);
          
          // ---- label10 ----
          label10.setText("Prix calcul\u00e9");
          label10.setName("label10");
          panel2.add(label10);
          label10.setBounds(370, 45, 70, 20);
          
          // ---- WPVCX ----
          WPVCX.setText("@WPVCX@");
          WPVCX.setHorizontalAlignment(SwingConstants.RIGHT);
          WPVCX.setName("WPVCX");
          WPVCX.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WPVCXMouseClicked(e);
            }
          });
          panel2.add(WPVCX);
          WPVCX.setBounds(440, 40, 90, WPVCX.getPreferredSize().height);
          
          // ---- L1PVBX ----
          L1PVBX.setComponentPopupMenu(null);
          L1PVBX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVBX.setName("L1PVBX");
          panel2.add(L1PVBX);
          L1PVBX.setBounds(135, 40, 90, L1PVBX.getPreferredSize().height);
          
          // ---- label12 ----
          label12.setText("Ou prix net saisi");
          label12.setName("label12");
          panel2.add(label12);
          label12.setBounds(540, 45, 100, 20);
          
          // ---- UTAR ----
          UTAR.setComponentPopupMenu(BTD);
          UTAR.setName("UTAR");
          panel2.add(UTAR);
          UTAR.setBounds(90, 40, 45, UTAR.getPreferredSize().height);
          
          // ---- nego ----
          nego.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          nego.setText("N\u00e9gocier");
          nego.setComponentPopupMenu(BTD4);
          nego.setName("nego");
          nego.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              negoActionPerformed(e);
            }
          });
          panel2.add(nego);
          nego.setBounds(735, 40, 100, 25);
          
          // ---- flash ----
          flash.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          flash.setText("Prix flash");
          flash.setComponentPopupMenu(BTD4);
          flash.setName("flash");
          flash.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              flashActionPerformed(e);
            }
          });
          panel2.add(flash);
          flash.setBounds(735, 65, 100, 25);
          
          // ---- L1IN20 ----
          L1IN20.setModel(new DefaultComboBoxModel(new String[] { "Article", "Quantitative" }));
          L1IN20.setName("L1IN20");
          L1IN20.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              L1IN20ItemStateChanged(e);
            }
          });
          panel2.add(L1IN20);
          L1IN20.setBounds(25, 75, 110, L1IN20.getPreferredSize().height);
          
          // ---- cnvchantier ----
          cnvchantier.setText("@WARTT@");
          cnvchantier.setOpaque(false);
          cnvchantier.setName("cnvchantier");
          panel2.add(cnvchantier);
          cnvchantier.setBounds(135, 75, 186, cnvchantier.getPreferredSize().height);
          
          // ---- OBJ_146 ----
          OBJ_146.setText("Type de gratuit");
          OBJ_146.setName("OBJ_146");
          panel2.add(OBJ_146);
          OBJ_146.setBounds(350, 76, 90, 22);
          
          // ---- L1IN18 ----
          L1IN18.setModel(
              new DefaultComboBoxModel(new String[] { "Prix garanti", "Prix non garanti", "Ancien prix", "Prix garanti/devis" }));
          L1IN18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1IN18.setName("L1IN18");
          panel2.add(L1IN18);
          L1IN18.setBounds(540, 73, 190, 29);
          
          // ---- L1IN2 ----
          L1IN2.setComponentPopupMenu(null);
          L1IN2.setText("@L1IN2@");
          L1IN2.setName("L1IN2");
          panel2.add(L1IN2);
          L1IN2.setBounds(440, 75, 24, L1IN2.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(15, 85, 850, 115);
        
        // ======== p_marge ========
        {
          p_marge.setBorder(new TitledBorder("Marge"));
          p_marge.setOpaque(false);
          p_marge.setName("p_marge");
          p_marge.setLayout(null);
          
          // ---- label13 ----
          label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
          label13.setText("Marge r\u00e9elle");
          label13.setName("label13");
          p_marge.add(label13);
          label13.setBounds(25, 35, 85, 20);
          
          // ---- label14 ----
          label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
          label14.setText("Marge th\u00e9orique");
          label14.setName("label14");
          p_marge.add(label14);
          label14.setBounds(25, 65, 100, 20);
          
          // ---- label16 ----
          label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
          label16.setText("Action commerciale");
          label16.setName("label16");
          p_marge.add(label16);
          label16.setBounds(25, 95, 120, 20);
          
          // ---- WMMR ----
          WMMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMR.setText("@WMMR@");
          WMMR.setName("WMMR");
          p_marge.add(WMMR);
          WMMR.setBounds(150, 35, 90, WMMR.getPreferredSize().height);
          
          // ---- WMMT ----
          WMMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMT.setText("@WMMT@");
          WMMT.setName("WMMT");
          p_marge.add(WMMT);
          WMMT.setBounds(150, 65, 90, WMMT.getPreferredSize().height);
          
          // ---- WMAC ----
          WMAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WMAC.setText("@WMAC@");
          WMAC.setName("WMAC");
          p_marge.add(WMAC);
          WMAC.setBounds(150, 95, 90, WMAC.getPreferredSize().height);
          
          // ---- label17 ----
          label17.setText("% Chiffre d'affaire r\u00e9el");
          label17.setName("label17");
          p_marge.add(label17);
          label17.setBounds(260, 35, 135, 20);
          
          // ---- label18 ----
          label18.setText("% Chiffre d'affaire r\u00e9el");
          label18.setName("label18");
          p_marge.add(label18);
          label18.setBounds(260, 65, 135, 20);
          
          // ---- label19 ----
          label19.setText("% Chiffre d'affaire th\u00e9orique");
          label19.setName("label19");
          p_marge.add(label19);
          label19.setBounds(260, 95, 160, 20);
          
          // ---- WPMR ----
          WPMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMR.setText("@WPMR@");
          WPMR.setName("WPMR");
          p_marge.add(WPMR);
          WPMR.setBounds(420, 35, 70, WPMR.getPreferredSize().height);
          
          // ---- WPMT ----
          WPMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMT.setText("@WPMT@");
          WPMT.setName("WPMT");
          p_marge.add(WPMT);
          WPMT.setBounds(420, 65, 70, WPMT.getPreferredSize().height);
          
          // ---- WPAC ----
          WPAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPAC.setText("@WPAC@");
          WPAC.setName("WPAC");
          p_marge.add(WPAC);
          WPAC.setBounds(420, 95, 70, WPAC.getPreferredSize().height);
          
          // ---- label20 ----
          label20.setText("Prix de revient");
          label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
          label20.setName("label20");
          p_marge.add(label20);
          label20.setBounds(510, 35, 110, 20);
          
          // ---- label21 ----
          label21.setText("C.Affaire r\u00e9el");
          label21.setName("label21");
          p_marge.add(label21);
          label21.setBounds(510, 65, 120, 20);
          
          // ---- label22 ----
          label22.setText("C.Affaire th\u00e9orique");
          label22.setName("label22");
          p_marge.add(label22);
          label22.setBounds(510, 95, 150, 20);
          
          // ---- WTPRL ----
          WTPRL.setHorizontalAlignment(SwingConstants.RIGHT);
          WTPRL.setText("@WTPRL@");
          WTPRL.setName("WTPRL");
          p_marge.add(WTPRL);
          WTPRL.setBounds(660, 35, 75, WTPRL.getPreferredSize().height);
          
          // ---- L1MHT ----
          L1MHT.setHorizontalAlignment(SwingConstants.RIGHT);
          L1MHT.setText("@L1MHT@");
          L1MHT.setName("L1MHT");
          p_marge.add(L1MHT);
          L1MHT.setBounds(660, 65, 75, L1MHT.getPreferredSize().height);
          
          // ---- WTPVL ----
          WTPVL.setHorizontalAlignment(SwingConstants.RIGHT);
          WTPVL.setText("@WTPVL@");
          WTPVL.setName("WTPVL");
          p_marge.add(WTPVL);
          WTPVL.setBounds(660, 95, 75, WTPVL.getPreferredSize().height);
          
          // ---- marge ----
          marge.setName("marge");
          p_marge.add(marge);
          marge.setBounds(115, 30, 30, 30);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_marge.getComponentCount(); i++) {
              Rectangle bounds = p_marge.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_marge.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_marge.setMinimumSize(preferredSize);
            p_marge.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_marge);
        p_marge.setBounds(15, 205, 850, 150);
        
        // ======== panel16 ========
        {
          panel16.setBorder(new TitledBorder("Options d'\u00e9dition"));
          panel16.setOpaque(false);
          panel16.setName("panel16");
          panel16.setLayout(null);
          
          // ---- L1TE1 ----
          L1TE1.setText("sans code article");
          L1TE1.setToolTipText("Soumis \u00e0 la taxe additionnelle");
          L1TE1.setComponentPopupMenu(null);
          L1TE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE1.setName("L1TE1");
          panel16.add(L1TE1);
          L1TE1.setBounds(15, 35, 140, 20);
          
          // ---- L1TE2 ----
          L1TE2.setText("sans libell\u00e9");
          L1TE2.setToolTipText("Soumis \u00e0 la taxe parafiscale");
          L1TE2.setComponentPopupMenu(null);
          L1TE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE2.setName("L1TE2");
          panel16.add(L1TE2);
          L1TE2.setBounds(180, 35, 140, 20);
          
          // ---- L1TE3 ----
          L1TE3.setText("Sans prix de vente");
          L1TE3.setToolTipText("Soumis \u00e0 la taxe additionnelle");
          L1TE3.setComponentPopupMenu(null);
          L1TE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE3.setName("L1TE3");
          panel16.add(L1TE3);
          L1TE3.setBounds(345, 35, 140, 20);
        }
        p_contenu.add(panel16);
        panel16.setBounds(20, 355, 850, 78);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 32));
      barre_tete.setName("barre_tete");
      
      // ======== P_Infos ========
      {
        P_Infos.setPreferredSize(new Dimension(0, 30));
        P_Infos.setBorder(null);
        P_Infos.setMinimumSize(new Dimension(709, 0));
        P_Infos.setOpaque(false);
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);
        
        // ---- WNLI ----
        WNLI.setComponentPopupMenu(null);
        WNLI.setOpaque(false);
        WNLI.setText("@WNLI@");
        WNLI.setName("WNLI");
        P_Infos.add(WNLI);
        WNLI.setBounds(70, 2, 50, WNLI.getPreferredSize().height);
        
        // ---- WARTT ----
        WARTT.setComponentPopupMenu(BTD);
        WARTT.setOpaque(false);
        WARTT.setText("@WARTT@");
        WARTT.setName("WARTT");
        P_Infos.add(WARTT);
        WARTT.setBounds(225, 2, 207, WARTT.getPreferredSize().height);
        
        // ---- OBJ_87 ----
        OBJ_87.setText("N\u00b0 ligne");
        OBJ_87.setName("OBJ_87");
        P_Infos.add(OBJ_87);
        OBJ_87.setBounds(15, 4, 60, 20);
        
        // ---- OBJ_88 ----
        OBJ_88.setText("@WLCA@");
        OBJ_88.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_88.setName("OBJ_88");
        P_Infos.add(OBJ_88);
        OBJ_88.setBounds(140, 4, 71, 20);
        
        // ---- etat ----
        etat.setText("etat");
        etat.setHorizontalAlignment(SwingConstants.RIGHT);
        etat.setFont(etat.getFont().deriveFont(etat.getFont().getStyle() | Font.BOLD));
        etat.setForeground(new Color(204, 0, 0));
        etat.setMinimumSize(new Dimension(150, 16));
        etat.setPreferredSize(new Dimension(150, 16));
        etat.setName("etat");
        P_Infos.add(etat);
        etat.setBounds(800, 4, 155, 20);
        
        // ---- L1LIB1 ----
        L1LIB1.setComponentPopupMenu(null);
        L1LIB1.setFont(L1LIB1.getFont().deriveFont(L1LIB1.getFont().getStyle() | Font.BOLD));
        L1LIB1.setToolTipText("Libell\u00e9 article");
        L1LIB1.setName("L1LIB1");
        P_Infos.add(L1LIB1);
        L1LIB1.setBounds(440, 0, 290, L1LIB1.getPreferredSize().height);
        
        // ---- bt_ecran ----
        bt_ecran.setBorder(BorderFactory.createEmptyBorder());
        bt_ecran.setToolTipText("Passage \u00e0 l'affichage complet");
        bt_ecran.setPreferredSize(new Dimension(30, 30));
        bt_ecran.setName("bt_ecran");
        bt_ecran.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            bt_ecranMouseClicked(e);
          }
        });
        P_Infos.add(bt_ecran);
        bt_ecran.setBounds(new Rectangle(new Point(1010, 0), bt_ecran.getPreferredSize()));
        
        // ---- riBoutonDetail3 ----
        riBoutonDetail3.setToolTipText("Extensions de libell\u00e9s");
        riBoutonDetail3.setName("riBoutonDetail3");
        riBoutonDetail3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riBoutonDetail3ActionPerformed(e);
          }
        });
        P_Infos.add(riBoutonDetail3);
        riBoutonDetail3.setBounds(new Rectangle(new Point(735, 5), riBoutonDetail3.getPreferredSize()));
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(P_Infos);
    }
    add(barre_tete, BorderLayout.NORTH);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    
    // ======== FCT1 ========
    {
      FCT1.setName("FCT1");
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_22);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Convertisseur mon\u00e9taire");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_20);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_21);
    }
    
    // ======== riSousMenu3 ========
    {
      riSousMenu3.setName("riSousMenu3");
    }
    
    // ======== panel1 ========
    {
      panel1.setName("panel1");
      panel1.setLayout(null);
      
      // ---- label6 ----
      label6.setText("Mode");
      label6.setName("label6");
      panel1.add(label6);
      label6.setBounds(10, 10, 40, 19);
      
      // ---- WCOD ----
      WCOD.setComponentPopupMenu(null);
      WCOD.setOpaque(false);
      WCOD.setText("@WCOD@");
      WCOD.setName("WCOD");
      panel1.add(WCOD);
      WCOD.setBounds(55, 7, 20, WCOD.getPreferredSize().height);
      
      // ---- OBJ_100 ----
      OBJ_100.setText("Repr\u00e9sentant");
      OBJ_100.setName("OBJ_100");
      panel1.add(OBJ_100);
      OBJ_100.setBounds(80, 9, 84, 20);
      
      // ---- L1REP ----
      L1REP.setComponentPopupMenu(BTD);
      L1REP.setName("L1REP");
      panel1.add(L1REP);
      L1REP.setBounds(165, 5, 34, L1REP.getPreferredSize().height);
      
      // ---- OBJ_112 ----
      OBJ_112.setText("Nature");
      OBJ_112.setName("OBJ_112");
      panel1.add(OBJ_112);
      OBJ_112.setBounds(10, 37, 43, 20);
      
      // ---- L20NAT ----
      L20NAT.setComponentPopupMenu(BTD);
      L20NAT.setName("L20NAT");
      panel1.add(L20NAT);
      L20NAT.setBounds(60, 35, 60, L20NAT.getPreferredSize().height);
      
      // ---- OBJ_102 ----
      OBJ_102.setText("@TYPS@");
      OBJ_102.setName("OBJ_102");
      panel1.add(OBJ_102);
      OBJ_102.setBounds(155, 35, 39, OBJ_102.getPreferredSize().height);
      
      // ---- OBJ_103 ----
      OBJ_103.setText("@TYPM@");
      OBJ_103.setHorizontalAlignment(SwingConstants.RIGHT);
      OBJ_103.setName("OBJ_103");
      panel1.add(OBJ_103);
      OBJ_103.setBounds(10, 67, 42, OBJ_103.getPreferredSize().height);
      
      // ---- OBJ_129 ----
      OBJ_129.setText("Coefficient");
      OBJ_129.setName("OBJ_129");
      panel1.add(OBJ_129);
      OBJ_129.setBounds(60, 69, 70, 20);
      
      // ---- L1COEX ----
      L1COEX.setComponentPopupMenu(null);
      L1COEX.setToolTipText("Coefficient");
      L1COEX.setName("L1COEX");
      panel1.add(L1COEX);
      L1COEX.setBounds(130, 65, 66, L1COEX.getPreferredSize().height);
      
      { // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    
    // ---- WREM2 ----
    WREM2.setComponentPopupMenu(BTD);
    WREM2.setHorizontalAlignment(SwingConstants.RIGHT);
    WREM2.setName("WREM2");
    
    // ---- WREM3 ----
    WREM3.setComponentPopupMenu(BTD);
    WREM3.setHorizontalAlignment(SwingConstants.RIGHT);
    WREM3.setName("WREM3");
    
    // ---- riBoutonDetail1 ----
    riBoutonDetail1.setToolTipText("@L1UNV@");
    riBoutonDetail1.setName("riBoutonDetail1");
    riBoutonDetail1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riBoutonDetail1ActionPerformed(e);
      }
    });
    
    // ---- OBJ_109 ----
    OBJ_109.setText("Tarif L1PVBX");
    OBJ_109.setName("OBJ_109");
    
    // ---- L1UNV_ ----
    L1UNV_.setComponentPopupMenu(BTD);
    L1UNV_.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    L1UNV_.setName("L1UNV_");
    L1UNV_.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        L1UNVActionPerformed(e);
      }
    });
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix unit\u00e9");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD2.add(menuItem1);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_19);
    }
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- OBJ_23 ----
      OBJ_23.setText("Remises suppl\u00e9mentaires");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_23);
      
      // ---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_24);
    }
    
    // ======== BTD4 ========
    {
      BTD4.setName("BTD4");
      
      // ---- appliquer ----
      appliquer.setText("Appliquer");
      appliquer.setToolTipText("Appliquer le prix flash comme prix net de l'article");
      appliquer.setName("appliquer");
      appliquer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          appliquerActionPerformed(e);
        }
      });
      BTD4.add(appliquer);
    }
    
    // ======== BTD5 ========
    {
      BTD5.setName("BTD5");
      
      // ---- menuItem2 ----
      menuItem2.setText("Choix unit\u00e9");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD5.add(menuItem2);
      
      // ---- menuItem3 ----
      menuItem3.setText("Calculatrice");
      menuItem3.setName("menuItem3");
      menuItem3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem3ActionPerformed(e);
        }
      });
      BTD5.add(menuItem3);
      
      // ---- OBJ_25 ----
      OBJ_25.setText("Aide en ligne");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD5.add(OBJ_25);
    }
    
    // ======== riSousMenu1 ========
    {
      riSousMenu1.setName("riSousMenu1");
      
      // ---- riSousMenu_bt1 ----
      riSousMenu_bt1.setText("Affichage \u00e9tendu");
      riSousMenu_bt1.setToolTipText("Affichage \u00e9tendu");
      riSousMenu_bt1.setName("riSousMenu_bt1");
      riSousMenu_bt1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt1ActionPerformed(e);
        }
      });
      riSousMenu1.add(riSousMenu_bt1);
    }
    
    // ---- riSousMenu_bt3 ----
    riSousMenu_bt3.setText("Affaire");
    riSousMenu_bt3.setName("riSousMenu_bt3");
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel panel7;
  private XRiTextField L1QTEX;
  private JLabel OBJ_106;
  private RiZoneSortie L1UNV;
  private RiZoneSortie A1UNL;
  private RiZoneSortie L1CNDX;
  private JLabel label15;
  private JLabel OBJ_136;
  private XRiTextField L1CPL;
  private JPanel p_stock;
  private SNBoutonDetail riBoutonDetailListe1;
  private JPanel p_s3;
  private RiZoneSortie WSTKX_3;
  private JLabel OBJ_94;
  private JLabel OBJ_444;
  private RiZoneSortie WRESX_3;
  private RiZoneSortie WAFFX_3;
  private JLabel OBJ_96;
  private JLabel OBJ_97;
  private RiZoneSortie WDISX_3;
  private RiZoneSortie WATTX_3;
  private JLabel commande_88;
  private JLabel OBJ_605;
  private JLabel disp_11;
  private JLabel att_11;
  private JPanel p_s6;
  private RiZoneSortie WSTKX_6;
  private JLabel OBJ_95;
  private JLabel OBJ_494;
  private RiZoneSortie WAFFX_6;
  private JLabel label1;
  private JLabel OBJ_99;
  private RiZoneSortie WDISX_6;
  private JLabel label2;
  private RiZoneSortie WCNRX_6;
  private JLabel OBJ_101;
  private JLabel label3;
  private RiZoneSortie WSTTX_6;
  private JLabel label4;
  private JLabel OBJ_104;
  private RiZoneSortie WATTX_6;
  private JLabel label5;
  private JPanel p_s8;
  private RiZoneSortie WDISX_8;
  private JLabel disp_22;
  private RiZoneSortie WRESX_8;
  private JLabel commande_9;
  private RiZoneSortie WATTX_8;
  private JLabel att_12;
  private RiZoneSortie WAFFX_7;
  private JLabel att_13;
  private XRiTextField L1MAG;
  private JLabel OBJ_98;
  private JLabel OBJ_110;
  private RiZoneSortie OBJ_144;
  private JLabel label8;
  private SNBoutonLeger bt_GA;
  private JLabel label11;
  private XRiCheckBox WSER;
  private JLabel label9;
  private JPanel panel2;
  private XRiTextField L1PVNX;
  private JLabel OBJ_107;
  private JLabel label7;
  private XRiTextField WREM1;
  private SNBoutonDetail riBoutonDetail2;
  private JLabel label10;
  private RiZoneSortie WPVCX;
  private XRiTextField L1PVBX;
  private JLabel label12;
  private XRiSpinner UTAR;
  private SNBoutonDetail nego;
  private SNBoutonDetail flash;
  private XRiComboBox L1IN20;
  private RiZoneSortie cnvchantier;
  private JLabel OBJ_146;
  private XRiComboBox L1IN18;
  private RiZoneSortie L1IN2;
  private JPanel p_marge;
  private JLabel label13;
  private JLabel label14;
  private JLabel label16;
  private RiZoneSortie WMMR;
  private RiZoneSortie WMMT;
  private RiZoneSortie WMAC;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private RiZoneSortie WPMR;
  private RiZoneSortie WPMT;
  private RiZoneSortie WPAC;
  private JLabel label20;
  private JLabel label21;
  private JLabel label22;
  private RiZoneSortie WTPRL;
  private RiZoneSortie L1MHT;
  private RiZoneSortie WTPVL;
  private JLabel marge;
  private JPanel panel16;
  private XRiCheckBox L1TE1;
  private XRiCheckBox L1TE2;
  private XRiCheckBox L1TE3;
  private JMenuBar barre_tete;
  private JPanel P_Infos;
  private RiZoneSortie WNLI;
  private RiZoneSortie WARTT;
  private JLabel OBJ_87;
  private JLabel OBJ_88;
  private JLabel etat;
  private XRiTextField L1LIB1;
  private JLabel bt_ecran;
  private SNBoutonDetail riBoutonDetail3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private RiSousMenu riSousMenu3;
  private JPanel panel1;
  private JLabel label6;
  private RiZoneSortie WCOD;
  private JLabel OBJ_100;
  private XRiTextField L1REP;
  private JLabel OBJ_112;
  private XRiTextField L20NAT;
  private RiZoneSortie OBJ_102;
  private RiZoneSortie OBJ_103;
  private JLabel OBJ_129;
  private XRiTextField L1COEX;
  private XRiTextField WREM2;
  private XRiTextField WREM3;
  private SNBoutonDetail riBoutonDetail1;
  private JLabel OBJ_109;
  private SNBoutonDetail L1UNV_;
  private JPopupMenu BTD2;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_19;
  private JPopupMenu BTD3;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JPopupMenu BTD4;
  private JMenuItem appliquer;
  private JPopupMenu BTD5;
  private JMenuItem menuItem2;
  private JMenuItem menuItem3;
  private JMenuItem OBJ_25;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu_bt riSousMenu_bt3;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
