
package ri.serien.libecranrpg.vgvm.VGVM11CF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11CF_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM11CF_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("@(TITLE)@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    xTitledPanel2 = new JPanel();
    L6001 = new XRiTextField();
    L6002 = new XRiTextField();
    L6003 = new XRiTextField();
    L6004 = new XRiTextField();
    L6005 = new XRiTextField();
    L6006 = new XRiTextField();
    L6007 = new XRiTextField();
    L6008 = new XRiTextField();
    L6009 = new XRiTextField();
    L6010 = new XRiTextField();
    L6011 = new XRiTextField();
    L6012 = new XRiTextField();
    L6013 = new XRiTextField();
    L6014 = new XRiTextField();
    L6015 = new XRiTextField();
    L6016 = new XRiTextField();
    L6017 = new XRiTextField();
    L6018 = new XRiTextField();
    L6019 = new XRiTextField();
    L6020 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(820, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 80));
            menus_haut.setPreferredSize(new Dimension(160, 80));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Saisie 30/60 caract\u00e8res");
              riSousMenu_bt6.setToolTipText("Saisie 30/60 caract\u00e8res");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(720, 460));
        p_contenu.setName("p_contenu");

        //======== xTitledPanel2 ========
        {
          xTitledPanel2.setBorder(new TitledBorder("Saisie de commentaires sur Bon"));
          xTitledPanel2.setOpaque(false);
          xTitledPanel2.setName("xTitledPanel2");
          xTitledPanel2.setLayout(null);

          //---- L6001 ----
          L6001.setFont(L6001.getFont().deriveFont(L6001.getFont().getSize() - 1f));
          L6001.setName("L6001");
          xTitledPanel2.add(L6001);
          L6001.setBounds(25, 35, 570, 25);

          //---- L6002 ----
          L6002.setFont(L6002.getFont().deriveFont(L6002.getFont().getSize() - 1f));
          L6002.setName("L6002");
          xTitledPanel2.add(L6002);
          L6002.setBounds(25, 55, 570, 25);

          //---- L6003 ----
          L6003.setFont(L6003.getFont().deriveFont(L6003.getFont().getSize() - 1f));
          L6003.setName("L6003");
          xTitledPanel2.add(L6003);
          L6003.setBounds(25, 75, 570, 25);

          //---- L6004 ----
          L6004.setFont(L6004.getFont().deriveFont(L6004.getFont().getSize() - 1f));
          L6004.setName("L6004");
          xTitledPanel2.add(L6004);
          L6004.setBounds(25, 95, 570, 25);

          //---- L6005 ----
          L6005.setFont(L6005.getFont().deriveFont(L6005.getFont().getSize() - 1f));
          L6005.setName("L6005");
          xTitledPanel2.add(L6005);
          L6005.setBounds(25, 115, 570, 25);

          //---- L6006 ----
          L6006.setFont(L6006.getFont().deriveFont(L6006.getFont().getSize() - 1f));
          L6006.setName("L6006");
          xTitledPanel2.add(L6006);
          L6006.setBounds(25, 135, 570, 25);

          //---- L6007 ----
          L6007.setFont(L6007.getFont().deriveFont(L6007.getFont().getSize() - 1f));
          L6007.setName("L6007");
          xTitledPanel2.add(L6007);
          L6007.setBounds(25, 155, 570, 25);

          //---- L6008 ----
          L6008.setFont(L6008.getFont().deriveFont(L6008.getFont().getSize() - 1f));
          L6008.setName("L6008");
          xTitledPanel2.add(L6008);
          L6008.setBounds(25, 175, 570, 25);

          //---- L6009 ----
          L6009.setFont(L6009.getFont().deriveFont(L6009.getFont().getSize() - 1f));
          L6009.setName("L6009");
          xTitledPanel2.add(L6009);
          L6009.setBounds(25, 195, 570, 25);

          //---- L6010 ----
          L6010.setFont(L6010.getFont().deriveFont(L6010.getFont().getSize() - 1f));
          L6010.setName("L6010");
          xTitledPanel2.add(L6010);
          L6010.setBounds(25, 215, 570, 25);

          //---- L6011 ----
          L6011.setFont(L6011.getFont().deriveFont(L6011.getFont().getSize() - 1f));
          L6011.setName("L6011");
          xTitledPanel2.add(L6011);
          L6011.setBounds(25, 235, 570, 25);

          //---- L6012 ----
          L6012.setFont(L6012.getFont().deriveFont(L6012.getFont().getSize() - 1f));
          L6012.setName("L6012");
          xTitledPanel2.add(L6012);
          L6012.setBounds(25, 255, 570, 25);

          //---- L6013 ----
          L6013.setFont(L6013.getFont().deriveFont(L6013.getFont().getSize() - 1f));
          L6013.setName("L6013");
          xTitledPanel2.add(L6013);
          L6013.setBounds(25, 275, 570, 25);

          //---- L6014 ----
          L6014.setFont(L6014.getFont().deriveFont(L6014.getFont().getSize() - 1f));
          L6014.setName("L6014");
          xTitledPanel2.add(L6014);
          L6014.setBounds(25, 295, 570, 25);

          //---- L6015 ----
          L6015.setFont(L6015.getFont().deriveFont(L6015.getFont().getSize() - 1f));
          L6015.setName("L6015");
          xTitledPanel2.add(L6015);
          L6015.setBounds(25, 315, 570, 25);

          //---- L6016 ----
          L6016.setFont(L6016.getFont().deriveFont(L6016.getFont().getSize() - 1f));
          L6016.setName("L6016");
          xTitledPanel2.add(L6016);
          L6016.setBounds(25, 335, 570, 25);

          //---- L6017 ----
          L6017.setFont(L6017.getFont().deriveFont(L6017.getFont().getSize() - 1f));
          L6017.setName("L6017");
          xTitledPanel2.add(L6017);
          L6017.setBounds(25, 355, 570, 25);

          //---- L6018 ----
          L6018.setFont(L6018.getFont().deriveFont(L6018.getFont().getSize() - 1f));
          L6018.setName("L6018");
          xTitledPanel2.add(L6018);
          L6018.setBounds(25, 375, 570, 25);

          //---- L6019 ----
          L6019.setFont(L6019.getFont().deriveFont(L6019.getFont().getSize() - 1f));
          L6019.setName("L6019");
          xTitledPanel2.add(L6019);
          L6019.setBounds(25, 395, 570, 25);

          //---- L6020 ----
          L6020.setFont(L6020.getFont().deriveFont(L6020.getFont().getSize() - 1f));
          L6020.setName("L6020");
          xTitledPanel2.add(L6020);
          L6020.setBounds(25, 415, 570, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < xTitledPanel2.getComponentCount(); i++) {
              Rectangle bounds = xTitledPanel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = xTitledPanel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            xTitledPanel2.setMinimumSize(preferredSize);
            xTitledPanel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 620, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(20, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 462, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(27, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel xTitledPanel2;
  private XRiTextField L6001;
  private XRiTextField L6002;
  private XRiTextField L6003;
  private XRiTextField L6004;
  private XRiTextField L6005;
  private XRiTextField L6006;
  private XRiTextField L6007;
  private XRiTextField L6008;
  private XRiTextField L6009;
  private XRiTextField L6010;
  private XRiTextField L6011;
  private XRiTextField L6012;
  private XRiTextField L6013;
  private XRiTextField L6014;
  private XRiTextField L6015;
  private XRiTextField L6016;
  private XRiTextField L6017;
  private XRiTextField L6018;
  private XRiTextField L6019;
  private XRiTextField L6020;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
