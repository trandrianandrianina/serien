
package ri.serien.libecranrpg.vgvm.VGVM11CF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11CF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM11CF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    scrollPane1 = new JScrollPane();
    panel2 = new JPanel();
    LIB01 = new XRiTextField();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB6 = new XRiTextField();
    LIB7 = new XRiTextField();
    LIB8 = new XRiTextField();
    LIB9 = new XRiTextField();
    LIB10 = new XRiTextField();
    LIB11 = new XRiTextField();
    LIB12 = new XRiTextField();
    LIB13 = new XRiTextField();
    LIB14 = new XRiTextField();
    LIB15 = new XRiTextField();
    LIB16 = new XRiTextField();
    LIB17 = new XRiTextField();
    LIB18 = new XRiTextField();
    LIB19 = new XRiTextField();
    LIB20 = new XRiTextField();
    LIB21 = new XRiTextField();
    LIB22 = new XRiTextField();
    LIB23 = new XRiTextField();
    LIB24 = new XRiTextField();
    LIB25 = new XRiTextField();
    LIB26 = new XRiTextField();
    LIB27 = new XRiTextField();
    LIB28 = new XRiTextField();
    LIB29 = new XRiTextField();
    LIB30 = new XRiTextField();
    LIB31 = new XRiTextField();
    LIB32 = new XRiTextField();
    LIB33 = new XRiTextField();
    LIB34 = new XRiTextField();
    LIB35 = new XRiTextField();
    LIB36 = new XRiTextField();
    LIB37 = new XRiTextField();
    LIB38 = new XRiTextField();
    LIB39 = new XRiTextField();
    LIB40 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Saisie des commentaires sur bon");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 150));
            menus_haut.setPreferredSize(new Dimension(160, 150));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Saisie 30/60 caract\u00e8res");
              riSousMenu_bt6.setToolTipText("Saisie 30/60 caract\u00e8res");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(550, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Saisie des commentaires sur bon"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== scrollPane1 ========
            {
              scrollPane1.setPreferredSize(new Dimension(356, 450));
              scrollPane1.setMaximumSize(new Dimension(32767, 450));
              scrollPane1.setBackground(new Color(239, 239, 222));
              scrollPane1.setOpaque(true);
              scrollPane1.setName("scrollPane1");

              //======== panel2 ========
              {
                panel2.setBackground(new Color(239, 239, 222));
                panel2.setName("panel2");
                panel2.setLayout(null);

                //---- LIB01 ----
                LIB01.setName("LIB01");
                panel2.add(LIB01);
                LIB01.setBounds(20, 10, 330, 25);

                //---- LIB02 ----
                LIB02.setName("LIB02");
                panel2.add(LIB02);
                LIB02.setBounds(20, 30, 330, 25);

                //---- LIB03 ----
                LIB03.setName("LIB03");
                panel2.add(LIB03);
                LIB03.setBounds(20, 50, 330, 25);

                //---- LIB04 ----
                LIB04.setName("LIB04");
                panel2.add(LIB04);
                LIB04.setBounds(20, 70, 330, 25);

                //---- LIB05 ----
                LIB05.setName("LIB05");
                panel2.add(LIB05);
                LIB05.setBounds(20, 90, 330, 25);

                //---- LIB6 ----
                LIB6.setName("LIB6");
                panel2.add(LIB6);
                LIB6.setBounds(20, 110, 330, 25);

                //---- LIB7 ----
                LIB7.setName("LIB7");
                panel2.add(LIB7);
                LIB7.setBounds(20, 130, 330, 25);

                //---- LIB8 ----
                LIB8.setName("LIB8");
                panel2.add(LIB8);
                LIB8.setBounds(20, 150, 330, 25);

                //---- LIB9 ----
                LIB9.setName("LIB9");
                panel2.add(LIB9);
                LIB9.setBounds(20, 170, 330, 25);

                //---- LIB10 ----
                LIB10.setName("LIB10");
                panel2.add(LIB10);
                LIB10.setBounds(20, 190, 330, 25);

                //---- LIB11 ----
                LIB11.setName("LIB11");
                panel2.add(LIB11);
                LIB11.setBounds(20, 210, 330, 25);

                //---- LIB12 ----
                LIB12.setName("LIB12");
                panel2.add(LIB12);
                LIB12.setBounds(20, 230, 330, 25);

                //---- LIB13 ----
                LIB13.setName("LIB13");
                panel2.add(LIB13);
                LIB13.setBounds(20, 250, 330, 25);

                //---- LIB14 ----
                LIB14.setName("LIB14");
                panel2.add(LIB14);
                LIB14.setBounds(20, 270, 330, 25);

                //---- LIB15 ----
                LIB15.setName("LIB15");
                panel2.add(LIB15);
                LIB15.setBounds(20, 290, 330, 25);

                //---- LIB16 ----
                LIB16.setName("LIB16");
                panel2.add(LIB16);
                LIB16.setBounds(20, 310, 330, 25);

                //---- LIB17 ----
                LIB17.setName("LIB17");
                panel2.add(LIB17);
                LIB17.setBounds(20, 330, 330, 25);

                //---- LIB18 ----
                LIB18.setName("LIB18");
                panel2.add(LIB18);
                LIB18.setBounds(20, 350, 330, 25);

                //---- LIB19 ----
                LIB19.setName("LIB19");
                panel2.add(LIB19);
                LIB19.setBounds(20, 370, 330, 25);

                //---- LIB20 ----
                LIB20.setName("LIB20");
                panel2.add(LIB20);
                LIB20.setBounds(20, 390, 330, 25);

                //---- LIB21 ----
                LIB21.setName("LIB21");
                panel2.add(LIB21);
                LIB21.setBounds(20, 410, 330, 25);

                //---- LIB22 ----
                LIB22.setName("LIB22");
                panel2.add(LIB22);
                LIB22.setBounds(20, 430, 330, 25);

                //---- LIB23 ----
                LIB23.setName("LIB23");
                panel2.add(LIB23);
                LIB23.setBounds(20, 450, 330, 25);

                //---- LIB24 ----
                LIB24.setName("LIB24");
                panel2.add(LIB24);
                LIB24.setBounds(20, 470, 330, 25);

                //---- LIB25 ----
                LIB25.setName("LIB25");
                panel2.add(LIB25);
                LIB25.setBounds(20, 490, 330, 25);

                //---- LIB26 ----
                LIB26.setName("LIB26");
                panel2.add(LIB26);
                LIB26.setBounds(20, 510, 330, 25);

                //---- LIB27 ----
                LIB27.setName("LIB27");
                panel2.add(LIB27);
                LIB27.setBounds(20, 530, 330, 25);

                //---- LIB28 ----
                LIB28.setName("LIB28");
                panel2.add(LIB28);
                LIB28.setBounds(20, 550, 330, 25);

                //---- LIB29 ----
                LIB29.setName("LIB29");
                panel2.add(LIB29);
                LIB29.setBounds(20, 570, 330, 25);

                //---- LIB30 ----
                LIB30.setName("LIB30");
                panel2.add(LIB30);
                LIB30.setBounds(20, 590, 330, 25);

                //---- LIB31 ----
                LIB31.setName("LIB31");
                panel2.add(LIB31);
                LIB31.setBounds(20, 610, 330, 25);

                //---- LIB32 ----
                LIB32.setName("LIB32");
                panel2.add(LIB32);
                LIB32.setBounds(20, 630, 330, 25);

                //---- LIB33 ----
                LIB33.setName("LIB33");
                panel2.add(LIB33);
                LIB33.setBounds(20, 650, 330, 25);

                //---- LIB34 ----
                LIB34.setName("LIB34");
                panel2.add(LIB34);
                LIB34.setBounds(20, 670, 330, 25);

                //---- LIB35 ----
                LIB35.setName("LIB35");
                panel2.add(LIB35);
                LIB35.setBounds(20, 690, 330, 25);

                //---- LIB36 ----
                LIB36.setName("LIB36");
                panel2.add(LIB36);
                LIB36.setBounds(20, 710, 330, 25);

                //---- LIB37 ----
                LIB37.setName("LIB37");
                panel2.add(LIB37);
                LIB37.setBounds(20, 730, 330, 25);

                //---- LIB38 ----
                LIB38.setName("LIB38");
                panel2.add(LIB38);
                LIB38.setBounds(20, 750, 330, 25);

                //---- LIB39 ----
                LIB39.setName("LIB39");
                panel2.add(LIB39);
                LIB39.setBounds(20, 770, 330, 25);

                //---- LIB40 ----
                LIB40.setName("LIB40");
                panel2.add(LIB40);
                LIB40.setBounds(20, 790, 330, 25);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < panel2.getComponentCount(); i++) {
                    Rectangle bounds = panel2.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = panel2.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  panel2.setMinimumSize(preferredSize);
                  panel2.setPreferredSize(preferredSize);
                }
              }
              scrollPane1.setViewportView(panel2);
            }
            panel1.add(scrollPane1);
            scrollPane1.setBounds(45, 40, 390, 400);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 483, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 472, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane scrollPane1;
  private JPanel panel2;
  private XRiTextField LIB01;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB6;
  private XRiTextField LIB7;
  private XRiTextField LIB8;
  private XRiTextField LIB9;
  private XRiTextField LIB10;
  private XRiTextField LIB11;
  private XRiTextField LIB12;
  private XRiTextField LIB13;
  private XRiTextField LIB14;
  private XRiTextField LIB15;
  private XRiTextField LIB16;
  private XRiTextField LIB17;
  private XRiTextField LIB18;
  private XRiTextField LIB19;
  private XRiTextField LIB20;
  private XRiTextField LIB21;
  private XRiTextField LIB22;
  private XRiTextField LIB23;
  private XRiTextField LIB24;
  private XRiTextField LIB25;
  private XRiTextField LIB26;
  private XRiTextField LIB27;
  private XRiTextField LIB28;
  private XRiTextField LIB29;
  private XRiTextField LIB30;
  private XRiTextField LIB31;
  private XRiTextField LIB32;
  private XRiTextField LIB33;
  private XRiTextField LIB34;
  private XRiTextField LIB35;
  private XRiTextField LIB36;
  private XRiTextField LIB37;
  private XRiTextField LIB38;
  private XRiTextField LIB39;
  private XRiTextField LIB40;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
