
package ri.serien.libecranrpg.vgvm.VGVM033F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM033F_B1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String topErreur = "19";
  private final static String BOUTON_DEFINIR_CODEBARRE_PRINCIPAL = "Définir comme principal";
  private final static String BOUTON_SUPPRIMER_CODEBARRE = "Supprimer code barre";
  
  /**
   * Constructeur.
   */
  public VGVM033F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    setTitle("Codes Barres");
    
    // Ajout
    initDiverses();
    
    XRiBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    XRiBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    XRiBarreBouton.ajouterBouton(BOUTON_DEFINIR_CODEBARRE_PRINCIPAL, 'p', true);
    XRiBarreBouton.ajouterBouton(BOUTON_SUPPRIMER_CODEBARRE, 's', true);
    XRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Active les boutons liés au V01F
    XRiBarreBouton.rafraichir(lexique);
    
    // Active les boutons
    activerBouton();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Efface les sélections
    chkLigne02.setSelected(false);
    chkLigne02.setSelected(false);
    chkLigne03.setSelected(false);
    chkLigne04.setSelected(false);
    chkLigne05.setSelected(false);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Affichage des boutons.
   */
  private void activerBouton() {
    boolean actif = false;
    int numeroLigne = getNumeroLigneSelectionnee();
    if (numeroLigne > 0) {
      switch (numeroLigne) {
        case 1:
          if (GCD01.getText().trim().isEmpty() && LIB01.getText().trim().isEmpty()) {
            actif = false;
          }
          else {
            actif = true;
          }
          break;
        case 2:
          if (GCD02.getText().trim().isEmpty() && LIB02.getText().trim().isEmpty()) {
            actif = false;
          }
          else {
            actif = true;
          }
          break;
        case 3:
          if (GCD03.getText().trim().isEmpty() && LIB03.getText().trim().isEmpty()) {
            actif = false;
          }
          else {
            actif = true;
          }
          break;
        case 4:
          if (GCD04.getText().trim().isEmpty() && LIB04.getText().trim().isEmpty()) {
            actif = false;
          }
          else {
            actif = true;
          }
          break;
        case 5:
          if (GCD05.getText().trim().isEmpty() && LIB05.getText().trim().isEmpty()) {
            actif = false;
          }
          else {
            actif = true;
          }
          break;
        
        default:
          return;
      }
    }
    
    XRiBarreBouton.activerBouton(BOUTON_DEFINIR_CODEBARRE_PRINCIPAL, actif);
    XRiBarreBouton.activerBouton(BOUTON_SUPPRIMER_CODEBARRE, actif);
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    XRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12", true);
      }
      else if (pSNBouton.isBouton(BOUTON_DEFINIR_CODEBARRE_PRINCIPAL)) {
        effectuerAction("P");
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER_CODEBARRE)) {
        effectuerAction("S");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne la ligne sélectionnée, -1 sinon.
   */
  private int getNumeroLigneSelectionnee() {
    if (chkLigne01.isSelected()) {
      return 1;
    }
    else if (chkLigne02.isSelected()) {
      return 2;
    }
    else if (chkLigne03.isSelected()) {
      return 3;
    }
    else if (chkLigne04.isSelected()) {
      return 4;
    }
    else if (chkLigne05.isSelected()) {
      return 5;
    }
    
    return -1;
  }
  
  /**
   * Effectue l'action sur la ligne sélectionnée.
   */
  private void effectuerAction(String pOperation) {
    pOperation = Constantes.normerTexte(pOperation);
    if (pOperation == null) {
      return;
    }
    
    String nomVariable = null;
    switch (getNumeroLigneSelectionnee()) {
      case 1:
        if (GCD01.getText().trim().isEmpty() && LIB01.getText().trim().isEmpty()) {
          return;
        }
        nomVariable = "OPT01";
        break;
      case 2:
        if (GCD02.getText().trim().isEmpty() && LIB02.getText().trim().isEmpty()) {
          return;
        }
        nomVariable = "OPT02";
        break;
      case 3:
        if (GCD03.getText().trim().isEmpty() && LIB03.getText().trim().isEmpty()) {
          return;
        }
        nomVariable = "OPT03";
        break;
      case 4:
        if (GCD04.getText().trim().isEmpty() && LIB04.getText().trim().isEmpty()) {
          return;
        }
        nomVariable = "OPT04";
        break;
      case 5:
        if (GCD05.getText().trim().isEmpty() && LIB05.getText().trim().isEmpty()) {
          return;
        }
        nomVariable = "OPT05";
        break;
      default:
        return;
    }
    
    // Lancement de l'action
    lexique.HostFieldPutData(nomVariable, 0, pOperation);
    lexique.HostScreenSendKey(this, "ENTER");
    lexique.HostFieldPutData(nomVariable, 0, " ");
  }
  
  /**
   * Gère les checkbox.
   */
  private void gererSelection(int pLigne) {
    boolean actif = false;
    switch (pLigne) {
      case 1:
        chkLigne02.setSelected(actif);
        chkLigne03.setSelected(actif);
        chkLigne04.setSelected(actif);
        chkLigne05.setSelected(actif);
        break;
      case 2:
        chkLigne01.setSelected(actif);
        chkLigne03.setSelected(actif);
        chkLigne04.setSelected(actif);
        chkLigne05.setSelected(actif);
        break;
      case 3:
        chkLigne01.setSelected(actif);
        chkLigne02.setSelected(actif);
        chkLigne04.setSelected(actif);
        chkLigne05.setSelected(actif);
        break;
      case 4:
        chkLigne01.setSelected(actif);
        chkLigne02.setSelected(actif);
        chkLigne03.setSelected(actif);
        chkLigne05.setSelected(actif);
        break;
      case 5:
        chkLigne01.setSelected(actif);
        chkLigne02.setSelected(actif);
        chkLigne03.setSelected(actif);
        chkLigne04.setSelected(actif);
        break;
    }
    activerBouton();
  }
  
  private void chkLigne01ActionPerformed(ActionEvent e) {
    try {
      gererSelection(1);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkLigne02ActionPerformed(ActionEvent e) {
    try {
      gererSelection(2);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkLigne03ActionPerformed(ActionEvent e) {
    try {
      gererSelection(3);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkLigne04ActionPerformed(ActionEvent e) {
    try {
      gererSelection(4);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkLigne05ActionPerformed(ActionEvent e) {
    try {
      gererSelection(5);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    lbListeCodeBarre = new SNLabelTitre();
    pnlListeCodeBarre = new SNPanel();
    lbTitreSelection = new JLabel();
    lbTitreCodeBarre = new JLabel();
    lbTitreLibelle = new JLabel();
    chkLigne01 = new JCheckBox();
    GCD01 = new XRiTextField();
    LIB01 = new XRiTextField();
    chkLigne02 = new JCheckBox();
    GCD02 = new XRiTextField();
    LIB02 = new XRiTextField();
    chkLigne03 = new JCheckBox();
    GCD03 = new XRiTextField();
    LIB03 = new XRiTextField();
    chkLigne04 = new JCheckBox();
    GCD04 = new XRiTextField();
    LIB04 = new XRiTextField();
    chkLigne05 = new JCheckBox();
    GCD05 = new XRiTextField();
    LIB05 = new XRiTextField();
    XRiBarreBouton = new XRiBarreBouton();

    //======== this ========
    setMinimumSize(new Dimension(650, 340));
    setPreferredSize(new Dimension(650, 340));
    setMaximumSize(new Dimension(650, 340));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());

      //======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

        //---- lbListeCodeBarre ----
        lbListeCodeBarre.setText("Liste des codes barres");
        lbListeCodeBarre.setName("lbListeCodeBarre");
        pnlContenu.add(lbListeCodeBarre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlListeCodeBarre ========
        {
          pnlListeCodeBarre.setName("pnlListeCodeBarre");
          pnlListeCodeBarre.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlListeCodeBarre.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlListeCodeBarre.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
          ((GridBagLayout)pnlListeCodeBarre.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlListeCodeBarre.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbTitreSelection ----
          lbTitreSelection.setText("S\u00e9lection");
          lbTitreSelection.setFont(lbTitreSelection.getFont().deriveFont(lbTitreSelection.getFont().getSize() + 2f));
          lbTitreSelection.setPreferredSize(new Dimension(70, 30));
          lbTitreSelection.setName("lbTitreSelection");
          pnlListeCodeBarre.add(lbTitreSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbTitreCodeBarre ----
          lbTitreCodeBarre.setText("Code barres");
          lbTitreCodeBarre.setFont(lbTitreCodeBarre.getFont().deriveFont(lbTitreCodeBarre.getFont().getSize() + 2f));
          lbTitreCodeBarre.setPreferredSize(new Dimension(150, 30));
          lbTitreCodeBarre.setName("lbTitreCodeBarre");
          pnlListeCodeBarre.add(lbTitreCodeBarre, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- lbTitreLibelle ----
          lbTitreLibelle.setText("Libell\u00e9");
          lbTitreLibelle.setFont(lbTitreLibelle.getFont().deriveFont(lbTitreLibelle.getFont().getSize() + 2f));
          lbTitreLibelle.setPreferredSize(new Dimension(200, 30));
          lbTitreLibelle.setName("lbTitreLibelle");
          pnlListeCodeBarre.add(lbTitreLibelle, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- chkLigne01 ----
          chkLigne01.setHorizontalAlignment(SwingConstants.CENTER);
          chkLigne01.setName("chkLigne01");
          chkLigne01.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkLigne01ActionPerformed(e);
            }
          });
          pnlListeCodeBarre.add(chkLigne01, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- GCD01 ----
          GCD01.setPreferredSize(new Dimension(150, 30));
          GCD01.setMinimumSize(new Dimension(150, 30));
          GCD01.setMaximumSize(new Dimension(150, 30));
          GCD01.setFont(GCD01.getFont().deriveFont(GCD01.getFont().getSize() + 2f));
          GCD01.setName("GCD01");
          pnlListeCodeBarre.add(GCD01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB01 ----
          LIB01.setPreferredSize(new Dimension(200, 30));
          LIB01.setMinimumSize(new Dimension(200, 30));
          LIB01.setMaximumSize(new Dimension(200, 30));
          LIB01.setFont(LIB01.getFont().deriveFont(LIB01.getFont().getSize() + 2f));
          LIB01.setName("LIB01");
          pnlListeCodeBarre.add(LIB01, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- chkLigne02 ----
          chkLigne02.setHorizontalAlignment(SwingConstants.CENTER);
          chkLigne02.setName("chkLigne02");
          chkLigne02.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkLigne02ActionPerformed(e);
            }
          });
          pnlListeCodeBarre.add(chkLigne02, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- GCD02 ----
          GCD02.setMaximumSize(new Dimension(150, 30));
          GCD02.setMinimumSize(new Dimension(150, 30));
          GCD02.setPreferredSize(new Dimension(150, 30));
          GCD02.setFont(GCD02.getFont().deriveFont(GCD02.getFont().getSize() + 2f));
          GCD02.setName("GCD02");
          pnlListeCodeBarre.add(GCD02, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB02 ----
          LIB02.setPreferredSize(new Dimension(200, 30));
          LIB02.setMinimumSize(new Dimension(200, 30));
          LIB02.setMaximumSize(new Dimension(200, 30));
          LIB02.setFont(LIB02.getFont().deriveFont(LIB02.getFont().getSize() + 2f));
          LIB02.setName("LIB02");
          pnlListeCodeBarre.add(LIB02, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- chkLigne03 ----
          chkLigne03.setHorizontalAlignment(SwingConstants.CENTER);
          chkLigne03.setName("chkLigne03");
          chkLigne03.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkLigne03ActionPerformed(e);
            }
          });
          pnlListeCodeBarre.add(chkLigne03, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- GCD03 ----
          GCD03.setMaximumSize(new Dimension(150, 30));
          GCD03.setMinimumSize(new Dimension(150, 30));
          GCD03.setPreferredSize(new Dimension(150, 30));
          GCD03.setFont(GCD03.getFont().deriveFont(GCD03.getFont().getSize() + 2f));
          GCD03.setName("GCD03");
          pnlListeCodeBarre.add(GCD03, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB03 ----
          LIB03.setPreferredSize(new Dimension(200, 30));
          LIB03.setMinimumSize(new Dimension(200, 30));
          LIB03.setMaximumSize(new Dimension(200, 30));
          LIB03.setFont(LIB03.getFont().deriveFont(LIB03.getFont().getSize() + 2f));
          LIB03.setName("LIB03");
          pnlListeCodeBarre.add(LIB03, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- chkLigne04 ----
          chkLigne04.setHorizontalAlignment(SwingConstants.CENTER);
          chkLigne04.setName("chkLigne04");
          chkLigne04.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkLigne04ActionPerformed(e);
            }
          });
          pnlListeCodeBarre.add(chkLigne04, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- GCD04 ----
          GCD04.setMaximumSize(new Dimension(150, 30));
          GCD04.setMinimumSize(new Dimension(150, 30));
          GCD04.setPreferredSize(new Dimension(150, 30));
          GCD04.setFont(GCD04.getFont().deriveFont(GCD04.getFont().getSize() + 2f));
          GCD04.setName("GCD04");
          pnlListeCodeBarre.add(GCD04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- LIB04 ----
          LIB04.setPreferredSize(new Dimension(200, 30));
          LIB04.setMinimumSize(new Dimension(200, 30));
          LIB04.setMaximumSize(new Dimension(200, 30));
          LIB04.setFont(LIB04.getFont().deriveFont(LIB04.getFont().getSize() + 2f));
          LIB04.setName("LIB04");
          pnlListeCodeBarre.add(LIB04, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- chkLigne05 ----
          chkLigne05.setHorizontalAlignment(SwingConstants.CENTER);
          chkLigne05.setName("chkLigne05");
          chkLigne05.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkLigne05ActionPerformed(e);
            }
          });
          pnlListeCodeBarre.add(chkLigne05, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- GCD05 ----
          GCD05.setMaximumSize(new Dimension(150, 30));
          GCD05.setMinimumSize(new Dimension(150, 30));
          GCD05.setPreferredSize(new Dimension(150, 30));
          GCD05.setFont(GCD05.getFont().deriveFont(GCD05.getFont().getSize() + 2f));
          GCD05.setName("GCD05");
          pnlListeCodeBarre.add(GCD05, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- LIB05 ----
          LIB05.setPreferredSize(new Dimension(200, 30));
          LIB05.setMinimumSize(new Dimension(200, 30));
          LIB05.setMaximumSize(new Dimension(200, 30));
          LIB05.setFont(LIB05.getFont().deriveFont(LIB05.getFont().getSize() + 2f));
          LIB05.setName("LIB05");
          pnlListeCodeBarre.add(LIB05, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlListeCodeBarre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);

      //---- XRiBarreBouton ----
      XRiBarreBouton.setName("XRiBarreBouton");
      pnlPrincipal.add(XRiBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbListeCodeBarre;
  private SNPanel pnlListeCodeBarre;
  private JLabel lbTitreSelection;
  private JLabel lbTitreCodeBarre;
  private JLabel lbTitreLibelle;
  private JCheckBox chkLigne01;
  private XRiTextField GCD01;
  private XRiTextField LIB01;
  private JCheckBox chkLigne02;
  private XRiTextField GCD02;
  private XRiTextField LIB02;
  private JCheckBox chkLigne03;
  private XRiTextField GCD03;
  private XRiTextField LIB03;
  private JCheckBox chkLigne04;
  private XRiTextField GCD04;
  private XRiTextField LIB04;
  private JCheckBox chkLigne05;
  private XRiTextField GCD05;
  private XRiTextField LIB05;
  private XRiBarreBouton XRiBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
