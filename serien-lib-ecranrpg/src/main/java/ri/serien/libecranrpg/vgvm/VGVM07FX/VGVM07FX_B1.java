
package ri.serien.libecranrpg.vgvm.VGVM07FX;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.sntypegratuit.SNTypeGratuit;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM07FX_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] T1TRA_Value = { "", "A", "T", "F", "S", "G", "*", "R", "L", "1", "2", "3", };
  private String[] T1TRA_Lib = { "", "Article", "Tarif", "Famille", "Sous famille", "Groupe", "Emboitée", "Ensemble", "Ligne",
      "Regroupement achat", "Fournisseur", "3", };
  private String[] T2TCD_Value = { "", "N", "B", "R", "K", "F", "+", "-", };
  private String[] T1TCD_Value = { "", "N", "B", "R", "K", "F", "+", "-", };
  private String[] WTAR_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "98", "99" };
  private int indexCombo1 = 0;
  private int indexCombo2 = 0;
  private boolean isEcotaxe = false;
  private static final String BOUTON_MODIFIER = "Modifier";
  private static final String BOUTON_CONSULTER = "Consulter";
  private static final String BOUTON_BLOC_NOTES = "Afficher le bloc-Notes";
  private static final String BOUTON_NEGOCIATION_ACHAT = "Négocier achat";
  private boolean isConsultation = true;
  
  public VGVM07FX_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    T2TCD.setValeurs(T2TCD_Value, null);
    T1TCD.setValeurs(T1TCD_Value, null);
    WTAR1.setValeurs(WTAR_Value, null);
    WTAR2.setValeurs(WTAR_Value, null);
    
    // Configuration des plage de validité
    snPeriodeValidite1.setDefilement(EnumDefilementPlageDate.SANS_DEFILEMENT);
    
    // Configuration de la barre de bouton
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'm', false);
    snBarreBouton.ajouterBouton(BOUTON_CONSULTER, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_BLOC_NOTES, 'b', true);
    snBarreBouton.ajouterBouton(BOUTON_NEGOCIATION_ACHAT, 'a', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      // Traitement des actions bouton
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1CNV@")).trim());
    CNV2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    T1REF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1REF@")).trim());
    RAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1RAT@")).trim());
    RAT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULIB@")).trim());
    tfEcoTaxe.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TEARRT@")).trim());
    tfMontant.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMTTEE@")).trim());
    tfNombre.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNBR@")).trim());
    tfReference1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1RTA@")).trim());
    tfReference2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULBTAR@")).trim());
    tfPrixRevient.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPRVA@")).trim());
    tfPrixVenteEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P11X@")).trim());
    tfPrixVenteEnCours1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P21X@")).trim());
    tfPrixVenteEnCours2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P31X@")).trim());
    tfPrixVenteEnCours3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P41X@")).trim());
    tfPrixVenteEnCours4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P51X@")).trim());
    tfPrixVenteEnCours5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P61X@")).trim());
    tfCoefEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K21X@")).trim());
    tfCoefEnCours1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K31X@")).trim());
    tfCoefEnCours2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K41X@")).trim());
    tfCoefEnCours3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K51X@")).trim());
    tfCoefEnCours4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K61X@")).trim());
    tfPrixVente.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P12X@")).trim());
    tfPrixVente1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P22X@")).trim());
    tfPrixVente2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P32X@")).trim());
    tfPrixVente3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P42X@")).trim());
    tfPrixVente4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P52X@")).trim());
    tfPrixVente5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1P62X@")).trim());
    tfCoef.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K22X@")).trim());
    tfCoef1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K32X@")).trim());
    tfCoef2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K42X@")).trim());
    tfCoef3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K52X@")).trim());
    tfCoef4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1K62X@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isConsultation = (lexique.getMode() == Lexical.MODE_CONSULTATION);
    isEcotaxe = lexique.isTrue("(71) AND (73) AND (N41)");
    
    pnlEcoTaxe.setVisible(isEcotaxe);
    pnlValeurTTC1.setVisible(lexique.isTrue("69")
        & (T1TCD.getSelectedItem().toString().contains("Prix") | T1TCD.getSelectedItem().toString().contains("valeur")));
    pnlValeurTTC2.setVisible(lexique.isTrue("69")
        & (T2TCD.getSelectedItem().toString().contains("Prix") | T2TCD.getSelectedItem().toString().contains("valeur")));
    
    pnlValeurHT1.setVisible(!lexique.isTrue("69")
        & (T1TCD.getSelectedItem().toString().contains("Prix") | T1TCD.getSelectedItem().toString().contains("valeur")));
    pnlValeurHT2.setVisible(!lexique.isTrue("69")
        & (T2TCD.getSelectedItem().toString().contains("Prix") | T2TCD.getSelectedItem().toString().contains("valeur")));
    
    btEcotaxe.setEnabled(lexique.getMode() != Lexical.MODE_CONSULTATION);
    lbUnite.setVisible(lexique.isPresent("WUNV"));
    pnlTarif.setVisible(lexique.isTrue("73"));
    
    if (lexique.isTrue("69")) {
      pnlTarif.setTitre("Tarif T.T.C");
    }
    lbPrixDeRevientStandard.setVisible(lexique.isTrue("72"));
    tfPrixRevient.setVisible(lexique.isTrue("72"));
    btNegoAchat.setVisible(lexique.isTrue("72"));
    lbNegoAchat.setVisible(lexique.isTrue("72"));
    // titre panel si négociation achat
    if (lexique.isTrue("N35")) {
      pnlCondition1.setTitre("Condition 1");
      pnlCondition2.setTitre("Condition 2");
    }
    else {
      pnlCondition1.setTitre("Condition 1 (une négociation d'achat existe pour cette condition)");
      pnlCondition2.setTitre("Condition 2 (une négociation d'achat existe pour cette condition)");
    }
    if (!lexique.isTrue("41")) {
      lbCN.setText("Client");
    }
    lbAu.setVisible(lexique.isTrue("N42"));
    pnlDateTarif.setVisible(lexique.isTrue("N42"));
    occulte1.setSelected(lexique.HostFieldGetData("T1COE").contains("-"));
    occulte2.setSelected(lexique.HostFieldGetData("T2COE").contains("-"));
    indexCombo1 = T1TCD.getSelectedIndex();
    indexCombo2 = T2TCD.getSelectedIndex();
    
    WTAR1.removeAllItems();
    WTAR1.addItem("Tous");
    WTAR1.addItem("colonne 1 = " + lexique.HostFieldGetData("A1P11X"));
    WTAR1.addItem("colonne 2 = " + lexique.HostFieldGetData("A1P21X"));
    WTAR1.addItem("colonne 3 = " + lexique.HostFieldGetData("A1P31X"));
    WTAR1.addItem("colonne 4 = " + lexique.HostFieldGetData("A1P41X"));
    WTAR1.addItem("colonne 5 = " + lexique.HostFieldGetData("A1P51X"));
    WTAR1.addItem("colonne 6 = " + lexique.HostFieldGetData("A1P61X"));
    WTAR1.addItem("colonne 7");
    WTAR1.addItem("colonne 8");
    WTAR1.addItem("colonne 9");
    WTAR1.addItem("colonne 10");
    WTAR1.addItem("PUMP");
    WTAR1.addItem("prix de revient");
    
    WTAR2.removeAllItems();
    WTAR2.addItem("Tous");
    WTAR2.addItem("colonne 1 = " + lexique.HostFieldGetData("A1P11X"));
    WTAR2.addItem("colonne 2 = " + lexique.HostFieldGetData("A1P21X"));
    WTAR2.addItem("colonne 3 = " + lexique.HostFieldGetData("A1P31X"));
    WTAR2.addItem("colonne 4 = " + lexique.HostFieldGetData("A1P41X"));
    WTAR2.addItem("colonne 5 = " + lexique.HostFieldGetData("A1P51X"));
    WTAR2.addItem("colonne 6 = " + lexique.HostFieldGetData("A1P61X"));
    WTAR2.addItem("colonne 7");
    WTAR2.addItem("colonne 8");
    WTAR2.addItem("colonne 9");
    WTAR2.addItem("colonne 10");
    WTAR2.addItem("PUMP");
    WTAR2.addItem("prix de revient");
    
    WTAR1.setSelectedIndex(getIndice("WTAR1", WTAR_Value));
    WTAR2.setSelectedIndex(getIndice("WTAR2", WTAR_Value));
    
    // En emboitage
    boolean isT1traEgaleEtoile = lexique.HostFieldGetData("T1TRA").trim().equals("*");
    pnlCondition1.setVisible(!isT1traEgaleEtoile);
    pnlCondition2.setVisible(!isT1traEgaleEtoile);
    snPeriodeValidite1.setEnabled(!isConsultation);
    snPeriodeValidite2.setEnabled(!isConsultation);
    if (!isConsultation && isT1traEgaleEtoile) {
      snPeriodeValidite1.setEnabled(true);
      snPeriodeValidite2.setEnabled(true);
    }
    
    lbTypeRattachement.setText(T1TRA_Lib[getIndice("T1TRA", T1TRA_Value)]);
    
    pbPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    occulte1.setEnabled(!isConsultation);
    occulte2.setEnabled(!isConsultation);
    
    ckTypeGratuit1.setEnabled(!isConsultation);
    ckTypeGratuit2.setEnabled(!isConsultation);
    
    sNEtablissement.setSession(getSession());
    sNEtablissement.charger(false);
    sNEtablissement.setSelectionParChampRPG(lexique, "WETB");
    sNEtablissement.setEnabled(false);
    
    snTypeGratuit1.setSession(getSession());
    snTypeGratuit1.charger(false);
    snTypeGratuit1.setSelectionParChampRPG(lexique, "WTG1");
    snTypeGratuit1.setEnabled(!isConsultation);
    
    ckTypeGratuit1.setSelected(!lexique.HostFieldGetData("WTG1").trim().isEmpty());
    
    // Visibilité des composants du panel "Condition 1"
    setVisibliliteGratuitPeriode1(ckTypeGratuit1.isSelected());
    
    snTypeGratuit2.setSession(getSession());
    snTypeGratuit2.charger(false);
    snTypeGratuit2.setSelectionParChampRPG(lexique, "WTG2");
    snTypeGratuit2.setEnabled(!isConsultation);
    
    ckTypeGratuit2.setSelected(!lexique.HostFieldGetData("WTG2").trim().isEmpty());
    
    // Visibilité des composants du panel "Condition 2"
    setVisibliliteGratuitPeriode2(ckTypeGratuit2.isSelected());
    
    snDevise.setSession(getSession());
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "T1DEV");
    snDevise.setEnabled(!isConsultation);
    
    // Initialisation des plages de date
    snPeriodeValidite1.setDateDebutParChampRPG(lexique, "T1DTDX");
    snPeriodeValidite1.setDateFinParChampRPG(lexique, "T1DTFX");
    
    snPeriodeValidite2.setDateDebutParChampRPG(lexique, "T2DTDX");
    snPeriodeValidite2.setDateFinParChampRPG(lexique, "T2DTFX");
    
    traiterAffichageBouton();
  }
  
  @Override
  public void getData() {
    super.getData();
    sNEtablissement.renseignerChampRPG(lexique, "WETB");
    if (!ckTypeGratuit1.isSelected()) {
      snTypeGratuit1.setIdSelection(null);
    }
    snTypeGratuit1.renseignerChampRPG(lexique, "WTG1");
    if (!ckTypeGratuit2.isSelected()) {
      snTypeGratuit2.setIdSelection(null);
    }
    snTypeGratuit2.renseignerChampRPG(lexique, "WTG2");
    
    snDevise.renseignerChampRPG(lexique, "T1DEV");
    
    if (occulte1.isSelected()) {
      lexique.HostFieldPutData("T1COE", 0, lexique.HostFieldGetData("T1COE") + "-");
    }
    if (occulte2.isSelected()) {
      lexique.HostFieldPutData("T2COE", 0, lexique.HostFieldGetData("T2COE") + "-");
    }
    
    snPeriodeValidite1.renseignerChampRPGDebut(lexique, "T1DTDX");
    snPeriodeValidite1.renseignerChampRPGFin(lexique, "T1DTFX");
    
    snPeriodeValidite2.renseignerChampRPGDebut(lexique, "T2DTDX");
    snPeriodeValidite2.renseignerChampRPGFin(lexique, "T2DTFX");
  }
  
  /**
   * Actions des boutons
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_CONSULTER)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      else if (pSNBouton.isBouton(BOUTON_BLOC_NOTES)) {
        lexique.HostScreenSendKey(this, "F19");
      }
      else if (pSNBouton.isBouton(BOUTON_NEGOCIATION_ACHAT)) {
        lexique.HostCursorPut(6, 10);
        lexique.HostScreenSendKey(this, "F4");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void traiterAffichageBouton() {
    snBarreBouton.activerBouton(BOUTON_MODIFIER, isConsultation);
    snBarreBouton.activerBouton(BOUTON_CONSULTER, !isConsultation);
    snBarreBouton.activerBouton(BOUTON_NEGOCIATION_ACHAT, !isConsultation && lexique.isTrue("73"));
  }
  
  private void T1TCDActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CREATION) {
      if (T1TCD.getSelectedIndex() != indexCombo1) {
        T1VAL.setText("");
        T1FPR.setText("");
        T1REM1.setText("");
        T1REM2.setText("");
        T1REM3.setText("");
        T1REM4.setText("");
        T1REM5.setText("");
        T1REM6.setText("");
        T1COE.setText("");
      }
      T1VAL.setEnabled(T1TCD.getSelectedIndex() != 0);
      WTAR1.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1FPR.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1REM1.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1REM2.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1REM3.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1REM4.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1REM5.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1REM6.setEnabled(T1TCD.getSelectedIndex() != 0);
      T1COE.setEnabled(T1TCD.getSelectedIndex() != 0);
      snPeriodeValidite1.setEnabled(T1TCD.getSelectedIndex() != 0);
    }
    int indexType = T1TCD.getSelectedIndex();
    pnlValeurHT1.setVisible(!lexique.isTrue("69") & (indexType == 1 | indexType == 2 | indexType == 6 | indexType == 7));
    pnlValeurTTC1.setVisible(lexique.isTrue("69") & (indexType == 1 | indexType == 2 | indexType == 6 | indexType == 7));
    pnlTarif1.setVisible(indexType != 0 & !ckTypeGratuit1.isSelected());
    pnlRemise1.setVisible(indexType == 2 | indexType == 3);
    pnlCoefficient1.setVisible(indexType == 4);
    pnlFormule1.setVisible(indexType == 5);
  }
  
  private void T2TCDActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CREATION) {
      if (T2TCD.getSelectedIndex() != indexCombo2) {
        T2VAL.setText("");
        T2FPR.setText("");
        T2REM1.setText("");
        T2REM2.setText("");
        T2REM3.setText("");
        T2REM4.setText("");
        T2REM5.setText("");
        T2REM6.setText("");
        T2COE.setText("");
      }
      T2VAL.setEnabled(T2TCD.getSelectedIndex() != 0);
      WTAR2.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2FPR.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2REM1.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2REM2.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2REM3.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2REM4.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2REM5.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2REM6.setEnabled(T2TCD.getSelectedIndex() != 0);
      T2COE.setEnabled(T2TCD.getSelectedIndex() != 0);
      snPeriodeValidite2.setEnabled(T2TCD.getSelectedIndex() != 0);
    }
    int indexType = T2TCD.getSelectedIndex();
    pnlValeurHT2.setVisible(!lexique.isTrue("69") & (indexType == 1 | indexType == 2 | indexType == 6 | indexType == 7));
    pnlValeurTTC2.setVisible(lexique.isTrue("69") & (indexType == 1 | indexType == 2 | indexType == 6 | indexType == 7));
    pnlTarif2.setVisible(indexType != 0 & !ckTypeGratuit2.isSelected());
    pnlRemise2.setVisible(indexType == 2 | indexType == 3);
    pnlCoefficient2.setVisible(indexType == 4);
    pnlFormule2.setVisible(indexType == 5);
  }
  
  private void btEcotaxeActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 41);
    lexique.HostCursorPut("");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btNegoAchatActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(6, 10);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void ckTypeGratuit1ItemStateChanged(ItemEvent e) {
    try {
      boolean isGratuit1 = ckTypeGratuit1.isSelected();
      setVisibliliteGratuitPeriode1(isGratuit1);
      if (isGratuit1) {
        T1TCD.setSelectedIndex(1);
        T1VAL.setText("");
        snTypeGratuit1.setSession(getSession());
        snTypeGratuit1.charger(false);
      }
      else {
        T1TCD.setSelectedIndex(0);
        T1FPR.setText("");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckTypeGratuit2ItemStateChanged(ItemEvent e) {
    try {
      boolean isGratuit2 = ckTypeGratuit2.isSelected();
      setVisibliliteGratuitPeriode2(isGratuit2);
      if (isGratuit2) {
        T2TCD.setSelectedIndex(1);
        T2VAL.setText("");
      }
      else {
        T2TCD.setSelectedIndex(0);
        T2FPR.setText("");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void setVisibliliteGratuitPeriode1(Boolean pIsGratuit) {
    snTypeGratuit1.setVisible(pIsGratuit);
    pnlType1.setVisible(!pIsGratuit);
    pnlTarif1.setVisible(!pIsGratuit);
    pnlModificationType1.setVisible(!pIsGratuit);
    occulte1.setVisible(!pIsGratuit);
  }
  
  private void setVisibliliteGratuitPeriode2(Boolean pIsGratuit) {
    snTypeGratuit2.setVisible(pIsGratuit);
    pnlType2.setVisible(!pIsGratuit);
    pnlTarif2.setVisible(!pIsGratuit);
    pnlModificationType2.setVisible(!pIsGratuit);
    occulte2.setVisible(!pIsGratuit);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pbPresentation = new SNBandeauTitre();
    pnllPrincipal = new SNPanelContenu();
    pnlEntete = new SNPanel();
    OBJ_45 = new SNLabelChamp();
    sNEtablissement = new SNEtablissement();
    lbCN = new SNLabelUnite();
    CNV = new SNTexte();
    CNV2 = new SNTexte();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    lbReferenceCNV = new SNLabelChamp();
    T1REF = new XRiTextField();
    lbTypeRattachement = new SNLabelUnite();
    RAT = new SNTexte();
    RAT2 = new SNTexte();
    lbUnite = new SNLabelChamp();
    WUNV = new XRiTextField();
    pnlCondition1 = new SNPanelTitre();
    pnlType1 = new SNPanel();
    lbType1 = new SNLabelChamp();
    T1TCD = new XRiComboBox();
    pnlTarif1 = new SNPanel();
    lbTarif1 = new SNLabelChamp();
    WTAR1 = new XRiComboBox();
    pnlModificationType1 = new SNPanel();
    pnlValeurHT1 = new SNPanel();
    lbValeur1 = new SNLabelChamp();
    T1VAL = new XRiTextField();
    pnlFormule1 = new SNPanel();
    lbFormule1 = new SNLabelChamp();
    T1FPR = new XRiTextField();
    pnlValeurTTC1 = new SNPanel();
    lbValeur3 = new SNLabelChamp();
    WTTC1 = new XRiTextField();
    pnlCoefficient1 = new SNPanel();
    lbCoefficient1 = new SNLabelChamp();
    T1COE = new XRiTextField();
    occulte1 = new JCheckBox();
    pnlRemise1 = new SNPanel();
    lbRemises1 = new SNLabelChamp();
    T1REM1 = new XRiTextField();
    T1REM2 = new XRiTextField();
    T1REM3 = new XRiTextField();
    T1REM4 = new XRiTextField();
    T1REM5 = new XRiTextField();
    T1REM6 = new XRiTextField();
    pnlTypeGratuit1 = new SNPanel();
    ckTypeGratuit1 = new XRiCheckBox();
    snTypeGratuit1 = new SNTypeGratuit();
    pnlValidite1 = new SNPanel();
    lbPeriodeValidite1 = new SNLabelChamp();
    snPeriodeValidite1 = new SNPlageDate();
    pnlCondition2 = new SNPanelTitre();
    pnlType2 = new SNPanel();
    lbType2 = new SNLabelChamp();
    T2TCD = new XRiComboBox();
    pnlTarif2 = new SNPanel();
    lbTarif2 = new SNLabelChamp();
    WTAR2 = new XRiComboBox();
    pnlModificationType2 = new SNPanel();
    pnlValeurHT2 = new SNPanel();
    lbValeur2 = new SNLabelChamp();
    T2VAL = new XRiTextField();
    pnlValeurTTC2 = new SNPanel();
    lbValeur4 = new SNLabelChamp();
    WTTC2 = new XRiTextField();
    pnlCoefficient2 = new SNPanel();
    lbCoefficient2 = new SNLabelChamp();
    T2COE = new XRiTextField();
    occulte2 = new JCheckBox();
    pnlFormule2 = new SNPanel();
    lbFormule2 = new SNLabelChamp();
    T2FPR = new XRiTextField();
    pnlRemise2 = new SNPanel();
    lbRemises2 = new SNLabelChamp();
    T2REM1 = new XRiTextField();
    T2REM2 = new XRiTextField();
    T2REM3 = new XRiTextField();
    T2REM4 = new XRiTextField();
    T2REM5 = new XRiTextField();
    T2REM6 = new XRiTextField();
    pnlTypeGratuit2 = new SNPanel();
    ckTypeGratuit2 = new XRiCheckBox();
    snTypeGratuit2 = new SNTypeGratuit();
    pnlValidite2 = new SNPanel();
    lbPeriodeValidite2 = new SNLabelChamp();
    snPeriodeValidite2 = new SNPlageDate();
    pnlEcoTaxe = new SNPanel();
    lbEcoTaxe = new SNLabelChamp();
    btEcotaxe = new SNBoutonDetail();
    tfEcoTaxe = new SNTexte();
    lbNombre = new SNLabelChamp();
    tfNombre = new SNTexte();
    lbMontant = new SNLabelChamp();
    tfMontant = new SNTexte();
    pnlTarif = new SNPanelTitre();
    lbReference = new SNLabelChamp();
    lbEnCours = new SNLabelChamp();
    tfReference1 = new SNTexte();
    tfReference2 = new SNTexte();
    lbPrixDeRevientStandard = new SNLabelChamp();
    tfPrixRevient = new SNTexte();
    lbPrixVente1 = new SNLabelChamp();
    pnlPrixRevientCoeff = new SNPanel();
    tfPrixVenteEnCours = new SNTexte();
    tfPrixVenteEnCours1 = new SNTexte();
    tfPrixVenteEnCours2 = new SNTexte();
    tfPrixVenteEnCours3 = new SNTexte();
    tfPrixVenteEnCours4 = new SNTexte();
    tfPrixVenteEnCours5 = new SNTexte();
    tfCoefEnCours = new SNTexte();
    tfCoefEnCours1 = new SNTexte();
    tfCoefEnCours2 = new SNTexte();
    tfCoefEnCours3 = new SNTexte();
    tfCoefEnCours4 = new SNTexte();
    lbCoefficient3 = new SNLabelChamp();
    pnlDateTarif = new SNPanel();
    pnlTarifAu = new SNPanel();
    lbAu = new SNLabelChamp();
    W0DTDX = new XRiCalendrier();
    lbPrixVente2 = new SNLabelChamp();
    tfPrixVente = new SNTexte();
    tfPrixVente1 = new SNTexte();
    tfPrixVente2 = new SNTexte();
    tfPrixVente3 = new SNTexte();
    tfPrixVente4 = new SNTexte();
    tfPrixVente5 = new SNTexte();
    lbCoefficient4 = new SNLabelChamp();
    tfCoef = new SNTexte();
    tfCoef1 = new SNTexte();
    tfCoef2 = new SNTexte();
    tfCoef3 = new SNTexte();
    tfCoef4 = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    T1TRA2 = new JComboBox();
    lbNegoAchat = new SNLabelChamp();
    btNegoAchat = new SNBoutonDetail();
    
    // ======== this ========
    setPreferredSize(new Dimension(800, 600));
    setMinimumSize(new Dimension(800, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- pbPresentation ----
    pbPresentation.setText("Condition de ventes");
    pbPresentation.setName("pbPresentation");
    add(pbPresentation, BorderLayout.NORTH);
    
    // ======== pnllPrincipal ========
    {
      pnllPrincipal.setName("pnllPrincipal");
      pnllPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnllPrincipal.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnllPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnllPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnllPrincipal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlEntete ========
      {
        pnlEntete.setName("pnlEntete");
        pnlEntete.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEntete.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEntete.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlEntete.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlEntete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- OBJ_45 ----
        OBJ_45.setText("Etablissement");
        OBJ_45.setMinimumSize(new Dimension(100, 30));
        OBJ_45.setPreferredSize(new Dimension(100, 30));
        OBJ_45.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_45.setName("OBJ_45");
        pnlEntete.add(OBJ_45, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- sNEtablissement ----
        sNEtablissement.setEnabled(false);
        sNEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        sNEtablissement.setName("sNEtablissement");
        pnlEntete.add(sNEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbCN ----
        lbCN.setText("Code CN");
        lbCN.setMaximumSize(new Dimension(60, 30));
        lbCN.setMinimumSize(new Dimension(60, 30));
        lbCN.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCN.setPreferredSize(new Dimension(60, 30));
        lbCN.setName("lbCN");
        pnlEntete.add(lbCN, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CNV ----
        CNV.setOpaque(false);
        CNV.setText("@T1CNV@");
        CNV.setMinimumSize(new Dimension(200, 30));
        CNV.setPreferredSize(new Dimension(200, 30));
        CNV.setEditable(false);
        CNV.setEnabled(false);
        CNV.setName("CNV");
        pnlEntete.add(CNV, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- CNV2 ----
        CNV2.setOpaque(false);
        CNV2.setText("@WNOM@");
        CNV2.setMinimumSize(new Dimension(320, 30));
        CNV2.setPreferredSize(new Dimension(320, 30));
        CNV2.setEditable(false);
        CNV2.setEnabled(false);
        CNV2.setName("CNV2");
        pnlEntete.add(CNV2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDevise ----
        lbDevise.setText("Devise");
        lbDevise.setMinimumSize(new Dimension(45, 30));
        lbDevise.setPreferredSize(new Dimension(45, 30));
        lbDevise.setMaximumSize(new Dimension(45, 30));
        lbDevise.setName("lbDevise");
        pnlEntete.add(lbDevise, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDevise ----
        snDevise.setPreferredSize(new Dimension(200, 30));
        snDevise.setMaximumSize(new Dimension(200, 30));
        snDevise.setMinimumSize(new Dimension(200, 30));
        snDevise.setName("snDevise");
        pnlEntete.add(snDevise, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceCNV ----
        lbReferenceCNV.setText("R\u00e9f\u00e9rence");
        lbReferenceCNV.setName("lbReferenceCNV");
        pnlEntete.add(lbReferenceCNV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- T1REF ----
        T1REF.setMinimumSize(new Dimension(310, 30));
        T1REF.setMaximumSize(new Dimension(310, 30));
        T1REF.setPreferredSize(new Dimension(310, 30));
        T1REF.setText("@T1REF@");
        T1REF.setName("T1REF");
        pnlEntete.add(T1REF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTypeRattachement ----
        lbTypeRattachement.setComponentPopupMenu(null);
        lbTypeRattachement.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lbTypeRattachement.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTypeRattachement.setMaximumSize(new Dimension(60, 30));
        lbTypeRattachement.setMinimumSize(new Dimension(60, 30));
        lbTypeRattachement.setPreferredSize(new Dimension(60, 30));
        lbTypeRattachement.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTypeRattachement.setName("lbTypeRattachement");
        pnlEntete.add(lbTypeRattachement, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- RAT ----
        RAT.setOpaque(false);
        RAT.setText("@T1RAT@");
        RAT.setMinimumSize(new Dimension(200, 30));
        RAT.setPreferredSize(new Dimension(200, 30));
        RAT.setEditable(false);
        RAT.setEnabled(false);
        RAT.setName("RAT");
        pnlEntete.add(RAT, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- RAT2 ----
        RAT2.setOpaque(false);
        RAT2.setText("@ULIB@");
        RAT2.setMinimumSize(new Dimension(320, 30));
        RAT2.setPreferredSize(new Dimension(320, 30));
        RAT2.setEditable(false);
        RAT2.setEnabled(false);
        RAT2.setName("RAT2");
        pnlEntete.add(RAT2, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUnite ----
        lbUnite.setText("Unit\u00e9");
        lbUnite.setMinimumSize(new Dimension(40, 30));
        lbUnite.setPreferredSize(new Dimension(40, 30));
        lbUnite.setName("lbUnite");
        pnlEntete.add(lbUnite, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WUNV ----
        WUNV.setMinimumSize(new Dimension(36, 30));
        WUNV.setPreferredSize(new Dimension(36, 30));
        WUNV.setFont(new Font("sansserif", Font.PLAIN, 14));
        WUNV.setEditable(false);
        WUNV.setEnabled(false);
        WUNV.setName("WUNV");
        pnlEntete.add(WUNV, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllPrincipal.add(pnlEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCondition1 ========
      {
        pnlCondition1.setOpaque(false);
        pnlCondition1.setTitre("Condition 1");
        pnlCondition1.setPreferredSize(new Dimension(1875, 115));
        pnlCondition1.setMinimumSize(new Dimension(1886, 115));
        pnlCondition1.setName("pnlCondition1");
        pnlCondition1.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCondition1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlCondition1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCondition1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCondition1.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlType1 ========
        {
          pnlType1.setName("pnlType1");
          pnlType1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlType1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlType1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlType1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlType1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- lbType1 ----
          lbType1.setComponentPopupMenu(null);
          lbType1.setText("Type");
          lbType1.setMinimumSize(new Dimension(75, 30));
          lbType1.setPreferredSize(new Dimension(75, 30));
          lbType1.setName("lbType1");
          pnlType1.add(lbType1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- T1TCD ----
          T1TCD.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type ", "Prix net", "Prix de base", "Remise en %",
              "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
          T1TCD.setComponentPopupMenu(null);
          T1TCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T1TCD.setFont(new Font("sansserif", Font.PLAIN, 14));
          T1TCD.setPreferredSize(new Dimension(170, 30));
          T1TCD.setMinimumSize(new Dimension(170, 30));
          T1TCD.setName("T1TCD");
          T1TCD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              T1TCDActionPerformed(e);
            }
          });
          pnlType1.add(T1TCD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition1.add(pnlType1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlTarif1 ========
        {
          pnlTarif1.setName("pnlTarif1");
          pnlTarif1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarif1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarif1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTarif1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarif1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbTarif1 ----
          lbTarif1.setComponentPopupMenu(null);
          lbTarif1.setText("Tarif");
          lbTarif1.setMinimumSize(new Dimension(50, 30));
          lbTarif1.setMaximumSize(new Dimension(50, 30));
          lbTarif1.setPreferredSize(new Dimension(50, 30));
          lbTarif1.setName("lbTarif1");
          pnlTarif1.add(lbTarif1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WTAR1 ----
          WTAR1.setComponentPopupMenu(null);
          WTAR1.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTAR1.setPreferredSize(new Dimension(180, 30));
          WTAR1.setMinimumSize(new Dimension(188, 30));
          WTAR1.setName("WTAR1");
          pnlTarif1.add(WTAR1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition1.add(pnlTarif1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlModificationType1 ========
        {
          pnlModificationType1.setName("pnlModificationType1");
          pnlModificationType1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlModificationType1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlModificationType1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlModificationType1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlModificationType1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlValeurHT1 ========
          {
            pnlValeurHT1.setName("pnlValeurHT1");
            pnlValeurHT1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlValeurHT1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlValeurHT1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlValeurHT1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlValeurHT1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbValeur1 ----
            lbValeur1.setComponentPopupMenu(null);
            lbValeur1.setText("Valeur H.T.");
            lbValeur1.setPreferredSize(new Dimension(100, 30));
            lbValeur1.setMinimumSize(new Dimension(100, 30));
            lbValeur1.setName("lbValeur1");
            pnlValeurHT1.add(lbValeur1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1VAL ----
            T1VAL.setComponentPopupMenu(null);
            T1VAL.setPreferredSize(new Dimension(100, 30));
            T1VAL.setMinimumSize(new Dimension(100, 30));
            T1VAL.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1VAL.setName("T1VAL");
            pnlValeurHT1.add(T1VAL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType1.add(pnlValeurHT1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlFormule1 ========
          {
            pnlFormule1.setName("pnlFormule1");
            pnlFormule1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFormule1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFormule1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlFormule1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFormule1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbFormule1 ----
            lbFormule1.setComponentPopupMenu(null);
            lbFormule1.setText("Formule");
            lbFormule1.setMinimumSize(new Dimension(100, 19));
            lbFormule1.setPreferredSize(new Dimension(100, 19));
            lbFormule1.setName("lbFormule1");
            pnlFormule1.add(lbFormule1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1FPR ----
            T1FPR.setComponentPopupMenu(null);
            T1FPR.setMinimumSize(new Dimension(68, 28));
            T1FPR.setPreferredSize(new Dimension(65, 30));
            T1FPR.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1FPR.setName("T1FPR");
            pnlFormule1.add(T1FPR, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType1.add(pnlFormule1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlValeurTTC1 ========
          {
            pnlValeurTTC1.setName("pnlValeurTTC1");
            pnlValeurTTC1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlValeurTTC1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlValeurTTC1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlValeurTTC1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlValeurTTC1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbValeur3 ----
            lbValeur3.setComponentPopupMenu(null);
            lbValeur3.setText("Valeur T.T.C");
            lbValeur3.setMinimumSize(new Dimension(100, 19));
            lbValeur3.setPreferredSize(new Dimension(100, 19));
            lbValeur3.setMaximumSize(new Dimension(100, 30));
            lbValeur3.setName("lbValeur3");
            pnlValeurTTC1.add(lbValeur3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WTTC1 ----
            WTTC1.setComponentPopupMenu(null);
            WTTC1.setPreferredSize(new Dimension(100, 30));
            WTTC1.setMinimumSize(new Dimension(100, 30));
            WTTC1.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTTC1.setName("WTTC1");
            pnlValeurTTC1.add(WTTC1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType1.add(pnlValeurTTC1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlCoefficient1 ========
          {
            pnlCoefficient1.setName("pnlCoefficient1");
            pnlCoefficient1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCoefficient1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCoefficient1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCoefficient1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCoefficient1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCoefficient1 ----
            lbCoefficient1.setComponentPopupMenu(null);
            lbCoefficient1.setText("Coefficient");
            lbCoefficient1.setMinimumSize(new Dimension(100, 30));
            lbCoefficient1.setPreferredSize(new Dimension(100, 30));
            lbCoefficient1.setName("lbCoefficient1");
            pnlCoefficient1.add(lbCoefficient1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1COE ----
            T1COE.setComponentPopupMenu(null);
            T1COE.setMinimumSize(new Dimension(60, 30));
            T1COE.setPreferredSize(new Dimension(60, 30));
            T1COE.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1COE.setName("T1COE");
            pnlCoefficient1.add(T1COE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- occulte1 ----
            occulte1.setText("occulte");
            occulte1.setToolTipText("modifie le prix mais occulte le coefficient sur les \u00e9ditions");
            occulte1.setFont(new Font("sansserif", Font.PLAIN, 14));
            occulte1.setMinimumSize(new Dimension(67, 30));
            occulte1.setPreferredSize(new Dimension(67, 30));
            occulte1.setMaximumSize(new Dimension(67, 30));
            occulte1.setName("occulte1");
            pnlCoefficient1.add(occulte1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType1.add(pnlCoefficient1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlRemise1 ========
          {
            pnlRemise1.setName("pnlRemise1");
            pnlRemise1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemise1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemise1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemise1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemise1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbRemises1 ----
            lbRemises1.setComponentPopupMenu(null);
            lbRemises1.setText("Remises");
            lbRemises1.setMinimumSize(new Dimension(100, 19));
            lbRemises1.setPreferredSize(new Dimension(100, 19));
            lbRemises1.setMaximumSize(new Dimension(75, 30));
            lbRemises1.setName("lbRemises1");
            pnlRemise1.add(lbRemises1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1REM1 ----
            T1REM1.setComponentPopupMenu(null);
            T1REM1.setPreferredSize(new Dimension(50, 30));
            T1REM1.setMinimumSize(new Dimension(50, 30));
            T1REM1.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1REM1.setMaximumSize(new Dimension(50, 28));
            T1REM1.setName("T1REM1");
            pnlRemise1.add(T1REM1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1REM2 ----
            T1REM2.setComponentPopupMenu(null);
            T1REM2.setPreferredSize(new Dimension(50, 30));
            T1REM2.setMinimumSize(new Dimension(50, 30));
            T1REM2.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1REM2.setName("T1REM2");
            pnlRemise1.add(T1REM2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1REM3 ----
            T1REM3.setComponentPopupMenu(null);
            T1REM3.setMinimumSize(new Dimension(50, 30));
            T1REM3.setPreferredSize(new Dimension(50, 30));
            T1REM3.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1REM3.setName("T1REM3");
            pnlRemise1.add(T1REM3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1REM4 ----
            T1REM4.setComponentPopupMenu(null);
            T1REM4.setMinimumSize(new Dimension(50, 30));
            T1REM4.setPreferredSize(new Dimension(50, 30));
            T1REM4.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1REM4.setMaximumSize(new Dimension(50, 28));
            T1REM4.setName("T1REM4");
            pnlRemise1.add(T1REM4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1REM5 ----
            T1REM5.setComponentPopupMenu(null);
            T1REM5.setPreferredSize(new Dimension(50, 30));
            T1REM5.setMinimumSize(new Dimension(50, 30));
            T1REM5.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1REM5.setName("T1REM5");
            pnlRemise1.add(T1REM5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T1REM6 ----
            T1REM6.setComponentPopupMenu(null);
            T1REM6.setPreferredSize(new Dimension(50, 30));
            T1REM6.setMinimumSize(new Dimension(50, 30));
            T1REM6.setFont(new Font("sansserif", Font.PLAIN, 14));
            T1REM6.setName("T1REM6");
            pnlRemise1.add(T1REM6, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType1.add(pnlRemise1, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition1.add(pnlModificationType1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTypeGratuit1 ========
        {
          pnlTypeGratuit1.setName("pnlTypeGratuit1");
          pnlTypeGratuit1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTypeGratuit1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTypeGratuit1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTypeGratuit1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTypeGratuit1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- ckTypeGratuit1 ----
          ckTypeGratuit1.setText("Type gratuit");
          ckTypeGratuit1.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckTypeGratuit1.setMaximumSize(new Dimension(100, 30));
          ckTypeGratuit1.setMinimumSize(new Dimension(100, 30));
          ckTypeGratuit1.setPreferredSize(new Dimension(100, 30));
          ckTypeGratuit1.setName("ckTypeGratuit1");
          ckTypeGratuit1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              ckTypeGratuit1ItemStateChanged(e);
            }
          });
          pnlTypeGratuit1.add(ckTypeGratuit1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snTypeGratuit1 ----
          snTypeGratuit1.setPreferredSize(new Dimension(250, 30));
          snTypeGratuit1.setMinimumSize(new Dimension(250, 30));
          snTypeGratuit1.setMaximumSize(new Dimension(250, 30));
          snTypeGratuit1.setName("snTypeGratuit1");
          pnlTypeGratuit1.add(snTypeGratuit1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition1.add(pnlTypeGratuit1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlValidite1 ========
        {
          pnlValidite1.setMinimumSize(new Dimension(362, 30));
          pnlValidite1.setPreferredSize(new Dimension(362, 30));
          pnlValidite1.setName("pnlValidite1");
          pnlValidite1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlValidite1.getLayout()).columnWidths = new int[] { 129, 0, 0 };
          ((GridBagLayout) pnlValidite1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlValidite1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlValidite1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbPeriodeValidite1 ----
          lbPeriodeValidite1.setText("P\u00e9riode de validit\u00e9");
          lbPeriodeValidite1.setMinimumSize(new Dimension(140, 30));
          lbPeriodeValidite1.setMaximumSize(new Dimension(140, 30));
          lbPeriodeValidite1.setPreferredSize(new Dimension(140, 30));
          lbPeriodeValidite1.setName("lbPeriodeValidite1");
          pnlValidite1.add(lbPeriodeValidite1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snPeriodeValidite1 ----
          snPeriodeValidite1.setPreferredSize(new Dimension(350, 30));
          snPeriodeValidite1.setMinimumSize(new Dimension(350, 30));
          snPeriodeValidite1.setName("snPeriodeValidite1");
          pnlValidite1.add(snPeriodeValidite1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition1.add(pnlValidite1, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllPrincipal.add(pnlCondition1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCondition2 ========
      {
        pnlCondition2.setTitre("Condition 2");
        pnlCondition2.setPreferredSize(new Dimension(1875, 115));
        pnlCondition2.setMinimumSize(new Dimension(1886, 115));
        pnlCondition2.setName("pnlCondition2");
        pnlCondition2.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCondition2.getLayout()).columnWidths = new int[] { 0, 0, 584, 0 };
        ((GridBagLayout) pnlCondition2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCondition2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCondition2.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlType2 ========
        {
          pnlType2.setMaximumSize(new Dimension(250, 30));
          pnlType2.setName("pnlType2");
          pnlType2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlType2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlType2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlType2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlType2.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- lbType2 ----
          lbType2.setComponentPopupMenu(null);
          lbType2.setText("Type");
          lbType2.setPreferredSize(new Dimension(75, 30));
          lbType2.setMinimumSize(new Dimension(75, 30));
          lbType2.setName("lbType2");
          pnlType2.add(lbType2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- T2TCD ----
          T2TCD.setModel(new DefaultComboBoxModel(new String[] { "Choisissez un type", "Prix net", "Prix de base", "Remise en %",
              "Coefficient", "Formule (FP)", "Ajout en valeur", "Remise en valeur" }));
          T2TCD.setComponentPopupMenu(null);
          T2TCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          T2TCD.setFont(new Font("sansserif", Font.PLAIN, 14));
          T2TCD.setPreferredSize(new Dimension(170, 30));
          T2TCD.setMinimumSize(new Dimension(170, 30));
          T2TCD.setName("T2TCD");
          T2TCD.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              T2TCDActionPerformed(e);
            }
          });
          pnlType2.add(T2TCD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition2.add(pnlType2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlTarif2 ========
        {
          pnlTarif2.setMaximumSize(new Dimension(235, 30));
          pnlTarif2.setMinimumSize(new Dimension(235, 30));
          pnlTarif2.setName("pnlTarif2");
          pnlTarif2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarif2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarif2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTarif2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarif2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbTarif2 ----
          lbTarif2.setComponentPopupMenu(null);
          lbTarif2.setText("Tarif");
          lbTarif2.setPreferredSize(new Dimension(50, 30));
          lbTarif2.setMinimumSize(new Dimension(50, 30));
          lbTarif2.setName("lbTarif2");
          pnlTarif2.add(lbTarif2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WTAR2 ----
          WTAR2.setComponentPopupMenu(null);
          WTAR2.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTAR2.setMinimumSize(new Dimension(188, 30));
          WTAR2.setPreferredSize(new Dimension(180, 30));
          WTAR2.setName("WTAR2");
          pnlTarif2.add(WTAR2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition2.add(pnlTarif2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlModificationType2 ========
        {
          pnlModificationType2.setMinimumSize(new Dimension(0, 30));
          pnlModificationType2.setPreferredSize(new Dimension(275, 30));
          pnlModificationType2.setName("pnlModificationType2");
          pnlModificationType2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlModificationType2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlModificationType2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlModificationType2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlModificationType2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlValeurHT2 ========
          {
            pnlValeurHT2.setName("pnlValeurHT2");
            pnlValeurHT2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlValeurHT2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlValeurHT2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlValeurHT2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlValeurHT2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbValeur2 ----
            lbValeur2.setComponentPopupMenu(null);
            lbValeur2.setText("Valeur H.T.");
            lbValeur2.setPreferredSize(new Dimension(100, 30));
            lbValeur2.setMinimumSize(new Dimension(100, 30));
            lbValeur2.setName("lbValeur2");
            pnlValeurHT2.add(lbValeur2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2VAL ----
            T2VAL.setComponentPopupMenu(null);
            T2VAL.setPreferredSize(new Dimension(100, 30));
            T2VAL.setMinimumSize(new Dimension(100, 30));
            T2VAL.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2VAL.setName("T2VAL");
            pnlValeurHT2.add(T2VAL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType2.add(pnlValeurHT2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlValeurTTC2 ========
          {
            pnlValeurTTC2.setName("pnlValeurTTC2");
            pnlValeurTTC2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlValeurTTC2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlValeurTTC2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlValeurTTC2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlValeurTTC2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbValeur4 ----
            lbValeur4.setComponentPopupMenu(null);
            lbValeur4.setText("Valeur T.T.C.");
            lbValeur4.setPreferredSize(new Dimension(100, 30));
            lbValeur4.setMinimumSize(new Dimension(100, 30));
            lbValeur4.setMaximumSize(new Dimension(100, 30));
            lbValeur4.setName("lbValeur4");
            pnlValeurTTC2.add(lbValeur4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WTTC2 ----
            WTTC2.setComponentPopupMenu(null);
            WTTC2.setMinimumSize(new Dimension(100, 30));
            WTTC2.setPreferredSize(new Dimension(100, 30));
            WTTC2.setFont(new Font("sansserif", Font.PLAIN, 14));
            WTTC2.setName("WTTC2");
            pnlValeurTTC2.add(WTTC2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType2.add(pnlValeurTTC2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlCoefficient2 ========
          {
            pnlCoefficient2.setName("pnlCoefficient2");
            pnlCoefficient2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCoefficient2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCoefficient2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCoefficient2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCoefficient2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbCoefficient2 ----
            lbCoefficient2.setComponentPopupMenu(null);
            lbCoefficient2.setText("Coefficient");
            lbCoefficient2.setPreferredSize(new Dimension(100, 30));
            lbCoefficient2.setMinimumSize(new Dimension(100, 30));
            lbCoefficient2.setHorizontalTextPosition(SwingConstants.LEADING);
            lbCoefficient2.setMaximumSize(new Dimension(100, 30));
            lbCoefficient2.setName("lbCoefficient2");
            pnlCoefficient2.add(lbCoefficient2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2COE ----
            T2COE.setComponentPopupMenu(null);
            T2COE.setPreferredSize(new Dimension(60, 30));
            T2COE.setMinimumSize(new Dimension(60, 30));
            T2COE.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2COE.setName("T2COE");
            pnlCoefficient2.add(T2COE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- occulte2 ----
            occulte2.setText("occulte");
            occulte2.setToolTipText("modifie le prix mais occulte le coefficient sur les \u00e9ditions");
            occulte2.setFont(new Font("sansserif", Font.PLAIN, 14));
            occulte2.setMinimumSize(new Dimension(67, 30));
            occulte2.setPreferredSize(new Dimension(67, 30));
            occulte2.setMaximumSize(new Dimension(67, 30));
            occulte2.setName("occulte2");
            pnlCoefficient2.add(occulte2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType2.add(pnlCoefficient2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlFormule2 ========
          {
            pnlFormule2.setName("pnlFormule2");
            pnlFormule2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFormule2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFormule2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlFormule2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFormule2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbFormule2 ----
            lbFormule2.setComponentPopupMenu(null);
            lbFormule2.setText("Formule");
            lbFormule2.setPreferredSize(new Dimension(100, 30));
            lbFormule2.setMinimumSize(new Dimension(100, 30));
            lbFormule2.setHorizontalTextPosition(SwingConstants.LEADING);
            lbFormule2.setName("lbFormule2");
            pnlFormule2.add(lbFormule2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2FPR ----
            T2FPR.setComponentPopupMenu(null);
            T2FPR.setMinimumSize(new Dimension(68, 30));
            T2FPR.setPreferredSize(new Dimension(65, 30));
            T2FPR.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2FPR.setName("T2FPR");
            pnlFormule2.add(T2FPR, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType2.add(pnlFormule2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlRemise2 ========
          {
            pnlRemise2.setName("pnlRemise2");
            pnlRemise2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemise2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemise2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemise2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemise2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbRemises2 ----
            lbRemises2.setComponentPopupMenu(null);
            lbRemises2.setText("Remises");
            lbRemises2.setPreferredSize(new Dimension(100, 30));
            lbRemises2.setMinimumSize(new Dimension(100, 30));
            lbRemises2.setName("lbRemises2");
            pnlRemise2.add(lbRemises2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2REM1 ----
            T2REM1.setComponentPopupMenu(null);
            T2REM1.setPreferredSize(new Dimension(50, 30));
            T2REM1.setMinimumSize(new Dimension(50, 30));
            T2REM1.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2REM1.setMaximumSize(new Dimension(50, 28));
            T2REM1.setName("T2REM1");
            pnlRemise2.add(T2REM1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2REM2 ----
            T2REM2.setComponentPopupMenu(null);
            T2REM2.setMinimumSize(new Dimension(50, 30));
            T2REM2.setPreferredSize(new Dimension(50, 30));
            T2REM2.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2REM2.setName("T2REM2");
            pnlRemise2.add(T2REM2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2REM3 ----
            T2REM3.setComponentPopupMenu(null);
            T2REM3.setPreferredSize(new Dimension(50, 30));
            T2REM3.setMinimumSize(new Dimension(50, 30));
            T2REM3.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2REM3.setName("T2REM3");
            pnlRemise2.add(T2REM3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2REM4 ----
            T2REM4.setComponentPopupMenu(null);
            T2REM4.setMinimumSize(new Dimension(50, 30));
            T2REM4.setPreferredSize(new Dimension(50, 30));
            T2REM4.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2REM4.setMaximumSize(new Dimension(50, 28));
            T2REM4.setName("T2REM4");
            pnlRemise2.add(T2REM4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2REM5 ----
            T2REM5.setComponentPopupMenu(null);
            T2REM5.setPreferredSize(new Dimension(50, 30));
            T2REM5.setMinimumSize(new Dimension(50, 30));
            T2REM5.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2REM5.setName("T2REM5");
            pnlRemise2.add(T2REM5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- T2REM6 ----
            T2REM6.setComponentPopupMenu(null);
            T2REM6.setMinimumSize(new Dimension(50, 30));
            T2REM6.setPreferredSize(new Dimension(50, 30));
            T2REM6.setFont(new Font("sansserif", Font.PLAIN, 14));
            T2REM6.setName("T2REM6");
            pnlRemise2.add(T2REM6, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlModificationType2.add(pnlRemise2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition2.add(pnlModificationType2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTypeGratuit2 ========
        {
          pnlTypeGratuit2.setName("pnlTypeGratuit2");
          pnlTypeGratuit2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTypeGratuit2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTypeGratuit2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTypeGratuit2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTypeGratuit2.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- ckTypeGratuit2 ----
          ckTypeGratuit2.setText("Type gratuit");
          ckTypeGratuit2.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckTypeGratuit2.setPreferredSize(new Dimension(100, 30));
          ckTypeGratuit2.setMinimumSize(new Dimension(100, 30));
          ckTypeGratuit2.setMaximumSize(new Dimension(100, 30));
          ckTypeGratuit2.setName("ckTypeGratuit2");
          ckTypeGratuit2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              ckTypeGratuit2ItemStateChanged(e);
            }
          });
          pnlTypeGratuit2.add(ckTypeGratuit2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snTypeGratuit2 ----
          snTypeGratuit2.setPreferredSize(new Dimension(250, 30));
          snTypeGratuit2.setMinimumSize(new Dimension(250, 30));
          snTypeGratuit2.setMaximumSize(new Dimension(250, 30));
          snTypeGratuit2.setName("snTypeGratuit2");
          pnlTypeGratuit2.add(snTypeGratuit2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition2.add(pnlTypeGratuit2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlValidite2 ========
        {
          pnlValidite2.setPreferredSize(new Dimension(700, 30));
          pnlValidite2.setMinimumSize(new Dimension(700, 30));
          pnlValidite2.setMaximumSize(new Dimension(700, 30));
          pnlValidite2.setName("pnlValidite2");
          pnlValidite2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlValidite2.getLayout()).columnWidths = new int[] { 124, 379, 0 };
          ((GridBagLayout) pnlValidite2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlValidite2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlValidite2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbPeriodeValidite2 ----
          lbPeriodeValidite2.setText("Periode de validit\u00e9");
          lbPeriodeValidite2.setMaximumSize(new Dimension(140, 30));
          lbPeriodeValidite2.setPreferredSize(new Dimension(140, 30));
          lbPeriodeValidite2.setMinimumSize(new Dimension(140, 30));
          lbPeriodeValidite2.setHorizontalTextPosition(SwingConstants.LEADING);
          lbPeriodeValidite2.setName("lbPeriodeValidite2");
          pnlValidite2.add(lbPeriodeValidite2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snPeriodeValidite2 ----
          snPeriodeValidite2.setMaximumSize(new Dimension(320, 30));
          snPeriodeValidite2.setName("snPeriodeValidite2");
          pnlValidite2.add(snPeriodeValidite2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCondition2.add(pnlValidite2, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllPrincipal.add(pnlCondition2,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlEcoTaxe ========
      {
        pnlEcoTaxe.setName("pnlEcoTaxe");
        pnlEcoTaxe.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEcoTaxe.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEcoTaxe.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlEcoTaxe.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlEcoTaxe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbEcoTaxe ----
        lbEcoTaxe.setComponentPopupMenu(null);
        lbEcoTaxe.setText("Eco-taxe");
        lbEcoTaxe.setMinimumSize(new Dimension(140, 30));
        lbEcoTaxe.setPreferredSize(new Dimension(140, 30));
        lbEcoTaxe.setName("lbEcoTaxe");
        pnlEcoTaxe.add(lbEcoTaxe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- btEcotaxe ----
        btEcotaxe.setMinimumSize(new Dimension(18, 30));
        btEcotaxe.setPreferredSize(new Dimension(18, 30));
        btEcotaxe.setName("btEcotaxe");
        btEcotaxe.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            btEcotaxeActionPerformed(e);
          }
        });
        pnlEcoTaxe.add(btEcotaxe, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfEcoTaxe ----
        tfEcoTaxe.setComponentPopupMenu(null);
        tfEcoTaxe.setText("@TEARRT@");
        tfEcoTaxe.setPreferredSize(new Dimension(128, 30));
        tfEcoTaxe.setMinimumSize(new Dimension(128, 30));
        tfEcoTaxe.setEditable(false);
        tfEcoTaxe.setEnabled(false);
        tfEcoTaxe.setName("tfEcoTaxe");
        pnlEcoTaxe.add(tfEcoTaxe, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbNombre ----
        lbNombre.setComponentPopupMenu(null);
        lbNombre.setText("Nombre");
        lbNombre.setPreferredSize(new Dimension(105, 30));
        lbNombre.setMinimumSize(new Dimension(100, 30));
        lbNombre.setName("lbNombre");
        pnlEcoTaxe.add(lbNombre, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfNombre ----
        tfNombre.setComponentPopupMenu(null);
        tfNombre.setText("@WNBR@");
        tfNombre.setHorizontalAlignment(SwingConstants.RIGHT);
        tfNombre.setMinimumSize(new Dimension(44, 30));
        tfNombre.setPreferredSize(new Dimension(44, 30));
        tfNombre.setEditable(false);
        tfNombre.setName("tfNombre");
        pnlEcoTaxe.add(tfNombre, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbMontant ----
        lbMontant.setComponentPopupMenu(null);
        lbMontant.setText("Montant");
        lbMontant.setPreferredSize(new Dimension(70, 30));
        lbMontant.setMinimumSize(new Dimension(125, 30));
        lbMontant.setName("lbMontant");
        pnlEcoTaxe.add(lbMontant, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfMontant ----
        tfMontant.setComponentPopupMenu(null);
        tfMontant.setText("@WMTTEE@");
        tfMontant.setHorizontalAlignment(SwingConstants.RIGHT);
        tfMontant.setPreferredSize(new Dimension(116, 30));
        tfMontant.setMinimumSize(new Dimension(116, 30));
        tfMontant.setEditable(false);
        tfMontant.setName("tfMontant");
        pnlEcoTaxe.add(tfMontant, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllPrincipal.add(pnlEcoTaxe,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTarif ========
      {
        pnlTarif.setOpaque(false);
        pnlTarif.setTitre("Tarif  H.T");
        pnlTarif.setMinimumSize(new Dimension(1875, 220));
        pnlTarif.setPreferredSize(new Dimension(1875, 220));
        pnlTarif.setName("pnlTarif");
        pnlTarif.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlTarif.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlTarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbReference ----
        lbReference.setText("R\u00e9f\u00e9rence");
        lbReference.setHorizontalAlignment(SwingConstants.LEFT);
        lbReference.setMinimumSize(new Dimension(150, 19));
        lbReference.setPreferredSize(new Dimension(180, 30));
        lbReference.setMaximumSize(new Dimension(180, 30));
        lbReference.setName("lbReference");
        pnlTarif.add(lbReference, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbEnCours ----
        lbEnCours.setText("Tarif en cours");
        lbEnCours.setHorizontalAlignment(SwingConstants.LEFT);
        lbEnCours.setMinimumSize(new Dimension(150, 19));
        lbEnCours.setPreferredSize(new Dimension(180, 30));
        lbEnCours.setMaximumSize(new Dimension(180, 30));
        lbEnCours.setName("lbEnCours");
        pnlTarif.add(lbEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfReference1 ----
        tfReference1.setText("@A1RTA@");
        tfReference1.setPreferredSize(new Dimension(68, 30));
        tfReference1.setMinimumSize(new Dimension(68, 30));
        tfReference1.setEditable(false);
        tfReference1.setEnabled(false);
        tfReference1.setName("tfReference1");
        pnlTarif.add(tfReference1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfReference2 ----
        tfReference2.setText("@ULBTAR@");
        tfReference2.setPreferredSize(new Dimension(512, 30));
        tfReference2.setMinimumSize(new Dimension(512, 30));
        tfReference2.setEditable(false);
        tfReference2.setEnabled(false);
        tfReference2.setName("tfReference2");
        pnlTarif.add(tfReference2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixDeRevientStandard ----
        lbPrixDeRevientStandard.setText("Prix de revient standard HT");
        lbPrixDeRevientStandard.setMinimumSize(new Dimension(190, 30));
        lbPrixDeRevientStandard.setPreferredSize(new Dimension(190, 30));
        lbPrixDeRevientStandard.setMaximumSize(new Dimension(190, 30));
        lbPrixDeRevientStandard.setName("lbPrixDeRevientStandard");
        pnlTarif.add(lbPrixDeRevientStandard, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPrixRevient ----
        tfPrixRevient.setText("@WPRVA@");
        tfPrixRevient.setHorizontalAlignment(SwingConstants.RIGHT);
        tfPrixRevient.setEditable(false);
        tfPrixRevient.setEnabled(false);
        tfPrixRevient.setName("tfPrixRevient");
        pnlTarif.add(tfPrixRevient, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPrixVente1 ----
        lbPrixVente1.setText("Prix de vente");
        lbPrixVente1.setMaximumSize(new Dimension(100, 30));
        lbPrixVente1.setMinimumSize(new Dimension(100, 30));
        lbPrixVente1.setPreferredSize(new Dimension(100, 30));
        lbPrixVente1.setName("lbPrixVente1");
        pnlTarif.add(lbPrixVente1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlPrixRevientCoeff ========
        {
          pnlPrixRevientCoeff.setName("pnlPrixRevientCoeff");
          pnlPrixRevientCoeff.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixRevientCoeff.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlPrixRevientCoeff.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixRevientCoeff.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixRevientCoeff.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfPrixVenteEnCours ----
          tfPrixVenteEnCours.setText("@A1P11X@");
          tfPrixVenteEnCours.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVenteEnCours.setMinimumSize(new Dimension(128, 30));
          tfPrixVenteEnCours.setPreferredSize(new Dimension(128, 30));
          tfPrixVenteEnCours.setEditable(false);
          tfPrixVenteEnCours.setEnabled(false);
          tfPrixVenteEnCours.setName("tfPrixVenteEnCours");
          pnlPrixRevientCoeff.add(tfPrixVenteEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVenteEnCours1 ----
          tfPrixVenteEnCours1.setText("@A1P21X@");
          tfPrixVenteEnCours1.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVenteEnCours1.setPreferredSize(new Dimension(128, 30));
          tfPrixVenteEnCours1.setMinimumSize(new Dimension(128, 30));
          tfPrixVenteEnCours1.setEditable(false);
          tfPrixVenteEnCours1.setEnabled(false);
          tfPrixVenteEnCours1.setName("tfPrixVenteEnCours1");
          pnlPrixRevientCoeff.add(tfPrixVenteEnCours1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVenteEnCours2 ----
          tfPrixVenteEnCours2.setText("@A1P31X@");
          tfPrixVenteEnCours2.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVenteEnCours2.setMinimumSize(new Dimension(128, 30));
          tfPrixVenteEnCours2.setPreferredSize(new Dimension(128, 30));
          tfPrixVenteEnCours2.setEditable(false);
          tfPrixVenteEnCours2.setEnabled(false);
          tfPrixVenteEnCours2.setName("tfPrixVenteEnCours2");
          pnlPrixRevientCoeff.add(tfPrixVenteEnCours2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVenteEnCours3 ----
          tfPrixVenteEnCours3.setText("@A1P41X@");
          tfPrixVenteEnCours3.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVenteEnCours3.setPreferredSize(new Dimension(128, 30));
          tfPrixVenteEnCours3.setMinimumSize(new Dimension(128, 30));
          tfPrixVenteEnCours3.setEditable(false);
          tfPrixVenteEnCours3.setEnabled(false);
          tfPrixVenteEnCours3.setName("tfPrixVenteEnCours3");
          pnlPrixRevientCoeff.add(tfPrixVenteEnCours3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVenteEnCours4 ----
          tfPrixVenteEnCours4.setText("@A1P51X@");
          tfPrixVenteEnCours4.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVenteEnCours4.setMinimumSize(new Dimension(128, 30));
          tfPrixVenteEnCours4.setPreferredSize(new Dimension(128, 30));
          tfPrixVenteEnCours4.setEditable(false);
          tfPrixVenteEnCours4.setEnabled(false);
          tfPrixVenteEnCours4.setName("tfPrixVenteEnCours4");
          pnlPrixRevientCoeff.add(tfPrixVenteEnCours4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVenteEnCours5 ----
          tfPrixVenteEnCours5.setText("@A1P61X@");
          tfPrixVenteEnCours5.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVenteEnCours5.setPreferredSize(new Dimension(128, 30));
          tfPrixVenteEnCours5.setMinimumSize(new Dimension(128, 30));
          tfPrixVenteEnCours5.setEditable(false);
          tfPrixVenteEnCours5.setEnabled(false);
          tfPrixVenteEnCours5.setName("tfPrixVenteEnCours5");
          pnlPrixRevientCoeff.add(tfPrixVenteEnCours5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfCoefEnCours ----
          tfCoefEnCours.setText("@A1K21X@");
          tfCoefEnCours.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoefEnCours.setPreferredSize(new Dimension(80, 30));
          tfCoefEnCours.setMinimumSize(new Dimension(80, 30));
          tfCoefEnCours.setEditable(false);
          tfCoefEnCours.setEnabled(false);
          tfCoefEnCours.setName("tfCoefEnCours");
          pnlPrixRevientCoeff.add(tfCoefEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoefEnCours1 ----
          tfCoefEnCours1.setText("@A1K31X@");
          tfCoefEnCours1.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoefEnCours1.setPreferredSize(new Dimension(80, 30));
          tfCoefEnCours1.setMinimumSize(new Dimension(80, 30));
          tfCoefEnCours1.setEditable(false);
          tfCoefEnCours1.setEnabled(false);
          tfCoefEnCours1.setName("tfCoefEnCours1");
          pnlPrixRevientCoeff.add(tfCoefEnCours1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoefEnCours2 ----
          tfCoefEnCours2.setText("@A1K41X@");
          tfCoefEnCours2.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoefEnCours2.setMinimumSize(new Dimension(80, 30));
          tfCoefEnCours2.setPreferredSize(new Dimension(80, 30));
          tfCoefEnCours2.setEditable(false);
          tfCoefEnCours2.setEnabled(false);
          tfCoefEnCours2.setName("tfCoefEnCours2");
          pnlPrixRevientCoeff.add(tfCoefEnCours2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoefEnCours3 ----
          tfCoefEnCours3.setText("@A1K51X@");
          tfCoefEnCours3.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoefEnCours3.setPreferredSize(new Dimension(80, 30));
          tfCoefEnCours3.setMinimumSize(new Dimension(80, 30));
          tfCoefEnCours3.setEditable(false);
          tfCoefEnCours3.setEnabled(false);
          tfCoefEnCours3.setName("tfCoefEnCours3");
          pnlPrixRevientCoeff.add(tfCoefEnCours3, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoefEnCours4 ----
          tfCoefEnCours4.setText("@A1K61X@");
          tfCoefEnCours4.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoefEnCours4.setMinimumSize(new Dimension(80, 30));
          tfCoefEnCours4.setPreferredSize(new Dimension(80, 30));
          tfCoefEnCours4.setEditable(false);
          tfCoefEnCours4.setEnabled(false);
          tfCoefEnCours4.setName("tfCoefEnCours4");
          pnlPrixRevientCoeff.add(tfCoefEnCours4, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlPrixRevientCoeff, new GridBagConstraints(2, 1, 3, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCoefficient3 ----
        lbCoefficient3.setText("Coefficient");
        lbCoefficient3.setName("lbCoefficient3");
        pnlTarif.add(lbCoefficient3, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlDateTarif ========
        {
          pnlDateTarif.setOpaque(false);
          pnlDateTarif.setName("pnlDateTarif");
          pnlDateTarif.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateTarif.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateTarif.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDateTarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDateTarif.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlTarifAu ========
          {
            pnlTarifAu.setMinimumSize(new Dimension(150, 30));
            pnlTarifAu.setPreferredSize(new Dimension(180, 30));
            pnlTarifAu.setName("pnlTarifAu");
            pnlTarifAu.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifAu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTarifAu.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTarifAu.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifAu.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbAu ----
            lbAu.setText("Tarif au");
            lbAu.setMinimumSize(new Dimension(50, 30));
            lbAu.setPreferredSize(new Dimension(50, 30));
            lbAu.setHorizontalAlignment(SwingConstants.LEFT);
            lbAu.setName("lbAu");
            pnlTarifAu.add(lbAu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- W0DTDX ----
            W0DTDX.setPreferredSize(new Dimension(120, 30));
            W0DTDX.setMinimumSize(new Dimension(120, 30));
            W0DTDX.setFont(new Font("sansserif", Font.PLAIN, 14));
            W0DTDX.setName("W0DTDX");
            pnlTarifAu.add(W0DTDX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDateTarif.add(pnlTarifAu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixVente2 ----
          lbPrixVente2.setText("Prix de vente");
          lbPrixVente2.setName("lbPrixVente2");
          pnlDateTarif.add(lbPrixVente2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVente ----
          tfPrixVente.setText("@A1P12X@");
          tfPrixVente.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVente.setPreferredSize(new Dimension(128, 30));
          tfPrixVente.setMinimumSize(new Dimension(128, 30));
          tfPrixVente.setEditable(false);
          tfPrixVente.setEnabled(false);
          tfPrixVente.setName("tfPrixVente");
          pnlDateTarif.add(tfPrixVente, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVente1 ----
          tfPrixVente1.setText("@A1P22X@");
          tfPrixVente1.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVente1.setMinimumSize(new Dimension(128, 30));
          tfPrixVente1.setPreferredSize(new Dimension(128, 30));
          tfPrixVente1.setEditable(false);
          tfPrixVente1.setEnabled(false);
          tfPrixVente1.setName("tfPrixVente1");
          pnlDateTarif.add(tfPrixVente1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVente2 ----
          tfPrixVente2.setText("@A1P32X@");
          tfPrixVente2.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVente2.setMinimumSize(new Dimension(128, 30));
          tfPrixVente2.setPreferredSize(new Dimension(128, 30));
          tfPrixVente2.setEditable(false);
          tfPrixVente2.setEnabled(false);
          tfPrixVente2.setName("tfPrixVente2");
          pnlDateTarif.add(tfPrixVente2, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVente3 ----
          tfPrixVente3.setText("@A1P42X@");
          tfPrixVente3.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVente3.setPreferredSize(new Dimension(128, 30));
          tfPrixVente3.setMinimumSize(new Dimension(128, 30));
          tfPrixVente3.setEditable(false);
          tfPrixVente3.setEnabled(false);
          tfPrixVente3.setName("tfPrixVente3");
          pnlDateTarif.add(tfPrixVente3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVente4 ----
          tfPrixVente4.setText("@A1P52X@");
          tfPrixVente4.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVente4.setPreferredSize(new Dimension(128, 30));
          tfPrixVente4.setMinimumSize(new Dimension(128, 30));
          tfPrixVente4.setEditable(false);
          tfPrixVente4.setEnabled(false);
          tfPrixVente4.setName("tfPrixVente4");
          pnlDateTarif.add(tfPrixVente4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixVente5 ----
          tfPrixVente5.setText("@A1P62X@");
          tfPrixVente5.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixVente5.setPreferredSize(new Dimension(128, 30));
          tfPrixVente5.setMinimumSize(new Dimension(128, 30));
          tfPrixVente5.setEditable(false);
          tfPrixVente5.setEnabled(false);
          tfPrixVente5.setName("tfPrixVente5");
          pnlDateTarif.add(tfPrixVente5, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCoefficient4 ----
          lbCoefficient4.setText("Coefficient");
          lbCoefficient4.setPreferredSize(new Dimension(100, 30));
          lbCoefficient4.setMaximumSize(new Dimension(100, 30));
          lbCoefficient4.setMinimumSize(new Dimension(100, 30));
          lbCoefficient4.setName("lbCoefficient4");
          pnlDateTarif.add(lbCoefficient4, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoef ----
          tfCoef.setText("@A1K22X@");
          tfCoef.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoef.setMinimumSize(new Dimension(80, 30));
          tfCoef.setPreferredSize(new Dimension(80, 30));
          tfCoef.setEditable(false);
          tfCoef.setEnabled(false);
          tfCoef.setName("tfCoef");
          pnlDateTarif.add(tfCoef, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoef1 ----
          tfCoef1.setText("@A1K32X@");
          tfCoef1.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoef1.setPreferredSize(new Dimension(80, 30));
          tfCoef1.setMinimumSize(new Dimension(80, 30));
          tfCoef1.setEditable(false);
          tfCoef1.setEnabled(false);
          tfCoef1.setName("tfCoef1");
          pnlDateTarif.add(tfCoef1, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoef2 ----
          tfCoef2.setText("@A1K42X@");
          tfCoef2.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoef2.setMinimumSize(new Dimension(80, 30));
          tfCoef2.setPreferredSize(new Dimension(80, 30));
          tfCoef2.setEditable(false);
          tfCoef2.setEnabled(false);
          tfCoef2.setName("tfCoef2");
          pnlDateTarif.add(tfCoef2, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoef3 ----
          tfCoef3.setText("@A1K52X@");
          tfCoef3.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoef3.setMinimumSize(new Dimension(80, 30));
          tfCoef3.setPreferredSize(new Dimension(80, 30));
          tfCoef3.setEditable(false);
          tfCoef3.setEnabled(false);
          tfCoef3.setName("tfCoef3");
          pnlDateTarif.add(tfCoef3, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfCoef4 ----
          tfCoef4.setText("@A1K62X@");
          tfCoef4.setHorizontalAlignment(SwingConstants.RIGHT);
          tfCoef4.setPreferredSize(new Dimension(80, 30));
          tfCoef4.setMinimumSize(new Dimension(80, 30));
          tfCoef4.setEditable(false);
          tfCoef4.setEnabled(false);
          tfCoef4.setName("tfCoef4");
          pnlDateTarif.add(tfCoef4, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlDateTarif, new GridBagConstraints(0, 3, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllPrincipal.add(pnlTarif, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnllPrincipal, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- T1TRA2 ----
    T1TRA2.setModel(new DefaultComboBoxModel(new String[] { "Article", "Tarif", "@\"@LRRAT1@\"", "@\"@LRRAT2@\"", "@\"@LRRAT3@\"",
        "Embo\u00eetage ou g\u00e9n\u00e9ral", "Ensemble d'articles", "Num de ligne (rang)", "Regroupement achat", "Fournisseur" }));
    T1TRA2.setComponentPopupMenu(null);
    T1TRA2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    T1TRA2.setPreferredSize(new Dimension(154, 30));
    T1TRA2.setMinimumSize(new Dimension(154, 30));
    T1TRA2.setEnabled(false);
    T1TRA2.setFont(new Font("sansserif", Font.PLAIN, 14));
    T1TRA2.setName("T1TRA2");
    
    // ---- lbNegoAchat ----
    lbNegoAchat.setText("N\u00e9gociation achat");
    lbNegoAchat.setMaximumSize(new Dimension(170, 30));
    lbNegoAchat.setMinimumSize(new Dimension(170, 30));
    lbNegoAchat.setPreferredSize(new Dimension(170, 30));
    lbNegoAchat.setName("lbNegoAchat");
    
    // ---- btNegoAchat ----
    btNegoAchat.setMinimumSize(new Dimension(18, 30));
    btNegoAchat.setPreferredSize(new Dimension(18, 30));
    btNegoAchat.setName("btNegoAchat");
    btNegoAchat.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btEcotaxeActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre pbPresentation;
  private SNPanelContenu pnllPrincipal;
  private SNPanel pnlEntete;
  private SNLabelChamp OBJ_45;
  private SNEtablissement sNEtablissement;
  private SNLabelUnite lbCN;
  private SNTexte CNV;
  private SNTexte CNV2;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNLabelChamp lbReferenceCNV;
  private XRiTextField T1REF;
  private SNLabelUnite lbTypeRattachement;
  private SNTexte RAT;
  private SNTexte RAT2;
  private SNLabelChamp lbUnite;
  private XRiTextField WUNV;
  private SNPanelTitre pnlCondition1;
  private SNPanel pnlType1;
  private SNLabelChamp lbType1;
  private XRiComboBox T1TCD;
  private SNPanel pnlTarif1;
  private SNLabelChamp lbTarif1;
  private XRiComboBox WTAR1;
  private SNPanel pnlModificationType1;
  private SNPanel pnlValeurHT1;
  private SNLabelChamp lbValeur1;
  private XRiTextField T1VAL;
  private SNPanel pnlFormule1;
  private SNLabelChamp lbFormule1;
  private XRiTextField T1FPR;
  private SNPanel pnlValeurTTC1;
  private SNLabelChamp lbValeur3;
  private XRiTextField WTTC1;
  private SNPanel pnlCoefficient1;
  private SNLabelChamp lbCoefficient1;
  private XRiTextField T1COE;
  private JCheckBox occulte1;
  private SNPanel pnlRemise1;
  private SNLabelChamp lbRemises1;
  private XRiTextField T1REM1;
  private XRiTextField T1REM2;
  private XRiTextField T1REM3;
  private XRiTextField T1REM4;
  private XRiTextField T1REM5;
  private XRiTextField T1REM6;
  private SNPanel pnlTypeGratuit1;
  private XRiCheckBox ckTypeGratuit1;
  private SNTypeGratuit snTypeGratuit1;
  private SNPanel pnlValidite1;
  private SNLabelChamp lbPeriodeValidite1;
  private SNPlageDate snPeriodeValidite1;
  private SNPanelTitre pnlCondition2;
  private SNPanel pnlType2;
  private SNLabelChamp lbType2;
  private XRiComboBox T2TCD;
  private SNPanel pnlTarif2;
  private SNLabelChamp lbTarif2;
  private XRiComboBox WTAR2;
  private SNPanel pnlModificationType2;
  private SNPanel pnlValeurHT2;
  private SNLabelChamp lbValeur2;
  private XRiTextField T2VAL;
  private SNPanel pnlValeurTTC2;
  private SNLabelChamp lbValeur4;
  private XRiTextField WTTC2;
  private SNPanel pnlCoefficient2;
  private SNLabelChamp lbCoefficient2;
  private XRiTextField T2COE;
  private JCheckBox occulte2;
  private SNPanel pnlFormule2;
  private SNLabelChamp lbFormule2;
  private XRiTextField T2FPR;
  private SNPanel pnlRemise2;
  private SNLabelChamp lbRemises2;
  private XRiTextField T2REM1;
  private XRiTextField T2REM2;
  private XRiTextField T2REM3;
  private XRiTextField T2REM4;
  private XRiTextField T2REM5;
  private XRiTextField T2REM6;
  private SNPanel pnlTypeGratuit2;
  private XRiCheckBox ckTypeGratuit2;
  private SNTypeGratuit snTypeGratuit2;
  private SNPanel pnlValidite2;
  private SNLabelChamp lbPeriodeValidite2;
  private SNPlageDate snPeriodeValidite2;
  private SNPanel pnlEcoTaxe;
  private SNLabelChamp lbEcoTaxe;
  private SNBoutonDetail btEcotaxe;
  private SNTexte tfEcoTaxe;
  private SNLabelChamp lbNombre;
  private SNTexte tfNombre;
  private SNLabelChamp lbMontant;
  private SNTexte tfMontant;
  private SNPanelTitre pnlTarif;
  private SNLabelChamp lbReference;
  private SNLabelChamp lbEnCours;
  private SNTexte tfReference1;
  private SNTexte tfReference2;
  private SNLabelChamp lbPrixDeRevientStandard;
  private SNTexte tfPrixRevient;
  private SNLabelChamp lbPrixVente1;
  private SNPanel pnlPrixRevientCoeff;
  private SNTexte tfPrixVenteEnCours;
  private SNTexte tfPrixVenteEnCours1;
  private SNTexte tfPrixVenteEnCours2;
  private SNTexte tfPrixVenteEnCours3;
  private SNTexte tfPrixVenteEnCours4;
  private SNTexte tfPrixVenteEnCours5;
  private SNTexte tfCoefEnCours;
  private SNTexte tfCoefEnCours1;
  private SNTexte tfCoefEnCours2;
  private SNTexte tfCoefEnCours3;
  private SNTexte tfCoefEnCours4;
  private SNLabelChamp lbCoefficient3;
  private SNPanel pnlDateTarif;
  private SNPanel pnlTarifAu;
  private SNLabelChamp lbAu;
  private XRiCalendrier W0DTDX;
  private SNLabelChamp lbPrixVente2;
  private SNTexte tfPrixVente;
  private SNTexte tfPrixVente1;
  private SNTexte tfPrixVente2;
  private SNTexte tfPrixVente3;
  private SNTexte tfPrixVente4;
  private SNTexte tfPrixVente5;
  private SNLabelChamp lbCoefficient4;
  private SNTexte tfCoef;
  private SNTexte tfCoef1;
  private SNTexte tfCoef2;
  private SNTexte tfCoef3;
  private SNTexte tfCoef4;
  private SNBarreBouton snBarreBouton;
  private JComboBox T1TRA2;
  private SNLabelChamp lbNegoAchat;
  private SNBoutonDetail btNegoAchat;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
