
package ri.serien.libecranrpg.vgvm.VGVM03FF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM03FF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WCLFP = new XRiTextField();
    WCLFS = new XRiTextField();
    WLBCLF = new XRiTextField();
    WCC1 = new XRiTextField();
    WLBCC1 = new XRiTextField();
    WCC2 = new XRiTextField();
    WLBCC2 = new XRiTextField();
    WCC3 = new XRiTextField();
    WLBCC3 = new XRiTextField();
    WCLP = new XRiTextField();
    WLBCLP = new XRiTextField();
    WAFA = new XRiTextField();
    WNCG = new XRiTextField();
    WNCA = new XRiTextField();
    WTRA = new XRiTextField();
    WLBTRA = new XRiTextField();
    OBJ_34_OBJ_34 = new JLabel();
    OBJ_29_OBJ_29 = new JLabel();
    OBJ_18_OBJ_18 = new JLabel();
    OBJ_37_OBJ_37 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_22_OBJ_22 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 315));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Clients fac. rattach\u00e9s");
              riSousMenu_bt6.setToolTipText("Clients factur\u00e9s rattach\u00e9s");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Clients payeurs rattach\u00e9s");
              riSousMenu_bt7.setToolTipText("Clients payeurs rattach\u00e9s");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("R\u00e9capitulatif"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WCLFP ----
          WCLFP.setName("WCLFP");
          panel2.add(WCLFP);
          WCLFP.setBounds(160, 36, 60, WCLFP.getPreferredSize().height);

          //---- WCLFS ----
          WCLFS.setName("WCLFS");
          panel2.add(WCLFS);
          WCLFS.setBounds(224, 36, 36, WCLFS.getPreferredSize().height);

          //---- WLBCLF ----
          WLBCLF.setName("WLBCLF");
          panel2.add(WLBCLF);
          WLBCLF.setBounds(264, 36, 310, WLBCLF.getPreferredSize().height);

          //---- WCC1 ----
          WCC1.setName("WCC1");
          panel2.add(WCC1);
          WCC1.setBounds(160, 66, 60, WCC1.getPreferredSize().height);

          //---- WLBCC1 ----
          WLBCC1.setName("WLBCC1");
          panel2.add(WLBCC1);
          WLBCC1.setBounds(225, 66, 350, WLBCC1.getPreferredSize().height);

          //---- WCC2 ----
          WCC2.setName("WCC2");
          panel2.add(WCC2);
          WCC2.setBounds(160, 96, 60, WCC2.getPreferredSize().height);

          //---- WLBCC2 ----
          WLBCC2.setName("WLBCC2");
          panel2.add(WLBCC2);
          WLBCC2.setBounds(225, 96, 350, WLBCC2.getPreferredSize().height);

          //---- WCC3 ----
          WCC3.setName("WCC3");
          panel2.add(WCC3);
          WCC3.setBounds(160, 126, 60, WCC3.getPreferredSize().height);

          //---- WLBCC3 ----
          WLBCC3.setName("WLBCC3");
          panel2.add(WLBCC3);
          WLBCC3.setBounds(225, 126, 350, WLBCC3.getPreferredSize().height);

          //---- WCLP ----
          WCLP.setName("WCLP");
          panel2.add(WCLP);
          WCLP.setBounds(160, 156, 60, WCLP.getPreferredSize().height);

          //---- WLBCLP ----
          WLBCLP.setName("WLBCLP");
          panel2.add(WLBCLP);
          WLBCLP.setBounds(225, 156, 350, WLBCLP.getPreferredSize().height);

          //---- WAFA ----
          WAFA.setName("WAFA");
          panel2.add(WAFA);
          WAFA.setBounds(160, 186, 80, WAFA.getPreferredSize().height);

          //---- WNCG ----
          WNCG.setName("WNCG");
          panel2.add(WNCG);
          WNCG.setBounds(160, 216, 60, WNCG.getPreferredSize().height);

          //---- WNCA ----
          WNCA.setName("WNCA");
          panel2.add(WNCA);
          WNCA.setBounds(225, 216, 60, WNCA.getPreferredSize().height);

          //---- WTRA ----
          WTRA.setName("WTRA");
          panel2.add(WTRA);
          WTRA.setBounds(160, 246, 60, WTRA.getPreferredSize().height);

          //---- WLBTRA ----
          WLBTRA.setName("WLBTRA");
          panel2.add(WLBTRA);
          WLBTRA.setBounds(225, 246, 350, WLBTRA.getPreferredSize().height);

          //---- OBJ_34_OBJ_34 ----
          OBJ_34_OBJ_34.setText("Num\u00e9ro de compte");
          OBJ_34_OBJ_34.setName("OBJ_34_OBJ_34");
          panel2.add(OBJ_34_OBJ_34);
          OBJ_34_OBJ_34.setBounds(30, 220, 118, 20);

          //---- OBJ_29_OBJ_29 ----
          OBJ_29_OBJ_29.setText("Client payeur");
          OBJ_29_OBJ_29.setName("OBJ_29_OBJ_29");
          panel2.add(OBJ_29_OBJ_29);
          OBJ_29_OBJ_29.setBounds(30, 160, 100, 20);

          //---- OBJ_18_OBJ_18 ----
          OBJ_18_OBJ_18.setText("Client factur\u00e9");
          OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");
          panel2.add(OBJ_18_OBJ_18);
          OBJ_18_OBJ_18.setBounds(30, 40, 97, 20);

          //---- OBJ_37_OBJ_37 ----
          OBJ_37_OBJ_37.setText("Client transitaire");
          OBJ_37_OBJ_37.setName("OBJ_37_OBJ_37");
          panel2.add(OBJ_37_OBJ_37);
          OBJ_37_OBJ_37.setBounds(30, 250, 110, 20);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Affactureur");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel2.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(30, 190, 85, 20);

          //---- OBJ_22_OBJ_22 ----
          OBJ_22_OBJ_22.setText("Centrales");
          OBJ_22_OBJ_22.setName("OBJ_22_OBJ_22");
          panel2.add(OBJ_22_OBJ_22);
          OBJ_22_OBJ_22.setBounds(30, 70, 105, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 610, 295);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WCLFP;
  private XRiTextField WCLFS;
  private XRiTextField WLBCLF;
  private XRiTextField WCC1;
  private XRiTextField WLBCC1;
  private XRiTextField WCC2;
  private XRiTextField WLBCC2;
  private XRiTextField WCC3;
  private XRiTextField WLBCC3;
  private XRiTextField WCLP;
  private XRiTextField WLBCLP;
  private XRiTextField WAFA;
  private XRiTextField WNCG;
  private XRiTextField WNCA;
  private XRiTextField WTRA;
  private XRiTextField WLBTRA;
  private JLabel OBJ_34_OBJ_34;
  private JLabel OBJ_29_OBJ_29;
  private JLabel OBJ_18_OBJ_18;
  private JLabel OBJ_37_OBJ_37;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_22_OBJ_22;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
