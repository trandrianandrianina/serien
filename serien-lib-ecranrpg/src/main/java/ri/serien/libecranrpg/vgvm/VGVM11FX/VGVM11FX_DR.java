
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.commun.memo.EnumTypeMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.gescom.commun.adressedocument.EnumCodeEnteteAdresseDocument;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.vente.adresselivraison.ModeleListeAdresseLivraisonChantier;
import ri.serien.libswing.composant.metier.vente.adresselivraison.VueListeAdresseLivraisonChantier;
import ri.serien.libswing.composant.metier.vente.documentvente.snmodeexpedition.SNModeExpedition;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNAdresseMail;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_DR extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] E1TRC_Value = { "", "1", "N", "G" };
  private ODialog dialog_REGL = null;
  
  private RiPanelNav riPanelNav1 = null;
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  private boolean isChantier = false;
  private Memo memoLivraisonEnlevement = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_DR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    E1NHO.setValeursSelection("1", "");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/navigation.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/agrandir.png", true));
    
    // Navigation graphique du menu
    riPanelNav1 = new RiPanelNav();
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de devis", true, null);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du devis", false, bouton_valider);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de devis", false, button1);
    riSousMenu1.add(riPanelNav1);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WVDNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    LTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LTEL@")).trim());
    LTEL.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    E1CLFP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLFP@")).trim());
    E1CLFS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CLFS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    
    riSousMenu7.setVisible(!isChantier && lexique.getMode() != 1);
    riSousMenu7.setEnabled(!isChantier);
    
    lbPosition.setVisible(lexique.isPresent("WEPOS"));
    
    if (lexique.isTrue("79")) {
      WEPOS.setForeground(Color.RED);
    }
    
    lbDepassement.setVisible(lexique.isTrue("(N08) AND (N38)"));
    lbEcart.setVisible(!lbDepassement.isVisible());
    
    riSousMenu19.setVisible(isChantier);
    
    // régularisation stock palette chantier
    riSousMenu17.setEnabled(isChantier);
    riSousMenu17.setVisible(isChantier && interpreteurD.analyseExpression("@REGPAL@").trim().equals("STKPAL"));
    
    // Label état du bon
    String etatBon = lexique.HostFieldGetData("ETABON");
    
    OBJ_129.setText("");
    if (etatBon.equals("C")) {
      OBJ_129.setText("Comptabilisé");
    }
    else if (etatBon.equals("T")) {
      OBJ_129.setText("Commande type");
    }
    else if (etatBon.equals("P")) {
      OBJ_129.setText("Positionnement");
    }
    else if (etatBon.equals("A")) {
      OBJ_129.setText("Attente");
    }
    else if (etatBon.equals("E")) {
      OBJ_129.setText("Expédié");
    }
    else if (etatBon.equals("H")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("R")) {
      OBJ_129.setText("Réservé");
    }
    else if (etatBon.equals("D")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("c")) {
      OBJ_129.setText("Clôturé");
    }
    else if (etatBon.equals("p")) {
      OBJ_129.setText("Perdu");
    }
    else if (etatBon.equals("d")) {
      OBJ_129.setText("Dépassé");
    }
    else if (etatBon.equalsIgnoreCase("s")) {
      OBJ_129.setText("Signé");
    }
    else if (etatBon.equals("e")) {
      OBJ_129.setText("Envoyé");
    }
    else if (etatBon.equals("V")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("F")) {
      OBJ_129.setText("Facturé");
    }
    
    E3TEL.setEnabled(lexique.isPresent("E3TEL"));
    WATN.setVisible(!lexique.HostFieldGetData("WATN").trim().isEmpty());
    lbAttention.setVisible(WATN.isVisible());
    
    if (lbDepassement.isVisible()) {
      lbDepassement.setForeground(Color.BLACK);
    }
    else {
      lbDepassement.setForeground(Color.RED);
    }
    
    // Chantier
    if (isChantier) {
      lbDateValidite.setText("Date validité fin");
      lbDateLivraisonSouhaitee.setText("Date validité début");
      lbDateLivraisonPrevue.setVisible(false);
      E1DLPX.setVisible(false);
      lbReferenceLongue.setText("Chantier");
      E1DT2X.setBounds(165, 35, E1DT2X.getWidth(), E1DT2X.getHeight());
      E1DLSX.setBounds(165, 10, E1DLSX.getWidth(), E1DLSX.getHeight());
      lbDateRelanceDevis.setVisible(false);
      WDTREL.setVisible(false);
      if (!lexique.HostFieldGetData("WNUM").trim().isEmpty() && !lexique.HostFieldGetData("WNUM").equals("000000")) {
        btnAutresAdressesLivraison.setVisible(true);
      }
      else {
        btnAutresAdressesLivraison.setVisible(false);
      }
      lbBlanc.setVisible(true);
    }
    else {
      lbDateValidite.setText("Validité du devis");
      lbDateLivraisonSouhaitee.setText("Livraison souhaitée");
      lbDateLivraisonPrevue.setVisible(true);
      E1DLPX.setVisible(true);
      lbReferenceLongue.setText("Référence longue");
      E1DT2X.setBounds(165, 10, E1DT2X.getWidth(), E1DT2X.getHeight());
      E1DLSX.setBounds(165, 35, E1DLSX.getWidth(), E1DLSX.getHeight());
      lbDateRelanceDevis.setVisible(false);
      WDTREL.setVisible(false);
      riSousMenu19.setEnabled(false);
      riSousMenu_bt19.setVisible(false);
      btnAutresAdressesLivraison.setVisible(false);
      lbBlanc.setVisible(true);
    }
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    
    // Informations de livraison : pour les chantiers uniquement
    memoLivraisonEnlevement = null;
    if (isChantier) {
      // Pas d'informations en création car le GAP n'a pas encore attribué de numéro de document !
      if (chargerIdDocumentVenteEnCours() == null) {
        taInformationLivraisonEnlevement.setText("Non saisissable à ce stade. Validez d'abord l'entête du chantier.");
        taInformationLivraisonEnlevement.setEnabled(false);
      }
      else {
        taInformationLivraisonEnlevement.setEnabled(true);
        taInformationLivraisonEnlevement.setText(chargerInformationsLivraison());
      }
    }
    else {
      if (tbpInfos.indexOfComponent(pnlInfosLivraison) >= 0) {
        tbpInfos.remove(pnlInfosLivraison);
      }
    }
    
    // Client facturé
    snClientFac.setSession(getSession());
    snClientFac.setIdEtablissement(idEtablissement);
    snClientFac.setRechercheProspectAutorisee(true);
    snClientFac.charger(false);
    snClientFac.setSelectionParChampRPG(lexique, "E1CLFP", "E1CLFS");
    if (snClientFac.getSelection() != null && snClientFac.getSelection().isParticulier()) {
      lbNom.setText("Nom");
      lbPrenom.setText("Prénom");
    }
    else {
      lbNom.setText("Raison sociale");
      lbPrenom.setText("Complément");
    }
    snClientFac.setEnabled(false);
    
    // Client livré
    snClientLiv.setSession(getSession());
    snClientLiv.setIdEtablissement(idEtablissement);
    snClientLiv.setRechercheProspectAutorisee(true);
    snClientLiv.charger(false);
    snClientLiv.setSelectionParChampRPG(lexique, "E1CLLP", "E1CLLS");
    if (snClientLiv.getSelection() != null && snClientLiv.getSelection().isParticulier()) {
      lbNom2.setText("Nom");
      lbPrenom2.setText("Prénom");
    }
    else {
      lbNom2.setText("Raison sociale");
      lbPrenom2.setText("Complément");
    }
    snClientLiv.setEnabled(false);
    
    // Commune client facturé
    snCodePostalCommuneFac.setSession(getSession());
    snCodePostalCommuneFac.setIdEtablissement(idEtablissement);
    snCodePostalCommuneFac.charger(false);
    snCodePostalCommuneFac.setSelectionParChampRPG(lexique, "E3CDPX", "E3VILN");
    
    // Commune client livré
    snCodePostalCommuneLiv.setSession(getSession());
    snCodePostalCommuneLiv.setIdEtablissement(idEtablissement);
    snCodePostalCommuneLiv.charger(false);
    snCodePostalCommuneLiv.setSelectionParChampRPG(lexique, "E2CDPX", "E2VILN");
    
    // Pays client facturé
    snPaysFac.setSession(getSession());
    snPaysFac.setIdEtablissement(idEtablissement);
    snPaysFac.charger(false);
    snPaysFac.setSelectionParChampRPG(lexique, "E3COP");
    
    // Pays client livré
    snPaysLiv.setSession(getSession());
    snPaysLiv.setIdEtablissement(idEtablissement);
    snPaysLiv.charger(false);
    snPaysLiv.setSelectionParChampRPG(lexique, "E2COP");
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "E1MAG");
    
    // Représentant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "E1REP");
    
    // Mode d'expédition
    snModeExpedition.setSession(getSession());
    snModeExpedition.setIdEtablissement(idEtablissement);
    snModeExpedition.charger(false);
    snModeExpedition.setSelectionParChampRPG(lexique, "E1MEX");
    
    // Transporteur
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(idEtablissement);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "E1CTR");
    
    // Contact
    snContact.setSession(getSession());
    snContact.setIdEtablissement(idEtablissement);
    snContact.setTypeContact(EnumTypeContact.CLIENT);
    if (pnlAdresses.getSelectedIndex() == 0) {
      snContact.setIdClient(snClientFac.getIdSelection());
    }
    else {
      snContact.setIdClient(snClientLiv.getIdSelection());
    }
    snContact.charger(false, false);
    int numeroContact = 0;
    if (!lexique.HostFieldGetData("LRENUM").trim().isEmpty()) {
      numeroContact = Integer.parseInt(lexique.HostFieldGetData("LRENUM"));
    }
    snContact.setSelectionParId(IdContact.getInstance(numeroContact));
    
    // E-mail
    snAdresseMail.setText(lexique.HostFieldGetData("LMAIL"));
    snAdresseMail.setEnabled(false);
    
    // Panel de l'encours
    p_encours.setVisible(lexique.isTrue("(N38) AND (N39)"));
    WEPLF.setVisible(true);
    WEDEP.setVisible(true);
    WEPLF.setEnabled(false);
    WEDEP.setEnabled(false);
    
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snClientFac.renseignerChampRPG(lexique, "E2CDPX", "E2VILN");
    snClientLiv.renseignerChampRPG(lexique, "E1CLLP", "E1CLLS");
    snCodePostalCommuneFac.renseignerChampRPG(lexique, "E3CDPX", "E3VILN", false);
    snCodePostalCommuneLiv.renseignerChampRPG(lexique, "E2CDPX", "E2VILN", false);
    snPaysFac.renseignerChampRPG(lexique, "E3COP", "E3PAYN");
    snPaysLiv.renseignerChampRPG(lexique, "E2COP", "E2PAYN");
    snMagasin.renseignerChampRPG(lexique, "E1MAG");
    snRepresentant.renseignerChampRPG(lexique, "E1REP");
    snModeExpedition.renseignerChampRPG(lexique, "E1MEX");
    snTransporteur.renseignerChampRPG(lexique, "E1CTR");
    if (snContact.getSelection() != null) {
      lexique.HostFieldPutData("LRENUM", 0, snContact.getSelection().getId().getCode() + "");
    }
    // Informations de livraison pour les chantiers seulement. Pas d'informations en création car le GAP n'a pas encore attribué de numéro
    // de document !
    if (isChantier && chargerIdDocumentVenteEnCours() != null) {
      sauverInformationsLivraison(taInformationLivraisonEnlevement.getText());
    }
  }
  
  /**
   * Modification des informations de livraison
   */
  private void sauverInformationsLivraison(String pTexte) {
    if (chargerIdDocumentVenteEnCours() == null) {
      return;
    }
    // Créer le mémo s'il n'existe pas
    if (memoLivraisonEnlevement == null) {
      IdMemo idMemo = IdMemo.getInstancePourDocumentVente(chargerIdDocumentVenteEnCours(), 1);
      memoLivraisonEnlevement = new Memo(idMemo);
      memoLivraisonEnlevement.setType(EnumTypeMemo.LIVRAISON);
    }
    
    // Modifier le texte
    if (pTexte != null) {
      memoLivraisonEnlevement.setTexte(pTexte);
    }
    else {
      // Modifier le texte
      memoLivraisonEnlevement.setTexte("");
    }
    memoLivraisonEnlevement.sauver(getIdSession());
  }
  
  /**
   * Lecture des informations de livraison
   */
  private String chargerInformationsLivraison() {
    if (chargerIdDocumentVenteEnCours() == null) {
      return "";
    }
    memoLivraisonEnlevement = Memo.charger(getIdSession(), chargerIdDocumentVenteEnCours(), EnumTypeMemo.LIVRAISON);
    if (memoLivraisonEnlevement == null) {
      return "";
    }
    return memoLivraisonEnlevement.getTexte();
  }
  
  /**
   * Création d'une IdDocumentVente correspondant au document en cours à partir des informations de l'écran RPG
   */
  private IdDocumentVente chargerIdDocumentVenteEnCours() {
    if (lexique.HostFieldGetData("WNUM").trim().isEmpty() || lexique.HostFieldGetData("WNUM").equalsIgnoreCase("000000")) {
      return null;
    }
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    int numeroDocument = Integer.parseInt(lexique.HostFieldGetData("WNUM"));
    int suffixeDocument = 0;
    if (!lexique.HostFieldGetData("WSUF").trim().isEmpty()) {
      suffixeDocument = Integer.parseInt(lexique.HostFieldGetData("WSUF"));
    }
    IdDocumentVente idDocument =
        IdDocumentVente.getInstanceGenerique(idEtablissement, EnumCodeEnteteDocumentVente.DEVIS, numeroDocument, suffixeDocument, 0);
    return idDocument;
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 34);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    // client facturé
    lexique.HostCursorPut(5, 31);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /*
    private void riBoutonDetail3ActionPerformed(ActionEvent e) {
      lexique.HostCursorPut("E1REP");
      lexique.HostScreenSendKey(this, "F4");
    }
  */
  private void bt_encoursActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 80);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_reglementActionPerformed(ActionEvent e) {
    if (dialog_REGL == null) {
      dialog_REGL = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_REGL(this));
    }
    dialog_REGL.affichePopupPerso();
  }
  
  private void bt_conditionActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 42);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 10);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  /*
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 5);
    lexique.HostScreenSendKey(this, "F11");
  }*/
  
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riBoutonDetailMailActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1CLFP");
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 82);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 90);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btnAutresAdressesLivraisonActionPerformed(ActionEvent e) {
    try {
      if (lexique.HostFieldGetData("WNUM").trim().isEmpty() || lexique.HostFieldGetData("WNUM").equals("000000")) {
        return;
      }
      IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
      int numeroDocument = Integer.parseInt(lexique.HostFieldGetData("WNUM"));
      int suffixeDocument = 0;
      if (!lexique.HostFieldGetData("WSUF").trim().isEmpty()) {
        suffixeDocument = Integer.parseInt(lexique.HostFieldGetData("WSUF"));
      }
      IdDocumentVente idDocument =
          IdDocumentVente.getInstanceGenerique(idEtablissement, EnumCodeEnteteDocumentVente.DEVIS, numeroDocument, suffixeDocument, 0);
      ModeleListeAdresseLivraisonChantier modeleAdresse =
          new ModeleListeAdresseLivraisonChantier(getSession(), idDocument, EnumCodeEnteteAdresseDocument.ADRESSE_LIVRAISON_DEVIS);
      VueListeAdresseLivraisonChantier vueAdresse = new VueListeAdresseLivraisonChantier(modeleAdresse);
      vueAdresse.afficher();
      if (modeleAdresse.isSortieAvecValidation()) {
        E2NOM.setText(modeleAdresse.getAdressePrincipale().getNom());
        E2CPL.setText(modeleAdresse.getAdressePrincipale().getComplementNom());
        E2RUE.setText(modeleAdresse.getAdressePrincipale().getRue());
        E2LOC.setText(modeleAdresse.getAdressePrincipale().getLocalisation());
        snCodePostalCommuneLiv.setSelection(CodePostalCommune.getInstance(modeleAdresse.getAdressePrincipale().getCodePostalFormate(),
            modeleAdresse.getAdressePrincipale().getVille()));
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snContactValueChanged(SNComposantEvent e) {
    try {
      if (snContact.getSelection() != null) {
        snAdresseMail.setText(snContact.getSelection().getEmail());
        LTEL.setText(snContact.getSelection().getNumeroTelephone1());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snPaysFacValueChanged(SNComposantEvent e) {
    try {
      // La recherche du composant est désactivée si le code pays correspond à un pays étranger
      boolean desactivationRecherche = false;
      if (snPaysFac.getSelection() != null && !snPaysFac.getSelection().isFrance()) {
        desactivationRecherche = true;
      }
      snCodePostalCommuneFac.setDesactivationRecherche(desactivationRecherche);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snPaysLivValueChanged(SNComposantEvent e) {
    try {
      // La recherche du composant est désactivée si le code pays correspond à un pays étranger
      boolean desactivationRecherche = false;
      if (snPaysLiv.getSelection() != null && !snPaysLiv.getSelection().isFrance()) {
        desactivationRecherche = true;
      }
      snCodePostalCommuneLiv.setDesactivationRecherche(desactivationRecherche);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void pnlAdressesStateChanged(ChangeEvent e) {
    try {
      if (pnlAdresses.getSelectedIndex() == 0) {
        snContact.setIdClient(snClientFac.getIdSelection());
      }
      else {
        snContact.setIdClient(snClientLiv.getIdSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WNUM = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    WSUF = new RiZoneSortie();
    OBJ_53 = new JLabel();
    E1VDE = new XRiTextField();
    riZoneSortie4 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    bt_ecran = new JLabel();
    OBJ_129 = new RiZoneSortie();
    pnlContenu = new SNPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    p_contenu = new SNPanelContenu();
    pnlAdresses = new JTabbedPane();
    sNPanelContenu1 = new SNPanelContenu();
    lbBlanc = new SNLabelChamp();
    lbClient = new SNLabelChamp();
    snClientFac = new SNClient();
    lbNom = new SNLabelChamp();
    E3NOM = new XRiTextField();
    lbPrenom = new SNLabelChamp();
    E3CPL = new XRiTextField();
    lbAdresse1 = new SNLabelChamp();
    E3RUE = new XRiTextField();
    lbAdresse2 = new SNLabelChamp();
    E3LOC = new XRiTextField();
    lbCommune = new SNLabelChamp();
    snCodePostalCommuneFac = new SNCodePostalCommune();
    lbPays = new SNLabelChamp();
    snPaysFac = new SNPays();
    lbTelephone = new SNLabelChamp();
    E3TEL = new XRiTextField();
    sNPanelContenu2 = new SNPanelContenu();
    btnAutresAdressesLivraison = new SNBoutonDetail();
    lbClient2 = new SNLabelChamp();
    snClientLiv = new SNClient();
    lbNom2 = new SNLabelChamp();
    E2NOM = new XRiTextField();
    lbPrenom2 = new SNLabelChamp();
    E2CPL = new XRiTextField();
    lbAdresse3 = new SNLabelChamp();
    E2RUE = new XRiTextField();
    lbAdresse4 = new SNLabelChamp();
    E2LOC = new XRiTextField();
    lbCommune2 = new SNLabelChamp();
    snCodePostalCommuneLiv = new SNCodePostalCommune();
    lbPays2 = new SNLabelChamp();
    snPaysLiv = new SNPays();
    lbTelephone2 = new SNLabelChamp();
    E2TEL = new XRiTextField();
    tbpInfos = new JTabbedPane();
    panel5 = new JPanel();
    E1NHO = new XRiCheckBox();
    lbRepresentant = new SNLabelChamp();
    lbMagasin = new SNLabelChamp();
    lbReferenceCourte = new SNLabelChamp();
    E1NCC = new XRiTextField();
    lbReferenceLongue = new SNLabelChamp();
    E1RCC = new XRiTextField();
    snMagasin = new SNMagasin();
    snRepresentant = new SNRepresentant();
    pnlInfosLivraison = new SNPanelContenu();
    taInformationLivraisonEnlevement = new RiTextArea();
    xTitledPanel4 = new SNPanel();
    lbDateLivraisonSouhaitee = new SNLabelChamp();
    E1DLSX = new XRiCalendrier();
    lbDateLivraisonPrevue = new SNLabelChamp();
    E1DLPX = new XRiCalendrier();
    lbDateValidite = new SNLabelChamp();
    E1DT2X = new XRiCalendrier();
    lbDateRelanceDevis = new SNLabelChamp();
    WDTREL = new XRiCalendrier();
    p_exped = new SNPanel();
    lbFranco = new SNLabelChamp();
    lbModeExpedition = new SNLabelChamp();
    snModeExpedition = new SNModeExpedition();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    WMFRP = new XRiTextField();
    lbTitreDates = new SNLabelTitre();
    bt_encours = new SNBoutonDetail();
    sNLabelTitre2 = new SNLabelTitre();
    sNLabelTitre3 = new SNLabelTitre();
    p_encours = new SNPanel();
    lbPosition = new SNLabelChamp();
    WEPOS = new XRiTextField();
    lbPlafond = new SNLabelChamp();
    WEPLF = new XRiTextField();
    lbDepassement = new SNLabelChamp();
    lbEcart = new SNLabelChamp();
    WEDEP = new XRiTextField();
    panel1 = new JPanel();
    lbContact = new SNLabelChamp();
    snContact = new SNContact();
    lbTelContact = new SNLabelChamp();
    LTEL = new SNTexte();
    lbMailContact = new SNLabelChamp();
    snAdresseMail = new SNAdresseMail();
    panel3 = new JPanel();
    lbObservation = new SNLabelChamp();
    WOBS = new XRiTextField();
    lbAttention = new SNLabelChamp();
    WATN = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    bt_reglement = new SNBoutonDetail();
    bt_condition = new SNBoutonDetail();
    button1 = new JButton();
    E1CLFP = new RiZoneSortie();
    E1CLFS = new RiZoneSortie();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(990, 590));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setText("@WNUM@");
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");
          
          // ---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          
          // ---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");
          
          // ---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro");
          OBJ_49.setName("OBJ_49");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          WSUF.setName("WSUF");
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Vendeur");
          OBJ_53.setName("OBJ_53");
          
          // ---- E1VDE ----
          E1VDE.setComponentPopupMenu(BTD);
          E1VDE.setName("E1VDE");
          
          // ---- riZoneSortie4 ----
          riZoneSortie4.setText("@WVDNOM@");
          riZoneSortie4.setOpaque(false);
          riZoneSortie4.setName("riZoneSortie4");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(riZoneSortie4, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 19,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1ETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 19,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(riZoneSortie4,
                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(60, 0));
          p_tete_droite.setMinimumSize(new Dimension(60, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- bt_ecran ----
          bt_ecran.setBorder(BorderFactory.createEmptyBorder());
          bt_ecran.setToolTipText("Passage \u00e0 l'affichage complet");
          bt_ecran.setPreferredSize(new Dimension(30, 30));
          bt_ecran.setName("bt_ecran");
          bt_ecran.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              bt_ecranMouseClicked(e);
            }
          });
          p_tete_droite.add(bt_ecran);
          
          // ---- OBJ_129 ----
          OBJ_129.setOpaque(false);
          OBJ_129.setBorder(null);
          OBJ_129.setText("obj_129");
          OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getStyle() | Font.BOLD, OBJ_129.getFont().getSize() + 3f));
          OBJ_129.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_129.setHorizontalTextPosition(SwingConstants.LEADING);
          OBJ_129.setName("OBJ_129");
          p_tete_droite.add(OBJ_129);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout(5, 5));
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Acc\u00e8s lignes");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Sortir");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes du devis");
              riSousMenu_bt14.setToolTipText("Bloc-notes du devis : saisie libre de notes \u00e0 propos de ce devis");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes client");
              riSousMenu_bt15
                  .setToolTipText("bloc-notes du client : saisie libre de notes \u00e0 propos du client factur\u00e9 de ce devis");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Acc\u00e9s compte");
              riSousMenu_bt6.setToolTipText(
                  "Acc\u00e9s compte : acc\u00e8s en visualisation en comptabilit\u00e9 du compte de tiers correspondant au client");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique devis");
              riSousMenu_bt7.setToolTipText("Historique devis : historique des modifications apport\u00e9es \u00e0 ce devis");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Options clients");
              riSousMenu_bt8
                  .setToolTipText("Options clients : fonctionnalit\u00e9s li\u00e9es au client pour lequel est \u00e9tabli ce devis");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Valider le chantier");
              riSousMenu_bt19.setToolTipText("Valider le chantier");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Stock palette chantier");
              riSousMenu_bt17.setToolTipText("Valider le chantier");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt17ActionPerformed(e);
                }
              });
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("Navigation");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setMinimumSize(new Dimension(104, 260));
              riSousMenu1.setMaximumSize(new Dimension(104, 260));
              riSousMenu1.setPreferredSize(new Dimension(170, 260));
              riSousMenu1.setName("riSousMenu1");
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      pnlContenu.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setPreferredSize(new Dimension(830, 510));
        p_contenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        p_contenu.setBackground(new Color(239, 239, 222));
        p_contenu.setMinimumSize(new Dimension(800, 640));
        p_contenu.setOpaque(true);
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout) p_contenu.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) p_contenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) p_contenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) p_contenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlAdresses ========
        {
          pnlAdresses.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlAdresses.setBorder(new DropShadowBorder());
          pnlAdresses.setName("pnlAdresses");
          pnlAdresses.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
              pnlAdressesStateChanged(e);
            }
          });
          
          // ======== sNPanelContenu1 ========
          {
            sNPanelContenu1.setName("sNPanelContenu1");
            sNPanelContenu1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbBlanc ----
            lbBlanc.setMaximumSize(new Dimension(150, 16));
            lbBlanc.setMinimumSize(new Dimension(150, 16));
            lbBlanc.setPreferredSize(new Dimension(150, 16));
            lbBlanc.setName("lbBlanc");
            sNPanelContenu1.add(lbBlanc, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClient ----
            lbClient.setText("Client");
            lbClient.setMinimumSize(new Dimension(100, 30));
            lbClient.setPreferredSize(new Dimension(100, 30));
            lbClient.setName("lbClient");
            sNPanelContenu1.add(lbClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientFac ----
            snClientFac.setName("snClientFac");
            sNPanelContenu1.add(snClientFac, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNom ----
            lbNom.setText("Raison sociale");
            lbNom.setMinimumSize(new Dimension(100, 30));
            lbNom.setPreferredSize(new Dimension(100, 30));
            lbNom.setName("lbNom");
            sNPanelContenu1.add(lbNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E3NOM ----
            E3NOM.setComponentPopupMenu(BTD);
            E3NOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3NOM.setPreferredSize(new Dimension(315, 30));
            E3NOM.setMinimumSize(new Dimension(315, 30));
            E3NOM.setName("E3NOM");
            sNPanelContenu1.add(E3NOM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPrenom ----
            lbPrenom.setText("Compl\u00e9ment");
            lbPrenom.setMinimumSize(new Dimension(100, 30));
            lbPrenom.setPreferredSize(new Dimension(100, 30));
            lbPrenom.setName("lbPrenom");
            sNPanelContenu1.add(lbPrenom, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E3CPL ----
            E3CPL.setComponentPopupMenu(BTD);
            E3CPL.setMinimumSize(new Dimension(315, 30));
            E3CPL.setPreferredSize(new Dimension(315, 30));
            E3CPL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3CPL.setName("E3CPL");
            sNPanelContenu1.add(E3CPL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAdresse1 ----
            lbAdresse1.setText("Adresse 1");
            lbAdresse1.setMinimumSize(new Dimension(100, 30));
            lbAdresse1.setPreferredSize(new Dimension(100, 30));
            lbAdresse1.setName("lbAdresse1");
            sNPanelContenu1.add(lbAdresse1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E3RUE ----
            E3RUE.setComponentPopupMenu(BTD);
            E3RUE.setMinimumSize(new Dimension(315, 30));
            E3RUE.setPreferredSize(new Dimension(315, 30));
            E3RUE.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3RUE.setName("E3RUE");
            sNPanelContenu1.add(E3RUE, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAdresse2 ----
            lbAdresse2.setText("Adresse 2");
            lbAdresse2.setMinimumSize(new Dimension(100, 30));
            lbAdresse2.setPreferredSize(new Dimension(100, 30));
            lbAdresse2.setName("lbAdresse2");
            sNPanelContenu1.add(lbAdresse2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E3LOC ----
            E3LOC.setComponentPopupMenu(BTD);
            E3LOC.setMinimumSize(new Dimension(315, 30));
            E3LOC.setPreferredSize(new Dimension(315, 30));
            E3LOC.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3LOC.setName("E3LOC");
            sNPanelContenu1.add(E3LOC, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCommune ----
            lbCommune.setText("Commune");
            lbCommune.setMinimumSize(new Dimension(100, 30));
            lbCommune.setPreferredSize(new Dimension(100, 30));
            lbCommune.setName("lbCommune");
            sNPanelContenu1.add(lbCommune, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCodePostalCommuneFac ----
            snCodePostalCommuneFac.setName("snCodePostalCommuneFac");
            sNPanelContenu1.add(snCodePostalCommuneFac, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPays ----
            lbPays.setText("Pays");
            lbPays.setMinimumSize(new Dimension(100, 30));
            lbPays.setPreferredSize(new Dimension(100, 30));
            lbPays.setName("lbPays");
            sNPanelContenu1.add(lbPays, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPaysFac ----
            snPaysFac.setName("snPaysFac");
            snPaysFac.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPaysFacValueChanged(e);
              }
            });
            sNPanelContenu1.add(snPaysFac, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTelephone ----
            lbTelephone.setText("T\u00e9l\u00e9phone");
            lbTelephone.setMinimumSize(new Dimension(100, 30));
            lbTelephone.setPreferredSize(new Dimension(100, 30));
            lbTelephone.setName("lbTelephone");
            sNPanelContenu1.add(lbTelephone, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E3TEL ----
            E3TEL.setComponentPopupMenu(BTD);
            E3TEL.setMinimumSize(new Dimension(150, 30));
            E3TEL.setPreferredSize(new Dimension(150, 30));
            E3TEL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3TEL.setName("E3TEL");
            sNPanelContenu1.add(E3TEL, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlAdresses.addTab("Adresse de facturation", sNPanelContenu1);
          
          // ======== sNPanelContenu2 ========
          {
            sNPanelContenu2.setName("sNPanelContenu2");
            sNPanelContenu2.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelContenu2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelContenu2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelContenu2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanelContenu2.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- btnAutresAdressesLivraison ----
            btnAutresAdressesLivraison.setToolTipText("Autres adresses de livraison");
            btnAutresAdressesLivraison.setName("btnAutresAdressesLivraison");
            btnAutresAdressesLivraison.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnAutresAdressesLivraisonActionPerformed(e);
              }
            });
            sNPanelContenu2.add(btnAutresAdressesLivraison, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClient2 ----
            lbClient2.setText("Client");
            lbClient2.setMinimumSize(new Dimension(100, 30));
            lbClient2.setPreferredSize(new Dimension(100, 30));
            lbClient2.setName("lbClient2");
            sNPanelContenu2.add(lbClient2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientLiv ----
            snClientLiv.setName("snClientLiv");
            sNPanelContenu2.add(snClientLiv, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNom2 ----
            lbNom2.setText("Raison sociale");
            lbNom2.setMinimumSize(new Dimension(100, 30));
            lbNom2.setPreferredSize(new Dimension(100, 30));
            lbNom2.setName("lbNom2");
            sNPanelContenu2.add(lbNom2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E2NOM ----
            E2NOM.setComponentPopupMenu(BTD);
            E2NOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2NOM.setPreferredSize(new Dimension(315, 30));
            E2NOM.setMinimumSize(new Dimension(315, 30));
            E2NOM.setName("E2NOM");
            sNPanelContenu2.add(E2NOM, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPrenom2 ----
            lbPrenom2.setText("Compl\u00e9ment");
            lbPrenom2.setMinimumSize(new Dimension(100, 30));
            lbPrenom2.setPreferredSize(new Dimension(100, 30));
            lbPrenom2.setName("lbPrenom2");
            sNPanelContenu2.add(lbPrenom2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E2CPL ----
            E2CPL.setComponentPopupMenu(BTD);
            E2CPL.setMinimumSize(new Dimension(315, 30));
            E2CPL.setPreferredSize(new Dimension(315, 30));
            E2CPL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2CPL.setName("E2CPL");
            sNPanelContenu2.add(E2CPL, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAdresse3 ----
            lbAdresse3.setText("Adresse 1");
            lbAdresse3.setMinimumSize(new Dimension(100, 30));
            lbAdresse3.setPreferredSize(new Dimension(100, 30));
            lbAdresse3.setName("lbAdresse3");
            sNPanelContenu2.add(lbAdresse3, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E2RUE ----
            E2RUE.setComponentPopupMenu(BTD);
            E2RUE.setMinimumSize(new Dimension(315, 30));
            E2RUE.setPreferredSize(new Dimension(315, 30));
            E2RUE.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2RUE.setName("E2RUE");
            sNPanelContenu2.add(E2RUE, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAdresse4 ----
            lbAdresse4.setText("Adresse 2");
            lbAdresse4.setMinimumSize(new Dimension(100, 30));
            lbAdresse4.setPreferredSize(new Dimension(100, 30));
            lbAdresse4.setName("lbAdresse4");
            sNPanelContenu2.add(lbAdresse4, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E2LOC ----
            E2LOC.setComponentPopupMenu(BTD);
            E2LOC.setMinimumSize(new Dimension(315, 30));
            E2LOC.setPreferredSize(new Dimension(315, 30));
            E2LOC.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2LOC.setName("E2LOC");
            sNPanelContenu2.add(E2LOC, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCommune2 ----
            lbCommune2.setText("Commune");
            lbCommune2.setMinimumSize(new Dimension(100, 30));
            lbCommune2.setPreferredSize(new Dimension(100, 30));
            lbCommune2.setName("lbCommune2");
            sNPanelContenu2.add(lbCommune2, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snCodePostalCommuneLiv ----
            snCodePostalCommuneLiv.setName("snCodePostalCommuneLiv");
            sNPanelContenu2.add(snCodePostalCommuneLiv, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPays2 ----
            lbPays2.setText("Pays");
            lbPays2.setMinimumSize(new Dimension(100, 30));
            lbPays2.setPreferredSize(new Dimension(100, 30));
            lbPays2.setName("lbPays2");
            sNPanelContenu2.add(lbPays2, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPaysLiv ----
            snPaysLiv.setName("snPaysLiv");
            snPaysLiv.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPaysLivValueChanged(e);
              }
            });
            sNPanelContenu2.add(snPaysLiv, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTelephone2 ----
            lbTelephone2.setText("T\u00e9l\u00e9phone");
            lbTelephone2.setMinimumSize(new Dimension(100, 30));
            lbTelephone2.setPreferredSize(new Dimension(100, 30));
            lbTelephone2.setName("lbTelephone2");
            sNPanelContenu2.add(lbTelephone2, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E2TEL ----
            E2TEL.setComponentPopupMenu(BTD);
            E2TEL.setMinimumSize(new Dimension(150, 30));
            E2TEL.setPreferredSize(new Dimension(150, 30));
            E2TEL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2TEL.setName("E2TEL");
            sNPanelContenu2.add(E2TEL, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlAdresses.addTab("Adresses de livraison", sNPanelContenu2);
        }
        p_contenu.add(pnlAdresses, new GridBagConstraints(0, 0, 2, 4, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== tbpInfos ========
        {
          tbpInfos.setFont(new Font("sansserif", Font.PLAIN, 14));
          tbpInfos.setMinimumSize(new Dimension(445, 240));
          tbpInfos.setPreferredSize(new Dimension(445, 240));
          tbpInfos.setName("tbpInfos");
          
          // ======== panel5 ========
          {
            panel5.setBackground(new Color(239, 239, 222));
            panel5.setBorder(new EmptyBorder(10, 10, 10, 10));
            panel5.setMinimumSize(new Dimension(495, 235));
            panel5.setPreferredSize(new Dimension(495, 235));
            panel5.setName("panel5");
            panel5.setLayout(new GridBagLayout());
            ((GridBagLayout) panel5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) panel5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) panel5.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) panel5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- E1NHO ----
            E1NHO.setText("Bloqu\u00e9");
            E1NHO.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1NHO.setMinimumSize(new Dimension(66, 30));
            E1NHO.setPreferredSize(new Dimension(66, 30));
            E1NHO.setName("E1NHO");
            panel5.add(E1NHO, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbRepresentant ----
            lbRepresentant.setText("Repr\u00e9sentant");
            lbRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbRepresentant.setName("lbRepresentant");
            panel5.add(lbRepresentant, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbMagasin.setName("lbMagasin");
            panel5.add(lbMagasin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbReferenceCourte ----
            lbReferenceCourte.setText("R\u00e9f\u00e9rence courte");
            lbReferenceCourte.setMaximumSize(new Dimension(130, 16));
            lbReferenceCourte.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbReferenceCourte.setName("lbReferenceCourte");
            panel5.add(lbReferenceCourte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E1NCC ----
            E1NCC.setComponentPopupMenu(BTD);
            E1NCC.setPreferredSize(new Dimension(100, 28));
            E1NCC.setMinimumSize(new Dimension(100, 28));
            E1NCC.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1NCC.setName("E1NCC");
            panel5.add(E1NCC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbReferenceLongue ----
            lbReferenceLongue.setText("R\u00e9f\u00e9rence longue");
            lbReferenceLongue.setMaximumSize(new Dimension(130, 16));
            lbReferenceLongue.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbReferenceLongue.setName("lbReferenceLongue");
            panel5.add(lbReferenceLongue, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- E1RCC ----
            E1RCC.setComponentPopupMenu(null);
            E1RCC.setPreferredSize(new Dimension(300, 28));
            E1RCC.setMinimumSize(new Dimension(300, 28));
            E1RCC.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1RCC.setName("E1RCC");
            panel5.add(E1RCC, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            panel5.add(snMagasin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snRepresentant ----
            snRepresentant.setName("snRepresentant");
            panel5.add(snRepresentant, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          tbpInfos.addTab("Informations principales", panel5);
          
          // ======== pnlInfosLivraison ========
          {
            pnlInfosLivraison.setName("pnlInfosLivraison");
            pnlInfosLivraison.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlInfosLivraison.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlInfosLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlInfosLivraison.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlInfosLivraison.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ---- taInformationLivraisonEnlevement ----
            taInformationLivraisonEnlevement.setFont(new Font("sansserif", Font.PLAIN, 14));
            taInformationLivraisonEnlevement.setName("taInformationLivraisonEnlevement");
            pnlInfosLivraison.add(taInformationLivraisonEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tbpInfos.addTab("Informations livraison", pnlInfosLivraison);
        }
        p_contenu.add(tbpInfos, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== xTitledPanel4 ========
        {
          xTitledPanel4.setBorder(new TitledBorder(""));
          xTitledPanel4.setName("xTitledPanel4");
          xTitledPanel4.setLayout(new GridBagLayout());
          ((GridBagLayout) xTitledPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) xTitledPanel4.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) xTitledPanel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) xTitledPanel4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbDateLivraisonSouhaitee ----
          lbDateLivraisonSouhaitee.setText("Livraison souhait\u00e9e");
          lbDateLivraisonSouhaitee.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateLivraisonSouhaitee.setName("lbDateLivraisonSouhaitee");
          xTitledPanel4.add(lbDateLivraisonSouhaitee, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- E1DLSX ----
          E1DLSX.setComponentPopupMenu(null);
          E1DLSX.setPreferredSize(new Dimension(115, 30));
          E1DLSX.setMinimumSize(new Dimension(115, 30));
          E1DLSX.setMaximumSize(new Dimension(115, 30));
          E1DLSX.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1DLSX.setName("E1DLSX");
          xTitledPanel4.add(E1DLSX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbDateLivraisonPrevue ----
          lbDateLivraisonPrevue.setText("Livraison pr\u00e9vue");
          lbDateLivraisonPrevue.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateLivraisonPrevue.setName("lbDateLivraisonPrevue");
          xTitledPanel4.add(lbDateLivraisonPrevue, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- E1DLPX ----
          E1DLPX.setComponentPopupMenu(null);
          E1DLPX.setPreferredSize(new Dimension(115, 30));
          E1DLPX.setMinimumSize(new Dimension(115, 30));
          E1DLPX.setMaximumSize(new Dimension(115, 30));
          E1DLPX.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1DLPX.setName("E1DLPX");
          xTitledPanel4.add(E1DLPX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDateValidite ----
          lbDateValidite.setText("Validit\u00e9 du devis");
          lbDateValidite.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateValidite.setName("lbDateValidite");
          xTitledPanel4.add(lbDateValidite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- E1DT2X ----
          E1DT2X.setComponentPopupMenu(null);
          E1DT2X.setPreferredSize(new Dimension(115, 30));
          E1DT2X.setMinimumSize(new Dimension(115, 30));
          E1DT2X.setMaximumSize(new Dimension(115, 30));
          E1DT2X.setFont(new Font("sansserif", Font.PLAIN, 14));
          E1DT2X.setName("E1DT2X");
          xTitledPanel4.add(E1DT2X, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDateRelanceDevis ----
          lbDateRelanceDevis.setText("Date de relance du devis");
          lbDateRelanceDevis.setMinimumSize(new Dimension(100, 28));
          lbDateRelanceDevis.setPreferredSize(new Dimension(140, 28));
          lbDateRelanceDevis.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateRelanceDevis.setName("lbDateRelanceDevis");
          xTitledPanel4.add(lbDateRelanceDevis, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WDTREL ----
          WDTREL.setComponentPopupMenu(null);
          WDTREL.setPreferredSize(new Dimension(115, 30));
          WDTREL.setMinimumSize(new Dimension(115, 30));
          WDTREL.setMaximumSize(new Dimension(115, 30));
          WDTREL.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDTREL.setName("WDTREL");
          xTitledPanel4.add(WDTREL, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(xTitledPanel4, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== p_exped ========
        {
          p_exped.setBorder(new TitledBorder(""));
          p_exped.setName("p_exped");
          p_exped.setLayout(new GridBagLayout());
          ((GridBagLayout) p_exped.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) p_exped.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) p_exped.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) p_exped.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbFranco ----
          lbFranco.setText("Franco");
          lbFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbFranco.setName("lbFranco");
          p_exped.add(lbFranco, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbModeExpedition ----
          lbModeExpedition.setText("Mode");
          lbModeExpedition.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbModeExpedition.setName("lbModeExpedition");
          p_exped.add(lbModeExpedition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snModeExpedition ----
          snModeExpedition.setName("snModeExpedition");
          p_exped.add(snModeExpedition, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTransporteur ----
          lbTransporteur.setText("Transporteur");
          lbTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTransporteur.setName("lbTransporteur");
          p_exped.add(lbTransporteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTransporteur ----
          snTransporteur.setName("snTransporteur");
          p_exped.add(snTransporteur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WMFRP ----
          WMFRP.setMinimumSize(new Dimension(70, 30));
          WMFRP.setPreferredSize(new Dimension(70, 30));
          WMFRP.setFont(new Font("sansserif", Font.PLAIN, 14));
          WMFRP.setName("WMFRP");
          p_exped.add(WMFRP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        p_contenu.add(p_exped, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ---- lbTitreDates ----
        lbTitreDates.setText("Dates");
        lbTitreDates.setPreferredSize(new Dimension(150, 20));
        lbTitreDates.setMinimumSize(new Dimension(150, 20));
        lbTitreDates.setName("lbTitreDates");
        p_contenu.add(lbTitreDates, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ---- bt_encours ----
        bt_encours.setToolTipText("Encours");
        bt_encours.setName("bt_encours");
        bt_encours.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_encoursActionPerformed(e);
          }
        });
        p_contenu.add(bt_encours, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 2, 5), 0, 0));
        
        // ---- sNLabelTitre2 ----
        sNLabelTitre2.setText("Encours");
        sNLabelTitre2.setName("sNLabelTitre2");
        p_contenu.add(sNLabelTitre2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 5), 0, 0));
        
        // ---- sNLabelTitre3 ----
        sNLabelTitre3.setText("Exp\u00e9dition");
        sNLabelTitre3.setName("sNLabelTitre3");
        p_contenu.add(sNLabelTitre3, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== p_encours ========
        {
          p_encours.setBorder(new TitledBorder(""));
          p_encours.setName("p_encours");
          p_encours.setLayout(new GridBagLayout());
          ((GridBagLayout) p_encours.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) p_encours.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) p_encours.getLayout()).columnWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) p_encours.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPosition ----
          lbPosition.setText("Position");
          lbPosition.setMinimumSize(new Dimension(100, 30));
          lbPosition.setPreferredSize(new Dimension(100, 30));
          lbPosition.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPosition.setName("lbPosition");
          p_encours.add(lbPosition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WEPOS ----
          WEPOS.setMinimumSize(new Dimension(70, 30));
          WEPOS.setPreferredSize(new Dimension(70, 30));
          WEPOS.setFont(new Font("sansserif", Font.PLAIN, 14));
          WEPOS.setName("WEPOS");
          p_encours.add(WEPOS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPlafond ----
          lbPlafond.setText("Plafond");
          lbPlafond.setMinimumSize(new Dimension(100, 30));
          lbPlafond.setPreferredSize(new Dimension(100, 30));
          lbPlafond.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPlafond.setName("lbPlafond");
          p_encours.add(lbPlafond, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WEPLF ----
          WEPLF.setMinimumSize(new Dimension(70, 30));
          WEPLF.setPreferredSize(new Dimension(70, 30));
          WEPLF.setFont(new Font("sansserif", Font.PLAIN, 14));
          WEPLF.setName("WEPLF");
          p_encours.add(WEPLF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbDepassement ----
          lbDepassement.setText("D\u00e9passement");
          lbDepassement.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDepassement.setForeground(new Color(215, 17, 17));
          lbDepassement.setMinimumSize(new Dimension(100, 30));
          lbDepassement.setPreferredSize(new Dimension(100, 30));
          lbDepassement.setName("lbDepassement");
          p_encours.add(lbDepassement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbEcart ----
          lbEcart.setText("Ecart");
          lbEcart.setMinimumSize(new Dimension(100, 28));
          lbEcart.setPreferredSize(new Dimension(100, 28));
          lbEcart.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbEcart.setName("lbEcart");
          p_encours.add(lbEcart, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WEDEP ----
          WEDEP.setMinimumSize(new Dimension(70, 30));
          WEDEP.setPreferredSize(new Dimension(70, 30));
          WEDEP.setFont(new Font("sansserif", Font.PLAIN, 14));
          WEDEP.setName("WEDEP");
          p_encours.add(WEDEP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        p_contenu.add(p_encours, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
          panel1.setName("panel1");
          panel1.setLayout(new GridBagLayout());
          ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbContact ----
          lbContact.setText("Contact");
          lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbContact.setPreferredSize(new Dimension(110, 30));
          lbContact.setMinimumSize(new Dimension(110, 30));
          lbContact.setName("lbContact");
          panel1.add(lbContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snContact ----
          snContact.setName("snContact");
          snContact.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snContactValueChanged(e);
            }
          });
          panel1.add(snContact, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTelContact ----
          lbTelContact.setText("T\u00e9l\u00e9phone");
          lbTelContact.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTelContact.setPreferredSize(new Dimension(150, 28));
          lbTelContact.setMinimumSize(new Dimension(150, 28));
          lbTelContact.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTelContact.setName("lbTelContact");
          panel1.add(lbTelContact, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- LTEL ----
          LTEL.setText("@LTEL@");
          LTEL.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
          LTEL.setMinimumSize(new Dimension(130, 28));
          LTEL.setPreferredSize(new Dimension(130, 28));
          LTEL.setFont(new Font("sansserif", Font.PLAIN, 14));
          LTEL.setEnabled(false);
          LTEL.setName("LTEL");
          panel1.add(LTEL, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMailContact ----
          lbMailContact.setText("E-mail");
          lbMailContact.setMinimumSize(new Dimension(110, 30));
          lbMailContact.setPreferredSize(new Dimension(110, 30));
          lbMailContact.setName("lbMailContact");
          panel1.add(lbMailContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snAdresseMail ----
          snAdresseMail.setEnabled(false);
          snAdresseMail.setName("snAdresseMail");
          panel1.add(snAdresseMail, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
        }
        p_contenu.add(panel1, new GridBagConstraints(0, 7, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(new GridBagLayout());
          ((GridBagLayout) panel3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) panel3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) panel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) panel3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbObservation ----
          lbObservation.setText("Observation");
          lbObservation.setPreferredSize(new Dimension(110, 30));
          lbObservation.setMinimumSize(new Dimension(110, 30));
          lbObservation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbObservation.setName("lbObservation");
          panel3.add(lbObservation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WOBS ----
          WOBS.setMinimumSize(new Dimension(300, 30));
          WOBS.setPreferredSize(new Dimension(300, 30));
          WOBS.setFont(new Font("sansserif", Font.PLAIN, 14));
          WOBS.setName("WOBS");
          panel3.add(WOBS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbAttention ----
          lbAttention.setText("Attention");
          lbAttention.setName("lbAttention");
          panel3.add(lbAttention, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WATN ----
          WATN.setMinimumSize(new Dimension(150, 30));
          WATN.setPreferredSize(new Dimension(150, 30));
          WATN.setFont(new Font("sansserif", Font.PLAIN, 14));
          WATN.setName("WATN");
          panel3.add(WATN, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu.add(panel3, new GridBagConstraints(0, 6, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 2, 0), 0, 0));
      }
      pnlContenu.add(p_contenu, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("M\u00e9morisation curseur");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    
    // ---- bt_reglement ----
    bt_reglement.setToolTipText("R\u00e9glements");
    bt_reglement.setName("bt_reglement");
    bt_reglement.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_reglementActionPerformed(e);
      }
    });
    
    // ---- bt_condition ----
    bt_condition.setToolTipText("Conditions");
    bt_condition.setName("bt_condition");
    bt_condition.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_conditionActionPerformed(e);
      }
    });
    
    // ---- button1 ----
    button1.setText("Pied de bon : ne pas supprimer");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });
    
    // ---- E1CLFP ----
    E1CLFP.setComponentPopupMenu(null);
    E1CLFP.setText("@E1CLFP@");
    E1CLFP.setPreferredSize(new Dimension(70, 28));
    E1CLFP.setMinimumSize(new Dimension(70, 28));
    E1CLFP.setName("E1CLFP");
    
    // ---- E1CLFS ----
    E1CLFS.setComponentPopupMenu(null);
    E1CLFS.setText("@E1CLFS@");
    E1CLFS.setPreferredSize(new Dimension(40, 28));
    E1CLFS.setMinimumSize(new Dimension(40, 28));
    E1CLFS.setName("E1CLFS");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WNUM;
  private RiZoneSortie E1ETB;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private RiZoneSortie WSUF;
  private JLabel OBJ_53;
  private XRiTextField E1VDE;
  private RiZoneSortie riZoneSortie4;
  private JPanel p_tete_droite;
  private JLabel bt_ecran;
  private RiZoneSortie OBJ_129;
  private SNPanel pnlContenu;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private SNPanelContenu p_contenu;
  private JTabbedPane pnlAdresses;
  private SNPanelContenu sNPanelContenu1;
  private SNLabelChamp lbBlanc;
  private SNLabelChamp lbClient;
  private SNClient snClientFac;
  private SNLabelChamp lbNom;
  private XRiTextField E3NOM;
  private SNLabelChamp lbPrenom;
  private XRiTextField E3CPL;
  private SNLabelChamp lbAdresse1;
  private XRiTextField E3RUE;
  private SNLabelChamp lbAdresse2;
  private XRiTextField E3LOC;
  private SNLabelChamp lbCommune;
  private SNCodePostalCommune snCodePostalCommuneFac;
  private SNLabelChamp lbPays;
  private SNPays snPaysFac;
  private SNLabelChamp lbTelephone;
  private XRiTextField E3TEL;
  private SNPanelContenu sNPanelContenu2;
  private SNBoutonDetail btnAutresAdressesLivraison;
  private SNLabelChamp lbClient2;
  private SNClient snClientLiv;
  private SNLabelChamp lbNom2;
  private XRiTextField E2NOM;
  private SNLabelChamp lbPrenom2;
  private XRiTextField E2CPL;
  private SNLabelChamp lbAdresse3;
  private XRiTextField E2RUE;
  private SNLabelChamp lbAdresse4;
  private XRiTextField E2LOC;
  private SNLabelChamp lbCommune2;
  private SNCodePostalCommune snCodePostalCommuneLiv;
  private SNLabelChamp lbPays2;
  private SNPays snPaysLiv;
  private SNLabelChamp lbTelephone2;
  private XRiTextField E2TEL;
  private JTabbedPane tbpInfos;
  private JPanel panel5;
  private XRiCheckBox E1NHO;
  private SNLabelChamp lbRepresentant;
  private SNLabelChamp lbMagasin;
  private SNLabelChamp lbReferenceCourte;
  private XRiTextField E1NCC;
  private SNLabelChamp lbReferenceLongue;
  private XRiTextField E1RCC;
  private SNMagasin snMagasin;
  private SNRepresentant snRepresentant;
  private SNPanelContenu pnlInfosLivraison;
  private RiTextArea taInformationLivraisonEnlevement;
  private SNPanel xTitledPanel4;
  private SNLabelChamp lbDateLivraisonSouhaitee;
  private XRiCalendrier E1DLSX;
  private SNLabelChamp lbDateLivraisonPrevue;
  private XRiCalendrier E1DLPX;
  private SNLabelChamp lbDateValidite;
  private XRiCalendrier E1DT2X;
  private SNLabelChamp lbDateRelanceDevis;
  private XRiCalendrier WDTREL;
  private SNPanel p_exped;
  private SNLabelChamp lbFranco;
  private SNLabelChamp lbModeExpedition;
  private SNModeExpedition snModeExpedition;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private XRiTextField WMFRP;
  private SNLabelTitre lbTitreDates;
  private SNBoutonDetail bt_encours;
  private SNLabelTitre sNLabelTitre2;
  private SNLabelTitre sNLabelTitre3;
  private SNPanel p_encours;
  private SNLabelChamp lbPosition;
  private XRiTextField WEPOS;
  private SNLabelChamp lbPlafond;
  private XRiTextField WEPLF;
  private SNLabelChamp lbDepassement;
  private SNLabelChamp lbEcart;
  private XRiTextField WEDEP;
  private JPanel panel1;
  private SNLabelChamp lbContact;
  private SNContact snContact;
  private SNLabelChamp lbTelContact;
  private SNTexte LTEL;
  private SNLabelChamp lbMailContact;
  private SNAdresseMail snAdresseMail;
  private JPanel panel3;
  private SNLabelChamp lbObservation;
  private XRiTextField WOBS;
  private SNLabelChamp lbAttention;
  private XRiTextField WATN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_20;
  private SNBoutonDetail bt_reglement;
  private SNBoutonDetail bt_condition;
  private JButton button1;
  private RiZoneSortie E1CLFP;
  private RiZoneSortie E1CLFS;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
