
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDocument;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiMailto;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_D3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] options_sans_edition = { "DFA", "DER", "ALC", "RGL", "MAR", "AFS", "AFA", "COL", "VAR", "COR", "LDP", "DIS", "EXT",
      "ACT", "HIS", "MES", "AUT", "DVD", "PER", "AVN", "BON" };
  private String[] _LD01_Title = { "HLD01", };
  private String[][] _LD01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _LD01_Width = { 900, };
  // private String[] _LIST_Top=null;
  private String[] listeCellules =
      { "LD01", "LD02", "LD03", "LD04", "LD05", "LD06", "LD07", "LD08", "LD09", "LD10", "LD11", "LD12", "LD13", "LD14", "LD15" };
  private Color[][] couleurLigne = new Color[listeCellules.length][1];
  private Color[][] couleurFond = new Color[15][1];
  private boolean isToutesOptions = true;
  private int premierPassage = 0;
  private RiPanelNav riPanelNav1 = null;
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  public boolean isDevis;
  public boolean isChantier;
  private String libelleDirectOuInterne = "";
  
  /**
   * Constructeur.
   */
  public VGVM11FX_D3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LD01.setAspectTable(null, _LD01_Title, _LD01_Data, _LD01_Width, false, null, couleurLigne, couleurFond, null);
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/fin_p.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/fin_p.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    riMenu_bt_impr.setIcon(lexique.chargerImage("images/impression.png", true));
    button_sms.setIcon(lexique.chargerImage("images/sms.png", true));
    
    // Navigation graphique
    riPanelNav1 = new RiPanelNav();
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de bon", false, retourentete);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du bon", false, bouton_retour);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de bon", true, null);
    riSousMenu18.add(riPanelNav1);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@  fin de traitement")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E3NOM@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@  @MALIB@")).trim()));
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DEV1@")).trim());
    riBouton1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP1@ - @LIBOPT1@")).trim());
    riBouton2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP2@ - @LIBOPT2@")).trim());
    riBouton3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP3@ - @LIBOPT3@")).trim());
    riBouton4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP4@ - @LIBOPT4@")).trim());
    riBouton5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP5@ - @LIBOPT5@")).trim());
    riBouton6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP6@ - @LIBOPT6@")).trim());
    riBouton7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP7@ - @LIBOPT7@")).trim());
    riBouton8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP8@ - @LIBOPT8@")).trim());
    riBouton9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP9@ - @LIBOPT9@")).trim());
    riBouton10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP10@ - @LIBOPT10@")).trim());
    riBouton11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP11@ - @LIBOPT11@")).trim());
    riBouton12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOPP12@ - @LIBOPT12@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    WTHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTHT@")).trim());
    WTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTVA@")).trim());
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    WSL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSL1@")).trim());
    WSL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSL2@")).trim());
    WSL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSL3@")).trim());
    E1TL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TL1@")).trim());
    E1TL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TL2@")).trim());
    E1TL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TL3@")).trim());
    WLT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLT1@")).trim());
    WLT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLT2@")).trim());
    WLT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLT3@")).trim());
    OBJ_89.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1DEV@")).trim());
    OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTVA1@")).trim());
    OBJ_79.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTVA2@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTVA3@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TFLIB@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BASX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    gererCouleurListe();
    couleurFond[13][0] = new Color(170, 197, 225);
    
    setDiverses();
    
    int numClientLivre = 0;
    if (!lexique.HostFieldGetData("E1CLLP").trim().isEmpty()) {
      numClientLivre = Integer.parseInt(lexique.HostFieldGetData("E1CLLP"));
    }
    
    isDevis = lexique.HostFieldGetData("ISDEVIS").equals("1");
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    
    riSousMenu1.setEnabled(numClientLivre > 999900);
    riSousMenu6.setEnabled(!isDevis);
    riSousMenu7.setEnabled(!isDevis);
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    riBouton13.setVisible(!isDevis);
    OBJ_81.setVisible(lexique.isPresent("CTVA3"));
    OBJ_79.setVisible(lexique.isPresent("CTVA2"));
    OBJ_77.setVisible(lexique.isPresent("CTVA1"));
    WSUF.setVisible(lexique.isPresent("WSUF"));
    E1ETB.setVisible(lexique.isPresent("E1ETB"));
    OBJ_89.setVisible(lexique.isPresent("E1DEV"));
    OBJ_36.setVisible(lexique.isPresent("E1DEV1"));
    WLT3.setVisible(lexique.isPresent("WLT3"));
    WLT2.setVisible(lexique.isPresent("WLT2"));
    WLT1.setVisible(lexique.isPresent("WLT1"));
    OBJ_88.setVisible(lexique.isPresent("BASX"));
    E1TL3.setVisible(lexique.isPresent("E1TL3"));
    E1TL2.setVisible(lexique.isPresent("E1TL2"));
    E1TL1.setVisible(lexique.isPresent("E1TL1"));
    WSL3.setVisible(lexique.isPresent("WSL3"));
    WSL2.setVisible(lexique.isPresent("WSL2"));
    WSL1.setVisible(lexique.isPresent("WSL1"));
    
    // Label état du bon
    String etatBon = lexique.HostFieldGetData("ETABON");
    OBJ_129.setText("");
    if (etatBon.equals("C")) {
      OBJ_129.setText("Comptabilisé");
    }
    else if (etatBon.equals("T")) {
      OBJ_129.setText("Commande type");
    }
    else if (etatBon.equals("P")) {
      OBJ_129.setText("Positionnement");
    }
    else if (etatBon.equals("A")) {
      OBJ_129.setText("Attente");
    }
    else if (etatBon.equals("E")) {
      OBJ_129.setText("Expédié");
    }
    else if (etatBon.equals("H")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("R")) {
      OBJ_129.setText("Réservé");
    }
    else if (etatBon.equals("D")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("c")) {
      OBJ_129.setText("Clôturé");
    }
    else if (etatBon.equals("p")) {
      OBJ_129.setText("Perdu");
    }
    else if (etatBon.equals("d")) {
      OBJ_129.setText("Dépassé");
    }
    else if (etatBon.equalsIgnoreCase("s")) {
      OBJ_129.setText("Signé");
    }
    else if (etatBon.equals("e")) {
      OBJ_129.setText("Envoyé");
    }
    else if (etatBon.equals("V")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("F")) {
      OBJ_129.setText("Facturé");
    }
    
    // ToolTipText suivant l'état
    if (isDevis) {
      riMenu_bt1.setToolTipText("Mettre ce devis en attente");
    }
    else if (etatBon.equals("F")) {
      riMenu_bt1.setToolTipText("Mettre cette facture en attente");
    }
    else {
      riMenu_bt1.setToolTipText("Mettre ce bon en attente");
    }
    
    // Les options de fin de traitement
    if (!lexique.HostFieldGetData("TOPP1").trim().equalsIgnoreCase("")) {
      panel4.setVisible(true);
      panel5.setVisible(true);
      navig_valid.setVisible(true);
      /* ALors... j'ai remis la visibilité sur les 3emes options car des fois deux options ... à suivre*/
      riBouton1.setVisible(!lexique.HostFieldGetData("TOPP1").trim().equalsIgnoreCase(""));
      riBouton2.setVisible(!lexique.HostFieldGetData("TOPP2").trim().equalsIgnoreCase(""));
      riBouton3.setVisible(!lexique.HostFieldGetData("TOPP3").trim().equalsIgnoreCase(""));
      riBouton4.setEnabled(!lexique.HostFieldGetData("TOPP4").trim().equalsIgnoreCase(""));
      riBouton5.setEnabled(!lexique.HostFieldGetData("TOPP5").trim().equalsIgnoreCase(""));
      riBouton6.setEnabled(!lexique.HostFieldGetData("TOPP6").trim().equalsIgnoreCase(""));
      riBouton7.setEnabled(!lexique.HostFieldGetData("TOPP7").trim().equalsIgnoreCase(""));
      riBouton8.setEnabled(!lexique.HostFieldGetData("TOPP8").trim().equalsIgnoreCase(""));
      riBouton9.setEnabled(!lexique.HostFieldGetData("TOPP9").trim().equalsIgnoreCase(""));
      riBouton10.setEnabled(!lexique.HostFieldGetData("TOPP10").trim().equalsIgnoreCase(""));
      riBouton11.setEnabled(!lexique.HostFieldGetData("TOPP11").trim().equalsIgnoreCase(""));
      riBouton12.setEnabled(!lexique.HostFieldGetData("TOPP12").trim().equalsIgnoreCase(""));
      bt_plus.setVisible(
          riBouton4.isEnabled() || riBouton5.isEnabled() || riBouton6.isEnabled() || riBouton7.isEnabled() || riBouton8.isEnabled()
              || riBouton9.isEnabled() || riBouton10.isEnabled() || riBouton11.isEnabled() || riBouton12.isEnabled());
      if (premierPassage == 0) {
        switchOptionsPlus();
        premierPassage++;
      }
      panel4.setSize(300, panel6.getHeight() + 80);
      p_contenu.setPreferredSize(new Dimension(1000, 590));
      panel1.setSize(670, panel1.getHeight());
      panel2.setSize(670, panel2.getHeight());
      panel7.setSize(670, panel7.getHeight());
      SCROLLPANE_LIST2.setSize(625, 270);
      LD01.setSize(625, 270);
    }
    else {
      panel4.setVisible(false);
      panel5.setVisible(false);
      navig_valid.setVisible(false);
      panel1.setSize(980, panel1.getHeight());
      panel2.setSize(980, panel2.getHeight());
      panel7.setSize(980, panel7.getHeight());
      SCROLLPANE_LIST2.setSize(930, 270);
      LD01.setSize(930, 270);
    }
    
    riMenu1.setVisible(lexique.isTrue("N46"));
    riSousMenuF_dupli2.setEnabled(lexique.isTrue("N46"));
    E1TTC.setVisible(lexique.isPresent("E1TTC"));
    WTVA.setVisible(lexique.isPresent("WTVA"));
    WTHT.setVisible(lexique.isPresent("WTHT"));
    WDATEX.setVisible(lexique.isPresent("WDATEX"));
    OBJ_87.setVisible(lexique.isPresent("DVLIBR"));
    OBJ_25.setVisible(lexique.isPresent("E3NOM"));
    
    if (V06F3.getText().equalsIgnoreCase("?µ*")) {
      V06F3.setText("");
    }
    
    testerOption(V06F3.getText().trim());
    
    // Bandeau du panel
    // Capitalisation du contenu de la variable typ11 (à voir plus tard - il existe déjà une méthode dans Lexical)
    String chaineMaj = null;
    String typ11 = lexique.HostFieldGetData("TYP11");
    String facturation = " facturation ";
    if (!isDevis) {
      if (typ11 != null && !typ11.trim().isEmpty()) {
        chaineMaj = typ11.trim().toLowerCase().replaceFirst(".", (typ11.charAt(0) + "").toUpperCase());
      }
      if (isChantier) {
        p_bpresentation.setText(typ11);
      }
      else {
        p_bpresentation.setText(chaineMaj);
      }
    }
    else {
      p_bpresentation.setText("Devis");
    }
    if (isChantier) {
      p_bpresentation.setText("Chantier");
    }
    String e1in18 = lexique.HostFieldGetData("E1IN18").trim();
    if (e1in18.equals("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (e1in18.equals("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText().replaceAll(":", "").trim() + " (facturation "
        + lexique.HostFieldGetData("TFLIB").trim() + ")" + libelleDirectOuInterne);
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
    
    riMenu3.setVisible(lexique.isTrue("46"));
    riSousMenu10.setVisible(!lexique.HostFieldGetData("RESTOK").trim().equals("0"));
    
    String jonction1 = "";
    String jonction2 = "";
    String topp0e = lexique.HostFieldGetData("TOPP0E").trim();
    if (!topp0e.isEmpty()) {
      jonction1 = "<br/>";
      jonction2 = " - ";
    }
    riBouton14.setText("<html>" + lexique.HostFieldGetData("TOPP0") + " - " + lexique.HostFieldGetData("LIBOPT0") + jonction1 + topp0e
        + jonction2 + lexique.HostFieldGetData("LIBOPT0E") + "</html>");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Permet de gérer la couleur des lignes de la liste en fonction du type de ligne (article, commentaire , edition, annulé...)
   */
  private void gererCouleurListe() {
    for (int i = 0; i < listeCellules.length - 2; i++) {
      String chaineTest = Constantes.normerTexte(lexique.HostFieldGetData(listeCellules[i]));
      if (!chaineTest.isEmpty() && chaineTest.length() > 7) {
        String marqueur = chaineTest.substring(5, 7).trim();
        if (marqueur.isEmpty() || marqueur.equals("*E")) {
          couleurLigne[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        }
        else if (marqueur.equals("**")) {
          couleurLigne[i][0] = Constantes.COULEUR_LISTE_ANNULATION;
        }
        else {
          couleurLigne[i][0] = Color.BLACK;
        }
      }
      else {
        couleurLigne[i][0] = Color.BLACK;
      }
    }
    couleurLigne[13][0] = Constantes.COULEUR_LISTE_BILAN;
    couleurLigne[14][0] = Constantes.COULEUR_LISTE_ERREUR;
  }
  
  private void switchOptionsPlus() {
    isToutesOptions = !isToutesOptions;
    riBouton4.setVisible(riBouton4.isEnabled() && isToutesOptions);
    riBouton5.setVisible(riBouton5.isEnabled() && isToutesOptions);
    riBouton6.setVisible(riBouton6.isEnabled() && isToutesOptions);
    riBouton7.setVisible(riBouton7.isEnabled() && isToutesOptions);
    riBouton8.setVisible(riBouton8.isEnabled() && isToutesOptions);
    riBouton9.setVisible(riBouton9.isEnabled() && isToutesOptions);
    riBouton10.setVisible(riBouton10.isEnabled() && isToutesOptions);
    riBouton11.setVisible(riBouton11.isEnabled() && isToutesOptions);
    riBouton12.setVisible(riBouton12.isEnabled() && isToutesOptions);
    p_saisie_tatoues.setVisible(isToutesOptions);
  }
  
  private void testerOption(String option) {
    for (int i = 0; i < options_sans_edition.length; i++) {
      if (option.equalsIgnoreCase(options_sans_edition[i])) {
        i = options_sans_edition.length;
        label2.setVisible(false);
        V06FE.setVisible(false);
        riBoutonDetail1.setVisible(false);
      }
      else if (i == options_sans_edition.length - 1) {
        label2.setVisible(true);
        V06FE.setVisible(true);
        riBoutonDetail1.setVisible(true);
      }
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FE", 0, "DLP");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FE", 0, "DLs");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    V06F3.setText("MAR");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    V06F3.setText("LDP");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FE", 1, "");
    lexique.HostCursorPut("V06FE"); // rajouté par mes soins ....
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FE", 0, "");
    lexique.HostCursorPut("V06FE"); // rajouté par mes soins ....
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riMenu_bt1ActionPerformed(ActionEvent e) {
    V06FE.setText("");
    V06F3.setText("ATT");
    lexique.HostFieldPutData("WREP4", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton1ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP1"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_plusActionPerformed(ActionEvent e) {
    switchOptionsPlus();
  }
  
  private void riBouton2ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP2"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton3ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP3"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton4ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP4"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton5ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP5"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton6ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP6"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton7ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP7"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton8ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP8"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton9ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP9"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton10ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP10"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton11ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP11"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton12ActionPerformed(ActionEvent e) {
    V06F3.setText(lexique.HostFieldGetData("TOPP12"));
    lexique.HostFieldPutData("WREP4", 1, "?");
    testerOption(V06F3.getText().trim());
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton13ActionPerformed(ActionEvent e) {
    V06F3.setText("RGL");
    lexique.HostFieldPutData("WREP4", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER");
    testerOption(V06F3.getText().trim());
  }
  
  private void riSousMenu_bt_dupli2ActionPerformed(ActionEvent e) {
    V06F3.setText("ANN");
    lexique.HostFieldPutData("WREP4", 1, "?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riMenu_bt_imprActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("APDF", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonVisu1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("APDF", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    V06F3.setText("CLI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riMenu_b5t3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    V06F3.setText("ENC");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void retourentete(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void mi_RechercheContactActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void button_smsActionPerformed(ActionEvent e) {
    V06F3.setText("SMS");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    V06F3.setText("RST");
    lexique.HostFieldPutData("WREP", 1, "RST");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_25 = new RiZoneSortie();
    WDATEX = new RiZoneSortie();
    WNUM = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    WSUF = new RiZoneSortie();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    p_tete_droite = new JPanel();
    OBJ_129 = new RiZoneSortie();
    riBoutonVisu1 = new SNBoutonDocument();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenuF_dupli2 = new RiSousMenu();
    riSousMenu_bt_dupli2 = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu5 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel8 = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    LD01 = new XRiTable();
    E1DLPX = new XRiCalendrier();
    OBJ_37 = new JLabel();
    OBJ_36 = new JLabel();
    panel5 = new JPanel();
    riBouton14 = new JLabel();
    panel4 = new JPanel();
    panel6 = new JPanel();
    riBouton1 = new SNBoutonLeger();
    riBouton2 = new SNBoutonLeger();
    riBouton3 = new SNBoutonLeger();
    bt_plus = new SNBoutonLeger();
    riBouton4 = new SNBoutonLeger();
    riBouton5 = new SNBoutonLeger();
    riBouton6 = new SNBoutonLeger();
    riBouton7 = new SNBoutonLeger();
    riBouton8 = new SNBoutonLeger();
    riBouton9 = new SNBoutonLeger();
    riBouton10 = new SNBoutonLeger();
    riBouton11 = new SNBoutonLeger();
    riBouton12 = new SNBoutonLeger();
    panel3 = new JPanel();
    p_saisie_tatoues = new JPanel();
    label3 = new JLabel();
    V06F3 = new XRiTextField();
    label2 = new JLabel();
    V06FE = new XRiTextField();
    riBoutonDetail1 = new SNBoutonDetail();
    riBouton13 = new SNBoutonLeger();
    panel2 = new JPanel();
    OBJ_87 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_64 = new JLabel();
    WTHT = new RiZoneSortie();
    WTVA = new RiZoneSortie();
    E1TTC = new RiZoneSortie();
    OBJ_76 = new JLabel();
    OBJ_83 = new JLabel();
    WSL1 = new RiZoneSortie();
    WSL2 = new RiZoneSortie();
    WSL3 = new RiZoneSortie();
    E1TL1 = new RiZoneSortie();
    E1TL2 = new RiZoneSortie();
    E1TL3 = new RiZoneSortie();
    OBJ_74 = new JLabel();
    OBJ_68 = new JLabel();
    WLT1 = new RiZoneSortie();
    WLT2 = new RiZoneSortie();
    WLT3 = new RiZoneSortie();
    OBJ_89 = new RiZoneSortie();
    OBJ_77 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_81 = new JLabel();
    panel7 = new JPanel();
    DEST = new XRiMailto();
    button_sms = new JButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    FCT1 = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    label1 = new JLabel();
    BTD2 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    OBJ_88 = new RiZoneSortie();
    riMenu4 = new RiMenu();
    riMenu_bt_impr = new RiMenu_bt();
    retourentete = new JButton();
    BTD3 = new JPopupMenu();
    mi_RechercheContact = new JMenuItem();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1130, 650));
    setMaximumSize(new Dimension(1250, 800));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@  fin de traitement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(990, 0));
          p_tete_gauche.setMinimumSize(new Dimension(990, 29));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_25 ----
          OBJ_25.setText("@E3NOM@");
          OBJ_25.setOpaque(false);
          OBJ_25.setName("OBJ_25");
          
          // ---- WDATEX ----
          WDATEX.setComponentPopupMenu(BTD);
          WDATEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WDATEX.setText("@WDATEX@");
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setOpaque(false);
          WDATEX.setName("WDATEX");
          
          // ---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setText("@WNUM@");
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");
          
          // ---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          
          // ---- WSUF ----
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          WSUF.setName("WSUF");
          
          // ---- label4 ----
          label4.setText("Etablissement");
          label4.setName("label4");
          
          // ---- label5 ----
          label5.setText("Num\u00e9ro");
          label5.setName("label5");
          
          // ---- label6 ----
          label6.setText("Date de traitement");
          label6.setName("label6");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(label4, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(label5, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(11, 11, 11)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                  .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 290, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(label6, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                  .addComponent(WDATEX, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(label4, GroupLayout.PREFERRED_SIZE, 22,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(E1ETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(label5, GroupLayout.PREFERRED_SIZE, 22,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(label6, GroupLayout.PREFERRED_SIZE, 22,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(WDATEX, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(50, 0));
          p_tete_droite.setMinimumSize(new Dimension(50, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 2));
          
          // ---- OBJ_129 ----
          OBJ_129.setOpaque(false);
          OBJ_129.setBorder(null);
          OBJ_129.setText("obj_129");
          OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getStyle() | Font.BOLD, OBJ_129.getFont().getSize() + 3f));
          OBJ_129.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_129.setName("OBJ_129");
          p_tete_droite.add(OBJ_129);
        }
        barre_tete.add(p_tete_droite);
        
        // ---- riBoutonVisu1 ----
        riBoutonVisu1.setName("riBoutonVisu1");
        riBoutonVisu1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riBoutonVisu1ActionPerformed(e);
          }
        });
        barre_tete.add(riBoutonVisu1);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");
            
            // ---- riMenu_bt3 ----
            riMenu_bt3.setText("Fin");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu_bt3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_b5t3ActionPerformed(e);
              }
            });
            riMenu3.add(riMenu_bt3);
          }
          menus_bas.add(riMenu3);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Option propos\u00e9e");
            bouton_valider.setToolTipText("Option propos\u00e9e");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour lignes");
            bouton_retour.setToolTipText("Retour lignes");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
          
          // ======== riMenu1 ========
          {
            riMenu1.setName("riMenu1");
            
            // ---- riMenu_bt1 ----
            riMenu_bt1.setText("Sortir");
            riMenu_bt1.setToolTipText("Sortir");
            riMenu_bt1.setName("riMenu_bt1");
            riMenu_bt1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_bt1ActionPerformed(e);
              }
            });
            riMenu1.add(riMenu_bt1);
          }
          menus_bas.add(riMenu1);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 400));
            menus_haut.setPreferredSize(new Dimension(160, 400));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenuF_dupli2 ========
            {
              riSousMenuF_dupli2.setName("riSousMenuF_dupli2");
              
              // ---- riSousMenu_bt_dupli2 ----
              riSousMenu_bt_dupli2.setText("Annulation");
              riSousMenu_bt_dupli2.setToolTipText("Annulation");
              riSousMenu_bt_dupli2.setName("riSousMenu_bt_dupli2");
              riSousMenu_bt_dupli2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_dupli2ActionPerformed(e);
                }
              });
              riSousMenuF_dupli2.add(riSousMenu_bt_dupli2);
            }
            menus_haut.add(riSousMenuF_dupli2);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Encours comptable");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Afficher les marges");
              riSousMenu_bt11.setToolTipText("Afficher les marges");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Ligne de pied");
              riSousMenu_bt12.setToolTipText("Ligne de pied");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Client en compte");
              riSousMenu_bt1.setToolTipText("Client en compte");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Livraison pr\u00e9vue");
              riSousMenu_bt6.setToolTipText("Livraison pr\u00e9vue");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Livraison souhait\u00e9e");
              riSousMenu_bt7.setToolTipText("Livraison souhait\u00e9e");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Historique");
              riSousMenu_bt9.setToolTipText("Historique bon");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Restaurer");
              riSousMenu_bt10.setToolTipText("Restaurer le document avant modification");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riMenu5 ========
            {
              riMenu5.setName("riMenu5");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Navigation ");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu5.add(riMenu_bt4);
            }
            menus_haut.add(riMenu5);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setPreferredSize(new Dimension(170, 260));
              riSousMenu18.setMinimumSize(new Dimension(170, 260));
              riSousMenu18.setMaximumSize(new Dimension(170, 260));
              riSousMenu18.setMargin(new Insets(0, -2, 0, 0));
              riSousMenu18.setName("riSousMenu18");
            }
            menus_haut.add(riSousMenu18);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setMinimumSize(new Dimension(700, 600));
        p_centrage.setPreferredSize(new Dimension(900, 620));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 590));
          p_contenu.setForeground(Color.black);
          p_contenu.setMaximumSize(new Dimension(1000, 620));
          p_contenu.setName("p_contenu");
          
          // ======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(null);
            
            // ======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("@E1MAG@  @MALIB@"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ======== SCROLLPANE_LIST2 ========
              {
                SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
                SCROLLPANE_LIST2.setFocusable(false);
                SCROLLPANE_LIST2.setMaximumSize(new Dimension(625, 270));
                SCROLLPANE_LIST2.setPreferredSize(new Dimension(625, 270));
                SCROLLPANE_LIST2.setAutoscrolls(true);
                SCROLLPANE_LIST2.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
                SCROLLPANE_LIST2.setMinimumSize(new Dimension(625, 270));
                SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");
                
                // ---- LD01 ----
                LD01.setFocusable(false);
                LD01.setPreferredSize(new Dimension(900, 270));
                LD01.setMinimumSize(new Dimension(900, 270));
                LD01.setMaximumSize(new Dimension(900, 270));
                LD01.setPreferredScrollableViewportSize(new Dimension(625, 270));
                LD01.setRowSelectionAllowed(false);
                LD01.setName("LD01");
                SCROLLPANE_LIST2.setViewportView(LD01);
              }
              panel1.add(SCROLLPANE_LIST2);
              SCROLLPANE_LIST2.setBounds(new Rectangle(new Point(24, 68), SCROLLPANE_LIST2.getPreferredSize()));
              
              // ---- E1DLPX ----
              E1DLPX.setName("E1DLPX");
              panel1.add(E1DLPX);
              E1DLPX.setBounds(544, 32, 105, 30);
              
              // ---- OBJ_37 ----
              OBJ_37.setText("Livraison pr\u00e9vue");
              OBJ_37.setToolTipText(
                  "<HTML>Plusieurs dates de livraison pr\u00e9vues...<BR>Clic pour visualiser les lignes tri\u00e9es par DLP.</HTML>");
              OBJ_37.setName("OBJ_37");
              panel1.add(OBJ_37);
              OBJ_37.setBounds(384, 37, 145, 20);
              
              // ---- OBJ_36 ----
              OBJ_36.setText("@E1DEV1@");
              OBJ_36.setName("OBJ_36");
              panel1.add(OBJ_36);
              OBJ_36.setBounds(24, 39, 55, OBJ_36.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel8.add(panel1);
            panel1.setBounds(5, 5, 670, 350);
            
            // ======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("Option propos\u00e9e"));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ---- riBouton14 ----
              riBouton14.setHorizontalAlignment(SwingConstants.LEFT);
              riBouton14.setForeground(new Color(44, 74, 116));
              riBouton14.setFont(
                  riBouton14.getFont().deriveFont(riBouton14.getFont().getStyle() | Font.BOLD, riBouton14.getFont().getSize() + 1f));
              riBouton14.setName("riBouton14");
              panel5.add(riBouton14);
              riBouton14.setBounds(10, 25, 280, 50);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel8.add(panel5);
            panel5.setBounds(680, 5, 300, 90);
            
            // ======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Options de traitement"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ======== panel6 ========
              {
                panel6.setOpaque(false);
                panel6.setName("panel6");
                panel6.setLayout(new VerticalLayout());
                
                // ---- riBouton1 ----
                riBouton1.setText("@TOPP1@ - @LIBOPT1@");
                riBouton1.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton1.setPreferredSize(new Dimension(148, 26));
                riBouton1.setMargin(new Insets(0, 3, 0, 0));
                riBouton1.setName("riBouton1");
                riBouton1.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton1ActionPerformed(e);
                  }
                });
                panel6.add(riBouton1);
                
                // ---- riBouton2 ----
                riBouton2.setText("@TOPP2@ - @LIBOPT2@");
                riBouton2.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton2.setPreferredSize(new Dimension(148, 26));
                riBouton2.setMargin(new Insets(0, 3, 0, 0));
                riBouton2.setName("riBouton2");
                riBouton2.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton2ActionPerformed(e);
                  }
                });
                panel6.add(riBouton2);
                
                // ---- riBouton3 ----
                riBouton3.setText("@TOPP3@ - @LIBOPT3@");
                riBouton3.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton3.setPreferredSize(new Dimension(148, 26));
                riBouton3.setMargin(new Insets(0, 3, 0, 0));
                riBouton3.setName("riBouton3");
                riBouton3.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton3ActionPerformed(e);
                  }
                });
                panel6.add(riBouton3);
                
                // ---- bt_plus ----
                bt_plus.setText("Autres options");
                bt_plus.setFont(bt_plus.getFont().deriveFont(bt_plus.getFont().getStyle() | Font.BOLD, bt_plus.getFont().getSize() + 1f));
                bt_plus.setPreferredSize(new Dimension(139, 40));
                bt_plus.setName("bt_plus");
                bt_plus.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    bt_plusActionPerformed(e);
                  }
                });
                panel6.add(bt_plus);
                
                // ---- riBouton4 ----
                riBouton4.setText("@TOPP4@ - @LIBOPT4@");
                riBouton4.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton4.setPreferredSize(new Dimension(148, 26));
                riBouton4.setMargin(new Insets(0, 3, 0, 0));
                riBouton4.setName("riBouton4");
                riBouton4.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton4ActionPerformed(e);
                  }
                });
                panel6.add(riBouton4);
                
                // ---- riBouton5 ----
                riBouton5.setText("@TOPP5@ - @LIBOPT5@");
                riBouton5.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton5.setPreferredSize(new Dimension(148, 26));
                riBouton5.setMargin(new Insets(0, 3, 0, 0));
                riBouton5.setName("riBouton5");
                riBouton5.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton5ActionPerformed(e);
                  }
                });
                panel6.add(riBouton5);
                
                // ---- riBouton6 ----
                riBouton6.setText("@TOPP6@ - @LIBOPT6@");
                riBouton6.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton6.setPreferredSize(new Dimension(148, 26));
                riBouton6.setMargin(new Insets(0, 3, 0, 0));
                riBouton6.setName("riBouton6");
                riBouton6.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton6ActionPerformed(e);
                  }
                });
                panel6.add(riBouton6);
                
                // ---- riBouton7 ----
                riBouton7.setText("@TOPP7@ - @LIBOPT7@");
                riBouton7.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton7.setPreferredSize(new Dimension(148, 26));
                riBouton7.setMargin(new Insets(0, 3, 0, 0));
                riBouton7.setName("riBouton7");
                riBouton7.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton7ActionPerformed(e);
                  }
                });
                panel6.add(riBouton7);
                
                // ---- riBouton8 ----
                riBouton8.setText("@TOPP8@ - @LIBOPT8@");
                riBouton8.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton8.setPreferredSize(new Dimension(148, 26));
                riBouton8.setMargin(new Insets(0, 3, 0, 0));
                riBouton8.setName("riBouton8");
                riBouton8.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton8ActionPerformed(e);
                  }
                });
                panel6.add(riBouton8);
                
                // ---- riBouton9 ----
                riBouton9.setText("@TOPP9@ - @LIBOPT9@");
                riBouton9.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton9.setPreferredSize(new Dimension(148, 26));
                riBouton9.setMargin(new Insets(0, 3, 0, 0));
                riBouton9.setName("riBouton9");
                riBouton9.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton9ActionPerformed(e);
                  }
                });
                panel6.add(riBouton9);
                
                // ---- riBouton10 ----
                riBouton10.setText("@TOPP10@ - @LIBOPT10@");
                riBouton10.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton10.setPreferredSize(new Dimension(148, 26));
                riBouton10.setMargin(new Insets(0, 3, 0, 0));
                riBouton10.setName("riBouton10");
                riBouton10.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton10ActionPerformed(e);
                  }
                });
                panel6.add(riBouton10);
                
                // ---- riBouton11 ----
                riBouton11.setText("@TOPP11@ - @LIBOPT11@");
                riBouton11.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton11.setPreferredSize(new Dimension(148, 26));
                riBouton11.setMargin(new Insets(0, 3, 0, 0));
                riBouton11.setName("riBouton11");
                riBouton11.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton11ActionPerformed(e);
                  }
                });
                panel6.add(riBouton11);
                
                // ---- riBouton12 ----
                riBouton12.setText("@TOPP12@ - @LIBOPT12@");
                riBouton12.setHorizontalAlignment(SwingConstants.LEFT);
                riBouton12.setPreferredSize(new Dimension(148, 26));
                riBouton12.setMargin(new Insets(0, 3, 0, 0));
                riBouton12.setName("riBouton12");
                riBouton12.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    riBouton12ActionPerformed(e);
                  }
                });
                panel6.add(riBouton12);
                
                // ======== panel3 ========
                {
                  panel3.setOpaque(false);
                  panel3.setPreferredSize(new Dimension(173, 50));
                  panel3.setMinimumSize(new Dimension(173, 50));
                  panel3.setName("panel3");
                  panel3.setLayout(null);
                  
                  // ======== p_saisie_tatoues ========
                  {
                    p_saisie_tatoues.setOpaque(false);
                    p_saisie_tatoues.setName("p_saisie_tatoues");
                    p_saisie_tatoues.setLayout(null);
                    
                    // ---- label3 ----
                    label3.setText("ou");
                    label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() & ~Font.BOLD));
                    label3.setName("label3");
                    p_saisie_tatoues.add(label3);
                    label3.setBounds(5, 0, 20, 28);
                    
                    // ---- V06F3 ----
                    V06F3.setName("V06F3");
                    p_saisie_tatoues.add(V06F3);
                    V06F3.setBounds(25, 0, 40, V06F3.getPreferredSize().height);
                    
                    // ---- label2 ----
                    label2.setText("Edition");
                    label2.setName("label2");
                    p_saisie_tatoues.add(label2);
                    label2.setBounds(80, 4, 45, 20);
                    
                    // ---- V06FE ----
                    V06FE.setComponentPopupMenu(BTD2);
                    V06FE.setName("V06FE");
                    p_saisie_tatoues.add(V06FE);
                    V06FE.setBounds(125, 0, 24, V06FE.getPreferredSize().height);
                    
                    // ---- riBoutonDetail1 ----
                    riBoutonDetail1.setName("riBoutonDetail1");
                    riBoutonDetail1.addActionListener(new ActionListener() {
                      @Override
                      public void actionPerformed(ActionEvent e) {
                        riBoutonDetail1ActionPerformed(e);
                      }
                    });
                    p_saisie_tatoues.add(riBoutonDetail1);
                    riBoutonDetail1.setBounds(new Rectangle(new Point(150, 5), riBoutonDetail1.getPreferredSize()));
                    
                    {
                      // compute preferred size
                      Dimension preferredSize = new Dimension();
                      for (int i = 0; i < p_saisie_tatoues.getComponentCount(); i++) {
                        Rectangle bounds = p_saisie_tatoues.getComponent(i).getBounds();
                        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                      }
                      Insets insets = p_saisie_tatoues.getInsets();
                      preferredSize.width += insets.right;
                      preferredSize.height += insets.bottom;
                      p_saisie_tatoues.setMinimumSize(preferredSize);
                      p_saisie_tatoues.setPreferredSize(preferredSize);
                    }
                  }
                  panel3.add(p_saisie_tatoues);
                  p_saisie_tatoues.setBounds(0, 10, 180, 30);
                  
                  {
                    // compute preferred size
                    Dimension preferredSize = new Dimension();
                    for (int i = 0; i < panel3.getComponentCount(); i++) {
                      Rectangle bounds = panel3.getComponent(i).getBounds();
                      preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                      preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                    }
                    Insets insets = panel3.getInsets();
                    preferredSize.width += insets.right;
                    preferredSize.height += insets.bottom;
                    panel3.setMinimumSize(preferredSize);
                    panel3.setPreferredSize(preferredSize);
                  }
                }
                panel6.add(panel3);
              }
              panel4.add(panel6);
              panel6.setBounds(10, 35, 280, 400);
              
              // ---- riBouton13 ----
              riBouton13.setText("R\u00e8glement");
              riBouton13.setFont(riBouton13.getFont().deriveFont(riBouton13.getFont().getStyle() | Font.BOLD));
              riBouton13.setName("riBouton13");
              riBouton13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBouton13ActionPerformed(e);
                }
              });
              panel4.add(riBouton13);
              riBouton13.setBounds(new Rectangle(new Point(85, 435), riBouton13.getPreferredSize()));
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel8.add(panel4);
            panel4.setBounds(680, 105, 300, 480);
            
            // ======== panel2 ========
            {
              panel2.setBorder(new TitledBorder(""));
              panel2.setOpaque(false);
              panel2.setPreferredSize(new Dimension(600, 180));
              panel2.setName("panel2");
              
              // ---- OBJ_87 ----
              OBJ_87.setText("@DVLIBR@");
              OBJ_87.setHorizontalAlignment(SwingConstants.RIGHT);
              OBJ_87.setName("OBJ_87");
              
              // ---- OBJ_70 ----
              OBJ_70.setText("Montants taxes");
              OBJ_70.setName("OBJ_70");
              
              // ---- OBJ_64 ----
              OBJ_64.setText("Bases taxables");
              OBJ_64.setName("OBJ_64");
              
              // ---- WTHT ----
              WTHT.setComponentPopupMenu(FCT1);
              WTHT.setText("@WTHT@");
              WTHT.setHorizontalTextPosition(SwingConstants.RIGHT);
              WTHT.setHorizontalAlignment(SwingConstants.RIGHT);
              WTHT.setName("WTHT");
              
              // ---- WTVA ----
              WTVA.setComponentPopupMenu(FCT1);
              WTVA.setText("@WTVA@");
              WTVA.setHorizontalTextPosition(SwingConstants.RIGHT);
              WTVA.setHorizontalAlignment(SwingConstants.RIGHT);
              WTVA.setName("WTVA");
              
              // ---- E1TTC ----
              E1TTC.setComponentPopupMenu(FCT1);
              E1TTC.setText("@E1TTC@");
              E1TTC.setHorizontalTextPosition(SwingConstants.RIGHT);
              E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
              E1TTC.setFont(E1TTC.getFont().deriveFont(E1TTC.getFont().getStyle() | Font.BOLD));
              E1TTC.setName("E1TTC");
              
              // ---- OBJ_76 ----
              OBJ_76.setText("Codes taxes");
              OBJ_76.setName("OBJ_76");
              
              // ---- OBJ_83 ----
              OBJ_83.setText("Total TTC");
              OBJ_83.setFont(OBJ_83.getFont().deriveFont(OBJ_83.getFont().getStyle() | Font.BOLD));
              OBJ_83.setName("OBJ_83");
              
              // ---- WSL1 ----
              WSL1.setComponentPopupMenu(BTD);
              WSL1.setText("@WSL1@");
              WSL1.setHorizontalTextPosition(SwingConstants.RIGHT);
              WSL1.setHorizontalAlignment(SwingConstants.RIGHT);
              WSL1.setName("WSL1");
              
              // ---- WSL2 ----
              WSL2.setComponentPopupMenu(BTD);
              WSL2.setText("@WSL2@");
              WSL2.setHorizontalTextPosition(SwingConstants.RIGHT);
              WSL2.setHorizontalAlignment(SwingConstants.RIGHT);
              WSL2.setName("WSL2");
              
              // ---- WSL3 ----
              WSL3.setComponentPopupMenu(BTD);
              WSL3.setText("@WSL3@");
              WSL3.setHorizontalTextPosition(SwingConstants.RIGHT);
              WSL3.setHorizontalAlignment(SwingConstants.RIGHT);
              WSL3.setName("WSL3");
              
              // ---- E1TL1 ----
              E1TL1.setComponentPopupMenu(BTD);
              E1TL1.setText("@E1TL1@");
              E1TL1.setHorizontalTextPosition(SwingConstants.RIGHT);
              E1TL1.setHorizontalAlignment(SwingConstants.RIGHT);
              E1TL1.setName("E1TL1");
              
              // ---- E1TL2 ----
              E1TL2.setComponentPopupMenu(BTD);
              E1TL2.setText("@E1TL2@");
              E1TL2.setHorizontalTextPosition(SwingConstants.RIGHT);
              E1TL2.setHorizontalAlignment(SwingConstants.RIGHT);
              E1TL2.setName("E1TL2");
              
              // ---- E1TL3 ----
              E1TL3.setComponentPopupMenu(BTD);
              E1TL3.setText("@E1TL3@");
              E1TL3.setHorizontalTextPosition(SwingConstants.RIGHT);
              E1TL3.setHorizontalAlignment(SwingConstants.RIGHT);
              E1TL3.setName("E1TL3");
              
              // ---- OBJ_74 ----
              OBJ_74.setText("Total TVA");
              OBJ_74.setName("OBJ_74");
              
              // ---- OBJ_68 ----
              OBJ_68.setText("Total H.T");
              OBJ_68.setName("OBJ_68");
              
              // ---- WLT1 ----
              WLT1.setComponentPopupMenu(BTD);
              WLT1.setHorizontalAlignment(SwingConstants.RIGHT);
              WLT1.setText("@WLT1@");
              WLT1.setHorizontalTextPosition(SwingConstants.RIGHT);
              WLT1.setName("WLT1");
              
              // ---- WLT2 ----
              WLT2.setComponentPopupMenu(BTD);
              WLT2.setText("@WLT2@");
              WLT2.setHorizontalTextPosition(SwingConstants.RIGHT);
              WLT2.setHorizontalAlignment(SwingConstants.RIGHT);
              WLT2.setName("WLT2");
              
              // ---- WLT3 ----
              WLT3.setComponentPopupMenu(BTD);
              WLT3.setText("@WLT3@");
              WLT3.setHorizontalTextPosition(SwingConstants.RIGHT);
              WLT3.setHorizontalAlignment(SwingConstants.RIGHT);
              WLT3.setName("WLT3");
              
              // ---- OBJ_89 ----
              OBJ_89.setText("@E1DEV@");
              OBJ_89.setName("OBJ_89");
              
              // ---- OBJ_77 ----
              OBJ_77.setText("@CTVA1@");
              OBJ_77.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_77.setName("OBJ_77");
              
              // ---- OBJ_79 ----
              OBJ_79.setText("@CTVA2@");
              OBJ_79.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_79.setName("OBJ_79");
              
              // ---- OBJ_81 ----
              OBJ_81.setText("@CTVA3@");
              OBJ_81.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_81.setName("OBJ_81");
              
              GroupLayout panel2Layout = new GroupLayout(panel2);
              panel2.setLayout(panel2Layout);
              panel2Layout
                  .setHorizontalGroup(
                      panel2Layout.createParallelGroup()
                          .addGroup(
                              panel2Layout.createSequentialGroup().addGap(24, 24, 24)
                                  .addGroup(panel2Layout.createParallelGroup().addGroup(panel2Layout.createSequentialGroup()
                                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                                      .addComponent(WSL1, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                                      .addComponent(WSL2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                                      .addComponent(WSL3, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                                      .addGroup(panel2Layout.createSequentialGroup()
                                          .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                                          .addGap(0, 0, 0).addComponent(E1TL1, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                          .addGap(10, 10, 10)
                                          .addComponent(E1TL2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                          .addGap(10, 10, 10)
                                          .addComponent(E1TL3, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
                                      .addGroup(panel2Layout.createSequentialGroup()
                                          .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                                          .addGap(20, 20, 20)
                                          .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                                          .addGap(3, 3, 3).addComponent(WLT1, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                                          .addGap(11, 11, 11)
                                          .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                                          .addGap(3, 3, 3).addComponent(WLT2, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                                          .addGap(11, 11, 11)
                                          .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 12, GroupLayout.PREFERRED_SIZE)
                                          .addGap(3, 3, 3)
                                          .addComponent(WLT3, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)))
                                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 37, Short.MAX_VALUE)
                                  .addGroup(panel2Layout.createParallelGroup().addGroup(panel2Layout.createSequentialGroup()
                                      .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                                      .addGap(15, 15, 15)
                                      .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE))
                                      .addGroup(panel2Layout.createSequentialGroup().addGap(60, 60, 60).addGroup(panel2Layout
                                          .createParallelGroup()
                                          .addGroup(panel2Layout.createSequentialGroup()
                                              .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                              .addGap(0, 0, 0)
                                              .addComponent(WTHT, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
                                          .addGroup(panel2Layout.createSequentialGroup()
                                              .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                              .addGap(0, 0, 0)
                                              .addComponent(WTVA, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE))
                                          .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                                          .addGroup(panel2Layout.createSequentialGroup().addGap(70, 70, 70).addComponent(E1TTC,
                                              GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)))))
                                  .addContainerGap()));
              panel2Layout
                  .setVerticalGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup().addGap(9, 9, 9)
                          .addGroup(panel2Layout.createParallelGroup()
                              .addGroup(panel2Layout.createSequentialGroup().addGroup(panel2Layout.createParallelGroup()
                                  .addComponent(WTHT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                  .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_68,
                                      GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel2Layout.createParallelGroup()
                                      .addComponent(WTVA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5)
                                          .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel2Layout.createParallelGroup()
                                      .addComponent(E1TTC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5)
                                          .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel2Layout.createParallelGroup()
                                      .addComponent(OBJ_87, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addComponent(
                                          OBJ_89, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                              .addGroup(panel2Layout.createSequentialGroup().addGroup(panel2Layout.createParallelGroup()
                                  .addComponent(WSL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(WSL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                  .addComponent(WSL3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                  .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_64,
                                      GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel2Layout.createParallelGroup()
                                      .addComponent(E1TL1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addComponent(E1TL2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addComponent(E1TL3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addGroup(
                                          panel2Layout.createSequentialGroup().addGap(5, 5, 5)
                                              .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel2Layout.createParallelGroup()
                                      .addComponent(WLT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addComponent(WLT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addComponent(WLT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addGroup(panel2Layout.createSequentialGroup().addGap(5, 5, 5)
                                          .addGroup(panel2Layout.createParallelGroup()
                                              .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                              .addComponent(OBJ_77, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                              .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                              .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))))
                          .addContainerGap()));
            }
            panel8.add(panel2);
            panel2.setBounds(5, 365, 670, 145);
            
            // ======== panel7 ========
            {
              panel7.setBorder(new TitledBorder(""));
              panel7.setOpaque(false);
              panel7.setName("panel7");
              
              // ---- DEST ----
              DEST.setMonClicDroit(BTD3);
              DEST.setName("DEST");
              
              // ---- button_sms ----
              button_sms.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              button_sms.setName("button_sms");
              button_sms.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  button_smsActionPerformed(e);
                }
              });
              
              GroupLayout panel7Layout = new GroupLayout(panel7);
              panel7.setLayout(panel7Layout);
              panel7Layout.setHorizontalGroup(panel7Layout.createParallelGroup()
                  .addGroup(panel7Layout.createSequentialGroup().addContainerGap()
                      .addComponent(DEST, GroupLayout.PREFERRED_SIZE, 550, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                      .addComponent(button_sms, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                      .addContainerGap(34, Short.MAX_VALUE)));
              panel7Layout
                  .setVerticalGroup(panel7Layout.createParallelGroup()
                      .addGroup(panel7Layout.createSequentialGroup()
                          .addGroup(panel7Layout.createParallelGroup()
                              .addComponent(button_sms, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel7Layout.createSequentialGroup().addGap(1, 1, 1).addComponent(DEST,
                                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                          .addGap(0, 10, Short.MAX_VALUE)));
            }
            panel8.add(panel7);
            panel7.setBounds(5, 520, 670, 60);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
              p_contenuLayout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 985, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE).addGap(0, 0, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== CMD ========
    {
      CMD.setName("CMD");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Chiffrage/Traitements diff\u00e9r\u00e9s");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);
      
      // ---- OBJ_6 ----
      OBJ_6.setText("Historique bon");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);
      
      // ---- OBJ_7 ----
      OBJ_7.setText("Fonction Recherche de Bons");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    
    // ======== FCT1 ========
    {
      FCT1.setName("FCT1");
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Convertisseur mon\u00e9taire");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_13);
    }
    
    // ---- label1 ----
    label1.setText("@TFLIB@");
    label1.setName("label1");
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix possibles");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD2.add(menuItem1);
    }
    
    // ---- OBJ_88 ----
    OBJ_88.setText("@BASX@");
    OBJ_88.setName("OBJ_88");
    
    // ======== riMenu4 ========
    {
      riMenu4.setName("riMenu4");
      
      // ---- riMenu_bt_impr ----
      riMenu_bt_impr.setText("Impression");
      riMenu_bt_impr.setName("riMenu_bt_impr");
      riMenu_bt_impr.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riMenu_bt_imprActionPerformed(e);
        }
      });
      riMenu4.add(riMenu_bt_impr);
    }
    
    // ---- retourentete ----
    retourentete.setText("retour sur entete");
    retourentete.setName("retourentete");
    retourentete.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        retourentete(e);
      }
    });
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- mi_RechercheContact ----
      mi_RechercheContact.setText("Recherche contact");
      mi_RechercheContact.setName("mi_RechercheContact");
      mi_RechercheContact.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_RechercheContactActionPerformed(e);
        }
      });
      BTD3.add(mi_RechercheContact);
    }
    
    // ======== riSousMenu8 ========
    {
      riSousMenu8.setName("riSousMenu8");
      
      // ---- riSousMenu_bt8 ----
      riSousMenu_bt8.setText("Affichage complet");
      riSousMenu_bt8.setToolTipText("Affichage complet");
      riSousMenu_bt8.setName("riSousMenu_bt8");
      riSousMenu_bt8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt8ActionPerformed(e);
        }
      });
      riSousMenu8.add(riSousMenu_bt8);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie OBJ_25;
  private RiZoneSortie WDATEX;
  private RiZoneSortie WNUM;
  private RiZoneSortie E1ETB;
  private RiZoneSortie WSUF;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JPanel p_tete_droite;
  private RiZoneSortie OBJ_129;
  private SNBoutonDocument riBoutonVisu1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenuF_dupli2;
  private RiSousMenu_bt riSousMenu_bt_dupli2;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu5;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel8;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable LD01;
  private XRiCalendrier E1DLPX;
  private JLabel OBJ_37;
  private JLabel OBJ_36;
  private JPanel panel5;
  private JLabel riBouton14;
  private JPanel panel4;
  private JPanel panel6;
  private SNBoutonLeger riBouton1;
  private SNBoutonLeger riBouton2;
  private SNBoutonLeger riBouton3;
  private SNBoutonLeger bt_plus;
  private SNBoutonLeger riBouton4;
  private SNBoutonLeger riBouton5;
  private SNBoutonLeger riBouton6;
  private SNBoutonLeger riBouton7;
  private SNBoutonLeger riBouton8;
  private SNBoutonLeger riBouton9;
  private SNBoutonLeger riBouton10;
  private SNBoutonLeger riBouton11;
  private SNBoutonLeger riBouton12;
  private JPanel panel3;
  private JPanel p_saisie_tatoues;
  private JLabel label3;
  private XRiTextField V06F3;
  private JLabel label2;
  private XRiTextField V06FE;
  private SNBoutonDetail riBoutonDetail1;
  private SNBoutonLeger riBouton13;
  private JPanel panel2;
  private JLabel OBJ_87;
  private JLabel OBJ_70;
  private JLabel OBJ_64;
  private RiZoneSortie WTHT;
  private RiZoneSortie WTVA;
  private RiZoneSortie E1TTC;
  private JLabel OBJ_76;
  private JLabel OBJ_83;
  private RiZoneSortie WSL1;
  private RiZoneSortie WSL2;
  private RiZoneSortie WSL3;
  private RiZoneSortie E1TL1;
  private RiZoneSortie E1TL2;
  private RiZoneSortie E1TL3;
  private JLabel OBJ_74;
  private JLabel OBJ_68;
  private RiZoneSortie WLT1;
  private RiZoneSortie WLT2;
  private RiZoneSortie WLT3;
  private RiZoneSortie OBJ_89;
  private JLabel OBJ_77;
  private JLabel OBJ_79;
  private JLabel OBJ_81;
  private JPanel panel7;
  private XRiMailto DEST;
  private JButton button_sms;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_13;
  private JLabel label1;
  private JPopupMenu BTD2;
  private JMenuItem menuItem1;
  private RiZoneSortie OBJ_88;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt_impr;
  private JButton retourentete;
  private JPopupMenu BTD3;
  private JMenuItem mi_RechercheContact;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
