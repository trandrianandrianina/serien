
package ri.serien.libecranrpg.vgvm.VGVM118F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;

/**
 * @author Stéphane Vénéri
 */
public class VGVM118F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM118F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    OPT03.setValeursSelection("1", " ");
    OPT02.setValeursSelection("1", " ");
    OPT01.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@SAIS@"));
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OPT01ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OPT02ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OPT03ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_18 = new JLabel();
    OBJ_21 = new JLabel();
    OBJ_24 = new JLabel();
    E1DLSX = new XRiCalendrier();
    E1DLPX = new XRiCalendrier();
    WDLPPX = new XRiCalendrier();
    panel2 = new JPanel();
    OPT01 = new XRiCheckBox();
    OPT02 = new XRiCheckBox();
    OPT03 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_15 = new JLabel();
    OBJ_25 = new JButton();
    OBJ_22 = new JButton();
    OBJ_28 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(700, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Libell\u00e9(s) sur ligne article");
            riSousMenu_bt6.setToolTipText("Saisie ou modification de libell\u00e9(s) sur ligne article");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Dates de livraison"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_18 ----
          OBJ_18.setText("Souhait\u00e9e");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(20, 39, 82, 20);

          //---- OBJ_21 ----
          OBJ_21.setText("Pr\u00e9vue");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(20, 74, 64, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("Possible");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(20, 109, 75, 20);

          //---- E1DLSX ----
          E1DLSX.setName("E1DLSX");
          panel1.add(E1DLSX);
          E1DLSX.setBounds(115, 35, 105, E1DLSX.getPreferredSize().height);

          //---- E1DLPX ----
          E1DLPX.setName("E1DLPX");
          panel1.add(E1DLPX);
          E1DLPX.setBounds(115, 70, 105, E1DLPX.getPreferredSize().height);

          //---- WDLPPX ----
          WDLPPX.setName("WDLPPX");
          panel1.add(WDLPPX);
          WDLPPX.setBounds(115, 105, 105, WDLPPX.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Options"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OPT01 ----
          OPT01.setText("Confirmation (VAL)");
          OPT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT01.setName("OPT01");
          OPT01.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OPT01ActionPerformed(e);
            }
          });
          panel2.add(OPT01);
          OPT01.setBounds(20, 40, 205, 20);

          //---- OPT02 ----
          OPT02.setText("D\u00e9coupage bon  (DIS)");
          OPT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT02.setName("OPT02");
          OPT02.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OPT02ActionPerformed(e);
            }
          });
          panel2.add(OPT02);
          OPT02.setBounds(20, 75, 205, 20);

          //---- OPT03 ----
          OPT03.setText("Modification (COR)");
          OPT03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPT03.setName("OPT03");
          OPT03.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OPT03ActionPerformed(e);
            }
          });
          panel2.add(OPT03);
          OPT03.setBounds(20, 110, 205, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
              .addGap(10, 10, 10)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- OBJ_15 ----
    OBJ_15.setText("Dates de livraison");
    OBJ_15.setName("OBJ_15");

    //---- OBJ_25 ----
    OBJ_25.setText("");
    OBJ_25.setToolTipText("<HTML>Choix d'une date gr\u00e2ce<BR>au calendrier graphique</HTML>");
    OBJ_25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_25.setName("OBJ_25");

    //---- OBJ_22 ----
    OBJ_22.setText("");
    OBJ_22.setToolTipText("<HTML>Choix d'une date gr\u00e2ce<BR>au calendrier graphique</HTML>");
    OBJ_22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_22.setName("OBJ_22");

    //---- OBJ_28 ----
    OBJ_28.setText("Options");
    OBJ_28.setName("OBJ_28");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_18;
  private JLabel OBJ_21;
  private JLabel OBJ_24;
  private XRiCalendrier E1DLSX;
  private XRiCalendrier E1DLPX;
  private XRiCalendrier WDLPPX;
  private JPanel panel2;
  private XRiCheckBox OPT01;
  private XRiCheckBox OPT02;
  private XRiCheckBox OPT03;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JLabel OBJ_15;
  private JButton OBJ_25;
  private JButton OBJ_22;
  private JLabel OBJ_28;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
