
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.ModeleListeDocumentStocke;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.VueDocumentStocke;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_B0 extends SNPanelEcranRPG implements ioFrame {
  private String topErreur = "19";
  private final static String BOUTON_AFFICHER_BLOC_NOTE = "Bloc-notes";
  private final static String BOUTON_AFFICHER_MEMOS = "Mémos";
  private final static String BOUTON_COPIER_BLOC_ADRESSE = "Copie du bloc adresse";
  private final static String BOUTON_AFFICHER_CONTACT = "Contacts";
  private final static String BOUTON_AFFICHER_EMBALLAGE_REPRIS = "Emballages repris";
  private final static String BOUTON_METTRE_AFFICHAGE_COMPLET = "Affichage complet";
  private final static String BOUTON_AFFICHER_AFFICHAGE_DOCUMENT = "Afficher les documents";
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   * @param param paramètres de l'écran
   */
  public VGVM03FM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    XRiBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    XRiBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    XRiBarreBouton.ajouterBouton(BOUTON_METTRE_AFFICHAGE_COMPLET, 'a', true);
    XRiBarreBouton.ajouterBouton(BOUTON_AFFICHER_BLOC_NOTE, 'b', true);
    XRiBarreBouton.ajouterBouton(BOUTON_AFFICHER_MEMOS, 'm', true);
    XRiBarreBouton.ajouterBouton(BOUTON_COPIER_BLOC_ADRESSE, 'b', true);
    XRiBarreBouton.ajouterBouton(BOUTON_AFFICHER_CONTACT, 'c', false);
    XRiBarreBouton.ajouterBouton(BOUTON_AFFICHER_EMBALLAGE_REPRIS, 'e', false);
    XRiBarreBouton.ajouterBouton(BOUTON_AFFICHER_AFFICHAGE_DOCUMENT, 'd', false);
    XRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    // Ajout
    initDiverses();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDCLI@")).trim());
    INDLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDLIV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Active les bouton liée au V01F
    XRiBarreBouton.rafraichir(lexique);
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    // Titre
    if (lexique.HostFieldGetData("CLIN29").trim().isEmpty()) {
      bpPresentation.setText("Fiche client");
      pnlCoordonnesClient.setTitre("Coordonnées du client");
    }
    else {
      bpPresentation.setText("Fiche prospect");
      pnlCoordonnesClient.setTitre("Coordonnées du prospect");
    }
    
    boolean isClientParticulier = lexique.isTrue("61");
    boolean isHistorique = lexique.isTrue("96");
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    boolean isModification = lexique.getMode() == Lexical.MODE_MODIFICATION;
    
    // Visibilité
    lbClient.setVisible(INDCLI.isVisible());
    RENETR.setVisible(isClientParticulier);
    lbCivilite.setVisible(isClientParticulier);
    snCivilite.setVisible(isClientParticulier);
    REPRE.setVisible(isClientParticulier);
    CLCPL.setVisible(!isClientParticulier);
    lbPays.setVisible(CLPAY.isVisible());
    lbMail.setVisible(RENETR.isVisible());
    lbTelephone.setVisible(CLTEL.isVisible());
    lbObservation.setVisible(CLOBS.isVisible());
    lbFax.setVisible(CLFAX.isVisible());
    
    // Type de fiche en création ou modification
    pnlChoixTypeClient.setVisible(isCreation || isModification);
    rbTypeClient.setSelected(lexique.HostFieldGetData("CLIN29").trim().isEmpty());
    rbTypeProspect.setSelected(lexique.HostFieldGetData("CLIN29").trim().equalsIgnoreCase("P"));
    
    if (isClientParticulier) {
      lbNom.setText("Nom");
      lbPrenom.setText("Prénom");
      
    }
    else {
      lbNom.setText("Raison sociale");
      lbPrenom.setText("Complément");
    }
    
    // Active les boutons
    activerBouton();
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    // Civilité
    snCivilite.setSession(getSession());
    snCivilite.setIdEtablissement(idEtablissement);
    snCivilite.charger(false);
    snCivilite.setSelectionParChampRPG(lexique, "RECIV");
    snCivilite.setEnabled(!isConsultation);
    
    // Commune client
    snCodePostalCommune.setSession(getSession());
    snCodePostalCommune.setIdEtablissement(idEtablissement);
    snCodePostalCommune.charger(false);
    snCodePostalCommune.setSelectionParChampRPG(lexique, "CLCDP", "CLVILR");
    snCodePostalCommune.setEnabled(!isConsultation);
    
    // Pays client
    try {
      snPays.setEnabled(!isConsultation);
      snPays.setSession(getSession());
      snPays.setIdEtablissement(idEtablissement);
      snPays.charger(false);
      snPays.setSelectionParChampRPG(lexique, "CLCOP", "CLPAY");
      snPays.setAucunAutorise(true);
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
    }
    
    snRepresentant.setEnabled(!isConsultation && !isRepresentantNonModifiable());
    snCategorieClient.setEnabled(!isConsultation);
    CLTEL.setEnabled(!isConsultation);
    
    // Renseigner l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(idEtablissement);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), idEtablissement);
    
    // Renseigne les représentant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.setTousAutorise(true);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "CLREP");
    
    // Renseigne les catégorie client
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(idEtablissement);
    snCategorieClient.setTousAutorise(true);
    snCategorieClient.charger(false);
    snCategorieClient.setSelectionParChampRPG(lexique, "CLCAT");
    
    // Renseigne le champs numéro de téléphone.
    CLTEL.setEnabled(!isConsultation);
    
    // Renseigne le champs numéro de fax.
    CLFAX.setEnabled(!isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snCivilite.renseignerChampRPG(lexique, "RECIV");
    snCodePostalCommune.renseignerChampRPG(lexique, "CLCDP", "CLVILR", false);
    snPays.renseignerChampRPG(lexique, "CLCOP", "CLPAY");
    snRepresentant.renseignerChampRPG(lexique, "CLREP");
    snCategorieClient.renseignerChampRPG(lexique, "CLCAT");
    
    if (rbTypeProspect.isSelected()) {
      lexique.HostFieldPutData("CLIN29", 0, "P");
    }
    else {
      lexique.HostFieldPutData("CLIN29", 0, "");
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * 
   * Lecture de la sécurité 180 pour vérifier le droit à modifier le représentant.
   * @return boolean
   */
  private boolean isRepresentantNonModifiable() {
    return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_NON_AUTORISE_A_MODIFIER_REPRESENTANT));
  }
  
  private void miChoixPossibleActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void miMemoriserChoixCurseurActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(pmBTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    lexique.WatchHelp(pmBTD.getInvoker().getName());
  }
  
  /**
   * Conditions d'affichage de certain bouton
   */
  private void activerBouton() {
    boolean isClientParticulier = lexique.isTrue("61");
    boolean isHistorique = lexique.isTrue("96");
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    
    if (!isHistorique && isConsultation) {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_EMBALLAGE_REPRIS, true);
    }
    else {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_EMBALLAGE_REPRIS, false);
    }
    if (!isClientParticulier) {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_CONTACT, true);
    }
    else {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_CONTACT, false);
    }
    if (isCreation) {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_BLOC_NOTE, false);
    }
    else {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_BLOC_NOTE, true);
    }
    
    if (!isCreation) {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_AFFICHAGE_DOCUMENT, true);
    }
    else {
      XRiBarreBouton.activerBouton(BOUTON_AFFICHER_AFFICHAGE_DOCUMENT, false);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Barre des boutons
   * @param pSNBouton le bouton activé
   */
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    XRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_METTRE_AFFICHAGE_COMPLET)) {
        lexique.HostScreenSendKey(this, "F7");
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_BLOC_NOTE)) {
        lexique.HostScreenSendKey(this, "F22");
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_MEMOS)) {
        lexique.HostScreenSendKey(this, "F23");
      }
      else if (pSNBouton.isBouton(BOUTON_COPIER_BLOC_ADRESSE)) {
        copierBlocAdresse();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_CONTACT)) {
        if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
          lexique.HostFieldPutData("TCI1", 0, "X");
        }
        else {
          lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI1"));
        }
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_EMBALLAGE_REPRIS)) {
        if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
          lexique.HostFieldPutData("TCI1", 0, "E");
        }
        else {
          lexique.HostFieldPutData("V06FO", 0, "E");
        }
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_AFFICHAGE_DOCUMENT)) {
        int numero = Integer.parseInt(lexique.HostFieldGetData("INDCLI").trim());
        String stringSuffixe = lexique.HostFieldGetData("INDLIV").trim();
        int suffixe = 0;
        if (!stringSuffixe.isEmpty()) {
          suffixe = Integer.parseInt(stringSuffixe);
        }
        IdClient idClient = IdClient.getInstance(snEtablissement.getIdSelection(), numero, suffixe);
        CritereDocumentStocke critere = new CritereDocumentStocke();
        critere.setIdEtablissement(snEtablissement.getIdSelection());
        critere.setIdClient(idClient);
        ModeleListeDocumentStocke modele = new ModeleListeDocumentStocke(getSession(), critere);
        modele.setNomClient(lexique.HostFieldGetData("CLNOM").trim());
        VueDocumentStocke vue = new VueDocumentStocke(modele);
        vue.afficher();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Copie le bloc adresse dans le presse-papier.
   */
  private void copierBlocAdresse() {
    String RC = System.getProperty("line.separator");
    String texte = "";
    boolean isClientParticulier = lexique.isTrue("61");
    if (isClientParticulier) {
      if (snCivilite.getSelection() != null) {
        texte = snCivilite.getSelection().getLibelle() + " ";
      }
      texte += RENOM.nettoyerLaSaisie() + " " + REPRE.nettoyerLaSaisie();
    }
    else {
      texte = CLNOM.nettoyerLaSaisie() + RC + CLCPL.nettoyerLaSaisie();
    }
    texte += RC + CLRUE.nettoyerLaSaisie() + RC + CLLOC.nettoyerLaSaisie();
    
    // Contrôle du composant snCodePostalcommune
    if (snCodePostalCommune.getSelection() != null) {
      texte += RC + snCodePostalCommune.getSelection().getCodePostal() + " " + snCodePostalCommune.getSelection().getVille();
    }
    // Contrôle du composant snPays
    if (snPays.getSelection() != null) {
      texte += RC + snPays.getSelection().getLibelle();
    }
    // Envoi du text dans le presse-papier
    Clipboard.envoyerTexte(texte);
  }
  
  private void snPaysValueChanged(SNComposantEvent e) {
    try {
      // La recherche du composant est désactivée si le code pays correspond à un pays étranger
      boolean desactivationRecherche = false;
      if (snPays.getSelection() != null && !snPays.getSelection().isFrance()) {
        desactivationRecherche = true;
      }
      snCodePostalCommune.setDesactivationRecherche(desactivationRecherche);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-générées (JFormDesigner)
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    pnlChoixTypeClient = new SNPanel();
    rbTypeClient = new SNRadioButton();
    rbTypeProspect = new SNRadioButton();
    pnlCoordonnesClient = new SNPanelTitre();
    lbCivilite = new SNLabelChamp();
    snCivilite = new SNCivilite();
    CLCPL = new XRiTextField();
    CLLOC = new XRiTextField();
    lbNom = new SNLabelChamp();
    RENOM = new XRiTextField();
    CLNOM = new XRiTextField();
    lbPrenom = new SNLabelChamp();
    REPRE = new XRiTextField();
    lbRue = new SNLabelChamp();
    CLRUE = new XRiTextField();
    lbLocalite = new SNLabelChamp();
    lbCodePostal = new SNLabelChamp();
    snCodePostalCommune = new SNCodePostalCommune();
    lbPays = new SNLabelChamp();
    sNPanel6 = new SNPanel();
    snPays = new SNPays();
    lbMail = new SNLabelChamp();
    RENETR = new XRiTextField();
    lbTelephone = new SNLabelChamp();
    CLTEL = new XRiTextField();
    lbFax = new SNLabelChamp();
    CLFAX = new XRiTextField();
    pnlCoordonneClient = new SNPanel();
    lbObservation = new SNLabelChamp();
    CLOBS = new XRiTextField();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlNumeroClient = new SNPanel();
    lbClient = new SNLabelChamp();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    sNPanel1 = new SNPanel();
    lbRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    lbCodeCategorie = new SNLabelChamp();
    snCategorieClient = new SNCategorieClient();
    lbModeReglement = new SNLabelChamp();
    CLRGL = new XRiTextField();
    XRiBarreBouton = new XRiBarreBouton();
    pmBTD = new JPopupMenu();
    miChoixPossible = new JMenuItem();
    miAideEnLigne = new JMenuItem();
    CLPAY = new XRiTextField();
    ULBRGL = new XRiTextField();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- bpPresentation ----
      bpPresentation.setText("Fiche client");
      bpPresentation.setName("bpPresentation");
      pnlNord.add(bpPresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
        
        // ======== pnlChoixTypeClient ========
        {
          pnlChoixTypeClient.setName("pnlChoixTypeClient");
          pnlChoixTypeClient.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlChoixTypeClient.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlChoixTypeClient.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlChoixTypeClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlChoixTypeClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- rbTypeClient ----
          rbTypeClient.setText("Client");
          rbTypeClient.setMinimumSize(new Dimension(100, 30));
          rbTypeClient.setPreferredSize(new Dimension(100, 30));
          rbTypeClient.setMaximumSize(new Dimension(100, 30));
          rbTypeClient.setName("rbTypeClient");
          pnlChoixTypeClient.add(rbTypeClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- rbTypeProspect ----
          rbTypeProspect.setText("Prospect");
          rbTypeProspect.setMinimumSize(new Dimension(100, 30));
          rbTypeProspect.setPreferredSize(new Dimension(100, 30));
          rbTypeProspect.setMaximumSize(new Dimension(100, 30));
          rbTypeProspect.setName("rbTypeProspect");
          pnlChoixTypeClient.add(rbTypeProspect, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlChoixTypeClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCoordonnesClient ========
        {
          pnlCoordonnesClient.setPreferredSize(new Dimension(300, 350));
          pnlCoordonnesClient.setOpaque(false);
          pnlCoordonnesClient.setTitre("Coordonn\u00e9es du client");
          pnlCoordonnesClient.setMinimumSize(new Dimension(300, 350));
          pnlCoordonnesClient.setName("pnlCoordonnesClient");
          pnlCoordonnesClient.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonnesClient.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCoordonnesClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonnesClient.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonnesClient.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCivilite ----
          lbCivilite.setText("Civilit\u00e9");
          lbCivilite.setMaximumSize(new Dimension(115, 30));
          lbCivilite.setMinimumSize(new Dimension(115, 30));
          lbCivilite.setPreferredSize(new Dimension(115, 30));
          lbCivilite.setName("lbCivilite");
          pnlCoordonnesClient.add(lbCivilite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCivilite ----
          snCivilite.setName("snCivilite");
          pnlCoordonnesClient.add(snCivilite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- CLCPL ----
          CLCPL.setBackground(Color.white);
          CLCPL.setPreferredSize(new Dimension(300, 30));
          CLCPL.setMinimumSize(new Dimension(300, 30));
          CLCPL.setMaximumSize(new Dimension(300, 30));
          CLCPL.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLCPL.setName("CLCPL");
          pnlCoordonnesClient.add(CLCPL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- CLLOC ----
          CLLOC.setBackground(Color.white);
          CLLOC.setMinimumSize(new Dimension(300, 30));
          CLLOC.setMaximumSize(new Dimension(300, 30));
          CLLOC.setPreferredSize(new Dimension(300, 30));
          CLLOC.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLLOC.setName("CLLOC");
          pnlCoordonnesClient.add(CLLOC, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNom ----
          lbNom.setText("Voir code");
          lbNom.setPreferredSize(new Dimension(115, 30));
          lbNom.setMinimumSize(new Dimension(115, 30));
          lbNom.setMaximumSize(new Dimension(115, 30));
          lbNom.setName("lbNom");
          pnlCoordonnesClient.add(lbNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- RENOM ----
          RENOM.setBackground(Color.white);
          RENOM.setFont(new Font("sansserif", Font.PLAIN, 14));
          RENOM.setMaximumSize(new Dimension(300, 30));
          RENOM.setMinimumSize(new Dimension(300, 30));
          RENOM.setPreferredSize(new Dimension(300, 30));
          RENOM.setName("RENOM");
          pnlCoordonnesClient.add(RENOM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- CLNOM ----
          CLNOM.setBackground(Color.white);
          CLNOM.setMaximumSize(new Dimension(300, 30));
          CLNOM.setMinimumSize(new Dimension(300, 30));
          CLNOM.setPreferredSize(new Dimension(300, 30));
          CLNOM.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLNOM.setName("CLNOM");
          pnlCoordonnesClient.add(CLNOM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPrenom ----
          lbPrenom.setText("Voir code");
          lbPrenom.setPreferredSize(new Dimension(115, 30));
          lbPrenom.setMinimumSize(new Dimension(115, 30));
          lbPrenom.setMaximumSize(new Dimension(115, 30));
          lbPrenom.setName("lbPrenom");
          pnlCoordonnesClient.add(lbPrenom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- REPRE ----
          REPRE.setBackground(Color.white);
          REPRE.setPreferredSize(new Dimension(300, 30));
          REPRE.setMinimumSize(new Dimension(300, 30));
          REPRE.setMaximumSize(new Dimension(300, 30));
          REPRE.setName("REPRE");
          pnlCoordonnesClient.add(REPRE, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRue ----
          lbRue.setText("Adresse 1 ");
          lbRue.setMaximumSize(new Dimension(115, 30));
          lbRue.setMinimumSize(new Dimension(115, 30));
          lbRue.setPreferredSize(new Dimension(115, 30));
          lbRue.setName("lbRue");
          pnlCoordonnesClient.add(lbRue, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CLRUE ----
          CLRUE.setBackground(Color.white);
          CLRUE.setMinimumSize(new Dimension(300, 30));
          CLRUE.setMaximumSize(new Dimension(300, 30));
          CLRUE.setPreferredSize(new Dimension(300, 30));
          CLRUE.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLRUE.setName("CLRUE");
          pnlCoordonnesClient.add(CLRUE, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLocalite ----
          lbLocalite.setText("Adresse 2");
          lbLocalite.setPreferredSize(new Dimension(115, 30));
          lbLocalite.setMinimumSize(new Dimension(115, 30));
          lbLocalite.setMaximumSize(new Dimension(115, 30));
          lbLocalite.setName("lbLocalite");
          pnlCoordonnesClient.add(lbLocalite, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbCodePostal ----
          lbCodePostal.setText("Commune");
          lbCodePostal.setMaximumSize(new Dimension(115, 30));
          lbCodePostal.setMinimumSize(new Dimension(115, 30));
          lbCodePostal.setPreferredSize(new Dimension(115, 30));
          lbCodePostal.setName("lbCodePostal");
          pnlCoordonnesClient.add(lbCodePostal, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCodePostalCommune ----
          snCodePostalCommune.setName("snCodePostalCommune");
          pnlCoordonnesClient.add(snCodePostalCommune, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPays ----
          lbPays.setText("Pays");
          lbPays.setPreferredSize(new Dimension(115, 30));
          lbPays.setMinimumSize(new Dimension(115, 30));
          lbPays.setMaximumSize(new Dimension(115, 30));
          lbPays.setName("lbPays");
          pnlCoordonnesClient.add(lbPays, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== sNPanel6 ========
          {
            sNPanel6.setName("sNPanel6");
            sNPanel6.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel6.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel6.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel6.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snPays ----
            snPays.setMinimumSize(new Dimension(300, 30));
            snPays.setPreferredSize(new Dimension(300, 30));
            snPays.setName("snPays");
            snPays.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPaysValueChanged(e);
              }
            });
            sNPanel6.add(snPays, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCoordonnesClient.add(sNPanel6, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMail ----
          lbMail.setText("Email");
          lbMail.setPreferredSize(new Dimension(115, 30));
          lbMail.setMinimumSize(new Dimension(115, 30));
          lbMail.setMaximumSize(new Dimension(115, 30));
          lbMail.setName("lbMail");
          pnlCoordonnesClient.add(lbMail, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- RENETR ----
          RENETR.setFont(new Font("sansserif", Font.PLAIN, 14));
          RENETR.setPreferredSize(new Dimension(250, 30));
          RENETR.setMinimumSize(new Dimension(250, 30));
          RENETR.setMaximumSize(new Dimension(250, 30));
          RENETR.setName("RENETR");
          pnlCoordonnesClient.add(RENETR, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTelephone ----
          lbTelephone.setText("T\u00e9l\u00e9phone");
          lbTelephone.setPreferredSize(new Dimension(75, 30));
          lbTelephone.setMinimumSize(new Dimension(75, 30));
          lbTelephone.setMaximumSize(new Dimension(75, 30));
          lbTelephone.setName("lbTelephone");
          pnlCoordonnesClient.add(lbTelephone, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- CLTEL ----
          CLTEL.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLTEL.setMaximumSize(new Dimension(110, 30));
          CLTEL.setMinimumSize(new Dimension(110, 30));
          CLTEL.setPreferredSize(new Dimension(110, 30));
          CLTEL.setName("CLTEL");
          pnlCoordonnesClient.add(CLTEL, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFax ----
          lbFax.setText("Fax");
          lbFax.setMaximumSize(new Dimension(115, 30));
          lbFax.setMinimumSize(new Dimension(115, 30));
          lbFax.setPreferredSize(new Dimension(115, 30));
          lbFax.setName("lbFax");
          pnlCoordonnesClient.add(lbFax, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CLFAX ----
          CLFAX.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLFAX.setMaximumSize(new Dimension(110, 30));
          CLFAX.setMinimumSize(new Dimension(110, 30));
          CLFAX.setPreferredSize(new Dimension(110, 30));
          CLFAX.setName("CLFAX");
          pnlCoordonnesClient.add(CLFAX, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCoordonnesClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCoordonneClient ========
        {
          pnlCoordonneClient.setName("pnlCoordonneClient");
          pnlCoordonneClient.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonneClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneClient.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCoordonneClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonneClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbObservation ----
          lbObservation.setText("Observation");
          lbObservation.setBackground(Color.white);
          lbObservation.setMaximumSize(new Dimension(100, 30));
          lbObservation.setMinimumSize(new Dimension(100, 30));
          lbObservation.setPreferredSize(new Dimension(125, 30));
          lbObservation.setName("lbObservation");
          pnlCoordonneClient.add(lbObservation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CLOBS ----
          CLOBS.setBackground(Color.white);
          CLOBS.setPreferredSize(new Dimension(224, 30));
          CLOBS.setMinimumSize(new Dimension(224, 30));
          CLOBS.setMaximumSize(new Dimension(224, 30));
          CLOBS.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLOBS.setName("CLOBS");
          pnlCoordonneClient.add(CLOBS, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(pnlCoordonneClient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setEnabled(false);
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlNumeroClient ========
        {
          pnlNumeroClient.setName("pnlNumeroClient");
          pnlNumeroClient.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlNumeroClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlNumeroClient.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlNumeroClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlNumeroClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbClient ----
          lbClient.setText("Num\u00e9ro client");
          lbClient.setPreferredSize(new Dimension(165, 30));
          lbClient.setMinimumSize(new Dimension(165, 30));
          lbClient.setMaximumSize(new Dimension(165, 30));
          lbClient.setName("lbClient");
          pnlNumeroClient.add(lbClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- INDCLI ----
          INDCLI.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDCLI.setText("@INDCLI@");
          INDCLI.setPreferredSize(new Dimension(70, 30));
          INDCLI.setMinimumSize(new Dimension(70, 30));
          INDCLI.setMaximumSize(new Dimension(70, 30));
          INDCLI.setName("INDCLI");
          pnlNumeroClient.add(INDCLI, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- INDLIV ----
          INDLIV.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDLIV.setText("@INDLIV@");
          INDLIV.setPreferredSize(new Dimension(40, 30));
          INDLIV.setMinimumSize(new Dimension(40, 30));
          INDLIV.setMaximumSize(new Dimension(40, 30));
          INDLIV.setName("INDLIV");
          pnlNumeroClient.add(INDLIV, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlNumeroClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbRepresentant ----
          lbRepresentant.setText("Repr\u00e9sentant");
          lbRepresentant.setMaximumSize(new Dimension(100, 30));
          lbRepresentant.setMinimumSize(new Dimension(100, 30));
          lbRepresentant.setPreferredSize(new Dimension(125, 30));
          lbRepresentant.setName("lbRepresentant");
          sNPanel1.add(lbRepresentant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snRepresentant ----
          snRepresentant.setName("snRepresentant");
          sNPanel1.add(snRepresentant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCodeCategorie ----
          lbCodeCategorie.setText("Cat\u00e9gorie client");
          lbCodeCategorie.setPreferredSize(new Dimension(125, 30));
          lbCodeCategorie.setMinimumSize(new Dimension(100, 30));
          lbCodeCategorie.setMaximumSize(new Dimension(100, 30));
          lbCodeCategorie.setName("lbCodeCategorie");
          sNPanel1.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCategorieClient ----
          snCategorieClient.setMaximumSize(new Dimension(300, 30));
          snCategorieClient.setMinimumSize(new Dimension(300, 30));
          snCategorieClient.setPreferredSize(new Dimension(300, 30));
          snCategorieClient.setFont(new Font("sansserif", Font.PLAIN, 14));
          snCategorieClient.setName("snCategorieClient");
          sNPanel1.add(snCategorieClient, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbModeReglement ----
          lbModeReglement.setText("Mode de r\u00e8glement");
          lbModeReglement.setMinimumSize(new Dimension(100, 30));
          lbModeReglement.setMaximumSize(new Dimension(100, 30));
          lbModeReglement.setPreferredSize(new Dimension(125, 30));
          lbModeReglement.setName("lbModeReglement");
          sNPanel1.add(lbModeReglement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- CLRGL ----
          CLRGL.setComponentPopupMenu(pmBTD);
          CLRGL.setFont(new Font("sansserif", Font.PLAIN, 14));
          CLRGL.setPreferredSize(new Dimension(36, 30));
          CLRGL.setMinimumSize(new Dimension(36, 30));
          CLRGL.setMaximumSize(new Dimension(36, 30));
          CLRGL.setName("CLRGL");
          sNPanel1.add(CLRGL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(sNPanel1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- XRiBarreBouton ----
    XRiBarreBouton.setName("XRiBarreBouton");
    add(XRiBarreBouton, BorderLayout.SOUTH);
    
    // ======== pmBTD ========
    {
      pmBTD.setName("pmBTD");
      
      // ---- miChoixPossible ----
      miChoixPossible.setText("Choix possibles");
      miChoixPossible.setName("miChoixPossible");
      miChoixPossible.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossibleActionPerformed(e);
        }
      });
      pmBTD.add(miChoixPossible);
      
      // ---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      pmBTD.add(miAideEnLigne);
    }
    
    // ---- CLPAY ----
    CLPAY.setBackground(Color.white);
    CLPAY.setPreferredSize(new Dimension(250, 30));
    CLPAY.setMinimumSize(new Dimension(250, 30));
    CLPAY.setMaximumSize(new Dimension(250, 30));
    CLPAY.setFont(new Font("sansserif", Font.PLAIN, 14));
    CLPAY.setName("CLPAY");
    
    // ---- ULBRGL ----
    ULBRGL.setFont(new Font("sansserif", Font.PLAIN, 14));
    ULBRGL.setMinimumSize(new Dimension(250, 30));
    ULBRGL.setMaximumSize(new Dimension(250, 30));
    ULBRGL.setPreferredSize(new Dimension(250, 30));
    ULBRGL.setName("ULBRGL");
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbTypeClient);
    buttonGroup1.add(rbTypeProspect);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlGauche;
  private SNPanel pnlChoixTypeClient;
  private SNRadioButton rbTypeClient;
  private SNRadioButton rbTypeProspect;
  private SNPanelTitre pnlCoordonnesClient;
  private SNLabelChamp lbCivilite;
  private SNCivilite snCivilite;
  private XRiTextField CLCPL;
  private XRiTextField CLLOC;
  private SNLabelChamp lbNom;
  private XRiTextField RENOM;
  private XRiTextField CLNOM;
  private SNLabelChamp lbPrenom;
  private XRiTextField REPRE;
  private SNLabelChamp lbRue;
  private XRiTextField CLRUE;
  private SNLabelChamp lbLocalite;
  private SNLabelChamp lbCodePostal;
  private SNCodePostalCommune snCodePostalCommune;
  private SNLabelChamp lbPays;
  private SNPanel sNPanel6;
  private SNPays snPays;
  private SNLabelChamp lbMail;
  private XRiTextField RENETR;
  private SNLabelChamp lbTelephone;
  private XRiTextField CLTEL;
  private SNLabelChamp lbFax;
  private XRiTextField CLFAX;
  private SNPanel pnlCoordonneClient;
  private SNLabelChamp lbObservation;
  private XRiTextField CLOBS;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanel pnlNumeroClient;
  private SNLabelChamp lbClient;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private SNPanel sNPanel1;
  private SNLabelChamp lbRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbCodeCategorie;
  private SNCategorieClient snCategorieClient;
  private SNLabelChamp lbModeReglement;
  private XRiTextField CLRGL;
  private XRiBarreBouton XRiBarreBouton;
  private JPopupMenu pmBTD;
  private JMenuItem miChoixPossible;
  private JMenuItem miAideEnLigne;
  private XRiTextField CLPAY;
  private XRiTextField ULBRGL;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
