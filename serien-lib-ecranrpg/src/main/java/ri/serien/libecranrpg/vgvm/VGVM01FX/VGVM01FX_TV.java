
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_TV extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] TVOP6_Value = { "", "R", "S", };
  private String[] TVOP5_Value = { "", "R", "S", };
  private String[] TVOP4_Value = { "", "R", "S", };
  private String[] TVOP3_Value = { "", "R", "S", };
  private String[] TVOP2_Value = { "", "R", "S", };
  private String[] TVOP1_Value = { "", "R", "S", };
  
  public VGVM01FX_TV(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TVOP6.setValeurs(TVOP6_Value, null);
    TVOP5.setValeurs(TVOP5_Value, null);
    TVOP4.setValeurs(TVOP4_Value, null);
    TVOP3.setValeurs(TVOP3_Value, null);
    TVOP2.setValeurs(TVOP2_Value, null);
    TVOP1.setValeurs(TVOP1_Value, null);
    TVFRS.setValeursSelection("OUI", "NON");
    TVROC.setValeursSelection("OUI", "NON");
    TVROV.setValeursSelection("OUI", "NON");
    TVPRV.setValeursSelection("OUI", "NON");
    TVPRC.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_56 = new JLabel();
    TVLIB = new XRiTextField();
    TVOP1 = new XRiComboBox();
    TVOP2 = new XRiComboBox();
    TVOP3 = new XRiComboBox();
    TVOP4 = new XRiComboBox();
    TVOP5 = new XRiComboBox();
    TVOP6 = new XRiComboBox();
    OBJ_65 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_81 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_57 = new JLabel();
    TVART = new XRiTextField();
    TVCLA1 = new XRiTextField();
    TVCLA2 = new XRiTextField();
    TVREF = new XRiTextField();
    TVFAM = new XRiTextField();
    TVRTA = new XRiTextField();
    OBJ_59 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_79 = new JLabel();
    panel1 = new JPanel();
    TVPRC = new XRiCheckBox();
    TVPRV = new XRiCheckBox();
    panel3 = new JPanel();
    TVROV = new XRiCheckBox();
    TVROC = new XRiCheckBox();
    panel4 = new JPanel();
    OBJ_99 = new JLabel();
    TVNBC = new XRiTextField();
    OBJ_100 = new JLabel();
    TVARG = new XRiTextField();
    OBJ_101 = new JLabel();
    TVDEB = new XRiTextField();
    panel5 = new JPanel();
    TVFRS = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(780, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Type variante");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_56 ----
              OBJ_56.setText("Description");
              OBJ_56.setName("OBJ_56");
              panel2.add(OBJ_56);
              OBJ_56.setBounds(10, 14, 150, 20);

              //---- TVLIB ----
              TVLIB.setComponentPopupMenu(BTD);
              TVLIB.setName("TVLIB");
              panel2.add(TVLIB);
              TVLIB.setBounds(170, 10, 310, TVLIB.getPreferredSize().height);
            }
            xTitledPanel3ContentContainer.add(panel2);
            panel2.setBounds(12, 15, 672, 50);

            //---- TVOP1 ----
            TVOP1.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Restriction / variantes suivantes",
              "S\u00e9lection fonction variante pr\u00e9c\u00e9dente"
            }));
            TVOP1.setComponentPopupMenu(BTD);
            TVOP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TVOP1.setName("TVOP1");
            xTitledPanel3ContentContainer.add(TVOP1);
            TVOP1.setBounds(440, 100, 247, TVOP1.getPreferredSize().height);

            //---- TVOP2 ----
            TVOP2.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Restriction / variantes suivantes",
              "S\u00e9lection fonction variante pr\u00e9c\u00e9dente"
            }));
            TVOP2.setComponentPopupMenu(BTD);
            TVOP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TVOP2.setName("TVOP2");
            xTitledPanel3ContentContainer.add(TVOP2);
            TVOP2.setBounds(440, 129, 247, TVOP2.getPreferredSize().height);

            //---- TVOP3 ----
            TVOP3.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Restriction / variantes suivantes",
              "S\u00e9lection fonction variante pr\u00e9c\u00e9dente"
            }));
            TVOP3.setComponentPopupMenu(BTD);
            TVOP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TVOP3.setName("TVOP3");
            xTitledPanel3ContentContainer.add(TVOP3);
            TVOP3.setBounds(440, 157, 247, TVOP3.getPreferredSize().height);

            //---- TVOP4 ----
            TVOP4.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Restriction / variantes suivantes",
              "S\u00e9lection fonction variante pr\u00e9c\u00e9dente"
            }));
            TVOP4.setComponentPopupMenu(BTD);
            TVOP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TVOP4.setName("TVOP4");
            xTitledPanel3ContentContainer.add(TVOP4);
            TVOP4.setBounds(440, 186, 247, TVOP4.getPreferredSize().height);

            //---- TVOP5 ----
            TVOP5.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Restriction / variantes suivantes",
              "S\u00e9lection fonction variante pr\u00e9c\u00e9dente"
            }));
            TVOP5.setComponentPopupMenu(BTD);
            TVOP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TVOP5.setName("TVOP5");
            xTitledPanel3ContentContainer.add(TVOP5);
            TVOP5.setBounds(440, 215, 247, TVOP5.getPreferredSize().height);

            //---- TVOP6 ----
            TVOP6.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Restriction / variantes suivantes",
              "S\u00e9lection fonction variante pr\u00e9c\u00e9dente"
            }));
            TVOP6.setComponentPopupMenu(BTD);
            TVOP6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TVOP6.setName("TVOP6");
            xTitledPanel3ContentContainer.add(TVOP6);
            TVOP6.setBounds(440, 244, 247, TVOP6.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setText("caract\u00e8res du mot de classement 1");
            OBJ_65.setName("OBJ_65");
            xTitledPanel3ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(215, 132, 211, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("caract\u00e8res du mot de classement 2");
            OBJ_69.setName("OBJ_69");
            xTitledPanel3ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(215, 160, 211, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("caract\u00e8res de la r\u00e9f.fournisseur");
            OBJ_81.setName("OBJ_81");
            xTitledPanel3ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(215, 247, 211, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("caract\u00e8res du code famille");
            OBJ_73.setName("OBJ_73");
            xTitledPanel3ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(215, 189, 211, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("caract\u00e8res du code article");
            OBJ_61.setName("OBJ_61");
            xTitledPanel3ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(215, 103, 211, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("caract\u00e8res du code tarif");
            OBJ_77.setName("OBJ_77");
            xTitledPanel3ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(215, 218, 211, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("Recherche articles sur");
            OBJ_58.setName("OBJ_58");
            xTitledPanel3ContentContainer.add(OBJ_58);
            OBJ_58.setBounds(12, 103, 136, 20);

            //---- OBJ_57 ----
            OBJ_57.setText("Option");
            OBJ_57.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_57.setName("OBJ_57");
            xTitledPanel3ContentContainer.add(OBJ_57);
            OBJ_57.setBounds(440, 75, 247, 17);

            //---- TVART ----
            TVART.setComponentPopupMenu(BTD);
            TVART.setName("TVART");
            xTitledPanel3ContentContainer.add(TVART);
            TVART.setBounds(181, 99, 30, TVART.getPreferredSize().height);

            //---- TVCLA1 ----
            TVCLA1.setComponentPopupMenu(BTD);
            TVCLA1.setName("TVCLA1");
            xTitledPanel3ContentContainer.add(TVCLA1);
            TVCLA1.setBounds(181, 128, 30, TVCLA1.getPreferredSize().height);

            //---- TVCLA2 ----
            TVCLA2.setComponentPopupMenu(BTD);
            TVCLA2.setName("TVCLA2");
            xTitledPanel3ContentContainer.add(TVCLA2);
            TVCLA2.setBounds(181, 156, 30, TVCLA2.getPreferredSize().height);

            //---- TVREF ----
            TVREF.setComponentPopupMenu(BTD);
            TVREF.setName("TVREF");
            xTitledPanel3ContentContainer.add(TVREF);
            TVREF.setBounds(181, 243, 30, TVREF.getPreferredSize().height);

            //---- TVFAM ----
            TVFAM.setComponentPopupMenu(BTD);
            TVFAM.setName("TVFAM");
            xTitledPanel3ContentContainer.add(TVFAM);
            TVFAM.setBounds(181, 185, 30, TVFAM.getPreferredSize().height);

            //---- TVRTA ----
            TVRTA.setComponentPopupMenu(BTD);
            TVRTA.setName("TVRTA");
            xTitledPanel3ContentContainer.add(TVRTA);
            TVRTA.setBounds(181, 214, 30, TVRTA.getPreferredSize().height);

            //---- OBJ_59 ----
            OBJ_59.setText("1");
            OBJ_59.setName("OBJ_59");
            xTitledPanel3ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(167, 103, 10, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("2");
            OBJ_63.setName("OBJ_63");
            xTitledPanel3ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(167, 132, 10, 20);

            //---- OBJ_67 ----
            OBJ_67.setText("3");
            OBJ_67.setName("OBJ_67");
            xTitledPanel3ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(167, 160, 10, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("4");
            OBJ_71.setName("OBJ_71");
            xTitledPanel3ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(167, 189, 10, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("5");
            OBJ_75.setName("OBJ_75");
            xTitledPanel3ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(167, 218, 10, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("6");
            OBJ_79.setName("OBJ_79");
            xTitledPanel3ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(167, 247, 10, 20);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("Proposition de remplacement"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- TVPRC ----
              TVPRC.setText("\u00e0 la fabrication");
              TVPRC.setComponentPopupMenu(BTD);
              TVPRC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TVPRC.setName("TVPRC");
              panel1.add(TVPRC);
              TVPRC.setBounds(185, 30, 112, 20);

              //---- TVPRV ----
              TVPRV.setText("\u00e0 la vente");
              TVPRV.setComponentPopupMenu(BTD);
              TVPRV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TVPRV.setName("TVPRV");
              panel1.add(TVPRV);
              TVPRV.setBounds(20, 30, 83, 20);
            }
            xTitledPanel3ContentContainer.add(panel1);
            panel1.setBounds(10, 340, 315, 65);

            //======== panel3 ========
            {
              panel3.setBorder(new TitledBorder("Remplacement obligatoire"));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- TVROV ----
              TVROV.setText("\u00e0 la vente");
              TVROV.setComponentPopupMenu(BTD);
              TVROV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TVROV.setName("TVROV");
              panel3.add(TVROV);
              TVROV.setBounds(15, 30, 83, 20);

              //---- TVROC ----
              TVROC.setText("\u00e0 la fabrication");
              TVROC.setComponentPopupMenu(BTD);
              TVROC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TVROC.setName("TVROC");
              panel3.add(TVROC);
              TVROC.setBounds(170, 30, 112, 20);
            }
            xTitledPanel3ContentContainer.add(panel3);
            panel3.setBounds(375, 340, 315, 65);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Configurateur"));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- OBJ_99 ----
              OBJ_99.setText("G\u00e9n\u00e9ration code avec");
              OBJ_99.setName("OBJ_99");
              panel4.add(OBJ_99);
              OBJ_99.setBounds(15, 34, 125, 20);

              //---- TVNBC ----
              TVNBC.setComponentPopupMenu(BTD);
              TVNBC.setName("TVNBC");
              panel4.add(TVNBC);
              TVNBC.setBounds(140, 30, 20, TVNBC.getPreferredSize().height);

              //---- OBJ_100 ----
              OBJ_100.setText("car. de l'argument N\u00b0");
              OBJ_100.setName("OBJ_100");
              panel4.add(OBJ_100);
              OBJ_100.setBounds(280, 34, 126, 20);

              //---- TVARG ----
              TVARG.setComponentPopupMenu(BTD);
              TVARG.setName("TVARG");
              panel4.add(TVARG);
              TVARG.setBounds(410, 30, 20, TVARG.getPreferredSize().height);

              //---- OBJ_101 ----
              OBJ_101.setText("\u00e0 partir de");
              OBJ_101.setName("OBJ_101");
              panel4.add(OBJ_101);
              OBJ_101.setBounds(515, 34, 63, 20);

              //---- TVDEB ----
              TVDEB.setComponentPopupMenu(BTD);
              TVDEB.setName("TVDEB");
              panel4.add(TVDEB);
              TVDEB.setBounds(580, 30, 30, TVDEB.getPreferredSize().height);
            }
            xTitledPanel3ContentContainer.add(panel4);
            panel4.setBounds(10, 415, 678, 72);

            //======== panel5 ========
            {
              panel5.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- TVFRS ----
              TVFRS.setText("Recherche article du m\u00eame fournisseur");
              TVFRS.setComponentPopupMenu(BTD);
              TVFRS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TVFRS.setName("TVFRS");
              panel5.add(TVFRS);
              TVFRS.setBounds(5, 7, 258, 20);
            }
            xTitledPanel3ContentContainer.add(panel5);
            panel5.setBounds(12, 285, 265, 36);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(25, Short.MAX_VALUE)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE)
                .addGap(23, 23, 23))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(15, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private JPanel panel2;
  private JLabel OBJ_56;
  private XRiTextField TVLIB;
  private XRiComboBox TVOP1;
  private XRiComboBox TVOP2;
  private XRiComboBox TVOP3;
  private XRiComboBox TVOP4;
  private XRiComboBox TVOP5;
  private XRiComboBox TVOP6;
  private JLabel OBJ_65;
  private JLabel OBJ_69;
  private JLabel OBJ_81;
  private JLabel OBJ_73;
  private JLabel OBJ_61;
  private JLabel OBJ_77;
  private JLabel OBJ_58;
  private JLabel OBJ_57;
  private XRiTextField TVART;
  private XRiTextField TVCLA1;
  private XRiTextField TVCLA2;
  private XRiTextField TVREF;
  private XRiTextField TVFAM;
  private XRiTextField TVRTA;
  private JLabel OBJ_59;
  private JLabel OBJ_63;
  private JLabel OBJ_67;
  private JLabel OBJ_71;
  private JLabel OBJ_75;
  private JLabel OBJ_79;
  private JPanel panel1;
  private XRiCheckBox TVPRC;
  private XRiCheckBox TVPRV;
  private JPanel panel3;
  private XRiCheckBox TVROV;
  private XRiCheckBox TVROC;
  private JPanel panel4;
  private JLabel OBJ_99;
  private XRiTextField TVNBC;
  private JLabel OBJ_100;
  private XRiTextField TVARG;
  private JLabel OBJ_101;
  private XRiTextField TVDEB;
  private JPanel panel5;
  private XRiCheckBox TVFRS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
