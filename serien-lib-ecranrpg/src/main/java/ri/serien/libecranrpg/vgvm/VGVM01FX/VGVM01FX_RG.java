
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_RG extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] RGCJO_Value = { "", "1", "2", "3", "4", "5", };
  private String[] RGSNS_Value = { "R", "P", };
  private String[] RGTYP_Value = { "E", "C", "B", "T", "R", "D", "P", "K", "I", "V", "A" };
  private String[] RGACC_Value = { "", "A", "C", };
  private String[] RGACC_Title = { "", "à l'acceptation", "Chèque comptant", };
  
  public VGVM01FX_RG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    RGCJO.setValeurs(RGCJO_Value, null);
    RGSNS.setValeurs(RGSNS_Value, null);
    RGTYP.setValeurs(RGTYP_Value, null);
    RGACC.setValeurs(RGACC_Value, RGACC_Title);
    RGEDT.setValeursSelection("1", " ");
    RGATT.setValeursSelection("1", " ");
    RGINT.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_43 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    RGTYP = new XRiComboBox();
    RGTXT = new XRiTextField();
    OBJ_79 = new JLabel();
    RGSNS = new XRiComboBox();
    RGLIB1 = new XRiTextField();
    OBJ_75 = new JLabel();
    RGCJO = new XRiComboBox();
    OBJ_77 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_76 = new JLabel();
    OBJ_42 = new JLabel();
    RGLIB2 = new XRiTextField();
    OBJ_41 = new JLabel();
    OBJ_80 = new JLabel();
    RGMTM = new XRiTextField();
    RGCRB = new XRiTextField();
    RGAFA = new XRiTextField();
    RGNBT = new XRiTextField();
    RGRGR = new XRiTextField();
    RGTRG = new XRiTextField();
    panel1 = new JPanel();
    RGEDT = new XRiCheckBox();
    RGACC = new XRiComboBox();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_78 = new JLabel();
    RGATT = new XRiCheckBox();
    RGINT = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_43 ----
          OBJ_43.setText("Etablissement");
          OBJ_43.setName("OBJ_43");
          p_tete_gauche.add(OBJ_43);
          OBJ_43.setBounds(5, 4, 93, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 1, 40, INDETB.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(160, 4, 36, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(200, 1, 34, INDTYP.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(260, 4, 39, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setText("@INDIND@");
          INDIND.setOpaque(false);
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(300, 1, 60, INDIND.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Code r\u00e8glement");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- RGTYP ----
            RGTYP.setModel(new DefaultComboBoxModel(new String[] {
              "Esp\u00e8ces",
              "Ch\u00e8ques",
              "Billets \u00e0 ordre",
              "Traites",
              "Relev\u00e9s de factures",
              "R\u00e8glements divers",
              "Pr\u00e9l\u00e8vements",
              "Cartes bancaires",
              "TIP",
              "Virement",
              "Avoir"
            }));
            RGTYP.setComponentPopupMenu(null);
            RGTYP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RGTYP.setName("RGTYP");
            xTitledPanel3ContentContainer.add(RGTYP);
            RGTYP.setBounds(235, 75, 145, RGTYP.getPreferredSize().height);

            //---- RGTXT ----
            RGTXT.setComponentPopupMenu(null);
            RGTXT.setName("RGTXT");
            xTitledPanel3ContentContainer.add(RGTXT);
            RGTXT.setBounds(20, 365, 610, RGTXT.getPreferredSize().height);

            //---- OBJ_79 ----
            OBJ_79.setText("Texte \u00e0 \u00e9diter sur la derni\u00e8re ligne de la facture");
            OBJ_79.setName("OBJ_79");
            xTitledPanel3ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(20, 345, 282, 20);

            //---- RGSNS ----
            RGSNS.setModel(new DefaultComboBoxModel(new String[] {
              "Recevoir",
              "Payer"
            }));
            RGSNS.setComponentPopupMenu(null);
            RGSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RGSNS.setName("RGSNS");
            xTitledPanel3ContentContainer.add(RGSNS);
            RGSNS.setBounds(235, 220, 85, RGSNS.getPreferredSize().height);

            //---- RGLIB1 ----
            RGLIB1.setToolTipText("partie \u00e9tit\u00e9e sur bons et factures");
            RGLIB1.setComponentPopupMenu(null);
            RGLIB1.setFont(new Font("Courier New", Font.BOLD, 14));
            RGLIB1.setName("RGLIB1");
            xTitledPanel3ContentContainer.add(RGLIB1);
            RGLIB1.setBounds(235, 35, 280, RGLIB1.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Nombres d'\u00e9ch\u00e9ances identiques");
            OBJ_75.setName("OBJ_75");
            xTitledPanel3ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(20, 119, 205, 20);

            //---- RGCJO ----
            RGCJO.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "1",
              "2",
              "3",
              "4",
              "5"
            }));
            RGCJO.setComponentPopupMenu(null);
            RGCJO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RGCJO.setName("RGCJO");
            xTitledPanel3ContentContainer.add(RGCJO);
            RGCJO.setBounds(665, 366, 50, RGCJO.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("Code r\u00e8glement associ\u00e9");
            OBJ_77.setToolTipText("remboursement = de sens oppos\u00e9");
            OBJ_77.setName("OBJ_77");
            xTitledPanel3ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(20, 299, 150, 20);

            //---- OBJ_40 ----
            OBJ_40.setText("Code de remplacement");
            OBJ_40.setName("OBJ_40");
            xTitledPanel3ContentContainer.add(OBJ_40);
            OBJ_40.setBounds(20, 264, 147, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Type de r\u00e8glement");
            OBJ_74.setName("OBJ_74");
            xTitledPanel3ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(20, 78, 123, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("Sens du r\u00e8glement");
            OBJ_76.setName("OBJ_76");
            xTitledPanel3ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(20, 223, 121, 20);

            //---- OBJ_42 ----
            OBJ_42.setText("Libell\u00e9 r\u00e8glement");
            OBJ_42.setName("OBJ_42");
            xTitledPanel3ContentContainer.add(OBJ_42);
            OBJ_42.setBounds(20, 39, 114, 20);

            //---- RGLIB2 ----
            RGLIB2.setComponentPopupMenu(null);
            RGLIB2.setFont(new Font("Courier New", Font.BOLD, 14));
            RGLIB2.setName("RGLIB2");
            xTitledPanel3ContentContainer.add(RGLIB2);
            RGLIB2.setBounds(520, 35, 140, RGLIB2.getPreferredSize().height);

            //---- OBJ_41 ----
            OBJ_41.setText("jusqu'au montant");
            OBJ_41.setName("OBJ_41");
            xTitledPanel3ContentContainer.add(OBJ_41);
            OBJ_41.setBounds(550, 264, 103, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("num\u00e9ro ZP/JO");
            OBJ_80.setName("OBJ_80");
            xTitledPanel3ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(665, 345, 90, 20);

            //---- RGMTM ----
            RGMTM.setComponentPopupMenu(null);
            RGMTM.setName("RGMTM");
            xTitledPanel3ContentContainer.add(RGMTM);
            RGMTM.setBounds(665, 260, 50, RGMTM.getPreferredSize().height);

            //---- RGCRB ----
            RGCRB.setToolTipText("remboursement = de sens oppos\u00e9");
            RGCRB.setComponentPopupMenu(BTD);
            RGCRB.setName("RGCRB");
            xTitledPanel3ContentContainer.add(RGCRB);
            RGCRB.setBounds(235, 295, 30, RGCRB.getPreferredSize().height);

            //---- RGAFA ----
            RGAFA.setComponentPopupMenu(null);
            RGAFA.setName("RGAFA");
            xTitledPanel3ContentContainer.add(RGAFA);
            RGAFA.setBounds(415, 74, 30, RGAFA.getPreferredSize().height);

            //---- RGNBT ----
            RGNBT.setComponentPopupMenu(null);
            RGNBT.setName("RGNBT");
            xTitledPanel3ContentContainer.add(RGNBT);
            RGNBT.setBounds(235, 115, 26, RGNBT.getPreferredSize().height);

            //---- RGRGR ----
            RGRGR.setComponentPopupMenu(BTD);
            RGRGR.setName("RGRGR");
            xTitledPanel3ContentContainer.add(RGRGR);
            RGRGR.setBounds(235, 260, 30, RGRGR.getPreferredSize().height);

            //---- RGTRG ----
            RGTRG.setComponentPopupMenu(null);
            RGTRG.setName("RGTRG");
            xTitledPanel3ContentContainer.add(RGTRG);
            RGTRG.setBounds(395, 74, 20, RGTRG.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- RGEDT ----
              RGEDT.setText("Ne pas \u00e9diter la traite");
              RGEDT.setComponentPopupMenu(BTD);
              RGEDT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              RGEDT.setName("RGEDT");
              panel1.add(RGEDT);
              RGEDT.setBounds(5, 15, 154, 20);

              //---- RGACC ----
              RGACC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              RGACC.setName("RGACC");
              panel1.add(RGACC);
              RGACC.setBounds(385, 12, 240, 26);

              //---- OBJ_49_OBJ_49 ----
              OBJ_49_OBJ_49.setText("Code acceptation");
              OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
              panel1.add(OBJ_49_OBJ_49);
              OBJ_49_OBJ_49.setBounds(230, 15, 130, 20);
            }
            xTitledPanel3ContentContainer.add(panel1);
            panel1.setBounds(10, 155, 700, 50);

            //---- OBJ_78 ----
            OBJ_78.setText("1.......10........20........");
            OBJ_78.setFont(new Font("Courier New", Font.PLAIN, 17));
            OBJ_78.setName("OBJ_78");
            xTitledPanel3ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(235, 5, 280, 30);

            //---- RGATT ----
            RGATT.setText("Activer les alertes sur les bons de vente");
            RGATT.setComponentPopupMenu(BTD);
            RGATT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RGATT.setName("RGATT");
            xTitledPanel3ContentContainer.add(RGATT);
            RGATT.setBounds(20, 405, 280, 20);

            //---- RGINT ----
            RGINT.setText("Interdit en vente");
            RGINT.setComponentPopupMenu(BTD);
            RGINT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RGINT.setName("RGINT");
            xTitledPanel3ContentContainer.add(RGINT);
            RGINT.setBounds(360, 405, 280, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_43;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private XRiComboBox RGTYP;
  private XRiTextField RGTXT;
  private JLabel OBJ_79;
  private XRiComboBox RGSNS;
  private XRiTextField RGLIB1;
  private JLabel OBJ_75;
  private XRiComboBox RGCJO;
  private JLabel OBJ_77;
  private JLabel OBJ_40;
  private JLabel OBJ_74;
  private JLabel OBJ_76;
  private JLabel OBJ_42;
  private XRiTextField RGLIB2;
  private JLabel OBJ_41;
  private JLabel OBJ_80;
  private XRiTextField RGMTM;
  private XRiTextField RGCRB;
  private XRiTextField RGAFA;
  private XRiTextField RGNBT;
  private XRiTextField RGRGR;
  private XRiTextField RGTRG;
  private JPanel panel1;
  private XRiCheckBox RGEDT;
  private XRiComboBox RGACC;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_78;
  private XRiCheckBox RGATT;
  private XRiCheckBox RGINT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
