
package ri.serien.libecranrpg.vgvm.VGVM43FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM43FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public VGVM43FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V01_OBJ_69_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T01@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL01@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP1@")).trim());
    V02_OBJ_72_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T02@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL02@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP2@")).trim());
    V03_OBJ_75_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T03@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL03@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP3@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL05@")).trim());
    V05_OBJ_81_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T05@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP5@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL06@")).trim());
    V06_OBJ_84_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T06@")).trim());
    OBJ_38.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP6@")).trim());
    OBJ_39.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL07@")).trim());
    V07_OBJ_87_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T07@")).trim());
    OBJ_40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP7@")).trim());
    OBJ_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL08@")).trim());
    V08_OBJ_90_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T08@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP8@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL09@")).trim());
    V09_OBJ_93_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T09@")).trim());
    OBJ_44.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP9@")).trim());
    OBJ_45.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL10@")).trim());
    V10_OBJ_96_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T10@")).trim());
    OBJ_46.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP10@")).trim());
    OBJ_47.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL11@")).trim());
    V11_OBJ_99_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T11@")).trim());
    OBJ_48.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP11@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL12@")).trim());
    V12_OBJ_102_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T12@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP12@")).trim());
    OBJ_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL13@")).trim());
    V13_OBJ_105_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T13@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP13@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL14@")).trim());
    V14_OBJ_108_51.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T14@")).trim());
    OBJ_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP14@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL15@")).trim());
    V15_OBJ_111_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T15@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP15@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL16@")).trim());
    V16_OBJ_114_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T16@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP16@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL17@")).trim());
    V17_OBJ_117_54.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T17@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP17@")).trim());
    OBJ_61.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL18@")).trim());
    V18_OBJ_120_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T18@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP18@")).trim());
    V04_OBJ_78_41.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T04@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZPL04@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERZP4@")).trim());
    OBJ_123.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Gestion des zones de sortie si présente et pas type booléen
    int width = 50;
    int height = 28;
    EBZP1.setVisible(
        (!lexique.HostFieldGetData("WZTT01").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT01").equalsIgnoreCase("B")));
    if (EBZP1.isVisible()) {
      width = calculerLongueur("WZTL01");
      EBZP1.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT01").equalsIgnoreCase("N")) {
        EBZP1.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP2.setVisible(
        (!lexique.HostFieldGetData("WZTT02").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT02").equalsIgnoreCase("B")));
    if (EBZP2.isVisible()) {
      width = calculerLongueur("WZTL02");
      EBZP2.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT02").equalsIgnoreCase("N")) {
        EBZP2.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP3.setVisible(
        (!lexique.HostFieldGetData("WZTT03").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT03").equalsIgnoreCase("B")));
    if (EBZP3.isVisible()) {
      width = calculerLongueur("WZTL03");
      EBZP3.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT03").equalsIgnoreCase("N")) {
        EBZP3.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP4.setVisible(
        (!lexique.HostFieldGetData("WZTT04").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT04").equalsIgnoreCase("B")));
    if (EBZP4.isVisible()) {
      width = calculerLongueur("WZTL04");
      EBZP4.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT04").equalsIgnoreCase("N")) {
        EBZP4.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP5.setVisible(
        (!lexique.HostFieldGetData("WZTT05").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT05").equalsIgnoreCase("B")));
    if (EBZP5.isVisible()) {
      width = calculerLongueur("WZTL05");
      EBZP5.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT05").equalsIgnoreCase("N")) {
        EBZP5.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP6.setVisible(
        (!lexique.HostFieldGetData("WZTT06").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT06").equalsIgnoreCase("B")));
    if (EBZP6.isVisible()) {
      width = calculerLongueur("WZTL06");
      EBZP6.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT06").equalsIgnoreCase("N")) {
        EBZP6.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP7.setVisible(
        (!lexique.HostFieldGetData("WZTT07").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT07").equalsIgnoreCase("B")));
    if (EBZP7.isVisible()) {
      width = calculerLongueur("WZTL07");
      EBZP7.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT07").equalsIgnoreCase("N")) {
        EBZP7.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP8.setVisible(
        (!lexique.HostFieldGetData("WZTT08").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT08").equalsIgnoreCase("B")));
    if (EBZP8.isVisible()) {
      width = calculerLongueur("WZTL08");
      EBZP8.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT08").equalsIgnoreCase("N")) {
        EBZP8.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP9.setVisible(
        (!lexique.HostFieldGetData("WZTT09").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT09").equalsIgnoreCase("B")));
    if (EBZP9.isVisible()) {
      width = calculerLongueur("WZTL09");
      EBZP9.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT09").equalsIgnoreCase("N")) {
        EBZP9.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP10.setVisible(
        (!lexique.HostFieldGetData("WZTT10").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT10").equalsIgnoreCase("B")));
    if (EBZP10.isVisible()) {
      width = calculerLongueur("WZTL10");
      EBZP10.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT10").equalsIgnoreCase("N")) {
        EBZP10.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP11.setVisible(
        (!lexique.HostFieldGetData("WZTT11").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT11").equalsIgnoreCase("B")));
    if (EBZP11.isVisible()) {
      width = calculerLongueur("WZTL11");
      EBZP11.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT11").equalsIgnoreCase("N")) {
        EBZP11.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP12.setVisible(
        (!lexique.HostFieldGetData("WZTT12").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT12").equalsIgnoreCase("B")));
    if (EBZP12.isVisible()) {
      width = calculerLongueur("WZTL12");
      EBZP12.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT12").equalsIgnoreCase("N")) {
        EBZP12.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP13.setVisible(
        (!lexique.HostFieldGetData("WZTT13").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT13").equalsIgnoreCase("B")));
    if (EBZP13.isVisible()) {
      width = calculerLongueur("WZTL13");
      EBZP13.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT13").equalsIgnoreCase("N")) {
        EBZP13.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP14.setVisible(
        (!lexique.HostFieldGetData("WZTT14").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT14").equalsIgnoreCase("B")));
    if (EBZP14.isVisible()) {
      width = calculerLongueur("WZTL14");
      EBZP14.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT14").equalsIgnoreCase("N")) {
        EBZP14.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP15.setVisible(
        (!lexique.HostFieldGetData("WZTT15").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT15").equalsIgnoreCase("B")));
    if (EBZP15.isVisible()) {
      width = calculerLongueur("WZTL15");
      EBZP15.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT15").equalsIgnoreCase("N")) {
        EBZP15.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP16.setVisible(
        (!lexique.HostFieldGetData("WZTT16").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT16").equalsIgnoreCase("B")));
    if (EBZP16.isVisible()) {
      width = calculerLongueur("WZTL16");
      EBZP16.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT16").equalsIgnoreCase("N")) {
        EBZP16.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP17.setVisible(
        (!lexique.HostFieldGetData("WZTT17").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT17").equalsIgnoreCase("B")));
    if (EBZP17.isVisible()) {
      width = calculerLongueur("WZTL17");
      EBZP17.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT17").equalsIgnoreCase("N")) {
        EBZP17.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    EBZP18.setVisible(
        (!lexique.HostFieldGetData("WZTT18").trim().equalsIgnoreCase("")) && (!lexique.HostFieldGetData("WZTT18").equalsIgnoreCase("B")));
    if (EBZP18.isVisible()) {
      width = calculerLongueur("WZTL18");
      EBZP18.setPreferredSize(new Dimension(width, height));
      if (lexique.HostFieldGetData("WZTT18").equalsIgnoreCase("N")) {
        EBZP18.setHorizontalAlignment(JTextField.RIGHT);
      }
    }
    repaint();
    
    // Gestion des cases à cocher (booléen)
    V18_OBJ_120_55.setVisible(lexique.HostFieldGetData("WZTT18").equalsIgnoreCase("B"));
    V18_OBJ_120_55.setSelected(lexique.HostFieldGetData("EBZP18").trim().equalsIgnoreCase("1"));
    V17_OBJ_117_54.setVisible(lexique.HostFieldGetData("WZTT17").equalsIgnoreCase("B"));
    V17_OBJ_117_54.setSelected(lexique.HostFieldGetData("EBZP17").trim().equalsIgnoreCase("1"));
    V16_OBJ_114_53.setVisible(lexique.HostFieldGetData("WZTT16").equalsIgnoreCase("B"));
    V16_OBJ_114_53.setSelected(lexique.HostFieldGetData("EBZP16").trim().equalsIgnoreCase("1"));
    V15_OBJ_111_52.setVisible(lexique.HostFieldGetData("WZTT15").equalsIgnoreCase("B"));
    V15_OBJ_111_52.setSelected(lexique.HostFieldGetData("EBZP15").trim().equalsIgnoreCase("1"));
    V14_OBJ_108_51.setVisible(lexique.HostFieldGetData("WZTT14").equalsIgnoreCase("B"));
    V14_OBJ_108_51.setSelected(lexique.HostFieldGetData("EBZP14").trim().equalsIgnoreCase("1"));
    V13_OBJ_105_50.setVisible(lexique.HostFieldGetData("WZTT13").equalsIgnoreCase("B"));
    V13_OBJ_105_50.setSelected(lexique.HostFieldGetData("EBZP13").trim().equalsIgnoreCase("1"));
    V12_OBJ_102_49.setVisible(lexique.HostFieldGetData("WZTT12").equalsIgnoreCase("B"));
    V12_OBJ_102_49.setSelected(lexique.HostFieldGetData("EBZP12").trim().equalsIgnoreCase("1"));
    V11_OBJ_99_48.setVisible(lexique.HostFieldGetData("WZTT11").equalsIgnoreCase("B"));
    V11_OBJ_99_48.setSelected(lexique.HostFieldGetData("EBZP11").trim().equalsIgnoreCase("1"));
    V10_OBJ_96_47.setVisible(lexique.HostFieldGetData("WZTT10").equalsIgnoreCase("B"));
    V10_OBJ_96_47.setSelected(lexique.HostFieldGetData("EBZP10").trim().equalsIgnoreCase("1"));
    V09_OBJ_93_46.setVisible(lexique.HostFieldGetData("WZTT09").equalsIgnoreCase("B"));
    V09_OBJ_93_46.setSelected(lexique.HostFieldGetData("EBZP09").trim().equalsIgnoreCase("1"));
    V08_OBJ_90_45.setVisible(lexique.HostFieldGetData("WZTT08").equalsIgnoreCase("B"));
    V08_OBJ_90_45.setSelected(lexique.HostFieldGetData("EBZP08").trim().equalsIgnoreCase("1"));
    V07_OBJ_87_44.setVisible(lexique.HostFieldGetData("WZTT07").equalsIgnoreCase("B"));
    V07_OBJ_87_44.setSelected(lexique.HostFieldGetData("EBZP07").trim().equalsIgnoreCase("1"));
    V06_OBJ_84_43.setVisible(lexique.HostFieldGetData("WZTT06").equalsIgnoreCase("B"));
    V06_OBJ_84_43.setSelected(lexique.HostFieldGetData("EBZP06").trim().equalsIgnoreCase("1"));
    V05_OBJ_81_42.setVisible(lexique.HostFieldGetData("WZTT05").equalsIgnoreCase("B"));
    V05_OBJ_81_42.setSelected(lexique.HostFieldGetData("EBZP05").trim().equalsIgnoreCase("1"));
    V04_OBJ_78_41.setVisible(lexique.HostFieldGetData("WZTT04").equalsIgnoreCase("B"));
    V04_OBJ_78_41.setSelected(lexique.HostFieldGetData("EBZP04").trim().equalsIgnoreCase("1"));
    V03_OBJ_75_40.setVisible(lexique.HostFieldGetData("WZTT03").equalsIgnoreCase("B"));
    V03_OBJ_75_40.setSelected(lexique.HostFieldGetData("EBZP03").trim().equalsIgnoreCase("1"));
    V02_OBJ_72_39.setVisible(lexique.HostFieldGetData("WZTT02").equalsIgnoreCase("B"));
    V02_OBJ_72_39.setSelected(lexique.HostFieldGetData("EBZP02").trim().equalsIgnoreCase("1"));
    V01_OBJ_69_38.setVisible(lexique.HostFieldGetData("WZTT01").equalsIgnoreCase("B"));
    V01_OBJ_69_38.setSelected(lexique.HostFieldGetData("EBZP01").trim().equalsIgnoreCase("1"));
    OBJ_123.setVisible(lexique.isPresent("CLNOM"));
    
    panel2.setVisible(!lexique.HostFieldGetData("WZTT01").trim().equalsIgnoreCase(""));
    panel3.setVisible(!lexique.HostFieldGetData("WZTT02").trim().equalsIgnoreCase(""));
    panel4.setVisible(!lexique.HostFieldGetData("WZTT03").trim().equalsIgnoreCase(""));
    panel6.setVisible(!lexique.HostFieldGetData("WZTT04").trim().equalsIgnoreCase(""));
    panel7.setVisible(!lexique.HostFieldGetData("WZTT05").trim().equalsIgnoreCase(""));
    panel8.setVisible(!lexique.HostFieldGetData("WZTT06").trim().equalsIgnoreCase(""));
    panel9.setVisible(!lexique.HostFieldGetData("WZTT07").trim().equalsIgnoreCase(""));
    panel10.setVisible(!lexique.HostFieldGetData("WZTT08").trim().equalsIgnoreCase(""));
    panel11.setVisible(!lexique.HostFieldGetData("WZTT09").trim().equalsIgnoreCase(""));
    panel12.setVisible(!lexique.HostFieldGetData("WZTT10").trim().equalsIgnoreCase(""));
    panel13.setVisible(!lexique.HostFieldGetData("WZTT11").trim().equalsIgnoreCase(""));
    panel14.setVisible(!lexique.HostFieldGetData("WZTT12").trim().equalsIgnoreCase(""));
    panel15.setVisible(!lexique.HostFieldGetData("WZTT13").trim().equalsIgnoreCase(""));
    panel16.setVisible(!lexique.HostFieldGetData("WZTT14").trim().equalsIgnoreCase(""));
    panel17.setVisible(!lexique.HostFieldGetData("WZTT15").trim().equalsIgnoreCase(""));
    panel18.setVisible(!lexique.HostFieldGetData("WZTT16").trim().equalsIgnoreCase(""));
    panel19.setVisible(!lexique.HostFieldGetData("WZTT17").trim().equalsIgnoreCase(""));
    panel20.setVisible(!lexique.HostFieldGetData("WZTT18").trim().equalsIgnoreCase(""));
    
    // Titre
    setTitle("Extension fiche client");
    
    

    
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(INDETB.getText()))
  }
  
  private int calculerLongueur(String nomChamps) {
    int width = Integer.parseInt(lexique.HostFieldGetData(nomChamps));
    if (width < 3) {
      width = ((width - 1) * 10) + 24;
    }
    else {
      width = ((width - 1) * 10) + 20;
    }
    return width;
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (V18_OBJ_120_55.isVisible()) {
      if (V18_OBJ_120_55.isSelected()) {
        lexique.HostFieldPutData("EBZP18", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP18", 0, " ");
      }
    }
    if (V17_OBJ_117_54.isVisible()) {
      if (V17_OBJ_117_54.isSelected()) {
        lexique.HostFieldPutData("EBZP17", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP17", 0, " ");
      }
    }
    if (V16_OBJ_114_53.isVisible()) {
      if (V16_OBJ_114_53.isSelected()) {
        lexique.HostFieldPutData("EBZP16", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP16", 0, " ");
      }
    }
    if (V15_OBJ_111_52.isVisible()) {
      if (V15_OBJ_111_52.isSelected()) {
        lexique.HostFieldPutData("EBZP15", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP15", 0, " ");
      }
    }
    if (V14_OBJ_108_51.isVisible()) {
      if (V14_OBJ_108_51.isSelected()) {
        lexique.HostFieldPutData("EBZP14", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP14", 0, " ");
      }
    }
    if (V13_OBJ_105_50.isVisible()) {
      if (V13_OBJ_105_50.isSelected()) {
        lexique.HostFieldPutData("EBZP13", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP13", 0, " ");
      }
    }
    if (V12_OBJ_102_49.isVisible()) {
      if (V12_OBJ_102_49.isSelected()) {
        lexique.HostFieldPutData("EBZP12", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP12", 0, " ");
      }
    }
    if (V11_OBJ_99_48.isVisible()) {
      if (V11_OBJ_99_48.isSelected()) {
        lexique.HostFieldPutData("EBZP11", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP11", 0, " ");
      }
    }
    if (V10_OBJ_96_47.isVisible()) {
      if (V10_OBJ_96_47.isSelected()) {
        lexique.HostFieldPutData("EBZP10", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP10", 0, " ");
      }
    }
    if (V09_OBJ_93_46.isVisible()) {
      if (V09_OBJ_93_46.isSelected()) {
        lexique.HostFieldPutData("EBZP09", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP09", 0, " ");
      }
    }
    if (V08_OBJ_90_45.isVisible()) {
      if (V08_OBJ_90_45.isSelected()) {
        lexique.HostFieldPutData("EBZP08", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP08", 0, " ");
      }
    }
    if (V07_OBJ_87_44.isVisible()) {
      if (V07_OBJ_87_44.isSelected()) {
        lexique.HostFieldPutData("EBZP07", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP07", 0, " ");
      }
    }
    if (V06_OBJ_84_43.isVisible()) {
      if (V06_OBJ_84_43.isSelected()) {
        lexique.HostFieldPutData("EBZP06", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP06", 0, " ");
      }
    }
    if (V05_OBJ_81_42.isVisible()) {
      if (V05_OBJ_81_42.isSelected()) {
        lexique.HostFieldPutData("EBZP05", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP05", 0, " ");
      }
    }
    if (V04_OBJ_78_41.isVisible()) {
      if (V04_OBJ_78_41.isSelected()) {
        lexique.HostFieldPutData("EBZP04", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP04", 0, " ");
      }
    }
    if (V03_OBJ_75_40.isVisible()) {
      if (V03_OBJ_75_40.isSelected()) {
        lexique.HostFieldPutData("EBZP03", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP03", 0, " ");
      }
    }
    if (V02_OBJ_72_39.isVisible()) {
      if (V02_OBJ_72_39.isSelected()) {
        lexique.HostFieldPutData("EBZP02", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP02", 0, " ");
      }
    }
    if (V01_OBJ_69_38.isVisible()) {
      if (V01_OBJ_69_38.isSelected()) {
        lexique.HostFieldPutData("EBZP01", 0, "1");
      }
      else {
        lexique.HostFieldPutData("EBZP01", 0, " ");
      }
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  // EVENEMENTIEL
  // +++++++++++++++++++++++++++++++++++++++++++
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel22 = new JPanel();
    panel2 = new JPanel();
    V01_OBJ_69_38 = new JCheckBox();
    OBJ_27 = new SNLabelChamp();
    OBJ_28 = new JLabel();
    EBZP1 = new XRiTextField();
    panel3 = new JPanel();
    V02_OBJ_72_39 = new JCheckBox();
    OBJ_29 = new SNLabelChamp();
    OBJ_30 = new JLabel();
    EBZP2 = new XRiTextField();
    panel4 = new JPanel();
    V03_OBJ_75_40 = new JCheckBox();
    OBJ_31 = new SNLabelChamp();
    OBJ_32 = new JLabel();
    EBZP3 = new XRiTextField();
    panel7 = new JPanel();
    OBJ_35 = new SNLabelChamp();
    V05_OBJ_81_42 = new JCheckBox();
    EBZP5 = new XRiTextField();
    OBJ_36 = new JLabel();
    panel8 = new JPanel();
    OBJ_37 = new SNLabelChamp();
    V06_OBJ_84_43 = new JCheckBox();
    EBZP6 = new XRiTextField();
    OBJ_38 = new JLabel();
    panel9 = new JPanel();
    OBJ_39 = new SNLabelChamp();
    V07_OBJ_87_44 = new JCheckBox();
    EBZP7 = new XRiTextField();
    OBJ_40 = new JLabel();
    panel10 = new JPanel();
    OBJ_41 = new SNLabelChamp();
    V08_OBJ_90_45 = new JCheckBox();
    EBZP8 = new XRiTextField();
    OBJ_42 = new JLabel();
    panel11 = new JPanel();
    OBJ_43 = new SNLabelChamp();
    V09_OBJ_93_46 = new JCheckBox();
    EBZP9 = new XRiTextField();
    OBJ_44 = new JLabel();
    panel12 = new JPanel();
    OBJ_45 = new SNLabelChamp();
    V10_OBJ_96_47 = new JCheckBox();
    EBZP10 = new XRiTextField();
    OBJ_46 = new JLabel();
    panel13 = new JPanel();
    OBJ_47 = new SNLabelChamp();
    V11_OBJ_99_48 = new JCheckBox();
    EBZP11 = new XRiTextField();
    OBJ_48 = new JLabel();
    panel14 = new JPanel();
    OBJ_49 = new SNLabelChamp();
    V12_OBJ_102_49 = new JCheckBox();
    EBZP12 = new XRiTextField();
    OBJ_50 = new JLabel();
    panel15 = new JPanel();
    OBJ_51 = new SNLabelChamp();
    V13_OBJ_105_50 = new JCheckBox();
    EBZP13 = new XRiTextField();
    OBJ_52 = new JLabel();
    panel16 = new JPanel();
    OBJ_53 = new SNLabelChamp();
    V14_OBJ_108_51 = new JCheckBox();
    EBZP14 = new XRiTextField();
    OBJ_54 = new JLabel();
    panel17 = new JPanel();
    OBJ_55 = new SNLabelChamp();
    V15_OBJ_111_52 = new JCheckBox();
    EBZP15 = new XRiTextField();
    OBJ_56 = new JLabel();
    panel18 = new JPanel();
    OBJ_57 = new SNLabelChamp();
    V16_OBJ_114_53 = new JCheckBox();
    EBZP16 = new XRiTextField();
    OBJ_58 = new JLabel();
    panel19 = new JPanel();
    OBJ_59 = new SNLabelChamp();
    V17_OBJ_117_54 = new JCheckBox();
    EBZP17 = new XRiTextField();
    OBJ_60 = new JLabel();
    panel20 = new JPanel();
    OBJ_61 = new SNLabelChamp();
    V18_OBJ_120_55 = new JCheckBox();
    EBZP18 = new XRiTextField();
    OBJ_62 = new JLabel();
    panel6 = new JPanel();
    V04_OBJ_78_41 = new JCheckBox();
    OBJ_33 = new SNLabelChamp();
    EBZP4 = new XRiTextField();
    OBJ_34 = new JLabel();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    OBJ_64 = new JLabel();
    CLCLI = new XRiTextField();
    CLLIV = new XRiTextField();
    OBJ_123 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    panel21 = new JPanel();

    //======== this ========
    setMinimumSize(new Dimension(1000, 595));
    setPreferredSize(new Dimension(1000, 595));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel22 ========
        {
          panel22.setOpaque(false);
          panel22.setName("panel22");
          panel22.setLayout(null);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- V01_OBJ_69_38 ----
            V01_OBJ_69_38.setText("@T01@");
            V01_OBJ_69_38.setComponentPopupMenu(BTD);
            V01_OBJ_69_38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V01_OBJ_69_38.setMinimumSize(new Dimension(70, 28));
            V01_OBJ_69_38.setPreferredSize(new Dimension(70, 28));
            V01_OBJ_69_38.setName("V01_OBJ_69_38");
            panel2.add(V01_OBJ_69_38, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_27 ----
            OBJ_27.setText("@WZPL01@");
            OBJ_27.setMinimumSize(new Dimension(240, 30));
            OBJ_27.setPreferredSize(new Dimension(240, 30));
            OBJ_27.setName("OBJ_27");
            panel2.add(OBJ_27, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_28 ----
            OBJ_28.setText("@ERZP1@");
            OBJ_28.setForeground(new Color(204, 0, 51));
            OBJ_28.setPreferredSize(new Dimension(65, 30));
            OBJ_28.setMinimumSize(new Dimension(65, 30));
            OBJ_28.setName("OBJ_28");
            panel2.add(OBJ_28, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- EBZP1 ----
            EBZP1.setComponentPopupMenu(BTD);
            EBZP1.setMinimumSize(new Dimension(50, 30));
            EBZP1.setPreferredSize(new Dimension(50, 30));
            EBZP1.setName("EBZP1");
            panel2.add(EBZP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          panel22.add(panel2);
          panel2.setBounds(0, 5, 813, 30);

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(new GridBagLayout());
            ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- V02_OBJ_72_39 ----
            V02_OBJ_72_39.setText("@T02@");
            V02_OBJ_72_39.setComponentPopupMenu(BTD);
            V02_OBJ_72_39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V02_OBJ_72_39.setMinimumSize(new Dimension(70, 28));
            V02_OBJ_72_39.setPreferredSize(new Dimension(70, 28));
            V02_OBJ_72_39.setName("V02_OBJ_72_39");
            panel3.add(V02_OBJ_72_39, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_29 ----
            OBJ_29.setText("@WZPL02@");
            OBJ_29.setMinimumSize(new Dimension(240, 30));
            OBJ_29.setPreferredSize(new Dimension(240, 30));
            OBJ_29.setName("OBJ_29");
            panel3.add(OBJ_29, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_30 ----
            OBJ_30.setText("@ERZP2@");
            OBJ_30.setForeground(new Color(204, 0, 51));
            OBJ_30.setPreferredSize(new Dimension(65, 30));
            OBJ_30.setMinimumSize(new Dimension(65, 30));
            OBJ_30.setName("OBJ_30");
            panel3.add(OBJ_30, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- EBZP2 ----
            EBZP2.setComponentPopupMenu(BTD);
            EBZP2.setMinimumSize(new Dimension(50, 30));
            EBZP2.setPreferredSize(new Dimension(50, 30));
            EBZP2.setName("EBZP2");
            panel3.add(EBZP2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          panel22.add(panel3);
          panel3.setBounds(0, 42, 813, 30);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout)panel4.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel4.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel4.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel4.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- V03_OBJ_75_40 ----
            V03_OBJ_75_40.setText("@T03@");
            V03_OBJ_75_40.setComponentPopupMenu(BTD);
            V03_OBJ_75_40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V03_OBJ_75_40.setMinimumSize(new Dimension(70, 28));
            V03_OBJ_75_40.setPreferredSize(new Dimension(70, 28));
            V03_OBJ_75_40.setName("V03_OBJ_75_40");
            panel4.add(V03_OBJ_75_40, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_31 ----
            OBJ_31.setText("@WZPL03@");
            OBJ_31.setMinimumSize(new Dimension(240, 30));
            OBJ_31.setPreferredSize(new Dimension(240, 30));
            OBJ_31.setName("OBJ_31");
            panel4.add(OBJ_31, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_32 ----
            OBJ_32.setText("@ERZP3@");
            OBJ_32.setForeground(new Color(204, 0, 51));
            OBJ_32.setPreferredSize(new Dimension(65, 30));
            OBJ_32.setMinimumSize(new Dimension(65, 30));
            OBJ_32.setName("OBJ_32");
            panel4.add(OBJ_32, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- EBZP3 ----
            EBZP3.setComponentPopupMenu(BTD);
            EBZP3.setMinimumSize(new Dimension(50, 30));
            EBZP3.setPreferredSize(new Dimension(50, 30));
            EBZP3.setName("EBZP3");
            panel4.add(EBZP3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          }
          panel22.add(panel4);
          panel4.setBounds(0, 72, 813, 30);

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(new GridBagLayout());
            ((GridBagLayout)panel7.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel7.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel7.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel7.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_35 ----
            OBJ_35.setText("@WZPL05@");
            OBJ_35.setMinimumSize(new Dimension(240, 30));
            OBJ_35.setPreferredSize(new Dimension(240, 30));
            OBJ_35.setName("OBJ_35");
            panel7.add(OBJ_35, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V05_OBJ_81_42 ----
            V05_OBJ_81_42.setText("@T05@");
            V05_OBJ_81_42.setComponentPopupMenu(BTD);
            V05_OBJ_81_42.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V05_OBJ_81_42.setMinimumSize(new Dimension(70, 28));
            V05_OBJ_81_42.setPreferredSize(new Dimension(70, 28));
            V05_OBJ_81_42.setName("V05_OBJ_81_42");
            panel7.add(V05_OBJ_81_42, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP5 ----
            EBZP5.setComponentPopupMenu(BTD);
            EBZP5.setMinimumSize(new Dimension(50, 30));
            EBZP5.setPreferredSize(new Dimension(50, 30));
            EBZP5.setName("EBZP5");
            panel7.add(EBZP5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_36 ----
            OBJ_36.setText("@ERZP5@");
            OBJ_36.setForeground(new Color(204, 0, 51));
            OBJ_36.setPreferredSize(new Dimension(65, 30));
            OBJ_36.setMinimumSize(new Dimension(65, 30));
            OBJ_36.setName("OBJ_36");
            panel7.add(OBJ_36, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel7);
          panel7.setBounds(0, 132, 813, 30);

          //======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(new GridBagLayout());
            ((GridBagLayout)panel8.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel8.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel8.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel8.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_37 ----
            OBJ_37.setText("@WZPL06@");
            OBJ_37.setMinimumSize(new Dimension(240, 30));
            OBJ_37.setPreferredSize(new Dimension(240, 30));
            OBJ_37.setName("OBJ_37");
            panel8.add(OBJ_37, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V06_OBJ_84_43 ----
            V06_OBJ_84_43.setText("@T06@");
            V06_OBJ_84_43.setComponentPopupMenu(BTD);
            V06_OBJ_84_43.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V06_OBJ_84_43.setMinimumSize(new Dimension(70, 28));
            V06_OBJ_84_43.setPreferredSize(new Dimension(70, 28));
            V06_OBJ_84_43.setName("V06_OBJ_84_43");
            panel8.add(V06_OBJ_84_43, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP6 ----
            EBZP6.setComponentPopupMenu(BTD);
            EBZP6.setMinimumSize(new Dimension(50, 30));
            EBZP6.setPreferredSize(new Dimension(50, 30));
            EBZP6.setName("EBZP6");
            panel8.add(EBZP6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_38 ----
            OBJ_38.setText("@ERZP6@");
            OBJ_38.setForeground(new Color(204, 0, 51));
            OBJ_38.setPreferredSize(new Dimension(65, 30));
            OBJ_38.setMinimumSize(new Dimension(65, 30));
            OBJ_38.setName("OBJ_38");
            panel8.add(OBJ_38, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel8);
          panel8.setBounds(0, 162, 813, 30);

          //======== panel9 ========
          {
            panel9.setOpaque(false);
            panel9.setName("panel9");
            panel9.setLayout(new GridBagLayout());
            ((GridBagLayout)panel9.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel9.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel9.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel9.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_39 ----
            OBJ_39.setText("@WZPL07@");
            OBJ_39.setMinimumSize(new Dimension(240, 30));
            OBJ_39.setPreferredSize(new Dimension(240, 30));
            OBJ_39.setName("OBJ_39");
            panel9.add(OBJ_39, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V07_OBJ_87_44 ----
            V07_OBJ_87_44.setText("@T07@");
            V07_OBJ_87_44.setComponentPopupMenu(BTD);
            V07_OBJ_87_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V07_OBJ_87_44.setMinimumSize(new Dimension(70, 28));
            V07_OBJ_87_44.setPreferredSize(new Dimension(70, 28));
            V07_OBJ_87_44.setName("V07_OBJ_87_44");
            panel9.add(V07_OBJ_87_44, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP7 ----
            EBZP7.setComponentPopupMenu(BTD);
            EBZP7.setMinimumSize(new Dimension(50, 30));
            EBZP7.setPreferredSize(new Dimension(50, 30));
            EBZP7.setName("EBZP7");
            panel9.add(EBZP7, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_40 ----
            OBJ_40.setText("@ERZP7@");
            OBJ_40.setForeground(new Color(204, 0, 51));
            OBJ_40.setPreferredSize(new Dimension(65, 30));
            OBJ_40.setMinimumSize(new Dimension(65, 30));
            OBJ_40.setName("OBJ_40");
            panel9.add(OBJ_40, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel9);
          panel9.setBounds(0, 192, 813, 30);

          //======== panel10 ========
          {
            panel10.setOpaque(false);
            panel10.setName("panel10");
            panel10.setLayout(new GridBagLayout());
            ((GridBagLayout)panel10.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel10.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel10.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel10.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_41 ----
            OBJ_41.setText("@WZPL08@");
            OBJ_41.setMinimumSize(new Dimension(240, 30));
            OBJ_41.setPreferredSize(new Dimension(240, 30));
            OBJ_41.setName("OBJ_41");
            panel10.add(OBJ_41, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V08_OBJ_90_45 ----
            V08_OBJ_90_45.setText("@T08@");
            V08_OBJ_90_45.setComponentPopupMenu(BTD);
            V08_OBJ_90_45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V08_OBJ_90_45.setMinimumSize(new Dimension(70, 28));
            V08_OBJ_90_45.setPreferredSize(new Dimension(70, 28));
            V08_OBJ_90_45.setName("V08_OBJ_90_45");
            panel10.add(V08_OBJ_90_45, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP8 ----
            EBZP8.setComponentPopupMenu(BTD);
            EBZP8.setMinimumSize(new Dimension(50, 30));
            EBZP8.setPreferredSize(new Dimension(50, 30));
            EBZP8.setName("EBZP8");
            panel10.add(EBZP8, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_42 ----
            OBJ_42.setText("@ERZP8@");
            OBJ_42.setForeground(new Color(204, 0, 51));
            OBJ_42.setPreferredSize(new Dimension(65, 30));
            OBJ_42.setMinimumSize(new Dimension(65, 30));
            OBJ_42.setName("OBJ_42");
            panel10.add(OBJ_42, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel10);
          panel10.setBounds(0, 222, 813, 30);

          //======== panel11 ========
          {
            panel11.setOpaque(false);
            panel11.setName("panel11");
            panel11.setLayout(new GridBagLayout());
            ((GridBagLayout)panel11.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel11.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel11.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel11.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_43 ----
            OBJ_43.setText("@WZPL09@");
            OBJ_43.setMinimumSize(new Dimension(240, 30));
            OBJ_43.setPreferredSize(new Dimension(240, 30));
            OBJ_43.setName("OBJ_43");
            panel11.add(OBJ_43, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V09_OBJ_93_46 ----
            V09_OBJ_93_46.setText("@T09@");
            V09_OBJ_93_46.setComponentPopupMenu(BTD);
            V09_OBJ_93_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V09_OBJ_93_46.setMinimumSize(new Dimension(70, 28));
            V09_OBJ_93_46.setPreferredSize(new Dimension(70, 28));
            V09_OBJ_93_46.setName("V09_OBJ_93_46");
            panel11.add(V09_OBJ_93_46, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP9 ----
            EBZP9.setComponentPopupMenu(BTD);
            EBZP9.setMinimumSize(new Dimension(50, 30));
            EBZP9.setPreferredSize(new Dimension(50, 30));
            EBZP9.setName("EBZP9");
            panel11.add(EBZP9, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_44 ----
            OBJ_44.setText("@ERZP9@");
            OBJ_44.setForeground(new Color(204, 0, 51));
            OBJ_44.setPreferredSize(new Dimension(65, 30));
            OBJ_44.setMinimumSize(new Dimension(65, 30));
            OBJ_44.setName("OBJ_44");
            panel11.add(OBJ_44, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel11);
          panel11.setBounds(0, 252, 813, 30);

          //======== panel12 ========
          {
            panel12.setOpaque(false);
            panel12.setName("panel12");
            panel12.setLayout(new GridBagLayout());
            ((GridBagLayout)panel12.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel12.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel12.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel12.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_45 ----
            OBJ_45.setText("@WZPL10@");
            OBJ_45.setMinimumSize(new Dimension(240, 30));
            OBJ_45.setPreferredSize(new Dimension(240, 30));
            OBJ_45.setName("OBJ_45");
            panel12.add(OBJ_45, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V10_OBJ_96_47 ----
            V10_OBJ_96_47.setText("@T10@");
            V10_OBJ_96_47.setComponentPopupMenu(BTD);
            V10_OBJ_96_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V10_OBJ_96_47.setMinimumSize(new Dimension(70, 28));
            V10_OBJ_96_47.setPreferredSize(new Dimension(70, 28));
            V10_OBJ_96_47.setName("V10_OBJ_96_47");
            panel12.add(V10_OBJ_96_47, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP10 ----
            EBZP10.setComponentPopupMenu(BTD);
            EBZP10.setMinimumSize(new Dimension(50, 30));
            EBZP10.setPreferredSize(new Dimension(50, 30));
            EBZP10.setName("EBZP10");
            panel12.add(EBZP10, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_46 ----
            OBJ_46.setText("@ERZP10@");
            OBJ_46.setForeground(new Color(204, 0, 51));
            OBJ_46.setPreferredSize(new Dimension(65, 30));
            OBJ_46.setMinimumSize(new Dimension(65, 30));
            OBJ_46.setName("OBJ_46");
            panel12.add(OBJ_46, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel12);
          panel12.setBounds(0, 282, 813, 30);

          //======== panel13 ========
          {
            panel13.setOpaque(false);
            panel13.setName("panel13");
            panel13.setLayout(new GridBagLayout());
            ((GridBagLayout)panel13.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel13.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel13.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel13.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_47 ----
            OBJ_47.setText("@WZPL11@");
            OBJ_47.setMinimumSize(new Dimension(240, 30));
            OBJ_47.setPreferredSize(new Dimension(240, 30));
            OBJ_47.setName("OBJ_47");
            panel13.add(OBJ_47, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V11_OBJ_99_48 ----
            V11_OBJ_99_48.setText("@T11@");
            V11_OBJ_99_48.setComponentPopupMenu(BTD);
            V11_OBJ_99_48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V11_OBJ_99_48.setMinimumSize(new Dimension(70, 28));
            V11_OBJ_99_48.setPreferredSize(new Dimension(70, 28));
            V11_OBJ_99_48.setName("V11_OBJ_99_48");
            panel13.add(V11_OBJ_99_48, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP11 ----
            EBZP11.setComponentPopupMenu(BTD);
            EBZP11.setMinimumSize(new Dimension(50, 30));
            EBZP11.setPreferredSize(new Dimension(50, 30));
            EBZP11.setName("EBZP11");
            panel13.add(EBZP11, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_48 ----
            OBJ_48.setText("@ERZP11@");
            OBJ_48.setForeground(new Color(204, 0, 51));
            OBJ_48.setPreferredSize(new Dimension(65, 30));
            OBJ_48.setMinimumSize(new Dimension(65, 30));
            OBJ_48.setName("OBJ_48");
            panel13.add(OBJ_48, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel13);
          panel13.setBounds(0, 312, 813, 30);

          //======== panel14 ========
          {
            panel14.setOpaque(false);
            panel14.setName("panel14");
            panel14.setLayout(new GridBagLayout());
            ((GridBagLayout)panel14.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel14.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel14.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel14.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_49 ----
            OBJ_49.setText("@WZPL12@");
            OBJ_49.setMinimumSize(new Dimension(240, 30));
            OBJ_49.setPreferredSize(new Dimension(240, 30));
            OBJ_49.setName("OBJ_49");
            panel14.add(OBJ_49, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V12_OBJ_102_49 ----
            V12_OBJ_102_49.setText("@T12@");
            V12_OBJ_102_49.setComponentPopupMenu(BTD);
            V12_OBJ_102_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V12_OBJ_102_49.setMinimumSize(new Dimension(70, 28));
            V12_OBJ_102_49.setPreferredSize(new Dimension(70, 28));
            V12_OBJ_102_49.setName("V12_OBJ_102_49");
            panel14.add(V12_OBJ_102_49, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP12 ----
            EBZP12.setComponentPopupMenu(BTD);
            EBZP12.setMinimumSize(new Dimension(50, 30));
            EBZP12.setPreferredSize(new Dimension(50, 30));
            EBZP12.setName("EBZP12");
            panel14.add(EBZP12, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_50 ----
            OBJ_50.setText("@ERZP12@");
            OBJ_50.setForeground(new Color(204, 0, 51));
            OBJ_50.setPreferredSize(new Dimension(65, 30));
            OBJ_50.setMinimumSize(new Dimension(65, 30));
            OBJ_50.setName("OBJ_50");
            panel14.add(OBJ_50, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel14);
          panel14.setBounds(0, 342, 813, 30);

          //======== panel15 ========
          {
            panel15.setOpaque(false);
            panel15.setName("panel15");
            panel15.setLayout(new GridBagLayout());
            ((GridBagLayout)panel15.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel15.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel15.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel15.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_51 ----
            OBJ_51.setText("@WZPL13@");
            OBJ_51.setMinimumSize(new Dimension(240, 30));
            OBJ_51.setPreferredSize(new Dimension(240, 30));
            OBJ_51.setName("OBJ_51");
            panel15.add(OBJ_51, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V13_OBJ_105_50 ----
            V13_OBJ_105_50.setText("@T13@");
            V13_OBJ_105_50.setComponentPopupMenu(BTD);
            V13_OBJ_105_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V13_OBJ_105_50.setMinimumSize(new Dimension(70, 28));
            V13_OBJ_105_50.setPreferredSize(new Dimension(70, 28));
            V13_OBJ_105_50.setName("V13_OBJ_105_50");
            panel15.add(V13_OBJ_105_50, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP13 ----
            EBZP13.setComponentPopupMenu(BTD);
            EBZP13.setMinimumSize(new Dimension(50, 30));
            EBZP13.setPreferredSize(new Dimension(50, 30));
            EBZP13.setName("EBZP13");
            panel15.add(EBZP13, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_52 ----
            OBJ_52.setText("@ERZP13@");
            OBJ_52.setForeground(new Color(204, 0, 51));
            OBJ_52.setPreferredSize(new Dimension(65, 30));
            OBJ_52.setMinimumSize(new Dimension(65, 30));
            OBJ_52.setName("OBJ_52");
            panel15.add(OBJ_52, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel15);
          panel15.setBounds(0, 372, 813, 30);

          //======== panel16 ========
          {
            panel16.setOpaque(false);
            panel16.setName("panel16");
            panel16.setLayout(new GridBagLayout());
            ((GridBagLayout)panel16.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel16.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel16.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel16.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_53 ----
            OBJ_53.setText("@WZPL14@");
            OBJ_53.setMinimumSize(new Dimension(240, 30));
            OBJ_53.setPreferredSize(new Dimension(240, 30));
            OBJ_53.setName("OBJ_53");
            panel16.add(OBJ_53, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V14_OBJ_108_51 ----
            V14_OBJ_108_51.setText("@T14@");
            V14_OBJ_108_51.setComponentPopupMenu(BTD);
            V14_OBJ_108_51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V14_OBJ_108_51.setMinimumSize(new Dimension(70, 28));
            V14_OBJ_108_51.setPreferredSize(new Dimension(70, 28));
            V14_OBJ_108_51.setName("V14_OBJ_108_51");
            panel16.add(V14_OBJ_108_51, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP14 ----
            EBZP14.setComponentPopupMenu(BTD);
            EBZP14.setMinimumSize(new Dimension(50, 30));
            EBZP14.setPreferredSize(new Dimension(50, 30));
            EBZP14.setName("EBZP14");
            panel16.add(EBZP14, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_54 ----
            OBJ_54.setText("@ERZP14@");
            OBJ_54.setForeground(new Color(204, 0, 51));
            OBJ_54.setPreferredSize(new Dimension(65, 30));
            OBJ_54.setMinimumSize(new Dimension(65, 30));
            OBJ_54.setName("OBJ_54");
            panel16.add(OBJ_54, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel16);
          panel16.setBounds(0, 402, 813, 30);

          //======== panel17 ========
          {
            panel17.setOpaque(false);
            panel17.setName("panel17");
            panel17.setLayout(new GridBagLayout());
            ((GridBagLayout)panel17.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel17.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel17.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel17.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_55 ----
            OBJ_55.setText("@WZPL15@");
            OBJ_55.setMinimumSize(new Dimension(240, 30));
            OBJ_55.setPreferredSize(new Dimension(240, 30));
            OBJ_55.setName("OBJ_55");
            panel17.add(OBJ_55, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V15_OBJ_111_52 ----
            V15_OBJ_111_52.setText("@T15@");
            V15_OBJ_111_52.setComponentPopupMenu(BTD);
            V15_OBJ_111_52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V15_OBJ_111_52.setMinimumSize(new Dimension(70, 28));
            V15_OBJ_111_52.setPreferredSize(new Dimension(70, 28));
            V15_OBJ_111_52.setName("V15_OBJ_111_52");
            panel17.add(V15_OBJ_111_52, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP15 ----
            EBZP15.setComponentPopupMenu(BTD);
            EBZP15.setMinimumSize(new Dimension(50, 30));
            EBZP15.setPreferredSize(new Dimension(50, 30));
            EBZP15.setName("EBZP15");
            panel17.add(EBZP15, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_56 ----
            OBJ_56.setText("@ERZP15@");
            OBJ_56.setForeground(new Color(204, 0, 51));
            OBJ_56.setPreferredSize(new Dimension(65, 30));
            OBJ_56.setMinimumSize(new Dimension(65, 30));
            OBJ_56.setName("OBJ_56");
            panel17.add(OBJ_56, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel17);
          panel17.setBounds(0, 432, 813, 30);

          //======== panel18 ========
          {
            panel18.setOpaque(false);
            panel18.setName("panel18");
            panel18.setLayout(new GridBagLayout());
            ((GridBagLayout)panel18.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel18.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel18.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel18.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_57 ----
            OBJ_57.setText("@WZPL16@");
            OBJ_57.setMinimumSize(new Dimension(240, 30));
            OBJ_57.setPreferredSize(new Dimension(240, 30));
            OBJ_57.setName("OBJ_57");
            panel18.add(OBJ_57, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V16_OBJ_114_53 ----
            V16_OBJ_114_53.setText("@T16@");
            V16_OBJ_114_53.setComponentPopupMenu(BTD);
            V16_OBJ_114_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V16_OBJ_114_53.setMinimumSize(new Dimension(70, 28));
            V16_OBJ_114_53.setPreferredSize(new Dimension(70, 28));
            V16_OBJ_114_53.setName("V16_OBJ_114_53");
            panel18.add(V16_OBJ_114_53, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP16 ----
            EBZP16.setComponentPopupMenu(BTD);
            EBZP16.setMinimumSize(new Dimension(50, 30));
            EBZP16.setPreferredSize(new Dimension(50, 30));
            EBZP16.setName("EBZP16");
            panel18.add(EBZP16, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_58 ----
            OBJ_58.setText("@ERZP16@");
            OBJ_58.setForeground(new Color(204, 0, 51));
            OBJ_58.setPreferredSize(new Dimension(65, 30));
            OBJ_58.setMinimumSize(new Dimension(65, 30));
            OBJ_58.setName("OBJ_58");
            panel18.add(OBJ_58, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel18);
          panel18.setBounds(0, 462, 813, 30);

          //======== panel19 ========
          {
            panel19.setOpaque(false);
            panel19.setName("panel19");
            panel19.setLayout(new GridBagLayout());
            ((GridBagLayout)panel19.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel19.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel19.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel19.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_59 ----
            OBJ_59.setText("@WZPL17@");
            OBJ_59.setMinimumSize(new Dimension(240, 30));
            OBJ_59.setPreferredSize(new Dimension(240, 30));
            OBJ_59.setName("OBJ_59");
            panel19.add(OBJ_59, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V17_OBJ_117_54 ----
            V17_OBJ_117_54.setText("@T17@");
            V17_OBJ_117_54.setComponentPopupMenu(BTD);
            V17_OBJ_117_54.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V17_OBJ_117_54.setMinimumSize(new Dimension(70, 28));
            V17_OBJ_117_54.setPreferredSize(new Dimension(70, 28));
            V17_OBJ_117_54.setName("V17_OBJ_117_54");
            panel19.add(V17_OBJ_117_54, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP17 ----
            EBZP17.setComponentPopupMenu(BTD);
            EBZP17.setMinimumSize(new Dimension(50, 30));
            EBZP17.setPreferredSize(new Dimension(50, 30));
            EBZP17.setName("EBZP17");
            panel19.add(EBZP17, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_60 ----
            OBJ_60.setText("@ERZP17@");
            OBJ_60.setForeground(new Color(204, 0, 51));
            OBJ_60.setPreferredSize(new Dimension(65, 30));
            OBJ_60.setMinimumSize(new Dimension(65, 30));
            OBJ_60.setName("OBJ_60");
            panel19.add(OBJ_60, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel19);
          panel19.setBounds(0, 492, 813, 30);

          //======== panel20 ========
          {
            panel20.setOpaque(false);
            panel20.setName("panel20");
            panel20.setLayout(new GridBagLayout());
            ((GridBagLayout)panel20.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel20.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel20.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel20.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- OBJ_61 ----
            OBJ_61.setText("@WZPL18@");
            OBJ_61.setMinimumSize(new Dimension(240, 30));
            OBJ_61.setPreferredSize(new Dimension(240, 30));
            OBJ_61.setName("OBJ_61");
            panel20.add(OBJ_61, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- V18_OBJ_120_55 ----
            V18_OBJ_120_55.setText("@T18@");
            V18_OBJ_120_55.setComponentPopupMenu(BTD);
            V18_OBJ_120_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V18_OBJ_120_55.setMinimumSize(new Dimension(70, 28));
            V18_OBJ_120_55.setPreferredSize(new Dimension(70, 28));
            V18_OBJ_120_55.setName("V18_OBJ_120_55");
            panel20.add(V18_OBJ_120_55, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP18 ----
            EBZP18.setComponentPopupMenu(BTD);
            EBZP18.setMinimumSize(new Dimension(50, 30));
            EBZP18.setPreferredSize(new Dimension(50, 30));
            EBZP18.setName("EBZP18");
            panel20.add(EBZP18, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_62 ----
            OBJ_62.setText("@ERZP18@");
            OBJ_62.setForeground(new Color(204, 0, 51));
            OBJ_62.setPreferredSize(new Dimension(65, 30));
            OBJ_62.setMinimumSize(new Dimension(65, 30));
            OBJ_62.setName("OBJ_62");
            panel20.add(OBJ_62, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel20);
          panel20.setBounds(0, 522, 813, 30);

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(new GridBagLayout());
            ((GridBagLayout)panel6.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel6.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel6.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel6.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- V04_OBJ_78_41 ----
            V04_OBJ_78_41.setText("@T04@");
            V04_OBJ_78_41.setComponentPopupMenu(BTD);
            V04_OBJ_78_41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            V04_OBJ_78_41.setMinimumSize(new Dimension(70, 28));
            V04_OBJ_78_41.setPreferredSize(new Dimension(70, 28));
            V04_OBJ_78_41.setName("V04_OBJ_78_41");
            panel6.add(V04_OBJ_78_41, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_33 ----
            OBJ_33.setText("@WZPL04@");
            OBJ_33.setMinimumSize(new Dimension(240, 30));
            OBJ_33.setPreferredSize(new Dimension(240, 30));
            OBJ_33.setName("OBJ_33");
            panel6.add(OBJ_33, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- EBZP4 ----
            EBZP4.setComponentPopupMenu(BTD);
            EBZP4.setName("EBZP4");
            panel6.add(EBZP4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_34 ----
            OBJ_34.setText("@ERZP4@");
            OBJ_34.setForeground(new Color(204, 0, 51));
            OBJ_34.setPreferredSize(new Dimension(65, 30));
            OBJ_34.setMinimumSize(new Dimension(65, 30));
            OBJ_34.setName("OBJ_34");
            panel6.add(OBJ_34, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          panel22.add(panel6);
          panel6.setBounds(0, 102, 813, 30);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel22.getComponentCount(); i++) {
              Rectangle bounds = panel22.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel22.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel22.setMinimumSize(preferredSize);
            panel22.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel22);
        panel22.setBounds(5, 0, 825, 565);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");

        //---- OBJ_64 ----
        OBJ_64.setText("Client");
        OBJ_64.setPreferredSize(new Dimension(40, 16));
        OBJ_64.setMinimumSize(new Dimension(40, 16));
        OBJ_64.setName("OBJ_64");

        //---- CLCLI ----
        CLCLI.setName("CLCLI");

        //---- CLLIV ----
        CLLIV.setName("CLLIV");

        //---- OBJ_123 ----
        OBJ_123.setText("@CLNOM@");
        OBJ_123.setFont(OBJ_123.getFont().deriveFont(OBJ_123.getFont().getStyle() | Font.BOLD));
        OBJ_123.setOpaque(false);
        OBJ_123.setName("OBJ_123");

        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
              .addGap(4, 4, 4)
              .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(8, 8, 8)
              .addComponent(OBJ_123, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
        );
        panel1Layout.setVerticalGroup(
          panel1Layout.createParallelGroup()
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(5, 5, 5)
              .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addComponent(CLCLI, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
            .addComponent(CLLIV, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
            .addGroup(panel1Layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(OBJ_123, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
        );
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== panel21 ========
    {
      panel21.setName("panel21");
      panel21.setLayout(null);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel21.getComponentCount(); i++) {
          Rectangle bounds = panel21.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel21.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel21.setMinimumSize(preferredSize);
        panel21.setPreferredSize(preferredSize);
      }
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel22;
  private JPanel panel2;
  private JCheckBox V01_OBJ_69_38;
  private SNLabelChamp OBJ_27;
  private JLabel OBJ_28;
  private XRiTextField EBZP1;
  private JPanel panel3;
  private JCheckBox V02_OBJ_72_39;
  private SNLabelChamp OBJ_29;
  private JLabel OBJ_30;
  private XRiTextField EBZP2;
  private JPanel panel4;
  private JCheckBox V03_OBJ_75_40;
  private SNLabelChamp OBJ_31;
  private JLabel OBJ_32;
  private XRiTextField EBZP3;
  private JPanel panel7;
  private SNLabelChamp OBJ_35;
  private JCheckBox V05_OBJ_81_42;
  private XRiTextField EBZP5;
  private JLabel OBJ_36;
  private JPanel panel8;
  private SNLabelChamp OBJ_37;
  private JCheckBox V06_OBJ_84_43;
  private XRiTextField EBZP6;
  private JLabel OBJ_38;
  private JPanel panel9;
  private SNLabelChamp OBJ_39;
  private JCheckBox V07_OBJ_87_44;
  private XRiTextField EBZP7;
  private JLabel OBJ_40;
  private JPanel panel10;
  private SNLabelChamp OBJ_41;
  private JCheckBox V08_OBJ_90_45;
  private XRiTextField EBZP8;
  private JLabel OBJ_42;
  private JPanel panel11;
  private SNLabelChamp OBJ_43;
  private JCheckBox V09_OBJ_93_46;
  private XRiTextField EBZP9;
  private JLabel OBJ_44;
  private JPanel panel12;
  private SNLabelChamp OBJ_45;
  private JCheckBox V10_OBJ_96_47;
  private XRiTextField EBZP10;
  private JLabel OBJ_46;
  private JPanel panel13;
  private SNLabelChamp OBJ_47;
  private JCheckBox V11_OBJ_99_48;
  private XRiTextField EBZP11;
  private JLabel OBJ_48;
  private JPanel panel14;
  private SNLabelChamp OBJ_49;
  private JCheckBox V12_OBJ_102_49;
  private XRiTextField EBZP12;
  private JLabel OBJ_50;
  private JPanel panel15;
  private SNLabelChamp OBJ_51;
  private JCheckBox V13_OBJ_105_50;
  private XRiTextField EBZP13;
  private JLabel OBJ_52;
  private JPanel panel16;
  private SNLabelChamp OBJ_53;
  private JCheckBox V14_OBJ_108_51;
  private XRiTextField EBZP14;
  private JLabel OBJ_54;
  private JPanel panel17;
  private SNLabelChamp OBJ_55;
  private JCheckBox V15_OBJ_111_52;
  private XRiTextField EBZP15;
  private JLabel OBJ_56;
  private JPanel panel18;
  private SNLabelChamp OBJ_57;
  private JCheckBox V16_OBJ_114_53;
  private XRiTextField EBZP16;
  private JLabel OBJ_58;
  private JPanel panel19;
  private SNLabelChamp OBJ_59;
  private JCheckBox V17_OBJ_117_54;
  private XRiTextField EBZP17;
  private JLabel OBJ_60;
  private JPanel panel20;
  private SNLabelChamp OBJ_61;
  private JCheckBox V18_OBJ_120_55;
  private XRiTextField EBZP18;
  private JLabel OBJ_62;
  private JPanel panel6;
  private JCheckBox V04_OBJ_78_41;
  private SNLabelChamp OBJ_33;
  private XRiTextField EBZP4;
  private JLabel OBJ_34;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel OBJ_64;
  private XRiTextField CLCLI;
  private XRiTextField CLLIV;
  private RiZoneSortie OBJ_123;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPanel panel21;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
