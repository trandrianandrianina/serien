
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_GR extends SNPanelEcranRPG implements ioFrame {
  
  private String[] GRRON_Value = { "", "1", "2", "3", "4", "5", "6", };
  private String[] GRNSA_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] GRLOT_Value = { "", "1", "2", "3", };
  
  public VGVM01FX_GR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    GRNSA.setValeurs(GRNSA_Value, null);
    GRRON.setValeurs(GRRON_Value, null);
    GRLOT.setValeurs(GRLOT_Value, null);
    GRWEB.setValeursSelection("1", " ");
    GRAQW.setValeursSelection("1", " ");
    GRTPF.setValeursSelection("1", " ");
    GRSPE.setValeursSelection("1", " ");
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LCG01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG01@")).trim());
    LCG02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG02@")).trim());
    LCG03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG03@")).trim());
    LCG04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG04@")).trim());
    LCG05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG05@")).trim());
    LCG06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG06@")).trim());
    LCG07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG07@")).trim());
    LCG08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG08@")).trim());
    LCG09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG09@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_61.setVisible(GRPQW.isVisible());
    OBJ_74.setVisible(GRDEL.isVisible());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_59 = new JLabel();
    GRLIB = new XRiTextField();
    OBJ_62 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_73 = new JLabel();
    GRUNV = new XRiTextField();
    GRUNS = new XRiTextField();
    GRIMG = new XRiTextField();
    OBJ_63 = new JLabel();
    GRGP = new XRiTextField();
    OBJ_72 = new JLabel();
    GRTVA = new XRiTextField();
    OBJ_69 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_64 = new JLabel();
    GRRTAR = new XRiTextField();
    GRCMA = new XRiTextField();
    GRCPR = new XRiTextField();
    OBJ_80 = new JLabel();
    GRMAR = new XRiTextField();
    OBJ_61 = new JLabel();
    GRPQW = new XRiTextField();
    GRSPE = new XRiCheckBox();
    GRTPF = new XRiCheckBox();
    GRAQW = new XRiCheckBox();
    GRWEB = new XRiCheckBox();
    OBJ_82 = new JLabel();
    GRRON = new XRiComboBox();
    GRDEL = new XRiTextField();
    OBJ_74 = new JLabel();
    GRLOT = new XRiComboBox();
    OBJ_113 = new JLabel();
    GRSAN = new XRiTextField();
    OBJ_124 = new JLabel();
    GRNSA = new XRiComboBox();
    SAN1 = new XRiTextField();
    SAN2 = new XRiTextField();
    SAN3 = new XRiTextField();
    SAN4 = new XRiTextField();
    SAN5 = new XRiTextField();
    SAN6 = new XRiTextField();
    SAN7 = new XRiTextField();
    SAN8 = new XRiTextField();
    SAN9 = new XRiTextField();
    OBJ_114 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_122 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    GRC01 = new XRiTextField();
    GRC02 = new XRiTextField();
    GRC03 = new XRiTextField();
    GRC04 = new XRiTextField();
    GRC05 = new XRiTextField();
    GRC06 = new XRiTextField();
    GRC07 = new XRiTextField();
    GRC08 = new XRiTextField();
    GRC09 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    LCG01 = new RiZoneSortie();
    LCG02 = new RiZoneSortie();
    LCG03 = new RiZoneSortie();
    LCG04 = new RiZoneSortie();
    LCG05 = new RiZoneSortie();
    LCG06 = new RiZoneSortie();
    LCG07 = new RiZoneSortie();
    LCG08 = new RiZoneSortie();
    LCG09 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(INDETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(INDTYP, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(INDIND, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(e -> bouton_validerActionPerformed(e));
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(e -> bouton_retourActionPerformed(e));
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(e -> riSousMenu_bt6ActionPerformed(e));
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(e -> riSousMenu_bt7ActionPerformed(e));
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Groupe d'articles");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);
            
            // ======== panel1 ========
            {
              panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);
              
              // ---- OBJ_59 ----
              OBJ_59.setText("Libell\u00e9 de groupe");
              OBJ_59.setName("OBJ_59");
              panel1.add(OBJ_59);
              OBJ_59.setBounds(20, 9, 115, 20);
              
              // ---- GRLIB ----
              GRLIB.setComponentPopupMenu(BTD);
              GRLIB.setName("GRLIB");
              panel1.add(GRLIB);
              GRLIB.setBounds(145, 5, 320, GRLIB.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(10, 10, 940, 40);
            
            // ---- OBJ_62 ----
            OBJ_62.setText("Type de fiche article");
            OBJ_62.setName("OBJ_62");
            xTitledPanel1ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(20, 69, 124, 20);
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Code unit\u00e9 de vente");
            OBJ_67.setName("OBJ_67");
            xTitledPanel1ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(20, 105, 123, 20);
            
            // ---- OBJ_73 ----
            OBJ_73.setText("Code unit\u00e9 de stock");
            OBJ_73.setName("OBJ_73");
            xTitledPanel1ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(20, 141, 121, 20);
            
            // ---- GRUNV ----
            GRUNV.setComponentPopupMenu(BTD);
            GRUNV.setName("GRUNV");
            xTitledPanel1ContentContainer.add(GRUNV);
            GRUNV.setBounds(155, 101, 30, GRUNV.getPreferredSize().height);
            
            // ---- GRUNS ----
            GRUNS.setComponentPopupMenu(BTD);
            GRUNS.setName("GRUNS");
            xTitledPanel1ContentContainer.add(GRUNS);
            GRUNS.setBounds(155, 137, 30, GRUNS.getPreferredSize().height);
            
            // ---- GRIMG ----
            GRIMG.setComponentPopupMenu(BTD);
            GRIMG.setName("GRIMG");
            xTitledPanel1ContentContainer.add(GRIMG);
            GRIMG.setBounds(155, 65, 20, GRIMG.getPreferredSize().height);
            
            // ---- OBJ_63 ----
            OBJ_63.setText("Gestionnaire produit");
            OBJ_63.setName("OBJ_63");
            xTitledPanel1ContentContainer.add(OBJ_63);
            OBJ_63.setBounds(20, 177, 123, 20);
            
            // ---- GRGP ----
            GRGP.setComponentPopupMenu(BTD);
            GRGP.setName("GRGP");
            xTitledPanel1ContentContainer.add(GRGP);
            GRGP.setBounds(155, 173, 20, GRGP.getPreferredSize().height);
            
            // ---- OBJ_72 ----
            OBJ_72.setText("Code TVA");
            OBJ_72.setName("OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(240, 69, 70, 20);
            
            // ---- GRTVA ----
            GRTVA.setComponentPopupMenu(BTD);
            GRTVA.setName("GRTVA");
            xTitledPanel1ContentContainer.add(GRTVA);
            GRTVA.setBounds(400, 65, 20, GRTVA.getPreferredSize().height);
            
            // ---- OBJ_69 ----
            OBJ_69.setText("Coefficient de vente/tarif 1");
            OBJ_69.setName("OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(240, 141, 150, 20);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("Coefficient prix de revient");
            OBJ_75.setName("OBJ_75");
            xTitledPanel1ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(240, 177, 151, 20);
            
            // ---- OBJ_64 ----
            OBJ_64.setText("R\u00e9f\u00e9rence tarif");
            OBJ_64.setName("OBJ_64");
            xTitledPanel1ContentContainer.add(OBJ_64);
            OBJ_64.setBounds(240, 105, 90, 20);
            
            // ---- GRRTAR ----
            GRRTAR.setComponentPopupMenu(BTD);
            GRRTAR.setName("GRRTAR");
            xTitledPanel1ContentContainer.add(GRRTAR);
            GRRTAR.setBounds(400, 101, 60, GRRTAR.getPreferredSize().height);
            
            // ---- GRCMA ----
            GRCMA.setComponentPopupMenu(BTD);
            GRCMA.setName("GRCMA");
            xTitledPanel1ContentContainer.add(GRCMA);
            GRCMA.setBounds(400, 137, 70, GRCMA.getPreferredSize().height);
            
            // ---- GRCPR ----
            GRCPR.setComponentPopupMenu(BTD);
            GRCPR.setName("GRCPR");
            xTitledPanel1ContentContainer.add(GRCPR);
            GRCPR.setBounds(400, 173, 70, GRCPR.getPreferredSize().height);
            
            // ---- OBJ_80 ----
            OBJ_80.setText("% minimum de marge");
            OBJ_80.setName("OBJ_80");
            xTitledPanel1ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(525, 69, 151, 20);
            
            // ---- GRMAR ----
            GRMAR.setComponentPopupMenu(BTD);
            GRMAR.setName("GRMAR");
            xTitledPanel1ContentContainer.add(GRMAR);
            GRMAR.setBounds(711, 65, 26, GRMAR.getPreferredSize().height);
            
            // ---- OBJ_61 ----
            OBJ_61.setText("Quantit\u00e9 maximum");
            OBJ_61.setName("OBJ_61");
            xTitledPanel1ContentContainer.add(OBJ_61);
            OBJ_61.setBounds(525, 105, 123, 20);
            
            // ---- GRPQW ----
            GRPQW.setComponentPopupMenu(BTD);
            GRPQW.setName("GRPQW");
            xTitledPanel1ContentContainer.add(GRPQW);
            GRPQW.setBounds(655, 101, 82, GRPQW.getPreferredSize().height);
            
            // ---- GRSPE ----
            GRSPE.setText("Sp\u00e9cial");
            GRSPE.setComponentPopupMenu(BTD);
            GRSPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GRSPE.setName("GRSPE");
            xTitledPanel1ContentContainer.add(GRSPE);
            GRSPE.setBounds(525, 133, 82, 20);
            
            // ---- GRTPF ----
            GRTPF.setText("Assujetti \u00e0 la taxe parafiscale");
            GRTPF.setComponentPopupMenu(BTD);
            GRTPF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GRTPF.setName("GRTPF");
            xTitledPanel1ContentContainer.add(GRTPF);
            GRTPF.setBounds(525, 157, 193, 20);
            
            // ---- GRAQW ----
            GRAQW.setText("Affichage dispo");
            GRAQW.setComponentPopupMenu(BTD);
            GRAQW.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GRAQW.setName("GRAQW");
            xTitledPanel1ContentContainer.add(GRAQW);
            GRAQW.setBounds(795, 69, 115, 20);
            
            // ---- GRWEB ----
            GRWEB.setText("Exclus Web");
            GRWEB.setComponentPopupMenu(BTD);
            GRWEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GRWEB.setName("GRWEB");
            xTitledPanel1ContentContainer.add(GRWEB);
            GRWEB.setBounds(796, 97, 140, 20);
            
            // ---- OBJ_82 ----
            OBJ_82.setText("Arrondi");
            OBJ_82.setName("OBJ_82");
            xTitledPanel1ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(795, 125, 64, 20);
            
            // ---- GRRON ----
            GRRON.setModel(new DefaultComboBoxModel<>(new String[] { "", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00" }));
            GRRON.setComponentPopupMenu(BTD);
            GRRON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GRRON.setName("GRRON");
            xTitledPanel1ContentContainer.add(GRRON);
            GRRON.setBounds(860, 122, 70, GRRON.getPreferredSize().height);
            
            // ---- GRDEL ----
            GRDEL.setComponentPopupMenu(BTD);
            GRDEL.setName("GRDEL");
            xTitledPanel1ContentContainer.add(GRDEL);
            GRDEL.setBounds(900, 150, 30, GRDEL.getPreferredSize().height);
            
            // ---- OBJ_74 ----
            OBJ_74.setText("D\u00e9lai de survente");
            OBJ_74.setName("OBJ_74");
            xTitledPanel1ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(765, 154, 130, 20);
            
            // ---- GRLOT ----
            GRLOT.setModel(new DefaultComboBoxModel<>(new String[] { "Pas de saisie des lots \u00e0 la commande",
                "Saisie des lots \u00e0 la validation de la commande de vente",
                "Saisie des lots \u00e0 la validation de la commande d'achat",
                "Saisie des lots  la validation des commandes de ventes et d'achats" }));
            GRLOT.setComponentPopupMenu(BTD);
            GRLOT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GRLOT.setName("GRLOT");
            xTitledPanel1ContentContainer.add(GRLOT);
            GRLOT.setBounds(525, 185, 405, GRLOT.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }
          
          // ---- OBJ_113 ----
          OBJ_113.setText("Code  analytique");
          OBJ_113.setName("OBJ_113");
          
          // ---- GRSAN ----
          GRSAN.setComponentPopupMenu(BTD);
          GRSAN.setName("GRSAN");
          
          // ---- OBJ_124 ----
          OBJ_124.setText("Pointeur");
          OBJ_124.setName("OBJ_124");
          
          // ---- GRNSA ----
          GRNSA.setModel(new DefaultComboBoxModel<>(new String[] { "", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
          GRNSA.setComponentPopupMenu(BTD);
          GRNSA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          GRNSA.setName("GRNSA");
          
          // ---- SAN1 ----
          SAN1.setName("SAN1");
          
          // ---- SAN2 ----
          SAN2.setName("SAN2");
          
          // ---- SAN3 ----
          SAN3.setName("SAN3");
          
          // ---- SAN4 ----
          SAN4.setName("SAN4");
          
          // ---- SAN5 ----
          SAN5.setName("SAN5");
          
          // ---- SAN6 ----
          SAN6.setName("SAN6");
          
          // ---- SAN7 ----
          SAN7.setName("SAN7");
          
          // ---- SAN8 ----
          SAN8.setName("SAN8");
          
          // ---- SAN9 ----
          SAN9.setName("SAN9");
          
          // ---- OBJ_114 ----
          OBJ_114.setText("1");
          OBJ_114.setName("OBJ_114");
          
          // ---- OBJ_115 ----
          OBJ_115.setText("2");
          OBJ_115.setName("OBJ_115");
          
          // ---- OBJ_116 ----
          OBJ_116.setText("3");
          OBJ_116.setName("OBJ_116");
          
          // ---- OBJ_117 ----
          OBJ_117.setText("4");
          OBJ_117.setName("OBJ_117");
          
          // ---- OBJ_118 ----
          OBJ_118.setText("5");
          OBJ_118.setName("OBJ_118");
          
          // ---- OBJ_119 ----
          OBJ_119.setText("6");
          OBJ_119.setName("OBJ_119");
          
          // ---- OBJ_120 ----
          OBJ_120.setText("7");
          OBJ_120.setName("OBJ_120");
          
          // ---- OBJ_121 ----
          OBJ_121.setText("8");
          OBJ_121.setName("OBJ_121");
          
          // ---- OBJ_122 ----
          OBJ_122.setText("9");
          OBJ_122.setName("OBJ_122");
          
          // ======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Comptabilisation ventes");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);
            
            // ---- GRC01 ----
            GRC01.setName("GRC01");
            xTitledPanel2ContentContainer.add(GRC01);
            GRC01.setBounds(335, 10, 36, 25);
            
            // ---- GRC02 ----
            GRC02.setName("GRC02");
            xTitledPanel2ContentContainer.add(GRC02);
            GRC02.setBounds(335, 30, 36, 25);
            
            // ---- GRC03 ----
            GRC03.setName("GRC03");
            xTitledPanel2ContentContainer.add(GRC03);
            GRC03.setBounds(335, 50, 36, 25);
            
            // ---- GRC04 ----
            GRC04.setName("GRC04");
            xTitledPanel2ContentContainer.add(GRC04);
            GRC04.setBounds(335, 70, 36, 25);
            
            // ---- GRC05 ----
            GRC05.setName("GRC05");
            xTitledPanel2ContentContainer.add(GRC05);
            GRC05.setBounds(335, 90, 36, 25);
            
            // ---- GRC06 ----
            GRC06.setName("GRC06");
            xTitledPanel2ContentContainer.add(GRC06);
            GRC06.setBounds(335, 110, 36, 25);
            
            // ---- GRC07 ----
            GRC07.setName("GRC07");
            xTitledPanel2ContentContainer.add(GRC07);
            GRC07.setBounds(335, 130, 36, 25);
            
            // ---- GRC08 ----
            GRC08.setName("GRC08");
            xTitledPanel2ContentContainer.add(GRC08);
            GRC08.setBounds(335, 150, 36, 25);
            
            // ---- GRC09 ----
            GRC09.setName("GRC09");
            xTitledPanel2ContentContainer.add(GRC09);
            GRC09.setBounds(335, 170, 36, 25);
            
            // ---- label1 ----
            label1.setText("N\u00b0CO ventes taxables TVA1");
            label1.setName("label1");
            xTitledPanel2ContentContainer.add(label1);
            label1.setBounds(145, 10, 185, 25);
            
            // ---- label2 ----
            label2.setText("N\u00b0CO ventes taxables TVA2");
            label2.setName("label2");
            xTitledPanel2ContentContainer.add(label2);
            label2.setBounds(145, 30, 185, 25);
            
            // ---- label3 ----
            label3.setText("N\u00b0CO ventes taxables TVA3");
            label3.setName("label3");
            xTitledPanel2ContentContainer.add(label3);
            label3.setBounds(145, 50, 185, 25);
            
            // ---- label4 ----
            label4.setText("N\u00b0CO ventes taxables TVA4");
            label4.setName("label4");
            xTitledPanel2ContentContainer.add(label4);
            label4.setBounds(145, 70, 185, 25);
            
            // ---- label5 ----
            label5.setText("N\u00b0CO ventes taxables TVA5");
            label5.setName("label5");
            xTitledPanel2ContentContainer.add(label5);
            label5.setBounds(145, 90, 185, 25);
            
            // ---- label6 ----
            label6.setText("N\u00b0CO ventes taxables TVA6");
            label6.setName("label6");
            xTitledPanel2ContentContainer.add(label6);
            label6.setBounds(145, 110, 185, 25);
            
            // ---- label7 ----
            label7.setText("N\u00b0CO ventes assimil\u00e9es export");
            label7.setName("label7");
            xTitledPanel2ContentContainer.add(label7);
            label7.setBounds(145, 170, 185, 25);
            
            // ---- label8 ----
            label8.setText("N\u00b0CO ventes export");
            label8.setName("label8");
            xTitledPanel2ContentContainer.add(label8);
            label8.setBounds(145, 150, 185, 25);
            
            // ---- label9 ----
            label9.setText("N\u00b0CO ventes export CEE");
            label9.setName("label9");
            xTitledPanel2ContentContainer.add(label9);
            label9.setBounds(145, 130, 185, 25);
            
            // ---- LCG01 ----
            LCG01.setText("@LCG01@");
            LCG01.setName("LCG01");
            xTitledPanel2ContentContainer.add(LCG01);
            LCG01.setBounds(380, 12, 245, 20);
            
            // ---- LCG02 ----
            LCG02.setText("@LCG02@");
            LCG02.setName("LCG02");
            xTitledPanel2ContentContainer.add(LCG02);
            LCG02.setBounds(380, 32, 245, 20);
            
            // ---- LCG03 ----
            LCG03.setText("@LCG03@");
            LCG03.setName("LCG03");
            xTitledPanel2ContentContainer.add(LCG03);
            LCG03.setBounds(380, 52, 245, 20);
            
            // ---- LCG04 ----
            LCG04.setText("@LCG04@");
            LCG04.setName("LCG04");
            xTitledPanel2ContentContainer.add(LCG04);
            LCG04.setBounds(380, 72, 245, 20);
            
            // ---- LCG05 ----
            LCG05.setText("@LCG05@");
            LCG05.setName("LCG05");
            xTitledPanel2ContentContainer.add(LCG05);
            LCG05.setBounds(380, 92, 245, 20);
            
            // ---- LCG06 ----
            LCG06.setText("@LCG06@");
            LCG06.setName("LCG06");
            xTitledPanel2ContentContainer.add(LCG06);
            LCG06.setBounds(380, 112, 245, 20);
            
            // ---- LCG07 ----
            LCG07.setText("@LCG07@");
            LCG07.setName("LCG07");
            xTitledPanel2ContentContainer.add(LCG07);
            LCG07.setBounds(380, 132, 245, 20);
            
            // ---- LCG08 ----
            LCG08.setText("@LCG08@");
            LCG08.setName("LCG08");
            xTitledPanel2ContentContainer.add(LCG08);
            LCG08.setBounds(380, 152, 245, 20);
            
            // ---- LCG09 ----
            LCG09.setText("@LCG09@");
            LCG09.setName("LCG09");
            xTitledPanel2ContentContainer.add(LCG09);
            LCG09.setBounds(380, 172, 245, 20);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(12, 12, 12)
                          .addGroup(p_contenuLayout.createParallelGroup()
                              .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 967, GroupLayout.PREFERRED_SIZE)
                              .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 955, GroupLayout.PREFERRED_SIZE)))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(39, 39, 39)
                          .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                          .addComponent(GRSAN, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                          .addComponent(OBJ_124, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                          .addComponent(GRNSA, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                          .addComponent(SAN1, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN2, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN3, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN4, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN5, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN6, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN7, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN8, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                          .addComponent(SAN9, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(319, 319, 319)
                          .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_116, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE).addGap(31, 31, 31)
                          .addComponent(OBJ_122, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)))
                  .addGap(19, 19, 19)));
          p_contenuLayout.linkSize(SwingConstants.HORIZONTAL, new Component[] { xTitledPanel1, xTitledPanel2 });
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(13, 13, 13)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 246, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 230, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_114, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_115, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_116, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_117, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_118, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_119, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_120, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_121, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_122, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
                  .addGap(1, 1, 1)
                  .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(OBJ_113, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(GRSAN, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_124, GroupLayout.PREFERRED_SIZE,
                          20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(GRNSA, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN1, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN2, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN3, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN4, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN5, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN6, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN7, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN8, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_contenuLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(SAN9, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(e -> OBJ_18ActionPerformed(e));
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(e -> OBJ_19ActionPerformed(e));
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel1;
  private JLabel OBJ_59;
  private XRiTextField GRLIB;
  private JLabel OBJ_62;
  private JLabel OBJ_67;
  private JLabel OBJ_73;
  private XRiTextField GRUNV;
  private XRiTextField GRUNS;
  private XRiTextField GRIMG;
  private JLabel OBJ_63;
  private XRiTextField GRGP;
  private JLabel OBJ_72;
  private XRiTextField GRTVA;
  private JLabel OBJ_69;
  private JLabel OBJ_75;
  private JLabel OBJ_64;
  private XRiTextField GRRTAR;
  private XRiTextField GRCMA;
  private XRiTextField GRCPR;
  private JLabel OBJ_80;
  private XRiTextField GRMAR;
  private JLabel OBJ_61;
  private XRiTextField GRPQW;
  private XRiCheckBox GRSPE;
  private XRiCheckBox GRTPF;
  private XRiCheckBox GRAQW;
  private XRiCheckBox GRWEB;
  private JLabel OBJ_82;
  private XRiComboBox GRRON;
  private XRiTextField GRDEL;
  private JLabel OBJ_74;
  private XRiComboBox GRLOT;
  private JLabel OBJ_113;
  private XRiTextField GRSAN;
  private JLabel OBJ_124;
  private XRiComboBox GRNSA;
  private XRiTextField SAN1;
  private XRiTextField SAN2;
  private XRiTextField SAN3;
  private XRiTextField SAN4;
  private XRiTextField SAN5;
  private XRiTextField SAN6;
  private XRiTextField SAN7;
  private XRiTextField SAN8;
  private XRiTextField SAN9;
  private JLabel OBJ_114;
  private JLabel OBJ_115;
  private JLabel OBJ_116;
  private JLabel OBJ_117;
  private JLabel OBJ_118;
  private JLabel OBJ_119;
  private JLabel OBJ_120;
  private JLabel OBJ_121;
  private JLabel OBJ_122;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField GRC01;
  private XRiTextField GRC02;
  private XRiTextField GRC03;
  private XRiTextField GRC04;
  private XRiTextField GRC05;
  private XRiTextField GRC06;
  private XRiTextField GRC07;
  private XRiTextField GRC08;
  private XRiTextField GRC09;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private RiZoneSortie LCG01;
  private RiZoneSortie LCG02;
  private RiZoneSortie LCG03;
  private RiZoneSortie LCG04;
  private RiZoneSortie LCG05;
  private RiZoneSortie LCG06;
  private RiZoneSortie LCG07;
  private RiZoneSortie LCG08;
  private RiZoneSortie LCG09;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
