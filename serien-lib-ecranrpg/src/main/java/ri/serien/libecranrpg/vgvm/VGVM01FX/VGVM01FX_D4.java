
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_D4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] DSCDP_Value = { "P", "R", "E", };
  
  public VGVM01FX_D4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DSCDP.setValeurs(DSCDP_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("DSCDP", 0, DSCDP_Value[DSCDP.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_49 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel3 = new JPanel();
    OBJ_43 = new JLabel();
    DSLIB = new XRiTextField();
    DSCDP = new XRiComboBox();
    OBJ_52 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_60 = new JLabel();
    DSR41 = new XRiTextField();
    DSR42 = new XRiTextField();
    DSR43 = new XRiTextField();
    DSR44 = new XRiTextField();
    DSR45 = new XRiTextField();
    DSR46 = new XRiTextField();
    DSR47 = new XRiTextField();
    DSK401 = new XRiTextField();
    DSK402 = new XRiTextField();
    DSK403 = new XRiTextField();
    DSK404 = new XRiTextField();
    DSK405 = new XRiTextField();
    DSK406 = new XRiTextField();
    DSK407 = new XRiTextField();
    DSK411 = new XRiTextField();
    DSK412 = new XRiTextField();
    DSK413 = new XRiTextField();
    DSREA = new XRiTextField();
    DSMAG = new XRiTextField();
    panel1 = new JPanel();
    OBJ_45 = new JLabel();
    DSPE1 = new XRiTextField();
    OBJ_46 = new JLabel();
    DSPE2 = new XRiTextField();
    OBJ_47 = new JLabel();
    DSPE3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(5, 3, 93, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(95, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(160, 3, 36, 18);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(200, 0, 34, INDTYP.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Ordre");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(250, 3, 39, 18);

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");
          p_tete_gauche.add(INDIND);
          INDIND.setBounds(290, 0, 60, INDIND.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("D\u00e9pr\u00e9ciation de stock");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setPreferredSize(new Dimension(640, 480));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_43 ----
              OBJ_43.setText("Libell\u00e9");
              OBJ_43.setName("OBJ_43");
              panel3.add(OBJ_43);
              OBJ_43.setBounds(10, 12, 61, 20);

              //---- DSLIB ----
              DSLIB.setComponentPopupMenu(null);
              DSLIB.setName("DSLIB");
              panel3.add(DSLIB);
              DSLIB.setBounds(155, 8, 310, DSLIB.getPreferredSize().height);
            }
            xTitledPanel1ContentContainer.add(panel3);
            panel3.setBounds(45, 10, 485, 45);

            //---- DSCDP ----
            DSCDP.setModel(new DefaultComboBoxModel(new String[] {
              "P.U.M.P",
              "Prix de revient",
              "Prix derni\u00e8re entr\u00e9e"
            }));
            DSCDP.setComponentPopupMenu(null);
            DSCDP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DSCDP.setName("DSCDP");
            xTitledPanel1ContentContainer.add(DSCDP);
            DSCDP.setBounds(430, 152, 170, DSCDP.getPreferredSize().height);

            //---- OBJ_52 ----
            OBJ_52.setText("% de d\u00e9pr\u00e9ciation  => Articles non g\u00e9r\u00e9s (type 8)");
            OBJ_52.setName("OBJ_52");
            xTitledPanel1ContentContainer.add(OBJ_52);
            OBJ_52.setBounds(80, 454, 385, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("Articles non g\u00e9r\u00e9s (type 8)");
            OBJ_88.setName("OBJ_88");
            xTitledPanel1ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(45, 400, 195, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("Articles fin de s\u00e9rie ou pr\u00e9-fin de s\u00e9rie");
            OBJ_85.setName("OBJ_85");
            xTitledPanel1ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(45, 375, 215, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Articles rebut magasin");
            OBJ_90.setName("OBJ_90");
            xTitledPanel1ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(45, 425, 137, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("Taux d\u00e9pr\u00e9ciation sur");
            OBJ_62.setName("OBJ_62");
            xTitledPanel1ContentContainer.add(OBJ_62);
            OBJ_62.setBounds(290, 155, 132, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Vitesse rotation");
            OBJ_60.setName("OBJ_60");
            xTitledPanel1ContentContainer.add(OBJ_60);
            OBJ_60.setBounds(55, 155, 128, 20);

            //---- DSR41 ----
            DSR41.setComponentPopupMenu(null);
            DSR41.setName("DSR41");
            xTitledPanel1ContentContainer.add(DSR41);
            DSR41.setBounds(135, 180, 52, DSR41.getPreferredSize().height);

            //---- DSR42 ----
            DSR42.setComponentPopupMenu(null);
            DSR42.setName("DSR42");
            xTitledPanel1ContentContainer.add(DSR42);
            DSR42.setBounds(135, 205, 52, DSR42.getPreferredSize().height);

            //---- DSR43 ----
            DSR43.setComponentPopupMenu(null);
            DSR43.setName("DSR43");
            xTitledPanel1ContentContainer.add(DSR43);
            DSR43.setBounds(135, 230, 52, DSR43.getPreferredSize().height);

            //---- DSR44 ----
            DSR44.setComponentPopupMenu(null);
            DSR44.setName("DSR44");
            xTitledPanel1ContentContainer.add(DSR44);
            DSR44.setBounds(135, 255, 52, DSR44.getPreferredSize().height);

            //---- DSR45 ----
            DSR45.setComponentPopupMenu(null);
            DSR45.setName("DSR45");
            xTitledPanel1ContentContainer.add(DSR45);
            DSR45.setBounds(135, 280, 52, DSR45.getPreferredSize().height);

            //---- DSR46 ----
            DSR46.setComponentPopupMenu(null);
            DSR46.setName("DSR46");
            xTitledPanel1ContentContainer.add(DSR46);
            DSR46.setBounds(135, 305, 52, DSR46.getPreferredSize().height);

            //---- DSR47 ----
            DSR47.setComponentPopupMenu(null);
            DSR47.setName("DSR47");
            xTitledPanel1ContentContainer.add(DSR47);
            DSR47.setBounds(135, 330, 52, DSR47.getPreferredSize().height);

            //---- DSK401 ----
            DSK401.setComponentPopupMenu(null);
            DSK401.setName("DSK401");
            xTitledPanel1ContentContainer.add(DSK401);
            DSK401.setBounds(345, 180, 36, DSK401.getPreferredSize().height);

            //---- DSK402 ----
            DSK402.setComponentPopupMenu(null);
            DSK402.setName("DSK402");
            xTitledPanel1ContentContainer.add(DSK402);
            DSK402.setBounds(345, 205, 36, DSK402.getPreferredSize().height);

            //---- DSK403 ----
            DSK403.setComponentPopupMenu(null);
            DSK403.setName("DSK403");
            xTitledPanel1ContentContainer.add(DSK403);
            DSK403.setBounds(345, 230, 36, DSK403.getPreferredSize().height);

            //---- DSK404 ----
            DSK404.setComponentPopupMenu(null);
            DSK404.setName("DSK404");
            xTitledPanel1ContentContainer.add(DSK404);
            DSK404.setBounds(345, 255, 36, DSK404.getPreferredSize().height);

            //---- DSK405 ----
            DSK405.setComponentPopupMenu(null);
            DSK405.setName("DSK405");
            xTitledPanel1ContentContainer.add(DSK405);
            DSK405.setBounds(345, 280, 36, DSK405.getPreferredSize().height);

            //---- DSK406 ----
            DSK406.setComponentPopupMenu(null);
            DSK406.setName("DSK406");
            xTitledPanel1ContentContainer.add(DSK406);
            DSK406.setBounds(345, 305, 36, DSK406.getPreferredSize().height);

            //---- DSK407 ----
            DSK407.setComponentPopupMenu(null);
            DSK407.setName("DSK407");
            xTitledPanel1ContentContainer.add(DSK407);
            DSK407.setBounds(345, 330, 36, DSK407.getPreferredSize().height);

            //---- DSK411 ----
            DSK411.setComponentPopupMenu(null);
            DSK411.setName("DSK411");
            xTitledPanel1ContentContainer.add(DSK411);
            DSK411.setBounds(345, 371, 36, DSK411.getPreferredSize().height);

            //---- DSK412 ----
            DSK412.setComponentPopupMenu(null);
            DSK412.setName("DSK412");
            xTitledPanel1ContentContainer.add(DSK412);
            DSK412.setBounds(345, 396, 36, DSK412.getPreferredSize().height);

            //---- DSK413 ----
            DSK413.setComponentPopupMenu(null);
            DSK413.setName("DSK413");
            xTitledPanel1ContentContainer.add(DSK413);
            DSK413.setBounds(345, 421, 36, DSK413.getPreferredSize().height);

            //---- DSREA ----
            DSREA.setComponentPopupMenu(null);
            DSREA.setName("DSREA");
            xTitledPanel1ContentContainer.add(DSREA);
            DSREA.setBounds(45, 450, 28, DSREA.getPreferredSize().height);

            //---- DSMAG ----
            DSMAG.setComponentPopupMenu(null);
            DSMAG.setName("DSMAG");
            xTitledPanel1ContentContainer.add(DSMAG);
            DSMAG.setBounds(190, 421, 30, DSMAG.getPreferredSize().height);

            //======== panel1 ========
            {
              panel1.setBorder(new TitledBorder("P\u00e9riode r\u00e9f\u00e9rence (mois)"));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_45 ----
              OBJ_45.setText("Carence");
              OBJ_45.setName("OBJ_45");
              panel1.add(OBJ_45);
              OBJ_45.setBounds(15, 34, 72, 20);

              //---- DSPE1 ----
              DSPE1.setComponentPopupMenu(null);
              DSPE1.setName("DSPE1");
              panel1.add(DSPE1);
              DSPE1.setBounds(95, 30, 28, DSPE1.getPreferredSize().height);

              //---- OBJ_46 ----
              OBJ_46.setText("Vente");
              OBJ_46.setName("OBJ_46");
              panel1.add(OBJ_46);
              OBJ_46.setBounds(245, 34, 56, 20);

              //---- DSPE2 ----
              DSPE2.setComponentPopupMenu(null);
              DSPE2.setName("DSPE2");
              panel1.add(DSPE2);
              DSPE2.setBounds(305, 30, 28, DSPE2.getPreferredSize().height);

              //---- OBJ_47 ----
              OBJ_47.setText("Achat");
              OBJ_47.setName("OBJ_47");
              panel1.add(OBJ_47);
              OBJ_47.setBounds(455, 34, 55, 20);

              //---- DSPE3 ----
              DSPE3.setComponentPopupMenu(null);
              DSPE3.setName("DSPE3");
              panel1.add(DSPE3);
              DSPE3.setBounds(515, 30, 28, DSPE3.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            xTitledPanel1ContentContainer.add(panel1);
            panel1.setBounds(42, 65, 590, 75);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_49;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel3;
  private JLabel OBJ_43;
  private XRiTextField DSLIB;
  private XRiComboBox DSCDP;
  private JLabel OBJ_52;
  private JLabel OBJ_88;
  private JLabel OBJ_85;
  private JLabel OBJ_90;
  private JLabel OBJ_62;
  private JLabel OBJ_60;
  private XRiTextField DSR41;
  private XRiTextField DSR42;
  private XRiTextField DSR43;
  private XRiTextField DSR44;
  private XRiTextField DSR45;
  private XRiTextField DSR46;
  private XRiTextField DSR47;
  private XRiTextField DSK401;
  private XRiTextField DSK402;
  private XRiTextField DSK403;
  private XRiTextField DSK404;
  private XRiTextField DSK405;
  private XRiTextField DSK406;
  private XRiTextField DSK407;
  private XRiTextField DSK411;
  private XRiTextField DSK412;
  private XRiTextField DSK413;
  private XRiTextField DSREA;
  private XRiTextField DSMAG;
  private JPanel panel1;
  private JLabel OBJ_45;
  private XRiTextField DSPE1;
  private JLabel OBJ_46;
  private XRiTextField DSPE2;
  private JLabel OBJ_47;
  private XRiTextField DSPE3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
