
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_ZP extends SNPanelEcranRPG implements ioFrame {
  
  private String[] ZPT5_Value = { "1", "2", "", };
  private String[] ZPT1_Value = { "", "A", "N", "M", "D", "B", "T", "R", };
  private String[] ZPT2_Value = { "", "1", "2", "3", };
  
  public VGVM01FX_ZP(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ZPT5.setValeurs(ZPT5_Value, null);
    ZPT1.setValeurs(ZPT1_Value, null);
    ZPT2.setValeurs(ZPT2_Value, null);
    ZPT4.setValeursSelection("1", " ");
    ZPT3.setValeursSelection("1", " ");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBZP@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    OBJ_53 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    ZPT2 = new XRiComboBox();
    ZPT1 = new XRiComboBox();
    ZPT5 = new XRiComboBox();
    ZPLIB = new XRiTextField();
    ZPPGM = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_72 = new JLabel();
    ZPENT = new XRiTextField();
    ZPDEC = new XRiTextField();
    OBJ_68 = new JLabel();
    ZPINI = new XRiTextField();
    panel3 = new JPanel();
    ZPT3 = new XRiCheckBox();
    panel4 = new JPanel();
    ZPT4 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");
          
          // ---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          
          // ---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          
          // ---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          
          // ---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");
          
          // ---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");
          
          // ---- OBJ_53 ----
          OBJ_53.setText("@LIBZP@");
          OBJ_53.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_53.setOpaque(false);
          OBJ_53.setName("OBJ_53");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE).addGap(2, 2, 2)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(4, 4, 4)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE).addGap(16, 16, 16)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 285, GroupLayout.PREFERRED_SIZE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setToolTipText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(640, 440));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Zone personnalis\u00e9e");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);
            
            // ---- ZPT2 ----
            ZPT2.setModel(new DefaultComboBoxModel(
                new String[] { "Aucun", "l'existence du param\u00e8tre", "obligatoire", "existence du param\u00e8tre et obligatoire" }));
            ZPT2.setComponentPopupMenu(BTD);
            ZPT2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZPT2.setName("ZPT2");
            xTitledPanel4ContentContainer.add(ZPT2);
            ZPT2.setBounds(80, 125, 245, ZPT2.getPreferredSize().height);
            
            // ---- ZPT1 ----
            ZPT1.setModel(new DefaultComboBoxModel(new String[] { "Alphanum\u00e9rique", "Alphab\u00e9tique", "Num\u00e9rique",
                "Minuscules", "Date", "Bool\u00e9en", "Matricielle", "Article" }));
            ZPT1.setComponentPopupMenu(BTD);
            ZPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZPT1.setName("ZPT1");
            xTitledPanel4ContentContainer.add(ZPT1);
            ZPT1.setBounds(80, 55, 140, ZPT1.getPreferredSize().height);
            
            // ---- ZPT5 ----
            ZPT5.setModel(new DefaultComboBoxModel(new String[] { "GVM", "GAM", "GVM et GAM" }));
            ZPT5.setComponentPopupMenu(BTD);
            ZPT5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ZPT5.setName("ZPT5");
            xTitledPanel4ContentContainer.add(ZPT5);
            ZPT5.setBounds(80, 285, 105, ZPT5.getPreferredSize().height);
            
            // ---- ZPLIB ----
            ZPLIB.setComponentPopupMenu(BTD);
            ZPLIB.setName("ZPLIB");
            xTitledPanel4ContentContainer.add(ZPLIB);
            ZPLIB.setBounds(80, 20, 310, ZPLIB.getPreferredSize().height);
            
            // ---- ZPPGM ----
            ZPPGM.setComponentPopupMenu(BTD);
            ZPPGM.setName("ZPPGM");
            xTitledPanel4ContentContainer.add(ZPPGM);
            ZPPGM.setBounds(80, 160, 110, ZPPGM.getPreferredSize().height);
            
            // ---- OBJ_76 ----
            OBJ_76.setText("Sp\u00e9cifique");
            OBJ_76.setName("OBJ_76");
            xTitledPanel4ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(15, 164, 65, 20);
            
            // ---- OBJ_74 ----
            OBJ_74.setText("d\u00e9cimales");
            OBJ_74.setToolTipText("Ne peut \u00eatre modifi\u00e9 que pour une zone num\u00e9rique");
            OBJ_74.setName("OBJ_74");
            xTitledPanel4ContentContainer.add(OBJ_74);
            OBJ_74.setBounds(180, 94, 66, 20);
            
            // ---- OBJ_78 ----
            OBJ_78.setText("Utilisation");
            OBJ_78.setName("OBJ_78");
            xTitledPanel4ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(15, 288, 61, 20);
            
            // ---- OBJ_75 ----
            OBJ_75.setText("Contr\u00f4le");
            OBJ_75.setName("OBJ_75");
            xTitledPanel4ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(15, 128, 54, 20);
            
            // ---- OBJ_69 ----
            OBJ_69.setText("Type");
            OBJ_69.setName("OBJ_69");
            xTitledPanel4ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(15, 58, 36, 20);
            
            // ---- OBJ_70 ----
            OBJ_70.setText("Taille");
            OBJ_70.setToolTipText("Ne peut \u00eatre modifi\u00e9 que pour une zone num\u00e9rique");
            OBJ_70.setName("OBJ_70");
            xTitledPanel4ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(15, 94, 36, 20);
            
            // ---- OBJ_67 ----
            OBJ_67.setText("Titre");
            OBJ_67.setName("OBJ_67");
            xTitledPanel4ContentContainer.add(OBJ_67);
            OBJ_67.setBounds(15, 24, 30, 20);
            
            // ---- OBJ_72 ----
            OBJ_72.setText("dont");
            OBJ_72.setToolTipText("Ne peut \u00eatre modifi\u00e9 que pour une zone num\u00e9rique");
            OBJ_72.setName("OBJ_72");
            xTitledPanel4ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(115, 94, 30, 20);
            
            // ---- ZPENT ----
            ZPENT.setComponentPopupMenu(BTD);
            ZPENT.setName("ZPENT");
            xTitledPanel4ContentContainer.add(ZPENT);
            ZPENT.setBounds(80, 90, 26, ZPENT.getPreferredSize().height);
            
            // ---- ZPDEC ----
            ZPDEC.setComponentPopupMenu(BTD);
            ZPDEC.setName("ZPDEC");
            xTitledPanel4ContentContainer.add(ZPDEC);
            ZPDEC.setBounds(150, 90, 26, ZPDEC.getPreferredSize().height);
            
            // ---- OBJ_68 ----
            OBJ_68.setText("Titre abr\u00e9g\u00e9");
            OBJ_68.setName("OBJ_68");
            xTitledPanel4ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(400, 24, 78, 20);
            
            // ---- ZPINI ----
            ZPINI.setComponentPopupMenu(BTD);
            ZPINI.setName("ZPINI");
            xTitledPanel4ContentContainer.add(ZPINI);
            ZPINI.setBounds(475, 20, 34, ZPINI.getPreferredSize().height);
            
            // ======== panel3 ========
            {
              panel3.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- ZPT3 ----
              ZPT3.setText("La valeur de la zone personnalis\u00e9e sera saisie");
              ZPT3.setComponentPopupMenu(BTD);
              ZPT3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ZPT3.setName("ZPT3");
              panel3.add(ZPT3);
              ZPT3.setBounds(5, 7, 290, 20);
            }
            xTitledPanel4ContentContainer.add(panel3);
            panel3.setBounds(15, 195, 315, 35);
            
            // ======== panel4 ========
            {
              panel4.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- ZPT4 ----
              ZPT4.setText("Modification impossible depuis un autre fichier");
              ZPT4.setComponentPopupMenu(BTD);
              ZPT4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ZPT4.setName("ZPT4");
              panel4.add(ZPT4);
              ZPT4.setBounds(5, 7, 298, 20);
            }
            xTitledPanel4ContentContainer.add(panel4);
            panel4.setBounds(15, 240, 315, 35);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
              p_contenuLayout.createSequentialGroup().addContainerGap(58, Short.MAX_VALUE)
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE).addGap(50, 50, 50)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addGap(42, 42, 42)
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(41, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private RiZoneSortie OBJ_53;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private XRiComboBox ZPT2;
  private XRiComboBox ZPT1;
  private XRiComboBox ZPT5;
  private XRiTextField ZPLIB;
  private XRiTextField ZPPGM;
  private JLabel OBJ_76;
  private JLabel OBJ_74;
  private JLabel OBJ_78;
  private JLabel OBJ_75;
  private JLabel OBJ_69;
  private JLabel OBJ_70;
  private JLabel OBJ_67;
  private JLabel OBJ_72;
  private XRiTextField ZPENT;
  private XRiTextField ZPDEC;
  private JLabel OBJ_68;
  private XRiTextField ZPINI;
  private JPanel panel3;
  private XRiCheckBox ZPT3;
  private JPanel panel4;
  private XRiCheckBox ZPT4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
