
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.commun.memo.EnumTypeMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.gescom.commun.adressedocument.EnumCodeEnteteAdresseDocument;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.adresselivraison.ModeleListeAdresseLivraisonChantier;
import ri.serien.libswing.composant.metier.vente.adresselivraison.VueListeAdresseLivraisonChantier;
import ri.serien.libswing.composant.metier.vente.affaire.snaffaire.SNAffaire;
import ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente.SNConditionVenteEtClient;
import ri.serien.libswing.composant.metier.vente.documentvente.snmodeexpedition.SNModeExpedition;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNAdresseMail;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_AD extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] E1TRC_Value = { "", "1", "N", "G" };
  private String[] E1PGC_Value = { "1", "", "2", "3" };
  private RiPanelNav riPanelNav1 = null;
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  private boolean isChantier = false;
  private boolean isProspect = false;
  private ODialog dialog_REGL = null;
  private String libelleDirectOuInterne = "";
  private Memo memoLivraisonEnlevement = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_AD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    E1PGC.setValeurs(E1PGC_Value, null);
    E1IN9.setValeursSelection("X", "");
    E1NHO.setValeursSelection("1", "");
    
    // Navigation graphique du menu
    riPanelNav1 = new RiPanelNav();
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de devis", true, null);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du devis", false, bouton_valider);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de devis", false, button1);
    riSousMenu1.add(riPanelNav1);
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/reduire.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/navigation.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@     @WLIEN@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    lbCan1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE1@")).trim());
    lbCan3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE3@")).trim());
    lbCan5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE5@")).trim());
    lbCan2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE2@")).trim());
    lbCan4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPE4@")).trim());
    LTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LTEL@")).trim());
    LTEL.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("mail : @LMAIL@ - fax : @LFAX@")).trim());
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WREPAC@")).trim());
    lib_Devise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    lbPlafond2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRG1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    isProspect = lexique.HostFieldGetData("TYP11").toLowerCase().contains("prospect");
    
    lbCan5.setVisible(lexique.isPresent("TIZPE5"));
    lbCan4.setVisible(lexique.isPresent("TIZPE4"));
    lbCan3.setVisible(lexique.isPresent("TIZPE3"));
    lbCan2.setVisible(lexique.isPresent("TIZPE2"));
    lbCan1.setVisible(lexique.isPresent("TIZPE1"));
    OBJ_102.setVisible(lexique.isPresent("WREPAC"));
    lbAffaire.setVisible(lexique.isPresent("E1ACT"));
    lbSection.setVisible(lexique.isPresent("E1SAN"));
    lbMotif.setVisible(lexique.isPresent("E1VEH"));
    
    if (lexique.isTrue("79")) {
      WEPOS.setForeground(Color.RED);
    }
    
    if (lexique.isTrue("(N08) AND (N38)")) {
      lbDepassement.setText("Dépassement");
    }
    else {
      lbDepassement.setText("Ecart");
    }
    
    riBoutonDetail1.setVisible(lexique.HostFieldGetData("WDLPM").equals("1"));
    riBoutonDetail2.setVisible(lexique.HostFieldGetData("WDLPM").equals("1"));
    
    // régularisation stock palette chantier
    riSousMenu17.setEnabled(isChantier);
    riSousMenu17.setVisible(isChantier && lexique.HostFieldGetData("REGPAL").equals("STKPAL"));
    
    // Client
    // Barre tête
    E1NFA.setVisible(!lexique.HostFieldGetData("E1NFA").trim().equals(""));
    OBJ_50.setVisible(E1NFA.isVisible());
    
    // Label état du bon
    String etatBon = lexique.HostFieldGetData("ETABON").trim();
    bouton_retour.setToolTipText("Mettre ce bon en attente");
    OBJ_129.setText("");
    if (etatBon.equals("C")) {
      OBJ_129.setText("Comptabilisé");
    }
    else if (etatBon.equals("T")) {
      OBJ_129.setText("Commande type");
    }
    else if (etatBon.equals("P")) {
      OBJ_129.setText("Positionnement");
    }
    else if (etatBon.equals("A")) {
      OBJ_129.setText("Attente");
    }
    else if (etatBon.equals("E")) {
      OBJ_129.setText("Expédié");
    }
    else if (etatBon.equals("H")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("R")) {
      OBJ_129.setText("Réservé");
    }
    else if (etatBon.equals("D")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("c")) {
      OBJ_129.setText("Clôturé");
    }
    else if (etatBon.equals("p")) {
      OBJ_129.setText("Perdu");
    }
    else if (etatBon.equals("d")) {
      OBJ_129.setText("Dépassé");
    }
    else if (etatBon.equalsIgnoreCase("s")) {
      OBJ_129.setText("Signé");
    }
    else if (etatBon.equals("e")) {
      OBJ_129.setText("Envoyé");
    }
    else if (etatBon.equals("V")) {
      OBJ_129.setText("Validé");
    }
    else if (etatBon.equals("F")) {
      OBJ_129.setText("Facturé");
      bouton_retour.setToolTipText("Mettre cette facture en attente");
    }
    
    // Panel des réglements
    lib_Devise.setVisible(lexique.isTrue("N86"));
    
    // Chantier
    if (isChantier) {
      lbDateValidite.setText("Date validité fin");
      lbDateLivraisonSouhaitee.setText("Date validité début");
      lbDateLivraisonPrevue.setVisible(false);
      riBoutonDetail2.setVisible(false);
      E1DLPX.setVisible(false);
      E1PFC.setVisible(false);
      E1PRE.setVisible(false);
      lbTypeDevis.setVisible(false);
      lbEspoir.setVisible(false);
      lbReferenceLongue.setText("Chantier");
      E1DT2X.setBounds(180, 30, E1DT2X.getWidth(), E1DT2X.getHeight());
      E1DLSX.setBounds(180, 4, E1DLSX.getWidth(), E1DLSX.getHeight());
      lbDateRelanceDevis.setVisible(false);
      WDTREL.setVisible(false);
      if (!lexique.HostFieldGetData("WNUM").trim().isEmpty() && !lexique.HostFieldGetData("WNUM").equals("000000")) {
        btnAutresAdressesLivraison.setVisible(true);
      }
      else {
        btnAutresAdressesLivraison.setVisible(false);
      }
      lbBlanc.setVisible(true);
    }
    else {
      lbDateValidite.setText("Validité du devis");
      lbDateLivraisonSouhaitee.setText("Livraison souhaitée");
      lbDateLivraisonPrevue.setVisible(true);
      lbDateRelanceDevis.setVisible(true);
      WDTREL.setVisible(true);
      
      riBoutonDetail2.setVisible(true);
      E1DLPX.setVisible(true);
      lbReferenceLongue.setText("Référence longue");
      if (isProspect) {
        lbReferenceLongue.setText("Références prospect");
      }
      E1DT2X.setBounds(180, 4, E1DT2X.getWidth(), E1DT2X.getHeight());
      E1DLSX.setBounds(180, 30, E1DLSX.getWidth(), E1DLSX.getHeight());
      riSousMenu19.setEnabled(false);
      riSousMenu_bt19.setVisible(false);
      btnAutresAdressesLivraison.setVisible(false);
      lbBlanc.setVisible(false);
    }
    
    // Panel de l'encours
    pnlEncours.setVisible(lexique.isTrue("(N38) AND (N39)"));
    pnlTitreEncours.setVisible(pnlEncours.isVisible());
    WEPLF.setVisible(true);
    WEDEP.setVisible(true);
    WEPLF.setEnabled(false);
    WEDEP.setEnabled(false);
    
    // Bandeau du panel
    String e1in18 = lexique.HostFieldGetData("E1IN18").trim();
    if (e1in18.equalsIgnoreCase("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (e1in18.equalsIgnoreCase("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    
    // Informations de livraison : pour les chantiers uniquement
    memoLivraisonEnlevement = null;
    if (isChantier) {
      // Pas d'informations en création car le GAP n'a pas encore attribué de numéro de document !
      if (chargerIdDocumentVenteEnCours() == null) {
        taInformationLivraisonEnlevement.setText("Non saisissable à ce stade. Validez d'abord l'entête du chantier.");
        taInformationLivraisonEnlevement.setEnabled(false);
      }
      else {
        taInformationLivraisonEnlevement.setEnabled(true);
        taInformationLivraisonEnlevement.setText(chargerInformationsLivraison());
      }
    }
    else {
      if (tbpInfos.indexOfComponent(pnlInfosLivraison) >= 0) {
        tbpInfos.remove(pnlInfosLivraison);
      }
    }
    
    // Client facturé
    snClientFac.setSession(getSession());
    snClientFac.setIdEtablissement(idEtablissement);
    if (lexique.HostFieldGetData("E1CLFP").trim().isEmpty()) {
      snClientFac.charger(false);
    }
    else {
      int numero = Integer.parseInt(lexique.HostFieldGetData("E1CLFP").trim());
      int suffixe = 0;
      if (!lexique.HostFieldGetData("E1CLFS").trim().isEmpty()) {
        suffixe = Integer.parseInt(lexique.HostFieldGetData("E1CLFS").trim());
      }
      snClientFac.forcerRechercheParID(IdClient.getInstance(idEtablissement, numero, suffixe));
    }
    snClientFac.setSelectionParChampRPG(lexique, "E1CLFP", "E1CLFS");
    if (snClientFac.getSelection() != null && snClientFac.getSelection().isParticulier()) {
      lbNom.setText("Nom");
      lbPrenom.setText("Prénom");
    }
    else {
      lbNom.setText("Raison sociale");
      lbPrenom.setText("Complément");
    }
    snClientFac.setEnabled(false);
    
    // Client livré
    snClientLiv.setSession(getSession());
    snClientLiv.setIdEtablissement(idEtablissement);
    if (lexique.HostFieldGetData("E1CLLP").trim().isEmpty()) {
      snClientLiv.charger(false);
    }
    else {
      int numero = Integer.parseInt(lexique.HostFieldGetData("E1CLLP").trim());
      int suffixe = 0;
      if (!lexique.HostFieldGetData("E1CLLS").trim().isEmpty()) {
        suffixe = Integer.parseInt(lexique.HostFieldGetData("E1CLLS").trim());
      }
      snClientLiv.forcerRechercheParID(IdClient.getInstance(idEtablissement, numero, suffixe));
    }
    snClientLiv.setSelectionParChampRPG(lexique, "E1CLLP", "E1CLLS");
    if (snClientLiv.getSelection() != null && snClientLiv.getSelection().isParticulier()) {
      lbNom2.setText("Nom");
      lbPrenom2.setText("Prénom");
    }
    else {
      lbNom2.setText("Raison sociale");
      lbPrenom2.setText("Complément");
    }
    snClientLiv.setEnabled(false);
    
    // Commune client facturé
    snCodePostalCommuneFac.setSession(getSession());
    snCodePostalCommuneFac.setIdEtablissement(idEtablissement);
    snCodePostalCommuneFac.charger(false);
    snCodePostalCommuneFac.setSelectionParChampRPG(lexique, "E3CDPX", "E3VILN");
    
    // Commune client livré
    snCodePostalCommuneLiv.setSession(getSession());
    snCodePostalCommuneLiv.setIdEtablissement(idEtablissement);
    snCodePostalCommuneLiv.charger(false);
    snCodePostalCommuneLiv.setSelectionParChampRPG(lexique, "E2CDPX", "E2VILN");
    
    // Pays client facturé
    snPaysFac.setSession(getSession());
    snPaysFac.setIdEtablissement(idEtablissement);
    snPaysFac.charger(false);
    snPaysFac.setSelectionParChampRPG(lexique, "E3COP");
    
    // Pays client livré
    snPaysLiv.setSession(getSession());
    snPaysLiv.setIdEtablissement(idEtablissement);
    snPaysLiv.charger(false);
    snPaysLiv.setSelectionParChampRPG(lexique, "E2COP");
    
    // Section analytique
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(idEtablissement);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "E1SAN");
    
    // Affaire analytique
    snAffaire.setSession(getSession());
    snAffaire.setIdEtablissement(idEtablissement);
    snAffaire.charger(false);
    snAffaire.setSelectionParChampRPG(lexique, "E1ACT");
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "E1MAG");
    
    // Représentant
    snRepresentant.setSession(getSession());
    snRepresentant.setIdEtablissement(idEtablissement);
    snRepresentant.charger(false);
    snRepresentant.setSelectionParChampRPG(lexique, "E1REP");
    
    // Mode d'expédition
    snModeExpedition.setSession(getSession());
    snModeExpedition.setIdEtablissement(idEtablissement);
    snModeExpedition.charger(false);
    snModeExpedition.setSelectionParChampRPG(lexique, "E1MEX");
    
    // Transporteur
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(idEtablissement);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "E1CTR");
    
    // Zone géographique de livraison
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(idEtablissement);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "E1ZTR");
    
    // Devise
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(idEtablissement);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "E1DEV");
    snDevise.setVisible(lexique.isPresent("E1DEV"));
    lbDevise.setVisible(lexique.isPresent("E1DEV"));
    
    // Contact
    snContact.setSession(getSession());
    snContact.setIdEtablissement(idEtablissement);
    snContact.setTypeContact(EnumTypeContact.CLIENT);
    if (pnlAdresses.getSelectedIndex() == 0) {
      snContact.setIdClient(snClientFac.getIdSelection());
    }
    else {
      snContact.setIdClient(snClientLiv.getIdSelection());
    }
    snContact.charger(false, false);
    int numeroContact = 0;
    if (!lexique.HostFieldGetData("LRENUM").trim().isEmpty()) {
      numeroContact = Integer.parseInt(lexique.HostFieldGetData("LRENUM"));
    }
    snContact.setSelectionParId(IdContact.getInstance(numeroContact));
    
    // E-mail
    snAdresseMail.setText(lexique.HostFieldGetData("LMAIL").trim());
    snAdresseMail.setEnabled(false);
    
    // Condition
    snConditionVenteEtClient.setSession(getSession());
    snConditionVenteEtClient.setIdEtablissement(idEtablissement);
    snConditionVenteEtClient.setSelectionParChampRPG(lexique, "E1CNV");
    
    // Barre d'entête
    p_bpresentation.setText(p_bpresentation.getText().replaceAll(":", "").trim() + libelleDirectOuInterne);
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snClientFac.renseignerChampRPG(lexique, "E2CDPX", "E2VILN");
    snClientLiv.renseignerChampRPG(lexique, "E1CLLP", "E1CLLS");
    snCodePostalCommuneFac.renseignerChampRPG(lexique, "E3CDPX", "E3VILN", false);
    snCodePostalCommuneLiv.renseignerChampRPG(lexique, "E2CDPX", "E2VILN", false);
    snPaysFac.renseignerChampRPG(lexique, "E3COP", "E3PAYN");
    snPaysLiv.renseignerChampRPG(lexique, "E2COP", "E2PAYN");
    snSectionAnalytique.renseignerChampRPG(lexique, "E1SAN");
    snAffaire.renseignerChampRPG(lexique, "E1ACT");
    snMagasin.renseignerChampRPG(lexique, "E1MAG");
    snRepresentant.renseignerChampRPG(lexique, "E1REP");
    snModeExpedition.renseignerChampRPG(lexique, "E1MEX");
    snTransporteur.renseignerChampRPG(lexique, "E1CTR");
    snZoneGeographique.renseignerChampRPG(lexique, "E1ZTR");
    snDevise.renseignerChampRPG(lexique, "E1DEV");
    if (snContact.getSelection() != null) {
      lexique.HostFieldPutData("LRENUM", 0, snContact.getSelection().getId().getCode() + "");
    }
    snConditionVenteEtClient.renseignerChampRPG(lexique, "E1CNV");
    // Informations de livraison pour les chantiers seulement. Pas d'informations en création car le GAP n'a pas encore attribué de numéro
    // de document !
    if (isChantier && chargerIdDocumentVenteEnCours() != null) {
      sauverInformationsLivraison(taInformationLivraisonEnlevement.getText());
    }
    
    // Mode Test Calcul prix en mode débug uniquement
    if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
      try {
        if (snClientFac.getSelection() != null) {
          lexique.addVariableGlobaleFromSession("idMagasin", snMagasin.getSelection().getId());
          lexique.addVariableGlobaleFromSession("idClient", snClientFac.getSelection().getId());
          lexique.addVariableGlobaleFromSession("affichageTTC", snClientFac.getSelection().isFactureEnTTC());
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "Problème avec l'initialisation du client pour le calcul prix de vente");
      }
    }
  }
  
  /**
   * Modification des informations de livraison
   */
  private void sauverInformationsLivraison(String pTexte) {
    if (chargerIdDocumentVenteEnCours() == null) {
      return;
    }
    // Créer le mémo s'il n'existe pas
    if (memoLivraisonEnlevement == null) {
      IdMemo idMemo = IdMemo.getInstancePourDocumentVente(chargerIdDocumentVenteEnCours(), 1);
      memoLivraisonEnlevement = new Memo(idMemo);
      memoLivraisonEnlevement.setType(EnumTypeMemo.LIVRAISON);
    }
    
    // Modifier le texte
    if (pTexte != null) {
      memoLivraisonEnlevement.setTexte(pTexte);
    }
    else {
      // Modifier le texte
      memoLivraisonEnlevement.setTexte("");
    }
    memoLivraisonEnlevement.sauver(getIdSession());
  }
  
  /**
   * Lecture des informations de livraison
   */
  private String chargerInformationsLivraison() {
    if (chargerIdDocumentVenteEnCours() == null) {
      return "";
    }
    
    memoLivraisonEnlevement = Memo.charger(getIdSession(), chargerIdDocumentVenteEnCours(), EnumTypeMemo.LIVRAISON);
    if (memoLivraisonEnlevement == null) {
      return "";
    }
    return memoLivraisonEnlevement.getTexte();
  }
  
  /**
   * Création d'une IdDocumentVente correspondant au document en cours à partir des informations de l'écran RPG
   */
  private IdDocumentVente chargerIdDocumentVenteEnCours() {
    if (lexique.HostFieldGetData("WNUM").trim().isEmpty() || lexique.HostFieldGetData("WNUM").equalsIgnoreCase("000000")) {
      return null;
    }
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    int numeroDocument = Integer.parseInt(lexique.HostFieldGetData("WNUM"));
    int suffixeDocument = 0;
    if (!lexique.HostFieldGetData("WSUF").trim().isEmpty()) {
      suffixeDocument = Integer.parseInt(lexique.HostFieldGetData("WSUF"));
    }
    IdDocumentVente idDocument =
        IdDocumentVente.getInstanceGenerique(idEtablissement, EnumCodeEnteteDocumentVente.DEVIS, numeroDocument, suffixeDocument, 0);
    return idDocument;
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 5);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 90);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riBoutonDetail3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1REP");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_encoursActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 80);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_reglementActionPerformed(ActionEvent e) {
    if (dialog_REGL == null) {
      dialog_REGL = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_REGL(this));
    }
    dialog_REGL.affichePopupPerso();
  }
  
  private void bt_conditionActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 42);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 10);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riBoutonDetail8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("E1CLFP");
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riBoutonDet6ActionPerformed(ActionEvent e) {
    if (dialog_REGL == null) {
      dialog_REGL = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_REGL(this));
    }
    dialog_REGL.affichePopupPerso();
  }
  
  private void riBoutonDetail7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(5, 56);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 5);
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
    lexique.HostCursorPut("E1RG2");
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut(21, 82);
      lexique.HostScreenSendKey(this, "F2");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btnAutresAdressesLivraisonActionPerformed(ActionEvent e) {
    try {
      if (lexique.HostFieldGetData("WNUM").trim().isEmpty() || lexique.HostFieldGetData("WNUM").equalsIgnoreCase("000000")) {
        return;
      }
      IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
      int numeroDocument = Integer.parseInt(lexique.HostFieldGetData("WNUM"));
      int suffixeDocument = 0;
      if (!lexique.HostFieldGetData("WSUF").trim().isEmpty()) {
        suffixeDocument = Integer.parseInt(lexique.HostFieldGetData("WSUF"));
      }
      IdDocumentVente idDocument =
          IdDocumentVente.getInstanceGenerique(idEtablissement, EnumCodeEnteteDocumentVente.DEVIS, numeroDocument, suffixeDocument, 0);
      ModeleListeAdresseLivraisonChantier modeleAdresse =
          new ModeleListeAdresseLivraisonChantier(getSession(), idDocument, EnumCodeEnteteAdresseDocument.ADRESSE_LIVRAISON_DEVIS);
      VueListeAdresseLivraisonChantier vueAdresse = new VueListeAdresseLivraisonChantier(modeleAdresse);
      vueAdresse.afficher();
      if (modeleAdresse.isSortieAvecValidation()) {
        E2NOM.setText(modeleAdresse.getAdressePrincipale().getNom());
        E2CPL.setText(modeleAdresse.getAdressePrincipale().getComplementNom());
        E2RUE.setText(modeleAdresse.getAdressePrincipale().getRue());
        E2LOC.setText(modeleAdresse.getAdressePrincipale().getLocalisation());
        snCodePostalCommuneLiv.setSelection(CodePostalCommune.getInstance(modeleAdresse.getAdressePrincipale().getCodePostalFormate(),
            modeleAdresse.getAdressePrincipale().getVille()));
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snContactValueChanged(SNComposantEvent e) {
    try {
      if (snContact.getSelection() != null) {
        snAdresseMail.setText(snContact.getSelection().getEmail());
        LTEL.setText(snContact.getSelection().getNumeroTelephone1());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snPaysFacValueChanged(SNComposantEvent e) {
    try {
      // La recherche du composant est désactivée si le code pays correspond à un pays étranger
      boolean desactivationRecherche = false;
      if (snPaysFac.getSelection() != null && !snPaysFac.getSelection().isFrance()) {
        desactivationRecherche = true;
      }
      snCodePostalCommuneFac.setDesactivationRecherche(desactivationRecherche);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snPaysLivValueChanged(SNComposantEvent e) {
    try {
      // La recherche du composant est désactivée si le code pays correspond à un pays étranger
      boolean desactivationRecherche = false;
      if (snPaysLiv.getSelection() != null && !snPaysLiv.getSelection().isFrance()) {
        desactivationRecherche = true;
      }
      snCodePostalCommuneLiv.setDesactivationRecherche(desactivationRecherche);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void pnlAdressesStateChanged(ChangeEvent e) {
    try {
      if (pnlAdresses.getSelectedIndex() == 0) {
        snContact.setIdClient(snClientFac.getIdSelection());
      }
      else {
        snContact.setIdClient(snClientLiv.getIdSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WNUM = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    WSUF = new RiZoneSortie();
    OBJ_52 = new JLabel();
    E1VDE = new XRiTextField();
    E1NFA = new RiZoneSortie();
    OBJ_50 = new JLabel();
    p_tete_droite = new JPanel();
    bt_ecran = new JLabel();
    OBJ_129 = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    p_contenu2 = new SNPanelContenu();
    pnlAdresses = new JTabbedPane();
    pnlAdresseFacturation = new SNPanelContenu();
    lbBlanc = new SNLabelChamp();
    lbClient = new SNLabelChamp();
    snClientFac = new SNClient();
    lbNom = new SNLabelChamp();
    E3NOM = new XRiTextField();
    lbPrenom = new SNLabelChamp();
    E3CPL = new XRiTextField();
    lbAdresse1 = new SNLabelChamp();
    E3RUE = new XRiTextField();
    lbAdresse2 = new SNLabelChamp();
    E3LOC = new XRiTextField();
    lbCommune = new SNLabelChamp();
    snCodePostalCommuneFac = new SNCodePostalCommune();
    lbPays = new SNLabelChamp();
    snPaysFac = new SNPays();
    lbTelephone = new SNLabelChamp();
    E3TEL = new XRiTextField();
    pnlAdressesLivraison = new SNPanelContenu();
    btnAutresAdressesLivraison = new SNBoutonDetail();
    lbClient2 = new SNLabelChamp();
    snClientLiv = new SNClient();
    lbNom2 = new SNLabelChamp();
    E2NOM = new XRiTextField();
    lbPrenom2 = new SNLabelChamp();
    E2CPL = new XRiTextField();
    lbAdresse3 = new SNLabelChamp();
    E2RUE = new XRiTextField();
    lbAdresse4 = new SNLabelChamp();
    E2LOC = new XRiTextField();
    lbCommune2 = new SNLabelChamp();
    snCodePostalCommuneLiv = new SNCodePostalCommune();
    lbPays2 = new SNLabelChamp();
    snPaysLiv = new SNPays();
    lbTelephone2 = new SNLabelChamp();
    E2TEL = new XRiTextField();
    pnlGrilleInfos = new SNPanel();
    tbpInfos = new JTabbedPane();
    pnlInfos = new SNPanelContenu();
    panel4 = new JPanel();
    lbAttention = new SNLabelChamp();
    WATN = new XRiTextField();
    E1NHO = new XRiCheckBox();
    lbReferenceCourte = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    E1NCC = new XRiTextField();
    lbReferenceLongue = new SNLabelChamp();
    E1RCC = new XRiTextField();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbRepresentant = new SNLabelChamp();
    snRepresentant = new SNRepresentant();
    lbObservation = new SNLabelChamp();
    WOBS = new XRiTextField();
    pnlInfo2 = new SNPanelContenu();
    lbTypePrix = new SNLabelChamp();
    E1PGC = new XRiComboBox();
    lbTypeDevis = new SNLabelChamp();
    E1PFC = new XRiTextField();
    lbEspoir = new SNLabelChamp();
    E1PRE = new XRiTextField();
    lbCommandeInitiale = new SNLabelChamp();
    E1CCT = new XRiTextField();
    lbPoids = new SNLabelChamp();
    pnlPoids = new SNPanel();
    E1PDS = new XRiTextField();
    lbColis = new SNLabelChamp();
    E1COL = new XRiTextField();
    lbMotif = new SNLabelChamp();
    E1VEH = new XRiTextField();
    pnlAnalytique = new SNPanelContenu();
    pnlAnal = new SNPanel();
    lbAffaire = new SNLabelChamp();
    snAffaire = new SNAffaire();
    lbSection = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbNature = new SNLabelChamp();
    E20NAT = new XRiTextField();
    sNLabelChamp2 = new SNLabelChamp();
    E1CAN = new XRiTextField();
    sNPanel5 = new SNPanel();
    lbCan1 = new SNLabelChamp();
    E1TP1 = new XRiTextField();
    lbCan3 = new SNLabelChamp();
    E1TP3 = new XRiTextField();
    lbCan5 = new SNLabelChamp();
    E1TP5 = new XRiTextField();
    lbCan2 = new SNLabelChamp();
    E1TP2 = new XRiTextField();
    lbCan4 = new SNLabelChamp();
    E1TP4 = new XRiTextField();
    pnlInfosLivraison = new SNPanelContenu();
    taInformationLivraisonEnlevement = new RiTextArea();
    lbTitreContact = new SNLabelTitre();
    pnlContact = new SNPanel();
    lbContact = new SNLabelChamp();
    snContact = new SNContact();
    lbTelContact = new SNLabelChamp();
    LTEL = new SNTexte();
    lbMailContact = new SNLabelChamp();
    snAdresseMail = new SNAdresseMail();
    pnlGrilleDateEEtEncours = new SNPanel();
    lbTitreDates = new SNLabelTitre();
    pnlTitreEncours = new SNPanel();
    bt_encours = new SNBoutonDetail();
    lbEncours = new SNLabelTitre();
    xTitledPanel5 = new SNPanel();
    lbDateLivraisonSouhaitee = new SNLabelChamp();
    E1DLPX = new XRiCalendrier();
    lbDateLivraisonPrevue = new SNLabelChamp();
    E1DLSX = new XRiCalendrier();
    lbDateValidite = new SNLabelChamp();
    E1DT2X = new XRiCalendrier();
    lbDateRelanceDevis = new SNLabelChamp();
    WDTREL = new XRiCalendrier();
    pnlEncours = new SNPanel();
    lbPosition = new SNLabelChamp();
    WEPOS = new XRiTextField();
    lbPlafond = new SNLabelChamp();
    WEPLF = new XRiTextField();
    lbDepassement = new SNLabelChamp();
    WEDEP = new XRiTextField();
    lbTitreExpedition = new SNLabelTitre();
    p_exped = new SNPanel();
    lbModeExpedition = new SNLabelChamp();
    snModeExpedition = new SNModeExpedition();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    lbFranco = new SNLabelChamp();
    sNPanel4 = new SNPanel();
    WMFRP = new XRiTextField();
    E1IN9 = new XRiCheckBox();
    lbZone = new SNLabelChamp();
    sNPanel6 = new SNPanel();
    snZoneGeographique = new SNZoneGeographique();
    pnlTitreConditions = new SNPanel();
    bt_condition = new SNBoutonDetail();
    lbConditions = new SNLabelTitre();
    pnlTitreReglement = new SNPanel();
    bt_reglement = new SNBoutonDetail();
    lbReglements = new SNLabelTitre();
    pnlConditions = new SNPanel();
    lbRemises = new SNLabelUnite();
    lbTarif = new SNLabelUnite();
    lbCondition = new SNLabelUnite();
    lbPromo = new SNLabelUnite();
    lbEscompte = new SNLabelUnite();
    pnlRemises = new SNPanel();
    E1REM1 = new XRiTextField();
    E1REM2 = new XRiTextField();
    E1REM3 = new XRiTextField();
    E1TAR = new XRiTextField();
    snConditionVenteEtClient = new SNConditionVenteEtClient();
    WCNP = new XRiTextField();
    E1ESCX = new XRiTextField();
    pnlReglement = new SNPanel();
    lbPosition2 = new SNLabelChamp();
    E1RG2 = new XRiTextField();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    button1 = new JButton();
    OBJ_102 = new JLabel();
    riBoutonDetail1 = new SNBoutonDetail();
    riBoutonDetail2 = new SNBoutonDetail();
    lib_Devise = new RiZoneSortie();
    lbPlafond2 = new SNLabelChamp();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1200, 670));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@     @WLIEN@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(800, 32));
          p_tete_gauche.setMinimumSize(new Dimension(1000, 32));
          p_tete_gauche.setMaximumSize(new Dimension(1160, 28));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setText("@WNUM@");
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");
          
          // ---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          
          // ---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");
          
          // ---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro");
          OBJ_49.setName("OBJ_49");
          
          // ---- WSUF ----
          WSUF.setComponentPopupMenu(BTD);
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          WSUF.setName("WSUF");
          
          // ---- OBJ_52 ----
          OBJ_52.setText("Vendeur");
          OBJ_52.setName("OBJ_52");
          
          // ---- E1VDE ----
          E1VDE.setComponentPopupMenu(BTD);
          E1VDE.setName("E1VDE");
          
          // ---- E1NFA ----
          E1NFA.setOpaque(false);
          E1NFA.setText("@E1NFA@");
          E1NFA.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NFA.setName("E1NFA");
          
          // ---- OBJ_50 ----
          OBJ_50.setText("Facture");
          OBJ_50.setName("OBJ_50");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WNUM, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(WSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1NFA, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                  .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(400, 400, 400)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 19,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1ETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 19,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WNUM, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WSUF, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(E1NFA, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(E1VDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- bt_ecran ----
          bt_ecran.setBorder(BorderFactory.createEmptyBorder());
          bt_ecran.setToolTipText("Passage \u00e0 l'affichage simplifi\u00e9");
          bt_ecran.setPreferredSize(new Dimension(30, 30));
          bt_ecran.setName("bt_ecran");
          bt_ecran.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              bt_ecranMouseClicked(e);
            }
          });
          p_tete_droite.add(bt_ecran);
          
          // ---- OBJ_129 ----
          OBJ_129.setOpaque(false);
          OBJ_129.setBorder(null);
          OBJ_129.setText("obj_129");
          OBJ_129.setFont(OBJ_129.getFont().deriveFont(OBJ_129.getFont().getStyle() | Font.BOLD, OBJ_129.getFont().getSize() + 3f));
          OBJ_129.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_129.setHorizontalTextPosition(SwingConstants.LEADING);
          OBJ_129.setName("OBJ_129");
          p_tete_droite.add(OBJ_129);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Acc\u00e8s lignes");
            bouton_valider.setToolTipText("Acc\u00e8s lignes");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Sortir");
            bouton_retour.setToolTipText("Sortir");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Bloc-notes du devis");
              riSousMenu_bt14.setToolTipText("Bloc-notes du devis : saisie libre de notes \u00e0 propos de ce devis");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes client");
              riSousMenu_bt15
                  .setToolTipText("bloc-notes du client : saisie libre de notes \u00e0 propos du client factur\u00e9 de ce devis");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Acc\u00e9s compte");
              riSousMenu_bt6.setToolTipText(
                  "Acc\u00e9s compte : acc\u00e8s en visualisation en comptabilit\u00e9 du compte de tiers correspondant au client");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique devis");
              riSousMenu_bt7.setToolTipText("Historique devis : historique des modifications apport\u00e9es \u00e0 ce devis");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Options clients");
              riSousMenu_bt8
                  .setToolTipText("Options clients : fonctionnalit\u00e9s li\u00e9es au client pour lequel est \u00e9tabli ce devis");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Documents li\u00e9s");
              riSousMenu_bt9.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Stock palette chantier");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt17ActionPerformed(e);
                }
              });
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Valider le chantier");
              riSousMenu_bt19.setToolTipText("Valider le chantier");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
            
            // ======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("Navigation");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setMinimumSize(new Dimension(104, 260));
              riSousMenu1.setMaximumSize(new Dimension(104, 260));
              riSousMenu1.setPreferredSize(new Dimension(170, 260));
              riSousMenu1.setName("riSousMenu1");
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu2 ========
      {
        p_contenu2.setPreferredSize(new Dimension(830, 510));
        p_contenu2.setBorder(new EmptyBorder(10, 10, 10, 10));
        p_contenu2.setBackground(new Color(239, 239, 222));
        p_contenu2.setMinimumSize(new Dimension(800, 640));
        p_contenu2.setOpaque(true);
        p_contenu2.setName("p_contenu2");
        p_contenu2.setLayout(new GridBagLayout());
        ((GridBagLayout) p_contenu2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) p_contenu2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) p_contenu2.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) p_contenu2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlAdresses ========
        {
          pnlAdresses.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlAdresses.setBorder(new DropShadowBorder());
          pnlAdresses.setMinimumSize(new Dimension(550, 340));
          pnlAdresses.setPreferredSize(new Dimension(550, 340));
          pnlAdresses.setName("pnlAdresses");
          pnlAdresses.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
              pnlAdressesStateChanged(e);
            }
          });
          
          // ======== pnlAdresseFacturation ========
          {
            pnlAdresseFacturation.setMinimumSize(new Dimension(625, 270));
            pnlAdresseFacturation.setPreferredSize(new Dimension(625, 270));
            pnlAdresseFacturation.setName("pnlAdresseFacturation");
            pnlAdresseFacturation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAdresseFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlAdresseFacturation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlAdresseFacturation.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlAdresseFacturation.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbBlanc ----
            lbBlanc.setMaximumSize(new Dimension(150, 16));
            lbBlanc.setMinimumSize(new Dimension(150, 16));
            lbBlanc.setPreferredSize(new Dimension(150, 16));
            lbBlanc.setName("lbBlanc");
            pnlAdresseFacturation.add(lbBlanc, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbClient ----
            lbClient.setText("Client");
            lbClient.setMinimumSize(new Dimension(100, 30));
            lbClient.setPreferredSize(new Dimension(100, 30));
            lbClient.setName("lbClient");
            pnlAdresseFacturation.add(lbClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snClientFac ----
            snClientFac.setName("snClientFac");
            pnlAdresseFacturation.add(snClientFac, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbNom ----
            lbNom.setText("Raison sociale");
            lbNom.setMinimumSize(new Dimension(100, 30));
            lbNom.setPreferredSize(new Dimension(100, 30));
            lbNom.setName("lbNom");
            pnlAdresseFacturation.add(lbNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E3NOM ----
            E3NOM.setComponentPopupMenu(BTD);
            E3NOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3NOM.setPreferredSize(new Dimension(315, 30));
            E3NOM.setMinimumSize(new Dimension(315, 30));
            E3NOM.setName("E3NOM");
            pnlAdresseFacturation.add(E3NOM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbPrenom ----
            lbPrenom.setText("Compl\u00e9ment");
            lbPrenom.setMinimumSize(new Dimension(100, 30));
            lbPrenom.setPreferredSize(new Dimension(100, 30));
            lbPrenom.setName("lbPrenom");
            pnlAdresseFacturation.add(lbPrenom, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E3CPL ----
            E3CPL.setComponentPopupMenu(BTD);
            E3CPL.setMinimumSize(new Dimension(315, 30));
            E3CPL.setPreferredSize(new Dimension(315, 30));
            E3CPL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3CPL.setName("E3CPL");
            pnlAdresseFacturation.add(E3CPL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbAdresse1 ----
            lbAdresse1.setText("Adresse 1");
            lbAdresse1.setMinimumSize(new Dimension(100, 30));
            lbAdresse1.setPreferredSize(new Dimension(100, 30));
            lbAdresse1.setName("lbAdresse1");
            pnlAdresseFacturation.add(lbAdresse1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E3RUE ----
            E3RUE.setComponentPopupMenu(BTD);
            E3RUE.setMinimumSize(new Dimension(315, 30));
            E3RUE.setPreferredSize(new Dimension(315, 30));
            E3RUE.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3RUE.setName("E3RUE");
            pnlAdresseFacturation.add(E3RUE, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbAdresse2 ----
            lbAdresse2.setText("Adresse 2");
            lbAdresse2.setMinimumSize(new Dimension(100, 30));
            lbAdresse2.setPreferredSize(new Dimension(100, 30));
            lbAdresse2.setName("lbAdresse2");
            pnlAdresseFacturation.add(lbAdresse2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E3LOC ----
            E3LOC.setComponentPopupMenu(BTD);
            E3LOC.setMinimumSize(new Dimension(315, 30));
            E3LOC.setPreferredSize(new Dimension(315, 30));
            E3LOC.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3LOC.setName("E3LOC");
            pnlAdresseFacturation.add(E3LOC, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbCommune ----
            lbCommune.setText("Commune");
            lbCommune.setMinimumSize(new Dimension(100, 30));
            lbCommune.setPreferredSize(new Dimension(100, 30));
            lbCommune.setName("lbCommune");
            pnlAdresseFacturation.add(lbCommune, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snCodePostalCommuneFac ----
            snCodePostalCommuneFac.setName("snCodePostalCommuneFac");
            pnlAdresseFacturation.add(snCodePostalCommuneFac, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbPays ----
            lbPays.setText("Pays");
            lbPays.setMinimumSize(new Dimension(100, 30));
            lbPays.setPreferredSize(new Dimension(100, 30));
            lbPays.setName("lbPays");
            pnlAdresseFacturation.add(lbPays, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snPaysFac ----
            snPaysFac.setName("snPaysFac");
            snPaysFac.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPaysFacValueChanged(e);
              }
            });
            pnlAdresseFacturation.add(snPaysFac, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbTelephone ----
            lbTelephone.setText("T\u00e9l\u00e9phone");
            lbTelephone.setMinimumSize(new Dimension(100, 30));
            lbTelephone.setPreferredSize(new Dimension(100, 30));
            lbTelephone.setName("lbTelephone");
            pnlAdresseFacturation.add(lbTelephone, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E3TEL ----
            E3TEL.setComponentPopupMenu(BTD);
            E3TEL.setMinimumSize(new Dimension(150, 30));
            E3TEL.setPreferredSize(new Dimension(150, 30));
            E3TEL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E3TEL.setName("E3TEL");
            pnlAdresseFacturation.add(E3TEL, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlAdresses.addTab("Adresse de facturation", pnlAdresseFacturation);
          
          // ======== pnlAdressesLivraison ========
          {
            pnlAdressesLivraison.setPreferredSize(new Dimension(600, 300));
            pnlAdressesLivraison.setMinimumSize(new Dimension(600, 300));
            pnlAdressesLivraison.setName("pnlAdressesLivraison");
            pnlAdressesLivraison.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlAdressesLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlAdressesLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlAdressesLivraison.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlAdressesLivraison.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- btnAutresAdressesLivraison ----
            btnAutresAdressesLivraison.setToolTipText("Autres adresses de livraison");
            btnAutresAdressesLivraison.setName("btnAutresAdressesLivraison");
            btnAutresAdressesLivraison.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnAutresAdressesLivraisonActionPerformed(e);
              }
            });
            pnlAdressesLivraison.add(btnAutresAdressesLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbClient2 ----
            lbClient2.setText("Client");
            lbClient2.setMinimumSize(new Dimension(100, 30));
            lbClient2.setPreferredSize(new Dimension(100, 30));
            lbClient2.setName("lbClient2");
            pnlAdressesLivraison.add(lbClient2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snClientLiv ----
            snClientLiv.setName("snClientLiv");
            pnlAdressesLivraison.add(snClientLiv, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbNom2 ----
            lbNom2.setText("Raison sociale");
            lbNom2.setMinimumSize(new Dimension(100, 30));
            lbNom2.setPreferredSize(new Dimension(100, 30));
            lbNom2.setName("lbNom2");
            pnlAdressesLivraison.add(lbNom2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E2NOM ----
            E2NOM.setComponentPopupMenu(BTD);
            E2NOM.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2NOM.setPreferredSize(new Dimension(315, 30));
            E2NOM.setMinimumSize(new Dimension(315, 30));
            E2NOM.setName("E2NOM");
            pnlAdressesLivraison.add(E2NOM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbPrenom2 ----
            lbPrenom2.setText("Compl\u00e9ment");
            lbPrenom2.setMinimumSize(new Dimension(100, 30));
            lbPrenom2.setPreferredSize(new Dimension(100, 30));
            lbPrenom2.setName("lbPrenom2");
            pnlAdressesLivraison.add(lbPrenom2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E2CPL ----
            E2CPL.setComponentPopupMenu(BTD);
            E2CPL.setMinimumSize(new Dimension(315, 30));
            E2CPL.setPreferredSize(new Dimension(315, 30));
            E2CPL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2CPL.setName("E2CPL");
            pnlAdressesLivraison.add(E2CPL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbAdresse3 ----
            lbAdresse3.setText("Adresse 1");
            lbAdresse3.setMinimumSize(new Dimension(100, 30));
            lbAdresse3.setPreferredSize(new Dimension(100, 30));
            lbAdresse3.setName("lbAdresse3");
            pnlAdressesLivraison.add(lbAdresse3, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E2RUE ----
            E2RUE.setComponentPopupMenu(BTD);
            E2RUE.setMinimumSize(new Dimension(315, 30));
            E2RUE.setPreferredSize(new Dimension(315, 30));
            E2RUE.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2RUE.setName("E2RUE");
            pnlAdressesLivraison.add(E2RUE, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbAdresse4 ----
            lbAdresse4.setText("Adresse 2");
            lbAdresse4.setMinimumSize(new Dimension(100, 30));
            lbAdresse4.setPreferredSize(new Dimension(100, 30));
            lbAdresse4.setName("lbAdresse4");
            pnlAdressesLivraison.add(lbAdresse4, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E2LOC ----
            E2LOC.setComponentPopupMenu(BTD);
            E2LOC.setMinimumSize(new Dimension(315, 30));
            E2LOC.setPreferredSize(new Dimension(315, 30));
            E2LOC.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2LOC.setName("E2LOC");
            pnlAdressesLivraison.add(E2LOC, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbCommune2 ----
            lbCommune2.setText("Commune");
            lbCommune2.setMinimumSize(new Dimension(100, 30));
            lbCommune2.setPreferredSize(new Dimension(100, 30));
            lbCommune2.setName("lbCommune2");
            pnlAdressesLivraison.add(lbCommune2, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snCodePostalCommuneLiv ----
            snCodePostalCommuneLiv.setName("snCodePostalCommuneLiv");
            pnlAdressesLivraison.add(snCodePostalCommuneLiv, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbPays2 ----
            lbPays2.setText("Pays");
            lbPays2.setMinimumSize(new Dimension(100, 30));
            lbPays2.setPreferredSize(new Dimension(100, 30));
            lbPays2.setName("lbPays2");
            pnlAdressesLivraison.add(lbPays2, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snPaysLiv ----
            snPaysLiv.setName("snPaysLiv");
            snPaysLiv.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPaysLivValueChanged(e);
              }
            });
            pnlAdressesLivraison.add(snPaysLiv, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbTelephone2 ----
            lbTelephone2.setText("T\u00e9l\u00e9phone");
            lbTelephone2.setMinimumSize(new Dimension(100, 30));
            lbTelephone2.setPreferredSize(new Dimension(100, 30));
            lbTelephone2.setName("lbTelephone2");
            pnlAdressesLivraison.add(lbTelephone2, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E2TEL ----
            E2TEL.setComponentPopupMenu(BTD);
            E2TEL.setMinimumSize(new Dimension(150, 30));
            E2TEL.setPreferredSize(new Dimension(150, 30));
            E2TEL.setFont(new Font("sansserif", Font.PLAIN, 14));
            E2TEL.setName("E2TEL");
            pnlAdressesLivraison.add(E2TEL, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlAdresses.addTab("Adresses de livraison", pnlAdressesLivraison);
        }
        p_contenu2.add(pnlAdresses, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== pnlGrilleInfos ========
        {
          pnlGrilleInfos.setName("pnlGrilleInfos");
          pnlGrilleInfos.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGrilleInfos.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGrilleInfos.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGrilleInfos.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGrilleInfos.getLayout()).rowWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== tbpInfos ========
          {
            tbpInfos.setFont(new Font("sansserif", Font.PLAIN, 14));
            tbpInfos.setMinimumSize(new Dimension(445, 260));
            tbpInfos.setPreferredSize(new Dimension(445, 260));
            tbpInfos.setName("tbpInfos");
            
            // ======== pnlInfos ========
            {
              pnlInfos.setBackground(new Color(239, 239, 222));
              pnlInfos.setBorder(new EmptyBorder(10, 10, 10, 10));
              pnlInfos.setMinimumSize(new Dimension(396, 235));
              pnlInfos.setPreferredSize(new Dimension(396, 235));
              pnlInfos.setName("pnlInfos");
              pnlInfos.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInfos.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlInfos.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfos.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlInfos.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ======== panel4 ========
              {
                panel4.setOpaque(false);
                panel4.setName("panel4");
                panel4.setLayout(new GridBagLayout());
                ((GridBagLayout) panel4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) panel4.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) panel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) panel4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- lbAttention ----
                lbAttention.setText("Attention");
                lbAttention.setName("lbAttention");
                panel4.add(lbAttention, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- WATN ----
                WATN.setMinimumSize(new Dimension(150, 30));
                WATN.setPreferredSize(new Dimension(150, 30));
                WATN.setFont(new Font("sansserif", Font.PLAIN, 14));
                WATN.setName("WATN");
                panel4.add(WATN, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- E1NHO ----
                E1NHO.setText("Bloqu\u00e9");
                E1NHO.setFont(new Font("sansserif", Font.PLAIN, 14));
                E1NHO.setMinimumSize(new Dimension(66, 30));
                E1NHO.setPreferredSize(new Dimension(66, 30));
                E1NHO.setName("E1NHO");
                panel4.add(E1NHO, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlInfos.add(panel4, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbReferenceCourte ----
              lbReferenceCourte.setText("R\u00e9f\u00e9rence courte");
              lbReferenceCourte.setMaximumSize(new Dimension(130, 16));
              lbReferenceCourte.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbReferenceCourte.setName("lbReferenceCourte");
              pnlInfos.add(lbReferenceCourte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
              
              // ======== sNPanel1 ========
              {
                sNPanel1.setName("sNPanel1");
                sNPanel1.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- E1NCC ----
                E1NCC.setComponentPopupMenu(BTD);
                E1NCC.setPreferredSize(new Dimension(100, 30));
                E1NCC.setMinimumSize(new Dimension(100, 30));
                E1NCC.setFont(new Font("sansserif", Font.PLAIN, 14));
                E1NCC.setName("E1NCC");
                sNPanel1.add(E1NCC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlInfos.add(sNPanel1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbReferenceLongue ----
              lbReferenceLongue.setText("R\u00e9f\u00e9rence longue");
              lbReferenceLongue.setMaximumSize(new Dimension(130, 16));
              lbReferenceLongue.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbReferenceLongue.setName("lbReferenceLongue");
              pnlInfos.add(lbReferenceLongue, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- E1RCC ----
              E1RCC.setComponentPopupMenu(null);
              E1RCC.setPreferredSize(new Dimension(150, 30));
              E1RCC.setMinimumSize(new Dimension(150, 30));
              E1RCC.setFont(new Font("sansserif", Font.PLAIN, 14));
              E1RCC.setName("E1RCC");
              pnlInfos.add(E1RCC, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbMagasin ----
              lbMagasin.setText("Magasin");
              lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbMagasin.setName("lbMagasin");
              pnlInfos.add(lbMagasin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- snMagasin ----
              snMagasin.setPreferredSize(new Dimension(150, 30));
              snMagasin.setMinimumSize(new Dimension(150, 30));
              snMagasin.setName("snMagasin");
              pnlInfos.add(snMagasin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbRepresentant ----
              lbRepresentant.setText("Repr\u00e9sentant");
              lbRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbRepresentant.setName("lbRepresentant");
              pnlInfos.add(lbRepresentant, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- snRepresentant ----
              snRepresentant.setPreferredSize(new Dimension(150, 30));
              snRepresentant.setMinimumSize(new Dimension(150, 30));
              snRepresentant.setName("snRepresentant");
              pnlInfos.add(snRepresentant, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbObservation ----
              lbObservation.setText("Observation");
              lbObservation.setPreferredSize(new Dimension(110, 30));
              lbObservation.setMinimumSize(new Dimension(110, 30));
              lbObservation.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbObservation.setName("lbObservation");
              pnlInfos.add(lbObservation, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WOBS ----
              WOBS.setMinimumSize(new Dimension(150, 30));
              WOBS.setPreferredSize(new Dimension(150, 30));
              WOBS.setFont(new Font("sansserif", Font.PLAIN, 14));
              WOBS.setName("WOBS");
              pnlInfos.add(WOBS, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            tbpInfos.addTab("Informations principales", pnlInfos);
            
            // ======== pnlInfo2 ========
            {
              pnlInfo2.setName("pnlInfo2");
              pnlInfo2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInfo2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlInfo2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfo2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlInfo2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbTypePrix ----
              lbTypePrix.setText("Type de prix");
              lbTypePrix.setMaximumSize(new Dimension(130, 16));
              lbTypePrix.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbTypePrix.setName("lbTypePrix");
              pnlInfo2.add(lbTypePrix, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- E1PGC ----
              E1PGC.setModel(
                  new DefaultComboBoxModel(new String[] { "Prix garanti", "Prix non garanti", "Ancien prix", "Prix garanti / devis" }));
              E1PGC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              E1PGC.setPreferredSize(new Dimension(150, 30));
              E1PGC.setMinimumSize(new Dimension(150, 30));
              E1PGC.setFont(new Font("sansserif", Font.PLAIN, 14));
              E1PGC.setBackground(Color.white);
              E1PGC.setName("E1PGC");
              pnlInfo2.add(E1PGC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbTypeDevis ----
              lbTypeDevis.setText("Type de devis");
              lbTypeDevis.setName("lbTypeDevis");
              pnlInfo2.add(lbTypeDevis, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- E1PFC ----
              E1PFC.setComponentPopupMenu(BTD);
              E1PFC.setFont(new Font("sansserif", Font.PLAIN, 14));
              E1PFC.setPreferredSize(new Dimension(50, 30));
              E1PFC.setMinimumSize(new Dimension(50, 30));
              E1PFC.setName("E1PFC");
              pnlInfo2.add(E1PFC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbEspoir ----
              lbEspoir.setText("Espoir de r\u00e9alisation");
              lbEspoir.setName("lbEspoir");
              pnlInfo2.add(lbEspoir, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- E1PRE ----
              E1PRE.setComponentPopupMenu(BTD);
              E1PRE.setPreferredSize(new Dimension(50, 30));
              E1PRE.setMinimumSize(new Dimension(50, 30));
              E1PRE.setFont(new Font("sansserif", Font.PLAIN, 14));
              E1PRE.setName("E1PRE");
              pnlInfo2.add(E1PRE, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbCommandeInitiale ----
              lbCommandeInitiale.setText("Commande initiale");
              lbCommandeInitiale.setMaximumSize(new Dimension(120, 30));
              lbCommandeInitiale.setMinimumSize(new Dimension(120, 30));
              lbCommandeInitiale.setPreferredSize(new Dimension(120, 30));
              lbCommandeInitiale.setName("lbCommandeInitiale");
              pnlInfo2.add(lbCommandeInitiale, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
              
              // ---- E1CCT ----
              E1CCT.setComponentPopupMenu(BTD);
              E1CCT.setMinimumSize(new Dimension(70, 30));
              E1CCT.setPreferredSize(new Dimension(70, 30));
              E1CCT.setFont(new Font("sansserif", Font.PLAIN, 14));
              E1CCT.setName("E1CCT");
              pnlInfo2.add(E1CCT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbPoids ----
              lbPoids.setText("Poids total");
              lbPoids.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbPoids.setPreferredSize(new Dimension(80, 30));
              lbPoids.setMinimumSize(new Dimension(80, 30));
              lbPoids.setMaximumSize(new Dimension(80, 30));
              lbPoids.setName("lbPoids");
              pnlInfo2.add(lbPoids, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 5), 0, 0));
              
              // ======== pnlPoids ========
              {
                pnlPoids.setName("pnlPoids");
                pnlPoids.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPoids.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlPoids.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPoids.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPoids.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- E1PDS ----
                E1PDS.setMinimumSize(new Dimension(70, 30));
                E1PDS.setPreferredSize(new Dimension(70, 30));
                E1PDS.setFont(new Font("sansserif", Font.PLAIN, 14));
                E1PDS.setName("E1PDS");
                pnlPoids.add(E1PDS, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbColis ----
                lbColis.setText("Nombre de colis");
                lbColis.setFont(new Font("sansserif", Font.PLAIN, 14));
                lbColis.setPreferredSize(new Dimension(140, 30));
                lbColis.setMinimumSize(new Dimension(140, 30));
                lbColis.setName("lbColis");
                pnlPoids.add(lbColis, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- E1COL ----
                E1COL.setMinimumSize(new Dimension(50, 30));
                E1COL.setPreferredSize(new Dimension(50, 30));
                E1COL.setFont(new Font("sansserif", Font.PLAIN, 14));
                E1COL.setName("E1COL");
                pnlPoids.add(E1COL, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlInfo2.add(pnlPoids, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ---- lbMotif ----
              lbMotif.setText("Motif devis perdu");
              lbMotif.setName("lbMotif");
              pnlInfo2.add(lbMotif, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- E1VEH ----
              E1VEH.setComponentPopupMenu(BTD);
              E1VEH.setMinimumSize(new Dimension(70, 30));
              E1VEH.setPreferredSize(new Dimension(70, 30));
              E1VEH.setFont(new Font("sansserif", Font.PLAIN, 14));
              E1VEH.setName("E1VEH");
              pnlInfo2.add(E1VEH, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            tbpInfos.addTab("Autres informations", pnlInfo2);
            
            // ======== pnlAnalytique ========
            {
              pnlAnalytique.setName("pnlAnalytique");
              pnlAnalytique.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlAnalytique.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlAnalytique.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlAnalytique.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) pnlAnalytique.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlAnal ========
              {
                pnlAnal.setName("pnlAnal");
                pnlAnal.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlAnal.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlAnal.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlAnal.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlAnal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbAffaire ----
                lbAffaire.setText("Affaire");
                lbAffaire.setPreferredSize(new Dimension(80, 30));
                lbAffaire.setMinimumSize(new Dimension(80, 30));
                lbAffaire.setMaximumSize(new Dimension(80, 30));
                lbAffaire.setName("lbAffaire");
                pnlAnal.add(lbAffaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- snAffaire ----
                snAffaire.setName("snAffaire");
                pnlAnal.add(snAffaire, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 0), 0, 0));
                
                // ---- lbSection ----
                lbSection.setText("Section");
                lbSection.setPreferredSize(new Dimension(80, 30));
                lbSection.setMinimumSize(new Dimension(80, 30));
                lbSection.setMaximumSize(new Dimension(80, 30));
                lbSection.setName("lbSection");
                pnlAnal.add(lbSection, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- snSectionAnalytique ----
                snSectionAnalytique.setName("snSectionAnalytique");
                pnlAnal.add(snSectionAnalytique, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
                
                // ---- lbNature ----
                lbNature.setText("Nature");
                lbNature.setPreferredSize(new Dimension(80, 30));
                lbNature.setMinimumSize(new Dimension(80, 30));
                lbNature.setMaximumSize(new Dimension(80, 30));
                lbNature.setName("lbNature");
                pnlAnal.add(lbNature, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- E20NAT ----
                E20NAT.setComponentPopupMenu(BTD);
                E20NAT.setFont(new Font("sansserif", Font.PLAIN, 14));
                E20NAT.setMinimumSize(new Dimension(60, 30));
                E20NAT.setPreferredSize(new Dimension(60, 30));
                E20NAT.setName("E20NAT");
                pnlAnal.add(E20NAT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 2, 0), 0, 0));
                
                // ---- sNLabelChamp2 ----
                sNLabelChamp2.setText("Canal");
                sNLabelChamp2.setMaximumSize(new Dimension(80, 30));
                sNLabelChamp2.setMinimumSize(new Dimension(80, 30));
                sNLabelChamp2.setPreferredSize(new Dimension(80, 30));
                sNLabelChamp2.setName("sNLabelChamp2");
                pnlAnal.add(sNLabelChamp2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- E1CAN ----
                E1CAN.setPreferredSize(new Dimension(40, 30));
                E1CAN.setMinimumSize(new Dimension(40, 30));
                E1CAN.setName("E1CAN");
                pnlAnal.add(E1CAN, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlAnalytique.add(pnlAnal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 2, 0), 0, 0));
              
              // ======== sNPanel5 ========
              {
                sNPanel5.setName("sNPanel5");
                sNPanel5.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbCan1 ----
                lbCan1.setText("@TIZPE1@");
                lbCan1.setMaximumSize(new Dimension(80, 30));
                lbCan1.setMinimumSize(new Dimension(80, 30));
                lbCan1.setPreferredSize(new Dimension(80, 30));
                lbCan1.setName("lbCan1");
                sNPanel5.add(lbCan1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- E1TP1 ----
                E1TP1.setComponentPopupMenu(BTD);
                E1TP1.setPreferredSize(new Dimension(30, 30));
                E1TP1.setMinimumSize(new Dimension(30, 30));
                E1TP1.setName("E1TP1");
                sNPanel5.add(E1TP1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- lbCan3 ----
                lbCan3.setText("@TIZPE3@");
                lbCan3.setMaximumSize(new Dimension(60, 30));
                lbCan3.setMinimumSize(new Dimension(60, 30));
                lbCan3.setPreferredSize(new Dimension(60, 30));
                lbCan3.setName("lbCan3");
                sNPanel5.add(lbCan3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- E1TP3 ----
                E1TP3.setComponentPopupMenu(BTD);
                E1TP3.setPreferredSize(new Dimension(30, 30));
                E1TP3.setMinimumSize(new Dimension(30, 30));
                E1TP3.setName("E1TP3");
                sNPanel5.add(E1TP3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- lbCan5 ----
                lbCan5.setText("@TIZPE5@");
                lbCan5.setMaximumSize(new Dimension(100, 30));
                lbCan5.setMinimumSize(new Dimension(100, 30));
                lbCan5.setPreferredSize(new Dimension(100, 30));
                lbCan5.setName("lbCan5");
                sNPanel5.add(lbCan5, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 2, 5), 0, 0));
                
                // ---- E1TP5 ----
                E1TP5.setComponentPopupMenu(BTD);
                E1TP5.setPreferredSize(new Dimension(30, 30));
                E1TP5.setMinimumSize(new Dimension(30, 30));
                E1TP5.setName("E1TP5");
                sNPanel5.add(E1TP5, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 2, 0), 0, 0));
                
                // ---- lbCan2 ----
                lbCan2.setText("@TIZPE2@");
                lbCan2.setMaximumSize(new Dimension(60, 30));
                lbCan2.setMinimumSize(new Dimension(60, 30));
                lbCan2.setPreferredSize(new Dimension(60, 30));
                lbCan2.setName("lbCan2");
                sNPanel5.add(lbCan2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- E1TP2 ----
                E1TP2.setComponentPopupMenu(BTD);
                E1TP2.setPreferredSize(new Dimension(30, 30));
                E1TP2.setMinimumSize(new Dimension(30, 30));
                E1TP2.setName("E1TP2");
                sNPanel5.add(E1TP2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbCan4 ----
                lbCan4.setText("@TIZPE4@");
                lbCan4.setMaximumSize(new Dimension(100, 30));
                lbCan4.setMinimumSize(new Dimension(100, 30));
                lbCan4.setPreferredSize(new Dimension(100, 30));
                lbCan4.setName("lbCan4");
                sNPanel5.add(lbCan4, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- E1TP4 ----
                E1TP4.setComponentPopupMenu(BTD);
                E1TP4.setPreferredSize(new Dimension(30, 30));
                E1TP4.setMinimumSize(new Dimension(30, 30));
                E1TP4.setName("E1TP4");
                sNPanel5.add(E1TP4, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlAnalytique.add(sNPanel5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            tbpInfos.addTab("Analytique", pnlAnalytique);
            
            // ======== pnlInfosLivraison ========
            {
              pnlInfosLivraison.setName("pnlInfosLivraison");
              pnlInfosLivraison.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInfosLivraison.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlInfosLivraison.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlInfosLivraison.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlInfosLivraison.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
              
              // ---- taInformationLivraisonEnlevement ----
              taInformationLivraisonEnlevement.setFont(new Font("sansserif", Font.PLAIN, 14));
              taInformationLivraisonEnlevement.setName("taInformationLivraisonEnlevement");
              pnlInfosLivraison.add(taInformationLivraisonEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            tbpInfos.addTab("Informations livraison", pnlInfosLivraison);
          }
          pnlGrilleInfos.add(tbpInfos, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbTitreContact ----
          lbTitreContact.setText("Contact");
          lbTitreContact.setPreferredSize(new Dimension(150, 20));
          lbTitreContact.setMinimumSize(new Dimension(150, 20));
          lbTitreContact.setName("lbTitreContact");
          pnlGrilleInfos.add(lbTitreContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlContact ========
          {
            pnlContact.setBorder(new TitledBorder(""));
            pnlContact.setName("pnlContact");
            pnlContact.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlContact.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlContact.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlContact.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlContact.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbContact ----
            lbContact.setText("Contact");
            lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbContact.setPreferredSize(new Dimension(110, 30));
            lbContact.setMinimumSize(new Dimension(110, 30));
            lbContact.setName("lbContact");
            pnlContact.add(lbContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snContact ----
            snContact.setMinimumSize(new Dimension(250, 30));
            snContact.setMaximumSize(new Dimension(200, 30));
            snContact.setPreferredSize(new Dimension(250, 30));
            snContact.setName("snContact");
            snContact.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snContactValueChanged(e);
              }
            });
            pnlContact.add(snContact, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbTelContact ----
            lbTelContact.setText("T\u00e9l\u00e9phone");
            lbTelContact.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbTelContact.setPreferredSize(new Dimension(100, 30));
            lbTelContact.setMinimumSize(new Dimension(100, 30));
            lbTelContact.setHorizontalAlignment(SwingConstants.RIGHT);
            lbTelContact.setMaximumSize(new Dimension(100, 30));
            lbTelContact.setName("lbTelContact");
            pnlContact.add(lbTelContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- LTEL ----
            LTEL.setText("@LTEL@");
            LTEL.setToolTipText("mail : @LMAIL@ - fax : @LFAX@");
            LTEL.setMinimumSize(new Dimension(130, 30));
            LTEL.setPreferredSize(new Dimension(130, 30));
            LTEL.setFont(new Font("sansserif", Font.PLAIN, 14));
            LTEL.setEnabled(false);
            LTEL.setName("LTEL");
            pnlContact.add(LTEL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbMailContact ----
            lbMailContact.setText("E-mail");
            lbMailContact.setMinimumSize(new Dimension(110, 30));
            lbMailContact.setPreferredSize(new Dimension(110, 30));
            lbMailContact.setName("lbMailContact");
            pnlContact.add(lbMailContact, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snAdresseMail ----
            snAdresseMail.setName("snAdresseMail");
            pnlContact.add(snAdresseMail, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGrilleInfos.add(pnlContact, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(pnlGrilleInfos, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlGrilleDateEEtEncours ========
        {
          pnlGrilleDateEEtEncours.setName("pnlGrilleDateEEtEncours");
          pnlGrilleDateEEtEncours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGrilleDateEEtEncours.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGrilleDateEEtEncours.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGrilleDateEEtEncours.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGrilleDateEEtEncours.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ---- lbTitreDates ----
          lbTitreDates.setText("Dates");
          lbTitreDates.setPreferredSize(new Dimension(150, 20));
          lbTitreDates.setMinimumSize(new Dimension(150, 20));
          lbTitreDates.setName("lbTitreDates");
          pnlGrilleDateEEtEncours.add(lbTitreDates, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlTitreEncours ========
          {
            pnlTitreEncours.setName("pnlTitreEncours");
            pnlTitreEncours.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTitreEncours.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTitreEncours.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTitreEncours.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTitreEncours.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- bt_encours ----
            bt_encours.setToolTipText("Encours");
            bt_encours.setName("bt_encours");
            bt_encours.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_encoursActionPerformed(e);
              }
            });
            pnlTitreEncours.add(bt_encours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbEncours ----
            lbEncours.setText("Encours");
            lbEncours.setPreferredSize(new Dimension(150, 20));
            lbEncours.setMinimumSize(new Dimension(150, 20));
            lbEncours.setName("lbEncours");
            pnlTitreEncours.add(lbEncours, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGrilleDateEEtEncours.add(pnlTitreEncours, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== xTitledPanel5 ========
          {
            xTitledPanel5.setBorder(new TitledBorder(""));
            xTitledPanel5.setName("xTitledPanel5");
            xTitledPanel5.setLayout(new GridBagLayout());
            ((GridBagLayout) xTitledPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) xTitledPanel5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) xTitledPanel5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) xTitledPanel5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbDateLivraisonSouhaitee ----
            lbDateLivraisonSouhaitee.setText("Livraison souhait\u00e9e");
            lbDateLivraisonSouhaitee.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateLivraisonSouhaitee.setName("lbDateLivraisonSouhaitee");
            xTitledPanel5.add(lbDateLivraisonSouhaitee, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E1DLPX ----
            E1DLPX.setComponentPopupMenu(null);
            E1DLPX.setPreferredSize(new Dimension(115, 30));
            E1DLPX.setMinimumSize(new Dimension(115, 30));
            E1DLPX.setMaximumSize(new Dimension(115, 30));
            E1DLPX.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1DLPX.setName("E1DLPX");
            xTitledPanel5.add(E1DLPX, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbDateLivraisonPrevue ----
            lbDateLivraisonPrevue.setText("Livraison pr\u00e9vue");
            lbDateLivraisonPrevue.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateLivraisonPrevue.setName("lbDateLivraisonPrevue");
            xTitledPanel5.add(lbDateLivraisonPrevue, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E1DLSX ----
            E1DLSX.setComponentPopupMenu(null);
            E1DLSX.setPreferredSize(new Dimension(115, 30));
            E1DLSX.setMinimumSize(new Dimension(115, 30));
            E1DLSX.setMaximumSize(new Dimension(115, 30));
            E1DLSX.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1DLSX.setName("E1DLSX");
            xTitledPanel5.add(E1DLSX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbDateValidite ----
            lbDateValidite.setText("Validit\u00e9 du devis");
            lbDateValidite.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateValidite.setName("lbDateValidite");
            xTitledPanel5.add(lbDateValidite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- E1DT2X ----
            E1DT2X.setComponentPopupMenu(null);
            E1DT2X.setPreferredSize(new Dimension(115, 30));
            E1DT2X.setMinimumSize(new Dimension(115, 30));
            E1DT2X.setMaximumSize(new Dimension(115, 30));
            E1DT2X.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1DT2X.setName("E1DT2X");
            xTitledPanel5.add(E1DT2X, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbDateRelanceDevis ----
            lbDateRelanceDevis.setText("Date de relance");
            lbDateRelanceDevis.setMinimumSize(new Dimension(100, 28));
            lbDateRelanceDevis.setPreferredSize(new Dimension(140, 28));
            lbDateRelanceDevis.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDateRelanceDevis.setName("lbDateRelanceDevis");
            xTitledPanel5.add(lbDateRelanceDevis, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WDTREL ----
            WDTREL.setComponentPopupMenu(null);
            WDTREL.setPreferredSize(new Dimension(115, 30));
            WDTREL.setMinimumSize(new Dimension(115, 30));
            WDTREL.setMaximumSize(new Dimension(115, 30));
            WDTREL.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDTREL.setName("WDTREL");
            xTitledPanel5.add(WDTREL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGrilleDateEEtEncours.add(xTitledPanel5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlEncours ========
          {
            pnlEncours.setBorder(new TitledBorder(""));
            pnlEncours.setName("pnlEncours");
            pnlEncours.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEncours.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEncours.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEncours.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEncours.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPosition ----
            lbPosition.setText("Position");
            lbPosition.setMinimumSize(new Dimension(100, 30));
            lbPosition.setPreferredSize(new Dimension(100, 30));
            lbPosition.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbPosition.setName("lbPosition");
            pnlEncours.add(lbPosition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- WEPOS ----
            WEPOS.setMinimumSize(new Dimension(70, 30));
            WEPOS.setPreferredSize(new Dimension(70, 30));
            WEPOS.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEPOS.setName("WEPOS");
            pnlEncours.add(WEPOS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbPlafond ----
            lbPlafond.setText("Plafond");
            lbPlafond.setMinimumSize(new Dimension(60, 30));
            lbPlafond.setPreferredSize(new Dimension(60, 30));
            lbPlafond.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbPlafond.setName("lbPlafond");
            pnlEncours.add(lbPlafond, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- WEPLF ----
            WEPLF.setMinimumSize(new Dimension(70, 30));
            WEPLF.setPreferredSize(new Dimension(70, 30));
            WEPLF.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEPLF.setName("WEPLF");
            pnlEncours.add(WEPLF, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbDepassement ----
            lbDepassement.setText("D\u00e9passement");
            lbDepassement.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbDepassement.setForeground(new Color(215, 17, 17));
            lbDepassement.setMinimumSize(new Dimension(90, 30));
            lbDepassement.setPreferredSize(new Dimension(90, 30));
            lbDepassement.setName("lbDepassement");
            pnlEncours.add(lbDepassement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- WEDEP ----
            WEDEP.setMinimumSize(new Dimension(70, 30));
            WEDEP.setPreferredSize(new Dimension(70, 30));
            WEDEP.setFont(new Font("sansserif", Font.PLAIN, 14));
            WEDEP.setName("WEDEP");
            pnlEncours.add(WEDEP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGrilleDateEEtEncours.add(pnlEncours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(pnlGrilleDateEEtEncours, new GridBagConstraints(0, 1, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
        
        // ---- lbTitreExpedition ----
        lbTitreExpedition.setText("Exp\u00e9dition");
        lbTitreExpedition.setPreferredSize(new Dimension(150, 20));
        lbTitreExpedition.setMinimumSize(new Dimension(150, 20));
        lbTitreExpedition.setName("lbTitreExpedition");
        p_contenu2.add(lbTitreExpedition, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== p_exped ========
        {
          p_exped.setBorder(new TitledBorder(""));
          p_exped.setName("p_exped");
          p_exped.setLayout(new GridBagLayout());
          ((GridBagLayout) p_exped.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) p_exped.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) p_exped.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) p_exped.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbModeExpedition ----
          lbModeExpedition.setText("Mode");
          lbModeExpedition.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbModeExpedition.setPreferredSize(new Dimension(110, 30));
          lbModeExpedition.setMinimumSize(new Dimension(110, 30));
          lbModeExpedition.setMaximumSize(new Dimension(110, 30));
          lbModeExpedition.setName("lbModeExpedition");
          p_exped.add(lbModeExpedition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- snModeExpedition ----
          snModeExpedition.setName("snModeExpedition");
          p_exped.add(snModeExpedition, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbTransporteur ----
          lbTransporteur.setText("Transporteur");
          lbTransporteur.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTransporteur.setMinimumSize(new Dimension(80, 30));
          lbTransporteur.setMaximumSize(new Dimension(80, 30));
          lbTransporteur.setPreferredSize(new Dimension(80, 30));
          lbTransporteur.setName("lbTransporteur");
          p_exped.add(lbTransporteur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- snTransporteur ----
          snTransporteur.setName("snTransporteur");
          p_exped.add(snTransporteur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbFranco ----
          lbFranco.setText("Franco");
          lbFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbFranco.setPreferredSize(new Dimension(80, 30));
          lbFranco.setMinimumSize(new Dimension(80, 30));
          lbFranco.setMaximumSize(new Dimension(80, 30));
          lbFranco.setName("lbFranco");
          p_exped.add(lbFranco, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== sNPanel4 ========
          {
            sNPanel4.setName("sNPanel4");
            sNPanel4.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel4.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- WMFRP ----
            WMFRP.setMinimumSize(new Dimension(70, 30));
            WMFRP.setPreferredSize(new Dimension(70, 30));
            WMFRP.setFont(new Font("sansserif", Font.PLAIN, 14));
            WMFRP.setName("WMFRP");
            sNPanel4.add(WMFRP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E1IN9 ----
            E1IN9.setText("Livraison partielle autoris\u00e9e");
            E1IN9.setFont(new Font("sansserif", Font.PLAIN, 14));
            E1IN9.setHorizontalAlignment(SwingConstants.TRAILING);
            E1IN9.setName("E1IN9");
            sNPanel4.add(E1IN9, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          p_exped.add(sNPanel4, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbZone ----
          lbZone.setText("Zone");
          lbZone.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbZone.setPreferredSize(new Dimension(60, 30));
          lbZone.setMinimumSize(new Dimension(60, 30));
          lbZone.setMaximumSize(new Dimension(60, 30));
          lbZone.setName("lbZone");
          p_exped.add(lbZone, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== sNPanel6 ========
          {
            sNPanel6.setName("sNPanel6");
            sNPanel6.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel6.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel6.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel6.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          }
          p_exped.add(sNPanel6, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snZoneGeographique ----
          snZoneGeographique.setMaximumSize(new Dimension(150, 30));
          snZoneGeographique.setMinimumSize(new Dimension(150, 30));
          snZoneGeographique.setPreferredSize(new Dimension(150, 30));
          snZoneGeographique.setName("snZoneGeographique");
          p_exped.add(snZoneGeographique, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(p_exped, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlTitreConditions ========
        {
          pnlTitreConditions.setName("pnlTitreConditions");
          pnlTitreConditions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreConditions.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreConditions.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreConditions.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreConditions.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- bt_condition ----
          bt_condition.setToolTipText("Conditions");
          bt_condition.setName("bt_condition");
          bt_condition.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_conditionActionPerformed(e);
            }
          });
          pnlTitreConditions.add(bt_condition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbConditions ----
          lbConditions.setText("Conditions");
          lbConditions.setPreferredSize(new Dimension(150, 20));
          lbConditions.setMinimumSize(new Dimension(150, 20));
          lbConditions.setName("lbConditions");
          pnlTitreConditions.add(lbConditions, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(pnlTitreConditions, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== pnlTitreReglement ========
        {
          pnlTitreReglement.setName("pnlTitreReglement");
          pnlTitreReglement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreReglement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreReglement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreReglement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreReglement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- bt_reglement ----
          bt_reglement.setToolTipText("R\u00e9glements");
          bt_reglement.setName("bt_reglement");
          bt_reglement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_reglementActionPerformed(e);
            }
          });
          pnlTitreReglement.add(bt_reglement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbReglements ----
          lbReglements.setText("R\u00e8glements");
          lbReglements.setPreferredSize(new Dimension(150, 20));
          lbReglements.setMinimumSize(new Dimension(150, 20));
          lbReglements.setName("lbReglements");
          pnlTitreReglement.add(lbReglements, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(pnlTitreReglement, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlConditions ========
        {
          pnlConditions.setBorder(new TitledBorder(""));
          pnlConditions.setMinimumSize(new Dimension(600, 82));
          pnlConditions.setPreferredSize(new Dimension(600, 82));
          pnlConditions.setName("pnlConditions");
          pnlConditions.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlConditions.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlConditions.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlConditions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlConditions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbRemises ----
          lbRemises.setText("Remises");
          lbRemises.setName("lbRemises");
          pnlConditions.add(lbRemises, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- lbTarif ----
          lbTarif.setText("Tarif");
          lbTarif.setName("lbTarif");
          pnlConditions.add(lbTarif, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- lbCondition ----
          lbCondition.setText("Condition");
          lbCondition.setName("lbCondition");
          pnlConditions.add(lbCondition, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- lbPromo ----
          lbPromo.setText("Promo");
          lbPromo.setName("lbPromo");
          pnlConditions.add(lbPromo, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- lbEscompte ----
          lbEscompte.setText("%Esc.");
          lbEscompte.setName("lbEscompte");
          pnlConditions.add(lbEscompte, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlRemises ========
          {
            pnlRemises.setName("pnlRemises");
            pnlRemises.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemises.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemises.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemises.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemises.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- E1REM1 ----
            E1REM1.setComponentPopupMenu(BTD);
            E1REM1.setPreferredSize(new Dimension(60, 30));
            E1REM1.setMinimumSize(new Dimension(60, 30));
            E1REM1.setName("E1REM1");
            pnlRemises.add(E1REM1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E1REM2 ----
            E1REM2.setComponentPopupMenu(BTD);
            E1REM2.setMinimumSize(new Dimension(60, 30));
            E1REM2.setPreferredSize(new Dimension(60, 30));
            E1REM2.setName("E1REM2");
            pnlRemises.add(E1REM2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- E1REM3 ----
            E1REM3.setComponentPopupMenu(BTD);
            E1REM3.setMinimumSize(new Dimension(60, 30));
            E1REM3.setPreferredSize(new Dimension(60, 30));
            E1REM3.setName("E1REM3");
            pnlRemises.add(E1REM3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlConditions.add(pnlRemises, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- E1TAR ----
          E1TAR.setComponentPopupMenu(BTD);
          E1TAR.setMinimumSize(new Dimension(30, 30));
          E1TAR.setPreferredSize(new Dimension(30, 30));
          E1TAR.setName("E1TAR");
          pnlConditions.add(E1TAR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snConditionVenteEtClient ----
          snConditionVenteEtClient.setName("snConditionVenteEtClient");
          pnlConditions.add(snConditionVenteEtClient, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WCNP ----
          WCNP.setMinimumSize(new Dimension(60, 30));
          WCNP.setPreferredSize(new Dimension(60, 30));
          WCNP.setName("WCNP");
          pnlConditions.add(WCNP, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- E1ESCX ----
          E1ESCX.setComponentPopupMenu(BTD);
          E1ESCX.setHorizontalAlignment(SwingConstants.RIGHT);
          E1ESCX.setMinimumSize(new Dimension(45, 30));
          E1ESCX.setPreferredSize(new Dimension(50, 30));
          E1ESCX.setName("E1ESCX");
          pnlConditions.add(E1ESCX, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(pnlConditions, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlReglement ========
        {
          pnlReglement.setBorder(new TitledBorder(""));
          pnlReglement.setName("pnlReglement");
          pnlReglement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlReglement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlReglement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlReglement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlReglement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPosition2 ----
          lbPosition2.setText("Mode");
          lbPosition2.setMinimumSize(new Dimension(100, 30));
          lbPosition2.setPreferredSize(new Dimension(100, 30));
          lbPosition2.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPosition2.setName("lbPosition2");
          pnlReglement.add(lbPosition2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- E1RG2 ----
          E1RG2.setComponentPopupMenu(BTD);
          E1RG2.setMinimumSize(new Dimension(40, 30));
          E1RG2.setPreferredSize(new Dimension(40, 30));
          E1RG2.setName("E1RG2");
          pnlReglement.add(E1RG2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbDevise ----
          lbDevise.setText("Devise");
          lbDevise.setName("lbDevise");
          pnlReglement.add(lbDevise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snDevise ----
          snDevise.setName("snDevise");
          pnlReglement.add(snDevise, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        p_contenu2.add(pnlReglement, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_contenu2, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("M\u00e9morisation curseur");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    
    // ---- button1 ----
    button1.setText("Pied de bon : ne pas supprimer");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });
    
    // ---- OBJ_102 ----
    OBJ_102.setText("@WREPAC@");
    OBJ_102.setName("OBJ_102");
    
    // ---- riBoutonDetail1 ----
    riBoutonDetail1.setToolTipText("Livraison souhait\u00e9e");
    riBoutonDetail1.setName("riBoutonDetail1");
    riBoutonDetail1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riBoutonDetail1ActionPerformed(e);
      }
    });
    
    // ---- riBoutonDetail2 ----
    riBoutonDetail2.setToolTipText("Livraison pr\u00e9vue");
    riBoutonDetail2.setName("riBoutonDetail2");
    
    // ---- lib_Devise ----
    lib_Devise.setText("@DVLIBR@");
    lib_Devise.setBackground(new Color(204, 204, 204));
    lib_Devise.setFont(lib_Devise.getFont().deriveFont(lib_Devise.getFont().getStyle() | Font.BOLD));
    lib_Devise.setForeground(Color.red);
    lib_Devise.setName("lib_Devise");
    
    // ---- lbPlafond2 ----
    lbPlafond2.setText("@LIBRG1@");
    lbPlafond2.setMinimumSize(new Dimension(100, 30));
    lbPlafond2.setPreferredSize(new Dimension(100, 30));
    lbPlafond2.setFont(new Font("sansserif", Font.PLAIN, 14));
    lbPlafond2.setName("lbPlafond2");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WNUM;
  private RiZoneSortie E1ETB;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private RiZoneSortie WSUF;
  private JLabel OBJ_52;
  private XRiTextField E1VDE;
  private RiZoneSortie E1NFA;
  private JLabel OBJ_50;
  private JPanel p_tete_droite;
  private JLabel bt_ecran;
  private RiZoneSortie OBJ_129;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu1;
  private SNPanelContenu p_contenu2;
  private JTabbedPane pnlAdresses;
  private SNPanelContenu pnlAdresseFacturation;
  private SNLabelChamp lbBlanc;
  private SNLabelChamp lbClient;
  private SNClient snClientFac;
  private SNLabelChamp lbNom;
  private XRiTextField E3NOM;
  private SNLabelChamp lbPrenom;
  private XRiTextField E3CPL;
  private SNLabelChamp lbAdresse1;
  private XRiTextField E3RUE;
  private SNLabelChamp lbAdresse2;
  private XRiTextField E3LOC;
  private SNLabelChamp lbCommune;
  private SNCodePostalCommune snCodePostalCommuneFac;
  private SNLabelChamp lbPays;
  private SNPays snPaysFac;
  private SNLabelChamp lbTelephone;
  private XRiTextField E3TEL;
  private SNPanelContenu pnlAdressesLivraison;
  private SNBoutonDetail btnAutresAdressesLivraison;
  private SNLabelChamp lbClient2;
  private SNClient snClientLiv;
  private SNLabelChamp lbNom2;
  private XRiTextField E2NOM;
  private SNLabelChamp lbPrenom2;
  private XRiTextField E2CPL;
  private SNLabelChamp lbAdresse3;
  private XRiTextField E2RUE;
  private SNLabelChamp lbAdresse4;
  private XRiTextField E2LOC;
  private SNLabelChamp lbCommune2;
  private SNCodePostalCommune snCodePostalCommuneLiv;
  private SNLabelChamp lbPays2;
  private SNPays snPaysLiv;
  private SNLabelChamp lbTelephone2;
  private XRiTextField E2TEL;
  private SNPanel pnlGrilleInfos;
  private JTabbedPane tbpInfos;
  private SNPanelContenu pnlInfos;
  private JPanel panel4;
  private SNLabelChamp lbAttention;
  private XRiTextField WATN;
  private XRiCheckBox E1NHO;
  private SNLabelChamp lbReferenceCourte;
  private SNPanel sNPanel1;
  private XRiTextField E1NCC;
  private SNLabelChamp lbReferenceLongue;
  private XRiTextField E1RCC;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbRepresentant;
  private SNRepresentant snRepresentant;
  private SNLabelChamp lbObservation;
  private XRiTextField WOBS;
  private SNPanelContenu pnlInfo2;
  private SNLabelChamp lbTypePrix;
  private XRiComboBox E1PGC;
  private SNLabelChamp lbTypeDevis;
  private XRiTextField E1PFC;
  private SNLabelChamp lbEspoir;
  private XRiTextField E1PRE;
  private SNLabelChamp lbCommandeInitiale;
  private XRiTextField E1CCT;
  private SNLabelChamp lbPoids;
  private SNPanel pnlPoids;
  private XRiTextField E1PDS;
  private SNLabelChamp lbColis;
  private XRiTextField E1COL;
  private SNLabelChamp lbMotif;
  private XRiTextField E1VEH;
  private SNPanelContenu pnlAnalytique;
  private SNPanel pnlAnal;
  private SNLabelChamp lbAffaire;
  private SNAffaire snAffaire;
  private SNLabelChamp lbSection;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbNature;
  private XRiTextField E20NAT;
  private SNLabelChamp sNLabelChamp2;
  private XRiTextField E1CAN;
  private SNPanel sNPanel5;
  private SNLabelChamp lbCan1;
  private XRiTextField E1TP1;
  private SNLabelChamp lbCan3;
  private XRiTextField E1TP3;
  private SNLabelChamp lbCan5;
  private XRiTextField E1TP5;
  private SNLabelChamp lbCan2;
  private XRiTextField E1TP2;
  private SNLabelChamp lbCan4;
  private XRiTextField E1TP4;
  private SNPanelContenu pnlInfosLivraison;
  private RiTextArea taInformationLivraisonEnlevement;
  private SNLabelTitre lbTitreContact;
  private SNPanel pnlContact;
  private SNLabelChamp lbContact;
  private SNContact snContact;
  private SNLabelChamp lbTelContact;
  private SNTexte LTEL;
  private SNLabelChamp lbMailContact;
  private SNAdresseMail snAdresseMail;
  private SNPanel pnlGrilleDateEEtEncours;
  private SNLabelTitre lbTitreDates;
  private SNPanel pnlTitreEncours;
  private SNBoutonDetail bt_encours;
  private SNLabelTitre lbEncours;
  private SNPanel xTitledPanel5;
  private SNLabelChamp lbDateLivraisonSouhaitee;
  private XRiCalendrier E1DLPX;
  private SNLabelChamp lbDateLivraisonPrevue;
  private XRiCalendrier E1DLSX;
  private SNLabelChamp lbDateValidite;
  private XRiCalendrier E1DT2X;
  private SNLabelChamp lbDateRelanceDevis;
  private XRiCalendrier WDTREL;
  private SNPanel pnlEncours;
  private SNLabelChamp lbPosition;
  private XRiTextField WEPOS;
  private SNLabelChamp lbPlafond;
  private XRiTextField WEPLF;
  private SNLabelChamp lbDepassement;
  private XRiTextField WEDEP;
  private SNLabelTitre lbTitreExpedition;
  private SNPanel p_exped;
  private SNLabelChamp lbModeExpedition;
  private SNModeExpedition snModeExpedition;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbFranco;
  private SNPanel sNPanel4;
  private XRiTextField WMFRP;
  private XRiCheckBox E1IN9;
  private SNLabelChamp lbZone;
  private SNPanel sNPanel6;
  private SNZoneGeographique snZoneGeographique;
  private SNPanel pnlTitreConditions;
  private SNBoutonDetail bt_condition;
  private SNLabelTitre lbConditions;
  private SNPanel pnlTitreReglement;
  private SNBoutonDetail bt_reglement;
  private SNLabelTitre lbReglements;
  private SNPanel pnlConditions;
  private SNLabelUnite lbRemises;
  private SNLabelUnite lbTarif;
  private SNLabelUnite lbCondition;
  private SNLabelUnite lbPromo;
  private SNLabelUnite lbEscompte;
  private SNPanel pnlRemises;
  private XRiTextField E1REM1;
  private XRiTextField E1REM2;
  private XRiTextField E1REM3;
  private XRiTextField E1TAR;
  private SNConditionVenteEtClient snConditionVenteEtClient;
  private XRiTextField WCNP;
  private XRiTextField E1ESCX;
  private SNPanel pnlReglement;
  private SNLabelChamp lbPosition2;
  private XRiTextField E1RG2;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_20;
  private JButton button1;
  private JLabel OBJ_102;
  private SNBoutonDetail riBoutonDetail1;
  private SNBoutonDetail riBoutonDetail2;
  private RiZoneSortie lib_Devise;
  private SNLabelChamp lbPlafond2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
