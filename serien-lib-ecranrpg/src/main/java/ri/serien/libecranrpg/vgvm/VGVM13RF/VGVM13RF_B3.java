
package ri.serien.libecranrpg.vgvm.VGVM13RF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class VGVM13RF_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM13RF_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    TIDX18.setValeurs("1", "TIDX18");
    TIDX12.setValeurs("1", "TIDX12");
    TIDX11.setValeurs("1", "TIDX11");
    TIDX10.setValeurs("1", "TIDX10");
    TIDX9.setValeurs("1", "TIDX9");
    TIDX8.setValeurs("1", "TIDX8");
    TIDX7.setValeurs("1", "TIDX7");
    TIDX6.setValeurs("1", "TIDX6");
    TIDX5.setValeurs("1", "TIDX5");
    TIDX4.setValeurs("1", "TIDX4");
    TIDX14.setValeurs("1", "TIDX14");
    TIDX3.setValeurs("1", "TIDX3");
    TIDX13.setValeurs("1", "TIDX13");
    TIDX1.setValeurs("1", "TIDX1");
    TIDX15.setValeurs("1", "TIDX15");
    OPTDAT.setValeurs("S", buttonGroup1);
    OPTDAT_P.setValeurs("P");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  LOGISTIQUE Commandes à livrer"));
    
    

    p_bpresentation.setCodeEtablissement(WETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_34 = new JLabel();
    WETB = new XRiTextField();
    WNTO = new XRiTextField();
    OBJ_35 = new JLabel();
    R13SEL = new XRiTextField();
    OBJ_89 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX3 = new XRiRadioButton();
    TIDX11 = new XRiRadioButton();
    TIDX12 = new XRiRadioButton();
    TIDX18 = new XRiRadioButton();
    TIDX6 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX14 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    OPTDAT = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    OPTDAT_P = new XRiRadioButton();
    TIDX13 = new XRiRadioButton();
    VOLDEB = new XRiTextField();
    VOLFIN = new XRiTextField();
    PDSDEB = new XRiTextField();
    PDSFIN = new XRiTextField();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    CRTDEB = new XRiTextField();
    ZGEDEB = new XRiTextField();
    ZGEFIN = new XRiTextField();
    CRTFIN = new XRiTextField();
    ORDDEB = new XRiTextField();
    ORDFIN = new XRiTextField();
    CDPDEB = new XRiTextField();
    CDPFIN = new XRiTextField();
    M2PDEB = new XRiTextField();
    M2PFIN = new XRiTextField();
    LGMDEB = new XRiTextField();
    LGMFIN = new XRiTextField();
    PREDEB = new XRiTextField();
    PREFIN = new XRiTextField();
    CANDEB = new XRiTextField();
    CANFIN = new XRiTextField();
    MAGDEB = new XRiTextField();
    MAGFIN = new XRiTextField();
    MEXDEB = new XRiTextField();
    MEXFIN = new XRiTextField();
    CTRDEB = new XRiTextField();
    CTRFIN = new XRiTextField();
    OBJ_51 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_112 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_150 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_80 = new JLabel();
    OBJ_93 = new JLabel();
    OBJ_97 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_128 = new JLabel();
    OBJ_147 = new JLabel();
    EXPDEB = new XRiTextField();
    EXPFIN = new XRiTextField();
    OBJ_43 = new JLabel();
    OBJ_45 = new JLabel();
    TIDX15 = new XRiRadioButton();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    buttonGroup2 = new ButtonGroup();
    buttonGroup1 = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Logistique : commandes \u00e0 livrer");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_34 ----
          OBJ_34.setText("Etablissement");
          OBJ_34.setName("OBJ_34");
          p_tete_gauche.add(OBJ_34);
          OBJ_34.setBounds(5, 0, 93, 28);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(125, 0, 40, WETB.getPreferredSize().height);

          //---- WNTO ----
          WNTO.setComponentPopupMenu(BTD);
          WNTO.setName("WNTO");
          p_tete_gauche.add(WNTO);
          WNTO.setBounds(305, 0, 50, WNTO.getPreferredSize().height);

          //---- OBJ_35 ----
          OBJ_35.setText("Pr\u00e9paration");
          OBJ_35.setName("OBJ_35");
          p_tete_gauche.add(OBJ_35);
          OBJ_35.setBounds(210, 0, 93, 28);

          //---- R13SEL ----
          R13SEL.setComponentPopupMenu(BTD);
          R13SEL.setName("R13SEL");
          p_tete_gauche.add(R13SEL);
          R13SEL.setBounds(450, 0, 60, R13SEL.getPreferredSize().height);

          //---- OBJ_89 ----
          OBJ_89.setText("Etats");
          OBJ_89.setName("OBJ_89");
          p_tete_gauche.add(OBJ_89);
          OBJ_89.setBounds(400, 0, 50, 28);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(730, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("S\u00e9lection des commandes"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX3 ----
            TIDX3.setText("Regroupement transport");
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(40, 132, 170, 25);

            //---- TIDX11 ----
            TIDX11.setText("M\u00b2 au plancher maxmum");
            TIDX11.setName("TIDX11");
            panel1.add(TIDX11);
            TIDX11.setBounds(40, 403, 170, 23);

            //---- TIDX12 ----
            TIDX12.setText("Longueur maximum");
            TIDX12.setName("TIDX12");
            panel1.add(TIDX12);
            TIDX12.setBounds(40, 432, 170, 25);

            //---- TIDX18 ----
            TIDX18.setText("Pr\u00e9parateur");
            TIDX18.setName("TIDX18");
            panel1.add(TIDX18);
            TIDX18.setBounds(40, 461, 170, 26);

            //---- TIDX6 ----
            TIDX6.setText("Zone G\u00e9ographique");
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(40, 253, 170, 23);

            //---- TIDX1 ----
            TIDX1.setText("Dates de livraison");
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(40, 71, 170, 26);

            //---- TIDX7 ----
            TIDX7.setText("Mode d'exp\u00e9dition");
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(40, 282, 170, 25);

            //---- TIDX9 ----
            TIDX9.setText("Volume maximum");
            TIDX9.setName("TIDX9");
            panel1.add(TIDX9);
            TIDX9.setBounds(40, 342, 170, 24);

            //---- TIDX8 ----
            TIDX8.setText("Code transporteur");
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(40, 311, 170, 27);

            //---- TIDX10 ----
            TIDX10.setText("Poids  maximum");
            TIDX10.setName("TIDX10");
            panel1.add(TIDX10);
            TIDX10.setBounds(40, 371, 170, 26);

            //---- TIDX14 ----
            TIDX14.setText("Ordre / tourn\u00e9e");
            TIDX14.setName("TIDX14");
            panel1.add(TIDX14);
            TIDX14.setBounds(40, 161, 170, 27);

            //---- TIDX4 ----
            TIDX4.setText("Canal de vente");
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(40, 192, 170, 24);

            //---- OPTDAT ----
            OPTDAT.setText("Souhait\u00e9e");
            OPTDAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTDAT.setName("OPTDAT");
            panel1.add(OPTDAT);
            OPTDAT.setBounds(585, 71, 81, 26);

            //---- TIDX5 ----
            TIDX5.setText("Code postal");
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(40, 221, 170, 26);

            //---- OPTDAT_P ----
            OPTDAT_P.setText("Pr\u00e9vue");
            OPTDAT_P.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OPTDAT_P.setName("OPTDAT_P");
            panel1.add(OPTDAT_P);
            OPTDAT_P.setBounds(515, 71, 65, 26);

            //---- TIDX13 ----
            TIDX13.setText("Magasin");
            TIDX13.setName("TIDX13");
            panel1.add(TIDX13);
            TIDX13.setBounds(40, 103, 170, 23);

            //---- VOLDEB ----
            VOLDEB.setComponentPopupMenu(BTD);
            VOLDEB.setName("VOLDEB");
            panel1.add(VOLDEB);
            VOLDEB.setBounds(240, 340, 74, VOLDEB.getPreferredSize().height);

            //---- VOLFIN ----
            VOLFIN.setComponentPopupMenu(BTD);
            VOLFIN.setName("VOLFIN");
            panel1.add(VOLFIN);
            VOLFIN.setBounds(400, 340, 74, VOLFIN.getPreferredSize().height);

            //---- PDSDEB ----
            PDSDEB.setComponentPopupMenu(BTD);
            PDSDEB.setName("PDSDEB");
            panel1.add(PDSDEB);
            PDSDEB.setBounds(240, 370, 74, PDSDEB.getPreferredSize().height);

            //---- PDSFIN ----
            PDSFIN.setComponentPopupMenu(BTD);
            PDSFIN.setName("PDSFIN");
            panel1.add(PDSFIN);
            PDSFIN.setBounds(400, 370, 74, PDSFIN.getPreferredSize().height);

            //---- DATDEB ----
            DATDEB.setComponentPopupMenu(BTD);
            DATDEB.setName("DATDEB");
            panel1.add(DATDEB);
            DATDEB.setBounds(240, 70, 105, DATDEB.getPreferredSize().height);

            //---- DATFIN ----
            DATFIN.setComponentPopupMenu(BTD);
            DATFIN.setName("DATFIN");
            panel1.add(DATFIN);
            DATFIN.setBounds(400, 70, 105, DATFIN.getPreferredSize().height);

            //---- CRTDEB ----
            CRTDEB.setComponentPopupMenu(BTD);
            CRTDEB.setName("CRTDEB");
            panel1.add(CRTDEB);
            CRTDEB.setBounds(240, 130, 60, CRTDEB.getPreferredSize().height);

            //---- ZGEDEB ----
            ZGEDEB.setComponentPopupMenu(BTD);
            ZGEDEB.setName("ZGEDEB");
            panel1.add(ZGEDEB);
            ZGEDEB.setBounds(240, 250, 60, ZGEDEB.getPreferredSize().height);

            //---- ZGEFIN ----
            ZGEFIN.setComponentPopupMenu(BTD);
            ZGEFIN.setName("ZGEFIN");
            panel1.add(ZGEFIN);
            ZGEFIN.setBounds(400, 250, 60, ZGEFIN.getPreferredSize().height);

            //---- CRTFIN ----
            CRTFIN.setComponentPopupMenu(BTD);
            CRTFIN.setName("CRTFIN");
            panel1.add(CRTFIN);
            CRTFIN.setBounds(400, 130, 60, CRTFIN.getPreferredSize().height);

            //---- ORDDEB ----
            ORDDEB.setComponentPopupMenu(BTD);
            ORDDEB.setName("ORDDEB");
            panel1.add(ORDDEB);
            ORDDEB.setBounds(240, 160, 50, ORDDEB.getPreferredSize().height);

            //---- ORDFIN ----
            ORDFIN.setComponentPopupMenu(BTD);
            ORDFIN.setName("ORDFIN");
            panel1.add(ORDFIN);
            ORDFIN.setBounds(400, 160, 50, ORDFIN.getPreferredSize().height);

            //---- CDPDEB ----
            CDPDEB.setComponentPopupMenu(BTD);
            CDPDEB.setName("CDPDEB");
            panel1.add(CDPDEB);
            CDPDEB.setBounds(240, 220, 50, CDPDEB.getPreferredSize().height);

            //---- CDPFIN ----
            CDPFIN.setComponentPopupMenu(BTD);
            CDPFIN.setName("CDPFIN");
            panel1.add(CDPFIN);
            CDPFIN.setBounds(400, 220, 50, CDPFIN.getPreferredSize().height);

            //---- M2PDEB ----
            M2PDEB.setComponentPopupMenu(BTD);
            M2PDEB.setName("M2PDEB");
            panel1.add(M2PDEB);
            M2PDEB.setBounds(240, 400, 50, M2PDEB.getPreferredSize().height);

            //---- M2PFIN ----
            M2PFIN.setComponentPopupMenu(BTD);
            M2PFIN.setName("M2PFIN");
            panel1.add(M2PFIN);
            M2PFIN.setBounds(400, 400, 50, M2PFIN.getPreferredSize().height);

            //---- LGMDEB ----
            LGMDEB.setComponentPopupMenu(BTD);
            LGMDEB.setName("LGMDEB");
            panel1.add(LGMDEB);
            LGMDEB.setBounds(240, 430, 50, LGMDEB.getPreferredSize().height);

            //---- LGMFIN ----
            LGMFIN.setComponentPopupMenu(BTD);
            LGMFIN.setName("LGMFIN");
            panel1.add(LGMFIN);
            LGMFIN.setBounds(400, 430, 50, LGMFIN.getPreferredSize().height);

            //---- PREDEB ----
            PREDEB.setComponentPopupMenu(BTD);
            PREDEB.setName("PREDEB");
            panel1.add(PREDEB);
            PREDEB.setBounds(240, 460, 50, PREDEB.getPreferredSize().height);

            //---- PREFIN ----
            PREFIN.setComponentPopupMenu(BTD);
            PREFIN.setName("PREFIN");
            panel1.add(PREFIN);
            PREFIN.setBounds(400, 460, 50, PREFIN.getPreferredSize().height);

            //---- CANDEB ----
            CANDEB.setComponentPopupMenu(BTD);
            CANDEB.setName("CANDEB");
            panel1.add(CANDEB);
            CANDEB.setBounds(240, 190, 40, CANDEB.getPreferredSize().height);

            //---- CANFIN ----
            CANFIN.setComponentPopupMenu(BTD);
            CANFIN.setName("CANFIN");
            panel1.add(CANFIN);
            CANFIN.setBounds(400, 190, 40, CANFIN.getPreferredSize().height);

            //---- MAGDEB ----
            MAGDEB.setComponentPopupMenu(BTD);
            MAGDEB.setName("MAGDEB");
            panel1.add(MAGDEB);
            MAGDEB.setBounds(240, 100, 30, MAGDEB.getPreferredSize().height);

            //---- MAGFIN ----
            MAGFIN.setComponentPopupMenu(BTD);
            MAGFIN.setName("MAGFIN");
            panel1.add(MAGFIN);
            MAGFIN.setBounds(400, 100, 30, MAGFIN.getPreferredSize().height);

            //---- MEXDEB ----
            MEXDEB.setComponentPopupMenu(BTD);
            MEXDEB.setName("MEXDEB");
            panel1.add(MEXDEB);
            MEXDEB.setBounds(240, 280, 30, MEXDEB.getPreferredSize().height);

            //---- MEXFIN ----
            MEXFIN.setComponentPopupMenu(BTD);
            MEXFIN.setName("MEXFIN");
            panel1.add(MEXFIN);
            MEXFIN.setBounds(400, 280, 30, MEXFIN.getPreferredSize().height);

            //---- CTRDEB ----
            CTRDEB.setComponentPopupMenu(BTD);
            CTRDEB.setName("CTRDEB");
            panel1.add(CTRDEB);
            CTRDEB.setBounds(240, 310, 30, CTRDEB.getPreferredSize().height);

            //---- CTRFIN ----
            CTRFIN.setComponentPopupMenu(BTD);
            CTRFIN.setName("CTRFIN");
            panel1.add(CTRFIN);
            CTRFIN.setBounds(400, 310, 30, CTRFIN.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("de");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(215, 104, 19, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("de");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(215, 134, 19, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("de");
            OBJ_71.setName("OBJ_71");
            panel1.add(OBJ_71);
            OBJ_71.setBounds(215, 164, 19, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("de");
            OBJ_78.setName("OBJ_78");
            panel1.add(OBJ_78);
            OBJ_78.setBounds(215, 194, 19, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("de");
            OBJ_91.setName("OBJ_91");
            panel1.add(OBJ_91);
            OBJ_91.setBounds(215, 254, 19, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("de");
            OBJ_100.setName("OBJ_100");
            panel1.add(OBJ_100);
            OBJ_100.setBounds(215, 284, 19, 20);

            //---- OBJ_106 ----
            OBJ_106.setText("de");
            OBJ_106.setName("OBJ_106");
            panel1.add(OBJ_106);
            OBJ_106.setBounds(215, 314, 19, 20);

            //---- OBJ_112 ----
            OBJ_112.setText("de");
            OBJ_112.setName("OBJ_112");
            panel1.add(OBJ_112);
            OBJ_112.setBounds(215, 344, 19, 20);

            //---- OBJ_118 ----
            OBJ_118.setText("de");
            OBJ_118.setName("OBJ_118");
            panel1.add(OBJ_118);
            OBJ_118.setBounds(215, 374, 19, 20);

            //---- OBJ_125 ----
            OBJ_125.setText("de");
            OBJ_125.setName("OBJ_125");
            panel1.add(OBJ_125);
            OBJ_125.setBounds(215, 404, 19, 20);

            //---- OBJ_131 ----
            OBJ_131.setText("de");
            OBJ_131.setName("OBJ_131");
            panel1.add(OBJ_131);
            OBJ_131.setBounds(215, 434, 19, 20);

            //---- OBJ_150 ----
            OBJ_150.setText("de");
            OBJ_150.setName("OBJ_150");
            panel1.add(OBJ_150);
            OBJ_150.setBounds(215, 464, 19, 20);

            //---- OBJ_44 ----
            OBJ_44.setText("du");
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(215, 74, 18, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("au");
            OBJ_46.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(350, 74, 50, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("du");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(215, 224, 18, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("au");
            OBJ_86.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(350, 224, 50, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("\u00e0");
            OBJ_53.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(350, 104, 50, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("\u00e0");
            OBJ_63.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(350, 134, 50, 20);

            //---- OBJ_73 ----
            OBJ_73.setText("\u00e0");
            OBJ_73.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(350, 164, 50, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("\u00e0");
            OBJ_80.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_80.setName("OBJ_80");
            panel1.add(OBJ_80);
            OBJ_80.setBounds(350, 194, 50, 20);

            //---- OBJ_93 ----
            OBJ_93.setText("\u00e0");
            OBJ_93.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_93.setName("OBJ_93");
            panel1.add(OBJ_93);
            OBJ_93.setBounds(350, 254, 50, 20);

            //---- OBJ_97 ----
            OBJ_97.setText("\u00e0");
            OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_97.setName("OBJ_97");
            panel1.add(OBJ_97);
            OBJ_97.setBounds(350, 284, 50, 20);

            //---- OBJ_103 ----
            OBJ_103.setText("\u00e0");
            OBJ_103.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_103.setName("OBJ_103");
            panel1.add(OBJ_103);
            OBJ_103.setBounds(350, 314, 50, 20);

            //---- OBJ_109 ----
            OBJ_109.setText("\u00e0");
            OBJ_109.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_109.setName("OBJ_109");
            panel1.add(OBJ_109);
            OBJ_109.setBounds(350, 344, 50, 20);

            //---- OBJ_115 ----
            OBJ_115.setText("\u00e0");
            OBJ_115.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_115.setName("OBJ_115");
            panel1.add(OBJ_115);
            OBJ_115.setBounds(350, 374, 50, 20);

            //---- OBJ_122 ----
            OBJ_122.setText("\u00e0");
            OBJ_122.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_122.setName("OBJ_122");
            panel1.add(OBJ_122);
            OBJ_122.setBounds(350, 404, 50, 20);

            //---- OBJ_128 ----
            OBJ_128.setText("\u00e0");
            OBJ_128.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_128.setName("OBJ_128");
            panel1.add(OBJ_128);
            OBJ_128.setBounds(350, 434, 50, 20);

            //---- OBJ_147 ----
            OBJ_147.setText("\u00e0");
            OBJ_147.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_147.setName("OBJ_147");
            panel1.add(OBJ_147);
            OBJ_147.setBounds(350, 464, 50, 20);

            //---- EXPDEB ----
            EXPDEB.setComponentPopupMenu(BTD);
            EXPDEB.setName("EXPDEB");
            panel1.add(EXPDEB);
            EXPDEB.setBounds(240, 40, 90, EXPDEB.getPreferredSize().height);

            //---- EXPFIN ----
            EXPFIN.setComponentPopupMenu(BTD);
            EXPFIN.setName("EXPFIN");
            panel1.add(EXPFIN);
            EXPFIN.setBounds(400, 40, 90, EXPFIN.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("du");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(215, 44, 18, 20);

            //---- OBJ_45 ----
            OBJ_45.setText("au");
            OBJ_45.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(350, 44, 50, 20);

            //---- TIDX15 ----
            TIDX15.setText("Exp\u00e9ditions r\u00e9elles");
            TIDX15.setName("TIDX15");
            panel1.add(TIDX15);
            TIDX15.setBounds(40, 41, 170, 27);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 704, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 516, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- buttonGroup2 ----
    buttonGroup2.add(TIDX3);
    buttonGroup2.add(TIDX11);
    buttonGroup2.add(TIDX12);
    buttonGroup2.add(TIDX18);
    buttonGroup2.add(TIDX6);
    buttonGroup2.add(TIDX1);
    buttonGroup2.add(TIDX7);
    buttonGroup2.add(TIDX9);
    buttonGroup2.add(TIDX8);
    buttonGroup2.add(TIDX10);
    buttonGroup2.add(TIDX14);
    buttonGroup2.add(TIDX4);
    buttonGroup2.add(TIDX5);
    buttonGroup2.add(TIDX13);
    buttonGroup2.add(TIDX15);

    //---- buttonGroup1 ----
    buttonGroup1.add(OPTDAT);
    buttonGroup1.add(OPTDAT_P);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_34;
  private XRiTextField WETB;
  private XRiTextField WNTO;
  private JLabel OBJ_35;
  private XRiTextField R13SEL;
  private JLabel OBJ_89;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX11;
  private XRiRadioButton TIDX12;
  private XRiRadioButton TIDX18;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX7;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX14;
  private XRiRadioButton TIDX4;
  private XRiRadioButton OPTDAT;
  private XRiRadioButton TIDX5;
  private XRiRadioButton OPTDAT_P;
  private XRiRadioButton TIDX13;
  private XRiTextField VOLDEB;
  private XRiTextField VOLFIN;
  private XRiTextField PDSDEB;
  private XRiTextField PDSFIN;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private XRiTextField CRTDEB;
  private XRiTextField ZGEDEB;
  private XRiTextField ZGEFIN;
  private XRiTextField CRTFIN;
  private XRiTextField ORDDEB;
  private XRiTextField ORDFIN;
  private XRiTextField CDPDEB;
  private XRiTextField CDPFIN;
  private XRiTextField M2PDEB;
  private XRiTextField M2PFIN;
  private XRiTextField LGMDEB;
  private XRiTextField LGMFIN;
  private XRiTextField PREDEB;
  private XRiTextField PREFIN;
  private XRiTextField CANDEB;
  private XRiTextField CANFIN;
  private XRiTextField MAGDEB;
  private XRiTextField MAGFIN;
  private XRiTextField MEXDEB;
  private XRiTextField MEXFIN;
  private XRiTextField CTRDEB;
  private XRiTextField CTRFIN;
  private JLabel OBJ_51;
  private JLabel OBJ_61;
  private JLabel OBJ_71;
  private JLabel OBJ_78;
  private JLabel OBJ_91;
  private JLabel OBJ_100;
  private JLabel OBJ_106;
  private JLabel OBJ_112;
  private JLabel OBJ_118;
  private JLabel OBJ_125;
  private JLabel OBJ_131;
  private JLabel OBJ_150;
  private JLabel OBJ_44;
  private JLabel OBJ_46;
  private JLabel OBJ_84;
  private JLabel OBJ_86;
  private JLabel OBJ_53;
  private JLabel OBJ_63;
  private JLabel OBJ_73;
  private JLabel OBJ_80;
  private JLabel OBJ_93;
  private JLabel OBJ_97;
  private JLabel OBJ_103;
  private JLabel OBJ_109;
  private JLabel OBJ_115;
  private JLabel OBJ_122;
  private JLabel OBJ_128;
  private JLabel OBJ_147;
  private XRiTextField EXPDEB;
  private XRiTextField EXPFIN;
  private JLabel OBJ_43;
  private JLabel OBJ_45;
  private XRiRadioButton TIDX15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  private ButtonGroup buttonGroup2;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
