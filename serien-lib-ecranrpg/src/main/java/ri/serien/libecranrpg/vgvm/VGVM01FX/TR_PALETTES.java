
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TR_PALETTES extends SNPanelEcranRPG implements ioFrame {
   
  
  public TR_PALETTES(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Forfait hayon"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    TRNBR1 = new XRiTextField();
    TRNBR2 = new XRiTextField();
    TRNBR3 = new XRiTextField();
    TRNBR4 = new XRiTextField();
    TRNBR5 = new XRiTextField();
    TRFOR1 = new XRiTextField();
    TRFOR2 = new XRiTextField();
    TRFOR3 = new XRiTextField();
    TRFOR4 = new XRiTextField();
    TRFOR5 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(470, 280));
    setPreferredSize(new Dimension(470, 280));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_menus ========
    {
      p_menus.setPreferredSize(new Dimension(170, 0));
      p_menus.setMinimumSize(new Dimension(170, 0));
      p_menus.setBackground(new Color(238, 239, 241));
      p_menus.setBorder(LineBorder.createGrayLineBorder());
      p_menus.setName("p_menus");
      p_menus.setLayout(new BorderLayout());

      //======== menus_bas ========
      {
        menus_bas.setOpaque(false);
        menus_bas.setBackground(new Color(238, 239, 241));
        menus_bas.setName("menus_bas");
        menus_bas.setLayout(new VerticalLayout());

        //======== navig_valid ========
        {
          navig_valid.setName("navig_valid");

          //---- bouton_valider ----
          bouton_valider.setText("Valider");
          bouton_valider.setName("bouton_valider");
          bouton_valider.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_validerActionPerformed(e);
            }
          });
          navig_valid.add(bouton_valider);
        }
        menus_bas.add(navig_valid);

        //======== navig_retour ========
        {
          navig_retour.setName("navig_retour");

          //---- bouton_retour ----
          bouton_retour.setText("Retour");
          bouton_retour.setName("bouton_retour");
          bouton_retour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_retourActionPerformed(e);
            }
          });
          navig_retour.add(bouton_retour);
        }
        menus_bas.add(navig_retour);
      }
      p_menus.add(menus_bas, BorderLayout.SOUTH);
    }
    add(p_menus, BorderLayout.EAST);

    //======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Nombre de palettes forfaits"));
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- TRNBR1 ----
        TRNBR1.setName("TRNBR1");
        panel1.add(TRNBR1);
        TRNBR1.setBounds(60, 35, 35, TRNBR1.getPreferredSize().height);

        //---- TRNBR2 ----
        TRNBR2.setName("TRNBR2");
        panel1.add(TRNBR2);
        TRNBR2.setBounds(60, 70, 35, TRNBR2.getPreferredSize().height);

        //---- TRNBR3 ----
        TRNBR3.setName("TRNBR3");
        panel1.add(TRNBR3);
        TRNBR3.setBounds(60, 105, 35, TRNBR3.getPreferredSize().height);

        //---- TRNBR4 ----
        TRNBR4.setName("TRNBR4");
        panel1.add(TRNBR4);
        TRNBR4.setBounds(60, 140, 35, TRNBR4.getPreferredSize().height);

        //---- TRNBR5 ----
        TRNBR5.setName("TRNBR5");
        panel1.add(TRNBR5);
        TRNBR5.setBounds(60, 175, 35, TRNBR5.getPreferredSize().height);

        //---- TRFOR1 ----
        TRFOR1.setName("TRFOR1");
        panel1.add(TRFOR1);
        TRFOR1.setBounds(110, 35, 80, TRFOR1.getPreferredSize().height);

        //---- TRFOR2 ----
        TRFOR2.setName("TRFOR2");
        panel1.add(TRFOR2);
        TRFOR2.setBounds(110, 70, 80, TRFOR2.getPreferredSize().height);

        //---- TRFOR3 ----
        TRFOR3.setName("TRFOR3");
        panel1.add(TRFOR3);
        TRFOR3.setBounds(110, 105, 80, TRFOR3.getPreferredSize().height);

        //---- TRFOR4 ----
        TRFOR4.setName("TRFOR4");
        panel1.add(TRFOR4);
        TRFOR4.setBounds(110, 140, 80, TRFOR4.getPreferredSize().height);

        //---- TRFOR5 ----
        TRFOR5.setName("TRFOR5");
        panel1.add(TRFOR5);
        TRFOR5.setBounds(110, 175, 80, TRFOR5.getPreferredSize().height);

        //---- label1 ----
        label1.setName("label1");
        panel1.add(label1);
        label1.setBounds(35, 40, 25, label1.getPreferredSize().height);

        //---- label2 ----
        label2.setName("label2");
        panel1.add(label2);
        label2.setBounds(35, 76, 25, label2.getPreferredSize().height);

        //---- label3 ----
        label3.setName("label3");
        panel1.add(label3);
        label3.setBounds(35, 111, 25, label3.getPreferredSize().height);

        //---- label4 ----
        label4.setName("label4");
        panel1.add(label4);
        label4.setBounds(35, 146, 25, label4.getPreferredSize().height);

        //---- label5 ----
        label5.setName("label5");
        panel1.add(label5);
        label5.setBounds(35, 181, 25, label5.getPreferredSize().height);

        //---- label6 ----
        label6.setText("1");
        label6.setHorizontalAlignment(SwingConstants.CENTER);
        label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD, label6.getFont().getSize() + 3f));
        label6.setName("label6");
        panel1.add(label6);
        label6.setBounds(20, 35, 30, 28);

        //---- label7 ----
        label7.setText("2");
        label7.setHorizontalAlignment(SwingConstants.CENTER);
        label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD, label7.getFont().getSize() + 3f));
        label7.setName("label7");
        panel1.add(label7);
        label7.setBounds(20, 70, 30, 28);

        //---- label8 ----
        label8.setText("3");
        label8.setHorizontalAlignment(SwingConstants.CENTER);
        label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD, label8.getFont().getSize() + 3f));
        label8.setName("label8");
        panel1.add(label8);
        label8.setBounds(20, 105, 30, 28);

        //---- label9 ----
        label9.setText("4");
        label9.setHorizontalAlignment(SwingConstants.CENTER);
        label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD, label9.getFont().getSize() + 3f));
        label9.setName("label9");
        panel1.add(label9);
        label9.setBounds(20, 140, 30, 28);

        //---- label10 ----
        label10.setText("5");
        label10.setHorizontalAlignment(SwingConstants.CENTER);
        label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD, label10.getFont().getSize() + 3f));
        label10.setName("label10");
        panel1.add(label10);
        label10.setBounds(20, 175, 30, 28);
      }
      P_Centre.add(panel1);
      panel1.setBounds(15, 15, 265, 225);
    }
    add(P_Centre, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel P_Centre;
  private JPanel panel1;
  private XRiTextField TRNBR1;
  private XRiTextField TRNBR2;
  private XRiTextField TRNBR3;
  private XRiTextField TRNBR4;
  private XRiTextField TRNBR5;
  private XRiTextField TRFOR1;
  private XRiTextField TRFOR2;
  private XRiTextField TRFOR3;
  private XRiTextField TRFOR4;
  private XRiTextField TRFOR5;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
