
package ri.serien.libecranrpg.vgvm.VGVM111F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM111F_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] P14TRL_Value = { "", "A" };
  private String[] P14BRL_Value = { "M", "" };
  
  public VGVM111F_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    P14BRL.setValeurs(P14BRL_Value, null);
    P14TRL.setValeurs(P14TRL_Value, null);
    L1RP1.setValeursSelection("1", " ");
    L1RP2.setValeursSelection("1", " ");
    L1RP3.setValeursSelection("1", " ");
    L1RP4.setValeursSelection("1", " ");
    L1RP5.setValeursSelection("1", " ");
    L1RP6.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // P14TRL.setSelectedIndex(getIndice("P14TRL", P14TRL_Value));
    // P14BRL.setSelectedIndex(getIndice("P14BRL", P14BRL_Value));
    
    // L1RP6.setVisible( lexique.isPresent("L1RP6"));
    // L1RP6.setSelected(lexique.HostFieldGetData("L1RP6").equalsIgnoreCase("1"));
    // L1RP5.setVisible( lexique.isPresent("L1RP5"));
    // L1RP5.setSelected(lexique.HostFieldGetData("L1RP5").equalsIgnoreCase("1"));
    // L1RP4.setVisible( lexique.isPresent("L1RP4"));
    // L1RP4.setSelected(lexique.HostFieldGetData("L1RP4").equalsIgnoreCase("1"));
    // L1RP3.setVisible( lexique.isPresent("L1RP3"));
    // L1RP3.setSelected(lexique.HostFieldGetData("L1RP3").equalsIgnoreCase("1"));
    // L1RP2.setVisible( lexique.isPresent("L1RP2"));
    // L1RP2.setSelected(lexique.HostFieldGetData("L1RP2").equalsIgnoreCase("1"));
    // L1RP1.setVisible( lexique.isPresent("L1RP1"));
    // L1RP1.setSelected(lexique.HostFieldGetData("L1RP1").equalsIgnoreCase("1"));
    
    L1REM6.setEnabled(lexique.isPresent("L1REM6"));
    L1REM5.setEnabled(lexique.isPresent("L1REM5"));
    L1REM4.setEnabled(lexique.isPresent("L1REM4"));
    L1REM3.setEnabled(lexique.isPresent("L1REM3"));
    L1REM2.setEnabled(lexique.isPresent("L1REM2"));
    L1REM1.setEnabled(lexique.isPresent("L1REM1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion de bons"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_21 = new JLabel();
    L1REM1 = new XRiTextField();
    L1REM2 = new XRiTextField();
    L1REM3 = new XRiTextField();
    L1REM4 = new XRiTextField();
    L1REM5 = new XRiTextField();
    L1REM6 = new XRiTextField();
    OBJ_19 = new JLabel();
    L1RP6 = new XRiCheckBox();
    L1RP5 = new XRiCheckBox();
    L1RP4 = new XRiCheckBox();
    L1RP3 = new XRiCheckBox();
    L1RP2 = new XRiCheckBox();
    L1RP1 = new XRiCheckBox();
    OBJ_30 = new JLabel();
    OBJ_37 = new JLabel();
    P14TAR = new XRiTextField();
    OBJ_39 = new JLabel();
    P14MAR = new XRiTextField();
    OBJ_41 = new JLabel();
    P14TRL = new XRiComboBox();
    P14BRL = new XRiComboBox();
    OBJ_20 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(920, 215));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Remises"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_21 ----
          OBJ_21.setText("Remises sur ligne");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(28, 54, 120, 20);

          //---- L1REM1 ----
          L1REM1.setName("L1REM1");
          panel1.add(L1REM1);
          L1REM1.setBounds(200, 50, 50, L1REM1.getPreferredSize().height);

          //---- L1REM2 ----
          L1REM2.setName("L1REM2");
          panel1.add(L1REM2);
          L1REM2.setBounds(250, 50, 50, L1REM2.getPreferredSize().height);

          //---- L1REM3 ----
          L1REM3.setName("L1REM3");
          panel1.add(L1REM3);
          L1REM3.setBounds(300, 50, 50, L1REM3.getPreferredSize().height);

          //---- L1REM4 ----
          L1REM4.setName("L1REM4");
          panel1.add(L1REM4);
          L1REM4.setBounds(350, 50, 50, L1REM4.getPreferredSize().height);

          //---- L1REM5 ----
          L1REM5.setName("L1REM5");
          panel1.add(L1REM5);
          L1REM5.setBounds(400, 50, 50, L1REM5.getPreferredSize().height);

          //---- L1REM6 ----
          L1REM6.setName("L1REM6");
          panel1.add(L1REM6);
          L1REM6.setBounds(450, 50, 50, L1REM6.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setText("Type");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(510, 30, 36, 20);

          //---- L1RP6 ----
          L1RP6.setText("6");
          L1RP6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1RP6.setName("L1RP6");
          panel1.add(L1RP6);
          L1RP6.setBounds(450, 95, 37, 20);

          //---- L1RP5 ----
          L1RP5.setText("5");
          L1RP5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1RP5.setName("L1RP5");
          panel1.add(L1RP5);
          L1RP5.setBounds(400, 95, 37, 20);

          //---- L1RP4 ----
          L1RP4.setText("4");
          L1RP4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1RP4.setName("L1RP4");
          panel1.add(L1RP4);
          L1RP4.setBounds(350, 95, 37, 20);

          //---- L1RP3 ----
          L1RP3.setText("3");
          L1RP3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1RP3.setName("L1RP3");
          panel1.add(L1RP3);
          L1RP3.setBounds(300, 95, 37, 20);

          //---- L1RP2 ----
          L1RP2.setText("2");
          L1RP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1RP2.setName("L1RP2");
          panel1.add(L1RP2);
          L1RP2.setBounds(250, 95, 37, 20);

          //---- L1RP1 ----
          L1RP1.setText("1");
          L1RP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1RP1.setName("L1RP1");
          panel1.add(L1RP1);
          L1RP1.setBounds(200, 95, 42, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("Exclusion remises sur pied");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(28, 95, 162, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Colonne tarif");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(28, 137, 87, 20);

          //---- P14TAR ----
          P14TAR.setName("P14TAR");
          panel1.add(P14TAR);
          P14TAR.setBounds(200, 133, 26, P14TAR.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Marge r\u00e9elle");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(250, 137, 100, 20);

          //---- P14MAR ----
          P14MAR.setName("P14MAR");
          panel1.add(P14MAR);
          P14MAR.setBounds(350, 133, 50, P14MAR.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setText("%");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(405, 137, 15, 20);

          //---- P14TRL ----
          P14TRL.setModel(new DefaultComboBoxModel(new String[] {
            "Cascade",
            "Ajout"
          }));
          P14TRL.setName("P14TRL");
          panel1.add(P14TRL);
          P14TRL.setBounds(505, 50, 90, P14TRL.getPreferredSize().height);

          //---- P14BRL ----
          P14BRL.setModel(new DefaultComboBoxModel(new String[] {
            "Montant",
            "Prix"
          }));
          P14BRL.setName("P14BRL");
          panel1.add(P14BRL);
          P14BRL.setBounds(600, 50, 90, P14BRL.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Base");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(605, 30, 65, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 736, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 189, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_21;
  private XRiTextField L1REM1;
  private XRiTextField L1REM2;
  private XRiTextField L1REM3;
  private XRiTextField L1REM4;
  private XRiTextField L1REM5;
  private XRiTextField L1REM6;
  private JLabel OBJ_19;
  private XRiCheckBox L1RP6;
  private XRiCheckBox L1RP5;
  private XRiCheckBox L1RP4;
  private XRiCheckBox L1RP3;
  private XRiCheckBox L1RP2;
  private XRiCheckBox L1RP1;
  private JLabel OBJ_30;
  private JLabel OBJ_37;
  private XRiTextField P14TAR;
  private JLabel OBJ_39;
  private XRiTextField P14MAR;
  private JLabel OBJ_41;
  private XRiComboBox P14TRL;
  private XRiComboBox P14BRL;
  private JLabel OBJ_20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
