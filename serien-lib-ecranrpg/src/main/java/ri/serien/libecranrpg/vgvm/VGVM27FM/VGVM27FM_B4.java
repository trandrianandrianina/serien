
package ri.serien.libecranrpg.vgvm.VGVM27FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM27FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM27FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    BRTOP2.setValeursSelection("1", " ");
    BRTOP1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ERR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR1@")).trim());
    ERR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR2@")).trim());
    ERR3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR3@")).trim());
    ERR4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR4@")).trim());
    ERR5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR5@")).trim());
    ERR6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR6@")).trim());
    ERR7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR7@")).trim());
    ERR8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR8@")).trim());
    ERR9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR9@")).trim());
    ERR10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR10@")).trim());
    ERR11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR11@")).trim());
    ERR12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR12@")).trim());
    ERR13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR13@")).trim());
    ERR14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR14@")).trim());
    ERR15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERR15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    BRTAV.setVisible(lexique.isPresent("BRTAV"));
    // BRTOP1.setVisible( lexique.isPresent("BRTOP1"));
    // BRTOP1.setSelected(lexique.HostFieldGetData("BRTOP1").equalsIgnoreCase("1"));
    BRPER3.setVisible(lexique.isPresent("BRPER3"));
    BRECH.setVisible(lexique.isPresent("BRART2"));
    BRRGL.setVisible(lexique.isPresent("BRART2"));
    // BRTOP2.setVisible( lexique.isPresent("BRTOP2"));
    // BRTOP2.setSelected(lexique.HostFieldGetData("BRTOP2").equalsIgnoreCase("1"));
    OBJ_92.setVisible(lexique.isPresent("BRART2"));
    OBJ_95.setVisible(lexique.isPresent("BRART3"));
    OBJ_91.setVisible(lexique.isPresent("BRART2"));
    // WFIN2X.setEnabled( lexique.isPresent("WFIN2X"));
    // WDEB2X.setEnabled( lexique.isPresent("WDEB2X"));
    // WFIN1X.setEnabled( lexique.isPresent("WFIN1X"));
    // WDEB1X.setEnabled( lexique.isPresent("WDEB1X"));
    BRCAP.setVisible(lexique.isPresent("BRCAP"));
    OBJ_93.setVisible(lexique.isPresent("BRART2"));
    OBJ_94.setVisible(lexique.isPresent("BRART2"));
    OBJ_96.setVisible(lexique.isPresent("BRART3"));
    OBJ_97.setVisible(lexique.isPresent("BRTOP1"));
    BRART1.setVisible(lexique.isPresent("BRART1"));
    BRART3.setVisible(lexique.isPresent("BRART3"));
    BRART2.setVisible(lexique.isPresent("BRART2"));
    BRLIB.setEnabled(lexique.isPresent("BRLIB"));
    BRDES4.setEnabled(lexique.isPresent("BRDES4"));
    BRDES3.setEnabled(lexique.isPresent("BRDES3"));
    BRDES2.setEnabled(lexique.isPresent("BRDES2"));
    BRDES1.setEnabled(lexique.isPresent("BRDES1"));
    
    if (lexique.isTrue("92")) {
      p_bpresentation.setText("Barèmes de bonifications");
    }
    else {
      p_bpresentation.setText("Barèmes de remises");
    }
    
    OBJ_12.setVisible(!lexique.isTrue("53"));
    
    panel5.setVisible(lexique.isTrue("N92"));
    panel6.setVisible(lexique.isTrue("N92"));
    panel7.setVisible(lexique.isPresent("BRCAP"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @BARE@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (BRTOP1.isSelected())
    // lexique.HostFieldPutData("BRTOP1", 0, "1");
    // else
    // lexique.HostFieldPutData("BRTOP1", 0, " ");
    // if (BRTOP2.isSelected())
    // lexique.HostFieldPutData("BRTOP2", 0, "1");
    // else
    // lexique.HostFieldPutData("BRTOP2", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    WETB = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    OBJ_50 = new JLabel();
    WCNV = new XRiTextField();
    BRLIB = new XRiTextField();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    BRPER1 = new XRiTextField();
    BRPER2 = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panel4 = new JPanel();
    OBJ_97 = new JLabel();
    BRTOP1 = new XRiCheckBox();
    BRDES1 = new XRiTextField();
    BRDES2 = new XRiTextField();
    BRART1 = new XRiTextField();
    BRDES3 = new XRiTextField();
    BRDES4 = new XRiTextField();
    BRDES5 = new XRiTextField();
    panel6 = new JPanel();
    BRART3 = new XRiTextField();
    OBJ_95 = new JLabel();
    OBJ_96 = new JLabel();
    BRPER3 = new XRiTextField();
    panel7 = new JPanel();
    BRCAP = new XRiTextField();
    panel5 = new JPanel();
    OBJ_91 = new JLabel();
    BRART2 = new XRiTextField();
    BRTOP2 = new XRiCheckBox();
    OBJ_92 = new JLabel();
    BRTAV = new XRiTextField();
    OBJ_93 = new JLabel();
    OBJ_94 = new JLabel();
    BRECH = new XRiTextField();
    BRRGL = new XRiTextField();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    WDEB1X = new XRiCalendrier();
    WFIN1X = new XRiCalendrier();
    R1011 = new XRiTextField();
    R1021 = new XRiTextField();
    R1031 = new XRiTextField();
    R1041 = new XRiTextField();
    R1051 = new XRiTextField();
    R1061 = new XRiTextField();
    R1071 = new XRiTextField();
    R1081 = new XRiTextField();
    R1091 = new XRiTextField();
    R1101 = new XRiTextField();
    R1111 = new XRiTextField();
    R1121 = new XRiTextField();
    R1131 = new XRiTextField();
    R1141 = new XRiTextField();
    R11511 = new XRiTextField();
    R1012 = new XRiTextField();
    R1022 = new XRiTextField();
    R1032 = new XRiTextField();
    R1042 = new XRiTextField();
    R1052 = new XRiTextField();
    R1062 = new XRiTextField();
    R1072 = new XRiTextField();
    R1082 = new XRiTextField();
    R1092 = new XRiTextField();
    R1102 = new XRiTextField();
    R1112 = new XRiTextField();
    R1122 = new XRiTextField();
    R1132 = new XRiTextField();
    R1142 = new XRiTextField();
    R1152 = new XRiTextField();
    R1013 = new XRiTextField();
    R1023 = new XRiTextField();
    R1033 = new XRiTextField();
    R1043 = new XRiTextField();
    R1053 = new XRiTextField();
    R1063 = new XRiTextField();
    R1073 = new XRiTextField();
    R1083 = new XRiTextField();
    R1093 = new XRiTextField();
    R1103 = new XRiTextField();
    R1113 = new XRiTextField();
    R1123 = new XRiTextField();
    R1133 = new XRiTextField();
    R1143 = new XRiTextField();
    R1153 = new XRiTextField();
    R1014 = new XRiTextField();
    R1024 = new XRiTextField();
    R1034 = new XRiTextField();
    R1044 = new XRiTextField();
    R1054 = new XRiTextField();
    R1064 = new XRiTextField();
    R1074 = new XRiTextField();
    R1084 = new XRiTextField();
    R1094 = new XRiTextField();
    R1104 = new XRiTextField();
    R1114 = new XRiTextField();
    R1124 = new XRiTextField();
    R1134 = new XRiTextField();
    R1144 = new XRiTextField();
    R1154 = new XRiTextField();
    separator2 = compFactory.createSeparator("Remise", SwingConstants.CENTER);
    OBJ_56 = new JLabel();
    OBJ_58 = new JLabel();
    WDEB2X = new XRiCalendrier();
    WFIN2X = new XRiCalendrier();
    R2011 = new XRiTextField();
    R2021 = new XRiTextField();
    R2031 = new XRiTextField();
    R2041 = new XRiTextField();
    R2051 = new XRiTextField();
    R2061 = new XRiTextField();
    R2071 = new XRiTextField();
    R2081 = new XRiTextField();
    R2091 = new XRiTextField();
    R2101 = new XRiTextField();
    R2111 = new XRiTextField();
    R2121 = new XRiTextField();
    R2131 = new XRiTextField();
    R2141 = new XRiTextField();
    R2151 = new XRiTextField();
    R2012 = new XRiTextField();
    R2022 = new XRiTextField();
    R2032 = new XRiTextField();
    R2042 = new XRiTextField();
    R2052 = new XRiTextField();
    R2062 = new XRiTextField();
    R2072 = new XRiTextField();
    R2082 = new XRiTextField();
    R2092 = new XRiTextField();
    R2102 = new XRiTextField();
    R2112 = new XRiTextField();
    R2122 = new XRiTextField();
    R2132 = new XRiTextField();
    R2142 = new XRiTextField();
    R2152 = new XRiTextField();
    R2013 = new XRiTextField();
    R2023 = new XRiTextField();
    R2033 = new XRiTextField();
    R2043 = new XRiTextField();
    R2053 = new XRiTextField();
    R2063 = new XRiTextField();
    R2073 = new XRiTextField();
    R2083 = new XRiTextField();
    R2093 = new XRiTextField();
    R2103 = new XRiTextField();
    R2113 = new XRiTextField();
    R2123 = new XRiTextField();
    R2133 = new XRiTextField();
    R2143 = new XRiTextField();
    R2153 = new XRiTextField();
    R2014 = new XRiTextField();
    R2024 = new XRiTextField();
    R2034 = new XRiTextField();
    R2044 = new XRiTextField();
    R2054 = new XRiTextField();
    R2064 = new XRiTextField();
    R2074 = new XRiTextField();
    R2084 = new XRiTextField();
    R2094 = new XRiTextField();
    R2104 = new XRiTextField();
    R2114 = new XRiTextField();
    R2124 = new XRiTextField();
    R2134 = new XRiTextField();
    R2144 = new XRiTextField();
    R2154 = new XRiTextField();
    separator3 = compFactory.createSeparator("Remise", SwingConstants.CENTER);
    Q101 = new XRiTextField();
    Q102 = new XRiTextField();
    Q103 = new XRiTextField();
    Q104 = new XRiTextField();
    Q105 = new XRiTextField();
    Q106 = new XRiTextField();
    Q107 = new XRiTextField();
    Q108 = new XRiTextField();
    Q109 = new XRiTextField();
    Q110 = new XRiTextField();
    Q111 = new XRiTextField();
    Q112 = new XRiTextField();
    Q113 = new XRiTextField();
    Q114 = new XRiTextField();
    Q115 = new XRiTextField();
    ERR1 = new JLabel();
    ERR2 = new JLabel();
    ERR3 = new JLabel();
    ERR4 = new JLabel();
    ERR5 = new JLabel();
    ERR6 = new JLabel();
    ERR7 = new JLabel();
    ERR8 = new JLabel();
    ERR9 = new JLabel();
    ERR10 = new JLabel();
    ERR11 = new JLabel();
    ERR12 = new JLabel();
    ERR13 = new JLabel();
    ERR14 = new JLabel();
    ERR15 = new JLabel();
    separator1 = compFactory.createSeparator("Montant mini");
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bar\u00e8mes de remises");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- OBJ_50 ----
          OBJ_50.setText("Code");
          OBJ_50.setName("OBJ_50");

          //---- WCNV ----
          WCNV.setComponentPopupMenu(BTD);
          WCNV.setName("WCNV");

          //---- BRLIB ----
          BRLIB.setComponentPopupMenu(BTD);
          BRLIB.setName("BRLIB");

          //---- OBJ_89 ----
          OBJ_89.setText("Mois d\u00e9but");
          OBJ_89.setName("OBJ_89");

          //---- OBJ_90 ----
          OBJ_90.setText("P\u00e9riodicit\u00e9");
          OBJ_90.setName("OBJ_90");

          //---- BRPER1 ----
          BRPER1.setComponentPopupMenu(BTD);
          BRPER1.setName("BRPER1");

          //---- BRPER2 ----
          BRPER2.setComponentPopupMenu(BTD);
          BRPER2.setName("BRPER2");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BRLIB, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(BRPER1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(BRPER2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BRLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_89, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
              .addComponent(BRPER1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(7, 7, 7)
                .addComponent(OBJ_90, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
              .addComponent(BRPER2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1150, 800));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setMinimumSize(new Dimension(1180, 600));
            panel1.setPreferredSize(new Dimension(1180, 600));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(""));
              panel4.setOpaque(false);
              panel4.setName("panel4");

              //---- OBJ_97 ----
              OBJ_97.setText("Commentaire");
              OBJ_97.setName("OBJ_97");

              //---- BRTOP1 ----
              BRTOP1.setText("");
              BRTOP1.setComponentPopupMenu(BTD);
              BRTOP1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BRTOP1.setName("BRTOP1");

              //---- BRDES1 ----
              BRDES1.setComponentPopupMenu(BTD);
              BRDES1.setName("BRDES1");

              //---- BRDES2 ----
              BRDES2.setComponentPopupMenu(BTD);
              BRDES2.setName("BRDES2");

              //---- BRART1 ----
              BRART1.setComponentPopupMenu(BTD);
              BRART1.setName("BRART1");

              //---- BRDES3 ----
              BRDES3.setComponentPopupMenu(BTD);
              BRDES3.setName("BRDES3");

              //---- BRDES4 ----
              BRDES4.setComponentPopupMenu(BTD);
              BRDES4.setName("BRDES4");

              //---- BRDES5 ----
              BRDES5.setComponentPopupMenu(BTD);
              BRDES5.setName("BRDES5");

              GroupLayout panel4Layout = new GroupLayout(panel4);
              panel4.setLayout(panel4Layout);
              panel4Layout.setHorizontalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(1, 1, 1)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(BRTOP1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BRART1, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))
                    .addGap(15, 15, 15)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(BRDES1, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BRDES5, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BRDES2, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BRDES3, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BRDES4, GroupLayout.PREFERRED_SIZE, 745, GroupLayout.PREFERRED_SIZE)))
              );
              panel4Layout.setVerticalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_97, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BRTOP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addComponent(BRART1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addComponent(BRDES1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(100, 100, 100)
                    .addComponent(BRDES5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addComponent(BRDES2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(50, 50, 50)
                    .addComponent(BRDES3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(75, 75, 75)
                    .addComponent(BRDES4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              );
            }
            panel1.add(panel4);
            panel4.setBounds(5, 5, 1010, 155);

            //======== panel6 ========
            {
              panel6.setBorder(new TitledBorder("G\u00e9n\u00e9ration acompte"));
              panel6.setOpaque(false);
              panel6.setPreferredSize(new Dimension(275, 112));
              panel6.setMinimumSize(new Dimension(275, 112));
              panel6.setName("panel6");

              //---- BRART3 ----
              BRART3.setComponentPopupMenu(BTD);
              BRART3.setName("BRART3");

              //---- OBJ_95 ----
              OBJ_95.setText("Article");
              OBJ_95.setName("OBJ_95");

              //---- OBJ_96 ----
              OBJ_96.setText("P\u00e9riodicit\u00e9");
              OBJ_96.setName("OBJ_96");

              //---- BRPER3 ----
              BRPER3.setComponentPopupMenu(BTD);
              BRPER3.setName("BRPER3");

              GroupLayout panel6Layout = new GroupLayout(panel6);
              panel6.setLayout(panel6Layout);
              panel6Layout.setHorizontalGroup(
                panel6Layout.createParallelGroup()
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(BRART3, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                    .addGap(43, 43, 43)
                    .addComponent(BRPER3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
              );
              panel6Layout.setVerticalGroup(
                panel6Layout.createParallelGroup()
                  .addGroup(panel6Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, 0)
                    .addComponent(BRART3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addGroup(panel6Layout.createParallelGroup()
                      .addGroup(panel6Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_96, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BRPER3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            panel1.add(panel6);
            panel6.setBounds(805, 372, 210, 135);

            //======== panel7 ========
            {
              panel7.setBorder(new TitledBorder("CA pr\u00e9visionnel"));
              panel7.setOpaque(false);
              panel7.setName("panel7");

              //---- BRCAP ----
              BRCAP.setComponentPopupMenu(BTD);
              BRCAP.setName("BRCAP");

              GroupLayout panel7Layout = new GroupLayout(panel7);
              panel7.setLayout(panel7Layout);
              panel7Layout.setHorizontalGroup(
                panel7Layout.createParallelGroup()
                  .addComponent(BRCAP, GroupLayout.PREFERRED_SIZE, 96, GroupLayout.PREFERRED_SIZE)
              );
              panel7Layout.setVerticalGroup(
                panel7Layout.createParallelGroup()
                  .addComponent(BRCAP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              );
            }
            panel1.add(panel7);
            panel7.setBounds(805, 512, 210, 75);

            //======== panel5 ========
            {
              panel5.setBorder(new TitledBorder("G\u00e9n\u00e9ration avoir"));
              panel5.setOpaque(false);
              panel5.setName("panel5");

              //---- OBJ_91 ----
              OBJ_91.setText("Article");
              OBJ_91.setName("OBJ_91");

              //---- BRART2 ----
              BRART2.setComponentPopupMenu(BTD);
              BRART2.setName("BRART2");

              //---- BRTOP2 ----
              BRTOP2.setText("");
              BRTOP2.setToolTipText("Avoir en attente");
              BRTOP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              BRTOP2.setName("BRTOP2");

              //---- OBJ_92 ----
              OBJ_92.setText("Type");
              OBJ_92.setName("OBJ_92");

              //---- BRTAV ----
              BRTAV.setComponentPopupMenu(BTD);
              BRTAV.setName("BRTAV");

              //---- OBJ_93 ----
              OBJ_93.setText("Mode r\u00e8glement");
              OBJ_93.setName("OBJ_93");

              //---- OBJ_94 ----
              OBJ_94.setText("Ech\u00e9ance");
              OBJ_94.setName("OBJ_94");

              //---- BRECH ----
              BRECH.setComponentPopupMenu(BTD);
              BRECH.setName("BRECH");

              //---- BRRGL ----
              BRRGL.setComponentPopupMenu(BTD);
              BRRGL.setName("BRRGL");

              GroupLayout panel5Layout = new GroupLayout(panel5);
              panel5.setLayout(panel5Layout);
              panel5Layout.setHorizontalGroup(
                panel5Layout.createParallelGroup()
                  .addComponent(BRART2, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel5Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(panel5Layout.createParallelGroup()
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                        .addGap(120, 120, 120)
                        .addComponent(BRTOP2, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(BRTAV, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addComponent(OBJ_93, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)
                        .addComponent(BRRGL, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addComponent(OBJ_94, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)
                        .addComponent(BRECH, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))))
              );
              panel5Layout.setVerticalGroup(
                panel5Layout.createParallelGroup()
                  .addGroup(panel5Layout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addGroup(panel5Layout.createParallelGroup()
                      .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BRTOP2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addComponent(BRART2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addGroup(panel5Layout.createParallelGroup()
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_92, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BRTAV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addGroup(panel5Layout.createParallelGroup()
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addComponent(OBJ_93, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BRRGL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(5, 5, 5)
                    .addGroup(panel5Layout.createParallelGroup()
                      .addGroup(panel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(OBJ_94, GroupLayout.PREFERRED_SIZE, 17, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BRECH, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }
            panel1.add(panel5);
            panel5.setBounds(805, 172, 210, 200);

            //---- OBJ_52 ----
            OBJ_52.setText("du");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(105, 172, 18, 20);

            //---- OBJ_54 ----
            OBJ_54.setText("au");
            OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_54.setName("OBJ_54");
            panel1.add(OBJ_54);
            OBJ_54.setBounds(265, 172, 50, 20);

            //---- WDEB1X ----
            WDEB1X.setName("WDEB1X");
            panel1.add(WDEB1X);
            WDEB1X.setBounds(160, 167, 105, WDEB1X.getPreferredSize().height);

            //---- WFIN1X ----
            WFIN1X.setName("WFIN1X");
            panel1.add(WFIN1X);
            WFIN1X.setBounds(315, 167, 105, WFIN1X.getPreferredSize().height);

            //---- R1011 ----
            R1011.setHorizontalAlignment(SwingConstants.RIGHT);
            R1011.setName("R1011");
            panel1.add(R1011);
            R1011.setBounds(105, 212, 80, R1011.getPreferredSize().height);

            //---- R1021 ----
            R1021.setHorizontalAlignment(SwingConstants.RIGHT);
            R1021.setName("R1021");
            panel1.add(R1021);
            R1021.setBounds(105, 237, 80, R1021.getPreferredSize().height);

            //---- R1031 ----
            R1031.setHorizontalAlignment(SwingConstants.RIGHT);
            R1031.setName("R1031");
            panel1.add(R1031);
            R1031.setBounds(105, 262, 80, R1031.getPreferredSize().height);

            //---- R1041 ----
            R1041.setHorizontalAlignment(SwingConstants.RIGHT);
            R1041.setName("R1041");
            panel1.add(R1041);
            R1041.setBounds(105, 287, 80, R1041.getPreferredSize().height);

            //---- R1051 ----
            R1051.setHorizontalAlignment(SwingConstants.RIGHT);
            R1051.setName("R1051");
            panel1.add(R1051);
            R1051.setBounds(105, 312, 80, R1051.getPreferredSize().height);

            //---- R1061 ----
            R1061.setHorizontalAlignment(SwingConstants.RIGHT);
            R1061.setName("R1061");
            panel1.add(R1061);
            R1061.setBounds(105, 337, 80, R1061.getPreferredSize().height);

            //---- R1071 ----
            R1071.setHorizontalAlignment(SwingConstants.RIGHT);
            R1071.setName("R1071");
            panel1.add(R1071);
            R1071.setBounds(105, 362, 80, R1071.getPreferredSize().height);

            //---- R1081 ----
            R1081.setHorizontalAlignment(SwingConstants.RIGHT);
            R1081.setName("R1081");
            panel1.add(R1081);
            R1081.setBounds(105, 387, 80, R1081.getPreferredSize().height);

            //---- R1091 ----
            R1091.setHorizontalAlignment(SwingConstants.RIGHT);
            R1091.setName("R1091");
            panel1.add(R1091);
            R1091.setBounds(105, 412, 80, R1091.getPreferredSize().height);

            //---- R1101 ----
            R1101.setHorizontalAlignment(SwingConstants.RIGHT);
            R1101.setName("R1101");
            panel1.add(R1101);
            R1101.setBounds(105, 437, 80, R1101.getPreferredSize().height);

            //---- R1111 ----
            R1111.setHorizontalAlignment(SwingConstants.RIGHT);
            R1111.setName("R1111");
            panel1.add(R1111);
            R1111.setBounds(105, 462, 80, R1111.getPreferredSize().height);

            //---- R1121 ----
            R1121.setHorizontalAlignment(SwingConstants.RIGHT);
            R1121.setName("R1121");
            panel1.add(R1121);
            R1121.setBounds(105, 487, 80, R1121.getPreferredSize().height);

            //---- R1131 ----
            R1131.setHorizontalAlignment(SwingConstants.RIGHT);
            R1131.setName("R1131");
            panel1.add(R1131);
            R1131.setBounds(105, 512, 80, R1131.getPreferredSize().height);

            //---- R1141 ----
            R1141.setHorizontalAlignment(SwingConstants.RIGHT);
            R1141.setName("R1141");
            panel1.add(R1141);
            R1141.setBounds(105, 537, 80, R1141.getPreferredSize().height);

            //---- R11511 ----
            R11511.setHorizontalAlignment(SwingConstants.RIGHT);
            R11511.setName("R11511");
            panel1.add(R11511);
            R11511.setBounds(105, 562, 80, R11511.getPreferredSize().height);

            //---- R1012 ----
            R1012.setHorizontalAlignment(SwingConstants.RIGHT);
            R1012.setName("R1012");
            panel1.add(R1012);
            R1012.setBounds(182, 212, 80, R1012.getPreferredSize().height);

            //---- R1022 ----
            R1022.setHorizontalAlignment(SwingConstants.RIGHT);
            R1022.setName("R1022");
            panel1.add(R1022);
            R1022.setBounds(182, 237, 80, R1022.getPreferredSize().height);

            //---- R1032 ----
            R1032.setHorizontalAlignment(SwingConstants.RIGHT);
            R1032.setName("R1032");
            panel1.add(R1032);
            R1032.setBounds(182, 262, 80, R1032.getPreferredSize().height);

            //---- R1042 ----
            R1042.setHorizontalAlignment(SwingConstants.RIGHT);
            R1042.setName("R1042");
            panel1.add(R1042);
            R1042.setBounds(182, 287, 80, R1042.getPreferredSize().height);

            //---- R1052 ----
            R1052.setHorizontalAlignment(SwingConstants.RIGHT);
            R1052.setName("R1052");
            panel1.add(R1052);
            R1052.setBounds(182, 312, 80, R1052.getPreferredSize().height);

            //---- R1062 ----
            R1062.setHorizontalAlignment(SwingConstants.RIGHT);
            R1062.setName("R1062");
            panel1.add(R1062);
            R1062.setBounds(182, 337, 80, R1062.getPreferredSize().height);

            //---- R1072 ----
            R1072.setHorizontalAlignment(SwingConstants.RIGHT);
            R1072.setName("R1072");
            panel1.add(R1072);
            R1072.setBounds(182, 362, 80, R1072.getPreferredSize().height);

            //---- R1082 ----
            R1082.setHorizontalAlignment(SwingConstants.RIGHT);
            R1082.setName("R1082");
            panel1.add(R1082);
            R1082.setBounds(182, 387, 80, R1082.getPreferredSize().height);

            //---- R1092 ----
            R1092.setHorizontalAlignment(SwingConstants.RIGHT);
            R1092.setName("R1092");
            panel1.add(R1092);
            R1092.setBounds(182, 412, 80, R1092.getPreferredSize().height);

            //---- R1102 ----
            R1102.setHorizontalAlignment(SwingConstants.RIGHT);
            R1102.setName("R1102");
            panel1.add(R1102);
            R1102.setBounds(182, 437, 80, R1102.getPreferredSize().height);

            //---- R1112 ----
            R1112.setHorizontalAlignment(SwingConstants.RIGHT);
            R1112.setName("R1112");
            panel1.add(R1112);
            R1112.setBounds(182, 462, 80, R1112.getPreferredSize().height);

            //---- R1122 ----
            R1122.setHorizontalAlignment(SwingConstants.RIGHT);
            R1122.setName("R1122");
            panel1.add(R1122);
            R1122.setBounds(182, 487, 80, R1122.getPreferredSize().height);

            //---- R1132 ----
            R1132.setHorizontalAlignment(SwingConstants.RIGHT);
            R1132.setName("R1132");
            panel1.add(R1132);
            R1132.setBounds(182, 512, 80, R1132.getPreferredSize().height);

            //---- R1142 ----
            R1142.setHorizontalAlignment(SwingConstants.RIGHT);
            R1142.setName("R1142");
            panel1.add(R1142);
            R1142.setBounds(182, 537, 80, R1142.getPreferredSize().height);

            //---- R1152 ----
            R1152.setHorizontalAlignment(SwingConstants.RIGHT);
            R1152.setName("R1152");
            panel1.add(R1152);
            R1152.setBounds(182, 562, 80, R1152.getPreferredSize().height);

            //---- R1013 ----
            R1013.setHorizontalAlignment(SwingConstants.RIGHT);
            R1013.setName("R1013");
            panel1.add(R1013);
            R1013.setBounds(260, 212, 80, R1013.getPreferredSize().height);

            //---- R1023 ----
            R1023.setHorizontalAlignment(SwingConstants.RIGHT);
            R1023.setName("R1023");
            panel1.add(R1023);
            R1023.setBounds(260, 237, 80, R1023.getPreferredSize().height);

            //---- R1033 ----
            R1033.setHorizontalAlignment(SwingConstants.RIGHT);
            R1033.setName("R1033");
            panel1.add(R1033);
            R1033.setBounds(260, 262, 80, R1033.getPreferredSize().height);

            //---- R1043 ----
            R1043.setHorizontalAlignment(SwingConstants.RIGHT);
            R1043.setName("R1043");
            panel1.add(R1043);
            R1043.setBounds(260, 287, 80, R1043.getPreferredSize().height);

            //---- R1053 ----
            R1053.setHorizontalAlignment(SwingConstants.RIGHT);
            R1053.setName("R1053");
            panel1.add(R1053);
            R1053.setBounds(260, 312, 80, R1053.getPreferredSize().height);

            //---- R1063 ----
            R1063.setHorizontalAlignment(SwingConstants.RIGHT);
            R1063.setName("R1063");
            panel1.add(R1063);
            R1063.setBounds(260, 337, 80, R1063.getPreferredSize().height);

            //---- R1073 ----
            R1073.setHorizontalAlignment(SwingConstants.RIGHT);
            R1073.setName("R1073");
            panel1.add(R1073);
            R1073.setBounds(260, 362, 80, R1073.getPreferredSize().height);

            //---- R1083 ----
            R1083.setHorizontalAlignment(SwingConstants.RIGHT);
            R1083.setName("R1083");
            panel1.add(R1083);
            R1083.setBounds(260, 387, 80, R1083.getPreferredSize().height);

            //---- R1093 ----
            R1093.setHorizontalAlignment(SwingConstants.RIGHT);
            R1093.setName("R1093");
            panel1.add(R1093);
            R1093.setBounds(260, 412, 80, R1093.getPreferredSize().height);

            //---- R1103 ----
            R1103.setHorizontalAlignment(SwingConstants.RIGHT);
            R1103.setName("R1103");
            panel1.add(R1103);
            R1103.setBounds(260, 437, 80, R1103.getPreferredSize().height);

            //---- R1113 ----
            R1113.setHorizontalAlignment(SwingConstants.RIGHT);
            R1113.setName("R1113");
            panel1.add(R1113);
            R1113.setBounds(260, 462, 80, R1113.getPreferredSize().height);

            //---- R1123 ----
            R1123.setHorizontalAlignment(SwingConstants.RIGHT);
            R1123.setName("R1123");
            panel1.add(R1123);
            R1123.setBounds(260, 487, 80, R1123.getPreferredSize().height);

            //---- R1133 ----
            R1133.setHorizontalAlignment(SwingConstants.RIGHT);
            R1133.setName("R1133");
            panel1.add(R1133);
            R1133.setBounds(260, 512, 80, R1133.getPreferredSize().height);

            //---- R1143 ----
            R1143.setHorizontalAlignment(SwingConstants.RIGHT);
            R1143.setName("R1143");
            panel1.add(R1143);
            R1143.setBounds(260, 537, 80, R1143.getPreferredSize().height);

            //---- R1153 ----
            R1153.setHorizontalAlignment(SwingConstants.RIGHT);
            R1153.setName("R1153");
            panel1.add(R1153);
            R1153.setBounds(260, 562, 80, R1153.getPreferredSize().height);

            //---- R1014 ----
            R1014.setHorizontalAlignment(SwingConstants.RIGHT);
            R1014.setName("R1014");
            panel1.add(R1014);
            R1014.setBounds(337, 212, 80, R1014.getPreferredSize().height);

            //---- R1024 ----
            R1024.setHorizontalAlignment(SwingConstants.RIGHT);
            R1024.setName("R1024");
            panel1.add(R1024);
            R1024.setBounds(337, 237, 80, R1024.getPreferredSize().height);

            //---- R1034 ----
            R1034.setHorizontalAlignment(SwingConstants.RIGHT);
            R1034.setName("R1034");
            panel1.add(R1034);
            R1034.setBounds(337, 262, 80, R1034.getPreferredSize().height);

            //---- R1044 ----
            R1044.setHorizontalAlignment(SwingConstants.RIGHT);
            R1044.setName("R1044");
            panel1.add(R1044);
            R1044.setBounds(337, 287, 80, R1044.getPreferredSize().height);

            //---- R1054 ----
            R1054.setHorizontalAlignment(SwingConstants.RIGHT);
            R1054.setName("R1054");
            panel1.add(R1054);
            R1054.setBounds(337, 312, 80, R1054.getPreferredSize().height);

            //---- R1064 ----
            R1064.setHorizontalAlignment(SwingConstants.RIGHT);
            R1064.setName("R1064");
            panel1.add(R1064);
            R1064.setBounds(337, 337, 80, R1064.getPreferredSize().height);

            //---- R1074 ----
            R1074.setHorizontalAlignment(SwingConstants.RIGHT);
            R1074.setName("R1074");
            panel1.add(R1074);
            R1074.setBounds(337, 362, 80, R1074.getPreferredSize().height);

            //---- R1084 ----
            R1084.setHorizontalAlignment(SwingConstants.RIGHT);
            R1084.setName("R1084");
            panel1.add(R1084);
            R1084.setBounds(337, 387, 80, R1084.getPreferredSize().height);

            //---- R1094 ----
            R1094.setHorizontalAlignment(SwingConstants.RIGHT);
            R1094.setName("R1094");
            panel1.add(R1094);
            R1094.setBounds(337, 412, 80, R1094.getPreferredSize().height);

            //---- R1104 ----
            R1104.setHorizontalAlignment(SwingConstants.RIGHT);
            R1104.setName("R1104");
            panel1.add(R1104);
            R1104.setBounds(337, 437, 80, R1104.getPreferredSize().height);

            //---- R1114 ----
            R1114.setHorizontalAlignment(SwingConstants.RIGHT);
            R1114.setName("R1114");
            panel1.add(R1114);
            R1114.setBounds(337, 462, 80, R1114.getPreferredSize().height);

            //---- R1124 ----
            R1124.setHorizontalAlignment(SwingConstants.RIGHT);
            R1124.setName("R1124");
            panel1.add(R1124);
            R1124.setBounds(337, 487, 80, R1124.getPreferredSize().height);

            //---- R1134 ----
            R1134.setHorizontalAlignment(SwingConstants.RIGHT);
            R1134.setName("R1134");
            panel1.add(R1134);
            R1134.setBounds(337, 512, 80, R1134.getPreferredSize().height);

            //---- R1144 ----
            R1144.setHorizontalAlignment(SwingConstants.RIGHT);
            R1144.setName("R1144");
            panel1.add(R1144);
            R1144.setBounds(337, 537, 80, R1144.getPreferredSize().height);

            //---- R1154 ----
            R1154.setHorizontalAlignment(SwingConstants.RIGHT);
            R1154.setName("R1154");
            panel1.add(R1154);
            R1154.setBounds(337, 562, 80, R1154.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            panel1.add(separator2);
            separator2.setBounds(105, 197, 310, 15);

            //---- OBJ_56 ----
            OBJ_56.setText("du");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(475, 172, 18, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("au");
            OBJ_58.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58);
            OBJ_58.setBounds(635, 172, 53, 20);

            //---- WDEB2X ----
            WDEB2X.setName("WDEB2X");
            panel1.add(WDEB2X);
            WDEB2X.setBounds(530, 167, 105, WDEB2X.getPreferredSize().height);

            //---- WFIN2X ----
            WFIN2X.setName("WFIN2X");
            panel1.add(WFIN2X);
            WFIN2X.setBounds(685, 167, 105, WFIN2X.getPreferredSize().height);

            //---- R2011 ----
            R2011.setHorizontalAlignment(SwingConstants.RIGHT);
            R2011.setName("R2011");
            panel1.add(R2011);
            R2011.setBounds(475, 212, 80, R2011.getPreferredSize().height);

            //---- R2021 ----
            R2021.setHorizontalAlignment(SwingConstants.RIGHT);
            R2021.setName("R2021");
            panel1.add(R2021);
            R2021.setBounds(475, 237, 80, R2021.getPreferredSize().height);

            //---- R2031 ----
            R2031.setHorizontalAlignment(SwingConstants.RIGHT);
            R2031.setName("R2031");
            panel1.add(R2031);
            R2031.setBounds(475, 262, 80, R2031.getPreferredSize().height);

            //---- R2041 ----
            R2041.setHorizontalAlignment(SwingConstants.RIGHT);
            R2041.setName("R2041");
            panel1.add(R2041);
            R2041.setBounds(475, 287, 80, R2041.getPreferredSize().height);

            //---- R2051 ----
            R2051.setHorizontalAlignment(SwingConstants.RIGHT);
            R2051.setName("R2051");
            panel1.add(R2051);
            R2051.setBounds(475, 312, 80, R2051.getPreferredSize().height);

            //---- R2061 ----
            R2061.setHorizontalAlignment(SwingConstants.RIGHT);
            R2061.setName("R2061");
            panel1.add(R2061);
            R2061.setBounds(475, 337, 80, R2061.getPreferredSize().height);

            //---- R2071 ----
            R2071.setHorizontalAlignment(SwingConstants.RIGHT);
            R2071.setName("R2071");
            panel1.add(R2071);
            R2071.setBounds(475, 362, 80, R2071.getPreferredSize().height);

            //---- R2081 ----
            R2081.setHorizontalAlignment(SwingConstants.RIGHT);
            R2081.setName("R2081");
            panel1.add(R2081);
            R2081.setBounds(475, 387, 80, R2081.getPreferredSize().height);

            //---- R2091 ----
            R2091.setHorizontalAlignment(SwingConstants.RIGHT);
            R2091.setName("R2091");
            panel1.add(R2091);
            R2091.setBounds(475, 412, 80, R2091.getPreferredSize().height);

            //---- R2101 ----
            R2101.setHorizontalAlignment(SwingConstants.RIGHT);
            R2101.setName("R2101");
            panel1.add(R2101);
            R2101.setBounds(475, 437, 80, R2101.getPreferredSize().height);

            //---- R2111 ----
            R2111.setHorizontalAlignment(SwingConstants.RIGHT);
            R2111.setName("R2111");
            panel1.add(R2111);
            R2111.setBounds(475, 462, 80, R2111.getPreferredSize().height);

            //---- R2121 ----
            R2121.setHorizontalAlignment(SwingConstants.RIGHT);
            R2121.setName("R2121");
            panel1.add(R2121);
            R2121.setBounds(475, 487, 80, R2121.getPreferredSize().height);

            //---- R2131 ----
            R2131.setHorizontalAlignment(SwingConstants.RIGHT);
            R2131.setName("R2131");
            panel1.add(R2131);
            R2131.setBounds(475, 512, 80, R2131.getPreferredSize().height);

            //---- R2141 ----
            R2141.setHorizontalAlignment(SwingConstants.RIGHT);
            R2141.setName("R2141");
            panel1.add(R2141);
            R2141.setBounds(475, 537, 80, R2141.getPreferredSize().height);

            //---- R2151 ----
            R2151.setHorizontalAlignment(SwingConstants.RIGHT);
            R2151.setName("R2151");
            panel1.add(R2151);
            R2151.setBounds(475, 562, 80, R2151.getPreferredSize().height);

            //---- R2012 ----
            R2012.setHorizontalAlignment(SwingConstants.RIGHT);
            R2012.setName("R2012");
            panel1.add(R2012);
            R2012.setBounds(552, 212, 80, R2012.getPreferredSize().height);

            //---- R2022 ----
            R2022.setHorizontalAlignment(SwingConstants.RIGHT);
            R2022.setName("R2022");
            panel1.add(R2022);
            R2022.setBounds(552, 237, 80, R2022.getPreferredSize().height);

            //---- R2032 ----
            R2032.setHorizontalAlignment(SwingConstants.RIGHT);
            R2032.setName("R2032");
            panel1.add(R2032);
            R2032.setBounds(552, 262, 80, R2032.getPreferredSize().height);

            //---- R2042 ----
            R2042.setHorizontalAlignment(SwingConstants.RIGHT);
            R2042.setName("R2042");
            panel1.add(R2042);
            R2042.setBounds(552, 287, 80, R2042.getPreferredSize().height);

            //---- R2052 ----
            R2052.setHorizontalAlignment(SwingConstants.RIGHT);
            R2052.setName("R2052");
            panel1.add(R2052);
            R2052.setBounds(552, 312, 80, R2052.getPreferredSize().height);

            //---- R2062 ----
            R2062.setHorizontalAlignment(SwingConstants.RIGHT);
            R2062.setName("R2062");
            panel1.add(R2062);
            R2062.setBounds(552, 337, 80, R2062.getPreferredSize().height);

            //---- R2072 ----
            R2072.setHorizontalAlignment(SwingConstants.RIGHT);
            R2072.setName("R2072");
            panel1.add(R2072);
            R2072.setBounds(552, 362, 80, R2072.getPreferredSize().height);

            //---- R2082 ----
            R2082.setHorizontalAlignment(SwingConstants.RIGHT);
            R2082.setName("R2082");
            panel1.add(R2082);
            R2082.setBounds(552, 387, 80, R2082.getPreferredSize().height);

            //---- R2092 ----
            R2092.setHorizontalAlignment(SwingConstants.RIGHT);
            R2092.setName("R2092");
            panel1.add(R2092);
            R2092.setBounds(552, 412, 80, R2092.getPreferredSize().height);

            //---- R2102 ----
            R2102.setHorizontalAlignment(SwingConstants.RIGHT);
            R2102.setName("R2102");
            panel1.add(R2102);
            R2102.setBounds(552, 437, 80, R2102.getPreferredSize().height);

            //---- R2112 ----
            R2112.setHorizontalAlignment(SwingConstants.RIGHT);
            R2112.setName("R2112");
            panel1.add(R2112);
            R2112.setBounds(552, 462, 80, R2112.getPreferredSize().height);

            //---- R2122 ----
            R2122.setHorizontalAlignment(SwingConstants.RIGHT);
            R2122.setName("R2122");
            panel1.add(R2122);
            R2122.setBounds(552, 487, 80, R2122.getPreferredSize().height);

            //---- R2132 ----
            R2132.setHorizontalAlignment(SwingConstants.RIGHT);
            R2132.setName("R2132");
            panel1.add(R2132);
            R2132.setBounds(552, 512, 80, R2132.getPreferredSize().height);

            //---- R2142 ----
            R2142.setHorizontalAlignment(SwingConstants.RIGHT);
            R2142.setName("R2142");
            panel1.add(R2142);
            R2142.setBounds(552, 537, 80, R2142.getPreferredSize().height);

            //---- R2152 ----
            R2152.setHorizontalAlignment(SwingConstants.RIGHT);
            R2152.setName("R2152");
            panel1.add(R2152);
            R2152.setBounds(552, 562, 80, R2152.getPreferredSize().height);

            //---- R2013 ----
            R2013.setHorizontalAlignment(SwingConstants.RIGHT);
            R2013.setName("R2013");
            panel1.add(R2013);
            R2013.setBounds(630, 212, 80, R2013.getPreferredSize().height);

            //---- R2023 ----
            R2023.setHorizontalAlignment(SwingConstants.RIGHT);
            R2023.setName("R2023");
            panel1.add(R2023);
            R2023.setBounds(630, 237, 80, R2023.getPreferredSize().height);

            //---- R2033 ----
            R2033.setHorizontalAlignment(SwingConstants.RIGHT);
            R2033.setName("R2033");
            panel1.add(R2033);
            R2033.setBounds(630, 262, 80, R2033.getPreferredSize().height);

            //---- R2043 ----
            R2043.setHorizontalAlignment(SwingConstants.RIGHT);
            R2043.setName("R2043");
            panel1.add(R2043);
            R2043.setBounds(630, 287, 80, R2043.getPreferredSize().height);

            //---- R2053 ----
            R2053.setHorizontalAlignment(SwingConstants.RIGHT);
            R2053.setName("R2053");
            panel1.add(R2053);
            R2053.setBounds(630, 312, 80, R2053.getPreferredSize().height);

            //---- R2063 ----
            R2063.setHorizontalAlignment(SwingConstants.RIGHT);
            R2063.setName("R2063");
            panel1.add(R2063);
            R2063.setBounds(630, 337, 80, R2063.getPreferredSize().height);

            //---- R2073 ----
            R2073.setHorizontalAlignment(SwingConstants.RIGHT);
            R2073.setName("R2073");
            panel1.add(R2073);
            R2073.setBounds(630, 362, 80, R2073.getPreferredSize().height);

            //---- R2083 ----
            R2083.setHorizontalAlignment(SwingConstants.RIGHT);
            R2083.setName("R2083");
            panel1.add(R2083);
            R2083.setBounds(630, 387, 80, R2083.getPreferredSize().height);

            //---- R2093 ----
            R2093.setHorizontalAlignment(SwingConstants.RIGHT);
            R2093.setName("R2093");
            panel1.add(R2093);
            R2093.setBounds(630, 412, 80, R2093.getPreferredSize().height);

            //---- R2103 ----
            R2103.setHorizontalAlignment(SwingConstants.RIGHT);
            R2103.setName("R2103");
            panel1.add(R2103);
            R2103.setBounds(630, 437, 80, R2103.getPreferredSize().height);

            //---- R2113 ----
            R2113.setHorizontalAlignment(SwingConstants.RIGHT);
            R2113.setName("R2113");
            panel1.add(R2113);
            R2113.setBounds(630, 462, 80, R2113.getPreferredSize().height);

            //---- R2123 ----
            R2123.setHorizontalAlignment(SwingConstants.RIGHT);
            R2123.setName("R2123");
            panel1.add(R2123);
            R2123.setBounds(630, 487, 80, R2123.getPreferredSize().height);

            //---- R2133 ----
            R2133.setHorizontalAlignment(SwingConstants.RIGHT);
            R2133.setName("R2133");
            panel1.add(R2133);
            R2133.setBounds(630, 512, 80, R2133.getPreferredSize().height);

            //---- R2143 ----
            R2143.setHorizontalAlignment(SwingConstants.RIGHT);
            R2143.setName("R2143");
            panel1.add(R2143);
            R2143.setBounds(630, 537, 80, R2143.getPreferredSize().height);

            //---- R2153 ----
            R2153.setHorizontalAlignment(SwingConstants.RIGHT);
            R2153.setName("R2153");
            panel1.add(R2153);
            R2153.setBounds(630, 562, 80, R2153.getPreferredSize().height);

            //---- R2014 ----
            R2014.setHorizontalAlignment(SwingConstants.RIGHT);
            R2014.setName("R2014");
            panel1.add(R2014);
            R2014.setBounds(707, 212, 80, R2014.getPreferredSize().height);

            //---- R2024 ----
            R2024.setHorizontalAlignment(SwingConstants.RIGHT);
            R2024.setName("R2024");
            panel1.add(R2024);
            R2024.setBounds(707, 237, 80, R2024.getPreferredSize().height);

            //---- R2034 ----
            R2034.setHorizontalAlignment(SwingConstants.RIGHT);
            R2034.setName("R2034");
            panel1.add(R2034);
            R2034.setBounds(707, 262, 80, R2034.getPreferredSize().height);

            //---- R2044 ----
            R2044.setHorizontalAlignment(SwingConstants.RIGHT);
            R2044.setName("R2044");
            panel1.add(R2044);
            R2044.setBounds(707, 287, 80, R2044.getPreferredSize().height);

            //---- R2054 ----
            R2054.setHorizontalAlignment(SwingConstants.RIGHT);
            R2054.setName("R2054");
            panel1.add(R2054);
            R2054.setBounds(707, 312, 80, R2054.getPreferredSize().height);

            //---- R2064 ----
            R2064.setHorizontalAlignment(SwingConstants.RIGHT);
            R2064.setName("R2064");
            panel1.add(R2064);
            R2064.setBounds(707, 337, 80, R2064.getPreferredSize().height);

            //---- R2074 ----
            R2074.setHorizontalAlignment(SwingConstants.RIGHT);
            R2074.setName("R2074");
            panel1.add(R2074);
            R2074.setBounds(707, 362, 80, R2074.getPreferredSize().height);

            //---- R2084 ----
            R2084.setHorizontalAlignment(SwingConstants.RIGHT);
            R2084.setName("R2084");
            panel1.add(R2084);
            R2084.setBounds(707, 387, 80, R2084.getPreferredSize().height);

            //---- R2094 ----
            R2094.setHorizontalAlignment(SwingConstants.RIGHT);
            R2094.setName("R2094");
            panel1.add(R2094);
            R2094.setBounds(707, 412, 80, R2094.getPreferredSize().height);

            //---- R2104 ----
            R2104.setHorizontalAlignment(SwingConstants.RIGHT);
            R2104.setName("R2104");
            panel1.add(R2104);
            R2104.setBounds(707, 437, 80, R2104.getPreferredSize().height);

            //---- R2114 ----
            R2114.setHorizontalAlignment(SwingConstants.RIGHT);
            R2114.setName("R2114");
            panel1.add(R2114);
            R2114.setBounds(707, 462, 80, R2114.getPreferredSize().height);

            //---- R2124 ----
            R2124.setHorizontalAlignment(SwingConstants.RIGHT);
            R2124.setName("R2124");
            panel1.add(R2124);
            R2124.setBounds(707, 487, 80, R2124.getPreferredSize().height);

            //---- R2134 ----
            R2134.setHorizontalAlignment(SwingConstants.RIGHT);
            R2134.setName("R2134");
            panel1.add(R2134);
            R2134.setBounds(707, 512, 80, R2134.getPreferredSize().height);

            //---- R2144 ----
            R2144.setHorizontalAlignment(SwingConstants.RIGHT);
            R2144.setName("R2144");
            panel1.add(R2144);
            R2144.setBounds(707, 537, 80, R2144.getPreferredSize().height);

            //---- R2154 ----
            R2154.setHorizontalAlignment(SwingConstants.RIGHT);
            R2154.setName("R2154");
            panel1.add(R2154);
            R2154.setBounds(707, 562, 80, R2154.getPreferredSize().height);

            //---- separator3 ----
            separator3.setName("separator3");
            panel1.add(separator3);
            separator3.setBounds(475, 197, 310, 15);

            //---- Q101 ----
            Q101.setHorizontalAlignment(SwingConstants.RIGHT);
            Q101.setName("Q101");
            panel1.add(Q101);
            Q101.setBounds(15, 212, 80, Q101.getPreferredSize().height);

            //---- Q102 ----
            Q102.setHorizontalAlignment(SwingConstants.RIGHT);
            Q102.setName("Q102");
            panel1.add(Q102);
            Q102.setBounds(15, 237, 80, Q102.getPreferredSize().height);

            //---- Q103 ----
            Q103.setHorizontalAlignment(SwingConstants.RIGHT);
            Q103.setName("Q103");
            panel1.add(Q103);
            Q103.setBounds(15, 262, 80, Q103.getPreferredSize().height);

            //---- Q104 ----
            Q104.setHorizontalAlignment(SwingConstants.RIGHT);
            Q104.setName("Q104");
            panel1.add(Q104);
            Q104.setBounds(15, 287, 80, Q104.getPreferredSize().height);

            //---- Q105 ----
            Q105.setHorizontalAlignment(SwingConstants.RIGHT);
            Q105.setName("Q105");
            panel1.add(Q105);
            Q105.setBounds(15, 312, 80, Q105.getPreferredSize().height);

            //---- Q106 ----
            Q106.setHorizontalAlignment(SwingConstants.RIGHT);
            Q106.setName("Q106");
            panel1.add(Q106);
            Q106.setBounds(15, 337, 80, Q106.getPreferredSize().height);

            //---- Q107 ----
            Q107.setHorizontalAlignment(SwingConstants.RIGHT);
            Q107.setName("Q107");
            panel1.add(Q107);
            Q107.setBounds(15, 362, 80, Q107.getPreferredSize().height);

            //---- Q108 ----
            Q108.setHorizontalAlignment(SwingConstants.RIGHT);
            Q108.setName("Q108");
            panel1.add(Q108);
            Q108.setBounds(15, 387, 80, Q108.getPreferredSize().height);

            //---- Q109 ----
            Q109.setHorizontalAlignment(SwingConstants.RIGHT);
            Q109.setName("Q109");
            panel1.add(Q109);
            Q109.setBounds(15, 412, 80, Q109.getPreferredSize().height);

            //---- Q110 ----
            Q110.setHorizontalAlignment(SwingConstants.RIGHT);
            Q110.setName("Q110");
            panel1.add(Q110);
            Q110.setBounds(15, 437, 80, Q110.getPreferredSize().height);

            //---- Q111 ----
            Q111.setHorizontalAlignment(SwingConstants.RIGHT);
            Q111.setName("Q111");
            panel1.add(Q111);
            Q111.setBounds(15, 462, 80, Q111.getPreferredSize().height);

            //---- Q112 ----
            Q112.setHorizontalAlignment(SwingConstants.RIGHT);
            Q112.setName("Q112");
            panel1.add(Q112);
            Q112.setBounds(15, 487, 80, Q112.getPreferredSize().height);

            //---- Q113 ----
            Q113.setHorizontalAlignment(SwingConstants.RIGHT);
            Q113.setName("Q113");
            panel1.add(Q113);
            Q113.setBounds(15, 512, 80, Q113.getPreferredSize().height);

            //---- Q114 ----
            Q114.setHorizontalAlignment(SwingConstants.RIGHT);
            Q114.setName("Q114");
            panel1.add(Q114);
            Q114.setBounds(15, 537, 80, Q114.getPreferredSize().height);

            //---- Q115 ----
            Q115.setHorizontalAlignment(SwingConstants.RIGHT);
            Q115.setName("Q115");
            panel1.add(Q115);
            Q115.setBounds(15, 562, 80, Q115.getPreferredSize().height);

            //---- ERR1 ----
            ERR1.setText("@ERR1@");
            ERR1.setHorizontalAlignment(SwingConstants.CENTER);
            ERR1.setForeground(new Color(153, 0, 0));
            ERR1.setFont(ERR1.getFont().deriveFont(ERR1.getFont().getStyle() | Font.BOLD));
            ERR1.setName("ERR1");
            panel1.add(ERR1);
            ERR1.setBounds(420, 212, 45, 28);

            //---- ERR2 ----
            ERR2.setText("@ERR2@");
            ERR2.setHorizontalAlignment(SwingConstants.CENTER);
            ERR2.setForeground(new Color(153, 0, 0));
            ERR2.setFont(ERR2.getFont().deriveFont(ERR2.getFont().getStyle() | Font.BOLD));
            ERR2.setName("ERR2");
            panel1.add(ERR2);
            ERR2.setBounds(420, 237, 45, 28);

            //---- ERR3 ----
            ERR3.setText("@ERR3@");
            ERR3.setHorizontalAlignment(SwingConstants.CENTER);
            ERR3.setForeground(new Color(153, 0, 0));
            ERR3.setFont(ERR3.getFont().deriveFont(ERR3.getFont().getStyle() | Font.BOLD));
            ERR3.setName("ERR3");
            panel1.add(ERR3);
            ERR3.setBounds(420, 262, 45, 28);

            //---- ERR4 ----
            ERR4.setText("@ERR4@");
            ERR4.setHorizontalAlignment(SwingConstants.CENTER);
            ERR4.setForeground(new Color(153, 0, 0));
            ERR4.setFont(ERR4.getFont().deriveFont(ERR4.getFont().getStyle() | Font.BOLD));
            ERR4.setName("ERR4");
            panel1.add(ERR4);
            ERR4.setBounds(420, 287, 45, 28);

            //---- ERR5 ----
            ERR5.setText("@ERR5@");
            ERR5.setHorizontalAlignment(SwingConstants.CENTER);
            ERR5.setForeground(new Color(153, 0, 0));
            ERR5.setFont(ERR5.getFont().deriveFont(ERR5.getFont().getStyle() | Font.BOLD));
            ERR5.setName("ERR5");
            panel1.add(ERR5);
            ERR5.setBounds(420, 312, 45, 28);

            //---- ERR6 ----
            ERR6.setText("@ERR6@");
            ERR6.setHorizontalAlignment(SwingConstants.CENTER);
            ERR6.setForeground(new Color(153, 0, 0));
            ERR6.setFont(ERR6.getFont().deriveFont(ERR6.getFont().getStyle() | Font.BOLD));
            ERR6.setName("ERR6");
            panel1.add(ERR6);
            ERR6.setBounds(420, 337, 45, 28);

            //---- ERR7 ----
            ERR7.setText("@ERR7@");
            ERR7.setHorizontalAlignment(SwingConstants.CENTER);
            ERR7.setForeground(new Color(153, 0, 0));
            ERR7.setFont(ERR7.getFont().deriveFont(ERR7.getFont().getStyle() | Font.BOLD));
            ERR7.setName("ERR7");
            panel1.add(ERR7);
            ERR7.setBounds(420, 362, 45, 28);

            //---- ERR8 ----
            ERR8.setText("@ERR8@");
            ERR8.setHorizontalAlignment(SwingConstants.CENTER);
            ERR8.setForeground(new Color(153, 0, 0));
            ERR8.setFont(ERR8.getFont().deriveFont(ERR8.getFont().getStyle() | Font.BOLD));
            ERR8.setName("ERR8");
            panel1.add(ERR8);
            ERR8.setBounds(420, 387, 45, 28);

            //---- ERR9 ----
            ERR9.setText("@ERR9@");
            ERR9.setHorizontalAlignment(SwingConstants.CENTER);
            ERR9.setForeground(new Color(153, 0, 0));
            ERR9.setFont(ERR9.getFont().deriveFont(ERR9.getFont().getStyle() | Font.BOLD));
            ERR9.setName("ERR9");
            panel1.add(ERR9);
            ERR9.setBounds(420, 412, 45, 28);

            //---- ERR10 ----
            ERR10.setText("@ERR10@");
            ERR10.setHorizontalAlignment(SwingConstants.CENTER);
            ERR10.setForeground(new Color(153, 0, 0));
            ERR10.setFont(ERR10.getFont().deriveFont(ERR10.getFont().getStyle() | Font.BOLD));
            ERR10.setName("ERR10");
            panel1.add(ERR10);
            ERR10.setBounds(420, 437, 45, 28);

            //---- ERR11 ----
            ERR11.setText("@ERR11@");
            ERR11.setHorizontalAlignment(SwingConstants.CENTER);
            ERR11.setForeground(new Color(153, 0, 0));
            ERR11.setFont(ERR11.getFont().deriveFont(ERR11.getFont().getStyle() | Font.BOLD));
            ERR11.setName("ERR11");
            panel1.add(ERR11);
            ERR11.setBounds(420, 462, 45, 28);

            //---- ERR12 ----
            ERR12.setText("@ERR12@");
            ERR12.setHorizontalAlignment(SwingConstants.CENTER);
            ERR12.setForeground(new Color(153, 0, 0));
            ERR12.setFont(ERR12.getFont().deriveFont(ERR12.getFont().getStyle() | Font.BOLD));
            ERR12.setName("ERR12");
            panel1.add(ERR12);
            ERR12.setBounds(420, 487, 45, 28);

            //---- ERR13 ----
            ERR13.setText("@ERR13@");
            ERR13.setHorizontalAlignment(SwingConstants.CENTER);
            ERR13.setForeground(new Color(153, 0, 0));
            ERR13.setFont(ERR13.getFont().deriveFont(ERR13.getFont().getStyle() | Font.BOLD));
            ERR13.setName("ERR13");
            panel1.add(ERR13);
            ERR13.setBounds(420, 512, 45, 28);

            //---- ERR14 ----
            ERR14.setText("@ERR14@");
            ERR14.setHorizontalAlignment(SwingConstants.CENTER);
            ERR14.setForeground(new Color(153, 0, 0));
            ERR14.setFont(ERR14.getFont().deriveFont(ERR14.getFont().getStyle() | Font.BOLD));
            ERR14.setName("ERR14");
            panel1.add(ERR14);
            ERR14.setBounds(420, 537, 45, 28);

            //---- ERR15 ----
            ERR15.setText("@ERR15@");
            ERR15.setHorizontalAlignment(SwingConstants.CENTER);
            ERR15.setForeground(new Color(153, 0, 0));
            ERR15.setFont(ERR15.getFont().deriveFont(ERR15.getFont().getStyle() | Font.BOLD));
            ERR15.setName("ERR15");
            panel1.add(ERR15);
            ERR15.setBounds(420, 562, 45, 28);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(15, 197, 80, separator1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(-5, 0, 1020, 600);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private XRiTextField WETB;
  private SNBoutonRecherche BT_ChgSoc;
  private JLabel OBJ_50;
  private XRiTextField WCNV;
  private XRiTextField BRLIB;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private XRiTextField BRPER1;
  private XRiTextField BRPER2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panel4;
  private JLabel OBJ_97;
  private XRiCheckBox BRTOP1;
  private XRiTextField BRDES1;
  private XRiTextField BRDES2;
  private XRiTextField BRART1;
  private XRiTextField BRDES3;
  private XRiTextField BRDES4;
  private XRiTextField BRDES5;
  private JPanel panel6;
  private XRiTextField BRART3;
  private JLabel OBJ_95;
  private JLabel OBJ_96;
  private XRiTextField BRPER3;
  private JPanel panel7;
  private XRiTextField BRCAP;
  private JPanel panel5;
  private JLabel OBJ_91;
  private XRiTextField BRART2;
  private XRiCheckBox BRTOP2;
  private JLabel OBJ_92;
  private XRiTextField BRTAV;
  private JLabel OBJ_93;
  private JLabel OBJ_94;
  private XRiTextField BRECH;
  private XRiTextField BRRGL;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private XRiCalendrier WDEB1X;
  private XRiCalendrier WFIN1X;
  private XRiTextField R1011;
  private XRiTextField R1021;
  private XRiTextField R1031;
  private XRiTextField R1041;
  private XRiTextField R1051;
  private XRiTextField R1061;
  private XRiTextField R1071;
  private XRiTextField R1081;
  private XRiTextField R1091;
  private XRiTextField R1101;
  private XRiTextField R1111;
  private XRiTextField R1121;
  private XRiTextField R1131;
  private XRiTextField R1141;
  private XRiTextField R11511;
  private XRiTextField R1012;
  private XRiTextField R1022;
  private XRiTextField R1032;
  private XRiTextField R1042;
  private XRiTextField R1052;
  private XRiTextField R1062;
  private XRiTextField R1072;
  private XRiTextField R1082;
  private XRiTextField R1092;
  private XRiTextField R1102;
  private XRiTextField R1112;
  private XRiTextField R1122;
  private XRiTextField R1132;
  private XRiTextField R1142;
  private XRiTextField R1152;
  private XRiTextField R1013;
  private XRiTextField R1023;
  private XRiTextField R1033;
  private XRiTextField R1043;
  private XRiTextField R1053;
  private XRiTextField R1063;
  private XRiTextField R1073;
  private XRiTextField R1083;
  private XRiTextField R1093;
  private XRiTextField R1103;
  private XRiTextField R1113;
  private XRiTextField R1123;
  private XRiTextField R1133;
  private XRiTextField R1143;
  private XRiTextField R1153;
  private XRiTextField R1014;
  private XRiTextField R1024;
  private XRiTextField R1034;
  private XRiTextField R1044;
  private XRiTextField R1054;
  private XRiTextField R1064;
  private XRiTextField R1074;
  private XRiTextField R1084;
  private XRiTextField R1094;
  private XRiTextField R1104;
  private XRiTextField R1114;
  private XRiTextField R1124;
  private XRiTextField R1134;
  private XRiTextField R1144;
  private XRiTextField R1154;
  private JComponent separator2;
  private JLabel OBJ_56;
  private JLabel OBJ_58;
  private XRiCalendrier WDEB2X;
  private XRiCalendrier WFIN2X;
  private XRiTextField R2011;
  private XRiTextField R2021;
  private XRiTextField R2031;
  private XRiTextField R2041;
  private XRiTextField R2051;
  private XRiTextField R2061;
  private XRiTextField R2071;
  private XRiTextField R2081;
  private XRiTextField R2091;
  private XRiTextField R2101;
  private XRiTextField R2111;
  private XRiTextField R2121;
  private XRiTextField R2131;
  private XRiTextField R2141;
  private XRiTextField R2151;
  private XRiTextField R2012;
  private XRiTextField R2022;
  private XRiTextField R2032;
  private XRiTextField R2042;
  private XRiTextField R2052;
  private XRiTextField R2062;
  private XRiTextField R2072;
  private XRiTextField R2082;
  private XRiTextField R2092;
  private XRiTextField R2102;
  private XRiTextField R2112;
  private XRiTextField R2122;
  private XRiTextField R2132;
  private XRiTextField R2142;
  private XRiTextField R2152;
  private XRiTextField R2013;
  private XRiTextField R2023;
  private XRiTextField R2033;
  private XRiTextField R2043;
  private XRiTextField R2053;
  private XRiTextField R2063;
  private XRiTextField R2073;
  private XRiTextField R2083;
  private XRiTextField R2093;
  private XRiTextField R2103;
  private XRiTextField R2113;
  private XRiTextField R2123;
  private XRiTextField R2133;
  private XRiTextField R2143;
  private XRiTextField R2153;
  private XRiTextField R2014;
  private XRiTextField R2024;
  private XRiTextField R2034;
  private XRiTextField R2044;
  private XRiTextField R2054;
  private XRiTextField R2064;
  private XRiTextField R2074;
  private XRiTextField R2084;
  private XRiTextField R2094;
  private XRiTextField R2104;
  private XRiTextField R2114;
  private XRiTextField R2124;
  private XRiTextField R2134;
  private XRiTextField R2144;
  private XRiTextField R2154;
  private JComponent separator3;
  private XRiTextField Q101;
  private XRiTextField Q102;
  private XRiTextField Q103;
  private XRiTextField Q104;
  private XRiTextField Q105;
  private XRiTextField Q106;
  private XRiTextField Q107;
  private XRiTextField Q108;
  private XRiTextField Q109;
  private XRiTextField Q110;
  private XRiTextField Q111;
  private XRiTextField Q112;
  private XRiTextField Q113;
  private XRiTextField Q114;
  private XRiTextField Q115;
  private JLabel ERR1;
  private JLabel ERR2;
  private JLabel ERR3;
  private JLabel ERR4;
  private JLabel ERR5;
  private JLabel ERR6;
  private JLabel ERR7;
  private JLabel ERR8;
  private JLabel ERR9;
  private JLabel ERR10;
  private JLabel ERR11;
  private JLabel ERR12;
  private JLabel ERR13;
  private JLabel ERR14;
  private JLabel ERR15;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
