
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.ModeleListeDocumentStocke;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.VueDocumentStocke;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.sntypefacturation.SNTypeFacturation;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.contact.listecontacttiers.DialogueListeContactTiers;
import ri.serien.libswing.composant.metier.referentiel.contact.listecontacttiers.ModeleListeContactTiers;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.sntransporteur.SNTransporteur;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.conditionvente.snconditionvente.SNConditionVenteEtClient;
import ri.serien.libswing.composant.metier.vente.documentvente.snmodeexpedition.SNModeExpedition;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNAdresseMail;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.outil.clipboard.Clipboard;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  private String[] CLTNS_Value = { "0", "1", "2", "5", "6", "9" };
  private String topErreur = "19";
  private String[] CLIN23_Value = { "1", "2", "3", "4" };
  private ODialog dialog_ZP = null;
  private Icon bloc_couleur = null;
  private Icon bloc_neutre = null;
  private Icon idesactive = null;
  private Icon iinterdit = null;
  private boolean isClientParticulier = false;
  private boolean isChargement = true;
  private boolean isCreation = false;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   * @param param paramètres de l'écran
   */
  public VGVM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    CLTNS.setValeurs(CLTNS_Value, null);
    CLIN23.setValeurs(CLIN23_Value, null);
    
    // Categorie
    CLIN9.setValeurs("", buttonGroupCategorie);
    CLIN9_1.setValeurs("1");
    
    initDiverses();
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/reduire.png", true));
    bloc_couleur = lexique.chargerImage("images/bloc_notes.png", true);
    bloc_neutre = lexique.chargerImage("images/bloc_notes_r.png", true);
    idesactive = lexique.chargerImage("images/desact.png", true);
    iinterdit = lexique.chargerImage("images/interdit.png", true);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCGSP@")).trim());
    labelHisto.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Historique des modifications @LIBPG@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCL1L@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCL2L@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLCL3L@")).trim());
    OBJ_156.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRPKG@")).trim());
    lbCaMois.setText(lexique.TranslationTable(interpreteurD.analyseExpression("CA du mois @WDSVX@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs(topErreur);
    
    // Gestion des fonctions disponibles en interrogation
    boolean isInterro = lexique.isTrue("53");
    isClientParticulier = lexique.isTrue("61");
    boolean isHistorique = lexique.isTrue("96");
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    boolean isModification = lexique.getMode() == Lexical.MODE_MODIFICATION;
    
    // zéros non-significatifs dans le code postal
    String codePostal = "00000";
    if (!lexique.HostFieldGetData("CLCDP").trim().isEmpty()) {
      codePostal = lexique.HostFieldGetData("CLCDP");
    }
    int numberAsInt = Integer.parseInt(codePostal);
    int size = 5;
    NumberFormat nf = NumberFormat.getInstance();
    nf.setMinimumIntegerDigits(size);
    nf.setMaximumIntegerDigits(size);
    nf.setGroupingUsed(false);
    
    // Traitement encours
    riSousMenu27.setEnabled((lexique.HostFieldGetData("SET031").equalsIgnoreCase("9"))
        && ((lexique.getMode() == Lexical.MODE_CONSULTATION) || (lexique.getMode() == Lexical.MODE_MODIFICATION)));
    riSousMenu29.setEnabled((lexique.HostFieldGetData("SET031").equalsIgnoreCase("9"))
        && ((lexique.getMode() == Lexical.MODE_CONSULTATION) || (lexique.getMode() == Lexical.MODE_MODIFICATION)));
    
    riMenu3.setVisible(!isHistorique);
    
    riSousMenu2.setEnabled(!isHistorique);
    riSousMenu3.setEnabled(!isHistorique && isConsultation);
    riSousMenu4.setEnabled(!isHistorique && isConsultation);
    riSousMenu5.setEnabled(!isHistorique);
    riSousMenu5.setEnabled(!isHistorique);
    riSousMenu7.setEnabled(!isHistorique);
    riSousMenu8.setEnabled(!isHistorique);
    riSousMenu9.setEnabled(!isHistorique);
    riSousMenu10.setEnabled(!isHistorique);
    riSousMenu11.setEnabled(!isHistorique);
    riSousMenu12.setEnabled(!isHistorique);
    riSousMenu14.setEnabled(!isHistorique);
    riSousMenu15.setEnabled(!isHistorique);
    riSousMenu16.setEnabled(!isHistorique);
    riSousMenu17.setEnabled(!isHistorique);
    riSousMenu18.setEnabled(!isHistorique && isConsultation);
    riSousMenu19.setEnabled(!isHistorique && isConsultation);
    riSousMenu20.setEnabled(!isHistorique && isConsultation);
    riSousMenu21.setEnabled(!isHistorique && isConsultation);
    riSousMenu24.setEnabled(!isHistorique);
    riSousMenu26.setEnabled(!isHistorique);
    
    riSousMenu32.setEnabled(!isHistorique && isConsultation);
    riSousMenu33.setEnabled(!isHistorique && isConsultation);
    riSousMenuDocument.setEnabled(!isCreation);
    
    pnlChoixTypeClient.setVisible(isCreation || isModification);
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt2);
    
    if (lexique.HostFieldGetData("CLIN29").trim().isEmpty()) {
      p_bpresentation.setText("Fiche client");
      OBJECT_4.setText("Client");
    }
    else {
      p_bpresentation.setText("Fiche prospect");
      OBJECT_4.setText("Prospect");
    }
    
    boolean isModif = (!lexique.isTrue("53"));
    
    navig_valid.setVisible(isModif);
    
    CLTNS.setEnabled(isModif);
    CLPLF.setFocusable(isModif);
    CLPLF2.setFocusable(isModif);
    SVCRM.setFocusable(isModif);
    SVCR1.setFocusable(isModif);
    SVCR0.setFocusable(isModif);
    CLREM1.setFocusable(isModif);
    CLREM2.setFocusable(isModif);
    CLREM3.setFocusable(isModif);
    
    lbEtatClient.setVisible(CLTNS.isVisible());
    lbDepassement.setVisible(WEDEP.isVisible());
    WEDEP.setForeground(Color.RED);
    lbAlerte.setVisible(CLATT.isVisible());
    
    // lbDepassement.setVisible(lexique.isTrue("(57) AND (N40) AND (53)"));
    // Bloc Observation
    if (lexique.HostFieldGetData("POSTIT").trim().equals("+") || lexique.isTrue("N75")) {
      OBJ_117.setIcon(bloc_couleur);
      OBJ_117.setToolTipText("Le bloc-notes et/ou le mémo contiennent des informations");
    }
    else {
      OBJ_117.setIcon(bloc_neutre);
      OBJ_117.setToolTipText("Le bloc-notes et le mémo sont vides");
    }
    
    // 96 -> mode historique
    labelHisto.setVisible(lexique.isTrue("96"));
    
    // ZP visibles uniquement par N32
    riSousMenu8.setEnabled(!lexique.isTrue("32"));
    
    // email client en attente d'implantation
    riSousMenu1.setEnabled(false);
    
    // radio particulier
    CLIN9_1.setEnabled(isModif);
    
    // Type de fiche en création
    rbTypeClient.setSelected(lexique.HostFieldGetData("CLIN29").trim().isEmpty());
    rbTypeProspect.setSelected(lexique.HostFieldGetData("CLIN29").trim().equalsIgnoreCase("P"));
    
    // boutons F4 visibles ou non en fonction de leur zone
    CLCL1.setEditable(isModif);
    CLCL2.setEditable(isModif);
    CLCL3.setEditable(isModif);
    CLAPEN.setEditable(isModif);
    CLFRP.setEditable(isModif);
    CLREM1.setEditable(isModif);
    CLREM2.setEditable(isModif);
    CLREM3.setEditable(isModif);
    CLPLF.setEditable(isModif);
    CLPLF2.setEditable(isModif);
    CLRST.setEditable(isModif);
    
    if (isInterro) {
      // panels détails
      if (lexique.HostFieldGetData("TCI2").trim().isEmpty()) {
        TCI2.setEnabled(false);
      }
      else {
        TCI2.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI3").trim().isEmpty()) {
        TCI3.setEnabled(false);
      }
      else {
        TCI3.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI4").trim().isEmpty()) {
        TCI4.setEnabled(false);
      }
      else {
        TCI4.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI5").trim().isEmpty()) {
        TCI5.setEnabled(false);
      }
      else {
        TCI5.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI6").trim().isEmpty()) {
        TCI6.setEnabled(false);
      }
      else {
        TCI6.setEnabled(true);
      }
    }
    else {
      TCI2.setEnabled(true);
      TCI3.setEnabled(true);
      TCI4.setEnabled(true);
      TCI5.setEnabled(true);
      TCI6.setEnabled(true);
    }
    
    CLFRP.forceVisibility();
    
    // LayeredPane
    if (lexique.HostFieldGetData("CLTNS").trim().equals("9")) {
      lab_desac.setText("Désactivé");
      lab_desac.setVisible(true);
    }
    else if (lexique.HostFieldGetData("CLTNS").trim().equals("2")) {
      lab_desac.setText("Interdit");
      lab_desac.setVisible(true);
    }
    else {
      lab_desac.setVisible(false);
    }
    
    // Gestion particulier/sociétés
    CLTEL.setVisible(!isClientParticulier);
    lbFax.setVisible(!isClientParticulier);
    CLFAX.setVisible(!isClientParticulier);
    lbCivilite.setVisible(isClientParticulier);
    snCivilite.setVisible(isClientParticulier);
    CLTEL2.setVisible(isClientParticulier);
    if (isClientParticulier) {
      CLTEL2.setText(lexique.HostFieldGetData("CLTEL"));
      lbNom.setText("Nom");
      lbComplementNom.setText("Prénom");
      CLTEL2.setEnabled(!isConsultation);
      lbTitreContact.setText("Contacter ce client");
      
    }
    else {
      lbNom.setText("Raison sociale");
      lbComplementNom.setText("Complément");
      lbTitreContact.setText("Contact");
    }
    CLTEL.setEnabled(!isConsultation);
    CLTEL2.setEnabled(!isConsultation);
    CLFAX.setEnabled(!isConsultation);
    
    snCivilite.setVisible(isClientParticulier);
    REPRE.setVisible(isClientParticulier);
    CLCPL.setVisible(!isClientParticulier);
    
    if (lexique.isTrue("35")) {
      lbNaf.setVisible(false);
      CLAPEN.setVisible(false);
    }
    
    // On rend inopérant les icones + (F4) en les grisant en interrogation
    btGencod.setEnabled(!isInterro);
    
    lbRemises.setVisible(CLREM1.isVisible());
    pnlRemises.setVisible(CLREM1.isVisible());
    
    lbPlafonds.setVisible(CLPLF.isVisible());
    pnlPlafond.setVisible(CLPLF.isVisible());
    
    lbCaMois.setVisible(SVCRM.isVisible());
    lbRegroupStat.setVisible(CLRST.isVisible());
    lbStatExe.setVisible(SVCR1.isVisible());
    lbStatExePre.setVisible(SVCR0.isVisible());
    lbcomRep.setVisible(CLTRP.isVisible());
    lbObservations.setVisible(CLOBS.isVisible());
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    // Civilité
    snCivilite.setSession(getSession());
    snCivilite.setIdEtablissement(idEtablissement);
    snCivilite.charger(false);
    snCivilite.setSelectionParChampRPG(lexique, "RECIV");
    snCivilite.setEnabled(isModif);
    
    // Commune client
    snCodePostalCommune.setSession(getSession());
    snCodePostalCommune.setIdEtablissement(idEtablissement);
    snCodePostalCommune.charger(false);
    snCodePostalCommune.setSelectionParChampRPG(lexique, "CLCDP", "CLVILR");
    snCodePostalCommune.setEnabled(isModif);
    
    // Pays client
    try {
      snPays.setEnabled(isModif);
      snPays.setSession(getSession());
      snPays.setIdEtablissement(idEtablissement);
      snPays.charger(false);
      snPays.setIdSelection(null);
      snPays.setSelectionParChampRPG(lexique, "CLCOP", "CLPAY");
      snPays.setAucunAutorise(true);
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
    }
    
    // Catégorie client
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(idEtablissement);
    snCategorieClient.charger(false);
    snCategorieClient.setSelectionParChampRPG(lexique, "CLCAT");
    snCategorieClient.setEnabled(isModif);
    
    // Représentant 1
    snRepresentant1.setSession(getSession());
    snRepresentant1.setIdEtablissement(idEtablissement);
    snRepresentant1.charger(false);
    snRepresentant1.setSelectionParChampRPG(lexique, "CLREP");
    snRepresentant1.setEnabled(isModif && !isRepresentantNonModifiable());
    
    // Représentant 2
    snRepresentant2.setSession(getSession());
    snRepresentant2.setIdEtablissement(idEtablissement);
    snRepresentant2.charger(false);
    snRepresentant2.setSelectionParChampRPG(lexique, "CLREP2");
    snRepresentant2.setEnabled(isModif && !isRepresentantNonModifiable());
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "CLMAG");
    snMagasin.setEnabled(isModif);
    
    // Mode expédition
    snModeExpedition.setSession(getSession());
    snModeExpedition.setIdEtablissement(idEtablissement);
    snModeExpedition.charger(false);
    snModeExpedition.setSelectionParChampRPG(lexique, "CLMEX");
    snModeExpedition.setEnabled(isModif);
    
    // Zone géographique
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(idEtablissement);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "CLGEO");
    snZoneGeographique.setEnabled(isModif);
    
    // Transporteur
    snTransporteur.setSession(getSession());
    snTransporteur.setIdEtablissement(idEtablissement);
    snTransporteur.charger(false);
    snTransporteur.setSelectionParChampRPG(lexique, "CLCTR");
    snTransporteur.setEnabled(isModif);
    
    // Type de facturation
    snTypeFacturation.setSession(getSession());
    snTypeFacturation.setIdEtablissement(idEtablissement);
    snTypeFacturation.charger(false);
    snTypeFacturation.ajouterFacturationUE(idEtablissement);
    snTypeFacturation.setSelectionParChampRPG(lexique, "CLTFA");
    snTypeFacturation.setEnabled(isModif);
    
    // Devise
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(idEtablissement);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "CLDEV");
    snDevise.setEnabled(isModif);
    
    // CNV
    snConditionVenteEtClient.setSession(getSession());
    snConditionVenteEtClient.setIdEtablissement(idEtablissement);
    snConditionVenteEtClient.setSelectionParChampRPG(lexique, "CLCNV");
    snConditionVenteEtClient.setEnabled(isModif);
    
    // Contact
    if (!isCreation) {
      snContact.setSession(getSession());
      snContact.setIdEtablissement(idEtablissement);
      snContact.setTypeContact(EnumTypeContact.CLIENT);
      snContact.setIdClient(getIdClient());
      snContact.charger(false, false);
      snContact.setSelectionParChampRPG(lexique, "RENUM");
      snContact.setEditable(false);
      snContact.setEnabled(false);
      btnListeContact.setEnabled(true);
    }
    else {
      snContact.setSelection(null);
      snContact.setEnabled(false);
      btnListeContact.setEnabled(false);
    }
    
    // Mail
    snAdresseMail.setText(lexique.HostFieldGetData("RENETR"));
    snAdresseMail.setEnabled(false);
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snCivilite.renseignerChampRPG(lexique, "RECIV");
    snCodePostalCommune.renseignerChampRPG(lexique, "CLCDP", "CLVILR", false);
    snPays.renseignerChampRPG(lexique, "CLCOP", "CLPAY");
    snCategorieClient.renseignerChampRPG(lexique, "CLCAT");
    snRepresentant1.renseignerChampRPG(lexique, "CLREP");
    snRepresentant2.renseignerChampRPG(lexique, "CLREP2");
    snMagasin.renseignerChampRPG(lexique, "CLMAG");
    snModeExpedition.renseignerChampRPG(lexique, "CLMEX");
    snZoneGeographique.renseignerChampRPG(lexique, "CLGEO");
    snTransporteur.renseignerChampRPG(lexique, "CLCTR");
    snTypeFacturation.renseignerChampRPG(lexique, "CLTFA");
    snDevise.renseignerChampRPG(lexique, "CLDEV");
    snConditionVenteEtClient.renseignerChampRPG(lexique, "CLCNV");
    
    if (isClientParticulier) {
      lexique.HostFieldPutData("CLTEL", 0, CLTEL2.getText());
    }
    
    if (rbTypeProspect.isSelected()) {
      lexique.HostFieldPutData("CLIN29", 0, "P");
    }
    else {
      lexique.HostFieldPutData("CLIN29", 0, "");
    }
    if (snContact.getSelection() != null) {
      lexique.HostFieldPutData("RENUM", 0, snContact.getSelection().getId().getCode() + "");
    }
    
    isChargement = true;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * 
   * Lecture de la sécurité 180 pour vérifier le droit à modifier le représentant.
   * 
   * @return boolean
   */
  private boolean isRepresentantNonModifiable() {
    return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_NON_AUTORISE_A_MODIFIER_REPRESENTANT));
  }
  
  /**
   * Retourner l'identifiant de l'établissement en cours d'affichage.
   */
  private IdEtablissement getIdEtablissement() {
    // Contrôler la présence du champ RPG
    if (lexique.HostFieldGetData("INDETB") == null || lexique.HostFieldGetData("INDETB").isEmpty()) {
      throw new MessageErreurException("L'identifant de l'établissement n'est pas renseigné.");
    }
    
    // Retourner l'identifiant de l'établissement
    return IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
  }
  
  /**
   * Retourner l'identifiant du client en cours d'affichage.
   */
  private IdClient getIdClient() {
    // Contrôler la présence du champ RPG
    if (lexique.HostFieldGetData("INDCLI") == null || lexique.HostFieldGetData("INDCLI").isEmpty()) {
      throw new MessageErreurException("Le code du client n'est pas renseigné.");
    }
    
    // Récupérer le numéro du client
    int numero = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("INDCLI"));
    
    // Récupérer le suffixe du client (0 si pas renseigné)
    int suffixe = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("INDLIV"));
    
    // Retourner l'identifiant du client
    return IdClient.getInstance(getIdEtablissement(), numero, suffixe);
  }
  
  /**
   * Retourner l'identifiant du contact principal en cours d'affichage.
   */
  private IdContact getIdContactPrincipal() {
    // Si on est en création pas de chargement de composant car pas de numéro composant généré par le système
    if (isCreation) {
      return null;
    }
    // Contrôler la présence du champ RPG
    if (lexique.HostFieldGetData("RENUM") == null || lexique.HostFieldGetData("RENUM").isEmpty()) {
      throw new MessageErreurException("L'identifiant du contact n'est pas renseigné.");
    }
    
    // Convertir le numéro
    Integer numeroContact = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("RENUM"));
    
    // Construire l'identifiant du contact
    return IdContact.getInstance(numeroContact);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Copie le bloc adresse dans le presse-papier.
   */
  private void copierBlocAdresse() {
    String RC = System.getProperty("line.separator");
    String texte = "";
    if (isClientParticulier) {
      if (snCivilite.getSelection() != null) {
        texte = snCivilite.getSelection().getLibelle() + " ";
      }
      texte += RENOM.nettoyerLaSaisie() + " " + REPRE.nettoyerLaSaisie();
    }
    else {
      texte = CLNOM.nettoyerLaSaisie() + RC + CLCPL.nettoyerLaSaisie();
    }
    texte += RC + CLRUE.nettoyerLaSaisie() + RC + CLLOC.nettoyerLaSaisie();
    
    // Contrôle du composant snCodePostalcommune
    if (snCodePostalCommune.getSelection() != null) {
      texte += RC + snCodePostalCommune.getSelection().getCodePostal() + " " + snCodePostalCommune.getSelection().getVille();
    }
    // Contrôle du composant snPays
    if (snPays.getSelection() != null) {
      texte += RC + snPays.getSelection().getLibelle();
    }
    // Envoi du text dans le presse-papier
    Clipboard.envoyerTexte(texte);
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(CMD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "°");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void TCI5ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI5", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI5"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void TCI6ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI6"));
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "X");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    getData();
    if (dialog_ZP == null) {
      dialog_ZP = new ODialog((Window) getTopLevelAncestor(), new ZP_PANEL(this));
    }
    dialog_ZP.affichePopupPerso();
    // majZP(zptop);
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "R");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "R");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    copierBlocAdresse();
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "Q");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "M");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "M");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "L");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "L");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "@@");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "@@");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WSIN");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "V");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "V");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "E");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "E");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "G");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "G");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "A");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "A");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "O");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "O");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "C");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "C");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "V");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "V");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt25ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "P");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "P");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "°");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt28ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt31ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("V06FO");
    lexique.HostCursorPut(24, 79);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt32ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "£");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("CLREP");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void btGencodActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("GCDPR");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_120ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("CLREP2");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void OBJ_117ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt33ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("TCI12", 0, "$");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "$");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt26ActionPerformed(ActionEvent e) {
    if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
      lexique.HostScreenSendKey(this, "F9");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, "S");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt27ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("W031");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt29ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("W0311");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * Afficher la boite de dialogue des documents stockés.
   * @param e
   */
  private void riAfficherDocumentStockeActionPerformed(ActionEvent e) {
    CritereDocumentStocke critere = new CritereDocumentStocke();
    critere.setIdEtablissement(getIdEtablissement());
    critere.setIdClient(getIdClient());
    ModeleListeDocumentStocke modele = new ModeleListeDocumentStocke(getSession(), critere);
    modele.setNomClient(lexique.HostFieldGetData("CLNOM").trim());
    VueDocumentStocke vue = new VueDocumentStocke(modele);
    vue.afficher();
  }
  
  /**
   * Traiter la modification du pays.
   * @param e
   */
  private void snPaysValueChanged(SNComposantEvent e) {
    try {
      // La recherche du composant snCodePostal est désactivée si le code pays correspond
      boolean desactivationRecherche = false;
      if (snPays.getSelection() != null && !snPays.getSelection().isFrance()) {
        desactivationRecherche = true;
      }
      snCodePostalCommune.setDesactivationRecherche(desactivationRecherche);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * A intégrer à terme par la barre de boutons
   */
  private void riSousMenu_bt50ActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut(1, 1);
      lexique.HostScreenSendKey(this, "F4");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter les clics sur le bouton "+" d'affichage de la liste des contacts.
   */
  private void btnListeContactActionPerformed(ActionEvent e) {
    try {
      // Si on est en création pas de chargement de composant car pas de numéro composant généré par le système
      if (isCreation) {
        return;
      }
      // Afficher la boîte de dialogue avec la liste des contacts
      ModeleListeContactTiers modele = new ModeleListeContactTiers(getSession(), getIdClient());
      DialogueListeContactTiers vue = new DialogueListeContactTiers(modele);
      vue.afficher();
      
      // Récupérer l'identifiant du contact principal de l'écran RPG
      IdContact idContactPrincipalRPG = getIdContactPrincipal();
      
      // Récupérer l'identifiant du contact principal issu de la boîte de dialogue
      IdContact idContactPrincipalJava = null;
      if (modele.getContactPrincipal() != null) {
        idContactPrincipalJava = modele.getContactPrincipal().getId();
      }
      
      // Comparer les identifiants
      if (!Constantes.equals(idContactPrincipalRPG, idContactPrincipalJava)) {
        // Demander au programme RPG de rafraichir les informations du contact principal
        if (lexique.getMode() != Lexical.MODE_CONSULTATION) {
          lexique.HostFieldPutData("TCI1", 0, "X");
        }
        else {
          lexique.HostFieldPutData("V06FO", 0, "1");
        }
        lexique.HostScreenSendKey(this, "ENTER");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-générées (JFormDesigner)
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJECT_3 = new JLabel();
    INDETB = new XRiTextField();
    OBJECT_4 = new JLabel();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    label6 = new JLabel();
    labelHisto = new JLabel();
    p_tete_droite = new JPanel();
    lab_desac = new JLabel();
    bt_ecran = new JLabel();
    pnlSud = new SNPanel();
    pnlDonnees = new SNPanelContenu();
    pnlCoordonneesClient = new JPanel();
    pnlChoixTypeClient = new SNPanel();
    rbTypeClient = new SNRadioButton();
    rbTypeProspect = new SNRadioButton();
    lbCivilite = new SNLabelChamp();
    snCivilite = new SNCivilite();
    lbNom = new SNLabelChamp();
    pnlNom = new SNPanel();
    RENOM = new XRiTextField();
    CLNOM = new XRiTextField();
    lbComplementNom = new SNLabelChamp();
    pnlPrenom = new SNPanel();
    REPRE = new XRiTextField();
    CLCPL = new XRiTextField();
    lbLocalisation = new SNLabelChamp();
    CLRUE = new XRiTextField();
    lbRue = new SNLabelChamp();
    CLLOC = new XRiTextField();
    lbCodePostal = new SNLabelChamp();
    snCodePostalCommune = new SNCodePostalCommune();
    lbCodePostal2 = new SNLabelChamp();
    sNPanel6 = new SNPanel();
    snPays = new SNPays();
    tbpInfos = new JTabbedPane();
    pnlInfosPrincipales = new SNPanelContenu();
    lbType = new SNLabelChamp();
    sNPanel1 = new SNPanel();
    CLIN9 = new XRiRadioButton();
    CLIN9_1 = new XRiRadioButton();
    OBJ_117 = new JButton();
    lbClassement1 = new SNLabelChamp();
    CLCLK = new XRiTextField();
    lbClassement2 = new SNLabelChamp();
    CLCLA = new XRiTextField();
    lbCategorie = new SNLabelChamp();
    snCategorieClient = new SNCategorieClient();
    lbRep1 = new SNLabelChamp();
    snRepresentant1 = new SNRepresentant();
    lbRep2 = new SNLabelChamp();
    snRepresentant2 = new SNRepresentant();
    lbObservations = new SNLabelChamp();
    CLOBS = new XRiTextField();
    pnlInfosAutres = new SNPanelContenu();
    lbEffectif = new SNLabelChamp();
    CLIN23 = new XRiComboBox();
    lbNaf = new SNLabelChamp();
    CLAPEN = new XRiTextField();
    lbGroupe = new SNLabelChamp();
    pnlGroupe = new SNPanel();
    CLCL1 = new XRiTextField();
    OBJ_86 = new RiZoneSortie();
    lbFamille = new SNLabelChamp();
    pnlFamille = new SNPanel();
    CLCL2 = new XRiTextField();
    OBJ_95 = new RiZoneSortie();
    lbSousFamille = new SNLabelChamp();
    pnlSousFamille = new SNPanel();
    CLCL3 = new XRiTextField();
    OBJ_103 = new RiZoneSortie();
    lbcomRep = new SNLabelChamp();
    CLTRP = new XRiTextField();
    lbFamille2 = new SNLabelChamp();
    sNPanel3 = new SNPanel();
    GCDPR = new XRiTextField();
    btGencod = new SNBoutonDetail();
    pnlPremierLigne = new SNPanel();
    pnllTitreContact = new SNPanel();
    btnListeContact = new SNBoutonDetail();
    lbTitreContact = new SNLabelTitre();
    pnllTitreExpedition = new SNPanel();
    TCI2 = new SNBoutonDetail();
    lbTitreLivraison = new SNLabelTitre();
    pnlTitreFacturation = new SNPanel();
    TCI3 = new SNBoutonDetail();
    lbTitreFacturation = new SNLabelTitre();
    sNPanel2 = new SNPanel();
    pnlContact = new SNPanel();
    lbContact = new SNLabelChamp();
    snContact = new SNContact();
    lbMail = new SNLabelChamp();
    snAdresseMail = new SNAdresseMail();
    lbTelephone = new SNLabelChamp();
    CLTEL = new XRiTextField();
    CLTEL2 = new XRiTextField();
    lbFax = new SNLabelChamp();
    CLFAX = new XRiTextField();
    pnlExpedition = new SNPanel();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbModeExpedition = new SNLabelChamp();
    snModeExpedition = new SNModeExpedition();
    lbMagasin3 = new SNLabelChamp();
    snZoneGeographique = new SNZoneGeographique();
    lbTransporteur = new SNLabelChamp();
    snTransporteur = new SNTransporteur();
    lbFranco = new SNLabelChamp();
    sNPanel4 = new SNPanel();
    CLFRP = new XRiTextField();
    OBJ_156 = new JLabel();
    pnlFacturation = new SNPanel();
    lbTypeFacturation = new SNLabelChamp();
    snTypeFacturation = new SNTypeFacturation();
    lbTarif = new SNLabelChamp();
    sNPanel5 = new SNPanel();
    WTAR = new XRiTextField();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    lbRemises = new SNLabelChamp();
    pnlRemises = new SNPanel();
    CLREM1 = new XRiTextField();
    CLREM2 = new XRiTextField();
    CLREM3 = new XRiTextField();
    lbCondition = new SNLabelChamp();
    snConditionVenteEtClient = new SNConditionVenteEtClient();
    lbMagasin7 = new SNLabelChamp();
    CLDVEX = new XRiCalendrier();
    pnlDeuxiemeLigne = new SNPanel();
    pnlTitreReglement = new SNPanel();
    TCI4 = new SNBoutonDetail();
    lbReglement = new SNLabelTitre();
    pnlTitreCompta = new SNPanel();
    TCI5 = new SNBoutonDetail();
    lbCompta = new SNLabelTitre();
    pnlTitreCompta2 = new SNPanel();
    TCI6 = new SNBoutonDetail();
    lbCompta2 = new SNLabelTitre();
    pnlReglement = new SNPanelTitre();
    lbModeReglement = new SNLabelChamp();
    CLRGL = new XRiTextField();
    lbEcheance = new SNLabelChamp();
    CLECH = new XRiTextField();
    ULBRGL = new XRiTextField();
    sNPanelTitre1 = new SNPanelTitre();
    lbEtatClient = new SNLabelChamp();
    CLTNS = new XRiComboBox();
    lbAlerte = new SNLabelChamp();
    CLATT = new XRiTextField();
    lbPlafonds = new SNLabelChamp();
    pnlPlafond = new SNPanel();
    CLPLF = new XRiTextField();
    CLPLF2 = new XRiTextField();
    lbDepassement = new SNLabelChamp();
    WEDEP = new XRiTextField();
    pnlStatistiques = new SNPanelTitre();
    lbCaMois = new SNLabelChamp();
    SVCRM = new XRiTextField();
    lbRegroupStat = new SNLabelChamp();
    CLRST = new XRiTextField();
    lbStatExe = new SNLabelChamp();
    SVCR1 = new XRiTextField();
    lbStatExePre = new SNLabelChamp();
    SVCR0 = new XRiTextField();
    p_menus = new JPanel();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu33 = new RiSousMenu();
    riSousMenu_bt33 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenuDocument = new RiSousMenu();
    riAfficherDocumentStocke = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu50 = new RiSousMenu();
    riSousMenu_bt50 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu26 = new RiSousMenu();
    riSousMenu_bt26 = new RiSousMenu_bt();
    riSousMenu32 = new RiSousMenu();
    riSousMenu_bt32 = new RiSousMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riSousMenu29 = new RiSousMenu();
    riSousMenu_bt29 = new RiSousMenu_bt();
    riSousMenu27 = new RiSousMenu();
    riSousMenu_bt27 = new RiSousMenu_bt();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    BTDA = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    CMD = new JPopupMenu();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    riSousMenu12 = new RiSousMenu();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();
    riSousMenu1 = new RiSousMenu();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu_bt5 = new RiSousMenu_bt();
    buttonGroupCategorie = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 710));
    setPreferredSize(new Dimension(1200, 710));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Fiche client");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 34));
          p_tete_gauche.setMinimumSize(new Dimension(700, 34));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJECT_3 ----
          OBJECT_3.setText("Etablissement");
          OBJECT_3.setName("OBJECT_3");
          
          // ---- INDETB ----
          INDETB.setName("INDETB");
          
          // ---- OBJECT_4 ----
          OBJECT_4.setText("Client");
          OBJECT_4.setName("OBJECT_4");
          
          // ---- INDCLI ----
          INDCLI.setName("INDCLI");
          
          // ---- INDLIV ----
          INDLIV.setName("INDLIV");
          
          // ---- label6 ----
          label6.setText("@CCGSP@");
          label6.setName("label6");
          
          // ---- labelHisto ----
          labelHisto.setText("Historique des modifications @LIBPG@");
          labelHisto.setForeground(new Color(44, 74, 116));
          labelHisto.setFont(labelHisto.getFont().deriveFont(labelHisto.getFont().getStyle() | Font.BOLD));
          labelHisto.setName("labelHisto");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJECT_3).addGap(5, 5, 5)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(54, 54, 54)
                  .addComponent(OBJECT_4, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                  .addComponent(label6, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED).addComponent(labelHisto)
                  .addContainerGap(241, Short.MAX_VALUE)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(INDCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                          .addComponent(INDLIV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(label6).addComponent(labelHisto))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6)
                          .addGroup(p_tete_gaucheLayout.createParallelGroup().addComponent(OBJECT_3).addComponent(OBJECT_4))))
                  .addContainerGap(2, Short.MAX_VALUE)));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
          
          // ---- lab_desac ----
          lab_desac.setText("D\u00e9sactiv\u00e9");
          lab_desac.setFont(new Font("sansserif", Font.BOLD, 16));
          lab_desac.setForeground(Color.red);
          lab_desac.setName("lab_desac");
          p_tete_droite.add(lab_desac);
        }
        barre_tete.add(p_tete_droite);
        
        // ---- bt_ecran ----
        bt_ecran.setBorder(BorderFactory.createEmptyBorder());
        bt_ecran.setToolTipText("Passage \u00e0 l'affichage simplifi\u00e9");
        bt_ecran.setPreferredSize(new Dimension(30, 30));
        bt_ecran.setName("bt_ecran");
        bt_ecran.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            bt_ecranMouseClicked(e);
          }
        });
        barre_tete.add(bt_ecran);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== pnlDonnees ========
      {
        pnlDonnees.setOpaque(false);
        pnlDonnees.setName("pnlDonnees");
        pnlDonnees.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDonnees.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDonnees.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDonnees.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDonnees.getLayout()).rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0E-4 };
        
        // ======== pnlCoordonneesClient ========
        {
          pnlCoordonneesClient.setBorder(
              new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
          pnlCoordonneesClient.setOpaque(false);
          pnlCoordonneesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlCoordonneesClient.setMinimumSize(new Dimension(505, 280));
          pnlCoordonneesClient.setPreferredSize(new Dimension(505, 280));
          pnlCoordonneesClient.setName("pnlCoordonneesClient");
          pnlCoordonneesClient.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWidths = new int[] { 135, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlChoixTypeClient ========
          {
            pnlChoixTypeClient.setName("pnlChoixTypeClient");
            pnlChoixTypeClient.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChoixTypeClient.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlChoixTypeClient.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlChoixTypeClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlChoixTypeClient.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- rbTypeClient ----
            rbTypeClient.setText("Client");
            rbTypeClient.setMinimumSize(new Dimension(100, 30));
            rbTypeClient.setPreferredSize(new Dimension(100, 30));
            rbTypeClient.setMaximumSize(new Dimension(100, 30));
            rbTypeClient.setName("rbTypeClient");
            pnlChoixTypeClient.add(rbTypeClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- rbTypeProspect ----
            rbTypeProspect.setText("Prospect");
            rbTypeProspect.setMinimumSize(new Dimension(100, 30));
            rbTypeProspect.setPreferredSize(new Dimension(100, 30));
            rbTypeProspect.setMaximumSize(new Dimension(100, 30));
            rbTypeProspect.setName("rbTypeProspect");
            pnlChoixTypeClient.add(rbTypeProspect, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCoordonneesClient.add(pnlChoixTypeClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbCivilite ----
          lbCivilite.setText("Civilit\u00e9");
          lbCivilite.setPreferredSize(new Dimension(80, 30));
          lbCivilite.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCivilite.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCivilite.setMinimumSize(new Dimension(80, 30));
          lbCivilite.setMaximumSize(new Dimension(100, 30));
          lbCivilite.setName("lbCivilite");
          pnlCoordonneesClient.add(lbCivilite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- snCivilite ----
          snCivilite.setPreferredSize(new Dimension(300, 30));
          snCivilite.setMinimumSize(new Dimension(300, 30));
          snCivilite.setName("snCivilite");
          pnlCoordonneesClient.add(snCivilite, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbNom ----
          lbNom.setText("Raison sociale");
          lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNom.setPreferredSize(new Dimension(80, 30));
          lbNom.setMinimumSize(new Dimension(80, 30));
          lbNom.setMaximumSize(new Dimension(100, 30));
          lbNom.setName("lbNom");
          pnlCoordonneesClient.add(lbNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== pnlNom ========
          {
            pnlNom.setName("pnlNom");
            pnlNom.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlNom.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlNom.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlNom.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlNom.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- RENOM ----
            RENOM.setBackground(Color.white);
            RENOM.setFont(RENOM.getFont().deriveFont(RENOM.getFont().getStyle() | Font.BOLD, RENOM.getFont().getSize() + 1f));
            RENOM.setName("RENOM");
            pnlNom.add(RENOM, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 1, 0), 0, 0));
            
            // ---- CLNOM ----
            CLNOM.setBackground(Color.white);
            CLNOM.setName("CLNOM");
            pnlNom.add(CLNOM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCoordonneesClient.add(pnlNom, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbComplementNom ----
          lbComplementNom.setText("Compl\u00e9ment");
          lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
          lbComplementNom.setPreferredSize(new Dimension(80, 30));
          lbComplementNom.setMinimumSize(new Dimension(80, 30));
          lbComplementNom.setMaximumSize(new Dimension(100, 30));
          lbComplementNom.setName("lbComplementNom");
          pnlCoordonneesClient.add(lbComplementNom, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== pnlPrenom ========
          {
            pnlPrenom.setName("pnlPrenom");
            pnlPrenom.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrenom.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrenom.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrenom.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPrenom.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- REPRE ----
            REPRE.setBackground(Color.white);
            REPRE.setName("REPRE");
            pnlPrenom.add(REPRE, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 1, 0), 0, 0));
            
            // ---- CLCPL ----
            CLCPL.setBackground(Color.white);
            CLCPL.setName("CLCPL");
            pnlPrenom.add(CLCPL, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCoordonneesClient.add(pnlPrenom, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbLocalisation ----
          lbLocalisation.setText("Adresse 1");
          lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLocalisation.setInheritsPopupMenu(false);
          lbLocalisation.setMaximumSize(new Dimension(100, 30));
          lbLocalisation.setMinimumSize(new Dimension(80, 30));
          lbLocalisation.setPreferredSize(new Dimension(80, 30));
          lbLocalisation.setName("lbLocalisation");
          pnlCoordonneesClient.add(lbLocalisation, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CLRUE ----
          CLRUE.setBackground(Color.white);
          CLRUE.setName("CLRUE");
          pnlCoordonneesClient.add(CLRUE, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbRue ----
          lbRue.setText("Adresse 2");
          lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
          lbRue.setPreferredSize(new Dimension(80, 30));
          lbRue.setMinimumSize(new Dimension(80, 30));
          lbRue.setName("lbRue");
          pnlCoordonneesClient.add(lbRue, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- CLLOC ----
          CLLOC.setBackground(Color.white);
          CLLOC.setName("CLLOC");
          pnlCoordonneesClient.add(CLLOC, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbCodePostal ----
          lbCodePostal.setText("Commune");
          lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodePostal.setPreferredSize(new Dimension(80, 30));
          lbCodePostal.setMinimumSize(new Dimension(80, 30));
          lbCodePostal.setName("lbCodePostal");
          pnlCoordonneesClient.add(lbCodePostal, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ---- snCodePostalCommune ----
          snCodePostalCommune.setMinimumSize(new Dimension(350, 30));
          snCodePostalCommune.setPreferredSize(new Dimension(350, 30));
          snCodePostalCommune.setName("snCodePostalCommune");
          pnlCoordonneesClient.add(snCodePostalCommune, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ---- lbCodePostal2 ----
          lbCodePostal2.setText("Pays");
          lbCodePostal2.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCodePostal2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodePostal2.setPreferredSize(new Dimension(80, 30));
          lbCodePostal2.setMinimumSize(new Dimension(80, 30));
          lbCodePostal2.setName("lbCodePostal2");
          pnlCoordonneesClient.add(lbCodePostal2, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== sNPanel6 ========
          {
            sNPanel6.setName("sNPanel6");
            sNPanel6.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel6.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel6.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel6.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snPays ----
            snPays.setMinimumSize(new Dimension(300, 30));
            snPays.setPreferredSize(new Dimension(300, 30));
            snPays.setName("snPays");
            snPays.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPaysValueChanged(e);
              }
            });
            sNPanel6.add(snPays, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCoordonneesClient.add(sNPanel6, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
        }
        pnlDonnees.add(pnlCoordonneesClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== tbpInfos ========
        {
          tbpInfos.setFont(new Font("sansserif", Font.PLAIN, 14));
          tbpInfos.setMinimumSize(new Dimension(465, 280));
          tbpInfos.setPreferredSize(new Dimension(465, 280));
          tbpInfos.setName("tbpInfos");
          
          // ======== pnlInfosPrincipales ========
          {
            pnlInfosPrincipales.setMinimumSize(new Dimension(465, 260));
            pnlInfosPrincipales.setPreferredSize(new Dimension(465, 260));
            pnlInfosPrincipales.setName("pnlInfosPrincipales");
            pnlInfosPrincipales.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlInfosPrincipales.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlInfosPrincipales.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlInfosPrincipales.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlInfosPrincipales.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbType ----
            lbType.setText("Type");
            lbType.setMaximumSize(new Dimension(100, 30));
            lbType.setMinimumSize(new Dimension(100, 30));
            lbType.setPreferredSize(new Dimension(100, 30));
            lbType.setName("lbType");
            pnlInfosPrincipales.add(lbType, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLIN9 ----
              CLIN9.setText("Professionnel");
              CLIN9.setMaximumSize(new Dimension(150, 30));
              CLIN9.setMinimumSize(new Dimension(150, 30));
              CLIN9.setPreferredSize(new Dimension(150, 30));
              CLIN9.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLIN9.setName("CLIN9");
              sNPanel1.add(CLIN9, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CLIN9_1 ----
              CLIN9_1.setText("Particulier");
              CLIN9_1.setMaximumSize(new Dimension(150, 30));
              CLIN9_1.setMinimumSize(new Dimension(150, 30));
              CLIN9_1.setPreferredSize(new Dimension(150, 30));
              CLIN9_1.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLIN9_1.setName("CLIN9_1");
              sNPanel1.add(CLIN9_1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_117 ----
              OBJ_117.setBorder(null);
              OBJ_117.setPreferredSize(new Dimension(30, 30));
              OBJ_117.setMinimumSize(new Dimension(30, 30));
              OBJ_117.setMaximumSize(new Dimension(30, 30));
              OBJ_117.setBorderPainted(false);
              OBJ_117.setContentAreaFilled(false);
              OBJ_117.setName("OBJ_117");
              OBJ_117.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_117ActionPerformed(e);
                }
              });
              sNPanel1.add(OBJ_117, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlInfosPrincipales.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbClassement1 ----
            lbClassement1.setText("Classement 1");
            lbClassement1.setMaximumSize(new Dimension(100, 30));
            lbClassement1.setMinimumSize(new Dimension(100, 30));
            lbClassement1.setPreferredSize(new Dimension(100, 30));
            lbClassement1.setName("lbClassement1");
            pnlInfosPrincipales.add(lbClassement1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLCLK ----
            CLCLK.setName("CLCLK");
            pnlInfosPrincipales.add(CLCLK, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbClassement2 ----
            lbClassement2.setText("Classement 2");
            lbClassement2.setMaximumSize(new Dimension(100, 30));
            lbClassement2.setMinimumSize(new Dimension(100, 30));
            lbClassement2.setPreferredSize(new Dimension(100, 30));
            lbClassement2.setName("lbClassement2");
            pnlInfosPrincipales.add(lbClassement2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLCLA ----
            CLCLA.setToolTipText("Mot de classement 2");
            CLCLA.setName("CLCLA");
            pnlInfosPrincipales.add(CLCLA, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbCategorie ----
            lbCategorie.setText("Cat\u00e9gorie");
            lbCategorie.setMaximumSize(new Dimension(100, 30));
            lbCategorie.setMinimumSize(new Dimension(100, 30));
            lbCategorie.setPreferredSize(new Dimension(100, 30));
            lbCategorie.setName("lbCategorie");
            pnlInfosPrincipales.add(lbCategorie, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snCategorieClient ----
            snCategorieClient.setMinimumSize(new Dimension(250, 30));
            snCategorieClient.setPreferredSize(new Dimension(250, 30));
            snCategorieClient.setName("snCategorieClient");
            pnlInfosPrincipales.add(snCategorieClient, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbRep1 ----
            lbRep1.setText("Repr\u00e9sentant 1");
            lbRep1.setMaximumSize(new Dimension(100, 30));
            lbRep1.setMinimumSize(new Dimension(100, 30));
            lbRep1.setPreferredSize(new Dimension(100, 30));
            lbRep1.setName("lbRep1");
            pnlInfosPrincipales.add(lbRep1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snRepresentant1 ----
            snRepresentant1.setName("snRepresentant1");
            pnlInfosPrincipales.add(snRepresentant1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbRep2 ----
            lbRep2.setText("Repr\u00e9sentant 2");
            lbRep2.setMaximumSize(new Dimension(100, 30));
            lbRep2.setMinimumSize(new Dimension(100, 30));
            lbRep2.setPreferredSize(new Dimension(100, 30));
            lbRep2.setName("lbRep2");
            pnlInfosPrincipales.add(lbRep2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snRepresentant2 ----
            snRepresentant2.setName("snRepresentant2");
            pnlInfosPrincipales.add(snRepresentant2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbObservations ----
            lbObservations.setText("Observations");
            lbObservations.setMaximumSize(new Dimension(100, 30));
            lbObservations.setMinimumSize(new Dimension(100, 30));
            lbObservations.setPreferredSize(new Dimension(100, 30));
            lbObservations.setName("lbObservations");
            pnlInfosPrincipales.add(lbObservations, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- CLOBS ----
            CLOBS.setBackground(Color.white);
            CLOBS.setName("CLOBS");
            pnlInfosPrincipales.add(CLOBS, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tbpInfos.addTab("Informations principales", pnlInfosPrincipales);
          
          // ======== pnlInfosAutres ========
          {
            pnlInfosAutres.setPreferredSize(new Dimension(270, 330));
            pnlInfosAutres.setMinimumSize(new Dimension(250, 330));
            pnlInfosAutres.setName("pnlInfosAutres");
            pnlInfosAutres.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlInfosAutres.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlInfosAutres.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlInfosAutres.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlInfosAutres.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEffectif ----
            lbEffectif.setText("Effectif");
            lbEffectif.setMaximumSize(new Dimension(100, 30));
            lbEffectif.setMinimumSize(new Dimension(100, 30));
            lbEffectif.setPreferredSize(new Dimension(100, 30));
            lbEffectif.setName("lbEffectif");
            pnlInfosAutres.add(lbEffectif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLIN23 ----
            CLIN23.setModel(new DefaultComboBoxModel(new String[] { "0 \u00e0 5", "6 \u00e0 9", "10 \u00e0 49", ">= \u00e0 50" }));
            CLIN23.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLIN23.setBackground(Color.white);
            CLIN23.setName("CLIN23");
            pnlInfosAutres.add(CLIN23, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbNaf ----
            lbNaf.setText("NAF");
            lbNaf.setMaximumSize(new Dimension(100, 30));
            lbNaf.setMinimumSize(new Dimension(100, 30));
            lbNaf.setPreferredSize(new Dimension(100, 30));
            lbNaf.setName("lbNaf");
            pnlInfosAutres.add(lbNaf, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLAPEN ----
            CLAPEN.setComponentPopupMenu(BTD);
            CLAPEN.setPreferredSize(new Dimension(80, 30));
            CLAPEN.setMinimumSize(new Dimension(80, 30));
            CLAPEN.setName("CLAPEN");
            pnlInfosAutres.add(CLAPEN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbGroupe ----
            lbGroupe.setText("Groupe");
            lbGroupe.setMaximumSize(new Dimension(100, 30));
            lbGroupe.setMinimumSize(new Dimension(100, 30));
            lbGroupe.setPreferredSize(new Dimension(100, 30));
            lbGroupe.setName("lbGroupe");
            pnlInfosAutres.add(lbGroupe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlGroupe ========
            {
              pnlGroupe.setName("pnlGroupe");
              pnlGroupe.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlGroupe.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlGroupe.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlGroupe.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlGroupe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLCL1 ----
              CLCL1.setComponentPopupMenu(BTD);
              CLCL1.setMinimumSize(new Dimension(40, 30));
              CLCL1.setPreferredSize(new Dimension(40, 30));
              CLCL1.setName("CLCL1");
              pnlGroupe.add(CLCL1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_86 ----
              OBJ_86.setText("@CLCL1L@");
              OBJ_86.setName("OBJ_86");
              pnlGroupe.add(OBJ_86, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlInfosAutres.add(pnlGroupe, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbFamille ----
            lbFamille.setText("Famille");
            lbFamille.setMaximumSize(new Dimension(100, 30));
            lbFamille.setMinimumSize(new Dimension(100, 30));
            lbFamille.setPreferredSize(new Dimension(100, 30));
            lbFamille.setName("lbFamille");
            pnlInfosAutres.add(lbFamille, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlFamille ========
            {
              pnlFamille.setName("pnlFamille");
              pnlFamille.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlFamille.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlFamille.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlFamille.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlFamille.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLCL2 ----
              CLCL2.setComponentPopupMenu(BTD);
              CLCL2.setMinimumSize(new Dimension(40, 30));
              CLCL2.setPreferredSize(new Dimension(40, 30));
              CLCL2.setName("CLCL2");
              pnlFamille.add(CLCL2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_95 ----
              OBJ_95.setText("@CLCL2L@");
              OBJ_95.setName("OBJ_95");
              pnlFamille.add(OBJ_95, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlInfosAutres.add(pnlFamille, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbSousFamille ----
            lbSousFamille.setText("Sous-famillle");
            lbSousFamille.setMaximumSize(new Dimension(100, 30));
            lbSousFamille.setMinimumSize(new Dimension(100, 30));
            lbSousFamille.setPreferredSize(new Dimension(100, 30));
            lbSousFamille.setName("lbSousFamille");
            pnlInfosAutres.add(lbSousFamille, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlSousFamille ========
            {
              pnlSousFamille.setName("pnlSousFamille");
              pnlSousFamille.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlSousFamille.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlSousFamille.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlSousFamille.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlSousFamille.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLCL3 ----
              CLCL3.setComponentPopupMenu(BTD);
              CLCL3.setMinimumSize(new Dimension(40, 30));
              CLCL3.setPreferredSize(new Dimension(40, 30));
              CLCL3.setName("CLCL3");
              pnlSousFamille.add(CLCL3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_103 ----
              OBJ_103.setText("@CLCL3L@");
              OBJ_103.setName("OBJ_103");
              pnlSousFamille.add(OBJ_103, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlInfosAutres.add(pnlSousFamille, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbcomRep ----
            lbcomRep.setText("Com.repr\u00e9sentant");
            lbcomRep.setMaximumSize(new Dimension(100, 30));
            lbcomRep.setMinimumSize(new Dimension(100, 30));
            lbcomRep.setPreferredSize(new Dimension(100, 30));
            lbcomRep.setName("lbcomRep");
            pnlInfosAutres.add(lbcomRep, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLTRP ----
            CLTRP.setToolTipText("Type de commissionnement sp\u00e9cial");
            CLTRP.setComponentPopupMenu(BTD);
            CLTRP.setMinimumSize(new Dimension(30, 30));
            CLTRP.setPreferredSize(new Dimension(30, 30));
            CLTRP.setName("CLTRP");
            pnlInfosAutres.add(CLTRP, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbFamille2 ----
            lbFamille2.setText("Gencod");
            lbFamille2.setMaximumSize(new Dimension(100, 30));
            lbFamille2.setMinimumSize(new Dimension(100, 30));
            lbFamille2.setPreferredSize(new Dimension(100, 30));
            lbFamille2.setName("lbFamille2");
            pnlInfosAutres.add(lbFamille2, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== sNPanel3 ========
            {
              sNPanel3.setName("sNPanel3");
              sNPanel3.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- GCDPR ----
              GCDPR.setComponentPopupMenu(BTD);
              GCDPR.setPreferredSize(new Dimension(140, 30));
              GCDPR.setMinimumSize(new Dimension(140, 30));
              GCDPR.setName("GCDPR");
              sNPanel3.add(GCDPR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- btGencod ----
              btGencod.setName("btGencod");
              btGencod.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btGencodActionPerformed(e);
                }
              });
              sNPanel3.add(btGencod, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlInfosAutres.add(sNPanel3, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          tbpInfos.addTab("Autres informations", pnlInfosAutres);
        }
        pnlDonnees.add(tbpInfos, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlPremierLigne ========
        {
          pnlPremierLigne.setName("pnlPremierLigne");
          pnlPremierLigne.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPremierLigne.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlPremierLigne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPremierLigne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPremierLigne.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ======== pnllTitreContact ========
          {
            pnllTitreContact.setName("pnllTitreContact");
            pnllTitreContact.setLayout(new GridBagLayout());
            ((GridBagLayout) pnllTitreContact.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnllTitreContact.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnllTitreContact.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnllTitreContact.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- btnListeContact ----
            btnListeContact.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btnListeContact.setBorder(null);
            btnListeContact.setToolTipText("Afficher la liste des contacts");
            btnListeContact.setName("btnListeContact");
            btnListeContact.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btnListeContactActionPerformed(e);
              }
            });
            pnllTitreContact.add(btnListeContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTitreContact ----
            lbTitreContact.setText("Contact");
            lbTitreContact.setPreferredSize(new Dimension(150, 20));
            lbTitreContact.setMinimumSize(new Dimension(150, 20));
            lbTitreContact.setMaximumSize(new Dimension(150, 20));
            lbTitreContact.setName("lbTitreContact");
            pnllTitreContact.add(lbTitreContact, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPremierLigne.add(pnllTitreContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== pnllTitreExpedition ========
          {
            pnllTitreExpedition.setName("pnllTitreExpedition");
            pnllTitreExpedition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnllTitreExpedition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnllTitreExpedition.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnllTitreExpedition.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnllTitreExpedition.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- TCI2 ----
            TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TCI2.setBorder(null);
            TCI2.setName("TCI2");
            TCI2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TCI2ActionPerformed(e);
              }
            });
            pnllTitreExpedition.add(TCI2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTitreLivraison ----
            lbTitreLivraison.setText("Exp\u00e9dition/Livraison");
            lbTitreLivraison.setPreferredSize(new Dimension(150, 20));
            lbTitreLivraison.setMinimumSize(new Dimension(150, 20));
            lbTitreLivraison.setMaximumSize(new Dimension(150, 20));
            lbTitreLivraison.setName("lbTitreLivraison");
            pnllTitreExpedition.add(lbTitreLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPremierLigne.add(pnllTitreExpedition, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== pnlTitreFacturation ========
          {
            pnlTitreFacturation.setName("pnlTitreFacturation");
            pnlTitreFacturation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTitreFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTitreFacturation.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTitreFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTitreFacturation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- TCI3 ----
            TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TCI3.setBorder(null);
            TCI3.setName("TCI3");
            TCI3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TCI3ActionPerformed(e);
              }
            });
            pnlTitreFacturation.add(TCI3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTitreFacturation ----
            lbTitreFacturation.setText("Facturation");
            lbTitreFacturation.setPreferredSize(new Dimension(150, 20));
            lbTitreFacturation.setMinimumSize(new Dimension(150, 20));
            lbTitreFacturation.setMaximumSize(new Dimension(150, 20));
            lbTitreFacturation.setName("lbTitreFacturation");
            pnlTitreFacturation.add(lbTitreFacturation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPremierLigne.add(pnlTitreFacturation, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== sNPanel2 ========
          {
            sNPanel2.setName("sNPanel2");
            sNPanel2.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== pnlContact ========
            {
              pnlContact.setBorder(new TitledBorder(""));
              pnlContact.setPreferredSize(new Dimension(335, 182));
              pnlContact.setMinimumSize(new Dimension(335, 182));
              pnlContact.setName("pnlContact");
              pnlContact.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlContact.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlContact.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlContact.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlContact.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbContact ----
              lbContact.setText("Nom");
              lbContact.setMaximumSize(new Dimension(50, 30));
              lbContact.setMinimumSize(new Dimension(50, 30));
              lbContact.setPreferredSize(new Dimension(50, 30));
              lbContact.setName("lbContact");
              pnlContact.add(lbContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snContact ----
              snContact.setName("snContact");
              pnlContact.add(snContact, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMail ----
              lbMail.setText("Mail");
              lbMail.setMaximumSize(new Dimension(50, 30));
              lbMail.setMinimumSize(new Dimension(50, 30));
              lbMail.setPreferredSize(new Dimension(50, 30));
              lbMail.setName("lbMail");
              pnlContact.add(lbMail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snAdresseMail ----
              snAdresseMail.setName("snAdresseMail");
              pnlContact.add(snAdresseMail, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTelephone ----
              lbTelephone.setText("T\u00e9l");
              lbTelephone.setMaximumSize(new Dimension(50, 30));
              lbTelephone.setMinimumSize(new Dimension(50, 30));
              lbTelephone.setPreferredSize(new Dimension(50, 30));
              lbTelephone.setName("lbTelephone");
              pnlContact.add(lbTelephone, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- CLTEL ----
              CLTEL.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLTEL.setMinimumSize(new Dimension(150, 30));
              CLTEL.setPreferredSize(new Dimension(150, 30));
              CLTEL.setMaximumSize(new Dimension(150, 30));
              CLTEL.setName("CLTEL");
              pnlContact.add(CLTEL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- CLTEL2 ----
              CLTEL2.setPreferredSize(new Dimension(150, 30));
              CLTEL2.setMinimumSize(new Dimension(150, 30));
              CLTEL2.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLTEL2.setMaximumSize(new Dimension(150, 30));
              CLTEL2.setName("CLTEL2");
              pnlContact.add(CLTEL2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFax ----
              lbFax.setText("Fax");
              lbFax.setMaximumSize(new Dimension(50, 30));
              lbFax.setMinimumSize(new Dimension(50, 30));
              lbFax.setPreferredSize(new Dimension(50, 30));
              lbFax.setName("lbFax");
              pnlContact.add(lbFax, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CLFAX ----
              CLFAX.setMinimumSize(new Dimension(150, 30));
              CLFAX.setPreferredSize(new Dimension(150, 30));
              CLFAX.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLFAX.setMaximumSize(new Dimension(150, 30));
              CLFAX.setName("CLFAX");
              pnlContact.add(CLFAX, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel2.add(pnlContact, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPremierLigne.add(sNPanel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlExpedition ========
          {
            pnlExpedition.setBorder(new TitledBorder(""));
            pnlExpedition.setName("pnlExpedition");
            pnlExpedition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlExpedition.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlExpedition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlExpedition.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlExpedition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setMaximumSize(new Dimension(80, 30));
            lbMagasin.setMinimumSize(new Dimension(130, 30));
            lbMagasin.setPreferredSize(new Dimension(130, 30));
            lbMagasin.setName("lbMagasin");
            pnlExpedition.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlExpedition.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbModeExpedition ----
            lbModeExpedition.setText("Mode");
            lbModeExpedition.setMaximumSize(new Dimension(100, 30));
            lbModeExpedition.setMinimumSize(new Dimension(130, 30));
            lbModeExpedition.setPreferredSize(new Dimension(130, 30));
            lbModeExpedition.setName("lbModeExpedition");
            pnlExpedition.add(lbModeExpedition, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snModeExpedition ----
            snModeExpedition.setName("snModeExpedition");
            pnlExpedition.add(snModeExpedition, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbMagasin3 ----
            lbMagasin3.setText("Zone g\u00e9ographique");
            lbMagasin3.setMaximumSize(new Dimension(100, 30));
            lbMagasin3.setMinimumSize(new Dimension(130, 30));
            lbMagasin3.setPreferredSize(new Dimension(130, 30));
            lbMagasin3.setName("lbMagasin3");
            pnlExpedition.add(lbMagasin3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snZoneGeographique ----
            snZoneGeographique.setName("snZoneGeographique");
            pnlExpedition.add(snZoneGeographique, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbTransporteur ----
            lbTransporteur.setText("Transporteur");
            lbTransporteur.setMaximumSize(new Dimension(80, 30));
            lbTransporteur.setMinimumSize(new Dimension(130, 30));
            lbTransporteur.setPreferredSize(new Dimension(130, 30));
            lbTransporteur.setName("lbTransporteur");
            pnlExpedition.add(lbTransporteur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snTransporteur ----
            snTransporteur.setName("snTransporteur");
            pnlExpedition.add(snTransporteur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbFranco ----
            lbFranco.setText("Franco");
            lbFranco.setMaximumSize(new Dimension(80, 30));
            lbFranco.setMinimumSize(new Dimension(130, 30));
            lbFranco.setPreferredSize(new Dimension(130, 30));
            lbFranco.setName("lbFranco");
            pnlExpedition.add(lbFranco, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== sNPanel4 ========
            {
              sNPanel4.setName("sNPanel4");
              sNPanel4.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel4.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) sNPanel4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLFRP ----
              CLFRP.setPreferredSize(new Dimension(80, 30));
              CLFRP.setMinimumSize(new Dimension(80, 30));
              CLFRP.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLFRP.setName("CLFRP");
              sNPanel4.add(CLFRP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- OBJ_156 ----
              OBJ_156.setText("@FRPKG@");
              OBJ_156.setFont(new Font("sansserif", Font.PLAIN, 14));
              OBJ_156.setName("OBJ_156");
              sNPanel4.add(OBJ_156, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlExpedition.add(sNPanel4, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPremierLigne.add(pnlExpedition, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlFacturation ========
          {
            pnlFacturation.setBorder(new TitledBorder(""));
            pnlFacturation.setName("pnlFacturation");
            pnlFacturation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFacturation.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFacturation.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFacturation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbTypeFacturation ----
            lbTypeFacturation.setText("Type de facturation");
            lbTypeFacturation.setMaximumSize(new Dimension(80, 30));
            lbTypeFacturation.setMinimumSize(new Dimension(130, 30));
            lbTypeFacturation.setPreferredSize(new Dimension(130, 30));
            lbTypeFacturation.setName("lbTypeFacturation");
            pnlFacturation.add(lbTypeFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snTypeFacturation ----
            snTypeFacturation.setName("snTypeFacturation");
            pnlFacturation.add(snTypeFacturation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbTarif ----
            lbTarif.setText("Tarif");
            lbTarif.setMaximumSize(new Dimension(80, 30));
            lbTarif.setMinimumSize(new Dimension(130, 30));
            lbTarif.setPreferredSize(new Dimension(130, 30));
            lbTarif.setName("lbTarif");
            pnlFacturation.add(lbTarif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== sNPanel5 ========
            {
              sNPanel5.setName("sNPanel5");
              sNPanel5.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- WTAR ----
              WTAR.setComponentPopupMenu(null);
              WTAR.setPreferredSize(new Dimension(40, 30));
              WTAR.setMinimumSize(new Dimension(40, 30));
              WTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
              WTAR.setName("WTAR");
              sNPanel5.add(WTAR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
            }
            pnlFacturation.add(sNPanel5, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbDevise ----
            lbDevise.setText("Devise");
            lbDevise.setMaximumSize(new Dimension(80, 30));
            lbDevise.setMinimumSize(new Dimension(130, 30));
            lbDevise.setPreferredSize(new Dimension(130, 30));
            lbDevise.setName("lbDevise");
            pnlFacturation.add(lbDevise, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snDevise ----
            snDevise.setName("snDevise");
            pnlFacturation.add(snDevise, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbRemises ----
            lbRemises.setText("Remises");
            lbRemises.setMaximumSize(new Dimension(80, 30));
            lbRemises.setMinimumSize(new Dimension(130, 30));
            lbRemises.setPreferredSize(new Dimension(130, 30));
            lbRemises.setName("lbRemises");
            pnlFacturation.add(lbRemises, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ======== pnlRemises ========
            {
              pnlRemises.setName("pnlRemises");
              pnlRemises.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlRemises.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlRemises.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlRemises.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlRemises.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLREM1 ----
              CLREM1.setMinimumSize(new Dimension(50, 30));
              CLREM1.setPreferredSize(new Dimension(50, 30));
              CLREM1.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLREM1.setName("CLREM1");
              pnlRemises.add(CLREM1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CLREM2 ----
              CLREM2.setMinimumSize(new Dimension(50, 30));
              CLREM2.setPreferredSize(new Dimension(50, 30));
              CLREM2.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLREM2.setName("CLREM2");
              pnlRemises.add(CLREM2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CLREM3 ----
              CLREM3.setMinimumSize(new Dimension(50, 30));
              CLREM3.setPreferredSize(new Dimension(50, 30));
              CLREM3.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLREM3.setName("CLREM3");
              pnlRemises.add(CLREM3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFacturation.add(pnlRemises, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbCondition ----
            lbCondition.setText("Condition");
            lbCondition.setMaximumSize(new Dimension(80, 30));
            lbCondition.setMinimumSize(new Dimension(130, 30));
            lbCondition.setPreferredSize(new Dimension(130, 30));
            lbCondition.setName("lbCondition");
            pnlFacturation.add(lbCondition, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- snConditionVenteEtClient ----
            snConditionVenteEtClient.setPreferredSize(new Dimension(350, 30));
            snConditionVenteEtClient.setMinimumSize(new Dimension(350, 30));
            snConditionVenteEtClient.setMaximumSize(new Dimension(350, 30));
            snConditionVenteEtClient.setName("snConditionVenteEtClient");
            pnlFacturation.add(snConditionVenteEtClient, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbMagasin7 ----
            lbMagasin7.setText("Derni\u00e8re facture le");
            lbMagasin7.setMaximumSize(new Dimension(80, 30));
            lbMagasin7.setMinimumSize(new Dimension(130, 30));
            lbMagasin7.setPreferredSize(new Dimension(130, 30));
            lbMagasin7.setName("lbMagasin7");
            pnlFacturation.add(lbMagasin7, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- CLDVEX ----
            CLDVEX.setMinimumSize(new Dimension(115, 30));
            CLDVEX.setPreferredSize(new Dimension(115, 30));
            CLDVEX.setMaximumSize(new Dimension(115, 30));
            CLDVEX.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLDVEX.setName("CLDVEX");
            pnlFacturation.add(CLDVEX, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPremierLigne.add(pnlFacturation, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDonnees.add(pnlPremierLigne, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlDeuxiemeLigne ========
        {
          pnlDeuxiemeLigne.setName("pnlDeuxiemeLigne");
          pnlDeuxiemeLigne.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ======== pnlTitreReglement ========
          {
            pnlTitreReglement.setName("pnlTitreReglement");
            pnlTitreReglement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTitreReglement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTitreReglement.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTitreReglement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTitreReglement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- TCI4 ----
            TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TCI4.setBorder(null);
            TCI4.setName("TCI4");
            TCI4.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TCI4ActionPerformed(e);
              }
            });
            pnlTitreReglement.add(TCI4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbReglement ----
            lbReglement.setText("R\u00e8glement");
            lbReglement.setPreferredSize(new Dimension(150, 20));
            lbReglement.setMinimumSize(new Dimension(150, 20));
            lbReglement.setMaximumSize(new Dimension(150, 20));
            lbReglement.setName("lbReglement");
            pnlTitreReglement.add(lbReglement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDeuxiemeLigne.add(pnlTitreReglement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== pnlTitreCompta ========
          {
            pnlTitreCompta.setName("pnlTitreCompta");
            pnlTitreCompta.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTitreCompta.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTitreCompta.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTitreCompta.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTitreCompta.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- TCI5 ----
            TCI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TCI5.setBorder(null);
            TCI5.setName("TCI5");
            TCI5.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TCI5ActionPerformed(e);
              }
            });
            pnlTitreCompta.add(TCI5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCompta ----
            lbCompta.setText("Comptabilit\u00e9");
            lbCompta.setPreferredSize(new Dimension(150, 20));
            lbCompta.setMinimumSize(new Dimension(150, 20));
            lbCompta.setMaximumSize(new Dimension(150, 20));
            lbCompta.setName("lbCompta");
            pnlTitreCompta.add(lbCompta, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDeuxiemeLigne.add(pnlTitreCompta, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
          
          // ======== pnlTitreCompta2 ========
          {
            pnlTitreCompta2.setName("pnlTitreCompta2");
            pnlTitreCompta2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTitreCompta2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTitreCompta2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTitreCompta2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTitreCompta2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- TCI6 ----
            TCI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TCI6.setBorder(null);
            TCI6.setName("TCI6");
            TCI6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TCI6ActionPerformed(e);
              }
            });
            pnlTitreCompta2.add(TCI6, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbCompta2 ----
            lbCompta2.setText("Statistiques");
            lbCompta2.setPreferredSize(new Dimension(150, 20));
            lbCompta2.setMinimumSize(new Dimension(150, 20));
            lbCompta2.setMaximumSize(new Dimension(150, 20));
            lbCompta2.setName("lbCompta2");
            pnlTitreCompta2.add(lbCompta2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDeuxiemeLigne.add(pnlTitreCompta2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlReglement ========
          {
            pnlReglement.setMinimumSize(new Dimension(220, 110));
            pnlReglement.setPreferredSize(new Dimension(220, 110));
            pnlReglement.setMaximumSize(new Dimension(335, 2147483647));
            pnlReglement.setName("pnlReglement");
            pnlReglement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlReglement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlReglement.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlReglement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlReglement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbModeReglement ----
            lbModeReglement.setText("Mode");
            lbModeReglement.setMaximumSize(new Dimension(80, 30));
            lbModeReglement.setMinimumSize(new Dimension(80, 30));
            lbModeReglement.setPreferredSize(new Dimension(80, 30));
            lbModeReglement.setName("lbModeReglement");
            pnlReglement.add(lbModeReglement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLRGL ----
            CLRGL.setComponentPopupMenu(BTD);
            CLRGL.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLRGL.setMinimumSize(new Dimension(40, 30));
            CLRGL.setPreferredSize(new Dimension(40, 30));
            CLRGL.setName("CLRGL");
            pnlReglement.add(CLRGL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbEcheance ----
            lbEcheance.setText("Ech\u00e9ance");
            lbEcheance.setMaximumSize(new Dimension(80, 30));
            lbEcheance.setMinimumSize(new Dimension(80, 30));
            lbEcheance.setPreferredSize(new Dimension(80, 30));
            lbEcheance.setName("lbEcheance");
            pnlReglement.add(lbEcheance, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLECH ----
            CLECH.setComponentPopupMenu(BTD);
            CLECH.setPreferredSize(new Dimension(40, 30));
            CLECH.setMinimumSize(new Dimension(40, 30));
            CLECH.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLECH.setName("CLECH");
            pnlReglement.add(CLECH, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- ULBRGL ----
            ULBRGL.setFont(new Font("sansserif", Font.PLAIN, 14));
            ULBRGL.setName("ULBRGL");
            pnlReglement.add(ULBRGL, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDeuxiemeLigne.add(pnlReglement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== sNPanelTitre1 ========
          {
            sNPanelTitre1.setPreferredSize(new Dimension(320, 110));
            sNPanelTitre1.setMinimumSize(new Dimension(320, 110));
            sNPanelTitre1.setName("sNPanelTitre1");
            sNPanelTitre1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtatClient ----
            lbEtatClient.setText("Etat");
            lbEtatClient.setMinimumSize(new Dimension(60, 30));
            lbEtatClient.setPreferredSize(new Dimension(60, 30));
            lbEtatClient.setName("lbEtatClient");
            sNPanelTitre1.add(lbEtatClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLTNS ----
            CLTNS.setModel(
                new DefaultComboBoxModel(new String[] { "Client actif", "Alerte en rouge", "Client interdit devis autoris\u00e9",
                    "Acompte obligatoire", "Paiement \u00e0 la commande", "Client d\u00e9sactiv\u00e9" }));
            CLTNS.setBackground(Color.white);
            CLTNS.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLTNS.setMaximumSize(new Dimension(210, 30));
            CLTNS.setName("CLTNS");
            sNPanelTitre1.add(CLTNS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbAlerte ----
            lbAlerte.setText("Alerte");
            lbAlerte.setMinimumSize(new Dimension(60, 30));
            lbAlerte.setPreferredSize(new Dimension(60, 30));
            lbAlerte.setName("lbAlerte");
            sNPanelTitre1.add(lbAlerte, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLATT ----
            CLATT.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLATT.setMaximumSize(new Dimension(210, 30));
            CLATT.setMinimumSize(new Dimension(13, 30));
            CLATT.setPreferredSize(new Dimension(13, 30));
            CLATT.setName("CLATT");
            sNPanelTitre1.add(CLATT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 0), 0, 0));
            
            // ---- lbPlafonds ----
            lbPlafonds.setText("Plafonds");
            lbPlafonds.setMinimumSize(new Dimension(60, 30));
            lbPlafonds.setPreferredSize(new Dimension(60, 30));
            lbPlafonds.setName("lbPlafonds");
            sNPanelTitre1.add(lbPlafonds, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlPlafond ========
            {
              pnlPlafond.setName("pnlPlafond");
              pnlPlafond.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPlafond.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPlafond.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlPlafond.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPlafond.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- CLPLF ----
              CLPLF.setMinimumSize(new Dimension(80, 30));
              CLPLF.setPreferredSize(new Dimension(80, 30));
              CLPLF.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLPLF.setMaximumSize(new Dimension(80, 30));
              CLPLF.setName("CLPLF");
              pnlPlafond.add(CLPLF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- CLPLF2 ----
              CLPLF2.setPreferredSize(new Dimension(80, 30));
              CLPLF2.setMinimumSize(new Dimension(80, 30));
              CLPLF2.setFont(new Font("sansserif", Font.PLAIN, 14));
              CLPLF2.setMaximumSize(new Dimension(80, 30));
              CLPLF2.setName("CLPLF2");
              pnlPlafond.add(CLPLF2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDepassement ----
              lbDepassement.setText("D\u00e9passement");
              lbDepassement.setMaximumSize(new Dimension(80, 30));
              lbDepassement.setMinimumSize(new Dimension(80, 30));
              lbDepassement.setPreferredSize(new Dimension(50, 30));
              lbDepassement.setForeground(Color.red);
              lbDepassement.setName("lbDepassement");
              pnlPlafond.add(lbDepassement, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WEDEP ----
              WEDEP.setForeground(Color.red);
              WEDEP.setFont(new Font("sansserif", Font.PLAIN, 14));
              WEDEP.setMinimumSize(new Dimension(80, 30));
              WEDEP.setPreferredSize(new Dimension(80, 30));
              WEDEP.setMaximumSize(new Dimension(80, 30));
              WEDEP.setName("WEDEP");
              pnlPlafond.add(WEDEP, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanelTitre1.add(pnlPlafond, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDeuxiemeLigne.add(sNPanelTitre1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlStatistiques ========
          {
            pnlStatistiques.setMinimumSize(new Dimension(350, 110));
            pnlStatistiques.setPreferredSize(new Dimension(350, 110));
            pnlStatistiques.setName("pnlStatistiques");
            pnlStatistiques.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlStatistiques.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlStatistiques.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlStatistiques.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlStatistiques.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbCaMois ----
            lbCaMois.setText("CA du mois @WDSVX@");
            lbCaMois.setMaximumSize(new Dimension(120, 30));
            lbCaMois.setMinimumSize(new Dimension(120, 19));
            lbCaMois.setPreferredSize(new Dimension(120, 19));
            lbCaMois.setName("lbCaMois");
            pnlStatistiques.add(lbCaMois, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- SVCRM ----
            SVCRM.setMinimumSize(new Dimension(100, 30));
            SVCRM.setPreferredSize(new Dimension(100, 30));
            SVCRM.setFont(new Font("sansserif", Font.PLAIN, 14));
            SVCRM.setName("SVCRM");
            pnlStatistiques.add(SVCRM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbRegroupStat ----
            lbRegroupStat.setText("Regroupement");
            lbRegroupStat.setMaximumSize(new Dimension(120, 30));
            lbRegroupStat.setMinimumSize(new Dimension(120, 19));
            lbRegroupStat.setPreferredSize(new Dimension(120, 19));
            lbRegroupStat.setName("lbRegroupStat");
            pnlStatistiques.add(lbRegroupStat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- CLRST ----
            CLRST.setMinimumSize(new Dimension(100, 30));
            CLRST.setPreferredSize(new Dimension(100, 30));
            CLRST.setFont(new Font("sansserif", Font.PLAIN, 14));
            CLRST.setName("CLRST");
            pnlStatistiques.add(CLRST, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 2, 5), 0, 0));
            
            // ---- lbStatExe ----
            lbStatExe.setText("Exercice en cours");
            lbStatExe.setMaximumSize(new Dimension(120, 30));
            lbStatExe.setMinimumSize(new Dimension(120, 19));
            lbStatExe.setPreferredSize(new Dimension(120, 19));
            lbStatExe.setName("lbStatExe");
            pnlStatistiques.add(lbStatExe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SVCR1 ----
            SVCR1.setMinimumSize(new Dimension(100, 30));
            SVCR1.setPreferredSize(new Dimension(100, 30));
            SVCR1.setFont(new Font("sansserif", Font.PLAIN, 14));
            SVCR1.setName("SVCR1");
            pnlStatistiques.add(SVCR1, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbStatExePre ----
            lbStatExePre.setText("Pr\u00e9c\u00e9dent");
            lbStatExePre.setMinimumSize(new Dimension(70, 30));
            lbStatExePre.setPreferredSize(new Dimension(70, 30));
            lbStatExePre.setName("lbStatExePre");
            pnlStatistiques.add(lbStatExePre, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SVCR0 ----
            SVCR0.setMinimumSize(new Dimension(100, 30));
            SVCR0.setPreferredSize(new Dimension(100, 30));
            SVCR0.setFont(new Font("sansserif", Font.PLAIN, 14));
            SVCR0.setName("SVCR0");
            pnlStatistiques.add(SVCR0, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDeuxiemeLigne.add(pnlStatistiques, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDonnees.add(pnlDeuxiemeLigne, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlSud.add(pnlDonnees, BorderLayout.CENTER);
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu33 ========
            {
              riSousMenu33.setName("riSousMenu33");
              
              // ---- riSousMenu_bt33 ----
              riSousMenu_bt33.setText("Encours comptable");
              riSousMenu_bt33
                  .setToolTipText("Encours comptable : Position du compte, plafonds, factures en cours, effets non \u00e9chus...");
              riSousMenu_bt33.setName("riSousMenu_bt33");
              riSousMenu_bt33.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt33ActionPerformed(e);
                }
              });
              riSousMenu33.add(riSousMenu_bt33);
            }
            menus_haut.add(riSousMenu33);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Derniers \u00e9v\u00e8nements");
              riSousMenu_bt10
                  .setToolTipText("Derniers \u00e9v\u00e9nements : tableau r\u00e9capitulatif de l\u2019activit\u00e9 du client");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Fiche r\u00e9capitulative");
              riSousMenu_bt11.setToolTipText(
                  "Fiche r\u00e9capitulative : Liens entre diff\u00e9rentes fiches clients (livr\u00e9, factur\u00e9, centrales...) (Shift + F7)");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Bloc-notes");
              riSousMenu_bt16.setToolTipText("Bloc-notes : page d'informations li\u00e9es au client et articles li\u00e9s (Shift + F10)");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("M\u00e9mos");
              riSousMenu_bt14.setToolTipText("M\u00e9mos : saisie d'\u00e9v\u00e9nements g\u00e9r\u00e9s par leurs types (Shift + F11)");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setToolTipText("Documents li\u00e9s : dossiers des documents associ\u00e9s au client (F11)");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Copie du bloc adresse");
              riSousMenu_bt17.setToolTipText("Copie du bloc adresse dans le presse papier");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt17ActionPerformed(e);
                }
              });
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riSousMenuDocument ========
            {
              riSousMenuDocument.setName("riSousMenuDocument");
              
              // ---- riAfficherDocumentStocke ----
              riAfficherDocumentStocke.setText("Afficher les documents");
              riAfficherDocumentStocke.setName("riAfficherDocumentStocke");
              riAfficherDocumentStocke.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riAfficherDocumentStockeActionPerformed(e);
                }
              });
              riSousMenuDocument.add(riAfficherDocumentStocke);
            }
            menus_haut.add(riSousMenuDocument);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Compl\u00e9ments");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Extensions fiche client");
              riSousMenu_bt7.setToolTipText("Extensions fiche client : zones de texte param\u00e9tr\u00e9es, libres en saisie");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu50 ========
            {
              riSousMenu50.setName("riSousMenu50");
              
              // ---- riSousMenu_bt50 ----
              riSousMenu_bt50.setText("Lien client/fournisseur");
              riSousMenu_bt50.setToolTipText("Extensions fiche client : zones de texte param\u00e9tr\u00e9es, libres en saisie");
              riSousMenu_bt50.setName("riSousMenu_bt50");
              riSousMenu_bt50.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt50ActionPerformed(e);
                }
              });
              riSousMenu50.add(riSousMenu_bt50);
            }
            menus_haut.add(riSousMenu50);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Zones personnalis\u00e9es");
              riSousMenu_bt8
                  .setToolTipText("Zones personnalis\u00e9es : zones de 2 caract\u00e8res param\u00e9tr\u00e9es (param\u00e8tre ZP)");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Affichage personnalis\u00e9");
              riSousMenu_bt9.setToolTipText("Affichage personnalis\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu26 ========
            {
              riSousMenu26.setName("riSousMenu26");
              
              // ---- riSousMenu_bt26 ----
              riSousMenu_bt26.setText("Sp\u00e9cifique");
              riSousMenu_bt26.setName("riSousMenu_bt26");
              riSousMenu_bt26.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt26ActionPerformed(e);
                }
              });
              riSousMenu26.add(riSousMenu_bt26);
            }
            menus_haut.add(riSousMenu26);
            
            // ======== riSousMenu32 ========
            {
              riSousMenu32.setName("riSousMenu32");
              
              // ---- riSousMenu_bt32 ----
              riSousMenu_bt32.setText("CNV par article");
              riSousMenu_bt32.setToolTipText("CNV par article : saisie sp\u00e9ciale des conditions de vente par article");
              riSousMenu_bt32.setName("riSousMenu_bt32");
              riSousMenu_bt32.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt32ActionPerformed(e);
                }
              });
              riSousMenu32.add(riSousMenu_bt32);
            }
            menus_haut.add(riSousMenu32);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");
              
              // ---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Gestion CRM");
              riSousMenu_bt18.setToolTipText("Gestion CRM : relation client (scoring, synth\u00e8se statistique)");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Internet");
              riSousMenu_bt2.setToolTipText("Internet : Site(s) internet du client");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Fiche prospect");
              riSousMenu_bt19.setToolTipText("Fiche prospect : zones li\u00e9es \u00e0 la prospection pour ce client (Shift + F5)");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
            
            // ======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");
              
              // ---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Fiche mat\u00e9riel");
              riSousMenu_bt20.setToolTipText("Fiche mat\u00e9riel : articles s\u00e9rialis\u00e9s vendus \u00e0 ce client");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);
            
            // ======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");
              
              // ---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Lots");
              riSousMenu_bt21.setToolTipText("Lots : articles lotis vendus \u00e0 ce client");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Articles pr\u00e9t\u00e9s");
              riSousMenu_bt3.setToolTipText("Articles pr\u00eat\u00e9s : articles dans le magasin de pr\u00eats");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
            
            // ======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");
              
              // ---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Articles consign\u00e9s");
              riSousMenu_bt4.setToolTipText("Articles consign\u00e9s : liste des emballages et consignes en cours");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Historique modifications");
              riSousMenu_bt13.setToolTipText("Historique des modifications apport\u00e9es \u00e0 cette fiche client");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");
              
              // ---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("Historique de vente");
              riSousMenu_bt24.setToolTipText("Historique de vente : d\u00e9tail des articles vendus par magasin");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);
            
            // ======== riSousMenu29 ========
            {
              riSousMenu29.setName("riSousMenu29");
              
              // ---- riSousMenu_bt29 ----
              riSousMenu_bt29.setText("Histo. d\u00e9blocage encours");
              riSousMenu_bt29.setToolTipText("Historique du d\u00e9blocage de l'encours");
              riSousMenu_bt29.setName("riSousMenu_bt29");
              riSousMenu_bt29.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt29ActionPerformed(e);
                }
              });
              riSousMenu29.add(riSousMenu_bt29);
            }
            menus_haut.add(riSousMenu29);
            
            // ======== riSousMenu27 ========
            {
              riSousMenu27.setName("riSousMenu27");
              
              // ---- riSousMenu_bt27 ----
              riSousMenu_bt27.setText("D\u00e9blocage encours");
              riSousMenu_bt27.setToolTipText("D\u00e9blocage de l'encours");
              riSousMenu_bt27.setName("riSousMenu_bt27");
              riSousMenu_bt27.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt27ActionPerformed(e);
                }
              });
              riSousMenu27.add(riSousMenu_bt27);
            }
            menus_haut.add(riSousMenu27);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      pnlSud.add(p_menus, BorderLayout.EAST);
    }
    add(pnlSud, BorderLayout.CENTER);
    
    // ======== BTDA ========
    {
      BTDA.setName("BTDA");
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_5);
    }
    
    // ======== CMD ========
    {
      CMD.setName("CMD");
      
      // ---- OBJ_9 ----
      OBJ_9.setText("Choix possibles");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      
      // ---- OBJ_10 ----
      OBJ_10.setText("R\u00e9afficher");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Recherche multi-crit\u00e8res");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Fonctions");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Annuler");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);
      CMD.addSeparator();
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Tableau de synth\u00e8se");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Fiche client simplif\u00e9e");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      CMD.add(OBJ_15);
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Fiche observations");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      CMD.add(OBJ_16);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("M\u00e9mo");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      CMD.add(OBJ_17);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Autre affichage");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      CMD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Historique de modifications");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      CMD.add(OBJ_19);
    }
    
    // ======== riSousMenu12 ========
    {
      riSousMenu12.setName("riSousMenu12");
    }
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);
      
      // ---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }
    
    // ======== riSousMenu1 ========
    {
      riSousMenu1.setName("riSousMenu1");
    }
    
    // ======== riSousMenu5 ========
    {
      riSousMenu5.setName("riSousMenu5");
    }
    
    // ---- riSousMenu_bt1 ----
    riSousMenu_bt1.setText("Envoi de mails");
    riSousMenu_bt1.setEnabled(false);
    riSousMenu_bt1.setName("riSousMenu_bt1");
    riSousMenu_bt1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riSousMenu_bt1ActionPerformed(e);
      }
    });
    
    // ---- riSousMenu_bt12 ----
    riSousMenu_bt12.setText("Actions commerciales");
    riSousMenu_bt12.setToolTipText("Actions commerciales");
    riSousMenu_bt12.setName("riSousMenu_bt12");
    riSousMenu_bt12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riSousMenu_bt12ActionPerformed(e);
      }
    });
    
    // ---- riSousMenu_bt5 ----
    riSousMenu_bt5.setText("Garanties/reprises");
    riSousMenu_bt5.setToolTipText("Garanties/reprises");
    riSousMenu_bt5.setName("riSousMenu_bt5");
    riSousMenu_bt5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riSousMenu_bt5ActionPerformed(e);
      }
    });
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbTypeClient);
    buttonGroup1.add(rbTypeProspect);
    
    // ---- buttonGroupCategorie ----
    buttonGroupCategorie.add(CLIN9);
    buttonGroupCategorie.add(CLIN9_1);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJECT_3;
  private XRiTextField INDETB;
  private JLabel OBJECT_4;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private JLabel label6;
  private JLabel labelHisto;
  private JPanel p_tete_droite;
  private JLabel lab_desac;
  private JLabel bt_ecran;
  private SNPanel pnlSud;
  private SNPanelContenu pnlDonnees;
  private JPanel pnlCoordonneesClient;
  private SNPanel pnlChoixTypeClient;
  private SNRadioButton rbTypeClient;
  private SNRadioButton rbTypeProspect;
  private SNLabelChamp lbCivilite;
  private SNCivilite snCivilite;
  private SNLabelChamp lbNom;
  private SNPanel pnlNom;
  private XRiTextField RENOM;
  private XRiTextField CLNOM;
  private SNLabelChamp lbComplementNom;
  private SNPanel pnlPrenom;
  private XRiTextField REPRE;
  private XRiTextField CLCPL;
  private SNLabelChamp lbLocalisation;
  private XRiTextField CLRUE;
  private SNLabelChamp lbRue;
  private XRiTextField CLLOC;
  private SNLabelChamp lbCodePostal;
  private SNCodePostalCommune snCodePostalCommune;
  private SNLabelChamp lbCodePostal2;
  private SNPanel sNPanel6;
  private SNPays snPays;
  private JTabbedPane tbpInfos;
  private SNPanelContenu pnlInfosPrincipales;
  private SNLabelChamp lbType;
  private SNPanel sNPanel1;
  private XRiRadioButton CLIN9;
  private XRiRadioButton CLIN9_1;
  private JButton OBJ_117;
  private SNLabelChamp lbClassement1;
  private XRiTextField CLCLK;
  private SNLabelChamp lbClassement2;
  private XRiTextField CLCLA;
  private SNLabelChamp lbCategorie;
  private SNCategorieClient snCategorieClient;
  private SNLabelChamp lbRep1;
  private SNRepresentant snRepresentant1;
  private SNLabelChamp lbRep2;
  private SNRepresentant snRepresentant2;
  private SNLabelChamp lbObservations;
  private XRiTextField CLOBS;
  private SNPanelContenu pnlInfosAutres;
  private SNLabelChamp lbEffectif;
  private XRiComboBox CLIN23;
  private SNLabelChamp lbNaf;
  private XRiTextField CLAPEN;
  private SNLabelChamp lbGroupe;
  private SNPanel pnlGroupe;
  private XRiTextField CLCL1;
  private RiZoneSortie OBJ_86;
  private SNLabelChamp lbFamille;
  private SNPanel pnlFamille;
  private XRiTextField CLCL2;
  private RiZoneSortie OBJ_95;
  private SNLabelChamp lbSousFamille;
  private SNPanel pnlSousFamille;
  private XRiTextField CLCL3;
  private RiZoneSortie OBJ_103;
  private SNLabelChamp lbcomRep;
  private XRiTextField CLTRP;
  private SNLabelChamp lbFamille2;
  private SNPanel sNPanel3;
  private XRiTextField GCDPR;
  private SNBoutonDetail btGencod;
  private SNPanel pnlPremierLigne;
  private SNPanel pnllTitreContact;
  private SNBoutonDetail btnListeContact;
  private SNLabelTitre lbTitreContact;
  private SNPanel pnllTitreExpedition;
  private SNBoutonDetail TCI2;
  private SNLabelTitre lbTitreLivraison;
  private SNPanel pnlTitreFacturation;
  private SNBoutonDetail TCI3;
  private SNLabelTitre lbTitreFacturation;
  private SNPanel sNPanel2;
  private SNPanel pnlContact;
  private SNLabelChamp lbContact;
  private SNContact snContact;
  private SNLabelChamp lbMail;
  private SNAdresseMail snAdresseMail;
  private SNLabelChamp lbTelephone;
  private XRiTextField CLTEL;
  private XRiTextField CLTEL2;
  private SNLabelChamp lbFax;
  private XRiTextField CLFAX;
  private SNPanel pnlExpedition;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbModeExpedition;
  private SNModeExpedition snModeExpedition;
  private SNLabelChamp lbMagasin3;
  private SNZoneGeographique snZoneGeographique;
  private SNLabelChamp lbTransporteur;
  private SNTransporteur snTransporteur;
  private SNLabelChamp lbFranco;
  private SNPanel sNPanel4;
  private XRiTextField CLFRP;
  private JLabel OBJ_156;
  private SNPanel pnlFacturation;
  private SNLabelChamp lbTypeFacturation;
  private SNTypeFacturation snTypeFacturation;
  private SNLabelChamp lbTarif;
  private SNPanel sNPanel5;
  private XRiTextField WTAR;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNLabelChamp lbRemises;
  private SNPanel pnlRemises;
  private XRiTextField CLREM1;
  private XRiTextField CLREM2;
  private XRiTextField CLREM3;
  private SNLabelChamp lbCondition;
  private SNConditionVenteEtClient snConditionVenteEtClient;
  private SNLabelChamp lbMagasin7;
  private XRiCalendrier CLDVEX;
  private SNPanel pnlDeuxiemeLigne;
  private SNPanel pnlTitreReglement;
  private SNBoutonDetail TCI4;
  private SNLabelTitre lbReglement;
  private SNPanel pnlTitreCompta;
  private SNBoutonDetail TCI5;
  private SNLabelTitre lbCompta;
  private SNPanel pnlTitreCompta2;
  private SNBoutonDetail TCI6;
  private SNLabelTitre lbCompta2;
  private SNPanelTitre pnlReglement;
  private SNLabelChamp lbModeReglement;
  private XRiTextField CLRGL;
  private SNLabelChamp lbEcheance;
  private XRiTextField CLECH;
  private XRiTextField ULBRGL;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbEtatClient;
  private XRiComboBox CLTNS;
  private SNLabelChamp lbAlerte;
  private XRiTextField CLATT;
  private SNLabelChamp lbPlafonds;
  private SNPanel pnlPlafond;
  private XRiTextField CLPLF;
  private XRiTextField CLPLF2;
  private SNLabelChamp lbDepassement;
  private XRiTextField WEDEP;
  private SNPanelTitre pnlStatistiques;
  private SNLabelChamp lbCaMois;
  private XRiTextField SVCRM;
  private SNLabelChamp lbRegroupStat;
  private XRiTextField CLRST;
  private SNLabelChamp lbStatExe;
  private XRiTextField SVCR1;
  private SNLabelChamp lbStatExePre;
  private XRiTextField SVCR0;
  private JPanel p_menus;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu33;
  private RiSousMenu_bt riSousMenu_bt33;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenuDocument;
  private RiSousMenu_bt riAfficherDocumentStocke;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu50;
  private RiSousMenu_bt riSousMenu_bt50;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu26;
  private RiSousMenu_bt riSousMenu_bt26;
  private RiSousMenu riSousMenu32;
  private RiSousMenu_bt riSousMenu_bt32;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiSousMenu riSousMenu29;
  private RiSousMenu_bt riSousMenu_bt29;
  private RiSousMenu riSousMenu27;
  private RiSousMenu_bt riSousMenu_bt27;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_5;
  private JPopupMenu CMD;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private RiSousMenu riSousMenu12;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  private RiSousMenu riSousMenu1;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu_bt riSousMenu_bt5;
  private ButtonGroup buttonGroupCategorie;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
