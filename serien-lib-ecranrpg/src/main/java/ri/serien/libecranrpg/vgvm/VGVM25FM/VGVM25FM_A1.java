
package ri.serien.libecranrpg.vgvm.VGVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM25FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM25FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NDOR@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NDEB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    if (lexique.isTrue("92") && !lexique.isTrue("82")) {
      OBJ_17.setVisible(true);
      OBJ_17.setText("N° de devis :");
      NUM1.setVisible(true);
      SUF1.setVisible(true);
    }
    
    if (lexique.isTrue("91")) {
      OBJ_17.setVisible(true);
      OBJ_17.setText("N° de devis :");
      NUM1.setVisible(true);
      SUF1.setVisible(true);
    }
    if (lexique.isTrue("91") && !lexique.isTrue("82")) {
      OBJ_20.setVisible(true);
      OBJ_20.setText("N° de bon :");
      NUM2.setVisible(true);
      SUF2.setVisible(true);
    }
    
    if (lexique.isTrue("92")) {
      OBJ_20.setVisible(true);
      OBJ_20.setText("N° de bon :");
      NUM2.setVisible(true);
      SUF2.setVisible(true);
      setTitle(interpreteurD.analyseExpression("Duplication d'un bon en devis"));
    }
    if (lexique.isTrue("93")) {
      OBJ_17.setVisible(true);
      OBJ_17.setText("N° d'origine :");
      NUM1.setVisible(true);
      NUM2.setVisible(true);
      SUF1.setVisible(true);
      SUF2.setVisible(true);
      OBJ_20.setVisible(true);
      OBJ_20.setText("Nouveau N° :");
    }
    
    if (lexique.isTrue("91") && !lexique.isTrue("99")) {
      setTitle(interpreteurD.analyseExpression("Duplication d'un devis en bon"));
    }
    
    if (lexique.isTrue("91") && lexique.isTrue("99")) {
      setTitle(interpreteurD.analyseExpression("Transformation d'un devis en bon"));
    }
    
    if (lexique.isTrue("93") && !lexique.isTrue("95")) {
      setTitle(interpreteurD.analyseExpression("Duplication de bon ou devis"));
    }
    
    if (lexique.isTrue("93") && lexique.isTrue("95")) {
      setTitle(interpreteurD.analyseExpression("Avoir automatique sur bon/facture"));
    }
    
    SUF1.setVisible(!lexique.HostFieldGetData("SUF1").trim().equalsIgnoreCase(""));
    SUF2.setVisible(!lexique.HostFieldGetData("SUF2").trim().equalsIgnoreCase(""));
    
    mess1.setVisible(lexique.HostFieldGetData("NUM2").trim().equalsIgnoreCase(""));
    mess2.setVisible(!lexique.HostFieldGetData("NUM2").trim().equalsIgnoreCase(""));
    navig_retour.setVisible(lexique.HostFieldGetData("NUM2").trim().equalsIgnoreCase(""));
    bouton_retour.setVisible(lexique.HostFieldGetData("NUM2").trim().equalsIgnoreCase(""));
    repaint();
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // lexique.HostCursorPut(getInvoker().getName());
    // lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void NUM2InputMethodTextChanged(InputMethodEvent e) {
    navig_retour.setVisible(lexique.HostFieldGetData("NUM2").trim().equalsIgnoreCase(""));
    bouton_retour.setVisible(lexique.HostFieldGetData("NUM2").trim().equalsIgnoreCase(""));
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_17 = new JLabel();
    NUM1 = new XRiTextField();
    SUF1 = new XRiTextField();
    OBJ_20 = new JLabel();
    NUM2 = new XRiTextField();
    SUF2 = new XRiTextField();
    mess1 = new JLabel();
    mess2 = new JLabel();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(635, 170));
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- OBJ_17 ----
          OBJ_17.setText("@NDOR@");
          OBJ_17.setName("OBJ_17");
          panel3.add(OBJ_17);
          OBJ_17.setBounds(23, 24, 87, 20);

          //---- NUM1 ----
          NUM1.setName("NUM1");
          panel3.add(NUM1);
          NUM1.setBounds(115, 20, 58, NUM1.getPreferredSize().height);

          //---- SUF1 ----
          SUF1.setName("SUF1");
          panel3.add(SUF1);
          SUF1.setBounds(175, 20, 24, SUF1.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("@NDEB@");
          OBJ_20.setName("OBJ_20");
          panel3.add(OBJ_20);
          OBJ_20.setBounds(223, 24, 87, 20);

          //---- NUM2 ----
          NUM2.setName("NUM2");
          NUM2.addInputMethodListener(new InputMethodListener() {
            @Override
            public void caretPositionChanged(InputMethodEvent e) {}
            @Override
            public void inputMethodTextChanged(InputMethodEvent e) {
              NUM2InputMethodTextChanged(e);
            }
          });
          panel3.add(NUM2);
          NUM2.setBounds(305, 20, 58, NUM2.getPreferredSize().height);

          //---- SUF2 ----
          SUF2.setName("SUF2");
          panel3.add(SUF2);
          SUF2.setBounds(365, 20, 24, SUF2.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(21, 44, 425, 70);

        //---- mess1 ----
        mess1.setText("Pressez 'Valider' pour confirmer ou 'Retour' pour annuler");
        mess1.setHorizontalAlignment(SwingConstants.CENTER);
        mess1.setFont(mess1.getFont().deriveFont(mess1.getFont().getStyle() | Font.BOLD, mess1.getFont().getSize() + 3f));
        mess1.setName("mess1");
        p_contenu.add(mess1);
        mess1.setBounds(21, 121, 425, 30);

        //---- mess2 ----
        mess2.setText("Pressez 'Valider' pour acc\u00e9der au nouveau bon ou devis");
        mess2.setHorizontalAlignment(SwingConstants.CENTER);
        mess2.setFont(mess2.getFont().deriveFont(mess2.getFont().getStyle() | Font.BOLD, mess2.getFont().getSize() + 3f));
        mess2.setName("mess2");
        p_contenu.add(mess2);
        mess2.setBounds(21, 121, 425, 30);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== riSousMenu6 ========
    {
      riSousMenu6.setName("riSousMenu6");

      //---- riSousMenu_bt6 ----
      riSousMenu_bt6.setText("D\u00e9tail des lignes");
      riSousMenu_bt6.setToolTipText("D\u00e9tail des lignes");
      riSousMenu_bt6.setName("riSousMenu_bt6");
      riSousMenu_bt6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt6ActionPerformed(e);
        }
      });
      riSousMenu6.add(riSousMenu_bt6);
    }

    //======== riMenu2 ========
    {
      riMenu2.setName("riMenu2");

      //---- riMenu_bt2 ----
      riMenu_bt2.setText("Options");
      riMenu_bt2.setName("riMenu_bt2");
      riMenu2.add(riMenu_bt2);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel OBJ_17;
  private XRiTextField NUM1;
  private XRiTextField SUF1;
  private JLabel OBJ_20;
  private XRiTextField NUM2;
  private XRiTextField SUF2;
  private JLabel mess1;
  private JLabel mess2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
