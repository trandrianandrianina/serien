
package ri.serien.libecranrpg.vgvm.VGVM52FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM52FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "UTIT", "Prix Conso.", "Err", };
  private String[][] _WTP01_Data = { { "LD01", "ZL21", "PL01", }, { "LD02", "ZL22", "PL02", }, { "LD03", "ZL23", "PL03", },
      { "LD04", "ZL24", "PL04", }, { "LD05", "ZL25", "PL05", }, { "LD06", "ZL26", "PL06", }, { "LD07", "ZL27", "PL07", },
      { "LD08", "ZL28", "PL08", }, { "LD09", "ZL29", "PL09", }, { "LD10", "ZL210", "PL10", }, { "LD11", "ZL211", "PL11", },
      { "LD12", "ZL212", "PL12", }, { "LD13", "ZL213", "PL13", }, { "LD14", "ZL214", "PL14", }, { "LD15", "ZL215", "PL15", }, };
  private int[] _WTP01_Width = { 381, 56, 17, };
  
  public VGVM52FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, true, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LD01R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01R@")).trim());
    LD02R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02R@")).trim());
    LD03R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03R@")).trim());
    LD04R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04R@")).trim());
    LD05R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05R@")).trim());
    LD06R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06R@")).trim());
    LD07R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07R@")).trim());
    LD08R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08R@")).trim());
    LD09R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09R@")).trim());
    LD10R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10R@")).trim());
    LD11R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11R@")).trim());
    LD12R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12R@")).trim());
    LD13R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13R@")).trim());
    LD14R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14R@")).trim());
    LD15R.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15R@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UTITR@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT1@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT2@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL01@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL02@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL03@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL04@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL05@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL06@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL07@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL08@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL09@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL10@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL11@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL12@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL13@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL14@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PL15@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    panel1.setVisible(lexique.isTrue("91"));
    panel2.setVisible(lexique.isTrue("N91"));
    if (lexique.isTrue("91")) {
      p_contenu.setPreferredSize(new Dimension(800, 380));
    }
    else {
      p_contenu.setPreferredSize(new Dimension(1020, 500));
    }
    
    if (lexique.isTrue("91")) {
      p_bpresentation.setText("Saisie des prix de la concurence");
    }
    else {
      p_bpresentation.setText("Informations sur les prix consommateur");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    BT_PGDOWN2.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP2.setIcon(lexique.chargerImage("images/pgup20.png", true));
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void LIST1MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIR2ActionPerformed(ActionEvent e) {
    String nomZone = BTD2.getInvoker().getName();
    int ligne;
    if (nomZone.length() == 5) {
      ligne = Integer.parseInt(nomZone.substring(3, 5));
    }
    else {
      if (nomZone.startsWith("X")) {
        ligne = Integer.parseInt(nomZone.substring(3, 4));
      }
      else {
        ligne = Integer.parseInt(nomZone.substring(2, 4));
      }
    }
    String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
    lexique.HostFieldPutData(top, 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void LD01RMouseClicked(MouseEvent e) {
    if (e.getClickCount() == 2) {
      String nomZone = e.getComponent().getName();
      int ligne;
      if (nomZone.length() == 5) {
        ligne = Integer.parseInt(nomZone.substring(3, 5));
      }
      else {
        if (nomZone.startsWith("X")) {
          ligne = Integer.parseInt(nomZone.substring(3, 4));
        }
        else {
          ligne = Integer.parseInt(nomZone.substring(2, 4));
        }
      }
      String top = "WTP" + String.valueOf((ligne) < 10 ? "0" + (ligne) : (ligne));
      lexique.HostFieldPutData(top, 0, "1");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void BT_PGUP2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void BT_PGDOWN2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    CLNOM = new RiZoneSortie();
    OBJ_27 = new JLabel();
    WETB = new XRiTextField();
    OBJ_28 = new JLabel();
    WCNV = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    SCROLLPANE_LIST1 = new JScrollPane();
    WTP01 = new XRiTable();
    panel2 = new JPanel();
    BT_PGUP2 = new JButton();
    BT_PGDOWN2 = new JButton();
    LD01R = new RiZoneSortie();
    XL01 = new XRiTextField();
    ZL71 = new XRiTextField();
    ZL21 = new XRiTextField();
    LD02R = new RiZoneSortie();
    XL02 = new XRiTextField();
    ZL72 = new XRiTextField();
    ZL22 = new XRiTextField();
    LD03R = new RiZoneSortie();
    XL03 = new XRiTextField();
    ZL73 = new XRiTextField();
    ZL23 = new XRiTextField();
    LD04R = new RiZoneSortie();
    XL04 = new XRiTextField();
    ZL74 = new XRiTextField();
    ZL24 = new XRiTextField();
    LD05R = new RiZoneSortie();
    XL05 = new XRiTextField();
    ZL75 = new XRiTextField();
    ZL25 = new XRiTextField();
    LD06R = new RiZoneSortie();
    XL06 = new XRiTextField();
    ZL76 = new XRiTextField();
    ZL26 = new XRiTextField();
    LD07R = new RiZoneSortie();
    XL07 = new XRiTextField();
    ZL77 = new XRiTextField();
    ZL27 = new XRiTextField();
    LD08R = new RiZoneSortie();
    XL08 = new XRiTextField();
    ZL78 = new XRiTextField();
    ZL28 = new XRiTextField();
    LD09R = new RiZoneSortie();
    XL09 = new XRiTextField();
    ZL79 = new XRiTextField();
    ZL29 = new XRiTextField();
    LD10R = new RiZoneSortie();
    XL10 = new XRiTextField();
    ZL710 = new XRiTextField();
    ZL210 = new XRiTextField();
    LD11R = new RiZoneSortie();
    XL11 = new XRiTextField();
    ZL711 = new XRiTextField();
    ZL211 = new XRiTextField();
    LD12R = new RiZoneSortie();
    XL12 = new XRiTextField();
    ZL712 = new XRiTextField();
    ZL212 = new XRiTextField();
    LD13R = new RiZoneSortie();
    XL13 = new XRiTextField();
    ZL713 = new XRiTextField();
    ZL213 = new XRiTextField();
    LD14R = new RiZoneSortie();
    XL14 = new XRiTextField();
    ZL714 = new XRiTextField();
    ZL214 = new XRiTextField();
    LD15R = new RiZoneSortie();
    XL15 = new XRiTextField();
    ZL715 = new XRiTextField();
    ZL215 = new XRiTextField();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    label19 = new JLabel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR2 = new JMenuItem();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Informations sur les prix consommateur");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- CLNOM ----
          CLNOM.setOpaque(false);
          CLNOM.setText("@CLNOM@");
          CLNOM.setName("CLNOM");

          //---- OBJ_27 ----
          OBJ_27.setText("Etablissement");
          OBJ_27.setName("OBJ_27");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- OBJ_28 ----
          OBJ_28.setText("Client");
          OBJ_28.setName("OBJ_28");

          //---- WCNV ----
          WCNV.setComponentPopupMenu(BTD);
          WCNV.setName("WCNV");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(CLNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Articles d\u00e9sactiv\u00e9s");
              riSousMenu_bt7.setToolTipText("ON/OFF Affichage des articles d\u00e9sactiv\u00e9s");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Autre vue");
              riSousMenu_bt8.setToolTipText("Autre affichage en recherche articles");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Recherche article");
              riSousMenu_bt1.setToolTipText("Recherche article");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(710, 40, 25, 127);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(710, 185, 25, 127);

            //======== SCROLLPANE_LIST1 ========
            {
              SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  LIST1MouseClicked(e);
                }
              });
              SCROLLPANE_LIST1.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST1);
            SCROLLPANE_LIST1.setBounds(20, 40, 680, 270);
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 770, 337);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- BT_PGUP2 ----
            BT_PGUP2.setText("");
            BT_PGUP2.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP2.setName("BT_PGUP2");
            BT_PGUP2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGUP2ActionPerformed(e);
              }
            });
            panel2.add(BT_PGUP2);
            BT_PGUP2.setBounds(955, 58, 25, 177);

            //---- BT_PGDOWN2 ----
            BT_PGDOWN2.setText("");
            BT_PGDOWN2.setToolTipText("Page suivante");
            BT_PGDOWN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN2.setName("BT_PGDOWN2");
            BT_PGDOWN2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_PGDOWN2ActionPerformed(e);
              }
            });
            panel2.add(BT_PGDOWN2);
            BT_PGDOWN2.setBounds(955, 259, 25, 177);

            //---- LD01R ----
            LD01R.setText("@LD01R@");
            LD01R.setFont(new Font("Courier New", Font.PLAIN, 11));
            LD01R.setComponentPopupMenu(BTD2);
            LD01R.setName("LD01R");
            LD01R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD01R);
            LD01R.setBounds(10, 60, 355, LD01R.getPreferredSize().height);

            //---- XL01 ----
            XL01.setComponentPopupMenu(BTD2);
            XL01.setForeground(Color.black);
            XL01.setName("XL01");
            XL01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL01);
            XL01.setBounds(370, 58, 240, XL01.getPreferredSize().height);

            //---- ZL71 ----
            ZL71.setComponentPopupMenu(BTD2);
            ZL71.setForeground(Color.black);
            ZL71.setName("ZL71");
            ZL71.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL71);
            ZL71.setBounds(610, 58, 240, ZL71.getPreferredSize().height);

            //---- ZL21 ----
            ZL21.setComponentPopupMenu(BTD2);
            ZL21.setName("ZL21");
            ZL21.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL21);
            ZL21.setBounds(850, 58, 100, ZL21.getPreferredSize().height);

            //---- LD02R ----
            LD02R.setText("@LD02R@");
            LD02R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD02R.setComponentPopupMenu(BTD2);
            LD02R.setName("LD02R");
            LD02R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD02R);
            LD02R.setBounds(10, 85, 355, LD02R.getPreferredSize().height);

            //---- XL02 ----
            XL02.setComponentPopupMenu(BTD2);
            XL02.setForeground(Color.black);
            XL02.setName("XL02");
            XL02.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL02);
            XL02.setBounds(370, 83, 240, XL02.getPreferredSize().height);

            //---- ZL72 ----
            ZL72.setComponentPopupMenu(BTD2);
            ZL72.setForeground(Color.black);
            ZL72.setName("ZL72");
            ZL72.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL72);
            ZL72.setBounds(610, 83, 240, ZL72.getPreferredSize().height);

            //---- ZL22 ----
            ZL22.setComponentPopupMenu(BTD2);
            ZL22.setName("ZL22");
            ZL22.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL22);
            ZL22.setBounds(850, 83, 100, ZL22.getPreferredSize().height);

            //---- LD03R ----
            LD03R.setText("@LD03R@");
            LD03R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD03R.setComponentPopupMenu(BTD2);
            LD03R.setName("LD03R");
            LD03R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD03R);
            LD03R.setBounds(10, 110, 355, LD03R.getPreferredSize().height);

            //---- XL03 ----
            XL03.setComponentPopupMenu(BTD2);
            XL03.setForeground(Color.black);
            XL03.setName("XL03");
            XL03.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL03);
            XL03.setBounds(370, 108, 240, XL03.getPreferredSize().height);

            //---- ZL73 ----
            ZL73.setComponentPopupMenu(BTD2);
            ZL73.setForeground(Color.black);
            ZL73.setName("ZL73");
            ZL73.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL73);
            ZL73.setBounds(610, 108, 240, ZL73.getPreferredSize().height);

            //---- ZL23 ----
            ZL23.setComponentPopupMenu(BTD2);
            ZL23.setName("ZL23");
            ZL23.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL23);
            ZL23.setBounds(850, 108, 100, ZL23.getPreferredSize().height);

            //---- LD04R ----
            LD04R.setText("@LD04R@");
            LD04R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD04R.setComponentPopupMenu(BTD2);
            LD04R.setName("LD04R");
            LD04R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD04R);
            LD04R.setBounds(10, 135, 355, LD04R.getPreferredSize().height);

            //---- XL04 ----
            XL04.setComponentPopupMenu(BTD2);
            XL04.setForeground(Color.black);
            XL04.setName("XL04");
            XL04.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL04);
            XL04.setBounds(370, 133, 240, XL04.getPreferredSize().height);

            //---- ZL74 ----
            ZL74.setComponentPopupMenu(BTD2);
            ZL74.setForeground(Color.black);
            ZL74.setName("ZL74");
            ZL74.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL74);
            ZL74.setBounds(610, 133, 240, ZL74.getPreferredSize().height);

            //---- ZL24 ----
            ZL24.setComponentPopupMenu(BTD2);
            ZL24.setName("ZL24");
            ZL24.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL24);
            ZL24.setBounds(850, 133, 100, ZL24.getPreferredSize().height);

            //---- LD05R ----
            LD05R.setText("@LD05R@");
            LD05R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD05R.setComponentPopupMenu(BTD2);
            LD05R.setName("LD05R");
            LD05R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD05R);
            LD05R.setBounds(10, 160, 355, LD05R.getPreferredSize().height);

            //---- XL05 ----
            XL05.setComponentPopupMenu(BTD2);
            XL05.setForeground(Color.black);
            XL05.setName("XL05");
            XL05.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL05);
            XL05.setBounds(370, 158, 240, XL05.getPreferredSize().height);

            //---- ZL75 ----
            ZL75.setComponentPopupMenu(BTD2);
            ZL75.setForeground(Color.black);
            ZL75.setName("ZL75");
            ZL75.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL75);
            ZL75.setBounds(610, 158, 240, ZL75.getPreferredSize().height);

            //---- ZL25 ----
            ZL25.setComponentPopupMenu(BTD2);
            ZL25.setName("ZL25");
            ZL25.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL25);
            ZL25.setBounds(850, 158, 100, ZL25.getPreferredSize().height);

            //---- LD06R ----
            LD06R.setText("@LD06R@");
            LD06R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD06R.setComponentPopupMenu(BTD2);
            LD06R.setName("LD06R");
            LD06R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD06R);
            LD06R.setBounds(10, 185, 355, LD06R.getPreferredSize().height);

            //---- XL06 ----
            XL06.setComponentPopupMenu(BTD2);
            XL06.setForeground(Color.black);
            XL06.setName("XL06");
            XL06.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL06);
            XL06.setBounds(370, 183, 240, XL06.getPreferredSize().height);

            //---- ZL76 ----
            ZL76.setComponentPopupMenu(BTD2);
            ZL76.setForeground(Color.black);
            ZL76.setName("ZL76");
            ZL76.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL76);
            ZL76.setBounds(610, 183, 240, ZL76.getPreferredSize().height);

            //---- ZL26 ----
            ZL26.setComponentPopupMenu(BTD2);
            ZL26.setName("ZL26");
            ZL26.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL26);
            ZL26.setBounds(850, 183, 100, ZL26.getPreferredSize().height);

            //---- LD07R ----
            LD07R.setText("@LD07R@");
            LD07R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD07R.setComponentPopupMenu(BTD2);
            LD07R.setName("LD07R");
            LD07R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD07R);
            LD07R.setBounds(10, 210, 355, LD07R.getPreferredSize().height);

            //---- XL07 ----
            XL07.setComponentPopupMenu(BTD2);
            XL07.setForeground(Color.black);
            XL07.setName("XL07");
            XL07.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL07);
            XL07.setBounds(370, 208, 240, XL07.getPreferredSize().height);

            //---- ZL77 ----
            ZL77.setComponentPopupMenu(BTD2);
            ZL77.setForeground(Color.black);
            ZL77.setName("ZL77");
            ZL77.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL77);
            ZL77.setBounds(610, 208, 240, ZL77.getPreferredSize().height);

            //---- ZL27 ----
            ZL27.setComponentPopupMenu(BTD2);
            ZL27.setName("ZL27");
            ZL27.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL27);
            ZL27.setBounds(850, 208, 100, ZL27.getPreferredSize().height);

            //---- LD08R ----
            LD08R.setText("@LD08R@");
            LD08R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD08R.setComponentPopupMenu(BTD2);
            LD08R.setName("LD08R");
            LD08R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD08R);
            LD08R.setBounds(10, 235, 355, LD08R.getPreferredSize().height);

            //---- XL08 ----
            XL08.setComponentPopupMenu(BTD2);
            XL08.setForeground(Color.black);
            XL08.setName("XL08");
            XL08.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL08);
            XL08.setBounds(370, 233, 240, XL08.getPreferredSize().height);

            //---- ZL78 ----
            ZL78.setComponentPopupMenu(BTD2);
            ZL78.setForeground(Color.black);
            ZL78.setName("ZL78");
            ZL78.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL78);
            ZL78.setBounds(610, 233, 240, ZL78.getPreferredSize().height);

            //---- ZL28 ----
            ZL28.setComponentPopupMenu(BTD2);
            ZL28.setName("ZL28");
            ZL28.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL28);
            ZL28.setBounds(850, 233, 100, ZL28.getPreferredSize().height);

            //---- LD09R ----
            LD09R.setText("@LD09R@");
            LD09R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD09R.setComponentPopupMenu(BTD2);
            LD09R.setName("LD09R");
            LD09R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD09R);
            LD09R.setBounds(10, 260, 355, LD09R.getPreferredSize().height);

            //---- XL09 ----
            XL09.setComponentPopupMenu(BTD2);
            XL09.setForeground(Color.black);
            XL09.setName("XL09");
            XL09.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL09);
            XL09.setBounds(370, 258, 240, XL09.getPreferredSize().height);

            //---- ZL79 ----
            ZL79.setComponentPopupMenu(BTD2);
            ZL79.setForeground(Color.black);
            ZL79.setName("ZL79");
            ZL79.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL79);
            ZL79.setBounds(610, 258, 240, ZL79.getPreferredSize().height);

            //---- ZL29 ----
            ZL29.setComponentPopupMenu(BTD2);
            ZL29.setName("ZL29");
            ZL29.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL29);
            ZL29.setBounds(850, 258, 100, ZL29.getPreferredSize().height);

            //---- LD10R ----
            LD10R.setText("@LD10R@");
            LD10R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD10R.setComponentPopupMenu(BTD2);
            LD10R.setName("LD10R");
            LD10R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD10R);
            LD10R.setBounds(10, 285, 355, LD10R.getPreferredSize().height);

            //---- XL10 ----
            XL10.setComponentPopupMenu(BTD2);
            XL10.setForeground(Color.black);
            XL10.setName("XL10");
            XL10.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL10);
            XL10.setBounds(370, 283, 240, XL10.getPreferredSize().height);

            //---- ZL710 ----
            ZL710.setComponentPopupMenu(BTD2);
            ZL710.setForeground(Color.black);
            ZL710.setName("ZL710");
            ZL710.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL710);
            ZL710.setBounds(610, 283, 240, ZL710.getPreferredSize().height);

            //---- ZL210 ----
            ZL210.setComponentPopupMenu(BTD2);
            ZL210.setName("ZL210");
            ZL210.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL210);
            ZL210.setBounds(850, 283, 100, ZL210.getPreferredSize().height);

            //---- LD11R ----
            LD11R.setText("@LD11R@");
            LD11R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD11R.setComponentPopupMenu(BTD2);
            LD11R.setName("LD11R");
            LD11R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD11R);
            LD11R.setBounds(10, 310, 355, LD11R.getPreferredSize().height);

            //---- XL11 ----
            XL11.setComponentPopupMenu(BTD2);
            XL11.setForeground(Color.black);
            XL11.setName("XL11");
            XL11.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL11);
            XL11.setBounds(370, 308, 240, XL11.getPreferredSize().height);

            //---- ZL711 ----
            ZL711.setComponentPopupMenu(BTD2);
            ZL711.setForeground(Color.black);
            ZL711.setName("ZL711");
            ZL711.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL711);
            ZL711.setBounds(610, 308, 240, ZL711.getPreferredSize().height);

            //---- ZL211 ----
            ZL211.setComponentPopupMenu(BTD2);
            ZL211.setName("ZL211");
            ZL211.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL211);
            ZL211.setBounds(850, 308, 100, ZL211.getPreferredSize().height);

            //---- LD12R ----
            LD12R.setText("@LD12R@");
            LD12R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD12R.setComponentPopupMenu(BTD2);
            LD12R.setName("LD12R");
            LD12R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD12R);
            LD12R.setBounds(10, 335, 355, LD12R.getPreferredSize().height);

            //---- XL12 ----
            XL12.setComponentPopupMenu(BTD2);
            XL12.setForeground(Color.black);
            XL12.setName("XL12");
            XL12.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL12);
            XL12.setBounds(370, 333, 240, XL12.getPreferredSize().height);

            //---- ZL712 ----
            ZL712.setComponentPopupMenu(BTD2);
            ZL712.setForeground(Color.black);
            ZL712.setName("ZL712");
            ZL712.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL712);
            ZL712.setBounds(610, 333, 240, ZL712.getPreferredSize().height);

            //---- ZL212 ----
            ZL212.setComponentPopupMenu(BTD2);
            ZL212.setName("ZL212");
            ZL212.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL212);
            ZL212.setBounds(850, 333, 100, ZL212.getPreferredSize().height);

            //---- LD13R ----
            LD13R.setText("@LD13R@");
            LD13R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD13R.setComponentPopupMenu(BTD2);
            LD13R.setName("LD13R");
            LD13R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD13R);
            LD13R.setBounds(10, 360, 355, LD13R.getPreferredSize().height);

            //---- XL13 ----
            XL13.setComponentPopupMenu(BTD2);
            XL13.setForeground(Color.black);
            XL13.setName("XL13");
            XL13.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL13);
            XL13.setBounds(370, 358, 240, XL13.getPreferredSize().height);

            //---- ZL713 ----
            ZL713.setComponentPopupMenu(BTD2);
            ZL713.setForeground(Color.black);
            ZL713.setName("ZL713");
            ZL713.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL713);
            ZL713.setBounds(610, 358, 240, ZL713.getPreferredSize().height);

            //---- ZL213 ----
            ZL213.setComponentPopupMenu(BTD2);
            ZL213.setName("ZL213");
            ZL213.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL213);
            ZL213.setBounds(850, 358, 100, ZL213.getPreferredSize().height);

            //---- LD14R ----
            LD14R.setText("@LD14R@");
            LD14R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD14R.setComponentPopupMenu(BTD2);
            LD14R.setName("LD14R");
            LD14R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD14R);
            LD14R.setBounds(10, 385, 355, LD14R.getPreferredSize().height);

            //---- XL14 ----
            XL14.setComponentPopupMenu(BTD2);
            XL14.setForeground(Color.black);
            XL14.setName("XL14");
            XL14.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL14);
            XL14.setBounds(370, 383, 240, XL14.getPreferredSize().height);

            //---- ZL714 ----
            ZL714.setComponentPopupMenu(BTD2);
            ZL714.setForeground(Color.black);
            ZL714.setName("ZL714");
            ZL714.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL714);
            ZL714.setBounds(610, 383, 240, ZL714.getPreferredSize().height);

            //---- ZL214 ----
            ZL214.setComponentPopupMenu(BTD2);
            ZL214.setName("ZL214");
            ZL214.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL214);
            ZL214.setBounds(850, 383, 100, ZL214.getPreferredSize().height);

            //---- LD15R ----
            LD15R.setText("@LD15R@");
            LD15R.setFont(new Font("Courier New", Font.PLAIN, 12));
            LD15R.setComponentPopupMenu(BTD2);
            LD15R.setName("LD15R");
            LD15R.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(LD15R);
            LD15R.setBounds(10, 410, 355, LD15R.getPreferredSize().height);

            //---- XL15 ----
            XL15.setComponentPopupMenu(BTD2);
            XL15.setForeground(Color.black);
            XL15.setName("XL15");
            XL15.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(XL15);
            XL15.setBounds(370, 408, 240, XL15.getPreferredSize().height);

            //---- ZL715 ----
            ZL715.setComponentPopupMenu(BTD2);
            ZL715.setForeground(Color.black);
            ZL715.setName("ZL715");
            ZL715.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL715);
            ZL715.setBounds(610, 408, 240, ZL715.getPreferredSize().height);

            //---- ZL215 ----
            ZL215.setComponentPopupMenu(BTD2);
            ZL215.setName("ZL215");
            ZL215.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                LD01RMouseClicked(e);
              }
            });
            panel2.add(ZL215);
            ZL215.setBounds(850, 408, 100, ZL215.getPreferredSize().height);

            //---- label16 ----
            label16.setText("@UTITR@");
            label16.setFont(new Font("Courier New", Font.BOLD, 12));
            label16.setForeground(Color.black);
            label16.setName("label16");
            panel2.add(label16);
            label16.setBounds(10, 35, 355, 21);

            //---- label17 ----
            label17.setText("@TIT1@");
            label17.setHorizontalAlignment(SwingConstants.LEFT);
            label17.setFont(new Font("Courier New", Font.BOLD, 12));
            label17.setForeground(Color.black);
            label17.setName("label17");
            panel2.add(label17);
            label17.setBounds(370, 35, 240, 21);

            //---- label18 ----
            label18.setText("@TIT2@");
            label18.setHorizontalAlignment(SwingConstants.LEFT);
            label18.setFont(new Font("Courier New", Font.BOLD, 12));
            label18.setForeground(Color.black);
            label18.setName("label18");
            panel2.add(label18);
            label18.setBounds(610, 35, 240, 21);

            //---- label19 ----
            label19.setText("Prix conso.");
            label19.setHorizontalAlignment(SwingConstants.CENTER);
            label19.setFont(new Font("Courier New", Font.BOLD, 12));
            label19.setName("label19");
            panel2.add(label19);
            label19.setBounds(850, 35, 95, 21);

            //---- label1 ----
            label1.setText("@PL01@");
            label1.setForeground(new Color(153, 0, 0));
            label1.setName("label1");
            panel2.add(label1);
            label1.setBounds(985, 65, 15, label1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("@PL02@");
            label2.setForeground(new Color(153, 0, 0));
            label2.setName("label2");
            panel2.add(label2);
            label2.setBounds(985, 90, 15, label2.getPreferredSize().height);

            //---- label3 ----
            label3.setText("@PL03@");
            label3.setForeground(new Color(153, 0, 0));
            label3.setName("label3");
            panel2.add(label3);
            label3.setBounds(985, 115, 15, label3.getPreferredSize().height);

            //---- label4 ----
            label4.setText("@PL04@");
            label4.setForeground(new Color(153, 0, 0));
            label4.setName("label4");
            panel2.add(label4);
            label4.setBounds(985, 140, 15, label4.getPreferredSize().height);

            //---- label5 ----
            label5.setText("@PL05@");
            label5.setForeground(new Color(153, 0, 0));
            label5.setName("label5");
            panel2.add(label5);
            label5.setBounds(985, 165, 15, label5.getPreferredSize().height);

            //---- label6 ----
            label6.setText("@PL06@");
            label6.setForeground(new Color(153, 0, 0));
            label6.setName("label6");
            panel2.add(label6);
            label6.setBounds(985, 190, 15, label6.getPreferredSize().height);

            //---- label7 ----
            label7.setText("@PL07@");
            label7.setForeground(new Color(153, 0, 0));
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(985, 215, 15, label7.getPreferredSize().height);

            //---- label8 ----
            label8.setText("@PL08@");
            label8.setForeground(new Color(153, 0, 0));
            label8.setName("label8");
            panel2.add(label8);
            label8.setBounds(985, 240, 15, label8.getPreferredSize().height);

            //---- label9 ----
            label9.setText("@PL09@");
            label9.setForeground(new Color(153, 0, 0));
            label9.setName("label9");
            panel2.add(label9);
            label9.setBounds(985, 265, 15, label9.getPreferredSize().height);

            //---- label10 ----
            label10.setText("@PL10@");
            label10.setForeground(new Color(153, 0, 0));
            label10.setName("label10");
            panel2.add(label10);
            label10.setBounds(985, 290, 15, label10.getPreferredSize().height);

            //---- label11 ----
            label11.setText("@PL11@");
            label11.setForeground(new Color(153, 0, 0));
            label11.setName("label11");
            panel2.add(label11);
            label11.setBounds(985, 315, 15, label11.getPreferredSize().height);

            //---- label12 ----
            label12.setText("@PL12@");
            label12.setForeground(new Color(153, 0, 0));
            label12.setName("label12");
            panel2.add(label12);
            label12.setBounds(985, 340, 15, label12.getPreferredSize().height);

            //---- label13 ----
            label13.setText("@PL13@");
            label13.setForeground(new Color(153, 0, 0));
            label13.setName("label13");
            panel2.add(label13);
            label13.setBounds(985, 365, 15, label13.getPreferredSize().height);

            //---- label14 ----
            label14.setText("@PL14@");
            label14.setForeground(new Color(153, 0, 0));
            label14.setName("label14");
            panel2.add(label14);
            label14.setBounds(985, 390, 15, label14.getPreferredSize().height);

            //---- label15 ----
            label15.setText("@PL15@");
            label15.setForeground(new Color(153, 0, 0));
            label15.setName("label15");
            panel2.add(label15);
            label15.setBounds(985, 415, 15, label15.getPreferredSize().height);
          }
          p_contenu.add(panel2);
          panel2.setBounds(10, 10, 1010, 466);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("D\u00e9tail");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR2 ----
      CHOISIR2.setText("D\u00e9tail");
      CHOISIR2.setName("CHOISIR2");
      CHOISIR2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIR2ActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR2);
    }

    //======== riSousMenu6 ========
    {
      riSousMenu6.setName("riSousMenu6");

      //---- riSousMenu_bt6 ----
      riSousMenu_bt6.setText("Recherche client");
      riSousMenu_bt6.setToolTipText("Recherche client");
      riSousMenu_bt6.setName("riSousMenu_bt6");
      riSousMenu_bt6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt6ActionPerformed(e);
        }
      });
      riSousMenu6.add(riSousMenu_bt6);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie CLNOM;
  private JLabel OBJ_27;
  private XRiTextField WETB;
  private JLabel OBJ_28;
  private XRiTextField WCNV;
  private SNBoutonRecherche BT_ChgSoc;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable WTP01;
  private JPanel panel2;
  private JButton BT_PGUP2;
  private JButton BT_PGDOWN2;
  private RiZoneSortie LD01R;
  private XRiTextField XL01;
  private XRiTextField ZL71;
  private XRiTextField ZL21;
  private RiZoneSortie LD02R;
  private XRiTextField XL02;
  private XRiTextField ZL72;
  private XRiTextField ZL22;
  private RiZoneSortie LD03R;
  private XRiTextField XL03;
  private XRiTextField ZL73;
  private XRiTextField ZL23;
  private RiZoneSortie LD04R;
  private XRiTextField XL04;
  private XRiTextField ZL74;
  private XRiTextField ZL24;
  private RiZoneSortie LD05R;
  private XRiTextField XL05;
  private XRiTextField ZL75;
  private XRiTextField ZL25;
  private RiZoneSortie LD06R;
  private XRiTextField XL06;
  private XRiTextField ZL76;
  private XRiTextField ZL26;
  private RiZoneSortie LD07R;
  private XRiTextField XL07;
  private XRiTextField ZL77;
  private XRiTextField ZL27;
  private RiZoneSortie LD08R;
  private XRiTextField XL08;
  private XRiTextField ZL78;
  private XRiTextField ZL28;
  private RiZoneSortie LD09R;
  private XRiTextField XL09;
  private XRiTextField ZL79;
  private XRiTextField ZL29;
  private RiZoneSortie LD10R;
  private XRiTextField XL10;
  private XRiTextField ZL710;
  private XRiTextField ZL210;
  private RiZoneSortie LD11R;
  private XRiTextField XL11;
  private XRiTextField ZL711;
  private XRiTextField ZL211;
  private RiZoneSortie LD12R;
  private XRiTextField XL12;
  private XRiTextField ZL712;
  private XRiTextField ZL212;
  private RiZoneSortie LD13R;
  private XRiTextField XL13;
  private XRiTextField ZL713;
  private XRiTextField ZL213;
  private RiZoneSortie LD14R;
  private XRiTextField XL14;
  private XRiTextField ZL714;
  private XRiTextField ZL214;
  private RiZoneSortie LD15R;
  private XRiTextField XL15;
  private XRiTextField ZL715;
  private XRiTextField ZL215;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JLabel label19;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
