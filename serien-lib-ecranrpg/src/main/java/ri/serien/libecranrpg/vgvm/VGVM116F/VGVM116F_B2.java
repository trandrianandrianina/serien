
package ri.serien.libecranrpg.vgvm.VGVM116F;
// Nom Fichier: pop_VGVM116F_FMTB2_979.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM116F_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM116F_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    L17UN.setEnabled(lexique.isPresent("L17UN"));
    L17UN1.setEnabled(lexique.isPresent("L17UN1"));
    L17QT.setEnabled(lexique.isPresent("L17QT"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_35.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    panel1 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_35 = new JButton();
    panel3 = new JPanel();
    OBJ_17 = new JPanel();
    OBJ_20 = new JLabel();
    A1PDS = new XRiTextField();
    A1VOL = new XRiTextField();
    OBJ_18 = new JLabel();
    OBJ_22 = new JLabel();
    A1LNG = new XRiTextField();
    A1LRG = new XRiTextField();
    A1HTR = new XRiTextField();
    OBJ_24 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_16 = new JPanel();
    OBJ_28 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_32 = new JLabel();
    L17QT = new XRiTextField();
    L17UN1 = new XRiTextField();
    L17UN = new XRiTextField();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setPreferredSize(new Dimension(395, 250));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel2 ========
    {
      panel2.setName("panel2");

      //======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("OK");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_34ActionPerformed(e);
          }
        });
        panel1.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_35 ----
        OBJ_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_35.setToolTipText("Retour");
        OBJ_35.setName("OBJ_35");
        OBJ_35.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_35ActionPerformed(e);
          }
        });
        panel1.add(OBJ_35);
        OBJ_35.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }

      GroupLayout panel2Layout = new GroupLayout(panel2);
      panel2.setLayout(panel2Layout);
      panel2Layout.setHorizontalGroup(
        panel2Layout.createParallelGroup()
          .addGroup(panel2Layout.createSequentialGroup()
            .addGap(275, 275, 275)
            .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
      );
      panel2Layout.setVerticalGroup(
        panel2Layout.createParallelGroup()
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
      );
    }
    add(panel2, BorderLayout.SOUTH);

    //======== panel3 ========
    {
      panel3.setName("panel3");
      panel3.setLayout(null);

      //======== OBJ_17 ========
      {
        OBJ_17.setBorder(new TitledBorder("Dimensions unitaires"));
        OBJ_17.setName("OBJ_17");
        OBJ_17.setLayout(null);

        //---- OBJ_20 ----
        OBJ_20.setText("Volume");
        OBJ_20.setName("OBJ_20");
        OBJ_17.add(OBJ_20);
        OBJ_20.setBounds(165, 30, 67, 20);

        //---- A1PDS ----
        A1PDS.setName("A1PDS");
        OBJ_17.add(A1PDS);
        A1PDS.setBounds(80, 26, 70, A1PDS.getPreferredSize().height);

        //---- A1VOL ----
        A1VOL.setName("A1VOL");
        OBJ_17.add(A1VOL);
        A1VOL.setBounds(255, 26, 70, A1VOL.getPreferredSize().height);

        //---- OBJ_18 ----
        OBJ_18.setText("Poids");
        OBJ_18.setName("OBJ_18");
        OBJ_17.add(OBJ_18);
        OBJ_18.setBounds(15, 30, 57, 20);

        //---- OBJ_22 ----
        OBJ_22.setText("LxlxH");
        OBJ_22.setName("OBJ_22");
        OBJ_17.add(OBJ_22);
        OBJ_22.setBounds(15, 57, 54, 20);

        //---- A1LNG ----
        A1LNG.setName("A1LNG");
        OBJ_17.add(A1LNG);
        A1LNG.setBounds(80, 53, 49, A1LNG.getPreferredSize().height);

        //---- A1LRG ----
        A1LRG.setName("A1LRG");
        OBJ_17.add(A1LRG);
        A1LRG.setBounds(165, 53, 49, A1LRG.getPreferredSize().height);

        //---- A1HTR ----
        A1HTR.setName("A1HTR");
        OBJ_17.add(A1HTR);
        A1HTR.setBounds(255, 53, 49, A1HTR.getPreferredSize().height);

        //---- OBJ_24 ----
        OBJ_24.setText("x");
        OBJ_24.setName("OBJ_24");
        OBJ_17.add(OBJ_24);
        OBJ_24.setBounds(140, 57, 15, 20);

        //---- OBJ_26 ----
        OBJ_26.setText("x");
        OBJ_26.setName("OBJ_26");
        OBJ_17.add(OBJ_26);
        OBJ_26.setBounds(235, 57, 15, 20);
      }
      panel3.add(OBJ_17);
      OBJ_17.setBounds(5, 5, 384, 95);

      //======== OBJ_16 ========
      {
        OBJ_16.setBorder(new TitledBorder("Colisage"));
        OBJ_16.setName("OBJ_16");
        OBJ_16.setLayout(null);

        //---- OBJ_28 ----
        OBJ_28.setText("Unit\u00e9 de transport");
        OBJ_28.setName("OBJ_28");
        OBJ_16.add(OBJ_28);
        OBJ_28.setBounds(15, 30, 127, 20);

        //---- OBJ_30 ----
        OBJ_30.setText("Non Rgr");
        OBJ_30.setName("OBJ_30");
        OBJ_16.add(OBJ_30);
        OBJ_30.setBounds(255, 30, 72, 20);

        //---- OBJ_32 ----
        OBJ_32.setText("Quantit\u00e9");
        OBJ_32.setName("OBJ_32");
        OBJ_16.add(OBJ_32);
        OBJ_32.setBounds(15, 59, 70, 20);

        //---- L17QT ----
        L17QT.setComponentPopupMenu(BTD);
        L17QT.setName("L17QT");
        OBJ_16.add(L17QT);
        L17QT.setBounds(165, 55, 58, L17QT.getPreferredSize().height);

        //---- L17UN1 ----
        L17UN1.setComponentPopupMenu(BTD);
        L17UN1.setName("L17UN1");
        OBJ_16.add(L17UN1);
        L17UN1.setBounds(339, 26, 20, L17UN1.getPreferredSize().height);

        //---- L17UN ----
        L17UN.setComponentPopupMenu(BTD);
        L17UN.setName("L17UN");
        OBJ_16.add(L17UN);
        L17UN.setBounds(165, 26, 30, L17UN.getPreferredSize().height);
      }
      panel3.add(OBJ_16);
      OBJ_16.setBounds(5, 100, 384, 100);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel3.getComponentCount(); i++) {
          Rectangle bounds = panel3.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel3.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel3.setMinimumSize(preferredSize);
        panel3.setPreferredSize(preferredSize);
      }
    }
    add(panel3, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Exploitation");
      OBJ_9.setName("OBJ_9");
      OBJ_4.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel2;
  private JPanel panel1;
  private JButton BT_ENTER;
  private JButton OBJ_35;
  private JPanel panel3;
  private JPanel OBJ_17;
  private JLabel OBJ_20;
  private XRiTextField A1PDS;
  private XRiTextField A1VOL;
  private JLabel OBJ_18;
  private JLabel OBJ_22;
  private XRiTextField A1LNG;
  private XRiTextField A1LRG;
  private XRiTextField A1HTR;
  private JLabel OBJ_24;
  private JLabel OBJ_26;
  private JPanel OBJ_16;
  private JLabel OBJ_28;
  private JLabel OBJ_30;
  private JLabel OBJ_32;
  private XRiTextField L17QT;
  private XRiTextField L17UN1;
  private XRiTextField L17UN;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
