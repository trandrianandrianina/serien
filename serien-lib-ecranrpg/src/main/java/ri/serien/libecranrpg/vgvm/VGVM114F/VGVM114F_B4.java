
package ri.serien.libecranrpg.vgvm.VGVM114F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM114F_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM114F_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    L1LIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB1@")).trim());
    L1LIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB2@")).trim());
    L1LIB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB3@")).trim());
    L1LIB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB4@")).trim());
    L1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ART@")).trim());
    L1PVC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1LIB4@")).trim());
    WDPRC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDPRC@")).trim());
    WDPRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDPRP@")).trim());
    A1PDA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1PDA@")).trim());
    L1PRV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1PRV@")).trim());
    L1QTEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1QTEX@")).trim());
    L1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNV@")).trim());
    WDDPRC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDDPRC@")).trim());
    WDDPRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDDPRP@")).trim());
    A1DDAX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1DDAX@")).trim());
    WMMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMR@")).trim());
    WMMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMT@")).trim());
    WMAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAC@")).trim());
    WTPRL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPRL@")).trim());
    L1MHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1MHT@")).trim());
    WTPVL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPVL@")).trim());
    WPMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMR@")).trim());
    WPMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMT@")).trim());
    WPAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affichage étendu"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm114"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    L1LIB1 = new RiZoneSortie();
    L1LIB2 = new RiZoneSortie();
    L1LIB3 = new RiZoneSortie();
    L1LIB4 = new RiZoneSortie();
    L1ART = new RiZoneSortie();
    OBJ_62 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_68 = new JLabel();
    L1PVN = new XRiTextField();
    L1PVB = new XRiTextField();
    L1PVC = new RiZoneSortie();
    WDPRC = new RiZoneSortie();
    WDPRP = new RiZoneSortie();
    A1PDA = new RiZoneSortie();
    L1PRV = new RiZoneSortie();
    L1QTEX = new RiZoneSortie();
    OBJ_70 = new JLabel();
    WMARGL = new XRiTextField();
    OBJ_75 = new JLabel();
    OBJ_77 = new JLabel();
    L1REM1 = new XRiTextField();
    L1REM2 = new XRiTextField();
    L1REM3 = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_79 = new JLabel();
    OBJ_63 = new JLabel();
    L1UNV = new RiZoneSortie();
    WSER = new XRiTextField();
    L1IN3 = new XRiTextField();
    L1IN2 = new XRiTextField();
    OBJ_80 = new JLabel();
    WDDPRC = new RiZoneSortie();
    WDDPRP = new RiZoneSortie();
    A1DDAX = new RiZoneSortie();
    xTitledSeparator1 = new JXTitledSeparator();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    panel2 = new JPanel();
    OBJ_59 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_58 = new JLabel();
    WMMR = new RiZoneSortie();
    WMMT = new RiZoneSortie();
    WMAC = new RiZoneSortie();
    WTPRL = new RiZoneSortie();
    L1MHT = new RiZoneSortie();
    WTPVL = new RiZoneSortie();
    OBJ_18 = new JLabel();
    WPMR = new RiZoneSortie();
    WPMT = new RiZoneSortie();
    WPAC = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_57 = new JLabel();
    OBJ_19 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(905, 490));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Ligne de vente"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setText("@L1LIB1@");
          L1LIB1.setName("L1LIB1");
          panel3.add(L1LIB1);
          L1LIB1.setBounds(25, 62, 310, L1LIB1.getPreferredSize().height);

          //---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setText("@L1LIB2@");
          L1LIB2.setName("L1LIB2");
          panel3.add(L1LIB2);
          L1LIB2.setBounds(25, 89, 310, L1LIB2.getPreferredSize().height);

          //---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setText("@L1LIB3@");
          L1LIB3.setName("L1LIB3");
          panel3.add(L1LIB3);
          L1LIB3.setBounds(25, 116, 310, L1LIB3.getPreferredSize().height);

          //---- L1LIB4 ----
          L1LIB4.setComponentPopupMenu(BTD);
          L1LIB4.setText("@L1LIB4@");
          L1LIB4.setName("L1LIB4");
          panel3.add(L1LIB4);
          L1LIB4.setBounds(25, 143, 310, L1LIB4.getPreferredSize().height);

          //---- L1ART ----
          L1ART.setComponentPopupMenu(BTD);
          L1ART.setText("@L1ART@");
          L1ART.setName("L1ART");
          panel3.add(L1ART);
          L1ART.setBounds(75, 35, 235, L1ART.getPreferredSize().height);

          //---- OBJ_62 ----
          OBJ_62.setText("Prix de vente calcul\u00e9");
          OBJ_62.setName("OBJ_62");
          panel3.add(OBJ_62);
          OBJ_62.setBounds(380, 37, 125, 20);

          //---- OBJ_65 ----
          OBJ_65.setText("Dernier prix pratiqu\u00e9");
          OBJ_65.setName("OBJ_65");
          panel3.add(OBJ_65);
          OBJ_65.setBounds(380, 91, 125, 20);

          //---- OBJ_66 ----
          OBJ_66.setText("Dernier prix d'achat");
          OBJ_66.setName("OBJ_66");
          panel3.add(OBJ_66);
          OBJ_66.setBounds(380, 118, 125, 20);

          //---- OBJ_64 ----
          OBJ_64.setText("Dernier prix client");
          OBJ_64.setName("OBJ_64");
          panel3.add(OBJ_64);
          OBJ_64.setBounds(380, 64, 125, 20);

          //---- OBJ_69 ----
          OBJ_69.setText("Type de gratuit");
          OBJ_69.setName("OBJ_69");
          panel3.add(OBJ_69);
          OBJ_69.setBounds(220, 174, 94, 20);

          //---- OBJ_67 ----
          OBJ_67.setText("Prix de revient");
          OBJ_67.setName("OBJ_67");
          panel3.add(OBJ_67);
          OBJ_67.setBounds(380, 145, 125, 20);

          //---- OBJ_68 ----
          OBJ_68.setText("Regroupement");
          OBJ_68.setName("OBJ_68");
          panel3.add(OBJ_68);
          OBJ_68.setBounds(25, 174, 91, 20);

          //---- L1PVN ----
          L1PVN.setComponentPopupMenu(BTD);
          L1PVN.setName("L1PVN");
          panel3.add(L1PVN);
          L1PVN.setBounds(285, 225, 90, L1PVN.getPreferredSize().height);

          //---- L1PVB ----
          L1PVB.setComponentPopupMenu(BTD);
          L1PVB.setName("L1PVB");
          panel3.add(L1PVB);
          L1PVB.setBounds(375, 225, 90, L1PVB.getPreferredSize().height);

          //---- L1PVC ----
          L1PVC.setComponentPopupMenu(BTD);
          L1PVC.setText("@L1LIB4@");
          L1PVC.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVC.setName("L1PVC");
          panel3.add(L1PVC);
          L1PVC.setBounds(510, 35, 90, L1PVC.getPreferredSize().height);

          //---- WDPRC ----
          WDPRC.setComponentPopupMenu(BTD);
          WDPRC.setText("@WDPRC@");
          WDPRC.setHorizontalAlignment(SwingConstants.RIGHT);
          WDPRC.setName("WDPRC");
          panel3.add(WDPRC);
          WDPRC.setBounds(510, 62, 90, WDPRC.getPreferredSize().height);

          //---- WDPRP ----
          WDPRP.setComponentPopupMenu(BTD);
          WDPRP.setText("@WDPRP@");
          WDPRP.setHorizontalAlignment(SwingConstants.RIGHT);
          WDPRP.setName("WDPRP");
          panel3.add(WDPRP);
          WDPRP.setBounds(510, 89, 90, WDPRP.getPreferredSize().height);

          //---- A1PDA ----
          A1PDA.setComponentPopupMenu(BTD);
          A1PDA.setText("@A1PDA@");
          A1PDA.setHorizontalAlignment(SwingConstants.RIGHT);
          A1PDA.setName("A1PDA");
          panel3.add(A1PDA);
          A1PDA.setBounds(510, 116, 90, A1PDA.getPreferredSize().height);

          //---- L1PRV ----
          L1PRV.setComponentPopupMenu(BTD);
          L1PRV.setText("@L1PRV@");
          L1PRV.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PRV.setName("L1PRV");
          panel3.add(L1PRV);
          L1PRV.setBounds(510, 143, 90, L1PRV.getPreferredSize().height);

          //---- L1QTEX ----
          L1QTEX.setComponentPopupMenu(BTD);
          L1QTEX.setText("@L1QTEX@");
          L1QTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTEX.setName("L1QTEX");
          panel3.add(L1QTEX);
          L1QTEX.setBounds(80, 225, 74, L1QTEX.getPreferredSize().height);

          //---- OBJ_70 ----
          OBJ_70.setText("Quantit\u00e9");
          OBJ_70.setName("OBJ_70");
          panel3.add(OBJ_70);
          OBJ_70.setBounds(25, 227, 52, 20);

          //---- WMARGL ----
          WMARGL.setComponentPopupMenu(BTD);
          WMARGL.setName("WMARGL");
          panel3.add(WMARGL);
          WMARGL.setBounds(615, 225, 58, WMARGL.getPreferredSize().height);

          //---- OBJ_75 ----
          OBJ_75.setText("Article");
          OBJ_75.setName("OBJ_75");
          panel3.add(OBJ_75);
          OBJ_75.setBounds(25, 37, 40, 20);

          //---- OBJ_77 ----
          OBJ_77.setText("Prix tarif");
          OBJ_77.setName("OBJ_77");
          panel3.add(OBJ_77);
          OBJ_77.setBounds(380, 205, 49, 16);

          //---- L1REM1 ----
          L1REM1.setComponentPopupMenu(BTD);
          L1REM1.setName("L1REM1");
          panel3.add(L1REM1);
          L1REM1.setBounds(465, 225, 50, L1REM1.getPreferredSize().height);

          //---- L1REM2 ----
          L1REM2.setComponentPopupMenu(BTD);
          L1REM2.setName("L1REM2");
          panel3.add(L1REM2);
          L1REM2.setBounds(515, 225, 50, L1REM2.getPreferredSize().height);

          //---- L1REM3 ----
          L1REM3.setComponentPopupMenu(BTD);
          L1REM3.setName("L1REM3");
          panel3.add(L1REM3);
          L1REM3.setBounds(565, 225, 50, L1REM3.getPreferredSize().height);

          //---- OBJ_76 ----
          OBJ_76.setText("Prix net");
          OBJ_76.setName("OBJ_76");
          panel3.add(OBJ_76);
          OBJ_76.setBounds(290, 205, 46, 16);

          //---- OBJ_79 ----
          OBJ_79.setText("Marge");
          OBJ_79.setName("OBJ_79");
          panel3.add(OBJ_79);
          OBJ_79.setBounds(627, 205, 43, 16);

          //---- OBJ_63 ----
          OBJ_63.setText("Date");
          OBJ_63.setName("OBJ_63");
          panel3.add(OBJ_63);
          OBJ_63.setBounds(607, 37, 48, 20);

          //---- L1UNV ----
          L1UNV.setComponentPopupMenu(BTD);
          L1UNV.setText("@L1UNV@");
          L1UNV.setName("L1UNV");
          panel3.add(L1UNV);
          L1UNV.setBounds(160, 225, 30, L1UNV.getPreferredSize().height);

          //---- WSER ----
          WSER.setComponentPopupMenu(BTD);
          WSER.setName("WSER");
          panel3.add(WSER);
          WSER.setBounds(316, 33, 20, WSER.getPreferredSize().height);

          //---- L1IN3 ----
          L1IN3.setComponentPopupMenu(BTD);
          L1IN3.setName("L1IN3");
          panel3.add(L1IN3);
          L1IN3.setBounds(134, 170, 20, L1IN3.getPreferredSize().height);

          //---- L1IN2 ----
          L1IN2.setComponentPopupMenu(BTD);
          L1IN2.setName("L1IN2");
          panel3.add(L1IN2);
          L1IN2.setBounds(316, 170, 20, L1IN2.getPreferredSize().height);

          //---- OBJ_80 ----
          OBJ_80.setText("ou");
          OBJ_80.setName("OBJ_80");
          panel3.add(OBJ_80);
          OBJ_80.setBounds(607, 205, 18, 16);

          //---- WDDPRC ----
          WDDPRC.setText("@WDDPRC@");
          WDDPRC.setName("WDDPRC");
          panel3.add(WDDPRC);
          WDDPRC.setBounds(605, 62, 65, WDDPRC.getPreferredSize().height);

          //---- WDDPRP ----
          WDDPRP.setText("@WDDPRP@");
          WDDPRP.setName("WDDPRP");
          panel3.add(WDDPRP);
          WDDPRP.setBounds(605, 89, 65, WDDPRP.getPreferredSize().height);

          //---- A1DDAX ----
          A1DDAX.setText("@A1DDAX@");
          A1DDAX.setName("A1DDAX");
          panel3.add(A1DDAX);
          A1DDAX.setBounds(605, 116, 65, A1DDAX.getPreferredSize().height);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 12));
          xTitledSeparator1.setTitle("Remises");
          xTitledSeparator1.setName("xTitledSeparator1");
          panel3.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(467, 205, 133, xTitledSeparator1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(15, 20, 705, 275);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== panel2 ========
    {
      panel2.setBorder(new TitledBorder("Marges et action commerciale"));
      panel2.setOpaque(false);
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- OBJ_59 ----
      OBJ_59.setText("Action commerciale");
      OBJ_59.setName("OBJ_59");
      panel2.add(OBJ_59);
      OBJ_59.setBounds(25, 113, 120, 20);

      //---- OBJ_56 ----
      OBJ_56.setText("Marge th\u00e9orique");
      OBJ_56.setName("OBJ_56");
      panel2.add(OBJ_56);
      OBJ_56.setBounds(25, 85, 102, 20);

      //---- OBJ_55 ----
      OBJ_55.setText("Prix de revient");
      OBJ_55.setName("OBJ_55");
      panel2.add(OBJ_55);
      OBJ_55.setBounds(430, 57, 105, 20);

      //---- OBJ_61 ----
      OBJ_61.setText("Chiffre d'affaire th\u00e9orique");
      OBJ_61.setName("OBJ_61");
      panel2.add(OBJ_61);
      OBJ_61.setBounds(430, 113, 140, 20);

      //---- OBJ_60 ----
      OBJ_60.setText("CA th\u00e9orique");
      OBJ_60.setName("OBJ_60");
      panel2.add(OBJ_60);
      OBJ_60.setBounds(325, 113, 81, 20);

      //---- OBJ_20 ----
      OBJ_20.setText("Marge r\u00e9elle");
      OBJ_20.setName("OBJ_20");
      panel2.add(OBJ_20);
      OBJ_20.setBounds(25, 57, 79, 20);

      //---- OBJ_58 ----
      OBJ_58.setText("Chiffre d'affaire r\u00e9el");
      OBJ_58.setName("OBJ_58");
      panel2.add(OBJ_58);
      OBJ_58.setBounds(430, 85, 120, 20);

      //---- WMMR ----
      WMMR.setComponentPopupMenu(BTD);
      WMMR.setText("@WMMR@");
      WMMR.setHorizontalAlignment(SwingConstants.RIGHT);
      WMMR.setName("WMMR");
      panel2.add(WMMR);
      WMMR.setBounds(150, 55, 98, WMMR.getPreferredSize().height);

      //---- WMMT ----
      WMMT.setComponentPopupMenu(BTD);
      WMMT.setText("@WMMT@");
      WMMT.setHorizontalAlignment(SwingConstants.RIGHT);
      WMMT.setName("WMMT");
      panel2.add(WMMT);
      WMMT.setBounds(150, 83, 98, WMMT.getPreferredSize().height);

      //---- WMAC ----
      WMAC.setComponentPopupMenu(BTD);
      WMAC.setText("@WMAC@");
      WMAC.setHorizontalAlignment(SwingConstants.RIGHT);
      WMAC.setName("WMAC");
      panel2.add(WMAC);
      WMAC.setBounds(150, 111, 98, WMAC.getPreferredSize().height);

      //---- WTPRL ----
      WTPRL.setComponentPopupMenu(BTD);
      WTPRL.setText("@WTPRL@");
      WTPRL.setHorizontalAlignment(SwingConstants.RIGHT);
      WTPRL.setName("WTPRL");
      panel2.add(WTPRL);
      WTPRL.setBounds(575, 55, 98, WTPRL.getPreferredSize().height);

      //---- L1MHT ----
      L1MHT.setComponentPopupMenu(BTD);
      L1MHT.setText("@L1MHT@");
      L1MHT.setHorizontalAlignment(SwingConstants.RIGHT);
      L1MHT.setName("L1MHT");
      panel2.add(L1MHT);
      L1MHT.setBounds(575, 83, 98, L1MHT.getPreferredSize().height);

      //---- WTPVL ----
      WTPVL.setComponentPopupMenu(BTD);
      WTPVL.setText("@WTPVL@");
      WTPVL.setHorizontalAlignment(SwingConstants.RIGHT);
      WTPVL.setName("WTPVL");
      panel2.add(WTPVL);
      WTPVL.setBounds(575, 111, 98, WTPVL.getPreferredSize().height);

      //---- OBJ_18 ----
      OBJ_18.setText("Pourcents");
      OBJ_18.setName("OBJ_18");
      panel2.add(OBJ_18);
      OBJ_18.setBounds(255, 30, 63, 20);

      //---- WPMR ----
      WPMR.setComponentPopupMenu(BTD);
      WPMR.setText("@WPMR@");
      WPMR.setHorizontalAlignment(SwingConstants.RIGHT);
      WPMR.setName("WPMR");
      panel2.add(WPMR);
      WPMR.setBounds(250, 55, 66, WPMR.getPreferredSize().height);

      //---- WPMT ----
      WPMT.setComponentPopupMenu(BTD);
      WPMT.setText("@WPMT@");
      WPMT.setHorizontalAlignment(SwingConstants.RIGHT);
      WPMT.setName("WPMT");
      panel2.add(WPMT);
      WPMT.setBounds(250, 83, 66, WPMT.getPreferredSize().height);

      //---- WPAC ----
      WPAC.setComponentPopupMenu(BTD);
      WPAC.setText("@WPAC@");
      WPAC.setHorizontalAlignment(SwingConstants.RIGHT);
      WPAC.setName("WPAC");
      panel2.add(WPAC);
      WPAC.setBounds(250, 111, 66, WPAC.getPreferredSize().height);

      //---- OBJ_54 ----
      OBJ_54.setText("CA r\u00e9el");
      OBJ_54.setName("OBJ_54");
      panel2.add(OBJ_54);
      OBJ_54.setBounds(325, 57, 48, 20);

      //---- OBJ_57 ----
      OBJ_57.setText("CA r\u00e9el");
      OBJ_57.setName("OBJ_57");
      panel2.add(OBJ_57);
      OBJ_57.setBounds(325, 85, 48, 20);

      //---- OBJ_19 ----
      OBJ_19.setText("du");
      OBJ_19.setName("OBJ_19");
      panel2.add(OBJ_19);
      OBJ_19.setBounds(325, 30, 18, 20);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel2.getComponentCount(); i++) {
          Rectangle bounds = panel2.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel2.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel2.setMinimumSize(preferredSize);
        panel2.setPreferredSize(preferredSize);
      }
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private RiZoneSortie L1LIB1;
  private RiZoneSortie L1LIB2;
  private RiZoneSortie L1LIB3;
  private RiZoneSortie L1LIB4;
  private RiZoneSortie L1ART;
  private JLabel OBJ_62;
  private JLabel OBJ_65;
  private JLabel OBJ_66;
  private JLabel OBJ_64;
  private JLabel OBJ_69;
  private JLabel OBJ_67;
  private JLabel OBJ_68;
  private XRiTextField L1PVN;
  private XRiTextField L1PVB;
  private RiZoneSortie L1PVC;
  private RiZoneSortie WDPRC;
  private RiZoneSortie WDPRP;
  private RiZoneSortie A1PDA;
  private RiZoneSortie L1PRV;
  private RiZoneSortie L1QTEX;
  private JLabel OBJ_70;
  private XRiTextField WMARGL;
  private JLabel OBJ_75;
  private JLabel OBJ_77;
  private XRiTextField L1REM1;
  private XRiTextField L1REM2;
  private XRiTextField L1REM3;
  private JLabel OBJ_76;
  private JLabel OBJ_79;
  private JLabel OBJ_63;
  private RiZoneSortie L1UNV;
  private XRiTextField WSER;
  private XRiTextField L1IN3;
  private XRiTextField L1IN2;
  private JLabel OBJ_80;
  private RiZoneSortie WDDPRC;
  private RiZoneSortie WDDPRP;
  private RiZoneSortie A1DDAX;
  private JXTitledSeparator xTitledSeparator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JPanel panel2;
  private JLabel OBJ_59;
  private JLabel OBJ_56;
  private JLabel OBJ_55;
  private JLabel OBJ_61;
  private JLabel OBJ_60;
  private JLabel OBJ_20;
  private JLabel OBJ_58;
  private RiZoneSortie WMMR;
  private RiZoneSortie WMMT;
  private RiZoneSortie WMAC;
  private RiZoneSortie WTPRL;
  private RiZoneSortie L1MHT;
  private RiZoneSortie WTPVL;
  private JLabel OBJ_18;
  private RiZoneSortie WPMR;
  private RiZoneSortie WPMT;
  private RiZoneSortie WPAC;
  private JLabel OBJ_54;
  private JLabel OBJ_57;
  private JLabel OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
