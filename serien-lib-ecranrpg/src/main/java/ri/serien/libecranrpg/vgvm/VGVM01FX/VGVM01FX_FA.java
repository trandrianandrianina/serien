
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.sntypegratuit.SNTypeGratuit;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_FA extends SNPanelEcranRPG implements ioFrame {
  
  private boolean isConsultation = false;
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  
  private String[] FARON_Value = { "", "1", "2", "3", "4", "5", "6", };
  private String[] FATDUO_Value = { "", "J", "S", "M", };
  private String[] FANSA_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] FAGSP_Value = { "", "1", "2", "3", "4", };
  private String[] FAEQI_Value = { "", "1", "2", };
  private String[] FACQQ_Value = { "", "V", "C", "S", "X", "Y", };
  private String[] FAEREM_Value = { "", "1", };
  private String[] FAEXF_Value = { "", "1", "2", "3", "9" };
  
  public VGVM01FX_FA(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    FAEQI.setValeurs(FAEQI_Value, null);
    FACQQ.setValeurs(FACQQ_Value, null);
    FARON.setValeurs(FARON_Value, null);
    FAGSP.setValeurs(FAGSP_Value, null);
    FANSA.setValeurs(FANSA_Value, null);
    FATDUO.setValeurs(FATDUO_Value, null);
    FAEREM.setValeurs(FAEREM_Value, null);
    FAEXF.setValeurs(FAEXF_Value, null);
    FAEFR.setValeursSelection("1", " ");
    FAETQ.setValeursSelection("1", " ");
    FALOTF.setValeursSelection("1", " ");
    FALOTE.setValeursSelection("1", " ");
    FALOTH.setValeursSelection("1", " ");
    FALOTP.setValeursSelection("1", " ");
    FADEB.setValeursSelection("1", " ");
    FAWEB.setValeursSelection("1", " ");
    FAPHO.setValeursSelection("1", " ");
    FAADS.setValeursSelection("1", " ");
    FASPE.setValeursSelection("1", " ");
    FADPOS.setValeursSelection("1", " ");
    FAVOD.setValeursSelection("1", " ");
    FAPDG.setValeursSelection("1", " ");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LCG01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG01@")).trim());
    LCG02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG02@")).trim());
    LCG03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG03@")).trim());
    LCG04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG04@")).trim());
    LCG05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG05@")).trim());
    LCG06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG06@")).trim());
    LCG07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG07@")).trim());
    LCG08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG08@")).trim());
    LCG09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LCG09@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LISTFA, LISTFA.get_LIST_Title_Data_Brut(), _LISTFA_Top, _LISTFA_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "FAMAG");
    snMagasin.setEnabled(!isConsultation);
    
    snTypeGratuit.setSession(getSession());
    snTypeGratuit.setIdEtablissement(snEtablissement.getIdSelection());
    snTypeGratuit.charger(false);
    snTypeGratuit.setSelectionParChampRPG(lexique, "FATYGR");
    snTypeGratuit.setEnabled(!isConsultation);
    
    // Visibilité des boutons
    rafraichirBoutons();
    
    

    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snMagasin.renseignerChampRPG(lexique, "FAMAG");
    snTypeGratuit.renseignerChampRPG(lexique, "FATYGR");
    
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbCodeCategorie = new SNLabelChamp();
    INDIND = new XRiTextField();
    lbLibelle = new SNLabelChamp();
    FALIB = new XRiTextField();
    tpnFamille = new JTabbedPane();
    pnlDescription = new SNPanel();
    pnlGauche = new SNPanelContenu();
    pnlInfosRepercutees = new SNPanelTitre();
    lbTypeFicheArticle = new SNLabelChamp();
    FAIMG = new XRiTextField();
    lbCodeUnite = new SNLabelChamp();
    FAUNV = new XRiTextField();
    lbCodeUniteStock = new SNLabelChamp();
    FAUNS = new XRiTextField();
    lbReferenceTarif = new SNLabelChamp();
    FARTAR = new XRiTextField();
    lbCoefVenteT1 = new SNLabelChamp();
    FACMA = new XRiTextField();
    lbCoeffPRV = new SNLabelChamp();
    FACPR = new XRiTextField();
    lbCodeTVA = new SNLabelChamp();
    FATVA = new XRiTextField();
    lbExclusionFrais = new SNLabelChamp();
    FAEXF = new XRiComboBox();
    lbCodeCIP = new SNLabelChamp();
    FACIP = new XRiTextField();
    lbZoneTarif = new SNLabelChamp();
    FACTA = new XRiTextField();
    lbRattachementPIC = new SNLabelChamp();
    FAPIC = new XRiTextField();
    lbTraitementSpecifique = new SNLabelChamp();
    FAGSP = new XRiComboBox();
    pnlDroite = new SNPanelContenu();
    pnlCaracteristiquesImposees = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbPourcentageMiniMarge = new SNLabelChamp();
    FAMAR = new XRiTextField();
    lbArrondi = new SNLabelChamp();
    FARON = new XRiComboBox();
    FAVOD = new XRiCheckBox();
    FAPDG = new XRiCheckBox();
    FAPHO = new XRiCheckBox();
    FADPOS = new XRiCheckBox();
    FAWEB = new XRiCheckBox();
    FASPE = new XRiCheckBox();
    lbRegroupement = new SNLabelChamp();
    FAFAR = new XRiTextField();
    lbCalculPRV = new SNLabelChamp();
    FAPR = new XRiTextField();
    lbTaxeParafiscale = new SNLabelChamp();
    FATPF = new XRiTextField();
    lbTypeGratuit = new SNLabelChamp();
    snTypeGratuit = new SNTypeGratuit();
    pnlStocks = new SNPanel();
    pnlGauche2 = new SNPanelContenu();
    pnlInfosRepercutees2 = new SNPanelTitre();
    lbPeriodeAnalyse = new SNLabelChamp();
    FAPER = new XRiTextField();
    lbSemaines = new SNLabelUnite();
    lbGestionnaire = new SNLabelChamp();
    FAGP = new XRiTextField();
    lbGestionnaire2 = new SNLabelChamp();
    FADEL = new XRiTextField();
    sNLabelTitre1 = new SNLabelTitre();
    lbClasseA = new SNLabelChamp();
    F3NV1 = new XRiTextField();
    lbClasseB = new SNLabelChamp();
    F3NV2 = new XRiTextField();
    lbClasseC = new SNLabelChamp();
    F3NV3 = new XRiTextField();
    lbClasseD = new SNLabelChamp();
    F3NV4 = new XRiTextField();
    sNPanel1 = new SNPanel();
    lbNombreInventaires = new SNLabelChamp();
    FAJIT = new XRiTextField();
    lbDepreciationStock = new SNLabelChamp();
    FADPS = new XRiTextField();
    pnlDroite2 = new SNPanelContenu();
    pnlDelaiUO = new SNPanelTitre();
    lbTypeDelai = new SNLabelChamp();
    FATDUO = new XRiComboBox();
    lbDelaiStandard = new SNLabelChamp();
    FADUO1 = new XRiTextField();
    lbDelaiReception = new SNLabelChamp();
    FADUO2 = new XRiTextField();
    lbDelaiVente = new SNLabelChamp();
    FADUO3 = new XRiTextField();
    pnlCompta = new SNPanel();
    pnlGauche3 = new SNPanelContenu();
    pnlInfosRepercutees3 = new SNPanelTitre();
    lbCO1 = new SNLabelChamp();
    FAC01 = new XRiTextField();
    LCG01 = new RiZoneSortie();
    lbCO2 = new SNLabelChamp();
    FAC02 = new XRiTextField();
    LCG02 = new RiZoneSortie();
    lbCO3 = new SNLabelChamp();
    FAC03 = new XRiTextField();
    LCG03 = new RiZoneSortie();
    lbCO4 = new SNLabelChamp();
    FAC04 = new XRiTextField();
    LCG04 = new RiZoneSortie();
    lbCO5 = new SNLabelChamp();
    FAC05 = new XRiTextField();
    LCG05 = new RiZoneSortie();
    lbCO6 = new SNLabelChamp();
    FAC06 = new XRiTextField();
    LCG06 = new RiZoneSortie();
    lbCO7 = new SNLabelChamp();
    FAC07 = new XRiTextField();
    LCG07 = new RiZoneSortie();
    lbCO8 = new SNLabelChamp();
    FAC08 = new XRiTextField();
    LCG08 = new RiZoneSortie();
    lbCO9 = new SNLabelChamp();
    FAC09 = new XRiTextField();
    LCG09 = new RiZoneSortie();
    pnlDroite3 = new SNPanelContenu();
    pnlDelaiUO2 = new SNPanelTitre();
    lbCodeAnalytique = new SNLabelChamp();
    FASAN = new XRiTextField();
    lbPointeur = new SNLabelChamp();
    FANSA = new XRiComboBox();
    sNPanel2 = new SNPanel();
    OBJ_114 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_117 = new JLabel();
    OBJ_118 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_122 = new JLabel();
    SAN1 = new XRiTextField();
    SAN2 = new XRiTextField();
    SAN3 = new XRiTextField();
    SAN4 = new XRiTextField();
    SAN5 = new XRiTextField();
    SAN6 = new XRiTextField();
    SAN7 = new XRiTextField();
    SAN8 = new XRiTextField();
    SAN9 = new XRiTextField();
    pnlOptions = new SNPanel();
    pnlGauche4 = new SNPanelContenu();
    pnlOptions1 = new SNPanelTitre();
    lbSystemeVariables = new SNLabelChamp();
    FAVA1 = new XRiTextField();
    OBJ_176 = new JLabel();
    FAVA2 = new XRiTextField();
    lbLibelleSystemeVariables = new SNLabelChamp();
    FASVL = new XRiTextField();
    lbConditionQuantitative = new SNLabelChamp();
    FACQQ = new XRiComboBox();
    lbArticlesEquivalents = new SNLabelChamp();
    FAEQI = new XRiComboBox();
    FADEB = new XRiCheckBox();
    FAADS = new XRiCheckBox();
    lbGarantie = new SNLabelChamp();
    FACG = new XRiTextField();
    pnlDroite4 = new SNPanelContenu();
    pnlOptions2 = new SNPanelTitre();
    lbTypeZpArticle = new SNLabelChamp();
    FATEX = new XRiTextField();
    lbTypeZpMateriel = new SNLabelChamp();
    FATEM = new XRiTextField();
    pnlOptions3 = new SNPanel();
    pnlGauche5 = new SNPanelContenu();
    pnlEditions1 = new SNPanelTitre();
    FALOTP = new XRiCheckBox();
    FALOTH = new XRiCheckBox();
    FALOTE = new XRiCheckBox();
    FALOTF = new XRiCheckBox();
    pnlDroite5 = new SNPanelContenu();
    pnlEditions2 = new SNPanelTitre();
    FAEFR = new XRiCheckBox();
    FAETQ = new XRiCheckBox();
    lbRemises = new SNLabelChamp();
    FAEREM = new XRiComboBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes : Famille d'articles");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodeCategorie ----
        lbCodeCategorie.setText("code");
        lbCodeCategorie.setName("lbCodeCategorie");
        pnlPersonnalisation.add(lbCodeCategorie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- INDIND ----
        INDIND.setPreferredSize(new Dimension(40, 30));
        INDIND.setMinimumSize(new Dimension(40, 30));
        INDIND.setMaximumSize(new Dimension(40, 30));
        INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
        INDIND.setName("INDIND");
        pnlPersonnalisation.add(INDIND, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setMinimumSize(new Dimension(80, 30));
        lbLibelle.setMaximumSize(new Dimension(80, 30));
        lbLibelle.setPreferredSize(new Dimension(80, 30));
        lbLibelle.setName("lbLibelle");
        pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- FALIB ----
        FALIB.setPreferredSize(new Dimension(400, 30));
        FALIB.setMinimumSize(new Dimension(400, 30));
        FALIB.setMaximumSize(new Dimension(400, 30));
        FALIB.setFont(new Font("sansserif", Font.PLAIN, 14));
        FALIB.setName("FALIB");
        pnlPersonnalisation.add(FALIB, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlPersonnalisation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== tpnFamille ========
      {
        tpnFamille.setMinimumSize(new Dimension(890, 450));
        tpnFamille.setPreferredSize(new Dimension(890, 450));
        tpnFamille.setBackground(new Color(239, 239, 222));
        tpnFamille.setOpaque(true);
        tpnFamille.setFont(new Font("sansserif", Font.PLAIN, 14));
        tpnFamille.setName("tpnFamille");
        
        // ======== pnlDescription ========
        {
          pnlDescription.setOpaque(false);
          pnlDescription.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlDescription.setName("pnlDescription");
          pnlDescription.setLayout(new GridLayout(1, 2));
          
          // ======== pnlGauche ========
          {
            pnlGauche.setName("pnlGauche");
            pnlGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlInfosRepercutees ========
            {
              pnlInfosRepercutees.setTitre("Informations r\u00e9percut\u00e9es sur les articles de cette famille");
              pnlInfosRepercutees.setName("pnlInfosRepercutees");
              pnlInfosRepercutees.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInfosRepercutees.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlInfosRepercutees.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfosRepercutees.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlInfosRepercutees.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbTypeFicheArticle ----
              lbTypeFicheArticle.setText("Type de fiche article");
              lbTypeFicheArticle.setName("lbTypeFicheArticle");
              pnlInfosRepercutees.add(lbTypeFicheArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAIMG ----
              FAIMG.setComponentPopupMenu(null);
              FAIMG.setPreferredSize(new Dimension(40, 30));
              FAIMG.setMinimumSize(new Dimension(40, 30));
              FAIMG.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAIMG.setName("FAIMG");
              pnlInfosRepercutees.add(FAIMG, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeUnite ----
              lbCodeUnite.setText("Code unit\u00e9 de vente");
              lbCodeUnite.setName("lbCodeUnite");
              pnlInfosRepercutees.add(lbCodeUnite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAUNV ----
              FAUNV.setComponentPopupMenu(null);
              FAUNV.setMinimumSize(new Dimension(40, 30));
              FAUNV.setPreferredSize(new Dimension(40, 30));
              FAUNV.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAUNV.setName("FAUNV");
              pnlInfosRepercutees.add(FAUNV, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeUniteStock ----
              lbCodeUniteStock.setText("Code unit\u00e9 de stock");
              lbCodeUniteStock.setName("lbCodeUniteStock");
              pnlInfosRepercutees.add(lbCodeUniteStock, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAUNS ----
              FAUNS.setComponentPopupMenu(null);
              FAUNS.setPreferredSize(new Dimension(40, 30));
              FAUNS.setMinimumSize(new Dimension(40, 30));
              FAUNS.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAUNS.setName("FAUNS");
              pnlInfosRepercutees.add(FAUNS, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbReferenceTarif ----
              lbReferenceTarif.setText("R\u00e9f\u00e9rence tarif");
              lbReferenceTarif.setName("lbReferenceTarif");
              pnlInfosRepercutees.add(lbReferenceTarif, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FARTAR ----
              FARTAR.setComponentPopupMenu(null);
              FARTAR.setPreferredSize(new Dimension(60, 28));
              FARTAR.setMinimumSize(new Dimension(60, 28));
              FARTAR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FARTAR.setName("FARTAR");
              pnlInfosRepercutees.add(FARTAR, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCoefVenteT1 ----
              lbCoefVenteT1.setText("Coefficient de vente/tarif 1");
              lbCoefVenteT1.setMinimumSize(new Dimension(200, 30));
              lbCoefVenteT1.setMaximumSize(new Dimension(250, 30));
              lbCoefVenteT1.setPreferredSize(new Dimension(200, 30));
              lbCoefVenteT1.setName("lbCoefVenteT1");
              pnlInfosRepercutees.add(lbCoefVenteT1, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FACMA ----
              FACMA.setComponentPopupMenu(null);
              FACMA.setPreferredSize(new Dimension(70, 28));
              FACMA.setMinimumSize(new Dimension(70, 28));
              FACMA.setFont(new Font("sansserif", Font.PLAIN, 14));
              FACMA.setName("FACMA");
              pnlInfosRepercutees.add(FACMA, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCoeffPRV ----
              lbCoeffPRV.setText("Coefficient prix de revient");
              lbCoeffPRV.setName("lbCoeffPRV");
              pnlInfosRepercutees.add(lbCoeffPRV, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FACPR ----
              FACPR.setComponentPopupMenu(null);
              FACPR.setMinimumSize(new Dimension(70, 28));
              FACPR.setPreferredSize(new Dimension(70, 28));
              FACPR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FACPR.setName("FACPR");
              pnlInfosRepercutees.add(FACPR, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeTVA ----
              lbCodeTVA.setText("Code TVA");
              lbCodeTVA.setName("lbCodeTVA");
              pnlInfosRepercutees.add(lbCodeTVA, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FATVA ----
              FATVA.setComponentPopupMenu(null);
              FATVA.setFont(new Font("sansserif", Font.PLAIN, 14));
              FATVA.setMinimumSize(new Dimension(40, 30));
              FATVA.setPreferredSize(new Dimension(40, 30));
              FATVA.setName("FATVA");
              pnlInfosRepercutees.add(FATVA, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbExclusionFrais ----
              lbExclusionFrais.setText("Exclusion des frais calcul\u00e9s");
              lbExclusionFrais.setName("lbExclusionFrais");
              pnlInfosRepercutees.add(lbExclusionFrais, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAEXF ----
              FAEXF.setModel(
                  new DefaultComboBoxModel(new String[] { " ", "Frais n\u00b01", "Frais n\u00b02", "Frais n\u00b03", "Tous les frais" }));
              FAEXF.setComponentPopupMenu(null);
              FAEXF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAEXF.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAEXF.setName("FAEXF");
              pnlInfosRepercutees.add(FAEXF, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeCIP ----
              lbCodeCIP.setText("Code CIP");
              lbCodeCIP.setName("lbCodeCIP");
              pnlInfosRepercutees.add(lbCodeCIP, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FACIP ----
              FACIP.setComponentPopupMenu(null);
              FACIP.setFont(new Font("sansserif", Font.PLAIN, 14));
              FACIP.setMinimumSize(new Dimension(40, 30));
              FACIP.setPreferredSize(new Dimension(40, 30));
              FACIP.setName("FACIP");
              pnlInfosRepercutees.add(FACIP, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbZoneTarif ----
              lbZoneTarif.setText("Zone tarif");
              lbZoneTarif.setName("lbZoneTarif");
              pnlInfosRepercutees.add(lbZoneTarif, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FACTA ----
              FACTA.setComponentPopupMenu(null);
              FACTA.setFont(new Font("sansserif", Font.PLAIN, 14));
              FACTA.setMinimumSize(new Dimension(40, 30));
              FACTA.setPreferredSize(new Dimension(40, 30));
              FACTA.setName("FACTA");
              pnlInfosRepercutees.add(FACTA, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbRattachementPIC ----
              lbRattachementPIC.setText("Rattachement PIC");
              lbRattachementPIC.setName("lbRattachementPIC");
              pnlInfosRepercutees.add(lbRattachementPIC, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAPIC ----
              FAPIC.setComponentPopupMenu(null);
              FAPIC.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAPIC.setMinimumSize(new Dimension(40, 30));
              FAPIC.setPreferredSize(new Dimension(40, 30));
              FAPIC.setName("FAPIC");
              pnlInfosRepercutees.add(FAPIC, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTraitementSpecifique ----
              lbTraitementSpecifique.setText("Traitement sp\u00e9cifique");
              lbTraitementSpecifique.setName("lbTraitementSpecifique");
              pnlInfosRepercutees.add(lbTraitementSpecifique, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- FAGSP ----
              FAGSP.setModel(new DefaultComboBoxModel(
                  new String[] { "  ", "Sur l'article", "Article et vente", "Article et achat", "Vente et achat" }));
              FAGSP.setComponentPopupMenu(null);
              FAGSP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAGSP.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAGSP.setName("FAGSP");
              pnlInfosRepercutees.add(FAGSP, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche.add(pnlInfosRepercutees, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDescription.add(pnlGauche);
          
          // ======== pnlDroite ========
          {
            pnlDroite.setName("pnlDroite");
            pnlDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlCaracteristiquesImposees ========
            {
              pnlCaracteristiquesImposees.setTitre("Caract\u00e9ristiques impos\u00e9es aux articles de cette famille");
              pnlCaracteristiquesImposees.setName("pnlCaracteristiquesImposees");
              pnlCaracteristiquesImposees.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCaracteristiquesImposees.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCaracteristiquesImposees.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlCaracteristiquesImposees.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCaracteristiquesImposees.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbMagasin ----
              lbMagasin.setText("Magasin");
              lbMagasin.setName("lbMagasin");
              pnlCaracteristiquesImposees.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snMagasin ----
              snMagasin.setName("snMagasin");
              pnlCaracteristiquesImposees.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbPourcentageMiniMarge ----
              lbPourcentageMiniMarge.setText("Pourcentage minimal de marge");
              lbPourcentageMiniMarge.setName("lbPourcentageMiniMarge");
              pnlCaracteristiquesImposees.add(lbPourcentageMiniMarge, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAMAR ----
              FAMAR.setComponentPopupMenu(null);
              FAMAR.setPreferredSize(new Dimension(40, 30));
              FAMAR.setMinimumSize(new Dimension(40, 30));
              FAMAR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAMAR.setName("FAMAR");
              pnlCaracteristiquesImposees.add(FAMAR, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbArrondi ----
              lbArrondi.setText("Arrondi");
              lbArrondi.setName("lbArrondi");
              pnlCaracteristiquesImposees.add(lbArrondi, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FARON ----
              FARON.setModel(new DefaultComboBoxModel(new String[] { "", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00" }));
              FARON.setComponentPopupMenu(null);
              FARON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FARON.setFont(new Font("sansserif", Font.PLAIN, 14));
              FARON.setName("FARON");
              pnlCaracteristiquesImposees.add(FARON, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FAVOD ----
              FAVOD.setText("Volume en dm3");
              FAVOD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAVOD.setPreferredSize(new Dimension(200, 30));
              FAVOD.setMinimumSize(new Dimension(200, 30));
              FAVOD.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAVOD.setMaximumSize(new Dimension(250, 30));
              FAVOD.setName("FAVOD");
              pnlCaracteristiquesImposees.add(FAVOD, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAPDG ----
              FAPDG.setText("Poids en grammes");
              FAPDG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAPDG.setPreferredSize(new Dimension(200, 30));
              FAPDG.setMinimumSize(new Dimension(200, 30));
              FAPDG.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAPDG.setName("FAPDG");
              pnlCaracteristiquesImposees.add(FAPDG, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FAPHO ----
              FAPHO.setText("Photo en premier sur la fiche article");
              FAPHO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAPHO.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAPHO.setMinimumSize(new Dimension(247, 30));
              FAPHO.setMaximumSize(new Dimension(247, 30));
              FAPHO.setPreferredSize(new Dimension(250, 30));
              FAPHO.setName("FAPHO");
              pnlCaracteristiquesImposees.add(FAPHO, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FADPOS ----
              FADPOS.setText("Gestion d\u00e9positaire");
              FADPOS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FADPOS.setPreferredSize(new Dimension(150, 28));
              FADPOS.setMinimumSize(new Dimension(150, 28));
              FADPOS.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADPOS.setName("FADPOS");
              pnlCaracteristiquesImposees.add(FADPOS, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAWEB ----
              FAWEB.setText("Les articles de cette familles sont exclus de la vente via le WebShop");
              FAWEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAWEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAWEB.setPreferredSize(new Dimension(150, 30));
              FAWEB.setMinimumSize(new Dimension(150, 30));
              FAWEB.setMaximumSize(new Dimension(150, 30));
              FAWEB.setName("FAWEB");
              pnlCaracteristiquesImposees.add(FAWEB, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FASPE ----
              FASPE.setText("Sp\u00e9cial");
              FASPE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FASPE.setFont(new Font("sansserif", Font.PLAIN, 14));
              FASPE.setPreferredSize(new Dimension(150, 30));
              FASPE.setMinimumSize(new Dimension(150, 30));
              FASPE.setMaximumSize(new Dimension(150, 30));
              FASPE.setName("FASPE");
              pnlCaracteristiquesImposees.add(FASPE, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbRegroupement ----
              lbRegroupement.setText("Regroupement");
              lbRegroupement.setName("lbRegroupement");
              pnlCaracteristiquesImposees.add(lbRegroupement, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAFAR ----
              FAFAR.setComponentPopupMenu(null);
              FAFAR.setMinimumSize(new Dimension(40, 30));
              FAFAR.setPreferredSize(new Dimension(40, 30));
              FAFAR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAFAR.setName("FAFAR");
              pnlCaracteristiquesImposees.add(FAFAR, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCalculPRV ----
              lbCalculPRV.setText("Calcul du prix de revient");
              lbCalculPRV.setName("lbCalculPRV");
              pnlCaracteristiquesImposees.add(lbCalculPRV, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAPR ----
              FAPR.setComponentPopupMenu(null);
              FAPR.setMinimumSize(new Dimension(40, 30));
              FAPR.setPreferredSize(new Dimension(40, 30));
              FAPR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAPR.setName("FAPR");
              pnlCaracteristiquesImposees.add(FAPR, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTaxeParafiscale ----
              lbTaxeParafiscale.setText("Assujetti \u00e0 la taxe parafiscale");
              lbTaxeParafiscale.setName("lbTaxeParafiscale");
              pnlCaracteristiquesImposees.add(lbTaxeParafiscale, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FATPF ----
              FATPF.setComponentPopupMenu(null);
              FATPF.setMinimumSize(new Dimension(40, 30));
              FATPF.setPreferredSize(new Dimension(40, 30));
              FATPF.setFont(new Font("sansserif", Font.PLAIN, 14));
              FATPF.setName("FATPF");
              pnlCaracteristiquesImposees.add(FATPF, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeGratuit ----
              lbTypeGratuit.setText("Type gratuit");
              lbTypeGratuit.setName("lbTypeGratuit");
              pnlCaracteristiquesImposees.add(lbTypeGratuit, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snTypeGratuit ----
              snTypeGratuit.setName("snTypeGratuit");
              pnlCaracteristiquesImposees.add(snTypeGratuit, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite.add(pnlCaracteristiquesImposees, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlDescription.add(pnlDroite);
        }
        tpnFamille.addTab("Articles", pnlDescription);
        
        // ======== pnlStocks ========
        {
          pnlStocks.setOpaque(false);
          pnlStocks.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlStocks.setName("pnlStocks");
          pnlStocks.setLayout(new GridLayout(1, 2));
          
          // ======== pnlGauche2 ========
          {
            pnlGauche2.setName("pnlGauche2");
            pnlGauche2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlGauche2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlInfosRepercutees2 ========
            {
              pnlInfosRepercutees2.setTitre("Calcul de la rotation en stock");
              pnlInfosRepercutees2.setName("pnlInfosRepercutees2");
              pnlInfosRepercutees2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInfosRepercutees2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfosRepercutees2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfosRepercutees2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlInfosRepercutees2.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbPeriodeAnalyse ----
              lbPeriodeAnalyse.setText("P\u00e9riode d'analyse");
              lbPeriodeAnalyse.setName("lbPeriodeAnalyse");
              pnlInfosRepercutees2.add(lbPeriodeAnalyse, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAPER ----
              FAPER.setComponentPopupMenu(null);
              FAPER.setPreferredSize(new Dimension(30, 28));
              FAPER.setMinimumSize(new Dimension(30, 28));
              FAPER.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAPER.setName("FAPER");
              pnlInfosRepercutees2.add(FAPER, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbSemaines ----
              lbSemaines.setText("semaines");
              lbSemaines.setMaximumSize(new Dimension(150, 30));
              lbSemaines.setMinimumSize(new Dimension(100, 30));
              lbSemaines.setPreferredSize(new Dimension(100, 30));
              lbSemaines.setName("lbSemaines");
              pnlInfosRepercutees2.add(lbSemaines, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbGestionnaire ----
              lbGestionnaire.setText("Gestionnaire produit");
              lbGestionnaire.setName("lbGestionnaire");
              pnlInfosRepercutees2.add(lbGestionnaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAGP ----
              FAGP.setComponentPopupMenu(null);
              FAGP.setMinimumSize(new Dimension(25, 28));
              FAGP.setPreferredSize(new Dimension(25, 28));
              FAGP.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAGP.setName("FAGP");
              pnlInfosRepercutees2.add(FAGP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbGestionnaire2 ----
              lbGestionnaire2.setText("D\u00e9lai de survente");
              lbGestionnaire2.setName("lbGestionnaire2");
              pnlInfosRepercutees2.add(lbGestionnaire2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FADEL ----
              FADEL.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADEL.setName("FADEL");
              pnlInfosRepercutees2.add(FADEL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- sNLabelTitre1 ----
              sNLabelTitre1.setText("Nombre de ventes par classe");
              sNLabelTitre1.setName("sNLabelTitre1");
              pnlInfosRepercutees2.add(sNLabelTitre1, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbClasseA ----
              lbClasseA.setText("Classe A");
              lbClasseA.setName("lbClasseA");
              pnlInfosRepercutees2.add(lbClasseA, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- F3NV1 ----
              F3NV1.setComponentPopupMenu(null);
              F3NV1.setPreferredSize(new Dimension(40, 28));
              F3NV1.setMinimumSize(new Dimension(40, 28));
              F3NV1.setFont(new Font("sansserif", Font.PLAIN, 14));
              F3NV1.setName("F3NV1");
              pnlInfosRepercutees2.add(F3NV1, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbClasseB ----
              lbClasseB.setText("Classe B");
              lbClasseB.setName("lbClasseB");
              pnlInfosRepercutees2.add(lbClasseB, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- F3NV2 ----
              F3NV2.setComponentPopupMenu(null);
              F3NV2.setPreferredSize(new Dimension(40, 28));
              F3NV2.setMinimumSize(new Dimension(40, 28));
              F3NV2.setFont(new Font("sansserif", Font.PLAIN, 14));
              F3NV2.setName("F3NV2");
              pnlInfosRepercutees2.add(F3NV2, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbClasseC ----
              lbClasseC.setText("Classe C");
              lbClasseC.setMinimumSize(new Dimension(200, 30));
              lbClasseC.setMaximumSize(new Dimension(250, 30));
              lbClasseC.setPreferredSize(new Dimension(200, 30));
              lbClasseC.setName("lbClasseC");
              pnlInfosRepercutees2.add(lbClasseC, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- F3NV3 ----
              F3NV3.setComponentPopupMenu(null);
              F3NV3.setPreferredSize(new Dimension(40, 28));
              F3NV3.setMinimumSize(new Dimension(40, 28));
              F3NV3.setFont(new Font("sansserif", Font.PLAIN, 14));
              F3NV3.setName("F3NV3");
              pnlInfosRepercutees2.add(F3NV3, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbClasseD ----
              lbClasseD.setText("Classe D");
              lbClasseD.setName("lbClasseD");
              pnlInfosRepercutees2.add(lbClasseD, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- F3NV4 ----
              F3NV4.setComponentPopupMenu(null);
              F3NV4.setPreferredSize(new Dimension(40, 28));
              F3NV4.setMinimumSize(new Dimension(40, 28));
              F3NV4.setFont(new Font("sansserif", Font.PLAIN, 14));
              F3NV4.setName("F3NV4");
              pnlInfosRepercutees2.add(F3NV4, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            }
            pnlGauche2.add(pnlInfosRepercutees2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== sNPanel1 ========
            {
              sNPanel1.setName("sNPanel1");
              sNPanel1.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbNombreInventaires ----
              lbNombreInventaires.setText("Nombre d'inventaires par an");
              lbNombreInventaires.setMaximumSize(new Dimension(250, 30));
              lbNombreInventaires.setMinimumSize(new Dimension(200, 30));
              lbNombreInventaires.setPreferredSize(new Dimension(200, 30));
              lbNombreInventaires.setName("lbNombreInventaires");
              sNPanel1.add(lbNombreInventaires, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAJIT ----
              FAJIT.setComponentPopupMenu(null);
              FAJIT.setMinimumSize(new Dimension(30, 30));
              FAJIT.setPreferredSize(new Dimension(30, 30));
              FAJIT.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAJIT.setName("FAJIT");
              sNPanel1.add(FAJIT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDepreciationStock ----
              lbDepreciationStock.setText("D\u00e9preciation stock");
              lbDepreciationStock.setName("lbDepreciationStock");
              sNPanel1.add(lbDepreciationStock, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FADPS ----
              FADPS.setComponentPopupMenu(null);
              FADPS.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADPS.setName("FADPS");
              sNPanel1.add(FADPS, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlGauche2.add(sNPanel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlStocks.add(pnlGauche2);
          
          // ======== pnlDroite2 ========
          {
            pnlDroite2.setName("pnlDroite2");
            pnlDroite2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite2.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite2.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlDelaiUO ========
            {
              pnlDelaiUO.setTitre("D\u00e9lai d'utilisation optimale (DLUO)");
              pnlDelaiUO.setName("pnlDelaiUO");
              pnlDelaiUO.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDelaiUO.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlDelaiUO.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDelaiUO.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDelaiUO.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbTypeDelai ----
              lbTypeDelai.setText("D\u00e9lai exprim\u00e9 en");
              lbTypeDelai.setName("lbTypeDelai");
              pnlDelaiUO.add(lbTypeDelai, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FATDUO ----
              FATDUO.setModel(new DefaultComboBoxModel(new String[] { " ", "Jour", "Semaine", "Mois" }));
              FATDUO.setComponentPopupMenu(null);
              FATDUO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FATDUO.setFont(new Font("sansserif", Font.PLAIN, 14));
              FATDUO.setName("FATDUO");
              pnlDelaiUO.add(FATDUO, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDelaiStandard ----
              lbDelaiStandard.setText("Standard");
              lbDelaiStandard.setName("lbDelaiStandard");
              pnlDelaiUO.add(lbDelaiStandard, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FADUO1 ----
              FADUO1.setToolTipText("Nombre de jours");
              FADUO1.setComponentPopupMenu(null);
              FADUO1.setPreferredSize(new Dimension(30, 28));
              FADUO1.setMinimumSize(new Dimension(30, 28));
              FADUO1.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADUO1.setName("FADUO1");
              pnlDelaiUO.add(FADUO1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDelaiReception ----
              lbDelaiReception.setText("R\u00e9ception");
              lbDelaiReception.setName("lbDelaiReception");
              pnlDelaiUO.add(lbDelaiReception, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FADUO2 ----
              FADUO2.setToolTipText("Nombre de semaines");
              FADUO2.setComponentPopupMenu(null);
              FADUO2.setPreferredSize(new Dimension(30, 28));
              FADUO2.setMinimumSize(new Dimension(30, 28));
              FADUO2.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADUO2.setName("FADUO2");
              pnlDelaiUO.add(FADUO2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbDelaiVente ----
              lbDelaiVente.setText("Vente");
              lbDelaiVente.setName("lbDelaiVente");
              pnlDelaiUO.add(lbDelaiVente, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- FADUO3 ----
              FADUO3.setToolTipText("Nombre de mois");
              FADUO3.setComponentPopupMenu(null);
              FADUO3.setPreferredSize(new Dimension(30, 28));
              FADUO3.setMinimumSize(new Dimension(30, 28));
              FADUO3.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADUO3.setName("FADUO3");
              pnlDelaiUO.add(FADUO3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlDroite2.add(pnlDelaiUO, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlStocks.add(pnlDroite2);
        }
        tpnFamille.addTab("Stocks", pnlStocks);
        
        // ======== pnlCompta ========
        {
          pnlCompta.setOpaque(false);
          pnlCompta.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlCompta.setName("pnlCompta");
          pnlCompta.setLayout(new GridLayout(1, 2));
          
          // ======== pnlGauche3 ========
          {
            pnlGauche3.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlGauche3.setName("pnlGauche3");
            pnlGauche3.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche3.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlGauche3.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlInfosRepercutees3 ========
            {
              pnlInfosRepercutees3.setTitre("Comptabilisation des ventes");
              pnlInfosRepercutees3.setFont(new Font("sansserif", Font.PLAIN, 14));
              pnlInfosRepercutees3.setName("pnlInfosRepercutees3");
              pnlInfosRepercutees3.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInfosRepercutees3.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfosRepercutees3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlInfosRepercutees3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlInfosRepercutees3.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbCO1 ----
              lbCO1.setText("N\u00b0CO ventes taxables TVA1");
              lbCO1.setMinimumSize(new Dimension(200, 30));
              lbCO1.setMaximumSize(new Dimension(200, 30));
              lbCO1.setPreferredSize(new Dimension(200, 30));
              lbCO1.setName("lbCO1");
              pnlInfosRepercutees3.add(lbCO1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC01 ----
              FAC01.setPreferredSize(new Dimension(40, 28));
              FAC01.setMinimumSize(new Dimension(40, 25));
              FAC01.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC01.setName("FAC01");
              pnlInfosRepercutees3.add(FAC01, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG01 ----
              LCG01.setText("@LCG01@");
              LCG01.setMinimumSize(new Dimension(245, 25));
              LCG01.setPreferredSize(new Dimension(245, 25));
              LCG01.setMaximumSize(new Dimension(245, 25));
              LCG01.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG01.setName("LCG01");
              pnlInfosRepercutees3.add(LCG01, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO2 ----
              lbCO2.setText("N\u00b0CO ventes taxables TVA2");
              lbCO2.setMinimumSize(new Dimension(200, 30));
              lbCO2.setMaximumSize(new Dimension(200, 30));
              lbCO2.setPreferredSize(new Dimension(200, 30));
              lbCO2.setName("lbCO2");
              pnlInfosRepercutees3.add(lbCO2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC02 ----
              FAC02.setPreferredSize(new Dimension(40, 28));
              FAC02.setMinimumSize(new Dimension(40, 25));
              FAC02.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC02.setName("FAC02");
              pnlInfosRepercutees3.add(FAC02, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG02 ----
              LCG02.setText("@LCG02@");
              LCG02.setPreferredSize(new Dimension(245, 25));
              LCG02.setMinimumSize(new Dimension(245, 25));
              LCG02.setMaximumSize(new Dimension(245, 25));
              LCG02.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG02.setName("LCG02");
              pnlInfosRepercutees3.add(LCG02, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO3 ----
              lbCO3.setText("N\u00b0CO ventes taxables TVA3");
              lbCO3.setMinimumSize(new Dimension(200, 30));
              lbCO3.setMaximumSize(new Dimension(200, 30));
              lbCO3.setPreferredSize(new Dimension(200, 30));
              lbCO3.setName("lbCO3");
              pnlInfosRepercutees3.add(lbCO3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC03 ----
              FAC03.setPreferredSize(new Dimension(40, 28));
              FAC03.setMinimumSize(new Dimension(40, 25));
              FAC03.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC03.setName("FAC03");
              pnlInfosRepercutees3.add(FAC03, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG03 ----
              LCG03.setText("@LCG03@");
              LCG03.setPreferredSize(new Dimension(245, 25));
              LCG03.setMinimumSize(new Dimension(245, 25));
              LCG03.setMaximumSize(new Dimension(245, 25));
              LCG03.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG03.setName("LCG03");
              pnlInfosRepercutees3.add(LCG03, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO4 ----
              lbCO4.setText("N\u00b0CO ventes taxables TVA4");
              lbCO4.setMinimumSize(new Dimension(200, 30));
              lbCO4.setMaximumSize(new Dimension(200, 30));
              lbCO4.setPreferredSize(new Dimension(200, 30));
              lbCO4.setName("lbCO4");
              pnlInfosRepercutees3.add(lbCO4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC04 ----
              FAC04.setPreferredSize(new Dimension(40, 28));
              FAC04.setMinimumSize(new Dimension(40, 25));
              FAC04.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC04.setName("FAC04");
              pnlInfosRepercutees3.add(FAC04, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG04 ----
              LCG04.setText("@LCG04@");
              LCG04.setPreferredSize(new Dimension(245, 25));
              LCG04.setMinimumSize(new Dimension(245, 25));
              LCG04.setMaximumSize(new Dimension(245, 25));
              LCG04.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG04.setName("LCG04");
              pnlInfosRepercutees3.add(LCG04, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO5 ----
              lbCO5.setText("N\u00b0CO ventes taxables TVA5");
              lbCO5.setMinimumSize(new Dimension(200, 30));
              lbCO5.setMaximumSize(new Dimension(200, 30));
              lbCO5.setPreferredSize(new Dimension(200, 30));
              lbCO5.setName("lbCO5");
              pnlInfosRepercutees3.add(lbCO5, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC05 ----
              FAC05.setPreferredSize(new Dimension(40, 28));
              FAC05.setMinimumSize(new Dimension(40, 25));
              FAC05.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC05.setName("FAC05");
              pnlInfosRepercutees3.add(FAC05, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG05 ----
              LCG05.setText("@LCG05@");
              LCG05.setPreferredSize(new Dimension(245, 25));
              LCG05.setMinimumSize(new Dimension(245, 25));
              LCG05.setMaximumSize(new Dimension(245, 25));
              LCG05.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG05.setName("LCG05");
              pnlInfosRepercutees3.add(LCG05, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO6 ----
              lbCO6.setText("N\u00b0CO ventes taxables TVA6");
              lbCO6.setMinimumSize(new Dimension(200, 30));
              lbCO6.setMaximumSize(new Dimension(200, 30));
              lbCO6.setPreferredSize(new Dimension(200, 30));
              lbCO6.setName("lbCO6");
              pnlInfosRepercutees3.add(lbCO6, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC06 ----
              FAC06.setPreferredSize(new Dimension(40, 28));
              FAC06.setMinimumSize(new Dimension(40, 25));
              FAC06.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC06.setName("FAC06");
              pnlInfosRepercutees3.add(FAC06, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG06 ----
              LCG06.setText("@LCG06@");
              LCG06.setPreferredSize(new Dimension(245, 25));
              LCG06.setMinimumSize(new Dimension(245, 25));
              LCG06.setMaximumSize(new Dimension(245, 25));
              LCG06.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG06.setName("LCG06");
              pnlInfosRepercutees3.add(LCG06, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO7 ----
              lbCO7.setText("N\u00b0CO ventes export CEE");
              lbCO7.setMinimumSize(new Dimension(200, 30));
              lbCO7.setMaximumSize(new Dimension(200, 30));
              lbCO7.setPreferredSize(new Dimension(200, 30));
              lbCO7.setName("lbCO7");
              pnlInfosRepercutees3.add(lbCO7, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC07 ----
              FAC07.setPreferredSize(new Dimension(40, 28));
              FAC07.setMinimumSize(new Dimension(40, 25));
              FAC07.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC07.setName("FAC07");
              pnlInfosRepercutees3.add(FAC07, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG07 ----
              LCG07.setText("@LCG07@");
              LCG07.setPreferredSize(new Dimension(245, 25));
              LCG07.setMinimumSize(new Dimension(245, 25));
              LCG07.setMaximumSize(new Dimension(245, 25));
              LCG07.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG07.setName("LCG07");
              pnlInfosRepercutees3.add(LCG07, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO8 ----
              lbCO8.setText("N\u00b0CO ventes export");
              lbCO8.setMinimumSize(new Dimension(200, 30));
              lbCO8.setMaximumSize(new Dimension(200, 30));
              lbCO8.setPreferredSize(new Dimension(200, 30));
              lbCO8.setName("lbCO8");
              pnlInfosRepercutees3.add(lbCO8, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAC08 ----
              FAC08.setPreferredSize(new Dimension(40, 28));
              FAC08.setMinimumSize(new Dimension(40, 25));
              FAC08.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC08.setName("FAC08");
              pnlInfosRepercutees3.add(FAC08, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- LCG08 ----
              LCG08.setText("@LCG08@");
              LCG08.setPreferredSize(new Dimension(245, 25));
              LCG08.setMinimumSize(new Dimension(245, 25));
              LCG08.setMaximumSize(new Dimension(245, 25));
              LCG08.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG08.setName("LCG08");
              pnlInfosRepercutees3.add(LCG08, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCO9 ----
              lbCO9.setText("N\u00b0CO ventes assimil\u00e9es export");
              lbCO9.setMinimumSize(new Dimension(200, 30));
              lbCO9.setMaximumSize(new Dimension(200, 30));
              lbCO9.setPreferredSize(new Dimension(200, 30));
              lbCO9.setName("lbCO9");
              pnlInfosRepercutees3.add(lbCO9, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- FAC09 ----
              FAC09.setPreferredSize(new Dimension(40, 28));
              FAC09.setMinimumSize(new Dimension(40, 25));
              FAC09.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAC09.setName("FAC09");
              pnlInfosRepercutees3.add(FAC09, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- LCG09 ----
              LCG09.setText("@LCG09@");
              LCG09.setPreferredSize(new Dimension(245, 25));
              LCG09.setMinimumSize(new Dimension(245, 25));
              LCG09.setMaximumSize(new Dimension(245, 25));
              LCG09.setFont(new Font("sansserif", Font.PLAIN, 14));
              LCG09.setName("LCG09");
              pnlInfosRepercutees3.add(LCG09, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlGauche3.add(pnlInfosRepercutees3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlCompta.add(pnlGauche3);
          
          // ======== pnlDroite3 ========
          {
            pnlDroite3.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlDroite3.setName("pnlDroite3");
            pnlDroite3.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite3.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite3.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite3.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlDelaiUO2 ========
            {
              pnlDelaiUO2.setTitre("Analytique");
              pnlDelaiUO2.setFont(new Font("sansserif", Font.PLAIN, 14));
              pnlDelaiUO2.setName("pnlDelaiUO2");
              pnlDelaiUO2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDelaiUO2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlDelaiUO2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlDelaiUO2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlDelaiUO2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbCodeAnalytique ----
              lbCodeAnalytique.setText("Code analytique");
              lbCodeAnalytique.setName("lbCodeAnalytique");
              pnlDelaiUO2.add(lbCodeAnalytique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FASAN ----
              FASAN.setComponentPopupMenu(null);
              FASAN.setPreferredSize(new Dimension(60, 30));
              FASAN.setMinimumSize(new Dimension(60, 30));
              FASAN.setFont(new Font("sansserif", Font.PLAIN, 14));
              FASAN.setName("FASAN");
              pnlDelaiUO2.add(FASAN, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbPointeur ----
              lbPointeur.setText("Pointeur");
              lbPointeur.setName("lbPointeur");
              pnlDelaiUO2.add(lbPointeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FANSA ----
              FANSA.setModel(new DefaultComboBoxModel(new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" }));
              FANSA.setComponentPopupMenu(null);
              FANSA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FANSA.setFont(new Font("sansserif", Font.PLAIN, 14));
              FANSA.setPreferredSize(new Dimension(50, 30));
              FANSA.setMinimumSize(new Dimension(50, 30));
              FANSA.setName("FANSA");
              pnlDelaiUO2.add(FANSA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== sNPanel2 ========
              {
                sNPanel2.setFont(new Font("sansserif", Font.PLAIN, 14));
                sNPanel2.setName("sNPanel2");
                sNPanel2.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) sNPanel2.getLayout()).columnWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- OBJ_114 ----
                OBJ_114.setText("1");
                OBJ_114.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_114.setName("OBJ_114");
                sNPanel2.add(OBJ_114, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_115 ----
                OBJ_115.setText("2");
                OBJ_115.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_115.setName("OBJ_115");
                sNPanel2.add(OBJ_115, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_116 ----
                OBJ_116.setText("3");
                OBJ_116.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_116.setName("OBJ_116");
                sNPanel2.add(OBJ_116, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_117 ----
                OBJ_117.setText("4");
                OBJ_117.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_117.setName("OBJ_117");
                sNPanel2.add(OBJ_117, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_118 ----
                OBJ_118.setText("5");
                OBJ_118.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_118.setName("OBJ_118");
                sNPanel2.add(OBJ_118, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_119 ----
                OBJ_119.setText("6");
                OBJ_119.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_119.setName("OBJ_119");
                sNPanel2.add(OBJ_119, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_120 ----
                OBJ_120.setText("7");
                OBJ_120.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_120.setName("OBJ_120");
                sNPanel2.add(OBJ_120, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_121 ----
                OBJ_121.setText("8");
                OBJ_121.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_121.setName("OBJ_121");
                sNPanel2.add(OBJ_121, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- OBJ_122 ----
                OBJ_122.setText("9");
                OBJ_122.setFont(new Font("sansserif", Font.PLAIN, 14));
                OBJ_122.setName("OBJ_122");
                sNPanel2.add(OBJ_122, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- SAN1 ----
                SAN1.setComponentPopupMenu(null);
                SAN1.setPreferredSize(new Dimension(50, 30));
                SAN1.setMinimumSize(new Dimension(50, 30));
                SAN1.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN1.setName("SAN1");
                sNPanel2.add(SAN1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN2 ----
                SAN2.setComponentPopupMenu(null);
                SAN2.setPreferredSize(new Dimension(50, 30));
                SAN2.setMinimumSize(new Dimension(50, 30));
                SAN2.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN2.setName("SAN2");
                sNPanel2.add(SAN2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN3 ----
                SAN3.setComponentPopupMenu(null);
                SAN3.setPreferredSize(new Dimension(50, 30));
                SAN3.setMinimumSize(new Dimension(50, 30));
                SAN3.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN3.setName("SAN3");
                sNPanel2.add(SAN3, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN4 ----
                SAN4.setComponentPopupMenu(null);
                SAN4.setPreferredSize(new Dimension(50, 30));
                SAN4.setMinimumSize(new Dimension(50, 30));
                SAN4.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN4.setName("SAN4");
                sNPanel2.add(SAN4, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN5 ----
                SAN5.setComponentPopupMenu(null);
                SAN5.setPreferredSize(new Dimension(50, 30));
                SAN5.setMinimumSize(new Dimension(50, 30));
                SAN5.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN5.setName("SAN5");
                sNPanel2.add(SAN5, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN6 ----
                SAN6.setComponentPopupMenu(null);
                SAN6.setPreferredSize(new Dimension(50, 30));
                SAN6.setMinimumSize(new Dimension(50, 30));
                SAN6.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN6.setName("SAN6");
                sNPanel2.add(SAN6, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN7 ----
                SAN7.setComponentPopupMenu(null);
                SAN7.setPreferredSize(new Dimension(50, 30));
                SAN7.setMinimumSize(new Dimension(50, 30));
                SAN7.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN7.setName("SAN7");
                sNPanel2.add(SAN7, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN8 ----
                SAN8.setComponentPopupMenu(null);
                SAN8.setPreferredSize(new Dimension(50, 30));
                SAN8.setMinimumSize(new Dimension(50, 30));
                SAN8.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN8.setName("SAN8");
                sNPanel2.add(SAN8, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- SAN9 ----
                SAN9.setComponentPopupMenu(null);
                SAN9.setPreferredSize(new Dimension(50, 30));
                SAN9.setMinimumSize(new Dimension(50, 30));
                SAN9.setFont(new Font("sansserif", Font.PLAIN, 14));
                SAN9.setName("SAN9");
                sNPanel2.add(SAN9, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlDelaiUO2.add(sNPanel2, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlDroite3.add(pnlDelaiUO2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCompta.add(pnlDroite3);
        }
        tpnFamille.addTab("Comptabilit\u00e9", pnlCompta);
        
        // ======== pnlOptions ========
        {
          pnlOptions.setOpaque(false);
          pnlOptions.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridLayout(1, 2));
          
          // ======== pnlGauche4 ========
          {
            pnlGauche4.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlGauche4.setName("pnlGauche4");
            pnlGauche4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche4.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlGauche4.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlOptions1 ========
            {
              pnlOptions1.setFont(new Font("sansserif", Font.PLAIN, 14));
              pnlOptions1.setName("pnlOptions1");
              pnlOptions1.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOptions1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOptions1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOptions1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlOptions1.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbSystemeVariables ----
              lbSystemeVariables.setText("Syst\u00e8me variable");
              lbSystemeVariables.setName("lbSystemeVariables");
              pnlOptions1.add(lbSystemeVariables, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAVA1 ----
              FAVA1.setComponentPopupMenu(null);
              FAVA1.setMinimumSize(new Dimension(40, 30));
              FAVA1.setPreferredSize(new Dimension(40, 30));
              FAVA1.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAVA1.setName("FAVA1");
              pnlOptions1.add(FAVA1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- OBJ_176 ----
              OBJ_176.setText("/");
              OBJ_176.setFont(new Font("sansserif", Font.PLAIN, 14));
              OBJ_176.setName("OBJ_176");
              pnlOptions1.add(OBJ_176, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAVA2 ----
              FAVA2.setComponentPopupMenu(null);
              FAVA2.setMinimumSize(new Dimension(40, 30));
              FAVA2.setPreferredSize(new Dimension(40, 30));
              FAVA2.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAVA2.setName("FAVA2");
              pnlOptions1.add(FAVA2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbLibelleSystemeVariables ----
              lbLibelleSystemeVariables.setText("Libell\u00e9 syst\u00e8me variable");
              lbLibelleSystemeVariables.setMinimumSize(new Dimension(200, 30));
              lbLibelleSystemeVariables.setMaximumSize(new Dimension(250, 30));
              lbLibelleSystemeVariables.setPreferredSize(new Dimension(200, 30));
              lbLibelleSystemeVariables.setName("lbLibelleSystemeVariables");
              pnlOptions1.add(lbLibelleSystemeVariables, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FASVL ----
              FASVL.setComponentPopupMenu(null);
              FASVL.setFont(new Font("sansserif", Font.PLAIN, 14));
              FASVL.setName("FASVL");
              pnlOptions1.add(FASVL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbConditionQuantitative ----
              lbConditionQuantitative.setText("Conditions quantitatives");
              lbConditionQuantitative.setName("lbConditionQuantitative");
              pnlOptions1.add(lbConditionQuantitative, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FACQQ ----
              FACQQ.setModel(new DefaultComboBoxModel(new String[] { "", "en unit\u00e9 de vente", "en unit\u00e9 de conditionnement",
                  "en unit\u00e9 de surconditionnement", "multipli\u00e9es par 100", "multipli\u00e9e par 1000" }));
              FACQQ.setComponentPopupMenu(null);
              FACQQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FACQQ.setFont(new Font("sansserif", Font.PLAIN, 14));
              FACQQ.setName("FACQQ");
              pnlOptions1.add(FACQQ, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbArticlesEquivalents ----
              lbArticlesEquivalents.setText("Articles \u00e9quivalents");
              lbArticlesEquivalents.setName("lbArticlesEquivalents");
              pnlOptions1.add(lbArticlesEquivalents, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAEQI ----
              FAEQI.setModel(new DefaultComboBoxModel(
                  new String[] { "Recherche manuelle", "Recherche syst\u00e9matique", "Recherche si stock nul" }));
              FAEQI.setToolTipText("Type de traitement en saisie de commande");
              FAEQI.setComponentPopupMenu(null);
              FAEQI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAEQI.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAEQI.setName("FAEQI");
              pnlOptions1.add(FAEQI, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FADEB ----
              FADEB.setText("Pas d'unit\u00e9s suppl\u00e9mentaires sur la DEB");
              FADEB.setToolTipText("Pas d'unit\u00e9s suppl\u00e9mentaires sur la DEB");
              FADEB.setComponentPopupMenu(null);
              FADEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FADEB.setMinimumSize(new Dimension(250, 30));
              FADEB.setPreferredSize(new Dimension(250, 30));
              FADEB.setFont(new Font("sansserif", Font.PLAIN, 14));
              FADEB.setName("FADEB");
              pnlOptions1.add(FADEB, new GridBagConstraints(0, 4, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FAADS ----
              FAADS.setText("Contr\u00f4le article d\u00e9j\u00e0 saisi dans le bon");
              FAADS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAADS.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAADS.setMinimumSize(new Dimension(259, 30));
              FAADS.setMaximumSize(new Dimension(259, 30));
              FAADS.setPreferredSize(new Dimension(259, 30));
              FAADS.setName("FAADS");
              pnlOptions1.add(FAADS, new GridBagConstraints(0, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbGarantie ----
              lbGarantie.setText("Garantie");
              lbGarantie.setName("lbGarantie");
              pnlOptions1.add(lbGarantie, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FACG ----
              FACG.setComponentPopupMenu(null);
              FACG.setFont(new Font("sansserif", Font.PLAIN, 14));
              FACG.setName("FACG");
              pnlOptions1.add(FACG, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
            }
            pnlGauche4.add(pnlOptions1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlOptions.add(pnlGauche4);
          
          // ======== pnlDroite4 ========
          {
            pnlDroite4.setFont(new Font("sansserif", Font.PLAIN, 14));
            pnlDroite4.setName("pnlDroite4");
            pnlDroite4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite4.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite4.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite4.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlOptions2 ========
            {
              pnlOptions2.setTitre("Type de zones personnalis\u00e9es");
              pnlOptions2.setFont(new Font("sansserif", Font.PLAIN, 14));
              pnlOptions2.setName("pnlOptions2");
              pnlOptions2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOptions2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlOptions2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOptions2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOptions2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbTypeZpArticle ----
              lbTypeZpArticle.setText("Pour fiche article");
              lbTypeZpArticle.setName("lbTypeZpArticle");
              pnlOptions2.add(lbTypeZpArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FATEX ----
              FATEX.setComponentPopupMenu(null);
              FATEX.setFont(new Font("sansserif", Font.PLAIN, 14));
              FATEX.setName("FATEX");
              pnlOptions2.add(FATEX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeZpMateriel ----
              lbTypeZpMateriel.setText("Pour fiche mat\u00e9riel");
              lbTypeZpMateriel.setName("lbTypeZpMateriel");
              pnlOptions2.add(lbTypeZpMateriel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FATEM ----
              FATEM.setComponentPopupMenu(null);
              FATEM.setFont(new Font("sansserif", Font.PLAIN, 14));
              FATEM.setName("FATEM");
              pnlOptions2.add(FATEM, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlDroite4.add(pnlOptions2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlOptions.add(pnlDroite4);
        }
        tpnFamille.addTab("Options", pnlOptions);
        
        // ======== pnlOptions3 ========
        {
          pnlOptions3.setOpaque(false);
          pnlOptions3.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlOptions3.setName("pnlOptions3");
          pnlOptions3.setLayout(new GridLayout(1, 2));
          
          // ======== pnlGauche5 ========
          {
            pnlGauche5.setName("pnlGauche5");
            pnlGauche5.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlGauche5.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlGauche5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlGauche5.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlGauche5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlEditions1 ========
            {
              pnlEditions1.setTitre("Ne pas \u00e9diter le d\u00e9tail des lots");
              pnlEditions1.setName("pnlEditions1");
              pnlEditions1.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEditions1.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlEditions1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlEditions1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEditions1.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- FALOTP ----
              FALOTP.setText("sur bon de pr\u00e9paration");
              FALOTP.setComponentPopupMenu(null);
              FALOTP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FALOTP.setFont(new Font("sansserif", Font.PLAIN, 14));
              FALOTP.setPreferredSize(new Dimension(200, 30));
              FALOTP.setMinimumSize(new Dimension(200, 30));
              FALOTP.setName("FALOTP");
              pnlEditions1.add(FALOTP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FALOTH ----
              FALOTH.setText("sur bon de commande");
              FALOTH.setComponentPopupMenu(null);
              FALOTH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FALOTH.setFont(new Font("sansserif", Font.PLAIN, 14));
              FALOTH.setPreferredSize(new Dimension(200, 30));
              FALOTH.setMinimumSize(new Dimension(200, 30));
              FALOTH.setName("FALOTH");
              pnlEditions1.add(FALOTH, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FALOTE ----
              FALOTE.setText("sur bon d'exp\u00e9dition");
              FALOTE.setComponentPopupMenu(null);
              FALOTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FALOTE.setFont(new Font("sansserif", Font.PLAIN, 14));
              FALOTE.setPreferredSize(new Dimension(200, 30));
              FALOTE.setMinimumSize(new Dimension(200, 30));
              FALOTE.setName("FALOTE");
              pnlEditions1.add(FALOTE, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FALOTF ----
              FALOTF.setText("sur facture");
              FALOTF.setComponentPopupMenu(null);
              FALOTF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FALOTF.setFont(new Font("sansserif", Font.PLAIN, 14));
              FALOTF.setPreferredSize(new Dimension(200, 30));
              FALOTF.setMinimumSize(new Dimension(200, 30));
              FALOTF.setName("FALOTF");
              pnlEditions1.add(FALOTF, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlGauche5.add(pnlEditions1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlOptions3.add(pnlGauche5);
          
          // ======== pnlDroite5 ========
          {
            pnlDroite5.setName("pnlDroite5");
            pnlDroite5.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDroite5.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite5.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlDroite5.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDroite5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlEditions2 ========
            {
              pnlEditions2.setName("pnlEditions2");
              pnlEditions2.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEditions2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEditions2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlEditions2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEditions2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- FAEFR ----
              FAEFR.setText("Regroupement des lignes article par famille sur facture");
              FAEFR.setComponentPopupMenu(null);
              FAEFR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAEFR.setPreferredSize(new Dimension(319, 30));
              FAEFR.setMinimumSize(new Dimension(319, 30));
              FAEFR.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAEFR.setName("FAEFR");
              pnlEditions2.add(FAEFR, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- FAETQ ----
              FAETQ.setText("Etiquette colis non \u00e9dit\u00e9e");
              FAETQ.setComponentPopupMenu(null);
              FAETQ.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAETQ.setPreferredSize(new Dimension(319, 30));
              FAETQ.setMinimumSize(new Dimension(319, 30));
              FAETQ.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAETQ.setName("FAETQ");
              pnlEditions2.add(FAETQ, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbRemises ----
              lbRemises.setText("Edition des remises");
              lbRemises.setName("lbRemises");
              pnlEditions2.add(lbRemises, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- FAEREM ----
              FAEREM.setModel(new DefaultComboBoxModel(new String[] { "Edition \u00e9ventuelle", "Pas d'\u00e9dition" }));
              FAEREM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FAEREM.setFont(new Font("sansserif", Font.PLAIN, 14));
              FAEREM.setName("FAEREM");
              pnlEditions2.add(FAEREM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlDroite5.add(pnlEditions2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlOptions3.add(pnlDroite5);
        }
        tpnFamille.addTab("Editions", pnlOptions3);
      }
      pnlContenu.add(tpnFamille,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbCodeCategorie;
  private XRiTextField INDIND;
  private SNLabelChamp lbLibelle;
  private XRiTextField FALIB;
  private JTabbedPane tpnFamille;
  private SNPanel pnlDescription;
  private SNPanelContenu pnlGauche;
  private SNPanelTitre pnlInfosRepercutees;
  private SNLabelChamp lbTypeFicheArticle;
  private XRiTextField FAIMG;
  private SNLabelChamp lbCodeUnite;
  private XRiTextField FAUNV;
  private SNLabelChamp lbCodeUniteStock;
  private XRiTextField FAUNS;
  private SNLabelChamp lbReferenceTarif;
  private XRiTextField FARTAR;
  private SNLabelChamp lbCoefVenteT1;
  private XRiTextField FACMA;
  private SNLabelChamp lbCoeffPRV;
  private XRiTextField FACPR;
  private SNLabelChamp lbCodeTVA;
  private XRiTextField FATVA;
  private SNLabelChamp lbExclusionFrais;
  private XRiComboBox FAEXF;
  private SNLabelChamp lbCodeCIP;
  private XRiTextField FACIP;
  private SNLabelChamp lbZoneTarif;
  private XRiTextField FACTA;
  private SNLabelChamp lbRattachementPIC;
  private XRiTextField FAPIC;
  private SNLabelChamp lbTraitementSpecifique;
  private XRiComboBox FAGSP;
  private SNPanelContenu pnlDroite;
  private SNPanelTitre pnlCaracteristiquesImposees;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbPourcentageMiniMarge;
  private XRiTextField FAMAR;
  private SNLabelChamp lbArrondi;
  private XRiComboBox FARON;
  private XRiCheckBox FAVOD;
  private XRiCheckBox FAPDG;
  private XRiCheckBox FAPHO;
  private XRiCheckBox FADPOS;
  private XRiCheckBox FAWEB;
  private XRiCheckBox FASPE;
  private SNLabelChamp lbRegroupement;
  private XRiTextField FAFAR;
  private SNLabelChamp lbCalculPRV;
  private XRiTextField FAPR;
  private SNLabelChamp lbTaxeParafiscale;
  private XRiTextField FATPF;
  private SNLabelChamp lbTypeGratuit;
  private SNTypeGratuit snTypeGratuit;
  private SNPanel pnlStocks;
  private SNPanelContenu pnlGauche2;
  private SNPanelTitre pnlInfosRepercutees2;
  private SNLabelChamp lbPeriodeAnalyse;
  private XRiTextField FAPER;
  private SNLabelUnite lbSemaines;
  private SNLabelChamp lbGestionnaire;
  private XRiTextField FAGP;
  private SNLabelChamp lbGestionnaire2;
  private XRiTextField FADEL;
  private SNLabelTitre sNLabelTitre1;
  private SNLabelChamp lbClasseA;
  private XRiTextField F3NV1;
  private SNLabelChamp lbClasseB;
  private XRiTextField F3NV2;
  private SNLabelChamp lbClasseC;
  private XRiTextField F3NV3;
  private SNLabelChamp lbClasseD;
  private XRiTextField F3NV4;
  private SNPanel sNPanel1;
  private SNLabelChamp lbNombreInventaires;
  private XRiTextField FAJIT;
  private SNLabelChamp lbDepreciationStock;
  private XRiTextField FADPS;
  private SNPanelContenu pnlDroite2;
  private SNPanelTitre pnlDelaiUO;
  private SNLabelChamp lbTypeDelai;
  private XRiComboBox FATDUO;
  private SNLabelChamp lbDelaiStandard;
  private XRiTextField FADUO1;
  private SNLabelChamp lbDelaiReception;
  private XRiTextField FADUO2;
  private SNLabelChamp lbDelaiVente;
  private XRiTextField FADUO3;
  private SNPanel pnlCompta;
  private SNPanelContenu pnlGauche3;
  private SNPanelTitre pnlInfosRepercutees3;
  private SNLabelChamp lbCO1;
  private XRiTextField FAC01;
  private RiZoneSortie LCG01;
  private SNLabelChamp lbCO2;
  private XRiTextField FAC02;
  private RiZoneSortie LCG02;
  private SNLabelChamp lbCO3;
  private XRiTextField FAC03;
  private RiZoneSortie LCG03;
  private SNLabelChamp lbCO4;
  private XRiTextField FAC04;
  private RiZoneSortie LCG04;
  private SNLabelChamp lbCO5;
  private XRiTextField FAC05;
  private RiZoneSortie LCG05;
  private SNLabelChamp lbCO6;
  private XRiTextField FAC06;
  private RiZoneSortie LCG06;
  private SNLabelChamp lbCO7;
  private XRiTextField FAC07;
  private RiZoneSortie LCG07;
  private SNLabelChamp lbCO8;
  private XRiTextField FAC08;
  private RiZoneSortie LCG08;
  private SNLabelChamp lbCO9;
  private XRiTextField FAC09;
  private RiZoneSortie LCG09;
  private SNPanelContenu pnlDroite3;
  private SNPanelTitre pnlDelaiUO2;
  private SNLabelChamp lbCodeAnalytique;
  private XRiTextField FASAN;
  private SNLabelChamp lbPointeur;
  private XRiComboBox FANSA;
  private SNPanel sNPanel2;
  private JLabel OBJ_114;
  private JLabel OBJ_115;
  private JLabel OBJ_116;
  private JLabel OBJ_117;
  private JLabel OBJ_118;
  private JLabel OBJ_119;
  private JLabel OBJ_120;
  private JLabel OBJ_121;
  private JLabel OBJ_122;
  private XRiTextField SAN1;
  private XRiTextField SAN2;
  private XRiTextField SAN3;
  private XRiTextField SAN4;
  private XRiTextField SAN5;
  private XRiTextField SAN6;
  private XRiTextField SAN7;
  private XRiTextField SAN8;
  private XRiTextField SAN9;
  private SNPanel pnlOptions;
  private SNPanelContenu pnlGauche4;
  private SNPanelTitre pnlOptions1;
  private SNLabelChamp lbSystemeVariables;
  private XRiTextField FAVA1;
  private JLabel OBJ_176;
  private XRiTextField FAVA2;
  private SNLabelChamp lbLibelleSystemeVariables;
  private XRiTextField FASVL;
  private SNLabelChamp lbConditionQuantitative;
  private XRiComboBox FACQQ;
  private SNLabelChamp lbArticlesEquivalents;
  private XRiComboBox FAEQI;
  private XRiCheckBox FADEB;
  private XRiCheckBox FAADS;
  private SNLabelChamp lbGarantie;
  private XRiTextField FACG;
  private SNPanelContenu pnlDroite4;
  private SNPanelTitre pnlOptions2;
  private SNLabelChamp lbTypeZpArticle;
  private XRiTextField FATEX;
  private SNLabelChamp lbTypeZpMateriel;
  private XRiTextField FATEM;
  private SNPanel pnlOptions3;
  private SNPanelContenu pnlGauche5;
  private SNPanelTitre pnlEditions1;
  private XRiCheckBox FALOTP;
  private XRiCheckBox FALOTH;
  private XRiCheckBox FALOTE;
  private XRiCheckBox FALOTF;
  private SNPanelContenu pnlDroite5;
  private SNPanelTitre pnlEditions2;
  private XRiCheckBox FAEFR;
  private XRiCheckBox FAETQ;
  private SNLabelChamp lbRemises;
  private XRiComboBox FAEREM;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
