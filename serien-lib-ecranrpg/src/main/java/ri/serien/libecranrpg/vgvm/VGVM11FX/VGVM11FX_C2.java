
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.EnumTypePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.DialogueDetailPrixVente;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiSpinner;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_C2 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] WSPR_Value = { "", "R", "D", "N", "P", "C", "F", "A" };
  private String[] L1IN18_Value = { "1", "0", "2", "3" };
  private String[] UTAR_Value = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  private String[] L1IN19_Value = new String[] { "", "1", "3" };
  private String[] L1IN19_Titre = new String[] { "CNV/Qté si existe", "pas de CNV/Qté", "CNV/Qté forcée" };
  private String[] L1TVA_Value = new String[4];
  private String[] L1TVA_Text = new String[4];
  private String[] dimensions_Value = { "", "1", "2", "3" };
  private String[] dimensions_Libelle = { "m", "m", "cm", "mm" };
  private String[] L1IN20_Value = { "", "1" };
  
  private ODialog dialog_MARGES = null;
  private ODialog dialog_DIM = null;
  private ODialog dialog_GA = null;
  private int etatMarge;
  
  public boolean isChantier;
  public boolean isSec55;
  
  private DefaultComboBoxModel model = new DefaultComboBoxModel();
  
  /**
   * Classe interne.
   * Objet TVA.
   */
  class tva {
    private int id;
    private String name;
    
    public tva(int id, String name) {
      this.id = id;
      this.name = name;
    }
    
    @Override
    public String toString() {
      return this.name;
    }
  }
  
  /**
   * Constructeur.
   */
  public VGVM11FX_C2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    
    initDiverses();
    WSPR.setValeurs(WSPR_Value, null);
    L1IN18.setValeurs(L1IN18_Value, null);
    L1IN19.setValeurs(L1IN19_Value, L1IN19_Titre);
    L1IN20.setValeurs(L1IN20_Value, null);
    L1IN1.setValeursSelection("1", "");
    L1TPF.setValeursSelection("1", "0");
    L1IN12.setValeursSelection("1", " ");
    L1TNC.setValeursSelection("1", " ");
    WSER.setValeursSelection("L", " ");
    L1TVA.setModel(model);
    A1IN28.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN29.setValeurs(dimensions_Value, dimensions_Libelle);
    A1IN30.setValeurs(dimensions_Value, dimensions_Libelle);
    
    L1TE1.setValeursSelection("X", "");
    L1TE2.setValeursSelection("X", "");
    L1TE3.setValeursSelection("X", "");
    L1TE4.setValeursSelection("X", "");
    UTAR.setValeurs(UTAR_Value);
    
    setCloseKey("ENTER", "F12", "F21");
    
    // Titre de la boite de dialogue
    setTitle("Ligne de saisie");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/fonctions.png", true));
    bt_ecran.setIcon(lexique.chargerImage("images/reduire.png", true));
    
    OBJ_95.setIcon(lexique.chargerImage("images/plus_petit.png", true));
    OBJ_94.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    OBJ_97.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    OBJ_99.setIcon(lexique.chargerImage("images/moins_petit.png", true));
    
    OBJ_96.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    OBJ_98.setIcon(lexique.chargerImage("images/egal_petit.png", true));
    OBJ_101.setIcon(lexique.chargerImage("images/egal_petit.png", true));
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes surchargées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_153.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCPL@")).trim());
    OBJ_237.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LSUBST@")).trim());
    L1QTCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1QTCX@")).trim());
    L1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1UNV@")).trim());
    OBJ_130.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNVLIB@")).trim());
    A1UNL.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    A1UNL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNL@")).trim());
    A1UNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNV@")).trim());
    A1KCS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1KCS@")).trim());
    A1UCS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UCS@")).trim());
    WPVCX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPVCX@")).trim());
    T1CNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1CNV@")).trim());
    T1TRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1TRA@")).trim());
    WCCD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCCD@")).trim());
    T1RATR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@T1RATR@")).trim());
    WTCD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTCD@")).trim());
    cnvchantier.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    OBJ_193.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL1@")).trim());
    OBJ_195.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL2@")).trim());
    OBJ_197.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL3@")).trim());
    OBJ_199.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL4@")).trim());
    OBJ_201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIZPL5@")).trim());
    WDATTX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATTX@")).trim());
    WDELX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDELX@")).trim());
    bt_GA
        .setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("<HTML>Aucune génération<BR>@&L000999@</HTML>")).trim());
    label4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPS@")).trim());
    WMMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMR@")).trim());
    WMMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMMT@")).trim());
    WMAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WMAC@")).trim());
    WPMR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMR@")).trim());
    WPMT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPMT@")).trim());
    WPAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPAC@")).trim());
    WTPRL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPRL@")).trim());
    L1MHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1MHT@")).trim());
    WTPVL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPVL@")).trim());
    WDISX_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WSTKX_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WRESX_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WAFFX_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    WATTX_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WDISX_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WSTKX_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTKX@")).trim());
    WATTX_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WAFFX_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    WCNRX_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCNRX@")).trim());
    WSTTX_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSTTX@")).trim());
    WDISX_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDISX@")).trim());
    WRESX_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRESX@")).trim());
    WATTX_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATTX@")).trim());
    WAFFX_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WAFFX@")).trim());
    label3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYPS@")).trim());
    OBJ_192.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AVOIR@")).trim());
    OBJ_169.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VALE@")).trim());
    WCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCOD@")).trim());
    WNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNLI@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLCA@")).trim());
    WARTT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTT@")).trim());
    etat.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Commercialisé le @A1DT1X@")).trim());
    OBJ_236.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@STOC@")).trim());
    L1TVA0.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLT1@")).trim());
    L1TVA_1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLT2@")).trim());
    L1TVA_2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLT3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    gererAffichageMenus(riMenu_bt1);
    gererLesErreurs("19");
    
    // Ici source d'amélioration si les données non pas changé ne pas regénérer le contenu
    model.removeAllElements();
    for (int i = 1; i < L1TVA_Text.length; i++) {
      L1TVA_Value[i] = lexique.HostFieldGetData("CTVA" + i);
      L1TVA_Text[i] = lexique.HostFieldGetData("WLT" + i);
      if (!L1TVA_Value[i].isEmpty() && !L1TVA_Text[i].isEmpty() && L1TVA_Text[i] != null) {
        model.addElement(new tva(Integer.parseInt(L1TVA_Value[i]), L1TVA_Text[i]));
      }
    }
    if (L1TVA.getItemCount() > 0) {
      for (int i = 1; i < L1TVA_Text.length; i++) {
        if (lexique.HostFieldGetData("L1TVA").equals(lexique.HostFieldGetData("CTVA" + i))) {
          L1TVA.setSelectedIndex(i - 1);
        }
      }
    }
    
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    isSec55 = lexique.HostFieldGetData("SEC055").trim().equals("0");
    
    riSousMenu3.setEnabled(lexique.isTrue("82"));
    
    OBJ_129.setVisible(lexique.isTrue("(15) AND (N10)"));
    OBJ_132.setVisible(OBJ_129.isVisible());
    
    OBJ_201.setVisible(lexique.isPresent("TIZPL5"));
    OBJ_199.setVisible(lexique.isPresent("TIZPL4"));
    OBJ_197.setVisible(lexique.isPresent("TIZPL3"));
    OBJ_195.setVisible(lexique.isPresent("TIZPL2"));
    OBJ_193.setVisible(lexique.isPresent("TIZPL1"));
    OBJ_176.setEnabled(lexique.isPresent("WREM1"));
    L1TE4.setVisible(false);
    
    boolean isLibelleNombrePiece = lexique.HostFieldGetData("LIBCPL").trim().equalsIgnoreCase("Nbre Pièces  ->");
    L20NAT.setVisible(!isLibelleNombrePiece && lexique.isPresent("L20NAT"));
    
    if (!lexique.HostFieldGetData("A1IN2").trim().isEmpty()) {
      WSER.setText("Lots");
      WSER.setValeursSelection("L", " ");
      WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("L"));
    }
    else if (lexique.HostFieldGetData("A1TSP").trim().equalsIgnoreCase("S")) {
      WSER.setText("Série");
      WSER.setValeursSelection("S", " ");
      WSER.setSelected(lexique.HostFieldGetData("WSER").equalsIgnoreCase("S"));
    }
    else {
      WSER.setVisible(false);
    }
    
    OBJ_136.setVisible(lexique.isPresent("L1CPL"));
    OBJ_151.setVisible(lexique.isPresent("L20NAT"));
    OBJ_236.setVisible(lexique.isPresent("WASB"));
    OBJ_137.setVisible(!isLibelleNombrePiece && lexique.HostFieldGetData("A1UCS").trim().isEmpty());
    
    String wnpu = lexique.HostFieldGetData("WNPU").trim();
    String promo = lexique.HostFieldGetData("PROMO").trim();
    
    CPL_doublon_125.setVisible(!isLibelleNombrePiece && lexique.isPresent("CPL") && lexique.isTrue("40"));
    OBJ_140.setVisible(lexique.isPresent("LPPDBR"));
    // C'est quoi cette condition ???
    OBJ_150.setVisible(!promo.equalsIgnoreCase("P") && promo.equalsIgnoreCase("P"));
    OBJ_208.setVisible(wnpu.equals("2"));
    OBJ_115.setVisible(lexique.isPresent("WLCA"));
    if (wnpu.equalsIgnoreCase("D")) {
      etat.setText("Déprécié");
    }
    if (wnpu.equals("6")) {
      etat.setText("Fin de série");
    }
    else if (lexique.HostFieldGetData("A1DT1X").trim().isEmpty()) {
      etat.setText("Commercialisé");
    }
    
    OBJ_227.setVisible(lexique.isPresent("WPVCX"));
    OBJ_130.setVisible(lexique.isPresent("UNVLIB"));
    
    String l1in18 = lexique.HostFieldGetData("L1IN18").trim();
    L1IN18.setVisible(!l1in18.equalsIgnoreCase("D") && !l1in18.equalsIgnoreCase("A"));
    OBJ_230.setVisible(l1in18.equalsIgnoreCase("A"));
    OBJ_229.setVisible(l1in18.equalsIgnoreCase("D"));
    OBJ_153.setVisible(lexique.isPresent("LIBCPL") && CPL_doublon_125.isVisible());
    panel1.setEnabled(!lexique.HostFieldGetData("T1CNV").trim().isEmpty());
    if (promo.equalsIgnoreCase("P") && panel1.isVisible()) {
      OBJ_160.setText("Promotion appliquée");
    }
    else {
      OBJ_160.setText("Condition de vente");
    }
    if (!lexique.HostFieldGetData("T1CNV").trim().isEmpty()) {
      OBJ_160.setForeground(Color.BLUE);
    }
    else {
      OBJ_160.setForeground(Color.BLACK);
    }
    
    L1CPL.setVisible(lexique.isTrue("40"));
    
    // Bouton prix flash
    flash.setVisible(!isChantier);
    if (flash.isVisible()) {
      if (!lexique.HostFieldGetData("WPVF").trim().isEmpty()) {
        flash.setForeground(Color.BLUE);
      }
      else {
        flash.setForeground(Color.BLACK);
      }
      String heure = "";
      String wuheu = lexique.HostFieldGetData("WUHEU");
      if (wuheu.length() > 0) {
        if (wuheu.length() == 4) {
          heure = " à " + lexique.HostFieldGetNumericString(wuheu, 0).substring(0, 2) + " heure "
              + lexique.HostFieldGetNumericString(wuheu, 0).substring(2, 4);
        }
        else {
          heure = " à 0" + lexique.HostFieldGetNumericString(wuheu, 0).substring(0, 1) + " heure "
              + lexique.HostFieldGetNumericString(wuheu, 0).substring(1, 3);
        }
      }
      String wpvf = lexique.HostFieldGetData("WPVF").trim();
      if (!wpvf.isEmpty()) {
        flash.setToolTipText(
            "Prix négocié à " + wpvf + " par " + lexique.HostFieldGetData("WUSER") + " le " + lexique.HostFieldGetData("WUDAT") + heure);
      }
      else {
        flash.setToolTipText("Pas de prix flash pour cet article");
      }
    }
    
    boolean in88 = lexique.isTrue("88");
    OBJ_184.setVisible(in88);
    L1REP.setVisible(in88);
    
    OBJ_192.setVisible(!OBJ_192.getText().trim().isEmpty());
    OBJ_169.setVisible(!OBJ_169.getText().trim().isEmpty());
    
    // Zones personnalisées
    L1TP1.setVisible(!lexique.HostFieldGetData("TIZPL1").trim().isEmpty());
    L1TP2.setVisible(!lexique.HostFieldGetData("TIZPL2").trim().isEmpty());
    L1TP3.setVisible(!lexique.HostFieldGetData("TIZPL3").trim().isEmpty());
    L1TP4.setVisible(!lexique.HostFieldGetData("TIZPL4").trim().isEmpty());
    L1TP5.setVisible(!lexique.HostFieldGetData("TIZPL5").trim().isEmpty());
    
    // Panels de stocks s1 à s7
    String typs = lexique.HostFieldGetData("TYPS").trim();
    p_S1.setVisible(typs.equalsIgnoreCase("S1"));
    p_S5.setVisible(typs.equalsIgnoreCase("S5"));
    p_S7.setVisible(typs.equalsIgnoreCase("S7"));
    p_stock.setVisible(!typs.isEmpty());
    
    // Mettre à jour les bons textes du bouton génération d'achat et de son label de livraison
    String texteBouton = "pas de génération";
    String texteLabel = "";
    
    String l1gba = lexique.HostFieldGetData("L1GBA").trim();
    if (!l1gba.isEmpty()) {
      int valeurN = Integer.parseInt(l1gba);
      switch (valeurN) {
        case 1:
          texteBouton = "Différée";
          break;
        case 2:
          texteBouton = "Différée";
          texteLabel = "Livraison directe assurée";
          break;
        case 3:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          break;
        case 4:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          break;
        case 5:
          texteBouton = "Immédiate";
          break;
        case 6:
          texteBouton = "Immédiate";
          texteLabel = "Livraison directe assurée";
          break;
        case 7:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          break;
        case 8:
          texteBouton = "Bon d'achat généré";
          texteLabel = "";
          break;
        default:
          texteBouton = "Aucune";
          break;
      }
    }
    bt_GA.setText(texteBouton);
    label11.setText(texteLabel);
    
    // Gestion des modifications de libellé
    riSousMenu7.setEnabled(!lexique.isTrue("16"));
    if (lexique.isTrue("25")) {
      riSousMenu_bt7.setText("Libellé protégé");
    }
    else {
      riSousMenu_bt7.setText("Libellé saisissable");
    }
    
    // Panels dimensions
    plongueur.setVisible(lexique.isTrue("(82) AND (84) AND (77)"));
    psurface.setVisible(lexique.isTrue("(82) AND (N84) AND (77)"));
    pvolume.setVisible(lexique.isTrue("(82) AND (N84) AND (N77)"));
    
    String l2qt1x = lexique.HostFieldGetData("L2QT1X");
    String l2qt2x = lexique.HostFieldGetData("L2QT2X");
    longueur1.setText(l2qt1x);
    surface1.setText(l2qt1x);
    surface2.setText(l2qt2x);
    volume1.setText(l2qt1x);
    volume2.setText(l2qt2x);
    volume3.setText(lexique.HostFieldGetData("L2QT3X"));
    
    OBJ_141.setVisible(lexique.isPresent("L2NBRX"));
    l_x3.setVisible(lexique.isPresent("L2NBRX"));
    
    if (!lexique.isTrue("82") && lexique.isPresent("L2NBRX")) {
      panelDimension.setVisible(true);
      panel4.setBounds(350, 25, panel4.getWidth(), panel4.getHeight());
      panel5.setBounds(350, 115, panel5.getWidth(), panel5.getHeight());
    }
    else {
      panelDimension.setVisible(false);
      panel4.setBounds(350, 15, panel4.getWidth(), panel4.getHeight());
      panel5.setBounds(350, 160, panel5.getWidth(), panel5.getHeight());
    }
    
    String typm = lexique.HostFieldGetData("TYPM");
    p_stock.setVisible(!isChantier && !typm.startsWith("M"));
    p_marge.setVisible((isChantier || typm.startsWith("M")) && isSec55);
    riSousMenu15.setEnabled(!isChantier);
    
    if (!p_stock.isVisible() && !p_marge.isVisible()) {
      this.setPreferredSize(new Dimension(1090, 540));
    }
    else {
      this.setPreferredSize(new Dimension(1090, 655));
    }
    
    // Saisie palette
    String a1ucs = lexique.HostFieldGetData("A1UCS").trim();
    if (!a1ucs.isEmpty()) {
      OBJ_138.setVisible(true);
      A1UCS.setVisible(true);
      OBJ_131.setVisible(true);
      A1KCS.setVisible(true);
      LPNBC.setVisible(true);
      OBJ_137.setVisible(true);
    }
    else {
      OBJ_138.setVisible(false);
      A1UCS.setVisible(false);
      OBJ_131.setVisible(false);
      A1KCS.setVisible(false);
      LPNBC.setVisible(false);
      OBJ_137.setVisible(false);
    }
    A1UCS.setSelected(false);
    
    // Marges
    etatMarge = 0;
    String chaine = lexique.HostFieldGetData("MARGU").trim();
    if (chaine.length() > 0) {
      etatMarge = Integer.parseInt(chaine);
    }
    afficherLesMarges();
    
    // Chantier
    L1IN20.setVisible(isChantier);
    cnvchantier.setVisible(isChantier);
    String l1in20 = lexique.HostFieldGetData("L1IN20").trim();
    if (isChantier && l1in20.equals("1")) {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    else {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (lexique.isTrue("(82) AND (84) AND (77)")) {
      lexique.HostFieldPutData("L2QT1X", 1, longueur1.getText());
    }
    if (lexique.isTrue("(82) AND (N84) AND (77)")) {
      lexique.HostFieldPutData("L2QT1X", 1, surface1.getText());
      lexique.HostFieldPutData("L2QT2X", 1, surface2.getText());
    }
    if (lexique.isTrue("(82) AND (N84) AND (N77)")) {
      lexique.HostFieldPutData("L2QT1X", 1, volume1.getText());
      lexique.HostFieldPutData("L2QT2X", 1, volume2.getText());
      lexique.HostFieldPutData("L2QT3X", 1, volume3.getText());
    }
    
    lexique.HostFieldPutData("L1TVA", 0, L1TVA_Value[L1TVA.getSelectedIndex() + 1]);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Afficher la boîte de dialogue de détail du calcul du montant.
   */
  private void afficherDetailCalculMontant(EnumTypePrixVente pTypePrixVente) {
    ParametreChargement parametreChargement = new ParametreChargement();
    
    // Récupérer l'identifiant du document de vente
    IdDocumentVente idDocumentVente = (IdDocumentVente) lexique.getValeurVariableGlobaleFromSession("idDocumentVente");
    parametreChargement.setIdDocumentVente(idDocumentVente);
    if (idDocumentVente == null) {
      throw new MessageErreurException("L'identifiant du document de vente est invalide");
    }
    
    // Récupérer l'identifiant de la ligne de vente
    int numeroLigne = Constantes.convertirTexteEnInteger(WNLI.getText());
    IdLigneVente idLigneVente = IdLigneVente.getInstance(idDocumentVente.getIdEtablissement(), idDocumentVente.getCodeEntete(),
        idDocumentVente.getNumero(), idDocumentVente.getSuffixe(), numeroLigne);
    parametreChargement.setIdLigneDocumentVente(idLigneVente);
    
    // Récupérer l'identifiant du client (inutile car il est stocké dans l'entête du document)
    // IdClient idClient = (IdClient) lexique.getValeurVariableGlobaleFromSession("idClient");
    // parametreChargement.setIdClientFacture(idClient);
    
    // Récupérer l'identifiant du magasin
    // IdMagasin idMagasin = (IdMagasin) lexique.getValeurVariableGlobaleFromSession("idMagasin");
    if (!Constantes.normerTexte(L1MAG.getText()).isEmpty()) {
      IdMagasin idMagasin = IdMagasin.getInstance(idDocumentVente.getIdEtablissement(), L1MAG.getText());
      parametreChargement.setIdMagasin(idMagasin);
    }
    
    // Renseigner l'identifiant de l'article
    if (!Constantes.normerTexte(WARTT.getText()).isEmpty()) {
      IdArticle idArticle = IdArticle.getInstance(idDocumentVente.getIdEtablissement(), WARTT.getText());
      parametreChargement.setIdEtablissement(idArticle.getIdEtablissement());
      parametreChargement.setIdArticle(idArticle);
    }
    
    // Afficher la boite de dialogue du détail du calcul d'un prix de vente
    ModeleDialogueDetailPrixVente modele = new ModeleDialogueDetailPrixVente(getSession(), parametreChargement, pTypePrixVente);
    DialogueDetailPrixVente vue = new DialogueDetailPrixVente(modele);
    vue.afficher();
  }
  
  public void fonctionF23() {
    Robot robot;
    try {
      this.requestFocusInWindow();
      robot = new Robot();
      robot.keyPress(KeyEvent.VK_F23);
      robot.keyPress(KeyEvent.VK_F23);
    }
    catch (AWTException e) {
      e.printStackTrace();
    }
  }
  
  private void afficherLesMarges() {
    if (etatMarge > 0) {
      if (dialog_MARGES == null) {
        dialog_MARGES = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_MARGES(this));
      }
      dialog_MARGES.affichePopupPerso();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // if Trim(@"@UNV@") = "KG" then
    // ScriptCall("G_BALANC")
    // PanelFieldPutData("QTEX", 0, poids)
    // end if
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    WSER.setText("L");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    WSPR.setSelectedIndex(1);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    WSPR.setSelectedIndex(0);
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    WSPR.setSelectedIndex(4);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("BTAR", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(15, 31);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 33);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(22, 80);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_GAActionPerformed(ActionEvent e) {
    if (dialog_GA == null) {
      dialog_GA = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_GA(this));
    }
    dialog_GA.affichePopupPerso();
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 01);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_178ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 40);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_176ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 55);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // ScriptCall("G_CONVER")
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    
    lexique.WatchHelp(FCT1.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    
    lexique.HostCursorPut(FCT1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("BCNQ", 0, "2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 40);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    if (dialog_DIM == null) {
      dialog_DIM = new ODialog((JFrame) getTopLevelAncestor(), new VGVM11FX_DIM(this));
    }
    dialog_DIM.affichePopupPerso();
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("L1GBA");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 45);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    // WSPR.setText("N");
    WSPR.setSelectedIndex(3);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt221ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("L1IN18", 1, "A");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("COLPAL", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    String folder =
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim();
    File directory = new File(folder);
    try {
      if (directory.exists()) {
        Desktop.getDesktop().open(directory);
      }
      else {
        JOptionPane.showMessageDialog(null, "Le dossier spécifié pour les documents liés n'existe pas");
      }
    }
    catch (IOException e1) {
      JOptionPane.showMessageDialog(null, e1.toString());
    }
  }
  
  private void flashActionPerformed(ActionEvent e) {
    WSPR.setSelectedIndex(6);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void appliquerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("L1PVNX", 0, lexique.HostFieldGetData("WPVF"));
    L1PVNX.setText(lexique.HostFieldGetData("WPVF"));
  }
  
  private void negoActionPerformed(ActionEvent e) {
    WSPR.setSelectedIndex(3);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_177ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("BTAR", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void volume1FocusGained(FocusEvent e) {
    volume1.selectAll();
  }
  
  private void volume2FocusGained(FocusEvent e) {
    volume2.selectAll();
  }
  
  private void volume3FocusGained(FocusEvent e) {
    volume3.selectAll();
  }
  
  private void surface1FocusGained(FocusEvent e) {
    surface1.selectAll();
  }
  
  private void surface2FocusGained(FocusEvent e) {
    surface2.selectAll();
  }
  
  private void longueur1FocusGained(FocusEvent e) {
    longueur1.selectAll();
  }
  
  private void bt_ecranMouseClicked(MouseEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riBoutonDetail3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 1);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void A1UCSActionPerformed(ActionEvent e) {
    if (A1UCS.isSelected()) {
      L1QTEX.setEnabled(false);
      LPNBC.setEnabled(true);
      lexique.HostFieldPutData("TQCS", 0, "1");
    }
    else {
      L1QTEX.setEnabled(true);
      LPNBC.setEnabled(false);
      lexique.HostFieldPutData("TQCS", 0, "");
    }
    
  }
  
  private void L1IN20ItemStateChanged(ItemEvent e) {
    if (isChantier && L1IN20.getSelectedIndex() == 1) {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    
    else {
      cnvchantier.setText(lexique.HostFieldGetData("WARTT"));
    }
    cnvchantier.repaint();
  }
  
  private void riSousMenu_bt25ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 59);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt26ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18");
  }
  
  /**
   * Afficher la boîte de dialogue de détail du calcul d'un prix de vente.
   * @param e Evènement graphique.
   */
  private void DialogueCalculPrixVenteMouseClicked(MouseEvent e) {
    try {
      // Pour que l'action soit exécutée il faut un CTRL + clic souris dans le composant
      if (!e.isControlDown()) {
        return;
      }
      
      // Afficher la boîte de dialogue
      afficherDetailCalculMontant(null);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    OBJ_64 = new JButton();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu23 = new RiSousMenu();
    riSousMenu_bt23 = new RiSousMenu_bt();
    riSousMenu26 = new RiSousMenu();
    riSousMenu_bt26 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu25 = new RiSousMenu();
    riSousMenu_bt25 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel7 = new JPanel();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    L1LIB3 = new XRiTextField();
    L1LIB4 = new XRiTextField();
    WASB = new XRiTextField();
    OBJ_153 = new JLabel();
    CPL_doublon_125 = new XRiTextField();
    OBJ_237 = new JLabel();
    panel4 = new JPanel();
    L1CNDX = new XRiTextField();
    L1QTEX = new XRiTextField();
    L1QTCX = new RiZoneSortie();
    L1UNV = new XRiTextField();
    OBJ_129 = new JLabel();
    OBJ_132 = new JLabel();
    LPPDBR = new XRiTextField();
    OBJ_137 = new JLabel();
    OBJ_136 = new JLabel();
    OBJ_140 = new JLabel();
    label14 = new JLabel();
    WSER = new XRiCheckBox();
    OBJ_130 = new RiZoneSortie();
    A1UNL = new XRiTextField();
    A1UNV = new XRiTextField();
    OBJ_138 = new JLabel();
    OBJ_131 = new JLabel();
    A1KCS = new RiZoneSortie();
    A1UCS = new JToggleButton();
    LPNBC = new XRiTextField();
    L1CPL = new XRiTextField();
    panel5 = new JPanel();
    L1PVBX = new XRiTextField();
    L1PVNX = new XRiTextField();
    OBJ_158 = new JLabel();
    WSPR = new XRiComboBox();
    OBJ_155 = new JLabel();
    panel9 = new JPanel();
    OBJ_227 = new JLabel();
    WPVCX = new JLabel();
    OBJ_161 = new JLabel();
    L1IN2 = new XRiTextField();
    UTAR = new XRiSpinner();
    OBJ_159 = new JLabel();
    WREM1 = new XRiTextField();
    WREM2 = new XRiTextField();
    WREM3 = new XRiTextField();
    OBJ_168 = new JLabel();
    L1COEX = new XRiTextField();
    panel1 = new JPanel();
    T1CNV = new RiZoneSortie();
    T1TRA = new RiZoneSortie();
    WCCD = new RiZoneSortie();
    T1RATR = new RiZoneSortie();
    WTCD = new RiZoneSortie();
    OBJ_178 = new SNBoutonDetail();
    OBJ_160 = new JLabel();
    OBJ_176 = new SNBoutonDetail();
    OBJ_229 = new JLabel();
    OBJ_230 = new JLabel();
    flash = new SNBoutonDetail();
    L1IN18 = new XRiComboBox();
    nego = new SNBoutonDetail();
    OBJ_177 = new SNBoutonDetail();
    L1IN19 = new XRiComboBox();
    L1IN20 = new XRiComboBox();
    cnvchantier = new RiZoneSortie();
    panel15 = new JPanel();
    OBJ_151 = new JLabel();
    L20NAT = new XRiTextField();
    OBJ_120 = new JLabel();
    L1IN3 = new XRiTextField();
    OBJ_193 = new JLabel();
    L1TP1 = new XRiTextField();
    OBJ_195 = new JLabel();
    L1TP2 = new XRiTextField();
    OBJ_197 = new JLabel();
    L1TP3 = new XRiTextField();
    OBJ_199 = new JLabel();
    L1TP4 = new XRiTextField();
    OBJ_201 = new JLabel();
    L1TP5 = new XRiTextField();
    OBJ_147 = new JLabel();
    L1IN11 = new XRiTextField();
    L1TNC = new XRiCheckBox();
    L1IN12 = new XRiCheckBox();
    L1TPF = new XRiCheckBox();
    L1IN1 = new XRiCheckBox();
    OBJ_174 = new JLabel();
    OBJ_184 = new JLabel();
    L1REP = new XRiTextField();
    L1TVA = new JComboBox();
    L1IN17 = new XRiTextField();
    OBJ_148 = new JLabel();
    L1IN21 = new XRiTextField();
    OBJ_121 = new JLabel();
    panel6 = new JPanel();
    WDATTX = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_109 = new JLabel();
    WDELX = new RiZoneSortie();
    L1DLPX = new XRiCalendrier();
    WDLSX = new XRiCalendrier();
    bt_GA = new SNBoutonLeger();
    label12 = new JLabel();
    label13 = new JLabel();
    label11 = new JLabel();
    label2 = new JLabel();
    panelDimension = new JPanel();
    L2NBRX = new XRiTextField();
    OBJ_141 = new JLabel();
    l_x3 = new JLabel();
    psurface = new JPanel();
    label17 = new JLabel();
    surface1 = new JTextField();
    l_x4 = new JLabel();
    surface2 = new JTextField();
    plongueur = new JPanel();
    label16 = new JLabel();
    longueur1 = new JTextField();
    A1IN30 = new XRiComboBox();
    A1IN28 = new XRiComboBox();
    A1IN29 = new XRiComboBox();
    pvolume = new JPanel();
    volume1 = new JTextField();
    l_x1 = new JLabel();
    volume2 = new JTextField();
    l_x2 = new JLabel();
    volume3 = new JTextField();
    label15 = new JLabel();
    p_stock = new JPanel();
    p_S7 = new JPanel();
    disp_7 = new JLabel();
    WDISX_7 = new RiZoneSortie();
    commande_7 = new JLabel();
    WRESX_7 = new RiZoneSortie();
    att_7 = new JLabel();
    WATTX_7 = new RiZoneSortie();
    attt_7 = new JLabel();
    WAFFX_7 = new RiZoneSortie();
    label3 = new JLabel();
    riBoutonDetailListe1 = new SNBoutonDetail();
    p_S1 = new JPanel();
    disp_1 = new JLabel();
    WDISX_1 = new RiZoneSortie();
    WSTKX_1 = new RiZoneSortie();
    stock_1 = new JLabel();
    WRESX_1 = new RiZoneSortie();
    commande_8 = new JLabel();
    WAFFX_1 = new RiZoneSortie();
    OBJ_57 = new JLabel();
    WATTX_1 = new RiZoneSortie();
    att_1 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_96 = new JLabel();
    p_S5 = new JPanel();
    disp_5 = new JLabel();
    WDISX_5 = new RiZoneSortie();
    WSTKX_5 = new RiZoneSortie();
    stock_5 = new JLabel();
    WATTX_5 = new RiZoneSortie();
    att_5 = new JLabel();
    OBJ_97 = new JLabel();
    reser_5 = new JLabel();
    WAFFX_5 = new RiZoneSortie();
    OBJ_98 = new JLabel();
    OBJ_99 = new JLabel();
    WCNRX_5 = new RiZoneSortie();
    commande_5 = new JLabel();
    WSTTX_5 = new RiZoneSortie();
    OBJ_101 = new JLabel();
    OBJ_55 = new JLabel();
    panel16 = new JPanel();
    L1TE1 = new XRiCheckBox();
    L1TE2 = new XRiCheckBox();
    L1TE3 = new XRiCheckBox();
    L1TE4 = new XRiCheckBox();
    p_marge = new JPanel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    WMMR = new RiZoneSortie();
    WMMT = new RiZoneSortie();
    WMAC = new RiZoneSortie();
    label8 = new JLabel();
    label9 = new JLabel();
    label18 = new JLabel();
    WPMR = new RiZoneSortie();
    WPMT = new RiZoneSortie();
    WPAC = new RiZoneSortie();
    label19 = new JLabel();
    label20 = new JLabel();
    label21 = new JLabel();
    WTPRL = new RiZoneSortie();
    L1MHT = new RiZoneSortie();
    WTPVL = new RiZoneSortie();
    marge = new JLabel();
    barre_tete = new JMenuBar();
    panel2 = new JPanel();
    OBJ_192 = new RiZoneSortie();
    OBJ_169 = new RiZoneSortie();
    WCOD = new RiZoneSortie();
    WNLI = new RiZoneSortie();
    label10 = new JLabel();
    OBJ_115 = new JLabel();
    WARTT = new RiZoneSortie();
    OBJ_118 = new JLabel();
    L1MAG = new XRiTextField();
    label1 = new JLabel();
    etat = new JLabel();
    bt_ecran = new JLabel();
    riBoutonDetail3 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    FCT1 = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_208 = new JLabel();
    OBJ_236 = new JLabel();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    BTD2 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_150 = new JLabel();
    BTD3 = new JPopupMenu();
    appliquer = new JMenuItem();
    L1TVA0 = new XRiRadioButton();
    L1TVA_1 = new XRiRadioButton();
    L1TVA_2 = new XRiRadioButton();
    buttonGroup1 = new ButtonGroup();
    
    // ---- OBJ_64 ----
    OBJ_64.setText("");
    OBJ_64.setToolTipText("Position en Stock");
    OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_64.setName("OBJ_64");
    OBJ_64.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_64ActionPerformed(e);
      }
    });
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1090, 655));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");
              
              // ---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("Documents li\u00e9s");
              riSousMenu_bt24.setToolTipText("Gestion des documents li\u00e9s \u00e0 cette ligne de bon");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options article");
              riSousMenu_bt6.setToolTipText("Options article");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Modification libell\u00e9(s)");
              riSousMenu_bt7.setToolTipText("Modification libell\u00e9(s)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Gestion extension ligne");
              riSousMenu_bt8.setToolTipText("Gestion extension ligne");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Articles \u00e9quivalents");
              riSousMenu_bt9.setToolTipText("Articles \u00e9quivalents");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Autre bon, facture, devis");
              riSousMenu_bt11.setToolTipText("Autre bon, facture, devis");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Dimensions");
              riSousMenu_bt3.setToolTipText("Dimensions");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
            
            // ======== riSousMenu23 ========
            {
              riSousMenu23.setName("riSousMenu23");
              
              // ---- riSousMenu_bt23 ----
              riSousMenu_bt23.setText("Lien internet");
              riSousMenu_bt23.setToolTipText("Site internet li\u00e9 \u00e0 l'article");
              riSousMenu_bt23.setName("riSousMenu_bt23");
              riSousMenu_bt23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt23ActionPerformed(e);
                }
              });
              riSousMenu23.add(riSousMenu_bt23);
            }
            menus_haut.add(riSousMenu23);
            
            // ======== riSousMenu26 ========
            {
              riSousMenu26.setName("riSousMenu26");
              
              // ---- riSousMenu_bt26 ----
              riSousMenu_bt26.setText("Fiches techniques");
              riSousMenu_bt26.setToolTipText("Fiches techniques");
              riSousMenu_bt26.setName("riSousMenu_bt26");
              riSousMenu_bt26.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt26ActionPerformed(e);
                }
              });
              riSousMenu26.add(riSousMenu_bt26);
            }
            menus_haut.add(riSousMenu26);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Gestion des lots");
              riSousMenu_bt13.setToolTipText("Gestion des lots");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Prix et conditions");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Prix de revient");
              riSousMenu_bt14.setToolTipText("Prix de revient");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Affichage des marges");
              riSousMenu_bt15.setToolTipText("Affichage des marges");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");
              
              // ---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("N\u00e9gocier");
              riSousMenu_bt22.setToolTipText("N\u00e9gocier");
              riSousMenu_bt22.setActionCommand("N\u00e9gocier");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt22ActionPerformed(e);
                }
              });
              riSousMenu22.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu22);
            
            // ======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");
              
              // ---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Prix pratiqu\u00e9s / client.");
              riSousMenu_bt16.setToolTipText("Prix pratiqu\u00e9s pour le client");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Visualisation des tarifs");
              riSousMenu_bt17.setToolTipText("Visualisation des tarifs");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt17ActionPerformed(e);
                }
              });
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Condition quantitative");
              riSousMenu_bt1.setToolTipText("Condition quantitative");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Bloc notes CNV");
              riSousMenu_bt2.setToolTipText("Bloc notes conditions de vente");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu25 ========
            {
              riSousMenu25.setName("riSousMenu25");
              
              // ---- riSousMenu_bt25 ----
              riSousMenu_bt25.setText("Bloc notes CNV quanti.");
              riSousMenu_bt25.setToolTipText("Bloc notes conditions de vente quantitatives");
              riSousMenu_bt25.setName("riSousMenu_bt25");
              riSousMenu_bt25.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt25ActionPerformed(e);
                }
              });
              riSousMenu25.add(riSousMenu_bt25);
            }
            menus_haut.add(riSousMenu25);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Historique et stats");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");
              
              // ---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Historique vente article");
              riSousMenu_bt18.setToolTipText("Historique vente article");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Statistiques crois\u00e9es");
              riSousMenu_bt19.setToolTipText("Acc\u00e8s aux statistiques crois\u00e9es");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
            
            // ======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");
              
              // ---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Commissionnement");
              riSousMenu_bt20.setToolTipText("D\u00e9tail du commissionnement");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);
            
            // ======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("Stock");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);
            
            // ======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");
              
              // ---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Affectation stock/attendu");
              riSousMenu_bt4.setToolTipText("Affectation stock/attendu");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);
            
            // ======== riSousMenu5 ========
            {
              riSousMenu5.setName("riSousMenu5");
              
              // ---- riSousMenu_bt5 ----
              riSousMenu_bt5.setText("Attendu / command\u00e9");
              riSousMenu_bt5.setToolTipText("Attendu / command\u00e9");
              riSousMenu_bt5.setName("riSousMenu_bt5");
              riSousMenu_bt5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt5ActionPerformed(e);
                }
              });
              riSousMenu5.add(riSousMenu_bt5);
            }
            menus_haut.add(riSousMenu5);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel7 ========
        {
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- L1LIB1 ----
          L1LIB1.setComponentPopupMenu(BTD);
          L1LIB1.setFont(L1LIB1.getFont().deriveFont(L1LIB1.getFont().getStyle() | Font.BOLD));
          L1LIB1.setName("L1LIB1");
          panel7.add(L1LIB1);
          L1LIB1.setBounds(5, 64, 310, L1LIB1.getPreferredSize().height);
          
          // ---- L1LIB2 ----
          L1LIB2.setComponentPopupMenu(BTD);
          L1LIB2.setName("L1LIB2");
          panel7.add(L1LIB2);
          L1LIB2.setBounds(5, 91, 310, L1LIB2.getPreferredSize().height);
          
          // ---- L1LIB3 ----
          L1LIB3.setComponentPopupMenu(BTD);
          L1LIB3.setName("L1LIB3");
          panel7.add(L1LIB3);
          L1LIB3.setBounds(5, 118, 310, L1LIB3.getPreferredSize().height);
          
          // ---- L1LIB4 ----
          L1LIB4.setComponentPopupMenu(BTD);
          L1LIB4.setName("L1LIB4");
          panel7.add(L1LIB4);
          L1LIB4.setBounds(5, 145, 310, L1LIB4.getPreferredSize().height);
          
          // ---- WASB ----
          WASB.setComponentPopupMenu(BTD);
          WASB.setName("WASB");
          panel7.add(WASB);
          WASB.setBounds(80, 10, 235, WASB.getPreferredSize().height);
          
          // ---- OBJ_153 ----
          OBJ_153.setText("@LIBCPL@");
          OBJ_153.setName("OBJ_153");
          panel7.add(OBJ_153);
          OBJ_153.setBounds(5, 41, 145, 20);
          
          // ---- CPL_doublon_125 ----
          CPL_doublon_125.setComponentPopupMenu(BTD);
          CPL_doublon_125.setName("CPL_doublon_125");
          panel7.add(CPL_doublon_125);
          CPL_doublon_125.setBounds(241, 37, 74, CPL_doublon_125.getPreferredSize().height);
          
          // ---- OBJ_237 ----
          OBJ_237.setText("@LSUBST@");
          OBJ_237.setName("OBJ_237");
          panel7.add(OBJ_237);
          OBJ_237.setBounds(5, 14, 80, 20);
        }
        p_contenu.add(panel7);
        panel7.setBounds(10, 15, 330, 185);
        
        // ======== panel4 ========
        {
          panel4.setBorder(new TitledBorder(""));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);
          
          // ---- L1CNDX ----
          L1CNDX.setComponentPopupMenu(null);
          L1CNDX.setName("L1CNDX");
          panel4.add(L1CNDX);
          L1CNDX.setBounds(195, 6, 90, L1CNDX.getPreferredSize().height);
          
          // ---- L1QTEX ----
          L1QTEX.setComponentPopupMenu(BTD2);
          L1QTEX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTEX.setName("L1QTEX");
          panel4.add(L1QTEX);
          L1QTEX.setBounds(67, 6, 74, L1QTEX.getPreferredSize().height);
          
          // ---- L1QTCX ----
          L1QTCX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1QTCX.setText("@L1QTCX@");
          L1QTCX.setName("L1QTCX");
          panel4.add(L1QTCX);
          L1QTCX.setBounds(320, 8, 90, L1QTCX.getPreferredSize().height);
          
          // ---- L1UNV ----
          L1UNV.setComponentPopupMenu(BTD);
          L1UNV.setHorizontalAlignment(SwingConstants.LEADING);
          L1UNV.setText("@L1UNV@");
          L1UNV.setName("L1UNV");
          panel4.add(L1UNV);
          L1UNV.setBounds(145, 6, 30, L1UNV.getPreferredSize().height);
          
          // ---- OBJ_129 ----
          OBJ_129.setText("x");
          OBJ_129.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_129.setName("OBJ_129");
          panel4.add(OBJ_129);
          OBJ_129.setBounds(175, 10, 20, 20);
          
          // ---- OBJ_132 ----
          OBJ_132.setText("=");
          OBJ_132.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_132.setName("OBJ_132");
          panel4.add(OBJ_132);
          OBJ_132.setBounds(285, 10, 35, 20);
          
          // ---- LPPDBR ----
          LPPDBR.setComponentPopupMenu(BTD);
          LPPDBR.setHorizontalAlignment(SwingConstants.RIGHT);
          LPPDBR.setName("LPPDBR");
          panel4.add(LPPDBR);
          LPPDBR.setBounds(475, 35, 74, LPPDBR.getPreferredSize().height);
          
          // ---- OBJ_137 ----
          OBJ_137.setText("Nb colis");
          OBJ_137.setName("OBJ_137");
          panel4.add(OBJ_137);
          OBJ_137.setBounds(45, 35, 58, 28);
          
          // ---- OBJ_136 ----
          OBJ_136.setText("pi\u00e8ces");
          OBJ_136.setName("OBJ_136");
          panel4.add(OBJ_136);
          OBJ_136.setBounds(295, 35, 43, 28);
          
          // ---- OBJ_140 ----
          OBJ_140.setText("Poids brut");
          OBJ_140.setName("OBJ_140");
          panel4.add(OBJ_140);
          OBJ_140.setBounds(415, 35, 60, 28);
          
          // ---- label14 ----
          label14.setText("Quantit\u00e9");
          label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
          label14.setName("label14");
          panel4.add(label14);
          label14.setBounds(12, 10, 55, 20);
          
          // ---- WSER ----
          WSER.setText("WSER");
          WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER.setHorizontalAlignment(SwingConstants.RIGHT);
          WSER.setName("WSER");
          panel4.add(WSER);
          WSER.setBounds(454, 7, 81, 26);
          
          // ---- OBJ_130 ----
          OBJ_130.setText("@UNVLIB@");
          OBJ_130.setName("OBJ_130");
          panel4.add(OBJ_130);
          OBJ_130.setBounds(195, 8, 110, OBJ_130.getPreferredSize().height);
          
          // ---- A1UNL ----
          A1UNL.setToolTipText("@A1UNL@");
          A1UNL.setText("@A1UNL@");
          A1UNL.setName("A1UNL");
          panel4.add(A1UNL);
          A1UNL.setBounds(145, 6, 30, A1UNL.getPreferredSize().height);
          
          // ---- A1UNV ----
          A1UNV.setComponentPopupMenu(BTD);
          A1UNV.setHorizontalAlignment(SwingConstants.LEADING);
          A1UNV.setText("@A1UNV@");
          A1UNV.setName("A1UNV");
          panel4.add(A1UNV);
          A1UNV.setBounds(420, 6, 30, A1UNV.getPreferredSize().height);
          
          // ---- OBJ_138 ----
          OBJ_138.setText("ou");
          OBJ_138.setName("OBJ_138");
          panel4.add(OBJ_138);
          OBJ_138.setBounds(20, 39, 25, 20);
          
          // ---- OBJ_131 ----
          OBJ_131.setText("x");
          OBJ_131.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_131.setName("OBJ_131");
          panel4.add(OBJ_131);
          OBJ_131.setBounds(180, 39, 15, 20);
          
          // ---- A1KCS ----
          A1KCS.setHorizontalAlignment(SwingConstants.RIGHT);
          A1KCS.setText("@A1KCS@");
          A1KCS.setName("A1KCS");
          panel4.add(A1KCS);
          A1KCS.setBounds(195, 37, 90, A1KCS.getPreferredSize().height);
          
          // ---- A1UCS ----
          A1UCS.setText("@A1UCS@");
          A1UCS.setFont(A1UCS.getFont().deriveFont(A1UCS.getFont().getSize() - 1f));
          A1UCS.setIconTextGap(0);
          A1UCS.setPreferredSize(new Dimension(35, 30));
          A1UCS.setMinimumSize(new Dimension(35, 30));
          A1UCS.setMaximumSize(new Dimension(85, 37));
          A1UCS.setHorizontalTextPosition(SwingConstants.CENTER);
          A1UCS.setToolTipText("Enfoncez ce bouton pour saisir dans cette unit\u00e9");
          A1UCS.setName("A1UCS");
          A1UCS.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              A1UCSActionPerformed(e);
            }
          });
          panel4.add(A1UCS);
          A1UCS.setBounds(138, 34, 45, 30);
          
          // ---- LPNBC ----
          LPNBC.setComponentPopupMenu(BTD);
          LPNBC.setName("LPNBC");
          panel4.add(LPNBC);
          LPNBC.setBounds(99, 35, 42, LPNBC.getPreferredSize().height);
          
          // ---- L1CPL ----
          L1CPL.setComponentPopupMenu(BTD);
          L1CPL.setHorizontalAlignment(SwingConstants.RIGHT);
          L1CPL.setName("L1CPL");
          panel4.add(L1CPL);
          L1CPL.setBounds(336, 35, 74, L1CPL.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(350, 15, 565, 70);
        
        // ======== panel5 ========
        {
          panel5.setBorder(new TitledBorder("Condition"));
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);
          
          // ---- L1PVBX ----
          L1PVBX.setComponentPopupMenu(FCT1);
          L1PVBX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVBX.setName("L1PVBX");
          L1PVBX.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              DialogueCalculPrixVenteMouseClicked(e);
            }
          });
          panel5.add(L1PVBX);
          L1PVBX.setBounds(180, 60, 90, L1PVBX.getPreferredSize().height);
          
          // ---- L1PVNX ----
          L1PVNX.setComponentPopupMenu(FCT1);
          L1PVNX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVNX.setName("L1PVNX");
          L1PVNX.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              DialogueCalculPrixVenteMouseClicked(e);
            }
          });
          panel5.add(L1PVNX);
          L1PVNX.setBounds(85, 31, 90, L1PVNX.getPreferredSize().height);
          
          // ---- OBJ_158 ----
          OBJ_158.setText("Tarif");
          OBJ_158.setName("OBJ_158");
          panel5.add(OBJ_158);
          OBJ_158.setBounds(15, 64, 80, 20);
          
          // ---- WSPR ----
          WSPR.setToolTipText(
              "<html><b>R</b>=Saisie du prix de revient<br><b>D</b>=Achat d\u00e9rogatoire<br><b>N</b>=Prix flash<br><b>P</b>=Derniers prix pratiqu\u00e9s par le client<br><b>C</b>=Prix consommateur<br><b>F</b>=PRV forc\u00e9<br><b>A</b>=Affaire d\u00e9rogation fournisseur</html>");
          WSPR.setComponentPopupMenu(null);
          WSPR.setName("WSPR");
          panel5.add(WSPR);
          WSPR.setBounds(180, 32, 45, WSPR.getPreferredSize().height);
          
          // ---- OBJ_155 ----
          OBJ_155.setText("Prix net");
          OBJ_155.setName("OBJ_155");
          panel5.add(OBJ_155);
          OBJ_155.setBounds(15, 35, 70, 20);
          
          // ======== panel9 ========
          {
            panel9.setBorder(new BevelBorder(BevelBorder.RAISED));
            panel9.setOpaque(false);
            panel9.setName("panel9");
            panel9.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                DialogueCalculPrixVenteMouseClicked(e);
              }
            });
            panel9.setLayout(null);
            
            // ---- OBJ_227 ----
            OBJ_227.setText("Prix de vente calcul\u00e9");
            OBJ_227.setName("OBJ_227");
            OBJ_227.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                DialogueCalculPrixVenteMouseClicked(e);
              }
            });
            panel9.add(OBJ_227);
            OBJ_227.setBounds(10, 2, 120, 25);
            
            // ---- WPVCX ----
            WPVCX.setText("@WPVCX@");
            WPVCX.setFont(WPVCX.getFont().deriveFont(WPVCX.getFont().getStyle() | Font.BOLD));
            WPVCX.setHorizontalAlignment(SwingConstants.CENTER);
            WPVCX.setName("WPVCX");
            WPVCX.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                DialogueCalculPrixVenteMouseClicked(e);
              }
            });
            panel9.add(WPVCX);
            WPVCX.setBounds(135, 2, 90, 25);
          }
          panel5.add(panel9);
          panel9.setBounds(320, 155, 230, 30);
          
          // ---- OBJ_161 ----
          OBJ_161.setText("ou type de gratuit");
          OBJ_161.setName("OBJ_161");
          panel5.add(OBJ_161);
          OBJ_161.setBounds(235, 31, 110, 28);
          
          // ---- L1IN2 ----
          L1IN2.setComponentPopupMenu(BTD);
          L1IN2.setName("L1IN2");
          panel5.add(L1IN2);
          L1IN2.setBounds(340, 31, 24, 28);
          
          // ---- UTAR ----
          UTAR.setComponentPopupMenu(BTD);
          UTAR.setName("UTAR");
          panel5.add(UTAR);
          UTAR.setBounds(270, 60, 45, UTAR.getPreferredSize().height);
          
          // ---- OBJ_159 ----
          OBJ_159.setText("Remises");
          OBJ_159.setName("OBJ_159");
          panel5.add(OBJ_159);
          OBJ_159.setBounds(15, 94, 80, 20);
          
          // ---- WREM1 ----
          WREM1.setComponentPopupMenu(BTD);
          WREM1.setName("WREM1");
          panel5.add(WREM1);
          WREM1.setBounds(180, 90, 50, WREM1.getPreferredSize().height);
          
          // ---- WREM2 ----
          WREM2.setComponentPopupMenu(BTD);
          WREM2.setName("WREM2");
          panel5.add(WREM2);
          WREM2.setBounds(230, 90, 50, WREM2.getPreferredSize().height);
          
          // ---- WREM3 ----
          WREM3.setComponentPopupMenu(BTD);
          WREM3.setName("WREM3");
          panel5.add(WREM3);
          WREM3.setBounds(280, 90, 50, WREM3.getPreferredSize().height);
          
          // ---- OBJ_168 ----
          OBJ_168.setText("ou coefficient");
          OBJ_168.setName("OBJ_168");
          panel5.add(OBJ_168);
          OBJ_168.setBounds(385, 90, 90, 28);
          
          // ---- L1COEX ----
          L1COEX.setComponentPopupMenu(BTD);
          L1COEX.setHorizontalAlignment(SwingConstants.RIGHT);
          L1COEX.setName("L1COEX");
          panel5.add(L1COEX);
          L1COEX.setBounds(483, 90, 71, 28);
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- T1CNV ----
            T1CNV.setText("@T1CNV@");
            T1CNV.setName("T1CNV");
            panel1.add(T1CNV);
            T1CNV.setBounds(170, 5, 70, T1CNV.getPreferredSize().height);
            
            // ---- T1TRA ----
            T1TRA.setText("@T1TRA@");
            T1TRA.setName("T1TRA");
            panel1.add(T1TRA);
            T1TRA.setBounds(250, 5, 20, T1TRA.getPreferredSize().height);
            
            // ---- WCCD ----
            WCCD.setText("@WCCD@");
            WCCD.setName("WCCD");
            panel1.add(WCCD);
            WCCD.setBounds(280, 5, 20, WCCD.getPreferredSize().height);
            
            // ---- T1RATR ----
            T1RATR.setText("@T1RATR@");
            T1RATR.setName("T1RATR");
            panel1.add(T1RATR);
            T1RATR.setBounds(335, 5, 120, T1RATR.getPreferredSize().height);
            
            // ---- WTCD ----
            WTCD.setText("@WTCD@");
            WTCD.setName("WTCD");
            panel1.add(WTCD);
            WTCD.setBounds(475, 5, 20, WTCD.getPreferredSize().height);
            
            // ---- OBJ_178 ----
            OBJ_178.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_178.setName("OBJ_178");
            OBJ_178.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_178ActionPerformed(e);
              }
            });
            panel1.add(OBJ_178);
            OBJ_178.setBounds(135, 5, 25, 25);
            
            // ---- OBJ_160 ----
            OBJ_160.setText("Promotion appliqu\u00e9e");
            OBJ_160.setName("OBJ_160");
            panel1.add(OBJ_160);
            OBJ_160.setBounds(5, 7, 125, 20);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          panel5.add(panel1);
          panel1.setBounds(10, 120, 540, 35);
          
          // ---- OBJ_176 ----
          OBJ_176.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_176.setName("OBJ_176");
          OBJ_176.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_176ActionPerformed(e);
            }
          });
          panel5.add(OBJ_176);
          OBJ_176.setBounds(145, 92, 25, 25);
          
          // ---- OBJ_229 ----
          OBJ_229.setText("D\u00e9rogatoire");
          OBJ_229.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_229.setName("OBJ_229");
          panel5.add(OBJ_229);
          OBJ_229.setBounds(235, 0, 130, 22);
          
          // ---- OBJ_230 ----
          OBJ_230.setText("Affaire");
          OBJ_230.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_230.setName("OBJ_230");
          panel5.add(OBJ_230);
          OBJ_230.setBounds(235, 0, 130, 22);
          
          // ---- flash ----
          flash.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          flash.setText("Prix flash");
          flash.setHorizontalAlignment(SwingConstants.LEFT);
          flash.setComponentPopupMenu(BTD3);
          flash.setName("flash");
          flash.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              flashActionPerformed(e);
            }
          });
          panel5.add(flash);
          flash.setBounds(460, 0, 100, 20);
          
          // ---- L1IN18 ----
          L1IN18.setModel(
              new DefaultComboBoxModel(new String[] { "Prix garanti", "Prix non garanti", "Ancien prix", "Prix garanti / devis" }));
          L1IN18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1IN18.setName("L1IN18");
          panel5.add(L1IN18);
          L1IN18.setBounds(394, 32, 135, 26);
          
          // ---- nego ----
          nego.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          nego.setText("N\u00e9gocier");
          nego.setComponentPopupMenu(null);
          nego.setName("nego");
          nego.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              negoActionPerformed(e);
            }
          });
          panel5.add(nego);
          nego.setBounds(370, 0, 100, 20);
          
          // ---- OBJ_177 ----
          OBJ_177.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_177.setToolTipText("Visualisation des tarifs");
          OBJ_177.setName("OBJ_177");
          OBJ_177.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_177ActionPerformed(e);
            }
          });
          panel5.add(OBJ_177);
          OBJ_177.setBounds(145, 62, 25, 25);
          
          // ---- L1IN19 ----
          L1IN19.setName("L1IN19");
          panel5.add(L1IN19);
          L1IN19.setBounds(394, 61, 160, L1IN19.getPreferredSize().height);
          
          // ---- L1IN20 ----
          L1IN20.setModel(new DefaultComboBoxModel(new String[] { "Article", "Quantitative" }));
          L1IN20.setName("L1IN20");
          L1IN20.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              L1IN20ItemStateChanged(e);
            }
          });
          panel5.add(L1IN20);
          L1IN20.setBounds(15, 157, 110, L1IN20.getPreferredSize().height);
          
          // ---- cnvchantier ----
          cnvchantier.setText("@WARTT@");
          cnvchantier.setOpaque(false);
          cnvchantier.setName("cnvchantier");
          panel5.add(cnvchantier);
          cnvchantier.setBounds(129, 158, 186, cnvchantier.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel5);
        panel5.setBounds(350, 170, 565, 200);
        
        // ======== panel15 ========
        {
          panel15.setBorder(new TitledBorder(""));
          panel15.setOpaque(false);
          panel15.setName("panel15");
          panel15.setLayout(null);
          
          // ---- OBJ_151 ----
          OBJ_151.setText("Nature");
          OBJ_151.setName("OBJ_151");
          panel15.add(OBJ_151);
          OBJ_151.setBounds(15, 52, 85, 20);
          
          // ---- L20NAT ----
          L20NAT.setComponentPopupMenu(BTD);
          L20NAT.setName("L20NAT");
          panel15.add(L20NAT);
          L20NAT.setBounds(105, 48, 50, L20NAT.getPreferredSize().height);
          
          // ---- OBJ_120 ----
          OBJ_120.setText("Specif,Taxe,Kit...");
          OBJ_120.setName("OBJ_120");
          panel15.add(OBJ_120);
          OBJ_120.setBounds(15, 108, 90, 20);
          
          // ---- L1IN3 ----
          L1IN3.setComponentPopupMenu(BTD);
          L1IN3.setName("L1IN3");
          panel15.add(L1IN3);
          L1IN3.setBounds(105, 104, 24, L1IN3.getPreferredSize().height);
          
          // ---- OBJ_193 ----
          OBJ_193.setText("@TIZPL1@");
          OBJ_193.setName("OBJ_193");
          panel15.add(OBJ_193);
          OBJ_193.setBounds(15, 200, 21, 18);
          
          // ---- L1TP1 ----
          L1TP1.setComponentPopupMenu(BTD);
          L1TP1.setName("L1TP1");
          panel15.add(L1TP1);
          L1TP1.setBounds(40, 195, 30, L1TP1.getPreferredSize().height);
          
          // ---- OBJ_195 ----
          OBJ_195.setText("@TIZPL2@");
          OBJ_195.setName("OBJ_195");
          panel15.add(OBJ_195);
          OBJ_195.setBounds(75, 200, 21, 18);
          
          // ---- L1TP2 ----
          L1TP2.setComponentPopupMenu(BTD);
          L1TP2.setName("L1TP2");
          panel15.add(L1TP2);
          L1TP2.setBounds(100, 195, 30, L1TP2.getPreferredSize().height);
          
          // ---- OBJ_197 ----
          OBJ_197.setText("@TIZPL3@");
          OBJ_197.setName("OBJ_197");
          panel15.add(OBJ_197);
          OBJ_197.setBounds(135, 200, 21, 18);
          
          // ---- L1TP3 ----
          L1TP3.setComponentPopupMenu(BTD);
          L1TP3.setName("L1TP3");
          panel15.add(L1TP3);
          L1TP3.setBounds(165, 195, 30, L1TP3.getPreferredSize().height);
          
          // ---- OBJ_199 ----
          OBJ_199.setText("@TIZPL4@");
          OBJ_199.setName("OBJ_199");
          panel15.add(OBJ_199);
          OBJ_199.setBounds(200, 200, 21, 18);
          
          // ---- L1TP4 ----
          L1TP4.setComponentPopupMenu(BTD);
          L1TP4.setName("L1TP4");
          panel15.add(L1TP4);
          L1TP4.setBounds(225, 195, 30, L1TP4.getPreferredSize().height);
          
          // ---- OBJ_201 ----
          OBJ_201.setText("@TIZPL5@");
          OBJ_201.setName("OBJ_201");
          panel15.add(OBJ_201);
          OBJ_201.setBounds(265, 200, 21, 18);
          
          // ---- L1TP5 ----
          L1TP5.setComponentPopupMenu(BTD);
          L1TP5.setName("L1TP5");
          panel15.add(L1TP5);
          L1TP5.setBounds(290, 195, 30, L1TP5.getPreferredSize().height);
          
          // ---- OBJ_147 ----
          OBJ_147.setText("Extraction");
          OBJ_147.setName("OBJ_147");
          panel15.add(OBJ_147);
          OBJ_147.setBounds(15, 136, 85, 20);
          
          // ---- L1IN11 ----
          L1IN11.setName("L1IN11");
          panel15.add(L1IN11);
          L1IN11.setBounds(105, 132, 24, L1IN11.getPreferredSize().height);
          
          // ---- L1TNC ----
          L1TNC.setText("Non commissionn\u00e9");
          L1TNC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TNC.setName("L1TNC");
          panel15.add(L1TNC);
          L1TNC.setBounds(160, 136, 140, 20);
          
          // ---- L1IN12 ----
          L1IN12.setText("TEC");
          L1IN12.setToolTipText("Soumis \u00e0 la taxe additionnelle");
          L1IN12.setComponentPopupMenu(null);
          L1IN12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1IN12.setName("L1IN12");
          panel15.add(L1IN12);
          L1IN12.setBounds(160, 52, 60, 20);
          
          // ---- L1TPF ----
          L1TPF.setText("TPF");
          L1TPF.setToolTipText("Soumis \u00e0 la taxe parafiscale");
          L1TPF.setComponentPopupMenu(null);
          L1TPF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TPF.setName("L1TPF");
          panel15.add(L1TPF);
          L1TPF.setBounds(160, 80, 50, 20);
          
          // ---- L1IN1 ----
          L1IN1.setText("Non soumis \u00e0 l'escompte");
          L1IN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1IN1.setComponentPopupMenu(null);
          L1IN1.setName("L1IN1");
          panel15.add(L1IN1);
          L1IN1.setBounds(160, 108, 170, 20);
          
          // ---- OBJ_174 ----
          OBJ_174.setText("Taux de TVA");
          OBJ_174.setName("OBJ_174");
          panel15.add(OBJ_174);
          OBJ_174.setBounds(15, 18, 85, 20);
          
          // ---- OBJ_184 ----
          OBJ_184.setText("Repr\u00e9sentant");
          OBJ_184.setName("OBJ_184");
          panel15.add(OBJ_184);
          OBJ_184.setBounds(15, 80, 85, 20);
          
          // ---- L1REP ----
          L1REP.setComponentPopupMenu(BTD);
          L1REP.setName("L1REP");
          panel15.add(L1REP);
          L1REP.setBounds(105, 76, 34, L1REP.getPreferredSize().height);
          
          // ---- L1TVA ----
          L1TVA.setName("L1TVA");
          panel15.add(L1TVA);
          L1TVA.setBounds(105, 15, 110, L1TVA.getPreferredSize().height);
          
          // ---- L1IN17 ----
          L1IN17.setComponentPopupMenu(BTD);
          L1IN17.setName("L1IN17");
          panel15.add(L1IN17);
          L1IN17.setBounds(105, 160, 24, L1IN17.getPreferredSize().height);
          
          // ---- OBJ_148 ----
          OBJ_148.setText("Type de vente");
          OBJ_148.setName("OBJ_148");
          panel15.add(OBJ_148);
          OBJ_148.setBounds(15, 164, 85, 20);
          
          // ---- L1IN21 ----
          L1IN21.setComponentPopupMenu(BTD);
          L1IN21.setName("L1IN21");
          panel15.add(L1IN21);
          L1IN21.setBounds(290, 160, 24, L1IN21.getPreferredSize().height);
          
          // ---- OBJ_121 ----
          OBJ_121.setText("Regroupement");
          OBJ_121.setName("OBJ_121");
          panel15.add(OBJ_121);
          OBJ_121.setBounds(160, 164, 120, 20);
        }
        p_contenu.add(panel15);
        panel15.setBounds(10, 195, 335, 230);
        
        // ======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Dates et d\u00e9lais"));
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- WDATTX ----
          WDATTX.setText("@WDATTX@");
          WDATTX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATTX.setName("WDATTX");
          panel6.add(WDATTX);
          WDATTX.setBounds(390, 32, 65, WDATTX.getPreferredSize().height);
          
          // ---- OBJ_56 ----
          OBJ_56.setText("Prochain attendu le");
          OBJ_56.setName("OBJ_56");
          panel6.add(OBJ_56);
          OBJ_56.setBounds(270, 34, 120, 20);
          
          // ---- OBJ_109 ----
          OBJ_109.setText("D\u00e9lai en semaines");
          OBJ_109.setName("OBJ_109");
          panel6.add(OBJ_109);
          OBJ_109.setBounds(20, 34, 117, 20);
          
          // ---- WDELX ----
          WDELX.setText("@WDELX@");
          WDELX.setHorizontalAlignment(SwingConstants.RIGHT);
          WDELX.setName("WDELX");
          panel6.add(WDELX);
          WDELX.setBounds(141, 32, 26, WDELX.getPreferredSize().height);
          
          // ---- L1DLPX ----
          L1DLPX.setComponentPopupMenu(BTD);
          L1DLPX.setName("L1DLPX");
          panel6.add(L1DLPX);
          L1DLPX.setBounds(140, 60, 105, L1DLPX.getPreferredSize().height);
          
          // ---- WDLSX ----
          WDLSX.setComponentPopupMenu(BTD);
          WDLSX.setName("WDLSX");
          panel6.add(WDLSX);
          WDLSX.setBounds(390, 60, 105, WDLSX.getPreferredSize().height);
          
          // ---- bt_GA ----
          bt_GA.setText("Aucune");
          bt_GA.setToolTipText("<HTML>Aucune g\u00e9n\u00e9ration<BR>@&L000999@</HTML>");
          bt_GA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_GA.setName("bt_GA");
          bt_GA.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_GAActionPerformed(e);
            }
          });
          panel6.add(bt_GA);
          bt_GA.setBounds(142, 90, 127, 25);
          
          // ---- label12 ----
          label12.setText("Livraison pr\u00e9vue");
          label12.setName("label12");
          panel6.add(label12);
          label12.setBounds(20, 64, 117, 20);
          
          // ---- label13 ----
          label13.setText("Livraison souhait\u00e9e");
          label13.setName("label13");
          panel6.add(label13);
          label13.setBounds(270, 64, 120, 20);
          
          // ---- label11 ----
          label11.setForeground(new Color(102, 102, 102));
          label11.setName("label11");
          panel6.add(label11);
          label11.setBounds(280, 91, 170, 22);
          
          // ---- label2 ----
          label2.setText("G\u00e9n\u00e9ration d'achat:");
          label2.setName("label2");
          panel6.add(label2);
          label2.setBounds(20, 92, 115, 21);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel6);
        panel6.setBounds(350, 370, 565, 130);
        
        // ======== panelDimension ========
        {
          panelDimension.setBorder(new TitledBorder("Dimensions"));
          panelDimension.setOpaque(false);
          panelDimension.setName("panelDimension");
          panelDimension.setLayout(null);
          
          // ---- L2NBRX ----
          L2NBRX.setHorizontalAlignment(SwingConstants.RIGHT);
          L2NBRX.setName("L2NBRX");
          panelDimension.add(L2NBRX);
          L2NBRX.setBounds(70, 25, 74, L2NBRX.getPreferredSize().height);
          
          // ---- OBJ_141 ----
          OBJ_141.setText("nombre");
          OBJ_141.setFont(OBJ_141.getFont().deriveFont(OBJ_141.getFont().getStyle() | Font.BOLD));
          OBJ_141.setName("OBJ_141");
          panelDimension.add(OBJ_141);
          OBJ_141.setBounds(15, 29, 55, 20);
          
          // ---- l_x3 ----
          l_x3.setText("x");
          l_x3.setHorizontalAlignment(SwingConstants.CENTER);
          l_x3.setFont(l_x3.getFont().deriveFont(l_x3.getFont().getStyle() | Font.BOLD));
          l_x3.setName("l_x3");
          panelDimension.add(l_x3);
          l_x3.setBounds(160, 25, 20, 28);
          
          // ======== psurface ========
          {
            psurface.setOpaque(false);
            psurface.setName("psurface");
            psurface.setLayout(null);
            
            // ---- label17 ----
            label17.setText("Surface");
            label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
            label17.setName("label17");
            psurface.add(label17);
            label17.setBounds(5, 5, 55, 20);
            
            // ---- surface1 ----
            surface1.setHorizontalAlignment(SwingConstants.RIGHT);
            surface1.setName("surface1");
            surface1.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                surface1FocusGained(e);
              }
            });
            psurface.add(surface1);
            surface1.setBounds(75, 0, 84, surface1.getPreferredSize().height);
            
            // ---- l_x4 ----
            l_x4.setText("x");
            l_x4.setHorizontalAlignment(SwingConstants.CENTER);
            l_x4.setFont(l_x4.getFont().deriveFont(l_x4.getFont().getStyle() | Font.BOLD));
            l_x4.setName("l_x4");
            psurface.add(l_x4);
            l_x4.setBounds(170, 0, 20, 28);
            
            // ---- surface2 ----
            surface2.setHorizontalAlignment(SwingConstants.RIGHT);
            surface2.setName("surface2");
            surface2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                surface2FocusGained(e);
              }
            });
            psurface.add(surface2);
            surface2.setBounds(200, 0, 84, surface2.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < psurface.getComponentCount(); i++) {
                Rectangle bounds = psurface.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = psurface.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              psurface.setMinimumSize(preferredSize);
              psurface.setPreferredSize(preferredSize);
            }
          }
          panelDimension.add(psurface);
          psurface.setBounds(195, 24, 310, 30);
          
          // ======== plongueur ========
          {
            plongueur.setOpaque(false);
            plongueur.setName("plongueur");
            plongueur.setLayout(null);
            
            // ---- label16 ----
            label16.setText("Longueur");
            label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
            label16.setName("label16");
            plongueur.add(label16);
            label16.setBounds(5, 5, 55, 20);
            
            // ---- longueur1 ----
            longueur1.setHorizontalAlignment(SwingConstants.RIGHT);
            longueur1.setName("longueur1");
            longueur1.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                longueur1FocusGained(e);
              }
            });
            plongueur.add(longueur1);
            longueur1.setBounds(75, 0, 84, longueur1.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < plongueur.getComponentCount(); i++) {
                Rectangle bounds = plongueur.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = plongueur.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              plongueur.setMinimumSize(preferredSize);
              plongueur.setPreferredSize(preferredSize);
            }
          }
          panelDimension.add(plongueur);
          plongueur.setBounds(195, 24, 310, 30);
          
          // ---- A1IN30 ----
          A1IN30.setName("A1IN30");
          panelDimension.add(A1IN30);
          A1IN30.setBounds(480, 50, 60, A1IN30.getPreferredSize().height);
          
          // ---- A1IN28 ----
          A1IN28.setName("A1IN28");
          panelDimension.add(A1IN28);
          A1IN28.setBounds(290, 50, 60, A1IN28.getPreferredSize().height);
          
          // ---- A1IN29 ----
          A1IN29.setName("A1IN29");
          panelDimension.add(A1IN29);
          A1IN29.setBounds(415, 50, 60, A1IN29.getPreferredSize().height);
          
          // ======== pvolume ========
          {
            pvolume.setOpaque(false);
            pvolume.setName("pvolume");
            pvolume.setLayout(null);
            
            // ---- volume1 ----
            volume1.setHorizontalAlignment(SwingConstants.RIGHT);
            volume1.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
            volume1.setAction(null);
            volume1.setName("volume1");
            volume1.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                volume1FocusGained(e);
              }
            });
            pvolume.add(volume1);
            volume1.setBounds(60, 0, 84, 28);
            
            // ---- l_x1 ----
            l_x1.setText("x");
            l_x1.setHorizontalAlignment(SwingConstants.CENTER);
            l_x1.setFont(l_x1.getFont().deriveFont(l_x1.getFont().getStyle() | Font.BOLD));
            l_x1.setName("l_x1");
            pvolume.add(l_x1);
            l_x1.setBounds(145, 0, 15, 28);
            
            // ---- volume2 ----
            volume2.setHorizontalAlignment(SwingConstants.RIGHT);
            volume2.setName("volume2");
            volume2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                volume2FocusGained(e);
              }
            });
            pvolume.add(volume2);
            volume2.setBounds(161, 0, 84, 28);
            
            // ---- l_x2 ----
            l_x2.setText("x");
            l_x2.setHorizontalAlignment(SwingConstants.CENTER);
            l_x2.setFont(l_x2.getFont().deriveFont(l_x2.getFont().getStyle() | Font.BOLD));
            l_x2.setName("l_x2");
            pvolume.add(l_x2);
            l_x2.setBounds(246, 0, 15, 28);
            
            // ---- volume3 ----
            volume3.setHorizontalAlignment(SwingConstants.RIGHT);
            volume3.setName("volume3");
            volume3.addFocusListener(new FocusAdapter() {
              @Override
              public void focusGained(FocusEvent e) {
                volume3FocusGained(e);
              }
            });
            pvolume.add(volume3);
            volume3.setBounds(262, 0, 84, 28);
            
            // ---- label15 ----
            label15.setText("Volume");
            label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
            label15.setName("label15");
            pvolume.add(label15);
            label15.setBounds(5, 5, 55, 20);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pvolume.getComponentCount(); i++) {
                Rectangle bounds = pvolume.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pvolume.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pvolume.setMinimumSize(preferredSize);
              pvolume.setPreferredSize(preferredSize);
            }
          }
          panelDimension.add(pvolume);
          pvolume.setBounds(195, 24, 355, 30);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panelDimension.getComponentCount(); i++) {
              Rectangle bounds = panelDimension.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panelDimension.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panelDimension.setMinimumSize(preferredSize);
            panelDimension.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panelDimension);
        panelDimension.setBounds(350, 85, 565, 85);
        
        // ======== p_stock ========
        {
          p_stock.setBorder(new TitledBorder("Stocks"));
          p_stock.setOpaque(false);
          p_stock.setName("p_stock");
          p_stock.setLayout(null);
          
          // ======== p_S7 ========
          {
            p_S7.setBorder(null);
            p_S7.setOpaque(false);
            p_S7.setName("p_S7");
            p_S7.setLayout(null);
            
            // ---- disp_7 ----
            disp_7.setText("Disponible");
            disp_7.setName("disp_7");
            p_S7.add(disp_7);
            disp_7.setBounds(5, 5, 69, 20);
            
            // ---- WDISX_7 ----
            WDISX_7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WDISX_7.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX_7.setText("@WDISX@");
            WDISX_7.setName("WDISX_7");
            p_S7.add(WDISX_7);
            WDISX_7.setBounds(5, 25, 98, WDISX_7.getPreferredSize().height);
            
            // ---- commande_7 ----
            commande_7.setText("Commandes clients");
            commande_7.setName("commande_7");
            p_S7.add(commande_7);
            commande_7.setBounds(136, 5, 121, 20);
            
            // ---- WRESX_7 ----
            WRESX_7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WRESX_7.setHorizontalAlignment(SwingConstants.RIGHT);
            WRESX_7.setText("@WRESX@");
            WRESX_7.setName("WRESX_7");
            p_S7.add(WRESX_7);
            WRESX_7.setBounds(136, 25, 98, WRESX_7.getPreferredSize().height);
            
            // ---- att_7 ----
            att_7.setText("Attendu");
            att_7.setName("att_7");
            p_S7.add(att_7);
            att_7.setBounds(267, 5, 60, 20);
            
            // ---- WATTX_7 ----
            WATTX_7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WATTX_7.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX_7.setText("@WATTX@");
            WATTX_7.setName("WATTX_7");
            p_S7.add(WATTX_7);
            WATTX_7.setBounds(267, 25, 98, WATTX_7.getPreferredSize().height);
            
            // ---- attt_7 ----
            attt_7.setText("Attendu Dispo");
            attt_7.setName("attt_7");
            p_S7.add(attt_7);
            attt_7.setBounds(398, 5, 85, 20);
            
            // ---- WAFFX_7 ----
            WAFFX_7.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WAFFX_7.setHorizontalAlignment(SwingConstants.RIGHT);
            WAFFX_7.setText("@WAFFX@");
            WAFFX_7.setName("WAFFX_7");
            p_S7.add(WAFFX_7);
            WAFFX_7.setBounds(398, 25, 98, WAFFX_7.getPreferredSize().height);
          }
          p_stock.add(p_S7);
          p_S7.setBounds(15, 30, 515, 60);
          
          // ---- label3 ----
          label3.setText(" ");
          label3.setToolTipText("@TYPS@");
          label3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          label3.setName("label3");
          p_stock.add(label3);
          label3.setBounds(840, 40, 40, 45);
          
          // ---- riBoutonDetailListe1 ----
          riBoutonDetailListe1.setToolTipText("position de stock");
          riBoutonDetailListe1.setName("riBoutonDetailListe1");
          riBoutonDetailListe1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonDetailListe1ActionPerformed(e);
            }
          });
          p_stock.add(riBoutonDetailListe1);
          riBoutonDetailListe1.setBounds(60, -5, 30, 30);
          
          // ======== p_S1 ========
          {
            p_S1.setBorder(null);
            p_S1.setOpaque(false);
            p_S1.setName("p_S1");
            p_S1.setLayout(null);
            
            // ---- disp_1 ----
            disp_1.setText("Disponible");
            disp_1.setName("disp_1");
            p_S1.add(disp_1);
            disp_1.setBounds(400, 5, 69, 20);
            
            // ---- WDISX_1 ----
            WDISX_1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WDISX_1.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX_1.setText("@WDISX@");
            WDISX_1.setName("WDISX_1");
            p_S1.add(WDISX_1);
            WDISX_1.setBounds(398, 25, 98, WDISX_1.getPreferredSize().height);
            
            // ---- WSTKX_1 ----
            WSTKX_1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WSTKX_1.setText("@WSTKX@");
            WSTKX_1.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTKX_1.setName("WSTKX_1");
            p_S1.add(WSTKX_1);
            WSTKX_1.setBounds(5, 25, 98, WSTKX_1.getPreferredSize().height);
            
            // ---- stock_1 ----
            stock_1.setText("Physique");
            stock_1.setName("stock_1");
            p_S1.add(stock_1);
            stock_1.setBounds(5, 5, 69, 20);
            
            // ---- WRESX_1 ----
            WRESX_1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WRESX_1.setHorizontalAlignment(SwingConstants.RIGHT);
            WRESX_1.setText("@WRESX@");
            WRESX_1.setName("WRESX_1");
            p_S1.add(WRESX_1);
            WRESX_1.setBounds(136, 25, 98, WRESX_1.getPreferredSize().height);
            
            // ---- commande_8 ----
            commande_8.setText("Command\u00e9");
            commande_8.setName("commande_8");
            p_S1.add(commande_8);
            commande_8.setBounds(136, 5, 121, 20);
            
            // ---- WAFFX_1 ----
            WAFFX_1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WAFFX_1.setHorizontalAlignment(SwingConstants.RIGHT);
            WAFFX_1.setText("@WAFFX@");
            WAFFX_1.setName("WAFFX_1");
            p_S1.add(WAFFX_1);
            WAFFX_1.setBounds(267, 25, 98, WAFFX_1.getPreferredSize().height);
            
            // ---- OBJ_57 ----
            OBJ_57.setText("Dont r\u00e9serv\u00e9");
            OBJ_57.setName("OBJ_57");
            p_S1.add(OBJ_57);
            OBJ_57.setBounds(267, 5, 90, 20);
            
            // ---- WATTX_1 ----
            WATTX_1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WATTX_1.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX_1.setText("@WATTX@");
            WATTX_1.setName("WATTX_1");
            p_S1.add(WATTX_1);
            WATTX_1.setBounds(530, 25, 98, WATTX_1.getPreferredSize().height);
            
            // ---- att_1 ----
            att_1.setText("Attendu");
            att_1.setName("att_1");
            p_S1.add(att_1);
            att_1.setBounds(530, 5, 60, 20);
            
            // ---- OBJ_94 ----
            OBJ_94.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_94.setFont(OBJ_94.getFont().deriveFont(OBJ_94.getFont().getStyle() | Font.BOLD, OBJ_94.getFont().getSize() + 3f));
            OBJ_94.setName("OBJ_94");
            p_S1.add(OBJ_94);
            OBJ_94.setBounds(110, 30, 19, 20);
            
            // ---- OBJ_95 ----
            OBJ_95.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD, OBJ_95.getFont().getSize() + 1f));
            OBJ_95.setName("OBJ_95");
            p_S1.add(OBJ_95);
            OBJ_95.setBounds(240, 30, 19, 20);
            
            // ---- OBJ_96 ----
            OBJ_96.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_96.setFont(OBJ_96.getFont().deriveFont(OBJ_96.getFont().getStyle() | Font.BOLD, OBJ_96.getFont().getSize() + 1f));
            OBJ_96.setName("OBJ_96");
            p_S1.add(OBJ_96);
            OBJ_96.setBounds(370, 30, 19, 20);
          }
          p_stock.add(p_S1);
          p_S1.setBounds(15, 30, 641, 60);
          
          // ======== p_S5 ========
          {
            p_S5.setBorder(null);
            p_S5.setOpaque(false);
            p_S5.setName("p_S5");
            p_S5.setLayout(null);
            
            // ---- disp_5 ----
            disp_5.setText("Disponible");
            disp_5.setName("disp_5");
            p_S5.add(disp_5);
            disp_5.setBounds(267, 5, 69, 20);
            
            // ---- WDISX_5 ----
            WDISX_5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WDISX_5.setHorizontalAlignment(SwingConstants.RIGHT);
            WDISX_5.setText("@WDISX@");
            WDISX_5.setName("WDISX_5");
            p_S5.add(WDISX_5);
            WDISX_5.setBounds(267, 25, 98, WDISX_5.getPreferredSize().height);
            
            // ---- WSTKX_5 ----
            WSTKX_5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WSTKX_5.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTKX_5.setText("@WSTKX@");
            WSTKX_5.setName("WSTKX_5");
            p_S5.add(WSTKX_5);
            WSTKX_5.setBounds(5, 25, 98, WSTKX_5.getPreferredSize().height);
            
            // ---- stock_5 ----
            stock_5.setText("Physique");
            stock_5.setName("stock_5");
            p_S5.add(stock_5);
            stock_5.setBounds(5, 5, 69, 20);
            
            // ---- WATTX_5 ----
            WATTX_5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WATTX_5.setHorizontalAlignment(SwingConstants.RIGHT);
            WATTX_5.setText("@WATTX@");
            WATTX_5.setName("WATTX_5");
            p_S5.add(WATTX_5);
            WATTX_5.setBounds(600, 25, 98, WATTX_5.getPreferredSize().height);
            
            // ---- att_5 ----
            att_5.setText("Attendu");
            att_5.setName("att_5");
            p_S5.add(att_5);
            att_5.setBounds(600, 5, 60, 20);
            
            // ---- OBJ_97 ----
            OBJ_97.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_97.setFont(OBJ_97.getFont().deriveFont(OBJ_97.getFont().getStyle() | Font.BOLD, OBJ_97.getFont().getSize() + 3f));
            OBJ_97.setName("OBJ_97");
            p_S5.add(OBJ_97);
            OBJ_97.setBounds(110, 30, 19, 20);
            
            // ---- reser_5 ----
            reser_5.setText("R\u00e9serv\u00e9");
            reser_5.setName("reser_5");
            p_S5.add(reser_5);
            reser_5.setBounds(136, 5, 80, 20);
            
            // ---- WAFFX_5 ----
            WAFFX_5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WAFFX_5.setHorizontalAlignment(SwingConstants.RIGHT);
            WAFFX_5.setText("@WAFFX@");
            WAFFX_5.setName("WAFFX_5");
            p_S5.add(WAFFX_5);
            WAFFX_5.setBounds(136, 25, 98, WAFFX_5.getPreferredSize().height);
            
            // ---- OBJ_98 ----
            OBJ_98.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_98.setFont(OBJ_98.getFont().deriveFont(OBJ_98.getFont().getStyle() | Font.BOLD, OBJ_98.getFont().getSize() + 1f));
            OBJ_98.setName("OBJ_98");
            p_S5.add(OBJ_98);
            OBJ_98.setBounds(240, 30, 19, 20);
            
            // ---- OBJ_99 ----
            OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getStyle() | Font.BOLD, OBJ_99.getFont().getSize() + 3f));
            OBJ_99.setName("OBJ_99");
            p_S5.add(OBJ_99);
            OBJ_99.setBounds(370, 30, 19, 20);
            
            // ---- WCNRX_5 ----
            WCNRX_5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WCNRX_5.setHorizontalAlignment(SwingConstants.RIGHT);
            WCNRX_5.setText("@WCNRX@");
            WCNRX_5.setName("WCNRX_5");
            p_S5.add(WCNRX_5);
            WCNRX_5.setBounds(398, 25, 69, WCNRX_5.getPreferredSize().height);
            
            // ---- commande_5 ----
            commande_5.setText("Command\u00e9");
            commande_5.setName("commande_5");
            p_S5.add(commande_5);
            commande_5.setBounds(400, 5, 70, 20);
            
            // ---- WSTTX_5 ----
            WSTTX_5.setBorder(new BevelBorder(BevelBorder.LOWERED));
            WSTTX_5.setHorizontalAlignment(SwingConstants.RIGHT);
            WSTTX_5.setText("@WSTTX@");
            WSTTX_5.setName("WSTTX_5");
            p_S5.add(WSTTX_5);
            WSTTX_5.setBounds(500, 25, 69, WSTTX_5.getPreferredSize().height);
            
            // ---- OBJ_101 ----
            OBJ_101.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_101.setFont(OBJ_101.getFont().deriveFont(OBJ_101.getFont().getStyle() | Font.BOLD, OBJ_101.getFont().getSize() + 1f));
            OBJ_101.setName("OBJ_101");
            p_S5.add(OBJ_101);
            OBJ_101.setBounds(470, 30, 19, 20);
            
            // ---- OBJ_55 ----
            OBJ_55.setText("Th\u00e9orique");
            OBJ_55.setName("OBJ_55");
            p_S5.add(OBJ_55);
            OBJ_55.setBounds(500, 5, 69, 20);
          }
          p_stock.add(p_S5);
          p_S5.setBounds(15, 30, 710, 60);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_stock.getComponentCount(); i++) {
              Rectangle bounds = p_stock.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_stock.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_stock.setMinimumSize(preferredSize);
            p_stock.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_stock);
        p_stock.setBounds(10, 500, 905, 105);
        
        // ======== panel16 ========
        {
          panel16.setBorder(new TitledBorder("Options d'\u00e9dition"));
          panel16.setOpaque(false);
          panel16.setName("panel16");
          panel16.setLayout(null);
          
          // ---- L1TE1 ----
          L1TE1.setText("sans code article");
          L1TE1.setToolTipText("Soumis \u00e0 la taxe additionnelle");
          L1TE1.setComponentPopupMenu(null);
          L1TE1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE1.setName("L1TE1");
          panel16.add(L1TE1);
          L1TE1.setBounds(15, 25, 140, 20);
          
          // ---- L1TE2 ----
          L1TE2.setText("sans libell\u00e9");
          L1TE2.setToolTipText("Soumis \u00e0 la taxe parafiscale");
          L1TE2.setComponentPopupMenu(null);
          L1TE2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE2.setName("L1TE2");
          panel16.add(L1TE2);
          L1TE2.setBounds(15, 45, 140, 20);
          
          // ---- L1TE3 ----
          L1TE3.setText("Sans prix de vente");
          L1TE3.setToolTipText("Soumis \u00e0 la taxe additionnelle");
          L1TE3.setComponentPopupMenu(null);
          L1TE3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE3.setName("L1TE3");
          panel16.add(L1TE3);
          L1TE3.setBounds(165, 25, 140, 20);
          
          // ---- L1TE4 ----
          L1TE4.setToolTipText("Soumis \u00e0 la taxe parafiscale");
          L1TE4.setComponentPopupMenu(null);
          L1TE4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1TE4.setName("L1TE4");
          panel16.add(L1TE4);
          L1TE4.setBounds(165, 45, 140, 20);
        }
        p_contenu.add(panel16);
        panel16.setBounds(10, 420, 335, 78);
        
        // ======== p_marge ========
        {
          p_marge.setBorder(new TitledBorder("Marge"));
          p_marge.setOpaque(false);
          p_marge.setName("p_marge");
          p_marge.setLayout(null);
          
          // ---- label4 ----
          label4.setText(" ");
          label4.setToolTipText("@TYPS@");
          label4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          label4.setName("label4");
          p_marge.add(label4);
          label4.setBounds(840, 40, 40, 45);
          
          // ---- label5 ----
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setText("Marge r\u00e9elle");
          label5.setName("label5");
          p_marge.add(label5);
          label5.setBounds(30, 27, 85, 20);
          
          // ---- label6 ----
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setText("Marge th\u00e9orique");
          label6.setName("label6");
          p_marge.add(label6);
          label6.setBounds(30, 52, 100, 20);
          
          // ---- label7 ----
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setText("Action commerciale");
          label7.setName("label7");
          p_marge.add(label7);
          label7.setBounds(30, 77, 120, 20);
          
          // ---- WMMR ----
          WMMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMR.setText("@WMMR@");
          WMMR.setName("WMMR");
          p_marge.add(WMMR);
          WMMR.setBounds(155, 25, 90, WMMR.getPreferredSize().height);
          
          // ---- WMMT ----
          WMMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WMMT.setText("@WMMT@");
          WMMT.setName("WMMT");
          p_marge.add(WMMT);
          WMMT.setBounds(155, 50, 90, WMMT.getPreferredSize().height);
          
          // ---- WMAC ----
          WMAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WMAC.setText("@WMAC@");
          WMAC.setName("WMAC");
          p_marge.add(WMAC);
          WMAC.setBounds(155, 75, 90, WMAC.getPreferredSize().height);
          
          // ---- label8 ----
          label8.setText("% Chiffre d'affaire r\u00e9el");
          label8.setName("label8");
          p_marge.add(label8);
          label8.setBounds(265, 27, 135, 20);
          
          // ---- label9 ----
          label9.setText("% Chiffre d'affaire r\u00e9el");
          label9.setName("label9");
          p_marge.add(label9);
          label9.setBounds(265, 52, 135, 20);
          
          // ---- label18 ----
          label18.setText("% Chiffre d'affaire th\u00e9orique");
          label18.setName("label18");
          p_marge.add(label18);
          label18.setBounds(265, 77, 160, 20);
          
          // ---- WPMR ----
          WPMR.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMR.setText("@WPMR@");
          WPMR.setName("WPMR");
          p_marge.add(WPMR);
          WPMR.setBounds(425, 25, 70, WPMR.getPreferredSize().height);
          
          // ---- WPMT ----
          WPMT.setHorizontalAlignment(SwingConstants.RIGHT);
          WPMT.setText("@WPMT@");
          WPMT.setName("WPMT");
          p_marge.add(WPMT);
          WPMT.setBounds(425, 50, 70, WPMT.getPreferredSize().height);
          
          // ---- WPAC ----
          WPAC.setHorizontalAlignment(SwingConstants.RIGHT);
          WPAC.setText("@WPAC@");
          WPAC.setName("WPAC");
          p_marge.add(WPAC);
          WPAC.setBounds(425, 75, 70, WPAC.getPreferredSize().height);
          
          // ---- label19 ----
          label19.setText("Prix de revient");
          label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
          label19.setName("label19");
          p_marge.add(label19);
          label19.setBounds(515, 27, 110, 20);
          
          // ---- label20 ----
          label20.setText("C.Affaire r\u00e9el");
          label20.setName("label20");
          p_marge.add(label20);
          label20.setBounds(515, 52, 120, 20);
          
          // ---- label21 ----
          label21.setText("C.Affaire th\u00e9orique");
          label21.setName("label21");
          p_marge.add(label21);
          label21.setBounds(515, 77, 150, 20);
          
          // ---- WTPRL ----
          WTPRL.setHorizontalAlignment(SwingConstants.RIGHT);
          WTPRL.setText("@WTPRL@");
          WTPRL.setName("WTPRL");
          p_marge.add(WTPRL);
          WTPRL.setBounds(665, 25, 75, WTPRL.getPreferredSize().height);
          
          // ---- L1MHT ----
          L1MHT.setHorizontalAlignment(SwingConstants.RIGHT);
          L1MHT.setText("@L1MHT@");
          L1MHT.setName("L1MHT");
          p_marge.add(L1MHT);
          L1MHT.setBounds(665, 50, 75, L1MHT.getPreferredSize().height);
          
          // ---- WTPVL ----
          WTPVL.setHorizontalAlignment(SwingConstants.RIGHT);
          WTPVL.setText("@WTPVL@");
          WTPVL.setName("WTPVL");
          p_marge.add(WTPVL);
          WTPVL.setBounds(665, 75, 75, WTPVL.getPreferredSize().height);
          
          // ---- marge ----
          marge.setName("marge");
          p_marge.add(marge);
          marge.setBounds(120, 22, 30, 30);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_marge.getComponentCount(); i++) {
              Rectangle bounds = p_marge.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_marge.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_marge.setMinimumSize(preferredSize);
            p_marge.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_marge);
        p_marge.setBounds(10, 500, 905, 115);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 32));
      barre_tete.setName("barre_tete");
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setPreferredSize(new Dimension(800, 32));
        panel2.setMinimumSize(new Dimension(1100, 32));
        panel2.setMaximumSize(new Dimension(1100, 29));
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- OBJ_192 ----
        OBJ_192.setText("@AVOIR@");
        OBJ_192.setOpaque(false);
        OBJ_192.setName("OBJ_192");
        panel2.add(OBJ_192);
        OBJ_192.setBounds(670, 2, 99, OBJ_192.getPreferredSize().height);
        
        // ---- OBJ_169 ----
        OBJ_169.setText("@VALE@");
        OBJ_169.setOpaque(false);
        OBJ_169.setName("OBJ_169");
        panel2.add(OBJ_169);
        OBJ_169.setBounds(585, 2, 75, OBJ_169.getPreferredSize().height);
        
        // ---- WCOD ----
        WCOD.setOpaque(false);
        WCOD.setText("@WCOD@");
        WCOD.setName("WCOD");
        panel2.add(WCOD);
        WCOD.setBounds(45, 2, 20, WCOD.getPreferredSize().height);
        
        // ---- WNLI ----
        WNLI.setOpaque(false);
        WNLI.setText("@WNLI@");
        WNLI.setHorizontalAlignment(SwingConstants.RIGHT);
        WNLI.setName("WNLI");
        panel2.add(WNLI);
        WNLI.setBounds(130, 2, 50, WNLI.getPreferredSize().height);
        
        // ---- label10 ----
        label10.setText("N\u00b0 ligne");
        label10.setName("label10");
        panel2.add(label10);
        label10.setBounds(75, 0, 55, 28);
        
        // ---- OBJ_115 ----
        OBJ_115.setText("@WLCA@");
        OBJ_115.setHorizontalAlignment(SwingConstants.RIGHT);
        OBJ_115.setName("OBJ_115");
        panel2.add(OBJ_115);
        OBJ_115.setBounds(190, 0, 72, 28);
        
        // ---- WARTT ----
        WARTT.setText("@WARTT@");
        WARTT.setOpaque(false);
        WARTT.setName("WARTT");
        panel2.add(WARTT);
        WARTT.setBounds(270, 2, 186, WARTT.getPreferredSize().height);
        
        // ---- OBJ_118 ----
        OBJ_118.setText("Magasin");
        OBJ_118.setName("OBJ_118");
        panel2.add(OBJ_118);
        OBJ_118.setBounds(490, 0, 55, 28);
        
        // ---- L1MAG ----
        L1MAG.setComponentPopupMenu(BTD);
        L1MAG.setName("L1MAG");
        panel2.add(L1MAG);
        L1MAG.setBounds(545, 0, 34, L1MAG.getPreferredSize().height);
        
        // ---- label1 ----
        label1.setText("Mode");
        label1.setName("label1");
        panel2.add(label1);
        label1.setBounds(5, 0, 35, 28);
        
        // ---- etat ----
        etat.setText("Commercialis\u00e9 le @A1DT1X@");
        etat.setHorizontalAlignment(SwingConstants.RIGHT);
        etat.setForeground(Color.black);
        etat.setFont(etat.getFont().deriveFont(etat.getFont().getStyle() | Font.BOLD));
        etat.setName("etat");
        panel2.add(etat);
        etat.setBounds(839, 0, 182, 28);
        
        // ---- bt_ecran ----
        bt_ecran.setBorder(BorderFactory.createEmptyBorder());
        bt_ecran.setToolTipText("Passage \u00e0 l'affichage r\u00e9duit");
        bt_ecran.setPreferredSize(new Dimension(30, 30));
        bt_ecran.setName("bt_ecran");
        bt_ecran.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            bt_ecranMouseClicked(e);
          }
        });
        panel2.add(bt_ecran);
        bt_ecran.setBounds(new Rectangle(new Point(1048, 2), bt_ecran.getPreferredSize()));
        
        // ---- riBoutonDetail3 ----
        riBoutonDetail3.setToolTipText("Extensions de libell\u00e9s");
        riBoutonDetail3.setName("riBoutonDetail3");
        riBoutonDetail3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            riBoutonDetail3ActionPerformed(e);
          }
        });
        panel2.add(riBoutonDetail3);
        riBoutonDetail3.setBounds(460, 0, riBoutonDetail3.getPreferredSize().width, 28);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel2);
    }
    add(barre_tete, BorderLayout.NORTH);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    
    // ======== FCT1 ========
    {
      FCT1.setName("FCT1");
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_22);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Convertisseur mon\u00e9taire");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_20);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      FCT1.add(OBJ_21);
    }
    
    // ---- OBJ_208 ----
    OBJ_208.setText("Epuis\u00e9");
    OBJ_208.setName("OBJ_208");
    
    // ---- OBJ_236 ----
    OBJ_236.setText("@STOC@");
    OBJ_236.setName("OBJ_236");
    
    // ======== riSousMenu10 ========
    {
      riSousMenu10.setName("riSousMenu10");
      
      // ---- riSousMenu_bt10 ----
      riSousMenu_bt10.setText("Saisie du poids");
      riSousMenu_bt10.setToolTipText("Saisie du poids");
      riSousMenu_bt10.setName("riSousMenu_bt10");
      riSousMenu_bt10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt10ActionPerformed(e);
        }
      });
      riSousMenu10.add(riSousMenu_bt10);
    }
    
    // ======== riSousMenu21 ========
    {
      riSousMenu21.setName("riSousMenu21");
      
      // ---- riSousMenu_bt21 ----
      riSousMenu_bt21.setText("Affaire");
      riSousMenu_bt21.setName("riSousMenu_bt21");
      riSousMenu_bt21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt221ActionPerformed(e);
        }
      });
      riSousMenu21.add(riSousMenu_bt21);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- menuItem1 ----
      menuItem1.setText("Choix unit\u00e9 ");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD2.add(menuItem1);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_19);
    }
    
    // ---- OBJ_150 ----
    OBJ_150.setIcon(new ImageIcon("images/ppromo.gif"));
    OBJ_150.setToolTipText("Article en promotion");
    OBJ_150.setName("OBJ_150");
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- appliquer ----
      appliquer.setText("Appliquer");
      appliquer.setToolTipText("Appliquer le prix flash comme prix net de l'article");
      appliquer.setName("appliquer");
      appliquer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          appliquerActionPerformed(e);
        }
      });
      BTD3.add(appliquer);
    }
    
    // ---- L1TVA0 ----
    L1TVA0.setText("@WLT1@");
    L1TVA0.setFont(L1TVA0.getFont().deriveFont(L1TVA0.getFont().getSize() - 1f));
    L1TVA0.setName("L1TVA0");
    
    // ---- L1TVA_1 ----
    L1TVA_1.setText("@WLT2@");
    L1TVA_1.setFont(L1TVA_1.getFont().deriveFont(L1TVA_1.getFont().getSize() - 1f));
    L1TVA_1.setName("L1TVA_1");
    
    // ---- L1TVA_2 ----
    L1TVA_2.setText("@WLT3@");
    L1TVA_2.setFont(L1TVA_2.getFont().deriveFont(L1TVA_2.getFont().getSize() - 1f));
    L1TVA_2.setName("L1TVA_2");
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(L1TVA0);
    buttonGroup1.add(L1TVA_1);
    buttonGroup1.add(L1TVA_2);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JButton OBJ_64;
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu23;
  private RiSousMenu_bt riSousMenu_bt23;
  private RiSousMenu riSousMenu26;
  private RiSousMenu_bt riSousMenu_bt26;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu25;
  private RiSousMenu_bt riSousMenu_bt25;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private JPanel p_contenu;
  private JPanel panel7;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private XRiTextField L1LIB3;
  private XRiTextField L1LIB4;
  private XRiTextField WASB;
  private JLabel OBJ_153;
  private XRiTextField CPL_doublon_125;
  private JLabel OBJ_237;
  private JPanel panel4;
  private XRiTextField L1CNDX;
  private XRiTextField L1QTEX;
  private RiZoneSortie L1QTCX;
  private XRiTextField L1UNV;
  private JLabel OBJ_129;
  private JLabel OBJ_132;
  private XRiTextField LPPDBR;
  private JLabel OBJ_137;
  private JLabel OBJ_136;
  private JLabel OBJ_140;
  private JLabel label14;
  private XRiCheckBox WSER;
  private RiZoneSortie OBJ_130;
  private XRiTextField A1UNL;
  private XRiTextField A1UNV;
  private JLabel OBJ_138;
  private JLabel OBJ_131;
  private RiZoneSortie A1KCS;
  private JToggleButton A1UCS;
  private XRiTextField LPNBC;
  private XRiTextField L1CPL;
  private JPanel panel5;
  private XRiTextField L1PVBX;
  private XRiTextField L1PVNX;
  private JLabel OBJ_158;
  private XRiComboBox WSPR;
  private JLabel OBJ_155;
  private JPanel panel9;
  private JLabel OBJ_227;
  private JLabel WPVCX;
  private JLabel OBJ_161;
  private XRiTextField L1IN2;
  private XRiSpinner UTAR;
  private JLabel OBJ_159;
  private XRiTextField WREM1;
  private XRiTextField WREM2;
  private XRiTextField WREM3;
  private JLabel OBJ_168;
  private XRiTextField L1COEX;
  private JPanel panel1;
  private RiZoneSortie T1CNV;
  private RiZoneSortie T1TRA;
  private RiZoneSortie WCCD;
  private RiZoneSortie T1RATR;
  private RiZoneSortie WTCD;
  private SNBoutonDetail OBJ_178;
  private JLabel OBJ_160;
  private SNBoutonDetail OBJ_176;
  private JLabel OBJ_229;
  private JLabel OBJ_230;
  private SNBoutonDetail flash;
  private XRiComboBox L1IN18;
  private SNBoutonDetail nego;
  private SNBoutonDetail OBJ_177;
  private XRiComboBox L1IN19;
  private XRiComboBox L1IN20;
  private RiZoneSortie cnvchantier;
  private JPanel panel15;
  private JLabel OBJ_151;
  private XRiTextField L20NAT;
  private JLabel OBJ_120;
  private XRiTextField L1IN3;
  private JLabel OBJ_193;
  private XRiTextField L1TP1;
  private JLabel OBJ_195;
  private XRiTextField L1TP2;
  private JLabel OBJ_197;
  private XRiTextField L1TP3;
  private JLabel OBJ_199;
  private XRiTextField L1TP4;
  private JLabel OBJ_201;
  private XRiTextField L1TP5;
  private JLabel OBJ_147;
  private XRiTextField L1IN11;
  private XRiCheckBox L1TNC;
  private XRiCheckBox L1IN12;
  private XRiCheckBox L1TPF;
  private XRiCheckBox L1IN1;
  private JLabel OBJ_174;
  private JLabel OBJ_184;
  private XRiTextField L1REP;
  private JComboBox L1TVA;
  private XRiTextField L1IN17;
  private JLabel OBJ_148;
  private XRiTextField L1IN21;
  private JLabel OBJ_121;
  private JPanel panel6;
  private RiZoneSortie WDATTX;
  private JLabel OBJ_56;
  private JLabel OBJ_109;
  private RiZoneSortie WDELX;
  private XRiCalendrier L1DLPX;
  private XRiCalendrier WDLSX;
  private SNBoutonLeger bt_GA;
  private JLabel label12;
  private JLabel label13;
  private JLabel label11;
  private JLabel label2;
  private JPanel panelDimension;
  private XRiTextField L2NBRX;
  private JLabel OBJ_141;
  private JLabel l_x3;
  private JPanel psurface;
  private JLabel label17;
  private JTextField surface1;
  private JLabel l_x4;
  private JTextField surface2;
  private JPanel plongueur;
  private JLabel label16;
  private JTextField longueur1;
  private XRiComboBox A1IN30;
  private XRiComboBox A1IN28;
  private XRiComboBox A1IN29;
  private JPanel pvolume;
  private JTextField volume1;
  private JLabel l_x1;
  private JTextField volume2;
  private JLabel l_x2;
  private JTextField volume3;
  private JLabel label15;
  private JPanel p_stock;
  private JPanel p_S7;
  private JLabel disp_7;
  private RiZoneSortie WDISX_7;
  private JLabel commande_7;
  private RiZoneSortie WRESX_7;
  private JLabel att_7;
  private RiZoneSortie WATTX_7;
  private JLabel attt_7;
  private RiZoneSortie WAFFX_7;
  private JLabel label3;
  private SNBoutonDetail riBoutonDetailListe1;
  private JPanel p_S1;
  private JLabel disp_1;
  private RiZoneSortie WDISX_1;
  private RiZoneSortie WSTKX_1;
  private JLabel stock_1;
  private RiZoneSortie WRESX_1;
  private JLabel commande_8;
  private RiZoneSortie WAFFX_1;
  private JLabel OBJ_57;
  private RiZoneSortie WATTX_1;
  private JLabel att_1;
  private JLabel OBJ_94;
  private JLabel OBJ_95;
  private JLabel OBJ_96;
  private JPanel p_S5;
  private JLabel disp_5;
  private RiZoneSortie WDISX_5;
  private RiZoneSortie WSTKX_5;
  private JLabel stock_5;
  private RiZoneSortie WATTX_5;
  private JLabel att_5;
  private JLabel OBJ_97;
  private JLabel reser_5;
  private RiZoneSortie WAFFX_5;
  private JLabel OBJ_98;
  private JLabel OBJ_99;
  private RiZoneSortie WCNRX_5;
  private JLabel commande_5;
  private RiZoneSortie WSTTX_5;
  private JLabel OBJ_101;
  private JLabel OBJ_55;
  private JPanel panel16;
  private XRiCheckBox L1TE1;
  private XRiCheckBox L1TE2;
  private XRiCheckBox L1TE3;
  private XRiCheckBox L1TE4;
  private JPanel p_marge;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private RiZoneSortie WMMR;
  private RiZoneSortie WMMT;
  private RiZoneSortie WMAC;
  private JLabel label8;
  private JLabel label9;
  private JLabel label18;
  private RiZoneSortie WPMR;
  private RiZoneSortie WPMT;
  private RiZoneSortie WPAC;
  private JLabel label19;
  private JLabel label20;
  private JLabel label21;
  private RiZoneSortie WTPRL;
  private RiZoneSortie L1MHT;
  private RiZoneSortie WTPVL;
  private JLabel marge;
  private JMenuBar barre_tete;
  private JPanel panel2;
  private RiZoneSortie OBJ_192;
  private RiZoneSortie OBJ_169;
  private RiZoneSortie WCOD;
  private RiZoneSortie WNLI;
  private JLabel label10;
  private JLabel OBJ_115;
  private RiZoneSortie WARTT;
  private JLabel OBJ_118;
  private XRiTextField L1MAG;
  private JLabel label1;
  private JLabel etat;
  private JLabel bt_ecran;
  private SNBoutonDetail riBoutonDetail3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu FCT1;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JLabel OBJ_208;
  private JLabel OBJ_236;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private JPopupMenu BTD2;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_19;
  private JLabel OBJ_150;
  private JPopupMenu BTD3;
  private JMenuItem appliquer;
  private XRiRadioButton L1TVA0;
  private XRiRadioButton L1TVA_1;
  private XRiRadioButton L1TVA_2;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
