
package ri.serien.libecranrpg.vgvm.VGVM50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/*
 * Created by JFormDesigner on Wed Nov 10 08:46:41 CET 2010
 */

/**
 * @author Stéphane Vénéri
 */
public class VGVM50FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WT301_Top = { "WT301", "WT302", "WT303", "WT304", "WT305", "WT306", "WT307", "WT308", "WT309", "WT310", };
  private String[] _WT301_Title = { "TITLD", };
  private String[][] _WT301_Data =
      { { "L301", }, { "L302", }, { "L303", }, { "L304", }, { "L305", }, { "L306", }, { "L307", }, { "L308", }, { "L309", }, { "L310", }, };
  private int[] _WT301_Width = { 501, };
  
  public VGVM50FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    initDiverses();
    WT301.setAspectTable(_WT301_Top, _WT301_Title, _WT301_Data, _WT301_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_36.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRTTAR@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENCO@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Tarif lot : @P50LOT@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDEV@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DXDVL@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(),_WT301_Top);
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    OBJ_27.setVisible(lexique.isPresent("WDEV"));
    OBJ_36.setVisible(lexique.isPresent("WRTTAR"));
    OBJ_35.setVisible(lexique.isPresent("WRTTAR"));
    OBJ_28.setVisible(lexique.isPresent("DXDVL"));
    OBJ_43.setVisible(lexique.isTrue("95"));
    OBJ_28.setVisible(lexique.isTrue("95"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Gestion des tarifs articles et devises - @P50ART@ - @A1LIB@"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    bouton_fin.setIcon(lexique.chargerImage("images/fin_p.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed() {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt7ActionPerformed() {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void WT301MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WT301_Top, "1", "ENTER", e);
    if (WT301.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "1", "Enter");
    WT301.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "2", "Enter");
    WT301.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "4", "Enter");
    WT301.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "5", "Enter");
    WT301.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WT301_Top, "6", "Enter");
    WT301.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  /*
    private void OBJ_20ActionPerformed(ActionEvent e) {
      lexique.WatchHelp(BTD.getInvoker().getName());
    }
  
    private void OBJ_21ActionPerformed(ActionEvent e) {
      lexique.HostCursorPut(BTD.getInvoker().getName());
      lexique.HostScreenSendKey(this, "F4", false);
    }
  
    private void OBJ_5ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F3", false);
    }
  
    private void OBJ_7ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F5", false);
    }
  
    private void OBJ_8ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F12", false);
    }
  
    private void OBJ_9ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F13", false);
    }
  
    private void OBJ_10ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F14", false);
    }
  
    private void OBJ_11ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F15", false);
    }
  
    private void OBJ_12ActionPerformed(ActionEvent e) {
      lexique.HostScreenSendKey(this, "F16", false);
    }
  */
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    navig_fin = new RiMenu();
    bouton_fin = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WT301 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_35 = new JLabel();
    OBJ_36 = new RiZoneSortie();
    OBJ_32 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    P_Centre = new JPanel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 300));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Tarifs par devise");
            bouton_retour.setToolTipText("Tarifs par devise");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);

          //======== navig_fin ========
          {
            navig_fin.setName("navig_fin");

            //---- bouton_fin ----
            bouton_fin.setText("Fiche article");
            bouton_fin.setName("bouton_fin");
            bouton_fin.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riMenu_bt3ActionPerformed(e);
              }
            });
            navig_fin.add(bouton_fin);
          }
          menus_bas.add(navig_fin);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 350));
            menus_haut.setPreferredSize(new Dimension(160, 350));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt1 ----
              riMenu_bt1.setText("@V01F@");
              riMenu_bt1.setPreferredSize(new Dimension(167, 50));
              riMenu_bt1.setMinimumSize(new Dimension(167, 50));
              riMenu_bt1.setMaximumSize(new Dimension(170, 50));
              riMenu_bt1.setFont(riMenu_bt1.getFont().deriveFont(riMenu_bt1.getFont().getSize() + 2f));
              riMenu_bt1.setName("riMenu_bt1");
              riMenu_V01F.add(riMenu_bt1);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("conditions de ventes");
              riSousMenu_bt6.setToolTipText("conditions de ventes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed();
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("conditions / quantit\u00e9");
              riSousMenu_bt7.setToolTipText("conditions / quantit\u00e9");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed();
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel4 ========
        {
          panel4.setBorder(new TitledBorder("Historique de tarifs"));
          panel4.setOpaque(false);
          panel4.setName("panel4");
          panel4.setLayout(null);

          //======== SCROLLPANE_LIST2 ========
          {
            SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

            //---- WT301 ----
            WT301.setComponentPopupMenu(BTD);
            WT301.setName("WT301");
            WT301.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WT301MouseClicked(e);
              }
            });
            SCROLLPANE_LIST2.setViewportView(WT301);
          }
          panel4.add(SCROLLPANE_LIST2);
          SCROLLPANE_LIST2.setBounds(20, 35, 519, 190);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel4.add(BT_PGUP);
          BT_PGUP.setBounds(545, 35, 25, 85);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel4.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(545, 140, 25, 84);

          //---- OBJ_35 ----
          OBJ_35.setText("Rattachement");
          OBJ_35.setName("OBJ_35");
          panel4.add(OBJ_35);
          OBJ_35.setBounds(25, 232, 95, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("@WRTTAR@");
          OBJ_36.setName("OBJ_36");
          panel4.add(OBJ_36);
          OBJ_36.setBounds(120, 230, 76, OBJ_36.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("@ENCO@");
          OBJ_32.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_32.setName("OBJ_32");
          panel4.add(OBJ_32);
          OBJ_32.setBounds(455, -2, 129, 20);

          //---- OBJ_43 ----
          OBJ_43.setText("Tarif lot : @P50LOT@");
          OBJ_43.setName("OBJ_43");
          panel4.add(OBJ_43);
          OBJ_43.setBounds(300, 230, 260, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("@WDEV@");
          OBJ_27.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_27.setName("OBJ_27");
          panel4.add(OBJ_27);
          OBJ_27.setBounds(462, 245, 75, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("@DXDVL@");
          OBJ_28.setName("OBJ_28");
          panel4.add(OBJ_28);
          OBJ_28.setBounds(300, 245, 165, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 280, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGap(0, 10, Short.MAX_VALUE)
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGap(0, 10, Short.MAX_VALUE)
      );
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_16 ----
      OBJ_16.setText("Modifier");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_17 ----
      OBJ_17.setText("Annuler");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Interroger");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("D\u00e9tail devise");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private RiMenu navig_fin;
  private RiMenu_bt bouton_fin;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WT301;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel OBJ_35;
  private RiZoneSortie OBJ_36;
  private JLabel OBJ_32;
  private JLabel OBJ_43;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JPanel P_Centre;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
