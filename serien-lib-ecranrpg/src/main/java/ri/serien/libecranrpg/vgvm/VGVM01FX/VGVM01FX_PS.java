
package ri.serien.libecranrpg.vgvm.VGVM01FX;
// Nom Fichier: b_VGVM01FM_WGVM01PS_WGVM01F_186.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_PS extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] PST13_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] PST7_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] PST12_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };
  private String[] PST28_Value = { "1", "2", };
  private String[] PST9_Value = { "1", "0", };
  private String[] PST4_Value = { "1", "2", };
  private String[] PST20_Value = { "1", "2", };
  
  public VGVM01FX_PS(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    PST13.setValeurs(PST13_Value, null);
    PST7.setValeurs(PST7_Value, null);
    PST12.setValeurs(PST12_Value, null);
    PST28.setValeurs(PST28_Value, null);
    PST20.setValeurs(PST20_Value, null);
    PST9.setValeurs(PST9_Value, null);
    PST4.setValeurs(PST4_Value, null);
    PST32.setValeursSelection("1", " ");
    PST30.setValeursSelection("1", " ");
    PST17.setValeursSelection("1", " ");
    PST16.setValeursSelection("1", " ");
    PST26.setValeursSelection("1", " ");
    PST23.setValeursSelection("1", " ");
    PST18.setValeursSelection("1", " ");
    PST19.setValeursSelection("1", " ");
    PST22.setValeursSelection("1", " ");
    PST15.setValeursSelection("1", " ");
    PST11.setValeursSelection("1", " ");
    PST37.setValeursSelection("1", " ");
    PST36.setValeursSelection("1", " ");
    PST34.setValeursSelection("1", " ");
    PST5.setValeursSelection("1", " ");
    PST10.setValeursSelection("1", " ");
    PST29.setValeursSelection("1", " ");
    PST14.setValeursSelection("1", " ");
    PST33.setValeursSelection("1", " ");
    PST27.setValeursSelection("1", " ");
    PST1.setValeursSelection("1", " ");
    PST25.setValeursSelection("1", " ");
    PST3.setValeursSelection("1", " ");
    PST2.setValeursSelection("1", " ");
    PST21.setValeursSelection("1", " ");
    PST24.setValeursSelection("1", " ");
    PST31.setValeursSelection("1", " ");
    PST8.setValeursSelection("1", " ");
    PST6.setValeursSelection("1", " ");
    PST35.setValeursSelection("1", " ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("Personnalisation de @LOCGRP/-1/@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_48.setVisible(lexique.isPresent("V01F"));
    
    // TODO Icones
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1");
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@");
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@");
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@");
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@");
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@");
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    OBJ_44 = new JLabel();
    INDIND = new XRiTextField();
    OBJ_48 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_46 = new JLabel();
    INDTYP = new XRiTextField();
    bt_Fonctions = new JButton();
    OBJ_43 = new JLabel();
    P_Centre = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    panel2 = new JPanel();
    PST4 = new XRiComboBox();
    PST9 = new XRiComboBox();
    PST35 = new XRiCheckBox();
    PST6 = new XRiCheckBox();
    PST8 = new XRiCheckBox();
    PST31 = new XRiCheckBox();
    PST24 = new XRiCheckBox();
    PST21 = new XRiCheckBox();
    PST2 = new XRiCheckBox();
    PST3 = new XRiCheckBox();
    PST25 = new XRiCheckBox();
    PST1 = new XRiCheckBox();
    PST27 = new XRiCheckBox();
    PST33 = new XRiCheckBox();
    PST14 = new XRiCheckBox();
    PST29 = new XRiCheckBox();
    PST10 = new XRiCheckBox();
    OBJ_58 = new JLabel();
    PST5 = new XRiCheckBox();
    OBJ_62 = new JLabel();
    panel3 = new JPanel();
    PST20 = new XRiComboBox();
    PST28 = new XRiComboBox();
    PST34 = new XRiCheckBox();
    PST36 = new XRiCheckBox();
    OBJ_54 = new JLabel();
    PST37 = new XRiCheckBox();
    OBJ_57 = new JLabel();
    PST11 = new XRiCheckBox();
    PST15 = new XRiCheckBox();
    PST22 = new XRiCheckBox();
    PST19 = new XRiCheckBox();
    PST18 = new XRiCheckBox();
    PST23 = new XRiCheckBox();
    PST26 = new XRiCheckBox();
    PST16 = new XRiCheckBox();
    PST17 = new XRiCheckBox();
    PST30 = new XRiCheckBox();
    OBJ_55 = new JLabel();
    PST32 = new XRiCheckBox();
    PST12 = new XRiComboBox();
    PST7 = new XRiComboBox();
    PST13 = new XRiComboBox();
    OBJ_68 = new JLabel();
    OBJ_65 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Edition des param\u00e8tres");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Retour");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("R\u00e9activation");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);

      //---- OBJ_15 ----
      OBJ_15.setText("Suppression");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      CMD.add(OBJ_15);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("Personnalisation de @LOCGRP/-1/@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- OBJ_44 ----
        OBJ_44.setText("Etablissement");
        OBJ_44.setName("OBJ_44");

        //---- INDIND ----
        INDIND.setComponentPopupMenu(BTD);
        INDIND.setName("INDIND");

        //---- OBJ_48 ----
        OBJ_48.setText("Ordre");
        OBJ_48.setName("OBJ_48");

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTD);
        INDETB.setName("INDETB");

        //---- OBJ_46 ----
        OBJ_46.setText("Code");
        OBJ_46.setName("OBJ_46");

        //---- INDTYP ----
        INDTYP.setComponentPopupMenu(BTD);
        INDTYP.setName("INDTYP");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- OBJ_43 ----
        OBJ_43.setText("@V01F@");
        OBJ_43.setFont(new Font("sansserif", Font.BOLD, 12));
        OBJ_43.setName("OBJ_43");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGap(8, 8, 8)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(155, 155, 155)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE))
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(240, 240, 240)
                  .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
              .addGap(20, 20, 20)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(30, 30, 30)
                  .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
              .addGap(15, 15, 15)
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(35, 35, 35)
                  .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 403, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup()
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addGroup(P_InfosLayout.createParallelGroup()
                    .addGroup(P_InfosLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(P_InfosLayout.createSequentialGroup()
                      .addGap(4, 4, 4)
                      .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addGroup(P_InfosLayout.createParallelGroup()
                    .addGroup(P_InfosLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(3, 3, 3)
                  .addGroup(P_InfosLayout.createParallelGroup()
                    .addGroup(P_InfosLayout.createSequentialGroup()
                      .addGap(5, 5, 5)
                      .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                    .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addComponent(bt_Fonctions))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //======== xTitledPanel1 ========
      {
        xTitledPanel1.setTitle("Personnalisation");
        xTitledPanel1.setBorder(new DropShadowBorder());
        xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
        xTitledPanel1.setName("xTitledPanel1");
        Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
        xTitledPanel1ContentContainer.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder(""));
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- PST4 ----
          PST4.setModel(new DefaultComboBoxModel(new String[] {
            "obligatoire",
            "lien magasin sur bon"
          }));
          PST4.setComponentPopupMenu(BTD);
          PST4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST4.setName("PST4");
          panel2.add(PST4);
          PST4.setBounds(135, 90, 155, PST4.getPreferredSize().height);

          //---- PST9 ----
          PST9.setModel(new DefaultComboBoxModel(new String[] {
            "livraison",
            "exp\u00e9dition"
          }));
          PST9.setComponentPopupMenu(BTD);
          PST9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST9.setName("PST9");
          panel2.add(PST9);
          PST9.setBounds(135, 205, 90, PST9.getPreferredSize().height);

          //---- PST35 ----
          PST35.setText("Edition du code article sur les \u00e9tiquettes de colis");
          PST35.setComponentPopupMenu(BTD);
          PST35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST35.setName("PST35");
          panel2.add(PST35);
          PST35.setBounds(25, 470, 305, 20);

          //---- PST6 ----
          PST6.setText("Edition de l'adresse de livraison syst\u00e9matique");
          PST6.setComponentPopupMenu(BTD);
          PST6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST6.setName("PST6");
          panel2.add(PST6);
          PST6.setBounds(25, 150, 305, 20);

          //---- PST8 ----
          PST8.setText("Occultation de la date de cr\u00e9ation sur traite");
          PST8.setComponentPopupMenu(BTD);
          PST8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST8.setName("PST8");
          panel2.add(PST8);
          PST8.setBounds(25, 175, 305, 20);

          //---- PST31 ----
          PST31.setText("Recherche des prix pour injection des bons");
          PST31.setComponentPopupMenu(BTD);
          PST31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST31.setName("PST31");
          panel2.add(PST31);
          PST31.setBounds(25, 420, 305, 20);

          //---- PST24 ----
          PST24.setText("Commission repr\u00e9sentant sur encaissement");
          PST24.setComponentPopupMenu(BTD);
          PST24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST24.setName("PST24");
          panel2.add(PST24);
          PST24.setBounds(25, 320, 305, 20);

          //---- PST21 ----
          PST21.setText("Edition des relicats sur bons exp\u00e9diteur");
          PST21.setComponentPopupMenu(BTD);
          PST21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST21.setName("PST21");
          panel2.add(PST21);
          PST21.setBounds(25, 270, 305, 20);

          //---- PST2 ----
          PST2.setText("Edition de num\u00e9ro de s\u00e9rie sur facture");
          PST2.setComponentPopupMenu(BTD);
          PST2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST2.setName("PST2");
          panel2.add(PST2);
          PST2.setBounds(25, 35, 305, 20);

          //---- PST3 ----
          PST3.setText("Calcul des marges \u00e0 partir du PUMP");
          PST3.setComponentPopupMenu(BTD);
          PST3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST3.setName("PST3");
          panel2.add(PST3);
          PST3.setBounds(25, 60, 305, 20);

          //---- PST25 ----
          PST25.setText("Vente impossible si PUMP \u00e9gal \u00e0 0");
          PST25.setComponentPopupMenu(BTD);
          PST25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST25.setName("PST25");
          panel2.add(PST25);
          PST25.setBounds(25, 345, 305, 20);

          //---- PST1 ----
          PST1.setText("Edition de num\u00e9ro de s\u00e9rie sur bon");
          PST1.setComponentPopupMenu(BTD);
          PST1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST1.setName("PST1");
          panel2.add(PST1);
          PST1.setBounds(25, 10, 305, 20);

          //---- PST27 ----
          PST27.setText("Affectation commande sur attendu");
          PST27.setComponentPopupMenu(BTD);
          PST27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST27.setName("PST27");
          panel2.add(PST27);
          PST27.setBounds(25, 370, 305, 20);

          //---- PST33 ----
          PST33.setText("Port r\u00e9percut\u00e9 sur lignes \u00e0 l'\u00e9dition");
          PST33.setComponentPopupMenu(BTD);
          PST33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST33.setName("PST33");
          panel2.add(PST33);
          PST33.setBounds(25, 445, 305, 20);

          //---- PST14 ----
          PST14.setText("Total quantit\u00e9 en pied de facture");
          PST14.setComponentPopupMenu(BTD);
          PST14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST14.setName("PST14");
          panel2.add(PST14);
          PST14.setBounds(25, 295, 305, 20);

          //---- PST29 ----
          PST29.setText("Statistiques sur les commandes actives");
          PST29.setComponentPopupMenu(BTD);
          PST29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST29.setName("PST29");
          panel2.add(PST29);
          PST29.setBounds(25, 395, 305, 20);

          //---- PST10 ----
          PST10.setText("Mention \"L.C.R.\" sur les traites");
          PST10.setComponentPopupMenu(BTD);
          PST10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST10.setName("PST10");
          panel2.add(PST10);
          PST10.setBounds(25, 245, 305, 20);

          //---- OBJ_58 ----
          OBJ_58.setText("Section analytique");
          OBJ_58.setName("OBJ_58");
          panel2.add(OBJ_58);
          OBJ_58.setBounds(25, 93, 114, 20);

          //---- PST5 ----
          PST5.setText("Affaire obligatoire");
          PST5.setComponentPopupMenu(BTD);
          PST5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST5.setName("PST5");
          panel2.add(PST5);
          PST5.setBounds(25, 125, 305, 20);

          //---- OBJ_62 ----
          OBJ_62.setText("D\u00e9livrement par");
          OBJ_62.setName("OBJ_62");
          panel2.add(OBJ_62);
          OBJ_62.setBounds(25, 210, 99, 20);
        }
        xTitledPanel1ContentContainer.add(panel2);
        panel2.setBounds(10, 5, 385, 500);

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- PST20 ----
          PST20.setModel(new DefaultComboBoxModel(new String[] {
            "obligatoire sur bons et factures",
            "li\u00e9 au repr\u00e9sentant"
          }));
          PST20.setComponentPopupMenu(BTD);
          PST20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST20.setName("PST20");
          panel3.add(PST20);
          PST20.setBounds(140, 287, 230, PST20.getPreferredSize().height);

          //---- PST28 ----
          PST28.setModel(new DefaultComboBoxModel(new String[] {
            "sur relev\u00e9s",
            "sur factures"
          }));
          PST28.setComponentPopupMenu(BTD);
          PST28.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST28.setName("PST28");
          panel3.add(PST28);
          PST28.setBounds(140, 342, 110, PST28.getPreferredSize().height);

          //---- PST34 ----
          PST34.setText("Edition des articles par code douanier sur export");
          PST34.setComponentPopupMenu(BTD);
          PST34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST34.setName("PST34");
          panel3.add(PST34);
          PST34.setBounds(25, 420, 320, 20);

          //---- PST36 ----
          PST36.setText("Edition du d\u00e9tail des nomenclatures sur factures");
          PST36.setComponentPopupMenu(BTD);
          PST36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST36.setName("PST36");
          panel3.add(PST36);
          PST36.setBounds(25, 445, 320, 20);

          //---- OBJ_54 ----
          OBJ_54.setText("Nombre de caract\u00e8res de regroupement");
          OBJ_54.setName("OBJ_54");
          panel3.add(OBJ_54);
          OBJ_54.setBounds(25, 10, 244, 20);

          //---- PST37 ----
          PST37.setText("Edition des adresses de stockage sur factures");
          PST37.setComponentPopupMenu(BTD);
          PST37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST37.setName("PST37");
          panel3.add(PST37);
          PST37.setBounds(25, 470, 320, 20);

          //---- OBJ_57 ----
          OBJ_57.setText("Nombre de carac\u00e8res du code article");
          OBJ_57.setName("OBJ_57");
          panel3.add(OBJ_57);
          OBJ_57.setBounds(25, 60, 226, 20);

          //---- PST11 ----
          PST11.setText("Valorisation de remise sur ligne de facture");
          PST11.setComponentPopupMenu(BTD);
          PST11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST11.setName("PST11");
          panel3.add(PST11);
          PST11.setBounds(25, 260, 320, 20);

          //---- PST15 ----
          PST15.setText("Commentaire pour ligne article en rupture");
          PST15.setComponentPopupMenu(BTD);
          PST15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST15.setName("PST15");
          panel3.add(PST15);
          PST15.setBounds(25, 110, 320, 20);

          //---- PST22 ----
          PST22.setText("Regroupement lignes sur factures actif");
          PST22.setComponentPopupMenu(BTD);
          PST22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST22.setName("PST22");
          panel3.add(PST22);
          PST22.setBounds(25, 85, 320, 20);

          //---- PST19 ----
          PST19.setText("Num\u00e9rotation automatique des clients");
          PST19.setComponentPopupMenu(BTD);
          PST19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST19.setName("PST19");
          panel3.add(PST19);
          PST19.setBounds(25, 235, 320, 20);

          //---- PST18 ----
          PST18.setText("Repr\u00e9sentant obligatoire pour client");
          PST18.setComponentPopupMenu(BTD);
          PST18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST18.setName("PST18");
          panel3.add(PST18);
          PST18.setBounds(25, 185, 320, 20);

          //---- PST23 ----
          PST23.setText("Statistiques en quantit\u00e9s pour client");
          PST23.setComponentPopupMenu(BTD);
          PST23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST23.setName("PST23");
          panel3.add(PST23);
          PST23.setBounds(25, 210, 320, 20);

          //---- PST26 ----
          PST26.setText("Vente inf\u00e9rieure \u00e0 PUMP impossible");
          PST26.setComponentPopupMenu(BTD);
          PST26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST26.setName("PST26");
          panel3.add(PST26);
          PST26.setBounds(25, 320, 320, 20);

          //---- PST16 ----
          PST16.setText("Enchainer BL et \u00e9tiquettes colis");
          PST16.setComponentPopupMenu(BTD);
          PST16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST16.setName("PST16");
          panel3.add(PST16);
          PST16.setBounds(25, 135, 320, 20);

          //---- PST17 ----
          PST17.setText("Cat\u00e9gorie obligatoire pour client");
          PST17.setComponentPopupMenu(BTD);
          PST17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST17.setName("PST17");
          panel3.add(PST17);
          PST17.setBounds(25, 160, 320, 20);

          //---- PST30 ----
          PST30.setText("Gestion des articles par lots");
          PST30.setComponentPopupMenu(BTD);
          PST30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST30.setName("PST30");
          panel3.add(PST30);
          PST30.setBounds(25, 370, 320, 20);

          //---- OBJ_55 ----
          OBJ_55.setText("Code article sur produit");
          OBJ_55.setName("OBJ_55");
          panel3.add(OBJ_55);
          OBJ_55.setBounds(25, 35, 141, 20);

          //---- PST32 ----
          PST32.setText("Utilisation CNV sp\u00e9ciales");
          PST32.setComponentPopupMenu(BTD);
          PST32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST32.setName("PST32");
          panel3.add(PST32);
          PST32.setBounds(25, 395, 320, 20);

          //---- PST12 ----
          PST12.setModel(new DefaultComboBoxModel(new String[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
          }));
          PST12.setComponentPopupMenu(BTD);
          PST12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST12.setName("PST12");
          panel3.add(PST12);
          PST12.setBounds(255, 7, 40, PST12.getPreferredSize().height);

          //---- PST7 ----
          PST7.setModel(new DefaultComboBoxModel(new String[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
          }));
          PST7.setComponentPopupMenu(BTD);
          PST7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST7.setName("PST7");
          panel3.add(PST7);
          PST7.setBounds(255, 32, 40, PST7.getPreferredSize().height);

          //---- PST13 ----
          PST13.setModel(new DefaultComboBoxModel(new String[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9"
          }));
          PST13.setComponentPopupMenu(BTD);
          PST13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PST13.setName("PST13");
          panel3.add(PST13);
          PST13.setBounds(255, 57, 40, PST13.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Pas de caviardage");
          OBJ_68.setName("OBJ_68");
          panel3.add(OBJ_68);
          OBJ_68.setBounds(25, 345, 118, 20);

          //---- OBJ_65 ----
          OBJ_65.setText("Code vendeur");
          OBJ_65.setName("OBJ_65");
          panel3.add(OBJ_65);
          OBJ_65.setBounds(25, 290, 130, 20);
        }
        xTitledPanel1ContentContainer.add(panel3);
        panel3.setBounds(425, 5, 385, 500);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
            Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = xTitledPanel1ContentContainer.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
          xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
        }
      }
      P_Centre.add(xTitledPanel1);
      xTitledPanel1.setBounds(30, 20, 825, 540);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel OBJ_44;
  private XRiTextField INDIND;
  private JLabel OBJ_48;
  private XRiTextField INDETB;
  private JLabel OBJ_46;
  private XRiTextField INDTYP;
  private JButton bt_Fonctions;
  private JLabel OBJ_43;
  private JPanel P_Centre;
  private JXTitledPanel xTitledPanel1;
  private JPanel panel2;
  private XRiComboBox PST4;
  private XRiComboBox PST9;
  private XRiCheckBox PST35;
  private XRiCheckBox PST6;
  private XRiCheckBox PST8;
  private XRiCheckBox PST31;
  private XRiCheckBox PST24;
  private XRiCheckBox PST21;
  private XRiCheckBox PST2;
  private XRiCheckBox PST3;
  private XRiCheckBox PST25;
  private XRiCheckBox PST1;
  private XRiCheckBox PST27;
  private XRiCheckBox PST33;
  private XRiCheckBox PST14;
  private XRiCheckBox PST29;
  private XRiCheckBox PST10;
  private JLabel OBJ_58;
  private XRiCheckBox PST5;
  private JLabel OBJ_62;
  private JPanel panel3;
  private XRiComboBox PST20;
  private XRiComboBox PST28;
  private XRiCheckBox PST34;
  private XRiCheckBox PST36;
  private JLabel OBJ_54;
  private XRiCheckBox PST37;
  private JLabel OBJ_57;
  private XRiCheckBox PST11;
  private XRiCheckBox PST15;
  private XRiCheckBox PST22;
  private XRiCheckBox PST19;
  private XRiCheckBox PST18;
  private XRiCheckBox PST23;
  private XRiCheckBox PST26;
  private XRiCheckBox PST16;
  private XRiCheckBox PST17;
  private XRiCheckBox PST30;
  private JLabel OBJ_55;
  private XRiCheckBox PST32;
  private XRiComboBox PST12;
  private XRiComboBox PST7;
  private XRiComboBox PST13;
  private JLabel OBJ_68;
  private JLabel OBJ_65;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
