
package ri.serien.libecranrpg.vgvm.VGVM114F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM114F_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM114F_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    REPERC.setValeursSelection("X1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@AFFI@")).trim()));
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNOM@")).trim());
    L1ART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1ART@")).trim());
    L1PVC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1PVC@")).trim());
    FRNRO.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@FRNRO@")).trim());
    A1FRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1FRS@")).trim());
    CADEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CADEV@")).trim());
    A1COF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1COF@")).trim());
    OBJ_35.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WKAP@")).trim());
    L1PVC4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1PVC4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    A1COF.setEnabled(lexique.isPresent("A1COF"));
    CADEV.setVisible(lexique.isPresent("CADEV"));
    A1FRS.setEnabled(lexique.isPresent("A1FRS"));
    WMARGL.setEnabled(lexique.isPresent("WMARGL"));
    OBJ_35.setVisible(lexique.isPresent("WKAP"));
    FRNRO.setEnabled(lexique.isPresent("FRNRO"));
    L23KAS.setEnabled(lexique.isPresent("L23KAS"));
    // L23DTX.setEnabled( lexique.isPresent("L23DTX"));
    L23AUT.setEnabled(lexique.isPresent("L23AUT"));
    L23PA4.setEnabled(lexique.isPresent("L23PA4"));
    L1ART.setEnabled(lexique.isPresent("L1ART"));
    OBJ_29.setVisible(lexique.isPresent("FRNOM"));
    OBJ_28.setVisible(lexique.isPresent("A1LIB"));
    L1PVC.setVisible(lexique.isPresent("L1PVC"));
    L1PVC4.setVisible(!L1PVC.isVisible());
    
    // REPERC.setSelected(lexique.HostFieldGetData("REPERC").equalsIgnoreCase("X"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affaires"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (REPERC.isSelected())
    // lexique.HostFieldPutData("REPERC", 0, "X1");
    // else
    // lexique.HostFieldPutData("REPERC", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_28 = new RiZoneSortie();
    OBJ_29 = new RiZoneSortie();
    L1ART = new RiZoneSortie();
    OBJ_46 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_32 = new JLabel();
    L23PA4 = new XRiTextField();
    OBJ_38 = new JLabel();
    L23AUT = new XRiTextField();
    OBJ_36 = new JLabel();
    L1PVC = new RiZoneSortie();
    OBJ_23 = new JLabel();
    FRNRO = new RiZoneSortie();
    WMARGL = new XRiTextField();
    OBJ_37 = new JLabel();
    A1FRS = new RiZoneSortie();
    CADEV = new RiZoneSortie();
    A1COF = new RiZoneSortie();
    OBJ_21 = new JPanel();
    L23DTX = new XRiCalendrier();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    OBJ_34 = new JLabel();
    OBJ_35 = new RiZoneSortie();
    OBJ_41 = new JLabel();
    L23KAS = new XRiTextField();
    REPERC = new XRiCheckBox();
    L23PRA = new XRiTextField();
    L1PVC4 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(830, 310));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@AFFI@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setText("@A1LIB@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(15, 60, 280, OBJ_28.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("@FRNOM@");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(330, 60, 280, OBJ_29.getPreferredSize().height);

          //---- L1ART ----
          L1ART.setComponentPopupMenu(BTD);
          L1ART.setText("@L1ART@");
          L1ART.setName("L1ART");
          panel1.add(L1ART);
          L1ART.setBounds(85, 30, 210, L1ART.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Marge calcul\u00e9e %");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(330, 154, 114, 20);

          //---- OBJ_39 ----
          OBJ_39.setText("Prix de vente");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(330, 124, 100, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Fournisseur");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(330, 32, 91, 20);

          //---- OBJ_32 ----
          OBJ_32.setText("Prix d'achat");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(330, 94, 90, 20);

          //---- L23PA4 ----
          L23PA4.setComponentPopupMenu(BTD);
          L23PA4.setName("L23PA4");
          panel1.add(L23PA4);
          L23PA4.setBounds(445, 90, 90, L23PA4.getPreferredSize().height);

          //---- OBJ_38 ----
          OBJ_38.setText("Autorisation");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(215, 122, 73, 20);

          //---- L23AUT ----
          L23AUT.setComponentPopupMenu(BTD);
          L23AUT.setName("L23AUT");
          panel1.add(L23AUT);
          L23AUT.setBounds(210, 150, 90, L23AUT.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("N\u00b0client");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(15, 122, 50, 20);

          //---- L1PVC ----
          L1PVC.setComponentPopupMenu(BTD);
          L1PVC.setText("@L1PVC@");
          L1PVC.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVC.setName("L1PVC");
          panel1.add(L1PVC);
          L1PVC.setBounds(445, 122, 89, L1PVC.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Article");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(15, 32, 60, 20);

          //---- FRNRO ----
          FRNRO.setComponentPopupMenu(BTD);
          FRNRO.setText("@FRNRO@");
          FRNRO.setName("FRNRO");
          panel1.add(FRNRO);
          FRNRO.setBounds(15, 152, 80, FRNRO.getPreferredSize().height);

          //---- WMARGL ----
          WMARGL.setComponentPopupMenu(BTD);
          WMARGL.setName("WMARGL");
          panel1.add(WMARGL);
          WMARGL.setBounds(445, 150, 56, WMARGL.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Date");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(105, 122, 50, 20);

          //---- A1FRS ----
          A1FRS.setComponentPopupMenu(BTD);
          A1FRS.setText("@A1FRS@");
          A1FRS.setName("A1FRS");
          panel1.add(A1FRS);
          A1FRS.setBounds(470, 30, 60, A1FRS.getPreferredSize().height);

          //---- CADEV ----
          CADEV.setText("@CADEV@");
          CADEV.setName("CADEV");
          panel1.add(CADEV);
          CADEV.setBounds(540, 92, 40, CADEV.getPreferredSize().height);

          //---- A1COF ----
          A1COF.setComponentPopupMenu(BTD);
          A1COF.setText("@A1COF@");
          A1COF.setName("A1COF");
          panel1.add(A1COF);
          A1COF.setBounds(445, 30, 20, A1COF.getPreferredSize().height);

          //======== OBJ_21 ========
          {
            OBJ_21.setBackground(new Color(102, 102, 102));
            OBJ_21.setName("OBJ_21");
            OBJ_21.setLayout(null);
          }
          panel1.add(OBJ_21);
          OBJ_21.setBounds(315, 30, 1, 170);

          //---- L23DTX ----
          L23DTX.setName("L23DTX");
          panel1.add(L23DTX);
          L23DTX.setBounds(100, 150, 105, L23DTX.getPreferredSize().height);

          //---- xTitledSeparator1 ----
          xTitledSeparator1.setTitle("R\u00e9f\u00e9rence de l'autorisation");
          xTitledSeparator1.setName("xTitledSeparator1");
          panel1.add(xTitledSeparator1);
          xTitledSeparator1.setBounds(15, 95, 280, 19);

          //---- xTitledSeparator2 ----
          xTitledSeparator2.setTitle("Coefficient d'approche :");
          xTitledSeparator2.setName("xTitledSeparator2");
          panel1.add(xTitledSeparator2);
          xTitledSeparator2.setBounds(15, 210, 595, 19);

          //---- OBJ_34 ----
          OBJ_34.setText("Moyen");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(15, 240, 62, 20);

          //---- OBJ_35 ----
          OBJ_35.setText("@WKAP@");
          OBJ_35.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_35.setName("OBJ_35");
          panel1.add(OBJ_35);
          OBJ_35.setBounds(100, 235, 60, OBJ_35.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setText("Saisie");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(195, 240, 51, 20);

          //---- L23KAS ----
          L23KAS.setComponentPopupMenu(BTD);
          L23KAS.setName("L23KAS");
          panel1.add(L23KAS);
          L23KAS.setBounds(240, 235, 60, L23KAS.getPreferredSize().height);

          //---- REPERC ----
          REPERC.setText("R\u00e9percut\u00e9e");
          REPERC.setName("REPERC");
          panel1.add(REPERC);
          REPERC.setBounds(210, 185, 90, REPERC.getPreferredSize().height);

          //---- L23PRA ----
          L23PRA.setName("L23PRA");
          panel1.add(L23PRA);
          L23PRA.setBounds(445, 90, 90, L23PRA.getPreferredSize().height);

          //---- L1PVC4 ----
          L1PVC4.setComponentPopupMenu(BTD);
          L1PVC4.setText("@L1PVC4@");
          L1PVC4.setHorizontalAlignment(SwingConstants.RIGHT);
          L1PVC4.setName("L1PVC4");
          panel1.add(L1PVC4);
          L1PVC4.setBounds(445, 122, 89, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_29;
  private RiZoneSortie L1ART;
  private JLabel OBJ_46;
  private JLabel OBJ_39;
  private JLabel OBJ_25;
  private JLabel OBJ_32;
  private XRiTextField L23PA4;
  private JLabel OBJ_38;
  private XRiTextField L23AUT;
  private JLabel OBJ_36;
  private RiZoneSortie L1PVC;
  private JLabel OBJ_23;
  private RiZoneSortie FRNRO;
  private XRiTextField WMARGL;
  private JLabel OBJ_37;
  private RiZoneSortie A1FRS;
  private RiZoneSortie CADEV;
  private RiZoneSortie A1COF;
  private JPanel OBJ_21;
  private XRiCalendrier L23DTX;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private JLabel OBJ_34;
  private RiZoneSortie OBJ_35;
  private JLabel OBJ_41;
  private XRiTextField L23KAS;
  private XRiCheckBox REPERC;
  private XRiTextField L23PRA;
  private RiZoneSortie L1PVC4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
