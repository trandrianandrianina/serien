
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.client.sntypefacturation.SNTypeFacturation;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.sntypeavoir.SNTypeAvoir;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_A1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] WCTRT_Value = { "", "EXT", "DIS", "OPT", "DUP", "LDP", "AFF", "AVR", "AVA" };
  
  /**
   * Constructeur.
   */
  public VGVM11FX_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    initDiverses();
    
    riMenu_bt1.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Création de @TYP11@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    WIN17.setEnabled(lexique.isPresent("WIN17"));
    lbTypeFacture.setVisible(!lexique.HostFieldGetData("CLLP4").trim().equalsIgnoreCase("9999"));
    lbTypeFacture.setEnabled(lbTypeFacture.isVisible());
    
    lbTypeAvoir.setVisible(lexique.isPresent("E1AVR"));
    snTypeAvoir.setVisible(lexique.isPresent("E1AVR"));
    snMagasin.setEnabled(lexique.isPresent("WMAGE"));
    snVendeur.setEnabled(lexique.isPresent("WVDE"));
    snClientLivre.setVisible(!lexique.HostFieldGetData("TEST2").trim().equalsIgnoreCase("R"));
    lbDevise.setVisible(lexique.isPresent("WDEV"));
    snDevise.setVisible(lexique.isPresent("WDEV"));
    WDATEX.setEditable(lexique.isTrue("90"));
    
    riSousMenu2.setVisible(!lexique.HostFieldGetData("TIP11").trim().equalsIgnoreCase("DEVIS:"));
    riMenu1.setVisible(riSousMenu2.isVisible());
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("E1ETB"));
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(idEtablissement);
    
    // Client livré
    snClientLivre.setSession(getSession());
    snClientLivre.setIdEtablissement(idEtablissement);
    snClientLivre.charger(false);
    snClientLivre.setRechercheProspectAutorisee(true);
    snClientLivre.setSelectionParChampRPG(lexique, "E1CLLP", "E1CLLS");
    
    // Client facturé
    snClientFacture.setSession(getSession());
    snClientFacture.setIdEtablissement(idEtablissement);
    snClientFacture.charger(false);
    snClientFacture.setRechercheProspectAutorisee(true);
    snClientFacture.setSelectionParChampRPG(lexique, "E1CLFP", "E1CLFS");
    
    // Vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(idEtablissement);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "WVDE");
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAGE");
    
    // Type de facturation
    snTypeFacturation.setSession(getSession());
    snTypeFacturation.setIdEtablissement(idEtablissement);
    snTypeFacturation.charger(false);
    snTypeFacturation.ajouterFacturationSansTVA(idEtablissement);
    snTypeFacturation.setSelectionParChampRPG(lexique, "E1TFA");
    
    // Type d'avoir
    snTypeAvoir.setSession(getSession());
    snTypeAvoir.setIdEtablissement(idEtablissement);
    snTypeAvoir.charger(false);
    snTypeAvoir.setSelectionParChampRPG(lexique, "E1AVR");
    
    // Devise
    snDevise.setSession(getSession());
    snDevise.setIdEtablissement(idEtablissement);
    snDevise.charger(false);
    snDevise.setSelectionParChampRPG(lexique, "WDEV");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "E1ETB");
    snClientLivre.renseignerChampRPG(lexique, "E1CLLP", "E1CLLS");
    snClientFacture.renseignerChampRPG(lexique, "E1CLFP", "E1CLFS");
    snVendeur.renseignerChampRPG(lexique, "WVDE");
    snMagasin.renseignerChampRPG(lexique, "WMAGE");
    snTypeFacturation.renseignerChampRPG(lexique, "E1TFA");
    snTypeAvoir.renseignerChampRPG(lexique, "E1AVR");
    snDevise.renseignerChampRPG(lexique, "WDEV");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    snBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riBoutonDetail2ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Modification de l'établissement
   */
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      // Initialisation des composants de sélection
      snClientLivre.setSelection(null);
      snClientFacture.setSelection(null);
      snVendeur.setSelection(null);
      snMagasin.setSelection(null);
      snTypeFacturation.setSelection(null);
      snTypeAvoir.setSelection(null);
      snDevise.setSelection(null);
      // Validation de l'écran pour renvoi du buffer
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new XRiBarreBouton();
    pnlContenu = new SNPanel();
    pnlGauche = new SNPanelContenu();
    sNPanelTitre1 = new SNPanelTitre();
    lbClientLivre = new SNLabelChamp();
    snClientLivre = new SNClient();
    lbClientFacture = new SNLabelChamp();
    snClientFacture = new SNClient();
    sNPanelTitre2 = new SNPanelTitre();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbTypeCommande = new SNLabelChamp();
    WIN17 = new XRiTextField();
    lbTypeFacture = new SNLabelChamp();
    snTypeFacturation = new SNTypeFacturation();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    lbTypeAvoir = new SNLabelChamp();
    snTypeAvoir = new SNTypeAvoir();
    pnlDroite = new SNPanelContenu();
    pnlEtablissement = new SNPanel();
    sNLabelChamp3 = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    sNLabelChamp4 = new SNLabelChamp();
    WDATEX = new XRiCalendrier();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Cr\u00e9ation de @TYP11@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout(1, 2));
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setTitre("S\u00e9lection du client");
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbClientLivre ----
          lbClientLivre.setText("Client livr\u00e9");
          lbClientLivre.setName("lbClientLivre");
          sNPanelTitre1.add(lbClientLivre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snClientLivre ----
          snClientLivre.setName("snClientLivre");
          sNPanelTitre1.add(snClientLivre, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbClientFacture ----
          lbClientFacture.setText("Client factur\u00e9");
          lbClientFacture.setName("lbClientFacture");
          sNPanelTitre1.add(lbClientFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snClientFacture ----
          snClientFacture.setName("snClientFacture");
          sNPanelTitre1.add(snClientFacture, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(sNPanelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanelTitre2 ========
        {
          sNPanelTitre2.setTitre("Options");
          sNPanelTitre2.setName("sNPanelTitre2");
          sNPanelTitre2.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanelTitre2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanelTitre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbVendeur ----
          lbVendeur.setText("Vendeur");
          lbVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbVendeur.setName("lbVendeur");
          sNPanelTitre2.add(lbVendeur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snVendeur ----
          snVendeur.setPreferredSize(new Dimension(320, 30));
          snVendeur.setName("snVendeur");
          sNPanelTitre2.add(snVendeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeCommande ----
          lbTypeCommande.setText("Type de commande");
          lbTypeCommande.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeCommande.setName("lbTypeCommande");
          sNPanelTitre2.add(lbTypeCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WIN17 ----
          WIN17.setPreferredSize(new Dimension(40, 30));
          WIN17.setMinimumSize(new Dimension(40, 30));
          WIN17.setFont(new Font("sansserif", Font.PLAIN, 14));
          WIN17.setComponentPopupMenu(null);
          WIN17.setName("WIN17");
          sNPanelTitre2.add(WIN17, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeFacture ----
          lbTypeFacture.setText("Type de facturation");
          lbTypeFacture.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeFacture.setName("lbTypeFacture");
          sNPanelTitre2.add(lbTypeFacture, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snTypeFacturation ----
          snTypeFacturation.setPreferredSize(new Dimension(320, 30));
          snTypeFacturation.setName("snTypeFacturation");
          sNPanelTitre2.add(snTypeFacturation, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbMagasin.setName("lbMagasin");
          sNPanelTitre2.add(lbMagasin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setPreferredSize(new Dimension(320, 30));
          snMagasin.setName("snMagasin");
          sNPanelTitre2.add(snMagasin, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDevise ----
          lbDevise.setText("Devise");
          lbDevise.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDevise.setName("lbDevise");
          sNPanelTitre2.add(lbDevise, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snDevise ----
          snDevise.setPreferredSize(new Dimension(320, 30));
          snDevise.setName("snDevise");
          sNPanelTitre2.add(snDevise, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTypeAvoir ----
          lbTypeAvoir.setText("Type d'avoir");
          lbTypeAvoir.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeAvoir.setName("lbTypeAvoir");
          sNPanelTitre2.add(lbTypeAvoir, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snTypeAvoir ----
          snTypeAvoir.setPreferredSize(new Dimension(320, 30));
          snTypeAvoir.setName("snTypeAvoir");
          sNPanelTitre2.add(snTypeAvoir, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(sNPanelTitre2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp3 ----
          sNLabelChamp3.setText("Etablissement");
          sNLabelChamp3.setName("sNLabelChamp3");
          pnlEtablissement.add(sNLabelChamp3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- sNLabelChamp4 ----
          sNLabelChamp4.setText("Date de traitement");
          sNLabelChamp4.setName("sNLabelChamp4");
          pnlEtablissement.add(sNLabelChamp4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WDATEX ----
          WDATEX.setMinimumSize(new Dimension(115, 30));
          WDATEX.setPreferredSize(new Dimension(115, 30));
          WDATEX.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDATEX.setName("WDATEX");
          pnlEtablissement.add(WDATEX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ======== scroll_droite ========
    {
      scroll_droite.setBackground(new Color(238, 239, 241));
      scroll_droite.setPreferredSize(new Dimension(16, 520));
      scroll_droite.setBorder(null);
      scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
      scroll_droite.setName("scroll_droite");
      
      // ======== menus_haut ========
      {
        menus_haut.setMinimumSize(new Dimension(160, 520));
        menus_haut.setPreferredSize(new Dimension(160, 520));
        menus_haut.setBackground(new Color(238, 239, 241));
        menus_haut.setAutoscrolls(true);
        menus_haut.setName("menus_haut");
        menus_haut.setLayout(new VerticalLayout());
        
        // ======== riMenu_V01F ========
        {
          riMenu_V01F.setMinimumSize(new Dimension(104, 50));
          riMenu_V01F.setPreferredSize(new Dimension(170, 50));
          riMenu_V01F.setMaximumSize(new Dimension(104, 50));
          riMenu_V01F.setName("riMenu_V01F");
          
          // ---- riMenu_bt_V01F ----
          riMenu_bt_V01F.setText("@V01F@");
          riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
          riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
          riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
          riMenu_bt_V01F.setName("riMenu_bt_V01F");
          riMenu_V01F.add(riMenu_bt_V01F);
        }
        menus_haut.add(riMenu_V01F);
        
        // ======== riSousMenu_consult ========
        {
          riSousMenu_consult.setName("riSousMenu_consult");
          
          // ---- riSousMenu_bt_consult ----
          riSousMenu_bt_consult.setText("Consultation");
          riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
          riSousMenu_consult.add(riSousMenu_bt_consult);
        }
        menus_haut.add(riSousMenu_consult);
        
        // ======== riSousMenu_modif ========
        {
          riSousMenu_modif.setName("riSousMenu_modif");
          
          // ---- riSousMenu_bt_modif ----
          riSousMenu_bt_modif.setText("Modification");
          riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
          riSousMenu_modif.add(riSousMenu_bt_modif);
        }
        menus_haut.add(riSousMenu_modif);
        
        // ======== riSousMenu_crea ========
        {
          riSousMenu_crea.setName("riSousMenu_crea");
          
          // ---- riSousMenu_bt_crea ----
          riSousMenu_bt_crea.setText("Cr\u00e9ation");
          riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
          riSousMenu_crea.add(riSousMenu_bt_crea);
        }
        menus_haut.add(riSousMenu_crea);
        
        // ======== riSousMenu_suppr ========
        {
          riSousMenu_suppr.setName("riSousMenu_suppr");
          
          // ---- riSousMenu_bt_suppr ----
          riSousMenu_bt_suppr.setText("Suppression");
          riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
          riSousMenu_suppr.add(riSousMenu_bt_suppr);
        }
        menus_haut.add(riSousMenu_suppr);
        
        // ======== riSousMenuF_dupli ========
        {
          riSousMenuF_dupli.setName("riSousMenuF_dupli");
          
          // ---- riSousMenu_bt_dupli ----
          riSousMenu_bt_dupli.setText("Duplication");
          riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
          riSousMenuF_dupli.add(riSousMenu_bt_dupli);
        }
        menus_haut.add(riSousMenuF_dupli);
        
        // ======== riMenu1 ========
        {
          riMenu1.setName("riMenu1");
          
          // ---- riMenu_bt1 ----
          riMenu_bt1.setText("Options");
          riMenu_bt1.setName("riMenu_bt1");
          riMenu1.add(riMenu_bt1);
        }
        menus_haut.add(riMenu1);
        
        // ======== riSousMenu2 ========
        {
          riSousMenu2.setName("riSousMenu2");
          
          // ---- riSousMenu_bt2 ----
          riSousMenu_bt2.setText("Comptoir");
          riSousMenu_bt2.setName("riSousMenu_bt2");
          riSousMenu_bt2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riSousMenu_bt2ActionPerformed(e);
            }
          });
          riSousMenu2.add(riSousMenu_bt2);
        }
        menus_haut.add(riSousMenu2);
      }
      scroll_droite.setViewportView(menus_haut);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private XRiBarreBouton snBarreBouton;
  private SNPanel pnlContenu;
  private SNPanelContenu pnlGauche;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbClientLivre;
  private SNClient snClientLivre;
  private SNLabelChamp lbClientFacture;
  private SNClient snClientFacture;
  private SNPanelTitre sNPanelTitre2;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbTypeCommande;
  private XRiTextField WIN17;
  private SNLabelChamp lbTypeFacture;
  private SNTypeFacturation snTypeFacturation;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNLabelChamp lbTypeAvoir;
  private SNTypeAvoir snTypeAvoir;
  private SNPanelContenu pnlDroite;
  private SNPanel pnlEtablissement;
  private SNLabelChamp sNLabelChamp3;
  private SNEtablissement snEtablissement;
  private SNLabelChamp sNLabelChamp4;
  private XRiCalendrier WDATEX;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
