
package ri.serien.libecranrpg.vgvm.VGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM03FM_CR extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM03FM_CR(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FICHE CLIENT   @INDCLI@ @INDLIV@ @LIB@"));
    
    boolean isModif = (!lexique.isTrue("53"));
    navig_valid.setVisible(isModif);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WLBCC1 = new XRiTextField();
    WLBCC2 = new XRiTextField();
    WLBCC3 = new XRiTextField();
    WLBCLP = new XRiTextField();
    WLBTRA = new XRiTextField();
    WLBCLF = new XRiTextField();
    OBJ_32_OBJ_32 = new JLabel();
    OBJ_27_OBJ_27 = new JLabel();
    OBJ_16_OBJ_16 = new JLabel();
    OBJ_35_OBJ_35 = new JLabel();
    OBJ_30_OBJ_30 = new JLabel();
    OBJ_20_OBJ_20 = new JLabel();
    CLCLFP = new XRiTextField();
    CLCC1 = new XRiTextField();
    CLCC2 = new XRiTextField();
    CLCC3 = new XRiTextField();
    CLCLP = new XRiTextField();
    CLAFA = new XRiTextField();
    CLNCG = new XRiTextField();
    CLNCA = new XRiTextField();
    CLTRA = new XRiTextField();
    CLCLFS = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Suppression");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("R\u00e9capitulatif"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WLBCC1 ----
          WLBCC1.setName("WLBCC1");
          panel1.add(WLBCC1);
          WLBCC1.setBounds(224, 93, 277, WLBCC1.getPreferredSize().height);

          //---- WLBCC2 ----
          WLBCC2.setName("WLBCC2");
          panel1.add(WLBCC2);
          WLBCC2.setBounds(224, 119, 277, WLBCC2.getPreferredSize().height);

          //---- WLBCC3 ----
          WLBCC3.setName("WLBCC3");
          panel1.add(WLBCC3);
          WLBCC3.setBounds(224, 145, 277, WLBCC3.getPreferredSize().height);

          //---- WLBCLP ----
          WLBCLP.setName("WLBCLP");
          panel1.add(WLBCLP);
          WLBCLP.setBounds(224, 180, 277, WLBCLP.getPreferredSize().height);

          //---- WLBTRA ----
          WLBTRA.setName("WLBTRA");
          panel1.add(WLBTRA);
          WLBTRA.setBounds(224, 281, 277, WLBTRA.getPreferredSize().height);

          //---- WLBCLF ----
          WLBCLF.setName("WLBCLF");
          panel1.add(WLBCLF);
          WLBCLF.setBounds(269, 53, 232, WLBCLF.getPreferredSize().height);

          //---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Num\u00e9ro de compte");
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          panel1.add(OBJ_32_OBJ_32);
          OBJ_32_OBJ_32.setBounds(33, 240, 118, 20);

          //---- OBJ_27_OBJ_27 ----
          OBJ_27_OBJ_27.setText("Client payeur");
          OBJ_27_OBJ_27.setName("OBJ_27_OBJ_27");
          panel1.add(OBJ_27_OBJ_27);
          OBJ_27_OBJ_27.setBounds(33, 180, 100, 20);

          //---- OBJ_16_OBJ_16 ----
          OBJ_16_OBJ_16.setText("Client factur\u00e9");
          OBJ_16_OBJ_16.setName("OBJ_16_OBJ_16");
          panel1.add(OBJ_16_OBJ_16);
          OBJ_16_OBJ_16.setBounds(33, 57, 97, 20);

          //---- OBJ_35_OBJ_35 ----
          OBJ_35_OBJ_35.setText("Clt transitaire");
          OBJ_35_OBJ_35.setName("OBJ_35_OBJ_35");
          panel1.add(OBJ_35_OBJ_35);
          OBJ_35_OBJ_35.setBounds(33, 285, 97, 20);

          //---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("Affactureur");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          panel1.add(OBJ_30_OBJ_30);
          OBJ_30_OBJ_30.setBounds(33, 210, 85, 20);

          //---- OBJ_20_OBJ_20 ----
          OBJ_20_OBJ_20.setText("Centrales");
          OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
          panel1.add(OBJ_20_OBJ_20);
          OBJ_20_OBJ_20.setBounds(33, 97, 79, 20);

          //---- CLCLFP ----
          CLCLFP.setName("CLCLFP");
          panel1.add(CLCLFP);
          CLCLFP.setBounds(159, 53, 57, CLCLFP.getPreferredSize().height);

          //---- CLCC1 ----
          CLCC1.setName("CLCC1");
          panel1.add(CLCC1);
          CLCC1.setBounds(159, 93, 57, CLCC1.getPreferredSize().height);

          //---- CLCC2 ----
          CLCC2.setName("CLCC2");
          panel1.add(CLCC2);
          CLCC2.setBounds(159, 119, 57, CLCC2.getPreferredSize().height);

          //---- CLCC3 ----
          CLCC3.setName("CLCC3");
          panel1.add(CLCC3);
          CLCC3.setBounds(159, 145, 57, CLCC3.getPreferredSize().height);

          //---- CLCLP ----
          CLCLP.setName("CLCLP");
          panel1.add(CLCLP);
          CLCLP.setBounds(159, 180, 57, CLCLP.getPreferredSize().height);

          //---- CLAFA ----
          CLAFA.setName("CLAFA");
          panel1.add(CLAFA);
          CLAFA.setBounds(159, 205, 57, CLAFA.getPreferredSize().height);

          //---- CLNCG ----
          CLNCG.setName("CLNCG");
          panel1.add(CLNCG);
          CLNCG.setBounds(159, 235, 57, CLNCG.getPreferredSize().height);

          //---- CLNCA ----
          CLNCA.setName("CLNCA");
          panel1.add(CLNCA);
          CLNCA.setBounds(224, 235, 57, CLNCA.getPreferredSize().height);

          //---- CLTRA ----
          CLTRA.setName("CLTRA");
          panel1.add(CLTRA);
          CLTRA.setBounds(159, 281, 57, CLTRA.getPreferredSize().height);

          //---- CLCLFS ----
          CLCLFS.setName("CLCLFS");
          panel1.add(CLCLFS);
          CLCLFS.setBounds(224, 53, 36, CLCLFS.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(15, 10, 540, 340);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WLBCC1;
  private XRiTextField WLBCC2;
  private XRiTextField WLBCC3;
  private XRiTextField WLBCLP;
  private XRiTextField WLBTRA;
  private XRiTextField WLBCLF;
  private JLabel OBJ_32_OBJ_32;
  private JLabel OBJ_27_OBJ_27;
  private JLabel OBJ_16_OBJ_16;
  private JLabel OBJ_35_OBJ_35;
  private JLabel OBJ_30_OBJ_30;
  private JLabel OBJ_20_OBJ_20;
  private XRiTextField CLCLFP;
  private XRiTextField CLCC1;
  private XRiTextField CLCC2;
  private XRiTextField CLCC3;
  private XRiTextField CLCLP;
  private XRiTextField CLAFA;
  private XRiTextField CLNCG;
  private XRiTextField CLNCA;
  private XRiTextField CLTRA;
  private XRiTextField CLCLFS;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
