
package ri.serien.libecranrpg.vgvm.VGVM11FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDocument;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiPanelNav;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_C1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 1000, };
  
  private String[] WLTAR_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  
  private boolean isConsult = false;
  private String st_argument = null;
  private String st_saisie = null;
  private String[] fonctions = { "", "F16", "F17", "F18", "F5" };
  private RiPanelNav riPanelNav1 = null;
  public static final int ETAT_NEUTRE = 0;
  public static final int ETAT_SELECTION = 1;
  public static final int ETAT_ENCOURS = 2;
  public boolean isDevis;
  public boolean isChantier;
  public boolean isAvoir;
  private boolean isOption = false;
  private String libelleDirectOuInterne = "";
  
  // Panels "indépendants"
  private ODialog dialog_MAR = null;
  private ODialog dialog_COL = null;
  
  /**
   * Constructeur.
   */
  public VGVM11FX_C1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    
    riMenu_bt1.setIcon(lexique.chargerImage("images/fin_p.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt5.setIcon(lexique.chargerImage("images/fonctions.png", true));
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // Navigation graphique du menu
    riPanelNav1 = new RiPanelNav();
    riPanelNav1.setImageEtatAtIndex(ETAT_NEUTRE, (lexique.chargerImage("images/blank.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_SELECTION, (lexique.chargerImage("images/navselec.png", true)));
    riPanelNav1.setImageEtatAtIndex(ETAT_ENCOURS, (lexique.chargerImage("images/navencours.png", true)));
    riPanelNav1.setImageDeFond(lexique.chargerImage("images/vgam15_fac999.jpg", true));
    riPanelNav1.setBoutonNav(10, 5, 140, 50, "btnEntete", "Entête de bon", false, bouton_retour);
    riPanelNav1.setBoutonNav(10, 80, 140, 110, "btnCorps", "Lignes du bon", true, null);
    riPanelNav1.setBoutonNav(10, 200, 140, 50, "btnPied", "Options de fin de bon", false, bouton_valider);
    riSousMenu18.add(riPanelNav1);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP11@")).trim());
    OBJ_97.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E3NOM@")).trim());
    WDATEX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDATEX@")).trim());
    WNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUM@")).trim());
    E1ETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1ETB@")).trim());
    WSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSUF@")).trim());
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@E1MAG@  @MALIB@")).trim()));
    devise.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIBR@")).trim());
    E1TTC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1TTC@")).trim());
    WTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTVA@")).trim());
    OBJ_161.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTA@")).trim());
    OBJ_255.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBCO@")).trim());
    WTHT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTHT@")).trim());
    E1NFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NFA@")).trim());
    E1CNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1CNV@")).trim());
    WCNP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCNP@")).trim());
    OBJ_142.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCHPA@")).trim());
    arg_classement.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Par @TMC1@")).trim());
    arg_classement2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Par @TMC2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    isConsult = lexique.isTrue("46");
    isDevis = lexique.HostFieldGetData("ISDEVIS").trim().equals("1");
    isChantier = lexique.HostFieldGetData("ISCHANTIER").trim().equals("1");
    isAvoir = lexique.HostFieldGetData("ISAVOIR").trim().equals("1");
    
    boolean iscomptant = lexique.HostFieldGetData("ISCOMPTANT").trim().equals("1");
    
    // Permet d'afficher aucun sous-menu
    riSousMenu9.setEnabled(!isConsult);
    riSousMenu10.setEnabled(!isConsult && !isDevis);
    riSousMenu11.setEnabled(!isConsult && lexique.HostFieldGetData("E3NOM").trim().startsWith("?"));
    riSousMenu12.setEnabled(!isConsult);
    // Outils
    riSousMenu14.setEnabled(!isConsult);
    riSousMenu15.setEnabled(!isConsult);
    riSousMenu16.setEnabled(!isConsult);
    
    // Saisie
    riSousMenu7.setEnabled(!isChantier);
    riSousMenu26.setEnabled(!isChantier && !isDevis);
    riSousMenu21.setEnabled(!isConsult);
    riSousMenu1.setEnabled(!isConsult);
    riSousMenu2.setEnabled(!isConsult);
    riSousMenu17.setEnabled(!isConsult);
    riSousMenu22.setEnabled(!isConsult);
    riSousMenu23.setEnabled(!isConsult);
    riSousMenu24.setEnabled(!isConsult);
    riSousMenu25.setEnabled(!isConsult && !isDevis);
    
    riSousMenu6.setEnabled(!isChantier);
    riSousMenu9.setEnabled(!isChantier);
    riSousMenu10.setEnabled(!isChantier);
    
    // Modes d'affichages CONSULT / MODIF /CREA
    // Options
    riMenu3.setVisible(!isConsult && !iscomptant);
    riMenu4.setVisible(!isConsult);
    // Recherche
    riMenu1.setVisible(!isConsult);
    
    if (iscomptant) {
      // Outils
      riSousMenu21.setEnabled(false);
      // Saisie
      riSousMenu1.setEnabled(false);
      riSousMenu17.setEnabled(false);
      riSousMenu22.setEnabled(false);
      riSousMenu24.setEnabled(false);
      riSousMenu3.setEnabled(false);
      riSousMenu25.setEnabled(false);
      // Options
      riSousMenu6.setEnabled(false);
      riSousMenu9.setEnabled(false);
      riSousMenu10.setEnabled(false);
      riSousMenu12.setEnabled(false);
      riSousMenu13.setEnabled(false);
    }
    
    WVAL.setValeursSelection("V", "");
    WLTAR.setValeurs(WLTAR_Value, null);
    
    gererAffichageMenus(null);
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    if (isConsult) {
      WTP01.setComponentPopupMenu(null);
    }
    else {
      WTP01.setComponentPopupMenu(BTD);
    }
    
    WIN2.setVisible(lexique.isPresent("WIN2"));
    WIN3.setVisible(lexique.isPresent("WIN3"));
    OBJ_174.setVisible(WIN3.isVisible());
    WVAL.setEnabled(!isDevis);
    WAVR.setEnabled(!isDevis);
    WSUF.setVisible(lexique.isPresent("WSUF"));
    WMAGL.setVisible(lexique.isPresent("WMAGL"));
    OBJ_176.setVisible(WMAGL.isVisible());
    WLTAR.setVisible(lexique.isPresent("WLTAR"));
    OBJ_171.setVisible(WLTAR.isVisible());
    WNLI.setVisible(lexique.isPresent("WNLI"));
    E1ETB.setVisible(lexique.isPresent("E1ETB"));
    
    devise.setVisible(lexique.isPresent("DVLIBR"));
    OBJ_166.setVisible(devise.isVisible());
    
    if (lexique.isPresent("WTVA")) {
      OBJ_255.setText("Total T.V.A.");
    }
    if (lexique.isPresent("E1TTC")) {
      OBJ_161.setText("Total T.T.C.");
      OBJ_161.setVisible(true);
    }
    else {
      OBJ_161.setVisible(false);
    }
    OBJ_235.setVisible(lexique.isPresent("WTHT"));
    OBJ_50.setVisible(!isDevis);
    
    // Barre entête
    E1NFA.setVisible(!lexique.HostFieldGetData("E1NFA").trim().isEmpty());
    label9.setVisible(E1NFA.isVisible());
    
    // Label état du bon
    String etatBon = lexique.HostFieldGetData("ETABON").trim();
    
    if (etatBon.isEmpty()) {
      z_etat.setVisible(false);
      label3.setVisible(false);
    }
    else {
      z_etat.setVisible(true);
      label3.setVisible(true);
      if (etatBon.equals("N")) {
        z_etat.setText("Annulé");
      }
      else if (etatBon.equals("C")) {
        z_etat.setText("Comptabilisé");
      }
      else if (etatBon.equals("c")) {
        z_etat.setText("Cloturé");
      }
      else if (etatBon.equals("T")) {
        z_etat.setText("Commande type");
      }
      else if (etatBon.equals("P")) {
        z_etat.setText("Positionnement");
      }
      else if (etatBon.equals("p")) {
        z_etat.setText("Perdu");
      }
      else if (etatBon.equals("A")) {
        z_etat.setText("Attente");
      }
      else if (etatBon.equals("E")) {
        z_etat.setText("Expédié");
      }
      else if (etatBon.equals("e")) {
        z_etat.setText("Envoyé");
      }
      else if (etatBon.equals("H")) {
        z_etat.setText("Validé");
      }
      else if (etatBon.equals("R")) {
        z_etat.setText("Réservé");
      }
      else if (etatBon.equals("F")) {
        z_etat.setText("Facturé");
      }
      else if (etatBon.equals("d")) {
        z_etat.setText("Validité dépassée");
      }
      else if (etatBon.equals("D")) {
        z_etat.setText("Validé non édité");
      }
      else if (etatBon.equals("s")) {
        z_etat.setText("Signé");
      }
      else {
        z_etat.setText(etatBon);
      }
    }
    
    WCOLC.setVisible(!lexique.isPresent("WTVA"));
    WTHT.setVisible(lexique.isPresent("WTHT"));
    WTVA.setVisible(lexique.isPresent("WTVA"));
    E1TTC.setVisible(lexique.isPresent("E1TTC"));
    WPDSC.setVisible(!lexique.isPresent("E1TTC"));
    WDATEX.setVisible(lexique.isPresent("WDATEX"));
    WART21.setVisible((lexique.isPresent("WART21")) && (!isConsult));
    WART23.setVisible(WART21.isVisible());
    WART22.setVisible(WART21.isVisible());
    OBJ_142.setVisible(lexique.isPresent("WCHPA"));
    WARTT.setVisible(lexique.isPresent("WARTT"));
    WLOTP.setVisible(lexique.isTrue("(N46) AND (04)") && !isDevis && !isChantier);
    label5.setVisible(WLOTP.isVisible());
    WLOTS.setVisible(WLOTP.isVisible());
    OBJ_97.setVisible(!lexique.HostFieldGetData("E3NOM").trim().isEmpty());
    
    // Gestion du type d'arguments recherchés
    String WCHPA = lexique.HostFieldGetData("WCHPA");
    String[] tab_WCHPA = WCHPA.split("/");
    if (tab_WCHPA.length > 1) {
      st_saisie = tab_WCHPA[1].trim();
    }
    else {
      st_saisie = null;
    }
    st_argument = tab_WCHPA[0].trim();
    gererArguments(st_argument);
    if (st_argument.equals("code à barres")) {
      ck_saisie.setText("Saisie unitaire");
      fonctions[0] = fonctions[4];
    }
    else if (st_argument.equals("Famille")) {
      fonctions[0] = fonctions[1];
    }
    else if (st_argument.equals(lexique.HostFieldGetData("TMC1").trim())) {
      st_argument = lexique.HostFieldGetData("TMC1").trim();
      fonctions[0] = fonctions[2];
    }
    else if (st_argument.equals(lexique.HostFieldGetData("TMC2").trim())) {
      st_argument = lexique.HostFieldGetData("TMC2").trim();
      fonctions[0] = fonctions[3];
    }
    else {
      fonctions[0] = "";
    }
    
    bt_argument.setText(" " + st_argument);
    ck_saisie.setSelected(st_saisie != null);
    
    bt_argument.setVisible(WARTT.isVisible());
    ck_saisie.setVisible(WARTT.isVisible());
    riBoutonDetail1.setVisible(WARTT.isVisible());
    
    if (lexique.HostFieldGetData("WTPRU").trim().equals("1")) {
      label6.setVisible(true);
      p_contenu.setPreferredSize(new Dimension(1000, 560));
    }
    else {
      label6.setVisible(false);
      p_contenu.setPreferredSize(new Dimension(1000, 530));
      
    }
    
    // Modificateurs de la ligne
    panel4.setVisible(isOption);
    
    // Avoir
    // Si on est en avoir la case à cocher sert à créer des lignes positives (c'est à dire à payer par le client) dans un avoir
    if (isAvoir) {
      c_avoir.setText("Ligne de facture");
      b_avoir.setVisible(false);
      WAVR.setVisible(false);
    }
    // Si on est en facture la case à cocher sert à créer des lignes négatives (c'est à dire à rembourser au client) dans une facture
    else {
      c_avoir.setText("Ligne d'avoir");
      b_avoir.setVisible(c_avoir.isSelected());
      WAVR.setVisible(c_avoir.isSelected());
    }
    
    // Gratuit
    String win2 = lexique.HostFieldGetData("WIN2").trim();
    c_gratuit.setSelected(!win2.isEmpty() && !win2.equalsIgnoreCase("F"));
    b_gratuit.setVisible(c_gratuit.isSelected());
    WIN2.setVisible(c_gratuit.isSelected());
    c_forfait.setSelected(win2.equalsIgnoreCase("F"));
    
    // Le bandeau du panel
    String e1in18 = lexique.HostFieldGetData("E1IN18").trim();
    if (e1in18.equalsIgnoreCase("D")) {
      libelleDirectOuInterne = " (Direct usine)";
    }
    else if (e1in18.equalsIgnoreCase("I")) {
      libelleDirectOuInterne = " (Commande interne)";
    }
    else {
      libelleDirectOuInterne = "";
    }
    p_bpresentation.setText(p_bpresentation.getText().replaceAll(":", "").trim() + " (facturation "
        + lexique.HostFieldGetData("TFLIB").trim() + ")" + libelleDirectOuInterne);
    p_bpresentation.setCodeEtablissement(E1ETB.getText());
    
    // Initialiser les variables de sessions
    try {
      // Initialiser l'identifiant établissement
      IdEtablissement idEtablissement = IdEtablissement.getInstance(E1ETB.getText());
      
      // Initialiser l'identifiant document de vente
      int numero = Constantes.convertirTexteEnInteger(WNUM.getText());
      if (numero != 0) {
        EnumCodeEnteteDocumentVente codeEntete = EnumCodeEnteteDocumentVente.COMMANDE_OU_BON;
        if (isDevis) {
          codeEntete = EnumCodeEnteteDocumentVente.DEVIS;
        }
        int suffixe = Constantes.convertirTexteEnInteger(WSUF.getText());
        IdDocumentVente idDocument = IdDocumentVente.getInstanceGenerique(idEtablissement, codeEntete, numero, suffixe, null);
        lexique.addVariableGlobaleFromSession("idDocumentVente", idDocument);
        
        // Initialiser la date de traitement
        Date dateTraitement = DateHeure.convertirFormat8VersDate(WDATEX.getText());
        lexique.addVariableGlobaleFromSession("dateTraitement", dateTraitement);
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Problème avec l'initialisation des variables de session");
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Si on est en création de ligne d'avoir et que l'on coche "ligne de facture" on force la valeur A dans WAVR
    // Cela sert à créer des lignes positives (c'est à dire à payer par le client) dans un avoir
    if (isAvoir) {
      if (c_avoir.isSelected()) {
        lexique.HostFieldPutData("WAVR", 0, "A");
      }
      else {
        lexique.HostFieldPutData("WAVR", 0, "");
      }
      c_avoir.setSelected(false);
    }
  }
  
  /**
   * Permet d'afficher ou pas tous les menus du clic droit sur la liste en fonction du type de ligne (article, commentaire , edition,
   * annulé...).
   */
  private void gererLaPopUpMenu(int index) {
    if (index == -1) {
      return;
    }
    
    String chaineTest = lexique.HostFieldGetData(_WTP01_Data[index][0]);
    if (chaineTest == null) {
      return;
    }
    
    chaineTest = chaineTest.trim();
    if (!chaineTest.isEmpty() && (chaineTest.length() > 7)) {
      String marqueur = chaineTest.substring(5, 7).trim();
      if (marqueur.isEmpty() || marqueur.equals("*E") || marqueur.equals("**")) {
        OBJ_30.setVisible(false);
        OBJ_31.setVisible(false);
        menuItem1.setVisible(false);
      }
      else {
        OBJ_30.setVisible(true);
        OBJ_31.setVisible(true);
        menuItem1.setVisible(true);
      }
    }
  }
  
  /**
   * Cache le jmenuItem qui est en cours
   */
  private void gererArguments(String argEnCours) {
    arg_article.setVisible(!argEnCours.equals("Article"));
    arg_famille.setVisible(!argEnCours.equals("Famille"));
    arg_classement.setVisible(!argEnCours.equals(lexique.HostFieldGetData("TMC1").trim()));
    arg_classement2.setVisible(!argEnCours.equals(lexique.HostFieldGetData("TMC2").trim()));
    arg_gencod.setVisible(!argEnCours.equals("GenCod"));
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    WARTT.setText("*SCND");
    lexique.HostFieldPutData("WARTT", 0, "*SCND");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*ADRCLI");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*ADRCLI");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*EXO");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*EXO");
    }
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*CHGCLI");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*CHGCLI");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*PRU");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*PRU");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*TRI");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*TRI");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut((WTP01.getSelectedRow() + 4), 4);
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // dim ligne, colonne as integer
    // HostCursorGet(ligne,colonne)
    // HostFieldPutData("PT01",ligne-4,"F")
    // touche="Enter"
    // ScriptCall("G_Touche")
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F14");
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("SCAN", 0, "S");
    lexique.HostCursorPut(20, 9);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    gererLaPopUpMenu(WTP01.getSelectedRow());
    if (WTP01.doubleClicSelection(e)) {
      if (lexique.HostFieldGetData("WTPRU").trim().equals("1")) {
        WTP01.setValeurTop("1");
        label6.setVisible(true);
        p_contenu.setPreferredSize(new Dimension(1000, 560));
      }
      else {
        WTP01.setValeurTop("2");
        label6.setVisible(false);
        p_contenu.setPreferredSize(new Dimension(1000, 530));
      }
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    popupMenu1.show(WTP01, 50, 50);
  }
  
  private void riMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ANNUL", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt17ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("!!");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "!!");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt22ActionPerformed(ActionEvent e) {
    String chaine = "ùù" + lexique.HostFieldGetData("WCNP");
    if (WARTT.isVisible()) {
      WARTT.setText(chaine);
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, chaine);
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt23ActionPerformed(ActionEvent e) {
    String chaine = "§§" + lexique.HostFieldGetData("WCNP");
    if (WARTT.isVisible()) {
      WARTT.setText(chaine);
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, chaine);
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt24ActionPerformed(ActionEvent e) {
    String chaine = "§§" + lexique.HostFieldGetData("E1CNV");
    if (WARTT.isVisible()) {
      WARTT.setText(chaine);
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, chaine);
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt25ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*DATLIV");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*DATLIV");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_argumentActionPerformed(ActionEvent e) {
    arguments.show(bt_argument, 0, -150);
  }
  
  private void ck_saisieActionPerformed(ActionEvent e) {
    if (st_argument.equals("GenCod")) {
      lexique.HostScreenSendKey(this, "F6");
    }
    else {
      lexique.HostScreenSendKey(this, "F7");
    }
  }
  
  private void arg_articleActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, fonctions[0]);
  }
  
  private void arg_familleActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[1];
    lexique.HostScreenSendKey(this, fonctions[1]);
  }
  
  private void arg_classementActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[2];
    lexique.HostScreenSendKey(this, fonctions[2]);
  }
  
  private void arg_classement2ActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[3];
    lexique.HostScreenSendKey(this, fonctions[3]);
  }
  
  private void arg_gencodActionPerformed(ActionEvent e) {
    fonctions[0] = fonctions[4];
    lexique.HostScreenSendKey(this, fonctions[4]);
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void arg_commActionPerformed(ActionEvent e) {
    WARTT.setText("");
    lexique.HostCursorPut("WARTT");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void arg_art_commActionPerformed(ActionEvent e) {
    WARTT.setText(" *" + WARTT.getText());
    lexique.HostCursorPut("WARTT");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt_dupli2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ANNUL", 0, "A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    if (WARTT.isVisible()) {
      WARTT.setText("*DDL");
    }
    else {
      lexique.HostFieldPutData("WARTT", 0, "*DDL");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riBoutonVisu1ActionPerformed(ActionEvent e) {
    if (lexique.isIntoCurrentRecord(lexique.getHostField("APDF"))) {
      lexique.HostFieldPutData("APDF", 0, "1");
    }
    else if (lexique.isIntoCurrentRecord(lexique.getHostField("V06F4"))) {
      lexique.HostFieldPutData("V06F4", 0, "APDF");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(popupMenu2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(3, 5);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void b_avoirActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WAVR");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void b_gratuitActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WIN2");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void c_avoirActionPerformed(ActionEvent e) {
    if (isAvoir) {
      return;
    }
    b_avoir.setVisible(!b_avoir.isVisible());
    WAVR.setVisible(!WAVR.isVisible());
    WAVR.setText("A");
  }
  
  private void c_gratuitActionPerformed(ActionEvent e) {
    b_gratuit.setVisible(!b_gratuit.isVisible());
    WIN2.setVisible(!WIN2.isVisible());
    if (c_gratuit.isSelected()) {
      c_forfait.setSelected(false);
    }
  }
  
  private void c_forfaitActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WIN2", 0, "F");
    if (c_forfait.isSelected()) {
      c_gratuit.setSelected(false);
      b_gratuit.setVisible(false);
      WIN2.setVisible(false);
    }
  }
  
  private void b_optionsActionPerformed(ActionEvent e) {
    panel4.setVisible(!panel4.isVisible());
    isOption = !isOption;
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    if (dialog_MAR == null) {
      dialog_MAR = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_MAR(this));
    }
    dialog_MAR.affichePopupPerso();
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    if (dialog_COL == null) {
      dialog_COL = new ODialog((Window) getTopLevelAncestor(), new VGVM11FX_COL(this));
    }
    dialog_COL.affichePopupPerso();
  }
  
  private void collerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop(">");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void couperActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("<");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void copierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("=");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseExited(MouseEvent e) {
    // Ne pas utiliser le clear ici, car lors de l'activation du menu contextuel, il considère qu'il est sortie de la
    // liste et donc il cleare
    // WTP01.clearSelection();
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    WARTT.setText("*AVR");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt26ActionPerformed(ActionEvent e) {
    WARTT.setText("*DUP");
    lexique.HostCursorPut("WARTT");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt27ActionPerformed(ActionEvent e) {
    WARTT.setText("*DUPCHGT");
    lexique.HostCursorPut("WARTT");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void b_palettesActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    WCOLC = new XRiTextField();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_97 = new RiZoneSortie();
    WDATEX = new RiZoneSortie();
    WNUM = new RiZoneSortie();
    E1ETB = new RiZoneSortie();
    WSUF = new RiZoneSortie();
    label1 = new JLabel();
    z_etat = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    p_tete_droite = new JPanel();
    riBoutonVisu1 = new SNBoutonDocument();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu27 = new RiSousMenu();
    riSousMenu_bt26 = new RiSousMenu_bt();
    riSousMenu28 = new RiSousMenu();
    riSousMenu_bt27 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu26 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu17 = new RiSousMenu();
    riSousMenu_bt17 = new RiSousMenu_bt();
    riSousMenu22 = new RiSousMenu();
    riSousMenu_bt22 = new RiSousMenu_bt();
    riSousMenu23 = new RiSousMenu();
    riSousMenu_bt23 = new RiSousMenu_bt();
    riSousMenu24 = new RiSousMenu();
    riSousMenu_bt24 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu25 = new RiSousMenu();
    riSousMenu_bt25 = new RiSousMenu_bt();
    riMenu5 = new RiMenu();
    riMenu_bt5 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    p_centrage = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_DEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_FIN = new JButton();
    panel2 = new JPanel();
    WARTT = new XRiTextField();
    devise = new RiZoneSortie();
    E1TTC = new RiZoneSortie();
    WTVA = new RiZoneSortie();
    OBJ_161 = new JLabel();
    OBJ_235 = new JLabel();
    OBJ_255 = new JLabel();
    OBJ_166 = new JLabel();
    WNLI = new XRiTextField();
    WTHT = new RiZoneSortie();
    WPDSC = new XRiTextField();
    WLOTS = new XRiTextField();
    WLOTP = new XRiTextField();
    WART21 = new XRiTextField();
    WART22 = new XRiTextField();
    WART23 = new XRiTextField();
    label5 = new JLabel();
    bt_argument = new SNBoutonLeger();
    riBoutonDetail1 = new SNBoutonDetail();
    b_options = new JButton();
    ck_saisie = new JCheckBox();
    panel4 = new JPanel();
    OBJ_171 = new JLabel();
    WLTAR = new XRiComboBox();
    WVAL = new XRiCheckBox();
    WAVR = new XRiTextField();
    c_avoir = new JCheckBox();
    WIN2 = new XRiTextField();
    c_gratuit = new JCheckBox();
    WMAGL = new XRiTextField();
    OBJ_176 = new JLabel();
    c_forfait = new JCheckBox();
    b_avoir = new SNBoutonDetail();
    b_gratuit = new SNBoutonDetail();
    OBJ_174 = new JLabel();
    WIN3 = new XRiTextField();
    OBJ_175 = new JLabel();
    WIN21 = new XRiTextField();
    label7 = new JLabel();
    label6 = new JLabel();
    E1NFA = new RiZoneSortie();
    label9 = new JLabel();
    BTD = new JPopupMenu();
    copier = new JMenuItem();
    couper = new JMenuItem();
    coller = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    OBJ_53 = new JMenuItem();
    OBJ_50 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_52 = new JMenuItem();
    OBJ_49 = new JMenuItem();
    OBJ_47 = new JMenuItem();
    OBJ_48 = new JMenuItem();
    OBJ_46 = new JMenuItem();
    OBJ_51 = new JMenuItem();
    BTD3 = new JPopupMenu();
    OBJ_56 = new JMenuItem();
    OBJ_54 = new JMenuItem();
    OBJ_55 = new JMenuItem();
    FONC4 = new JPopupMenu();
    OBJ_58 = new JMenuItem();
    popupMenu1 = new JPopupMenu();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_43 = new JMenuItem();
    OBJ_42 = new JMenuItem();
    OBJ_38 = new JMenuItem();
    E1CNV = new RiZoneSortie();
    WCNP = new JLabel();
    OBJ_142 = new JLabel();
    arguments = new JPopupMenu();
    menuItem2 = new JMenuItem();
    arg_article = new JMenuItem();
    arg_famille = new JMenuItem();
    arg_classement = new JMenuItem();
    arg_classement2 = new JMenuItem();
    arg_gencod = new JMenuItem();
    arg_art_comm = new JMenuItem();
    arg_comm = new JMenuItem();
    menuItem1 = new JMenuItem();
    button1 = new JButton();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    popupMenu2 = new JPopupMenu();
    menuItem3 = new JMenuItem();
    label8 = new JLabel();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenuF_dupli2 = new RiSousMenu();
    riSousMenu_bt_dupli2 = new RiSousMenu_bt();
    
    // ======== riMenu1 ========
    {
      riMenu1.setName("riMenu1");
      
      // ---- riMenu_bt1 ----
      riMenu_bt1.setText("Annulation");
      riMenu_bt1.setToolTipText("Annulation et sortie");
      riMenu_bt1.setName("riMenu_bt1");
      riMenu_bt1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riMenu_bt1ActionPerformed(e);
        }
      });
      riMenu1.add(riMenu_bt1);
    }
    
    // ======== riSousMenu16 ========
    {
      riSousMenu16.setName("riSousMenu16");
      
      // ---- riSousMenu_bt16 ----
      riSousMenu_bt16.setText("Forfait commentaire");
      riSousMenu_bt16.setToolTipText("Gestion d'un forfait commentaire avec ligne d'\u00e9cart");
      riSousMenu_bt16.setName("riSousMenu_bt16");
      riSousMenu_bt16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt16ActionPerformed(e);
        }
      });
      riSousMenu16.add(riSousMenu_bt16);
    }
    
    // ---- WCOLC ----
    WCOLC.setName("WCOLC");
    
    // ======== this ========
    setMinimumSize(new Dimension(1000, 530));
    setPreferredSize(new Dimension(1000, 530));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("@TYP11@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(980, 32));
          p_tete_gauche.setMinimumSize(new Dimension(980, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ---- OBJ_97 ----
          OBJ_97.setText("@E3NOM@");
          OBJ_97.setOpaque(false);
          OBJ_97.setName("OBJ_97");
          p_tete_gauche.add(OBJ_97);
          OBJ_97.setBounds(319, 0, 290, OBJ_97.getPreferredSize().height);
          
          // ---- WDATEX ----
          WDATEX.setComponentPopupMenu(BTD);
          WDATEX.setBorder(new BevelBorder(BevelBorder.LOWERED));
          WDATEX.setHorizontalAlignment(SwingConstants.CENTER);
          WDATEX.setHorizontalTextPosition(SwingConstants.CENTER);
          WDATEX.setFont(WDATEX.getFont().deriveFont(WDATEX.getFont().getStyle() & ~Font.BOLD));
          WDATEX.setText("@WDATEX@");
          WDATEX.setOpaque(false);
          WDATEX.setName("WDATEX");
          p_tete_gauche.add(WDATEX);
          WDATEX.setBounds(908, 0, 70, WDATEX.getPreferredSize().height);
          
          // ---- WNUM ----
          WNUM.setOpaque(false);
          WNUM.setText("@WNUM@");
          WNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          WNUM.setName("WNUM");
          p_tete_gauche.add(WNUM);
          WNUM.setBounds(200, 0, 70, WNUM.getPreferredSize().height);
          
          // ---- E1ETB ----
          E1ETB.setOpaque(false);
          E1ETB.setText("@E1ETB@");
          E1ETB.setName("E1ETB");
          p_tete_gauche.add(E1ETB);
          E1ETB.setBounds(95, 0, 40, E1ETB.getPreferredSize().height);
          
          // ---- WSUF ----
          WSUF.setOpaque(false);
          WSUF.setText("@WSUF@");
          WSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          WSUF.setName("WSUF");
          p_tete_gauche.add(WSUF);
          WSUF.setBounds(276, 0, 20, WSUF.getPreferredSize().height);
          
          // ---- label1 ----
          label1.setText("Etablissement");
          label1.setName("label1");
          p_tete_gauche.add(label1);
          label1.setBounds(5, 2, 85, 20);
          
          // ---- z_etat ----
          z_etat.setOpaque(false);
          z_etat.setHorizontalAlignment(SwingConstants.CENTER);
          z_etat.setName("z_etat");
          p_tete_gauche.add(z_etat);
          z_etat.setBounds(664, 0, 110, z_etat.getPreferredSize().height);
          
          // ---- label2 ----
          label2.setText("Num\u00e9ro");
          label2.setName("label2");
          p_tete_gauche.add(label2);
          label2.setBounds(145, 2, 55, 20);
          
          // ---- label3 ----
          label3.setText("Etat");
          label3.setName("label3");
          p_tete_gauche.add(label3);
          label3.setBounds(629, 2, 35, 21);
          
          // ---- label4 ----
          label4.setText("Date de traitement");
          label4.setName("label4");
          p_tete_gauche.add(label4);
          label4.setBounds(793, 2, 110, 20);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(50, 0));
          p_tete_droite.setMinimumSize(new Dimension(50, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 1));
          
          // ---- riBoutonVisu1 ----
          riBoutonVisu1.setName("riBoutonVisu1");
          riBoutonVisu1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonVisu1ActionPerformed(e);
            }
          });
          p_tete_droite.add(riBoutonVisu1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour ent\u00eate");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setToolTipText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Autres vues");
              riSousMenu_bt8.setToolTipText("Autres vues");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Historique");
              riSousMenu_bt6.setToolTipText("Historique bon");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Bloc adresse ");
              riSousMenu_bt9.setToolTipText("R\u00e9cup\u00e9ration bloc adresse en commentaire");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Commande ouverte");
              riSousMenu_bt10.setToolTipText("Extraction d'une commande ouverte");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Client d\u00e9finitif ");
              riSousMenu_bt11.setToolTipText("Client d\u00e9finitif pour cette facture");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Autre bon, facture, devis");
              riSousMenu_bt12.setToolTipText("Recherche d'un autre bon, facture, devis");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");
              
              // ---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Marge");
              riSousMenu_bt18.setToolTipText("Visualisaion de la marge commerciale");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu19);
            
            // ======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");
              
              // ---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Colis, poids et volume");
              riSousMenu_bt19.setToolTipText("Gestion du conditionnement de la commande");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu20);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("R\u00e9percussion \u00e9l\u00e9ments");
              riSousMenu_bt14.setToolTipText("R\u00e9percussion \u00e9l\u00e9ments sur lignes");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Renum\u00e9rotation lignes");
              riSousMenu_bt15.setToolTipText("Renum\u00e9rotation des lignes");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
            
            // ======== riSousMenu27 ========
            {
              riSousMenu27.setName("riSousMenu27");
              
              // ---- riSousMenu_bt26 ----
              riSousMenu_bt26.setText("Duplication doc en cours");
              riSousMenu_bt26.setToolTipText("Duplication du document en cours");
              riSousMenu_bt26.setName("riSousMenu_bt26");
              riSousMenu_bt26.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt26ActionPerformed(e);
                }
              });
              riSousMenu27.add(riSousMenu_bt26);
            }
            menus_haut.add(riSousMenu27);
            
            // ======== riSousMenu28 ========
            {
              riSousMenu28.setName("riSousMenu28");
              
              // ---- riSousMenu_bt27 ----
              riSousMenu_bt27.setText("Duplic. pour autre client");
              riSousMenu_bt27.setToolTipText("Duplication pour un autre client");
              riSousMenu_bt27.setName("riSousMenu_bt27");
              riSousMenu_bt27.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt27ActionPerformed(e);
                }
              });
              riSousMenu28.add(riSousMenu_bt27);
            }
            menus_haut.add(riSousMenu28);
            
            // ======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");
              
              // ---- riMenu_bt4 ----
              riMenu_bt4.setText("Saisie");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Ajouter palette");
              riSousMenu_bt7.setToolTipText("Ajouter article palette");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu26 ========
            {
              riSousMenu26.setName("riSousMenu26");
              
              // ---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Articles pour avoir");
              riSousMenu_bt20.setToolTipText("Articles pour avoir");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu26.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu26);
            
            // ======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");
              
              // ---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Modif. qt\u00e9 pleine page");
              riSousMenu_bt21.setToolTipText("Modification quantit\u00e9s pleine page");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Saisie ligne rapide");
              riSousMenu_bt1.setToolTipText("Mise En/Hors Fonction utilisation Ligne \u00e9tendue");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu17 ========
            {
              riSousMenu17.setName("riSousMenu17");
              
              // ---- riSousMenu_bt17 ----
              riSousMenu_bt17.setText("Derni\u00e8res ventes");
              riSousMenu_bt17.setToolTipText("Saisie sur derni\u00e8res ventes");
              riSousMenu_bt17.setName("riSousMenu_bt17");
              riSousMenu_bt17.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt17ActionPerformed(e);
                }
              });
              riSousMenu17.add(riSousMenu_bt17);
            }
            menus_haut.add(riSousMenu17);
            
            // ======== riSousMenu22 ========
            {
              riSousMenu22.setName("riSousMenu22");
              
              // ---- riSousMenu_bt22 ----
              riSousMenu_bt22.setText("Saisie conditionnements");
              riSousMenu_bt22.setToolTipText("Saisie conditionnements");
              riSousMenu_bt22.setName("riSousMenu_bt22");
              riSousMenu_bt22.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt22ActionPerformed(e);
                }
              });
              riSousMenu22.add(riSousMenu_bt22);
            }
            menus_haut.add(riSousMenu22);
            
            // ======== riSousMenu23 ========
            {
              riSousMenu23.setName("riSousMenu23");
              
              // ---- riSousMenu_bt23 ----
              riSousMenu_bt23.setText("Articles en promotion");
              riSousMenu_bt23.setToolTipText("Saisie articles en promotion");
              riSousMenu_bt23.setName("riSousMenu_bt23");
              riSousMenu_bt23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt23ActionPerformed(e);
                }
              });
              riSousMenu23.add(riSousMenu_bt23);
            }
            menus_haut.add(riSousMenu23);
            
            // ======== riSousMenu24 ========
            {
              riSousMenu24.setName("riSousMenu24");
              
              // ---- riSousMenu_bt24 ----
              riSousMenu_bt24.setText("Articles r\u00e9f\u00e9renc\u00e9s");
              riSousMenu_bt24.setToolTipText("Articles r\u00e9f\u00e9renc\u00e9s");
              riSousMenu_bt24.setName("riSousMenu_bt24");
              riSousMenu_bt24.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt24ActionPerformed(e);
                }
              });
              riSousMenu24.add(riSousMenu_bt24);
            }
            menus_haut.add(riSousMenu24);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Dupli. ligne saisie");
              riSousMenu_bt3.setToolTipText("Duplication derni\u00e8re ligne de saisie");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
            
            // ======== riSousMenu25 ========
            {
              riSousMenu25.setName("riSousMenu25");
              
              // ---- riSousMenu_bt25 ----
              riSousMenu_bt25.setText("Dates de livraison");
              riSousMenu_bt25.setToolTipText("Saisie pleine page des dates de livraison");
              riSousMenu_bt25.setName("riSousMenu_bt25");
              riSousMenu_bt25.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt25ActionPerformed(e);
                }
              });
              riSousMenu25.add(riSousMenu_bt25);
            }
            menus_haut.add(riSousMenu25);
            
            // ======== riMenu5 ========
            {
              riMenu5.setName("riMenu5");
              
              // ---- riMenu_bt5 ----
              riMenu_bt5.setText("Navigation ");
              riMenu_bt5.setName("riMenu_bt5");
              riMenu5.add(riMenu_bt5);
            }
            menus_haut.add(riMenu5);
            
            // ======== riSousMenu18 ========
            {
              riSousMenu18.setPreferredSize(new Dimension(170, 260));
              riSousMenu18.setMinimumSize(new Dimension(170, 260));
              riSousMenu18.setMaximumSize(new Dimension(170, 260));
              riSousMenu18.setMargin(new Insets(0, -2, 0, 0));
              riSousMenu18.setName("riSousMenu18");
            }
            menus_haut.add(riSousMenu18);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(239, 239, 222));
        p_centrage.setAutoscrolls(true);
        p_centrage.setPreferredSize(new Dimension(740, 570));
        p_centrage.setMinimumSize(new Dimension(740, 570));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout) p_centrage.getLayout()).columnWidths = new int[] { 1050, 0 };
        ((GridBagLayout) p_centrage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) p_centrage.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) p_centrage.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 590));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 590));
          p_contenu.setMaximumSize(new Dimension(10000, 5900));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout) p_contenu.getLayout()).columnWidths = new int[] { 73, 136, 797, 0 };
          ((GridBagLayout) p_contenu.getLayout()).rowHeights = new int[] { 0, 0, 192, 0 };
          ((GridBagLayout) p_contenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) p_contenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("@E1MAG@  @MALIB@"));
            panel1.setOpaque(false);
            panel1.setMinimumSize(new Dimension(1030, 350));
            panel1.setPreferredSize(new Dimension(1030, 350));
            panel1.setName("panel1");
            panel1.setLayout(new GridBagLayout());
            ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 943, 28, 0 };
            ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 0, 33, 105, 105, 33, 0, 0 };
            ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.5, 0.0, 0.0, 0.0, 0.0, 0.5, 1.0E-4 };
            
            // ======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setAutoscrolls(true);
              SCROLLPANE_LIST.setMinimumSize(new Dimension(940, 275));
              SCROLLPANE_LIST.setPreferredSize(new Dimension(940, 275));
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
              
              // ---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setPreferredScrollableViewportSize(new Dimension(940, 275));
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
                
                @Override
                public void mouseExited(MouseEvent e) {
                  WTP01MouseExited(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST, new GridBagConstraints(0, 1, 1, 4, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setMinimumSize(new Dimension(28, 30));
            BT_DEB.setPreferredSize(new Dimension(28, 30));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_DEBActionPerformed(e);
              }
            });
            panel1.add(BT_DEB, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMinimumSize(new Dimension(28, 100));
            BT_PGUP.setPreferredSize(new Dimension(28, 100));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMinimumSize(new Dimension(28, 100));
            BT_PGDOWN.setPreferredSize(new Dimension(28, 100));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setPreferredSize(new Dimension(28, 30));
            BT_FIN.setMinimumSize(new Dimension(28, 30));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_FINActionPerformed(e);
              }
            });
            panel1.add(BT_FIN, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          p_contenu.add(panel1, new GridBagConstraints(0, 1, 3, 1, 0.5, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Saisie de ligne"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);
            
            // ---- WARTT ----
            WARTT.setComponentPopupMenu(BTD2);
            WARTT.setToolTipText("Argument de recherche");
            WARTT.setName("WARTT");
            panel2.add(WARTT);
            WARTT.setBounds(75, 55, 210, 28);
            
            // ---- devise ----
            devise.setText("@DVLIBR@");
            devise.setName("devise");
            panel2.add(devise);
            devise.setBounds(614, 85, 115, devise.getPreferredSize().height);
            
            // ---- E1TTC ----
            E1TTC.setComponentPopupMenu(FONC4);
            E1TTC.setText("@E1TTC@");
            E1TTC.setHorizontalAlignment(SwingConstants.RIGHT);
            E1TTC.setFont(E1TTC.getFont().deriveFont(E1TTC.getFont().getStyle() | Font.BOLD));
            E1TTC.setName("E1TTC");
            panel2.add(E1TTC);
            E1TTC.setBounds(835, 85, 90, E1TTC.getPreferredSize().height);
            
            // ---- WTVA ----
            WTVA.setComponentPopupMenu(FONC4);
            WTVA.setText("@WTVA@");
            WTVA.setHorizontalAlignment(SwingConstants.RIGHT);
            WTVA.setName("WTVA");
            panel2.add(WTVA);
            WTVA.setBounds(835, 55, 90, WTVA.getPreferredSize().height);
            
            // ---- OBJ_161 ----
            OBJ_161.setText("@TOTA@");
            OBJ_161.setFont(OBJ_161.getFont().deriveFont(OBJ_161.getFont().getStyle() | Font.BOLD));
            OBJ_161.setName("OBJ_161");
            panel2.add(OBJ_161);
            OBJ_161.setBounds(755, 87, 72, 20);
            
            // ---- OBJ_235 ----
            OBJ_235.setText("Total H.T.");
            OBJ_235.setName("OBJ_235");
            panel2.add(OBJ_235);
            OBJ_235.setBounds(755, 27, 72, 20);
            
            // ---- OBJ_255 ----
            OBJ_255.setText("@NBCO@");
            OBJ_255.setName("OBJ_255");
            panel2.add(OBJ_255);
            OBJ_255.setBounds(755, 57, 68, 20);
            
            // ---- OBJ_166 ----
            OBJ_166.setText("Prix en");
            OBJ_166.setName("OBJ_166");
            panel2.add(OBJ_166);
            OBJ_166.setBounds(555, 87, 50, 20);
            
            // ---- WNLI ----
            WNLI.setComponentPopupMenu(null);
            WNLI.setName("WNLI");
            panel2.add(WNLI);
            WNLI.setBounds(15, 55, 44, WNLI.getPreferredSize().height);
            
            // ---- WTHT ----
            WTHT.setComponentPopupMenu(FONC4);
            WTHT.setText("@WTHT@");
            WTHT.setHorizontalAlignment(SwingConstants.RIGHT);
            WTHT.setName("WTHT");
            panel2.add(WTHT);
            WTHT.setBounds(835, 25, 90, WTHT.getPreferredSize().height);
            
            // ---- WPDSC ----
            WPDSC.setName("WPDSC");
            panel2.add(WPDSC);
            WPDSC.setBounds(835, 83, 90, WPDSC.getPreferredSize().height);
            
            // ---- WLOTS ----
            WLOTS.setComponentPopupMenu(BTD2);
            WLOTS.setName("WLOTS");
            panel2.add(WLOTS);
            WLOTS.setBounds(695, 26, 34, WLOTS.getPreferredSize().height);
            
            // ---- WLOTP ----
            WLOTP.setComponentPopupMenu(BTD2);
            WLOTP.setName("WLOTP");
            panel2.add(WLOTP);
            WLOTP.setBounds(385, 26, 312, WLOTP.getPreferredSize().height);
            
            // ---- WART21 ----
            WART21.setComponentPopupMenu(BTD);
            WART21.setName("WART21");
            panel2.add(WART21);
            WART21.setBounds(75, 55, 111, WART21.getPreferredSize().height);
            
            // ---- WART22 ----
            WART22.setComponentPopupMenu(BTD);
            WART22.setName("WART22");
            panel2.add(WART22);
            WART22.setBounds(185, 55, 50, WART22.getPreferredSize().height);
            
            // ---- WART23 ----
            WART23.setComponentPopupMenu(BTD);
            WART23.setName("WART23");
            panel2.add(WART23);
            WART23.setBounds(240, 55, 50, WART23.getPreferredSize().height);
            
            // ---- label5 ----
            label5.setText("Lot");
            label5.setName("label5");
            panel2.add(label5);
            label5.setBounds(350, 30, 35, 20);
            
            // ---- bt_argument ----
            bt_argument.setText("Article");
            bt_argument.setComponentPopupMenu(null);
            bt_argument.setToolTipText("Type d'argument de recherche");
            bt_argument.setHorizontalAlignment(SwingConstants.LEADING);
            bt_argument.setVerifyInputWhenFocusTarget(false);
            bt_argument.setMargin(new Insets(0, 0, 0, 0));
            bt_argument.setName("bt_argument");
            bt_argument.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_argumentActionPerformed(e);
              }
            });
            panel2.add(bt_argument);
            bt_argument.setBounds(80, 30, 183, 20);
            
            // ---- riBoutonDetail1 ----
            riBoutonDetail1.setToolTipText("Recherche avanc\u00e9e");
            riBoutonDetail1.setName("riBoutonDetail1");
            riBoutonDetail1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetail1ActionPerformed(e);
              }
            });
            panel2.add(riBoutonDetail1);
            riBoutonDetail1.setBounds(new Rectangle(new Point(270, 30), riBoutonDetail1.getPreferredSize()));
            
            // ---- b_options ----
            b_options.setText("Options sur ligne");
            b_options.setPreferredSize(new Dimension(190, 30));
            b_options.setMinimumSize(new Dimension(190, 30));
            b_options.setMaximumSize(new Dimension(190, 30));
            b_options.setHorizontalAlignment(SwingConstants.LEFT);
            b_options.setName("b_options");
            b_options.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                b_optionsActionPerformed(e);
              }
            });
            panel2.add(b_options);
            b_options.setBounds(10, 90, 190, 30);
            
            // ---- ck_saisie ----
            ck_saisie.setText("Saisie de quantit\u00e9s sur liste d'articles");
            ck_saisie.setToolTipText("Saisie quantit\u00e9 sur une liste ou recherche sur argument");
            ck_saisie.setName("ck_saisie");
            ck_saisie.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ck_saisieActionPerformed(e);
              }
            });
            panel2.add(ck_saisie);
            ck_saisie.setBounds(350, 57, 310, ck_saisie.getPreferredSize().height);
            
            // ======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setBorder(LineBorder.createGrayLineBorder());
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- OBJ_171 ----
              OBJ_171.setText("Tarif");
              OBJ_171.setName("OBJ_171");
              panel4.add(OBJ_171);
              OBJ_171.setBounds(10, 13, 35, 20);
              
              // ---- WLTAR ----
              WLTAR.setModel(new DefaultComboBoxModel(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
              WLTAR.setName("WLTAR");
              panel4.add(WLTAR);
              WLTAR.setBounds(45, 10, 44, WLTAR.getPreferredSize().height);
              
              // ---- WVAL ----
              WVAL.setToolTipText("Ligne en valeur");
              WVAL.setComponentPopupMenu(null);
              WVAL.setText("Ligne en valeur");
              WVAL.setName("WVAL");
              panel4.add(WVAL);
              WVAL.setBounds(470, 12, 115, 25);
              
              // ---- WAVR ----
              WAVR.setToolTipText("Ligne d'avoir");
              WAVR.setComponentPopupMenu(popupMenu2);
              WAVR.setName("WAVR");
              panel4.add(WAVR);
              WAVR.setBounds(240, 10, 24, WAVR.getPreferredSize().height);
              
              // ---- c_avoir ----
              c_avoir.setText("Avoir");
              c_avoir.setName("c_avoir");
              c_avoir.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  c_avoirActionPerformed(e);
                }
              });
              panel4.add(c_avoir);
              c_avoir.setBounds(95, 14, 130, c_avoir.getPreferredSize().height);
              
              // ---- WIN2 ----
              WIN2.setToolTipText("Type de ligne d'article gratuit");
              WIN2.setComponentPopupMenu(popupMenu2);
              WIN2.setName("WIN2");
              panel4.add(WIN2);
              WIN2.setBounds(440, 10, 24, WIN2.getPreferredSize().height);
              
              // ---- c_gratuit ----
              c_gratuit.setText("Gratuit");
              c_gratuit.setName("c_gratuit");
              c_gratuit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  c_gratuitActionPerformed(e);
                }
              });
              panel4.add(c_gratuit);
              c_gratuit.setBounds(355, 15, 65, c_gratuit.getPreferredSize().height);
              
              // ---- WMAGL ----
              WMAGL.setToolTipText("Magasin");
              WMAGL.setComponentPopupMenu(popupMenu2);
              WMAGL.setName("WMAGL");
              panel4.add(WMAGL);
              WMAGL.setBounds(645, 10, 34, WMAGL.getPreferredSize().height);
              
              // ---- OBJ_176 ----
              OBJ_176.setText("Magasin");
              OBJ_176.setName("OBJ_176");
              panel4.add(OBJ_176);
              OBJ_176.setBounds(590, 14, 55, 20);
              
              // ---- c_forfait ----
              c_forfait.setToolTipText("Ligne en valeur");
              c_forfait.setComponentPopupMenu(null);
              c_forfait.setText("Forfait");
              c_forfait.setName("c_forfait");
              c_forfait.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  c_forfaitActionPerformed(e);
                  c_forfaitActionPerformed(e);
                }
              });
              panel4.add(c_forfait);
              c_forfait.setBounds(275, 12, 70, 25);
              
              // ---- b_avoir ----
              b_avoir.setToolTipText("Choisir un type d'avoir");
              b_avoir.setName("b_avoir");
              b_avoir.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  b_avoirActionPerformed(e);
                }
              });
              panel4.add(b_avoir);
              b_avoir.setBounds(220, 10, 20, 25);
              
              // ---- b_gratuit ----
              b_gratuit.setToolTipText("Choisir un type de gratuit");
              b_gratuit.setName("b_gratuit");
              b_gratuit.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  b_gratuitActionPerformed(e);
                }
              });
              panel4.add(b_gratuit);
              b_gratuit.setBounds(420, 10, 20, 25);
              
              // ---- OBJ_174 ----
              OBJ_174.setText("Specif,Taxe,Kit...");
              OBJ_174.setName("OBJ_174");
              panel4.add(OBJ_174);
              OBJ_174.setBounds(685, 14, 100, 20);
              
              // ---- WIN3 ----
              WIN3.setToolTipText("Regroupement");
              WIN3.setComponentPopupMenu(null);
              WIN3.setName("WIN3");
              panel4.add(WIN3);
              WIN3.setBounds(780, 10, 24, WIN3.getPreferredSize().height);
              
              // ---- OBJ_175 ----
              OBJ_175.setText("Regroupement");
              OBJ_175.setName("OBJ_175");
              panel4.add(OBJ_175);
              OBJ_175.setBounds(820, 14, 100, 20);
              
              // ---- WIN21 ----
              WIN21.setToolTipText("Regroupement");
              WIN21.setComponentPopupMenu(null);
              WIN21.setName("WIN21");
              panel4.add(WIN21);
              WIN21.setBounds(915, 10, 24, WIN21.getPreferredSize().height);
              
              { // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel4);
            panel4.setBounds(10, 125, 950, 45);
            
            // ---- label7 ----
            label7.setText("N\u00b0Ligne");
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(15, 30, 55, 20);
          }
          p_contenu.add(panel2, new GridBagConstraints(0, 2, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- label6 ----
          label6.setText("Fonction r\u00e9percussion active");
          label6.setHorizontalAlignment(SwingConstants.RIGHT);
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          p_contenu.add(label6, new GridBagConstraints(2, 0, 1, 1, 80.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- E1NFA ----
          E1NFA.setOpaque(false);
          E1NFA.setText("@E1NFA@");
          E1NFA.setHorizontalAlignment(SwingConstants.RIGHT);
          E1NFA.setName("E1NFA");
          p_contenu.add(E1NFA, new GridBagConstraints(1, 0, 1, 1, 10.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- label9 ----
          label9.setText("Facture");
          label9.setName("label9");
          p_contenu.add(label9, new GridBagConstraints(0, 0, 1, 1, 10.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(10, 10, 10, 10), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- copier ----
      copier.setText("Copier");
      copier.setToolTipText("Copier une ligne afin de la dupliquer");
      copier.setName("copier");
      copier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          copierActionPerformed(e);
        }
      });
      BTD.add(copier);
      
      // ---- couper ----
      couper.setText("Couper");
      couper.setToolTipText(" Couper une ligne afin de la d\u00e9placer");
      couper.setName("couper");
      couper.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          couperActionPerformed(e);
        }
      });
      BTD.add(couper);
      
      // ---- coller ----
      coller.setText("Coller");
      coller.setToolTipText("Coller une ligne sous la ligne s\u00e9lectionn\u00e9e ");
      coller.setName("coller");
      coller.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          collerActionPerformed(e);
        }
      });
      BTD.add(coller);
      BTD.addSeparator();
      
      // ---- OBJ_27 ----
      OBJ_27.setText("Modifier");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD.add(OBJ_27);
      
      // ---- OBJ_28 ----
      OBJ_28.setText("Annuler");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD.add(OBJ_28);
      
      // ---- OBJ_44 ----
      OBJ_44.setText("Ins\u00e9rer une ligne avant");
      OBJ_44.setName("OBJ_44");
      OBJ_44.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_44ActionPerformed(e);
        }
      });
      BTD.add(OBJ_44);
      
      // ---- OBJ_30 ----
      OBJ_30.setText("Options article");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD.add(OBJ_30);
      
      // ---- OBJ_31 ----
      OBJ_31.setText("Visualisation marge");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);
      
      // ---- OBJ_39 ----
      OBJ_39.setText("");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      BTD.add(OBJ_39);
      
      // ---- OBJ_45 ----
      OBJ_45.setText("Sous total");
      OBJ_45.setToolTipText("Sous total");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_45ActionPerformed(e);
        }
      });
      BTD.add(OBJ_45);
      
      // ---- OBJ_53 ----
      OBJ_53.setText("Forfait avec ligne d'\u00e9cart");
      OBJ_53.setToolTipText("Sous total");
      OBJ_53.setName("OBJ_53");
      OBJ_53.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_53ActionPerformed(e);
        }
      });
      BTD.add(OBJ_53);
      
      // ---- OBJ_50 ----
      OBJ_50.setText("Lien sur achats");
      OBJ_50.setName("OBJ_50");
      OBJ_50.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      BTD.add(OBJ_50);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- OBJ_52 ----
      OBJ_52.setText("Choix possibles");
      OBJ_52.setName("OBJ_52");
      OBJ_52.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_52ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_52);
      
      // ---- OBJ_49 ----
      OBJ_49.setText("Recherche scan");
      OBJ_49.setName("OBJ_49");
      OBJ_49.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_49ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_49);
      
      // ---- OBJ_47 ----
      OBJ_47.setText("Recherche des articles \u00e9quivalents");
      OBJ_47.setName("OBJ_47");
      OBJ_47.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_47ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_47);
      
      // ---- OBJ_48 ----
      OBJ_48.setText("Options article");
      OBJ_48.setName("OBJ_48");
      OBJ_48.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_48ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_48);
      
      // ---- OBJ_46 ----
      OBJ_46.setText("Configurateur");
      OBJ_46.setName("OBJ_46");
      OBJ_46.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_46ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_46);
      
      // ---- OBJ_51 ----
      OBJ_51.setText("Aide en ligne");
      OBJ_51.setName("OBJ_51");
      OBJ_51.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_51ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_51);
    }
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- OBJ_56 ----
      OBJ_56.setText("Choix possibles");
      OBJ_56.setName("OBJ_56");
      OBJ_56.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_56ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_56);
      
      // ---- OBJ_54 ----
      OBJ_54.setText("Regroupement/totalisation multi-lignes");
      OBJ_54.setName("OBJ_54");
      OBJ_54.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_54ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_54);
      
      // ---- OBJ_55 ----
      OBJ_55.setText("Aide en ligne");
      OBJ_55.setName("OBJ_55");
      OBJ_55.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_55ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_55);
    }
    
    // ======== FONC4 ========
    {
      FONC4.setName("FONC4");
      
      // ---- OBJ_58 ----
      OBJ_58.setText("Convertisseur mon\u00e9taire");
      OBJ_58.setName("OBJ_58");
      OBJ_58.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_58ActionPerformed(e);
        }
      });
      FONC4.add(OBJ_58);
    }
    
    // ======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");
      
      // ---- OBJ_32 ----
      OBJ_32.setText("Gestion regroupement de lignes (Kit, sous-total)");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_32);
      
      // ---- OBJ_33 ----
      OBJ_33.setText("Gestion d'un forfait commentaire avec ligne d'\u00e9cart");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_33);
      
      // ---- OBJ_34 ----
      OBJ_34.setText("Article \u00e9puis\u00e9");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_34);
      
      // ---- OBJ_35 ----
      OBJ_35.setText("Intervention GMM");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_35);
      
      // ---- OBJ_36 ----
      OBJ_36.setText("Lien sur achats");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_36);
      
      // ---- OBJ_37 ----
      OBJ_37.setText("D\u00e9rogation");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_37);
      
      // ---- OBJ_41 ----
      OBJ_41.setText("Affectation adresses de stocks");
      OBJ_41.setName("OBJ_41");
      OBJ_41.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_41ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_41);
      
      // ---- OBJ_40 ----
      OBJ_40.setText("Saisie dimensions / NMC");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_40);
      
      // ---- OBJ_43 ----
      OBJ_43.setText("Saisie tableau (sp\u00e9cial taille/couleur)");
      OBJ_43.setName("OBJ_43");
      OBJ_43.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_43ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_43);
      
      // ---- OBJ_42 ----
      OBJ_42.setText("Saisie vecteur (sp\u00e9cial taille/couleur)");
      OBJ_42.setName("OBJ_42");
      OBJ_42.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_42ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_42);
    }
    
    // ---- OBJ_38 ----
    OBJ_38.setText("Duplication");
    OBJ_38.setName("OBJ_38");
    OBJ_38.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_38ActionPerformed(e);
      }
    });
    
    // ---- E1CNV ----
    E1CNV.setText("@E1CNV@");
    E1CNV.setName("E1CNV");
    
    // ---- WCNP ----
    WCNP.setText("@WCNP@");
    WCNP.setHorizontalAlignment(SwingConstants.CENTER);
    WCNP.setName("WCNP");
    
    // ---- OBJ_142 ----
    OBJ_142.setText("@WCHPA@");
    OBJ_142.setName("OBJ_142");
    
    // ======== arguments ========
    {
      arguments.setName("arguments");
      
      // ---- menuItem2 ----
      menuItem2.setText("Recherche");
      menuItem2.setFont(menuItem2.getFont().deriveFont(Font.BOLD));
      menuItem2.setBackground(new Color(204, 204, 204));
      menuItem2.setOpaque(true);
      menuItem2.setRequestFocusEnabled(false);
      menuItem2.setName("menuItem2");
      arguments.add(menuItem2);
      
      // ---- arg_article ----
      arg_article.setText("Par code article");
      arg_article.setName("arg_article");
      arg_article.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_articleActionPerformed(e);
        }
      });
      arguments.add(arg_article);
      
      // ---- arg_famille ----
      arg_famille.setText("Par famille");
      arg_famille.setName("arg_famille");
      arg_famille.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_familleActionPerformed(e);
        }
      });
      arguments.add(arg_famille);
      
      // ---- arg_classement ----
      arg_classement.setText("Par @TMC1@");
      arg_classement.setName("arg_classement");
      arg_classement.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_classementActionPerformed(e);
        }
      });
      arguments.add(arg_classement);
      
      // ---- arg_classement2 ----
      arg_classement2.setText("Par @TMC2@");
      arg_classement2.setName("arg_classement2");
      arg_classement2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_classement2ActionPerformed(e);
        }
      });
      arguments.add(arg_classement2);
      
      // ---- arg_gencod ----
      arg_gencod.setText("Par code \u00e0 barres");
      arg_gencod.setName("arg_gencod");
      arg_gencod.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_gencodActionPerformed(e);
        }
      });
      arguments.add(arg_gencod);
      
      // ---- arg_art_comm ----
      arg_art_comm.setText("Article commentaire");
      arg_art_comm.setToolTipText("Recherche d'article commentaires par le code");
      arg_art_comm.setFont(arg_art_comm.getFont().deriveFont(arg_art_comm.getFont().getStyle() | Font.ITALIC));
      arg_art_comm.setName("arg_art_comm");
      arg_art_comm.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_art_commActionPerformed(e);
        }
      });
      arguments.add(arg_art_comm);
      
      // ---- arg_comm ----
      arg_comm.setText("Saisie commentaire");
      arg_comm.setBackground(new Color(217, 217, 217));
      arg_comm.setOpaque(true);
      arg_comm.setToolTipText("saisie commentaire libre");
      arg_comm.setFont(arg_comm.getFont().deriveFont(arg_comm.getFont().getStyle() | Font.ITALIC));
      arg_comm.setName("arg_comm");
      arg_comm.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          arg_commActionPerformed(e);
        }
      });
      arguments.add(arg_comm);
    }
    
    // ---- menuItem1 ----
    menuItem1.setText("+ Autres options");
    menuItem1.setName("menuItem1");
    menuItem1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        menuItem1ActionPerformed(e);
      }
    });
    
    // ---- button1 ----
    button1.setText("pied de bons : ne pas supprimer");
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });
    
    // ======== riSousMenu2 ========
    {
      riSousMenu2.setName("riSousMenu2");
      
      // ---- riSousMenu_bt2 ----
      riSousMenu_bt2.setText("Code article morcel\u00e9");
      riSousMenu_bt2.setToolTipText("Saisie code article morcel\u00e9");
      riSousMenu_bt2.setName("riSousMenu_bt2");
      riSousMenu_bt2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt2ActionPerformed(e);
        }
      });
      riSousMenu2.add(riSousMenu_bt2);
    }
    
    // ======== popupMenu2 ========
    {
      popupMenu2.setName("popupMenu2");
      
      // ---- menuItem3 ----
      menuItem3.setText("Choix possibles");
      menuItem3.setName("menuItem3");
      menuItem3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem3ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem3);
    }
    
    // ---- label8 ----
    label8.setText("Num\u00e9ro");
    label8.setName("label8");
    
    // ---- riSousMenu_bt13 ----
    riSousMenu_bt13.setText("Param\u00e9trage colonnes");
    riSousMenu_bt13.setToolTipText("Param\u00e9trages des colonnes constitutives du bons de vente");
    riSousMenu_bt13.setName("riSousMenu_bt13");
    riSousMenu_bt13.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        riSousMenu_bt13ActionPerformed(e);
      }
    });
    
    // ======== riSousMenu13 ========
    {
      riSousMenu13.setName("riSousMenu13");
    }
    
    // ======== riMenu_V01F ========
    {
      riMenu_V01F.setMinimumSize(new Dimension(104, 50));
      riMenu_V01F.setPreferredSize(new Dimension(170, 50));
      riMenu_V01F.setMaximumSize(new Dimension(104, 50));
      riMenu_V01F.setName("riMenu_V01F");
      
      // ---- riMenu_bt_V01F ----
      riMenu_bt_V01F.setText("@V01F@");
      riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
      riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
      riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
      riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
      riMenu_bt_V01F.setName("riMenu_bt_V01F");
      riMenu_V01F.add(riMenu_bt_V01F);
    }
    
    // ======== riSousMenu_consult ========
    {
      riSousMenu_consult.setName("riSousMenu_consult");
      
      // ---- riSousMenu_bt_consult ----
      riSousMenu_bt_consult.setText("Consultation");
      riSousMenu_bt_consult.setToolTipText("Consultation");
      riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
      riSousMenu_consult.add(riSousMenu_bt_consult);
    }
    
    // ======== riSousMenu_modif ========
    {
      riSousMenu_modif.setName("riSousMenu_modif");
      
      // ---- riSousMenu_bt_modif ----
      riSousMenu_bt_modif.setText("Modification");
      riSousMenu_bt_modif.setToolTipText("Modification");
      riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
      riSousMenu_modif.add(riSousMenu_bt_modif);
    }
    
    // ======== riSousMenu_crea ========
    {
      riSousMenu_crea.setName("riSousMenu_crea");
      
      // ---- riSousMenu_bt_crea ----
      riSousMenu_bt_crea.setText("Cr\u00e9ation");
      riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
      riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
      riSousMenu_crea.add(riSousMenu_bt_crea);
    }
    
    // ======== riSousMenu_suppr ========
    {
      riSousMenu_suppr.setName("riSousMenu_suppr");
      
      // ---- riSousMenu_bt_suppr ----
      riSousMenu_bt_suppr.setText("Suppression");
      riSousMenu_bt_suppr.setToolTipText("Suppression");
      riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
      riSousMenu_suppr.add(riSousMenu_bt_suppr);
    }
    
    // ======== riSousMenuF_dupli ========
    {
      riSousMenuF_dupli.setName("riSousMenuF_dupli");
      
      // ---- riSousMenu_bt_dupli ----
      riSousMenu_bt_dupli.setText("Duplication");
      riSousMenu_bt_dupli.setToolTipText("Duplication");
      riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
      riSousMenuF_dupli.add(riSousMenu_bt_dupli);
    }
    
    // ======== riSousMenuF_dupli2 ========
    {
      riSousMenuF_dupli2.setName("riSousMenuF_dupli2");
      
      // ---- riSousMenu_bt_dupli2 ----
      riSousMenu_bt_dupli2.setText("Annulation");
      riSousMenu_bt_dupli2.setToolTipText("Annulation");
      riSousMenu_bt_dupli2.setName("riSousMenu_bt_dupli2");
      riSousMenu_bt_dupli2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt_dupli2ActionPerformed(e);
        }
      });
      riSousMenuF_dupli2.add(riSousMenu_bt_dupli2);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private XRiTextField WCOLC;
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie OBJ_97;
  private RiZoneSortie WDATEX;
  private RiZoneSortie WNUM;
  private RiZoneSortie E1ETB;
  private RiZoneSortie WSUF;
  private JLabel label1;
  private RiZoneSortie z_etat;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JPanel p_tete_droite;
  private SNBoutonDocument riBoutonVisu1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu27;
  private RiSousMenu_bt riSousMenu_bt26;
  private RiSousMenu riSousMenu28;
  private RiSousMenu_bt riSousMenu_bt27;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu26;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu17;
  private RiSousMenu_bt riSousMenu_bt17;
  private RiSousMenu riSousMenu22;
  private RiSousMenu_bt riSousMenu_bt22;
  private RiSousMenu riSousMenu23;
  private RiSousMenu_bt riSousMenu_bt23;
  private RiSousMenu riSousMenu24;
  private RiSousMenu_bt riSousMenu_bt24;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu25;
  private RiSousMenu_bt riSousMenu_bt25;
  private RiMenu riMenu5;
  private RiMenu_bt riMenu_bt5;
  private RiSousMenu riSousMenu18;
  private JPanel p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_DEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_FIN;
  private JPanel panel2;
  private XRiTextField WARTT;
  private RiZoneSortie devise;
  private RiZoneSortie E1TTC;
  private RiZoneSortie WTVA;
  private JLabel OBJ_161;
  private JLabel OBJ_235;
  private JLabel OBJ_255;
  private JLabel OBJ_166;
  private XRiTextField WNLI;
  private RiZoneSortie WTHT;
  private XRiTextField WPDSC;
  private XRiTextField WLOTS;
  private XRiTextField WLOTP;
  private XRiTextField WART21;
  private XRiTextField WART22;
  private XRiTextField WART23;
  private JLabel label5;
  private SNBoutonLeger bt_argument;
  private SNBoutonDetail riBoutonDetail1;
  private JButton b_options;
  private JCheckBox ck_saisie;
  private JPanel panel4;
  private JLabel OBJ_171;
  private XRiComboBox WLTAR;
  private XRiCheckBox WVAL;
  private XRiTextField WAVR;
  private JCheckBox c_avoir;
  private XRiTextField WIN2;
  private JCheckBox c_gratuit;
  private XRiTextField WMAGL;
  private JLabel OBJ_176;
  private JCheckBox c_forfait;
  private SNBoutonDetail b_avoir;
  private SNBoutonDetail b_gratuit;
  private JLabel OBJ_174;
  private XRiTextField WIN3;
  private JLabel OBJ_175;
  private XRiTextField WIN21;
  private JLabel label7;
  private JLabel label6;
  private RiZoneSortie E1NFA;
  private JLabel label9;
  private JPopupMenu BTD;
  private JMenuItem copier;
  private JMenuItem couper;
  private JMenuItem coller;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_44;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_39;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_53;
  private JMenuItem OBJ_50;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_52;
  private JMenuItem OBJ_49;
  private JMenuItem OBJ_47;
  private JMenuItem OBJ_48;
  private JMenuItem OBJ_46;
  private JMenuItem OBJ_51;
  private JPopupMenu BTD3;
  private JMenuItem OBJ_56;
  private JMenuItem OBJ_54;
  private JMenuItem OBJ_55;
  private JPopupMenu FONC4;
  private JMenuItem OBJ_58;
  private JPopupMenu popupMenu1;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_37;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_43;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_38;
  private RiZoneSortie E1CNV;
  private JLabel WCNP;
  private JLabel OBJ_142;
  private JPopupMenu arguments;
  private JMenuItem menuItem2;
  private JMenuItem arg_article;
  private JMenuItem arg_famille;
  private JMenuItem arg_classement;
  private JMenuItem arg_classement2;
  private JMenuItem arg_gencod;
  private JMenuItem arg_art_comm;
  private JMenuItem arg_comm;
  private JMenuItem menuItem1;
  private JButton button1;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private JPopupMenu popupMenu2;
  private JMenuItem menuItem3;
  private JLabel label8;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu13;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenuF_dupli2;
  private RiSousMenu_bt riSousMenu_bt_dupli2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
