
package ri.serien.libecranrpg.vgvm.VGVM64FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM64FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM64FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULIB1@")).trim());
    OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ULIB2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WETB.setEnabled(lexique.isPresent("WETB"));
    INDARD.setEnabled(lexique.isPresent("INDARD"));
    OBJ_20.setVisible(lexique.isPresent("ULIB2"));
    OBJ_19.setVisible(lexique.isPresent("ULIB1"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_33 = new JLabel();
    WETB = new XRiTextField();
    OBJ_34 = new JLabel();
    INDARD = new XRiTextField();
    OBJ_19 = new RiZoneSortie();
    OBJ_20 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel4 = new JPanel();
    ART01 = new XRiTextField();
    ART02 = new XRiTextField();
    ART03 = new XRiTextField();
    ART04 = new XRiTextField();
    ART05 = new XRiTextField();
    ART06 = new XRiTextField();
    ART07 = new XRiTextField();
    ART08 = new XRiTextField();
    ART09 = new XRiTextField();
    ART10 = new XRiTextField();
    ART11 = new XRiTextField();
    ART12 = new XRiTextField();
    ART13 = new XRiTextField();
    ART14 = new XRiTextField();
    ART15 = new XRiTextField();
    LIB01 = new XRiTextField();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB06 = new XRiTextField();
    LIB07 = new XRiTextField();
    LIB08 = new XRiTextField();
    LIB09 = new XRiTextField();
    LIB10 = new XRiTextField();
    LIB11 = new XRiTextField();
    LIB12 = new XRiTextField();
    LIB13 = new XRiTextField();
    LIB14 = new XRiTextField();
    LIB15 = new XRiTextField();
    NBR01 = new XRiTextField();
    NBR02 = new XRiTextField();
    NBR03 = new XRiTextField();
    NBR04 = new XRiTextField();
    NBR05 = new XRiTextField();
    NBR06 = new XRiTextField();
    NBR07 = new XRiTextField();
    NBR08 = new XRiTextField();
    NBR09 = new XRiTextField();
    NBR10 = new XRiTextField();
    NBR11 = new XRiTextField();
    NBR12 = new XRiTextField();
    NBR13 = new XRiTextField();
    NBR14 = new XRiTextField();
    NBR15 = new XRiTextField();
    TYP01 = new XRiTextField();
    TYP02 = new XRiTextField();
    TYP03 = new XRiTextField();
    TYP04 = new XRiTextField();
    TYP05 = new XRiTextField();
    TYP06 = new XRiTextField();
    TYP07 = new XRiTextField();
    TYP08 = new XRiTextField();
    TYP09 = new XRiTextField();
    TYP10 = new XRiTextField();
    TYP11 = new XRiTextField();
    TYP12 = new XRiTextField();
    TYP13 = new XRiTextField();
    TYP14 = new XRiTextField();
    TYP15 = new XRiTextField();
    EDT01 = new XRiTextField();
    EDT02 = new XRiTextField();
    EDT03 = new XRiTextField();
    EDT04 = new XRiTextField();
    EDT05 = new XRiTextField();
    EDT06 = new XRiTextField();
    EDT07 = new XRiTextField();
    EDT08 = new XRiTextField();
    EDT09 = new XRiTextField();
    EDT10 = new XRiTextField();
    EDT11 = new XRiTextField();
    EDT12 = new XRiTextField();
    EDT13 = new XRiTextField();
    EDT14 = new XRiTextField();
    EDT15 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Taxes sur article");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(900, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_33 ----
          OBJ_33.setText("Etablissement");
          OBJ_33.setName("OBJ_33");

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");

          //---- OBJ_34 ----
          OBJ_34.setText("Article d\u00e9part");
          OBJ_34.setName("OBJ_34");

          //---- INDARD ----
          INDARD.setComponentPopupMenu(BTD);
          INDARD.setName("INDARD");

          //---- OBJ_19 ----
          OBJ_19.setText("@ULIB1@");
          OBJ_19.setOpaque(false);
          OBJ_19.setName("OBJ_19");

          //---- OBJ_20 ----
          OBJ_20.setText("@ULIB2@");
          OBJ_20.setOpaque(false);
          OBJ_20.setName("OBJ_20");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addComponent(INDARD, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(66, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(OBJ_33, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(OBJ_34, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(INDARD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setBorder(new TitledBorder(""));
            panel4.setName("panel4");

            //---- ART01 ----
            ART01.setName("ART01");

            //---- ART02 ----
            ART02.setName("ART02");

            //---- ART03 ----
            ART03.setName("ART03");

            //---- ART04 ----
            ART04.setName("ART04");

            //---- ART05 ----
            ART05.setName("ART05");

            //---- ART06 ----
            ART06.setName("ART06");

            //---- ART07 ----
            ART07.setName("ART07");

            //---- ART08 ----
            ART08.setName("ART08");

            //---- ART09 ----
            ART09.setName("ART09");

            //---- ART10 ----
            ART10.setName("ART10");

            //---- ART11 ----
            ART11.setName("ART11");

            //---- ART12 ----
            ART12.setName("ART12");

            //---- ART13 ----
            ART13.setName("ART13");

            //---- ART14 ----
            ART14.setName("ART14");

            //---- ART15 ----
            ART15.setName("ART15");

            //---- LIB01 ----
            LIB01.setName("LIB01");

            //---- LIB02 ----
            LIB02.setName("LIB02");

            //---- LIB03 ----
            LIB03.setName("LIB03");

            //---- LIB04 ----
            LIB04.setName("LIB04");

            //---- LIB05 ----
            LIB05.setName("LIB05");

            //---- LIB06 ----
            LIB06.setName("LIB06");

            //---- LIB07 ----
            LIB07.setName("LIB07");

            //---- LIB08 ----
            LIB08.setName("LIB08");

            //---- LIB09 ----
            LIB09.setName("LIB09");

            //---- LIB10 ----
            LIB10.setName("LIB10");

            //---- LIB11 ----
            LIB11.setName("LIB11");

            //---- LIB12 ----
            LIB12.setName("LIB12");

            //---- LIB13 ----
            LIB13.setName("LIB13");

            //---- LIB14 ----
            LIB14.setName("LIB14");

            //---- LIB15 ----
            LIB15.setName("LIB15");

            //---- NBR01 ----
            NBR01.setName("NBR01");

            //---- NBR02 ----
            NBR02.setName("NBR02");

            //---- NBR03 ----
            NBR03.setName("NBR03");

            //---- NBR04 ----
            NBR04.setName("NBR04");

            //---- NBR05 ----
            NBR05.setName("NBR05");

            //---- NBR06 ----
            NBR06.setName("NBR06");

            //---- NBR07 ----
            NBR07.setName("NBR07");

            //---- NBR08 ----
            NBR08.setName("NBR08");

            //---- NBR09 ----
            NBR09.setName("NBR09");

            //---- NBR10 ----
            NBR10.setName("NBR10");

            //---- NBR11 ----
            NBR11.setName("NBR11");

            //---- NBR12 ----
            NBR12.setName("NBR12");

            //---- NBR13 ----
            NBR13.setName("NBR13");

            //---- NBR14 ----
            NBR14.setName("NBR14");

            //---- NBR15 ----
            NBR15.setName("NBR15");

            //---- TYP01 ----
            TYP01.setName("TYP01");

            //---- TYP02 ----
            TYP02.setName("TYP02");

            //---- TYP03 ----
            TYP03.setName("TYP03");

            //---- TYP04 ----
            TYP04.setName("TYP04");

            //---- TYP05 ----
            TYP05.setName("TYP05");

            //---- TYP06 ----
            TYP06.setName("TYP06");

            //---- TYP07 ----
            TYP07.setName("TYP07");

            //---- TYP08 ----
            TYP08.setName("TYP08");

            //---- TYP09 ----
            TYP09.setName("TYP09");

            //---- TYP10 ----
            TYP10.setName("TYP10");

            //---- TYP11 ----
            TYP11.setName("TYP11");

            //---- TYP12 ----
            TYP12.setName("TYP12");

            //---- TYP13 ----
            TYP13.setName("TYP13");

            //---- TYP14 ----
            TYP14.setName("TYP14");

            //---- TYP15 ----
            TYP15.setName("TYP15");

            //---- EDT01 ----
            EDT01.setName("EDT01");

            //---- EDT02 ----
            EDT02.setName("EDT02");

            //---- EDT03 ----
            EDT03.setName("EDT03");

            //---- EDT04 ----
            EDT04.setName("EDT04");

            //---- EDT05 ----
            EDT05.setName("EDT05");

            //---- EDT06 ----
            EDT06.setName("EDT06");

            //---- EDT07 ----
            EDT07.setName("EDT07");

            //---- EDT08 ----
            EDT08.setName("EDT08");

            //---- EDT09 ----
            EDT09.setName("EDT09");

            //---- EDT10 ----
            EDT10.setName("EDT10");

            //---- EDT11 ----
            EDT11.setName("EDT11");

            //---- EDT12 ----
            EDT12.setName("EDT12");

            //---- EDT13 ----
            EDT13.setName("EDT13");

            //---- EDT14 ----
            EDT14.setName("EDT14");

            //---- EDT15 ----
            EDT15.setName("EDT15");

            //---- label1 ----
            label1.setText("Article \"taxe\"");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");

            //---- label2 ----
            label2.setText("Libell\u00e9");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");

            //---- label3 ----
            label3.setText("Nbr.");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setName("label3");

            //---- label4 ----
            label4.setText("T");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setHorizontalAlignment(SwingConstants.CENTER);
            label4.setName("label4");

            //---- label5 ----
            label5.setText("E");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setHorizontalAlignment(SwingConstants.CENTER);
            label5.setName("label5");

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(14, 14, 14)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(label1)
                      .addGap(113, 113, 113)
                      .addComponent(label2)
                      .addGap(213, 213, 213)
                      .addComponent(label3, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(ART01, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART02, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART05, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART06, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART09, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART11, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART04, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART13, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART10, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART08, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART12, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART14, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART07, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART15, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ART03, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(LIB04, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB03, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB05, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB01, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB06, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB02, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB15, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB09, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB08, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB07, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(NBR01, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR13, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR12, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR08, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR09, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR05, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR14, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR15, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR10, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR07, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR06, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR04, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR11, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR03, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                        .addComponent(NBR02, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(TYP02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(TYP04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGap(10, 10, 10)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(EDT01, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT14, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT09, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT04, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT03, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT06, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT08, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT07, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT12, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT05, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(EDT02, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))))
                  .addContainerGap(45, Short.MAX_VALUE))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addComponent(label1)
                    .addComponent(label2)
                    .addComponent(label3)
                    .addComponent(label4)
                    .addComponent(label5))
                  .addGap(4, 4, 4)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(ART01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(ART02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(ART05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(ART06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(ART09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(ART11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(ART04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(ART13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(ART10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(ART08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(ART12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(ART14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(ART07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(ART15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(50, 50, 50)
                      .addComponent(ART03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIB04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIB03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIB05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LIB01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIB06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIB02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(LIB15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIB09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LIB08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(150, 150, 150)
                      .addComponent(LIB07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(NBR01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(NBR13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(NBR12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(NBR08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(NBR09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(NBR05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(NBR14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(300, 300, 300)
                          .addComponent(NBR15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(NBR10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(NBR07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(NBR06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(NBR04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(NBR11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(NBR03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(NBR02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(TYP02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(TYP01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(TYP03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(TYP07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(TYP14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(TYP06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(TYP05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(TYP15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(TYP10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(TYP12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(TYP11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(TYP13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(TYP08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(TYP09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(75, 75, 75)
                      .addComponent(TYP04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(EDT01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(EDT14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(EDT09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(EDT13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(EDT04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(300, 300, 300)
                          .addComponent(EDT15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(EDT03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(EDT06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(EDT10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(EDT08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(EDT07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(EDT11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(EDT12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel4Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(EDT05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(EDT02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(71, 71, 71)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_33;
  private XRiTextField WETB;
  private JLabel OBJ_34;
  private XRiTextField INDARD;
  private RiZoneSortie OBJ_19;
  private RiZoneSortie OBJ_20;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel4;
  private XRiTextField ART01;
  private XRiTextField ART02;
  private XRiTextField ART03;
  private XRiTextField ART04;
  private XRiTextField ART05;
  private XRiTextField ART06;
  private XRiTextField ART07;
  private XRiTextField ART08;
  private XRiTextField ART09;
  private XRiTextField ART10;
  private XRiTextField ART11;
  private XRiTextField ART12;
  private XRiTextField ART13;
  private XRiTextField ART14;
  private XRiTextField ART15;
  private XRiTextField LIB01;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB06;
  private XRiTextField LIB07;
  private XRiTextField LIB08;
  private XRiTextField LIB09;
  private XRiTextField LIB10;
  private XRiTextField LIB11;
  private XRiTextField LIB12;
  private XRiTextField LIB13;
  private XRiTextField LIB14;
  private XRiTextField LIB15;
  private XRiTextField NBR01;
  private XRiTextField NBR02;
  private XRiTextField NBR03;
  private XRiTextField NBR04;
  private XRiTextField NBR05;
  private XRiTextField NBR06;
  private XRiTextField NBR07;
  private XRiTextField NBR08;
  private XRiTextField NBR09;
  private XRiTextField NBR10;
  private XRiTextField NBR11;
  private XRiTextField NBR12;
  private XRiTextField NBR13;
  private XRiTextField NBR14;
  private XRiTextField NBR15;
  private XRiTextField TYP01;
  private XRiTextField TYP02;
  private XRiTextField TYP03;
  private XRiTextField TYP04;
  private XRiTextField TYP05;
  private XRiTextField TYP06;
  private XRiTextField TYP07;
  private XRiTextField TYP08;
  private XRiTextField TYP09;
  private XRiTextField TYP10;
  private XRiTextField TYP11;
  private XRiTextField TYP12;
  private XRiTextField TYP13;
  private XRiTextField TYP14;
  private XRiTextField TYP15;
  private XRiTextField EDT01;
  private XRiTextField EDT02;
  private XRiTextField EDT03;
  private XRiTextField EDT04;
  private XRiTextField EDT05;
  private XRiTextField EDT06;
  private XRiTextField EDT07;
  private XRiTextField EDT08;
  private XRiTextField EDT09;
  private XRiTextField EDT10;
  private XRiTextField EDT11;
  private XRiTextField EDT12;
  private XRiTextField EDT13;
  private XRiTextField EDT14;
  private XRiTextField EDT15;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
