
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_ST extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_ST(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    STST8.setValeursSelection("1", " ");
    STST4.setValeursSelection("1", " ");
    STST9.setValeursSelection("1", " ");
    STST3.setValeursSelection("1", " ");
    STST6.setValeursSelection("1", " ");
    STST10.setValeursSelection("1", " ");
    STST2.setValeursSelection("1", " ");
    STST1.setValeursSelection("1", " ");
    STST5.setValeursSelection("1", " ");
    STST14.setValeursSelection("1", " ");
    STST12.setValeursSelection("1", " ");
    STST13.setValeursSelection("1", " ");
    STST7.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OBSERV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    OBJ_78 = new JLabel();
    OBJ_82 = new JLabel();
    OBJ_75 = new JLabel();
    OBJ_79 = new JLabel();
    STNAT1 = new XRiTextField();
    STNAT2 = new XRiTextField();
    OBJ_76 = new JLabel();
    OBJ_80 = new JLabel();
    STCS11 = new XRiTextField();
    STCS12 = new XRiTextField();
    STCS21 = new XRiTextField();
    STCS22 = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_81 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_83 = new JLabel();
    STST7 = new XRiCheckBox();
    STST13 = new XRiCheckBox();
    STST12 = new XRiCheckBox();
    STST14 = new XRiCheckBox();
    STST5 = new XRiCheckBox();
    STST1 = new XRiCheckBox();
    STST2 = new XRiCheckBox();
    STST10 = new XRiCheckBox();
    STST6 = new XRiCheckBox();
    STST3 = new XRiCheckBox();
    STST9 = new XRiCheckBox();
    STST4 = new XRiCheckBox();
    STST8 = new XRiCheckBox();
    STTT01 = new XRiTextField();
    STTT02 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setPreferredSize(new Dimension(780, 405));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(770, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Statistiques crois\u00e9es/ventes");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- OBJ_78 ----
            OBJ_78.setText("pour les \u00e9l\u00e9ments");
            OBJ_78.setName("OBJ_78");
            xTitledPanel3ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(400, 15, 109, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("pour les \u00e9l\u00e9ments");
            OBJ_82.setName("OBJ_82");
            xTitledPanel3ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(400, 50, 109, 20);

            //---- OBJ_75 ----
            OBJ_75.setText("Croisement 1");
            OBJ_75.setName("OBJ_75");
            xTitledPanel3ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(10, 15, 81, 20);

            //---- OBJ_79 ----
            OBJ_79.setText("Croisement 2");
            OBJ_79.setName("OBJ_79");
            xTitledPanel3ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(10, 50, 81, 20);

            //---- STNAT1 ----
            STNAT1.setComponentPopupMenu(BTD);
            STNAT1.setName("STNAT1");
            xTitledPanel3ContentContainer.add(STNAT1);
            STNAT1.setBounds(675, 10, 40, STNAT1.getPreferredSize().height);

            //---- STNAT2 ----
            STNAT2.setComponentPopupMenu(BTD);
            STNAT2.setName("STNAT2");
            xTitledPanel3ContentContainer.add(STNAT2);
            STNAT2.setBounds(675, 45, 40, STNAT2.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("entre");
            OBJ_76.setName("OBJ_76");
            xTitledPanel3ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(130, 15, 33, 20);

            //---- OBJ_80 ----
            OBJ_80.setText("entre");
            OBJ_80.setName("OBJ_80");
            xTitledPanel3ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(130, 50, 33, 20);

            //---- STCS11 ----
            STCS11.setComponentPopupMenu(BTD);
            STCS11.setName("STCS11");
            xTitledPanel3ContentContainer.add(STCS11);
            STCS11.setBounds(170, 10, 20, STCS11.getPreferredSize().height);

            //---- STCS12 ----
            STCS12.setComponentPopupMenu(BTD);
            STCS12.setName("STCS12");
            xTitledPanel3ContentContainer.add(STCS12);
            STCS12.setBounds(290, 10, 20, STCS12.getPreferredSize().height);

            //---- STCS21 ----
            STCS21.setComponentPopupMenu(BTD);
            STCS21.setName("STCS21");
            xTitledPanel3ContentContainer.add(STCS21);
            STCS21.setBounds(170, 45, 20, STCS21.getPreferredSize().height);

            //---- STCS22 ----
            STCS22.setComponentPopupMenu(BTD);
            STCS22.setName("STCS22");
            xTitledPanel3ContentContainer.add(STCS22);
            STCS22.setBounds(290, 45, 20, STCS22.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("et");
            OBJ_77.setName("OBJ_77");
            xTitledPanel3ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(230, 15, 15, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("et");
            OBJ_81.setName("OBJ_81");
            xTitledPanel3ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(230, 50, 15, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Choix des croisements statistiques sp\u00e9ciaux");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- OBJ_83 ----
            OBJ_83.setText("@OBSERV@");
            OBJ_83.setName("OBJ_83");
            xTitledPanel4ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(15, 10, 435, 20);

            //---- STST7 ----
            STST7.setText("07. Client/classement 2 articles/classement 2 articles");
            STST7.setComponentPopupMenu(BTD);
            STST7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST7.setName("STST7");
            xTitledPanel4ContentContainer.add(STST7);
            STST7.setBounds(15, 120, 335, 22);

            //---- STST13 ----
            STST13.setText("13. Zone g\u00e9ographique/repr\u00e9sentant/groupe, famille");
            STST13.setComponentPopupMenu(BTD);
            STST13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST13.setName("STST13");
            xTitledPanel4ContentContainer.add(STST13);
            STST13.setBounds(375, 170, 315, 22);

            //---- STST12 ----
            STST12.setText("12. Zone g\u00e9ographique/repr\u00e9sentant/client");
            STST12.setComponentPopupMenu(BTD);
            STST12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST12.setName("STST12");
            xTitledPanel4ContentContainer.add(STST12);
            STST12.setBounds(15, 170, 260, 22);

            //---- STST14 ----
            STST14.setText("14. Repr\u00e9sentant/cat\u00e9gorie client/client");
            STST14.setComponentPopupMenu(BTD);
            STST14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST14.setName("STST14");
            xTitledPanel4ContentContainer.add(STST14);
            STST14.setBounds(15, 195, 250, 22);

            //---- STST5 ----
            STST5.setText("05. Repr\u00e9sentant/client/groupe, famille");
            STST5.setComponentPopupMenu(BTD);
            STST5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST5.setName("STST5");
            xTitledPanel4ContentContainer.add(STST5);
            STST5.setBounds(15, 95, 250, 22);

            //---- STST1 ----
            STST1.setText("01. Client/classement 2 articles/article");
            STST1.setComponentPopupMenu(BTD);
            STST1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST1.setName("STST1");
            xTitledPanel4ContentContainer.add(STST1);
            STST1.setBounds(15, 45, 240, 22);

            //---- STST2 ----
            STST2.setText("02. Client/classement 2 articles/famille");
            STST2.setComponentPopupMenu(BTD);
            STST2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST2.setName("STST2");
            xTitledPanel4ContentContainer.add(STST2);
            STST2.setBounds(375, 45, 245, 22);

            //---- STST10 ----
            STST10.setText("10. Cat\u00e9gorie/groupe, famille/article");
            STST10.setComponentPopupMenu(BTD);
            STST10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST10.setName("STST10");
            xTitledPanel4ContentContainer.add(STST10);
            STST10.setBounds(375, 145, 235, 22);

            //---- STST6 ----
            STST6.setText("06. Repr\u00e9sentant/groupe/famille");
            STST6.setComponentPopupMenu(BTD);
            STST6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST6.setName("STST6");
            xTitledPanel4ContentContainer.add(STST6);
            STST6.setBounds(375, 95, 220, 22);

            //---- STST3 ----
            STST3.setText("03. Client/groupe, famille/article");
            STST3.setComponentPopupMenu(BTD);
            STST3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST3.setName("STST3");
            xTitledPanel4ContentContainer.add(STST3);
            STST3.setBounds(15, 70, 200, 22);

            //---- STST9 ----
            STST9.setText("09. RST/groupe, famille/article");
            STST9.setComponentPopupMenu(BTD);
            STST9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST9.setName("STST9");
            xTitledPanel4ContentContainer.add(STST9);
            STST9.setBounds(15, 145, 205, 22);

            //---- STST4 ----
            STST4.setText("04. Repr\u00e9sentant/client/article");
            STST4.setComponentPopupMenu(BTD);
            STST4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST4.setName("STST4");
            xTitledPanel4ContentContainer.add(STST4);
            STST4.setBounds(375, 70, 210, 22);

            //---- STST8 ----
            STST8.setText("08. RST/client/article");
            STST8.setComponentPopupMenu(BTD);
            STST8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STST8.setName("STST8");
            xTitledPanel4ContentContainer.add(STST8);
            STST8.setBounds(375, 120, 165, 22);

            //---- STTT01 ----
            STTT01.setComponentPopupMenu(BTD);
            STTT01.setName("STTT01");
            xTitledPanel4ContentContainer.add(STTT01);
            STTT01.setBounds(280, 167, 20, STTT01.getPreferredSize().height);

            //---- STTT02 ----
            STTT02.setComponentPopupMenu(BTD);
            STTT02.setName("STTT02");
            xTitledPanel4ContentContainer.add(STTT02);
            STTT02.setBounds(695, 142, 20, STTT02.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 730, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private JLabel OBJ_78;
  private JLabel OBJ_82;
  private JLabel OBJ_75;
  private JLabel OBJ_79;
  private XRiTextField STNAT1;
  private XRiTextField STNAT2;
  private JLabel OBJ_76;
  private JLabel OBJ_80;
  private XRiTextField STCS11;
  private XRiTextField STCS12;
  private XRiTextField STCS21;
  private XRiTextField STCS22;
  private JLabel OBJ_77;
  private JLabel OBJ_81;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_83;
  private XRiCheckBox STST7;
  private XRiCheckBox STST13;
  private XRiCheckBox STST12;
  private XRiCheckBox STST14;
  private XRiCheckBox STST5;
  private XRiCheckBox STST1;
  private XRiCheckBox STST2;
  private XRiCheckBox STST10;
  private XRiCheckBox STST6;
  private XRiCheckBox STST3;
  private XRiCheckBox STST9;
  private XRiCheckBox STST4;
  private XRiCheckBox STST8;
  private XRiTextField STTT01;
  private XRiTextField STTT02;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
