
package ri.serien.libecranrpg.vgvm.VGVM06FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.JPanel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM06FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM06FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Ajouter la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité conditionnée
    lbEtablissement.setVisible(lexique.isTrue("59"));
    snEtablissement.setVisible(lexique.isTrue("59"));
    pnlDevise.setVisible(lexique.isTrue("54"));
    
    // Etablissement
    if (snEtablissement.isVisible()) {
      snEtablissement.setSession(getSession());
      snEtablissement.charger(false);
      snEtablissement.setSelectionParChampRPG(lexique, "WETB2");
    }
    
    // Devises
    if (pnlDevise.isVisible()) {
      snDevise.setSession(getSession());
      snDevise.setIdEtablissement(snEtablissement.getIdSelection());
      snDevise.charger(false);
      snDevise.setSelectionParChampRPG(lexique, "WNDEV");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Modification tarif de vente"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    // Etablissement
    if (snEtablissement.isVisible()) {
      snEtablissement.renseignerChampRPG(lexique, "WETB2");
    }
    // Devise
    if (pnlDevise.isVisible()) {
      snDevise.renseignerChampRPG(lexique, "WNDEV");
    }
  }
  
  /**
   * 
   * Actions des boutons de la barre.
   * 
   * @param pSNBouton
   */
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    p_contenu = new SNPanelContenu();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbDateApplication = new SNLabelChamp();
    WNDAPX = new XRiCalendrier();
    pnlDevise = new JPanel();
    lbDevise = new SNLabelChamp();
    snDevise = new SNDevise();
    lbDevise2 = new SNLabelChamp();
    WNRON = new XRiTextField();
    lbDevise3 = new SNLabelChamp();
    WNCNV = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(650, 260));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== p_contenu ========
    {
      p_contenu.setBackground(new Color(238, 238, 210));
      p_contenu.setName("p_contenu");
      p_contenu.setLayout(new GridBagLayout());
      ((GridBagLayout) p_contenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) p_contenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) p_contenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) p_contenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ---- lbEtablissement ----
      lbEtablissement.setText("Nouvel \u00e9tablissement");
      lbEtablissement.setName("lbEtablissement");
      p_contenu.add(lbEtablissement,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snEtablissement ----
      snEtablissement.setName("snEtablissement");
      p_contenu.add(snEtablissement,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbDateApplication ----
      lbDateApplication.setText("Nouvelle date d'application");
      lbDateApplication.setMaximumSize(new Dimension(200, 30));
      lbDateApplication.setMinimumSize(new Dimension(200, 30));
      lbDateApplication.setPreferredSize(new Dimension(200, 30));
      lbDateApplication.setName("lbDateApplication");
      p_contenu.add(lbDateApplication,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- WNDAPX ----
      WNDAPX.setMinimumSize(new Dimension(115, 30));
      WNDAPX.setPreferredSize(new Dimension(115, 30));
      WNDAPX.setMaximumSize(new Dimension(115, 30));
      WNDAPX.setName("WNDAPX");
      p_contenu.add(WNDAPX, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlDevise ========
      {
        pnlDevise.setOpaque(false);
        pnlDevise.setName("pnlDevise");
        pnlDevise.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDevise.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDevise.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDevise.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDevise.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDevise ----
        lbDevise.setText("Nouvelle devise");
        lbDevise.setMaximumSize(new Dimension(200, 30));
        lbDevise.setMinimumSize(new Dimension(200, 30));
        lbDevise.setPreferredSize(new Dimension(200, 30));
        lbDevise.setName("lbDevise");
        pnlDevise.add(lbDevise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDevise ----
        snDevise.setName("snDevise");
        pnlDevise.add(snDevise, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDevise2 ----
        lbDevise2.setText("Nouvel arrondi");
        lbDevise2.setMaximumSize(new Dimension(200, 30));
        lbDevise2.setMinimumSize(new Dimension(200, 30));
        lbDevise2.setPreferredSize(new Dimension(200, 30));
        lbDevise2.setName("lbDevise2");
        pnlDevise.add(lbDevise2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- WNRON ----
        WNRON.setMinimumSize(new Dimension(35, 30));
        WNRON.setPreferredSize(new Dimension(35, 30));
        WNRON.setMaximumSize(new Dimension(35, 30));
        WNRON.setName("WNRON");
        pnlDevise.add(WNRON, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDevise3 ----
        lbDevise3.setText("Nouveau regroupement CNV");
        lbDevise3.setMaximumSize(new Dimension(200, 30));
        lbDevise3.setMinimumSize(new Dimension(200, 30));
        lbDevise3.setPreferredSize(new Dimension(200, 30));
        lbDevise3.setName("lbDevise3");
        pnlDevise.add(lbDevise3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- WNCNV ----
        WNCNV.setMaximumSize(new Dimension(70, 30));
        WNCNV.setMinimumSize(new Dimension(70, 30));
        WNCNV.setPreferredSize(new Dimension(70, 30));
        WNCNV.setName("WNCNV");
        pnlDevise.add(WNCNV, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_contenu.add(pnlDevise,
          new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(p_contenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu p_contenu;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbDateApplication;
  private XRiCalendrier WNDAPX;
  private JPanel pnlDevise;
  private SNLabelChamp lbDevise;
  private SNDevise snDevise;
  private SNLabelChamp lbDevise2;
  private XRiTextField WNRON;
  private SNLabelChamp lbDevise3;
  private XRiTextField WNCNV;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
