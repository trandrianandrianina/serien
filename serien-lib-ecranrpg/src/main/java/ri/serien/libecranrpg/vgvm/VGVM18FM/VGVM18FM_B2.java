
package ri.serien.libecranrpg.vgvm.VGVM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM18FM_B2 extends SNPanelEcranRPG implements ioFrame {
  private boolean isConsultation = true;
  
  public VGVM18FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel1.setBorder(new TitledBorder("Saisie simplifiée"));
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Articles liés @TITRE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isConsultation = lexique.isTrue("(N91) AND (N92)");
    
    riSousMenu3.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    riSousMenu_bt3.setVisible(lexique.getMode() != Lexical.MODE_CONSULTATION);
    
    if (lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim().equalsIgnoreCase("Automatiques")) {
      menuItem2.setText("Transformer en article lié proposé");
    }
    else {
      menuItem2.setText("Transformer en article lié automatiquement");
    }
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Articles
    ARL01.setSession(getSession());
    ARL01.setIdEtablissement(idEtablissement);
    ARL01.charger(false);
    ARL01.setSelectionParChampRPG(lexique, "ARL01");
    ARL01.setEnabled(!isConsultation);
    
    ARL02.setSession(getSession());
    ARL02.setIdEtablissement(idEtablissement);
    ARL02.charger(false);
    ARL02.setSelectionParChampRPG(lexique, "ARL02");
    ARL02.setEnabled(!isConsultation);
    
    ARL03.setSession(getSession());
    ARL03.setIdEtablissement(idEtablissement);
    ARL03.charger(false);
    ARL03.setSelectionParChampRPG(lexique, "ARL03");
    ARL03.setEnabled(!isConsultation);
    
    ARL04.setSession(getSession());
    ARL04.setIdEtablissement(idEtablissement);
    ARL04.charger(false);
    ARL04.setSelectionParChampRPG(lexique, "ARL04");
    ARL04.setEnabled(!isConsultation);
    
    ARL05.setSession(getSession());
    ARL05.setIdEtablissement(idEtablissement);
    ARL05.charger(false);
    ARL05.setSelectionParChampRPG(lexique, "ARL05");
    ARL05.setEnabled(!isConsultation);
    
    ARL06.setSession(getSession());
    ARL06.setIdEtablissement(idEtablissement);
    ARL06.charger(false);
    ARL06.setSelectionParChampRPG(lexique, "ARL06");
    ARL06.setEnabled(!isConsultation);
    
    ARL07.setSession(getSession());
    ARL07.setIdEtablissement(idEtablissement);
    ARL07.charger(false);
    ARL07.setSelectionParChampRPG(lexique, "ARL07");
    ARL07.setEnabled(!isConsultation);
    
    ARL08.setSession(getSession());
    ARL08.setIdEtablissement(idEtablissement);
    ARL08.charger(false);
    ARL08.setSelectionParChampRPG(lexique, "ARL08");
    ARL08.setEnabled(!isConsultation);
    
    ARL09.setSession(getSession());
    ARL09.setIdEtablissement(idEtablissement);
    ARL09.charger(false);
    ARL09.setSelectionParChampRPG(lexique, "ARL09");
    ARL09.setEnabled(!isConsultation);
    
    ARL10.setSession(getSession());
    ARL10.setIdEtablissement(idEtablissement);
    ARL10.charger(false);
    ARL10.setSelectionParChampRPG(lexique, "ARL10");
    ARL10.setEnabled(!isConsultation);
    
    ARL11.setSession(getSession());
    ARL11.setIdEtablissement(idEtablissement);
    ARL11.charger(false);
    ARL11.setSelectionParChampRPG(lexique, "ARL11");
    ARL11.setEnabled(!isConsultation);
    
    ARL12.setSession(getSession());
    ARL12.setIdEtablissement(idEtablissement);
    ARL12.charger(false);
    ARL12.setSelectionParChampRPG(lexique, "ARL12");
    ARL12.setEnabled(!isConsultation);
    
    ARL13.setSession(getSession());
    ARL13.setIdEtablissement(idEtablissement);
    ARL13.charger(false);
    ARL13.setSelectionParChampRPG(lexique, "ARL13");
    ARL13.setEnabled(!isConsultation);
    
    ARL14.setSession(getSession());
    ARL14.setIdEtablissement(idEtablissement);
    ARL14.charger(false);
    ARL14.setSelectionParChampRPG(lexique, "ARL14");
    ARL14.setEnabled(!isConsultation);
    
    ARL15.setSession(getSession());
    ARL15.setIdEtablissement(idEtablissement);
    ARL15.charger(false);
    ARL15.setSelectionParChampRPG(lexique, "ARL15");
    ARL15.setEnabled(!isConsultation);
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    ARL01.renseignerChampRPG(lexique, "ARL01");
    ARL02.renseignerChampRPG(lexique, "ARL02");
    ARL03.renseignerChampRPG(lexique, "ARL03");
    ARL04.renseignerChampRPG(lexique, "ARL04");
    ARL05.renseignerChampRPG(lexique, "ARL05");
    ARL06.renseignerChampRPG(lexique, "ARL06");
    ARL07.renseignerChampRPG(lexique, "ARL07");
    ARL08.renseignerChampRPG(lexique, "ARL08");
    ARL09.renseignerChampRPG(lexique, "ARL09");
    ARL10.renseignerChampRPG(lexique, "ARL10");
    ARL11.renseignerChampRPG(lexique, "ARL11");
    ARL12.renseignerChampRPG(lexique, "ARL12");
    ARL13.renseignerChampRPG(lexique, "ARL13");
    ARL14.renseignerChampRPG(lexique, "ARL14");
    ARL15.renseignerChampRPG(lexique, "ARL15");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    riZoneSortie1 = new RiZoneSortie();
    label1 = new JLabel();
    WART = new XRiTextField();
    label2 = new JLabel();
    riZoneSortie3 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ARL01 = new SNArticle();
    ARL02 = new SNArticle();
    ARL03 = new SNArticle();
    ARL04 = new SNArticle();
    ARL05 = new SNArticle();
    ARL06 = new SNArticle();
    ARL07 = new SNArticle();
    ARL08 = new SNArticle();
    ARL09 = new SNArticle();
    ARL10 = new SNArticle();
    ARL11 = new SNArticle();
    ARL12 = new SNArticle();
    ARL13 = new SNArticle();
    ARL14 = new SNArticle();
    ARL15 = new SNArticle();
    BTD = new JPopupMenu();
    menuItem2 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1120, 710));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Articles li\u00e9s");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 32));
          p_tete_gauche.setMinimumSize(new Dimension(850, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- riZoneSortie1 ----
          riZoneSortie1.setText("@WETB@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");
          
          // ---- label1 ----
          label1.setText("Etablissement");
          label1.setName("label1");
          
          // ---- WART ----
          WART.setName("WART");
          
          // ---- label2 ----
          label2.setText("Article parent");
          label2.setName("label2");
          
          // ---- riZoneSortie3 ----
          riZoneSortie3.setText("@WLIB@");
          riZoneSortie3.setOpaque(false);
          riZoneSortie3.setName("riZoneSortie3");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(label1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                  .addComponent(label2, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(riZoneSortie3, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(label1,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(riZoneSortie1,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2))
              .addGroup(GroupLayout.Alignment.TRAILING,
                  p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(label2,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(riZoneSortie3,
                          GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Saisie d\u00e9taill\u00e9e");
              riSousMenu_bt2.setToolTipText("Saisie simplifi\u00e9e des articles li\u00e9s");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Derniers articles li\u00e9s");
              riSousMenu_bt3.setToolTipText("Derniers articles li\u00e9s saisis");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(550, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(550, 550));
          p_contenu.setMaximumSize(new Dimension(1010, 610));
          p_contenu.setName("p_contenu");
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Articles li\u00e9s @TITRE@"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- ARL01 ----
            ARL01.setComponentPopupMenu(BTD);
            ARL01.setName("ARL01");
            panel1.add(ARL01);
            ARL01.setBounds(45, 55, 440, ARL01.getPreferredSize().height);
            
            // ---- ARL02 ----
            ARL02.setComponentPopupMenu(BTD);
            ARL02.setName("ARL02");
            panel1.add(ARL02);
            ARL02.setBounds(45, 85, 440, ARL02.getPreferredSize().height);
            
            // ---- ARL03 ----
            ARL03.setComponentPopupMenu(BTD);
            ARL03.setName("ARL03");
            panel1.add(ARL03);
            ARL03.setBounds(45, 115, 440, ARL03.getPreferredSize().height);
            
            // ---- ARL04 ----
            ARL04.setComponentPopupMenu(BTD);
            ARL04.setName("ARL04");
            panel1.add(ARL04);
            ARL04.setBounds(45, 145, 440, ARL04.getPreferredSize().height);
            
            // ---- ARL05 ----
            ARL05.setComponentPopupMenu(BTD);
            ARL05.setName("ARL05");
            panel1.add(ARL05);
            ARL05.setBounds(45, 175, 440, ARL05.getPreferredSize().height);
            
            // ---- ARL06 ----
            ARL06.setComponentPopupMenu(BTD);
            ARL06.setName("ARL06");
            panel1.add(ARL06);
            ARL06.setBounds(45, 205, 440, ARL06.getPreferredSize().height);
            
            // ---- ARL07 ----
            ARL07.setComponentPopupMenu(BTD);
            ARL07.setName("ARL07");
            panel1.add(ARL07);
            ARL07.setBounds(45, 235, 440, ARL07.getPreferredSize().height);
            
            // ---- ARL08 ----
            ARL08.setComponentPopupMenu(BTD);
            ARL08.setName("ARL08");
            panel1.add(ARL08);
            ARL08.setBounds(45, 265, 440, ARL08.getPreferredSize().height);
            
            // ---- ARL09 ----
            ARL09.setComponentPopupMenu(BTD);
            ARL09.setName("ARL09");
            panel1.add(ARL09);
            ARL09.setBounds(45, 295, 440, ARL09.getPreferredSize().height);
            
            // ---- ARL10 ----
            ARL10.setComponentPopupMenu(BTD);
            ARL10.setName("ARL10");
            panel1.add(ARL10);
            ARL10.setBounds(45, 325, 440, ARL10.getPreferredSize().height);
            
            // ---- ARL11 ----
            ARL11.setComponentPopupMenu(BTD);
            ARL11.setName("ARL11");
            panel1.add(ARL11);
            ARL11.setBounds(45, 355, 440, ARL11.getPreferredSize().height);
            
            // ---- ARL12 ----
            ARL12.setComponentPopupMenu(BTD);
            ARL12.setName("ARL12");
            panel1.add(ARL12);
            ARL12.setBounds(45, 385, 440, ARL12.getPreferredSize().height);
            
            // ---- ARL13 ----
            ARL13.setComponentPopupMenu(BTD);
            ARL13.setName("ARL13");
            panel1.add(ARL13);
            ARL13.setBounds(45, 415, 440, ARL13.getPreferredSize().height);
            
            // ---- ARL14 ----
            ARL14.setComponentPopupMenu(BTD);
            ARL14.setName("ARL14");
            panel1.add(ARL14);
            ARL14.setBounds(45, 445, 440, ARL14.getPreferredSize().height);
            
            // ---- ARL15 ----
            ARL15.setComponentPopupMenu(BTD);
            ARL15.setName("ARL15");
            panel1.add(ARL15);
            ARL15.setBounds(45, 475, 440, ARL15.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 524, Short.MAX_VALUE).addContainerGap()));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap().addComponent(panel1, GroupLayout.DEFAULT_SIZE, 522, Short.MAX_VALUE).addContainerGap()));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- menuItem2 ----
      menuItem2.setText("Transformer");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      BTD.add(menuItem2);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie riZoneSortie1;
  private JLabel label1;
  private XRiTextField WART;
  private JLabel label2;
  private RiZoneSortie riZoneSortie3;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private SNArticle ARL01;
  private SNArticle ARL02;
  private SNArticle ARL03;
  private SNArticle ARL04;
  private SNArticle ARL05;
  private SNArticle ARL06;
  private SNArticle ARL07;
  private SNArticle ARL08;
  private SNArticle ARL09;
  private SNArticle ARL10;
  private SNArticle ARL11;
  private SNArticle ARL12;
  private SNArticle ARL13;
  private SNArticle ARL14;
  private SNArticle ARL15;
  private JPopupMenu BTD;
  private JMenuItem menuItem2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
