
package ri.serien.libecranrpg.vgvm.VGVM11FX;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;

/**
 * @author Stéphane Vénéri
 */
public class VGVM11FX_GA extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public VGVM11FX_GA(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    
    
    bouton_valider.setIcon(lexique.chargerImage("images/OK_p.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // Titre
    setTitle("Génération d'achat");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Récupérer la valeur pour cocher les bonnes cases ou radio
    int valeur = 0;
    String l1gba = lexique.HostFieldGetData("L1GBA");
    if (lexique.isPresent("L1GBA")) {
      try {
        valeur = Integer.parseInt(l1gba);
      }
      catch (Exception e) {
        valeur = 0;
      }
    }
    else {
      if (!l1gba.trim().isEmpty()) {
        valeur = Integer.parseInt(l1gba);
      }
    }
    
    switch (valeur) {
      case 1:
        gene1.setSelected(true);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
      case 2:
        gene1.setSelected(true);
        livr1.setSelected(true);
        livr2.setSelected(false);
        break;
      case 3:
        gene0.setSelected(false);
        gene1.setSelected(false);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
      case 4:
        gene0.setSelected(false);
        gene1.setSelected(false);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
      case 5:
        gene2.setSelected(true);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
      case 6:
        gene2.setSelected(true);
        livr1.setSelected(false);
        livr2.setSelected(true);
        break;
      case 7:
        gene0.setSelected(false);
        gene1.setSelected(false);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
      case 8:
        gene0.setSelected(false);
        gene1.setSelected(false);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
      default:
        gene0.setSelected(true);
        livr1.setSelected(false);
        livr2.setSelected(false);
        break;
    }
    
    // Affichage dynamique des boutons radio
    majGeneration();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    String valeur = "";
    if (gene1.isSelected()) {
      if (livr1.isSelected()) {
        valeur = "2";
      }
      else {
        valeur = "1";
      }
    }
    else if (gene2.isSelected()) {
      if (livr2.isSelected()) {
        valeur = "6";
      }
      else {
        valeur = "5";
      }
    }
    else if (gene0.isSelected()) {
      valeur = "";
    }
    
    // Traitement en fonction du panel qui appelle la pop-up
    if (lexique.isPresent("L1GBA")) {
      lexique.HostFieldPutData("L1GBA", 0, valeur);
    }
    else if (lexique.isPresent("E1GBA")) {
      lexique.HostFieldPutData("E1GBA", 0, valeur);
    }
  }
  
  private void majGeneration() {
    if (gene1.isSelected()) {
      livr1.setVisible(true);
      livr2.setVisible(false);
      livr2.setSelected(false);
    }
    else if (gene2.isSelected()) {
      livr1.setVisible(false);
      livr1.setSelected(false);
      livr2.setVisible(true);
    }
    else {
      livr1.setVisible(false);
      livr1.setSelected(false);
      livr2.setVisible(false);
      livr2.setSelected(false);
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void gene0ActionPerformed(ActionEvent e) {
    majGeneration();
  }
  
  private void gene1ActionPerformed(ActionEvent e) {
    majGeneration();
  }
  
  private void gene2ActionPerformed(ActionEvent e) {
    majGeneration();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
    dispose();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    getData();
    if (lexique.isPresent("L1GBA")) {
      lexique.HostCursorPut("L1GBA");
    }
    else {
      lexique.HostCursorPut("E1GBA");
      
    }
    
    lexique.HostScreenSendKey(this, "F4");
    closePopupLinkWithBuffer(true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel13 = new JPanel();
    gene0 = new JRadioButton();
    gene1 = new JRadioButton();
    gene2 = new JRadioButton();
    livr1 = new JCheckBox();
    livr2 = new JCheckBox();

    //======== this ========
    setMinimumSize(new Dimension(600, 230));
    setPreferredSize(new Dimension(580, 180));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Conditions");
              riSousMenu_bt6.setToolTipText("Conditions");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel13 ========
        {
          panel13.setBorder(new TitledBorder("G\u00e9n\u00e9ration d'achat"));
          panel13.setOpaque(false);
          panel13.setName("panel13");
          panel13.setLayout(null);

          //---- gene0 ----
          gene0.setText("Aucune g\u00e9n\u00e9ration");
          gene0.setName("gene0");
          gene0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              gene0ActionPerformed(e);
            }
          });
          panel13.add(gene0);
          gene0.setBounds(20, 35, 165, 20);

          //---- gene1 ----
          gene1.setText("G\u00e9n\u00e9ration diff\u00e9r\u00e9e");
          gene1.setName("gene1");
          gene1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              gene1ActionPerformed(e);
            }
          });
          panel13.add(gene1);
          gene1.setBounds(20, 72, 165, 20);

          //---- gene2 ----
          gene2.setText("G\u00e9n\u00e9ration imm\u00e9diate");
          gene2.setName("gene2");
          gene2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              gene2ActionPerformed(e);
            }
          });
          panel13.add(gene2);
          gene2.setBounds(20, 109, 165, 20);

          //---- livr1 ----
          livr1.setText("livraison directe assur\u00e9e");
          livr1.setName("livr1");
          panel13.add(livr1);
          livr1.setBounds(190, 72, 170, 20);

          //---- livr2 ----
          livr2.setText("livraison directe assur\u00e9e");
          livr2.setName("livr2");
          panel13.add(livr2);
          livr2.setBounds(190, 109, 170, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel13.getComponentCount(); i++) {
              Rectangle bounds = panel13.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel13.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel13.setMinimumSize(preferredSize);
            panel13.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel13, GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel13, GroupLayout.DEFAULT_SIZE, 154, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(gene0);
    buttonGroup1.add(gene1);
    buttonGroup1.add(gene2);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel13;
  private JRadioButton gene0;
  private JRadioButton gene1;
  private JRadioButton gene2;
  private JCheckBox livr1;
  private JCheckBox livr2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
