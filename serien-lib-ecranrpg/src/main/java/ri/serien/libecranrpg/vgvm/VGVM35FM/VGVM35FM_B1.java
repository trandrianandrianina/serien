//$$david$$ ££06/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNConditionCommissionnement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNAdresseMail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] RPMOC_Value = { "1", "0", "2" };
  private static final String BOUTON_CONSULTER = "Consulter";
  private static final String BOUTON_MODIFIER = "Modifier";
  private static final String BOUTON_LIAISON_WEB = "Liaison web";
  private static final String BOUTON_ACTION_COMMERCIALE = "Actions commerciales";
  private static final String BOUTON_DOCUMENTS_LIES = "Documents liés";
  private static final String BOUTON_PROSPECTION = "Prospection";
  private static final String BOUTON_FICHE_CLIENT = "Fiche client";
  private static final String BOUTON_TOURNEE = "Tournée";
  private static final String BOUTON_BLOCNOTES = "Bloc-notes";
  private static final String BOUTON_OBSERVATIONS = "Observations";
  
  public VGVM35FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_CONSULTER, 's', false);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_LIAISON_WEB, 'w', true);
    snBarreBouton.ajouterBouton(BOUTON_ACTION_COMMERCIALE, 'r', true);
    snBarreBouton.ajouterBouton(BOUTON_DOCUMENTS_LIES, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_PROSPECTION, 'p', true);
    snBarreBouton.ajouterBouton(BOUTON_FICHE_CLIENT, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_TOURNEE, 't', true);
    snBarreBouton.ajouterBouton(BOUTON_BLOCNOTES, 'b', true);
    snBarreBouton.ajouterBouton(BOUTON_OBSERVATIONS, 'o', true);
    // -
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    // Ajout
    RPMOC.setValeurs(RPMOC_Value, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbLibtns.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTNS@")).trim());
    lbMois.setText(lexique.TranslationTable(interpreteurD.analyseExpression("CA du mois @WDSVX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    boolean isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    boolean isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    boolean isModification = lexique.getMode() == Lexical.MODE_MODIFICATION;
    
    lbObservation.setVisible(RPOBS.isVisible());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB"));
    
    // Civilité
    snCivilite.setSession(getSession());
    snCivilite.setIdEtablissement(idEtablissement);
    snCivilite.charger(false);
    snCivilite.setSelectionParChampRPG(lexique, "RPCIV");
    snCivilite.setEnabled(!isConsultation);
    
    // Commune
    snCodePostalCommune.setSession(getSession());
    snCodePostalCommune.setIdEtablissement(idEtablissement);
    snCodePostalCommune.charger(false);
    snCodePostalCommune.setSelectionParChampRPG(lexique, "RPCDP", "RPVILR");
    snCodePostalCommune.setEnabled(!isConsultation);
    
    // Pays
    snPays.setSession(getSession());
    snPays.setIdEtablissement(idEtablissement);
    snPays.charger(false);
    snPays.setSelectionParChampRPG(lexique, "RPCOP");
    snPays.setAucunAutorise(true);
    snPays.setEnabled(!isConsultation);
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "RPMAG");
    snMagasin.setEnabled(!isConsultation);
    
    // Zone géographique
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(idEtablissement);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "RPGEO");
    snZoneGeographique.setEnabled(!isConsultation);
    
    // Vendeur
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(idEtablissement);
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "RPVDE");
    snVendeur.setEnabled(!isConsultation);
    
    // Section analytique
    snSectionAnalytique.setSession(getSession());
    snSectionAnalytique.setIdEtablissement(idEtablissement);
    snSectionAnalytique.charger(false);
    snSectionAnalytique.setSelectionParChampRPG(lexique, "RPSAN");
    snSectionAnalytique.setEnabled(!isConsultation);
    
    // Mail
    if (!lexique.HostFieldGetData("RPNETR").trim().isEmpty()) {
      try {
        snMail.setText(lexique.HostFieldGetData("RPNETR"));
      }
      catch (Exception e) {
        // pas d'affichage d'erreur mail car il n'est pas modifiable sur la fiche cllient
      }
    }
    snMail.setEnabled(!isConsultation);
    
    // Regroupement
    snConditionCommissionnement.setSession(getSession());
    snConditionCommissionnement.setIdEtablissement(idEtablissement);
    snConditionCommissionnement.charger(false);
    snConditionCommissionnement.setSelectionParChampRPG(lexique, "RPCNC");
    snConditionCommissionnement.setEnabled(!isConsultation);
    
    // Visibilité boutons
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.RETOURNER_RECHERCHE, isConsultation);
    snBarreBouton.activerBouton(BOUTON_MODIFIER, isConsultation);
    snBarreBouton.activerBouton(BOUTON_CONSULTER, !isConsultation);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snCivilite.renseignerChampRPG(lexique, "RPCIV");
    snCodePostalCommune.renseignerChampRPG(lexique, "RPCDP", "RPVILR", false);
    snPays.renseignerChampRPG(lexique, "RPCOP", "RPPAY");
    snMagasin.renseignerChampRPG(lexique, "RPMAG");
    snZoneGeographique.renseignerChampRPG(lexique, "RPGEO");
    snVendeur.renseignerChampRPG(lexique, "RPVDE");
    snSectionAnalytique.renseignerChampRPG(lexique, "RPSAN");
    lexique.HostFieldPutData("RPNETR", 0, snMail.getText());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(9, 79);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 19);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 19);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    if (Desktop.isDesktopSupported()) {
      if (Desktop.getDesktop().isSupported(Desktop.Action.MAIL)) {
        try {
          if (!lexique.HostFieldGetData("RPNETR").trim().equals("")) {
            Desktop.getDesktop().mail(new URI("mailto:" + lexique.HostFieldGetData("RPNETR").trim()));
          }
        }
        catch (IOException e1) {
          e1.printStackTrace();
        }
        catch (URISyntaxException e2) {
          e2.printStackTrace();
        }
      }
    }
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 63);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(2, 76);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 76);
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void btSectionAnalytiqueActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "3");
    }
    else {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btDiversActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "1");
    }
    else {
      lexique.HostFieldPutData("TCI1", 0, "X");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btCommissionnementActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "2");
    }
    else {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_objectifActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "5");
    }
    else {
      lexique.HostFieldPutData("TCI5", 0, "X");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btStatsActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostFieldPutData("V06FO", 0, "6");
    }
    else {
      lexique.HostFieldPutData("TCI6", 0, "X");
    }
    
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CONSULTER)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_LIAISON_WEB)) {
        lexique.HostCursorPut(9, 79);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_ACTION_COMMERCIALE)) {
        lexique.HostCursorPut(17, 19);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_DOCUMENTS_LIES)) {
        lexique.HostScreenSendKey(this, "F11");
      }
      else if (pSNBouton.isBouton(BOUTON_PROSPECTION)) {
        lexique.HostCursorPut(17, 63);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_FICHE_CLIENT)) {
        lexique.HostCursorPut(2, 76);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_TOURNEE)) {
        lexique.HostCursorPut(17, 76);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_BLOCNOTES)) {
        lexique.HostScreenSendKey(this, "F22");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void forcerModeConsultation() {
    
  }
  
  private void snCiviliteValueChanged(SNComposantEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCodePostalCommuneValueChanged(SNComposantEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void TCI5ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void TCI6ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlCoordonnees = new SNPanelTitre();
    lbCodeRepresentant2 = new SNLabelChamp();
    INDREP = new XRiTextField();
    INDREP2 = new XRiTextField();
    lbCivilite = new SNLabelChamp();
    lbCivilite2 = new SNLabelChamp();
    snCivilite = new SNCivilite();
    snCivilite2 = new SNCivilite();
    lbNom = new SNLabelChamp();
    lbNom2 = new SNLabelChamp();
    RPNOM = new XRiTextField();
    RPNOM2 = new XRiTextField();
    lbLibelleEdition = new SNLabelChamp();
    lbLibelleEdition2 = new SNLabelChamp();
    RPEDT = new XRiTextField();
    RPEDT2 = new XRiTextField();
    lbLocalisation = new SNLabelChamp();
    RPRUE = new XRiTextField();
    lbRue = new SNLabelChamp();
    RPLOC = new XRiTextField();
    lbCodePostal2 = new SNLabelChamp();
    snCodePostalCommune = new SNCodePostalCommune();
    lbCodePostal3 = new SNLabelChamp();
    snPays = new SNPays();
    tabbedPane1 = new JTabbedPane();
    pnlInfos = new SNPanelContenu();
    pnlBlocInfosHaut = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    sNPanel2 = new SNPanel();
    snEtablissement = new SNEtablissement();
    lbLibtns = new SNLabelChamp();
    lbMagasin2 = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbClassement = new SNLabelChamp();
    RPCLK = new XRiTextField();
    lbType = new SNLabelChamp();
    RPTYP = new XRiTextField();
    lbTelephone = new SNLabelChamp();
    RPTEL = new XRiTextField();
    lbFax = new SNLabelChamp();
    RPFAX = new XRiTextField();
    lbMail = new SNLabelChamp();
    snMail = new SNAdresseMail();
    lbObservation = new SNLabelChamp();
    RPOBS = new XRiTextField();
    sNPanel3 = new SNPanelContenu();
    lbSectionAnalytique = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    lbPointeur = new SNLabelChamp();
    sNPanel4 = new SNPanel();
    RPNSA = new XRiTextField();
    SAN1 = new XRiTextField();
    SAN2 = new XRiTextField();
    SAN3 = new XRiTextField();
    SAN4 = new XRiTextField();
    SAN5 = new XRiTextField();
    SAN6 = new XRiTextField();
    SAN7 = new XRiTextField();
    SAN8 = new XRiTextField();
    SAN9 = new XRiTextField();
    pnlDeuxiemeLigne2 = new SNPanel();
    pnlTitreReglement2 = new SNPanel();
    btDivers = new SNBoutonDetail();
    lbReglement2 = new SNLabelTitre();
    pnlTitreCompta3 = new SNPanel();
    btCommissionnement = new SNBoutonDetail();
    lbCompta3 = new SNLabelTitre();
    pnlDivers2 = new SNPanelTitre();
    lbAvanceNotesDeFrais = new SNLabelChamp();
    sNPanel5 = new SNPanel();
    RPNOF = new XRiTextField();
    lbVendeur2 = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbZoneGeographique2 = new SNLabelChamp();
    snZoneGeographique = new SNZoneGeographique();
    pnlCommissionnement2 = new SNPanelTitre();
    lbCodeRegroupement2 = new SNLabelChamp();
    snConditionCommissionnement = new SNConditionCommissionnement();
    lbSeuil2 = new SNLabelChamp();
    sNPanel7 = new SNPanel();
    RPSEU = new XRiTextField();
    lbTaux2 = new SNLabelChamp();
    RPCOM = new XRiTextField();
    lbTaux3 = new SNLabelChamp();
    RPMOC = new XRiComboBox();
    pnlDeuxiemeLigne = new SNPanel();
    pnlTitreReglement = new SNPanel();
    bt_objectif = new SNBoutonDetail();
    lbReglement = new SNLabelTitre();
    pnlTitreCompta2 = new SNPanel();
    btStats = new SNBoutonDetail();
    lbCompta2 = new SNLabelTitre();
    pnlDivers = new SNPanelTitre();
    sNLabelChamp1 = new SNLabelChamp();
    sNPanel8 = new SNPanel();
    RPO01 = new XRiTextField();
    RPO02 = new XRiTextField();
    RPO03 = new XRiTextField();
    sNLabelChamp2 = new SNLabelChamp();
    RPCO2 = new XRiTextField();
    pnlStatistiques = new SNPanelTitre();
    lbMois = new SNLabelChamp();
    SVCRM = new XRiTextField();
    lbExN = new SNLabelChamp();
    SVCR1 = new XRiTextField();
    lbN1 = new SNLabelChamp();
    SVCR0 = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1120, 660));
    setPreferredSize(new Dimension(1120, 660));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Fiche repr\u00e9sentant");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
      
      // ======== pnlCoordonnees ========
      {
        pnlCoordonnees.setTitre("Coordonn\u00e9es");
        pnlCoordonnees.setName("pnlCoordonnees");
        pnlCoordonnees.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonnees.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCoordonnees.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonnees.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonnees.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCodeRepresentant2 ----
        lbCodeRepresentant2.setText("Code");
        lbCodeRepresentant2.setName("lbCodeRepresentant2");
        pnlCoordonnees.add(lbCodeRepresentant2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- INDREP ----
        INDREP.setMinimumSize(new Dimension(36, 30));
        INDREP.setMaximumSize(new Dimension(36, 30));
        INDREP.setPreferredSize(new Dimension(36, 30));
        INDREP.setName("INDREP");
        pnlCoordonnees.add(INDREP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- INDREP2 ----
        INDREP2.setMinimumSize(new Dimension(36, 30));
        INDREP2.setMaximumSize(new Dimension(36, 30));
        INDREP2.setPreferredSize(new Dimension(36, 30));
        INDREP2.setName("INDREP2");
        pnlCoordonnees.add(INDREP2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCivilite ----
        lbCivilite.setText("Civilit\u00e9");
        lbCivilite.setPreferredSize(new Dimension(80, 30));
        lbCivilite.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCivilite.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCivilite.setMinimumSize(new Dimension(80, 30));
        lbCivilite.setMaximumSize(new Dimension(100, 30));
        lbCivilite.setName("lbCivilite");
        pnlCoordonnees.add(lbCivilite, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbCivilite2 ----
        lbCivilite2.setText("Civilit\u00e9");
        lbCivilite2.setPreferredSize(new Dimension(80, 30));
        lbCivilite2.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCivilite2.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCivilite2.setMinimumSize(new Dimension(80, 30));
        lbCivilite2.setMaximumSize(new Dimension(100, 30));
        lbCivilite2.setName("lbCivilite2");
        pnlCoordonnees.add(lbCivilite2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCivilite ----
        snCivilite.setPreferredSize(new Dimension(300, 30));
        snCivilite.setMinimumSize(new Dimension(300, 30));
        snCivilite.setName("snCivilite");
        snCivilite.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snCiviliteValueChanged(e);
          }
        });
        pnlCoordonnees.add(snCivilite, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- snCivilite2 ----
        snCivilite2.setPreferredSize(new Dimension(300, 30));
        snCivilite2.setMinimumSize(new Dimension(300, 30));
        snCivilite2.setName("snCivilite2");
        snCivilite2.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snCiviliteValueChanged(e);
          }
        });
        pnlCoordonnees.add(snCivilite2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNom ----
        lbNom.setText("Nom");
        lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom.setPreferredSize(new Dimension(80, 30));
        lbNom.setMinimumSize(new Dimension(80, 30));
        lbNom.setMaximumSize(new Dimension(100, 30));
        lbNom.setName("lbNom");
        pnlCoordonnees.add(lbNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbNom2 ----
        lbNom2.setText("Nom");
        lbNom2.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom2.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom2.setPreferredSize(new Dimension(80, 30));
        lbNom2.setMinimumSize(new Dimension(80, 30));
        lbNom2.setMaximumSize(new Dimension(100, 30));
        lbNom2.setName("lbNom2");
        pnlCoordonnees.add(lbNom2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- RPNOM ----
        RPNOM.setToolTipText("Nom");
        RPNOM.setFont(new Font("sansserif", Font.BOLD, 14));
        RPNOM.setMinimumSize(new Dimension(13, 30));
        RPNOM.setPreferredSize(new Dimension(13, 30));
        RPNOM.setMaximumSize(new Dimension(2147483647, 30));
        RPNOM.setName("RPNOM");
        pnlCoordonnees.add(RPNOM, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- RPNOM2 ----
        RPNOM2.setToolTipText("Nom");
        RPNOM2.setFont(new Font("sansserif", Font.BOLD, 14));
        RPNOM2.setMinimumSize(new Dimension(13, 30));
        RPNOM2.setPreferredSize(new Dimension(13, 30));
        RPNOM2.setMaximumSize(new Dimension(2147483647, 30));
        RPNOM2.setName("RPNOM2");
        pnlCoordonnees.add(RPNOM2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLibelleEdition ----
        lbLibelleEdition.setText("Libell\u00e9 d'\u00e9dition");
        lbLibelleEdition.setName("lbLibelleEdition");
        pnlCoordonnees.add(lbLibelleEdition, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbLibelleEdition2 ----
        lbLibelleEdition2.setText("Libell\u00e9 d'\u00e9dition");
        lbLibelleEdition2.setName("lbLibelleEdition2");
        pnlCoordonnees.add(lbLibelleEdition2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- RPEDT ----
        RPEDT.setToolTipText("Libell\u00e9 d'\u00e9dition");
        RPEDT.setMinimumSize(new Dimension(13, 30));
        RPEDT.setName("RPEDT");
        pnlCoordonnees.add(RPEDT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- RPEDT2 ----
        RPEDT2.setToolTipText("Libell\u00e9 d'\u00e9dition");
        RPEDT2.setMinimumSize(new Dimension(13, 30));
        RPEDT2.setName("RPEDT2");
        pnlCoordonnees.add(RPEDT2, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLocalisation ----
        lbLocalisation.setText("Adresse 1");
        lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisation.setInheritsPopupMenu(false);
        lbLocalisation.setMaximumSize(new Dimension(100, 30));
        lbLocalisation.setMinimumSize(new Dimension(80, 30));
        lbLocalisation.setPreferredSize(new Dimension(80, 30));
        lbLocalisation.setName("lbLocalisation");
        pnlCoordonnees.add(lbLocalisation, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- RPRUE ----
        RPRUE.setToolTipText("Rue");
        RPRUE.setMinimumSize(new Dimension(13, 30));
        RPRUE.setFont(new Font("sansserif", Font.PLAIN, 14));
        RPRUE.setMaximumSize(new Dimension(2147483647, 30));
        RPRUE.setPreferredSize(new Dimension(13, 30));
        RPRUE.setName("RPRUE");
        pnlCoordonnees.add(RPRUE, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbRue ----
        lbRue.setText("Adresse 2");
        lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRue.setPreferredSize(new Dimension(80, 30));
        lbRue.setMinimumSize(new Dimension(80, 30));
        lbRue.setName("lbRue");
        pnlCoordonnees.add(lbRue, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- RPLOC ----
        RPLOC.setToolTipText("Localit\u00e9");
        RPLOC.setMinimumSize(new Dimension(13, 30));
        RPLOC.setFont(new Font("sansserif", Font.PLAIN, 14));
        RPLOC.setName("RPLOC");
        pnlCoordonnees.add(RPLOC, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodePostal2 ----
        lbCodePostal2.setText("Commune");
        lbCodePostal2.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal2.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal2.setPreferredSize(new Dimension(80, 30));
        lbCodePostal2.setMinimumSize(new Dimension(80, 30));
        lbCodePostal2.setName("lbCodePostal2");
        pnlCoordonnees.add(lbCodePostal2, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCodePostalCommune ----
        snCodePostalCommune.setMinimumSize(new Dimension(350, 30));
        snCodePostalCommune.setPreferredSize(new Dimension(350, 30));
        snCodePostalCommune.setName("snCodePostalCommune");
        snCodePostalCommune.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snCodePostalCommuneValueChanged(e);
          }
        });
        pnlCoordonnees.add(snCodePostalCommune, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodePostal3 ----
        lbCodePostal3.setText("Pays");
        lbCodePostal3.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal3.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal3.setPreferredSize(new Dimension(80, 30));
        lbCodePostal3.setMinimumSize(new Dimension(80, 30));
        lbCodePostal3.setName("lbCodePostal3");
        pnlCoordonnees.add(lbCodePostal3, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snPays ----
        snPays.setMinimumSize(new Dimension(300, 30));
        snPays.setPreferredSize(new Dimension(300, 30));
        snPays.setName("snPays");
        pnlCoordonnees.add(snPays, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCoordonnees,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
        tabbedPane1.setName("tabbedPane1");
        
        // ======== pnlInfos ========
        {
          pnlInfos.setName("pnlInfos");
          pnlInfos.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInfos.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlInfos.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlInfos.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlInfos.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== pnlBlocInfosHaut ========
          {
            pnlBlocInfosHaut.setName("pnlBlocInfosHaut");
            pnlBlocInfosHaut.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBlocInfosHaut.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlBlocInfosHaut.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlBlocInfosHaut.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlBlocInfosHaut.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlBlocInfosHaut.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== sNPanel2 ========
            {
              sNPanel2.setName("sNPanel2");
              sNPanel2.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- snEtablissement ----
              snEtablissement.setName("snEtablissement");
              sNPanel2.add(snEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbLibtns ----
              lbLibtns.setText("@LIBTNS@");
              lbLibtns.setOpaque(false);
              lbLibtns.setName("lbLibtns");
              sNPanel2.add(lbLibtns, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlBlocInfosHaut.add(sNPanel2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin");
            lbMagasin2.setMaximumSize(new Dimension(80, 30));
            lbMagasin2.setMinimumSize(new Dimension(80, 30));
            lbMagasin2.setPreferredSize(new Dimension(80, 30));
            lbMagasin2.setName("lbMagasin2");
            pnlBlocInfosHaut.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlBlocInfosHaut.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClassement ----
            lbClassement.setText("Classement");
            lbClassement.setName("lbClassement");
            pnlBlocInfosHaut.add(lbClassement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RPCLK ----
            RPCLK.setMinimumSize(new Dimension(13, 30));
            RPCLK.setName("RPCLK");
            pnlBlocInfosHaut.add(RPCLK, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbType ----
            lbType.setText("Type de repr\u00e9sentant");
            lbType.setName("lbType");
            pnlBlocInfosHaut.add(lbType, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RPTYP ----
            RPTYP.setMinimumSize(new Dimension(13, 30));
            RPTYP.setName("RPTYP");
            pnlBlocInfosHaut.add(RPTYP, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTelephone ----
            lbTelephone.setText("T\u00e9l\u00e9phone");
            lbTelephone.setName("lbTelephone");
            pnlBlocInfosHaut.add(lbTelephone, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RPTEL ----
            RPTEL.setToolTipText("Num\u00e9ro de fax");
            RPTEL.setPreferredSize(new Dimension(200, 30));
            RPTEL.setMinimumSize(new Dimension(200, 30));
            RPTEL.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPTEL.setName("RPTEL");
            pnlBlocInfosHaut.add(RPTEL, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFax ----
            lbFax.setText("Fax");
            lbFax.setPreferredSize(new Dimension(50, 30));
            lbFax.setMinimumSize(new Dimension(50, 30));
            lbFax.setMaximumSize(new Dimension(50, 30));
            lbFax.setName("lbFax");
            pnlBlocInfosHaut.add(lbFax, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- RPFAX ----
            RPFAX.setToolTipText("Num\u00e9ro de fax");
            RPFAX.setPreferredSize(new Dimension(200, 30));
            RPFAX.setMinimumSize(new Dimension(200, 30));
            RPFAX.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPFAX.setName("RPFAX");
            pnlBlocInfosHaut.add(RPFAX, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMail ----
            lbMail.setText("Adresse e-mail");
            lbMail.setName("lbMail");
            pnlBlocInfosHaut.add(lbMail, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMail ----
            snMail.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMail.setName("snMail");
            pnlBlocInfosHaut.add(snMail, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbObservation ----
            lbObservation.setText("Observation");
            lbObservation.setName("lbObservation");
            pnlBlocInfosHaut.add(lbObservation, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RPOBS ----
            RPOBS.setMinimumSize(new Dimension(13, 30));
            RPOBS.setPreferredSize(new Dimension(13, 30));
            RPOBS.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPOBS.setName("RPOBS");
            pnlBlocInfosHaut.add(RPOBS, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInfos.add(pnlBlocInfosHaut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("Informations principales", pnlInfos);
        
        // ======== sNPanel3 ========
        {
          sNPanel3.setName("sNPanel3");
          sNPanel3.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbSectionAnalytique ----
          lbSectionAnalytique.setText("Section analytique");
          lbSectionAnalytique.setName("lbSectionAnalytique");
          sNPanel3.add(lbSectionAnalytique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snSectionAnalytique ----
          snSectionAnalytique.setName("snSectionAnalytique");
          sNPanel3.add(snSectionAnalytique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPointeur ----
          lbPointeur.setText("ou Pointeur");
          lbPointeur.setName("lbPointeur");
          sNPanel3.add(lbPointeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== sNPanel4 ========
          {
            sNPanel4.setName("sNPanel4");
            sNPanel4.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel4.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- RPNSA ----
            RPNSA.setPreferredSize(new Dimension(20, 30));
            RPNSA.setMinimumSize(new Dimension(20, 30));
            RPNSA.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPNSA.setName("RPNSA");
            sNPanel4.add(RPNSA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- SAN1 ----
            SAN1.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN1.setPreferredSize(new Dimension(50, 30));
            SAN1.setMinimumSize(new Dimension(50, 30));
            SAN1.setMaximumSize(new Dimension(50, 30));
            SAN1.setName("SAN1");
            sNPanel4.add(SAN1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- SAN2 ----
            SAN2.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN2.setPreferredSize(new Dimension(50, 30));
            SAN2.setMinimumSize(new Dimension(50, 30));
            SAN2.setMaximumSize(new Dimension(50, 30));
            SAN2.setName("SAN2");
            sNPanel4.add(SAN2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- SAN3 ----
            SAN3.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN3.setPreferredSize(new Dimension(50, 30));
            SAN3.setMinimumSize(new Dimension(50, 30));
            SAN3.setMaximumSize(new Dimension(50, 30));
            SAN3.setName("SAN3");
            sNPanel4.add(SAN3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- SAN4 ----
            SAN4.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN4.setPreferredSize(new Dimension(50, 30));
            SAN4.setMinimumSize(new Dimension(50, 30));
            SAN4.setMaximumSize(new Dimension(50, 30));
            SAN4.setName("SAN4");
            sNPanel4.add(SAN4, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- SAN5 ----
            SAN5.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN5.setPreferredSize(new Dimension(50, 30));
            SAN5.setMinimumSize(new Dimension(50, 30));
            SAN5.setMaximumSize(new Dimension(50, 30));
            SAN5.setName("SAN5");
            sNPanel4.add(SAN5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SAN6 ----
            SAN6.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN6.setPreferredSize(new Dimension(50, 30));
            SAN6.setMinimumSize(new Dimension(50, 30));
            SAN6.setMaximumSize(new Dimension(50, 30));
            SAN6.setName("SAN6");
            sNPanel4.add(SAN6, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SAN7 ----
            SAN7.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN7.setPreferredSize(new Dimension(50, 30));
            SAN7.setMinimumSize(new Dimension(50, 30));
            SAN7.setMaximumSize(new Dimension(50, 30));
            SAN7.setName("SAN7");
            sNPanel4.add(SAN7, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SAN8 ----
            SAN8.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN8.setPreferredSize(new Dimension(50, 30));
            SAN8.setMinimumSize(new Dimension(50, 30));
            SAN8.setMaximumSize(new Dimension(50, 30));
            SAN8.setName("SAN8");
            sNPanel4.add(SAN8, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- SAN9 ----
            SAN9.setFont(new Font("sansserif", Font.PLAIN, 14));
            SAN9.setPreferredSize(new Dimension(50, 30));
            SAN9.setMinimumSize(new Dimension(50, 30));
            SAN9.setMaximumSize(new Dimension(50, 30));
            SAN9.setName("SAN9");
            sNPanel4.add(SAN9, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          sNPanel3.add(sNPanel4, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("Analytique", sNPanel3);
      }
      pnlContenu.add(tabbedPane1,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlDeuxiemeLigne2 ========
      {
        pnlDeuxiemeLigne2.setName("pnlDeuxiemeLigne2");
        pnlDeuxiemeLigne2.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDeuxiemeLigne2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDeuxiemeLigne2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDeuxiemeLigne2.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDeuxiemeLigne2.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlTitreReglement2 ========
        {
          pnlTitreReglement2.setName("pnlTitreReglement2");
          pnlTitreReglement2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreReglement2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreReglement2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreReglement2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreReglement2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- btDivers ----
          btDivers.setToolTipText("Divers");
          btDivers.setName("btDivers");
          btDivers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btDiversActionPerformed(e);
            }
          });
          pnlTitreReglement2.add(btDivers, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbReglement2 ----
          lbReglement2.setText("Divers");
          lbReglement2.setPreferredSize(new Dimension(150, 20));
          lbReglement2.setMinimumSize(new Dimension(150, 20));
          lbReglement2.setMaximumSize(new Dimension(150, 20));
          lbReglement2.setName("lbReglement2");
          pnlTitreReglement2.add(lbReglement2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne2.add(pnlTitreReglement2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== pnlTitreCompta3 ========
        {
          pnlTitreCompta3.setName("pnlTitreCompta3");
          pnlTitreCompta3.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreCompta3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreCompta3.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreCompta3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreCompta3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- btCommissionnement ----
          btCommissionnement.setToolTipText("Commissionnement");
          btCommissionnement.setName("btCommissionnement");
          btCommissionnement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btCommissionnementActionPerformed(e);
            }
          });
          pnlTitreCompta3.add(btCommissionnement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbCompta3 ----
          lbCompta3.setText("Commissionnement");
          lbCompta3.setPreferredSize(new Dimension(150, 20));
          lbCompta3.setMinimumSize(new Dimension(150, 20));
          lbCompta3.setMaximumSize(new Dimension(150, 20));
          lbCompta3.setName("lbCompta3");
          pnlTitreCompta3.add(lbCompta3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne2.add(pnlTitreCompta3, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlDivers2 ========
        {
          pnlDivers2.setMinimumSize(new Dimension(487, 180));
          pnlDivers2.setPreferredSize(new Dimension(487, 180));
          pnlDivers2.setName("pnlDivers2");
          pnlDivers2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDivers2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDivers2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDivers2.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDivers2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbAvanceNotesDeFrais ----
          lbAvanceNotesDeFrais.setText("Avance sur notes de frais");
          lbAvanceNotesDeFrais.setMinimumSize(new Dimension(180, 30));
          lbAvanceNotesDeFrais.setMaximumSize(new Dimension(180, 30));
          lbAvanceNotesDeFrais.setPreferredSize(new Dimension(180, 30));
          lbAvanceNotesDeFrais.setName("lbAvanceNotesDeFrais");
          pnlDivers2.add(lbAvanceNotesDeFrais, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== sNPanel5 ========
          {
            sNPanel5.setName("sNPanel5");
            sNPanel5.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- RPNOF ----
            RPNOF.setPreferredSize(new Dimension(90, 30));
            RPNOF.setMinimumSize(new Dimension(90, 30));
            RPNOF.setName("RPNOF");
            sNPanel5.add(RPNOF, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDivers2.add(sNPanel5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbVendeur2 ----
          lbVendeur2.setText("Vendeur");
          lbVendeur2.setMaximumSize(new Dimension(80, 30));
          lbVendeur2.setMinimumSize(new Dimension(80, 30));
          lbVendeur2.setPreferredSize(new Dimension(80, 30));
          lbVendeur2.setName("lbVendeur2");
          pnlDivers2.add(lbVendeur2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snVendeur ----
          snVendeur.setMaximumSize(new Dimension(250, 30));
          snVendeur.setMinimumSize(new Dimension(250, 30));
          snVendeur.setPreferredSize(new Dimension(250, 30));
          snVendeur.setName("snVendeur");
          pnlDivers2.add(snVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbZoneGeographique2 ----
          lbZoneGeographique2.setText("Zone g\u00e9ographique");
          lbZoneGeographique2.setName("lbZoneGeographique2");
          pnlDivers2.add(lbZoneGeographique2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snZoneGeographique ----
          snZoneGeographique.setMaximumSize(new Dimension(250, 30));
          snZoneGeographique.setMinimumSize(new Dimension(250, 30));
          snZoneGeographique.setPreferredSize(new Dimension(250, 30));
          snZoneGeographique.setName("snZoneGeographique");
          pnlDivers2.add(snZoneGeographique, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne2.add(pnlDivers2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlCommissionnement2 ========
        {
          pnlCommissionnement2.setPreferredSize(new Dimension(222, 180));
          pnlCommissionnement2.setMinimumSize(new Dimension(222, 180));
          pnlCommissionnement2.setName("pnlCommissionnement2");
          pnlCommissionnement2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCommissionnement2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlCommissionnement2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlCommissionnement2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCommissionnement2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCodeRegroupement2 ----
          lbCodeRegroupement2.setText("Regroupement");
          lbCodeRegroupement2.setName("lbCodeRegroupement2");
          pnlCommissionnement2.add(lbCodeRegroupement2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snConditionCommissionnement ----
          snConditionCommissionnement.setMaximumSize(new Dimension(250, 30));
          snConditionCommissionnement.setMinimumSize(new Dimension(250, 30));
          snConditionCommissionnement.setPreferredSize(new Dimension(250, 30));
          snConditionCommissionnement.setName("snConditionCommissionnement");
          pnlCommissionnement2.add(snConditionCommissionnement, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbSeuil2 ----
          lbSeuil2.setText("Seuil");
          lbSeuil2.setMaximumSize(new Dimension(80, 30));
          lbSeuil2.setMinimumSize(new Dimension(80, 30));
          lbSeuil2.setPreferredSize(new Dimension(80, 30));
          lbSeuil2.setName("lbSeuil2");
          pnlCommissionnement2.add(lbSeuil2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== sNPanel7 ========
          {
            sNPanel7.setName("sNPanel7");
            sNPanel7.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel7.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel7.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel7.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel7.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- RPSEU ----
            RPSEU.setMinimumSize(new Dimension(60, 30));
            RPSEU.setPreferredSize(new Dimension(60, 30));
            RPSEU.setMaximumSize(new Dimension(60, 30));
            RPSEU.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPSEU.setName("RPSEU");
            sNPanel7.add(RPSEU, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbTaux2 ----
            lbTaux2.setText("Taux");
            lbTaux2.setMaximumSize(new Dimension(80, 30));
            lbTaux2.setMinimumSize(new Dimension(80, 30));
            lbTaux2.setPreferredSize(new Dimension(80, 30));
            lbTaux2.setName("lbTaux2");
            sNPanel7.add(lbTaux2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCommissionnement2.add(sNPanel7, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- RPCOM ----
          RPCOM.setMinimumSize(new Dimension(72, 30));
          RPCOM.setPreferredSize(new Dimension(72, 30));
          RPCOM.setFont(new Font("sansserif", Font.PLAIN, 14));
          RPCOM.setMaximumSize(new Dimension(72, 30));
          RPCOM.setName("RPCOM");
          pnlCommissionnement2.add(RPCOM, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTaux3 ----
          lbTaux3.setText("Base");
          lbTaux3.setMaximumSize(new Dimension(80, 30));
          lbTaux3.setMinimumSize(new Dimension(80, 30));
          lbTaux3.setPreferredSize(new Dimension(80, 30));
          lbTaux3.setName("lbTaux3");
          pnlCommissionnement2.add(lbTaux3, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- RPMOC ----
          RPMOC.setModel(new DefaultComboBoxModel(new String[] { "Sur marge", "Sur chiffre d'affaires", "Hors droits sur alcools" }));
          RPMOC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RPMOC.setFont(new Font("sansserif", Font.PLAIN, 14));
          RPMOC.setMinimumSize(new Dimension(100, 30));
          RPMOC.setPreferredSize(new Dimension(100, 30));
          RPMOC.setMaximumSize(new Dimension(100, 30));
          RPMOC.setBackground(Color.white);
          RPMOC.setName("RPMOC");
          pnlCommissionnement2.add(RPMOC, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne2.add(pnlCommissionnement2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDeuxiemeLigne2,
          new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlDeuxiemeLigne ========
      {
        pnlDeuxiemeLigne.setName("pnlDeuxiemeLigne");
        pnlDeuxiemeLigne.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDeuxiemeLigne.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlTitreReglement ========
        {
          pnlTitreReglement.setName("pnlTitreReglement");
          pnlTitreReglement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreReglement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreReglement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreReglement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreReglement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- bt_objectif ----
          bt_objectif.setToolTipText("Objectifs");
          bt_objectif.setName("bt_objectif");
          bt_objectif.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_objectifActionPerformed(e);
            }
          });
          pnlTitreReglement.add(bt_objectif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbReglement ----
          lbReglement.setText("Objectifs");
          lbReglement.setPreferredSize(new Dimension(150, 20));
          lbReglement.setMinimumSize(new Dimension(150, 20));
          lbReglement.setMaximumSize(new Dimension(150, 20));
          lbReglement.setName("lbReglement");
          pnlTitreReglement.add(lbReglement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne.add(pnlTitreReglement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 2, 5), 0, 0));
        
        // ======== pnlTitreCompta2 ========
        {
          pnlTitreCompta2.setName("pnlTitreCompta2");
          pnlTitreCompta2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTitreCompta2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTitreCompta2.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTitreCompta2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTitreCompta2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- btStats ----
          btStats.setToolTipText("Statistiques");
          btStats.setName("btStats");
          btStats.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btStatsActionPerformed(e);
            }
          });
          pnlTitreCompta2.add(btStats, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbCompta2 ----
          lbCompta2.setText("Statistiques");
          lbCompta2.setPreferredSize(new Dimension(150, 20));
          lbCompta2.setMinimumSize(new Dimension(150, 20));
          lbCompta2.setMaximumSize(new Dimension(150, 20));
          lbCompta2.setName("lbCompta2");
          pnlTitreCompta2.add(lbCompta2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne.add(pnlTitreCompta2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
        
        // ======== pnlDivers ========
        {
          pnlDivers.setMinimumSize(new Dimension(487, 140));
          pnlDivers.setPreferredSize(new Dimension(487, 140));
          pnlDivers.setName("pnlDivers");
          pnlDivers.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDivers.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDivers.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDivers.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDivers.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- sNLabelChamp1 ----
          sNLabelChamp1.setText("Mois/Montant");
          sNLabelChamp1.setName("sNLabelChamp1");
          pnlDivers.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== sNPanel8 ========
          {
            sNPanel8.setName("sNPanel8");
            sNPanel8.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel8.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel8.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel8.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel8.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- RPO01 ----
            RPO01.setPreferredSize(new Dimension(90, 30));
            RPO01.setMinimumSize(new Dimension(90, 30));
            RPO01.setName("RPO01");
            sNPanel8.add(RPO01, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RPO02 ----
            RPO02.setPreferredSize(new Dimension(90, 30));
            RPO02.setMinimumSize(new Dimension(90, 30));
            RPO02.setName("RPO02");
            sNPanel8.add(RPO02, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- RPO03 ----
            RPO03.setPreferredSize(new Dimension(90, 30));
            RPO03.setMinimumSize(new Dimension(90, 30));
            RPO03.setName("RPO03");
            sNPanel8.add(RPO03, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDivers.add(sNPanel8, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- sNLabelChamp2 ----
          sNLabelChamp2.setText("Plafond atteint");
          sNLabelChamp2.setName("sNLabelChamp2");
          pnlDivers.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- RPCO2 ----
          RPCO2.setPreferredSize(new Dimension(90, 30));
          RPCO2.setMinimumSize(new Dimension(90, 30));
          RPCO2.setName("RPCO2");
          pnlDivers.add(RPCO2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne.add(pnlDivers, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlStatistiques ========
        {
          pnlStatistiques.setMinimumSize(new Dimension(285, 140));
          pnlStatistiques.setPreferredSize(new Dimension(285, 140));
          pnlStatistiques.setName("pnlStatistiques");
          pnlStatistiques.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlStatistiques.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlStatistiques.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlStatistiques.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlStatistiques.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMois ----
          lbMois.setText("Chiffre d'affaires @WDSVX@");
          lbMois.setPreferredSize(new Dimension(200, 30));
          lbMois.setMaximumSize(new Dimension(200, 30));
          lbMois.setMinimumSize(new Dimension(200, 30));
          lbMois.setName("lbMois");
          pnlStatistiques.add(lbMois, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- SVCRM ----
          SVCRM.setHorizontalAlignment(SwingConstants.RIGHT);
          SVCRM.setPreferredSize(new Dimension(130, 30));
          SVCRM.setMinimumSize(new Dimension(130, 30));
          SVCRM.setMaximumSize(new Dimension(130, 30));
          SVCRM.setFont(new Font("sansserif", Font.PLAIN, 14));
          SVCRM.setName("SVCRM");
          pnlStatistiques.add(SVCRM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbExN ----
          lbExN.setText("Exercice N");
          lbExN.setPreferredSize(new Dimension(100, 30));
          lbExN.setMaximumSize(new Dimension(100, 30));
          lbExN.setMinimumSize(new Dimension(100, 30));
          lbExN.setName("lbExN");
          pnlStatistiques.add(lbExN, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- SVCR1 ----
          SVCR1.setHorizontalAlignment(SwingConstants.RIGHT);
          SVCR1.setPreferredSize(new Dimension(130, 30));
          SVCR1.setMinimumSize(new Dimension(130, 30));
          SVCR1.setMaximumSize(new Dimension(130, 30));
          SVCR1.setFont(new Font("sansserif", Font.PLAIN, 14));
          SVCR1.setName("SVCR1");
          pnlStatistiques.add(SVCR1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbN1 ----
          lbN1.setText("Exercice N-1");
          lbN1.setPreferredSize(new Dimension(100, 30));
          lbN1.setMinimumSize(new Dimension(100, 30));
          lbN1.setMaximumSize(new Dimension(100, 30));
          lbN1.setName("lbN1");
          pnlStatistiques.add(lbN1, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- SVCR0 ----
          SVCR0.setHorizontalAlignment(SwingConstants.RIGHT);
          SVCR0.setPreferredSize(new Dimension(130, 30));
          SVCR0.setMinimumSize(new Dimension(130, 30));
          SVCR0.setMaximumSize(new Dimension(130, 30));
          SVCR0.setName("SVCR0");
          pnlStatistiques.add(SVCR0, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDeuxiemeLigne.add(pnlStatistiques, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDeuxiemeLigne,
          new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCoordonnees;
  private SNLabelChamp lbCodeRepresentant2;
  private XRiTextField INDREP;
  private XRiTextField INDREP2;
  private SNLabelChamp lbCivilite;
  private SNLabelChamp lbCivilite2;
  private SNCivilite snCivilite;
  private SNCivilite snCivilite2;
  private SNLabelChamp lbNom;
  private SNLabelChamp lbNom2;
  private XRiTextField RPNOM;
  private XRiTextField RPNOM2;
  private SNLabelChamp lbLibelleEdition;
  private SNLabelChamp lbLibelleEdition2;
  private XRiTextField RPEDT;
  private XRiTextField RPEDT2;
  private SNLabelChamp lbLocalisation;
  private XRiTextField RPRUE;
  private SNLabelChamp lbRue;
  private XRiTextField RPLOC;
  private SNLabelChamp lbCodePostal2;
  private SNCodePostalCommune snCodePostalCommune;
  private SNLabelChamp lbCodePostal3;
  private SNPays snPays;
  private JTabbedPane tabbedPane1;
  private SNPanelContenu pnlInfos;
  private SNPanel pnlBlocInfosHaut;
  private SNLabelChamp lbEtablissement;
  private SNPanel sNPanel2;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbLibtns;
  private SNLabelChamp lbMagasin2;
  private SNMagasin snMagasin;
  private SNLabelChamp lbClassement;
  private XRiTextField RPCLK;
  private SNLabelChamp lbType;
  private XRiTextField RPTYP;
  private SNLabelChamp lbTelephone;
  private XRiTextField RPTEL;
  private SNLabelChamp lbFax;
  private XRiTextField RPFAX;
  private SNLabelChamp lbMail;
  private SNAdresseMail snMail;
  private SNLabelChamp lbObservation;
  private XRiTextField RPOBS;
  private SNPanelContenu sNPanel3;
  private SNLabelChamp lbSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNLabelChamp lbPointeur;
  private SNPanel sNPanel4;
  private XRiTextField RPNSA;
  private XRiTextField SAN1;
  private XRiTextField SAN2;
  private XRiTextField SAN3;
  private XRiTextField SAN4;
  private XRiTextField SAN5;
  private XRiTextField SAN6;
  private XRiTextField SAN7;
  private XRiTextField SAN8;
  private XRiTextField SAN9;
  private SNPanel pnlDeuxiemeLigne2;
  private SNPanel pnlTitreReglement2;
  private SNBoutonDetail btDivers;
  private SNLabelTitre lbReglement2;
  private SNPanel pnlTitreCompta3;
  private SNBoutonDetail btCommissionnement;
  private SNLabelTitre lbCompta3;
  private SNPanelTitre pnlDivers2;
  private SNLabelChamp lbAvanceNotesDeFrais;
  private SNPanel sNPanel5;
  private XRiTextField RPNOF;
  private SNLabelChamp lbVendeur2;
  private SNVendeur snVendeur;
  private SNLabelChamp lbZoneGeographique2;
  private SNZoneGeographique snZoneGeographique;
  private SNPanelTitre pnlCommissionnement2;
  private SNLabelChamp lbCodeRegroupement2;
  private SNConditionCommissionnement snConditionCommissionnement;
  private SNLabelChamp lbSeuil2;
  private SNPanel sNPanel7;
  private XRiTextField RPSEU;
  private SNLabelChamp lbTaux2;
  private XRiTextField RPCOM;
  private SNLabelChamp lbTaux3;
  private XRiComboBox RPMOC;
  private SNPanel pnlDeuxiemeLigne;
  private SNPanel pnlTitreReglement;
  private SNBoutonDetail bt_objectif;
  private SNLabelTitre lbReglement;
  private SNPanel pnlTitreCompta2;
  private SNBoutonDetail btStats;
  private SNLabelTitre lbCompta2;
  private SNPanelTitre pnlDivers;
  private SNLabelChamp sNLabelChamp1;
  private SNPanel sNPanel8;
  private XRiTextField RPO01;
  private XRiTextField RPO02;
  private XRiTextField RPO03;
  private SNLabelChamp sNLabelChamp2;
  private XRiTextField RPCO2;
  private SNPanelTitre pnlStatistiques;
  private SNLabelChamp lbMois;
  private XRiTextField SVCRM;
  private SNLabelChamp lbExN;
  private XRiTextField SVCR1;
  private SNLabelChamp lbN1;
  private XRiTextField SVCR0;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
