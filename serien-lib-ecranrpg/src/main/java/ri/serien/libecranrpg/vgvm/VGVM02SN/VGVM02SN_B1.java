
package ri.serien.libecranrpg.vgvm.VGVM02SN;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM02SN_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] PZ11_Value = { "", "30", "31", "32", "33", };
  private String[] PZ10_Value = { "", "30", "31", "32", "33", };
  private String[] PZ09_Value = { "", "30", "31", "32", "33", };
  private String[] PZ08_Value = { "", "30", "31", "32", "33", };
  private String[] SNE9D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE9G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE8D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE8G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE7D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE7G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE6D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE6G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE5D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE5G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE4D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE4G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE3D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE3G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE2D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE2G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE1D_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] SNE1G_Value = { "", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", };
  private String[] PZ07_Value = { "", "11", "12", "20", "21", "25", "26", "99", "96", "97", "98", "95", };
  private String[] PZ06_Value = { "", "11", "12", "20", "21", "25", "26", "99", "96", "97", "98", "95", };
  private String[] PZ05_Value = { "", "11", "12", "20", "21", "25", "26", "99", "96", "97", "98", "95", };
  private String[] PZ04_Value = { "", "11", "12", "20", "21", "25", "26", "99", "96", "97", "98", "95", };
  private String[] PZ03_Value = { "", "11", "12", "20", "21", "25", "99", };
  private String[] PZ02_Value = { "", "1", "2", "3", "4", "5", };
  private String[] PZ01_Value = { "", "1", "2", "3", "4", "5", };
  
  public VGVM02SN_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    SNE9D.setValeurs(SNE9D_Value, null);
    SNE9G.setValeurs(SNE9G_Value, null);
    SNE8D.setValeurs(SNE8D_Value, null);
    SNE8G.setValeurs(SNE8G_Value, null);
    SNE7D.setValeurs(SNE7D_Value, null);
    SNE7G.setValeurs(SNE7G_Value, null);
    SNE6D.setValeurs(SNE6D_Value, null);
    SNE6G.setValeurs(SNE6G_Value, null);
    SNE5D.setValeurs(SNE5D_Value, null);
    SNE5G.setValeurs(SNE5G_Value, null);
    SNE4D.setValeurs(SNE4D_Value, null);
    SNE4G.setValeurs(SNE4G_Value, null);
    SNE3D.setValeurs(SNE3D_Value, null);
    SNE3G.setValeurs(SNE3G_Value, null);
    SNE2D.setValeurs(SNE2D_Value, null);
    SNE2G.setValeurs(SNE2G_Value, null);
    SNE1D.setValeurs(SNE1D_Value, null);
    SNE1G.setValeurs(SNE1G_Value, null);
    PZ11.setValeurs(PZ11_Value, null);
    PZ10.setValeurs(PZ10_Value, null);
    PZ09.setValeurs(PZ09_Value, null);
    PZ08.setValeurs(PZ08_Value, null);
    PZ07.setValeurs(PZ07_Value, null);
    PZ06.setValeurs(PZ06_Value, null);
    PZ05.setValeurs(PZ05_Value, null);
    PZ04.setValeurs(PZ04_Value, null);
    PZ03.setValeurs(PZ03_Value, null);
    PZ02.setValeurs(PZ02_Value, null);
    PZ01.setValeurs(PZ01_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // bloquer les critères si interro
    boolean etatProgramme = true;
    if (lexique.isTrue("53")) {
      etatProgramme = false;
    }
    
    // PZ11.setSelectedIndex(getIndice("PZ11", PZ11_Value));
    PZ11.setEnabled(etatProgramme);
    // PZ10.setSelectedIndex(getIndice("PZ10", PZ10_Value));
    // PZ10.setEnabled( lexique.isPresent("PZ10"));
    PZ10.setEnabled(etatProgramme);
    // PZ09.setSelectedIndex(getIndice("PZ09", PZ09_Value));
    PZ09.setEnabled(etatProgramme);
    // PZ08.setSelectedIndex(getIndice("PZ08", PZ08_Value));
    PZ08.setEnabled(etatProgramme);
    // SNE9D.setSelectedIndex(getIndice("SNE9D", SNE9D_Value));
    SNE9D.setEnabled(etatProgramme);
    // SNE9G.setSelectedIndex(getIndice("SNE9G", SNE9G_Value));
    SNE9G.setEnabled(etatProgramme);
    // SNE8D.setSelectedIndex(getIndice("SNE8D", SNE8D_Value));
    SNE8D.setEnabled(etatProgramme);
    // SNE8G.setSelectedIndex(getIndice("SNE8G", SNE8G_Value));
    SNE8G.setEnabled(etatProgramme);
    // SNE7D.setSelectedIndex(getIndice("SNE7D", SNE7D_Value));
    SNE7D.setEnabled(etatProgramme);
    // SNE7G.setSelectedIndex(getIndice("SNE7G", SNE7G_Value));
    SNE7G.setEnabled(etatProgramme);
    // SNE6D.setSelectedIndex(getIndice("SNE6D", SNE6D_Value));
    SNE6D.setEnabled(etatProgramme);
    // SNE6G.setSelectedIndex(getIndice("SNE6G", SNE6G_Value));
    SNE6G.setEnabled(etatProgramme);
    // SNE5D.setSelectedIndex(getIndice("SNE5D", SNE5D_Value));
    SNE5D.setEnabled(etatProgramme);
    // SNE5G.setSelectedIndex(getIndice("SNE5G", SNE5G_Value));
    SNE5G.setEnabled(etatProgramme);
    // SNE4D.setSelectedIndex(getIndice("SNE4D", SNE4D_Value));
    SNE4D.setEnabled(etatProgramme);
    // SNE4G.setSelectedIndex(getIndice("SNE4G", SNE4G_Value));
    SNE4G.setEnabled(etatProgramme);
    // SNE3D.setSelectedIndex(getIndice("SNE3D", SNE3D_Value));
    SNE3D.setEnabled(etatProgramme);
    // SNE3G.setSelectedIndex(getIndice("SNE3G", SNE3G_Value));
    SNE3G.setEnabled(etatProgramme);
    // SNE2D.setSelectedIndex(getIndice("SNE2D", SNE2D_Value));
    SNE2D.setEnabled(etatProgramme);
    // SNE2G.setSelectedIndex(getIndice("SNE2G", SNE2G_Value));
    SNE2G.setEnabled(etatProgramme);
    // SNE1D.setSelectedIndex(getIndice("SNE1D", SNE1D_Value));
    SNE1D.setEnabled(etatProgramme);
    // SNE1G.setSelectedIndex(getIndice("SNE1G", SNE1G_Value));
    SNE1G.setEnabled(etatProgramme);
    // PZ07.setSelectedIndex(getIndice("PZ07", PZ07_Value));
    PZ07.setEnabled(etatProgramme);
    // PZ06.setSelectedIndex(getIndice("PZ06", PZ06_Value));
    PZ06.setEnabled(etatProgramme);
    // PZ05.setSelectedIndex(getIndice("PZ05", PZ05_Value));
    PZ05.setEnabled(etatProgramme);
    // PZ04.setSelectedIndex(getIndice("PZ04", PZ04_Value));
    PZ04.setEnabled(etatProgramme);
    // PZ03.setSelectedIndex(getIndice("PZ03", PZ03_Value));
    PZ03.setEnabled(etatProgramme);
    // PZ02.setSelectedIndex(getIndice("PZ02", PZ02_Value));
    PZ02.setEnabled(etatProgramme);
    // PZ01.setSelectedIndex(getIndice("PZ01", PZ01_Value));
    PZ01.setEnabled(etatProgramme);
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@ STATISTIQUES"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("PZ11", 0, PZ11_Value[PZ11.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ10", 0, PZ10_Value[PZ10.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ09", 0, PZ09_Value[PZ09.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ08", 0, PZ08_Value[PZ08.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE9D", 0, SNE9D_Value[SNE9D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE9G", 0, SNE9G_Value[SNE9G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE8D", 0, SNE8D_Value[SNE8D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE8G", 0, SNE8G_Value[SNE8G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE7D", 0, SNE7D_Value[SNE7D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE7G", 0, SNE7G_Value[SNE7G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE6D", 0, SNE6D_Value[SNE6D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE6G", 0, SNE6G_Value[SNE6G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE5D", 0, SNE5D_Value[SNE5D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE5G", 0, SNE5G_Value[SNE5G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE4D", 0, SNE4D_Value[SNE4D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE4G", 0, SNE4G_Value[SNE4G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE3D", 0, SNE3D_Value[SNE3D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE3G", 0, SNE3G_Value[SNE3G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE2D", 0, SNE2D_Value[SNE2D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE2G", 0, SNE2G_Value[SNE2G.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE1D", 0, SNE1D_Value[SNE1D.getSelectedIndex()]);
    // lexique.HostFieldPutData("SNE1G", 0, SNE1G_Value[SNE1G.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ07", 0, PZ07_Value[PZ07.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ06", 0, PZ06_Value[PZ06.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ05", 0, PZ05_Value[PZ05.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ04", 0, PZ04_Value[PZ04.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ03", 0, PZ03_Value[PZ03.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ02", 0, PZ02_Value[PZ02.getSelectedIndex()]);
    // lexique.HostFieldPutData("PZ01", 0, PZ01_Value[PZ01.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_44 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_46 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    PZ01 = new XRiComboBox();
    PZ02 = new XRiComboBox();
    PZ03 = new XRiComboBox();
    PZ04 = new XRiComboBox();
    PZ05 = new XRiComboBox();
    PZ06 = new XRiComboBox();
    PZ07 = new XRiComboBox();
    OBJ_85 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_83 = new JLabel();
    PZ08 = new XRiComboBox();
    PZ09 = new XRiComboBox();
    PZ10 = new XRiComboBox();
    PZ11 = new XRiComboBox();
    OBJ_84 = new JLabel();
    OBJ_86 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_90 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    SNE1G = new XRiComboBox();
    SNE1D = new XRiComboBox();
    SNE2G = new XRiComboBox();
    SNE2D = new XRiComboBox();
    SNE3G = new XRiComboBox();
    SNE3D = new XRiComboBox();
    SNE4G = new XRiComboBox();
    SNE4D = new XRiComboBox();
    SNE5G = new XRiComboBox();
    SNE5D = new XRiComboBox();
    SNE6G = new XRiComboBox();
    SNE6D = new XRiComboBox();
    SNE7G = new XRiComboBox();
    SNE7D = new XRiComboBox();
    SNE8G = new XRiComboBox();
    SNE8D = new XRiComboBox();
    SNE9G = new XRiComboBox();
    SNE9D = new XRiComboBox();
    OBJ_97 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_105 = new JLabel();
    OBJ_106 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setOpaque(false);
          INDTYP.setText("@INDTYP@");
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setOpaque(false);
          INDIND.setText("@INDIND@");
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Param\u00e9trage des zones statistiques");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- PZ01 ----
            PZ01.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en UV",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UL",
              "Double quantit\u00e9",
              "Quantit\u00e9 en unit\u00e9 de col (alcools)"
            }));
            PZ01.setComponentPopupMenu(BTD);
            PZ01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ01.setName("PZ01");
            xTitledPanel1ContentContainer.add(PZ01);
            PZ01.setBounds(190, 10, 220, PZ01.getPreferredSize().height);

            //---- PZ02 ----
            PZ02.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Quantit\u00e9 en UV",
              "Quantit\u00e9 en US",
              "Quantit\u00e9 en UL",
              "Double quantit\u00e9",
              "Quantit\u00e9 en unit\u00e9 de col (alcools)"
            }));
            PZ02.setComponentPopupMenu(BTD);
            PZ02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ02.setName("PZ02");
            xTitledPanel1ContentContainer.add(PZ02);
            PZ02.setBounds(190, 40, 220, PZ02.getPreferredSize().height);

            //---- PZ03 ----
            PZ03.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "PUMP",
              "Prix achat",
              "Montant transport",
              "Montant promotions",
              "Montant droits",
              "Acc\u00e8s ext\u00e9rieur"
            }));
            PZ03.setComponentPopupMenu(BTD);
            PZ03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ03.setName("PZ03");
            xTitledPanel1ContentContainer.add(PZ03);
            PZ03.setBounds(190, 70, 220, PZ03.getPreferredSize().height);

            //---- PZ04 ----
            PZ04.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "PUMP",
              "Prix achat",
              "Montant transport",
              "Montant promotions",
              "Montant droits",
              "CA avec RFA par anticipation",
              "Acc\u00e8s ext\u00e9rieur",
              "Nombre de palettes",
              "Volume",
              "Poids",
              "Nombre de colis"
            }));
            PZ04.setComponentPopupMenu(BTD);
            PZ04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ04.setName("PZ04");
            xTitledPanel1ContentContainer.add(PZ04);
            PZ04.setBounds(190, 100, 220, PZ04.getPreferredSize().height);

            //---- PZ05 ----
            PZ05.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "PUMP",
              "Prix achat",
              "Montant transport",
              "Montant promotions",
              "Montant droits",
              "CA avec RFA par anticipation",
              "Acc\u00e8s ext\u00e9rieur",
              "Nombre de palettes",
              "Volume",
              "Poids",
              "Nombre de colis"
            }));
            PZ05.setComponentPopupMenu(BTD);
            PZ05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ05.setName("PZ05");
            xTitledPanel1ContentContainer.add(PZ05);
            PZ05.setBounds(190, 130, 220, PZ05.getPreferredSize().height);

            //---- PZ06 ----
            PZ06.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "PUMP",
              "Prix achat",
              "Montant transport",
              "Montant promotions",
              "Montant droits",
              "CA avec RFA par anticipation",
              "Acc\u00e8s ext\u00e9rieur",
              "Nombre de palettes",
              "Volume",
              "Poids",
              "Nombre de colis"
            }));
            PZ06.setComponentPopupMenu(BTD);
            PZ06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ06.setName("PZ06");
            xTitledPanel1ContentContainer.add(PZ06);
            PZ06.setBounds(190, 160, 220, PZ06.getPreferredSize().height);

            //---- PZ07 ----
            PZ07.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "PUMP",
              "Prix achat",
              "Montant transport",
              "Montant promotions",
              "Montant droits",
              "CA avec RFA par anticipation",
              "Acc\u00e8s ext\u00e9rieur",
              "Nombre de palettes",
              "Volume",
              "Poids",
              "Nombre de colis"
            }));
            PZ07.setComponentPopupMenu(BTD);
            PZ07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ07.setName("PZ07");
            xTitledPanel1ContentContainer.add(PZ07);
            PZ07.setBounds(190, 190, 220, PZ07.getPreferredSize().height);

            //---- OBJ_85 ----
            OBJ_85.setText("CA / prix de revient");
            OBJ_85.setName("OBJ_85");
            xTitledPanel1ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(15, 73, 137, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("Zone personnalis\u00e9e  1");
            OBJ_87.setName("OBJ_87");
            xTitledPanel1ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(15, 103, 137, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("Zone personnalis\u00e9e  2");
            OBJ_89.setName("OBJ_89");
            xTitledPanel1ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(15, 133, 137, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("Zone personnalis\u00e9e  3");
            OBJ_91.setName("OBJ_91");
            xTitledPanel1ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(15, 163, 137, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("Zone personnalis\u00e9e  4");
            OBJ_92.setName("OBJ_92");
            xTitledPanel1ContentContainer.add(OBJ_92);
            OBJ_92.setBounds(15, 193, 137, 20);

            //---- OBJ_40 ----
            OBJ_40.setText("Quantit\u00e9  1");
            OBJ_40.setName("OBJ_40");
            xTitledPanel1ContentContainer.add(OBJ_40);
            OBJ_40.setBounds(15, 13, 83, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("Quantit\u00e9  2");
            OBJ_83.setName("OBJ_83");
            xTitledPanel1ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(15, 43, 83, 20);

            //---- PZ08 ----
            PZ08.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Rang CA",
              "Rang marge",
              "Rang facture",
              "Rang quantit\u00e9"
            }));
            PZ08.setComponentPopupMenu(BTD);
            PZ08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ08.setName("PZ08");
            xTitledPanel1ContentContainer.add(PZ08);
            PZ08.setBounds(495, 10, 120, PZ08.getPreferredSize().height);

            //---- PZ09 ----
            PZ09.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Rang CA",
              "Rang marge",
              "Rang facture",
              "Rang quantit\u00e9"
            }));
            PZ09.setComponentPopupMenu(BTD);
            PZ09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ09.setName("PZ09");
            xTitledPanel1ContentContainer.add(PZ09);
            PZ09.setBounds(495, 40, 120, PZ09.getPreferredSize().height);

            //---- PZ10 ----
            PZ10.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Rang CA",
              "Rang marge",
              "Rang facture",
              "Rang quantit\u00e9"
            }));
            PZ10.setComponentPopupMenu(BTD);
            PZ10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ10.setName("PZ10");
            xTitledPanel1ContentContainer.add(PZ10);
            PZ10.setBounds(495, 70, 120, PZ10.getPreferredSize().height);

            //---- PZ11 ----
            PZ11.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Rang CA",
              "Rang marge",
              "Rang facture",
              "Rang quantit\u00e9"
            }));
            PZ11.setComponentPopupMenu(BTD);
            PZ11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PZ11.setName("PZ11");
            xTitledPanel1ContentContainer.add(PZ11);
            PZ11.setBounds(495, 100, 120, PZ11.getPreferredSize().height);

            //---- OBJ_84 ----
            OBJ_84.setText("Rang 1");
            OBJ_84.setName("OBJ_84");
            xTitledPanel1ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(440, 13, 46, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Rang 2");
            OBJ_86.setName("OBJ_86");
            xTitledPanel1ContentContainer.add(OBJ_86);
            OBJ_86.setBounds(440, 43, 46, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("Rang 3");
            OBJ_88.setName("OBJ_88");
            xTitledPanel1ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(440, 73, 46, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Rang 4");
            OBJ_90.setName("OBJ_90");
            xTitledPanel1ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(440, 103, 46, 20);
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Personnalisation tableau de bord");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- SNE1G ----
            SNE1G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE1G.setComponentPopupMenu(BTD);
            SNE1G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE1G.setName("SNE1G");
            xTitledPanel2ContentContainer.add(SNE1G);
            SNE1G.setBounds(85, 10, 160, SNE1G.getPreferredSize().height);

            //---- SNE1D ----
            SNE1D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE1D.setComponentPopupMenu(BTD);
            SNE1D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE1D.setName("SNE1D");
            xTitledPanel2ContentContainer.add(SNE1D);
            SNE1D.setBounds(250, 10, 160, SNE1D.getPreferredSize().height);

            //---- SNE2G ----
            SNE2G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE2G.setComponentPopupMenu(BTD);
            SNE2G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE2G.setName("SNE2G");
            xTitledPanel2ContentContainer.add(SNE2G);
            SNE2G.setBounds(85, 40, 160, SNE2G.getPreferredSize().height);

            //---- SNE2D ----
            SNE2D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE2D.setComponentPopupMenu(BTD);
            SNE2D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE2D.setName("SNE2D");
            xTitledPanel2ContentContainer.add(SNE2D);
            SNE2D.setBounds(250, 40, 160, SNE2D.getPreferredSize().height);

            //---- SNE3G ----
            SNE3G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE3G.setComponentPopupMenu(BTD);
            SNE3G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE3G.setName("SNE3G");
            xTitledPanel2ContentContainer.add(SNE3G);
            SNE3G.setBounds(85, 70, 160, SNE3G.getPreferredSize().height);

            //---- SNE3D ----
            SNE3D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE3D.setComponentPopupMenu(BTD);
            SNE3D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE3D.setName("SNE3D");
            xTitledPanel2ContentContainer.add(SNE3D);
            SNE3D.setBounds(250, 70, 160, SNE3D.getPreferredSize().height);

            //---- SNE4G ----
            SNE4G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE4G.setComponentPopupMenu(BTD);
            SNE4G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE4G.setName("SNE4G");
            xTitledPanel2ContentContainer.add(SNE4G);
            SNE4G.setBounds(85, 100, 160, SNE4G.getPreferredSize().height);

            //---- SNE4D ----
            SNE4D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE4D.setComponentPopupMenu(BTD);
            SNE4D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE4D.setName("SNE4D");
            xTitledPanel2ContentContainer.add(SNE4D);
            SNE4D.setBounds(250, 100, 160, SNE4D.getPreferredSize().height);

            //---- SNE5G ----
            SNE5G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE5G.setComponentPopupMenu(BTD);
            SNE5G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE5G.setName("SNE5G");
            xTitledPanel2ContentContainer.add(SNE5G);
            SNE5G.setBounds(85, 130, 160, SNE5G.getPreferredSize().height);

            //---- SNE5D ----
            SNE5D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE5D.setComponentPopupMenu(BTD);
            SNE5D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE5D.setName("SNE5D");
            xTitledPanel2ContentContainer.add(SNE5D);
            SNE5D.setBounds(250, 130, 160, SNE5D.getPreferredSize().height);

            //---- SNE6G ----
            SNE6G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE6G.setComponentPopupMenu(BTD);
            SNE6G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE6G.setName("SNE6G");
            xTitledPanel2ContentContainer.add(SNE6G);
            SNE6G.setBounds(495, 10, 160, SNE6G.getPreferredSize().height);

            //---- SNE6D ----
            SNE6D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE6D.setComponentPopupMenu(BTD);
            SNE6D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE6D.setName("SNE6D");
            xTitledPanel2ContentContainer.add(SNE6D);
            SNE6D.setBounds(660, 10, 160, SNE6D.getPreferredSize().height);

            //---- SNE7G ----
            SNE7G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE7G.setComponentPopupMenu(BTD);
            SNE7G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE7G.setName("SNE7G");
            xTitledPanel2ContentContainer.add(SNE7G);
            SNE7G.setBounds(495, 40, 160, SNE7G.getPreferredSize().height);

            //---- SNE7D ----
            SNE7D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE7D.setComponentPopupMenu(BTD);
            SNE7D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE7D.setName("SNE7D");
            xTitledPanel2ContentContainer.add(SNE7D);
            SNE7D.setBounds(660, 40, 160, SNE7D.getPreferredSize().height);

            //---- SNE8G ----
            SNE8G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE8G.setComponentPopupMenu(BTD);
            SNE8G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE8G.setName("SNE8G");
            xTitledPanel2ContentContainer.add(SNE8G);
            SNE8G.setBounds(495, 70, 160, SNE8G.getPreferredSize().height);

            //---- SNE8D ----
            SNE8D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE8D.setComponentPopupMenu(BTD);
            SNE8D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE8D.setName("SNE8D");
            xTitledPanel2ContentContainer.add(SNE8D);
            SNE8D.setBounds(660, 70, 160, SNE8D.getPreferredSize().height);

            //---- SNE9G ----
            SNE9G.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE9G.setComponentPopupMenu(BTD);
            SNE9G.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE9G.setName("SNE9G");
            xTitledPanel2ContentContainer.add(SNE9G);
            SNE9G.setBounds(495, 100, 160, SNE9G.getPreferredSize().height);

            //---- SNE9D ----
            SNE9D.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Chiffre d'affaire",
              "CA th\u00e9orique",
              "Marge brute",
              "Pourcentage de marge",
              "Quantit\u00e9 1",
              "Quantit\u00e9 2",
              "CA / prix de revient",
              "Zone personnalis\u00e9e 1",
              "Zone personnalis\u00e9e 2",
              "Zone personnalis\u00e9e 3",
              "Zone personnalis\u00e9e 4"
            }));
            SNE9D.setComponentPopupMenu(BTD);
            SNE9D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SNE9D.setName("SNE9D");
            xTitledPanel2ContentContainer.add(SNE9D);
            SNE9D.setBounds(660, 100, 160, SNE9D.getPreferredSize().height);

            //---- OBJ_97 ----
            OBJ_97.setText("Ecran 1");
            OBJ_97.setName("OBJ_97");
            xTitledPanel2ContentContainer.add(OBJ_97);
            OBJ_97.setBounds(15, 13, 48, 20);

            //---- OBJ_98 ----
            OBJ_98.setText("Ecran 2");
            OBJ_98.setName("OBJ_98");
            xTitledPanel2ContentContainer.add(OBJ_98);
            OBJ_98.setBounds(15, 43, 48, 20);

            //---- OBJ_100 ----
            OBJ_100.setText("Ecran 3");
            OBJ_100.setName("OBJ_100");
            xTitledPanel2ContentContainer.add(OBJ_100);
            OBJ_100.setBounds(15, 73, 48, 20);

            //---- OBJ_101 ----
            OBJ_101.setText("Ecran 4");
            OBJ_101.setName("OBJ_101");
            xTitledPanel2ContentContainer.add(OBJ_101);
            OBJ_101.setBounds(15, 103, 48, 20);

            //---- OBJ_102 ----
            OBJ_102.setText("Ecran 5");
            OBJ_102.setName("OBJ_102");
            xTitledPanel2ContentContainer.add(OBJ_102);
            OBJ_102.setBounds(15, 133, 48, 20);

            //---- OBJ_103 ----
            OBJ_103.setText("Ecran 6");
            OBJ_103.setName("OBJ_103");
            xTitledPanel2ContentContainer.add(OBJ_103);
            OBJ_103.setBounds(440, 13, 48, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("Ecran 7");
            OBJ_104.setName("OBJ_104");
            xTitledPanel2ContentContainer.add(OBJ_104);
            OBJ_104.setBounds(440, 43, 48, 20);

            //---- OBJ_105 ----
            OBJ_105.setText("Ecran 8");
            OBJ_105.setName("OBJ_105");
            xTitledPanel2ContentContainer.add(OBJ_105);
            OBJ_105.setBounds(440, 73, 48, 20);

            //---- OBJ_106 ----
            OBJ_106.setText("Ecran 9");
            OBJ_106.setName("OBJ_106");
            xTitledPanel2ContentContainer.add(OBJ_106);
            OBJ_106.setBounds(440, 103, 48, 20);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 855, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 855, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(21, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 205, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(22, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private RiZoneSortie INDETB;
  private JLabel OBJ_44;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_46;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiComboBox PZ01;
  private XRiComboBox PZ02;
  private XRiComboBox PZ03;
  private XRiComboBox PZ04;
  private XRiComboBox PZ05;
  private XRiComboBox PZ06;
  private XRiComboBox PZ07;
  private JLabel OBJ_85;
  private JLabel OBJ_87;
  private JLabel OBJ_89;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private JLabel OBJ_40;
  private JLabel OBJ_83;
  private XRiComboBox PZ08;
  private XRiComboBox PZ09;
  private XRiComboBox PZ10;
  private XRiComboBox PZ11;
  private JLabel OBJ_84;
  private JLabel OBJ_86;
  private JLabel OBJ_88;
  private JLabel OBJ_90;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox SNE1G;
  private XRiComboBox SNE1D;
  private XRiComboBox SNE2G;
  private XRiComboBox SNE2D;
  private XRiComboBox SNE3G;
  private XRiComboBox SNE3D;
  private XRiComboBox SNE4G;
  private XRiComboBox SNE4D;
  private XRiComboBox SNE5G;
  private XRiComboBox SNE5D;
  private XRiComboBox SNE6G;
  private XRiComboBox SNE6D;
  private XRiComboBox SNE7G;
  private XRiComboBox SNE7D;
  private XRiComboBox SNE8G;
  private XRiComboBox SNE8D;
  private XRiComboBox SNE9G;
  private XRiComboBox SNE9D;
  private JLabel OBJ_97;
  private JLabel OBJ_98;
  private JLabel OBJ_100;
  private JLabel OBJ_101;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private JLabel OBJ_105;
  private JLabel OBJ_106;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
