//$$david$$ ££07/01/11££ -> tests et modifs

package ri.serien.libecranrpg.vgvm.VGVM35FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.comptabilite.snsectionanalytique.SNSectionAnalytique;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGVM35FM_C3 extends SNPanelEcranRPG implements ioFrame {
  
  public VGVM35FM_C3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    RPNSA.setVisible(lexique.isPresent("RPNSA"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FICHE REPRESENTANT"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlSectionAnalytique = new SNPanelTitre();
    pnlSelectionSectionAnalytique = new SNPanel();
    lbSectionAnalytique = new SNLabelChamp();
    snSectionAnalytique = new SNSectionAnalytique();
    pnlPointeurChamps = new SNPanel();
    lbPointeur = new SNLabelChamp();
    pnlPointeur = new SNPanel();
    RPNSA = new XRiTextField();
    pnlChamps = new SNPanel();
    SAN1 = new XRiTextField();
    SAN2 = new XRiTextField();
    SAN3 = new XRiTextField();
    SAN4 = new XRiTextField();
    SAN5 = new XRiTextField();
    SAN6 = new XRiTextField();
    SAN7 = new XRiTextField();
    SAN8 = new XRiTextField();
    SAN9 = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(845, 210));
    setPreferredSize(new Dimension(845, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlSectionAnalytique ========
      {
        pnlSectionAnalytique.setTitre("Section analytique");
        pnlSectionAnalytique.setName("pnlSectionAnalytique");
        pnlSectionAnalytique.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSectionAnalytique.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlSectionAnalytique.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSectionAnalytique.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlSectionAnalytique.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlSelectionSectionAnalytique ========
        {
          pnlSelectionSectionAnalytique.setName("pnlSelectionSectionAnalytique");
          pnlSelectionSectionAnalytique.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlSelectionSectionAnalytique.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlSelectionSectionAnalytique.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlSelectionSectionAnalytique.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlSelectionSectionAnalytique.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbSectionAnalytique ----
          lbSectionAnalytique.setText("Section analytique");
          lbSectionAnalytique.setName("lbSectionAnalytique");
          pnlSelectionSectionAnalytique.add(lbSectionAnalytique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snSectionAnalytique ----
          snSectionAnalytique.setName("snSectionAnalytique");
          pnlSelectionSectionAnalytique.add(snSectionAnalytique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSectionAnalytique.add(pnlSelectionSectionAnalytique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlPointeurChamps ========
        {
          pnlPointeurChamps.setName("pnlPointeurChamps");
          pnlPointeurChamps.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPointeurChamps.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPointeurChamps.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPointeurChamps.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlPointeurChamps.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbPointeur ----
          lbPointeur.setText("ou Pointeur");
          lbPointeur.setName("lbPointeur");
          pnlPointeurChamps.add(lbPointeur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlPointeur ========
          {
            pnlPointeur.setName("pnlPointeur");
            pnlPointeur.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPointeur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPointeur.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPointeur.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlPointeur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- RPNSA ----
            RPNSA.setMaximumSize(new Dimension(20, 30));
            RPNSA.setMinimumSize(new Dimension(20, 30));
            RPNSA.setPreferredSize(new Dimension(20, 30));
            RPNSA.setName("RPNSA");
            pnlPointeur.add(RPNSA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlChamps ========
            {
              pnlChamps.setName("pnlChamps");
              pnlChamps.setLayout(new GridLayout(1, 9, 5, 5));
              
              // ---- SAN1 ----
              SAN1.setMaximumSize(new Dimension(60, 30));
              SAN1.setMinimumSize(new Dimension(60, 30));
              SAN1.setPreferredSize(new Dimension(60, 30));
              SAN1.setName("SAN1");
              pnlChamps.add(SAN1);
              
              // ---- SAN2 ----
              SAN2.setMaximumSize(new Dimension(60, 30));
              SAN2.setMinimumSize(new Dimension(60, 30));
              SAN2.setPreferredSize(new Dimension(60, 30));
              SAN2.setName("SAN2");
              pnlChamps.add(SAN2);
              
              // ---- SAN3 ----
              SAN3.setMaximumSize(new Dimension(60, 30));
              SAN3.setMinimumSize(new Dimension(60, 30));
              SAN3.setPreferredSize(new Dimension(60, 30));
              SAN3.setName("SAN3");
              pnlChamps.add(SAN3);
              
              // ---- SAN4 ----
              SAN4.setMaximumSize(new Dimension(60, 30));
              SAN4.setMinimumSize(new Dimension(60, 30));
              SAN4.setPreferredSize(new Dimension(60, 30));
              SAN4.setName("SAN4");
              pnlChamps.add(SAN4);
              
              // ---- SAN5 ----
              SAN5.setMaximumSize(new Dimension(60, 30));
              SAN5.setMinimumSize(new Dimension(60, 30));
              SAN5.setPreferredSize(new Dimension(60, 30));
              SAN5.setName("SAN5");
              pnlChamps.add(SAN5);
              
              // ---- SAN6 ----
              SAN6.setMaximumSize(new Dimension(60, 30));
              SAN6.setMinimumSize(new Dimension(60, 30));
              SAN6.setPreferredSize(new Dimension(60, 30));
              SAN6.setName("SAN6");
              pnlChamps.add(SAN6);
              
              // ---- SAN7 ----
              SAN7.setMaximumSize(new Dimension(60, 30));
              SAN7.setMinimumSize(new Dimension(60, 30));
              SAN7.setPreferredSize(new Dimension(60, 30));
              SAN7.setName("SAN7");
              pnlChamps.add(SAN7);
              
              // ---- SAN8 ----
              SAN8.setMaximumSize(new Dimension(60, 30));
              SAN8.setMinimumSize(new Dimension(60, 30));
              SAN8.setPreferredSize(new Dimension(60, 30));
              SAN8.setName("SAN8");
              pnlChamps.add(SAN8);
              
              // ---- SAN9 ----
              SAN9.setMaximumSize(new Dimension(60, 30));
              SAN9.setMinimumSize(new Dimension(60, 30));
              SAN9.setPreferredSize(new Dimension(60, 30));
              SAN9.setName("SAN9");
              pnlChamps.add(SAN9);
            }
            pnlPointeur.add(pnlChamps, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPointeurChamps.add(pnlPointeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSectionAnalytique.add(pnlPointeurChamps, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSectionAnalytique);
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlSectionAnalytique;
  private SNPanel pnlSelectionSectionAnalytique;
  private SNLabelChamp lbSectionAnalytique;
  private SNSectionAnalytique snSectionAnalytique;
  private SNPanel pnlPointeurChamps;
  private SNLabelChamp lbPointeur;
  private SNPanel pnlPointeur;
  private XRiTextField RPNSA;
  private SNPanel pnlChamps;
  private XRiTextField SAN1;
  private XRiTextField SAN2;
  private XRiTextField SAN3;
  private XRiTextField SAN4;
  private XRiTextField SAN5;
  private XRiTextField SAN6;
  private XRiTextField SAN7;
  private XRiTextField SAN8;
  private XRiTextField SAN9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
