
package ri.serien.libecranrpg.vgvm.VGVM112F;
// Nom Fichier: pop_VGVM112F_FMTB1_FMTF1_18.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGVM112F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private String[] _LIST1_Top=null;
  private String[] _AR01_Title = { "Code", "Libellé", "Erreur(s)", "Prix Net", };
  private String[][] _AR01_Data = { { "AR01", "LI01", "LDP01", "PV01", }, { "AR02", "LI02", "LDP02", "PV02", },
      { "AR03", "LI03", "LDP03", "PV03", }, { "AR04", "LI04", "LDP04", "PV04", }, };
  private int[] _AR01_Width = { 78, 213, 63, 75, };
  // private String[][] _LIST1_Title_Data_Brut=null;
  
  public VGVM112F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // _LIST1_Title_Data_Brut = initTable(LIST1, "Courier New", Font.BOLD, 12, "Courier New", Font.PLAIN, 12);
    // setDialog(true);
    
    // Ajout
    initDiverses();
    AR01.setAspectTable(null, _AR01_Title, _AR01_Data, _AR01_Width, false, null, null, null, null);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCON@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST1, _LIST1_Title_Data_Brut, _LIST1_Top);
    
    
    EB1DLH.setEnabled(lexique.isPresent("EB1DLH"));
    EB1DLJ.setEnabled(lexique.isPresent("EB1DLJ"));
    E1CTR.setEnabled(lexique.isPresent("E1CTR"));
    E1MEX.setEnabled(lexique.isPresent("E1MEX"));
    WCOLC.setEnabled(lexique.isPresent("WCOLC"));
    E1COL.setEnabled(lexique.isPresent("E1COL"));
    EB1VIG.setEnabled(lexique.isPresent("EB1VIG"));
    EB1NDT.setEnabled(lexique.isPresent("EB1NDT"));
    WPDSC.setEnabled(lexique.isPresent("WPDSC"));
    E1PDS.setEnabled(lexique.isPresent("E1PDS"));
    WVOLC.setEnabled(lexique.isPresent("WVOLC"));
    // EB1DCX.setEnabled( lexique.isPresent("EB1DCX"));
    LDPVEH.setEnabled(lexique.isPresent("LDPVEH"));
    TRLIBR.setEnabled(lexique.isPresent("TRLIBR"));
    EXLIBR.setEnabled(lexique.isPresent("EXLIBR"));
    EB1VCE.setEnabled(lexique.isPresent("EB1VCE"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations de fin de bon"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Touche="ENTER"
    // Scriptcall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vgvm112"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  /*
    private void AR01MouseClicked(MouseEvent e) {
      //lexique.doubleClicSelection(LIST1, _LIST1_Top, "1", "ENTER", e);
      if (AR01.doubleClicSelection(e))
        lexique.HostScreenSendKey(this, "ENTER");
    }
  */
  
  /*
    private void ???ActionPerformed(ActionEvent e) {
      //lexique.validSelection(LIST1, _LIST1_Top, "1", "Enter");
      AR01.setValeurTop("1");
      lexique.HostScreenSendKey(this, "Enter");
    }
  */
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel5 = new JPanel();
    panel4 = new JPanel();
    BT_ENTER = new JButton();
    panel6 = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST1 = new JScrollPane();
    AR01 = new XRiTable();
    panel2 = new JPanel();
    EB1VCE = new XRiTextField();
    OBJ_50 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_45 = new JLabel();
    WVOLC = new XRiTextField();
    E1PDS = new XRiTextField();
    WPDSC = new XRiTextField();
    OBJ_42 = new JLabel();
    EB1VIG = new XRiTextField();
    OBJ_41 = new JLabel();
    E1COL = new XRiTextField();
    WCOLC = new XRiTextField();
    OBJ_39 = new JLabel();
    EB1DCX = new XRiCalendrier();
    panel3 = new JPanel();
    EXLIBR = new XRiTextField();
    TRLIBR = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_51 = new JLabel();
    LDPVEH = new XRiTextField();
    OBJ_44 = new JLabel();
    EB1NDT = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_46 = new JLabel();
    E1MEX = new XRiTextField();
    E1CTR = new XRiTextField();
    EB1DLJ = new XRiTextField();
    EB1DLH = new XRiTextField();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setPreferredSize(new Dimension(540, 480));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel5 ========
    {
      panel5.setName("panel5");
      panel5.setLayout(null);

      //======== panel4 ========
      {
        panel4.setName("panel4");
        panel4.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("<HTML>Attention : toute modification<BR>implique une double validation</HTML>");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_38ActionPerformed(e);
          }
        });
        panel4.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel4.getComponentCount(); i++) {
            Rectangle bounds = panel4.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel4.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel4.setMinimumSize(preferredSize);
          panel4.setPreferredSize(preferredSize);
        }
      }
      panel5.add(panel4);
      panel4.setBounds(470, 0, 65, 50);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel5.getComponentCount(); i++) {
          Rectangle bounds = panel5.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel5.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel5.setMinimumSize(preferredSize);
        panel5.setPreferredSize(preferredSize);
      }
    }
    add(panel5, BorderLayout.SOUTH);

    //======== panel6 ========
    {
      panel6.setName("panel6");
      panel6.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Lignes de pied de bon"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //======== SCROLLPANE_LIST1 ========
        {
          SCROLLPANE_LIST1.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST1.setName("SCROLLPANE_LIST1");

          //---- AR01 ----
          AR01.setName("AR01");
          SCROLLPANE_LIST1.setViewportView(AR01);
        }
        panel1.add(SCROLLPANE_LIST1);
        SCROLLPANE_LIST1.setBounds(20, 35, 485, 95);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel6.add(panel1);
      panel1.setBounds(5, 5, 530, 155);

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder(""));
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- EB1VCE ----
        EB1VCE.setComponentPopupMenu(BTD);
        EB1VCE.setName("EB1VCE");
        panel2.add(EB1VCE);
        EB1VCE.setBounds(110, 66, 310, EB1VCE.getPreferredSize().height);

        //---- OBJ_50 ----
        OBJ_50.setText("Date de retour");
        OBJ_50.setName("OBJ_50");
        panel2.add(OBJ_50);
        OBJ_50.setBounds(235, 100, 88, 20);

        //---- OBJ_49 ----
        OBJ_49.setText("@NCON@");
        OBJ_49.setName("OBJ_49");
        panel2.add(OBJ_49);
        OBJ_49.setBounds(20, 100, 82, 20);

        //---- OBJ_45 ----
        OBJ_45.setText("Sortie CEE");
        OBJ_45.setName("OBJ_45");
        panel2.add(OBJ_45);
        OBJ_45.setBounds(20, 70, 69, 20);

        //---- WVOLC ----
        WVOLC.setComponentPopupMenu(BTD);
        WVOLC.setName("WVOLC");
        panel2.add(WVOLC);
        WVOLC.setBounds(345, 36, 74, WVOLC.getPreferredSize().height);

        //---- E1PDS ----
        E1PDS.setComponentPopupMenu(BTD);
        E1PDS.setName("E1PDS");
        panel2.add(E1PDS);
        E1PDS.setBounds(110, 36, 74, E1PDS.getPreferredSize().height);

        //---- WPDSC ----
        WPDSC.setComponentPopupMenu(BTD);
        WPDSC.setName("WPDSC");
        panel2.add(WPDSC);
        WPDSC.setBounds(195, 36, 74, WPDSC.getPreferredSize().height);

        //---- OBJ_42 ----
        OBJ_42.setText("Volume");
        OBJ_42.setName("OBJ_42");
        panel2.add(OBJ_42);
        OBJ_42.setBounds(280, 40, 49, 20);

        //---- EB1VIG ----
        EB1VIG.setComponentPopupMenu(BTD);
        EB1VIG.setName("EB1VIG");
        panel2.add(EB1VIG);
        EB1VIG.setBounds(110, 96, 58, EB1VIG.getPreferredSize().height);

        //---- OBJ_41 ----
        OBJ_41.setText("Poids");
        OBJ_41.setName("OBJ_41");
        panel2.add(OBJ_41);
        OBJ_41.setBounds(20, 40, 39, 20);

        //---- E1COL ----
        E1COL.setComponentPopupMenu(BTD);
        E1COL.setName("E1COL");
        panel2.add(E1COL);
        E1COL.setBounds(110, 6, 42, E1COL.getPreferredSize().height);

        //---- WCOLC ----
        WCOLC.setComponentPopupMenu(BTD);
        WCOLC.setName("WCOLC");
        panel2.add(WCOLC);
        WCOLC.setBounds(155, 6, 42, WCOLC.getPreferredSize().height);

        //---- OBJ_39 ----
        OBJ_39.setText("Colis");
        OBJ_39.setName("OBJ_39");
        panel2.add(OBJ_39);
        OBJ_39.setBounds(20, 10, 33, 20);

        //---- EB1DCX ----
        EB1DCX.setName("EB1DCX");
        panel2.add(EB1DCX);
        EB1DCX.setBounds(345, 100, 115, EB1DCX.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel6.add(panel2);
      panel2.setBounds(5, 160, 530, 140);

      //======== panel3 ========
      {
        panel3.setBorder(new TitledBorder("Transport"));
        panel3.setName("panel3");
        panel3.setLayout(null);

        //---- EXLIBR ----
        EXLIBR.setName("EXLIBR");
        panel3.add(EXLIBR);
        EXLIBR.setBounds(180, 26, 160, EXLIBR.getPreferredSize().height);

        //---- TRLIBR ----
        TRLIBR.setName("TRLIBR");
        panel3.add(TRLIBR);
        TRLIBR.setBounds(180, 53, 160, TRLIBR.getPreferredSize().height);

        //---- OBJ_40 ----
        OBJ_40.setText("Mode d'exp\u00e9dition");
        OBJ_40.setName("OBJ_40");
        panel3.add(OBJ_40);
        OBJ_40.setBounds(20, 30, 112, 20);

        //---- OBJ_43 ----
        OBJ_43.setText("Code transporteur");
        OBJ_43.setName("OBJ_43");
        panel3.add(OBJ_43);
        OBJ_43.setBounds(20, 57, 111, 20);

        //---- OBJ_51 ----
        OBJ_51.setText("D\u00e9claration");
        OBJ_51.setName("OBJ_51");
        panel3.add(OBJ_51);
        OBJ_51.setBounds(325, 84, 72, 20);

        //---- LDPVEH ----
        LDPVEH.setComponentPopupMenu(BTD);
        LDPVEH.setName("LDPVEH");
        panel3.add(LDPVEH);
        LDPVEH.setBounds(410, 53, 110, LDPVEH.getPreferredSize().height);

        //---- OBJ_44 ----
        OBJ_44.setText("V\u00e9hicule");
        OBJ_44.setName("OBJ_44");
        panel3.add(OBJ_44);
        OBJ_44.setBounds(350, 57, 55, 20);

        //---- EB1NDT ----
        EB1NDT.setComponentPopupMenu(BTD);
        EB1NDT.setName("EB1NDT");
        panel3.add(EB1NDT);
        EB1NDT.setBounds(405, 80, 70, EB1NDT.getPreferredSize().height);

        //---- OBJ_48 ----
        OBJ_48.setText("Heure(s)");
        OBJ_48.setName("OBJ_48");
        panel3.add(OBJ_48);
        OBJ_48.setBounds(180, 84, 52, 20);

        //---- OBJ_47 ----
        OBJ_47.setText("Jour(s)");
        OBJ_47.setName("OBJ_47");
        panel3.add(OBJ_47);
        OBJ_47.setBounds(90, 84, 40, 20);

        //---- OBJ_46 ----
        OBJ_46.setText("D\u00e9lai");
        OBJ_46.setName("OBJ_46");
        panel3.add(OBJ_46);
        OBJ_46.setBounds(20, 84, 36, 20);

        //---- E1MEX ----
        E1MEX.setComponentPopupMenu(BTD);
        E1MEX.setName("E1MEX");
        panel3.add(E1MEX);
        E1MEX.setBounds(145, 26, 30, E1MEX.getPreferredSize().height);

        //---- E1CTR ----
        E1CTR.setComponentPopupMenu(BTD);
        E1CTR.setName("E1CTR");
        panel3.add(E1CTR);
        E1CTR.setBounds(145, 53, 30, E1CTR.getPreferredSize().height);

        //---- EB1DLJ ----
        EB1DLJ.setComponentPopupMenu(BTD);
        EB1DLJ.setName("EB1DLJ");
        panel3.add(EB1DLJ);
        EB1DLJ.setBounds(65, 80, 26, EB1DLJ.getPreferredSize().height);

        //---- EB1DLH ----
        EB1DLH.setComponentPopupMenu(BTD);
        EB1DLH.setName("EB1DLH");
        panel3.add(EB1DLH);
        EB1DLH.setBounds(145, 80, 26, EB1DLH.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel3.getComponentCount(); i++) {
            Rectangle bounds = panel3.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel3.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel3.setMinimumSize(preferredSize);
          panel3.setPreferredSize(preferredSize);
        }
      }
      panel6.add(panel3);
      panel3.setBounds(5, 300, 530, 125);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel6.getComponentCount(); i++) {
          Rectangle bounds = panel6.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel6.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel6.setMinimumSize(preferredSize);
        panel6.setPreferredSize(preferredSize);
      }
    }
    add(panel6, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Exploitation");
      OBJ_7.setName("OBJ_7");
      OBJ_4.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Choix possibles");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel5;
  private JPanel panel4;
  private JButton BT_ENTER;
  private JPanel panel6;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST1;
  private XRiTable AR01;
  private JPanel panel2;
  private XRiTextField EB1VCE;
  private JLabel OBJ_50;
  private JLabel OBJ_49;
  private JLabel OBJ_45;
  private XRiTextField WVOLC;
  private XRiTextField E1PDS;
  private XRiTextField WPDSC;
  private JLabel OBJ_42;
  private XRiTextField EB1VIG;
  private JLabel OBJ_41;
  private XRiTextField E1COL;
  private XRiTextField WCOLC;
  private JLabel OBJ_39;
  private XRiCalendrier EB1DCX;
  private JPanel panel3;
  private XRiTextField EXLIBR;
  private XRiTextField TRLIBR;
  private JLabel OBJ_40;
  private JLabel OBJ_43;
  private JLabel OBJ_51;
  private XRiTextField LDPVEH;
  private JLabel OBJ_44;
  private XRiTextField EB1NDT;
  private JLabel OBJ_48;
  private JLabel OBJ_47;
  private JLabel OBJ_46;
  private XRiTextField E1MEX;
  private XRiTextField E1CTR;
  private XRiTextField EB1DLJ;
  private XRiTextField EB1DLH;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
