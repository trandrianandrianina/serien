
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_VT extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_VT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    VTRIN1.setValeursSelection("1", " ");
    VTRRPC.setValeurs("C", VTRRPC_GRP);
    VTRRPC_QTE.setValeurs("Q");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ de @LOCGRP/-1/@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel3 = new JXTitledPanel();
    panel2 = new JPanel();
    OBJ_55 = new JLabel();
    VTLIB = new XRiTextField();
    OBJ_56 = new JTabbedPane();
    OBJ_57 = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    panel1 = new JPanel();
    OBJ_63 = new JLabel();
    OBJ_61 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_67 = new JLabel();
    VTMS1 = new XRiTextField();
    WDU1 = new XRiTextField();
    WAU1 = new XRiTextField();
    WE11 = new XRiTextField();
    WE12 = new XRiTextField();
    WE13 = new XRiTextField();
    OBJ_71 = new JLabel();
    VTM11 = new XRiTextField();
    VTP11 = new XRiTextField();
    VTM12 = new XRiTextField();
    VTP12 = new XRiTextField();
    VTM13 = new XRiTextField();
    VTP13 = new XRiTextField();
    OBJ_65 = new JLabel();
    panel3 = new JPanel();
    OBJ_82 = new JLabel();
    OBJ_98 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_90 = new JLabel();
    VTMS2 = new XRiTextField();
    WDU2 = new XRiTextField();
    WAU2 = new XRiTextField();
    WE21 = new XRiTextField();
    WE22 = new XRiTextField();
    WE23 = new XRiTextField();
    OBJ_84 = new JLabel();
    OBJ_89 = new JLabel();
    VTM21 = new XRiTextField();
    VTP21 = new XRiTextField();
    VTM22 = new XRiTextField();
    VTP22 = new XRiTextField();
    VTM23 = new XRiTextField();
    VTP23 = new XRiTextField();
    OBJ_99 = new JPanel();
    xTitledPanel6 = new JXTitledPanel();
    OBJ_103 = new JLabel();
    OBJ_115 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_108 = new JLabel();
    OBJ_117 = new JLabel();
    VTRIN1 = new XRiCheckBox();
    OBJ_119 = new JLabel();
    VTRRPC_QTE = new XRiRadioButton();
    VTRRPC = new XRiRadioButton();
    VTRPC = new XRiTextField();
    VTRMO1 = new XRiTextField();
    VTRMO3 = new XRiTextField();
    OBJ_104 = new JLabel();
    OBJ_106 = new JLabel();
    OBJ_111 = new JLabel();
    OBJ_113 = new JLabel();
    VTRD1X = new XRiCalendrier();
    VTRD2X = new XRiCalendrier();
    VTRD3X = new XRiCalendrier();
    VTRD4X = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    VTRRPC_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(640, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //======== panel2 ========
            {
              panel2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- OBJ_55 ----
              OBJ_55.setText("Libell\u00e9");
              OBJ_55.setName("OBJ_55");
              panel2.add(OBJ_55);
              OBJ_55.setBounds(15, 16, 46, 17);

              //---- VTLIB ----
              VTLIB.setComponentPopupMenu(BTD);
              VTLIB.setName("VTLIB");
              panel2.add(VTLIB);
              VTLIB.setBounds(75, 10, 310, VTLIB.getPreferredSize().height);
            }
            xTitledPanel3ContentContainer.add(panel2);
            panel2.setBounds(15, 10, 408, 50);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== OBJ_56 ========
          {
            OBJ_56.setName("OBJ_56");

            //======== OBJ_57 ========
            {
              OBJ_57.setOpaque(false);
              OBJ_57.setName("OBJ_57");
              OBJ_57.setLayout(null);

              //======== xTitledPanel4 ========
              {
                xTitledPanel4.setTitle("Modes de r\u00e8glement et \u00e9ch\u00e9ances pour ce type de vente");
                xTitledPanel4.setBorder(new DropShadowBorder());
                xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
                xTitledPanel4.setName("xTitledPanel4");
                Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
                xTitledPanel4ContentContainer.setLayout(null);

                //======== panel1 ========
                {
                  panel1.setBorder(new TitledBorder(""));
                  panel1.setOpaque(false);
                  panel1.setName("panel1");
                  panel1.setLayout(null);

                  //---- OBJ_63 ----
                  OBJ_63.setText("P\u00e9riode de commande : du");
                  OBJ_63.setName("OBJ_63");
                  panel1.add(OBJ_63);
                  OBJ_63.setBounds(10, 44, 165, 20);

                  //---- OBJ_61 ----
                  OBJ_61.setText("Montant sup\u00e9rieur \u00e0");
                  OBJ_61.setName("OBJ_61");
                  panel1.add(OBJ_61);
                  OBJ_61.setBounds(40, 14, 138, 20);

                  //---- OBJ_72 ----
                  OBJ_72.setText("Pourcentage");
                  OBJ_72.setName("OBJ_72");
                  panel1.add(OBJ_72);
                  OBJ_72.setBounds(270, 75, 80, 18);

                  //---- OBJ_67 ----
                  OBJ_67.setText("Ech\u00e9ance");
                  OBJ_67.setName("OBJ_67");
                  panel1.add(OBJ_67);
                  OBJ_67.setBounds(130, 75, 64, 18);

                  //---- VTMS1 ----
                  VTMS1.setComponentPopupMenu(BTD);
                  VTMS1.setName("VTMS1");
                  panel1.add(VTMS1);
                  VTMS1.setBounds(200, 10, 66, VTMS1.getPreferredSize().height);

                  //---- WDU1 ----
                  WDU1.setComponentPopupMenu(BTD);
                  WDU1.setHorizontalAlignment(SwingConstants.CENTER);
                  WDU1.setName("WDU1");
                  panel1.add(WDU1);
                  WDU1.setBounds(200, 40, 52, WDU1.getPreferredSize().height);

                  //---- WAU1 ----
                  WAU1.setComponentPopupMenu(BTD);
                  WAU1.setHorizontalAlignment(SwingConstants.CENTER);
                  WAU1.setName("WAU1");
                  panel1.add(WAU1);
                  WAU1.setBounds(335, 40, 52, WAU1.getPreferredSize().height);

                  //---- WE11 ----
                  WE11.setToolTipText("jj.mm");
                  WE11.setComponentPopupMenu(BTD);
                  WE11.setHorizontalAlignment(SwingConstants.CENTER);
                  WE11.setName("WE11");
                  panel1.add(WE11);
                  WE11.setBounds(200, 70, 52, WE11.getPreferredSize().height);

                  //---- WE12 ----
                  WE12.setToolTipText("jj.mm");
                  WE12.setComponentPopupMenu(BTD);
                  WE12.setHorizontalAlignment(SwingConstants.CENTER);
                  WE12.setName("WE12");
                  panel1.add(WE12);
                  WE12.setBounds(200, 100, 52, WE12.getPreferredSize().height);

                  //---- WE13 ----
                  WE13.setToolTipText("jj.mm");
                  WE13.setComponentPopupMenu(BTD);
                  WE13.setHorizontalAlignment(SwingConstants.CENTER);
                  WE13.setName("WE13");
                  panel1.add(WE13);
                  WE13.setBounds(200, 130, 52, WE13.getPreferredSize().height);

                  //---- OBJ_71 ----
                  OBJ_71.setText("Mode");
                  OBJ_71.setName("OBJ_71");
                  panel1.add(OBJ_71);
                  OBJ_71.setBounds(40, 75, 38, 18);

                  //---- VTM11 ----
                  VTM11.setComponentPopupMenu(BTD);
                  VTM11.setName("VTM11");
                  panel1.add(VTM11);
                  VTM11.setBounds(95, 70, 30, VTM11.getPreferredSize().height);

                  //---- VTP11 ----
                  VTP11.setComponentPopupMenu(BTD);
                  VTP11.setName("VTP11");
                  panel1.add(VTP11);
                  VTP11.setBounds(357, 70, 30, VTP11.getPreferredSize().height);

                  //---- VTM12 ----
                  VTM12.setComponentPopupMenu(BTD);
                  VTM12.setName("VTM12");
                  panel1.add(VTM12);
                  VTM12.setBounds(95, 100, 30, VTM12.getPreferredSize().height);

                  //---- VTP12 ----
                  VTP12.setComponentPopupMenu(BTD);
                  VTP12.setName("VTP12");
                  panel1.add(VTP12);
                  VTP12.setBounds(357, 100, 30, VTP12.getPreferredSize().height);

                  //---- VTM13 ----
                  VTM13.setComponentPopupMenu(BTD);
                  VTM13.setName("VTM13");
                  panel1.add(VTM13);
                  VTM13.setBounds(95, 130, 30, VTM13.getPreferredSize().height);

                  //---- VTP13 ----
                  VTP13.setComponentPopupMenu(BTD);
                  VTP13.setName("VTP13");
                  panel1.add(VTP13);
                  VTP13.setBounds(357, 130, 30, VTP13.getPreferredSize().height);

                  //---- OBJ_65 ----
                  OBJ_65.setText("au");
                  OBJ_65.setName("OBJ_65");
                  panel1.add(OBJ_65);
                  OBJ_65.setBounds(270, 44, 18, 20);
                }
                xTitledPanel4ContentContainer.add(panel1);
                panel1.setBounds(60, 5, 410, 165);

                //======== panel3 ========
                {
                  panel3.setBorder(new TitledBorder(""));
                  panel3.setOpaque(false);
                  panel3.setName("panel3");
                  panel3.setLayout(null);

                  //---- OBJ_82 ----
                  OBJ_82.setText("P\u00e9riode de commande : du");
                  OBJ_82.setName("OBJ_82");
                  panel3.add(OBJ_82);
                  OBJ_82.setBounds(10, 44, 165, 20);

                  //---- OBJ_98 ----
                  OBJ_98.setText("Montant sup\u00e9rieur \u00e0");
                  OBJ_98.setName("OBJ_98");
                  panel3.add(OBJ_98);
                  OBJ_98.setBounds(35, 14, 138, 20);

                  //---- OBJ_91 ----
                  OBJ_91.setText("Pourcentage");
                  OBJ_91.setName("OBJ_91");
                  panel3.add(OBJ_91);
                  OBJ_91.setBounds(265, 75, 80, 18);

                  //---- OBJ_90 ----
                  OBJ_90.setText("Ech\u00e9ance");
                  OBJ_90.setName("OBJ_90");
                  panel3.add(OBJ_90);
                  OBJ_90.setBounds(125, 75, 64, 18);

                  //---- VTMS2 ----
                  VTMS2.setComponentPopupMenu(BTD);
                  VTMS2.setName("VTMS2");
                  panel3.add(VTMS2);
                  VTMS2.setBounds(195, 10, 66, VTMS2.getPreferredSize().height);

                  //---- WDU2 ----
                  WDU2.setComponentPopupMenu(BTD);
                  WDU2.setHorizontalAlignment(SwingConstants.CENTER);
                  WDU2.setName("WDU2");
                  panel3.add(WDU2);
                  WDU2.setBounds(195, 40, 52, WDU2.getPreferredSize().height);

                  //---- WAU2 ----
                  WAU2.setComponentPopupMenu(BTD);
                  WAU2.setHorizontalAlignment(SwingConstants.CENTER);
                  WAU2.setName("WAU2");
                  panel3.add(WAU2);
                  WAU2.setBounds(330, 40, 52, WAU2.getPreferredSize().height);

                  //---- WE21 ----
                  WE21.setToolTipText("jj.mm");
                  WE21.setComponentPopupMenu(BTD);
                  WE21.setHorizontalAlignment(SwingConstants.CENTER);
                  WE21.setName("WE21");
                  panel3.add(WE21);
                  WE21.setBounds(195, 70, 52, WE21.getPreferredSize().height);

                  //---- WE22 ----
                  WE22.setToolTipText("jj.mm");
                  WE22.setComponentPopupMenu(BTD);
                  WE22.setHorizontalAlignment(SwingConstants.CENTER);
                  WE22.setName("WE22");
                  panel3.add(WE22);
                  WE22.setBounds(195, 100, 52, WE22.getPreferredSize().height);

                  //---- WE23 ----
                  WE23.setToolTipText("jj.mm");
                  WE23.setComponentPopupMenu(BTD);
                  WE23.setHorizontalAlignment(SwingConstants.CENTER);
                  WE23.setName("WE23");
                  panel3.add(WE23);
                  WE23.setBounds(195, 130, 52, WE23.getPreferredSize().height);

                  //---- OBJ_84 ----
                  OBJ_84.setText("au");
                  OBJ_84.setName("OBJ_84");
                  panel3.add(OBJ_84);
                  OBJ_84.setBounds(265, 44, 36, 20);

                  //---- OBJ_89 ----
                  OBJ_89.setText("Mode");
                  OBJ_89.setName("OBJ_89");
                  panel3.add(OBJ_89);
                  OBJ_89.setBounds(35, 75, 38, 18);

                  //---- VTM21 ----
                  VTM21.setComponentPopupMenu(BTD);
                  VTM21.setName("VTM21");
                  panel3.add(VTM21);
                  VTM21.setBounds(90, 70, 30, VTM21.getPreferredSize().height);

                  //---- VTP21 ----
                  VTP21.setComponentPopupMenu(BTD);
                  VTP21.setName("VTP21");
                  panel3.add(VTP21);
                  VTP21.setBounds(352, 70, 30, VTP21.getPreferredSize().height);

                  //---- VTM22 ----
                  VTM22.setComponentPopupMenu(BTD);
                  VTM22.setName("VTM22");
                  panel3.add(VTM22);
                  VTM22.setBounds(90, 100, 30, VTM22.getPreferredSize().height);

                  //---- VTP22 ----
                  VTP22.setComponentPopupMenu(BTD);
                  VTP22.setName("VTP22");
                  panel3.add(VTP22);
                  VTP22.setBounds(352, 100, 30, VTP22.getPreferredSize().height);

                  //---- VTM23 ----
                  VTM23.setComponentPopupMenu(BTD);
                  VTM23.setName("VTM23");
                  panel3.add(VTM23);
                  VTM23.setBounds(90, 130, 30, VTM23.getPreferredSize().height);

                  //---- VTP23 ----
                  VTP23.setComponentPopupMenu(BTD);
                  VTP23.setName("VTP23");
                  panel3.add(VTP23);
                  VTP23.setBounds(352, 130, 30, VTP23.getPreferredSize().height);
                }
                xTitledPanel4ContentContainer.add(panel3);
                panel3.setBounds(60, 175, 410, 165);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel4ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
                }
              }
              OBJ_57.add(xTitledPanel4);
              xTitledPanel4.setBounds(10, 10, 580, 370);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_57.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_57.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_57.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_57.setMinimumSize(preferredSize);
                OBJ_57.setPreferredSize(preferredSize);
              }
            }
            OBJ_56.addTab("R\u00e9glements \u00e9ch\u00e9ances", OBJ_57);

            //======== OBJ_99 ========
            {
              OBJ_99.setOpaque(false);
              OBJ_99.setName("OBJ_99");
              OBJ_99.setLayout(null);

              //======== xTitledPanel6 ========
              {
                xTitledPanel6.setTitle("Modalit\u00e9s de retour");
                xTitledPanel6.setBorder(new DropShadowBorder());
                xTitledPanel6.setTitleFont(new Font("sansserif", Font.BOLD, 12));
                xTitledPanel6.setName("xTitledPanel6");
                Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();
                xTitledPanel6ContentContainer.setLayout(null);

                //---- OBJ_103 ----
                OBJ_103.setText("P\u00e9riode vente ouvrant droit \u00e0 retour");
                OBJ_103.setName("OBJ_103");
                xTitledPanel6ContentContainer.add(OBJ_103);
                OBJ_103.setBounds(15, 14, 229, 20);

                //---- OBJ_115 ----
                OBJ_115.setText("ou Nombre mois apr\u00e8s 1\u00e8re facture");
                OBJ_115.setName("OBJ_115");
                xTitledPanel6ContentContainer.add(OBJ_115);
                OBJ_115.setBounds(270, 134, 213, 20);

                //---- OBJ_110 ----
                OBJ_110.setText("P\u00e9riode de retour autoris\u00e9e");
                OBJ_110.setName("OBJ_110");
                xTitledPanel6ContentContainer.add(OBJ_110);
                OBJ_110.setBounds(15, 94, 166, 20);

                //---- OBJ_108 ----
                OBJ_108.setText("ou Nombre mois de garde");
                OBJ_108.setName("OBJ_108");
                xTitledPanel6ContentContainer.add(OBJ_108);
                OBJ_108.setBounds(270, 54, 160, 20);

                //---- OBJ_117 ----
                OBJ_117.setText("% retour autoris\u00e9");
                OBJ_117.setName("OBJ_117");
                xTitledPanel6ContentContainer.add(OBJ_117);
                OBJ_117.setBounds(15, 174, 103, 20);

                //---- VTRIN1 ----
                VTRIN1.setText("En global");
                VTRIN1.setComponentPopupMenu(BTD);
                VTRIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                VTRIN1.setName("VTRIN1");
                xTitledPanel6ContentContainer.add(VTRIN1);
                VTRIN1.setBounds(430, 174, 82, 20);

                //---- OBJ_119 ----
                OBJ_119.setText("par rapport");
                OBJ_119.setFont(new Font("sansserif", Font.BOLD, 12));
                OBJ_119.setName("OBJ_119");
                xTitledPanel6ContentContainer.add(OBJ_119);
                OBJ_119.setBounds(185, 174, 74, 20);

                //---- VTRRPC_QTE ----
                VTRRPC_QTE.setText("Quantit\u00e9");
                VTRRPC_QTE.setComponentPopupMenu(BTD);
                VTRRPC_QTE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                VTRRPC_QTE.setName("VTRRPC_QTE");
                xTitledPanel6ContentContainer.add(VTRRPC_QTE);
                VTRRPC_QTE.setBounds(325, 174, 74, 20);

                //---- VTRRPC ----
                VTRRPC.setText("CAF");
                VTRRPC.setComponentPopupMenu(BTD);
                VTRRPC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                VTRRPC.setName("VTRRPC");
                xTitledPanel6ContentContainer.add(VTRRPC);
                VTRRPC.setBounds(265, 174, 51, 20);

                //---- VTRPC ----
                VTRPC.setComponentPopupMenu(BTD);
                VTRPC.setName("VTRPC");
                xTitledPanel6ContentContainer.add(VTRPC);
                VTRPC.setBounds(125, 170, 50, VTRPC.getPreferredSize().height);

                //---- VTRMO1 ----
                VTRMO1.setComponentPopupMenu(BTD);
                VTRMO1.setName("VTRMO1");
                xTitledPanel6ContentContainer.add(VTRMO1);
                VTRMO1.setBounds(492, 50, 26, VTRMO1.getPreferredSize().height);

                //---- VTRMO3 ----
                VTRMO3.setComponentPopupMenu(BTD);
                VTRMO3.setName("VTRMO3");
                xTitledPanel6ContentContainer.add(VTRMO3);
                VTRMO3.setBounds(492, 130, 26, VTRMO3.getPreferredSize().height);

                //---- OBJ_104 ----
                OBJ_104.setText("du");
                OBJ_104.setName("OBJ_104");
                xTitledPanel6ContentContainer.add(OBJ_104);
                OBJ_104.setBounds(275, 14, 18, 20);

                //---- OBJ_106 ----
                OBJ_106.setText("au");
                OBJ_106.setName("OBJ_106");
                xTitledPanel6ContentContainer.add(OBJ_106);
                OBJ_106.setBounds(420, 14, 18, 20);

                //---- OBJ_111 ----
                OBJ_111.setText("du");
                OBJ_111.setName("OBJ_111");
                xTitledPanel6ContentContainer.add(OBJ_111);
                OBJ_111.setBounds(275, 94, 18, 20);

                //---- OBJ_113 ----
                OBJ_113.setText("au");
                OBJ_113.setName("OBJ_113");
                xTitledPanel6ContentContainer.add(OBJ_113);
                OBJ_113.setBounds(420, 94, 18, 20);

                //---- VTRD1X ----
                VTRD1X.setName("VTRD1X");
                xTitledPanel6ContentContainer.add(VTRD1X);
                VTRD1X.setBounds(295, 10, 115, VTRD1X.getPreferredSize().height);

                //---- VTRD2X ----
                VTRD2X.setName("VTRD2X");
                xTitledPanel6ContentContainer.add(VTRD2X);
                VTRD2X.setBounds(445, 10, 115, VTRD2X.getPreferredSize().height);

                //---- VTRD3X ----
                VTRD3X.setName("VTRD3X");
                xTitledPanel6ContentContainer.add(VTRD3X);
                VTRD3X.setBounds(295, 90, 115, VTRD3X.getPreferredSize().height);

                //---- VTRD4X ----
                VTRD4X.setName("VTRD4X");
                xTitledPanel6ContentContainer.add(VTRD4X);
                VTRD4X.setBounds(445, 90, 115, VTRD4X.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < xTitledPanel6ContentContainer.getComponentCount(); i++) {
                    Rectangle bounds = xTitledPanel6ContentContainer.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = xTitledPanel6ContentContainer.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  xTitledPanel6ContentContainer.setMinimumSize(preferredSize);
                  xTitledPanel6ContentContainer.setPreferredSize(preferredSize);
                }
              }
              OBJ_99.add(xTitledPanel6);
              xTitledPanel6.setBounds(10, 40, 580, 250);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < OBJ_99.getComponentCount(); i++) {
                  Rectangle bounds = OBJ_99.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = OBJ_99.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                OBJ_99.setMinimumSize(preferredSize);
                OBJ_99.setPreferredSize(preferredSize);
              }
            }
            OBJ_56.addTab("Modalit\u00e9s retour", OBJ_99);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                  .addComponent(xTitledPanel3, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(OBJ_56, GroupLayout.Alignment.LEADING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(25, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(26, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Invite");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //---- VTRRPC_GRP ----
    VTRRPC_GRP.add(VTRRPC_QTE);
    VTRRPC_GRP.add(VTRRPC);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel3;
  private JPanel panel2;
  private JLabel OBJ_55;
  private XRiTextField VTLIB;
  private JTabbedPane OBJ_56;
  private JPanel OBJ_57;
  private JXTitledPanel xTitledPanel4;
  private JPanel panel1;
  private JLabel OBJ_63;
  private JLabel OBJ_61;
  private JLabel OBJ_72;
  private JLabel OBJ_67;
  private XRiTextField VTMS1;
  private XRiTextField WDU1;
  private XRiTextField WAU1;
  private XRiTextField WE11;
  private XRiTextField WE12;
  private XRiTextField WE13;
  private JLabel OBJ_71;
  private XRiTextField VTM11;
  private XRiTextField VTP11;
  private XRiTextField VTM12;
  private XRiTextField VTP12;
  private XRiTextField VTM13;
  private XRiTextField VTP13;
  private JLabel OBJ_65;
  private JPanel panel3;
  private JLabel OBJ_82;
  private JLabel OBJ_98;
  private JLabel OBJ_91;
  private JLabel OBJ_90;
  private XRiTextField VTMS2;
  private XRiTextField WDU2;
  private XRiTextField WAU2;
  private XRiTextField WE21;
  private XRiTextField WE22;
  private XRiTextField WE23;
  private JLabel OBJ_84;
  private JLabel OBJ_89;
  private XRiTextField VTM21;
  private XRiTextField VTP21;
  private XRiTextField VTM22;
  private XRiTextField VTP22;
  private XRiTextField VTM23;
  private XRiTextField VTP23;
  private JPanel OBJ_99;
  private JXTitledPanel xTitledPanel6;
  private JLabel OBJ_103;
  private JLabel OBJ_115;
  private JLabel OBJ_110;
  private JLabel OBJ_108;
  private JLabel OBJ_117;
  private XRiCheckBox VTRIN1;
  private JLabel OBJ_119;
  private XRiRadioButton VTRRPC_QTE;
  private XRiRadioButton VTRRPC;
  private XRiTextField VTRPC;
  private XRiTextField VTRMO1;
  private XRiTextField VTRMO3;
  private JLabel OBJ_104;
  private JLabel OBJ_106;
  private JLabel OBJ_111;
  private JLabel OBJ_113;
  private XRiCalendrier VTRD1X;
  private XRiCalendrier VTRD2X;
  private XRiCalendrier VTRD3X;
  private XRiCalendrier VTRD4X;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private ButtonGroup VTRRPC_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
