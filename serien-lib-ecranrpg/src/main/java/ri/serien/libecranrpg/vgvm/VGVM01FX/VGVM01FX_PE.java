
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_PE extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_PE(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDTYP@")).trim());
    INDIND.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDIND@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    INDETB = new RiZoneSortie();
    OBJ_50 = new JLabel();
    INDTYP = new RiZoneSortie();
    OBJ_51 = new JLabel();
    INDIND = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_49 = new JLabel();
    OBJ_37 = new JLabel();
    PEART = new XRiTextField();
    PEF01 = new XRiTextField();
    PEF02 = new XRiTextField();
    PEF03 = new XRiTextField();
    PEF04 = new XRiTextField();
    PEF05 = new XRiTextField();
    PEF06 = new XRiTextField();
    PEF07 = new XRiTextField();
    PEF08 = new XRiTextField();
    PEF09 = new XRiTextField();
    PEF10 = new XRiTextField();
    OBJ_35 = new JLabel();
    PES01 = new XRiTextField();
    PEP01 = new XRiTextField();
    PES02 = new XRiTextField();
    PEP02 = new XRiTextField();
    PES03 = new XRiTextField();
    PEP03 = new XRiTextField();
    PES04 = new XRiTextField();
    PEP04 = new XRiTextField();
    PES05 = new XRiTextField();
    PEP05 = new XRiTextField();
    PES06 = new XRiTextField();
    PEP06 = new XRiTextField();
    PES07 = new XRiTextField();
    PEP07 = new XRiTextField();
    PES08 = new XRiTextField();
    PEP08 = new XRiTextField();
    PES09 = new XRiTextField();
    PEP09 = new XRiTextField();
    PES10 = new XRiTextField();
    PEP10 = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_45 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_36 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setText("@INDETB@");
          INDETB.setOpaque(false);
          INDETB.setName("INDETB");

          //---- OBJ_50 ----
          OBJ_50.setText("Code");
          OBJ_50.setName("OBJ_50");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(null);
          INDTYP.setText("@INDTYP@");
          INDTYP.setOpaque(false);
          INDTYP.setName("INDTYP");

          //---- OBJ_51 ----
          OBJ_51.setText("Ordre");
          OBJ_51.setName("OBJ_51");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(null);
          INDIND.setText("@INDIND@");
          INDIND.setOpaque(false);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(90, 90, 90)
                    .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(25, 25, 25)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(500, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Participation frais d'exp\u00e9dition");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- OBJ_49 ----
            OBJ_49.setText("Code article associ\u00e9");
            OBJ_49.setName("OBJ_49");
            xTitledPanel4ContentContainer.add(OBJ_49);
            OBJ_49.setBounds(30, 414, 145, 20);

            //---- OBJ_37 ----
            OBJ_37.setText("Montant fixe");
            OBJ_37.setFont(OBJ_37.getFont().deriveFont(OBJ_37.getFont().getStyle() | Font.BOLD));
            OBJ_37.setName("OBJ_37");
            xTitledPanel4ContentContainer.add(OBJ_37);
            OBJ_37.setBounds(258, 25, 70, 20);

            //---- PEART ----
            PEART.setComponentPopupMenu(BTD);
            PEART.setName("PEART");
            xTitledPanel4ContentContainer.add(PEART);
            PEART.setBounds(175, 410, 110, PEART.getPreferredSize().height);

            //---- PEF01 ----
            PEF01.setComponentPopupMenu(BTD);
            PEF01.setName("PEF01");
            xTitledPanel4ContentContainer.add(PEF01);
            PEF01.setBounds(260, 45, 66, PEF01.getPreferredSize().height);

            //---- PEF02 ----
            PEF02.setComponentPopupMenu(BTD);
            PEF02.setName("PEF02");
            xTitledPanel4ContentContainer.add(PEF02);
            PEF02.setBounds(260, 80, 66, PEF02.getPreferredSize().height);

            //---- PEF03 ----
            PEF03.setComponentPopupMenu(BTD);
            PEF03.setName("PEF03");
            xTitledPanel4ContentContainer.add(PEF03);
            PEF03.setBounds(260, 115, 66, PEF03.getPreferredSize().height);

            //---- PEF04 ----
            PEF04.setComponentPopupMenu(BTD);
            PEF04.setName("PEF04");
            xTitledPanel4ContentContainer.add(PEF04);
            PEF04.setBounds(260, 150, 66, PEF04.getPreferredSize().height);

            //---- PEF05 ----
            PEF05.setComponentPopupMenu(BTD);
            PEF05.setName("PEF05");
            xTitledPanel4ContentContainer.add(PEF05);
            PEF05.setBounds(260, 185, 66, PEF05.getPreferredSize().height);

            //---- PEF06 ----
            PEF06.setComponentPopupMenu(BTD);
            PEF06.setName("PEF06");
            xTitledPanel4ContentContainer.add(PEF06);
            PEF06.setBounds(260, 220, 66, PEF06.getPreferredSize().height);

            //---- PEF07 ----
            PEF07.setComponentPopupMenu(BTD);
            PEF07.setName("PEF07");
            xTitledPanel4ContentContainer.add(PEF07);
            PEF07.setBounds(260, 255, 66, PEF07.getPreferredSize().height);

            //---- PEF08 ----
            PEF08.setComponentPopupMenu(BTD);
            PEF08.setName("PEF08");
            xTitledPanel4ContentContainer.add(PEF08);
            PEF08.setBounds(260, 290, 66, PEF08.getPreferredSize().height);

            //---- PEF09 ----
            PEF09.setComponentPopupMenu(BTD);
            PEF09.setName("PEF09");
            xTitledPanel4ContentContainer.add(PEF09);
            PEF09.setBounds(260, 325, 66, PEF09.getPreferredSize().height);

            //---- PEF10 ----
            PEF10.setComponentPopupMenu(BTD);
            PEF10.setName("PEF10");
            xTitledPanel4ContentContainer.add(PEF10);
            PEF10.setBounds(260, 360, 66, PEF10.getPreferredSize().height);

            //---- OBJ_35 ----
            OBJ_35.setText("Seuil");
            OBJ_35.setFont(OBJ_35.getFont().deriveFont(OBJ_35.getFont().getStyle() | Font.BOLD));
            OBJ_35.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_35.setName("OBJ_35");
            xTitledPanel4ContentContainer.add(OBJ_35);
            OBJ_35.setBounds(85, 25, 50, 20);

            //---- PES01 ----
            PES01.setComponentPopupMenu(BTD);
            PES01.setName("PES01");
            xTitledPanel4ContentContainer.add(PES01);
            PES01.setBounds(85, 45, 50, PES01.getPreferredSize().height);

            //---- PEP01 ----
            PEP01.setComponentPopupMenu(BTD);
            PEP01.setName("PEP01");
            xTitledPanel4ContentContainer.add(PEP01);
            PEP01.setBounds(175, 45, 50, PEP01.getPreferredSize().height);

            //---- PES02 ----
            PES02.setComponentPopupMenu(BTD);
            PES02.setName("PES02");
            xTitledPanel4ContentContainer.add(PES02);
            PES02.setBounds(85, 80, 50, PES02.getPreferredSize().height);

            //---- PEP02 ----
            PEP02.setComponentPopupMenu(BTD);
            PEP02.setName("PEP02");
            xTitledPanel4ContentContainer.add(PEP02);
            PEP02.setBounds(175, 80, 50, PEP02.getPreferredSize().height);

            //---- PES03 ----
            PES03.setComponentPopupMenu(BTD);
            PES03.setName("PES03");
            xTitledPanel4ContentContainer.add(PES03);
            PES03.setBounds(85, 115, 50, PES03.getPreferredSize().height);

            //---- PEP03 ----
            PEP03.setComponentPopupMenu(BTD);
            PEP03.setName("PEP03");
            xTitledPanel4ContentContainer.add(PEP03);
            PEP03.setBounds(175, 115, 50, PEP03.getPreferredSize().height);

            //---- PES04 ----
            PES04.setComponentPopupMenu(BTD);
            PES04.setName("PES04");
            xTitledPanel4ContentContainer.add(PES04);
            PES04.setBounds(85, 150, 50, PES04.getPreferredSize().height);

            //---- PEP04 ----
            PEP04.setComponentPopupMenu(BTD);
            PEP04.setName("PEP04");
            xTitledPanel4ContentContainer.add(PEP04);
            PEP04.setBounds(175, 150, 50, PEP04.getPreferredSize().height);

            //---- PES05 ----
            PES05.setComponentPopupMenu(BTD);
            PES05.setName("PES05");
            xTitledPanel4ContentContainer.add(PES05);
            PES05.setBounds(85, 185, 50, PES05.getPreferredSize().height);

            //---- PEP05 ----
            PEP05.setComponentPopupMenu(BTD);
            PEP05.setName("PEP05");
            xTitledPanel4ContentContainer.add(PEP05);
            PEP05.setBounds(175, 185, 50, PEP05.getPreferredSize().height);

            //---- PES06 ----
            PES06.setComponentPopupMenu(BTD);
            PES06.setName("PES06");
            xTitledPanel4ContentContainer.add(PES06);
            PES06.setBounds(85, 220, 50, PES06.getPreferredSize().height);

            //---- PEP06 ----
            PEP06.setComponentPopupMenu(BTD);
            PEP06.setName("PEP06");
            xTitledPanel4ContentContainer.add(PEP06);
            PEP06.setBounds(175, 220, 50, PEP06.getPreferredSize().height);

            //---- PES07 ----
            PES07.setComponentPopupMenu(BTD);
            PES07.setName("PES07");
            xTitledPanel4ContentContainer.add(PES07);
            PES07.setBounds(85, 255, 50, PES07.getPreferredSize().height);

            //---- PEP07 ----
            PEP07.setComponentPopupMenu(BTD);
            PEP07.setName("PEP07");
            xTitledPanel4ContentContainer.add(PEP07);
            PEP07.setBounds(175, 255, 50, PEP07.getPreferredSize().height);

            //---- PES08 ----
            PES08.setComponentPopupMenu(BTD);
            PES08.setName("PES08");
            xTitledPanel4ContentContainer.add(PES08);
            PES08.setBounds(85, 290, 50, PES08.getPreferredSize().height);

            //---- PEP08 ----
            PEP08.setComponentPopupMenu(BTD);
            PEP08.setName("PEP08");
            xTitledPanel4ContentContainer.add(PEP08);
            PEP08.setBounds(175, 290, 50, PEP08.getPreferredSize().height);

            //---- PES09 ----
            PES09.setComponentPopupMenu(BTD);
            PES09.setName("PES09");
            xTitledPanel4ContentContainer.add(PES09);
            PES09.setBounds(85, 325, 50, PES09.getPreferredSize().height);

            //---- PEP09 ----
            PEP09.setComponentPopupMenu(BTD);
            PEP09.setName("PEP09");
            xTitledPanel4ContentContainer.add(PEP09);
            PEP09.setBounds(175, 325, 50, PEP09.getPreferredSize().height);

            //---- PES10 ----
            PES10.setComponentPopupMenu(BTD);
            PES10.setName("PES10");
            xTitledPanel4ContentContainer.add(PES10);
            PES10.setBounds(85, 360, 50, PES10.getPreferredSize().height);

            //---- PEP10 ----
            PEP10.setComponentPopupMenu(BTD);
            PEP10.setName("PEP10");
            xTitledPanel4ContentContainer.add(PEP10);
            PEP10.setBounds(175, 360, 50, PEP10.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("01");
            OBJ_38.setFont(OBJ_38.getFont().deriveFont(OBJ_38.getFont().getStyle() | Font.BOLD));
            OBJ_38.setName("OBJ_38");
            xTitledPanel4ContentContainer.add(OBJ_38);
            OBJ_38.setBounds(30, 49, 36, 20);

            //---- OBJ_39 ----
            OBJ_39.setText("02");
            OBJ_39.setFont(OBJ_39.getFont().deriveFont(OBJ_39.getFont().getStyle() | Font.BOLD));
            OBJ_39.setName("OBJ_39");
            xTitledPanel4ContentContainer.add(OBJ_39);
            OBJ_39.setBounds(30, 84, 36, 20);

            //---- OBJ_40 ----
            OBJ_40.setText("03");
            OBJ_40.setFont(OBJ_40.getFont().deriveFont(OBJ_40.getFont().getStyle() | Font.BOLD));
            OBJ_40.setName("OBJ_40");
            xTitledPanel4ContentContainer.add(OBJ_40);
            OBJ_40.setBounds(30, 119, 36, 20);

            //---- OBJ_41 ----
            OBJ_41.setText("04");
            OBJ_41.setFont(OBJ_41.getFont().deriveFont(OBJ_41.getFont().getStyle() | Font.BOLD));
            OBJ_41.setName("OBJ_41");
            xTitledPanel4ContentContainer.add(OBJ_41);
            OBJ_41.setBounds(30, 154, 36, 20);

            //---- OBJ_42 ----
            OBJ_42.setText("05");
            OBJ_42.setFont(OBJ_42.getFont().deriveFont(OBJ_42.getFont().getStyle() | Font.BOLD));
            OBJ_42.setName("OBJ_42");
            xTitledPanel4ContentContainer.add(OBJ_42);
            OBJ_42.setBounds(30, 189, 36, 20);

            //---- OBJ_43 ----
            OBJ_43.setText("06");
            OBJ_43.setFont(OBJ_43.getFont().deriveFont(OBJ_43.getFont().getStyle() | Font.BOLD));
            OBJ_43.setName("OBJ_43");
            xTitledPanel4ContentContainer.add(OBJ_43);
            OBJ_43.setBounds(30, 224, 36, 20);

            //---- OBJ_44 ----
            OBJ_44.setText("07");
            OBJ_44.setFont(OBJ_44.getFont().deriveFont(OBJ_44.getFont().getStyle() | Font.BOLD));
            OBJ_44.setName("OBJ_44");
            xTitledPanel4ContentContainer.add(OBJ_44);
            OBJ_44.setBounds(30, 259, 36, 20);

            //---- OBJ_45 ----
            OBJ_45.setText("08");
            OBJ_45.setFont(OBJ_45.getFont().deriveFont(OBJ_45.getFont().getStyle() | Font.BOLD));
            OBJ_45.setName("OBJ_45");
            xTitledPanel4ContentContainer.add(OBJ_45);
            OBJ_45.setBounds(30, 294, 36, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("09");
            OBJ_46.setFont(OBJ_46.getFont().deriveFont(OBJ_46.getFont().getStyle() | Font.BOLD));
            OBJ_46.setName("OBJ_46");
            xTitledPanel4ContentContainer.add(OBJ_46);
            OBJ_46.setBounds(30, 329, 36, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("10");
            OBJ_47.setFont(OBJ_47.getFont().deriveFont(OBJ_47.getFont().getStyle() | Font.BOLD));
            OBJ_47.setName("OBJ_47");
            xTitledPanel4ContentContainer.add(OBJ_47);
            OBJ_47.setBounds(30, 364, 36, 20);

            //---- OBJ_36 ----
            OBJ_36.setText("%");
            OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
            OBJ_36.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_36.setName("OBJ_36");
            xTitledPanel4ContentContainer.add(OBJ_36);
            OBJ_36.setBounds(175, 25, 50, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(71, Short.MAX_VALUE)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 364, GroupLayout.PREFERRED_SIZE)
                .addGap(63, 63, 63))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(39, Short.MAX_VALUE)
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private RiZoneSortie INDETB;
  private JLabel OBJ_50;
  private RiZoneSortie INDTYP;
  private JLabel OBJ_51;
  private RiZoneSortie INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_49;
  private JLabel OBJ_37;
  private XRiTextField PEART;
  private XRiTextField PEF01;
  private XRiTextField PEF02;
  private XRiTextField PEF03;
  private XRiTextField PEF04;
  private XRiTextField PEF05;
  private XRiTextField PEF06;
  private XRiTextField PEF07;
  private XRiTextField PEF08;
  private XRiTextField PEF09;
  private XRiTextField PEF10;
  private JLabel OBJ_35;
  private XRiTextField PES01;
  private XRiTextField PEP01;
  private XRiTextField PES02;
  private XRiTextField PEP02;
  private XRiTextField PES03;
  private XRiTextField PEP03;
  private XRiTextField PES04;
  private XRiTextField PEP04;
  private XRiTextField PES05;
  private XRiTextField PEP05;
  private XRiTextField PES06;
  private XRiTextField PEP06;
  private XRiTextField PES07;
  private XRiTextField PEP07;
  private XRiTextField PES08;
  private XRiTextField PEP08;
  private XRiTextField PES09;
  private XRiTextField PEP09;
  private XRiTextField PES10;
  private XRiTextField PEP10;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private JLabel OBJ_44;
  private JLabel OBJ_45;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private JLabel OBJ_36;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
