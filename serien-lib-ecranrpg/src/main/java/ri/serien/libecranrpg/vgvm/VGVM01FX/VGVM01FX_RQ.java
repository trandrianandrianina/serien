
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_RQ extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGVM01FX_RQ(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }

  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_44 = new JLabel();
    INDTYP = new XRiTextField();
    OBJ_46 = new JLabel();
    INDIND = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel4 = new JXTitledPanel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_130 = new JLabel();
    OBJ_131 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    RQSC01 = new XRiTextField();
    RQSC02 = new XRiTextField();
    RQSC03 = new XRiTextField();
    RQSC04 = new XRiTextField();
    RQSC05 = new XRiTextField();
    RQSC06 = new XRiTextField();
    RQSC07 = new XRiTextField();
    RQSC08 = new XRiTextField();
    RQSC09 = new XRiTextField();
    RQSC10 = new XRiTextField();
    RQSC11 = new XRiTextField();
    RQSC12 = new XRiTextField();
    RQM01D = new XRiTextField();
    RQM01F = new XRiTextField();
    RQM02D = new XRiTextField();
    RQM02F = new XRiTextField();
    RQM03D = new XRiTextField();
    RQM03F = new XRiTextField();
    RQM04D = new XRiTextField();
    RQM04F = new XRiTextField();
    RQM05D = new XRiTextField();
    RQM05F = new XRiTextField();
    RQM06D = new XRiTextField();
    RQM06F = new XRiTextField();
    RQM07D = new XRiTextField();
    RQM07F = new XRiTextField();
    RQM08D = new XRiTextField();
    RQM08F = new XRiTextField();
    RQM09D = new XRiTextField();
    RQM09F = new XRiTextField();
    RQM10D = new XRiTextField();
    RQM10F = new XRiTextField();
    RQM11D = new XRiTextField();
    RQM11F = new XRiTextField();
    RQM12D = new XRiTextField();
    RQM12F = new XRiTextField();
    RQD01D = new XRiTextField();
    RQD01F = new XRiTextField();
    RQD02D = new XRiTextField();
    RQD02F = new XRiTextField();
    RQD03D = new XRiTextField();
    RQD03F = new XRiTextField();
    RQD04D = new XRiTextField();
    RQD04F = new XRiTextField();
    RQD05D = new XRiTextField();
    RQD05F = new XRiTextField();
    RQD06D = new XRiTextField();
    RQD06F = new XRiTextField();
    RQD07D = new XRiTextField();
    RQD07F = new XRiTextField();
    RQD08D = new XRiTextField();
    RQD08F = new XRiTextField();
    RQD09D = new XRiTextField();
    RQD09F = new XRiTextField();
    RQD10D = new XRiTextField();
    RQD10F = new XRiTextField();
    RQD11D = new XRiTextField();
    RQD11F = new XRiTextField();
    RQD12D = new XRiTextField();
    RQD12F = new XRiTextField();
    OBJ_35 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_77 = new JLabel();
    OBJ_83 = new JLabel();
    OBJ_89 = new JLabel();
    OBJ_95 = new JLabel();
    OBJ_101 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");

          //---- OBJ_46 ----
          OBJ_46.setText("Ordre");
          OBJ_46.setName("OBJ_46");

          //---- INDIND ----
          INDIND.setComponentPopupMenu(BTD);
          INDIND.setName("INDIND");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDTYP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDIND, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Edition des param\u00e8tres");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Historique modifications");
              riSousMenu_bt7.setToolTipText("Historique des modifications");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("R\u00e9f\u00e9rence quota");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- OBJ_30 ----
            OBJ_30.setText("D\u00e9but");
            OBJ_30.setName("OBJ_30");
            xTitledPanel4ContentContainer.add(OBJ_30);
            OBJ_30.setBounds(150, 10, 50, 20);

            //---- OBJ_31 ----
            OBJ_31.setText("Fin");
            OBJ_31.setName("OBJ_31");
            xTitledPanel4ContentContainer.add(OBJ_31);
            OBJ_31.setBounds(310, 10, 90, 20);

            //---- OBJ_130 ----
            OBJ_130.setText("Mois / D\u00e9cade");
            OBJ_130.setName("OBJ_130");
            xTitledPanel4ContentContainer.add(OBJ_130);
            OBJ_130.setBounds(150, 35, 90, 20);

            //---- OBJ_131 ----
            OBJ_131.setText("Mois / D\u00e9cade");
            OBJ_131.setName("OBJ_131");
            xTitledPanel4ContentContainer.add(OBJ_131);
            OBJ_131.setBounds(310, 35, 90, 20);

            //---- OBJ_34 ----
            OBJ_34.setText("P\u00e9riodes");
            OBJ_34.setName("OBJ_34");
            xTitledPanel4ContentContainer.add(OBJ_34);
            OBJ_34.setBounds(10, 59, 58, 20);

            //---- OBJ_32 ----
            OBJ_32.setText("Seuil");
            OBJ_32.setName("OBJ_32");
            xTitledPanel4ContentContainer.add(OBJ_32);
            OBJ_32.setBounds(470, 10, 52, 20);

            //---- OBJ_33 ----
            OBJ_33.setText("critique");
            OBJ_33.setName("OBJ_33");
            xTitledPanel4ContentContainer.add(OBJ_33);
            OBJ_33.setBounds(470, 35, 52, 20);

            //---- RQSC01 ----
            RQSC01.setComponentPopupMenu(BTD);
            RQSC01.setName("RQSC01");
            xTitledPanel4ContentContainer.add(RQSC01);
            RQSC01.setBounds(475, 55, 34, RQSC01.getPreferredSize().height);

            //---- RQSC02 ----
            RQSC02.setComponentPopupMenu(BTD);
            RQSC02.setName("RQSC02");
            xTitledPanel4ContentContainer.add(RQSC02);
            RQSC02.setBounds(475, 90, 34, RQSC02.getPreferredSize().height);

            //---- RQSC03 ----
            RQSC03.setComponentPopupMenu(BTD);
            RQSC03.setName("RQSC03");
            xTitledPanel4ContentContainer.add(RQSC03);
            RQSC03.setBounds(475, 125, 34, RQSC03.getPreferredSize().height);

            //---- RQSC04 ----
            RQSC04.setComponentPopupMenu(BTD);
            RQSC04.setName("RQSC04");
            xTitledPanel4ContentContainer.add(RQSC04);
            RQSC04.setBounds(475, 160, 34, RQSC04.getPreferredSize().height);

            //---- RQSC05 ----
            RQSC05.setComponentPopupMenu(BTD);
            RQSC05.setName("RQSC05");
            xTitledPanel4ContentContainer.add(RQSC05);
            RQSC05.setBounds(475, 200, 34, RQSC05.getPreferredSize().height);

            //---- RQSC06 ----
            RQSC06.setComponentPopupMenu(BTD);
            RQSC06.setName("RQSC06");
            xTitledPanel4ContentContainer.add(RQSC06);
            RQSC06.setBounds(475, 235, 34, RQSC06.getPreferredSize().height);

            //---- RQSC07 ----
            RQSC07.setComponentPopupMenu(BTD);
            RQSC07.setName("RQSC07");
            xTitledPanel4ContentContainer.add(RQSC07);
            RQSC07.setBounds(475, 270, 34, RQSC07.getPreferredSize().height);

            //---- RQSC08 ----
            RQSC08.setComponentPopupMenu(BTD);
            RQSC08.setName("RQSC08");
            xTitledPanel4ContentContainer.add(RQSC08);
            RQSC08.setBounds(475, 305, 34, RQSC08.getPreferredSize().height);

            //---- RQSC09 ----
            RQSC09.setComponentPopupMenu(BTD);
            RQSC09.setName("RQSC09");
            xTitledPanel4ContentContainer.add(RQSC09);
            RQSC09.setBounds(475, 340, 34, RQSC09.getPreferredSize().height);

            //---- RQSC10 ----
            RQSC10.setComponentPopupMenu(BTD);
            RQSC10.setName("RQSC10");
            xTitledPanel4ContentContainer.add(RQSC10);
            RQSC10.setBounds(475, 375, 34, RQSC10.getPreferredSize().height);

            //---- RQSC11 ----
            RQSC11.setComponentPopupMenu(BTD);
            RQSC11.setName("RQSC11");
            xTitledPanel4ContentContainer.add(RQSC11);
            RQSC11.setBounds(475, 410, 34, RQSC11.getPreferredSize().height);

            //---- RQSC12 ----
            RQSC12.setComponentPopupMenu(BTD);
            RQSC12.setName("RQSC12");
            xTitledPanel4ContentContainer.add(RQSC12);
            RQSC12.setBounds(475, 445, 34, RQSC12.getPreferredSize().height);

            //---- RQM01D ----
            RQM01D.setComponentPopupMenu(BTD);
            RQM01D.setName("RQM01D");
            xTitledPanel4ContentContainer.add(RQM01D);
            RQM01D.setBounds(165, 55, 26, RQM01D.getPreferredSize().height);

            //---- RQM01F ----
            RQM01F.setComponentPopupMenu(BTD);
            RQM01F.setName("RQM01F");
            xTitledPanel4ContentContainer.add(RQM01F);
            RQM01F.setBounds(320, 55, 26, RQM01F.getPreferredSize().height);

            //---- RQM02D ----
            RQM02D.setComponentPopupMenu(BTD);
            RQM02D.setName("RQM02D");
            xTitledPanel4ContentContainer.add(RQM02D);
            RQM02D.setBounds(165, 90, 26, RQM02D.getPreferredSize().height);

            //---- RQM02F ----
            RQM02F.setComponentPopupMenu(BTD);
            RQM02F.setName("RQM02F");
            xTitledPanel4ContentContainer.add(RQM02F);
            RQM02F.setBounds(320, 90, 26, RQM02F.getPreferredSize().height);

            //---- RQM03D ----
            RQM03D.setComponentPopupMenu(BTD);
            RQM03D.setName("RQM03D");
            xTitledPanel4ContentContainer.add(RQM03D);
            RQM03D.setBounds(165, 125, 26, RQM03D.getPreferredSize().height);

            //---- RQM03F ----
            RQM03F.setComponentPopupMenu(BTD);
            RQM03F.setName("RQM03F");
            xTitledPanel4ContentContainer.add(RQM03F);
            RQM03F.setBounds(320, 125, 26, RQM03F.getPreferredSize().height);

            //---- RQM04D ----
            RQM04D.setComponentPopupMenu(BTD);
            RQM04D.setName("RQM04D");
            xTitledPanel4ContentContainer.add(RQM04D);
            RQM04D.setBounds(165, 160, 26, RQM04D.getPreferredSize().height);

            //---- RQM04F ----
            RQM04F.setComponentPopupMenu(BTD);
            RQM04F.setName("RQM04F");
            xTitledPanel4ContentContainer.add(RQM04F);
            RQM04F.setBounds(320, 160, 26, RQM04F.getPreferredSize().height);

            //---- RQM05D ----
            RQM05D.setComponentPopupMenu(BTD);
            RQM05D.setName("RQM05D");
            xTitledPanel4ContentContainer.add(RQM05D);
            RQM05D.setBounds(165, 200, 26, RQM05D.getPreferredSize().height);

            //---- RQM05F ----
            RQM05F.setComponentPopupMenu(BTD);
            RQM05F.setName("RQM05F");
            xTitledPanel4ContentContainer.add(RQM05F);
            RQM05F.setBounds(320, 200, 26, RQM05F.getPreferredSize().height);

            //---- RQM06D ----
            RQM06D.setComponentPopupMenu(BTD);
            RQM06D.setName("RQM06D");
            xTitledPanel4ContentContainer.add(RQM06D);
            RQM06D.setBounds(165, 235, 26, RQM06D.getPreferredSize().height);

            //---- RQM06F ----
            RQM06F.setComponentPopupMenu(BTD);
            RQM06F.setName("RQM06F");
            xTitledPanel4ContentContainer.add(RQM06F);
            RQM06F.setBounds(320, 235, 26, RQM06F.getPreferredSize().height);

            //---- RQM07D ----
            RQM07D.setComponentPopupMenu(BTD);
            RQM07D.setName("RQM07D");
            xTitledPanel4ContentContainer.add(RQM07D);
            RQM07D.setBounds(165, 270, 26, RQM07D.getPreferredSize().height);

            //---- RQM07F ----
            RQM07F.setComponentPopupMenu(BTD);
            RQM07F.setName("RQM07F");
            xTitledPanel4ContentContainer.add(RQM07F);
            RQM07F.setBounds(320, 270, 26, RQM07F.getPreferredSize().height);

            //---- RQM08D ----
            RQM08D.setComponentPopupMenu(BTD);
            RQM08D.setName("RQM08D");
            xTitledPanel4ContentContainer.add(RQM08D);
            RQM08D.setBounds(165, 305, 26, RQM08D.getPreferredSize().height);

            //---- RQM08F ----
            RQM08F.setComponentPopupMenu(BTD);
            RQM08F.setName("RQM08F");
            xTitledPanel4ContentContainer.add(RQM08F);
            RQM08F.setBounds(320, 305, 26, RQM08F.getPreferredSize().height);

            //---- RQM09D ----
            RQM09D.setComponentPopupMenu(BTD);
            RQM09D.setName("RQM09D");
            xTitledPanel4ContentContainer.add(RQM09D);
            RQM09D.setBounds(165, 340, 26, RQM09D.getPreferredSize().height);

            //---- RQM09F ----
            RQM09F.setComponentPopupMenu(BTD);
            RQM09F.setName("RQM09F");
            xTitledPanel4ContentContainer.add(RQM09F);
            RQM09F.setBounds(320, 340, 26, RQM09F.getPreferredSize().height);

            //---- RQM10D ----
            RQM10D.setComponentPopupMenu(BTD);
            RQM10D.setName("RQM10D");
            xTitledPanel4ContentContainer.add(RQM10D);
            RQM10D.setBounds(165, 375, 26, RQM10D.getPreferredSize().height);

            //---- RQM10F ----
            RQM10F.setComponentPopupMenu(BTD);
            RQM10F.setName("RQM10F");
            xTitledPanel4ContentContainer.add(RQM10F);
            RQM10F.setBounds(320, 375, 26, RQM10F.getPreferredSize().height);

            //---- RQM11D ----
            RQM11D.setComponentPopupMenu(BTD);
            RQM11D.setName("RQM11D");
            xTitledPanel4ContentContainer.add(RQM11D);
            RQM11D.setBounds(165, 410, 26, RQM11D.getPreferredSize().height);

            //---- RQM11F ----
            RQM11F.setComponentPopupMenu(BTD);
            RQM11F.setName("RQM11F");
            xTitledPanel4ContentContainer.add(RQM11F);
            RQM11F.setBounds(320, 410, 26, RQM11F.getPreferredSize().height);

            //---- RQM12D ----
            RQM12D.setComponentPopupMenu(BTD);
            RQM12D.setName("RQM12D");
            xTitledPanel4ContentContainer.add(RQM12D);
            RQM12D.setBounds(165, 445, 26, RQM12D.getPreferredSize().height);

            //---- RQM12F ----
            RQM12F.setComponentPopupMenu(BTD);
            RQM12F.setName("RQM12F");
            xTitledPanel4ContentContainer.add(RQM12F);
            RQM12F.setBounds(320, 445, 26, RQM12F.getPreferredSize().height);

            //---- RQD01D ----
            RQD01D.setComponentPopupMenu(BTD);
            RQD01D.setName("RQD01D");
            xTitledPanel4ContentContainer.add(RQD01D);
            RQD01D.setBounds(205, 55, 18, RQD01D.getPreferredSize().height);

            //---- RQD01F ----
            RQD01F.setComponentPopupMenu(BTD);
            RQD01F.setName("RQD01F");
            xTitledPanel4ContentContainer.add(RQD01F);
            RQD01F.setBounds(360, 55, 18, RQD01F.getPreferredSize().height);

            //---- RQD02D ----
            RQD02D.setComponentPopupMenu(BTD);
            RQD02D.setName("RQD02D");
            xTitledPanel4ContentContainer.add(RQD02D);
            RQD02D.setBounds(205, 90, 18, RQD02D.getPreferredSize().height);

            //---- RQD02F ----
            RQD02F.setComponentPopupMenu(BTD);
            RQD02F.setName("RQD02F");
            xTitledPanel4ContentContainer.add(RQD02F);
            RQD02F.setBounds(360, 90, 18, RQD02F.getPreferredSize().height);

            //---- RQD03D ----
            RQD03D.setComponentPopupMenu(BTD);
            RQD03D.setName("RQD03D");
            xTitledPanel4ContentContainer.add(RQD03D);
            RQD03D.setBounds(205, 125, 18, RQD03D.getPreferredSize().height);

            //---- RQD03F ----
            RQD03F.setComponentPopupMenu(BTD);
            RQD03F.setName("RQD03F");
            xTitledPanel4ContentContainer.add(RQD03F);
            RQD03F.setBounds(360, 125, 18, RQD03F.getPreferredSize().height);

            //---- RQD04D ----
            RQD04D.setComponentPopupMenu(BTD);
            RQD04D.setName("RQD04D");
            xTitledPanel4ContentContainer.add(RQD04D);
            RQD04D.setBounds(205, 160, 18, RQD04D.getPreferredSize().height);

            //---- RQD04F ----
            RQD04F.setComponentPopupMenu(BTD);
            RQD04F.setName("RQD04F");
            xTitledPanel4ContentContainer.add(RQD04F);
            RQD04F.setBounds(360, 160, 18, RQD04F.getPreferredSize().height);

            //---- RQD05D ----
            RQD05D.setComponentPopupMenu(BTD);
            RQD05D.setName("RQD05D");
            xTitledPanel4ContentContainer.add(RQD05D);
            RQD05D.setBounds(205, 200, 18, RQD05D.getPreferredSize().height);

            //---- RQD05F ----
            RQD05F.setComponentPopupMenu(BTD);
            RQD05F.setName("RQD05F");
            xTitledPanel4ContentContainer.add(RQD05F);
            RQD05F.setBounds(360, 200, 18, RQD05F.getPreferredSize().height);

            //---- RQD06D ----
            RQD06D.setComponentPopupMenu(BTD);
            RQD06D.setName("RQD06D");
            xTitledPanel4ContentContainer.add(RQD06D);
            RQD06D.setBounds(205, 235, 18, RQD06D.getPreferredSize().height);

            //---- RQD06F ----
            RQD06F.setComponentPopupMenu(BTD);
            RQD06F.setName("RQD06F");
            xTitledPanel4ContentContainer.add(RQD06F);
            RQD06F.setBounds(360, 235, 18, RQD06F.getPreferredSize().height);

            //---- RQD07D ----
            RQD07D.setComponentPopupMenu(BTD);
            RQD07D.setName("RQD07D");
            xTitledPanel4ContentContainer.add(RQD07D);
            RQD07D.setBounds(205, 270, 18, RQD07D.getPreferredSize().height);

            //---- RQD07F ----
            RQD07F.setComponentPopupMenu(BTD);
            RQD07F.setName("RQD07F");
            xTitledPanel4ContentContainer.add(RQD07F);
            RQD07F.setBounds(360, 270, 18, RQD07F.getPreferredSize().height);

            //---- RQD08D ----
            RQD08D.setComponentPopupMenu(BTD);
            RQD08D.setName("RQD08D");
            xTitledPanel4ContentContainer.add(RQD08D);
            RQD08D.setBounds(205, 305, 18, RQD08D.getPreferredSize().height);

            //---- RQD08F ----
            RQD08F.setComponentPopupMenu(BTD);
            RQD08F.setName("RQD08F");
            xTitledPanel4ContentContainer.add(RQD08F);
            RQD08F.setBounds(360, 305, 18, RQD08F.getPreferredSize().height);

            //---- RQD09D ----
            RQD09D.setComponentPopupMenu(BTD);
            RQD09D.setName("RQD09D");
            xTitledPanel4ContentContainer.add(RQD09D);
            RQD09D.setBounds(205, 340, 18, RQD09D.getPreferredSize().height);

            //---- RQD09F ----
            RQD09F.setComponentPopupMenu(BTD);
            RQD09F.setName("RQD09F");
            xTitledPanel4ContentContainer.add(RQD09F);
            RQD09F.setBounds(360, 340, 18, RQD09F.getPreferredSize().height);

            //---- RQD10D ----
            RQD10D.setComponentPopupMenu(BTD);
            RQD10D.setName("RQD10D");
            xTitledPanel4ContentContainer.add(RQD10D);
            RQD10D.setBounds(205, 375, 18, RQD10D.getPreferredSize().height);

            //---- RQD10F ----
            RQD10F.setComponentPopupMenu(BTD);
            RQD10F.setName("RQD10F");
            xTitledPanel4ContentContainer.add(RQD10F);
            RQD10F.setBounds(360, 375, 18, RQD10F.getPreferredSize().height);

            //---- RQD11D ----
            RQD11D.setComponentPopupMenu(BTD);
            RQD11D.setName("RQD11D");
            xTitledPanel4ContentContainer.add(RQD11D);
            RQD11D.setBounds(205, 410, 18, RQD11D.getPreferredSize().height);

            //---- RQD11F ----
            RQD11F.setComponentPopupMenu(BTD);
            RQD11F.setName("RQD11F");
            xTitledPanel4ContentContainer.add(RQD11F);
            RQD11F.setBounds(360, 410, 18, RQD11F.getPreferredSize().height);

            //---- RQD12D ----
            RQD12D.setComponentPopupMenu(BTD);
            RQD12D.setName("RQD12D");
            xTitledPanel4ContentContainer.add(RQD12D);
            RQD12D.setBounds(205, 445, 18, RQD12D.getPreferredSize().height);

            //---- RQD12F ----
            RQD12F.setComponentPopupMenu(BTD);
            RQD12F.setName("RQD12F");
            xTitledPanel4ContentContainer.add(RQD12F);
            RQD12F.setBounds(360, 445, 18, RQD12F.getPreferredSize().height);

            //---- OBJ_35 ----
            OBJ_35.setText("01");
            OBJ_35.setName("OBJ_35");
            xTitledPanel4ContentContainer.add(OBJ_35);
            OBJ_35.setBounds(105, 59, 18, 20);

            //---- OBJ_41 ----
            OBJ_41.setText("02");
            OBJ_41.setName("OBJ_41");
            xTitledPanel4ContentContainer.add(OBJ_41);
            OBJ_41.setBounds(105, 94, 18, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("03");
            OBJ_47.setName("OBJ_47");
            xTitledPanel4ContentContainer.add(OBJ_47);
            OBJ_47.setBounds(105, 129, 18, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("04");
            OBJ_53.setName("OBJ_53");
            xTitledPanel4ContentContainer.add(OBJ_53);
            OBJ_53.setBounds(105, 164, 18, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("05");
            OBJ_59.setName("OBJ_59");
            xTitledPanel4ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(105, 204, 18, 20);

            //---- OBJ_65 ----
            OBJ_65.setText("06");
            OBJ_65.setName("OBJ_65");
            xTitledPanel4ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(105, 239, 18, 20);

            //---- OBJ_71 ----
            OBJ_71.setText("07");
            OBJ_71.setName("OBJ_71");
            xTitledPanel4ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(105, 274, 18, 20);

            //---- OBJ_77 ----
            OBJ_77.setText("08");
            OBJ_77.setName("OBJ_77");
            xTitledPanel4ContentContainer.add(OBJ_77);
            OBJ_77.setBounds(105, 309, 18, 20);

            //---- OBJ_83 ----
            OBJ_83.setText("09");
            OBJ_83.setName("OBJ_83");
            xTitledPanel4ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(105, 344, 18, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("10");
            OBJ_89.setName("OBJ_89");
            xTitledPanel4ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(105, 379, 18, 20);

            //---- OBJ_95 ----
            OBJ_95.setText("11");
            OBJ_95.setName("OBJ_95");
            xTitledPanel4ContentContainer.add(OBJ_95);
            OBJ_95.setBounds(105, 414, 18, 20);

            //---- OBJ_101 ----
            OBJ_101.setText("12");
            OBJ_101.setName("OBJ_101");
            xTitledPanel4ContentContainer.add(OBJ_101);
            OBJ_101.setBounds(105, 449, 18, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 573, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(13, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel4, GroupLayout.PREFERRED_SIZE, 515, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(10, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Invite");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_44;
  private XRiTextField INDTYP;
  private JLabel OBJ_46;
  private XRiTextField INDIND;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel4;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private JLabel OBJ_130;
  private JLabel OBJ_131;
  private JLabel OBJ_34;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private XRiTextField RQSC01;
  private XRiTextField RQSC02;
  private XRiTextField RQSC03;
  private XRiTextField RQSC04;
  private XRiTextField RQSC05;
  private XRiTextField RQSC06;
  private XRiTextField RQSC07;
  private XRiTextField RQSC08;
  private XRiTextField RQSC09;
  private XRiTextField RQSC10;
  private XRiTextField RQSC11;
  private XRiTextField RQSC12;
  private XRiTextField RQM01D;
  private XRiTextField RQM01F;
  private XRiTextField RQM02D;
  private XRiTextField RQM02F;
  private XRiTextField RQM03D;
  private XRiTextField RQM03F;
  private XRiTextField RQM04D;
  private XRiTextField RQM04F;
  private XRiTextField RQM05D;
  private XRiTextField RQM05F;
  private XRiTextField RQM06D;
  private XRiTextField RQM06F;
  private XRiTextField RQM07D;
  private XRiTextField RQM07F;
  private XRiTextField RQM08D;
  private XRiTextField RQM08F;
  private XRiTextField RQM09D;
  private XRiTextField RQM09F;
  private XRiTextField RQM10D;
  private XRiTextField RQM10F;
  private XRiTextField RQM11D;
  private XRiTextField RQM11F;
  private XRiTextField RQM12D;
  private XRiTextField RQM12F;
  private XRiTextField RQD01D;
  private XRiTextField RQD01F;
  private XRiTextField RQD02D;
  private XRiTextField RQD02F;
  private XRiTextField RQD03D;
  private XRiTextField RQD03F;
  private XRiTextField RQD04D;
  private XRiTextField RQD04F;
  private XRiTextField RQD05D;
  private XRiTextField RQD05F;
  private XRiTextField RQD06D;
  private XRiTextField RQD06F;
  private XRiTextField RQD07D;
  private XRiTextField RQD07F;
  private XRiTextField RQD08D;
  private XRiTextField RQD08F;
  private XRiTextField RQD09D;
  private XRiTextField RQD09F;
  private XRiTextField RQD10D;
  private XRiTextField RQD10F;
  private XRiTextField RQD11D;
  private XRiTextField RQD11F;
  private XRiTextField RQD12D;
  private XRiTextField RQD12F;
  private JLabel OBJ_35;
  private JLabel OBJ_41;
  private JLabel OBJ_47;
  private JLabel OBJ_53;
  private JLabel OBJ_59;
  private JLabel OBJ_65;
  private JLabel OBJ_71;
  private JLabel OBJ_77;
  private JLabel OBJ_83;
  private JLabel OBJ_89;
  private JLabel OBJ_95;
  private JLabel OBJ_101;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
