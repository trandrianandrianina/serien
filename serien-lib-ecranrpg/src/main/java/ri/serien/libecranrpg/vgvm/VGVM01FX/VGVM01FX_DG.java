
package ri.serien.libecranrpg.vgvm.VGVM01FX;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JTabbedPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sndevise.SNDevise;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VGVM01FX_DG extends SNPanelEcranRPG implements ioFrame {
  
  private String[] DGTC2_Value = { "", "S", "M", };
  private String[] DGRON_Value = { "", "1", "2", "3", "4", "5", "6", };
  private String[] DGTC1_Value = { "V", "C", "X", "1", "Y", "P" };
  private boolean pilote = false;
  boolean isConsultation = true;
  boolean isCreation = false;
  
  // Boutons
  private static final String BOUTON_MODIFICATION = "Modifier";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  private static final String BOUTON_MAGASIN_ETABLISSEMENT = "Voir magasins";
  
  public VGVM01FX_DG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    DGTC2.setValeurs(DGTC2_Value, null);
    DGTC1.setValeurs(DGTC1_Value, null);
    DGRON.setValeurs(DGRON_Value, null);
    DGCGE.setValeursSelection("1", " ");
    DGIN2.setValeursSelection("1", " ");
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MODIFICATION, 'm', true);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', true);
    snBarreBouton.ajouterBouton(BOUTON_MAGASIN_ETABLISSEMENT, 'g', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    pilote = lexique.isTrue("09");
    isConsultation = lexique.getMode() == Lexical.MODE_CONSULTATION;
    isCreation = lexique.getMode() == Lexical.MODE_CREATION;
    
    // Initialisation de l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    snEtablissement.setEnabled(!isConsultation);
    snEtablissement.setVisible(snEtablissement.getSelection() != null);
    lbEtablissement.setVisible(snEtablissement.getSelection() != null);
    
    // Etablissement pilote
    if (pilote) {
      snEtablissementPilote.setSession(getSession());
      snEtablissementPilote.charger(false);
      snEtablissementPilote.setSelectionParChampRPG(lexique, "DGETPX");
      snEtablissementPilote.setEnabled(!isConsultation);
    }
    snEtablissementPilote.setVisible(pilote);
    lbEtablissementPilote.setVisible(pilote);
    lbRechercheSecurite.setVisible(pilote);
    DGIN1.setVisible(pilote);
    
    // Magasin général
    if (snEtablissement.getIdSelection() != null) {
      snMagasinGeneral.setSession(getSession());
      snMagasinGeneral.setIdEtablissement(snEtablissement.getIdSelection());
      snMagasinGeneral.charger(false);
      snMagasinGeneral.setSelectionParChampRPG(lexique, "DGMAGX");
      snMagasinGeneral.setVisible(!pilote);
      lbMagasinGeneral.setVisible(!pilote);
      snMagasinGeneral.setEnabled(!isConsultation);
      lbMagasinGeneral.setVisible(!isCreation);
      snMagasinGeneral.setVisible(!isCreation);
    }
    
    // Devise locale
    if (!pilote) {
      lbDeviseLocale.setVisible(true);
      snDeviseLocale.setVisible(true);
      if (snEtablissement.getIdSelection() != null) {
        snDeviseLocale.setSession(getSession());
        snDeviseLocale.setIdEtablissement(snEtablissement.getIdSelection());
        snDeviseLocale.charger(false);
        snDeviseLocale.setSelectionParChampRPG(lexique, "DGDEV");
        snDeviseLocale.setEnabled(!isConsultation);
      }
    }
    else {
      lbDeviseLocale.setVisible(false);
      snDeviseLocale.setVisible(false);
    }
    snDeviseLocale.setVisible(!pilote);
    DGRON.setVisible(!pilote);
    DGJRL.setVisible(!pilote);
    lbICS.setVisible(lexique.isPresent("DGICS"));
    lbJourReleve.setVisible(lexique.isPresent("DGJRL"));
    lbJourConservation.setVisible(lexique.isPresent("DGPXFL"));
    lbMontantMax.setVisible(lexique.isPresent("DGMTES"));
    pnlCNV.setVisible(!pilote);
    pnlExercices.setVisible(!pilote);
    
    lbPhaseEuro.setVisible(lexique.isTrue("N82"));
    DGEUR.setVisible(lexique.isTrue("N82"));
    
    DGPXFL.setVisible(snEtablissement.getSelection() != null);
    
    if (snEtablissement.getSelection() != null) {
      p_bpresentation.setCodeEtablissement(snEtablissement.getSelection().getId().getCodeEtablissement());
    }
    
    rafraichirBoutons();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snEtablissementPilote.renseignerChampRPG(lexique, "DGETPX");
    snMagasinGeneral.renseignerChampRPG(lexique, "DGMAGX");
    if (!lexique.HostFieldGetData("DGDEV").trim().equalsIgnoreCase("EUR") && !lexique.HostFieldGetData("DGDEV").trim().isEmpty()) {
      snDeviseLocale.renseignerChampRPG(lexique, "DGDEV");
    }
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFICATION)) {
        lexique.HostScreenSendKey(this, "F14");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      else if (pSNBouton.isBouton(BOUTON_MAGASIN_ETABLISSEMENT)) {
        lexique.HostScreenSendKey(this, "F2");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirBoutons() {
    snBarreBouton.activerBouton(BOUTON_MODIFICATION, isConsultation);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isConsultation);
    snBarreBouton.activerBouton(BOUTON_MAGASIN_ETABLISSEMENT, isConsultation);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.ANNULER, !isConsultation);
    snBarreBouton.activerBouton(EnumBouton.FERMER, isConsultation);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlbandeau = new SNPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    sNPanelContenu1 = new SNPanelContenu();
    pnlPersonnalisation = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbLibelle = new SNLabelChamp();
    PG01LI = new XRiTextField();
    sNPanel1 = new SNPanel();
    lbNom = new SNLabelChamp();
    DGNOM = new XRiTextField();
    lbICS = new SNLabelChamp();
    DGICS = new XRiTextField();
    lbComplement = new SNLabelChamp();
    DGCPL = new XRiTextField();
    lbMagasinGeneral = new SNLabelChamp();
    snMagasinGeneral = new SNMagasin();
    lbEtablissementPilote = new SNLabelChamp();
    snEtablissementPilote = new SNEtablissement();
    lbRechercheSecurite = new SNLabelChamp();
    DGIN1 = new XRiTextField();
    tabbedPane1 = new JTabbedPane();
    pnlGeneral = new SNPanelContenu();
    lbDeviseLocale = new SNLabelChamp();
    pnlValidite = new SNPanel();
    snDeviseLocale = new SNDevise();
    DGCGE = new XRiCheckBox();
    lbPhaseEuro = new SNLabelChamp();
    pnlRemiseMax = new SNPanel();
    DGEUR = new XRiTextField();
    pnlExercices = new SNPanelTitre();
    sNPanel4 = new SNPanel();
    lbExerciceDemarrage = new SNLabelChamp();
    DGDEBX = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    DGFINX = new XRiCalendrier();
    sNPanel5 = new SNPanel();
    lbExerciceEnCours = new SNLabelChamp();
    DGDE1X = new XRiCalendrier();
    lbAu2 = new SNLabelChamp();
    DGFE1X = new XRiCalendrier();
    lbMoisEnCours = new SNLabelChamp();
    DGDECX = new XRiCalendrier();
    sNPanel6 = new SNPanel();
    lbExerciceSuivant = new SNLabelChamp();
    DGDE2X = new XRiCalendrier();
    lbAu3 = new SNLabelChamp();
    DGFE2X = new XRiCalendrier();
    pnlDocuments = new SNPanelContenu();
    sNPanel3 = new SNPanel();
    sNPanel2 = new SNPanel();
    sNPanel8 = new SNPanel();
    lbCoefNormal = new SNLabelChamp();
    DGCMA = new XRiTextField();
    lbJourReleve = new SNLabelChamp();
    DGJRL = new XRiTextField();
    lbJourConservation = new SNLabelChamp();
    DGPXFL = new XRiTextField();
    sNPanel7 = new SNPanel();
    lbMontantMax = new SNLabelChamp();
    DGMTES = new XRiTextField();
    pnlTaxes = new SNPanelTitre();
    pnlTVA = new SNPanel();
    lbTVA = new SNLabelChamp();
    lbCodeTVA1 = new SNLabelChamp();
    DGT01 = new XRiTextField();
    lbCodeTVA2 = new SNLabelChamp();
    DGT02 = new XRiTextField();
    lbCodeTVA3 = new SNLabelChamp();
    DGT03 = new XRiTextField();
    lbCodeTVA4 = new SNLabelChamp();
    DGT04 = new XRiTextField();
    lbCodeTVA5 = new SNLabelChamp();
    DGT05 = new XRiTextField();
    lbCodeTVA6 = new SNLabelChamp();
    DGT06 = new XRiTextField();
    pnlParafiscale = new SNPanel();
    sNLabelChamp3 = new SNLabelChamp();
    sNLabelChamp4 = new SNLabelChamp();
    DGTPF = new XRiTextField();
    sNLabelChamp5 = new SNLabelChamp();
    DGRBP = new XRiTextField();
    DGIN2 = new XRiCheckBox();
    sNLabelChamp6 = new SNLabelChamp();
    DGRON = new XRiComboBox();
    pnlCNV = new SNPanelTitre();
    lbGenerale = new SNLabelChamp();
    DGCNG = new XRiTextField();
    lbQuantitative = new SNLabelChamp();
    DGTC1 = new XRiComboBox();
    lbCumul = new SNLabelChamp();
    DGTC2 = new XRiComboBox();
    lbPromo = new SNLabelChamp();
    DGCNP = new XRiTextField();
    INDIND = new XRiTextField();
    lbCodeCategorie = new SNLabelChamp();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlbandeau ========
    {
      pnlbandeau.setName("pnlbandeau");
      pnlbandeau.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      pnlbandeau.add(p_bpresentation);
    }
    add(pnlbandeau, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanelContenu1 ========
    {
      sNPanelContenu1.setName("sNPanelContenu1");
      sNPanelContenu1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlPersonnalisation ========
      {
        pnlPersonnalisation.setName("pnlPersonnalisation");
        pnlPersonnalisation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlPersonnalisation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setName("lbEtablissement");
        pnlPersonnalisation.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        pnlPersonnalisation.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9");
        lbLibelle.setMinimumSize(new Dimension(80, 30));
        lbLibelle.setMaximumSize(new Dimension(80, 30));
        lbLibelle.setPreferredSize(new Dimension(80, 30));
        lbLibelle.setName("lbLibelle");
        pnlPersonnalisation.add(lbLibelle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- PG01LI ----
        PG01LI.setPreferredSize(new Dimension(400, 30));
        PG01LI.setMinimumSize(new Dimension(400, 30));
        PG01LI.setMaximumSize(new Dimension(400, 30));
        PG01LI.setFont(new Font("sansserif", Font.PLAIN, 14));
        PG01LI.setName("PG01LI");
        pnlPersonnalisation.add(PG01LI, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(pnlPersonnalisation,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== sNPanel1 ========
      {
        sNPanel1.setName("sNPanel1");
        sNPanel1.setLayout(new GridBagLayout());
        ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNom ----
        lbNom.setText("Nom ou raison sociale");
        lbNom.setName("lbNom");
        sNPanel1.add(lbNom, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- DGNOM ----
        DGNOM.setComponentPopupMenu(null);
        DGNOM.setFont(new Font("sansserif", Font.PLAIN, 14));
        DGNOM.setMinimumSize(new Dimension(300, 30));
        DGNOM.setMaximumSize(new Dimension(300, 30));
        DGNOM.setPreferredSize(new Dimension(300, 30));
        DGNOM.setName("DGNOM");
        sNPanel1.add(DGNOM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbICS ----
        lbICS.setText("ICS");
        lbICS.setName("lbICS");
        sNPanel1.add(lbICS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- DGICS ----
        DGICS.setMinimumSize(new Dimension(150, 30));
        DGICS.setMaximumSize(new Dimension(150, 30));
        DGICS.setPreferredSize(new Dimension(150, 30));
        DGICS.setFont(new Font("sansserif", Font.PLAIN, 14));
        DGICS.setName("DGICS");
        sNPanel1.add(DGICS, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbComplement ----
        lbComplement.setText("Compl\u00e9ment de nom");
        lbComplement.setName("lbComplement");
        sNPanel1.add(lbComplement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- DGCPL ----
        DGCPL.setComponentPopupMenu(null);
        DGCPL.setFont(new Font("sansserif", Font.PLAIN, 14));
        DGCPL.setMinimumSize(new Dimension(300, 30));
        DGCPL.setMaximumSize(new Dimension(300, 30));
        DGCPL.setPreferredSize(new Dimension(300, 30));
        DGCPL.setName("DGCPL");
        sNPanel1.add(DGCPL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbMagasinGeneral ----
        lbMagasinGeneral.setText("Magasin g\u00e9n\u00e9ral");
        lbMagasinGeneral.setName("lbMagasinGeneral");
        sNPanel1.add(lbMagasinGeneral, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snMagasinGeneral ----
        snMagasinGeneral.setComponentPopupMenu(null);
        snMagasinGeneral.setName("snMagasinGeneral");
        sNPanel1.add(snMagasinGeneral, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbEtablissementPilote ----
        lbEtablissementPilote.setText("Etablissement pilote");
        lbEtablissementPilote.setName("lbEtablissementPilote");
        sNPanel1.add(lbEtablissementPilote, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snEtablissementPilote ----
        snEtablissementPilote.setComponentPopupMenu(null);
        snEtablissementPilote.setMaximumSize(new Dimension(300, 30));
        snEtablissementPilote.setMinimumSize(new Dimension(300, 30));
        snEtablissementPilote.setPreferredSize(new Dimension(300, 30));
        snEtablissementPilote.setName("snEtablissementPilote");
        sNPanel1.add(snEtablissementPilote, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbRechercheSecurite ----
        lbRechercheSecurite.setText("ou recherche sur s\u00e9curit\u00e9 utilisateur");
        lbRechercheSecurite.setMaximumSize(new Dimension(250, 30));
        lbRechercheSecurite.setMinimumSize(new Dimension(250, 30));
        lbRechercheSecurite.setPreferredSize(new Dimension(250, 30));
        lbRechercheSecurite.setName("lbRechercheSecurite");
        sNPanel1.add(lbRechercheSecurite, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- DGIN1 ----
        DGIN1.setComponentPopupMenu(null);
        DGIN1.setMaximumSize(new Dimension(40, 30));
        DGIN1.setMinimumSize(new Dimension(40, 30));
        DGIN1.setPreferredSize(new Dimension(40, 30));
        DGIN1.setName("DGIN1");
        sNPanel1.add(DGIN1, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelContenu1.add(sNPanel1,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
        tabbedPane1.setName("tabbedPane1");
        
        // ======== pnlGeneral ========
        {
          pnlGeneral.setName("pnlGeneral");
          pnlGeneral.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbDeviseLocale ----
          lbDeviseLocale.setText("Devise locale");
          lbDeviseLocale.setName("lbDeviseLocale");
          pnlGeneral.add(lbDeviseLocale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlValidite ========
          {
            pnlValidite.setName("pnlValidite");
            pnlValidite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlValidite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlValidite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlValidite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlValidite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- snDeviseLocale ----
            snDeviseLocale.setFont(new Font("sansserif", Font.PLAIN, 14));
            snDeviseLocale.setBackground(Color.white);
            snDeviseLocale.setName("snDeviseLocale");
            pnlValidite.add(snDeviseLocale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DGCGE ----
            DGCGE.setText("Sans d\u00e9cimale");
            DGCGE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DGCGE.setFont(new Font("sansserif", Font.PLAIN, 14));
            DGCGE.setName("DGCGE");
            pnlValidite.add(DGCGE, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlGeneral.add(pnlValidite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPhaseEuro ----
          lbPhaseEuro.setText("Phase d'avancement vers l'euro");
          lbPhaseEuro.setMaximumSize(new Dimension(250, 30));
          lbPhaseEuro.setMinimumSize(new Dimension(250, 30));
          lbPhaseEuro.setPreferredSize(new Dimension(250, 30));
          lbPhaseEuro.setName("lbPhaseEuro");
          pnlGeneral.add(lbPhaseEuro, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlRemiseMax ========
          {
            pnlRemiseMax.setName("pnlRemiseMax");
            pnlRemiseMax.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemiseMax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRemiseMax.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemiseMax.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemiseMax.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- DGEUR ----
            DGEUR.setPreferredSize(new Dimension(40, 30));
            DGEUR.setMinimumSize(new Dimension(40, 30));
            DGEUR.setFont(new Font("sansserif", Font.PLAIN, 14));
            DGEUR.setName("DGEUR");
            pnlRemiseMax.add(DGEUR, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlGeneral.add(pnlRemiseMax, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlExercices ========
          {
            pnlExercices.setTitre("Exercices");
            pnlExercices.setName("pnlExercices");
            pnlExercices.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlExercices.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlExercices.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlExercices.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlExercices.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== sNPanel4 ========
            {
              sNPanel4.setName("sNPanel4");
              sNPanel4.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel4.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel4.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel4.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbExerciceDemarrage ----
              lbExerciceDemarrage.setText("Exercice de d\u00e9marrage du");
              lbExerciceDemarrage.setMaximumSize(new Dimension(240, 30));
              lbExerciceDemarrage.setMinimumSize(new Dimension(240, 30));
              lbExerciceDemarrage.setPreferredSize(new Dimension(240, 30));
              lbExerciceDemarrage.setName("lbExerciceDemarrage");
              sNPanel4.add(lbExerciceDemarrage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGDEBX ----
              DGDEBX.setTypeSaisie(6);
              DGDEBX.setMaximumSize(new Dimension(100, 30));
              DGDEBX.setMinimumSize(new Dimension(100, 30));
              DGDEBX.setPreferredSize(new Dimension(100, 30));
              DGDEBX.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGDEBX.setName("DGDEBX");
              sNPanel4.add(DGDEBX, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu ----
              lbAu.setText("au");
              lbAu.setMaximumSize(new Dimension(40, 30));
              lbAu.setMinimumSize(new Dimension(40, 30));
              lbAu.setPreferredSize(new Dimension(40, 30));
              lbAu.setName("lbAu");
              sNPanel4.add(lbAu, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGFINX ----
              DGFINX.setTypeSaisie(6);
              DGFINX.setMaximumSize(new Dimension(100, 30));
              DGFINX.setMinimumSize(new Dimension(100, 30));
              DGFINX.setPreferredSize(new Dimension(100, 30));
              DGFINX.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGFINX.setName("DGFINX");
              sNPanel4.add(DGFINX, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlExercices.add(sNPanel4, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== sNPanel5 ========
            {
              sNPanel5.setName("sNPanel5");
              sNPanel5.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel5.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel5.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbExerciceEnCours ----
              lbExerciceEnCours.setText("Exercice en cours du");
              lbExerciceEnCours.setMaximumSize(new Dimension(240, 30));
              lbExerciceEnCours.setMinimumSize(new Dimension(240, 30));
              lbExerciceEnCours.setPreferredSize(new Dimension(240, 30));
              lbExerciceEnCours.setName("lbExerciceEnCours");
              sNPanel5.add(lbExerciceEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGDE1X ----
              DGDE1X.setTypeSaisie(6);
              DGDE1X.setMaximumSize(new Dimension(100, 30));
              DGDE1X.setMinimumSize(new Dimension(100, 30));
              DGDE1X.setPreferredSize(new Dimension(100, 30));
              DGDE1X.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGDE1X.setName("DGDE1X");
              sNPanel5.add(DGDE1X, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu2 ----
              lbAu2.setText("au");
              lbAu2.setMaximumSize(new Dimension(40, 30));
              lbAu2.setMinimumSize(new Dimension(40, 30));
              lbAu2.setPreferredSize(new Dimension(40, 30));
              lbAu2.setName("lbAu2");
              sNPanel5.add(lbAu2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGFE1X ----
              DGFE1X.setTypeSaisie(6);
              DGFE1X.setMaximumSize(new Dimension(100, 30));
              DGFE1X.setMinimumSize(new Dimension(100, 30));
              DGFE1X.setPreferredSize(new Dimension(100, 30));
              DGFE1X.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGFE1X.setName("DGFE1X");
              sNPanel5.add(DGFE1X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbMoisEnCours ----
              lbMoisEnCours.setText("Mois en cours ");
              lbMoisEnCours.setMaximumSize(new Dimension(200, 30));
              lbMoisEnCours.setMinimumSize(new Dimension(200, 30));
              lbMoisEnCours.setPreferredSize(new Dimension(200, 30));
              lbMoisEnCours.setFont(lbMoisEnCours.getFont().deriveFont(lbMoisEnCours.getFont().getStyle() | Font.BOLD));
              lbMoisEnCours.setName("lbMoisEnCours");
              sNPanel5.add(lbMoisEnCours, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGDECX ----
              DGDECX.setTypeSaisie(6);
              DGDECX.setMaximumSize(new Dimension(100, 30));
              DGDECX.setMinimumSize(new Dimension(100, 30));
              DGDECX.setPreferredSize(new Dimension(100, 30));
              DGDECX.setFont(new Font("sansserif", Font.BOLD, 14));
              DGDECX.setName("DGDECX");
              sNPanel5.add(DGDECX, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlExercices.add(sNPanel5, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== sNPanel6 ========
            {
              sNPanel6.setName("sNPanel6");
              sNPanel6.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel6.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) sNPanel6.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel6.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel6.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbExerciceSuivant ----
              lbExerciceSuivant.setText("Exercice suivant du");
              lbExerciceSuivant.setMaximumSize(new Dimension(240, 30));
              lbExerciceSuivant.setMinimumSize(new Dimension(240, 30));
              lbExerciceSuivant.setPreferredSize(new Dimension(240, 30));
              lbExerciceSuivant.setName("lbExerciceSuivant");
              sNPanel6.add(lbExerciceSuivant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGDE2X ----
              DGDE2X.setTypeSaisie(6);
              DGDE2X.setMaximumSize(new Dimension(100, 30));
              DGDE2X.setMinimumSize(new Dimension(100, 30));
              DGDE2X.setPreferredSize(new Dimension(100, 30));
              DGDE2X.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGDE2X.setName("DGDE2X");
              sNPanel6.add(DGDE2X, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbAu3 ----
              lbAu3.setText("au");
              lbAu3.setMaximumSize(new Dimension(40, 30));
              lbAu3.setMinimumSize(new Dimension(40, 30));
              lbAu3.setPreferredSize(new Dimension(40, 30));
              lbAu3.setName("lbAu3");
              sNPanel6.add(lbAu3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGFE2X ----
              DGFE2X.setTypeSaisie(6);
              DGFE2X.setMaximumSize(new Dimension(100, 30));
              DGFE2X.setMinimumSize(new Dimension(100, 30));
              DGFE2X.setPreferredSize(new Dimension(100, 30));
              DGFE2X.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGFE2X.setName("DGFE2X");
              sNPanel6.add(DGFE2X, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlExercices.add(sNPanel6, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGeneral.add(pnlExercices, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
        }
        tabbedPane1.addTab("G\u00e9n\u00e9ral", pnlGeneral);
        
        // ======== pnlDocuments ========
        {
          pnlDocuments.setName("pnlDocuments");
          pnlDocuments.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDocuments.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDocuments.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDocuments.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDocuments.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== sNPanel3 ========
          {
            sNPanel3.setName("sNPanel3");
            sNPanel3.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel3.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel3.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== sNPanel2 ========
            {
              sNPanel2.setName("sNPanel2");
              sNPanel2.setLayout(new GridBagLayout());
              ((GridBagLayout) sNPanel2.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) sNPanel2.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) sNPanel2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== sNPanel8 ========
              {
                sNPanel8.setName("sNPanel8");
                sNPanel8.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel8.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) sNPanel8.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel8.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel8.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- lbCoefNormal ----
                lbCoefNormal.setText("Coefficient normal");
                lbCoefNormal.setMaximumSize(new Dimension(300, 30));
                lbCoefNormal.setMinimumSize(new Dimension(300, 30));
                lbCoefNormal.setPreferredSize(new Dimension(300, 30));
                lbCoefNormal.setName("lbCoefNormal");
                sNPanel8.add(lbCoefNormal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- DGCMA ----
                DGCMA.setComponentPopupMenu(null);
                DGCMA.setMinimumSize(new Dimension(70, 30));
                DGCMA.setPreferredSize(new Dimension(70, 30));
                DGCMA.setMaximumSize(new Dimension(70, 30));
                DGCMA.setFont(new Font("sansserif", Font.PLAIN, 14));
                DGCMA.setName("DGCMA");
                sNPanel8.add(DGCMA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbJourReleve ----
                lbJourReleve.setText("Jour de relev\u00e9");
                lbJourReleve.setName("lbJourReleve");
                sNPanel8.add(lbJourReleve, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- DGJRL ----
                DGJRL.setComponentPopupMenu(null);
                DGJRL.setPreferredSize(new Dimension(40, 30));
                DGJRL.setMinimumSize(new Dimension(40, 30));
                DGJRL.setMaximumSize(new Dimension(40, 30));
                DGJRL.setFont(new Font("sansserif", Font.PLAIN, 14));
                DGJRL.setName("DGJRL");
                sNPanel8.add(DGJRL, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbJourConservation ----
                lbJourConservation.setText("Nombre de jours de conservation des prix flash");
                lbJourConservation.setMaximumSize(new Dimension(350, 30));
                lbJourConservation.setMinimumSize(new Dimension(350, 30));
                lbJourConservation.setPreferredSize(new Dimension(350, 30));
                lbJourConservation.setName("lbJourConservation");
                sNPanel8.add(lbJourConservation, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- DGPXFL ----
                DGPXFL.setComponentPopupMenu(null);
                DGPXFL.setPreferredSize(new Dimension(40, 30));
                DGPXFL.setMinimumSize(new Dimension(40, 30));
                DGPXFL.setMaximumSize(new Dimension(40, 30));
                DGPXFL.setFont(new Font("sansserif", Font.PLAIN, 14));
                DGPXFL.setName("DGPXFL");
                sNPanel8.add(DGPXFL, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              sNPanel2.add(sNPanel8, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== sNPanel7 ========
              {
                sNPanel7.setName("sNPanel7");
                sNPanel7.setLayout(new GridBagLayout());
                ((GridBagLayout) sNPanel7.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) sNPanel7.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) sNPanel7.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) sNPanel7.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- lbMontantMax ----
                lbMontantMax.setText("Montant maximum r\u00e9glable en esp\u00e8ces");
                lbMontantMax.setMaximumSize(new Dimension(300, 30));
                lbMontantMax.setMinimumSize(new Dimension(300, 30));
                lbMontantMax.setPreferredSize(new Dimension(300, 30));
                lbMontantMax.setName("lbMontantMax");
                sNPanel7.add(lbMontantMax, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- DGMTES ----
                DGMTES.setComponentPopupMenu(null);
                DGMTES.setMinimumSize(new Dimension(90, 30));
                DGMTES.setPreferredSize(new Dimension(90, 30));
                DGMTES.setMaximumSize(new Dimension(90, 30));
                DGMTES.setFont(new Font("sansserif", Font.PLAIN, 14));
                DGMTES.setName("DGMTES");
                sNPanel7.add(DGMTES, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              sNPanel2.add(sNPanel7, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanel3.add(sNPanel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDocuments.add(sNPanel3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTaxes ========
          {
            pnlTaxes.setTitre("Taxes");
            pnlTaxes.setName("pnlTaxes");
            pnlTaxes.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTaxes.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTaxes.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTaxes.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTaxes.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlTVA ========
            {
              pnlTVA.setName("pnlTVA");
              pnlTVA.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlTVA.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlTVA.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlTVA.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlTVA.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbTVA ----
              lbTVA.setText("Taux de TVA");
              lbTVA.setName("lbTVA");
              pnlTVA.add(lbTVA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCodeTVA1 ----
              lbCodeTVA1.setText("1");
              lbCodeTVA1.setMaximumSize(new Dimension(60, 30));
              lbCodeTVA1.setMinimumSize(new Dimension(60, 30));
              lbCodeTVA1.setPreferredSize(new Dimension(60, 30));
              lbCodeTVA1.setName("lbCodeTVA1");
              pnlTVA.add(lbCodeTVA1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGT01 ----
              DGT01.setComponentPopupMenu(null);
              DGT01.setMinimumSize(new Dimension(60, 30));
              DGT01.setPreferredSize(new Dimension(60, 30));
              DGT01.setMaximumSize(new Dimension(60, 30));
              DGT01.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGT01.setName("DGT01");
              pnlTVA.add(DGT01, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCodeTVA2 ----
              lbCodeTVA2.setText("2");
              lbCodeTVA2.setMaximumSize(new Dimension(60, 30));
              lbCodeTVA2.setMinimumSize(new Dimension(60, 30));
              lbCodeTVA2.setPreferredSize(new Dimension(60, 30));
              lbCodeTVA2.setName("lbCodeTVA2");
              pnlTVA.add(lbCodeTVA2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGT02 ----
              DGT02.setComponentPopupMenu(null);
              DGT02.setMinimumSize(new Dimension(60, 30));
              DGT02.setPreferredSize(new Dimension(60, 30));
              DGT02.setMaximumSize(new Dimension(60, 30));
              DGT02.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGT02.setName("DGT02");
              pnlTVA.add(DGT02, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCodeTVA3 ----
              lbCodeTVA3.setText("3");
              lbCodeTVA3.setMaximumSize(new Dimension(60, 30));
              lbCodeTVA3.setMinimumSize(new Dimension(60, 30));
              lbCodeTVA3.setPreferredSize(new Dimension(60, 30));
              lbCodeTVA3.setName("lbCodeTVA3");
              pnlTVA.add(lbCodeTVA3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGT03 ----
              DGT03.setComponentPopupMenu(null);
              DGT03.setMinimumSize(new Dimension(60, 30));
              DGT03.setPreferredSize(new Dimension(60, 30));
              DGT03.setMaximumSize(new Dimension(60, 30));
              DGT03.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGT03.setName("DGT03");
              pnlTVA.add(DGT03, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCodeTVA4 ----
              lbCodeTVA4.setText("4");
              lbCodeTVA4.setMaximumSize(new Dimension(60, 30));
              lbCodeTVA4.setMinimumSize(new Dimension(60, 30));
              lbCodeTVA4.setPreferredSize(new Dimension(60, 30));
              lbCodeTVA4.setName("lbCodeTVA4");
              pnlTVA.add(lbCodeTVA4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGT04 ----
              DGT04.setComponentPopupMenu(null);
              DGT04.setMinimumSize(new Dimension(60, 30));
              DGT04.setPreferredSize(new Dimension(60, 30));
              DGT04.setMaximumSize(new Dimension(60, 30));
              DGT04.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGT04.setName("DGT04");
              pnlTVA.add(DGT04, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCodeTVA5 ----
              lbCodeTVA5.setText("5");
              lbCodeTVA5.setMaximumSize(new Dimension(60, 30));
              lbCodeTVA5.setMinimumSize(new Dimension(60, 30));
              lbCodeTVA5.setPreferredSize(new Dimension(60, 30));
              lbCodeTVA5.setName("lbCodeTVA5");
              pnlTVA.add(lbCodeTVA5, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGT05 ----
              DGT05.setComponentPopupMenu(null);
              DGT05.setMinimumSize(new Dimension(60, 30));
              DGT05.setPreferredSize(new Dimension(60, 30));
              DGT05.setMaximumSize(new Dimension(60, 30));
              DGT05.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGT05.setName("DGT05");
              pnlTVA.add(DGT05, new GridBagConstraints(10, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbCodeTVA6 ----
              lbCodeTVA6.setText("6");
              lbCodeTVA6.setMaximumSize(new Dimension(60, 30));
              lbCodeTVA6.setMinimumSize(new Dimension(60, 30));
              lbCodeTVA6.setPreferredSize(new Dimension(60, 30));
              lbCodeTVA6.setName("lbCodeTVA6");
              pnlTVA.add(lbCodeTVA6, new GridBagConstraints(11, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGT06 ----
              DGT06.setComponentPopupMenu(null);
              DGT06.setMinimumSize(new Dimension(60, 30));
              DGT06.setPreferredSize(new Dimension(60, 30));
              DGT06.setMaximumSize(new Dimension(60, 30));
              DGT06.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGT06.setName("DGT06");
              pnlTVA.add(DGT06, new GridBagConstraints(12, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlTaxes.add(pnlTVA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlParafiscale ========
            {
              pnlParafiscale.setName("pnlParafiscale");
              pnlParafiscale.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlParafiscale.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlParafiscale.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlParafiscale.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlParafiscale.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- sNLabelChamp3 ----
              sNLabelChamp3.setText("Taxe parafiscale");
              sNLabelChamp3.setName("sNLabelChamp3");
              pnlParafiscale.add(sNLabelChamp3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- sNLabelChamp4 ----
              sNLabelChamp4.setText("Taux");
              sNLabelChamp4.setMaximumSize(new Dimension(60, 30));
              sNLabelChamp4.setMinimumSize(new Dimension(60, 30));
              sNLabelChamp4.setPreferredSize(new Dimension(60, 30));
              sNLabelChamp4.setName("sNLabelChamp4");
              pnlParafiscale.add(sNLabelChamp4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGTPF ----
              DGTPF.setComponentPopupMenu(null);
              DGTPF.setMinimumSize(new Dimension(60, 30));
              DGTPF.setPreferredSize(new Dimension(60, 30));
              DGTPF.setMaximumSize(new Dimension(60, 30));
              DGTPF.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGTPF.setName("DGTPF");
              pnlParafiscale.add(DGTPF, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- sNLabelChamp5 ----
              sNLabelChamp5.setText("% base");
              sNLabelChamp5.setMaximumSize(new Dimension(60, 30));
              sNLabelChamp5.setMinimumSize(new Dimension(60, 30));
              sNLabelChamp5.setPreferredSize(new Dimension(60, 30));
              sNLabelChamp5.setName("sNLabelChamp5");
              pnlParafiscale.add(sNLabelChamp5, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGRBP ----
              DGRBP.setComponentPopupMenu(null);
              DGRBP.setMinimumSize(new Dimension(60, 30));
              DGRBP.setPreferredSize(new Dimension(60, 30));
              DGRBP.setMaximumSize(new Dimension(60, 30));
              DGRBP.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGRBP.setName("DGRBP");
              pnlParafiscale.add(DGRBP, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGIN2 ----
              DGIN2.setText("Sp\u00e9cial");
              DGIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DGIN2.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGIN2.setName("DGIN2");
              pnlParafiscale.add(DGIN2, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- sNLabelChamp6 ----
              sNLabelChamp6.setText("Arrondi");
              sNLabelChamp6.setName("sNLabelChamp6");
              pnlParafiscale.add(sNLabelChamp6, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- DGRON ----
              DGRON.setModel(new DefaultComboBoxModel(new String[] { "Pas d'arrondi", "0,05", "0,10", "0,25", "0,50", "1,00", "10,00" }));
              DGRON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DGRON.setFont(new Font("sansserif", Font.PLAIN, 14));
              DGRON.setName("DGRON");
              pnlParafiscale.add(DGRON, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlTaxes.add(pnlParafiscale, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDocuments.add(pnlTaxes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlCNV ========
          {
            pnlCNV.setTitre("Condition de vente");
            pnlCNV.setName("pnlCNV");
            pnlCNV.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCNV.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCNV.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCNV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlCNV.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbGenerale ----
            lbGenerale.setText("G\u00e9n\u00e9rale");
            lbGenerale.setName("lbGenerale");
            pnlCNV.add(lbGenerale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DGCNG ----
            DGCNG.setComponentPopupMenu(null);
            DGCNG.setMinimumSize(new Dimension(70, 30));
            DGCNG.setPreferredSize(new Dimension(70, 30));
            DGCNG.setMaximumSize(new Dimension(70, 30));
            DGCNG.setFont(new Font("sansserif", Font.PLAIN, 14));
            DGCNG.setName("DGCNG");
            pnlCNV.add(DGCNG, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbQuantitative ----
            lbQuantitative.setText("Quantitative");
            lbQuantitative.setName("lbQuantitative");
            pnlCNV.add(lbQuantitative, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DGTC1 ----
            DGTC1.setModel(new DefaultComboBoxModel(new String[] { "en unit\u00e9 de vente", "en unit\u00e9 de conditionnement",
                "multipli\u00e9es par 100", "au col", "\u00e0 la tonne", "au cumul palette" }));
            DGTC1.setComponentPopupMenu(null);
            DGTC1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DGTC1.setFont(new Font("sansserif", Font.PLAIN, 14));
            DGTC1.setName("DGTC1");
            pnlCNV.add(DGTC1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbCumul ----
            lbCumul.setText("Cumul");
            lbCumul.setName("lbCumul");
            pnlCNV.add(lbCumul, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- DGTC2 ----
            DGTC2.setModel(new DefaultComboBoxModel(new String[] { "", "quantit\u00e9s", "quantit\u00e9s par famille" }));
            DGTC2.setComponentPopupMenu(null);
            DGTC2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            DGTC2.setFont(new Font("sansserif", Font.PLAIN, 14));
            DGTC2.setName("DGTC2");
            pnlCNV.add(DGTC2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPromo ----
            lbPromo.setText("Promotionelle");
            lbPromo.setName("lbPromo");
            pnlCNV.add(lbPromo, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- DGCNP ----
            DGCNP.setComponentPopupMenu(null);
            DGCNP.setFont(new Font("sansserif", Font.PLAIN, 14));
            DGCNP.setName("DGCNP");
            pnlCNV.add(DGCNP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDocuments.add(pnlCNV, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        tabbedPane1.addTab("Ventes", pnlDocuments);
      }
      sNPanelContenu1.add(tabbedPane1,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelContenu1, BorderLayout.CENTER);
    
    // ---- INDIND ----
    INDIND.setPreferredSize(new Dimension(50, 30));
    INDIND.setMinimumSize(new Dimension(50, 30));
    INDIND.setMaximumSize(new Dimension(50, 30));
    INDIND.setFont(new Font("sansserif", Font.PLAIN, 14));
    INDIND.setName("INDIND");
    
    // ---- lbCodeCategorie ----
    lbCodeCategorie.setText("code");
    lbCodeCategorie.setName("lbCodeCategorie");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlbandeau;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel pnlPersonnalisation;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbLibelle;
  private XRiTextField PG01LI;
  private SNPanel sNPanel1;
  private SNLabelChamp lbNom;
  private XRiTextField DGNOM;
  private SNLabelChamp lbICS;
  private XRiTextField DGICS;
  private SNLabelChamp lbComplement;
  private XRiTextField DGCPL;
  private SNLabelChamp lbMagasinGeneral;
  private SNMagasin snMagasinGeneral;
  private SNLabelChamp lbEtablissementPilote;
  private SNEtablissement snEtablissementPilote;
  private SNLabelChamp lbRechercheSecurite;
  private XRiTextField DGIN1;
  private JTabbedPane tabbedPane1;
  private SNPanelContenu pnlGeneral;
  private SNLabelChamp lbDeviseLocale;
  private SNPanel pnlValidite;
  private SNDevise snDeviseLocale;
  private XRiCheckBox DGCGE;
  private SNLabelChamp lbPhaseEuro;
  private SNPanel pnlRemiseMax;
  private XRiTextField DGEUR;
  private SNPanelTitre pnlExercices;
  private SNPanel sNPanel4;
  private SNLabelChamp lbExerciceDemarrage;
  private XRiCalendrier DGDEBX;
  private SNLabelChamp lbAu;
  private XRiCalendrier DGFINX;
  private SNPanel sNPanel5;
  private SNLabelChamp lbExerciceEnCours;
  private XRiCalendrier DGDE1X;
  private SNLabelChamp lbAu2;
  private XRiCalendrier DGFE1X;
  private SNLabelChamp lbMoisEnCours;
  private XRiCalendrier DGDECX;
  private SNPanel sNPanel6;
  private SNLabelChamp lbExerciceSuivant;
  private XRiCalendrier DGDE2X;
  private SNLabelChamp lbAu3;
  private XRiCalendrier DGFE2X;
  private SNPanelContenu pnlDocuments;
  private SNPanel sNPanel3;
  private SNPanel sNPanel2;
  private SNPanel sNPanel8;
  private SNLabelChamp lbCoefNormal;
  private XRiTextField DGCMA;
  private SNLabelChamp lbJourReleve;
  private XRiTextField DGJRL;
  private SNLabelChamp lbJourConservation;
  private XRiTextField DGPXFL;
  private SNPanel sNPanel7;
  private SNLabelChamp lbMontantMax;
  private XRiTextField DGMTES;
  private SNPanelTitre pnlTaxes;
  private SNPanel pnlTVA;
  private SNLabelChamp lbTVA;
  private SNLabelChamp lbCodeTVA1;
  private XRiTextField DGT01;
  private SNLabelChamp lbCodeTVA2;
  private XRiTextField DGT02;
  private SNLabelChamp lbCodeTVA3;
  private XRiTextField DGT03;
  private SNLabelChamp lbCodeTVA4;
  private XRiTextField DGT04;
  private SNLabelChamp lbCodeTVA5;
  private XRiTextField DGT05;
  private SNLabelChamp lbCodeTVA6;
  private XRiTextField DGT06;
  private SNPanel pnlParafiscale;
  private SNLabelChamp sNLabelChamp3;
  private SNLabelChamp sNLabelChamp4;
  private XRiTextField DGTPF;
  private SNLabelChamp sNLabelChamp5;
  private XRiTextField DGRBP;
  private XRiCheckBox DGIN2;
  private SNLabelChamp sNLabelChamp6;
  private XRiComboBox DGRON;
  private SNPanelTitre pnlCNV;
  private SNLabelChamp lbGenerale;
  private XRiTextField DGCNG;
  private SNLabelChamp lbQuantitative;
  private XRiComboBox DGTC1;
  private SNLabelChamp lbCumul;
  private XRiComboBox DGTC2;
  private SNLabelChamp lbPromo;
  private XRiTextField DGCNP;
  private XRiTextField INDIND;
  private SNLabelChamp lbCodeCategorie;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
