
package ri.serien.libecranrpg.rstf.RSTFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RSTFM_P1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public RSTFM_P1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    RFIC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFIC@")).trim());
    CURLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CURLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REP", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REP", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_58 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_53 = new JXTitledSeparator();
    OBJ_56 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_61 = new JLabel();
    UNITE = new XRiTextField();
    RFIC = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_74 = new JLabel();
    label1 = new JLabel();
    OBJ_64 = new JLabel();
    CURLIB = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(730, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- OBJ_58 ----
          OBJ_58.setName("OBJ_58");

          //---- OBJ_68 ----
          OBJ_68.setName("OBJ_68");

          //---- OBJ_53 ----
          OBJ_53.setName("OBJ_53");

          //---- OBJ_56 ----
          OBJ_56.setName("OBJ_56");

          //---- OBJ_55 ----
          OBJ_55.setName("OBJ_55");

          //---- OBJ_67 ----
          OBJ_67.setName("OBJ_67");

          //---- OBJ_61 ----
          OBJ_61.setName("OBJ_61");

          //---- UNITE ----
          UNITE.setComponentPopupMenu(BTD);
          UNITE.setName("UNITE");

          //---- RFIC ----
          RFIC.setName("RFIC");

          //---- OBJ_73 ----
          OBJ_73.setName("OBJ_73");

          //---- OBJ_74 ----
          OBJ_74.setName("OBJ_74");

          //---- label1 ----
          label1.setName("label1");

          //---- OBJ_64 ----
          OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD, OBJ_64.getFont().getSize() + 1f));
          OBJ_64.setForeground(new Color(255, 51, 51));
          OBJ_64.setName("OBJ_64");

          //---- CURLIB ----
          CURLIB.setOpaque(false);
          CURLIB.setForeground(Color.red);
          CURLIB.setName("CURLIB");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(CURLIB, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 680, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 421, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 675, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 561, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 543, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(520, 520, 520)
                    .addComponent(RFIC, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 525, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 543, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, 114, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(CURLIB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(28, 28, 28)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(22, 22, 22)
                    .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(RFIC, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGap(15, 15, 15)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(OBJ_74, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(5, 5, 5)
                    .addComponent(OBJ_61, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(UNITE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(label1))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JLabel OBJ_58;
  private JLabel OBJ_68;
  private JXTitledSeparator OBJ_53;
  private JLabel OBJ_56;
  private JLabel OBJ_55;
  private JLabel OBJ_67;
  private JLabel OBJ_61;
  private XRiTextField UNITE;
  private JLabel RFIC;
  private JLabel OBJ_73;
  private JLabel OBJ_74;
  private JLabel label1;
  private JLabel OBJ_64;
  private RiZoneSortie CURLIB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
