
package ri.serien.libecranrpg.tgpm.TGPM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TGPM13FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM13FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ATNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATNOM@")).trim());
    ATRSP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATRSP@")).trim());
    ATTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATTEL@")).trim());
    LNTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNTYP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Lignes de nomenclature"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    ATNOM = new RiZoneSortie();
    ATRSP = new RiZoneSortie();
    LNLIB = new XRiTextField();
    ATTEL = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_25 = new JLabel();
    LNORDR = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_30 = new JLabel();
    LNNL1 = new XRiTextField();
    LNNL2 = new XRiTextField();
    LNNL3 = new XRiTextField();
    LNTYP = new RiZoneSortie();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_38 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 215));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Informations techniques");
            riSousMenu_bt6.setToolTipText("Informations techniques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Remplacement ordre");
            riSousMenu_bt7.setToolTipText("Remplacement ordre");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- ATNOM ----
          ATNOM.setComponentPopupMenu(BTD);
          ATNOM.setText("@ATNOM@");
          ATNOM.setName("ATNOM");
          p_recup.add(ATNOM);
          ATNOM.setBounds(350, 27, 310, ATNOM.getPreferredSize().height);

          //---- ATRSP ----
          ATRSP.setComponentPopupMenu(BTD);
          ATRSP.setText("@ATRSP@");
          ATRSP.setName("ATRSP");
          p_recup.add(ATRSP);
          ATRSP.setBounds(350, 62, 310, ATRSP.getPreferredSize().height);

          //---- LNLIB ----
          LNLIB.setComponentPopupMenu(null);
          LNLIB.setName("LNLIB");
          p_recup.add(LNLIB);
          LNLIB.setBounds(19, 95, 310, LNLIB.getPreferredSize().height);

          //---- ATTEL ----
          ATTEL.setToolTipText("T\u00e9l\u00e9phone");
          ATTEL.setComponentPopupMenu(BTD);
          ATTEL.setText("@ATTEL@");
          ATTEL.setName("ATTEL");
          p_recup.add(ATTEL);
          ATTEL.setBounds(450, 97, 210, ATTEL.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Responsable");
          OBJ_37.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_37.setName("OBJ_37");
          p_recup.add(OBJ_37);
          OBJ_37.setBounds(250, 64, 89, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Adresse");
          OBJ_25.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(265, 30, 74, 20);

          //---- LNORDR ----
          LNORDR.setComponentPopupMenu(BTD);
          LNORDR.setName("LNORDR");
          p_recup.add(LNORDR);
          LNORDR.setBounds(190, 60, 60, LNORDR.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("Atelier");
          OBJ_31.setName("OBJ_31");
          p_recup.add(OBJ_31);
          OBJ_31.setBounds(198, 30, 52, 18);

          //---- OBJ_30 ----
          OBJ_30.setText("Type");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(152, 30, 43, 18);

          //---- LNNL1 ----
          LNNL1.setComponentPopupMenu(BTD);
          LNNL1.setName("LNNL1");
          p_recup.add(LNNL1);
          LNNL1.setBounds(19, 60, 36, LNNL1.getPreferredSize().height);

          //---- LNNL2 ----
          LNNL2.setComponentPopupMenu(BTD);
          LNNL2.setName("LNNL2");
          p_recup.add(LNNL2);
          LNNL2.setBounds(60, 60, 36, LNNL2.getPreferredSize().height);

          //---- LNNL3 ----
          LNNL3.setComponentPopupMenu(BTD);
          LNNL3.setName("LNNL3");
          p_recup.add(LNNL3);
          LNNL3.setBounds(100, 60, 36, LNNL3.getPreferredSize().height);

          //---- LNTYP ----
          LNTYP.setComponentPopupMenu(BTD);
          LNTYP.setText("@LNTYP@");
          LNTYP.setName("LNTYP");
          p_recup.add(LNTYP);
          LNTYP.setBounds(145, 62, 34, LNTYP.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("Nl1");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(25, 30, 31, 18);

          //---- OBJ_28 ----
          OBJ_28.setText("Nl2");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(65, 30, 31, 18);

          //---- OBJ_29 ----
          OBJ_29.setText("Nl3");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(105, 30, 31, 18);

          //---- OBJ_38 ----
          OBJ_38.setText("Tel\u00e9phone");
          OBJ_38.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(360, 99, 80, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(25, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(40, 40, 40)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(45, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie ATNOM;
  private RiZoneSortie ATRSP;
  private XRiTextField LNLIB;
  private RiZoneSortie ATTEL;
  private JLabel OBJ_37;
  private JLabel OBJ_25;
  private XRiTextField LNORDR;
  private JLabel OBJ_31;
  private JLabel OBJ_30;
  private XRiTextField LNNL1;
  private XRiTextField LNNL2;
  private XRiTextField LNNL3;
  private RiZoneSortie LNTYP;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_38;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
