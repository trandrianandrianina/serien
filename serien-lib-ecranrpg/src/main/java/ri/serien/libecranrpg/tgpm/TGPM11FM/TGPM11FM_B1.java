/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.tgpm.TGPM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.SNPhotoArticle;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class TGPM11FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] ENIN1_Value = { "", "1", "2", "3", };
  
  public TGPM11FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    ENIN1.setValeurs(ENIN1_Value, null);
    ENIN2.setValeursSelection("*", " ");
    ENIN4.setValeursSelection("1", " ");
    ENIN6.setValeursSelection("1", " ");
    
    TCI3.setIcon(lexique.chargerImage("images/icones_2281.gif", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ENNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENNUM@")).trim());
    ENETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENETB@")).trim());
    ENSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENSUF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC1@")).trim());
    OBJ_96.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC2@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    OBJ_118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    OBJ_122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIBR@")).trim());
    LIBTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTYP@")).trim());
    K44T5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@K44T5@")).trim());
    K44T6X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@K44T6X@")).trim());
    A1UNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1UNS@")).trim());
    textField1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPPG@")).trim());
    textField2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@STPPG@")).trim());
    textField3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETPPG@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPFG@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@STPFG@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETPFG@")).trim());
    riZoneSortie4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPG@")).trim());
    riZoneSortie5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@STPG@")).trim());
    riZoneSortie6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETPG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    

    WTI5.setVisible(ENZP5.isVisible());
    WTI4.setVisible(ENZP4.isVisible());
    WTI3.setVisible(ENZP3.isVisible());
    WTI2.setVisible(ENZP2.isVisible());
    WTI1.setVisible(ENZP1.isVisible());
    OBJ_81.setVisible(ENNIV.isVisible());
    ENNUM.setVisible(lexique.isPresent("ENNUM"));
    ENSUF.setVisible(lexique.isPresent("ENSUF"));
    OBJ_49.setVisible(ENNUM.isVisible());
    K44T5.setEnabled(lexique.isPresent("K44T5"));
    TCI3.setVisible(lexique.isPresent("TCI3"));
    OBJ_96.setVisible(lexique.isPresent("LMC2"));
    OBJ_88.setVisible(lexique.isPresent("LMC1"));
    A1LB3.setVisible(lexique.isPresent("A1LB3"));
    A1LB2.setVisible(lexique.isPresent("A1LB2"));
    A1LB1.setVisible(lexique.isPresent("A1LB1"));
    A1LIB.setVisible(lexique.isPresent("A1LIB"));
    OBJ_135.setVisible(ENIN1.isVisible());
    
    riSousMenu8.setEnabled(ENART.isVisible() && (lexique.getMode() != Lexical.MODE_CONSULTATION));
    riSousMenu12.setEnabled(lexique.isPresent("V06FO"));
    
    p_bpresentation.setCodeEtablissement(ENETB.getText());
    
    // Charger la photographie de l'article
    snPhotoArticle.chargerImageArticle(
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim(),
        ENART.getText(), lexique.isTrue("N51"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("ENART");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("V06FO");
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("TCI3", 0, "3");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_47 = new JLabel();
    ENDDVX = new XRiTextField();
    ENDFVX = new XRiTextField();
    OBJ_49 = new JLabel();
    OBJ_51 = new JLabel();
    ENNUM = new RiZoneSortie();
    ENETB = new RiZoneSortie();
    OBJ_54 = new JLabel();
    ENSUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    ENART = new XRiTextField();
    OBJ_75 = new JLabel();
    OBJ_70 = new JLabel();
    OBJ_81 = new JLabel();
    WFAM = new XRiTextField();
    ENNIV = new XRiTextField();
    xTitledPanel1 = new JXTitledPanel();
    ENLIB = new XRiTextField();
    ENCLA1 = new XRiTextField();
    ENCLA2 = new XRiTextField();
    OBJ_88 = new JLabel();
    OBJ_96 = new JLabel();
    OBJ_90 = new JLabel();
    ENZP1 = new XRiTextField();
    WTI2 = new JLabel();
    ENZP2 = new XRiTextField();
    WTI3 = new JLabel();
    ENZP3 = new XRiTextField();
    WTI4 = new JLabel();
    ENZP4 = new XRiTextField();
    WTI5 = new JLabel();
    ENZP5 = new XRiTextField();
    WTI1 = new JLabel();
    xTitledPanel2 = new JXTitledPanel();
    ENIN1 = new XRiComboBox();
    OBJ_133 = new JLabel();
    OBJ_135 = new JLabel();
    OBJ_118 = new RiZoneSortie();
    OBJ_122 = new RiZoneSortie();
    OBJ_128 = new JLabel();
    OBJ_116 = new JLabel();
    OBJ_119 = new JLabel();
    OBJ_123 = new JLabel();
    LIBTYP = new RiZoneSortie();
    OBJ_112 = new JLabel();
    ENQMI = new XRiTextField();
    ENQTE = new XRiTextField();
    K44T5 = new RiZoneSortie();
    OBJ_120 = new JLabel();
    K44T6X = new RiZoneSortie();
    ENMAG = new XRiTextField();
    A1UNS = new RiZoneSortie();
    ENIN6 = new XRiCheckBox();
    ENIN3 = new XRiTextField();
    ENIN4 = new XRiCheckBox();
    ENIN2 = new XRiCheckBox();
    xTitledPanel3 = new JXTitledPanel();
    label1 = new JLabel();
    textField1 = new RiZoneSortie();
    textField2 = new RiZoneSortie();
    textField3 = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    riZoneSortie1 = new RiZoneSortie();
    riZoneSortie2 = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    label5 = new JLabel();
    riZoneSortie4 = new RiZoneSortie();
    riZoneSortie5 = new RiZoneSortie();
    riZoneSortie6 = new RiZoneSortie();
    label6 = new JLabel();
    OBJ_148 = new JLabel();
    ENFPV = new XRiTextField();
    snPhotoArticle = new SNPhotoArticle();
    BTD = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    TCI3 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1050, 720));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ent\u00eate de nomenclature");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_47 ----
          OBJ_47.setText("Etablissement");
          OBJ_47.setName("OBJ_47");

          //---- ENDDVX ----
          ENDDVX.setComponentPopupMenu(null);
          ENDDVX.setName("ENDDVX");

          //---- ENDFVX ----
          ENDFVX.setComponentPopupMenu(null);
          ENDFVX.setName("ENDFVX");

          //---- OBJ_49 ----
          OBJ_49.setText("Num\u00e9ro");
          OBJ_49.setName("OBJ_49");

          //---- OBJ_51 ----
          OBJ_51.setText("Validit\u00e9 de");
          OBJ_51.setName("OBJ_51");

          //---- ENNUM ----
          ENNUM.setComponentPopupMenu(BTD);
          ENNUM.setOpaque(false);
          ENNUM.setText("@ENNUM@");
          ENNUM.setName("ENNUM");

          //---- ENETB ----
          ENETB.setComponentPopupMenu(BTD);
          ENETB.setOpaque(false);
          ENETB.setText("@ENETB@");
          ENETB.setName("ENETB");

          //---- OBJ_54 ----
          OBJ_54.setText("\u00e0");
          OBJ_54.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_54.setName("OBJ_54");

          //---- ENSUF ----
          ENSUF.setOpaque(false);
          ENSUF.setText("@ENSUF@");
          ENSUF.setName("ENSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(95, 95, 95)
                    .addComponent(ENETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                .addGap(30, 30, 30)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ENNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(ENSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ENDDVX, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(ENDFVX, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ENETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(ENNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(ENSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(ENDDVX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(ENDFVX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options article");
              riSousMenu_bt6.setToolTipText("Options article");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Recherche article");
              riSousMenu_bt8.setToolTipText("Calcul du prix de vente");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Calcul du prix de vente");
              riSousMenu_bt7.setToolTipText("Calcul du prix de vente");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Chemin critique");
              riSousMenu_bt9.setToolTipText("Chemin critique");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Informations techniques");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Options fin de traitement");
              riSousMenu_bt12.setToolTipText("Historique des versions");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Historique des versions");
              riSousMenu_bt13.setToolTipText("Historique des versions");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Gestion des documents li\u00e9s");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(850, 620));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel1.add(A1LIB);
            A1LIB.setBounds(325, 7, 321, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            panel1.add(A1LB1);
            A1LB1.setBounds(325, 32, 321, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setText("@A1LB2@");
            A1LB2.setName("A1LB2");
            panel1.add(A1LB2);
            A1LB2.setBounds(325, 57, 321, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setText("@A1LB3@");
            A1LB3.setName("A1LB3");
            panel1.add(A1LB3);
            A1LB3.setBounds(325, 82, 321, A1LB3.getPreferredSize().height);

            //---- ENART ----
            ENART.setComponentPopupMenu(BTD);
            ENART.setName("ENART");
            panel1.add(ENART);
            ENART.setBounds(105, 5, 210, ENART.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Famille");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(10, 33, 66, 22);

            //---- OBJ_70 ----
            OBJ_70.setText("Article");
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(10, 9, 66, 21);

            //---- OBJ_81 ----
            OBJ_81.setText("Niveau");
            OBJ_81.setName("OBJ_81");
            panel1.add(OBJ_81);
            OBJ_81.setBounds(10, 60, 66, 18);

            //---- WFAM ----
            WFAM.setComponentPopupMenu(BTD);
            WFAM.setName("WFAM");
            panel1.add(WFAM);
            WFAM.setBounds(105, 30, 40, WFAM.getPreferredSize().height);

            //---- ENNIV ----
            ENNIV.setComponentPopupMenu(BTD);
            ENNIV.setName("ENNIV");
            panel1.add(ENNIV);
            ENNIV.setBounds(105, 55, 28, ENNIV.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Description");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- ENLIB ----
            ENLIB.setComponentPopupMenu(null);
            ENLIB.setName("ENLIB");
            xTitledPanel1ContentContainer.add(ENLIB);
            ENLIB.setBounds(465, 10, 312, ENLIB.getPreferredSize().height);

            //---- ENCLA1 ----
            ENCLA1.setComponentPopupMenu(null);
            ENCLA1.setName("ENCLA1");
            xTitledPanel1ContentContainer.add(ENCLA1);
            ENCLA1.setBounds(180, 10, 210, ENCLA1.getPreferredSize().height);

            //---- ENCLA2 ----
            ENCLA2.setComponentPopupMenu(null);
            ENCLA2.setName("ENCLA2");
            xTitledPanel1ContentContainer.add(ENCLA2);
            ENCLA2.setBounds(180, 35, 210, ENCLA2.getPreferredSize().height);

            //---- OBJ_88 ----
            OBJ_88.setText("@LMC1@");
            OBJ_88.setName("OBJ_88");
            xTitledPanel1ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(15, 14, 160, 20);

            //---- OBJ_96 ----
            OBJ_96.setText("@LMC2@");
            OBJ_96.setName("OBJ_96");
            xTitledPanel1ContentContainer.add(OBJ_96);
            OBJ_96.setBounds(15, 39, 160, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Libell\u00e9");
            OBJ_90.setName("OBJ_90");
            xTitledPanel1ContentContainer.add(OBJ_90);
            OBJ_90.setBounds(410, 14, 56, 20);

            //---- ENZP1 ----
            ENZP1.setComponentPopupMenu(null);
            ENZP1.setName("ENZP1");
            xTitledPanel1ContentContainer.add(ENZP1);
            ENZP1.setBounds(447, 35, 34, ENZP1.getPreferredSize().height);

            //---- WTI2 ----
            WTI2.setText("@WTI2@");
            WTI2.setName("WTI2");
            xTitledPanel1ContentContainer.add(WTI2);
            WTI2.setBounds(484, 39, 34, 20);

            //---- ENZP2 ----
            ENZP2.setComponentPopupMenu(null);
            ENZP2.setName("ENZP2");
            xTitledPanel1ContentContainer.add(ENZP2);
            ENZP2.setBounds(521, 35, 34, ENZP2.getPreferredSize().height);

            //---- WTI3 ----
            WTI3.setText("@WTI3@");
            WTI3.setName("WTI3");
            xTitledPanel1ContentContainer.add(WTI3);
            WTI3.setBounds(558, 39, 34, 20);

            //---- ENZP3 ----
            ENZP3.setComponentPopupMenu(null);
            ENZP3.setName("ENZP3");
            xTitledPanel1ContentContainer.add(ENZP3);
            ENZP3.setBounds(595, 35, 34, ENZP3.getPreferredSize().height);

            //---- WTI4 ----
            WTI4.setText("@WTI4@");
            WTI4.setName("WTI4");
            xTitledPanel1ContentContainer.add(WTI4);
            WTI4.setBounds(632, 39, 34, 20);

            //---- ENZP4 ----
            ENZP4.setComponentPopupMenu(null);
            ENZP4.setName("ENZP4");
            xTitledPanel1ContentContainer.add(ENZP4);
            ENZP4.setBounds(669, 35, 34, ENZP4.getPreferredSize().height);

            //---- WTI5 ----
            WTI5.setText("@WTI5@");
            WTI5.setName("WTI5");
            xTitledPanel1ContentContainer.add(WTI5);
            WTI5.setBounds(706, 39, 34, 20);

            //---- ENZP5 ----
            ENZP5.setComponentPopupMenu(null);
            ENZP5.setName("ENZP5");
            xTitledPanel1ContentContainer.add(ENZP5);
            ENZP5.setBounds(743, 35, 34, ENZP5.getPreferredSize().height);

            //---- WTI1 ----
            WTI1.setText("@WTI1@");
            WTI1.setName("WTI1");
            xTitledPanel1ContentContainer.add(WTI1);
            WTI1.setBounds(410, 39, 34, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Fabrication");
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- ENIN1 ----
            ENIN1.setModel(new DefaultComboBoxModel(new String[] {
              "Pas de g\u00e9n\u00e9ration",
              "G\u00e9n\u00e9ration quantit\u00e9 globale",
              "Quantit\u00e9 globale-stock",
              "Quantit\u00e9 globale-stock dispo"
            }));
            ENIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ENIN1.setName("ENIN1");
            xTitledPanel2ContentContainer.add(ENIN1);
            ENIN1.setBounds(545, 86, 230, ENIN1.getPreferredSize().height);

            //---- OBJ_133 ----
            OBJ_133.setText("Disponibilit\u00e9 composants");
            OBJ_133.setName("OBJ_133");
            xTitledPanel2ContentContainer.add(OBJ_133);
            OBJ_133.setBounds(15, 89, 165, 20);

            //---- OBJ_135 ----
            OBJ_135.setText("G\u00e9n\u00e9ration auto sous-OF");
            OBJ_135.setName("OBJ_135");
            xTitledPanel2ContentContainer.add(OBJ_135);
            OBJ_135.setBounds(360, 89, 162, 20);

            //---- OBJ_118 ----
            OBJ_118.setText("@MALIBR@");
            OBJ_118.setName("OBJ_118");
            xTitledPanel2ContentContainer.add(OBJ_118);
            OBJ_118.setBounds(545, 12, 230, OBJ_118.getPreferredSize().height);

            //---- OBJ_122 ----
            OBJ_122.setText("@UNLIBR@");
            OBJ_122.setName("OBJ_122");
            xTitledPanel2ContentContainer.add(OBJ_122);
            OBJ_122.setBounds(545, 37, 230, OBJ_122.getPreferredSize().height);

            //---- OBJ_128 ----
            OBJ_128.setText("Type de nomenclature");
            OBJ_128.setName("OBJ_128");
            xTitledPanel2ContentContainer.add(OBJ_128);
            OBJ_128.setBounds(360, 64, 140, 20);

            //---- OBJ_116 ----
            OBJ_116.setText("Magasin d'entr\u00e9e");
            OBJ_116.setName("OBJ_116");
            xTitledPanel2ContentContainer.add(OBJ_116);
            OBJ_116.setBounds(360, 14, 140, 20);

            //---- OBJ_119 ----
            OBJ_119.setText("Quantit\u00e9 \u00e9conomique");
            OBJ_119.setName("OBJ_119");
            xTitledPanel2ContentContainer.add(OBJ_119);
            OBJ_119.setBounds(15, 39, 155, 20);

            //---- OBJ_123 ----
            OBJ_123.setText("Quantit\u00e9 fabricable");
            OBJ_123.setName("OBJ_123");
            xTitledPanel2ContentContainer.add(OBJ_123);
            OBJ_123.setBounds(15, 64, 165, 20);

            //---- LIBTYP ----
            LIBTYP.setText("@LIBTYP@");
            LIBTYP.setName("LIBTYP");
            xTitledPanel2ContentContainer.add(LIBTYP);
            LIBTYP.setBounds(545, 62, 230, LIBTYP.getPreferredSize().height);

            //---- OBJ_112 ----
            OBJ_112.setText("Quantit\u00e9 minimale");
            OBJ_112.setName("OBJ_112");
            xTitledPanel2ContentContainer.add(OBJ_112);
            OBJ_112.setBounds(15, 14, 165, 20);

            //---- ENQMI ----
            ENQMI.setComponentPopupMenu(null);
            ENQMI.setName("ENQMI");
            xTitledPanel2ContentContainer.add(ENQMI);
            ENQMI.setBounds(180, 10, 106, ENQMI.getPreferredSize().height);

            //---- ENQTE ----
            ENQTE.setComponentPopupMenu(null);
            ENQTE.setName("ENQTE");
            xTitledPanel2ContentContainer.add(ENQTE);
            ENQTE.setBounds(180, 35, 106, ENQTE.getPreferredSize().height);

            //---- K44T5 ----
            K44T5.setComponentPopupMenu(BTD);
            K44T5.setText("@K44T5@");
            K44T5.setHorizontalAlignment(SwingConstants.RIGHT);
            K44T5.setName("K44T5");
            xTitledPanel2ContentContainer.add(K44T5);
            K44T5.setBounds(182, 62, 102, K44T5.getPreferredSize().height);

            //---- OBJ_120 ----
            OBJ_120.setText("Unit\u00e9 de stock");
            OBJ_120.setName("OBJ_120");
            xTitledPanel2ContentContainer.add(OBJ_120);
            OBJ_120.setBounds(360, 39, 140, 20);

            //---- K44T6X ----
            K44T6X.setText("@K44T6X@");
            K44T6X.setName("K44T6X");
            xTitledPanel2ContentContainer.add(K44T6X);
            K44T6X.setBounds(182, 87, 76, K44T6X.getPreferredSize().height);

            //---- ENMAG ----
            ENMAG.setComponentPopupMenu(BTD);
            ENMAG.setName("ENMAG");
            xTitledPanel2ContentContainer.add(ENMAG);
            ENMAG.setBounds(500, 10, 34, ENMAG.getPreferredSize().height);

            //---- A1UNS ----
            A1UNS.setText("@A1UNS@");
            A1UNS.setComponentPopupMenu(null);
            A1UNS.setName("A1UNS");
            xTitledPanel2ContentContainer.add(A1UNS);
            A1UNS.setBounds(502, 37, 32, A1UNS.getPreferredSize().height);

            //---- ENIN6 ----
            ENIN6.setText("");
            ENIN6.setToolTipText("Quantit\u00e9 multiple forc\u00e9e par exc\u00e8s");
            ENIN6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ENIN6.setName("ENIN6");
            xTitledPanel2ContentContainer.add(ENIN6);
            ENIN6.setBounds(290, 41, 20, 17);

            //---- ENIN3 ----
            ENIN3.setComponentPopupMenu(BTD);
            ENIN3.setName("ENIN3");
            xTitledPanel2ContentContainer.add(ENIN3);
            ENIN3.setBounds(500, 60, 24, ENIN3.getPreferredSize().height);

            //---- ENIN4 ----
            ENIN4.setText("Acquisition auto");
            ENIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ENIN4.setName("ENIN4");
            xTitledPanel2ContentContainer.add(ENIN4);
            ENIN4.setBounds(13, 114, 147, 20);

            //---- ENIN2 ----
            ENIN2.setText("G\u00e9n\u00e9ration d'OF \u00e0 la commande");
            ENIN2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ENIN2.setName("ENIN2");
            xTitledPanel2ContentContainer.add(ENIN2);
            ENIN2.setBounds(358, 114, 219, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Chiffrage du prix de revient");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- label1 ----
            label1.setText("Actuel");
            label1.setName("label1");
            xTitledPanel3ContentContainer.add(label1);
            label1.setBounds(15, 34, 75, 20);

            //---- textField1 ----
            textField1.setText("@WTPPG@");
            textField1.setHorizontalAlignment(SwingConstants.RIGHT);
            textField1.setName("textField1");
            xTitledPanel3ContentContainer.add(textField1);
            textField1.setBounds(105, 30, 100, textField1.getPreferredSize().height);

            //---- textField2 ----
            textField2.setText("@STPPG@");
            textField2.setHorizontalAlignment(SwingConstants.RIGHT);
            textField2.setName("textField2");
            xTitledPanel3ContentContainer.add(textField2);
            textField2.setBounds(105, 55, 100, textField2.getPreferredSize().height);

            //---- textField3 ----
            textField3.setText("@ETPPG@");
            textField3.setHorizontalAlignment(SwingConstants.RIGHT);
            textField3.setName("textField3");
            xTitledPanel3ContentContainer.add(textField3);
            textField3.setBounds(105, 80, 100, textField3.getPreferredSize().height);

            //---- label2 ----
            label2.setText("Standard");
            label2.setName("label2");
            xTitledPanel3ContentContainer.add(label2);
            label2.setBounds(15, 59, 75, 20);

            //---- label3 ----
            label3.setText("Ecart");
            label3.setName("label3");
            xTitledPanel3ContentContainer.add(label3);
            label3.setBounds(15, 84, 75, 20);

            //---- label4 ----
            label4.setText("Proportionnel");
            label4.setName("label4");
            xTitledPanel3ContentContainer.add(label4);
            label4.setBounds(106, 10, 95, 16);

            //---- riZoneSortie1 ----
            riZoneSortie1.setText("@WTPFG@");
            riZoneSortie1.setHorizontalAlignment(SwingConstants.RIGHT);
            riZoneSortie1.setName("riZoneSortie1");
            xTitledPanel3ContentContainer.add(riZoneSortie1);
            riZoneSortie1.setBounds(220, 30, 100, riZoneSortie1.getPreferredSize().height);

            //---- riZoneSortie2 ----
            riZoneSortie2.setText("@STPFG@");
            riZoneSortie2.setHorizontalAlignment(SwingConstants.RIGHT);
            riZoneSortie2.setName("riZoneSortie2");
            xTitledPanel3ContentContainer.add(riZoneSortie2);
            riZoneSortie2.setBounds(220, 55, 100, riZoneSortie2.getPreferredSize().height);

            //---- riZoneSortie3 ----
            riZoneSortie3.setText("@ETPFG@");
            riZoneSortie3.setHorizontalAlignment(SwingConstants.RIGHT);
            riZoneSortie3.setName("riZoneSortie3");
            xTitledPanel3ContentContainer.add(riZoneSortie3);
            riZoneSortie3.setBounds(220, 80, 100, riZoneSortie3.getPreferredSize().height);

            //---- label5 ----
            label5.setText("Fixe");
            label5.setName("label5");
            xTitledPanel3ContentContainer.add(label5);
            label5.setBounds(221, 10, 95, 16);

            //---- riZoneSortie4 ----
            riZoneSortie4.setText("@WTPG@");
            riZoneSortie4.setHorizontalAlignment(SwingConstants.RIGHT);
            riZoneSortie4.setName("riZoneSortie4");
            xTitledPanel3ContentContainer.add(riZoneSortie4);
            riZoneSortie4.setBounds(335, 30, 100, riZoneSortie4.getPreferredSize().height);

            //---- riZoneSortie5 ----
            riZoneSortie5.setText("@STPG@");
            riZoneSortie5.setHorizontalAlignment(SwingConstants.RIGHT);
            riZoneSortie5.setName("riZoneSortie5");
            xTitledPanel3ContentContainer.add(riZoneSortie5);
            riZoneSortie5.setBounds(335, 55, 100, 24);

            //---- riZoneSortie6 ----
            riZoneSortie6.setText("@ETPG@");
            riZoneSortie6.setHorizontalAlignment(SwingConstants.RIGHT);
            riZoneSortie6.setName("riZoneSortie6");
            xTitledPanel3ContentContainer.add(riZoneSortie6);
            riZoneSortie6.setBounds(335, 80, 100, 24);

            //---- label6 ----
            label6.setText("Economique");
            label6.setName("label6");
            xTitledPanel3ContentContainer.add(label6);
            label6.setBounds(336, 10, 95, 16);

            //---- OBJ_148 ----
            OBJ_148.setText("Formule de calcul du prix de vente");
            OBJ_148.setName("OBJ_148");
            xTitledPanel3ContentContainer.add(OBJ_148);
            OBJ_148.setBounds(445, 82, 210, 20);

            //---- ENFPV ----
            ENFPV.setComponentPopupMenu(null);
            ENFPV.setName("ENFPV");
            xTitledPanel3ContentContainer.add(ENFPV);
            ENFPV.setBounds(660, 78, 40, ENFPV.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //---- snPhotoArticle ----
          snPhotoArticle.setComponentPopupMenu(BTD);
          snPhotoArticle.setPreferredSize(new Dimension(100, 100));
          snPhotoArticle.setBorder(null);
          snPhotoArticle.setOpaque(false);
          snPhotoArticle.setName("snPhotoArticle");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(29, 29, 29)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 660, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(snPhotoArticle, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 800, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 800, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(24, 24, 24)
                    .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 800, GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(14, 14, 14)
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(22, 22, 22)
                    .addComponent(snPhotoArticle, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)))
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
    }

    //---- TCI3 ----
    TCI3.setText("");
    TCI3.setToolTipText("Chiffrage");
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_47;
  private XRiTextField ENDDVX;
  private XRiTextField ENDFVX;
  private JLabel OBJ_49;
  private JLabel OBJ_51;
  private RiZoneSortie ENNUM;
  private RiZoneSortie ENETB;
  private JLabel OBJ_54;
  private RiZoneSortie ENSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private XRiTextField ENART;
  private JLabel OBJ_75;
  private JLabel OBJ_70;
  private JLabel OBJ_81;
  private XRiTextField WFAM;
  private XRiTextField ENNIV;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField ENLIB;
  private XRiTextField ENCLA1;
  private XRiTextField ENCLA2;
  private JLabel OBJ_88;
  private JLabel OBJ_96;
  private JLabel OBJ_90;
  private XRiTextField ENZP1;
  private JLabel WTI2;
  private XRiTextField ENZP2;
  private JLabel WTI3;
  private XRiTextField ENZP3;
  private JLabel WTI4;
  private XRiTextField ENZP4;
  private JLabel WTI5;
  private XRiTextField ENZP5;
  private JLabel WTI1;
  private JXTitledPanel xTitledPanel2;
  private XRiComboBox ENIN1;
  private JLabel OBJ_133;
  private JLabel OBJ_135;
  private RiZoneSortie OBJ_118;
  private RiZoneSortie OBJ_122;
  private JLabel OBJ_128;
  private JLabel OBJ_116;
  private JLabel OBJ_119;
  private JLabel OBJ_123;
  private RiZoneSortie LIBTYP;
  private JLabel OBJ_112;
  private XRiTextField ENQMI;
  private XRiTextField ENQTE;
  private RiZoneSortie K44T5;
  private JLabel OBJ_120;
  private RiZoneSortie K44T6X;
  private XRiTextField ENMAG;
  private RiZoneSortie A1UNS;
  private XRiCheckBox ENIN6;
  private XRiTextField ENIN3;
  private XRiCheckBox ENIN4;
  private XRiCheckBox ENIN2;
  private JXTitledPanel xTitledPanel3;
  private JLabel label1;
  private RiZoneSortie textField1;
  private RiZoneSortie textField2;
  private RiZoneSortie textField3;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private RiZoneSortie riZoneSortie1;
  private RiZoneSortie riZoneSortie2;
  private RiZoneSortie riZoneSortie3;
  private JLabel label5;
  private RiZoneSortie riZoneSortie4;
  private RiZoneSortie riZoneSortie5;
  private RiZoneSortie riZoneSortie6;
  private JLabel label6;
  private JLabel OBJ_148;
  private XRiTextField ENFPV;
  private SNPhotoArticle snPhotoArticle;
  private JPopupMenu BTD;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  private JButton TCI3;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
