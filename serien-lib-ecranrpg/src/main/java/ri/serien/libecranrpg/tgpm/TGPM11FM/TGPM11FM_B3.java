
package ri.serien.libecranrpg.tgpm.TGPM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class TGPM11FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM11FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ENNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENNUM@")).trim());
    ENETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENETB@")).trim());
    ENSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ENSUF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC1@")).trim());
    OBJ_87.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC2@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    LIBTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTYP@")).trim());
    OBJ_60.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@&&NUMIMGC@/@&&NBRIMAGE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    ENSUF.setVisible(lexique.isPresent("ENSUF"));
    ENIN3.setEnabled(lexique.isPresent("ENIN3"));
    AVA.setVisible(!lexique.HostFieldGetData("&&NBRIMAGE").trim().equalsIgnoreCase("0"));
    RET.setVisible(!lexique.HostFieldGetData("&&NBRIMAGE").trim().equalsIgnoreCase("0"));
    ENZP5.setEnabled(lexique.isPresent("ENZP5"));
    WTI5.setVisible(lexique.isPresent("WTI5"));
    ENZP4.setEnabled(lexique.isPresent("ENZP4"));
    WTI4.setVisible(lexique.isPresent("WTI4"));
    ENZP3.setEnabled(lexique.isPresent("ENZP3"));
    WTI3.setVisible(lexique.isPresent("WTI3"));
    ENZP2.setEnabled(lexique.isPresent("ENZP2"));
    WTI2.setVisible(lexique.isPresent("WTI2"));
    ENZP1.setEnabled(lexique.isPresent("ENZP1"));
    WTI1.setVisible(lexique.isPresent("WTI1"));
    ENNIV.setEnabled(lexique.isPresent("ENNIV"));
    ENETB.setVisible(lexique.isPresent("ENETB"));
    WFAM.setEnabled(lexique.isPresent("WFAM"));
    OBJ_60.setVisible(!lexique.HostFieldGetData("&&NBRIMAGE").trim().equalsIgnoreCase("0"));
    ENNUM.setVisible(lexique.isPresent("ENNUM"));
    OBJ_112.setVisible(!lexique.HostFieldGetData("WOBS").trim().equalsIgnoreCase(""));
    OBJ_111.setVisible(lexique.HostFieldGetData("WOBS").equalsIgnoreCase(""));
    BTNPH.setVisible(lexique.HostFieldGetData("&&BASCULE").equalsIgnoreCase(""));
    OBJ_87.setVisible(lexique.isPresent("LMC2"));
    OBJ_83.setVisible(lexique.isPresent("LMC1"));
    ENCLA2.setEnabled(lexique.isPresent("ENCLA2"));
    ENCLA1.setEnabled(lexique.isPresent("ENCLA1"));
    ENART.setEnabled(lexique.isPresent("ENART"));
    ENLIB.setEnabled(lexique.isPresent("ENLIB"));
    A1LB3.setVisible(lexique.isPresent("A1LB3"));
    A1LB2.setVisible(lexique.isPresent("A1LB2"));
    A1LB1.setVisible(lexique.isPresent("A1LB1"));
    A1LIB.setVisible(lexique.isPresent("A1LIB"));
    PHO.setVisible(!lexique.HostFieldGetData("&&BASCULE").trim().equalsIgnoreCase(""));
    
    // TODO Icones
    AVA.setIcon(lexique.chargerImage("images/icones_3100.gif", true));
    RET.setIcon(lexique.chargerImage("images/icones_3110.gif", true));
    OBJ_112.setIcon(lexique.chargerImage("images/icones_35.gif", true));
    OBJ_111.setIcon(lexique.chargerImage("images/icones_30.gif", true));
    BTNPH.setIcon(lexique.chargerImage("images/icones_3090.gif", true));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void BTNPHActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Scriptcall ("G_BASCUL")
    // Scriptcall ("G_PHOTO")
    // Scriptcall ("G_BASCUL")
    // Scriptcall ("G_PHOTO")
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_112ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void RETActionPerformed(ActionEvent e) {
  }
  
  private void AVAActionPerformed(ActionEvent e) {
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Scriptcall ("G_BASCUL")
    // Scriptcall ("G_BASCUL")
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    OBJ_50 = new JLabel();
    ENNUM = new RiZoneSortie();
    ENETB = new RiZoneSortie();
    ENSUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    ENLIB = new XRiTextField();
    ENCLA1 = new XRiTextField();
    ENCLA2 = new XRiTextField();
    OBJ_83 = new JLabel();
    OBJ_87 = new JLabel();
    OBJ_85 = new JLabel();
    WTI1 = new JLabel();
    ENZP1 = new XRiTextField();
    WTI2 = new JLabel();
    ENZP2 = new XRiTextField();
    WTI3 = new JLabel();
    ENZP3 = new XRiTextField();
    WTI4 = new JLabel();
    ENZP4 = new XRiTextField();
    WTI5 = new JLabel();
    ENZP5 = new XRiTextField();
    panel1 = new JPanel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    ENART = new XRiTextField();
    OBJ_70 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_76 = new JLabel();
    WFAM = new XRiTextField();
    ENNIV = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_105 = new JLabel();
    LIBTYP = new RiZoneSortie();
    ENIN3 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    PHO = new JPanel();
    AVA = new JButton();
    RET = new JButton();
    BTNPH = new JButton();
    OBJ_111 = new JButton();
    OBJ_112 = new JButton();
    OBJ_60 = new JButton();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1040, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ent\u00eate de nomenclature");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");

          //---- OBJ_50 ----
          OBJ_50.setText("Num\u00e9ro");
          OBJ_50.setName("OBJ_50");

          //---- ENNUM ----
          ENNUM.setComponentPopupMenu(BTD);
          ENNUM.setOpaque(false);
          ENNUM.setText("@ENNUM@");
          ENNUM.setName("ENNUM");

          //---- ENETB ----
          ENETB.setComponentPopupMenu(BTD);
          ENETB.setOpaque(false);
          ENETB.setText("@ENETB@");
          ENETB.setName("ENETB");

          //---- ENSUF ----
          ENSUF.setComponentPopupMenu(BTD);
          ENSUF.setOpaque(false);
          ENSUF.setText("@ENSUF@");
          ENSUF.setName("ENSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(ENETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(ENNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(ENSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(ENETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(ENNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(ENSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options article");
              riSousMenu_bt6.setToolTipText("Options article");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Calcul du prix de vente");
              riSousMenu_bt7.setToolTipText("Calcul du prix de vente");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Fonctions");
              riSousMenu_bt8.setToolTipText("Fonctions");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Infos de classement");
              riSousMenu_bt9.setToolTipText("Informations de classement");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Informations techniques");
              riSousMenu_bt10.setToolTipText("Informations techniques");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Gestion des documents li\u00e9s");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(840, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Description");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- ENLIB ----
            ENLIB.setComponentPopupMenu(null);
            ENLIB.setName("ENLIB");
            xTitledPanel1ContentContainer.add(ENLIB);
            ENLIB.setBounds(460, 15, 310, ENLIB.getPreferredSize().height);

            //---- ENCLA1 ----
            ENCLA1.setComponentPopupMenu(null);
            ENCLA1.setName("ENCLA1");
            xTitledPanel1ContentContainer.add(ENCLA1);
            ENCLA1.setBounds(185, 15, 210, ENCLA1.getPreferredSize().height);

            //---- ENCLA2 ----
            ENCLA2.setComponentPopupMenu(null);
            ENCLA2.setName("ENCLA2");
            xTitledPanel1ContentContainer.add(ENCLA2);
            ENCLA2.setBounds(185, 45, 210, ENCLA2.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("@LMC1@");
            OBJ_83.setName("OBJ_83");
            xTitledPanel1ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(10, 19, 170, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("@LMC2@");
            OBJ_87.setName("OBJ_87");
            xTitledPanel1ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(10, 49, 170, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("Libell\u00e9");
            OBJ_85.setName("OBJ_85");
            xTitledPanel1ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(410, 19, 50, 20);

            //---- WTI1 ----
            WTI1.setText("@WTI1@");
            WTI1.setName("WTI1");
            xTitledPanel1ContentContainer.add(WTI1);
            WTI1.setBounds(410, 49, 34, 20);

            //---- ENZP1 ----
            ENZP1.setComponentPopupMenu(null);
            ENZP1.setName("ENZP1");
            xTitledPanel1ContentContainer.add(ENZP1);
            ENZP1.setBounds(445, 45, 34, ENZP1.getPreferredSize().height);

            //---- WTI2 ----
            WTI2.setText("@WTI2@");
            WTI2.setName("WTI2");
            xTitledPanel1ContentContainer.add(WTI2);
            WTI2.setBounds(490, 49, 34, 20);

            //---- ENZP2 ----
            ENZP2.setComponentPopupMenu(null);
            ENZP2.setName("ENZP2");
            xTitledPanel1ContentContainer.add(ENZP2);
            ENZP2.setBounds(520, 45, 34, ENZP2.getPreferredSize().height);

            //---- WTI3 ----
            WTI3.setText("@WTI3@");
            WTI3.setName("WTI3");
            xTitledPanel1ContentContainer.add(WTI3);
            WTI3.setBounds(565, 49, 34, 20);

            //---- ENZP3 ----
            ENZP3.setComponentPopupMenu(null);
            ENZP3.setName("ENZP3");
            xTitledPanel1ContentContainer.add(ENZP3);
            ENZP3.setBounds(595, 45, 34, ENZP3.getPreferredSize().height);

            //---- WTI4 ----
            WTI4.setText("@WTI4@");
            WTI4.setName("WTI4");
            xTitledPanel1ContentContainer.add(WTI4);
            WTI4.setBounds(635, 49, 34, 20);

            //---- ENZP4 ----
            ENZP4.setComponentPopupMenu(null);
            ENZP4.setName("ENZP4");
            xTitledPanel1ContentContainer.add(ENZP4);
            ENZP4.setBounds(665, 45, 34, ENZP4.getPreferredSize().height);

            //---- WTI5 ----
            WTI5.setText("@WTI5@");
            WTI5.setName("WTI5");
            xTitledPanel1ContentContainer.add(WTI5);
            WTI5.setBounds(705, 49, 34, 20);

            //---- ENZP5 ----
            ENZP5.setComponentPopupMenu(null);
            ENZP5.setName("ENZP5");
            xTitledPanel1ContentContainer.add(ENZP5);
            ENZP5.setBounds(735, 45, 34, ENZP5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel1.add(A1LIB);
            A1LIB.setBounds(335, 12, 321, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            panel1.add(A1LB1);
            A1LB1.setBounds(335, 42, 321, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setText("@A1LB2@");
            A1LB2.setName("A1LB2");
            panel1.add(A1LB2);
            A1LB2.setBounds(335, 72, 321, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setText("@A1LB3@");
            A1LB3.setName("A1LB3");
            panel1.add(A1LB3);
            A1LB3.setBounds(335, 102, 321, A1LB3.getPreferredSize().height);

            //---- ENART ----
            ENART.setComponentPopupMenu(BTD);
            ENART.setName("ENART");
            panel1.add(ENART);
            ENART.setBounds(110, 10, 210, ENART.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("Famille");
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(15, 46, 70, OBJ_70.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("Article");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(15, 16, 70, OBJ_66.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Niveau");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(15, 76, 70, OBJ_76.getPreferredSize().height);

            //---- WFAM ----
            WFAM.setComponentPopupMenu(BTD);
            WFAM.setName("WFAM");
            panel1.add(WFAM);
            WFAM.setBounds(110, 40, 40, WFAM.getPreferredSize().height);

            //---- ENNIV ----
            ENNIV.setComponentPopupMenu(BTD);
            ENNIV.setName("ENNIV");
            panel1.add(ENNIV);
            ENNIV.setBounds(110, 70, 28, ENNIV.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Fabrication");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_105 ----
            OBJ_105.setText("Type de nomenclature");
            OBJ_105.setName("OBJ_105");
            xTitledPanel2ContentContainer.add(OBJ_105);
            OBJ_105.setBounds(10, 19, 150, 20);

            //---- LIBTYP ----
            LIBTYP.setText("@LIBTYP@");
            LIBTYP.setName("LIBTYP");
            xTitledPanel2ContentContainer.add(LIBTYP);
            LIBTYP.setBounds(210, 17, 180, LIBTYP.getPreferredSize().height);

            //---- ENIN3 ----
            ENIN3.setComponentPopupMenu(BTD);
            ENIN3.setName("ENIN3");
            xTitledPanel2ContentContainer.add(ENIN3);
            ENIN3.setBounds(185, 15, 20, ENIN3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 790, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 790, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }

    //======== PHO ========
    {
      PHO.setComponentPopupMenu(BTD);
      PHO.setName("PHO");
      PHO.setLayout(null);

      PHO.setPreferredSize(new Dimension(374, 194));
    }

    //---- AVA ----
    AVA.setText("");
    AVA.setToolTipText("Photographie suivante");
    AVA.setComponentPopupMenu(BTD);
    AVA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    AVA.setName("AVA");
    AVA.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        AVAActionPerformed(e);
      }
    });

    //---- RET ----
    RET.setText("");
    RET.setToolTipText("Photographie pr\u00e9c\u00e9dente");
    RET.setComponentPopupMenu(BTD);
    RET.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    RET.setName("RET");
    RET.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        RETActionPerformed(e);
      }
    });

    //---- BTNPH ----
    BTNPH.setText("");
    BTNPH.setToolTipText("Photographies de l'article");
    BTNPH.setComponentPopupMenu(BTD);
    BTNPH.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BTNPH.setName("BTNPH");
    BTNPH.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BTNPHActionPerformed(e);
      }
    });

    //---- OBJ_111 ----
    OBJ_111.setText("");
    OBJ_111.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_111.setName("OBJ_111");
    OBJ_111.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_111ActionPerformed(e);
      }
    });

    //---- OBJ_112 ----
    OBJ_112.setText("");
    OBJ_112.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_112.setName("OBJ_112");
    OBJ_112.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_112ActionPerformed(e);
      }
    });

    //---- OBJ_60 ----
    OBJ_60.setText("@&&NUMIMGC@/@&&NBRIMAGE@");
    OBJ_60.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_60.setName("OBJ_60");

    //---- OBJ_9 ----
    OBJ_9.setText("Recherche multi-crit\u00e8res");
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private JLabel OBJ_50;
  private RiZoneSortie ENNUM;
  private RiZoneSortie ENETB;
  private RiZoneSortie ENSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private XRiTextField ENLIB;
  private XRiTextField ENCLA1;
  private XRiTextField ENCLA2;
  private JLabel OBJ_83;
  private JLabel OBJ_87;
  private JLabel OBJ_85;
  private JLabel WTI1;
  private XRiTextField ENZP1;
  private JLabel WTI2;
  private XRiTextField ENZP2;
  private JLabel WTI3;
  private XRiTextField ENZP3;
  private JLabel WTI4;
  private XRiTextField ENZP4;
  private JLabel WTI5;
  private XRiTextField ENZP5;
  private JPanel panel1;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private XRiTextField ENART;
  private JLabel OBJ_70;
  private JLabel OBJ_66;
  private JLabel OBJ_76;
  private XRiTextField WFAM;
  private XRiTextField ENNIV;
  private JXTitledPanel xTitledPanel2;
  private JLabel OBJ_105;
  private RiZoneSortie LIBTYP;
  private XRiTextField ENIN3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  private JPanel PHO;
  private JButton AVA;
  private JButton RET;
  private JButton BTNPH;
  private JButton OBJ_111;
  private JButton OBJ_112;
  private JButton OBJ_60;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
