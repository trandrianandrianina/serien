
package ri.serien.libecranrpg.tgpm.TGPM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TGPM13FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM13FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDLIB@")).trim());
    A1ASB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1ASB@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    TAFL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL1@")).trim());
    LNTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNTYP@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    A1IN4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1IN4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    OBJ_43.setVisible(lexique.isPresent("ORDLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Lignes de nomenclature"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_43 = new RiZoneSortie();
    LNLIB = new XRiTextField();
    LNORD = new XRiTextField();
    A1ASB = new RiZoneSortie();
    MALIB = new RiZoneSortie();
    TAFL1 = new RiZoneSortie();
    LNNBR = new XRiTextField();
    UQTE = new XRiTextField();
    LNQB1 = new XRiTextField();
    LNQB2 = new XRiTextField();
    LNQB3 = new XRiTextField();
    OBJ_64 = new JLabel();
    OBJ_57 = new JLabel();
    LNBA1 = new XRiTextField();
    OBJ_50 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_56 = new JLabel();
    LNNL1 = new XRiTextField();
    LNNL2 = new XRiTextField();
    LNNL3 = new XRiTextField();
    LNTYP = new RiZoneSortie();
    OBJ_65 = new JLabel();
    OBJ_67 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    LNMAG = new XRiTextField();
    WTI1 = new RiZoneSortie();
    WTI2 = new RiZoneSortie();
    WTI3 = new RiZoneSortie();
    WTI4 = new RiZoneSortie();
    WTI5 = new RiZoneSortie();
    LNZP1 = new XRiTextField();
    LNZP2 = new XRiTextField();
    LNZP3 = new XRiTextField();
    LNZP4 = new XRiTextField();
    LNZP5 = new XRiTextField();
    WUN1 = new XRiTextField();
    LNUN1 = new XRiTextField();
    LNUN2 = new XRiTextField();
    LNUN3 = new XRiTextField();
    OBJ_30 = new JLabel();
    LNAF1 = new XRiTextField();
    A1IN4 = new RiZoneSortie();
    LNIN1 = new XRiTextField();
    LNIN2 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(885, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Informations techniques");
            riSousMenu_bt6.setToolTipText("Informations techniques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Remplacement ordre");
            riSousMenu_bt7.setToolTipText("Remplacement ordre");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Documents li\u00e9s");
            riSousMenu_bt8.setToolTipText("Documents li\u00e9s");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Options article");
            riSousMenu_bt9.setToolTipText("Options article");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_43 ----
          OBJ_43.setText("@ORDLIB@");
          OBJ_43.setName("OBJ_43");
          p_recup.add(OBJ_43);
          OBJ_43.setBounds(235, 82, 400, OBJ_43.getPreferredSize().height);

          //---- LNLIB ----
          LNLIB.setComponentPopupMenu(BTD);
          LNLIB.setName("LNLIB");
          p_recup.add(LNLIB);
          LNLIB.setBounds(233, 115, 310, LNLIB.getPreferredSize().height);

          //---- LNORD ----
          LNORD.setComponentPopupMenu(BTD);
          LNORD.setName("LNORD");
          p_recup.add(LNORD);
          LNORD.setBounds(190, 45, 210, LNORD.getPreferredSize().height);

          //---- A1ASB ----
          A1ASB.setText("@A1ASB@");
          A1ASB.setName("A1ASB");
          p_recup.add(A1ASB);
          A1ASB.setBounds(145, 150, 220, A1ASB.getPreferredSize().height);

          //---- MALIB ----
          MALIB.setText("@MALIB@");
          MALIB.setName("MALIB");
          p_recup.add(MALIB);
          MALIB.setBounds(455, 47, 210, MALIB.getPreferredSize().height);

          //---- TAFL1 ----
          TAFL1.setComponentPopupMenu(BTD);
          TAFL1.setText("@TAFL1@");
          TAFL1.setName("TAFL1");
          p_recup.add(TAFL1);
          TAFL1.setBounds(420, 212, 215, TAFL1.getPreferredSize().height);

          //---- LNNBR ----
          LNNBR.setComponentPopupMenu(BTD);
          LNNBR.setName("LNNBR");
          p_recup.add(LNNBR);
          LNNBR.setBounds(25, 210, 114, LNNBR.getPreferredSize().height);

          //---- UQTE ----
          UQTE.setComponentPopupMenu(BTD);
          UQTE.setName("UQTE");
          p_recup.add(UQTE);
          UQTE.setBounds(145, 210, 106, UQTE.getPreferredSize().height);

          //---- LNQB1 ----
          LNQB1.setComponentPopupMenu(BTD);
          LNQB1.setName("LNQB1");
          p_recup.add(LNQB1);
          LNQB1.setBounds(25, 270, 106, LNQB1.getPreferredSize().height);

          //---- LNQB2 ----
          LNQB2.setComponentPopupMenu(BTD);
          LNQB2.setName("LNQB2");
          p_recup.add(LNQB2);
          LNQB2.setBounds(220, 270, 106, LNQB2.getPreferredSize().height);

          //---- LNQB3 ----
          LNQB3.setComponentPopupMenu(BTD);
          LNQB3.setName("LNQB3");
          p_recup.add(LNQB3);
          LNQB3.setBounds(395, 270, 106, LNQB3.getPreferredSize().height);

          //---- OBJ_64 ----
          OBJ_64.setText("Dimensions");
          OBJ_64.setName("OBJ_64");
          p_recup.add(OBJ_64);
          OBJ_64.setBounds(25, 245, 85, 18);

          //---- OBJ_57 ----
          OBJ_57.setText("Affectation");
          OBJ_57.setName("OBJ_57");
          p_recup.add(OBJ_57);
          OBJ_57.setBounds(395, 185, 95, 18);

          //---- LNBA1 ----
          LNBA1.setComponentPopupMenu(BTD);
          LNBA1.setName("LNBA1");
          p_recup.add(LNBA1);
          LNBA1.setBounds(323, 210, 66, LNBA1.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Variante");
          OBJ_50.setName("OBJ_50");
          p_recup.add(OBJ_50);
          OBJ_50.setBounds(25, 154, 70, 20);

          //---- OBJ_54 ----
          OBJ_54.setText("Nombre");
          OBJ_54.setName("OBJ_54");
          p_recup.add(OBJ_54);
          OBJ_54.setBounds(25, 185, 90, 18);

          //---- OBJ_55 ----
          OBJ_55.setText("Besoins");
          OBJ_55.setName("OBJ_55");
          p_recup.add(OBJ_55);
          OBJ_55.setBounds(145, 185, 85, 18);

          //---- OBJ_29 ----
          OBJ_29.setText("Article");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(195, 20, 85, 30);

          //---- OBJ_56 ----
          OBJ_56.setText("Base");
          OBJ_56.setName("OBJ_56");
          p_recup.add(OBJ_56);
          OBJ_56.setBounds(325, 185, 55, 18);

          //---- LNNL1 ----
          LNNL1.setComponentPopupMenu(BTD);
          LNNL1.setName("LNNL1");
          p_recup.add(LNNL1);
          LNNL1.setBounds(25, 45, 36, LNNL1.getPreferredSize().height);

          //---- LNNL2 ----
          LNNL2.setComponentPopupMenu(BTD);
          LNNL2.setName("LNNL2");
          p_recup.add(LNNL2);
          LNNL2.setBounds(65, 45, 36, LNNL2.getPreferredSize().height);

          //---- LNNL3 ----
          LNNL3.setComponentPopupMenu(BTD);
          LNNL3.setName("LNNL3");
          p_recup.add(LNNL3);
          LNNL3.setBounds(105, 45, 36, LNNL3.getPreferredSize().height);

          //---- LNTYP ----
          LNTYP.setComponentPopupMenu(BTD);
          LNTYP.setText("@LNTYP@");
          LNTYP.setName("LNTYP");
          p_recup.add(LNTYP);
          LNTYP.setBounds(147, 47, 35, LNTYP.getPreferredSize().height);

          //---- OBJ_65 ----
          OBJ_65.setText("Unit\u00e9    X");
          OBJ_65.setName("OBJ_65");
          p_recup.add(OBJ_65);
          OBJ_65.setBounds(145, 245, 55, 18);

          //---- OBJ_67 ----
          OBJ_67.setText("Unit\u00e9    X");
          OBJ_67.setName("OBJ_67");
          p_recup.add(OBJ_67);
          OBJ_67.setBounds(325, 245, 55, 18);

          //---- OBJ_69 ----
          OBJ_69.setText("Unit\u00e9");
          OBJ_69.setName("OBJ_69");
          p_recup.add(OBJ_69);
          OBJ_69.setBounds(500, 245, 50, 18);

          //---- OBJ_28 ----
          OBJ_28.setText("Type");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(147, 20, 38, 30);

          //---- OBJ_25 ----
          OBJ_25.setText("Nl1");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(25, 20, 31, 30);

          //---- OBJ_26 ----
          OBJ_26.setText("Nl2");
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(65, 20, 31, 30);

          //---- OBJ_27 ----
          OBJ_27.setText("Nl3");
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(105, 20, 31, 30);

          //---- LNMAG ----
          LNMAG.setComponentPopupMenu(BTD);
          LNMAG.setName("LNMAG");
          p_recup.add(LNMAG);
          LNMAG.setBounds(420, 45, 30, LNMAG.getPreferredSize().height);

          //---- WTI1 ----
          WTI1.setText("@WTI1@");
          WTI1.setName("WTI1");
          p_recup.add(WTI1);
          WTI1.setBounds(25, 82, 34, WTI1.getPreferredSize().height);

          //---- WTI2 ----
          WTI2.setText("@WTI2@");
          WTI2.setName("WTI2");
          p_recup.add(WTI2);
          WTI2.setBounds(65, 82, 34, WTI2.getPreferredSize().height);

          //---- WTI3 ----
          WTI3.setText("@WTI3@");
          WTI3.setName("WTI3");
          p_recup.add(WTI3);
          WTI3.setBounds(105, 82, 34, WTI3.getPreferredSize().height);

          //---- WTI4 ----
          WTI4.setText("@WTI4@");
          WTI4.setName("WTI4");
          p_recup.add(WTI4);
          WTI4.setBounds(145, 82, 34, WTI4.getPreferredSize().height);

          //---- WTI5 ----
          WTI5.setText("@WTI5@");
          WTI5.setName("WTI5");
          p_recup.add(WTI5);
          WTI5.setBounds(185, 82, 34, WTI5.getPreferredSize().height);

          //---- LNZP1 ----
          LNZP1.setComponentPopupMenu(BTD);
          LNZP1.setName("LNZP1");
          p_recup.add(LNZP1);
          LNZP1.setBounds(25, 115, 34, LNZP1.getPreferredSize().height);

          //---- LNZP2 ----
          LNZP2.setComponentPopupMenu(BTD);
          LNZP2.setName("LNZP2");
          p_recup.add(LNZP2);
          LNZP2.setBounds(65, 115, 34, LNZP2.getPreferredSize().height);

          //---- LNZP3 ----
          LNZP3.setComponentPopupMenu(BTD);
          LNZP3.setName("LNZP3");
          p_recup.add(LNZP3);
          LNZP3.setBounds(105, 115, 34, LNZP3.getPreferredSize().height);

          //---- LNZP4 ----
          LNZP4.setComponentPopupMenu(BTD);
          LNZP4.setName("LNZP4");
          p_recup.add(LNZP4);
          LNZP4.setBounds(145, 115, 34, LNZP4.getPreferredSize().height);

          //---- LNZP5 ----
          LNZP5.setComponentPopupMenu(BTD);
          LNZP5.setName("LNZP5");
          p_recup.add(LNZP5);
          LNZP5.setBounds(185, 115, 34, LNZP5.getPreferredSize().height);

          //---- WUN1 ----
          WUN1.setComponentPopupMenu(BTD);
          WUN1.setName("WUN1");
          p_recup.add(WUN1);
          WUN1.setBounds(255, 210, 30, WUN1.getPreferredSize().height);

          //---- LNUN1 ----
          LNUN1.setComponentPopupMenu(BTD);
          LNUN1.setName("LNUN1");
          p_recup.add(LNUN1);
          LNUN1.setBounds(145, 270, 34, LNUN1.getPreferredSize().height);

          //---- LNUN2 ----
          LNUN2.setComponentPopupMenu(BTD);
          LNUN2.setName("LNUN2");
          p_recup.add(LNUN2);
          LNUN2.setBounds(325, 270, 34, LNUN2.getPreferredSize().height);

          //---- LNUN3 ----
          LNUN3.setComponentPopupMenu(BTD);
          LNUN3.setName("LNUN3");
          p_recup.add(LNUN3);
          LNUN3.setBounds(500, 270, 34, LNUN3.getPreferredSize().height);

          //---- OBJ_30 ----
          OBJ_30.setText("Magasin");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(420, 20, 245, 30);

          //---- LNAF1 ----
          LNAF1.setComponentPopupMenu(BTD);
          LNAF1.setName("LNAF1");
          p_recup.add(LNAF1);
          LNAF1.setBounds(395, 210, 20, LNAF1.getPreferredSize().height);

          //---- A1IN4 ----
          A1IN4.setText("@A1IN4@");
          A1IN4.setName("A1IN4");
          p_recup.add(A1IN4);
          A1IN4.setBounds(105, 150, 25, A1IN4.getPreferredSize().height);

          //---- LNIN1 ----
          LNIN1.setComponentPopupMenu(BTD);
          LNIN1.setName("LNIN1");
          p_recup.add(LNIN1);
          LNIN1.setBounds(180, 270, 20, LNIN1.getPreferredSize().height);

          //---- LNIN2 ----
          LNIN2.setComponentPopupMenu(BTD);
          LNIN2.setName("LNIN2");
          p_recup.add(LNIN2);
          LNIN2.setBounds(360, 270, 20, LNIN2.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 691, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 364, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie OBJ_43;
  private XRiTextField LNLIB;
  private XRiTextField LNORD;
  private RiZoneSortie A1ASB;
  private RiZoneSortie MALIB;
  private RiZoneSortie TAFL1;
  private XRiTextField LNNBR;
  private XRiTextField UQTE;
  private XRiTextField LNQB1;
  private XRiTextField LNQB2;
  private XRiTextField LNQB3;
  private JLabel OBJ_64;
  private JLabel OBJ_57;
  private XRiTextField LNBA1;
  private JLabel OBJ_50;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private JLabel OBJ_29;
  private JLabel OBJ_56;
  private XRiTextField LNNL1;
  private XRiTextField LNNL2;
  private XRiTextField LNNL3;
  private RiZoneSortie LNTYP;
  private JLabel OBJ_65;
  private JLabel OBJ_67;
  private JLabel OBJ_69;
  private JLabel OBJ_28;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private XRiTextField LNMAG;
  private RiZoneSortie WTI1;
  private RiZoneSortie WTI2;
  private RiZoneSortie WTI3;
  private RiZoneSortie WTI4;
  private RiZoneSortie WTI5;
  private XRiTextField LNZP1;
  private XRiTextField LNZP2;
  private XRiTextField LNZP3;
  private XRiTextField LNZP4;
  private XRiTextField LNZP5;
  private XRiTextField WUN1;
  private XRiTextField LNUN1;
  private XRiTextField LNUN2;
  private XRiTextField LNUN3;
  private JLabel OBJ_30;
  private XRiTextField LNAF1;
  private RiZoneSortie A1IN4;
  private XRiTextField LNIN1;
  private XRiTextField LNIN2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
