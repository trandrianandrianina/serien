
package ri.serien.libecranrpg.tgpm.TGPM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TGPM13FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM13FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LNIN3.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ORDLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDLIB@")).trim());
    TAFL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL1@")).trim());
    TAFL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL2@")).trim());
    TAFL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL3@")).trim());
    OPQUA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPQUA@")).trim());
    OPPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPPOS@")).trim());
    LNTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNTYP@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    LNUN1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNUN1@")).trim());
    LNUN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNUN2@")).trim());
    LNUN3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNUN3@")).trim());
    LNAF1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNAF1@")).trim());
    LNAF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNAF2@")).trim());
    LNAF3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LNAF3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Lignes de nomenclature"));
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (LNIN3.isSelected())
    // lexique.HostFieldPutData("LNIN3", 0, "1");
    // else
    // lexique.HostFieldPutData("LNIN3", 0, " ");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BT1.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    ORDLIB = new RiZoneSortie();
    LNLIB = new XRiTextField();
    LNORD = new XRiTextField();
    OBJ_32 = new JLabel();
    TAFL1 = new RiZoneSortie();
    TAFL2 = new RiZoneSortie();
    TAFL3 = new RiZoneSortie();
    OBJ_54 = new JLabel();
    OBJ_41 = new JLabel();
    LNIN3 = new XRiCheckBox();
    OBJ_60 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_72 = new JLabel();
    LNNBR = new XRiTextField();
    LNQB1 = new XRiTextField();
    LNQB2 = new XRiTextField();
    LNQB3 = new XRiTextField();
    OPQUA = new RiZoneSortie();
    OBJ_87 = new JLabel();
    OBJ_38 = new JLabel();
    LNBA1 = new XRiTextField();
    LNBA2 = new XRiTextField();
    LNBA3 = new XRiTextField();
    OPPOS = new RiZoneSortie();
    OBJ_58 = new JLabel();
    OBJ_57 = new JLabel();
    LNNL1 = new XRiTextField();
    LNNL2 = new XRiTextField();
    LNNL3 = new XRiTextField();
    LNTYP = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    WTI1 = new RiZoneSortie();
    WTI2 = new RiZoneSortie();
    WTI3 = new RiZoneSortie();
    WTI4 = new RiZoneSortie();
    WTI5 = new RiZoneSortie();
    LNZP1 = new XRiTextField();
    LNZP2 = new XRiTextField();
    LNZP3 = new XRiTextField();
    LNZP4 = new XRiTextField();
    LNZP5 = new XRiTextField();
    LNUN1 = new RiZoneSortie();
    LNUN2 = new RiZoneSortie();
    LNUN3 = new RiZoneSortie();
    LNAF1 = new RiZoneSortie();
    LNAF2 = new RiZoneSortie();
    LNAF3 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BT1 = new JPopupMenu();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Informations techniques");
            riSousMenu_bt6.setToolTipText("Informations techniques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Documents li\u00e9s");
            riSousMenu_bt7.setToolTipText("Documents li\u00e9s");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Remplacement ordre");
            riSousMenu_bt8.setToolTipText("Remplacement ordre");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- ORDLIB ----
          ORDLIB.setText("@ORDLIB@");
          ORDLIB.setName("ORDLIB");
          p_recup.add(ORDLIB);
          ORDLIB.setBounds(245, 110, 309, ORDLIB.getPreferredSize().height);

          //---- LNLIB ----
          LNLIB.setComponentPopupMenu(BTD);
          LNLIB.setName("LNLIB");
          p_recup.add(LNLIB);
          LNLIB.setBounds(245, 140, 310, LNLIB.getPreferredSize().height);

          //---- LNORD ----
          LNORD.setComponentPopupMenu(BTD);
          LNORD.setName("LNORD");
          p_recup.add(LNORD);
          LNORD.setBounds(200, 45, 210, LNORD.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Op\u00e9ration");
          OBJ_32.setName("OBJ_32");
          p_recup.add(OBJ_32);
          OBJ_32.setBounds(205, 20, 189, 18);

          //---- TAFL1 ----
          TAFL1.setComponentPopupMenu(BTD);
          TAFL1.setText("@TAFL1@");
          TAFL1.setName("TAFL1");
          p_recup.add(TAFL1);
          TAFL1.setBounds(400, 247, 210, TAFL1.getPreferredSize().height);

          //---- TAFL2 ----
          TAFL2.setComponentPopupMenu(BTD);
          TAFL2.setText("@TAFL2@");
          TAFL2.setName("TAFL2");
          p_recup.add(TAFL2);
          TAFL2.setBounds(400, 282, 210, TAFL2.getPreferredSize().height);

          //---- TAFL3 ----
          TAFL3.setComponentPopupMenu(BTD);
          TAFL3.setText("@TAFL3@");
          TAFL3.setName("TAFL3");
          p_recup.add(TAFL3);
          TAFL3.setBounds(400, 317, 210, TAFL3.getPreferredSize().height);

          //---- OBJ_54 ----
          OBJ_54.setText("Nombre d'op\u00e9rations");
          OBJ_54.setName("OBJ_54");
          p_recup.add(OBJ_54);
          OBJ_54.setBounds(23, 179, 137, 20);

          //---- OBJ_41 ----
          OBJ_41.setText("Qualification production");
          OBJ_41.setName("OBJ_41");
          p_recup.add(OBJ_41);
          OBJ_41.setBounds(23, 80, 170, 18);

          //---- LNIN3 ----
          LNIN3.setText("Sous-traitance");
          LNIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LNIN3.setName("LNIN3");
          p_recup.add(LNIN3);
          LNIN3.setBounds(308, 179, 145, 20);

          //---- OBJ_60 ----
          OBJ_60.setText("Temps planning");
          OBJ_60.setName("OBJ_60");
          p_recup.add(OBJ_60);
          OBJ_60.setBounds(23, 249, 120, 20);

          //---- OBJ_66 ----
          OBJ_66.setText("Autre co\u00fbt N\u00b01");
          OBJ_66.setName("OBJ_66");
          p_recup.add(OBJ_66);
          OBJ_66.setBounds(23, 284, 120, 20);

          //---- OBJ_72 ----
          OBJ_72.setText("Autre co\u00fbt N\u00b02");
          OBJ_72.setName("OBJ_72");
          p_recup.add(OBJ_72);
          OBJ_72.setBounds(23, 319, 120, 20);

          //---- LNNBR ----
          LNNBR.setComponentPopupMenu(BTD);
          LNNBR.setName("LNNBR");
          p_recup.add(LNNBR);
          LNNBR.setBounds(155, 175, 106, LNNBR.getPreferredSize().height);

          //---- LNQB1 ----
          LNQB1.setComponentPopupMenu(BTD);
          LNQB1.setName("LNQB1");
          p_recup.add(LNQB1);
          LNQB1.setBounds(155, 245, 106, LNQB1.getPreferredSize().height);

          //---- LNQB2 ----
          LNQB2.setComponentPopupMenu(BTD);
          LNQB2.setName("LNQB2");
          p_recup.add(LNQB2);
          LNQB2.setBounds(155, 280, 106, LNQB2.getPreferredSize().height);

          //---- LNQB3 ----
          LNQB3.setComponentPopupMenu(BTD);
          LNQB3.setName("LNQB3");
          p_recup.add(LNQB3);
          LNQB3.setBounds(155, 315, 106, LNQB3.getPreferredSize().height);

          //---- OPQUA ----
          OPQUA.setText("@OPQUA@");
          OPQUA.setName("OPQUA");
          p_recup.add(OPQUA);
          OPQUA.setBounds(200, 77, 52, OPQUA.getPreferredSize().height);

          //---- OBJ_87 ----
          OBJ_87.setText("Affectation");
          OBJ_87.setName("OBJ_87");
          p_recup.add(OBJ_87);
          OBJ_87.setBounds(405, 220, 75, 18);

          //---- OBJ_38 ----
          OBJ_38.setText("Poste");
          OBJ_38.setName("OBJ_38");
          p_recup.add(OBJ_38);
          OBJ_38.setBounds(455, 49, 50, 20);

          //---- LNBA1 ----
          LNBA1.setComponentPopupMenu(BT1);
          LNBA1.setName("LNBA1");
          p_recup.add(LNBA1);
          LNBA1.setBounds(305, 245, 66, LNBA1.getPreferredSize().height);

          //---- LNBA2 ----
          LNBA2.setComponentPopupMenu(BT1);
          LNBA2.setName("LNBA2");
          p_recup.add(LNBA2);
          LNBA2.setBounds(305, 280, 66, LNBA2.getPreferredSize().height);

          //---- LNBA3 ----
          LNBA3.setComponentPopupMenu(BT1);
          LNBA3.setName("LNBA3");
          p_recup.add(LNBA3);
          LNBA3.setBounds(305, 315, 66, LNBA3.getPreferredSize().height);

          //---- OPPOS ----
          OPPOS.setText("@OPPOS@");
          OPPOS.setName("OPPOS");
          p_recup.add(OPPOS);
          OPPOS.setBounds(502, 47, 52, OPPOS.getPreferredSize().height);

          //---- OBJ_58 ----
          OBJ_58.setText("Besoins");
          OBJ_58.setName("OBJ_58");
          p_recup.add(OBJ_58);
          OBJ_58.setBounds(160, 220, 65, 18);

          //---- OBJ_57 ----
          OBJ_57.setText("Base");
          OBJ_57.setName("OBJ_57");
          p_recup.add(OBJ_57);
          OBJ_57.setBounds(310, 220, 45, 18);

          //---- LNNL1 ----
          LNNL1.setComponentPopupMenu(BTD);
          LNNL1.setName("LNNL1");
          p_recup.add(LNNL1);
          LNNL1.setBounds(20, 45, 36, LNNL1.getPreferredSize().height);

          //---- LNNL2 ----
          LNNL2.setComponentPopupMenu(BTD);
          LNNL2.setName("LNNL2");
          p_recup.add(LNNL2);
          LNNL2.setBounds(65, 45, 36, LNNL2.getPreferredSize().height);

          //---- LNNL3 ----
          LNNL3.setComponentPopupMenu(BTD);
          LNNL3.setName("LNNL3");
          p_recup.add(LNNL3);
          LNNL3.setBounds(110, 45, 36, LNNL3.getPreferredSize().height);

          //---- LNTYP ----
          LNTYP.setComponentPopupMenu(BTD);
          LNTYP.setText("@LNTYP@");
          LNTYP.setName("LNTYP");
          p_recup.add(LNTYP);
          LNTYP.setBounds(155, 47, 36, LNTYP.getPreferredSize().height);

          //---- OBJ_56 ----
          OBJ_56.setText("Unit\u00e9");
          OBJ_56.setName("OBJ_56");
          p_recup.add(OBJ_56);
          OBJ_56.setBounds(270, 220, 40, 18);

          //---- OBJ_31 ----
          OBJ_31.setText("Type");
          OBJ_31.setName("OBJ_31");
          p_recup.add(OBJ_31);
          OBJ_31.setBounds(160, 20, 40, 18);

          //---- OBJ_28 ----
          OBJ_28.setText("Niv1");
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(23, 20, 35, 18);

          //---- OBJ_29 ----
          OBJ_29.setText("Niv2");
          OBJ_29.setName("OBJ_29");
          p_recup.add(OBJ_29);
          OBJ_29.setBounds(70, 20, 35, 18);

          //---- OBJ_30 ----
          OBJ_30.setText("Niv3");
          OBJ_30.setName("OBJ_30");
          p_recup.add(OBJ_30);
          OBJ_30.setBounds(115, 20, 35, 18);

          //---- WTI1 ----
          WTI1.setComponentPopupMenu(BTD);
          WTI1.setText("@WTI1@");
          WTI1.setName("WTI1");
          p_recup.add(WTI1);
          WTI1.setBounds(20, 110, 34, WTI1.getPreferredSize().height);

          //---- WTI2 ----
          WTI2.setComponentPopupMenu(BTD);
          WTI2.setText("@WTI2@");
          WTI2.setName("WTI2");
          p_recup.add(WTI2);
          WTI2.setBounds(65, 110, 34, WTI2.getPreferredSize().height);

          //---- WTI3 ----
          WTI3.setComponentPopupMenu(BTD);
          WTI3.setText("@WTI3@");
          WTI3.setName("WTI3");
          p_recup.add(WTI3);
          WTI3.setBounds(110, 110, 34, WTI3.getPreferredSize().height);

          //---- WTI4 ----
          WTI4.setComponentPopupMenu(BTD);
          WTI4.setText("@WTI4@");
          WTI4.setName("WTI4");
          p_recup.add(WTI4);
          WTI4.setBounds(155, 110, 34, WTI4.getPreferredSize().height);

          //---- WTI5 ----
          WTI5.setComponentPopupMenu(BTD);
          WTI5.setText("@WTI5@");
          WTI5.setName("WTI5");
          p_recup.add(WTI5);
          WTI5.setBounds(200, 110, 34, WTI5.getPreferredSize().height);

          //---- LNZP1 ----
          LNZP1.setComponentPopupMenu(BTD);
          LNZP1.setName("LNZP1");
          p_recup.add(LNZP1);
          LNZP1.setBounds(20, 140, 34, LNZP1.getPreferredSize().height);

          //---- LNZP2 ----
          LNZP2.setComponentPopupMenu(BTD);
          LNZP2.setName("LNZP2");
          p_recup.add(LNZP2);
          LNZP2.setBounds(65, 140, 34, LNZP2.getPreferredSize().height);

          //---- LNZP3 ----
          LNZP3.setComponentPopupMenu(BTD);
          LNZP3.setName("LNZP3");
          p_recup.add(LNZP3);
          LNZP3.setBounds(110, 140, 34, LNZP3.getPreferredSize().height);

          //---- LNZP4 ----
          LNZP4.setComponentPopupMenu(BTD);
          LNZP4.setName("LNZP4");
          p_recup.add(LNZP4);
          LNZP4.setBounds(155, 140, 34, LNZP4.getPreferredSize().height);

          //---- LNZP5 ----
          LNZP5.setComponentPopupMenu(BTD);
          LNZP5.setName("LNZP5");
          p_recup.add(LNZP5);
          LNZP5.setBounds(200, 140, 34, LNZP5.getPreferredSize().height);

          //---- LNUN1 ----
          LNUN1.setComponentPopupMenu(BT1);
          LNUN1.setText("@LNUN1@");
          LNUN1.setName("LNUN1");
          p_recup.add(LNUN1);
          LNUN1.setBounds(265, 247, 34, LNUN1.getPreferredSize().height);

          //---- LNUN2 ----
          LNUN2.setComponentPopupMenu(BT1);
          LNUN2.setText("@LNUN2@");
          LNUN2.setName("LNUN2");
          p_recup.add(LNUN2);
          LNUN2.setBounds(265, 282, 34, LNUN2.getPreferredSize().height);

          //---- LNUN3 ----
          LNUN3.setComponentPopupMenu(BT1);
          LNUN3.setText("@LNUN3@");
          LNUN3.setName("LNUN3");
          p_recup.add(LNUN3);
          LNUN3.setBounds(265, 317, 34, LNUN3.getPreferredSize().height);

          //---- LNAF1 ----
          LNAF1.setComponentPopupMenu(BT1);
          LNAF1.setText("@LNAF1@");
          LNAF1.setName("LNAF1");
          p_recup.add(LNAF1);
          LNAF1.setBounds(375, 247, 20, LNAF1.getPreferredSize().height);

          //---- LNAF2 ----
          LNAF2.setComponentPopupMenu(BT1);
          LNAF2.setText("@LNAF2@");
          LNAF2.setName("LNAF2");
          p_recup.add(LNAF2);
          LNAF2.setBounds(375, 282, 20, LNAF2.getPreferredSize().height);

          //---- LNAF3 ----
          LNAF3.setComponentPopupMenu(BT1);
          LNAF3.setText("@LNAF3@");
          LNAF3.setName("LNAF3");
          p_recup.add(LNAF3);
          LNAF3.setBounds(375, 317, 20, LNAF3.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 370, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BT1 ========
    {
      BT1.setName("BT1");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BT1.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie ORDLIB;
  private XRiTextField LNLIB;
  private XRiTextField LNORD;
  private JLabel OBJ_32;
  private RiZoneSortie TAFL1;
  private RiZoneSortie TAFL2;
  private RiZoneSortie TAFL3;
  private JLabel OBJ_54;
  private JLabel OBJ_41;
  private XRiCheckBox LNIN3;
  private JLabel OBJ_60;
  private JLabel OBJ_66;
  private JLabel OBJ_72;
  private XRiTextField LNNBR;
  private XRiTextField LNQB1;
  private XRiTextField LNQB2;
  private XRiTextField LNQB3;
  private RiZoneSortie OPQUA;
  private JLabel OBJ_87;
  private JLabel OBJ_38;
  private XRiTextField LNBA1;
  private XRiTextField LNBA2;
  private XRiTextField LNBA3;
  private RiZoneSortie OPPOS;
  private JLabel OBJ_58;
  private JLabel OBJ_57;
  private XRiTextField LNNL1;
  private XRiTextField LNNL2;
  private XRiTextField LNNL3;
  private RiZoneSortie LNTYP;
  private JLabel OBJ_56;
  private JLabel OBJ_31;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private RiZoneSortie WTI1;
  private RiZoneSortie WTI2;
  private RiZoneSortie WTI3;
  private RiZoneSortie WTI4;
  private RiZoneSortie WTI5;
  private XRiTextField LNZP1;
  private XRiTextField LNZP2;
  private XRiTextField LNZP3;
  private XRiTextField LNZP4;
  private XRiTextField LNZP5;
  private RiZoneSortie LNUN1;
  private RiZoneSortie LNUN2;
  private RiZoneSortie LNUN3;
  private RiZoneSortie LNAF1;
  private RiZoneSortie LNAF2;
  private RiZoneSortie LNAF3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BT1;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
