/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.tgpm.TGPM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.SNPhotoArticle;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class TGPM11FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public TGPM11FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    xTitledPanel1.setRightDecoration(detail_fab);
    
    // Ajout
    initDiverses();
    ENIN3.setValeursSelection("V", "K");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@GREP@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    WTPPG.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTPPG@")).trim());
    K44T6X.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@K44T6X@")).trim());
    OBJ_84.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC1@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC2@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // bouton info
    
    
    
    ENZP5.setEnabled(lexique.isPresent("ENZP5"));
    WTI5.setVisible(lexique.isPresent("WTI5"));
    ENZP4.setEnabled(lexique.isPresent("ENZP4"));
    WTI4.setVisible(lexique.isPresent("WTI4"));
    ENZP3.setEnabled(lexique.isPresent("ENZP3"));
    WTI3.setVisible(lexique.isPresent("WTI3"));
    ENZP2.setEnabled(lexique.isPresent("ENZP2"));
    WTI2.setVisible(lexique.isPresent("WTI2"));
    ENZP1.setEnabled(lexique.isPresent("ENZP1"));
    WTI1.setVisible(lexique.isPresent("WTI1"));
    ENFPV.setEnabled(lexique.isPresent("ENFPV"));
    WFAM.setEnabled(lexique.isPresent("WFAM"));
    OBJ_89.setVisible(!lexique.HostFieldGetData("WOBS").trim().equalsIgnoreCase(""));
    OBJ_88.setVisible(lexique.HostFieldGetData("WOBS").equalsIgnoreCase(""));
    // ENIN3.setEnabled( lexique.isPresent("ENIN3"));
    // ENIN3.setSelected(lexique.HostFieldGetData("ENIN3").equalsIgnoreCase("V"));
    BTNRI.setVisible(lexique.HostFieldGetData("&&BASCULE").equalsIgnoreCase("P"));
    OBJ_91.setVisible(lexique.isPresent("LMC2"));
    OBJ_84.setVisible(lexique.isPresent("LMC1"));
    ENCLA2.setEnabled(lexique.isPresent("ENCLA2"));
    ENCLA1.setEnabled(lexique.isPresent("ENCLA1"));
    ENART.setEnabled(lexique.isPresent("ENART"));
    ENLIB.setEnabled(lexique.isPresent("ENLIB"));
    A1LB3.setVisible(lexique.isPresent("A1LB3"));
    A1LB2.setVisible(lexique.isPresent("A1LB2"));
    A1LB1.setVisible(lexique.isPresent("A1LB1"));
    A1LIB.setVisible(lexique.isPresent("A1LIB"));
    PHO.setVisible(lexique.HostFieldGetData("&&BASCULE").equalsIgnoreCase("P"));
    
    // TODO Icones
    OBJ_89.setIcon(lexique.chargerImage("images/icones_35.gif", true));
    OBJ_88.setIcon(lexique.chargerImage("images/icones_30.gif", true));
    BTNRI.setIcon(lexique.chargerImage("images/iconesml_941.gif", true));
    
    // Charger la photographie de l'article
    snPhotoArticle.chargerImageArticle(
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim(),
        ENART.getText(), lexique.isTrue("N51"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD1.getInvoker().getName());
  }
  
  private void BTNRIActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // Scriptcall ("G_BASCUL")
    // touche="F5"
    // Scriptcall ("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_88ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_89ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void detail_fabActionPerformed(ActionEvent e) {
    // Scriptcall ("G_BASCUL")
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    ENETB = new XRiTextField();
    OBJ_50 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    OBJ_75 = new JLabel();
    ENART = new XRiTextField();
    OBJ_71 = new JLabel();
    OBJ_73 = new JLabel();
    WTPPG = new RiZoneSortie();
    ENIN3 = new XRiCheckBox();
    OBJ_69 = new JLabel();
    OBJ_64 = new JLabel();
    WFAM = new XRiTextField();
    ENFPV = new XRiTextField();
    K44T6X = new RiZoneSortie();
    snPhotoArticle = new SNPhotoArticle();
    xTitledPanel2 = new JXTitledPanel();
    ENLIB = new XRiTextField();
    ENCLA1 = new XRiTextField();
    ENCLA2 = new XRiTextField();
    OBJ_84 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_88 = new JButton();
    OBJ_89 = new JButton();
    OBJ_86 = new JLabel();
    WTI1 = new JLabel();
    ENZP1 = new XRiTextField();
    WTI2 = new JLabel();
    ENZP2 = new XRiTextField();
    WTI3 = new JLabel();
    ENZP3 = new XRiTextField();
    WTI4 = new JLabel();
    ENZP4 = new XRiTextField();
    WTI5 = new JLabel();
    ENZP5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTD1 = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    BTNRI = new JButton();
    detail_fab = new SNBoutonDetail();
    PHO = new JPanel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1045, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ent\u00eate de kit");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setName("OBJ_48");

          //---- ENETB ----
          ENETB.setComponentPopupMenu(BTD);
          ENETB.setName("ENETB");

          //---- OBJ_50 ----
          OBJ_50.setText("@GREP@");
          OBJ_50.setName("OBJ_50");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ENETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(ENETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 290));
            menus_haut.setPreferredSize(new Dimension(160, 290));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(845, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Fabrication");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            xTitledPanel1ContentContainer.add(A1LIB);
            A1LIB.setBounds(20, 105, 279, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            xTitledPanel1ContentContainer.add(A1LB1);
            A1LB1.setBounds(20, 135, 279, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setText("@A1LB2@");
            A1LB2.setName("A1LB2");
            xTitledPanel1ContentContainer.add(A1LB2);
            A1LB2.setBounds(20, 165, 279, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setText("@A1LB3@");
            A1LB3.setName("A1LB3");
            xTitledPanel1ContentContainer.add(A1LB3);
            A1LB3.setBounds(20, 195, 279, A1LB3.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Formule de calcul du prix de vente");
            OBJ_75.setName("OBJ_75");
            xTitledPanel1ContentContainer.add(OBJ_75);
            OBJ_75.setBounds(315, 79, 200, 20);

            //---- ENART ----
            ENART.setComponentPopupMenu(BTD);
            ENART.setName("ENART");
            xTitledPanel1ContentContainer.add(ENART);
            ENART.setBounds(85, 15, 215, ENART.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("Disponibilit\u00e9 composants");
            OBJ_71.setName("OBJ_71");
            xTitledPanel1ContentContainer.add(OBJ_71);
            OBJ_71.setBounds(315, 18, 200, 22);

            //---- OBJ_73 ----
            OBJ_73.setText("Prix de revient");
            OBJ_73.setName("OBJ_73");
            xTitledPanel1ContentContainer.add(OBJ_73);
            OBJ_73.setBounds(315, 49, 200, 20);

            //---- WTPPG ----
            WTPPG.setText("@WTPPG@");
            WTPPG.setName("WTPPG");
            xTitledPanel1ContentContainer.add(WTPPG);
            WTPPG.setBounds(515, 47, 90, WTPPG.getPreferredSize().height);

            //---- ENIN3 ----
            ENIN3.setText("Virtuel");
            ENIN3.setToolTipText("Composants g\u00e9r\u00e9s en stock mais pas g\u00e9n\u00e9r\u00e9s dans le bon de vente");
            ENIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ENIN3.setName("ENIN3");
            xTitledPanel1ContentContainer.add(ENIN3);
            ENIN3.setBounds(20, 79, 88, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("Famille");
            OBJ_69.setName("OBJ_69");
            xTitledPanel1ContentContainer.add(OBJ_69);
            OBJ_69.setBounds(20, 51, 55, 16);

            //---- OBJ_64 ----
            OBJ_64.setText("Article");
            OBJ_64.setName("OBJ_64");
            xTitledPanel1ContentContainer.add(OBJ_64);
            OBJ_64.setBounds(20, 21, 55, 16);

            //---- WFAM ----
            WFAM.setComponentPopupMenu(null);
            WFAM.setName("WFAM");
            xTitledPanel1ContentContainer.add(WFAM);
            WFAM.setBounds(85, 45, 40, WFAM.getPreferredSize().height);

            //---- ENFPV ----
            ENFPV.setComponentPopupMenu(BTD);
            ENFPV.setName("ENFPV");
            xTitledPanel1ContentContainer.add(ENFPV);
            ENFPV.setBounds(515, 75, 40, ENFPV.getPreferredSize().height);

            //---- K44T6X ----
            K44T6X.setText("@K44T6X@");
            K44T6X.setName("K44T6X");
            xTitledPanel1ContentContainer.add(K44T6X);
            K44T6X.setBounds(515, 17, 75, K44T6X.getPreferredSize().height);

            //---- snPhotoArticle ----
            snPhotoArticle.setOpaque(false);
            snPhotoArticle.setName("snPhotoArticle");
            xTitledPanel1ContentContainer.add(snPhotoArticle);
            snPhotoArticle.setBounds(625, 105, 145, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setBorder(new DropShadowBorder());
            xTitledPanel2.setTitle("Description");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- ENLIB ----
            ENLIB.setComponentPopupMenu(null);
            ENLIB.setName("ENLIB");
            xTitledPanel2ContentContainer.add(ENLIB);
            ENLIB.setBounds(440, 15, 335, ENLIB.getPreferredSize().height);

            //---- ENCLA1 ----
            ENCLA1.setComponentPopupMenu(null);
            ENCLA1.setName("ENCLA1");
            xTitledPanel2ContentContainer.add(ENCLA1);
            ENCLA1.setBounds(170, 15, 210, ENCLA1.getPreferredSize().height);

            //---- ENCLA2 ----
            ENCLA2.setComponentPopupMenu(null);
            ENCLA2.setName("ENCLA2");
            xTitledPanel2ContentContainer.add(ENCLA2);
            ENCLA2.setBounds(170, 45, 210, ENCLA2.getPreferredSize().height);

            //---- OBJ_84 ----
            OBJ_84.setText("@LMC1@");
            OBJ_84.setName("OBJ_84");
            xTitledPanel2ContentContainer.add(OBJ_84);
            OBJ_84.setBounds(20, 19, 150, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("@LMC2@");
            OBJ_91.setName("OBJ_91");
            xTitledPanel2ContentContainer.add(OBJ_91);
            OBJ_91.setBounds(20, 49, 150, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("");
            OBJ_88.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_88.setName("OBJ_88");
            OBJ_88.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_88ActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(OBJ_88);
            OBJ_88.setBounds(660, 15, 57, 20);

            //---- OBJ_89 ----
            OBJ_89.setText("");
            OBJ_89.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_89.setName("OBJ_89");
            OBJ_89.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_89ActionPerformed(e);
              }
            });
            xTitledPanel2ContentContainer.add(OBJ_89);
            OBJ_89.setBounds(660, 15, 57, 20);

            //---- OBJ_86 ----
            OBJ_86.setText("Libell\u00e9");
            OBJ_86.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_86.setName("OBJ_86");
            xTitledPanel2ContentContainer.add(OBJ_86);
            OBJ_86.setBounds(379, 20, 55, 20);

            //---- WTI1 ----
            WTI1.setText("@WTI1@");
            WTI1.setHorizontalAlignment(SwingConstants.RIGHT);
            WTI1.setName("WTI1");
            xTitledPanel2ContentContainer.add(WTI1);
            WTI1.setBounds(400, 49, 34, 20);

            //---- ENZP1 ----
            ENZP1.setComponentPopupMenu(BTD);
            ENZP1.setName("ENZP1");
            xTitledPanel2ContentContainer.add(ENZP1);
            ENZP1.setBounds(440, 45, 34, ENZP1.getPreferredSize().height);

            //---- WTI2 ----
            WTI2.setText("@WTI2@");
            WTI2.setHorizontalAlignment(SwingConstants.RIGHT);
            WTI2.setName("WTI2");
            xTitledPanel2ContentContainer.add(WTI2);
            WTI2.setBounds(480, 49, 34, 20);

            //---- ENZP2 ----
            ENZP2.setComponentPopupMenu(BTD);
            ENZP2.setName("ENZP2");
            xTitledPanel2ContentContainer.add(ENZP2);
            ENZP2.setBounds(515, 45, 34, ENZP2.getPreferredSize().height);

            //---- WTI3 ----
            WTI3.setText("@WTI3@");
            WTI3.setHorizontalAlignment(SwingConstants.RIGHT);
            WTI3.setName("WTI3");
            xTitledPanel2ContentContainer.add(WTI3);
            WTI3.setBounds(555, 49, 34, 20);

            //---- ENZP3 ----
            ENZP3.setComponentPopupMenu(BTD);
            ENZP3.setName("ENZP3");
            xTitledPanel2ContentContainer.add(ENZP3);
            ENZP3.setBounds(590, 45, 34, ENZP3.getPreferredSize().height);

            //---- WTI4 ----
            WTI4.setText("@WTI4@");
            WTI4.setHorizontalAlignment(SwingConstants.RIGHT);
            WTI4.setName("WTI4");
            xTitledPanel2ContentContainer.add(WTI4);
            WTI4.setBounds(630, 49, 34, 20);

            //---- ENZP4 ----
            ENZP4.setComponentPopupMenu(BTD);
            ENZP4.setName("ENZP4");
            xTitledPanel2ContentContainer.add(ENZP4);
            ENZP4.setBounds(665, 45, 34, ENZP4.getPreferredSize().height);

            //---- WTI5 ----
            WTI5.setText("@WTI5@");
            WTI5.setHorizontalAlignment(SwingConstants.RIGHT);
            WTI5.setName("WTI5");
            xTitledPanel2ContentContainer.add(WTI5);
            WTI5.setBounds(705, 49, 34, 20);

            //---- ENZP5 ----
            ENZP5.setComponentPopupMenu(BTD);
            ENZP5.setName("ENZP5");
            xTitledPanel2ContentContainer.add(ENZP5);
            ENZP5.setBounds(740, 45, 34, ENZP5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 795, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 795, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTD1 ========
    {
      BTD1.setName("BTD1");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD1.add(OBJ_20);
    }

    //---- BTNRI ----
    BTNRI.setText("");
    BTNRI.setToolTipText("Affichage des infos concernant la fabrication");
    BTNRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BTNRI.setName("BTNRI");
    BTNRI.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BTNRIActionPerformed(e);
      }
    });

    //---- detail_fab ----
    detail_fab.setToolTipText("Affichage des infos concernant la fabrication");
    detail_fab.setName("detail_fab");
    detail_fab.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        detail_fabActionPerformed(e);
      }
    });

    //======== PHO ========
    {
      PHO.setComponentPopupMenu(BTD);
      PHO.setOpaque(false);
      PHO.setName("PHO");
      PHO.setLayout(null);

      PHO.setPreferredSize(new Dimension(130, 155));
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private XRiTextField ENETB;
  private JLabel OBJ_50;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private JLabel OBJ_75;
  private XRiTextField ENART;
  private JLabel OBJ_71;
  private JLabel OBJ_73;
  private RiZoneSortie WTPPG;
  private XRiCheckBox ENIN3;
  private JLabel OBJ_69;
  private JLabel OBJ_64;
  private XRiTextField WFAM;
  private XRiTextField ENFPV;
  private RiZoneSortie K44T6X;
  private SNPhotoArticle snPhotoArticle;
  private JXTitledPanel xTitledPanel2;
  private XRiTextField ENLIB;
  private XRiTextField ENCLA1;
  private XRiTextField ENCLA2;
  private JLabel OBJ_84;
  private JLabel OBJ_91;
  private JButton OBJ_88;
  private JButton OBJ_89;
  private JLabel OBJ_86;
  private JLabel WTI1;
  private XRiTextField ENZP1;
  private JLabel WTI2;
  private XRiTextField ENZP2;
  private JLabel WTI3;
  private XRiTextField ENZP3;
  private JLabel WTI4;
  private XRiTextField ENZP4;
  private JLabel WTI5;
  private XRiTextField ENZP5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD1;
  private JMenuItem OBJ_20;
  private JButton BTNRI;
  private SNBoutonDetail detail_fab;
  private JPanel PHO;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
