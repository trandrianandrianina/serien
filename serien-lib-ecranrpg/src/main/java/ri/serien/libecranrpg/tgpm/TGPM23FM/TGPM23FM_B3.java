
package ri.serien.libecranrpg.tgpm.TGPM23FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class TGPM23FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM23FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LBIN7.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_43.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDLIB@")).trim());
    LBORD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBORD@")).trim());
    TAFL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL1@")).trim());
    TAFL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL2@")).trim());
    TAFL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL3@")).trim());
    WQP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQP1@")).trim());
    WQP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQP2@")).trim());
    WQP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQP3@")).trim());
    OPQUA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPQUA@")).trim());
    LBTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBTYP@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    LBAF1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBAF1@")).trim());
    LBAF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBAF2@")).trim());
    LBAF3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBAF3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("LIGNE D'ORDRE DE FABRICATION"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_43 = new JLabel();
    LBLIB = new XRiTextField();
    LBORD = new RiZoneSortie();
    OBJ_36 = new JLabel();
    TAFL1 = new RiZoneSortie();
    TAFL2 = new RiZoneSortie();
    TAFL3 = new RiZoneSortie();
    OBJ_50 = new JLabel();
    LBIN7 = new XRiCheckBox();
    OBJ_59 = new JLabel();
    LBQB3 = new XRiTextField();
    LBNBR = new XRiTextField();
    LBQB1 = new XRiTextField();
    WQU1 = new XRiTextField();
    LBQC1 = new XRiTextField();
    LBCT1 = new XRiTextField();
    LBQB2 = new XRiTextField();
    WQU2 = new XRiTextField();
    LBQC2 = new XRiTextField();
    LBCT2 = new XRiTextField();
    WQU3 = new XRiTextField();
    LBQC3 = new XRiTextField();
    LBCT3 = new XRiTextField();
    WQP1 = new RiZoneSortie();
    WQP2 = new RiZoneSortie();
    WQP3 = new RiZoneSortie();
    LBBA1 = new XRiTextField();
    LBBA2 = new XRiTextField();
    LBBA3 = new XRiTextField();
    OBJ_57 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_25 = new JLabel();
    OPQUA = new RiZoneSortie();
    OPPOS = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_56 = new JLabel();
    LBNL1 = new XRiTextField();
    LBNL2 = new XRiTextField();
    LBNL3 = new XRiTextField();
    LBTYP = new RiZoneSortie();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    WTI1 = new RiZoneSortie();
    WTI2 = new RiZoneSortie();
    WTI3 = new RiZoneSortie();
    WTI4 = new RiZoneSortie();
    WTI5 = new RiZoneSortie();
    LBZP1 = new XRiTextField();
    LBZP2 = new XRiTextField();
    LBZP3 = new XRiTextField();
    LBZP4 = new XRiTextField();
    LBZP5 = new XRiTextField();
    LBUN1 = new XRiTextField();
    LBUN2 = new XRiTextField();
    LBUN3 = new XRiTextField();
    OBJ_55 = new JLabel();
    LBAF1 = new RiZoneSortie();
    LBAF2 = new RiZoneSortie();
    LBAF3 = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Temps planning");
    separator2 = compFactory.createSeparator("Autres co\u00fbts");
    separator3 = compFactory.createSeparator("Quantit\u00e9s");
    separator4 = compFactory.createSeparator("Affectation");
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1120, 390));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Informations techniques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Documents li\u00e9s");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_43 ----
          OBJ_43.setText("@ORDLIB@");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(465, 80, 232, 20);

          //---- LBLIB ----
          LBLIB.setComponentPopupMenu(BTD);
          LBLIB.setName("LBLIB");
          panel1.add(LBLIB);
          LBLIB.setBounds(465, 100, 310, LBLIB.getPreferredSize().height);

          //---- LBORD ----
          LBORD.setComponentPopupMenu(BTD);
          LBORD.setText("@LBORD@");
          LBORD.setName("LBORD");
          panel1.add(LBORD);
          LBORD.setBounds(241, 40, 210, LBORD.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("Qualification production");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(705, 44, 142, 20);

          //---- TAFL1 ----
          TAFL1.setComponentPopupMenu(BTD);
          TAFL1.setText("@TAFL1@");
          TAFL1.setName("TAFL1");
          panel1.add(TAFL1);
          TAFL1.setBounds(241, 225, 214, TAFL1.getPreferredSize().height);

          //---- TAFL2 ----
          TAFL2.setComponentPopupMenu(BTD);
          TAFL2.setText("@TAFL2@");
          TAFL2.setName("TAFL2");
          panel1.add(TAFL2);
          TAFL2.setBounds(241, 290, 214, TAFL2.getPreferredSize().height);

          //---- TAFL3 ----
          TAFL3.setComponentPopupMenu(BTD);
          TAFL3.setText("@TAFL3@");
          TAFL3.setName("TAFL3");
          panel1.add(TAFL3);
          TAFL3.setBounds(241, 320, 214, TAFL3.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Nombre d'op\u00e9rations");
          OBJ_50.setName("OBJ_50");
          panel1.add(OBJ_50);
          OBJ_50.setBounds(15, 140, 130, 20);

          //---- LBIN7 ----
          LBIN7.setText("Sous-traitance");
          LBIN7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          LBIN7.setName("LBIN7");
          panel1.add(LBIN7);
          LBIN7.setBounds(465, 140, 285, 20);

          //---- OBJ_59 ----
          OBJ_59.setText("Consomm\u00e9e");
          OBJ_59.setName("OBJ_59");
          panel1.add(OBJ_59);
          OBJ_59.setBounds(689, 205, 102, 18);

          //---- LBQB3 ----
          LBQB3.setComponentPopupMenu(BTD);
          LBQB3.setName("LBQB3");
          panel1.add(LBQB3);
          LBQB3.setBounds(15, 320, 106, LBQB3.getPreferredSize().height);

          //---- LBNBR ----
          LBNBR.setComponentPopupMenu(BTD);
          LBNBR.setName("LBNBR");
          panel1.add(LBNBR);
          LBNBR.setBounds(241, 136, 106, LBNBR.getPreferredSize().height);

          //---- LBQB1 ----
          LBQB1.setComponentPopupMenu(BTD);
          LBQB1.setName("LBQB1");
          panel1.add(LBQB1);
          LBQB1.setBounds(15, 225, 106, LBQB1.getPreferredSize().height);

          //---- WQU1 ----
          WQU1.setComponentPopupMenu(BTD);
          WQU1.setName("WQU1");
          panel1.add(WQU1);
          WQU1.setBounds(465, 225, 106, WQU1.getPreferredSize().height);

          //---- LBQC1 ----
          LBQC1.setComponentPopupMenu(BTD);
          LBQC1.setName("LBQC1");
          panel1.add(LBQC1);
          LBQC1.setBounds(689, 225, 106, LBQC1.getPreferredSize().height);

          //---- LBCT1 ----
          LBCT1.setComponentPopupMenu(BTD);
          LBCT1.setName("LBCT1");
          panel1.add(LBCT1);
          LBCT1.setBounds(810, 225, 106, LBCT1.getPreferredSize().height);

          //---- LBQB2 ----
          LBQB2.setComponentPopupMenu(BTD);
          LBQB2.setName("LBQB2");
          panel1.add(LBQB2);
          LBQB2.setBounds(15, 290, 106, LBQB2.getPreferredSize().height);

          //---- WQU2 ----
          WQU2.setComponentPopupMenu(BTD);
          WQU2.setName("WQU2");
          panel1.add(WQU2);
          WQU2.setBounds(465, 290, 106, WQU2.getPreferredSize().height);

          //---- LBQC2 ----
          LBQC2.setComponentPopupMenu(BTD);
          LBQC2.setName("LBQC2");
          panel1.add(LBQC2);
          LBQC2.setBounds(689, 290, 106, LBQC2.getPreferredSize().height);

          //---- LBCT2 ----
          LBCT2.setComponentPopupMenu(BTD);
          LBCT2.setName("LBCT2");
          panel1.add(LBCT2);
          LBCT2.setBounds(810, 290, 106, LBCT2.getPreferredSize().height);

          //---- WQU3 ----
          WQU3.setComponentPopupMenu(BTD);
          WQU3.setName("WQU3");
          panel1.add(WQU3);
          WQU3.setBounds(465, 320, 106, WQU3.getPreferredSize().height);

          //---- LBQC3 ----
          LBQC3.setComponentPopupMenu(BTD);
          LBQC3.setName("LBQC3");
          panel1.add(LBQC3);
          LBQC3.setBounds(689, 320, 106, LBQC3.getPreferredSize().height);

          //---- LBCT3 ----
          LBCT3.setComponentPopupMenu(BTD);
          LBCT3.setName("LBCT3");
          panel1.add(LBCT3);
          LBCT3.setBounds(810, 320, 106, LBCT3.getPreferredSize().height);

          //---- WQP1 ----
          WQP1.setComponentPopupMenu(BTD);
          WQP1.setText("@WQP1@");
          WQP1.setName("WQP1");
          panel1.add(WQP1);
          WQP1.setBounds(577, 225, 106, WQP1.getPreferredSize().height);

          //---- WQP2 ----
          WQP2.setComponentPopupMenu(BTD);
          WQP2.setText("@WQP2@");
          WQP2.setName("WQP2");
          panel1.add(WQP2);
          WQP2.setBounds(577, 290, 106, WQP2.getPreferredSize().height);

          //---- WQP3 ----
          WQP3.setComponentPopupMenu(BTD);
          WQP3.setText("@WQP3@");
          WQP3.setName("WQP3");
          panel1.add(WQP3);
          WQP3.setBounds(577, 320, 106, WQP3.getPreferredSize().height);

          //---- LBBA1 ----
          LBBA1.setComponentPopupMenu(BTD);
          LBBA1.setName("LBBA1");
          panel1.add(LBBA1);
          LBBA1.setBounds(153, 225, 66, LBBA1.getPreferredSize().height);

          //---- LBBA2 ----
          LBBA2.setComponentPopupMenu(BTD);
          LBBA2.setName("LBBA2");
          panel1.add(LBBA2);
          LBBA2.setBounds(153, 290, 66, LBBA2.getPreferredSize().height);

          //---- LBBA3 ----
          LBBA3.setComponentPopupMenu(BTD);
          LBBA3.setName("LBBA3");
          panel1.add(LBBA3);
          LBBA3.setBounds(153, 320, 66, LBBA3.getPreferredSize().height);

          //---- OBJ_57 ----
          OBJ_57.setText("Unitaire");
          OBJ_57.setName("OBJ_57");
          panel1.add(OBJ_57);
          OBJ_57.setBounds(465, 205, 70, 18);

          //---- OBJ_58 ----
          OBJ_58.setText("Pr\u00e9vue");
          OBJ_58.setName("OBJ_58");
          panel1.add(OBJ_58);
          OBJ_58.setBounds(577, 205, 69, 18);

          //---- OBJ_25 ----
          OBJ_25.setText("Op\u00e9ration");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(220, 15, 62, 20);

          //---- OPQUA ----
          OPQUA.setText("@OPQUA@");
          OPQUA.setName("OPQUA");
          panel1.add(OPQUA);
          OPQUA.setBounds(856, 40, 60, OPQUA.getPreferredSize().height);

          //---- OPPOS ----
          OPPOS.setName("OPPOS");
          panel1.add(OPPOS);
          OPPOS.setBounds(525, 40, 50, OPPOS.getPreferredSize().height);

          //---- OBJ_60 ----
          OBJ_60.setText("Co\u00fbt");
          OBJ_60.setName("OBJ_60");
          panel1.add(OBJ_60);
          OBJ_60.setBounds(810, 205, 55, 18);

          //---- OBJ_54 ----
          OBJ_54.setText("Besoin");
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(15, 205, 52, 18);

          //---- OBJ_34 ----
          OBJ_34.setText("Poste");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(465, 44, 38, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("Type");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(150, 15, 33, 20);

          //---- OBJ_56 ----
          OBJ_56.setText("Base");
          OBJ_56.setName("OBJ_56");
          panel1.add(OBJ_56);
          OBJ_56.setBounds(155, 205, 36, 18);

          //---- LBNL1 ----
          LBNL1.setComponentPopupMenu(BTD);
          LBNL1.setName("LBNL1");
          panel1.add(LBNL1);
          LBNL1.setBounds(15, 40, 34, LBNL1.getPreferredSize().height);

          //---- LBNL2 ----
          LBNL2.setComponentPopupMenu(BTD);
          LBNL2.setName("LBNL2");
          panel1.add(LBNL2);
          LBNL2.setBounds(60, 40, 34, LBNL2.getPreferredSize().height);

          //---- LBNL3 ----
          LBNL3.setComponentPopupMenu(BTD);
          LBNL3.setName("LBNL3");
          panel1.add(LBNL3);
          LBNL3.setBounds(105, 40, 34, LBNL3.getPreferredSize().height);

          //---- LBTYP ----
          LBTYP.setComponentPopupMenu(BTD);
          LBTYP.setText("@LBTYP@");
          LBTYP.setName("LBTYP");
          panel1.add(LBTYP);
          LBTYP.setBounds(145, 40, 50, LBTYP.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("Nl1");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(15, 20, 31, 18);

          //---- OBJ_27 ----
          OBJ_27.setText("Nl2");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(60, 20, 31, 18);

          //---- OBJ_28 ----
          OBJ_28.setText("Nl3");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(105, 20, 31, 18);

          //---- WTI1 ----
          WTI1.setText("@WTI1@");
          WTI1.setName("WTI1");
          panel1.add(WTI1);
          WTI1.setBounds(15, 70, 30, WTI1.getPreferredSize().height);

          //---- WTI2 ----
          WTI2.setText("@WTI2@");
          WTI2.setName("WTI2");
          panel1.add(WTI2);
          WTI2.setBounds(60, 70, 30, WTI2.getPreferredSize().height);

          //---- WTI3 ----
          WTI3.setText("@WTI3@");
          WTI3.setName("WTI3");
          panel1.add(WTI3);
          WTI3.setBounds(105, 70, 30, WTI3.getPreferredSize().height);

          //---- WTI4 ----
          WTI4.setText("@WTI4@");
          WTI4.setName("WTI4");
          panel1.add(WTI4);
          WTI4.setBounds(145, 70, 30, WTI4.getPreferredSize().height);

          //---- WTI5 ----
          WTI5.setText("@WTI5@");
          WTI5.setName("WTI5");
          panel1.add(WTI5);
          WTI5.setBounds(190, 70, 30, WTI5.getPreferredSize().height);

          //---- LBZP1 ----
          LBZP1.setComponentPopupMenu(BTD);
          LBZP1.setName("LBZP1");
          panel1.add(LBZP1);
          LBZP1.setBounds(15, 100, 30, LBZP1.getPreferredSize().height);

          //---- LBZP2 ----
          LBZP2.setComponentPopupMenu(BTD);
          LBZP2.setName("LBZP2");
          panel1.add(LBZP2);
          LBZP2.setBounds(60, 100, 30, LBZP2.getPreferredSize().height);

          //---- LBZP3 ----
          LBZP3.setComponentPopupMenu(BTD);
          LBZP3.setName("LBZP3");
          panel1.add(LBZP3);
          LBZP3.setBounds(105, 100, 30, LBZP3.getPreferredSize().height);

          //---- LBZP4 ----
          LBZP4.setComponentPopupMenu(BTD);
          LBZP4.setName("LBZP4");
          panel1.add(LBZP4);
          LBZP4.setBounds(145, 100, 30, LBZP4.getPreferredSize().height);

          //---- LBZP5 ----
          LBZP5.setComponentPopupMenu(BTD);
          LBZP5.setName("LBZP5");
          panel1.add(LBZP5);
          LBZP5.setBounds(190, 100, 30, LBZP5.getPreferredSize().height);

          //---- LBUN1 ----
          LBUN1.setComponentPopupMenu(BTD);
          LBUN1.setName("LBUN1");
          panel1.add(LBUN1);
          LBUN1.setBounds(122, 225, 30, LBUN1.getPreferredSize().height);

          //---- LBUN2 ----
          LBUN2.setComponentPopupMenu(BTD);
          LBUN2.setName("LBUN2");
          panel1.add(LBUN2);
          LBUN2.setBounds(120, 290, 30, LBUN2.getPreferredSize().height);

          //---- LBUN3 ----
          LBUN3.setComponentPopupMenu(BTD);
          LBUN3.setName("LBUN3");
          panel1.add(LBUN3);
          LBUN3.setBounds(120, 320, 30, LBUN3.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("Un");
          OBJ_55.setName("OBJ_55");
          panel1.add(OBJ_55);
          OBJ_55.setBounds(120, 205, 20, 18);

          //---- LBAF1 ----
          LBAF1.setComponentPopupMenu(BTD);
          LBAF1.setText("@LBAF1@");
          LBAF1.setName("LBAF1");
          panel1.add(LBAF1);
          LBAF1.setBounds(220, 225, 20, LBAF1.getPreferredSize().height);

          //---- LBAF2 ----
          LBAF2.setComponentPopupMenu(BTD);
          LBAF2.setText("@LBAF2@");
          LBAF2.setName("LBAF2");
          panel1.add(LBAF2);
          LBAF2.setBounds(220, 290, 20, LBAF2.getPreferredSize().height);

          //---- LBAF3 ----
          LBAF3.setComponentPopupMenu(BTD);
          LBAF3.setText("@LBAF3@");
          LBAF3.setName("LBAF3");
          panel1.add(LBAF3);
          LBAF3.setBounds(220, 320, 20, LBAF3.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(10, 175, 910, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          panel1.add(separator2);
          separator2.setBounds(10, 265, 910, separator2.getPreferredSize().height);

          //---- separator3 ----
          separator3.setName("separator3");
          panel1.add(separator3);
          separator3.setBounds(465, 190, 339, separator3.getPreferredSize().height);

          //---- separator4 ----
          separator4.setName("separator4");
          panel1.add(separator4);
          separator4.setBounds(220, 205, 235, separator4.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 930, 370);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_43;
  private XRiTextField LBLIB;
  private RiZoneSortie LBORD;
  private JLabel OBJ_36;
  private RiZoneSortie TAFL1;
  private RiZoneSortie TAFL2;
  private RiZoneSortie TAFL3;
  private JLabel OBJ_50;
  private XRiCheckBox LBIN7;
  private JLabel OBJ_59;
  private XRiTextField LBQB3;
  private XRiTextField LBNBR;
  private XRiTextField LBQB1;
  private XRiTextField WQU1;
  private XRiTextField LBQC1;
  private XRiTextField LBCT1;
  private XRiTextField LBQB2;
  private XRiTextField WQU2;
  private XRiTextField LBQC2;
  private XRiTextField LBCT2;
  private XRiTextField WQU3;
  private XRiTextField LBQC3;
  private XRiTextField LBCT3;
  private RiZoneSortie WQP1;
  private RiZoneSortie WQP2;
  private RiZoneSortie WQP3;
  private XRiTextField LBBA1;
  private XRiTextField LBBA2;
  private XRiTextField LBBA3;
  private JLabel OBJ_57;
  private JLabel OBJ_58;
  private JLabel OBJ_25;
  private RiZoneSortie OPQUA;
  private XRiTextField OPPOS;
  private JLabel OBJ_60;
  private JLabel OBJ_54;
  private JLabel OBJ_34;
  private JLabel OBJ_24;
  private JLabel OBJ_56;
  private XRiTextField LBNL1;
  private XRiTextField LBNL2;
  private XRiTextField LBNL3;
  private RiZoneSortie LBTYP;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private RiZoneSortie WTI1;
  private RiZoneSortie WTI2;
  private RiZoneSortie WTI3;
  private RiZoneSortie WTI4;
  private RiZoneSortie WTI5;
  private XRiTextField LBZP1;
  private XRiTextField LBZP2;
  private XRiTextField LBZP3;
  private XRiTextField LBZP4;
  private XRiTextField LBZP5;
  private XRiTextField LBUN1;
  private XRiTextField LBUN2;
  private XRiTextField LBUN3;
  private JLabel OBJ_55;
  private RiZoneSortie LBAF1;
  private RiZoneSortie LBAF2;
  private RiZoneSortie LBAF3;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JComponent separator4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
