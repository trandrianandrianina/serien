
package ri.serien.libecranrpg.tgpm.TGPM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TGPM21FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM21FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    LPR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPR1@")).trim());
    LPR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPR2@")).trim());
    LPR3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPR3@")).trim());
    LPR4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPR4@")).trim());
    LPR5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LPR5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Ordres de fabrication"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LPR1 = new RiZoneSortie();
    EBTPP1 = new XRiTextField();
    EBTPF1 = new XRiTextField();
    WTPE1 = new XRiTextField();
    LPR2 = new RiZoneSortie();
    EBTPP2 = new XRiTextField();
    EBTPF2 = new XRiTextField();
    WTPE2 = new XRiTextField();
    LPR3 = new RiZoneSortie();
    EBTPP3 = new XRiTextField();
    EBTPF3 = new XRiTextField();
    WTPE3 = new XRiTextField();
    LPR4 = new RiZoneSortie();
    EBTPP4 = new XRiTextField();
    EBTPF4 = new XRiTextField();
    WTPE4 = new XRiTextField();
    LPR5 = new RiZoneSortie();
    EBTPP5 = new XRiTextField();
    EBTPF5 = new XRiTextField();
    WTPE5 = new XRiTextField();
    WTPPG = new XRiTextField();
    WTPFG = new XRiTextField();
    WTPG = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 330));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Chiffrage du prix de revient"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- LPR1 ----
          LPR1.setText("@LPR1@");
          LPR1.setName("LPR1");
          panel1.add(LPR1);
          LPR1.setBounds(45, 70, 174, LPR1.getPreferredSize().height);

          //---- EBTPP1 ----
          EBTPP1.setName("EBTPP1");
          panel1.add(EBTPP1);
          EBTPP1.setBounds(240, 68, 100, EBTPP1.getPreferredSize().height);

          //---- EBTPF1 ----
          EBTPF1.setName("EBTPF1");
          panel1.add(EBTPF1);
          EBTPF1.setBounds(355, 68, 100, EBTPF1.getPreferredSize().height);

          //---- WTPE1 ----
          WTPE1.setName("WTPE1");
          panel1.add(WTPE1);
          WTPE1.setBounds(470, 68, 100, WTPE1.getPreferredSize().height);

          //---- LPR2 ----
          LPR2.setText("@LPR2@");
          LPR2.setName("LPR2");
          panel1.add(LPR2);
          LPR2.setBounds(45, 110, 174, LPR2.getPreferredSize().height);

          //---- EBTPP2 ----
          EBTPP2.setName("EBTPP2");
          panel1.add(EBTPP2);
          EBTPP2.setBounds(240, 105, 100, EBTPP2.getPreferredSize().height);

          //---- EBTPF2 ----
          EBTPF2.setName("EBTPF2");
          panel1.add(EBTPF2);
          EBTPF2.setBounds(355, 105, 100, EBTPF2.getPreferredSize().height);

          //---- WTPE2 ----
          WTPE2.setName("WTPE2");
          panel1.add(WTPE2);
          WTPE2.setBounds(470, 105, 100, WTPE2.getPreferredSize().height);

          //---- LPR3 ----
          LPR3.setText("@LPR3@");
          LPR3.setName("LPR3");
          panel1.add(LPR3);
          LPR3.setBounds(45, 145, 174, LPR3.getPreferredSize().height);

          //---- EBTPP3 ----
          EBTPP3.setName("EBTPP3");
          panel1.add(EBTPP3);
          EBTPP3.setBounds(240, 145, 100, EBTPP3.getPreferredSize().height);

          //---- EBTPF3 ----
          EBTPF3.setName("EBTPF3");
          panel1.add(EBTPF3);
          EBTPF3.setBounds(355, 145, 100, EBTPF3.getPreferredSize().height);

          //---- WTPE3 ----
          WTPE3.setName("WTPE3");
          panel1.add(WTPE3);
          WTPE3.setBounds(470, 145, 100, WTPE3.getPreferredSize().height);

          //---- LPR4 ----
          LPR4.setText("@LPR4@");
          LPR4.setName("LPR4");
          panel1.add(LPR4);
          LPR4.setBounds(45, 185, 174, LPR4.getPreferredSize().height);

          //---- EBTPP4 ----
          EBTPP4.setName("EBTPP4");
          panel1.add(EBTPP4);
          EBTPP4.setBounds(240, 185, 100, EBTPP4.getPreferredSize().height);

          //---- EBTPF4 ----
          EBTPF4.setName("EBTPF4");
          panel1.add(EBTPF4);
          EBTPF4.setBounds(355, 185, 100, EBTPF4.getPreferredSize().height);

          //---- WTPE4 ----
          WTPE4.setName("WTPE4");
          panel1.add(WTPE4);
          WTPE4.setBounds(470, 185, 100, WTPE4.getPreferredSize().height);

          //---- LPR5 ----
          LPR5.setText("@LPR5@");
          LPR5.setName("LPR5");
          panel1.add(LPR5);
          LPR5.setBounds(45, 225, 174, LPR5.getPreferredSize().height);

          //---- EBTPP5 ----
          EBTPP5.setName("EBTPP5");
          panel1.add(EBTPP5);
          EBTPP5.setBounds(240, 220, 100, EBTPP5.getPreferredSize().height);

          //---- EBTPF5 ----
          EBTPF5.setName("EBTPF5");
          panel1.add(EBTPF5);
          EBTPF5.setBounds(355, 220, 100, EBTPF5.getPreferredSize().height);

          //---- WTPE5 ----
          WTPE5.setName("WTPE5");
          panel1.add(WTPE5);
          WTPE5.setBounds(470, 220, 100, WTPE5.getPreferredSize().height);

          //---- WTPPG ----
          WTPPG.setName("WTPPG");
          panel1.add(WTPPG);
          WTPPG.setBounds(240, 260, 100, WTPPG.getPreferredSize().height);

          //---- WTPFG ----
          WTPFG.setName("WTPFG");
          panel1.add(WTPFG);
          WTPFG.setBounds(355, 260, 100, WTPFG.getPreferredSize().height);

          //---- WTPG ----
          WTPG.setName("WTPG");
          panel1.add(WTPG);
          WTPG.setBounds(470, 260, 100, WTPG.getPreferredSize().height);

          //---- label1 ----
          label1.setText("Totaux");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(45, 262, 174, 24);

          //---- label2 ----
          label2.setText("Proportionnel");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(240, 50, 100, 21);

          //---- label3 ----
          label3.setText("Fixe");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(355, 50, 100, 21);

          //---- label4 ----
          label4.setText("Fabrication");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(470, 50, 100, 21);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 620, 320);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie LPR1;
  private XRiTextField EBTPP1;
  private XRiTextField EBTPF1;
  private XRiTextField WTPE1;
  private RiZoneSortie LPR2;
  private XRiTextField EBTPP2;
  private XRiTextField EBTPF2;
  private XRiTextField WTPE2;
  private RiZoneSortie LPR3;
  private XRiTextField EBTPP3;
  private XRiTextField EBTPF3;
  private XRiTextField WTPE3;
  private RiZoneSortie LPR4;
  private XRiTextField EBTPP4;
  private XRiTextField EBTPF4;
  private XRiTextField WTPE4;
  private RiZoneSortie LPR5;
  private XRiTextField EBTPP5;
  private XRiTextField EBTPF5;
  private XRiTextField WTPE5;
  private XRiTextField WTPPG;
  private XRiTextField WTPFG;
  private XRiTextField WTPG;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
