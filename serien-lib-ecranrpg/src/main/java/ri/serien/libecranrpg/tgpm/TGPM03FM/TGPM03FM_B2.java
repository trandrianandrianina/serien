
package ri.serien.libecranrpg.tgpm.TGPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class TGPM03FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM03FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    PTLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PTLIB@")).trim());
    PTLIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PTLIB2@")).trim());
    PTLIB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PTLIB3@")).trim());
    TAFL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL1@")).trim());
    TAFL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL2@")).trim());
    TAFL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TAFL3@")).trim());
    LUN1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LUN1@")).trim());
    LUN2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LUN2@")).trim());
    LUN3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LUN3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    PTLIB = new RiZoneSortie();
    PTLIB2 = new RiZoneSortie();
    PTLIB3 = new RiZoneSortie();
    TAFL1 = new RiZoneSortie();
    TAFL2 = new RiZoneSortie();
    TAFL3 = new RiZoneSortie();
    LUN1 = new RiZoneSortie();
    LUN2 = new RiZoneSortie();
    LUN3 = new RiZoneSortie();
    OPCT1 = new XRiTextField();
    OPCT2 = new XRiTextField();
    OPCT3 = new XRiTextField();
    OBJ_20 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_36 = new JLabel();
    WQTE1 = new XRiTextField();
    WQTE2 = new XRiTextField();
    WQTE3 = new XRiTextField();
    OBJ_18 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_38 = new JLabel();
    separator1 = compFactory.createSeparator("Autre vue");
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(750, 335));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- PTLIB ----
          PTLIB.setText("@PTLIB@");
          PTLIB.setName("PTLIB");
          panel1.add(PTLIB);
          PTLIB.setBounds(80, 59, 231, 20);

          //---- PTLIB2 ----
          PTLIB2.setText("@PTLIB2@");
          PTLIB2.setName("PTLIB2");
          panel1.add(PTLIB2);
          PTLIB2.setBounds(80, 144, 231, 20);

          //---- PTLIB3 ----
          PTLIB3.setText("@PTLIB3@");
          PTLIB3.setName("PTLIB3");
          panel1.add(PTLIB3);
          PTLIB3.setBounds(80, 234, 231, 20);

          //---- TAFL1 ----
          TAFL1.setText("@TAFL1@");
          TAFL1.setName("TAFL1");
          panel1.add(TAFL1);
          TAFL1.setBounds(340, 89, 161, 20);

          //---- TAFL2 ----
          TAFL2.setText("@TAFL2@");
          TAFL2.setName("TAFL2");
          panel1.add(TAFL2);
          TAFL2.setBounds(340, 174, 161, 20);

          //---- TAFL3 ----
          TAFL3.setText("@TAFL3@");
          TAFL3.setName("TAFL3");
          panel1.add(TAFL3);
          TAFL3.setBounds(340, 264, 161, 20);

          //---- LUN1 ----
          LUN1.setText("@LUN1@");
          LUN1.setName("LUN1");
          panel1.add(LUN1);
          LUN1.setBounds(370, 59, 131, 20);

          //---- LUN2 ----
          LUN2.setText("@LUN2@");
          LUN2.setName("LUN2");
          panel1.add(LUN2);
          LUN2.setBounds(370, 144, 131, 20);

          //---- LUN3 ----
          LUN3.setText("@LUN3@");
          LUN3.setName("LUN3");
          panel1.add(LUN3);
          LUN3.setBounds(370, 234, 131, 20);

          //---- OPCT1 ----
          OPCT1.setName("OPCT1");
          panel1.add(OPCT1);
          OPCT1.setBounds(187, 85, 98, OPCT1.getPreferredSize().height);

          //---- OPCT2 ----
          OPCT2.setName("OPCT2");
          panel1.add(OPCT2);
          OPCT2.setBounds(187, 170, 98, OPCT2.getPreferredSize().height);

          //---- OPCT3 ----
          OPCT3.setName("OPCT3");
          panel1.add(OPCT3);
          OPCT3.setBounds(187, 260, 98, OPCT3.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Co\u00fbt unitaire");
          OBJ_20.setFont(OBJ_20.getFont().deriveFont(OBJ_20.getFont().getStyle() | Font.BOLD));
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(80, 89, 87, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("Co\u00fbt unitaire");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(80, 174, 87, 20);

          //---- OBJ_36 ----
          OBJ_36.setText("Co\u00fbt unitaire");
          OBJ_36.setFont(OBJ_36.getFont().deriveFont(OBJ_36.getFont().getStyle() | Font.BOLD));
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(80, 264, 87, 20);

          //---- WQTE1 ----
          WQTE1.setName("WQTE1");
          panel1.add(WQTE1);
          WQTE1.setBounds(20, 55, 50, WQTE1.getPreferredSize().height);

          //---- WQTE2 ----
          WQTE2.setName("WQTE2");
          panel1.add(WQTE2);
          WQTE2.setBounds(20, 140, 50, WQTE2.getPreferredSize().height);

          //---- WQTE3 ----
          WQTE3.setName("WQTE3");
          panel1.add(WQTE3);
          WQTE3.setBounds(20, 230, 50, WQTE3.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("par");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(340, 59, 23, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("par");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(340, 144, 23, 20);

          //---- OBJ_34 ----
          OBJ_34.setText("par");
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(335, 234, 23, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("de");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(305, 89, 19, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("de");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(305, 174, 19, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("de");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(305, 264, 19, 20);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(15, 15, 540, separator1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 570, 315);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie PTLIB;
  private RiZoneSortie PTLIB2;
  private RiZoneSortie PTLIB3;
  private RiZoneSortie TAFL1;
  private RiZoneSortie TAFL2;
  private RiZoneSortie TAFL3;
  private RiZoneSortie LUN1;
  private RiZoneSortie LUN2;
  private RiZoneSortie LUN3;
  private XRiTextField OPCT1;
  private XRiTextField OPCT2;
  private XRiTextField OPCT3;
  private JLabel OBJ_20;
  private JLabel OBJ_28;
  private JLabel OBJ_36;
  private XRiTextField WQTE1;
  private XRiTextField WQTE2;
  private XRiTextField WQTE3;
  private JLabel OBJ_18;
  private JLabel OBJ_26;
  private JLabel OBJ_34;
  private JLabel OBJ_22;
  private JLabel OBJ_30;
  private JLabel OBJ_38;
  private JComponent separator1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
