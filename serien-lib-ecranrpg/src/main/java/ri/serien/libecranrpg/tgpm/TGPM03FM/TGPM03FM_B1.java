
package ri.serien.libecranrpg.tgpm.TGPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class TGPM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    OPIN3.setValeursSelection("1", " ");
    OPIN1.setValeursSelection("1", " ");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    QPLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@QPLIBR@")).trim());
    OPCOD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OPCOD@")).trim());
    LMC1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC1@")).trim());
    LMC2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC2@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@ @LIBPG@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    QPLIBR = new RiZoneSortie();
    OPIN1 = new XRiCheckBox();
    PTLIB = new XRiTextField();
    OPLIB = new XRiTextField();
    OPCOD = new RiZoneSortie();
    TAFL1 = new XRiTextField();
    TAFL2 = new XRiTextField();
    TAFL3 = new XRiTextField();
    OPCLA1 = new XRiTextField();
    OPCLA2 = new XRiTextField();
    OBJ_51 = new JLabel();
    LMC1 = new JLabel();
    LMC2 = new JLabel();
    OPIN3 = new XRiCheckBox();
    OPQT1 = new XRiTextField();
    OPCT1 = new XRiTextField();
    OPQT2 = new XRiTextField();
    OPCT2 = new XRiTextField();
    OPQT3 = new XRiTextField();
    OPCT3 = new XRiTextField();
    OBJ_105 = new JLabel();
    OBJ_65 = new JLabel();
    OPPOS = new XRiTextField();
    OBJ_106 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_107 = new JLabel();
    OPBA1 = new XRiTextField();
    OPBA2 = new XRiTextField();
    OPBA3 = new XRiTextField();
    OPQUA = new XRiTextField();
    OBJ_59 = new JLabel();
    OBJ_67 = new JLabel();
    PTNBR = new XRiTextField();
    WTI1 = new RiZoneSortie();
    WTI2 = new RiZoneSortie();
    WTI3 = new RiZoneSortie();
    WTI4 = new RiZoneSortie();
    WTI5 = new RiZoneSortie();
    OPZP1 = new XRiTextField();
    OPZP2 = new XRiTextField();
    OPZP3 = new XRiTextField();
    OPZP4 = new XRiTextField();
    OPZP5 = new XRiTextField();
    OPUN1 = new XRiTextField();
    OPP11R = new XRiTextField();
    OPUN2 = new XRiTextField();
    OPP12R = new XRiTextField();
    OPUN3 = new XRiTextField();
    OPP13R = new XRiTextField();
    OBJ_66 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_70 = new JLabel();
    OPAF1 = new XRiTextField();
    OPAF2 = new XRiTextField();
    OPAF3 = new XRiTextField();
    separator1 = compFactory.createSeparator("Op\u00e9ration");
    separator2 = compFactory.createSeparator("Description");
    separator3 = compFactory.createSeparator("Poste");
    separator4 = compFactory.createSeparator("Co\u00fbts");
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(880, 550));
    setMaximumSize(new Dimension(880, 550));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Informations techniques");
            riSousMenu_bt6.setToolTipText("Informations techniques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Autres vues");
            riSousMenu_bt7.setToolTipText("Autres vues");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Documents li\u00e9s");
            riSousMenu_bt8.setToolTipText("Documents li\u00e9s");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- QPLIBR ----
          QPLIBR.setText("@QPLIBR@");
          QPLIBR.setName("QPLIBR");
          panel1.add(QPLIBR);
          QPLIBR.setBounds(285, 230, 342, 20);

          //---- OPIN1 ----
          OPIN1.setText("Duplication des informations techniques sur OF");
          OPIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPIN1.setName("OPIN1");
          panel1.add(OPIN1);
          OPIN1.setBounds(35, 75, 303, 20);

          //---- PTLIB ----
          PTLIB.setName("PTLIB");
          panel1.add(PTLIB);
          PTLIB.setBounds(210, 285, 232, PTLIB.getPreferredSize().height);

          //---- OPLIB ----
          OPLIB.setComponentPopupMenu(BTD);
          OPLIB.setName("OPLIB");
          panel1.add(OPLIB);
          OPLIB.setBounds(210, 41, 310, OPLIB.getPreferredSize().height);

          //---- OPCOD ----
          OPCOD.setText("@OPCOD@");
          OPCOD.setName("OPCOD");
          panel1.add(OPCOD);
          OPCOD.setBounds(35, 45, 162, 20);

          //---- TAFL1 ----
          TAFL1.setName("TAFL1");
          panel1.add(TAFL1);
          TAFL1.setBounds(465, 390, 162, TAFL1.getPreferredSize().height);

          //---- TAFL2 ----
          TAFL2.setName("TAFL2");
          panel1.add(TAFL2);
          TAFL2.setBounds(465, 460, 162, TAFL2.getPreferredSize().height);

          //---- TAFL3 ----
          TAFL3.setName("TAFL3");
          panel1.add(TAFL3);
          TAFL3.setBounds(465, 485, 162, TAFL3.getPreferredSize().height);

          //---- OPCLA1 ----
          OPCLA1.setComponentPopupMenu(BTD);
          OPCLA1.setName("OPCLA1");
          panel1.add(OPCLA1);
          OPCLA1.setBounds(210, 151, 210, OPCLA1.getPreferredSize().height);

          //---- OPCLA2 ----
          OPCLA2.setComponentPopupMenu(BTD);
          OPCLA2.setName("OPCLA2");
          panel1.add(OPCLA2);
          OPCLA2.setBounds(210, 176, 210, OPCLA2.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Qualification production");
          OBJ_51.setName("OBJ_51");
          panel1.add(OBJ_51);
          OBJ_51.setBounds(35, 230, 143, 20);

          //---- LMC1 ----
          LMC1.setText("@LMC1@");
          LMC1.setName("LMC1");
          panel1.add(LMC1);
          LMC1.setBounds(35, 155, 165, 20);

          //---- LMC2 ----
          LMC2.setText("@LMC2@");
          LMC2.setName("LMC2");
          panel1.add(LMC2);
          LMC2.setBounds(35, 180, 165, 20);

          //---- OPIN3 ----
          OPIN3.setText("Sous-traitance");
          OPIN3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OPIN3.setName("OPIN3");
          panel1.add(OPIN3);
          OPIN3.setBounds(435, 75, 125, 20);

          //---- OPQT1 ----
          OPQT1.setComponentPopupMenu(BTD);
          OPQT1.setName("OPQT1");
          panel1.add(OPQT1);
          OPQT1.setBounds(35, 390, 106, OPQT1.getPreferredSize().height);

          //---- OPCT1 ----
          OPCT1.setComponentPopupMenu(BTD);
          OPCT1.setName("OPCT1");
          panel1.add(OPCT1);
          OPCT1.setBounds(255, 390, 106, OPCT1.getPreferredSize().height);

          //---- OPQT2 ----
          OPQT2.setComponentPopupMenu(BTD);
          OPQT2.setName("OPQT2");
          panel1.add(OPQT2);
          OPQT2.setBounds(35, 460, 106, OPQT2.getPreferredSize().height);

          //---- OPCT2 ----
          OPCT2.setComponentPopupMenu(BTD);
          OPCT2.setName("OPCT2");
          panel1.add(OPCT2);
          OPCT2.setBounds(255, 460, 106, OPCT2.getPreferredSize().height);

          //---- OPQT3 ----
          OPQT3.setComponentPopupMenu(BTD);
          OPQT3.setName("OPQT3");
          panel1.add(OPQT3);
          OPQT3.setBounds(35, 485, 106, OPQT3.getPreferredSize().height);

          //---- OPCT3 ----
          OPCT3.setComponentPopupMenu(BTD);
          OPCT3.setName("OPCT3");
          panel1.add(OPCT3);
          OPCT3.setBounds(255, 485, 106, OPCT3.getPreferredSize().height);

          //---- OBJ_105 ----
          OBJ_105.setText("Temps \u00e0 planifier");
          OBJ_105.setFont(OBJ_105.getFont().deriveFont(OBJ_105.getFont().getStyle() | Font.BOLD));
          OBJ_105.setName("OBJ_105");
          panel1.add(OBJ_105);
          OBJ_105.setBounds(35, 350, 118, 15);

          //---- OBJ_65 ----
          OBJ_65.setText("Besoin");
          OBJ_65.setName("OBJ_65");
          panel1.add(OBJ_65);
          OBJ_65.setBounds(35, 370, 93, 17);

          //---- OPPOS ----
          OPPOS.setComponentPopupMenu(BTD);
          OPPOS.setName("OPPOS");
          panel1.add(OPPOS);
          OPPOS.setBounds(35, 285, 60, OPPOS.getPreferredSize().height);

          //---- OBJ_106 ----
          OBJ_106.setText("Autres co\u00fbts");
          OBJ_106.setFont(OBJ_106.getFont().deriveFont(OBJ_106.getFont().getStyle() | Font.BOLD));
          OBJ_106.setName("OBJ_106");
          panel1.add(OBJ_106);
          OBJ_106.setBounds(35, 435, 85, 15);

          //---- OBJ_68 ----
          OBJ_68.setText("Co\u00fbt");
          OBJ_68.setName("OBJ_68");
          panel1.add(OBJ_68);
          OBJ_68.setBounds(255, 370, 65, 17);

          //---- OBJ_107 ----
          OBJ_107.setText("Affectation");
          OBJ_107.setName("OBJ_107");
          panel1.add(OBJ_107);
          OBJ_107.setBounds(465, 370, 65, 17);

          //---- OPBA1 ----
          OPBA1.setComponentPopupMenu(BTD);
          OPBA1.setName("OPBA1");
          panel1.add(OPBA1);
          OPBA1.setBounds(185, 390, 66, OPBA1.getPreferredSize().height);

          //---- OPBA2 ----
          OPBA2.setComponentPopupMenu(BTD);
          OPBA2.setName("OPBA2");
          panel1.add(OPBA2);
          OPBA2.setBounds(185, 460, 66, OPBA2.getPreferredSize().height);

          //---- OPBA3 ----
          OPBA3.setComponentPopupMenu(BTD);
          OPBA3.setName("OPBA3");
          panel1.add(OPBA3);
          OPBA3.setBounds(185, 485, 66, OPBA3.getPreferredSize().height);

          //---- OPQUA ----
          OPQUA.setComponentPopupMenu(BTD);
          OPQUA.setName("OPQUA");
          panel1.add(OPQUA);
          OPQUA.setBounds(210, 226, 60, OPQUA.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("Nombre");
          OBJ_59.setName("OBJ_59");
          panel1.add(OBJ_59);
          OBJ_59.setBounds(465, 289, 52, 20);

          //---- OBJ_67 ----
          OBJ_67.setText("Base");
          OBJ_67.setName("OBJ_67");
          panel1.add(OBJ_67);
          OBJ_67.setBounds(185, 370, 55, 17);

          //---- PTNBR ----
          PTNBR.setComponentPopupMenu(BTD);
          PTNBR.setName("PTNBR");
          panel1.add(PTNBR);
          PTNBR.setBounds(585, 285, 42, PTNBR.getPreferredSize().height);

          //---- WTI1 ----
          WTI1.setText("@WTI1@");
          WTI1.setName("WTI1");
          panel1.add(WTI1);
          WTI1.setBounds(435, 155, 30, 20);

          //---- WTI2 ----
          WTI2.setText("@WTI2@");
          WTI2.setName("WTI2");
          panel1.add(WTI2);
          WTI2.setBounds(475, 155, 30, 20);

          //---- WTI3 ----
          WTI3.setText("@WTI3@");
          WTI3.setName("WTI3");
          panel1.add(WTI3);
          WTI3.setBounds(515, 155, 30, 20);

          //---- WTI4 ----
          WTI4.setText("@WTI4@");
          WTI4.setName("WTI4");
          panel1.add(WTI4);
          WTI4.setBounds(555, 155, 30, 20);

          //---- WTI5 ----
          WTI5.setText("@WTI5@");
          WTI5.setName("WTI5");
          panel1.add(WTI5);
          WTI5.setBounds(595, 155, 30, 20);

          //---- OPZP1 ----
          OPZP1.setComponentPopupMenu(BTD);
          OPZP1.setName("OPZP1");
          panel1.add(OPZP1);
          OPZP1.setBounds(435, 176, 30, OPZP1.getPreferredSize().height);

          //---- OPZP2 ----
          OPZP2.setComponentPopupMenu(BTD);
          OPZP2.setName("OPZP2");
          panel1.add(OPZP2);
          OPZP2.setBounds(475, 176, 30, OPZP2.getPreferredSize().height);

          //---- OPZP3 ----
          OPZP3.setComponentPopupMenu(BTD);
          OPZP3.setName("OPZP3");
          panel1.add(OPZP3);
          OPZP3.setBounds(515, 176, 30, OPZP3.getPreferredSize().height);

          //---- OPZP4 ----
          OPZP4.setComponentPopupMenu(BTD);
          OPZP4.setName("OPZP4");
          panel1.add(OPZP4);
          OPZP4.setBounds(555, 176, 30, OPZP4.getPreferredSize().height);

          //---- OPZP5 ----
          OPZP5.setComponentPopupMenu(BTD);
          OPZP5.setName("OPZP5");
          panel1.add(OPZP5);
          OPZP5.setBounds(595, 176, 30, OPZP5.getPreferredSize().height);

          //---- OPUN1 ----
          OPUN1.setComponentPopupMenu(BTD);
          OPUN1.setName("OPUN1");
          panel1.add(OPUN1);
          OPUN1.setBounds(145, 390, 30, OPUN1.getPreferredSize().height);

          //---- OPP11R ----
          OPP11R.setComponentPopupMenu(BTD);
          OPP11R.setName("OPP11R");
          panel1.add(OPP11R);
          OPP11R.setBounds(365, 390, 26, OPP11R.getPreferredSize().height);

          //---- OPUN2 ----
          OPUN2.setComponentPopupMenu(BTD);
          OPUN2.setName("OPUN2");
          panel1.add(OPUN2);
          OPUN2.setBounds(145, 460, 30, OPUN2.getPreferredSize().height);

          //---- OPP12R ----
          OPP12R.setComponentPopupMenu(BTD);
          OPP12R.setName("OPP12R");
          panel1.add(OPP12R);
          OPP12R.setBounds(365, 460, 26, OPP12R.getPreferredSize().height);

          //---- OPUN3 ----
          OPUN3.setComponentPopupMenu(BTD);
          OPUN3.setName("OPUN3");
          panel1.add(OPUN3);
          OPUN3.setBounds(145, 485, 30, OPUN3.getPreferredSize().height);

          //---- OPP13R ----
          OPP13R.setComponentPopupMenu(BTD);
          OPP13R.setName("OPP13R");
          panel1.add(OPP13R);
          OPP13R.setBounds(365, 485, 26, OPP13R.getPreferredSize().height);

          //---- OBJ_66 ----
          OBJ_66.setText("Un");
          OBJ_66.setName("OBJ_66");
          panel1.add(OBJ_66);
          OBJ_66.setBounds(145, 370, 24, 17);

          //---- OBJ_69 ----
          OBJ_69.setText("ou");
          OBJ_69.setName("OBJ_69");
          panel1.add(OBJ_69);
          OBJ_69.setBounds(330, 370, 24, 17);

          //---- OBJ_70 ----
          OBJ_70.setText("P1");
          OBJ_70.setName("OBJ_70");
          panel1.add(OBJ_70);
          OBJ_70.setBounds(365, 370, 24, 17);

          //---- OPAF1 ----
          OPAF1.setComponentPopupMenu(BTD);
          OPAF1.setName("OPAF1");
          panel1.add(OPAF1);
          OPAF1.setBounds(400, 390, 18, OPAF1.getPreferredSize().height);

          //---- OPAF2 ----
          OPAF2.setComponentPopupMenu(BTD);
          OPAF2.setName("OPAF2");
          panel1.add(OPAF2);
          OPAF2.setBounds(400, 460, 18, OPAF2.getPreferredSize().height);

          //---- OPAF3 ----
          OPAF3.setComponentPopupMenu(BTD);
          OPAF3.setName("OPAF3");
          panel1.add(OPAF3);
          OPAF3.setBounds(400, 485, 18, OPAF3.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(15, 15, 645, separator1.getPreferredSize().height);

          //---- separator2 ----
          separator2.setName("separator2");
          panel1.add(separator2);
          separator2.setBounds(15, 120, 645, separator2.getPreferredSize().height);

          //---- separator3 ----
          separator3.setName("separator3");
          panel1.add(separator3);
          separator3.setBounds(15, 260, 645, separator3.getPreferredSize().height);

          //---- separator4 ----
          separator4.setName("separator4");
          panel1.add(separator4);
          separator4.setBounds(15, 325, 645, separator4.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 690, 530);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie QPLIBR;
  private XRiCheckBox OPIN1;
  private XRiTextField PTLIB;
  private XRiTextField OPLIB;
  private RiZoneSortie OPCOD;
  private XRiTextField TAFL1;
  private XRiTextField TAFL2;
  private XRiTextField TAFL3;
  private XRiTextField OPCLA1;
  private XRiTextField OPCLA2;
  private JLabel OBJ_51;
  private JLabel LMC1;
  private JLabel LMC2;
  private XRiCheckBox OPIN3;
  private XRiTextField OPQT1;
  private XRiTextField OPCT1;
  private XRiTextField OPQT2;
  private XRiTextField OPCT2;
  private XRiTextField OPQT3;
  private XRiTextField OPCT3;
  private JLabel OBJ_105;
  private JLabel OBJ_65;
  private XRiTextField OPPOS;
  private JLabel OBJ_106;
  private JLabel OBJ_68;
  private JLabel OBJ_107;
  private XRiTextField OPBA1;
  private XRiTextField OPBA2;
  private XRiTextField OPBA3;
  private XRiTextField OPQUA;
  private JLabel OBJ_59;
  private JLabel OBJ_67;
  private XRiTextField PTNBR;
  private RiZoneSortie WTI1;
  private RiZoneSortie WTI2;
  private RiZoneSortie WTI3;
  private RiZoneSortie WTI4;
  private RiZoneSortie WTI5;
  private XRiTextField OPZP1;
  private XRiTextField OPZP2;
  private XRiTextField OPZP3;
  private XRiTextField OPZP4;
  private XRiTextField OPZP5;
  private XRiTextField OPUN1;
  private XRiTextField OPP11R;
  private XRiTextField OPUN2;
  private XRiTextField OPP12R;
  private XRiTextField OPUN3;
  private XRiTextField OPP13R;
  private JLabel OBJ_66;
  private JLabel OBJ_69;
  private JLabel OBJ_70;
  private XRiTextField OPAF1;
  private XRiTextField OPAF2;
  private XRiTextField OPAF3;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JComponent separator4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
