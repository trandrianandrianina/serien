
package ri.serien.libecranrpg.tgpm.TGPM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.metier.referentiel.article.snphotoarticle.SNPhotoArticle;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class TGPM21FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] EBIN1_Value = { "", "1", "2", "3", };
  
  public TGPM21FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    EBIN1.setValeurs(EBIN1_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    A1LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB2@")).trim());
    A1LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB3@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC1@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC2@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    c.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETALIB@")).trim());
    CLNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    MALIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    UNLIBR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNLIBR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    panel7.setVisible(lexique.isTrue("61"));
    OBJ_147.setVisible(lexique.isTrue("80"));
    CLNOM.setVisible(lexique.isTrue("80"));
    OBJ_125.setVisible(lexique.isTrue("N31"));
    OBJ_129.setVisible(lexique.isTrue("N31"));
    
    

    
    p_bpresentation.setCodeEtablissement(EBETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(EBETB.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Ordres de fabrication"));
    
    // Charger la photographie de l'article
    snPhotoArticle.chargerImageArticle(
        lexique.HostFieldGetData("RACINE").trim() + lexique.HostFieldGetData("CHEM1").trim() + lexique.HostFieldGetData("CHEM2").trim(),
        EBART.getText(), lexique.isTrue("N51"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("V06FO", 0, "2");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD1.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD1.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_58 = new JLabel();
    EBNUM = new XRiTextField();
    OBJ_59 = new JLabel();
    EBETB = new XRiTextField();
    EBSUF = new XRiTextField();
    label8 = new JLabel();
    EBART = new XRiTextField();
    OBJ_77 = new JLabel();
    EBNIV = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu16 = new RiSousMenu();
    riSousMenu_bt16 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    A1LB2 = new RiZoneSortie();
    A1LB3 = new RiZoneSortie();
    panel3 = new JPanel();
    EBLIB = new XRiTextField();
    EBCLA1 = new XRiTextField();
    EBCLA2 = new XRiTextField();
    OBJ_113 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_92 = new JLabel();
    OBJ_105 = new JLabel();
    EBCCT = new XRiTextField();
    OBJ_107 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_110 = new JLabel();
    EBSAN = new XRiTextField();
    EBACT = new XRiTextField();
    EBFPV = new XRiTextField();
    WTI1 = new RiZoneSortie();
    EBZP1 = new XRiTextField();
    WTI2 = new RiZoneSortie();
    EBZP2 = new XRiTextField();
    WTI3 = new RiZoneSortie();
    EBZP3 = new XRiTextField();
    WTI4 = new RiZoneSortie();
    EBZP4 = new XRiTextField();
    WTI5 = new RiZoneSortie();
    EBZP5 = new XRiTextField();
    panel4 = new JPanel();
    OBJ_115 = new JTabbedPane();
    OBJ_116 = new JPanel();
    c = new RiZoneSortie();
    OBJ_123 = new JLabel();
    OBJ_127 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_129 = new JLabel();
    OBJ_121 = new JLabel();
    OBJ_119 = new JLabel();
    EBQMI = new XRiTextField();
    EBQTR = new XRiTextField();
    ENQTE = new XRiTextField();
    EBQTE = new XRiTextField();
    K44T5 = new XRiTextField();
    K44T6X = new XRiTextField();
    OBJ_131 = new JPanel();
    EBIN1 = new XRiComboBox();
    CLNOM = new RiZoneSortie();
    MALIBR = new RiZoneSortie();
    UNLIBR = new RiZoneSortie();
    OBJ_141 = new JLabel();
    OBJ_140 = new JLabel();
    OBJ_134 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_144 = new JLabel();
    E1NUM = new XRiTextField();
    EBNUM0 = new XRiTextField();
    WCLI = new XRiTextField();
    OBJ_137 = new JLabel();
    WLIV = new XRiTextField();
    EBMAG = new XRiTextField();
    A1UNS = new XRiTextField();
    E1SUF = new XRiTextField();
    EBSUF0 = new XRiTextField();
    panel5 = new JPanel();
    EBDDPX = new XRiCalendrier();
    EBDFPX = new XRiCalendrier();
    OBJ_177 = new JLabel();
    EBDFSX = new XRiCalendrier();
    OBJ_165 = new JLabel();
    OBJ_173 = new JLabel();
    EBHFSH = new XRiTextField();
    EBHFSM = new XRiTextField();
    OBJ_180 = new JLabel();
    panel6 = new JPanel();
    WTPPG = new XRiTextField();
    WTPFG = new XRiTextField();
    WTPG = new XRiTextField();
    WTUG = new XRiTextField();
    STPPG = new XRiTextField();
    STPFG = new XRiTextField();
    STPG = new XRiTextField();
    STUG = new XRiTextField();
    ETPPG = new XRiTextField();
    ETPFG = new XRiTextField();
    ETPG = new XRiTextField();
    ETUG = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label6 = new JLabel();
    label5 = new JLabel();
    label7 = new JLabel();
    panel7 = new JPanel();
    OBJ_183 = new JLabel();
    OBJ_184 = new JLabel();
    ENDDVX = new XRiCalendrier();
    OBJ_185 = new JLabel();
    ENDFVX = new XRiCalendrier();
    panel1 = new JPanel();
    snPhotoArticle = new SNPhotoArticle();
    panel8 = new JPanel();
    BTD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    BTD1 = new JPopupMenu();
    OBJ_25 = new JMenuItem();
    OBJ_24 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ordres de fabrication");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_58 ----
          OBJ_58.setText("Etablissement");
          OBJ_58.setName("OBJ_58");
          p_tete_gauche.add(OBJ_58);
          OBJ_58.setBounds(5, 5, 93, 18);

          //---- EBNUM ----
          EBNUM.setName("EBNUM");
          p_tete_gauche.add(EBNUM);
          EBNUM.setBounds(230, 0, 66, EBNUM.getPreferredSize().height);

          //---- OBJ_59 ----
          OBJ_59.setText("Num\u00e9ro");
          OBJ_59.setName("OBJ_59");
          p_tete_gauche.add(OBJ_59);
          OBJ_59.setBounds(170, 5, 51, 18);

          //---- EBETB ----
          EBETB.setComponentPopupMenu(BTD);
          EBETB.setName("EBETB");
          p_tete_gauche.add(EBETB);
          EBETB.setBounds(100, 0, 40, EBETB.getPreferredSize().height);

          //---- EBSUF ----
          EBSUF.setName("EBSUF");
          p_tete_gauche.add(EBSUF);
          EBSUF.setBounds(300, 0, 20, EBSUF.getPreferredSize().height);

          //---- label8 ----
          label8.setText("Nomenclature");
          label8.setName("label8");
          p_tete_gauche.add(label8);
          label8.setBounds(350, 5, 100, 19);

          //---- EBART ----
          EBART.setComponentPopupMenu(BTD1);
          EBART.setName("EBART");
          p_tete_gauche.add(EBART);
          EBART.setBounds(455, 0, 210, EBART.getPreferredSize().height);

          //---- OBJ_77 ----
          OBJ_77.setText("Niveau");
          OBJ_77.setName("OBJ_77");
          p_tete_gauche.add(OBJ_77);
          OBJ_77.setBounds(700, 5, 48, 19);

          //---- EBNIV ----
          EBNIV.setComponentPopupMenu(BTD);
          EBNIV.setName("EBNIV");
          p_tete_gauche.add(EBNIV);
          EBNIV.setBounds(755, 0, 26, EBNIV.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Options article");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Chemin critique");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Documents li\u00e9s");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu16 ========
            {
              riSousMenu16.setName("riSousMenu16");

              //---- riSousMenu_bt16 ----
              riSousMenu_bt16.setText("Bloc-notes");
              riSousMenu_bt16.setName("riSousMenu_bt16");
              riSousMenu_bt16.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt16ActionPerformed(e);
                }
              });
              riSousMenu16.add(riSousMenu_bt16);
            }
            menus_haut.add(riSousMenu16);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Visualisation chiffrage");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Rechiffrage");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(900, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel2.add(A1LIB);
            A1LIB.setBounds(10, 10, 321, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            panel2.add(A1LB1);
            A1LB1.setBounds(10, 35, 321, A1LB1.getPreferredSize().height);

            //---- A1LB2 ----
            A1LB2.setText("@A1LB2@");
            A1LB2.setName("A1LB2");
            panel2.add(A1LB2);
            A1LB2.setBounds(10, 60, 321, A1LB2.getPreferredSize().height);

            //---- A1LB3 ----
            A1LB3.setText("@A1LB3@");
            A1LB3.setName("A1LB3");
            panel2.add(A1LB3);
            A1LB3.setBounds(10, 85, 321, A1LB3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel2);
          panel2.setBounds(13, 14, 350, 120);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Description"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- EBLIB ----
            EBLIB.setComponentPopupMenu(BTD);
            EBLIB.setName("EBLIB");
            panel3.add(EBLIB);
            EBLIB.setBounds(455, 30, 375, EBLIB.getPreferredSize().height);

            //---- EBCLA1 ----
            EBCLA1.setComponentPopupMenu(BTD);
            EBCLA1.setName("EBCLA1");
            panel3.add(EBCLA1);
            EBCLA1.setBounds(175, 30, 210, EBCLA1.getPreferredSize().height);

            //---- EBCLA2 ----
            EBCLA2.setComponentPopupMenu(BTD);
            EBCLA2.setName("EBCLA2");
            panel3.add(EBCLA2);
            EBCLA2.setBounds(175, 60, 210, EBCLA2.getPreferredSize().height);

            //---- OBJ_113 ----
            OBJ_113.setText("Formule calcul prix vente");
            OBJ_113.setName("OBJ_113");
            panel3.add(OBJ_113);
            OBJ_113.setBounds(635, 95, 150, 20);

            //---- OBJ_88 ----
            OBJ_88.setText("@LMC1@");
            OBJ_88.setName("OBJ_88");
            panel3.add(OBJ_88);
            OBJ_88.setBounds(20, 34, 150, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("@LMC2@");
            OBJ_92.setName("OBJ_92");
            panel3.add(OBJ_92);
            OBJ_92.setBounds(20, 64, 150, 20);

            //---- OBJ_105 ----
            OBJ_105.setText("OF initial");
            OBJ_105.setName("OBJ_105");
            panel3.add(OBJ_105);
            OBJ_105.setBounds(20, 94, 150, 20);

            //---- EBCCT ----
            EBCCT.setComponentPopupMenu(BTD);
            EBCCT.setName("EBCCT");
            panel3.add(EBCCT);
            EBCCT.setBounds(175, 91, 110, EBCCT.getPreferredSize().height);

            //---- OBJ_107 ----
            OBJ_107.setText("Section");
            OBJ_107.setName("OBJ_107");
            panel3.add(OBJ_107);
            OBJ_107.setBounds(400, 95, 48, 20);

            //---- OBJ_90 ----
            OBJ_90.setText("Libell\u00e9");
            OBJ_90.setName("OBJ_90");
            panel3.add(OBJ_90);
            OBJ_90.setBounds(400, 34, 43, 20);

            //---- OBJ_110 ----
            OBJ_110.setText("Affaire");
            OBJ_110.setName("OBJ_110");
            panel3.add(OBJ_110);
            OBJ_110.setBounds(520, 95, 42, 20);

            //---- EBSAN ----
            EBSAN.setComponentPopupMenu(BTD1);
            EBSAN.setName("EBSAN");
            panel3.add(EBSAN);
            EBSAN.setBounds(455, 91, 50, EBSAN.getPreferredSize().height);

            //---- EBACT ----
            EBACT.setComponentPopupMenu(BTD1);
            EBACT.setName("EBACT");
            panel3.add(EBACT);
            EBACT.setBounds(565, 91, 50, EBACT.getPreferredSize().height);

            //---- EBFPV ----
            EBFPV.setComponentPopupMenu(BTD1);
            EBFPV.setName("EBFPV");
            panel3.add(EBFPV);
            EBFPV.setBounds(790, 91, 40, EBFPV.getPreferredSize().height);

            //---- WTI1 ----
            WTI1.setText("@WTI1@");
            WTI1.setName("WTI1");
            panel3.add(WTI1);
            WTI1.setBounds(400, 62, 34, WTI1.getPreferredSize().height);

            //---- EBZP1 ----
            EBZP1.setComponentPopupMenu(BTD1);
            EBZP1.setName("EBZP1");
            panel3.add(EBZP1);
            EBZP1.setBounds(440, 60, 34, EBZP1.getPreferredSize().height);

            //---- WTI2 ----
            WTI2.setText("@WTI2@");
            WTI2.setName("WTI2");
            panel3.add(WTI2);
            WTI2.setBounds(490, 62, 34, WTI2.getPreferredSize().height);

            //---- EBZP2 ----
            EBZP2.setComponentPopupMenu(BTD1);
            EBZP2.setName("EBZP2");
            panel3.add(EBZP2);
            EBZP2.setBounds(529, 60, 34, EBZP2.getPreferredSize().height);

            //---- WTI3 ----
            WTI3.setText("@WTI3@");
            WTI3.setName("WTI3");
            panel3.add(WTI3);
            WTI3.setBounds(580, 62, 34, WTI3.getPreferredSize().height);

            //---- EBZP3 ----
            EBZP3.setComponentPopupMenu(BTD1);
            EBZP3.setName("EBZP3");
            panel3.add(EBZP3);
            EBZP3.setBounds(618, 60, 34, EBZP3.getPreferredSize().height);

            //---- WTI4 ----
            WTI4.setText("@WTI4@");
            WTI4.setName("WTI4");
            panel3.add(WTI4);
            WTI4.setBounds(665, 62, 34, WTI4.getPreferredSize().height);

            //---- EBZP4 ----
            EBZP4.setComponentPopupMenu(BTD1);
            EBZP4.setName("EBZP4");
            panel3.add(EBZP4);
            EBZP4.setBounds(707, 60, 34, EBZP4.getPreferredSize().height);

            //---- WTI5 ----
            WTI5.setText("@WTI5@");
            WTI5.setName("WTI5");
            panel3.add(WTI5);
            WTI5.setBounds(750, 60, 34, WTI5.getPreferredSize().height);

            //---- EBZP5 ----
            EBZP5.setComponentPopupMenu(BTD1);
            EBZP5.setName("EBZP5");
            panel3.add(EBZP5);
            EBZP5.setBounds(796, 60, 34, EBZP5.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(13, 141, 872, 137);

          //======== panel4 ========
          {
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== OBJ_115 ========
            {
              OBJ_115.setName("OBJ_115");

              //======== OBJ_116 ========
              {
                OBJ_116.setOpaque(false);
                OBJ_116.setName("OBJ_116");
                OBJ_116.setLayout(null);

                //---- c ----
                c.setComponentPopupMenu(BTD);
                c.setText("@ETALIB@");
                c.setName("c");
                OBJ_116.add(c);
                c.setBounds(26, 9, 135, c.getPreferredSize().height);

                //---- OBJ_123 ----
                OBJ_123.setText("Quantit\u00e9 \u00e9conomique");
                OBJ_123.setName("OBJ_123");
                OBJ_116.add(OBJ_123);
                OBJ_123.setBounds(182, 42, 130, 20);

                //---- OBJ_127 ----
                OBJ_127.setText("Quantit\u00e9 \u00e0 fabriquer");
                OBJ_127.setName("OBJ_127");
                OBJ_116.add(OBJ_127);
                OBJ_127.setBounds(182, 71, 120, 20);

                //---- OBJ_125 ----
                OBJ_125.setText("Dispo. composant");
                OBJ_125.setName("OBJ_125");
                OBJ_116.add(OBJ_125);
                OBJ_125.setBounds(449, 42, 115, 20);

                //---- OBJ_129 ----
                OBJ_129.setText("Quantit\u00e9 fabricable");
                OBJ_129.setName("OBJ_129");
                OBJ_116.add(OBJ_129);
                OBJ_129.setBounds(449, 71, 115, 20);

                //---- OBJ_121 ----
                OBJ_121.setText("Quantit\u00e9 fabriqu\u00e9e");
                OBJ_121.setName("OBJ_121");
                OBJ_116.add(OBJ_121);
                OBJ_121.setBounds(449, 13, 112, 20);

                //---- OBJ_119 ----
                OBJ_119.setText("Quantit\u00e9 th\u00e9orique");
                OBJ_119.setName("OBJ_119");
                OBJ_116.add(OBJ_119);
                OBJ_119.setBounds(182, 13, 111, 20);

                //---- EBQMI ----
                EBQMI.setComponentPopupMenu(BTD);
                EBQMI.setName("EBQMI");
                OBJ_116.add(EBQMI);
                EBQMI.setBounds(330, 9, 106, EBQMI.getPreferredSize().height);

                //---- EBQTR ----
                EBQTR.setComponentPopupMenu(BTD);
                EBQTR.setName("EBQTR");
                OBJ_116.add(EBQTR);
                EBQTR.setBounds(580, 9, 106, EBQTR.getPreferredSize().height);

                //---- ENQTE ----
                ENQTE.setComponentPopupMenu(BTD);
                ENQTE.setName("ENQTE");
                OBJ_116.add(ENQTE);
                ENQTE.setBounds(330, 38, 106, ENQTE.getPreferredSize().height);

                //---- EBQTE ----
                EBQTE.setComponentPopupMenu(BTD);
                EBQTE.setName("EBQTE");
                OBJ_116.add(EBQTE);
                EBQTE.setBounds(330, 67, 106, EBQTE.getPreferredSize().height);

                //---- K44T5 ----
                K44T5.setComponentPopupMenu(BTD);
                K44T5.setName("K44T5");
                OBJ_116.add(K44T5);
                K44T5.setBounds(580, 67, 106, K44T5.getPreferredSize().height);

                //---- K44T6X ----
                K44T6X.setToolTipText("Date de disponibilit\u00e9 des composants");
                K44T6X.setComponentPopupMenu(BTD);
                K44T6X.setName("K44T6X");
                OBJ_116.add(K44T6X);
                K44T6X.setBounds(580, 38, 74, K44T6X.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < OBJ_116.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_116.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_116.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_116.setMinimumSize(preferredSize);
                  OBJ_116.setPreferredSize(preferredSize);
                }
              }
              OBJ_115.addTab("Fabrication : Etat", OBJ_116);

              //======== OBJ_131 ========
              {
                OBJ_131.setOpaque(false);
                OBJ_131.setName("OBJ_131");
                OBJ_131.setLayout(null);

                //---- EBIN1 ----
                EBIN1.setModel(new DefaultComboBoxModel(new String[] {
                  "Pas de g\u00e9n\u00e9ration",
                  "G\u00e9n\u00e9ration quantit\u00e9 globale",
                  "Quantit\u00e9 globale-stock",
                  "Quantit\u00e9 globale-stock dispo"
                }));
                EBIN1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                EBIN1.setName("EBIN1");
                OBJ_131.add(EBIN1);
                EBIN1.setBounds(365, 11, 220, EBIN1.getPreferredSize().height);

                //---- CLNOM ----
                CLNOM.setText("@CLNOM@");
                CLNOM.setName("CLNOM");
                OBJ_131.add(CLNOM);
                CLNOM.setBounds(235, 76, 225, CLNOM.getPreferredSize().height);

                //---- MALIBR ----
                MALIBR.setText("@MALIBR@");
                MALIBR.setName("MALIBR");
                OBJ_131.add(MALIBR);
                MALIBR.setBounds(171, 46, 139, 20);

                //---- UNLIBR ----
                UNLIBR.setText("@UNLIBR@");
                UNLIBR.setName("UNLIBR");
                OBJ_131.add(UNLIBR);
                UNLIBR.setBounds(506, 46, 139, 20);

                //---- OBJ_141 ----
                OBJ_141.setText("Magasin d'entr\u00e9e");
                OBJ_141.setName("OBJ_141");
                OBJ_131.add(OBJ_141);
                OBJ_141.setBounds(6, 46, 108, 20);

                //---- OBJ_140 ----
                OBJ_140.setText("G\u00e9n\u00e9ration auto");
                OBJ_140.setName("OBJ_140");
                OBJ_131.add(OBJ_140);
                OBJ_140.setBounds(265, 14, 97, 20);

                //---- OBJ_134 ----
                OBJ_134.setText("Num\u00e9ro de bon");
                OBJ_134.setName("OBJ_134");
                OBJ_131.add(OBJ_134);
                OBJ_134.setBounds(630, 14, 96, 20);

                //---- OBJ_147 ----
                OBJ_147.setText("Client");
                OBJ_147.setName("OBJ_147");
                OBJ_131.add(OBJ_147);
                OBJ_147.setBounds(6, 78, 96, 20);

                //---- OBJ_144 ----
                OBJ_144.setText("Unit\u00e9 de stock");
                OBJ_144.setName("OBJ_144");
                OBJ_131.add(OBJ_144);
                OBJ_144.setBounds(365, 46, 88, 20);

                //---- E1NUM ----
                E1NUM.setName("E1NUM");
                OBJ_131.add(E1NUM);
                E1NUM.setBounds(735, 10, 60, E1NUM.getPreferredSize().height);

                //---- EBNUM0 ----
                EBNUM0.setComponentPopupMenu(BTD);
                EBNUM0.setName("EBNUM0");
                OBJ_131.add(EBNUM0);
                EBNUM0.setBounds(135, 10, 60, EBNUM0.getPreferredSize().height);

                //---- WCLI ----
                WCLI.setName("WCLI");
                OBJ_131.add(WCLI);
                WCLI.setBounds(135, 74, 60, WCLI.getPreferredSize().height);

                //---- OBJ_137 ----
                OBJ_137.setText("origine");
                OBJ_137.setName("OBJ_137");
                OBJ_131.add(OBJ_137);
                OBJ_137.setBounds(6, 14, 42, 20);

                //---- WLIV ----
                WLIV.setName("WLIV");
                OBJ_131.add(WLIV);
                WLIV.setBounds(193, 74, 33, WLIV.getPreferredSize().height);

                //---- EBMAG ----
                EBMAG.setComponentPopupMenu(BTD1);
                EBMAG.setName("EBMAG");
                OBJ_131.add(EBMAG);
                EBMAG.setBounds(135, 42, 30, EBMAG.getPreferredSize().height);

                //---- A1UNS ----
                A1UNS.setComponentPopupMenu(BTD);
                A1UNS.setName("A1UNS");
                OBJ_131.add(A1UNS);
                A1UNS.setBounds(469, 42, 30, A1UNS.getPreferredSize().height);

                //---- E1SUF ----
                E1SUF.setName("E1SUF");
                OBJ_131.add(E1SUF);
                E1SUF.setBounds(800, 10, 20, E1SUF.getPreferredSize().height);

                //---- EBSUF0 ----
                EBSUF0.setComponentPopupMenu(BTD);
                EBSUF0.setName("EBSUF0");
                OBJ_131.add(EBSUF0);
                EBSUF0.setBounds(205, 10, 20, EBSUF0.getPreferredSize().height);

                {
                  // compute preferred size
                  Dimension preferredSize = new Dimension();
                  for(int i = 0; i < OBJ_131.getComponentCount(); i++) {
                    Rectangle bounds = OBJ_131.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                  }
                  Insets insets = OBJ_131.getInsets();
                  preferredSize.width += insets.right;
                  preferredSize.height += insets.bottom;
                  OBJ_131.setMinimumSize(preferredSize);
                  OBJ_131.setPreferredSize(preferredSize);
                }
              }
              OBJ_115.addTab("Fabrication : Origine", OBJ_131);
            }
            panel4.add(OBJ_115);
            OBJ_115.setBounds(10, 5, 850, 135);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel4);
          panel4.setBounds(13, 285, 872, 146);

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder("Dates fabrication"));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- EBDDPX ----
            EBDDPX.setComponentPopupMenu(BTD);
            EBDDPX.setName("EBDDPX");
            panel5.add(EBDDPX);
            EBDDPX.setBounds(100, 30, 105, EBDDPX.getPreferredSize().height);

            //---- EBDFPX ----
            EBDFPX.setComponentPopupMenu(BTD);
            EBDFPX.setName("EBDFPX");
            panel5.add(EBDFPX);
            EBDFPX.setBounds(100, 60, 105, EBDFPX.getPreferredSize().height);

            //---- OBJ_177 ----
            OBJ_177.setText("Fin pr\u00e9vue");
            OBJ_177.setName("OBJ_177");
            panel5.add(OBJ_177);
            OBJ_177.setBounds(20, 94, 75, 20);

            //---- EBDFSX ----
            EBDFSX.setComponentPopupMenu(BTD);
            EBDFSX.setName("EBDFSX");
            panel5.add(EBDFSX);
            EBDFSX.setBounds(100, 90, 105, EBDFSX.getPreferredSize().height);

            //---- OBJ_165 ----
            OBJ_165.setText("D\u00e9but");
            OBJ_165.setName("OBJ_165");
            panel5.add(OBJ_165);
            OBJ_165.setBounds(20, 34, 75, 20);

            //---- OBJ_173 ----
            OBJ_173.setText("Fin");
            OBJ_173.setName("OBJ_173");
            panel5.add(OBJ_173);
            OBJ_173.setBounds(20, 64, 75, 20);

            //---- EBHFSH ----
            EBHFSH.setComponentPopupMenu(BTD);
            EBHFSH.setName("EBHFSH");
            panel5.add(EBHFSH);
            EBHFSH.setBounds(230, 90, 26, EBHFSH.getPreferredSize().height);

            //---- EBHFSM ----
            EBHFSM.setComponentPopupMenu(BTD);
            EBHFSM.setName("EBHFSM");
            panel5.add(EBHFSM);
            EBHFSM.setBounds(270, 90, 26, EBHFSM.getPreferredSize().height);

            //---- OBJ_180 ----
            OBJ_180.setText("h");
            OBJ_180.setName("OBJ_180");
            panel5.add(OBJ_180);
            OBJ_180.setBounds(260, 94, 12, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel5);
          panel5.setBounds(13, 438, 322, 138);

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder("                                                                                                                                                                                                                                                                                                                                                                                                                                         "));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- WTPPG ----
            WTPPG.setName("WTPPG");
            panel6.add(WTPPG);
            WTPPG.setBounds(90, 45, 100, WTPPG.getPreferredSize().height);

            //---- WTPFG ----
            WTPFG.setName("WTPFG");
            panel6.add(WTPFG);
            WTPFG.setBounds(195, 45, 100, WTPFG.getPreferredSize().height);

            //---- WTPG ----
            WTPG.setName("WTPG");
            panel6.add(WTPG);
            WTPG.setBounds(300, 45, 100, WTPG.getPreferredSize().height);

            //---- WTUG ----
            WTUG.setName("WTUG");
            panel6.add(WTUG);
            WTUG.setBounds(405, 45, 100, WTUG.getPreferredSize().height);

            //---- STPPG ----
            STPPG.setName("STPPG");
            panel6.add(STPPG);
            STPPG.setBounds(90, 70, 100, STPPG.getPreferredSize().height);

            //---- STPFG ----
            STPFG.setName("STPFG");
            panel6.add(STPFG);
            STPFG.setBounds(195, 70, 100, STPFG.getPreferredSize().height);

            //---- STPG ----
            STPG.setName("STPG");
            panel6.add(STPG);
            STPG.setBounds(300, 70, 100, STPG.getPreferredSize().height);

            //---- STUG ----
            STUG.setName("STUG");
            panel6.add(STUG);
            STUG.setBounds(405, 70, 100, STUG.getPreferredSize().height);

            //---- ETPPG ----
            ETPPG.setName("ETPPG");
            panel6.add(ETPPG);
            ETPPG.setBounds(90, 95, 100, ETPPG.getPreferredSize().height);

            //---- ETPFG ----
            ETPFG.setName("ETPFG");
            panel6.add(ETPFG);
            ETPFG.setBounds(195, 95, 100, ETPFG.getPreferredSize().height);

            //---- ETPG ----
            ETPG.setName("ETPG");
            panel6.add(ETPG);
            ETPG.setBounds(300, 95, 100, ETPG.getPreferredSize().height);

            //---- ETUG ----
            ETUG.setName("ETUG");
            panel6.add(ETUG);
            ETUG.setBounds(405, 95, 100, ETUG.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Proportionnel");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel6.add(label1);
            label1.setBounds(90, 25, 100, 25);

            //---- label2 ----
            label2.setText("Fixe");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            panel6.add(label2);
            label2.setBounds(195, 25, 100, 25);

            //---- label3 ----
            label3.setText("Fabrication");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setName("label3");
            panel6.add(label3);
            label3.setBounds(300, 25, 100, 25);

            //---- label4 ----
            label4.setText("Unitaire");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setHorizontalAlignment(SwingConstants.CENTER);
            label4.setName("label4");
            panel6.add(label4);
            label4.setBounds(405, 25, 100, 25);

            //---- label6 ----
            label6.setText("Standard");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setHorizontalAlignment(SwingConstants.LEFT);
            label6.setName("label6");
            panel6.add(label6);
            label6.setBounds(15, 72, 70, 25);

            //---- label5 ----
            label5.setText("Actuel");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setHorizontalAlignment(SwingConstants.LEFT);
            label5.setName("label5");
            panel6.add(label5);
            label5.setBounds(15, 47, 70, 25);

            //---- label7 ----
            label7.setText("Ecart");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setHorizontalAlignment(SwingConstants.LEFT);
            label7.setName("label7");
            panel6.add(label7);
            label7.setBounds(15, 97, 70, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel6);
          panel6.setBounds(340, 438, 545, 138);

          //======== panel7 ========
          {
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(null);

            //---- OBJ_183 ----
            OBJ_183.setText("Validit\u00e9");
            OBJ_183.setName("OBJ_183");
            panel7.add(OBJ_183);
            OBJ_183.setBounds(10, 30, 51, 18);

            //---- OBJ_184 ----
            OBJ_184.setText("du");
            OBJ_184.setName("OBJ_184");
            panel7.add(OBJ_184);
            OBJ_184.setBounds(70, 30, 20, 18);

            //---- ENDDVX ----
            ENDDVX.setComponentPopupMenu(BTD);
            ENDDVX.setName("ENDDVX");
            panel7.add(ENDDVX);
            ENDDVX.setBounds(90, 25, 105, ENDDVX.getPreferredSize().height);

            //---- OBJ_185 ----
            OBJ_185.setText("au");
            OBJ_185.setName("OBJ_185");
            panel7.add(OBJ_185);
            OBJ_185.setBounds(200, 30, 16, 18);

            //---- ENDFVX ----
            ENDFVX.setComponentPopupMenu(BTD);
            ENDFVX.setName("ENDFVX");
            panel7.add(ENDFVX);
            ENDFVX.setBounds(220, 25, 105, ENDFVX.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel7.getComponentCount(); i++) {
                Rectangle bounds = panel7.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel7.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel7.setMinimumSize(preferredSize);
              panel7.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel7);
          panel7.setBounds(375, 65, 329, 57);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- snPhotoArticle ----
            snPhotoArticle.setOpaque(false);
            snPhotoArticle.setBorder(null);
            snPhotoArticle.setName("snPhotoArticle");
            panel1.add(snPhotoArticle);
            snPhotoArticle.setBounds(30, 5, 135, 110);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(711, 14, 174, 120);

          //======== panel8 ========
          {
            panel8.setOpaque(false);
            panel8.setName("panel8");
            panel8.setLayout(null);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel8.getComponentCount(); i++) {
                Rectangle bounds = panel8.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel8.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel8.setMinimumSize(preferredSize);
              panel8.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel8);
          panel8.setBounds(375, 30, 329, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }

    //======== BTD1 ========
    {
      BTD1.setName("BTD1");

      //---- OBJ_25 ----
      OBJ_25.setText("Choix possibles");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD1.add(OBJ_25);

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD1.add(OBJ_24);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_58;
  private XRiTextField EBNUM;
  private JLabel OBJ_59;
  private XRiTextField EBETB;
  private XRiTextField EBSUF;
  private JLabel label8;
  private XRiTextField EBART;
  private JLabel OBJ_77;
  private XRiTextField EBNIV;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu16;
  private RiSousMenu_bt riSousMenu_bt16;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private RiZoneSortie A1LB2;
  private RiZoneSortie A1LB3;
  private JPanel panel3;
  private XRiTextField EBLIB;
  private XRiTextField EBCLA1;
  private XRiTextField EBCLA2;
  private JLabel OBJ_113;
  private JLabel OBJ_88;
  private JLabel OBJ_92;
  private JLabel OBJ_105;
  private XRiTextField EBCCT;
  private JLabel OBJ_107;
  private JLabel OBJ_90;
  private JLabel OBJ_110;
  private XRiTextField EBSAN;
  private XRiTextField EBACT;
  private XRiTextField EBFPV;
  private RiZoneSortie WTI1;
  private XRiTextField EBZP1;
  private RiZoneSortie WTI2;
  private XRiTextField EBZP2;
  private RiZoneSortie WTI3;
  private XRiTextField EBZP3;
  private RiZoneSortie WTI4;
  private XRiTextField EBZP4;
  private RiZoneSortie WTI5;
  private XRiTextField EBZP5;
  private JPanel panel4;
  private JTabbedPane OBJ_115;
  private JPanel OBJ_116;
  private RiZoneSortie c;
  private JLabel OBJ_123;
  private JLabel OBJ_127;
  private JLabel OBJ_125;
  private JLabel OBJ_129;
  private JLabel OBJ_121;
  private JLabel OBJ_119;
  private XRiTextField EBQMI;
  private XRiTextField EBQTR;
  private XRiTextField ENQTE;
  private XRiTextField EBQTE;
  private XRiTextField K44T5;
  private XRiTextField K44T6X;
  private JPanel OBJ_131;
  private XRiComboBox EBIN1;
  private RiZoneSortie CLNOM;
  private RiZoneSortie MALIBR;
  private RiZoneSortie UNLIBR;
  private JLabel OBJ_141;
  private JLabel OBJ_140;
  private JLabel OBJ_134;
  private JLabel OBJ_147;
  private JLabel OBJ_144;
  private XRiTextField E1NUM;
  private XRiTextField EBNUM0;
  private XRiTextField WCLI;
  private JLabel OBJ_137;
  private XRiTextField WLIV;
  private XRiTextField EBMAG;
  private XRiTextField A1UNS;
  private XRiTextField E1SUF;
  private XRiTextField EBSUF0;
  private JPanel panel5;
  private XRiCalendrier EBDDPX;
  private XRiCalendrier EBDFPX;
  private JLabel OBJ_177;
  private XRiCalendrier EBDFSX;
  private JLabel OBJ_165;
  private JLabel OBJ_173;
  private XRiTextField EBHFSH;
  private XRiTextField EBHFSM;
  private JLabel OBJ_180;
  private JPanel panel6;
  private XRiTextField WTPPG;
  private XRiTextField WTPFG;
  private XRiTextField WTPG;
  private XRiTextField WTUG;
  private XRiTextField STPPG;
  private XRiTextField STPFG;
  private XRiTextField STPG;
  private XRiTextField STUG;
  private XRiTextField ETPPG;
  private XRiTextField ETPFG;
  private XRiTextField ETPG;
  private XRiTextField ETUG;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label6;
  private JLabel label5;
  private JLabel label7;
  private JPanel panel7;
  private JLabel OBJ_183;
  private JLabel OBJ_184;
  private XRiCalendrier ENDDVX;
  private JLabel OBJ_185;
  private XRiCalendrier ENDFVX;
  private JPanel panel1;
  private SNPhotoArticle snPhotoArticle;
  private JPanel panel8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_5;
  private JPopupMenu BTD1;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_24;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
