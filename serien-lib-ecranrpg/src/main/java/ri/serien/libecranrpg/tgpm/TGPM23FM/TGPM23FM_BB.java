
package ri.serien.libecranrpg.tgpm.TGPM23FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class TGPM23FM_BB extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPM23FM_BB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ORDLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORDLIB@")).trim());
    WART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    LBORD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBORD@")).trim());
    MALIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIB@")).trim());
    WQP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQP1@")).trim());
    LBTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBTYP@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBQB3@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT5@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LBBA3@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1QT4@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    // setTitle(???);
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ORDLIB = new RiZoneSortie();
    LBLIB = new XRiTextField();
    WART = new RiZoneSortie();
    LBORD = new RiZoneSortie();
    MALIB = new RiZoneSortie();
    OBJ_87 = new JLabel();
    WFOR = new XRiCheckBox();
    OBJ_86 = new JLabel();
    LBNBR = new XRiTextField();
    UQTE = new XRiTextField();
    LBQB1 = new XRiTextField();
    LBQB2 = new XRiTextField();
    LBQB3 = new XRiTextField();
    LBQC1 = new XRiTextField();
    WQP1 = new RiZoneSortie();
    OBJ_68 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_52 = new JLabel();
    OBJ_54 = new JLabel();
    OBJ_55 = new JLabel();
    WSER = new XRiCheckBox();
    OBJ_290 = new JLabel();
    LBNL1 = new XRiTextField();
    LBNL2 = new XRiTextField();
    LBNL3 = new XRiTextField();
    LBTYP = new RiZoneSortie();
    OBJ_56 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_71 = new JLabel();
    OBJ_73 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_32 = new JLabel();
    LBMAG = new XRiTextField();
    WTI1 = new RiZoneSortie();
    WTI2 = new RiZoneSortie();
    WTI3 = new RiZoneSortie();
    WTI4 = new RiZoneSortie();
    WTI5 = new RiZoneSortie();
    LBZP1 = new XRiTextField();
    LBZP2 = new XRiTextField();
    LBZP3 = new XRiTextField();
    LBZP4 = new XRiTextField();
    LBZP5 = new XRiTextField();
    WUN = new XRiTextField();
    LBUN1 = new XRiTextField();
    LBUN2 = new XRiTextField();
    LBUN3 = new XRiTextField();
    LBIN6 = new XRiTextField();
    LBIN4 = new XRiTextField();
    LBIN1 = new XRiTextField();
    LBIN2 = new XRiTextField();
    OBJ_70 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_21 = new RiZoneSortie();
    OBJ_20 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_28 = new RiZoneSortie();
    OBJ_23 = new RiZoneSortie();
    OBJ_27 = new JLabel();
    OBJ_26 = new RiZoneSortie();
    OBJ_22 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1105, 515));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Informations techniques");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Options article");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Documents li\u00e9s");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ORDLIB ----
          ORDLIB.setText("@ORDLIB@");
          ORDLIB.setName("ORDLIB");
          panel1.add(ORDLIB);
          ORDLIB.setBounds(590, 45, 310, ORDLIB.getPreferredSize().height);

          //---- LBLIB ----
          LBLIB.setComponentPopupMenu(BTD);
          LBLIB.setName("LBLIB");
          panel1.add(LBLIB);
          LBLIB.setBounds(590, 75, 310, LBLIB.getPreferredSize().height);

          //---- WART ----
          WART.setComponentPopupMenu(BTD);
          WART.setText("@WART@");
          WART.setName("WART");
          panel1.add(WART);
          WART.setBounds(590, 20, 310, WART.getPreferredSize().height);

          //---- LBORD ----
          LBORD.setComponentPopupMenu(BTD);
          LBORD.setText("@LBORD@");
          LBORD.setName("LBORD");
          panel1.add(LBORD);
          LBORD.setBounds(270, 45, 210, LBORD.getPreferredSize().height);

          //---- MALIB ----
          MALIB.setComponentPopupMenu(BTD);
          MALIB.setText("@MALIB@");
          MALIB.setName("MALIB");
          panel1.add(MALIB);
          MALIB.setBounds(625, 135, 210, MALIB.getPreferredSize().height);

          //---- OBJ_87 ----
          OBJ_87.setText("Quantit\u00e9 produite");
          OBJ_87.setName("OBJ_87");
          panel1.add(OBJ_87);
          OBJ_87.setBounds(25, 454, 130, 20);

          //---- WFOR ----
          WFOR.setText("For\u00e7age \u00e0 z\u00e9ro");
          WFOR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WFOR.setName("WFOR");
          panel1.add(WFOR);
          WFOR.setBounds(325, 454, 116, 20);

          //---- OBJ_86 ----
          OBJ_86.setText("Quantit\u00e9 pr\u00e9vue");
          OBJ_86.setName("OBJ_86");
          panel1.add(OBJ_86);
          OBJ_86.setBounds(25, 417, 97, 20);

          //---- LBNBR ----
          LBNBR.setComponentPopupMenu(BTD);
          LBNBR.setName("LBNBR");
          panel1.add(LBNBR);
          LBNBR.setBounds(25, 215, 106, LBNBR.getPreferredSize().height);

          //---- UQTE ----
          UQTE.setComponentPopupMenu(BTD);
          UQTE.setName("UQTE");
          panel1.add(UQTE);
          UQTE.setBounds(160, 215, 106, UQTE.getPreferredSize().height);

          //---- LBQB1 ----
          LBQB1.setComponentPopupMenu(BTD);
          LBQB1.setName("LBQB1");
          panel1.add(LBQB1);
          LBQB1.setBounds(25, 270, 106, LBQB1.getPreferredSize().height);

          //---- LBQB2 ----
          LBQB2.setComponentPopupMenu(BTD);
          LBQB2.setName("LBQB2");
          panel1.add(LBQB2);
          LBQB2.setBounds(220, 270, 106, LBQB2.getPreferredSize().height);

          //---- LBQB3 ----
          LBQB3.setComponentPopupMenu(BTD);
          LBQB3.setName("LBQB3");
          panel1.add(LBQB3);
          LBQB3.setBounds(405, 270, 106, LBQB3.getPreferredSize().height);

          //---- LBQC1 ----
          LBQC1.setComponentPopupMenu(BTD);
          LBQC1.setName("LBQC1");
          panel1.add(LBQC1);
          LBQC1.setBounds(160, 450, 106, LBQC1.getPreferredSize().height);

          //---- WQP1 ----
          WQP1.setText("@WQP1@");
          WQP1.setName("WQP1");
          panel1.add(WQP1);
          WQP1.setBounds(160, 415, 85, WQP1.getPreferredSize().height);

          //---- OBJ_68 ----
          OBJ_68.setText("Dimensions");
          OBJ_68.setName("OBJ_68");
          panel1.add(OBJ_68);
          OBJ_68.setBounds(25, 250, 75, 18);

          //---- OBJ_74 ----
          OBJ_74.setText("Rendement");
          OBJ_74.setName("OBJ_74");
          panel1.add(OBJ_74);
          OBJ_74.setBounds(325, 418, 73, 18);

          //---- OBJ_44 ----
          OBJ_44.setText("Magasin");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(590, 114, 55, 20);

          //---- OBJ_52 ----
          OBJ_52.setText("Variante");
          OBJ_52.setName("OBJ_52");
          panel1.add(OBJ_52);
          OBJ_52.setBounds(270, 115, 54, 19);

          //---- OBJ_54 ----
          OBJ_54.setText("Nombre");
          OBJ_54.setName("OBJ_54");
          panel1.add(OBJ_54);
          OBJ_54.setBounds(25, 195, 52, 18);

          //---- OBJ_55 ----
          OBJ_55.setText("Besoin");
          OBJ_55.setName("OBJ_55");
          panel1.add(OBJ_55);
          OBJ_55.setBounds(160, 195, 52, 18);

          //---- WSER ----
          WSER.setText("Lot");
          WSER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WSER.setName("WSER");
          panel1.add(WSER);
          WSER.setBounds(500, 50, 55, 19);

          //---- OBJ_290 ----
          OBJ_290.setText("Article");
          OBJ_290.setName("OBJ_290");
          panel1.add(OBJ_290);
          OBJ_290.setBounds(270, 25, 37, 18);

          //---- LBNL1 ----
          LBNL1.setComponentPopupMenu(BTD);
          LBNL1.setName("LBNL1");
          panel1.add(LBNL1);
          LBNL1.setBounds(25, 45, 34, LBNL1.getPreferredSize().height);

          //---- LBNL2 ----
          LBNL2.setComponentPopupMenu(BTD);
          LBNL2.setName("LBNL2");
          panel1.add(LBNL2);
          LBNL2.setBounds(70, 45, 34, LBNL2.getPreferredSize().height);

          //---- LBNL3 ----
          LBNL3.setComponentPopupMenu(BTD);
          LBNL3.setName("LBNL3");
          panel1.add(LBNL3);
          LBNL3.setBounds(115, 45, 34, LBNL3.getPreferredSize().height);

          //---- LBTYP ----
          LBTYP.setComponentPopupMenu(BTD);
          LBTYP.setText("@LBTYP@");
          LBTYP.setName("LBTYP");
          panel1.add(LBTYP);
          LBTYP.setBounds(160, 45, 45, LBTYP.getPreferredSize().height);

          //---- OBJ_56 ----
          OBJ_56.setText("Unit\u00e9");
          OBJ_56.setName("OBJ_56");
          panel1.add(OBJ_56);
          OBJ_56.setBounds(265, 195, 34, 18);

          //---- OBJ_69 ----
          OBJ_69.setText("Unit\u00e9");
          OBJ_69.setName("OBJ_69");
          panel1.add(OBJ_69);
          OBJ_69.setBounds(160, 250, 34, 18);

          //---- OBJ_71 ----
          OBJ_71.setText("Unit\u00e9");
          OBJ_71.setName("OBJ_71");
          panel1.add(OBJ_71);
          OBJ_71.setBounds(325, 250, 34, 18);

          //---- OBJ_73 ----
          OBJ_73.setText("Unit\u00e9");
          OBJ_73.setName("OBJ_73");
          panel1.add(OBJ_73);
          OBJ_73.setBounds(590, 250, 34, 18);

          //---- OBJ_29 ----
          OBJ_29.setText("Type");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(160, 25, 33, 18);

          //---- OBJ_30 ----
          OBJ_30.setText("Nl1");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(25, 25, 31, 18);

          //---- OBJ_31 ----
          OBJ_31.setText("Nl2");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(70, 25, 31, 18);

          //---- OBJ_32 ----
          OBJ_32.setText("Nl3");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(115, 25, 31, 18);

          //---- LBMAG ----
          LBMAG.setComponentPopupMenu(BTD);
          LBMAG.setName("LBMAG");
          panel1.add(LBMAG);
          LBMAG.setBounds(590, 135, 30, LBMAG.getPreferredSize().height);

          //---- WTI1 ----
          WTI1.setText("@WTI1@");
          WTI1.setName("WTI1");
          panel1.add(WTI1);
          WTI1.setBounds(25, 110, 30, WTI1.getPreferredSize().height);

          //---- WTI2 ----
          WTI2.setText("@WTI2@");
          WTI2.setName("WTI2");
          panel1.add(WTI2);
          WTI2.setBounds(70, 110, 30, WTI2.getPreferredSize().height);

          //---- WTI3 ----
          WTI3.setText("@WTI3@");
          WTI3.setName("WTI3");
          panel1.add(WTI3);
          WTI3.setBounds(115, 110, 30, WTI3.getPreferredSize().height);

          //---- WTI4 ----
          WTI4.setText("@WTI4@");
          WTI4.setName("WTI4");
          panel1.add(WTI4);
          WTI4.setBounds(160, 110, 30, WTI4.getPreferredSize().height);

          //---- WTI5 ----
          WTI5.setText("@WTI5@");
          WTI5.setName("WTI5");
          panel1.add(WTI5);
          WTI5.setBounds(200, 110, 30, WTI5.getPreferredSize().height);

          //---- LBZP1 ----
          LBZP1.setComponentPopupMenu(BTD);
          LBZP1.setName("LBZP1");
          panel1.add(LBZP1);
          LBZP1.setBounds(25, 135, 30, LBZP1.getPreferredSize().height);

          //---- LBZP2 ----
          LBZP2.setComponentPopupMenu(BTD);
          LBZP2.setName("LBZP2");
          panel1.add(LBZP2);
          LBZP2.setBounds(70, 135, 30, LBZP2.getPreferredSize().height);

          //---- LBZP3 ----
          LBZP3.setComponentPopupMenu(BTD);
          LBZP3.setName("LBZP3");
          panel1.add(LBZP3);
          LBZP3.setBounds(115, 135, 30, LBZP3.getPreferredSize().height);

          //---- LBZP4 ----
          LBZP4.setComponentPopupMenu(BTD);
          LBZP4.setName("LBZP4");
          panel1.add(LBZP4);
          LBZP4.setBounds(160, 135, 30, LBZP4.getPreferredSize().height);

          //---- LBZP5 ----
          LBZP5.setComponentPopupMenu(BTD);
          LBZP5.setName("LBZP5");
          panel1.add(LBZP5);
          LBZP5.setBounds(200, 135, 30, LBZP5.getPreferredSize().height);

          //---- WUN ----
          WUN.setComponentPopupMenu(BTD);
          WUN.setName("WUN");
          panel1.add(WUN);
          WUN.setBounds(265, 215, 30, WUN.getPreferredSize().height);

          //---- LBUN1 ----
          LBUN1.setComponentPopupMenu(BTD);
          LBUN1.setName("LBUN1");
          panel1.add(LBUN1);
          LBUN1.setBounds(160, 270, 30, LBUN1.getPreferredSize().height);

          //---- LBUN2 ----
          LBUN2.setComponentPopupMenu(BTD);
          LBUN2.setName("LBUN2");
          panel1.add(LBUN2);
          LBUN2.setBounds(325, 270, 30, LBUN2.getPreferredSize().height);

          //---- LBUN3 ----
          LBUN3.setComponentPopupMenu(BTD);
          LBUN3.setName("LBUN3");
          panel1.add(LBUN3);
          LBUN3.setBounds(590, 270, 30, LBUN3.getPreferredSize().height);

          //---- LBIN6 ----
          LBIN6.setComponentPopupMenu(BTD);
          LBIN6.setName("LBIN6");
          panel1.add(LBIN6);
          LBIN6.setBounds(405, 413, 20, LBIN6.getPreferredSize().height);

          //---- LBIN4 ----
          LBIN4.setComponentPopupMenu(BTD);
          LBIN4.setName("LBIN4");
          panel1.add(LBIN4);
          LBIN4.setBounds(270, 135, 30, LBIN4.getPreferredSize().height);

          //---- LBIN1 ----
          LBIN1.setComponentPopupMenu(BTD);
          LBIN1.setName("LBIN1");
          panel1.add(LBIN1);
          LBIN1.setBounds(195, 270, 20, LBIN1.getPreferredSize().height);

          //---- LBIN2 ----
          LBIN2.setComponentPopupMenu(BTD);
          LBIN2.setName("LBIN2");
          panel1.add(LBIN2);
          LBIN2.setBounds(360, 270, 20, LBIN2.getPreferredSize().height);

          //---- OBJ_70 ----
          OBJ_70.setText("X");
          OBJ_70.setName("OBJ_70");
          panel1.add(OBJ_70);
          OBJ_70.setBounds(200, 250, 12, 18);

          //---- OBJ_72 ----
          OBJ_72.setText("X");
          OBJ_72.setName("OBJ_72");
          panel1.add(OBJ_72);
          OBJ_72.setBounds(370, 250, 12, 18);

          //---- OBJ_21 ----
          OBJ_21.setText("@LBQB3@");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(160, 310, 89, OBJ_21.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("Coefficient");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(30, 312, 65, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Epaisseur");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(30, 347, 64, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("@A1QT5@");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(new Rectangle(new Point(405, 345), OBJ_28.getPreferredSize()));

          //---- OBJ_23 ----
          OBJ_23.setText("@LBBA3@");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(405, 310, 59, OBJ_23.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("Densit\u00e9");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(325, 347, 49, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("@A1QT4@");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(160, 345, 48, OBJ_26.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("Base");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(325, 312, 35, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 915, 495);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie ORDLIB;
  private XRiTextField LBLIB;
  private RiZoneSortie WART;
  private RiZoneSortie LBORD;
  private RiZoneSortie MALIB;
  private JLabel OBJ_87;
  private XRiCheckBox WFOR;
  private JLabel OBJ_86;
  private XRiTextField LBNBR;
  private XRiTextField UQTE;
  private XRiTextField LBQB1;
  private XRiTextField LBQB2;
  private XRiTextField LBQB3;
  private XRiTextField LBQC1;
  private RiZoneSortie WQP1;
  private JLabel OBJ_68;
  private JLabel OBJ_74;
  private JLabel OBJ_44;
  private JLabel OBJ_52;
  private JLabel OBJ_54;
  private JLabel OBJ_55;
  private XRiCheckBox WSER;
  private JLabel OBJ_290;
  private XRiTextField LBNL1;
  private XRiTextField LBNL2;
  private XRiTextField LBNL3;
  private RiZoneSortie LBTYP;
  private JLabel OBJ_56;
  private JLabel OBJ_69;
  private JLabel OBJ_71;
  private JLabel OBJ_73;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private JLabel OBJ_32;
  private XRiTextField LBMAG;
  private RiZoneSortie WTI1;
  private RiZoneSortie WTI2;
  private RiZoneSortie WTI3;
  private RiZoneSortie WTI4;
  private RiZoneSortie WTI5;
  private XRiTextField LBZP1;
  private XRiTextField LBZP2;
  private XRiTextField LBZP3;
  private XRiTextField LBZP4;
  private XRiTextField LBZP5;
  private XRiTextField WUN;
  private XRiTextField LBUN1;
  private XRiTextField LBUN2;
  private XRiTextField LBUN3;
  private XRiTextField LBIN6;
  private XRiTextField LBIN4;
  private XRiTextField LBIN1;
  private XRiTextField LBIN2;
  private JLabel OBJ_70;
  private JLabel OBJ_72;
  private RiZoneSortie OBJ_21;
  private JLabel OBJ_20;
  private JLabel OBJ_25;
  private RiZoneSortie OBJ_28;
  private RiZoneSortie OBJ_23;
  private JLabel OBJ_27;
  private RiZoneSortie OBJ_26;
  private JLabel OBJ_22;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
