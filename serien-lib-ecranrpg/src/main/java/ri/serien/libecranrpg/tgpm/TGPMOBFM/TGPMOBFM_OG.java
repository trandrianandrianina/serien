
package ri.serien.libecranrpg.tgpm.TGPMOBFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TGPMOBFM_OG extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public TGPMOBFM_OG(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations techniques"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OB1 = new XRiTextField();
    OB2 = new XRiTextField();
    OB3 = new XRiTextField();
    OB4 = new XRiTextField();
    OB5 = new XRiTextField();
    OB6 = new XRiTextField();
    OB7 = new XRiTextField();
    OB8 = new XRiTextField();
    OB9 = new XRiTextField();
    OB10 = new XRiTextField();
    OB11 = new XRiTextField();
    OB12 = new XRiTextField();
    OB13 = new XRiTextField();
    OB14 = new XRiTextField();
    OB15 = new XRiTextField();
    OB16 = new XRiTextField();
    OB17 = new XRiTextField();
    OB18 = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(895, 580));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Informations techniques"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OB1 ----
          OB1.setPreferredSize(new Dimension(12, 26));
          OB1.setMinimumSize(new Dimension(12, 26));
          OB1.setName("OB1");
          panel1.add(OB1);
          OB1.setBounds(25, 45, 610, OB1.getPreferredSize().height);

          //---- OB2 ----
          OB2.setPreferredSize(new Dimension(12, 26));
          OB2.setMinimumSize(new Dimension(12, 26));
          OB2.setName("OB2");
          panel1.add(OB2);
          OB2.setBounds(25, 70, 610, 26);

          //---- OB3 ----
          OB3.setPreferredSize(new Dimension(12, 26));
          OB3.setMinimumSize(new Dimension(12, 26));
          OB3.setName("OB3");
          panel1.add(OB3);
          OB3.setBounds(25, 95, 610, 26);

          //---- OB4 ----
          OB4.setPreferredSize(new Dimension(12, 26));
          OB4.setMinimumSize(new Dimension(12, 26));
          OB4.setName("OB4");
          panel1.add(OB4);
          OB4.setBounds(25, 120, 610, 26);

          //---- OB5 ----
          OB5.setPreferredSize(new Dimension(12, 26));
          OB5.setMinimumSize(new Dimension(12, 26));
          OB5.setName("OB5");
          panel1.add(OB5);
          OB5.setBounds(25, 145, 610, 26);

          //---- OB6 ----
          OB6.setPreferredSize(new Dimension(12, 26));
          OB6.setMinimumSize(new Dimension(12, 26));
          OB6.setName("OB6");
          panel1.add(OB6);
          OB6.setBounds(25, 170, 610, 26);

          //---- OB7 ----
          OB7.setPreferredSize(new Dimension(12, 26));
          OB7.setMinimumSize(new Dimension(12, 26));
          OB7.setName("OB7");
          panel1.add(OB7);
          OB7.setBounds(25, 195, 610, 26);

          //---- OB8 ----
          OB8.setPreferredSize(new Dimension(12, 26));
          OB8.setMinimumSize(new Dimension(12, 26));
          OB8.setName("OB8");
          panel1.add(OB8);
          OB8.setBounds(25, 220, 610, 26);

          //---- OB9 ----
          OB9.setPreferredSize(new Dimension(12, 26));
          OB9.setMinimumSize(new Dimension(12, 26));
          OB9.setName("OB9");
          panel1.add(OB9);
          OB9.setBounds(25, 245, 610, 26);

          //---- OB10 ----
          OB10.setPreferredSize(new Dimension(12, 26));
          OB10.setMinimumSize(new Dimension(12, 26));
          OB10.setName("OB10");
          panel1.add(OB10);
          OB10.setBounds(25, 270, 610, 26);

          //---- OB11 ----
          OB11.setPreferredSize(new Dimension(12, 26));
          OB11.setMinimumSize(new Dimension(12, 26));
          OB11.setName("OB11");
          panel1.add(OB11);
          OB11.setBounds(25, 295, 610, 26);

          //---- OB12 ----
          OB12.setPreferredSize(new Dimension(12, 26));
          OB12.setMinimumSize(new Dimension(12, 26));
          OB12.setName("OB12");
          panel1.add(OB12);
          OB12.setBounds(25, 320, 610, 26);

          //---- OB13 ----
          OB13.setPreferredSize(new Dimension(12, 26));
          OB13.setMinimumSize(new Dimension(12, 26));
          OB13.setName("OB13");
          panel1.add(OB13);
          OB13.setBounds(25, 345, 610, 26);

          //---- OB14 ----
          OB14.setPreferredSize(new Dimension(12, 26));
          OB14.setMinimumSize(new Dimension(12, 26));
          OB14.setName("OB14");
          panel1.add(OB14);
          OB14.setBounds(25, 370, 610, 26);

          //---- OB15 ----
          OB15.setPreferredSize(new Dimension(12, 26));
          OB15.setMinimumSize(new Dimension(12, 26));
          OB15.setName("OB15");
          panel1.add(OB15);
          OB15.setBounds(25, 395, 610, 26);

          //---- OB16 ----
          OB16.setPreferredSize(new Dimension(12, 26));
          OB16.setMinimumSize(new Dimension(12, 26));
          OB16.setName("OB16");
          panel1.add(OB16);
          OB16.setBounds(25, 420, 610, 26);

          //---- OB17 ----
          OB17.setPreferredSize(new Dimension(12, 26));
          OB17.setMinimumSize(new Dimension(12, 26));
          OB17.setName("OB17");
          panel1.add(OB17);
          OB17.setBounds(25, 445, 610, 26);

          //---- OB18 ----
          OB18.setPreferredSize(new Dimension(12, 26));
          OB18.setMinimumSize(new Dimension(12, 26));
          OB18.setName("OB18");
          panel1.add(OB18);
          OB18.setBounds(25, 470, 610, 26);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(27, 27, 27)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 662, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(36, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 529, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(31, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OB1;
  private XRiTextField OB2;
  private XRiTextField OB3;
  private XRiTextField OB4;
  private XRiTextField OB5;
  private XRiTextField OB6;
  private XRiTextField OB7;
  private XRiTextField OB8;
  private XRiTextField OB9;
  private XRiTextField OB10;
  private XRiTextField OB11;
  private XRiTextField OB12;
  private XRiTextField OB13;
  private XRiTextField OB14;
  private XRiTextField OB15;
  private XRiTextField OB16;
  private XRiTextField OB17;
  private XRiTextField OB18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
