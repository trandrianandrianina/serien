
package ri.serien.libecranrpg.tgpm.TGPM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class TGPM13FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private boolean isKit = false;
  
  public TGPM13FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_52.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARTI@")).trim());
    OBJ_49.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TYP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    isKit = lexique.isTrue("87");
    
    OBJ_52.setVisible(lexique.isPresent("A1LIB"));
    
    // Titre
    
    if (isKit) {
      setTitle(interpreteurD.analyseExpression("Composant du kit"));
    }
    else {
      setTitle(interpreteurD.analyseExpression("Ligne de nomenclature"));
    }
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_52 = new RiZoneSortie();
    WORD = new XRiTextField();
    OBJ_50 = new JLabel();
    WQTE = new XRiTextField();
    OBJ_25 = new JXTitledSeparator();
    OBJ_45 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    WNL1 = new XRiTextField();
    WNL2 = new XRiTextField();
    WNL3 = new XRiTextField();
    WTYP = new XRiTextField();
    NL1D = new XRiTextField();
    NL2D = new XRiTextField();
    NL3D = new XRiTextField();
    NL1F = new XRiTextField();
    NL2F = new XRiTextField();
    NL3F = new XRiTextField();
    OBJ_51 = new JLabel();
    OBJ_46 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(825, 245));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Options article");
            riSousMenu_bt6.setToolTipText("Options article");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_52 ----
          OBJ_52.setText("@A1LIB@");
          OBJ_52.setName("OBJ_52");
          p_recup.add(OBJ_52);
          OBJ_52.setBounds(190, 92, 215, OBJ_52.getPreferredSize().height);

          //---- WORD ----
          WORD.setComponentPopupMenu(BTD);
          WORD.setName("WORD");
          p_recup.add(WORD);
          WORD.setBounds(199, 50, 210, WORD.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("@ARTI@");
          OBJ_50.setFont(OBJ_50.getFont().deriveFont(OBJ_50.getFont().getStyle() | Font.BOLD));
          OBJ_50.setName("OBJ_50");
          p_recup.add(OBJ_50);
          OBJ_50.setBounds(199, 25, 149, 20);

          //---- WQTE ----
          WQTE.setComponentPopupMenu(BTD);
          WQTE.setName("WQTE");
          p_recup.add(WQTE);
          WQTE.setBounds(85, 90, 98, WQTE.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setTitle("Niveaux");
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(420, 25, 180, 20);

          //---- OBJ_45 ----
          OBJ_45.setText("D\u00e9but");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(425, 54, 45, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("@TYP@");
          OBJ_49.setFont(OBJ_49.getFont().deriveFont(OBJ_49.getFont().getStyle() | Font.BOLD));
          OBJ_49.setName("OBJ_49");
          p_recup.add(OBJ_49);
          OBJ_49.setBounds(155, 25, 33, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("Nl1");
          OBJ_26.setFont(OBJ_26.getFont().deriveFont(OBJ_26.getFont().getStyle() | Font.BOLD));
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(25, 25, 31, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("Nl2");
          OBJ_27.setFont(OBJ_27.getFont().deriveFont(OBJ_27.getFont().getStyle() | Font.BOLD));
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(66, 25, 31, 20);

          //---- OBJ_28 ----
          OBJ_28.setText("Nl3");
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(110, 25, 31, 20);

          //---- WNL1 ----
          WNL1.setComponentPopupMenu(BTD);
          WNL1.setName("WNL1");
          p_recup.add(WNL1);
          WNL1.setBounds(25, 50, 36, WNL1.getPreferredSize().height);

          //---- WNL2 ----
          WNL2.setComponentPopupMenu(BTD);
          WNL2.setName("WNL2");
          p_recup.add(WNL2);
          WNL2.setBounds(66, 50, 36, WNL2.getPreferredSize().height);

          //---- WNL3 ----
          WNL3.setComponentPopupMenu(BTD);
          WNL3.setName("WNL3");
          p_recup.add(WNL3);
          WNL3.setBounds(110, 50, 36, WNL3.getPreferredSize().height);

          //---- WTYP ----
          WTYP.setComponentPopupMenu(BTD);
          WTYP.setName("WTYP");
          p_recup.add(WTYP);
          WTYP.setBounds(155, 50, 40, WTYP.getPreferredSize().height);

          //---- NL1D ----
          NL1D.setComponentPopupMenu(BTD);
          NL1D.setName("NL1D");
          p_recup.add(NL1D);
          NL1D.setBounds(475, 50, 36, NL1D.getPreferredSize().height);

          //---- NL2D ----
          NL2D.setComponentPopupMenu(BTD);
          NL2D.setName("NL2D");
          p_recup.add(NL2D);
          NL2D.setBounds(515, 50, 36, NL2D.getPreferredSize().height);

          //---- NL3D ----
          NL3D.setComponentPopupMenu(BTD);
          NL3D.setName("NL3D");
          p_recup.add(NL3D);
          NL3D.setBounds(555, 50, 36, NL3D.getPreferredSize().height);

          //---- NL1F ----
          NL1F.setComponentPopupMenu(BTD);
          NL1F.setName("NL1F");
          p_recup.add(NL1F);
          NL1F.setBounds(475, 90, 36, NL1F.getPreferredSize().height);

          //---- NL2F ----
          NL2F.setComponentPopupMenu(BTD);
          NL2F.setName("NL2F");
          p_recup.add(NL2F);
          NL2F.setBounds(515, 90, 36, NL2F.getPreferredSize().height);

          //---- NL3F ----
          NL3F.setComponentPopupMenu(BTD);
          NL3F.setName("NL3F");
          p_recup.add(NL3F);
          NL3F.setBounds(555, 90, 36, NL3F.getPreferredSize().height);

          //---- OBJ_51 ----
          OBJ_51.setText("Quantit\u00e9");
          OBJ_51.setName("OBJ_51");
          p_recup.add(OBJ_51);
          OBJ_51.setBounds(25, 94, 60, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Fin");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(425, 94, 45, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 630, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(45, 45, 45)
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 155, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie OBJ_52;
  private XRiTextField WORD;
  private JLabel OBJ_50;
  private XRiTextField WQTE;
  private JXTitledSeparator OBJ_25;
  private JLabel OBJ_45;
  private JLabel OBJ_49;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private XRiTextField WNL1;
  private XRiTextField WNL2;
  private XRiTextField WNL3;
  private XRiTextField WTYP;
  private XRiTextField NL1D;
  private XRiTextField NL2D;
  private XRiTextField NL3D;
  private XRiTextField NL1F;
  private XRiTextField NL2F;
  private XRiTextField NL3F;
  private JLabel OBJ_51;
  private JLabel OBJ_46;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
