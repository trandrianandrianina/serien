
package ri.serien.libecranrpg.vtvm.VTVM05FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VTVM05FM_G13 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private boolean[] forme = { true, true, true, true, true, true, true };
  private boolean[] axis = { true, false, false, false, false, false, false };
  private Color[] couleursCourbes = { new Color(25, 100, 184), new Color(9, 172, 166), new Color(9, 172, 101), new Color(217, 219, 52),
      new Color(255, 166, 27), new Color(128, 65, 127), new Color(157, 54, 86) };
  
  public VTVM05FM_G13(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // GRAPHE
    
    String[] jours = new String[7];
    String[] libelle = new String[14];
    String[] ligneDonnee1 = new String[14];
    String[] ligneDonnee2 = new String[14];
    String[] ligneDonnee3 = new String[14];
    String[] ligneDonnee4 = new String[14];
    String[] ligneDonnee5 = new String[14];
    String[] ligneDonnee6 = new String[14];
    String[] ligneDonnee7 = new String[14];
    String[] ligneDonnee8 = new String[14];
    String[] ligneDonnee9 = new String[14];
    String[] ligneDonnee10 = new String[14];
    String[] ligneDonnee11 = new String[14];
    String[] ligneDonnee12 = new String[14];
    String[] ligneDonnee13 = new String[14];
    String[] ligneDonnee14 = new String[14];
    
    // chargement jours
    for (int i = 0; i < jours.length; i++) {
      jours[i] = lexique.HostFieldGetData("J0" + (i + 1));
    }
    
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("H" + (i + 1));
    }
    
    // Chargement des données
    for (int i = 0; i < jours.length; i++) {
      
      // on charge toute les zones
      ligneDonnee1[i] = lexique.HostFieldGetData("L90" + (i + 1) + "01");
      ligneDonnee2[i] = lexique.HostFieldGetData("L90" + (i + 1) + "02");
      ligneDonnee3[i] = lexique.HostFieldGetData("L90" + (i + 1) + "03");
      ligneDonnee4[i] = lexique.HostFieldGetData("L90" + (i + 1) + "04");
      ligneDonnee5[i] = lexique.HostFieldGetData("L90" + (i + 1) + "05");
      ligneDonnee6[i] = lexique.HostFieldGetData("L90" + (i + 1) + "06");
      ligneDonnee7[i] = lexique.HostFieldGetData("L90" + (i + 1) + "07");
      ligneDonnee8[i] = lexique.HostFieldGetData("L90" + (i + 1) + "08");
      ligneDonnee9[i] = lexique.HostFieldGetData("L90" + (i + 1) + "09");
      ligneDonnee10[i] = lexique.HostFieldGetData("L90" + (i + 1) + "10");
      ligneDonnee11[i] = lexique.HostFieldGetData("L90" + (i + 1) + "11");
      ligneDonnee12[i] = lexique.HostFieldGetData("L90" + (i + 1) + "12");
      ligneDonnee13[i] = lexique.HostFieldGetData("L90" + (i + 1) + "13");
      ligneDonnee14[i] = lexique.HostFieldGetData("L90" + (i + 1) + "14");
      
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][2];
    
    for (int i = 0; i < jours.length; i++) {
      
      ligneDonnee1[i] = ligneDonnee1[i].replaceAll("\\s", "0");
      ligneDonnee2[i] = ligneDonnee2[i].replaceAll("\\s", "0");
      ligneDonnee3[i] = ligneDonnee3[i].replaceAll("\\s", "0");
      ligneDonnee4[i] = ligneDonnee4[i].replaceAll("\\s", "0");
      ligneDonnee5[i] = ligneDonnee5[i].replaceAll("\\s", "0");
      ligneDonnee6[i] = ligneDonnee6[i].replaceAll("\\s", "0");
      ligneDonnee7[i] = ligneDonnee7[i].replaceAll("\\s", "0");
      ligneDonnee8[i] = ligneDonnee8[i].replaceAll("\\s", "0");
      ligneDonnee9[i] = ligneDonnee9[i].replaceAll("\\s", "0");
      ligneDonnee10[i] = ligneDonnee10[i].replaceAll("\\s", "0");
      ligneDonnee11[i] = ligneDonnee11[i].replaceAll("\\s", "0");
      ligneDonnee12[i] = ligneDonnee12[i].replaceAll("\\s", "0");
      ligneDonnee13[i] = ligneDonnee13[i].replaceAll("\\s", "0");
      ligneDonnee14[i] = ligneDonnee14[i].replaceAll("\\s", "0");
      
      data[0][1] = Double.parseDouble(ligneDonnee1[i]);
      data[1][1] = Double.parseDouble(ligneDonnee2[i]);
      data[2][1] = Double.parseDouble(ligneDonnee3[i]);
      data[3][1] = Double.parseDouble(ligneDonnee4[i]);
      data[4][1] = Double.parseDouble(ligneDonnee5[i]);
      data[5][1] = Double.parseDouble(ligneDonnee6[i]);
      data[6][1] = Double.parseDouble(ligneDonnee7[i]);
      data[7][1] = Double.parseDouble(ligneDonnee8[i]);
      data[8][1] = Double.parseDouble(ligneDonnee9[i]);
      data[9][1] = Double.parseDouble(ligneDonnee10[i]);
      data[10][1] = Double.parseDouble(ligneDonnee11[i]);
      data[11][1] = Double.parseDouble(ligneDonnee12[i]);
      data[12][1] = Double.parseDouble(ligneDonnee13[i]);
      data[13][1] = Double.parseDouble(ligneDonnee14[i]);
    }
    
    graphe.setGraphCombiForme(forme);
    graphe.setGraphCombiColor(couleursCourbes);
    graphe.setGraphCombiRangeAxis(axis);
    // graphe.setOrientation(1);
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Evolution horaire pour les sept jours de la semaine", true);
    
    l_graphe.setIcon(graphe.getPicture(1068, 582));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle("Détail des statistiques horaires");
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    OBJ_10 = new JButton();
    l_graphe = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
              .addGroup(P_CentreLayout.createParallelGroup()
                  .addGroup(P_CentreLayout.createSequentialGroup().addGap(945, 945, 945).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE,
                      140, GroupLayout.PREFERRED_SIZE))
                  .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap().addComponent(l_graphe, GroupLayout.PREFERRED_SIZE,
                      1068, GroupLayout.PREFERRED_SIZE)))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap()
              .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 582, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JButton OBJ_10;
  private JLabel l_graphe;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
