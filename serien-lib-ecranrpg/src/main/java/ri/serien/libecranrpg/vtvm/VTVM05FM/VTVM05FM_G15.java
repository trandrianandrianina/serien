
package ri.serien.libecranrpg.vtvm.VTVM05FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VTVM05FM_G15 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  
  public VTVM05FM_G15(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // GRAPHE
    
    String[] libelle = new String[14];
    String[] ligneDonnee = new String[14];
    int onglet = tabbedPane1.getSelectedIndex();
    
    // noms des onglets
    for (int i = 0; i < tabbedPane1.getTabCount(); i++) {
      tabbedPane1.setTitleAt(i, lexique.HostFieldGetData("J0" + (i + 1)));
    }
    
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("H" + (i + 1));
    }
    
    // Chargement des données
    for (int i = 0; i < ligneDonnee.length; i++) {
      
      // on charge toute les zones pour l'onglet actif
      ligneDonnee[i] = lexique.HostFieldGetData("L90" + (onglet + 1) + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
      // " : " + ligneDonnee[i]);
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][2];
    
    for (int i = 0; i < libelle.length; i++) {
      data[i][0] = libelle[i];
      ligneDonnee[i] = ligneDonnee[i].replaceAll("\\s", "0");
      data[i][1] = Double.parseDouble(ligneDonnee[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Part par heure et par jour", false);
    
    // on place le graphe dans l'onglet actif
    switch (onglet) {
      case 0:
        l_graphe.setIcon(graphe.getPicture(1070, 565));
      case 1:
        l_graphe2.setIcon(graphe.getPicture(1070, 565));
      case 2:
        l_graphe3.setIcon(graphe.getPicture(1070, 565));
      case 3:
        l_graphe4.setIcon(graphe.getPicture(1070, 565));
      case 4:
        l_graphe5.setIcon(graphe.getPicture(1070, 565));
      case 5:
        l_graphe6.setIcon(graphe.getPicture(1070, 565));
      case 6:
        l_graphe7.setIcon(graphe.getPicture(1070, 565));
      case 7:
        l_graphe8.setIcon(graphe.getPicture(1070, 565));
    }
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // Titre
    setTitle("Détail des statistiques horaires");
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void tabbedPane1MouseClicked(MouseEvent e) {
    setData();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Bas = new JPanel();
    OBJ_10 = new JButton();
    tabbedPane1 = new JTabbedPane();
    panel1 = new JPanel();
    l_graphe = new JLabel();
    panel2 = new JPanel();
    l_graphe2 = new JLabel();
    panel3 = new JPanel();
    l_graphe3 = new JLabel();
    panel4 = new JPanel();
    l_graphe4 = new JLabel();
    panel5 = new JPanel();
    l_graphe5 = new JLabel();
    panel6 = new JPanel();
    l_graphe6 = new JLabel();
    panel7 = new JPanel();
    l_graphe7 = new JLabel();
    panel8 = new JPanel();
    l_graphe8 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Bas ========
    {
      P_Bas.setBackground(new Color(238, 238, 210));
      P_Bas.setPreferredSize(new Dimension(1092, 50));
      P_Bas.setName("P_Bas");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      GroupLayout P_BasLayout = new GroupLayout(P_Bas);
      P_Bas.setLayout(P_BasLayout);
      P_BasLayout.setHorizontalGroup(P_BasLayout.createParallelGroup().addGroup(P_BasLayout.createSequentialGroup().addGap(940, 940, 940)
          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)));
      P_BasLayout.setVerticalGroup(P_BasLayout.createParallelGroup().addGroup(P_BasLayout.createSequentialGroup().addGap(5, 5, 5)
          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
    }
    contentPane.add(P_Bas, BorderLayout.SOUTH);
    
    // ======== tabbedPane1 ========
    {
      tabbedPane1.setBackground(new Color(238, 238, 210));
      tabbedPane1.setOpaque(true);
      tabbedPane1.setName("tabbedPane1");
      tabbedPane1.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          tabbedPane1MouseClicked(e);
        }
      });
      
      // ======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);
        
        // ---- l_graphe ----
        l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe.setComponentPopupMenu(null);
        l_graphe.setBackground(new Color(214, 217, 223));
        l_graphe.setPreferredSize(new Dimension(1070, 565));
        l_graphe.setName("l_graphe");
        panel1.add(l_graphe);
        l_graphe.setBounds(new Rectangle(new Point(10, 10), l_graphe.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J01@", panel1);
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- l_graphe2 ----
        l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe2.setComponentPopupMenu(null);
        l_graphe2.setBackground(new Color(214, 217, 223));
        l_graphe2.setPreferredSize(new Dimension(1070, 565));
        l_graphe2.setName("l_graphe2");
        panel2.add(l_graphe2);
        l_graphe2.setBounds(new Rectangle(new Point(10, 10), l_graphe2.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J02@", panel2);
      
      // ======== panel3 ========
      {
        panel3.setOpaque(false);
        panel3.setName("panel3");
        panel3.setLayout(null);
        
        // ---- l_graphe3 ----
        l_graphe3.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe3.setComponentPopupMenu(null);
        l_graphe3.setBackground(new Color(214, 217, 223));
        l_graphe3.setPreferredSize(new Dimension(1070, 565));
        l_graphe3.setName("l_graphe3");
        panel3.add(l_graphe3);
        l_graphe3.setBounds(new Rectangle(new Point(10, 10), l_graphe3.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel3.getComponentCount(); i++) {
            Rectangle bounds = panel3.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel3.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel3.setMinimumSize(preferredSize);
          panel3.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J03@", panel3);
      
      // ======== panel4 ========
      {
        panel4.setOpaque(false);
        panel4.setName("panel4");
        panel4.setLayout(null);
        
        // ---- l_graphe4 ----
        l_graphe4.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe4.setComponentPopupMenu(null);
        l_graphe4.setBackground(new Color(214, 217, 223));
        l_graphe4.setPreferredSize(new Dimension(1070, 565));
        l_graphe4.setName("l_graphe4");
        panel4.add(l_graphe4);
        l_graphe4.setBounds(new Rectangle(new Point(10, 10), l_graphe4.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel4.getComponentCount(); i++) {
            Rectangle bounds = panel4.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel4.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel4.setMinimumSize(preferredSize);
          panel4.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J04@", panel4);
      
      // ======== panel5 ========
      {
        panel5.setOpaque(false);
        panel5.setName("panel5");
        panel5.setLayout(null);
        
        // ---- l_graphe5 ----
        l_graphe5.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe5.setComponentPopupMenu(null);
        l_graphe5.setBackground(new Color(214, 217, 223));
        l_graphe5.setPreferredSize(new Dimension(1070, 565));
        l_graphe5.setName("l_graphe5");
        panel5.add(l_graphe5);
        l_graphe5.setBounds(new Rectangle(new Point(10, 10), l_graphe5.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel5.getComponentCount(); i++) {
            Rectangle bounds = panel5.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel5.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel5.setMinimumSize(preferredSize);
          panel5.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J05@", panel5);
      
      // ======== panel6 ========
      {
        panel6.setOpaque(false);
        panel6.setName("panel6");
        panel6.setLayout(null);
        
        // ---- l_graphe6 ----
        l_graphe6.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe6.setComponentPopupMenu(null);
        l_graphe6.setBackground(new Color(214, 217, 223));
        l_graphe6.setPreferredSize(new Dimension(1070, 565));
        l_graphe6.setName("l_graphe6");
        panel6.add(l_graphe6);
        l_graphe6.setBounds(new Rectangle(new Point(10, 10), l_graphe6.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel6.getComponentCount(); i++) {
            Rectangle bounds = panel6.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel6.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel6.setMinimumSize(preferredSize);
          panel6.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J06@", panel6);
      
      // ======== panel7 ========
      {
        panel7.setOpaque(false);
        panel7.setName("panel7");
        panel7.setLayout(null);
        
        // ---- l_graphe7 ----
        l_graphe7.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe7.setComponentPopupMenu(null);
        l_graphe7.setBackground(new Color(214, 217, 223));
        l_graphe7.setPreferredSize(new Dimension(1070, 565));
        l_graphe7.setName("l_graphe7");
        panel7.add(l_graphe7);
        l_graphe7.setBounds(new Rectangle(new Point(10, 10), l_graphe7.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel7.getComponentCount(); i++) {
            Rectangle bounds = panel7.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel7.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel7.setMinimumSize(preferredSize);
          panel7.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("@J07@", panel7);
      
      // ======== panel8 ========
      {
        panel8.setOpaque(false);
        panel8.setName("panel8");
        panel8.setLayout(null);
        
        // ---- l_graphe8 ----
        l_graphe8.setHorizontalAlignment(SwingConstants.CENTER);
        l_graphe8.setComponentPopupMenu(null);
        l_graphe8.setBackground(new Color(214, 217, 223));
        l_graphe8.setPreferredSize(new Dimension(1070, 565));
        l_graphe8.setName("l_graphe8");
        panel8.add(l_graphe8);
        l_graphe8.setBounds(new Rectangle(new Point(10, 10), l_graphe8.getPreferredSize()));
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel8.getComponentCount(); i++) {
            Rectangle bounds = panel8.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel8.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel8.setMinimumSize(preferredSize);
          panel8.setPreferredSize(preferredSize);
        }
      }
      tabbedPane1.addTab("Total semaine", panel8);
    }
    contentPane.add(tabbedPane1, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Bas;
  private JButton OBJ_10;
  private JTabbedPane tabbedPane1;
  private JPanel panel1;
  private JLabel l_graphe;
  private JPanel panel2;
  private JLabel l_graphe2;
  private JPanel panel3;
  private JLabel l_graphe3;
  private JPanel panel4;
  private JLabel l_graphe4;
  private JPanel panel5;
  private JLabel l_graphe5;
  private JPanel panel6;
  private JLabel l_graphe6;
  private JPanel panel7;
  private JLabel l_graphe7;
  private JPanel panel8;
  private JLabel l_graphe8;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
