
package ri.serien.libecranrpg.vtvm.VTVM05FM;

// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.jfree.data.DefaultKeyedValues;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VTVM05FM_G12 extends JDialog {
  
  private Lexical lexique = null;
  
  private iData interpreteurD = null;
  
  private JPanel master = null;
  
  // couleurs des graphes
  
  private boolean[] forme = { false, false };
  private boolean[] axis = { true, false };
  private Color[] couleursCourbes = { new Color(25, 100, 184), new Color(9, 172, 166) };
  
  // valeur première période sur 12 mois
  private String[] va1 = new String[11];
  // valeur deuxième période sur 12 mois
  private String[] va2 = new String[11];
  
  // moyenne première période sur 12 mois
  private String[] mo1 = new String[11];
  // moyenne deuxième période sur 12 mois
  private String[] mo2 = new String[11];
  
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe1 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe2 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe3 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe4 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe5 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe6 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe7 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe8 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe9 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe10 = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  
  private String[] ligneDonnee = new String[17];
  private String[] libelle = new String[2];
  private String[] donneeva01 = new String[2];
  private String[] donneeva02 = new String[2];
  private String[] donneeva03 = new String[2];
  private String[] donneeva04 = new String[2];
  private String[] donneeva05 = new String[2];
  private String[] donneeva06 = new String[2];
  private String[] donneeva07 = new String[2];
  private String[] donneeva08 = new String[2];
  private String[] donneeva09 = new String[2];
  private String[] donneeva10 = new String[2];
  private String[] donneeva11 = new String[2];
  
  private String[] donneemo01 = new String[2];
  private String[] donneemo02 = new String[2];
  private String[] donneemo03 = new String[2];
  private String[] donneemo04 = new String[2];
  private String[] donneemo05 = new String[2];
  private String[] donneemo06 = new String[2];
  private String[] donneemo07 = new String[2];
  private String[] donneemo08 = new String[2];
  private String[] donneemo09 = new String[2];
  private String[] donneemo10 = new String[2];
  private String[] donneemo11 = new String[2];
  
  public VTVM05FM_G12(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // noms des onglets
    // for (int i = 0; i < tabbedPane1.getTabCount(); i++) {
    // tabbedPane1.setTitleAt(i, lexique.HostFieldGetData("M" + (i + 1)));
    // }
    
    // GRAPHE
    
    // Chargement des libellés
    
    libelle[0] = lexique.HostFieldGetData("HLDB11").substring(26, 46);
    libelle[1] = lexique.HostFieldGetData("HLDB11").substring(47, 68);
    
    // Chargement des données
    
    // on charge toutes les lignes de la LD
    for (int i = 0; i < ligneDonnee.length; i++) {
      
      ligneDonnee[i] = lexique.HostFieldGetData("LH" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
      
    }
    
    // On crée les tables de données qui nous intéresse depuis l'EBCDIC
    int indexBis = 0;
    for (int i = 0; i < ligneDonnee.length; i++) {
      indexBis = i;
      if ((i != 3) && (i != 6) && (i != 7) && (i != 8) && (i != 9) && (i != 10)) {
        if (i > 3) {
          indexBis = i - 1;
        }
        if (i > 10) {
          indexBis = i - 6;
        }
        
        va1[indexBis] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(26, 35).trim(), 0);
        
        va2[indexBis] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(47, 57).trim(), 0);
        
        mo1[indexBis] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(36, 46).trim(), 0);
        
        mo2[indexBis] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(58, 68).trim(), 0);
      }
    }
    
    // Préparation des données
    
    // Onglet 1
    getLabel(0, donneeva01, donneemo01, graphe, l_graphe);
    
    // Onglet 2
    getLabel(1, donneeva02, donneemo02, graphe1, l_graphe2);
    
    // Onglet 3
    getLabel(2, donneeva03, donneemo03, graphe2, l_graphe3);
    
    // Onglet 4
    getLabel(3, donneeva04, donneemo04, graphe3, l_graphe4);
    
    // Onglet 5
    getLabel(4, donneeva05, donneemo05, graphe4, l_graphe5);
    
    // Onglet 6
    getLabel(5, donneeva06, donneemo06, graphe5, l_graphe6);
    
    // Onglet 7
    getLabel(6, donneeva07, donneemo07, graphe6, l_graphe7);
    
    // Onglet 8
    getLabel(7, donneeva08, donneemo08, graphe7, l_graphe8);
    
    // Onglet 9
    getLabel(8, donneeva09, donneemo09, graphe8, l_graphe9);
    
    // Onglet 10
    getLabel(9, donneeva10, donneemo10, graphe9, l_graphe10);
    
    // Onglet 11
    getLabel(10, donneeva11, donneemo11, graphe10, l_graphe11);
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques " + lexique.HostFieldGetData("L1I1")));
  }
  
  public void getLabel(int numero, String[] tabVa, String[] tabMo, RiGraphe grp, JLabel label) {
    
    Object[][] data = new Object[2][3];
    DefaultKeyedValues dkv1 = new DefaultKeyedValues();
    DefaultKeyedValues dkv2 = new DefaultKeyedValues();
    
    tabVa[0] = va1[numero].replaceAll("\\s", "0");
    tabVa[1] = va2[numero].replaceAll("\\s", "0");
    tabMo[0] = mo1[numero].replaceAll("\\s", "0");
    tabMo[1] = mo2[numero].replaceAll("\\s", "0");
    
    for (int i = 0; i < libelle.length; i++) {
      dkv1.addValue(libelle[i], Double.parseDouble(tabVa[i]));
      dkv2.addValue(libelle[i], Double.parseDouble(tabMo[i]));
    }
    data[0][0] = "Valeur";
    data[1][0] = "Moyenne";
    data[0][1] = dkv1;
    data[1][1] = dkv2;
    
    grp.setGraphCombiForme(forme);
    grp.setGraphCombiColor(couleursCourbes);
    grp.setGraphCombiRangeAxis(axis);
    
    grp.setDonnee(data, "", false);
    grp.getGraphe(lexique.HostFieldGetData("LIBPOS"), true);
    
    // ajout du graphe dans le lablel
    label.setIcon(grp.getPicture(1070, 455));
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    tabbedPane1 = new JTabbedPane();
    panel3 = new JPanel();
    l_graphe = new JLabel();
    panel4 = new JPanel();
    l_graphe2 = new JLabel();
    panel5 = new JPanel();
    l_graphe3 = new JLabel();
    panel6 = new JPanel();
    l_graphe4 = new JLabel();
    panel7 = new JPanel();
    l_graphe5 = new JLabel();
    panel8 = new JPanel();
    l_graphe6 = new JLabel();
    panel9 = new JPanel();
    l_graphe7 = new JLabel();
    panel10 = new JPanel();
    l_graphe8 = new JLabel();
    panel11 = new JPanel();
    l_graphe9 = new JLabel();
    panel12 = new JPanel();
    l_graphe10 = new JLabel();
    panel13 = new JPanel();
    l_graphe11 = new JLabel();
    panel1 = new JPanel();
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 600));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel2 ========
    {
      panel2.setBackground(new Color(238, 238, 210));
      panel2.setName("panel2");
      panel2.setLayout(new BorderLayout());
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setBackground(new Color(238, 238, 210));
        tabbedPane1.setName("tabbedPane1");
        
        // ======== panel3 ========
        {
          panel3.setBackground(new Color(238, 238, 210));
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- l_graphe ----
          l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe.setComponentPopupMenu(null);
          l_graphe.setBackground(new Color(214, 217, 223));
          l_graphe.setPreferredSize(new Dimension(800, 650));
          l_graphe.setName("l_graphe");
          panel3.add(l_graphe);
          l_graphe.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("CA r\u00e9el", panel3);
        
        // ======== panel4 ========
        {
          panel4.setOpaque(false);
          panel4.setPreferredSize(new Dimension(1095, 885));
          panel4.setMinimumSize(new Dimension(1095, 885));
          panel4.setName("panel4");
          panel4.setLayout(null);
          
          // ---- l_graphe2 ----
          l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe2.setComponentPopupMenu(null);
          l_graphe2.setBackground(new Color(214, 217, 223));
          l_graphe2.setPreferredSize(new Dimension(800, 650));
          l_graphe2.setName("l_graphe2");
          panel4.add(l_graphe2);
          l_graphe2.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Prix de revient", panel4);
        
        // ======== panel5 ========
        {
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);
          
          // ---- l_graphe3 ----
          l_graphe3.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe3.setComponentPopupMenu(null);
          l_graphe3.setBackground(new Color(214, 217, 223));
          l_graphe3.setPreferredSize(new Dimension(800, 650));
          l_graphe3.setName("l_graphe3");
          panel5.add(l_graphe3);
          l_graphe3.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Marge brute", panel5);
        
        // ======== panel6 ========
        {
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- l_graphe4 ----
          l_graphe4.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe4.setComponentPopupMenu(null);
          l_graphe4.setBackground(new Color(214, 217, 223));
          l_graphe4.setPreferredSize(new Dimension(800, 650));
          l_graphe4.setName("l_graphe4");
          panel6.add(l_graphe4);
          l_graphe4.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("CA th\u00e9orique", panel6);
        
        // ======== panel7 ========
        {
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- l_graphe5 ----
          l_graphe5.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe5.setComponentPopupMenu(null);
          l_graphe5.setBackground(new Color(214, 217, 223));
          l_graphe5.setPreferredSize(new Dimension(800, 650));
          l_graphe5.setName("l_graphe5");
          panel7.add(l_graphe5);
          l_graphe5.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Co\u00fbt action commerciale", panel7);
        
        // ======== panel8 ========
        {
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);
          
          // ---- l_graphe6 ----
          l_graphe6.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe6.setComponentPopupMenu(null);
          l_graphe6.setBackground(new Color(214, 217, 223));
          l_graphe6.setPreferredSize(new Dimension(800, 650));
          l_graphe6.setName("l_graphe6");
          panel8.add(l_graphe6);
          l_graphe6.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Nombre factures", panel8);
        
        // ======== panel9 ========
        {
          panel9.setOpaque(false);
          panel9.setName("panel9");
          panel9.setLayout(null);
          
          // ---- l_graphe7 ----
          l_graphe7.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe7.setComponentPopupMenu(null);
          l_graphe7.setBackground(new Color(214, 217, 223));
          l_graphe7.setPreferredSize(new Dimension(800, 650));
          l_graphe7.setName("l_graphe7");
          panel9.add(l_graphe7);
          l_graphe7.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel9.getComponentCount(); i++) {
              Rectangle bounds = panel9.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel9.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel9.setMinimumSize(preferredSize);
            panel9.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Nombre de lignes", panel9);
        
        // ======== panel10 ========
        {
          panel10.setOpaque(false);
          panel10.setName("panel10");
          panel10.setLayout(null);
          
          // ---- l_graphe8 ----
          l_graphe8.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe8.setComponentPopupMenu(null);
          l_graphe8.setBackground(new Color(214, 217, 223));
          l_graphe8.setPreferredSize(new Dimension(800, 650));
          l_graphe8.setName("l_graphe8");
          panel10.add(l_graphe8);
          l_graphe8.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel10.getComponentCount(); i++) {
              Rectangle bounds = panel10.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel10.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel10.setMinimumSize(preferredSize);
            panel10.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Nombre d'avoirs", panel10);
        
        // ======== panel11 ========
        {
          panel11.setOpaque(false);
          panel11.setName("panel11");
          panel11.setLayout(null);
          
          // ---- l_graphe9 ----
          l_graphe9.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe9.setComponentPopupMenu(null);
          l_graphe9.setBackground(new Color(214, 217, 223));
          l_graphe9.setPreferredSize(new Dimension(800, 650));
          l_graphe9.setName("l_graphe9");
          panel11.add(l_graphe9);
          l_graphe9.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel11.getComponentCount(); i++) {
              Rectangle bounds = panel11.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel11.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel11.setMinimumSize(preferredSize);
            panel11.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("Nombre de lignes d'avoir", panel11);
        
        // ======== panel12 ========
        {
          panel12.setOpaque(false);
          panel12.setName("panel12");
          panel12.setLayout(null);
          
          // ---- l_graphe10 ----
          l_graphe10.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe10.setComponentPopupMenu(null);
          l_graphe10.setBackground(new Color(214, 217, 223));
          l_graphe10.setPreferredSize(new Dimension(800, 650));
          l_graphe10.setName("l_graphe10");
          panel12.add(l_graphe10);
          l_graphe10.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel12.getComponentCount(); i++) {
              Rectangle bounds = panel12.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel12.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel12.setMinimumSize(preferredSize);
            panel12.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("chiffre moy./facture", panel12);
        
        // ======== panel13 ========
        {
          panel13.setOpaque(false);
          panel13.setName("panel13");
          panel13.setLayout(null);
          
          // ---- l_graphe11 ----
          l_graphe11.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe11.setComponentPopupMenu(null);
          l_graphe11.setBackground(new Color(214, 217, 223));
          l_graphe11.setPreferredSize(new Dimension(800, 650));
          l_graphe11.setName("l_graphe11");
          panel13.add(l_graphe11);
          l_graphe11.setBounds(10, 20, 1070, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel13.getComponentCount(); i++) {
              Rectangle bounds = panel13.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel13.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel13.setMinimumSize(preferredSize);
            panel13.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("chiffre moy./ligne", panel13);
      }
      panel2.add(tabbedPane1, BorderLayout.NORTH);
      
      // ======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setMinimumSize(new Dimension(1080, 50));
        panel1.setMaximumSize(new Dimension(1080, 50));
        panel1.setPreferredSize(new Dimension(1080, 50));
        panel1.setName("panel1");
        
        // ---- OBJ_10 ----
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setToolTipText("Retour");
        OBJ_10.setText("Retour");
        OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        
        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup()
            .addGap(945, 945, 945).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)));
        panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5)
            .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
      }
      panel2.add(panel1, BorderLayout.SOUTH);
    }
    contentPane.add(panel2, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel panel2;
  private JTabbedPane tabbedPane1;
  private JPanel panel3;
  private JLabel l_graphe;
  private JPanel panel4;
  private JLabel l_graphe2;
  private JPanel panel5;
  private JLabel l_graphe3;
  private JPanel panel6;
  private JLabel l_graphe4;
  private JPanel panel7;
  private JLabel l_graphe5;
  private JPanel panel8;
  private JLabel l_graphe6;
  private JPanel panel9;
  private JLabel l_graphe7;
  private JPanel panel10;
  private JLabel l_graphe8;
  private JPanel panel11;
  private JLabel l_graphe9;
  private JPanel panel12;
  private JLabel l_graphe10;
  private JPanel panel13;
  private JLabel l_graphe11;
  private JPanel panel1;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
