
package ri.serien.libecranrpg.vtvm.VTVM05FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VTVM05FM_A8 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _M1_Title = { "Mois", "HLDA82", };
  private String[][] _M1_Data = { { "M1", "L81", }, { "M2", "L82", }, { "M3", "L83", }, { "M4", "L84", }, { "M5", "L85", },
      { "M6", "L86", }, { "M7", "L87", }, { "M8", "L88", }, { "M9", "L89", }, { "M10", "L810", }, { "M11", "L811", }, { "M12", "L812", }, };
  private int[] _M1_Width = { 80, 620, };
  private String[] _LIST3_Title = { "HLDA81", };
  
  private int[] _LIST3_Width = { 184, };
  private String[] _L813_Title = { "", };
  private String[][] _L813_Data = { { "L813", }, { "L814", }, { "L815", }, };
  private int[] _L813_Width = { 67, };
  // private String[] _LIST_Top=null;
  // private String[] _LIST1_Top=null;
  // private String[] _LIST3_Top=null;
  
  private VTVM05FM_G02 d_02 = null;
  private VTVM05FM_G03 d_03 = null;
  private VTVM05FM_G04 d_04 = null;
  private VTVM05FM_G05 d_05 = null;
  private VTVM05FM_G06 d_06 = null;
  private VTVM05FM_G07 d_07 = null;
  private VTVM05FM_G08 d_08 = null;
  private VTVM05FM_G09 d_09 = null;
  
  public VTVM05FM_A8(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    M1.setAspectTable(null, _M1_Title, _M1_Data, _M1_Width, false, null, null, null, null);
    LIST3.setAspectTable(null, _LIST3_Title, null, _LIST3_Width, false, null, null, null, null);
    L813.setAspectTable(null, _L813_Title, _L813_Data, _L813_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Tableau de bord  @L1I1@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L1TL1R@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    // majTable(L813, L813.get_LIST_Title_Data_Brut(), _L813_Top);
    // majTable(LIST3, LIST3.get_LIST_Title_Data_Brut(), _LIST3_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/stats.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (d_02 != null) {
      d_02.reveiller();
    }
    else {
      d_02 = new VTVM05FM_G02(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (d_03 != null) {
      d_03.reveiller();
    }
    else {
      d_03 = new VTVM05FM_G03(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    if (d_04 != null) {
      d_04.reveiller();
    }
    else {
      d_04 = new VTVM05FM_G04(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    if (d_05 != null) {
      d_05.reveiller();
    }
    else {
      d_05 = new VTVM05FM_G05(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    if (d_06 != null) {
      d_06.reveiller();
    }
    else {
      d_06 = new VTVM05FM_G06(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    if (d_07 != null) {
      d_07.reveiller();
    }
    else {
      d_07 = new VTVM05FM_G07(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    if (d_08 != null) {
      d_08.reveiller();
    }
    else {
      d_08 = new VTVM05FM_G08(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    if (d_09 != null) {
      d_09.reveiller();
    }
    else {
      d_09 = new VTVM05FM_G09(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void M1MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _LIST_Top, "1", "ENTER", e);
    if (M1.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_33 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_42 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST4 = new JScrollPane();
    M1 = new XRiTable();
    SCROLLPANE_LIST5 = new JScrollPane();
    L813 = new XRiTable();
    OBJ_86 = new JLabel();
    OBJ_88 = new JLabel();
    OBJ_75 = new JLabel();
    SCROLLPANE_LIST6 = new JScrollPane();
    LIST3 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord  @L1I1@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_33 ----
          OBJ_33.setText("Etablissement");
          OBJ_33.setName("OBJ_33");
          p_tete_gauche.add(OBJ_33);
          OBJ_33.setBounds(10, 6, 88, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 1, 40, INDETB.getPreferredSize().height);

          //---- OBJ_42 ----
          OBJ_42.setText("@L1TL1R@");
          OBJ_42.setOpaque(false);
          OBJ_42.setName("OBJ_42");
          p_tete_gauche.add(OBJ_42);
          OBJ_42.setBounds(145, 3, 354, OBJ_42.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Statistiques");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Statistiques g\u00e9n\u00e9rales");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Evolution des ratios");
              riSousMenu_bt7.setToolTipText("Evolution des diff\u00e9rents ratios");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Evolution du CA");
              riSousMenu_bt8.setToolTipText("Evolution du chiffre d'affaires");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Part de CA par mois");
              riSousMenu_bt9.setToolTipText("Part de chiffre d'affaires r\u00e9alis\u00e9 par mois");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Evolution des marges");
              riSousMenu_bt10.setToolTipText("Evolution des marges");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Part de marges par mois");
              riSousMenu_bt11.setToolTipText("Part de marges r\u00e9alis\u00e9es par mois");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Evolution des quantit\u00e9s");
              riSousMenu_bt12.setToolTipText("Evolution des quantit\u00e9s");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Part quantit\u00e9s/mois");
              riSousMenu_bt13.setToolTipText("Part des quantit\u00e9s par mois");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Historique sur 10 ans");
              riSousMenu_bt18.setToolTipText("Historique sur dix ans");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Modification de l'\u00e9chelle");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Impression");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("D\u00e9tail par mois"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST4 ========
            {
              SCROLLPANE_LIST4.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST4.setName("SCROLLPANE_LIST4");

              //---- M1 ----
              M1.setFont(new Font("Courier New", Font.PLAIN, 12));
              M1.setName("M1");
              SCROLLPANE_LIST4.setViewportView(M1);
            }
            panel2.add(SCROLLPANE_LIST4);
            SCROLLPANE_LIST4.setBounds(65, 60, 700, 221);

            //======== SCROLLPANE_LIST5 ========
            {
              SCROLLPANE_LIST5.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST5.setName("SCROLLPANE_LIST5");

              //---- L813 ----
              L813.setFont(new Font("Courier New", Font.PLAIN, 12));
              L813.setName("L813");
              SCROLLPANE_LIST5.setViewportView(L813);
            }
            panel2.add(SCROLLPANE_LIST5);
            SCROLLPANE_LIST5.setBounds(145, 295, 620, 61);

            //---- OBJ_86 ----
            OBJ_86.setText("Moyenne");
            OBJ_86.setFont(OBJ_86.getFont().deriveFont(OBJ_86.getFont().getStyle() | Font.BOLD));
            OBJ_86.setName("OBJ_86");
            panel2.add(OBJ_86);
            OBJ_86.setBounds(70, 315, 75, 18);

            //---- OBJ_88 ----
            OBJ_88.setText("Unit\u00e9");
            OBJ_88.setFont(OBJ_88.getFont().deriveFont(OBJ_88.getFont().getStyle() | Font.BOLD));
            OBJ_88.setName("OBJ_88");
            panel2.add(OBJ_88);
            OBJ_88.setBounds(70, 330, 75, 18);

            //---- OBJ_75 ----
            OBJ_75.setText("Total");
            OBJ_75.setFont(OBJ_75.getFont().deriveFont(OBJ_75.getFont().getStyle() | Font.BOLD));
            OBJ_75.setName("OBJ_75");
            panel2.add(OBJ_75);
            OBJ_75.setBounds(70, 300, 75, OBJ_75.getPreferredSize().height);

            //======== SCROLLPANE_LIST6 ========
            {
              SCROLLPANE_LIST6.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST6.setWheelScrollingEnabled(false);
              SCROLLPANE_LIST6.setFocusable(false);
              SCROLLPANE_LIST6.setRequestFocusEnabled(false);
              SCROLLPANE_LIST6.setName("SCROLLPANE_LIST6");

              //---- LIST3 ----
              LIST3.setRequestFocusEnabled(false);
              LIST3.setRowSelectionAllowed(false);
              LIST3.setFocusable(false);
              LIST3.setAutoscrolls(false);
              LIST3.setName("LIST3");
              LIST3.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  M1MouseClicked(e);
                }
              });
              SCROLLPANE_LIST6.setViewportView(LIST3);
            }
            panel2.add(SCROLLPANE_LIST6);
            SCROLLPANE_LIST6.setBounds(145, 40, 620, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 845, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 395, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(45, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_33;
  private XRiTextField INDETB;
  private RiZoneSortie OBJ_42;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST4;
  private XRiTable M1;
  private JScrollPane SCROLLPANE_LIST5;
  private XRiTable L813;
  private JLabel OBJ_86;
  private JLabel OBJ_88;
  private JLabel OBJ_75;
  private JScrollPane SCROLLPANE_LIST6;
  private XRiTable LIST3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
