
package ri.serien.libecranrpg.vtvm.VTVM05FM;

// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class VTVM05FM_G02 extends JDialog {
  
  private Lexical lexique = null;
  
  private iData interpreteurD = null;
  
  private JPanel master = null;
  
  // couleurs des graphes
  
  private Color couleurCa = null;
  private Color couleurMa = new Color(150, 102, 162);
  private Color couleurQt = new Color(9, 172, 166);
  private Color couleurRa = new Color(183, 18, 84);
  
  // chiffre d'affaire première période sur 12 mois
  private String[] ca1 = new String[12];
  // chiffre d'affaire deuxième période sur 12 mois
  private String[] ca2 = new String[12];
  // ratios chiffre d'affaire sur 12 mois
  private String[] ca3 = new String[12];
  // marge première période sur 12 mois
  private String[] ma1 = new String[12];
  // marge deuxième période sur 12 mois
  private String[] ma2 = new String[12];
  // ratios marge sur 12 mois
  private String[] ma3 = new String[12];
  // quantités première période sur 12 mois
  private String[] qu1 = new String[12];
  // quantités deuxième période sur 12 mois
  private String[] qu2 = new String[12];
  // ratios quantités sur 12 mois
  private String[] qu3 = new String[12];
  
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe1 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe2 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe3 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe4 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe5 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe6 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe7 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe8 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe9 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe10 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe11 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe12 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe13 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe14 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe15 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe16 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe17 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe18 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe19 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe20 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe21 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe22 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe23 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe24 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe25 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe26 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe27 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe28 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe29 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe30 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe31 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe32 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe33 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe34 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe35 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe36 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe37 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe38 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe39 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe40 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe41 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe42 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe43 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe44 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe45 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe46 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private RiGraphe graphe47 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  
  private String[] ligneDonnee = new String[12];
  
  public VTVM05FM_G02(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
    tabbedPane1.setTitleAt(0, lexique.TranslationTable(interpreteurD.analyseExpression("@M1@")).trim());
    tabbedPane1.setTitleAt(1, lexique.TranslationTable(interpreteurD.analyseExpression("@M2@")).trim());
    tabbedPane1.setTitleAt(2, lexique.TranslationTable(interpreteurD.analyseExpression("@M3@")).trim());
    tabbedPane1.setTitleAt(3, lexique.TranslationTable(interpreteurD.analyseExpression("@M4@")).trim());
    tabbedPane1.setTitleAt(4, lexique.TranslationTable(interpreteurD.analyseExpression("@M5@")).trim());
    tabbedPane1.setTitleAt(5, lexique.TranslationTable(interpreteurD.analyseExpression("@M6@")).trim());
    tabbedPane1.setTitleAt(6, lexique.TranslationTable(interpreteurD.analyseExpression("@M7@")).trim());
    tabbedPane1.setTitleAt(7, lexique.TranslationTable(interpreteurD.analyseExpression("@M8@")).trim());
    tabbedPane1.setTitleAt(8, lexique.TranslationTable(interpreteurD.analyseExpression("@M9@")).trim());
    tabbedPane1.setTitleAt(9, lexique.TranslationTable(interpreteurD.analyseExpression("@M10@")).trim());
    tabbedPane1.setTitleAt(10, lexique.TranslationTable(interpreteurD.analyseExpression("@M11@")).trim());
    tabbedPane1.setTitleAt(11, lexique.TranslationTable(interpreteurD.analyseExpression("@M12@")).trim());
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // noms des onglets
    for (int i = 0; i < tabbedPane1.getTabCount(); i++) {
      tabbedPane1.setTitleAt(i, lexique.HostFieldGetData("M" + (i + 1)));
    }
    
    // GRAPHE
    
    // Chargement des libellés
    
    String[] libelle = { "Chiffre d'affaires", "Marge", "Quantité", "Chiffre d'affaires", "Marge", "Quantité", "Chiffre d'affaires",
        "Marge", "Quantité" };
    
    String[] libelle1 = { lexique.HostFieldGetData("HLDA81").substring(2, 22), lexique.HostFieldGetData("HLDA81").substring(28, 48) };
    
    String[] libelle2 = { "Sur chiffre d'affaires", "Sur marge", "Sur quantité" };
    
    // Chargement des données
    
    // on charge toutes les lignes de la LD
    for (int i = 0; i < ligneDonnee.length; i++) {
      ligneDonnee[i] = lexique.HostFieldGetData("L8" + ((i + 1)));
    }
    
    // On crée les tables de données qui nous intéresse depuis l'EBCDIC
    for (int i = 0; i < ca1.length; i++) {
      ca1[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(0, 9).trim(), 0);
    }
    for (int i = 0; i < ca2.length; i++) {
      ca2[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(26, 35).trim(), 0);
    }
    for (int i = 0; i < ca3.length; i++) {
      ca3[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(51, 57).trim(), 2);
    }
    for (int i = 0; i < ma1.length; i++) {
      ma1[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(9, 16).trim(), 2);
    }
    for (int i = 0; i < ma2.length; i++) {
      ma2[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(35, 41).trim(), 2);
    }
    for (int i = 0; i < ma3.length; i++) {
      ma3[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(57, 63).trim(), 2);
    }
    for (int i = 0; i < qu1.length; i++) {
      qu1[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(15, 24).trim(), 0);
    }
    for (int i = 0; i < qu2.length; i++) {
      qu2[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(41, 50).trim(), 0);
    }
    for (int i = 0; i < qu3.length; i++) {
      qu3[i] = lexique.HostFieldGetNumericString(ligneDonnee[i].substring(63, 70).trim(), 0);
    }
    
    // CONSTITUTION DES ONGLETS
    // On sélectionne les données utiles pour chaque graphe (1 table = 1 graphe)
    
    // Graphes 1ère page
    String[] donneeG101 = new String[2];
    String[] donneeG201 = new String[2];
    String[] donneeG301 = new String[2];
    String[] donneeG401 = new String[3];
    
    chargeTable(donneeG101, 1, 1);
    chargeTable(donneeG201, 2, 1);
    chargeTable(donneeG301, 3, 1);
    chargeTable(donneeG401, 4, 1);
    
    // Graphes 2ème page
    String[] donneeG102 = new String[2];
    String[] donneeG202 = new String[2];
    String[] donneeG302 = new String[2];
    String[] donneeG402 = new String[3];
    
    chargeTable(donneeG102, 1, 2);
    chargeTable(donneeG202, 2, 2);
    chargeTable(donneeG302, 3, 2);
    chargeTable(donneeG402, 4, 2);
    
    // Graphes 3ème page
    String[] donneeG103 = new String[2];
    String[] donneeG203 = new String[2];
    String[] donneeG303 = new String[2];
    String[] donneeG403 = new String[3];
    
    chargeTable(donneeG103, 1, 3);
    chargeTable(donneeG203, 2, 3);
    chargeTable(donneeG303, 3, 3);
    chargeTable(donneeG403, 4, 3);
    
    // Graphes 4ème page
    String[] donneeG104 = new String[2];
    String[] donneeG204 = new String[2];
    String[] donneeG304 = new String[2];
    String[] donneeG404 = new String[3];
    
    chargeTable(donneeG104, 1, 4);
    chargeTable(donneeG204, 2, 4);
    chargeTable(donneeG304, 3, 4);
    chargeTable(donneeG404, 4, 4);
    
    // Graphes 5ème page
    String[] donneeG105 = new String[2];
    String[] donneeG205 = new String[2];
    String[] donneeG305 = new String[2];
    String[] donneeG405 = new String[3];
    
    chargeTable(donneeG105, 1, 5);
    chargeTable(donneeG205, 2, 5);
    chargeTable(donneeG305, 3, 5);
    chargeTable(donneeG405, 4, 5);
    
    // Graphes 6ème page
    String[] donneeG106 = new String[2];
    String[] donneeG206 = new String[2];
    String[] donneeG306 = new String[2];
    String[] donneeG406 = new String[3];
    
    chargeTable(donneeG106, 1, 6);
    chargeTable(donneeG206, 2, 6);
    chargeTable(donneeG306, 3, 6);
    chargeTable(donneeG406, 4, 6);
    
    // Graphes 7ème page
    String[] donneeG107 = new String[2];
    String[] donneeG207 = new String[2];
    String[] donneeG307 = new String[2];
    String[] donneeG407 = new String[3];
    
    chargeTable(donneeG107, 1, 7);
    chargeTable(donneeG207, 2, 7);
    chargeTable(donneeG307, 3, 7);
    chargeTable(donneeG407, 4, 7);
    
    // Graphes 8ème page
    String[] donneeG108 = new String[2];
    String[] donneeG208 = new String[2];
    String[] donneeG308 = new String[2];
    String[] donneeG408 = new String[3];
    
    chargeTable(donneeG108, 1, 8);
    chargeTable(donneeG208, 2, 8);
    chargeTable(donneeG308, 3, 8);
    chargeTable(donneeG408, 4, 8);
    
    // Graphes 9ème page
    String[] donneeG109 = new String[2];
    String[] donneeG209 = new String[2];
    String[] donneeG309 = new String[2];
    String[] donneeG409 = new String[3];
    
    chargeTable(donneeG109, 1, 9);
    chargeTable(donneeG209, 2, 9);
    chargeTable(donneeG309, 3, 9);
    chargeTable(donneeG409, 4, 9);
    
    // Graphes 10ème page
    String[] donneeG110 = new String[2];
    String[] donneeG210 = new String[2];
    String[] donneeG310 = new String[2];
    String[] donneeG410 = new String[3];
    
    chargeTable(donneeG110, 1, 10);
    chargeTable(donneeG210, 2, 10);
    chargeTable(donneeG310, 3, 10);
    chargeTable(donneeG410, 4, 10);
    
    // Graphes 11ème page
    String[] donneeG111 = new String[2];
    String[] donneeG211 = new String[2];
    String[] donneeG311 = new String[2];
    String[] donneeG411 = new String[3];
    
    chargeTable(donneeG111, 1, 11);
    chargeTable(donneeG211, 2, 11);
    chargeTable(donneeG311, 3, 11);
    chargeTable(donneeG411, 4, 11);
    
    // Graphes 12ème page
    String[] donneeG112 = new String[2];
    String[] donneeG212 = new String[2];
    String[] donneeG312 = new String[2];
    String[] donneeG412 = new String[3];
    
    chargeTable(donneeG112, 1, 12);
    chargeTable(donneeG212, 2, 12);
    chargeTable(donneeG312, 3, 12);
    chargeTable(donneeG412, 4, 12);
    
    // Préparation des données
    
    // Onglet 1
    preparationOnglet(graphe, l_graphe, donneeG101, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe1, l_graphe1, donneeG201, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe2, l_graphe2, donneeG301, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe3, l_graphe3, donneeG401, libelle2, "Ratios", couleurRa);
    
    // Onglet 2
    preparationOnglet(graphe4, l_graphe4, donneeG102, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe5, l_graphe5, donneeG202, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe6, l_graphe6, donneeG302, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe7, l_graphe7, donneeG402, libelle2, "Ratios", couleurRa);
    
    // Onglet 3
    preparationOnglet(graphe8, l_graphe8, donneeG103, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe9, l_graphe9, donneeG203, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe10, l_graphe10, donneeG303, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe11, l_graphe11, donneeG403, libelle2, "Ratios", couleurRa);
    
    // Onglet 4
    preparationOnglet(graphe12, l_graphe12, donneeG104, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe13, l_graphe13, donneeG204, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe14, l_graphe14, donneeG304, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe15, l_graphe15, donneeG404, libelle2, "Ratios", couleurRa);
    
    // Onglet 5
    preparationOnglet(graphe16, l_graphe16, donneeG105, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe17, l_graphe17, donneeG205, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe18, l_graphe18, donneeG305, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe19, l_graphe19, donneeG405, libelle2, "Ratios", couleurRa);
    
    // Onglet 6
    preparationOnglet(graphe20, l_graphe20, donneeG106, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe21, l_graphe21, donneeG206, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe22, l_graphe22, donneeG306, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe23, l_graphe23, donneeG406, libelle2, "Ratios", couleurRa);
    
    // Onglet 7
    preparationOnglet(graphe24, l_graphe24, donneeG107, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe25, l_graphe25, donneeG207, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe26, l_graphe26, donneeG307, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe27, l_graphe27, donneeG407, libelle2, "Ratios", couleurRa);
    
    // Onglet 8
    preparationOnglet(graphe28, l_graphe28, donneeG108, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe29, l_graphe29, donneeG208, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe30, l_graphe30, donneeG308, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe31, l_graphe31, donneeG408, libelle2, "Ratios", couleurRa);
    
    // Onglet 9
    preparationOnglet(graphe32, l_graphe32, donneeG109, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe33, l_graphe33, donneeG209, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe34, l_graphe34, donneeG309, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe35, l_graphe35, donneeG409, libelle2, "Ratios", couleurRa);
    
    // Onglet 10
    preparationOnglet(graphe36, l_graphe36, donneeG110, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe37, l_graphe37, donneeG210, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe38, l_graphe38, donneeG310, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe39, l_graphe39, donneeG410, libelle2, "Ratios", couleurRa);
    
    // Onglet 11
    preparationOnglet(graphe40, l_graphe40, donneeG111, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe41, l_graphe41, donneeG211, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe42, l_graphe42, donneeG311, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe43, l_graphe43, donneeG411, libelle2, "Ratios", couleurRa);
    
    // Onglet 12
    preparationOnglet(graphe44, l_graphe44, donneeG112, libelle1, "Chiffre d'affaires", couleurCa);
    preparationOnglet(graphe45, l_graphe45, donneeG212, libelle1, "Marge", couleurMa);
    preparationOnglet(graphe46, l_graphe46, donneeG312, libelle1, "Quantité", couleurQt);
    preparationOnglet(graphe47, l_graphe47, donneeG412, libelle2, "Ratios", couleurRa);
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques " + lexique.HostFieldGetData("L1I1")));
  }
  
  public Object[][] getDonnees(String[] tableDonnee, String[] tableLibelle) {
    
    Object[][] data = new Object[tableLibelle.length][tableDonnee.length];
    
    for (int i = 0; i < tableLibelle.length; i++) {
      data[i][0] = tableLibelle[i];
      tableDonnee[i] = tableDonnee[i].replaceAll("\\s", "0");
      data[i][1] = Double.parseDouble(tableDonnee[i]);
    }
    
    return data;
  }
  
  public void chargeTable(String[] tableEntree, int type, int onglet) {
    
    if (type == 1) {
      
      tableEntree[0] = ca1[onglet - 1];
      tableEntree[1] = ca2[onglet - 1];
      
    }
    
    if (type == 2) {
      tableEntree[0] = ma1[onglet - 1];
      tableEntree[1] = ma2[onglet - 1];
      
    }
    
    if (type == 3) {
      tableEntree[0] = qu1[onglet - 1];
      tableEntree[1] = qu2[onglet - 1];
      
    }
    
    if (type == 4) {
      tableEntree[0] = ca3[onglet - 1];
      tableEntree[1] = ma3[onglet - 1];
      tableEntree[2] = qu3[onglet - 1];
      
    }
  }
  
  public void preparationOnglet(RiGraphe graphe, JLabel icone, String[] donnees, String[] libelle, String titre, Color couleur) {
    if (couleur != null) {
      graphe.setGraphColor(couleur);
    }
    graphe.setDonnee(getDonnees(donnees, libelle), "", false);
    graphe.getGraphe(titre, false);
    icone.setIcon(graphe.getPicture(icone.getWidth(), icone.getHeight()));
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    panel2 = new JPanel();
    tabbedPane1 = new JTabbedPane();
    panel3 = new JPanel();
    l_graphe = new JLabel();
    l_graphe1 = new JLabel();
    l_graphe2 = new JLabel();
    l_graphe3 = new JLabel();
    panel4 = new JPanel();
    l_graphe4 = new JLabel();
    l_graphe5 = new JLabel();
    l_graphe6 = new JLabel();
    l_graphe7 = new JLabel();
    panel5 = new JPanel();
    l_graphe8 = new JLabel();
    l_graphe9 = new JLabel();
    l_graphe10 = new JLabel();
    l_graphe11 = new JLabel();
    panel6 = new JPanel();
    l_graphe12 = new JLabel();
    l_graphe13 = new JLabel();
    l_graphe14 = new JLabel();
    l_graphe15 = new JLabel();
    panel7 = new JPanel();
    l_graphe16 = new JLabel();
    l_graphe17 = new JLabel();
    l_graphe18 = new JLabel();
    l_graphe19 = new JLabel();
    panel8 = new JPanel();
    l_graphe20 = new JLabel();
    l_graphe21 = new JLabel();
    l_graphe22 = new JLabel();
    l_graphe23 = new JLabel();
    panel9 = new JPanel();
    l_graphe24 = new JLabel();
    l_graphe25 = new JLabel();
    l_graphe26 = new JLabel();
    l_graphe27 = new JLabel();
    panel10 = new JPanel();
    l_graphe28 = new JLabel();
    l_graphe29 = new JLabel();
    l_graphe30 = new JLabel();
    l_graphe31 = new JLabel();
    panel11 = new JPanel();
    l_graphe32 = new JLabel();
    l_graphe33 = new JLabel();
    l_graphe34 = new JLabel();
    l_graphe35 = new JLabel();
    panel12 = new JPanel();
    l_graphe36 = new JLabel();
    l_graphe37 = new JLabel();
    l_graphe38 = new JLabel();
    l_graphe39 = new JLabel();
    panel13 = new JPanel();
    l_graphe40 = new JLabel();
    l_graphe41 = new JLabel();
    l_graphe42 = new JLabel();
    l_graphe43 = new JLabel();
    panel14 = new JPanel();
    l_graphe44 = new JLabel();
    l_graphe45 = new JLabel();
    l_graphe46 = new JLabel();
    l_graphe47 = new JLabel();
    panel1 = new JPanel();
    OBJ_10 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 600));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel2 ========
    {
      panel2.setBackground(new Color(238, 238, 210));
      panel2.setName("panel2");
      panel2.setLayout(new BorderLayout());
      
      // ======== tabbedPane1 ========
      {
        tabbedPane1.setBackground(new Color(238, 238, 210));
        tabbedPane1.setName("tabbedPane1");
        
        // ======== panel3 ========
        {
          panel3.setBackground(new Color(238, 238, 210));
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- l_graphe ----
          l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe.setComponentPopupMenu(null);
          l_graphe.setBackground(new Color(214, 217, 223));
          l_graphe.setPreferredSize(new Dimension(800, 650));
          l_graphe.setName("l_graphe");
          panel3.add(l_graphe);
          l_graphe.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe1 ----
          l_graphe1.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe1.setComponentPopupMenu(null);
          l_graphe1.setBackground(new Color(214, 217, 223));
          l_graphe1.setPreferredSize(new Dimension(800, 650));
          l_graphe1.setName("l_graphe1");
          panel3.add(l_graphe1);
          l_graphe1.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe2 ----
          l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe2.setComponentPopupMenu(null);
          l_graphe2.setBackground(new Color(214, 217, 223));
          l_graphe2.setPreferredSize(new Dimension(800, 650));
          l_graphe2.setName("l_graphe2");
          panel3.add(l_graphe2);
          l_graphe2.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe3 ----
          l_graphe3.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe3.setComponentPopupMenu(null);
          l_graphe3.setBackground(new Color(214, 217, 223));
          l_graphe3.setPreferredSize(new Dimension(800, 650));
          l_graphe3.setName("l_graphe3");
          panel3.add(l_graphe3);
          l_graphe3.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M1@", panel3);
        
        // ======== panel4 ========
        {
          panel4.setOpaque(false);
          panel4.setPreferredSize(new Dimension(1095, 885));
          panel4.setMinimumSize(new Dimension(1095, 885));
          panel4.setName("panel4");
          panel4.setLayout(null);
          
          // ---- l_graphe4 ----
          l_graphe4.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe4.setComponentPopupMenu(null);
          l_graphe4.setBackground(new Color(214, 217, 223));
          l_graphe4.setPreferredSize(new Dimension(800, 650));
          l_graphe4.setName("l_graphe4");
          panel4.add(l_graphe4);
          l_graphe4.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe5 ----
          l_graphe5.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe5.setComponentPopupMenu(null);
          l_graphe5.setBackground(new Color(214, 217, 223));
          l_graphe5.setPreferredSize(new Dimension(800, 650));
          l_graphe5.setName("l_graphe5");
          panel4.add(l_graphe5);
          l_graphe5.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe6 ----
          l_graphe6.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe6.setComponentPopupMenu(null);
          l_graphe6.setBackground(new Color(214, 217, 223));
          l_graphe6.setPreferredSize(new Dimension(800, 650));
          l_graphe6.setName("l_graphe6");
          panel4.add(l_graphe6);
          l_graphe6.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe7 ----
          l_graphe7.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe7.setComponentPopupMenu(null);
          l_graphe7.setBackground(new Color(214, 217, 223));
          l_graphe7.setPreferredSize(new Dimension(800, 650));
          l_graphe7.setName("l_graphe7");
          panel4.add(l_graphe7);
          l_graphe7.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel4.getComponentCount(); i++) {
              Rectangle bounds = panel4.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4.setMinimumSize(preferredSize);
            panel4.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M2@", panel4);
        
        // ======== panel5 ========
        {
          panel5.setOpaque(false);
          panel5.setName("panel5");
          panel5.setLayout(null);
          
          // ---- l_graphe8 ----
          l_graphe8.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe8.setComponentPopupMenu(null);
          l_graphe8.setBackground(new Color(214, 217, 223));
          l_graphe8.setPreferredSize(new Dimension(800, 650));
          l_graphe8.setName("l_graphe8");
          panel5.add(l_graphe8);
          l_graphe8.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe9 ----
          l_graphe9.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe9.setComponentPopupMenu(null);
          l_graphe9.setBackground(new Color(214, 217, 223));
          l_graphe9.setPreferredSize(new Dimension(800, 650));
          l_graphe9.setName("l_graphe9");
          panel5.add(l_graphe9);
          l_graphe9.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe10 ----
          l_graphe10.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe10.setComponentPopupMenu(null);
          l_graphe10.setBackground(new Color(214, 217, 223));
          l_graphe10.setPreferredSize(new Dimension(800, 650));
          l_graphe10.setName("l_graphe10");
          panel5.add(l_graphe10);
          l_graphe10.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe11 ----
          l_graphe11.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe11.setComponentPopupMenu(null);
          l_graphe11.setBackground(new Color(214, 217, 223));
          l_graphe11.setPreferredSize(new Dimension(800, 650));
          l_graphe11.setName("l_graphe11");
          panel5.add(l_graphe11);
          l_graphe11.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel5.getComponentCount(); i++) {
              Rectangle bounds = panel5.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel5.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel5.setMinimumSize(preferredSize);
            panel5.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M3@", panel5);
        
        // ======== panel6 ========
        {
          panel6.setOpaque(false);
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- l_graphe12 ----
          l_graphe12.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe12.setComponentPopupMenu(null);
          l_graphe12.setBackground(new Color(214, 217, 223));
          l_graphe12.setPreferredSize(new Dimension(800, 650));
          l_graphe12.setName("l_graphe12");
          panel6.add(l_graphe12);
          l_graphe12.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe13 ----
          l_graphe13.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe13.setComponentPopupMenu(null);
          l_graphe13.setBackground(new Color(214, 217, 223));
          l_graphe13.setPreferredSize(new Dimension(800, 650));
          l_graphe13.setName("l_graphe13");
          panel6.add(l_graphe13);
          l_graphe13.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe14 ----
          l_graphe14.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe14.setComponentPopupMenu(null);
          l_graphe14.setBackground(new Color(214, 217, 223));
          l_graphe14.setPreferredSize(new Dimension(800, 650));
          l_graphe14.setName("l_graphe14");
          panel6.add(l_graphe14);
          l_graphe14.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe15 ----
          l_graphe15.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe15.setComponentPopupMenu(null);
          l_graphe15.setBackground(new Color(214, 217, 223));
          l_graphe15.setPreferredSize(new Dimension(800, 650));
          l_graphe15.setName("l_graphe15");
          panel6.add(l_graphe15);
          l_graphe15.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M4@", panel6);
        
        // ======== panel7 ========
        {
          panel7.setOpaque(false);
          panel7.setName("panel7");
          panel7.setLayout(null);
          
          // ---- l_graphe16 ----
          l_graphe16.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe16.setComponentPopupMenu(null);
          l_graphe16.setBackground(new Color(214, 217, 223));
          l_graphe16.setPreferredSize(new Dimension(800, 650));
          l_graphe16.setName("l_graphe16");
          panel7.add(l_graphe16);
          l_graphe16.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe17 ----
          l_graphe17.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe17.setComponentPopupMenu(null);
          l_graphe17.setBackground(new Color(214, 217, 223));
          l_graphe17.setPreferredSize(new Dimension(800, 650));
          l_graphe17.setName("l_graphe17");
          panel7.add(l_graphe17);
          l_graphe17.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe18 ----
          l_graphe18.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe18.setComponentPopupMenu(null);
          l_graphe18.setBackground(new Color(214, 217, 223));
          l_graphe18.setPreferredSize(new Dimension(800, 650));
          l_graphe18.setName("l_graphe18");
          panel7.add(l_graphe18);
          l_graphe18.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe19 ----
          l_graphe19.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe19.setComponentPopupMenu(null);
          l_graphe19.setBackground(new Color(214, 217, 223));
          l_graphe19.setPreferredSize(new Dimension(800, 650));
          l_graphe19.setName("l_graphe19");
          panel7.add(l_graphe19);
          l_graphe19.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel7.getComponentCount(); i++) {
              Rectangle bounds = panel7.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel7.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel7.setMinimumSize(preferredSize);
            panel7.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M5@", panel7);
        
        // ======== panel8 ========
        {
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);
          
          // ---- l_graphe20 ----
          l_graphe20.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe20.setComponentPopupMenu(null);
          l_graphe20.setBackground(new Color(214, 217, 223));
          l_graphe20.setPreferredSize(new Dimension(800, 650));
          l_graphe20.setName("l_graphe20");
          panel8.add(l_graphe20);
          l_graphe20.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe21 ----
          l_graphe21.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe21.setComponentPopupMenu(null);
          l_graphe21.setBackground(new Color(214, 217, 223));
          l_graphe21.setPreferredSize(new Dimension(800, 650));
          l_graphe21.setName("l_graphe21");
          panel8.add(l_graphe21);
          l_graphe21.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe22 ----
          l_graphe22.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe22.setComponentPopupMenu(null);
          l_graphe22.setBackground(new Color(214, 217, 223));
          l_graphe22.setPreferredSize(new Dimension(800, 650));
          l_graphe22.setName("l_graphe22");
          panel8.add(l_graphe22);
          l_graphe22.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe23 ----
          l_graphe23.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe23.setComponentPopupMenu(null);
          l_graphe23.setBackground(new Color(214, 217, 223));
          l_graphe23.setPreferredSize(new Dimension(800, 650));
          l_graphe23.setName("l_graphe23");
          panel8.add(l_graphe23);
          l_graphe23.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M6@", panel8);
        
        // ======== panel9 ========
        {
          panel9.setOpaque(false);
          panel9.setName("panel9");
          panel9.setLayout(null);
          
          // ---- l_graphe24 ----
          l_graphe24.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe24.setComponentPopupMenu(null);
          l_graphe24.setBackground(new Color(214, 217, 223));
          l_graphe24.setPreferredSize(new Dimension(800, 650));
          l_graphe24.setName("l_graphe24");
          panel9.add(l_graphe24);
          l_graphe24.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe25 ----
          l_graphe25.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe25.setComponentPopupMenu(null);
          l_graphe25.setBackground(new Color(214, 217, 223));
          l_graphe25.setPreferredSize(new Dimension(800, 650));
          l_graphe25.setName("l_graphe25");
          panel9.add(l_graphe25);
          l_graphe25.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe26 ----
          l_graphe26.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe26.setComponentPopupMenu(null);
          l_graphe26.setBackground(new Color(214, 217, 223));
          l_graphe26.setPreferredSize(new Dimension(800, 650));
          l_graphe26.setName("l_graphe26");
          panel9.add(l_graphe26);
          l_graphe26.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe27 ----
          l_graphe27.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe27.setComponentPopupMenu(null);
          l_graphe27.setBackground(new Color(214, 217, 223));
          l_graphe27.setPreferredSize(new Dimension(800, 650));
          l_graphe27.setName("l_graphe27");
          panel9.add(l_graphe27);
          l_graphe27.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel9.getComponentCount(); i++) {
              Rectangle bounds = panel9.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel9.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel9.setMinimumSize(preferredSize);
            panel9.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M7@", panel9);
        
        // ======== panel10 ========
        {
          panel10.setOpaque(false);
          panel10.setName("panel10");
          panel10.setLayout(null);
          
          // ---- l_graphe28 ----
          l_graphe28.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe28.setComponentPopupMenu(null);
          l_graphe28.setBackground(new Color(214, 217, 223));
          l_graphe28.setPreferredSize(new Dimension(800, 650));
          l_graphe28.setName("l_graphe28");
          panel10.add(l_graphe28);
          l_graphe28.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe29 ----
          l_graphe29.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe29.setComponentPopupMenu(null);
          l_graphe29.setBackground(new Color(214, 217, 223));
          l_graphe29.setPreferredSize(new Dimension(800, 650));
          l_graphe29.setName("l_graphe29");
          panel10.add(l_graphe29);
          l_graphe29.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe30 ----
          l_graphe30.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe30.setComponentPopupMenu(null);
          l_graphe30.setBackground(new Color(214, 217, 223));
          l_graphe30.setPreferredSize(new Dimension(800, 650));
          l_graphe30.setName("l_graphe30");
          panel10.add(l_graphe30);
          l_graphe30.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe31 ----
          l_graphe31.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe31.setComponentPopupMenu(null);
          l_graphe31.setBackground(new Color(214, 217, 223));
          l_graphe31.setPreferredSize(new Dimension(800, 650));
          l_graphe31.setName("l_graphe31");
          panel10.add(l_graphe31);
          l_graphe31.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel10.getComponentCount(); i++) {
              Rectangle bounds = panel10.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel10.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel10.setMinimumSize(preferredSize);
            panel10.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M8@", panel10);
        
        // ======== panel11 ========
        {
          panel11.setOpaque(false);
          panel11.setName("panel11");
          panel11.setLayout(null);
          
          // ---- l_graphe32 ----
          l_graphe32.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe32.setComponentPopupMenu(null);
          l_graphe32.setBackground(new Color(214, 217, 223));
          l_graphe32.setPreferredSize(new Dimension(800, 650));
          l_graphe32.setName("l_graphe32");
          panel11.add(l_graphe32);
          l_graphe32.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe33 ----
          l_graphe33.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe33.setComponentPopupMenu(null);
          l_graphe33.setBackground(new Color(214, 217, 223));
          l_graphe33.setPreferredSize(new Dimension(800, 650));
          l_graphe33.setName("l_graphe33");
          panel11.add(l_graphe33);
          l_graphe33.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe34 ----
          l_graphe34.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe34.setComponentPopupMenu(null);
          l_graphe34.setBackground(new Color(214, 217, 223));
          l_graphe34.setPreferredSize(new Dimension(800, 650));
          l_graphe34.setName("l_graphe34");
          panel11.add(l_graphe34);
          l_graphe34.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe35 ----
          l_graphe35.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe35.setComponentPopupMenu(null);
          l_graphe35.setBackground(new Color(214, 217, 223));
          l_graphe35.setPreferredSize(new Dimension(800, 650));
          l_graphe35.setName("l_graphe35");
          panel11.add(l_graphe35);
          l_graphe35.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel11.getComponentCount(); i++) {
              Rectangle bounds = panel11.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel11.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel11.setMinimumSize(preferredSize);
            panel11.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M9@", panel11);
        
        // ======== panel12 ========
        {
          panel12.setOpaque(false);
          panel12.setName("panel12");
          panel12.setLayout(null);
          
          // ---- l_graphe36 ----
          l_graphe36.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe36.setComponentPopupMenu(null);
          l_graphe36.setBackground(new Color(214, 217, 223));
          l_graphe36.setPreferredSize(new Dimension(800, 650));
          l_graphe36.setName("l_graphe36");
          panel12.add(l_graphe36);
          l_graphe36.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe37 ----
          l_graphe37.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe37.setComponentPopupMenu(null);
          l_graphe37.setBackground(new Color(214, 217, 223));
          l_graphe37.setPreferredSize(new Dimension(800, 650));
          l_graphe37.setName("l_graphe37");
          panel12.add(l_graphe37);
          l_graphe37.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe38 ----
          l_graphe38.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe38.setComponentPopupMenu(null);
          l_graphe38.setBackground(new Color(214, 217, 223));
          l_graphe38.setPreferredSize(new Dimension(800, 650));
          l_graphe38.setName("l_graphe38");
          panel12.add(l_graphe38);
          l_graphe38.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe39 ----
          l_graphe39.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe39.setComponentPopupMenu(null);
          l_graphe39.setBackground(new Color(214, 217, 223));
          l_graphe39.setPreferredSize(new Dimension(800, 650));
          l_graphe39.setName("l_graphe39");
          panel12.add(l_graphe39);
          l_graphe39.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel12.getComponentCount(); i++) {
              Rectangle bounds = panel12.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel12.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel12.setMinimumSize(preferredSize);
            panel12.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M10@", panel12);
        
        // ======== panel13 ========
        {
          panel13.setOpaque(false);
          panel13.setName("panel13");
          panel13.setLayout(null);
          
          // ---- l_graphe40 ----
          l_graphe40.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe40.setComponentPopupMenu(null);
          l_graphe40.setBackground(new Color(214, 217, 223));
          l_graphe40.setPreferredSize(new Dimension(800, 650));
          l_graphe40.setName("l_graphe40");
          panel13.add(l_graphe40);
          l_graphe40.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe41 ----
          l_graphe41.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe41.setComponentPopupMenu(null);
          l_graphe41.setBackground(new Color(214, 217, 223));
          l_graphe41.setPreferredSize(new Dimension(800, 650));
          l_graphe41.setName("l_graphe41");
          panel13.add(l_graphe41);
          l_graphe41.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe42 ----
          l_graphe42.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe42.setComponentPopupMenu(null);
          l_graphe42.setBackground(new Color(214, 217, 223));
          l_graphe42.setPreferredSize(new Dimension(800, 650));
          l_graphe42.setName("l_graphe42");
          panel13.add(l_graphe42);
          l_graphe42.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe43 ----
          l_graphe43.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe43.setComponentPopupMenu(null);
          l_graphe43.setBackground(new Color(214, 217, 223));
          l_graphe43.setPreferredSize(new Dimension(800, 650));
          l_graphe43.setName("l_graphe43");
          panel13.add(l_graphe43);
          l_graphe43.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel13.getComponentCount(); i++) {
              Rectangle bounds = panel13.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel13.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel13.setMinimumSize(preferredSize);
            panel13.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M11@", panel13);
        
        // ======== panel14 ========
        {
          panel14.setOpaque(false);
          panel14.setName("panel14");
          panel14.setLayout(null);
          
          // ---- l_graphe44 ----
          l_graphe44.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe44.setComponentPopupMenu(null);
          l_graphe44.setBackground(new Color(214, 217, 223));
          l_graphe44.setPreferredSize(new Dimension(800, 650));
          l_graphe44.setName("l_graphe44");
          panel14.add(l_graphe44);
          l_graphe44.setBounds(10, 20, 260, 455);
          
          // ---- l_graphe45 ----
          l_graphe45.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe45.setComponentPopupMenu(null);
          l_graphe45.setBackground(new Color(214, 217, 223));
          l_graphe45.setPreferredSize(new Dimension(800, 650));
          l_graphe45.setName("l_graphe45");
          panel14.add(l_graphe45);
          l_graphe45.setBounds(278, 20, 260, 455);
          
          // ---- l_graphe46 ----
          l_graphe46.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe46.setComponentPopupMenu(null);
          l_graphe46.setBackground(new Color(214, 217, 223));
          l_graphe46.setPreferredSize(new Dimension(800, 650));
          l_graphe46.setName("l_graphe46");
          panel14.add(l_graphe46);
          l_graphe46.setBounds(546, 20, 260, 455);
          
          // ---- l_graphe47 ----
          l_graphe47.setHorizontalAlignment(SwingConstants.CENTER);
          l_graphe47.setComponentPopupMenu(null);
          l_graphe47.setBackground(new Color(214, 217, 223));
          l_graphe47.setPreferredSize(new Dimension(800, 650));
          l_graphe47.setName("l_graphe47");
          panel14.add(l_graphe47);
          l_graphe47.setBounds(814, 20, 260, 455);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel14.getComponentCount(); i++) {
              Rectangle bounds = panel14.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel14.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel14.setMinimumSize(preferredSize);
            panel14.setPreferredSize(preferredSize);
          }
        }
        tabbedPane1.addTab("@M12@", panel14);
      }
      panel2.add(tabbedPane1, BorderLayout.CENTER);
      
      // ======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setMinimumSize(new Dimension(1080, 50));
        panel1.setMaximumSize(new Dimension(1080, 50));
        panel1.setPreferredSize(new Dimension(1080, 50));
        panel1.setName("panel1");
        
        // ---- OBJ_10 ----
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setToolTipText("Retour");
        OBJ_10.setText("Retour");
        OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        
        GroupLayout panel1Layout = new GroupLayout(panel1);
        panel1.setLayout(panel1Layout);
        panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup()
            .addGap(945, 945, 945).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)));
        panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup().addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5)
            .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)));
      }
      panel2.add(panel1, BorderLayout.SOUTH);
    }
    contentPane.add(panel2, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel panel2;
  private JTabbedPane tabbedPane1;
  private JPanel panel3;
  private JLabel l_graphe;
  private JLabel l_graphe1;
  private JLabel l_graphe2;
  private JLabel l_graphe3;
  private JPanel panel4;
  private JLabel l_graphe4;
  private JLabel l_graphe5;
  private JLabel l_graphe6;
  private JLabel l_graphe7;
  private JPanel panel5;
  private JLabel l_graphe8;
  private JLabel l_graphe9;
  private JLabel l_graphe10;
  private JLabel l_graphe11;
  private JPanel panel6;
  private JLabel l_graphe12;
  private JLabel l_graphe13;
  private JLabel l_graphe14;
  private JLabel l_graphe15;
  private JPanel panel7;
  private JLabel l_graphe16;
  private JLabel l_graphe17;
  private JLabel l_graphe18;
  private JLabel l_graphe19;
  private JPanel panel8;
  private JLabel l_graphe20;
  private JLabel l_graphe21;
  private JLabel l_graphe22;
  private JLabel l_graphe23;
  private JPanel panel9;
  private JLabel l_graphe24;
  private JLabel l_graphe25;
  private JLabel l_graphe26;
  private JLabel l_graphe27;
  private JPanel panel10;
  private JLabel l_graphe28;
  private JLabel l_graphe29;
  private JLabel l_graphe30;
  private JLabel l_graphe31;
  private JPanel panel11;
  private JLabel l_graphe32;
  private JLabel l_graphe33;
  private JLabel l_graphe34;
  private JLabel l_graphe35;
  private JPanel panel12;
  private JLabel l_graphe36;
  private JLabel l_graphe37;
  private JLabel l_graphe38;
  private JLabel l_graphe39;
  private JPanel panel13;
  private JLabel l_graphe40;
  private JLabel l_graphe41;
  private JLabel l_graphe42;
  private JLabel l_graphe43;
  private JPanel panel14;
  private JLabel l_graphe44;
  private JLabel l_graphe45;
  private JLabel l_graphe46;
  private JLabel l_graphe47;
  private JPanel panel1;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
