
package ri.serien.libecranrpg.sgvs.SGVS15FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGVS15FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGVS15FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    UTRI.setValeursSelection("X", " ");
    WRED.setValeursSelection("OUI", "NON");
    WTOU.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    WETB.setVisible(lexique.isPresent("WETB"));
    NUMFIN.setEnabled(lexique.isPresent("NUMFIN"));
    NUMDEB.setEnabled(lexique.isPresent("NUMDEB"));
    // WRED.setVisible( lexique.isPresent("WRED"));
    // WRED.setEnabled( lexique.isPresent("WRED"));
    // WRED.setSelected(lexique.HostFieldGetData("WRED").equalsIgnoreCase("OUI"));
    // UTRI.setVisible( lexique.isPresent("UTRI"));
    // UTRI.setSelected(lexique.HostFieldGetData("UTRI").equalsIgnoreCase("X"));
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").equalsIgnoreCase("**"));
    OBJ_27.setVisible(lexique.isPresent("DGNOM"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@&&G_TITRE@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WRED.isSelected())
    // lexique.HostFieldPutData("WRED", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WRED", 0, "NON");
    // if (UTRI.isSelected())
    // lexique.HostFieldPutData("UTRI", 0, "X");
    // else
    // lexique.HostFieldPutData("UTRI", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riBoutonRecherche1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    l_LOCTP = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_27 = new JXTitledSeparator();
    OBJ_32 = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    WETB = new XRiTextField();
    riBoutonRecherche1 = new SNBoutonRecherche();
    OBJ_23 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_28 = new JLabel();
    OBJ_31 = new JLabel();
    NUMDEB = new XRiTextField();
    NUMFIN = new XRiTextField();
    WTOU = new XRiCheckBox();
    WRED = new XRiCheckBox();
    OBJ_22 = new JXTitledSeparator();
    UTRI = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    P_PnlOpts = new JPanel();
    OBJ_50 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- l_LOCTP ----
          l_LOCTP.setText("@LOCTP@");
          l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
          l_LOCTP.setPreferredSize(new Dimension(120, 20));
          l_LOCTP.setHorizontalTextPosition(SwingConstants.LEADING);
          l_LOCTP.setHorizontalAlignment(SwingConstants.RIGHT);
          l_LOCTP.setName("l_LOCTP");
          p_tete_droite.add(l_LOCTP);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(700, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setTitle("Etablissement s\u00e9lectionn\u00e9");
          OBJ_27.setName("OBJ_27");
          p_contenu.add(OBJ_27);
          OBJ_27.setBounds(33, 23, 625, OBJ_27.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("@DGNOM@");
          OBJ_32.setName("OBJ_32");
          p_contenu.add(OBJ_32);
          OBJ_32.setBounds(203, 43, 260, OBJ_32.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("@WENCX@");
          OBJ_33.setName("OBJ_33");
          p_contenu.add(OBJ_33);
          OBJ_33.setBounds(203, 73, 260, OBJ_33.getPreferredSize().height);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_contenu.add(WETB);
          WETB.setBounds(48, 58, 40, WETB.getPreferredSize().height);

          //---- riBoutonRecherche1 ----
          riBoutonRecherche1.setToolTipText("Changement d'\u00e9tablissement");
          riBoutonRecherche1.setName("riBoutonRecherche1");
          riBoutonRecherche1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonRecherche1ActionPerformed(e);
            }
          });
          p_contenu.add(riBoutonRecherche1);
          riBoutonRecherche1.setBounds(new Rectangle(new Point(93, 58), riBoutonRecherche1.getPreferredSize()));

          //---- OBJ_23 ----
          OBJ_23.setTitle("Plage de relev\u00e9s \u00e0 traiter");
          OBJ_23.setName("OBJ_23");
          p_contenu.add(OBJ_23);
          OBJ_23.setBounds(33, 127, 625, OBJ_23.getPreferredSize().height);

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new TitledBorder(""));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- OBJ_28 ----
            OBJ_28.setText("Num\u00e9ro de relev\u00e9 de d\u00e9but");
            OBJ_28.setName("OBJ_28");
            P_SEL0.add(OBJ_28);
            OBJ_28.setBounds(15, 19, 200, 20);

            //---- OBJ_31 ----
            OBJ_31.setText("Num\u00e9ro de relev\u00e9 de fin");
            OBJ_31.setName("OBJ_31");
            P_SEL0.add(OBJ_31);
            OBJ_31.setBounds(15, 54, 200, 20);

            //---- NUMDEB ----
            NUMDEB.setComponentPopupMenu(BTD);
            NUMDEB.setName("NUMDEB");
            P_SEL0.add(NUMDEB);
            NUMDEB.setBounds(260, 15, 58, NUMDEB.getPreferredSize().height);

            //---- NUMFIN ----
            NUMFIN.setComponentPopupMenu(BTD);
            NUMFIN.setName("NUMFIN");
            P_SEL0.add(NUMFIN);
            NUMFIN.setBounds(260, 50, 58, NUMFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < P_SEL0.getComponentCount(); i++) {
                Rectangle bounds = P_SEL0.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_SEL0.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_SEL0.setMinimumSize(preferredSize);
              P_SEL0.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(P_SEL0);
          P_SEL0.setBounds(203, 160, 340, 95);

          //---- WTOU ----
          WTOU.setText("S\u00e9lection compl\u00e8te");
          WTOU.setComponentPopupMenu(BTD);
          WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOU.setName("WTOU");
          p_contenu.add(WTOU);
          WTOU.setBounds(48, 197, 152, 20);

          //---- WRED ----
          WRED.setText("R\u00e9edition");
          WRED.setComponentPopupMenu(BTD);
          WRED.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WRED.setName("WRED");
          p_contenu.add(WRED);
          WRED.setBounds(48, 270, 134, 20);

          //---- OBJ_22 ----
          OBJ_22.setTitle("");
          OBJ_22.setName("OBJ_22");
          p_contenu.add(OBJ_22);
          OBJ_22.setBounds(33, 310, 625, OBJ_22.getPreferredSize().height);

          //---- UTRI ----
          UTRI.setText("Tri et s\u00e9lection");
          UTRI.setComponentPopupMenu(BTD);
          UTRI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          UTRI.setName("UTRI");
          p_contenu.add(UTRI);
          UTRI.setBounds(48, 330, 287, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);
    }

    //---- OBJ_50 ----
    OBJ_50.setText("");
    OBJ_50.setToolTipText("Appel syst\u00e8me");
    OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_50.setName("OBJ_50");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel l_LOCTP;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_27;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_33;
  private XRiTextField WETB;
  private SNBoutonRecherche riBoutonRecherche1;
  private JXTitledSeparator OBJ_23;
  private JPanel P_SEL0;
  private JLabel OBJ_28;
  private JLabel OBJ_31;
  private XRiTextField NUMDEB;
  private XRiTextField NUMFIN;
  private XRiCheckBox WTOU;
  private XRiCheckBox WRED;
  private JXTitledSeparator OBJ_22;
  private XRiCheckBox UTRI;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JPanel P_PnlOpts;
  private JButton OBJ_50;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
