/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.parametre;

import ri.serien.libcommun.outils.encodage.Chiffrage;

/**
 * Préférences utilisateur pour le Rad
 */
public class Preferences {
  // Constantes
  // private final static String NOM_ESPACE="NewSim";
  // Variables
  // private String espaceTravail=System.getProperty("user.home") + File.separatorChar + NOM_ESPACE;
  private String dernierPath = System.getProperty("user.home");
  private Chiffrage chiffrage = new Chiffrage("ISO8859-15");
  
  private String serveur = null;
  private String profil = null;
  private String mdp = null;
  private transient String mdpclair = null;
  
  /**
   * @param espaceTravail the espaceTravail to set
   * 
   *          public void setEspaceTravail(String espaceTravail)
   *          {
   *          this.espaceTravail = espaceTravail;
   *          }
   * 
   *          /**
   * @return the espaceTravail
   * 
   *         public String getEspaceTravail()
   *         {
   *         return espaceTravail;
   *         }
   */
  
  /**
   * @param dernierPath the dernierPath to set
   */
  public void setDernierPath(String dernierPath) {
    this.dernierPath = dernierPath;
  }
  
  /**
   * @return the dernierPath
   */
  public String getDernierPath() {
    return dernierPath;
  }
  
  /**
   * @param serveur the serveur to set
   */
  public void setServeur(String serveur) {
    this.serveur = serveur;
  }
  
  /**
   * @return the serveur
   */
  public String getServeur() {
    return serveur;
  }
  
  /**
   * @param profil the profil to set
   */
  public void setProfil(String profil) {
    this.profil = profil;
  }
  
  /**
   * @return the profil
   */
  public String getProfil() {
    return profil;
  }
  
  /**
   * @param mdp the mdp to set
   */
  public void setMdp(String mdp) {
    this.mdp = mdp;
  }
  
  /**
   * @return the mdp
   */
  public String getMdp() {
    return mdp;
  }
  
  /**
   * @param mdpclair the mdpclair to set
   */
  public void setMdpclair(String mdpclair) {
    this.mdpclair = mdpclair;
    // On chiffre le mdp
    setMdp(chiffrage.cryptInString(mdpclair));
  }
  
  /**
   * @return the mdpclair
   */
  public String getMdpclair() {
    // On déchiffre le mot de passe
    mdpclair = chiffrage.decryptInString(getMdp());
    return mdpclair;
  }
  
}
