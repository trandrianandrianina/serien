/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.Color;
import java.awt.Graphics;

/**
 * Description d'un rectangle de sélection
 */
public class SelectionModeCaractere {
  // Constantes
  public static final String LIB = "LIGNE";
  
  // Variables
  private StringBuffer libelle = new StringBuffer(); // Libelle de la variable
  private String sauveLibelle = null; // Sauve l'ancien libellé (pour la recherche dans le pageEditor)
  private int x = 0;
  private int y = 0;
  private int w = 0;
  private int h = 0; // Dimension en pixels
  private int ligne = 0;
  private int colonne = 0;
  private int longueur = 0; // ligne & colonne commence à 0 (en nombre de caractères)
  private int largeurLettre = 0;
  private int hauteurLettre = 0;
  private boolean trim = false;
  private boolean lignecomplete = false;
  private boolean selection = false;
  private boolean chgDimChar = false;
  private Color couleur = Color.blue;
  private Color couleurSelection = Color.red;
  
  /**
   * Constructeur
   * @param x
   * @param y
   * @param w
   * @param h
   */
  public SelectionModeCaractere(int x, int y, int w, int h, int largeurchar, int hauteurchar) {
    setDimChar(largeurchar, hauteurchar);
    setX(x);
    setY(y);
    setW(w);
    setH(h);
  }
  
  /**
   * Constructeur
   * @param ligne
   * @param nbrcolonnes
   * @param largeurchar
   * @param hauteurchar
   * @param lignecomplete
   */
  public SelectionModeCaractere(int ligne, int colonne, int nbrcolonnes, int largeurchar, int hauteurchar, boolean lignecomplete) {
    setDimChar(largeurchar, hauteurchar);
    setLigne(ligne);
    setColonne(colonne);
    setLongueur(nbrcolonnes);
    this.lignecomplete = lignecomplete;
  }
  
  /**
   * @return the libelle
   */
  public String getLibelle() {
    // On planque le libellé actuel
    StringBuffer sauve = libelle;
    
    // On génère le nouveau
    libelle.setLength(0);
    libelle.append(LIB).append(ligne < 9 ? "0" : "").append(ligne + 1);
    if (!lignecomplete) {
      libelle.append('/').append(colonne).append(',').append(longueur).append(trim ? '\u00a8' : '^');
    }
    libelle.insert(0, '@').append('@');
    
    // Si le nouveau est différent alors on le planque (pour le pageEditor)
    if (!libelle.equals(sauve)) {
      sauveLibelle = sauve.toString();
    }
    return libelle.toString();
  }
  
  /**
   * Retourne l'ancien libellé
   * @return
   */
  public String getSauveLibelle() {
    return sauveLibelle;
  }
  
  /**
   * @return the x
   */
  public int getX() {
    return x;
  }
  
  /**
   * @param x the x to set
   */
  public void setX(int x) {
    if (x < 0) {
      return;
    }
    this.x = x;
    if (largeurLettre > 0) {
      colonne = x / largeurLettre;
    }
  }
  
  /**
   * @return the y
   */
  public int getY() {
    return y;
  }
  
  /**
   * @param y the y to set
   */
  public void setY(int y) {
    if (y < 0) {
      return;
    }
    this.y = y;
    if (hauteurLettre > 0) {
      ligne = y / hauteurLettre;
    }
  }
  
  /**
   * @return the w
   */
  public int getW() {
    return w;
  }
  
  /**
   * @param w the w to set
   */
  public void setW(int w) {
    if (w < largeurLettre) {
      return;
    }
    this.w = w;
    if (largeurLettre > 0) {
      longueur = w / largeurLettre;
    }
  }
  
  /**
   * @return the h
   */
  public int getH() {
    return h;
  }
  
  /**
   * @param h the h to set
   */
  public void setH(int h) {
    if (h < 0) {
      return;
    }
    this.h = h;
  }
  
  /**
   * @return the ligne
   */
  public int getLigne() {
    return ligne;
  }
  
  /**
   * @param ligne the ligne to set
   */
  public void setLigne(int ligne) {
    if (ligne < 1) {
      return;
    }
    this.ligne = ligne;
    y = ligne * hauteurLettre;
  }
  
  /**
   * @return the colonne
   */
  public int getColonne() {
    return colonne;
  }
  
  /**
   * @param colonne the colonne to set
   */
  public void setColonne(int colonne) {
    if (colonne < 1) {
      return;
    }
    this.colonne = colonne;
    x = colonne * largeurLettre;
  }
  
  /**
   * @return the longueur
   */
  public int getLongueur() {
    return longueur;
  }
  
  /**
   * @param longueur the longueur to set
   */
  public void setLongueur(int longueur) {
    if (longueur < 1) {
      return;
    }
    this.longueur = longueur;
    w = longueur * largeurLettre;
  }
  
  /**
   * @return the trim
   */
  public boolean isTrim() {
    return trim;
  }
  
  /**
   * @param trim the trim to set
   */
  public void setTrim(boolean trim) {
    this.trim = trim;
  }
  
  /**
   * @return the lignecomplete
   */
  public boolean isLignecomplete() {
    return lignecomplete;
  }
  
  /**
   * @param lignecomplete the lignecomplete to set
   */
  public void setLignecomplete(boolean lignecomplete) {
    this.lignecomplete = lignecomplete;
    // On a besoin de recalculer les dimensions de la sélection
    chgDimChar = true;
  }
  
  /**
   * @param selection the selection to set
   */
  public void setSelection(boolean selection) {
    this.selection = selection;
  }
  
  /**
   * @return the selection
   */
  public boolean isSelection() {
    return selection;
  }
  
  /**
   * @param selection the selection to set
   */
  public boolean setIsIn(int mx, int my) {
    setSelection((mx >= x) && (mx <= (x + w) && (my >= y) && (my <= (y + h))));
    return isSelection();
  }
  
  /**
   * Dessine le rectangle dans un contexte graphique
   * @param g
   */
  public void paint(Graphics g) {
    if (selection) {
      g.setColor(couleurSelection);
    }
    else {
      g.setColor(couleur);
    }
    if (chgDimChar) {
      calculeDim();
    }
    g.drawRect(x, y, w, h);
  }
  
  /**
   * @param h_char the h_char to set
   */
  public void setDimChar(int w_char, int h_char) {
    this.largeurLettre = w_char;
    this.hauteurLettre = h_char;
    chgDimChar = true;
  }
  
  /**
   * Reclacule la position et la taille du rectangle suite à changement taille de police
   */
  private void calculeDim() {
    chgDimChar = false;
    x = colonne * largeurLettre;
    y = ligne * hauteurLettre;
    w = longueur * largeurLettre;
    h = hauteurLettre;
  }
  
  /**
   * @param couleur the couleur to set
   */
  public void setCouleur(Color couleur) {
    this.couleur = couleur;
  }
  
  /**
   * @return the couleur
   */
  public Color getCouleur() {
    return couleur;
  }
  
  /**
   * @param couleurSelection the couleurSelection to set
   */
  public void setCouleurSelection(Color couleurSelection) {
    this.couleurSelection = couleurSelection;
  }
  
  /**
   * @return the couleurSelection
   */
  public Color getCouleurSelection() {
    return couleurSelection;
  }
}
