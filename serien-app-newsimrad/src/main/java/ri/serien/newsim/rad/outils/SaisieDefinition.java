/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.outils;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

/**
 * Filtre permettant de saisir en majuscule, changer une saisie clavier et de limiter le nombre de caractères saisis.
 */
public class SaisieDefinition extends DocumentFilter {
  // Constantes
  // Chiffre, +, -, virgule et point
  public static final int NUMERIQUE_NOMBRE = 0;
  // Que des chiffres
  public static final int NUMERIQUE_STRICT = 1;
  
  private static final char[] NUMERICS = { '+', '-', ',', '.' };
  
  // Variables
  private boolean isUpperCase = false;
  // Par défaut valeur max d'un entier (ou presque)
  private int longueur = 0x0FFFFFFF;
  // Liste des caractères autorisés
  private char[] listeCaracteresAutorises = null;
  // Liste des caractères non autorisés
  private char[] listeCaracteresNonAutorises = null;
  // Hostfield numérique ou alpha (par défaut doit être alpha car c'est le cas le plus courant)
  // Modifié le 22/09/2017, la valeur précédente était false
  private boolean isAlpha = true;
  private int typeNumerique = NUMERIQUE_NOMBRE;
  // Autorise ou pas la tabulation automatique lorsque la longueur max de la zone est atteinte
  // Attention si on autorise la tabulation, il a un problème avec le requestfocus si la tabulation est déclenchée
  private boolean tabulationAutomatique = true;
  private StringBuilder sb = new StringBuilder();
  private static Robot tab = null;
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur) {
    setNbrMaxCar(alongueur);
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(boolean auppercase) {
    setUpperCase(auppercase);
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur, boolean auppercase) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
  }
  
  /**
   * Constructeur.
   * @param listeA des caractères autorisés.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
  }
  
  /**
   * Constructeur.
   * @param listeA des caractères autorisés.
   * @param listeNA des caractères non autorisés.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, char[] listeNA) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
    listeCaracteresNonAutorises = listeNA;
  }
  
  /**
   * Constructeur.
   * @param listeA des caractères autorisés.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, boolean isAlphaNum) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
    isAlpha = isAlphaNum;
  }
  
  /**
   * Constructeur.
   * @param listeA des caractères autorisés.
   * @param listeNA des caractères non autorisés.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, char[] listeA, char[] listeNA, boolean isAlphaNum) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    listeCaracteresAutorises = listeA;
    listeCaracteresNonAutorises = listeNA;
    isAlpha = isAlphaNum;
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur, boolean auppercase, boolean isAlphaNum, boolean tabulationAuto) {
    setNbrMaxCar(alongueur);
    setUpperCase(auppercase);
    isAlpha = isAlphaNum;
    tabulationAutomatique = tabulationAuto;
  }
  
  /**
   * Constructeur.
   */
  public SaisieDefinition(int alongueur, int pTypeNumerique) {
    setNbrMaxCar(alongueur);
    isAlpha = false;
    typeNumerique = pTypeNumerique;
  }
  
  /**
   * Initialise le nombre de carctères max.
   */
  private void setNbrMaxCar(int alongueur) {
    longueur = alongueur;
  }
  
  /**
   * Initialise si la saisie n'accepte que des majuscules.
   */
  private void setUpperCase(boolean auppercase) {
    isUpperCase = auppercase;
  }
  
  /**
   * Vérifie que le caractère saisie soit autorisé.
   */
  private boolean isAutorized(String texte) {
    boolean trouve = true;
    char[] lettres = texte.toCharArray();
    
    // Test des caractères autorisés
    if (listeCaracteresAutorises != null) {
      for (char lettre : lettres) {
        trouve = false;
        for (int i = listeCaracteresAutorises.length; --i >= 0;) {
          if (lettre == listeCaracteresAutorises[i]) {
            trouve = true;
            break;
          }
        }
        if (!trouve) {
          break;
        }
      }
    }
    else {
      trouve = true;
    }
    
    // Test des caractères non autorisés
    if ((listeCaracteresNonAutorises != null) && (trouve)) {
      for (char lettre : lettres) {
        for (int i = listeCaracteresNonAutorises.length; --i >= 0;) {
          if (lettre == listeCaracteresNonAutorises[i]) {
            trouve = false;
            break;
          }
        }
        if (!trouve) {
          break;
        }
      }
    }
    
    return trouve;
  }
  
  /**
   * Teste le contenu d'une zone numérique.
   */
  private boolean valideNumerique(StringBuilder asb) {
    /* Je ne comprend pas ce truc - 03/11/2016
    if ((asb.indexOf("-+") != -1) || (asb.indexOf("+-") != -1)) {
      return false;
    }
    if ((asb.indexOf("-,") != -1) || (asb.indexOf(",-") != -1)) {
      return false;
    }
    if ((asb.indexOf("+,") != -1) || (asb.indexOf(",+") != -1)) {
      return false;
    }
    if ((asb.indexOf(" ,") != -1) || (asb.indexOf(", ") != -1)) {
      return false;
    }
    if ((asb.indexOf(" +") != -1) || (asb.indexOf("+ ") != -1)) {
      return false;
    }
    if ((asb.indexOf(" -") != -1) || (asb.indexOf("- ") != -1)) {
      return false;
    }*/
    /* Alternative un peu trop véloce
    try {  
      double d = Double.parseDouble(asb.toString());  
    }
    catch (NumberFormatException nfe) {  
      return false;
    }*/
    
    switch (typeNumerique) {
      case NUMERIQUE_NOMBRE:
        for (char d : asb.toString().toCharArray()) {
          boolean trouve = false;
          if (!Character.isDigit(d)) {
            for (char c : NUMERICS) {
              if (d == c) {
                trouve = true;
                break;
              }
            }
            return trouve;
          }
        }
        break;
      case NUMERIQUE_STRICT:
        for (char d : asb.toString().toCharArray()) {
          if (!Character.isDigit(d)) {
            return false;
          }
        }
        break;
    }
    return true;
  }
  
  /**
   * En insertion.
   */
  public void insertString(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
    // Vérification du caractère
    if (!isAutorized(text)) {
      return;
    }
    if (!isAlpha) {
      // On affine le test avec les zones numériques
      sb.setLength(0);
      sb.append(fb.getDocument().getText(0, fb.getDocument().getLength())).insert(offset, text);
      if (!valideNumerique(sb)) {
        return;
      }
    }
    
    // Insertion
    if ((fb.getDocument().getLength() + text.length() - length) <= longueur) {
      super.insertString(fb, offset, isUpperCase ? getUpperCase(text) : text, attrs);
    }
    else {
      Toolkit.getDefaultToolkit().beep();
      // super.insertString(fb, offset, string.toUpperCase(), attr);
    }
  }
  
  /**
   * En remplacement.
   */
  @Override
  public void replace(FilterBypass fb, int offset, int length, String text, AttributeSet attrs) throws BadLocationException {
    // Vérification du caractère
    if (!isAutorized(text)) {
      return;
    }
    if (!isAlpha) {
      // On affine le test avec les zones numériques
      sb.setLength(0);
      sb.append(fb.getDocument().getText(0, fb.getDocument().getLength())).replace(offset, offset + length, text);
      if (!valideNumerique(sb)) {
        return;
      }
    }
    
    // Remplacement
    if ((fb.getDocument().getLength() - length) < longueur) {
      // traitement du point remplaçé par la virgule en format numérique
      if (!isAlpha && text.equals(".")) {
        super.replace(fb, offset, length, ",", attrs);
        // sinon mise en majuscule
      }
      else {
        super.replace(fb, offset, length, isUpperCase ? getUpperCase(text) : text, attrs);
      }
      
      if (tabulationAutomatique && (fb.getDocument().getLength() == longueur)) {
        tabulation();
      }
    }
    else {
      Toolkit.getDefaultToolkit().beep();
      // super.replace(fb, offset, length, text.toUpperCase(), attrs);
    }
  }
  
  // private
  /**
   * Traitement pour la transformation d'un chaine en majuscule.
   */
  private String getUpperCase(String chaine) {
    // Traitement spécial sur le µ qui est transformé en M lors du passage en Majuscule
    int pos = chaine.indexOf('µ');
    if (pos == -1) {
      return chaine.toUpperCase();
    }
    else {
      return chaine.replaceAll("µ", "¤").toUpperCase().replaceAll("¤", "µ");
    }
  }
  
  /**
   * Simulation de la Tabulation.
   */
  private static void tabulation() {
    try {
      // if (composant != null) composant.requestFocus();
      if (tab == null) {
        tab = new Robot();
      }
      // On fait attention au Shift sinon on effectue une tabulation arrière (avec les portables)
      tab.keyPress(KeyEvent.VK_TAB);
      tab.keyRelease(KeyEvent.VK_TAB);
    }
    catch (AWTException e) {
      Toolkit.getDefaultToolkit().beep();
    }
  }
  
  public int getTypeNumerique() {
    return typeNumerique;
  }
  
  public void setTypeNumerique(int typeNumerique) {
    this.typeNumerique = typeNumerique;
  }
  
  public boolean isAlpha() {
    return isAlpha;
  }
  
  public void setAlpha(boolean isAlpha) {
    this.isAlpha = isAlpha;
  }
}
