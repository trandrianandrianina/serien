/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;

import ri.serien.newsim.description.DescriptionObject;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.outils.CalculDPI;
import ri.serien.newsim.rad.outils.RiFileChooser;

/**
 * Objet de base
 */
public class GfxObject extends JLabel implements Comparable<Object> {
  // Constantes
  
  public static final String PAS_DE_COND = "(Aucune)";
  
  // Variables
  private File dernierPath = null;
  protected GfxPageEditor pageEditor = null;
  private int dx = 0;
  private int dy = 0;
  protected String nomPolice = "Monospaced";
  // 0=Plain 1=Bold 2=Italic
  protected int stylePolice = 0;
  // Taille de la police en pt
  protected float taillePolice = 10f;
  private boolean focusBorder = true;
  protected boolean redimensionne = false;
  protected boolean deplace = false;
  // Nombre de dpi dans le rad
  protected int dpi_aff = 72;
  // Nombre de dpi dans le rad
  protected int dpi_img = 72;
  protected boolean clic_gauche = false;
  protected boolean key_ctrl = false;
  // Suite des conditions saisie en notation polonaise (liste contenu dans la description de page),
  // le résultat doit être vrai pour que l'objet s'affiche (ex: COND_1 COND_2 &)
  protected String condition = null;
  // Organise le trie sur X ou Y (il faut modifier tous les objets avant le sort)
  private boolean ytrie = false;
  
  /**
   * Constructeur
   * @param x
   * @param y
   */
  public GfxObject(int x, int y, final GfxPageEditor pageEditor) {
    setOpaque(true);
    setLocation(x, y);
    setSize(getPreferredSize());
    initDeplacement();
    setPageEditor(pageEditor);
  }
  
  /**
   * Constructeur
   * @param x
   * @param y
   * @param largeur
   * @param hauteur
   */
  public GfxObject(int x, int y, int largeur, int hauteur, final GfxPageEditor pageEditor) {
    setOpaque(true);
    setLocation(x, y);
    setSize(largeur, hauteur);
    initDeplacement();
    setPageEditor(pageEditor);
  }
  
  /**
   * Constructeur
   * @param text
   */
  public GfxObject(DescriptionObject dobject, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    this(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getXPos_cm()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getYPos_cm()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getLargeur()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dobject.getHauteur()),
        planTravail);
    setDpiAff(dpi_aff);
    setDpiAff(dpi_img);
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(final GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * Initialise la fonction de déplacement
   */
  public void initDeplacement() {
    // Ecouteur Souris
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        labelMouseMoved(e);
      }
    });
    
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseDragged(MouseEvent e) {
        labelMouseDragged(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      @Override
      public void mousePressed(MouseEvent e) {
        labelMousePressed(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent e) {
        labelMouseReleased(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseEntered(MouseEvent e) {
        labelMouseEntered(e);
      }
    });
    
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseExited(MouseEvent e) {
        labelMouseExited(e);
      }
    });
  }
  
  /**
   * Active le border pour indiquer qu'il a le focus
   * @param border
   */
  public void setFocusBorder(boolean border, boolean keypressed, boolean btdreleased) {
    // On demande l'activation de la sélection
    if (border) {
      // Est ce que Ctrl est enfoncé ?
      if (key_ctrl) {
        border = !focusBorder;
      }
      else if (getNbrSelection() > 1) {
        if (!keypressed && btdreleased) {
          enleveFocusBorder();
        }
      }
      else {
        enleveFocusBorder();
      }
    }
    
    // Traitement
    focusBorder = border;
    if (focusBorder) {
      setBorder(BorderFactory.createLineBorder(Color.red));
    }
    else {
      setBorder(null);
    }
  }
  
  /**
   * Active le border (forcé)
   * @param border
   */
  public void setFocusBorder(boolean border) {
    // Traitement
    focusBorder = border;
    if (focusBorder) {
      setBorder(BorderFactory.createLineBorder(Color.red));
    }
    else {
      setBorder(null);
    }
  }
  
  /**
   * Retourne si le label a le border du focus
   * @return
   */
  public boolean isFocusBorder() {
    return focusBorder;
  }
  
  /**
   * Enleve le focus Border des autres labels du panel
   */
  private void enleveFocusBorder() {
    if (getParent() == null) {
      return;
    }
    
    Component[] listeLabel = getParent().getComponents();
    for (int i = 0; i < listeLabel.length; i++) {
      if (listeLabel[i] instanceof GfxObject) {
        ((GfxObject) listeLabel[i]).setFocusBorder(false, true, true);
      }
    }
    getParent().repaint();
  }
  
  /**
   * Retourne le nombre d'objets sélectionnée
   * @return
   */
  private int getNbrSelection() {
    if (getParent() == null) {
      return 0;
    }
    
    int nbr = 0;
    Component[] listeLabel = getParent().getComponents();
    for (int i = 0; i < listeLabel.length; i++) {
      if ((listeLabel[i] instanceof GfxObject) && ((GfxObject) listeLabel[i]).isFocusBorder()) {
        nbr++;
      }
    }
    return nbr;
  }
  
  /**
   * Affiche le menu contextuel
   * @param e
   * @param btd
   */
  private void maybeShowPopup(MouseEvent e, JPopupMenu btd) {
    btd.show(e.getComponent(), e.getX(), e.getY());
  }
  
  /**
   * Action lors du déplacement de la souris avec bouton appuyé donc du label
   * @param e
   */
  protected void labelMouseDragged(MouseEvent e) {
    if (SwingUtilities.isRightMouseButton(e)) {
      return;
    }
    
    // On redimensionne l'objet
    if (redimensionne) {
      switch (getCursor().getType()) {
        case Cursor.N_RESIZE_CURSOR:
          setLocation(getX(), getY() + e.getY());
          setSize(getWidth(), getHeight() - e.getY());
          break;
        case Cursor.S_RESIZE_CURSOR:
          setSize(getWidth(), e.getY());
          break;
        case Cursor.E_RESIZE_CURSOR:
          setSize(e.getX(), getHeight());
          break;
        case Cursor.W_RESIZE_CURSOR:
          setLocation(getX() + e.getX(), getY());
          setSize(getWidth() - e.getX(), getHeight());
          break;
      }
    }
    else {
      // On déplace la sélection
      if (SwingUtilities.isLeftMouseButton(e)) {
        deplace = true;
        GfxEtiquette parent = (GfxEtiquette) getParent();
        ArrayList<GfxObject> listeSelectionObject = parent.getSelectionGfxObjet();
        // Calcul du delta de la position de la souris dans l'objet
        int deltaX = e.getX() - dx;
        int deltaY = e.getY() - dy;
        // Modification des coordonnées dans le cas où l'on a cliqué sur un objet sélectionné
        if (focusBorder) {
          if (listeSelectionObject.size() == 1) {
            setLocation(getX() + deltaX, getY() + deltaY);
          }
          else {
            for (int i = 0; i < listeSelectionObject.size(); i++) {
              Point p = listeSelectionObject.get(i).getLocation();
              listeSelectionObject.get(i).setLocation(p.x + deltaX, p.y + deltaY);
            }
          }
        }
      }
    }
  }
  
  /**
   * Action lors du déplacement de la souris donc du label
   * @param e
   */
  protected void labelMouseMoved(MouseEvent e) {
    // if ( (redimensionne) || ((getPlanTravail() != null) &&
    // (!getPlanTravail().getZoneCreation().getCursor().equals(Cursor.getDefaultCursor())))) return;
    
    // Les lignes
    Rectangle haut = new Rectangle(3, 0, getWidth() - 6, 2);
    Rectangle bas = new Rectangle(3, getHeight() - 2, getWidth() - 6, 2);
    Rectangle gauche = new Rectangle(0, 3, 2, getHeight() - 6);
    Rectangle droite = new Rectangle(getWidth() - 2, 3, 2, getHeight() - 4);
    
    if (haut.contains(e.getPoint())) {
      setCursor(new Cursor(Cursor.N_RESIZE_CURSOR));
    }
    else {
      if (bas.contains(e.getPoint())) {
        setCursor(new Cursor(Cursor.S_RESIZE_CURSOR));
      }
      else {
        if (gauche.contains(e.getPoint())) {
          setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
        }
        else {
          if (droite.contains(e.getPoint())) {
            setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
          }
          else {
            setCursor(Cursor.getDefaultCursor());
          }
        }
      }
    }
  }
  
  /**
   * Action lors du clic souris sur label (bouton pressé)
   * @param e
   */
  private void labelMousePressed(MouseEvent e) {
    clic_gauche = e.getButton() == MouseEvent.BUTTON1;
    key_ctrl = e.isMetaDown() || e.isControlDown();
    
    // On vérifie si c'est un redimensionnement du label
    if ((getCursor().getType() == Cursor.N_RESIZE_CURSOR) || (getCursor().getType() == Cursor.S_RESIZE_CURSOR)
        || (getCursor().getType() == Cursor.E_RESIZE_CURSOR) || (getCursor().getType() == Cursor.W_RESIZE_CURSOR)) {
      redimensionne = true;
    }
    else {
      redimensionne = false;
    }
    
    // Si bouton gauche pressé
    if (SwingUtilities.isLeftMouseButton(e)) {
      // Test si double clic pour saisie du libellé
      if (e.getClickCount() == 2) {
        saisieLibelle();
      }
      else {
        // On planque les coordonnées pour le déplacement
        dx = e.getX();
        dy = e.getY();
        // On change la couleur du border pour indiquer qu'il a le focus
        setFocusBorder(true, key_ctrl, false);
      }
    }
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  protected void labelMouseReleased(MouseEvent e) {
    clic_gauche = false;
    key_ctrl = e.isMetaDown() || e.isControlDown();
    
    if (SwingUtilities.isLeftMouseButton(e)) {
      if (redimensionne || deplace) {
        pageEditor.save(true);
      }
    }
    
    if (redimensionne) {
      redimensionne = false;
    }
    deplace = false;
    
    // On vérifie si on doit changer le menu contextuel
    if (SwingUtilities.isRightMouseButton(e)) {
      if ((getParent() != null) && (getParent() instanceof GfxEtiquette)) {
        GfxEtiquette parent = (GfxEtiquette) getParent();
        if (parent.getSelectionGfxObjet().size() <= 1) {
          if (this instanceof GfxLabel) {
            maybeShowPopup(e, parent.getPopMenuLabel());
          }
          else {
            if (this instanceof GfxImage) {
              maybeShowPopup(e, parent.getPopMenuImage());
            }
            else {
              if (this instanceof GfxCodeBarre) {
                maybeShowPopup(e, parent.getPopMenuCodeBarre());
              }
            }
          }
        }
        else {
          maybeShowPopup(e, parent.getPopMenuMultiSelection());
        }
      }
    }
  }
  
  /**
   * Action lorsque la souris est sur le label
   * @param e
   */
  private void labelMouseEntered(MouseEvent e) {
    setBorder(BorderFactory.createLineBorder(new Color(255, 153, 0)));
  }
  
  /**
   * Action lorsque la souris sort du label
   * @param e
   */
  private void labelMouseExited(MouseEvent e) {
    if (isFocusBorder()) {
      setBorder(BorderFactory.createLineBorder(Color.red));
    }
    else {
      setBorder(null);
    }
  }
  
  /**
   * Permet de saisir un libellé dans le label
   */
  private void saisieLibelle() {
    Dimension d = getSize();
    Point p = getLocation();
    if (p.y < (getParent().getHeight() - d.height * 2)) {
      p.y = p.y + d.height;
    }
    else {
      p.y = p.y - (d.height * 2);
    }
    d.height = d.height + getFont().getSize();
    new SaisieVolatile(p, d, this);
  }
  
  /**
   * Recherche le fichier à ouvrir
   * 
   * private String ouvrir(String[] extension, String fichier)
   * {
   * String chemin = null;
   * final JFileChooser choixFichier = new JFileChooser();
   * //choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
   * 
   * choixFichier.addChoosableFileFilter(new GestionFiltre(extension, fichier));
   * // On se place dans le dernier dossier courant s'il y en a un
   * if (dernierPath != null)
   * choixFichier.setCurrentDirectory(dernierPath);
   * else
   * dernierPath = pageEditor.getDernierPath();
   * choixFichier.setSelectedFile(dernierPath);
   * 
   * // Recuperation du chemin
   * if (choixFichier.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
   * chemin = choixFichier.getSelectedFile().getPath();
   * else
   * return null;
   * 
   * // On stocke comme le dossier courant
   * dernierPath = choixFichier.getCurrentDirectory();
   * 
   * return chemin;
   * }
   */
  private String ouvrir(String[] extension, String texte) {
    String chemin = null;
    // On se place dans le dernier dossier courant s'il y en a un
    if (dernierPath == null) {
      dernierPath = pageEditor.getDernierPath();
    }
    
    final RiFileChooser choixFichier = new RiFileChooser(pageEditor, dernierPath);
    chemin = choixFichier.choisirFichier(extension, texte);
    dernierPath = choixFichier.getDossierFile();
    
    return chemin;
  }
  
  /**
   * Permet de sélectionner une image
   */
  public void parcourirImage() {
    String chemin = ouvrir(new String[] { "png", "jpg", "gif" }, "Fichiers images (png, jpg, gif)");
    if (chemin != null) {
      ((GfxImage) this).setImageOriginale(chemin);
      
      // Ajout de la mise à jour des propriétés
      if (pageEditor != null) {
        pageEditor.getPropertyObject().getPanelProperty(this);
        pageEditor.save(true);
      }
    }
  }
  
  /**
   * @return the dpi
   */
  public int getDpiAff() {
    return dpi_aff;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiAff(int dpi) {
    this.dpi_aff = dpi;
  }
  
  /**
   * @return the dpi
   */
  public int getDpiImg() {
    return dpi_img;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiImg(int dpi) {
    this.dpi_img = dpi;
  }
  
  /**
   * @param taillePolice the taillePolice to set
   */
  public void setTaillePolice(float taillePolice) {
    this.taillePolice = taillePolice;
  }
  
  /**
   * @return the taillePolice
   */
  public float getTaillePolice() {
    return taillePolice;
  }
  
  /**
   * @return the nomPolice
   */
  public String getNomPolice() {
    return nomPolice;
  }
  
  /**
   * @param nomPolice the nomPolice to set
   */
  public void setNomPolice(String nomPolice) {
    this.nomPolice = nomPolice;
  }
  
  /**
   * @return the stylePolice
   */
  public int getStylePolice() {
    return stylePolice;
  }
  
  /**
   * @param stylePolice the stylePolice to set
   */
  public void setStylePolice(int stylePolice) {
    this.stylePolice = stylePolice;
  }
  
  /**
   * @return the condition
   */
  public String getCondition() {
    return condition;
  }
  
  /**
   * @param condition the condition to set
   */
  public void setCondition(String condition) {
    this.condition = condition;
  }
  
  /**
   * @return the ytrie
   */
  public boolean isYtrie() {
    return ytrie;
  }
  
  /**
   * @param ytrie the ytrie to set
   */
  public void setYtrie(boolean ytrie) {
    this.ytrie = ytrie;
  }
  
  /**
   * Permet de trier les objets lorsqu'ils sont dans une arraylist
   * @param objet
   * @return
   */
  @Override
  public int compareTo(Object objet) {
    if (ytrie) {
      if (getLocation().y > ((GfxObject) objet).getLocation().y) {
        return 1;
      }
      if (getLocation().y < ((GfxObject) objet).getLocation().y) {
        return -1;
      }
      if (getLocation().y == ((GfxObject) objet).getLocation().y) {
        return 0;
      }
    }
    else {
      if (getLocation().x > ((GfxObject) objet).getLocation().x) {
        return 1;
      }
      if (getLocation().x < ((GfxObject) objet).getLocation().x) {
        return -1;
      }
      if (getLocation().x == ((GfxObject) objet).getLocation().x) {
        return 0;
      }
    }
    return 0;
  }
  
}
