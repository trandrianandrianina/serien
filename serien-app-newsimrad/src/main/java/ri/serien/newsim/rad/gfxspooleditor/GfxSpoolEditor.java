/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JToggleButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.exploitation.edition.SpoolOld;
import ri.serien.libcommun.outils.collection.HashMapManager;
import ri.serien.newsim.description.DescriptionCondition;
import ri.serien.newsim.description.DescriptionPage;
import ri.serien.newsim.description.DescriptionVariable;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rendu.GenerationFichierFO;
import ri.serien.newsim.rendu.GenerationFichierFinal;

/**
 * Editeur de spool
 */
public class GfxSpoolEditor extends JFrame {
  // Constantes
  
  public static final int MODE_DATA = 0;
  public static final int MODE_CONDITION = 1;
  public static final int MODE_VARIABLE = 2;
  
  // Variables
  private SpoolOld spoolOld = null;
  private int pageCourante = 1;
  private HashMapManager listeSpoolEditor = null;
  private boolean isConnected = false;
  private int mode = MODE_DATA;
  protected GfxPageEditor pageEditor = null;
  private String dossierTravail = "";
  
  /**
   * Constructeur
   * @param nomspool
   * @param aspool
   * @param listeSpoolEditor
   */
  public GfxSpoolEditor(String nomspool, SpoolOld aspool, HashMapManager listeSpoolEditor) {
    this.listeSpoolEditor = listeSpoolEditor;
    
    int deltaX = 0;
    int deltaY = 0;
    pack();
    deltaX = getWidth();
    deltaY = getHeight();
    
    spoolOld = aspool;
    initComponents();
    if (nomspool != null) {
      setTitle(getTitle() + " - " + nomspool);
    }
    
    // Dimensionnement du TextArea pour les variables
    initTailleTextAreaData();
    ((SpoolTextAreaData) ta_PageData).setDimensionCaracteres(spoolOld.getNbrLignes(), spoolOld.getNbrColonnes());
    ((SpoolTextAreaData) ta_PageData).setComponentLibelle(l_Libelle);
    ((SpoolTextAreaData) ta_PageData).setComponentPosCurseur(l_PosCurseur);
    initTailleTextAreaCondition();
    ((SpoolTextAreaCondition) ta_PageCondition).setDimensionCaracteres(spoolOld.getNbrLignes(), spoolOld.getNbrColonnes());
    ((SpoolTextAreaCondition) ta_PageCondition).setComponentLibelle(l_Libelle);
    ((SpoolTextAreaCondition) ta_PageCondition).setComponentPosCurseur(l_PosCurseur);
    ((SpoolTextAreaCondition) ta_PageCondition).setTexteCondition(l_ConditionSelectionne);
    initTailleTextAreaVariable();
    ((SpoolTextAreaVariable) ta_PageVariable).setDimensionCaracteres(spoolOld.getNbrLignes(), spoolOld.getNbrColonnes());
    ((SpoolTextAreaVariable) ta_PageVariable).setComponentLibelle(l_Libelle);
    ((SpoolTextAreaVariable) ta_PageVariable).setComponentPosCurseur(l_PosCurseur);
    ((SpoolTextAreaVariable) ta_PageVariable).setTexteVariable(l_VariableSelectionnee);
    initPage(pageCourante);
    
    // Dimensionnement correct de la fenêtre
    Dimension d = getSize();
    setSize(d.width + deltaX + p_MargeDroite.getSize().width + p_MargeGauche.getSize().width,
        d.height + deltaY + p_MargeDroite.getSize().height + p_MargeGauche.getSize().height);
    
    tbt_ModeCondition.setEnabled(isConnected);
    tbt_ModeVariable.setEnabled(isConnected);
    chk_CreationAutoLabel.setEnabled(isConnected);
    
    setVisible(true);
  }
  
  /**
   * Renvoi la page courante du spool
   * @return
   */
  public String[] getPageCouranteSpool() {
    if (spoolOld == null) {
      return null;
    }
    String[] tab = new String[1];
    tab[0] = spoolOld.getPages()[pageCourante - 1];
    return tab;
  }
  
  /**
   * Renvoi le texte complet du spool
   * @return
   */
  public String[] getPagesSpool() {
    if (spoolOld == null) {
      return null;
    }
    return spoolOld.getPages();
  }
  
  /**
   * @param isConnected the isConnected to set
   */
  public void setConnected(boolean isConnected) {
    this.isConnected = isConnected;
    if (this.isConnected) {
      l_ConnexionPageEditor.setIcon(new ImageIcon(getClass().getResource("/images/aqua-balle-vert-icone-5675-16.png")));
    }
    else {
      l_ConnexionPageEditor.setIcon(new ImageIcon(getClass().getResource("/images/aqua-ballon-rouge-icone-8951-16.png")));
      setPageEditor(null);
    }
    tbt_ModeCondition.setEnabled(isConnected);
    tbt_ModeVariable.setEnabled(isConnected);
    if (!isConnected) {
      chk_CreationAutoLabel.setSelected(false);
    }
    chk_CreationAutoLabel.setEnabled(isConnected);
  }
  
  /**
   * @return the isConnected
   */
  public boolean isConnected() {
    return isConnected;
  }
  
  /**
   * @param mode the mode to set
   */
  public void setMode(int mode) {
    this.mode = mode;
  }
  
  /**
   * @return the mode
   */
  public int getMode() {
    return mode;
  }
  
  /**
   * Retourne le sool
   * @return
   */
  public SpoolOld getSpool() {
    return spoolOld;
  }
  
  /**
   * Initialise la liste des conditions
   * @return
   */
  public void setListeCondition(ArrayList<DescriptionCondition> listeCondition) {
    ((SpoolTextAreaCondition) ta_PageCondition).setListeCondition(listeCondition);
  }
  
  /**
   * Initialise la liste des variables
   * @return
   */
  public void setListeVariable(ArrayList<DescriptionVariable> listeVariable) {
    ((SpoolTextAreaVariable) ta_PageVariable).setListeVariable(listeVariable);
  }
  
  // ------------------------------------ Méthodes privées -------------------
  
  /**
   * Traitement lors du basculement du mode Variable/Condition
   */
  private void setTraitementMode(int mode) {
    switch (this.mode) {
      case MODE_DATA:
        panel8.remove(ta_PageData);
        panel9.remove(p_OutilsData);
        
        break;
      case MODE_CONDITION:
        panel8.remove(ta_PageCondition);
        panel9.remove(p_OutilsCondition);
        break;
      case MODE_VARIABLE:
        panel8.remove(ta_PageVariable);
        panel9.remove(p_OutilsVariable);
        break;
    }
    setMode(mode);
    
    switch (mode) {
      case MODE_DATA:
        panel8.add(ta_PageData, BorderLayout.CENTER);
        panel9.add(p_OutilsData);
        break;
      case MODE_CONDITION:
        panel8.add(ta_PageCondition, BorderLayout.CENTER);
        panel9.add(p_OutilsCondition);
        break;
      case MODE_VARIABLE:
        panel8.add(ta_PageVariable, BorderLayout.CENTER);
        // panel9.add(p_OutilsVariable);
        break;
    }
    
    validate();
    repaint();
  }
  
  /**
   * Initialise l'éditeur de page
   * @param pageeditor
   */
  public void setPageEditor(GfxPageEditor pageeditor) {
    pageEditor = pageeditor;
    ((SpoolTextAreaData) ta_PageData).setPageEditor(pageEditor);
  }
  
  /**
   * Retourne l'éditeur de page
   * @return
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * Dimensionnement du composant TextAreaData
   */
  private void initTailleTextAreaData() {
    int l = ta_PageData.getFontMetrics(ta_PageData.getFont()).charWidth('M');
    int h = ta_PageData.getFontMetrics(ta_PageData.getFont()).getHeight();
    Dimension d = new Dimension(spoolOld.getNbrColonnes() * l, spoolOld.getNbrLignes() * h);
    ta_PageData.setMinimumSize(d);
    ta_PageData.setMaximumSize(d);
    ta_PageData.setPreferredSize(d);
  }
  
  /**
   * Dimensionnement du composant TextAreaCondition
   */
  private void initTailleTextAreaCondition() {
    int l = ta_PageCondition.getFontMetrics(ta_PageCondition.getFont()).charWidth('M');
    int h = ta_PageCondition.getFontMetrics(ta_PageCondition.getFont()).getHeight();
    Dimension d = new Dimension(spoolOld.getNbrColonnes() * l, spoolOld.getNbrLignes() * h);
    ta_PageCondition.setMinimumSize(d);
    ta_PageCondition.setMaximumSize(d);
    ta_PageCondition.setPreferredSize(d);
  }
  
  /**
   * Dimensionnement du composant TextAreaVariable
   */
  private void initTailleTextAreaVariable() {
    int l = ta_PageVariable.getFontMetrics(ta_PageVariable.getFont()).charWidth('M');
    int h = ta_PageVariable.getFontMetrics(ta_PageVariable.getFont()).getHeight();
    Dimension d = new Dimension(spoolOld.getNbrColonnes() * l, spoolOld.getNbrLignes() * h);
    ta_PageVariable.setMinimumSize(d);
    ta_PageVariable.setMaximumSize(d);
    ta_PageVariable.setPreferredSize(d);
  }
  
  /**
   * Initialise la page
   */
  private void initPage(int numpage) {
    if ((spoolOld == null) && (numpage > 0) && (numpage >= spoolOld.getNbrPages())) {
      return;
    }
    
    l_NbrPages.setText(numpage + "/" + String.valueOf(spoolOld.getNbrPages()));
    sp_TaillePolice.setValue(Integer.valueOf(ta_PageData.getFont().getSize()));
    
    ta_PageData.setText(spoolOld.getPages()[numpage - 1]);
    ta_PageCondition.setText(spoolOld.getPages()[numpage - 1]);
    ta_PageVariable.setText(spoolOld.getPages()[numpage - 1]);
    
    // On positionne le curseur en haut de la page
    ta_PageData.setCaretPosition(0);
    ta_PageCondition.setCaretPosition(0);
    ta_PageVariable.setCaretPosition(0);
    
    // Grisage des boutons Suivant & Précedent
    bt_Precedent.setEnabled(!(numpage == 1));
    bt_Suivant.setEnabled(!(numpage == spoolOld.getNbrPages()));
  }
  
  /**
   * Fermeture de la fenêtre
   */
  private void fermeture() {
    setVisible(false);
    listeSpoolEditor.removeObject(this);
    dispose();
  }
  
  /**
   * @param dossierTravail the dossierTravail to set
   */
  public void setDossierTravail(String dossierTravail) {
    this.dossierTravail = dossierTravail;
  }
  
  /**
   * @return the dossierTravail
   */
  public String getDossierTravail() {
    return dossierTravail;
  }
  
  /**
   * Génère l'aperçu de la page courante sans description de page
   * @param pTypeDocument
   */
  private void previewPageCouranteSansDescription(EnumTypeDocument pTypeDocument) {
    if (spoolOld == null) {
      return;
    }
    
    // Création d'une description de document temporaire juste pour la préview
    // Si pas de description
    GenerationFichierFO gffo = new GenerationFichierFO(spoolOld, dossierTravail, true);
    GenerationFichierFinal gff = new GenerationFichierFinal();
    gff.setTypeSortie(pTypeDocument);
    gff.setFichierEntree(dossierTravail + File.separatorChar + "noname." + pTypeDocument.getExtension());
    String rendu = gff.renduFinal(dossierTravail, dossierTravail, gffo.genereFOtoString(), 72, 72);
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    // On lance l'application associée
    if (rendu == null) {
      return;
    }
    try {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      Desktop.getDesktop().open(new File(rendu));
    }
    catch (IOException e1) {
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }
  
  // ------------------------------------ Evènements -------------------------
  
  /**
   * Importe la description de la page
   * @param page
   */
  public void importerDescriptionPage(DescriptionPage page) {
    ((SpoolTextAreaData) ta_PageData).importerDescription(page);
    ((SpoolTextAreaCondition) ta_PageCondition).importerDescription();
    ((SpoolTextAreaVariable) ta_PageVariable).importerDescription();
  }
  
  private void sp_TaillePoliceStateChanged(ChangeEvent e) {
    ((SpoolTextAreaData) ta_PageData).setChgSizeFont(((Integer) sp_TaillePolice.getValue()).intValue());
    ((SpoolTextAreaCondition) ta_PageCondition).setChgSizeFont(((Integer) sp_TaillePolice.getValue()).intValue());
    ((SpoolTextAreaVariable) ta_PageVariable).setChgSizeFont(((Integer) sp_TaillePolice.getValue()).intValue());
    initTailleTextAreaData();
    initTailleTextAreaCondition();
    initTailleTextAreaVariable();
    pack();
  }
  
  private void bt_PrecedentActionPerformed(ActionEvent e) {
    initPage(--pageCourante);
  }
  
  private void bt_SuivantActionPerformed(ActionEvent e) {
    initPage(++pageCourante);
  }
  
  private void bt_SelectionAutoActionPerformed(ActionEvent e) {
    ((SpoolTextAreaData) ta_PageData).selectionAuto();
  }
  
  private void bt_EffacerSelectionActionPerformed(ActionEvent e) {
    ((SpoolTextAreaData) ta_PageData).effacerSelection();
  }
  
  private void thisWindowClosing(WindowEvent e) {
    fermeture();
  }
  
  private void tbt_ModeDataActionPerformed(ActionEvent e) {
    setTraitementMode(MODE_DATA);
  }
  
  private void tbt_ModeConditionActionPerformed(ActionEvent e) {
    setTraitementMode(MODE_CONDITION);
  }
  
  private void tbt_ModeVariableActionPerformed(ActionEvent e) {
    setTraitementMode(MODE_VARIABLE);
  }
  
  private void chk_CreationAutoLabelActionPerformed(ActionEvent e) {
    ((SpoolTextAreaData) ta_PageData).setCreationAutoLabel(chk_CreationAutoLabel.isSelected());
  }
  
  private void b_ApercuSansDescriptionActionPerformed(ActionEvent e) {
    previewPageCouranteSansDescription(EnumTypeDocument.PDF);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel2 = new JScrollPane();
    panel8 = new JPanel();
    p_MargeHaute = new JPanel();
    p_MargeDroite = new JPanel();
    p_MargeGauche = new JPanel();
    p_MargeBasse = new JPanel();
    ta_PageData = new SpoolTextAreaData();
    panel9 = new JPanel();
    l_ConnexionPageEditor = new JLabel();
    panel3 = new JPanel();
    label1 = new JLabel();
    l_NbrPages = new JLabel();
    panel4 = new JPanel();
    bt_Precedent = new JButton();
    bt_Suivant = new JButton();
    panel1 = new JPanel();
    label2 = new JLabel();
    sp_TaillePolice = new JSpinner();
    tbt_ModeData = new JToggleButton();
    tbt_ModeCondition = new JToggleButton();
    tbt_ModeVariable = new JToggleButton();
    hSpacer1 = new JPanel(null);
    panel5 = new JPanel();
    label3 = new JLabel();
    l_Libelle = new JLabel();
    p_OutilsData = new JPanel();
    bt_SelectionAuto = new JButton();
    bt_EffacerSelection = new JButton();
    chk_CreationAutoLabel = new JCheckBox();
    b_ApercuSansDescription = new JButton();
    l_PosCurseur = new JLabel();
    ta_PageCondition = new SpoolTextAreaCondition();
    p_OutilsCondition = new JPanel();
    l_ConditionSelectionne = new JLabel();
    ta_PageVariable = new SpoolTextAreaVariable();
    p_OutilsVariable = new JPanel();
    l_VariableSelectionnee = new JLabel();
    
    // ======== this ========
    setTitle("Editeur de spool");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel2 ========
    {
      panel2.setName("panel2");
      
      // ======== panel8 ========
      {
        panel8.setName("panel8");
        panel8.setLayout(new BorderLayout());
        
        // ======== p_MargeHaute ========
        {
          p_MargeHaute.setBackground(Color.gray);
          p_MargeHaute.setName("p_MargeHaute");
          p_MargeHaute.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeHaute, BorderLayout.SOUTH);
        
        // ======== p_MargeDroite ========
        {
          p_MargeDroite.setBackground(Color.gray);
          p_MargeDroite.setName("p_MargeDroite");
          p_MargeDroite.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeDroite, BorderLayout.EAST);
        
        // ======== p_MargeGauche ========
        {
          p_MargeGauche.setBackground(Color.gray);
          p_MargeGauche.setName("p_MargeGauche");
          p_MargeGauche.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeGauche, BorderLayout.WEST);
        
        // ======== p_MargeBasse ========
        {
          p_MargeBasse.setBackground(Color.gray);
          p_MargeBasse.setName("p_MargeBasse");
          p_MargeBasse.setLayout(new FlowLayout());
        }
        panel8.add(p_MargeBasse, BorderLayout.NORTH);
        
        // ---- ta_PageData ----
        ta_PageData.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
        ta_PageData.setName("ta_PageData");
        panel8.add(ta_PageData, BorderLayout.CENTER);
      }
      panel2.setViewportView(panel8);
    }
    contentPane.add(panel2, BorderLayout.CENTER);
    
    // ======== panel9 ========
    {
      panel9.setName("panel9");
      panel9.setLayout(new FlowLayout(FlowLayout.LEFT));
      
      // ---- l_ConnexionPageEditor ----
      l_ConnexionPageEditor.setIcon(new ImageIcon(getClass().getResource("/images/aqua-ballon-rouge-icone-8951-16.png")));
      l_ConnexionPageEditor.setName("l_ConnexionPageEditor");
      panel9.add(l_ConnexionPageEditor);
      
      // ======== panel3 ========
      {
        panel3.setName("panel3");
        panel3.setLayout(new VerticalLayout());
        
        // ---- label1 ----
        label1.setText("Page");
        label1.setName("label1");
        panel3.add(label1);
        
        // ---- l_NbrPages ----
        l_NbrPages.setText("??");
        l_NbrPages.setHorizontalAlignment(SwingConstants.CENTER);
        l_NbrPages.setName("l_NbrPages");
        panel3.add(l_NbrPages);
      }
      panel9.add(panel3);
      
      // ======== panel4 ========
      {
        panel4.setName("panel4");
        panel4.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        
        // ---- bt_Precedent ----
        bt_Precedent.setIcon(new ImageIcon(getClass().getResource("/images/fleche-erreur-a-gauche-precedente-icone-3917-32.png")));
        bt_Precedent.setToolTipText("Page pr\u00e9c\u00e9dente");
        bt_Precedent.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Precedent.setName("bt_Precedent");
        bt_Precedent.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_PrecedentActionPerformed(e);
          }
        });
        panel4.add(bt_Precedent);
        
        // ---- bt_Suivant ----
        bt_Suivant.setIcon(new ImageIcon(getClass().getResource("/images/fleche-a-cote-a-droite-icone-6873-32.png")));
        bt_Suivant.setToolTipText("Page suivante");
        bt_Suivant.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_Suivant.setName("bt_Suivant");
        bt_Suivant.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_SuivantActionPerformed(e);
          }
        });
        panel4.add(bt_Suivant);
      }
      panel9.add(panel4);
      
      // ======== panel1 ========
      {
        panel1.setName("panel1");
        panel1.setLayout(new VerticalLayout());
        
        // ---- label2 ----
        label2.setText("Taille police");
        label2.setName("label2");
        panel1.add(label2);
        
        // ---- sp_TaillePolice ----
        sp_TaillePolice.setName("sp_TaillePolice");
        sp_TaillePolice.addChangeListener(new ChangeListener() {
          @Override
          public void stateChanged(ChangeEvent e) {
            sp_TaillePoliceStateChanged(e);
          }
        });
        panel1.add(sp_TaillePolice);
      }
      panel9.add(panel1);
      
      // ---- tbt_ModeData ----
      tbt_ModeData.setToolTipText("Mode donn\u00e9e");
      tbt_ModeData.setIcon(new ImageIcon(getClass().getResource("/images/base-donnees-icone-6830-32.png")));
      tbt_ModeData.setSelected(true);
      tbt_ModeData.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeData.setName("tbt_ModeData");
      tbt_ModeData.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeDataActionPerformed(e);
        }
      });
      panel9.add(tbt_ModeData);
      
      // ---- tbt_ModeCondition ----
      tbt_ModeCondition.setIcon(new ImageIcon(getClass().getResource("/images/point-interrogation-utilisateur-icone-6379-32.png")));
      tbt_ModeCondition.setToolTipText("Mode condition");
      tbt_ModeCondition.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeCondition.setName("tbt_ModeCondition");
      tbt_ModeCondition.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeConditionActionPerformed(e);
        }
      });
      panel9.add(tbt_ModeCondition);
      
      // ---- tbt_ModeVariable ----
      tbt_ModeVariable.setIcon(new ImageIcon(getClass().getResource("/images/couleur-couleur-ligne-stylo-crayon-icone-8770-32.png")));
      tbt_ModeVariable.setToolTipText("Mode variable");
      tbt_ModeVariable.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeVariable.setName("tbt_ModeVariable");
      tbt_ModeVariable.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeVariableActionPerformed(e);
        }
      });
      panel9.add(tbt_ModeVariable);
      
      // ---- hSpacer1 ----
      hSpacer1.setName("hSpacer1");
      panel9.add(hSpacer1);
      
      // ======== panel5 ========
      {
        panel5.setName("panel5");
        panel5.setLayout(new VerticalLayout());
        
        // ---- label3 ----
        label3.setText("Libell\u00e9");
        label3.setName("label3");
        panel5.add(label3);
        
        // ---- l_Libelle ----
        l_Libelle.setText(" ");
        l_Libelle.setFont(new Font("DejaVu Sans", Font.BOLD, 12));
        l_Libelle.setBorder(LineBorder.createBlackLineBorder());
        l_Libelle.setName("l_Libelle");
        panel5.add(l_Libelle);
      }
      panel9.add(panel5);
      
      // ======== p_OutilsData ========
      {
        p_OutilsData.setName("p_OutilsData");
        p_OutilsData.setLayout(new FlowLayout(FlowLayout.RIGHT));
        
        // ---- bt_SelectionAuto ----
        bt_SelectionAuto.setIcon(new ImageIcon(getClass().getResource("/images/faire-icone-6352-32.png")));
        bt_SelectionAuto.setToolTipText("S\u00e9lection automatique");
        bt_SelectionAuto.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_SelectionAuto.setName("bt_SelectionAuto");
        bt_SelectionAuto.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_SelectionAutoActionPerformed(e);
          }
        });
        p_OutilsData.add(bt_SelectionAuto);
        
        // ---- bt_EffacerSelection ----
        bt_EffacerSelection.setToolTipText("Supprimer toutes les s\u00e9lections");
        bt_EffacerSelection
            .setIcon(new ImageIcon(getClass().getResource("/images/nettoyage-effacement-gomme-a-effacer-icone-6000-32.png")));
        bt_EffacerSelection.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bt_EffacerSelection.setName("bt_EffacerSelection");
        bt_EffacerSelection.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_EffacerSelectionActionPerformed(e);
          }
        });
        p_OutilsData.add(bt_EffacerSelection);
        
        // ---- chk_CreationAutoLabel ----
        chk_CreationAutoLabel.setText("<html>Cr\u00e9ation<br>auto<br>des labels</html>");
        chk_CreationAutoLabel.setName("chk_CreationAutoLabel");
        chk_CreationAutoLabel.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            chk_CreationAutoLabelActionPerformed(e);
          }
        });
        p_OutilsData.add(chk_CreationAutoLabel);
      }
      panel9.add(p_OutilsData);
      
      // ---- b_ApercuSansDescription ----
      b_ApercuSansDescription.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      b_ApercuSansDescription.setIcon(new ImageIcon(getClass().getResource("/images/document-preview-imprimer-icone-9540-32.png")));
      b_ApercuSansDescription.setToolTipText("Aper\u00e7u sans description");
      b_ApercuSansDescription.setName("b_ApercuSansDescription");
      b_ApercuSansDescription.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          b_ApercuSansDescriptionActionPerformed(e);
        }
      });
      panel9.add(b_ApercuSansDescription);
      
      // ---- l_PosCurseur ----
      l_PosCurseur.setText("Curseur");
      l_PosCurseur.setName("l_PosCurseur");
      panel9.add(l_PosCurseur);
    }
    contentPane.add(panel9, BorderLayout.NORTH);
    pack();
    setLocationRelativeTo(null);
    
    // ---- ta_PageCondition ----
    ta_PageCondition.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
    ta_PageCondition.setName("ta_PageCondition");
    
    // ======== p_OutilsCondition ========
    {
      p_OutilsCondition.setName("p_OutilsCondition");
      p_OutilsCondition.setLayout(new FlowLayout(FlowLayout.LEFT));
      
      // ---- l_ConditionSelectionne ----
      l_ConditionSelectionne.setName("l_ConditionSelectionne");
      p_OutilsCondition.add(l_ConditionSelectionne);
    }
    
    // ---- ta_PageVariable ----
    ta_PageVariable.setFont(new Font(Font.MONOSPACED, Font.PLAIN, 12));
    ta_PageVariable.setName("ta_PageVariable");
    
    // ======== p_OutilsVariable ========
    {
      p_OutilsVariable.setName("p_OutilsVariable");
      p_OutilsVariable.setLayout(new FlowLayout());
      
      // ---- l_VariableSelectionnee ----
      l_VariableSelectionnee.setText("text");
      l_VariableSelectionnee.setName("l_VariableSelectionnee");
      p_OutilsVariable.add(l_VariableSelectionnee);
    }
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(tbt_ModeData);
    buttonGroup1.add(tbt_ModeCondition);
    buttonGroup1.add(tbt_ModeVariable);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JScrollPane panel2;
  private JPanel panel8;
  private JPanel p_MargeHaute;
  private JPanel p_MargeDroite;
  private JPanel p_MargeGauche;
  private JPanel p_MargeBasse;
  private JTextArea ta_PageData;
  private JPanel panel9;
  private JLabel l_ConnexionPageEditor;
  private JPanel panel3;
  private JLabel label1;
  private JLabel l_NbrPages;
  private JPanel panel4;
  private JButton bt_Precedent;
  private JButton bt_Suivant;
  private JPanel panel1;
  private JLabel label2;
  private JSpinner sp_TaillePolice;
  private JToggleButton tbt_ModeData;
  private JToggleButton tbt_ModeCondition;
  private JToggleButton tbt_ModeVariable;
  private JPanel hSpacer1;
  private JPanel panel5;
  private JLabel label3;
  private JLabel l_Libelle;
  private JPanel p_OutilsData;
  private JButton bt_SelectionAuto;
  private JButton bt_EffacerSelection;
  private JCheckBox chk_CreationAutoLabel;
  private JButton b_ApercuSansDescription;
  private JLabel l_PosCurseur;
  private JTextArea ta_PageCondition;
  private JPanel p_OutilsCondition;
  private JLabel l_ConditionSelectionne;
  private JTextArea ta_PageVariable;
  private JPanel p_OutilsVariable;
  private JLabel l_VariableSelectionnee;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
