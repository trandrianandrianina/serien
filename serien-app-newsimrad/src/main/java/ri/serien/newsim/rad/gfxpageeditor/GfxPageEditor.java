/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxpageeditor;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXTaskPane;
import org.jdesktop.swingx.JXTaskPaneContainer;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.encodage.XMLTools;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.newsim.description.DescriptionCondition;
import ri.serien.newsim.description.DescriptionDocument;
import ri.serien.newsim.description.DescriptionPage;
import ri.serien.newsim.description.DescriptionVariable;
import ri.serien.newsim.rad.GfxRad;
import ri.serien.newsim.rad.UndoManager;
import ri.serien.newsim.rad.composant.GfxPage;
import ri.serien.newsim.rad.composant.GfxPropertyObject;
import ri.serien.newsim.rad.gfxspooleditor.GfxSpoolEditor;
import ri.serien.newsim.rad.outils.CalculDPI;
import ri.serien.newsim.rad.outils.RiFileChooser;
import ri.serien.newsim.rad.parametre.Preferences;
import ri.serien.newsim.rendu.GenerationFichierFO;
import ri.serien.newsim.rendu.GenerationFichierFinal;

/**
 * Description du plan de travail
 */
public class GfxPageEditor extends JFrame {
  // Constantes
  
  public static final String TITRE = "Editeur de page";
  
  // Variables
  private JPanel p_zonecreation = new JPanel(null);
  private GfxPage gfxpage = null;
  private Preferences prefs = null;
  private File dernierPath = null;
  private GfxPropertyObject p_PropertyObject = null;
  private DescriptionPage page = null;
  private File fichierddp = null;
  private String dossierTravail = null;
  private String nomFichier = "noname";
  private GfxSpoolEditor spoolEditorConnected = null;
  private ArrayList<DescriptionCondition> listeCondition = null;
  private ArrayList<DescriptionVariable> listeVariable = null;
  
  private static int numero = 0;
  private UndoManager undo = null;
  public boolean noSaveAuto = false;
  private boolean dspGrid = false;
  
  private int dpi_img = 72; // TODO à définir
  private int dpi_aff = 72; // TODO à définir
  
  private GfxRad rad = null;
  
  /**
   * Constructeur
   * @param rad
   * @param titre
   */
  // public GfxPageEditor(final GfxRad rad, String titre, String dossierTrav, DescriptionDocument doc)
  public GfxPageEditor(final GfxRad rad, String titre, FileNG fichierddp, Preferences prefs) {
    // Création du nom du fichier temporaire pour le Undo
    undo = new UndoManager(numero++);
    noSaveAuto = true;
    
    initComponents();
    this.rad = rad;
    
    this.prefs = prefs;
    dernierPath = new File(prefs.getDernierPath());
    
    // Initialisation du dossier de travail
    this.fichierddp = fichierddp;
    if (fichierddp == null) {
      setDossierTravail(null);
    }
    else {
      setDossierTravail(fichierddp.getParent());
    }
    
    // Ajout du panel des propriétés
    p_PropertyObject = new GfxPropertyObject(this);
    p_Gauche.add(p_PropertyObject, BorderLayout.NORTH);
    
    // Redimension de la fenêtre
    setPreferredSize(new Dimension(800, 700));
    chargementDDP(fichierddp);
    if (page == null) {
      gfxpage = new GfxPage(this);
    }
    else {
      gfxpage = new GfxPage(page, dpi_aff, dpi_img, this);
    }
    gfxpage.setInfos();
    gfxpage.setLabelPositionSouris(l_PositionSouris);
    chk_LienSpool.setSelected(gfxpage.isLienSpool());
    
    // Ajout de la zone de création
    p_zonecreation.setBackground(Color.gray);
    // On agrandit la zone de création afin de voir la page en entier
    resizeZoneCreation();
    p_zonecreation.add(gfxpage);
    gfxpage.setLocation(15, 15);
    add(new JScrollPane(p_zonecreation), BorderLayout.CENTER);
    
    // Initialisation de la fenêtre principale
    // xTaskPane1.setCollapsed(true);
    xTaskPane2.setCollapsed(true);
    
    setTitle(titre);
    setLocation(rad.getWidth() + 10, 0);
    
    // Initialisation des données
    majPanneau();
    // setLibelleImage(modele.pageCourante.getImage());
    
    // Affichage de la fenêtre
    setVisible(true);
    
    noSaveAuto = false;
  }
  
  /**
   * Charge le fichier de description de page (ddp)
   * @return
   */
  private boolean chargementDDP(FileNG fichier) {
    // Vérification
    if ((fichier == null) || (!fichier.exists())) {
      return false;
    }
    dernierPath = fichier.getParentFile();
    
    undo.copyOriginalFile(fichier);
    // Lecture du fichier ddp
    try {
      page = (DescriptionPage) XMLTools.decodeFromFile(fichier.getAbsolutePath());
      if (!XMLTools.getListeMsgExceptionDecoder().isEmpty()) {
        // Conversion des fichiers DDP pour la 2.14
        try {
          Constantes.convert213to214(fichier.getAbsolutePath(), "ri.serien.newsim.");
          page = (DescriptionPage) XMLTools.decodeFromFile(fichier.getAbsolutePath());
        }
        catch (Exception e1) {
          JOptionPane.showMessageDialog(null, e1.getMessage(), "Conversion fichier DDP", JOptionPane.ERROR_MESSAGE);
          return false;
        }
      }
    }
    catch (Exception e) {
      return false;
    }
    
    // On met à jour le dpi en fonction de l'image
    if (page.getImage() != null) {
      if (page.getImage().endsWith("svg")) {
        dpi_aff = 90;
        dpi_img = 90;
      }
      else {
        dpi_aff = 72;
        dpi_img = 72;
      }
    }
    
    return true;
  }
  
  /**
   * Retourne le dpi de la fenêtre (résultat pas juste apparemment)
   */
  public int getDpi() {
    Toolkit tk = Toolkit.getDefaultToolkit();
    return tk.getScreenResolution();
  }
  
  /**
   * Retourne la zone de création
   * @return
   */
  public GfxPage getGfxPage() {
    return gfxpage;
  }
  
  /**
   * Retourne le panel des propriétés de la sélection
   * @return
   */
  public GfxPropertyObject getPropertyObject() {
    return p_PropertyObject;
  }
  
  /**
   * Retourne l'éditeur de spool lié au plan de travail
   * @return
   */
  public GfxSpoolEditor getSpoolEditor() {
    return spoolEditorConnected;
  }
  
  /**
   * Retourne la liste des conditions
   * @return
   */
  public ArrayList<DescriptionCondition> getListeCondition() {
    if (listeCondition == null) {
      listeCondition = new ArrayList<DescriptionCondition>();
    }
    return listeCondition;
  }
  
  /**
   * Initialise la liste des conditions
   * @return
   */
  public void setListeCondition(ArrayList<DescriptionCondition> condition) {
    listeCondition = condition;
  }
  
  /**
   * Retourne la liste des variables
   * @return
   */
  public ArrayList<DescriptionVariable> getListeVariable() {
    if (listeVariable == null) {
      listeVariable = new ArrayList<DescriptionVariable>();
    }
    return listeVariable;
  }
  
  /**
   * Initialise la liste des variables
   * @return
   */
  public void setListeVariable(ArrayList<DescriptionVariable> variable) {
    listeVariable = variable;
  }
  
  /**
   * Mise à jour des données (en cm) du panneau contenant les caractéristiques de la description
   */
  public void majPanneau() {
    // Largeur & hauteur de la page
    sp_LargeurPage.setValue(Float.valueOf(gfxpage.getLargeurPage()));
    sp_HauteurPage.setValue(Float.valueOf(gfxpage.getHauteurPage()));
    
    // Marge de la page
    sp_MargeX.setValue(Float.valueOf(gfxpage.getMargeXPage()));
    // l_MargeXcm.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img, gfxpage.getMargeYPage()) + " px");
    sp_MargeY.setValue(Float.valueOf(gfxpage.getMargeYPage()));
    // l_MargeYcm.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeYPage()) + " px");
    
    // Nombre de ligne & colonne
    sp_NbrColonnes.setValue(Integer.valueOf(gfxpage.getNbrEtiquetteSurLargeur()));
    sp_NbrLignes.setValue(Integer.valueOf(gfxpage.getNbrEtiquetteSurHauteur()));
    
    // Marge d'une étiquette
    sp_MargeXEtq.setValue(Float.valueOf(gfxpage.getMargeXEtiquette()));
    // l_MargeXEtq.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeXEtiquette()) + " px");
    sp_MargeYEtq.setValue(Float.valueOf(gfxpage.getMargeYEtiquette()));
    // l_MargeYEtq.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeYEtiquette()) + " px");
    
    // Largeur & hauteur d'une étiquette
    sp_LargeurEtq.setValue(Float.valueOf(gfxpage.getEtiquette().getLargeurEtiquette()));
    sp_HauteurEtq.setValue(Float.valueOf(gfxpage.getEtiquette().getHauteurEtiquette()));
    
    // Portrait / Paysage
    rb_Portrait.setSelected(gfxpage.isPortrait());
    rb_Paysage.setSelected(!gfxpage.isPortrait());
    
    // Dpi pour Svg & Bitmap
    tf_Dpi.setText("" + dpi_aff);
  }
  
  /**
   * Détermine le mode dans lequel on se trouve
   */
  public void setMode(int mode) {
    switch (mode) {
      case GfxPage.MODE_DEPLACEMENT:
        tbt_ModeDeplacement.setSelected(true);
        break;
      case GfxPage.MODE_LABEL:
        tbt_ModeLabel.setSelected(true);
        break;
      case GfxPage.MODE_CODEBARRE:
        tbt_ModeCodeBarre.setSelected(true);
        break;
      case GfxPage.MODE_IMAGE:
        tbt_ModeImage.setSelected(true);
        break;
    }
  }
  
  /**
   * On redimensionne le panel afin de voir la page en entier
   */
  public void resizeZoneCreation() {
    if (gfxpage != null) {
      p_zonecreation.setPreferredSize(new Dimension(gfxpage.getWidth() + 30, gfxpage.getHeight() + 30));
    }
  }
  
  /**
   * Création d'un label suite à la création d'une zone dans le spool
   * @param libelle
   */
  public void creationAutoLabel(int x_lb, int y_lb, String libelle, Dimension spool) {
    if (gfxpage == null) {
      return;
    }
    // Recalcul des coordonnées pour que ce soit adapté à la page
    int pos_x = (x_lb * gfxpage.getEtiquette().getPreferredSize().width) / spool.width;
    int pos_y = (y_lb * gfxpage.getEtiquette().getPreferredSize().height) / spool.height;
    
    gfxpage.getEtiquette().creationLabel(pos_x, pos_y, libelle);
  }
  
  /**
   * Détermine le mode dans lequel on se trouve
   */
  private void initMode() {
    if (tbt_ModeDeplacement.isSelected()) {
      gfxpage.getEtiquette().setMode(GfxPage.MODE_DEPLACEMENT);
    }
    else {
      if (tbt_ModeLabel.isSelected()) {
        gfxpage.getEtiquette().setMode(GfxPage.MODE_LABEL);
      }
      else {
        if (tbt_ModeImage.isSelected()) {
          gfxpage.getEtiquette().setMode(GfxPage.MODE_IMAGE);
        }
        else {
          if (tbt_ModeCodeBarre.isSelected()) {
            gfxpage.getEtiquette().setMode(GfxPage.MODE_CODEBARRE);
          }
          else {
            gfxpage.getEtiquette().setMode(-1);
          }
        }
      }
    }
  }
  
  /**
   * Passe de Portrait à Paysage et inversement
   */
  private void flipflopPortraitPaysage() {
    gfxpage.initPreparation(rb_Portrait.isSelected());
    majPanneau();
    resizeZoneCreation();
    validate();
    repaint();
  }
  
  /**
   * Gestion du Undo ou du Redo
   */
  private void undoAction(boolean forundo) {
    noSaveAuto = true;
    
    // Récupération des données précédentes
    page = undo.action(forundo);
    bt_Undo.setEnabled(undo.isEnableUndoButton());
    bt_Redo.setEnabled(undo.isEnableRedoButton());
    
    // Rafraichissement du panel avec les nouvelles données
    p_zonecreation.remove(gfxpage);
    if (page == null) {
      gfxpage = new GfxPage(this);
    }
    else {
      gfxpage = new GfxPage(page, dpi_aff, dpi_img, this);
    }
    p_zonecreation.add(gfxpage);
    gfxpage.setInfos();
    gfxpage.setLocation(15, 15);
    
    gfxpage.validate();
    gfxpage.repaint();
    
    noSaveAuto = false;
  }
  
  /**
   * Ouvre la fenêtre de sauvegarde de la description de la page
   * @return
   * 
   *         private String saveDDP(boolean affichedialog)
   *         {
   *         String chemin = null;
   * 
   *         // On vérifie qu'on ait bien un nom de fichier
   *         if ((!affichedialog) && (!dernierPath.getName().endsWith(DescriptionPage.EXTENSION)))
   *         if (getTitle().endsWith(DescriptionPage.EXTENSION))
   *         dernierPath = new File(getTitle());
   *         else
   *         affichedialog = true;
   * 
   *         if (dernierPath.isDirectory()) affichedialog = true;
   *         else
   *         chemin = dernierPath.getAbsolutePath();
   * 
   *         // Affiche la boite de dialogue si besoin
   *         if (affichedialog)
   *         {
   *         final JFileChooser choixFichier = new JFileChooser();
   *         //choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
   * 
   *         choixFichier.addChoosableFileFilter(new GestionFiltre(new String[]{"ddp"},
   *         "Fichier description de page (*.ddp)"));
   *         // On se place dans le dernier dossier courant s'il y en a un
   *         if (dernierPath != null)
   *         choixFichier.setCurrentDirectory(dernierPath);
   * 
   *         // Récupération du chemin
   *         if (choixFichier.showSaveDialog(this) == JFileChooser.APPROVE_OPTION)
   *         chemin = choixFichier.getSelectedFile().getPath();
   *         else
   *         return null;
   * 
   *         // On stocke comme le dossier courant
   *         dernierPath = choixFichier.getSelectedFile();//getCurrentDirectory();
   *         }
   *         else
   *         dernierPath = new File(getTitle());
   * 
   *         // Vérifie qu'il ait bien l'extension
   *         if (chemin.lastIndexOf('.') == -1)
   *         chemin = chemin + DescriptionPage.EXTENSION;
   *         setTitle(chemin);
   * 
   *         // Génération du fichier XML
   *         try
   *         {
   *         page = gfxpage.getDescriptionPage();
   *         page.setNomFichierDDP(new File(chemin).getName());
   *         XMLTools.encodeToFile(page, chemin);
   *         }
   *         catch (Exception ex)
   *         {
   *         ex.printStackTrace();
   *         }
   * 
   *         return chemin;
   *         }
   */
  /**
   * Ouvre la fenêtre de sauvegarde de la description de page.
   */
  private String saveDDP(boolean affichedialog) {
    if (fichierddp == null) {
      fichierddp = dernierPath;
    }
    RiFileChooser selectionFichier = new RiFileChooser(this, fichierddp);
    
    // On vérifie qu'on ait bien un nom de fichier
    if (!affichedialog && !fichierddp.getName().endsWith(DescriptionPage.EXTENSION)) {
      affichedialog = true;
    }
    
    if (fichierddp.isDirectory()) {
      affichedialog = true;
    }
    
    // Affiche la boite de dialogue si besoin
    if (affichedialog) {
      fichierddp = selectionFichier.enregistreFichier(fichierddp, "ddp", "Fichier description de page (*.ddp)");
      if (fichierddp == null) {
        return null;
      }
    }
    
    // Vérifie qu'il ait bien l'extension
    if (fichierddp.getName().lastIndexOf('.') == -1) {
      fichierddp = new File(fichierddp.getAbsolutePath() + DescriptionPage.EXTENSION);
    }
    
    setDossierTravail(fichierddp.getParent());
    
    return save(false);
  }
  
  /**
   * Sauvegarde du fichier DDP.
   */
  public String save(final boolean forundo) {
    if (noSaveAuto) {
      return null;
    }
    
    new Thread() {
      @Override
      public void run() {
        // Génération du fichier XML
        try {
          page = gfxpage.getDescriptionPage();
          if (forundo) {
            undo.save();
            XMLTools.encodeToFile(page, undo.getPathFileName().getAbsolutePath());
          }
          else {
            page.setNomFichierDDP(fichierddp.getName());
            XMLTools.encodeToFile(page, fichierddp.getAbsolutePath());
          }
        }
        catch (Exception ex) {
          ex.printStackTrace();
        }
        
        if (forundo) {
          bt_Undo.setEnabled(undo.isEnableUndoButton());
          bt_Redo.setEnabled(undo.isEnableRedoButton());
        }
        else {
          if (prefs != null) {
            prefs.setDernierPath(fichierddp.getParent());
          }
        }
      }
    }.start();
    
    if (forundo) {
      return null;
    }
    else {
      return fichierddp.getAbsolutePath();
    }
  }
  
  /**
   * Recherche une image pour le fond du panel
   */
  private String choisirImage() {
    RiFileChooser choixFichier = new RiFileChooser(this, dernierPath);
    String chemin = choixFichier.choisirFichier("svg", "Fichier image SVG (*.svg)", true);
    dernierPath = choixFichier.getDossierFile();
    
    return chemin;
  }
  
  /**
   * @param modele the modele to set
   * 
   *          public void setModele(DescriptionDocument modele)
   *          {
   *          this.modele = modele;
   *          }
   * 
   *          /**
   * @return the modele
   * 
   *         public DescriptionDocument getModele()
   *         {
   *         return modele;
   *         }
   */
  
  /**
   * @param dossierTravail the dossierTravail to set
   */
  public void setDossierTravail(String dossierTravail) {
    if (dossierTravail == null) {
      File retour = null;
      do {
        RiFileChooser choixdossier = new RiFileChooser(this, dossierTravail);
        retour = choixdossier.choisirDossier(null);
        if (retour != null) {
          dossierTravail = retour.getAbsolutePath();
        }
      }
      while (retour == null);
    }
    this.dossierTravail = dossierTravail;
  }
  
  /**
   * @return the dossierTravail
   */
  public String getDossierTravail() {
    return dossierTravail;
  }
  
  /**
   * Met à jour le libellé du TitleBorder du panel Image
   * @param image
   */
  public void setLibelleImage(String fichier) {
    if (fichier == null) {
      ((TitledBorder) panel9.getBorder()).setTitle("Pas de fond de page");
    }
    else {
      ((TitledBorder) panel9.getBorder()).setTitle(fichier);
    }
    panel9.repaint();
  }
  
  /**
   * Remplit la combobox avec la liste des Spool Editor ouvert
   */
  public void initListeSpoolEditor() {
    String[] listeSpool = null;
    HashMap<?, ?> listeSpoolEditor = null;
    
    // On vérifie que le manager de la liste des spools existe
    if (rad.getListeSpoolEditor() != null) {
      listeSpoolEditor = rad.getListeSpoolEditor().getHashMap();
    }
    
    if (listeSpoolEditor != null) {
      listeSpool = new String[listeSpoolEditor.size() + 1];
      int i = 1;
      boolean trouve = false;
      
      // On balaye la liste des éditeur de spool afin de récupérer leur nom
      for (Entry<?, ?> e : listeSpoolEditor.entrySet()) {
        listeSpool[i++] = (String) e.getValue();
        // On vérifie que l'éditeur sur lequel on est connecté est toujours là
        if ((!trouve) && (e.getKey() == spoolEditorConnected)) {
          trouve = true;
        }
      }
      if (trouve) {
        spoolEditorConnected = null; // Car l'objet est enlevé de la liste après l'évènement (voir HashmapManager)
      }
    }
    else {
      listeSpool = new String[1];
      spoolEditorConnected = null;
    }
    listeSpool[0] = "Non connecté";
    cb_ListeSpoolEditor.setModel(new DefaultComboBoxModel(listeSpool));
    cb_ListeSpoolEditor.setEditable(false);
  }
  
  /**
   * Grise ou pas les objets si on est connecté ou pas à un spool éditor
   */
  public void enableActionSpoolEditor() {
    bt_Export.setEnabled(spoolEditorConnected != null);
    bt_Preview.setEnabled(spoolEditorConnected != null);
  }
  
  /**
   * Génère l'aperçu de la page courante.
   */
  private void previewPageCourante(EnumTypeDocument pTypeDocument) {
    if (spoolEditorConnected == null) {
      return;
    }
    
    // On vérifie que le travail est bien était enregistré
    if (page == null) {
      JOptionPane.showMessageDialog(null, "Veuillez enregistrer d'abord votre travail.", "Pré-visualisation de la page",
          JOptionPane.WARNING_MESSAGE);
      return;
    }
    
    // Récupération de la page courante du spool depuis le SpoolEditor
    String[] pagespool = spoolEditorConnected.getPageCouranteSpool();
    if (pagespool == null) {
      return;
    }
    
    // Création d'une description de document temporaire juste pour la préview
    previewDocument(pagespool, pTypeDocument);
  }
  
  /**
   * Génère l'aperçu du document complet
   */
  private void previewDocument(String[] pages, EnumTypeDocument pTypeDocument) {
    if (pages == null) {
      return;
    }
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    
    // Conversion du dessin vers le modèle
    // Créé un modèle temporaire
    DescriptionDocument doc = new DescriptionDocument();
    // Données sur le modèle
    doc.setNom(nomFichier);
    doc.setSource_dpi(dpi_aff);
    
    // Données pour une page (à améliorer pour la gestion du multi-page)
    doc.getSequence().clear(); // A virer plus tard
    DescriptionPage pagecourante = gfxpage.getDescriptionPage();
    doc.addSequence(pagecourante);
    
    // Génération du doc
    GenerationFichierFO gffo = new GenerationFichierFO(doc, pages, getDossierTravail());
    gffo.setListeVariables(new ArrayList<DescriptionVariable>());
    gffo.genereFOtoFile();
    
    // Rendu final
    GenerationFichierFinal gff = new GenerationFichierFinal();
    gff.setFichierEntree(gffo.getNomFichierFO());
    gff.setTypeSortie(pTypeDocument);
    String rendu = gff.renduFinal(doc.getSource_dpi(), getTargetDpi(doc, pTypeDocument));
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    // On lance l'application associée
    if (rendu == null) {
      return;
    }
    try {
      setCursor(new Cursor(Cursor.WAIT_CURSOR));
      Desktop.getDesktop().open(new File(rendu));
    }
    catch (IOException e1) {
    }
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }
  
  /**
   * Retourne le dpi en fonction de l'extension (format de sortie).
   */
  private int getTargetDpi(DescriptionDocument doc, EnumTypeDocument pTypeDocument) {
    // Valeur par défaut si paramètres incorrects
    if (doc == null || pTypeDocument == null) {
      return 72;
    }
    
    // Cas normal
    // for (int i=0; i<doc.getCible_format().length; i++)
    // if (doc.getCible_format()[i].equals(extension))
    // return doc.getCible_dpi()[i];
    
    return 72;
  }
  
  /**
   * Opération à effectuer lors de la fermeture de la fenêtre
   */
  private void fermeture() {
    setVisible(false);
    if (spoolEditorConnected != null) {
      spoolEditorConnected.setConnected(false);
      spoolEditorConnected = null;
    }
    
    // Suppression des fichiers temporaires (du Undo)
    undo.clearTemporyFiles();
    
    dispose();
  }
  
  /**
   * Lors du changement de DPi de l'image
   */
  public void setDpiImage(int dpi) {
    if (dpi != -1) {
      dpi_img = dpi;
      tf_Dpi.setText("" + dpi);
    }
    else {
      dpi_img = Integer.parseInt(tf_Dpi.getText().trim());
      gfxpage.setDpiImg(dpi_img);
    }
  }
  
  public void setDpiAffichage(int dpi) {
    if (dpi != -1) {
      dpi_aff = dpi;
      tf_DpiAff.setText("" + dpi);
    }
    else {
      dpi_aff = Integer.parseInt(tf_DpiAff.getText().trim());
      gfxpage.setDpiAff(dpi_aff);
    }
  }
  
  // ------------------------------------ Evènements -------------------------
  
  /**
   * @return le dernierPath
   */
  public File getDernierPath() {
    return dernierPath;
  }
  
  /**
   * @param dernierPath le dernierPath à définir
   */
  public void setDernierPath(File dernierPath) {
    this.dernierPath = dernierPath;
  }
  
  /**
   * @return le dspGrid
   */
  public boolean isDspGrid() {
    return dspGrid;
  }
  
  /**
   * @param dspGrid le dspGrid à définir
   */
  public void setDspGrid(boolean dspGrid) {
    this.dspGrid = dspGrid;
  }
  
  private void rb_PortraitActionPerformed(ActionEvent e) {
    flipflopPortraitPaysage();
  }
  
  private void rb_PaysageActionPerformed(ActionEvent e) {
    flipflopPortraitPaysage();
  }
  
  private void sp_MargeXStateChanged(ChangeEvent e) {
    if (gfxpage.getMargeXPage() != ((Float) sp_MargeX.getValue()).floatValue()) {
      gfxpage.setMargeXPage(((Float) sp_MargeX.getValue()).floatValue());
      // l_MargeXcm.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeXPage()) + " px");
      // ReCalcul de la largeur de l'étiquette en fonction de la marge
      gfxpage.getEtiquette().setLargeurEtiquette(
          (gfxpage.getLargeurPage() - gfxpage.getMargeXPage() - gfxpage.getNbrEtiquetteSurLargeur() * gfxpage.getMargeXEtiquette())
              / gfxpage.getNbrEtiquetteSurLargeur());
      // Repositionnement du panel etiquette
      gfxpage.getEtiquette().setBounds(CalculDPI.getCmtoPx(dpi_aff, dpi_img, gfxpage.getMargeXPage()), gfxpage.getEtiquette().getY(),
          CalculDPI.getCmtoPx(dpi_aff, dpi_img, gfxpage.getEtiquette().getLargeurEtiquette()), gfxpage.getEtiquette().getHeight());
      majPanneau();
      gfxpage.repaint();
    }
  }
  
  private void sp_MargeYStateChanged(ChangeEvent e) {
    if (gfxpage.getMargeYPage() != ((Float) sp_MargeY.getValue()).floatValue()) {
      gfxpage.setMargeYPage(((Float) sp_MargeY.getValue()).floatValue());
      // l_MargeYcm.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeYPage()) + " px");
      // ReCalcul de la hauteur de l'étiquette en fonction de la marge
      gfxpage.getEtiquette().setHauteurEtiquette(
          (gfxpage.getHauteurPage() - gfxpage.getMargeYPage() - gfxpage.getNbrEtiquetteSurHauteur() * gfxpage.getMargeYEtiquette())
              / gfxpage.getNbrEtiquetteSurHauteur());
      // Repositionnement du panel etiquette
      gfxpage.getEtiquette().setBounds(gfxpage.getEtiquette().getX(), CalculDPI.getCmtoPx(dpi_aff, dpi_img, gfxpage.getMargeYPage()),
          gfxpage.getEtiquette().getWidth(), CalculDPI.getCmtoPx(dpi_aff, dpi_img, gfxpage.getEtiquette().getHauteurEtiquette()));
      majPanneau();
      gfxpage.repaint();
    }
  }
  
  private void sp_NbrLignesStateChanged(ChangeEvent e) {
    if (gfxpage.getNbrEtiquetteSurHauteur() != ((Integer) sp_NbrLignes.getValue()).intValue()) {
      gfxpage.setNbrEtiquetteSurHauteur(((Integer) sp_NbrLignes.getValue()).intValue());
      float hauteurEtiquette =
          (gfxpage.getHauteurPage() - gfxpage.getMargeYPage() - (gfxpage.getMargeYEtiquette() * gfxpage.getNbrEtiquetteSurHauteur()))
              / gfxpage.getNbrEtiquetteSurHauteur();
      gfxpage.getEtiquette().setHauteurEtiquette(hauteurEtiquette);
      majPanneau();
      gfxpage.repaint();
    }
  }
  
  private void sp_NbrColonnesStateChanged(ChangeEvent e) {
    if (gfxpage.getNbrEtiquetteSurLargeur() != ((Integer) sp_NbrColonnes.getValue()).intValue()) {
      gfxpage.setNbrEtiquetteSurLargeur(((Integer) sp_NbrColonnes.getValue()).intValue());
      float largeurEtiquette =
          (gfxpage.getLargeurPage() - gfxpage.getMargeXPage() - (gfxpage.getMargeXEtiquette() * gfxpage.getNbrEtiquetteSurLargeur()))
              / gfxpage.getNbrEtiquetteSurLargeur();
      gfxpage.getEtiquette().setLargeurEtiquette(largeurEtiquette);
      majPanneau();
      gfxpage.repaint();
    }
  }
  
  private void sp_LargeurPageStateChanged(ChangeEvent e) {
    if (gfxpage.getLargeurPage() != ((Float) sp_LargeurPage.getValue()).floatValue()) {
      gfxpage.setLargeurPage(((Float) sp_LargeurPage.getValue()).floatValue());
      gfxpage.getEtiquette().setLargeurEtiquette(((Float) sp_LargeurPage.getValue()).floatValue());
      majPanneau();
      gfxpage.initPreparation(rb_Portrait.isSelected());
      gfxpage.validate();
      gfxpage.repaint();
    }
  }
  
  private void sp_HauteurPageStateChanged(ChangeEvent e) {
    if (gfxpage.getHauteurPage() != ((Float) sp_HauteurPage.getValue()).floatValue()) {
      gfxpage.setHauteurPage(((Float) sp_HauteurPage.getValue()).floatValue());
      gfxpage.getEtiquette().setHauteurEtiquette(((Float) sp_HauteurPage.getValue()).floatValue());
      majPanneau();
      gfxpage.initPreparation(rb_Portrait.isSelected());
      gfxpage.validate();
      gfxpage.repaint();
    }
  }
  
  private void sp_MargeXEtqStateChanged(ChangeEvent e) {
    if (gfxpage.getMargeXEtiquette() != ((Float) sp_MargeXEtq.getValue()).floatValue()) {
      gfxpage.setMargeXEtiquette(((Float) sp_MargeXEtq.getValue()).floatValue());
      // l_MargeXEtq.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeXEtiquette()) + " px");
      // ReCalcul de la largeur de l'étiquette en fonction de la marge
      gfxpage.getEtiquette().setLargeurEtiquette(
          (gfxpage.getLargeurPage() - gfxpage.getMargeXPage() - gfxpage.getNbrEtiquetteSurLargeur() * gfxpage.getMargeXEtiquette())
              / gfxpage.getNbrEtiquetteSurLargeur());
      majPanneau();
      gfxpage.repaint();
    }
  }
  
  private void sp_MargeYEtqStateChanged(ChangeEvent e) {
    if (gfxpage.getMargeYEtiquette() != ((Float) sp_MargeYEtq.getValue()).floatValue()) {
      gfxpage.setMargeYEtiquette(((Float) sp_MargeYEtq.getValue()).floatValue());
      // l_MargeYEtq.setText(CalculDPI.getCmtoPx(dpi_aff, dpi_img,gfxpage.getMargeYEtiquette()) + " px");
      // ReCalcul de la hauteur de l'étiquette en fonction de la marge
      gfxpage.getEtiquette().setHauteurEtiquette(
          (gfxpage.getHauteurPage() - gfxpage.getMargeYPage() - gfxpage.getNbrEtiquetteSurHauteur() * gfxpage.getMargeYEtiquette())
              / gfxpage.getNbrEtiquetteSurHauteur());
      majPanneau();
      gfxpage.repaint();
    }
  }
  
  private void sp_LargeurEtqStateChanged(ChangeEvent e) {
    if (gfxpage.getEtiquette().getLargeurEtiquette() != ((Float) sp_LargeurEtq.getValue()).floatValue()) {
      gfxpage.getEtiquette().setLargeurEtiquette(((Float) sp_LargeurEtq.getValue()).floatValue());
      gfxpage.repaint();
    }
  }
  
  private void sp_HauteurEtqStateChanged(ChangeEvent e) {
    if (gfxpage.getEtiquette().getHauteurEtiquette() != ((Float) sp_HauteurEtq.getValue()).floatValue()) {
      gfxpage.getEtiquette().setHauteurEtiquette(((Float) sp_HauteurEtq.getValue()).floatValue());
      gfxpage.repaint();
    }
  }
  
  private void bt_SaveActionPerformed(ActionEvent e) {
    // enregistreModele(false);
    saveDDP(false);
  }
  
  private void bt_SaveAsActionPerformed(ActionEvent e) {
    // enregistreModele(true);
    saveDDP(true);
  }
  
  private void bt_ExportActionPerformed(ActionEvent e) {
    if (spoolEditorConnected != null) {
      spoolEditorConnected.importerDescriptionPage(gfxpage.getDescriptionPage());
    }
  }
  
  private void bt_ChoisirImageActionPerformed(ActionEvent e) {
    String image = choisirImage();
    if (image == null) {
      return;
    }
    
    // Modification dynamique du dpi de l'image en fonction du type d'image
    if (image.toLowerCase().endsWith(".svg")) {
      tf_Dpi.setText("90");
      tf_DpiAff.setText("90");
    }
    else {
      tf_Dpi.setText("72");
      tf_DpiAff.setText("72");
    }
    
    setDpiImage(-1);
    setDpiAffichage(-1);
    
    File fichier = new File(image);
    setDossierTravail(fichier.getParent());
    // gfxpage.setDossierTravail(fichier.getParent());
    gfxpage.setCheminImageFond(fichier.getName());
  }
  
  private void bt_SansImageActionPerformed(ActionEvent e) {
    gfxpage.setCheminImageFond(null);
  }
  
  private void tbt_ModeDeplacementActionPerformed(ActionEvent e) {
    initMode();
  }
  
  private void tbt_ModeLabelActionPerformed(ActionEvent e) {
    initMode();
  }
  
  private void tbt_ModeImageActionPerformed(ActionEvent e) {
    initMode();
  }
  
  private void tbt_ModeCodeBarreActionPerformed(ActionEvent e) {
    initMode();
  }
  
  private void cb_ListeSpoolEditorActionPerformed(ActionEvent e) {
    if (rad.getListeSpoolEditor() == null) {
      return;
    }
    // On se déconnecte à l'éditeur de spool actuel
    if (spoolEditorConnected != null) {
      spoolEditorConnected.setConnected(false);
      spoolEditorConnected = null;
    }
    
    // On se connecte au nouveau
    int index = cb_ListeSpoolEditor.getSelectedIndex();
    if (index != 0) {
      spoolEditorConnected = (GfxSpoolEditor) rad.getListeSpoolEditor().getKeyAtIndex(index - 1);
      spoolEditorConnected.setConnected(true);
      spoolEditorConnected.setListeCondition(getListeCondition());
      spoolEditorConnected.setListeVariable(getListeVariable());
      spoolEditorConnected.setPageEditor(this);
    }
    
    // On active ou pas le bouton d'exportation
    enableActionSpoolEditor();
  }
  
  private void bt_PreviewActionPerformed(ActionEvent e) {
    // Désactivation du choix des différents formats car cela n'a pas d'intéret pour l'instant
    // pm_Preview.show(bt_Preview, 0, bt_Preview.getHeight());
    previewPageCourante(EnumTypeDocument.PDF);
  }
  
  private void mi_PPCpdfActionPerformed(ActionEvent e) {
    previewPageCourante(EnumTypeDocument.PDF);
  }
  
  private void mi_PPCpsActionPerformed(ActionEvent e) {
    previewPageCourante(EnumTypeDocument.POSTSCRIPT);
  }
  
  private void mi_PPCpngActionPerformed(ActionEvent e) {
    previewPageCourante(EnumTypeDocument.PNG);
  }
  
  private void mi_PPCtiffActionPerformed(ActionEvent e) {
    previewPageCourante(EnumTypeDocument.TIFF);
  }
  
  private void thisWindowClosing(WindowEvent e) {
    fermeture();
  }
  
  private void tf_DpiFocusLost(FocusEvent e) {
    setDpiImage(-1);
  }
  
  private void tf_DpiAffFocusLost(FocusEvent e) {
    setDpiAffichage(-1);
  }
  
  private void chk_LienSpoolActionPerformed(ActionEvent e) {
    gfxpage.setLienSpool(chk_LienSpool.isSelected());
  }
  
  private void bt_UndoActionPerformed(ActionEvent e) {
    undoAction(true);
  }
  
  private void bt_RedoActionPerformed(ActionEvent e) {
    undoAction(false);
  }
  
  private void bt_GridActionPerformed(ActionEvent e) {
    setDspGrid(!dspGrid);
    validate();
    repaint();
  }
  
  // @SuppressWarnings("deprecation")
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_Haut = new JPanel();
    bt_Save = new JButton();
    bt_SaveAs = new JButton();
    hSpacer1 = new JPanel(null);
    bt_Undo = new JButton();
    bt_Redo = new JButton();
    hSpacer3 = new JPanel(null);
    bt_Grid = new JButton();
    hSpacer4 = new JPanel(null);
    tbt_ModeDeplacement = new JToggleButton();
    tbt_ModeLabel = new JToggleButton();
    tbt_ModeImage = new JToggleButton();
    tbt_ModeCodeBarre = new JToggleButton();
    hSpacer2 = new JPanel(null);
    cb_ListeSpoolEditor = new JComboBox();
    bt_Export = new JButton();
    bt_Preview = new JButton();
    p_Gauche = new JPanel();
    scrollPane1 = new JScrollPane();
    xTaskPaneContainer1 = new JXTaskPaneContainer();
    xTaskPane1 = new JXTaskPane();
    panel4 = new JPanel();
    rb_Portrait = new JRadioButton();
    rb_Paysage = new JRadioButton();
    chk_LienSpool = new JCheckBox();
    panel8 = new JPanel();
    label10 = new JLabel();
    label11 = new JLabel();
    sp_LargeurPage = new JSpinner();
    sp_HauteurPage = new JSpinner();
    label12 = new JLabel();
    label13 = new JLabel();
    panel3 = new JPanel();
    label2 = new JLabel();
    label3 = new JLabel();
    sp_MargeX = new JSpinner();
    l_MargeXcm = new JLabel();
    sp_MargeY = new JSpinner();
    l_MargeYcm = new JLabel();
    panel9 = new JPanel();
    bt_ChoisirImage = new JButton();
    bt_SansImage = new JButton();
    label16 = new JLabel();
    tf_DpiAff = new JTextField();
    l_Dpi = new JLabel();
    tf_Dpi = new JTextField();
    xTaskPane2 = new JXTaskPane();
    panel7 = new JPanel();
    label8 = new JLabel();
    label9 = new JLabel();
    sp_MargeXEtq = new JSpinner();
    sp_MargeYEtq = new JSpinner();
    l_MargeXEtq = new JLabel();
    l_MargeYEtq = new JLabel();
    panel5 = new JPanel();
    label4 = new JLabel();
    label5 = new JLabel();
    sp_NbrLignes = new JSpinner();
    sp_NbrColonnes = new JSpinner();
    panel6 = new JPanel();
    label6 = new JLabel();
    label7 = new JLabel();
    sp_LargeurEtq = new JSpinner();
    sp_HauteurEtq = new JSpinner();
    label14 = new JLabel();
    label15 = new JLabel();
    p_Bas = new JPanel();
    label1 = new JLabel();
    l_PositionSouris = new JLabel();
    pm_Preview = new JPopupMenu();
    mi_PPCpdf = new JMenuItem();
    mi_PPCps = new JMenuItem();
    mi_PPCtiff = new JMenuItem();
    mi_PPCpng = new JMenuItem();
    CellConstraints cc = new CellConstraints();
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== p_Haut ========
    {
      p_Haut.setMinimumSize(new Dimension(0, 0));
      p_Haut.setName("p_Haut");
      p_Haut.setLayout(new FlowLayout(FlowLayout.LEFT));
      
      // ---- bt_Save ----
      bt_Save.setIcon(new ImageIcon(getClass().getResource("/images/3floppy-enregistrer-demonter-icone-7773-32.png")));
      bt_Save.setToolTipText("Enregistrer");
      bt_Save.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Save.setPreferredSize(new Dimension(50, 50));
      bt_Save.setName("bt_Save");
      bt_Save.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_SaveActionPerformed(e);
        }
      });
      p_Haut.add(bt_Save);
      
      // ---- bt_SaveAs ----
      bt_SaveAs.setIcon(new ImageIcon(getClass().getResource("/images/3floppy-enregistrer-sous-icone-7773-32.png")));
      bt_SaveAs.setToolTipText("Enregistrer sous");
      bt_SaveAs.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_SaveAs.setPreferredSize(new Dimension(50, 50));
      bt_SaveAs.setName("bt_SaveAs");
      bt_SaveAs.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_SaveAsActionPerformed(e);
        }
      });
      p_Haut.add(bt_SaveAs);
      
      // ---- hSpacer1 ----
      hSpacer1.setName("hSpacer1");
      p_Haut.add(hSpacer1);
      
      // ---- bt_Undo ----
      bt_Undo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Undo.setEnabled(false);
      bt_Undo.setIcon(new ImageIcon(getClass().getResource("/images/Undo-32.png")));
      bt_Undo.setToolTipText("D\u00e9faire la derni\u00e8re op\u00e9ration");
      bt_Undo.setName("bt_Undo");
      bt_Undo.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_UndoActionPerformed(e);
        }
      });
      p_Haut.add(bt_Undo);
      
      // ---- bt_Redo ----
      bt_Redo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Redo.setEnabled(false);
      bt_Redo.setToolTipText("Refaire la derni\u00e8re op\u00e9ration");
      bt_Redo.setIcon(new ImageIcon(getClass().getResource("/images/Redo-32.png")));
      bt_Redo.setName("bt_Redo");
      bt_Redo.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_RedoActionPerformed(e);
        }
      });
      p_Haut.add(bt_Redo);
      
      // ---- hSpacer3 ----
      hSpacer3.setName("hSpacer3");
      p_Haut.add(hSpacer3);
      
      // ---- bt_Grid ----
      bt_Grid.setToolTipText("Affiche ou cache la grille (trame de 0,5 cm)");
      bt_Grid.setIcon(new ImageIcon(getClass().getResource("/images/grid-32.png")));
      bt_Grid.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Grid.setName("bt_Grid");
      bt_Grid.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_GridActionPerformed(e);
        }
      });
      p_Haut.add(bt_Grid);
      
      // ---- hSpacer4 ----
      hSpacer4.setName("hSpacer4");
      p_Haut.add(hSpacer4);
      
      // ---- tbt_ModeDeplacement ----
      tbt_ModeDeplacement.setIcon(new ImageIcon(getClass().getResource("/images/curseur-icone-4580-32.png")));
      tbt_ModeDeplacement.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeDeplacement.setSelected(true);
      tbt_ModeDeplacement.setToolTipText("Mode d\u00e9placement");
      tbt_ModeDeplacement.setName("tbt_ModeDeplacement");
      tbt_ModeDeplacement.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeDeplacementActionPerformed(e);
        }
      });
      p_Haut.add(tbt_ModeDeplacement);
      
      // ---- tbt_ModeLabel ----
      tbt_ModeLabel.setIcon(new ImageIcon(getClass().getResource("/images/police-icone-6171-32.png")));
      tbt_ModeLabel.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeLabel.setToolTipText("Objet texte");
      tbt_ModeLabel.setName("tbt_ModeLabel");
      tbt_ModeLabel.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeLabelActionPerformed(e);
        }
      });
      p_Haut.add(tbt_ModeLabel);
      
      // ---- tbt_ModeImage ----
      tbt_ModeImage.setIcon(new ImageIcon(getClass().getResource("/images/alternative-detourage-photo-image-icone-4234-32.png")));
      tbt_ModeImage.setToolTipText("Objet image");
      tbt_ModeImage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeImage.setName("tbt_ModeImage");
      tbt_ModeImage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeImageActionPerformed(e);
        }
      });
      p_Haut.add(tbt_ModeImage);
      
      // ---- tbt_ModeCodeBarre ----
      tbt_ModeCodeBarre.setIcon(new ImageIcon(getClass().getResource("/images/code-a-barres-id-icone-8260-32.png")));
      tbt_ModeCodeBarre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_ModeCodeBarre.setToolTipText("Objet code barre");
      tbt_ModeCodeBarre.setName("tbt_ModeCodeBarre");
      tbt_ModeCodeBarre.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          tbt_ModeCodeBarreActionPerformed(e);
        }
      });
      p_Haut.add(tbt_ModeCodeBarre);
      
      // ---- hSpacer2 ----
      hSpacer2.setName("hSpacer2");
      p_Haut.add(hSpacer2);
      
      // ---- cb_ListeSpoolEditor ----
      cb_ListeSpoolEditor.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      cb_ListeSpoolEditor.setName("cb_ListeSpoolEditor");
      cb_ListeSpoolEditor.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          cb_ListeSpoolEditorActionPerformed(e);
        }
      });
      p_Haut.add(cb_ListeSpoolEditor);
      
      // ---- bt_Export ----
      bt_Export.setIcon(new ImageIcon(getClass().getResource("/images/document-a-exportation-icone-8861-32.png")));
      bt_Export.setToolTipText("Exporter la description vers l'\u00e9diteur de spool");
      bt_Export.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Export.setEnabled(false);
      bt_Export.setName("bt_Export");
      bt_Export.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_ExportActionPerformed(e);
        }
      });
      p_Haut.add(bt_Export);
      
      // ---- bt_Preview ----
      bt_Preview.setIcon(new ImageIcon(getClass().getResource("/images/document-preview-imprimer-icone-9540-32.png")));
      bt_Preview.setEnabled(false);
      bt_Preview.setToolTipText("L'aper\u00e7u de la page en cours");
      bt_Preview.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Preview.setName("bt_Preview");
      bt_Preview.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_PreviewActionPerformed(e);
        }
      });
      p_Haut.add(bt_Preview);
    }
    contentPane.add(p_Haut, BorderLayout.NORTH);
    
    // ======== p_Gauche ========
    {
      p_Gauche.setMinimumSize(new Dimension(0, 0));
      p_Gauche.setName("p_Gauche");
      p_Gauche.setLayout(new BorderLayout());
      
      // ======== scrollPane1 ========
      {
        scrollPane1.setMinimumSize(new Dimension(250, 25));
        scrollPane1.setPreferredSize(new Dimension(250, 882));
        scrollPane1.setName("scrollPane1");
        
        // ======== xTaskPaneContainer1 ========
        {
          xTaskPaneContainer1.setBackground(UIManager.getColor("Panel.background"));
          xTaskPaneContainer1.setBorder(null);
          xTaskPaneContainer1.setName("xTaskPaneContainer1");
          
          // ======== xTaskPane1 ========
          {
            xTaskPane1.setTitle("Page");
            xTaskPane1.setName("xTaskPane1");
            Container xTaskPane1ContentPane = xTaskPane1.getContentPane();
            xTaskPane1ContentPane.setLayout(new VerticalLayout());
            
            // ======== panel4 ========
            {
              panel4.setBorder(new TitledBorder(null, "Orientation", TitledBorder.LEADING, TitledBorder.ABOVE_TOP,
                  new Font("DejaVu Sans", Font.BOLD, 12)));
              panel4.setName("panel4");
              panel4.setLayout(null);
              
              // ---- rb_Portrait ----
              rb_Portrait.setSelected(true);
              rb_Portrait.setIcon(null);
              rb_Portrait.setText("Portrait");
              rb_Portrait.setName("rb_Portrait");
              rb_Portrait.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  rb_PortraitActionPerformed(e);
                }
              });
              panel4.add(rb_Portrait);
              rb_Portrait.setBounds(25, 35, 90, rb_Portrait.getPreferredSize().height);
              
              // ---- rb_Paysage ----
              rb_Paysage.setIcon(null);
              rb_Paysage.setText("Paysage");
              rb_Paysage.setName("rb_Paysage");
              rb_Paysage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  rb_PaysageActionPerformed(e);
                }
              });
              panel4.add(rb_Paysage);
              rb_Paysage.setBounds(25, 55, 90, rb_Paysage.getPreferredSize().height);
              
              // ---- chk_LienSpool ----
              chk_LienSpool.setText("<html>Lier<br>au<br>spool</html>");
              chk_LienSpool.setName("chk_LienSpool");
              chk_LienSpool.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  chk_LienSpoolActionPerformed(e);
                }
              });
              panel4.add(chk_LienSpool);
              chk_LienSpool.setBounds(new Rectangle(new Point(130, 35), chk_LienSpool.getPreferredSize()));
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            xTaskPane1ContentPane.add(panel4);
            
            // ======== panel8 ========
            {
              panel8.setBorder(new TitledBorder(null, "Dimension", TitledBorder.LEADING, TitledBorder.ABOVE_TOP,
                  new Font("DejaVu Sans", Font.BOLD, 12)));
              panel8.setName("panel8");
              panel8.setLayout(null);
              
              // ---- label10 ----
              label10.setText("Largeur");
              label10.setName("label10");
              panel8.add(label10);
              label10.setBounds(25, 35, 55, label10.getPreferredSize().height);
              
              // ---- label11 ----
              label11.setText("Hauteur");
              label11.setName("label11");
              panel8.add(label11);
              label11.setBounds(25, 65, 55, label11.getPreferredSize().height);
              
              // ---- sp_LargeurPage ----
              sp_LargeurPage.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 0.5F));
              sp_LargeurPage.setName("sp_LargeurPage");
              sp_LargeurPage.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_LargeurPageStateChanged(e);
                }
              });
              panel8.add(sp_LargeurPage);
              sp_LargeurPage.setBounds(85, 32, 60, sp_LargeurPage.getPreferredSize().height);
              
              // ---- sp_HauteurPage ----
              sp_HauteurPage.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 0.5F));
              sp_HauteurPage.setName("sp_HauteurPage");
              sp_HauteurPage.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_HauteurPageStateChanged(e);
                }
              });
              panel8.add(sp_HauteurPage);
              sp_HauteurPage.setBounds(85, 62, 60, sp_HauteurPage.getPreferredSize().height);
              
              // ---- label12 ----
              label12.setText("cm");
              label12.setName("label12");
              panel8.add(label12);
              label12.setBounds(new Rectangle(new Point(150, 38), label12.getPreferredSize()));
              
              // ---- label13 ----
              label13.setText("cm");
              label13.setName("label13");
              panel8.add(label13);
              label13.setBounds(150, 69, 19, 15);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            xTaskPane1ContentPane.add(panel8);
            
            // ======== panel3 ========
            {
              panel3.setBorder(
                  new TitledBorder(null, "Marges", TitledBorder.LEADING, TitledBorder.ABOVE_TOP, new Font("DejaVu Sans", Font.BOLD, 12)));
              panel3.setPreferredSize(new Dimension(137, 97));
              panel3.setMinimumSize(new Dimension(137, 97));
              panel3.setName("panel3");
              panel3.setLayout(null);
              
              // ---- label2 ----
              label2.setText("X");
              label2.setName("label2");
              panel3.add(label2);
              label2.setBounds(new Rectangle(new Point(25, 39), label2.getPreferredSize()));
              
              // ---- label3 ----
              label3.setText("Y");
              label3.setName("label3");
              panel3.add(label3);
              label3.setBounds(new Rectangle(new Point(25, 69), label3.getPreferredSize()));
              
              // ---- sp_MargeX ----
              sp_MargeX.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 0.5F));
              sp_MargeX.setName("sp_MargeX");
              sp_MargeX.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_MargeXStateChanged(e);
                }
              });
              panel3.add(sp_MargeX);
              sp_MargeX.setBounds(85, 32, 60, sp_MargeX.getPreferredSize().height);
              
              // ---- l_MargeXcm ----
              l_MargeXcm.setText("cm");
              l_MargeXcm.setName("l_MargeXcm");
              panel3.add(l_MargeXcm);
              l_MargeXcm.setBounds(150, 39, 19, l_MargeXcm.getPreferredSize().height);
              
              // ---- sp_MargeY ----
              sp_MargeY.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 0.5F));
              sp_MargeY.setName("sp_MargeY");
              sp_MargeY.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_MargeYStateChanged(e);
                }
              });
              panel3.add(sp_MargeY);
              sp_MargeY.setBounds(85, 62, 60, sp_MargeY.getPreferredSize().height);
              
              // ---- l_MargeYcm ----
              l_MargeYcm.setText("cm");
              l_MargeYcm.setName("l_MargeYcm");
              panel3.add(l_MargeYcm);
              l_MargeYcm.setBounds(150, 69, 19, l_MargeYcm.getPreferredSize().height);
            }
            xTaskPane1ContentPane.add(panel3);
            
            // ======== panel9 ========
            {
              panel9.setBorder(new TitledBorder(null, "Fond de page", TitledBorder.LEADING, TitledBorder.ABOVE_TOP,
                  new Font("DejaVu Sans", Font.BOLD, 12)));
              panel9.setName("panel9");
              panel9.setLayout(new FormLayout(
                  new ColumnSpec[] { new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW), FormFactory.DEFAULT_COLSPEC,
                      FormFactory.LABEL_COMPONENT_GAP_COLSPEC, FormFactory.DEFAULT_COLSPEC,
                      new ColumnSpec(ColumnSpec.FILL, Sizes.DEFAULT, FormSpec.DEFAULT_GROW) },
                  new RowSpec[] { new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.NO_GROW), FormFactory.LINE_GAP_ROWSPEC,
                      FormFactory.DEFAULT_ROWSPEC, new RowSpec(RowSpec.FILL, Sizes.DEFAULT, FormSpec.NO_GROW) }));
              
              // ---- bt_ChoisirImage ----
              bt_ChoisirImage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              bt_ChoisirImage.setIcon(new ImageIcon(getClass().getResource("/images/image-inserer-icone-5083-32.png")));
              bt_ChoisirImage.setToolTipText("Ajouter une image en fond");
              bt_ChoisirImage.setName("bt_ChoisirImage");
              bt_ChoisirImage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_ChoisirImageActionPerformed(e);
                }
              });
              panel9.add(bt_ChoisirImage, cc.xy(2, 1));
              
              // ---- bt_SansImage ----
              bt_SansImage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              bt_SansImage.setIcon(new ImageIcon(getClass().getResource("/images/image-supprimer-icone-8223-32.png")));
              bt_SansImage.setToolTipText("Enlever l'image de fond");
              bt_SansImage.setName("bt_SansImage");
              bt_SansImage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  bt_SansImageActionPerformed(e);
                }
              });
              panel9.add(bt_SansImage, cc.xy(4, 1));
              
              // ---- label16 ----
              label16.setText("Dpi aff");
              label16.setName("label16");
              panel9.add(label16, cc.xy(2, 3));
              
              // ---- tf_DpiAff ----
              tf_DpiAff.setText("72");
              tf_DpiAff.setHorizontalAlignment(SwingConstants.RIGHT);
              tf_DpiAff.setName("tf_DpiAff");
              tf_DpiAff.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  tf_DpiAffFocusLost(e);
                }
              });
              panel9.add(tf_DpiAff, cc.xy(4, 3));
              
              // ---- l_Dpi ----
              l_Dpi.setText("Dpi img");
              l_Dpi.setName("l_Dpi");
              panel9.add(l_Dpi, cc.xy(2, 4));
              
              // ---- tf_Dpi ----
              tf_Dpi.setHorizontalAlignment(SwingConstants.RIGHT);
              tf_Dpi.setName("tf_Dpi");
              tf_Dpi.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  tf_DpiFocusLost(e);
                }
              });
              panel9.add(tf_Dpi, cc.xy(4, 4));
            }
            xTaskPane1ContentPane.add(panel9);
          }
          xTaskPaneContainer1.add(xTaskPane1);
          
          // ======== xTaskPane2 ========
          {
            xTaskPane2.setTitle("Etiquette");
            xTaskPane2.setName("xTaskPane2");
            Container xTaskPane2ContentPane = xTaskPane2.getContentPane();
            xTaskPane2ContentPane.setLayout(new VerticalLayout());
            
            // ======== panel7 ========
            {
              panel7.setBorder(new TitledBorder(null, "Marges", TitledBorder.LEADING, TitledBorder.ABOVE_TOP));
              panel7.setName("panel7");
              panel7.setLayout(null);
              
              // ---- label8 ----
              label8.setText("X");
              label8.setName("label8");
              panel7.add(label8);
              label8.setBounds(new Rectangle(new Point(25, 35), label8.getPreferredSize()));
              
              // ---- label9 ----
              label9.setText("Y");
              label9.setName("label9");
              panel7.add(label9);
              label9.setBounds(new Rectangle(new Point(25, 65), label9.getPreferredSize()));
              
              // ---- sp_MargeXEtq ----
              sp_MargeXEtq.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 1.0F));
              sp_MargeXEtq.setName("sp_MargeXEtq");
              sp_MargeXEtq.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_MargeXEtqStateChanged(e);
                }
              });
              panel7.add(sp_MargeXEtq);
              sp_MargeXEtq.setBounds(50, 30, 60, 28);
              
              // ---- sp_MargeYEtq ----
              sp_MargeYEtq.setModel(new SpinnerNumberModel(0.0F, 0.0F, null, 1.0F));
              sp_MargeYEtq.setName("sp_MargeYEtq");
              sp_MargeYEtq.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_MargeYEtqStateChanged(e);
                }
              });
              panel7.add(sp_MargeYEtq);
              sp_MargeYEtq.setBounds(50, 60, 60, 28);
              
              // ---- l_MargeXEtq ----
              l_MargeXEtq.setText("? px");
              l_MargeXEtq.setName("l_MargeXEtq");
              panel7.add(l_MargeXEtq);
              l_MargeXEtq.setBounds(120, 37, 50, l_MargeXEtq.getPreferredSize().height);
              
              // ---- l_MargeYEtq ----
              l_MargeYEtq.setText("? px");
              l_MargeYEtq.setName("l_MargeYEtq");
              panel7.add(l_MargeYEtq);
              l_MargeYEtq.setBounds(120, 67, 50, l_MargeYEtq.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            xTaskPane2ContentPane.add(panel7);
            
            // ======== panel5 ========
            {
              panel5.setBorder(new TitledBorder(null, "Nombre d'\u00e9tiquettes", TitledBorder.LEADING, TitledBorder.ABOVE_TOP,
                  new Font("DejaVu Sans", Font.BOLD, 12)));
              panel5.setName("panel5");
              panel5.setLayout(null);
              
              // ---- label4 ----
              label4.setText("Lignes");
              label4.setName("label4");
              panel5.add(label4);
              label4.setBounds(25, 35, 60, label4.getPreferredSize().height);
              
              // ---- label5 ----
              label5.setText("Colonnes");
              label5.setName("label5");
              panel5.add(label5);
              label5.setBounds(25, 65, 60, label5.getPreferredSize().height);
              
              // ---- sp_NbrLignes ----
              sp_NbrLignes.setModel(new SpinnerNumberModel(1, 1, null, 1));
              sp_NbrLignes.setName("sp_NbrLignes");
              sp_NbrLignes.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_NbrLignesStateChanged(e);
                }
              });
              panel5.add(sp_NbrLignes);
              sp_NbrLignes.setBounds(90, 32, 50, sp_NbrLignes.getPreferredSize().height);
              
              // ---- sp_NbrColonnes ----
              sp_NbrColonnes.setModel(new SpinnerNumberModel(1, 1, null, 1));
              sp_NbrColonnes.setName("sp_NbrColonnes");
              sp_NbrColonnes.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_NbrColonnesStateChanged(e);
                }
              });
              panel5.add(sp_NbrColonnes);
              sp_NbrColonnes.setBounds(90, 62, 50, sp_NbrColonnes.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            xTaskPane2ContentPane.add(panel5);
            
            // ======== panel6 ========
            {
              panel6.setBorder(new TitledBorder(null, "Dimension", TitledBorder.LEADING, TitledBorder.ABOVE_TOP,
                  new Font("DejaVu Sans", Font.BOLD, 12)));
              panel6.setName("panel6");
              panel6.setLayout(null);
              
              // ---- label6 ----
              label6.setText("Largeur");
              label6.setName("label6");
              panel6.add(label6);
              label6.setBounds(25, 35, 55, label6.getPreferredSize().height);
              
              // ---- label7 ----
              label7.setText("Hauteur");
              label7.setName("label7");
              panel6.add(label7);
              label7.setBounds(25, 65, 55, label7.getPreferredSize().height);
              
              // ---- sp_LargeurEtq ----
              sp_LargeurEtq.setModel(new SpinnerNumberModel(0, 0, null, 1));
              sp_LargeurEtq.setName("sp_LargeurEtq");
              sp_LargeurEtq.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_LargeurEtqStateChanged(e);
                }
              });
              panel6.add(sp_LargeurEtq);
              sp_LargeurEtq.setBounds(85, 32, 60, sp_LargeurEtq.getPreferredSize().height);
              
              // ---- sp_HauteurEtq ----
              sp_HauteurEtq.setModel(new SpinnerNumberModel(0, 0, null, 1));
              sp_HauteurEtq.setName("sp_HauteurEtq");
              sp_HauteurEtq.addChangeListener(new ChangeListener() {
                @Override
                public void stateChanged(ChangeEvent e) {
                  sp_HauteurEtqStateChanged(e);
                }
              });
              panel6.add(sp_HauteurEtq);
              sp_HauteurEtq.setBounds(85, 62, 60, sp_HauteurEtq.getPreferredSize().height);
              
              // ---- label14 ----
              label14.setText("cm");
              label14.setName("label14");
              panel6.add(label14);
              label14.setBounds(150, 39, 18, 15);
              
              // ---- label15 ----
              label15.setText("cm");
              label15.setName("label15");
              panel6.add(label15);
              label15.setBounds(150, 69, 18, 15);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            xTaskPane2ContentPane.add(panel6);
          }
          xTaskPaneContainer1.add(xTaskPane2);
        }
        scrollPane1.setViewportView(xTaskPaneContainer1);
      }
      p_Gauche.add(scrollPane1, BorderLayout.CENTER);
    }
    contentPane.add(p_Gauche, BorderLayout.WEST);
    
    // ======== p_Bas ========
    {
      p_Bas.setName("p_Bas");
      p_Bas.setLayout(new FlowLayout(FlowLayout.LEFT));
      
      // ---- label1 ----
      label1.setText("Position:");
      label1.setName("label1");
      p_Bas.add(label1);
      
      // ---- l_PositionSouris ----
      l_PositionSouris.setText("0, 0");
      l_PositionSouris.setName("l_PositionSouris");
      p_Bas.add(l_PositionSouris);
    }
    contentPane.add(p_Bas, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ======== pm_Preview ========
    {
      pm_Preview.setName("pm_Preview");
      
      // ---- mi_PPCpdf ----
      mi_PPCpdf.setText("PDF");
      mi_PPCpdf.setName("mi_PPCpdf");
      mi_PPCpdf.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_PPCpdfActionPerformed(e);
        }
      });
      pm_Preview.add(mi_PPCpdf);
      
      // ---- mi_PPCps ----
      mi_PPCps.setText("Postscript");
      mi_PPCps.setName("mi_PPCps");
      mi_PPCps.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_PPCpsActionPerformed(e);
        }
      });
      pm_Preview.add(mi_PPCps);
      
      // ---- mi_PPCtiff ----
      mi_PPCtiff.setText("TIFF");
      mi_PPCtiff.setName("mi_PPCtiff");
      mi_PPCtiff.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_PPCtiffActionPerformed(e);
        }
      });
      pm_Preview.add(mi_PPCtiff);
      
      // ---- mi_PPCpng ----
      mi_PPCpng.setText("PNG");
      mi_PPCpng.setName("mi_PPCpng");
      mi_PPCpng.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_PPCpngActionPerformed(e);
        }
      });
      pm_Preview.add(mi_PPCpng);
    }
    
    // ---- buttonGroup2 ----
    ButtonGroup buttonGroup2 = new ButtonGroup();
    buttonGroup2.add(tbt_ModeDeplacement);
    buttonGroup2.add(tbt_ModeLabel);
    buttonGroup2.add(tbt_ModeImage);
    buttonGroup2.add(tbt_ModeCodeBarre);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rb_Portrait);
    buttonGroup1.add(rb_Paysage);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_Haut;
  private JButton bt_Save;
  private JButton bt_SaveAs;
  private JPanel hSpacer1;
  private JButton bt_Undo;
  private JButton bt_Redo;
  private JPanel hSpacer3;
  private JButton bt_Grid;
  private JPanel hSpacer4;
  private JToggleButton tbt_ModeDeplacement;
  private JToggleButton tbt_ModeLabel;
  private JToggleButton tbt_ModeImage;
  private JToggleButton tbt_ModeCodeBarre;
  private JPanel hSpacer2;
  private JComboBox cb_ListeSpoolEditor;
  private JButton bt_Export;
  private JButton bt_Preview;
  private JPanel p_Gauche;
  private JScrollPane scrollPane1;
  private JXTaskPaneContainer xTaskPaneContainer1;
  private JXTaskPane xTaskPane1;
  private JPanel panel4;
  private JRadioButton rb_Portrait;
  private JRadioButton rb_Paysage;
  private JCheckBox chk_LienSpool;
  private JPanel panel8;
  private JLabel label10;
  private JLabel label11;
  private JSpinner sp_LargeurPage;
  private JSpinner sp_HauteurPage;
  private JLabel label12;
  private JLabel label13;
  private JPanel panel3;
  private JLabel label2;
  private JLabel label3;
  private JSpinner sp_MargeX;
  private JLabel l_MargeXcm;
  private JSpinner sp_MargeY;
  private JLabel l_MargeYcm;
  private JPanel panel9;
  private JButton bt_ChoisirImage;
  private JButton bt_SansImage;
  private JLabel label16;
  private JTextField tf_DpiAff;
  private JLabel l_Dpi;
  private JTextField tf_Dpi;
  private JXTaskPane xTaskPane2;
  private JPanel panel7;
  private JLabel label8;
  private JLabel label9;
  private JSpinner sp_MargeXEtq;
  private JSpinner sp_MargeYEtq;
  private JLabel l_MargeXEtq;
  private JLabel l_MargeYEtq;
  private JPanel panel5;
  private JLabel label4;
  private JLabel label5;
  private JSpinner sp_NbrLignes;
  private JSpinner sp_NbrColonnes;
  private JPanel panel6;
  private JLabel label6;
  private JLabel label7;
  private JSpinner sp_LargeurEtq;
  private JSpinner sp_HauteurEtq;
  private JLabel label14;
  private JLabel label15;
  private JPanel p_Bas;
  private JLabel label1;
  private JLabel l_PositionSouris;
  private JPopupMenu pm_Preview;
  private JMenuItem mi_PPCpdf;
  private JMenuItem mi_PPCps;
  private JMenuItem mi_PPCtiff;
  private JMenuItem mi_PPCpng;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
