/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.MouseEvent;

import javax.swing.SwingConstants;

import ri.serien.newsim.description.DescriptionLabel;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.outils.CalculDPI;

/**
 * Description du label pour RAD (génération Etiquette)
 */
public class GfxLabel extends GfxObject {
  // Constantes
  
  // Variables
  private boolean trimTexte = true;
  
  // private String oldtexte="";
  // private GfxLabel thislabel = this;
  
  /**
   * Constructeur
   * @param text
   * @param x
   * @param y
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxLabel(String text, int x, int y, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(x, y, planTravail);
    setOpaque(false);
    setFont(new Font(getNomPolice(), getStylePolice(), 10).deriveFont(getTaillePolice()));
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    setText(text);
    FontMetrics fm = getFontMetrics(getFont());
    setSize(fm.charWidth('W') * (text.length() + 1), fm.getHeight());
    setBackground(Color.white);
  }
  
  /**
   * Constructeur
   * @param dlabel
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxLabel(DescriptionLabel dlabel, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(dlabel, dpi_aff, dpi_img, planTravail);
    setOpaque(false);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dlabel.getLargeur()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dlabel.getHauteur()));
    setBackground(Color.white);
    
    setText(dlabel.getTexte());
    setTrimTexte(dlabel.isTrimTexte());
    setNomPolice(dlabel.getNomPolice());
    setStylePolice(dlabel.getStylePolice());
    setTaillePolice(dlabel.getTaillePolice());
    setFont(new Font(nomPolice, stylePolice, 10).deriveFont(taillePolice));
    setForeground(dlabel.getForeGroundColor());
    setHorizontalAlignment(getDescription2HorizontalText(dlabel.getJustificationHTexte()));
    setFocusBorder(false, false, false);
    setCondition(dlabel.getCondition());
    // dpi_img);
  }
  
  /**
   * Permet de saisir un libellé dans le label
   * 
   * private void saisieLibelle()
   * {
   * Dimension d = getSize();
   * Point p = getLocation();
   * if (p.y < (getParent().getHeight()-d.height*2))
   * p.y = p.y+d.height;
   * else
   * p.y = p.y-(d.height*2);
   * d.height = d.height + getFont().getSize();
   * new SaisieVolatile(p, d, this);
   * }
   */
  
  /**
   * @return the ligne
   */
  public DescriptionLabel getDescription() {
    DescriptionLabel dlabel = new DescriptionLabel();
    dlabel.setTexte(getText());
    dlabel.setTrimTexte(trimTexte);
    dlabel.setHauteur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getHeight()));
    dlabel.setLargeur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getWidth()));
    dlabel.setXPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().x));
    dlabel.setYPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().y));
    // dlabel.setPolice(getFont());
    dlabel.setNomPolice(nomPolice);
    dlabel.setStylePolice(stylePolice);
    dlabel.setTaillePolice(taillePolice);
    dlabel.setForeGroundColor(getForeground());
    dlabel.setJustificationHTexte(getHorizontalText2Description(getHorizontalAlignment()));
    // private int justificationVTexte=1; // 0=Haut 1=Centre 2=Bas
    // private int orientationTexte=0; // 0=Horizontal 1=Vertical
    dlabel.setCondition(condition);
    
    return dlabel;
  }
  
  /**
   * Initialise le texte dans l'objet et son infobulle
   */
  public void setText(String texte) {
    // oldtexte = getText();
    super.setText(texte);
    setToolTipText(texte);
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(this);
      // if (!oldtexte.equals(texte))
      // pageEditor.save(true);
    }
  }
  
  /**
   * @return the trimTexte
   */
  public boolean isTrimTexte() {
    return trimTexte;
  }
  
  /**
   * @param trimTexte the trimTexte to set
   */
  public void setTrimTexte(boolean trimTexte) {
    this.trimTexte = trimTexte;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(final GfxPageEditor planTravail) {
    this.pageEditor = planTravail;
    if (planTravail != null) {
      planTravail.getPropertyObject().getPanelProperty(this);
    }
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * Convertit les constantes de positionnement horizontal en indice
   * @param constante
   * @return
   */
  public int getHorizontalText2Description(int constante) {
    switch (constante) {
      case SwingConstants.CENTER:
        return DescriptionLabel.CENTRE;
      case SwingConstants.LEFT:
        return DescriptionLabel.GAUCHE;
      case SwingConstants.RIGHT:
        return DescriptionLabel.DROITE;
    }
    return DescriptionLabel.GAUCHE;
  }
  
  /**
   * Convertit l'indice en constante de positionnement horizontal
   * @param valeur
   * @return
   */
  public int getDescription2HorizontalText(int indice) {
    switch (indice) {
      case DescriptionLabel.CENTRE:
        return SwingConstants.CENTER;
      case DescriptionLabel.GAUCHE:
        return SwingConstants.LEFT;
      case DescriptionLabel.DROITE:
        return SwingConstants.RIGHT;
    }
    return SwingConstants.LEFT;
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  protected void labelMouseReleased(MouseEvent e) {
    super.labelMouseReleased(e);
    
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  protected void labelMouseDragged(MouseEvent e) {
    super.labelMouseDragged(e);
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if ((clic_gauche) && (pageEditor != null)) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
  
}
