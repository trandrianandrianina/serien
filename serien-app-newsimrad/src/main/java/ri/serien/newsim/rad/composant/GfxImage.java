/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;

import ri.serien.newsim.description.DescriptionImage;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.outils.CalculDPI;

/**
 * Description de l'objet image pour RAD (génération Etiquette)
 */
public class GfxImage extends GfxObject {
  // Constantes
  
  // Variables
  private boolean keepRatio = true;
  private String cheminImage = null;
  private ImageIcon imageOriginale = null; // Image sans transformation
  private boolean avecDossier = false; // Précise si les images sont identifiées avec le chemin complet ou pas
  
  /**
   * Constructeur
   * @param x
   * @param y
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxImage(int x, int y, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(x, y, planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    setIcon(new ImageIcon(getClass().getResource("/images/alternative-detourage-photo-image-icone-4234-32.png")));
    setSize(getIcon().getIconWidth(), getIcon().getIconHeight());
    setHorizontalAlignment(CENTER);
    setBackground(Color.blue);
  }
  
  /**
   * Constructeur
   * @param dlabel
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxImage(DescriptionImage dimage, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(dimage, dpi_aff, dpi_img, planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    setImageOriginale(dimage.getCheminImage());
    
    if (getImageOriginale() == null) {
      setIcon(new ImageIcon(getClass().getResource("/images/alternative-detourage-photo-image-icone-4234-32.png")));
    }
    setHorizontalAlignment(CENTER);
    setBackground(Color.blue);
    
    setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dimage.getLargeur()), CalculDPI.getCmtoPx(dpi_aff, dpi_img, dimage.getHauteur()));
    setKeepRatio(dimage.isKeepRatio());
    if (dimage.getCheminImage() != null) {
      avecDossier = dimage.getCheminImage().trim().startsWith("/");
    }
    
    setFocusBorder(false, false, false);
    setCondition(dimage.getCondition());
  }
  
  /**
   * Redimensionne l'image
   * @param origine
   * @param rw
   * @param rh
   * @return
   * 
   *         private BufferedImage getResizeImage(BufferedImage origine, int w, int h)
   *         {
   *         if (origine == null) return null;
   *         float rw=1, rh=1;
   *         
   *         // Calcul du ratio
   *         if (w == -1) // La référence est la hauteur
   *         {
   *         rh = ((float)origine.getHeight() / h);
   *         rw = rh;
   *         }
   *         else
   *         if (h == -1) // La référence est la largeur
   *         {
   *         rw = ((float)origine.getWidth() / w);
   *         rh = rw;
   *         }
   *         else
   *         {
   *         rw = ((float)origine.getWidth() / w);
   *         rh = ((float)origine.getHeight() / h);
   *         }
   * 
   *         // Transformation de l'image
   *         AffineTransform tx = new AffineTransform();
   *         tx.scale(rw, rh);
   *         AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
   *         BufferedImage bufFinal = new BufferedImage( (int) (origine.getWidth() * rw), (int) (origine.getHeight() *
   *         rh), origine.getType());
   * 
   *         return op.filter(origine, bufFinal);
   *         }
   */
  
  /**
   * Retourne une icone redimensionnée
   * @param ii
   * @return
   */
  public ImageIcon getResizeIcon(ImageIcon origine, int w, int h) {
    if (origine == null) {
      return null;
    }
    float rw = 1;
    float rh = 1;
    
    // Calcul du ratio
    // La référence est la hauteur
    if (w == -1) {
      rh = ((float) origine.getIconHeight() / h);
      rw = rh;
    }
    else {
      // La référence est la largeur
      if (h == -1) {
        rw = ((float) origine.getIconWidth() / w);
        rh = rw;
      }
      else {
        rw = ((float) origine.getIconWidth() / w);
        rh = ((float) origine.getIconHeight() / h);
      }
    }
    
    // Calcul des nouvelles dimensions
    w = (int) (origine.getIconWidth() / rw);
    h = (int) (origine.getIconHeight() / rh);
    
    BufferedImage buf = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    // On dessine sur le Graphics de l'image bufferisée.
    Graphics2D g = buf.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.drawImage(origine.getImage(), 0, 0, w, h, null);
    g.dispose();
    return new ImageIcon(buf);
  }
  
  /**
   * Remet l'image à sa taille originale
   */
  public void setOriginalSize() {
    int oldwidth = getSize().width;
    int oldheight = getSize().height;
    
    setSize(imageOriginale.getIconWidth(), imageOriginale.getIconHeight());
    setIcon(getResizeIcon(imageOriginale, imageOriginale.getIconWidth(), imageOriginale.getIconHeight()));
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(this);
      if ((oldwidth != getSize().width) || (oldheight != getSize().height)) {
        pageEditor.save(true);
      }
    }
  }
  
  /**
   * @return the ligne
   */
  public DescriptionImage getDescription() {
    DescriptionImage dimage = new DescriptionImage();
    dimage.setHauteur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getHeight()));
    dimage.setLargeur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getWidth()));
    dimage.setXPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().x));
    dimage.setYPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().y));
    dimage.setCheminImage(cheminImage);
    dimage.setCondition(condition);
    
    return dimage;
  }
  
  /**
   * @param keepRatio the keepRatio to set
   */
  public void setKeepRatio(boolean keepRatio) {
    this.keepRatio = keepRatio;
    // Mise à jour des attributs
    // if (pageEditor != null)
    // pageEditor.getPropertyObject().getPanelProperty(this);
  }
  
  /**
   * @return the keepRatio
   */
  public boolean isKeepRatio() {
    return keepRatio;
  }
  
  /**
   * @return the cheminImage
   */
  public String getCheminImage() {
    return cheminImage;
  }
  
  /**
   * @return the imageOriginale
   */
  public ImageIcon getImageOriginale() {
    return imageOriginale;
  }
  
  /**
   * Chargement de l'image
   * Attention certaines images jpg peuvent ne pas être afficher
   * Faudrait intercepter l'erreur: sun.awt.image.ImageFormatException: Unsupported color conversion request
   * @param imageOriginale the imageOriginale to set
   */
  public void setImageOriginale(String chimage) {
    if (chimage == null) {
      imageOriginale = null;
      cheminImage = null;
    }
    else {
      if (avecDossier) {
        cheminImage = chimage;
        imageOriginale = new ImageIcon(cheminImage);
      }
      else {
        cheminImage = new File(chimage).getName();
        // if (pageEditor != null)
        imageOriginale = new ImageIcon(pageEditor.getDossierTravail() + File.separator + cheminImage);
      }
      setIcon(getResizeIcon(imageOriginale, getWidth(), getHeight()));
    }
  }
  
  /**
   * @param imageOriginale the imageOriginale to set
   */
  public void setImageOriginale(URL chimage) {
    setImageOriginale(chimage.getFile());
  }
  
  /**
   * @return the avecDossier
   */
  public boolean isAvecDossier() {
    return avecDossier;
  }
  
  /**
   * @param avecDossier the avecDossier to set
   */
  public void setAvecDossier(boolean avecDossier) {
    this.avecDossier = avecDossier;
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  @Override
  protected void labelMouseReleased(MouseEvent e) {
    // Gestion du redimensionnement de l'image
    if (redimensionne) {
      if (imageOriginale != null) {
        if (keepRatio) {
          switch (getCursor().getType()) {
            case Cursor.N_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, -1, getHeight()));
              break;
            case Cursor.S_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, -1, getHeight()));
              break;
            case Cursor.E_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, getWidth(), -1));
              break;
            case Cursor.W_RESIZE_CURSOR:
              setIcon(getResizeIcon(imageOriginale, getWidth(), -1));
              break;
          }
        }
        else {
          setIcon(getResizeIcon(imageOriginale, getWidth(), getHeight()));
        }
      }
    }
    
    // On effectue les opérations d'origine
    super.labelMouseReleased(e);
    
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  @Override
  protected void labelMouseDragged(MouseEvent e) {
    super.labelMouseDragged(e);
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if ((clic_gauche) && (pageEditor != null)) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
}
