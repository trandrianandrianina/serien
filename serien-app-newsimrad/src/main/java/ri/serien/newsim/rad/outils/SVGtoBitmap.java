/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.outils;

import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;

import javax.swing.JPanel;

import org.apache.batik.bridge.BridgeContext;
import org.apache.batik.bridge.GVTBuilder;
import org.apache.batik.bridge.UserAgentAdapter;
import org.apache.batik.gvt.GraphicsNode;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.batik.util.XMLResourceDescriptor;
import org.apache.fop.svg.FOPSAXSVGDocumentFactory;
import org.w3c.dom.svg.SVGDocument;

/**
 * Charge une image SVG dans un BufferedImage
 */
public class SVGtoBitmap {
  // Variables
  private static int dpi_svg = 90;
  
  /**
   * Retourne le dpi de l'image qui vient d'être chargé
   * @return
   */
  public int getDpi() {
    return dpi_svg;
  }
  
  /**
   * Charge une image SVG dans un BufferedImage
   * @param url location of svg resource.
   * @param dimension desired size
   * @param dpisvg dpi de l'image svg
   * @return image of the rendered svg
   * @throws IOException
   * @throws IOException
   * @throws IOException when svg resource cannot be read.
   */
  public static Image getImageFromSvg(URL url) throws IOException {
    // Load SVG resource into a document
    String parser = XMLResourceDescriptor.getXMLParserClassName();
    FOPSAXSVGDocumentFactory f = new FOPSAXSVGDocumentFactory(parser);
    SVGDocument document = (SVGDocument) f.createDocument(url.toString());
    
    // Build the tree and get the document dimensions
    UserAgentAdapter userAgentAdapter = new UserAgentAdapter();
    BridgeContext bridgeContext = new BridgeContext(userAgentAdapter);
    GVTBuilder builder = new GVTBuilder();
    GraphicsNode graphicsNode = builder.build(bridgeContext, document);
    
    // Paint svg into image buffer
    int width = (int) Math.round(graphicsNode.getBounds().getWidth());
    int height = (int) Math.round(graphicsNode.getBounds().getHeight());
    
    BufferedImage bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g2d = (Graphics2D) bufferedImage.getGraphics();
    graphicsNode.paint(g2d);
    
    // Calcul du dpi du svg
    // bridgeContext.getDocumentSize().getHeight() + " " + graphicsNode.);
    
    // Cleanup and return image
    g2d.dispose();
    return bufferedImage;
    /*
    //JSVGCanvas svgCanvas = new JSVGCanvas(null,true,false);
    final JSVGComponent svgComp = new JSVGComponent(null, true, false);
    svgComp.loadSVGDocument(url.toString());
     svgComp.addGVTTreeBuilderListener(new GVTTreeBuilderAdapter() {
         public void gvtBuildCompleted(GVTTreeBuilderEvent evt) {
             Dimension2D size = svgComp.getSVGDocumentSize();
             
         }
     });
    
    //svgCanvas.setURI(url.toString());
    //svgCanvas.getPaintingTransform();
    //svgCanvas.loadSVGDocument(url.toString());
    //Rectangle re = svgCanvas.getRenderRect();
    
    
    //return svgCanvas.getOffScreen();
     */
  }
  
  /**
   * Charge une image SVG dans un BufferedImage et le redimensionne en fonction du dpi
   * @param url
   * @param dimension
   * @param dpisvg
   * @return
   * @throws IOException
   */
  public static Image getImageFromSvg(URL url, int dpisvg) throws IOException {
    Image image = getImageFromSvg(url);
    
    int w = CalculDPI.getDim_px(dpisvg, image.getWidth(null));
    int h = CalculDPI.getDim_px(dpisvg, image.getHeight(null));
    
    BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    // On dessine sur le Graphics de l'image bufferisée.
    Graphics2D g = bufferedImage.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.drawImage(image, 0, 0, w, h, null);
    g.dispose();
    
    return bufferedImage;
  }
  
  /**
   * Charge une image SVG dans un BufferedImage à la dimension souhaitée
   * @param url
   * @param dimension
   * @param dpisvg
   * @return
   * @throws IOException
   */
  public static Image getImageFromSvg(URL url, Dimension dimension, int dpisvg) throws IOException {
    Image image = getImageFromSvg(url);
    
    int w = CalculDPI.getDim_px(dpisvg, dimension.width);
    int h = CalculDPI.getDim_px(dpisvg, dimension.height);
    BufferedImage bufferedImage = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    // On dessine sur le Graphics de l'image bufferisée.
    Graphics2D g = bufferedImage.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.drawImage(image, 0, 0, w, h, null);
    g.dispose();
    
    return bufferedImage;
  }
  
  /**
   * Charge une image SVG dans un BufferedImage (plus lent que la première)
   * @param url
   * @param dimension
   * @return
   */
  public static Image getImagePNGFromSvg(URL url, Dimension dimension) {
    // Create a PNG transcoder.
    Transcoder t = new PNGTranscoder();
    
    // Set the transcoding hints.
    t.addTranscodingHint(PNGTranscoder.KEY_WIDTH, Float.valueOf(dimension.width));
    t.addTranscodingHint(PNGTranscoder.KEY_HEIGHT, Float.valueOf(dimension.height));
    
    // Create the transcoder input.
    TranscoderInput input = new TranscoderInput(url.toString());
    
    ByteArrayOutputStream ostream = null;
    try {
      // Create the transcoder output.
      ostream = new ByteArrayOutputStream();
      TranscoderOutput output = new TranscoderOutput(ostream);
      
      // Save the image.
      t.transcode(input, output);
      
      // Flush and close the stream.
      ostream.flush();
      ostream.close();
    }
    catch (Exception ex) {
      ex.printStackTrace();
    }
    
    if (ostream == null) {
      return null;
    }
    // Convert the byte stream into an image.
    byte[] imgData = ostream.toByteArray();
    Image img = Toolkit.getDefaultToolkit().createImage(imgData);
    
    // Wait until the entire image is loaded.
    MediaTracker tracker = new MediaTracker(new JPanel());
    tracker.addImage(img, 0);
    try {
      tracker.waitForID(0);
    }
    catch (InterruptedException ex) {
      ex.printStackTrace();
    }
    
    // Return the newly rendered image.
    return img;
  }
  
}
