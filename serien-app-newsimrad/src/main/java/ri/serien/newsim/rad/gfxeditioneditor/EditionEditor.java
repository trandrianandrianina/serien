/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxeditioneditor;

import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import ri.serien.libcommun.outils.fichier.FiltreFichierParExtension;

/**
 * Editeur des éditions en mode solo
 */
public class EditionEditor {
  // Variables
  private File dernierPath = null;
  
  /**
   * Constructeur
   */
  public EditionEditor() {
    String fichierdde = ouvrir("dde", "Fichier description d'édition (*.dde)");
    if (fichierdde != null) {
      new GfxEditionEditor(fichierdde, null);
    }
    else {
      new GfxEditionEditor(null, null);
    }
  }
  
  /**
   * Recherche le fichier à ouvrir
   */
  private String ouvrir(String extension, String extdescription) {
    String chemin = null;
    final JFileChooser choixFichier = new JFileChooser();
    // choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
    
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { extension }, extdescription));
    // On se place dans le dernier dossier courant s'il y en a un
    if (dernierPath != null) {
      choixFichier.setCurrentDirectory(dernierPath);
    }
    
    // Recuperation du chemin
    if (choixFichier.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
      chemin = choixFichier.getSelectedFile().getPath();
    }
    else {
      return null;
    }
    
    // On stocke comme le dossier courant
    dernierPath = choixFichier.getCurrentDirectory();
    
    return chemin;
  }
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    // Utilisation du Look & Feel Nimbus
    try {
      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
      // UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {
    }
    
    // Démarrage du programme
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        // PreferencesManager.removeFile();
        new EditionEditor();
      }
    });
  }
  
}
