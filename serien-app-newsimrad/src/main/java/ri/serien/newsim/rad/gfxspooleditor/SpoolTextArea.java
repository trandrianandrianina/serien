/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextArea;

import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.outils.Clipboard;

/**
 * Description du textArea pour la sélection des zones
 */
public class SpoolTextArea extends JTextArea {
  // Constantes
  
  // Variables
  protected SelectionModeCaractereListener smcl = null;
  protected ArrayList<SelectionModeCaractere> listeRectangle = new ArrayList<SelectionModeCaractere>();
  protected boolean selectionEncours = false;
  protected int indiceSelection = -1;
  private JComponent libelle = null;
  private JComponent positionCurseur = null;
  
  protected Point positionSouris = null;
  protected GfxPageEditor pageEditor = null;
  
  protected JPopupMenu pm_Btd;
  private JMenuItem mi_Suppimer;
  protected int nbrlignes = 0;
  protected int nbrcolonnes = 0;
  private Color couleur = Color.blue;
  private Color couleurSelection = Color.red;
  private boolean creationAutoLabel = false;
  
  /**
   * Constructeur
   */
  public SpoolTextArea() {
    init();
  }
  
  /**
   * Initialise le composant
   */
  private void init() {
    // Personnalisation du composant
    setEditable(false);
    setSelectionColor(Color.WHITE);
    setSelectedTextColor(getForeground());
    setBorder(null);
    
    // Gestion des évènements
    smcl = new SelectionModeCaractereListener();
    addMouseListener(smcl);
    addMouseMotionListener(smcl);
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseReleased(MouseEvent e) {
        thisMouseReleased(e);
      }
      
      @Override
      public void mousePressed(MouseEvent e) {
        thisMousePressed(e);
      }
    });
    addMouseMotionListener(new MouseAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        super.mouseMoved(e);
        thisMouseMoved(e);
      }
    });
    addKeyListener(smcl);
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        refreshText(null);
      }
    });
    
    // Création du menu contextuel
    creationMenuContextuel();
  }
  
  /**
   * Initialise les dimensions en caractères du composant
   * @param nbrlignes
   * @param nbrcolonnes
   */
  public void setDimensionCaracteres(int nbrlignes, int nbrcolonnes) {
    this.nbrlignes = nbrlignes;
    this.nbrcolonnes = nbrcolonnes;
  }
  
  /**
   * Initialise le composant qui va recevoir le libellé
   * @param composant
   */
  public void setComponentLibelle(final JComponent composant) {
    libelle = composant;
  }
  
  /**
   * Initialise le composant qui va recevoir la position curseur
   * @param composant
   */
  public void setComponentPosCurseur(final JComponent composant) {
    positionCurseur = composant;
  }
  
  /**
   * Sélectionne automatiquement les lignes du spool
   */
  public void selectionAuto() {
    // Récupération de la taille d'un caractère
    FontMetrics fm = getFontMetrics(getFont());
    int w_char = fm.charWidth('M');
    int h_char = fm.getHeight();
    
    // Création des sélections dans une liste provisoire
    int tailleListe = listeRectangle.size();
    for (int i = 0; i < nbrlignes; i++) {
      // Création d'un nouvel élément
      SelectionModeCaractere smc = new SelectionModeCaractere(i, 0, nbrcolonnes, w_char, h_char, true);
      String chaine = smc.getLibelle().replace('@', ' ').trim();
      boolean trouve = false;
      // On recherche la chaine dans la liste officielle (du début) pour éviter les doublons
      for (int j = 0; j < tailleListe; j++) {
        if ((listeRectangle.get(j)).getLibelle().indexOf(chaine) != -1) {
          trouve = true;
          break;
        }
      }
      // Si on n'a pas trouvé on insère la sélection dans la liste
      if (!trouve) {
        listeRectangle.add(smc);
      }
    }
    repaint();
  }
  
  /**
   * Effacement de toutes les sélections
   */
  public void effacerSelection() {
    listeRectangle.clear();
    repaint();
  }
  
  /**
   * Recherche quel est la selection qui a le focus
   * @param e
   */
  protected int findWhoIsSelectionned(MouseEvent e) {
    int indice = -1;
    for (int i = 0; i < listeRectangle.size(); i++) {
      if (listeRectangle.get(i).setIsIn(e.getX(), e.getY())) {
        indice = i;
      }
    }
    repaint();
    
    return indice;
  }
  
  /**
   * Retourne la ligne dans le textearea de la sélection
   * @return
   */
  protected int getLigneSelection(String texte) {
    // Le texte doit être une variable de type @LIGNE...
    if ((texte != null) && (texte.startsWith('@' + SelectionModeCaractere.LIB))) {
      // Récupération de la ligne concernée
      int pos = texte.indexOf(SelectionModeCaractere.LIB) + SelectionModeCaractere.LIB.length();
      try {
        return Integer.parseInt(texte.substring(pos, pos + 2)) - 1;
      }
      catch (Exception e) {
        // JOptionPane.showMessageDialog(this, "Format du libellé incorrect : " + texte, "Erreur",
        // JOptionPane.ERROR_MESSAGE);
        return -1;
      }
    }
    return 0;
  }
  
  /**
   * Rafraichit le texte du composant libellé
   */
  protected void refreshText(SelectionModeCaractere rs) {
    if (libelle == null) {
      return;
    }
    String chaine = getLibelle();
    ((JLabel) libelle).setText(chaine);
    Clipboard.sendTextToClipBoard(chaine);
    
    // Mise à jour dans l'éditeur de page
    if ((pageEditor != null) && (rs != null)) {
      pageEditor.getGfxPage().getEtiquette().modifieObjet(rs.getSauveLibelle(), chaine);
    }
  }
  
  /**
   * Converti une description de page en rectangle (sélection)
   * @param texte
   * @param listeRectangle
   */
  protected void convertDescription2Selection(String texte, ArrayList<SelectionModeCaractere> listeRectangle) {
    if (texte == null) {
      return;
    }
    // Récupération de la taille d'un caractère
    FontMetrics fm = getFontMetrics(getFont());
    int w_char = fm.charWidth('M');
    int h_char = fm.getHeight();
    int posdeb = 0;
    int posfin = 0;
    boolean trim = false;
    
    if (texte.indexOf('@') != -1) {
      // Récupération de la ligne concernée
      int row = getLigneSelection(texte);
      if (row <= 0) {
        if (row == -1) {
          JOptionPane.showMessageDialog(this, "Format du libellé incorrect : " + texte, "Erreur", JOptionPane.ERROR_MESSAGE);
        }
        return;
      }
      
      // Récupération de la colonne
      int colonne = 0;
      posdeb = texte.indexOf('/');
      if (posdeb != -1) {
        posfin = texte.lastIndexOf(',');
        colonne = Integer.parseInt(texte.substring(posdeb + 1, posfin));
      }
      
      // Récupération de la longueur de la sélection
      int longueur = nbrcolonnes;
      posdeb = texte.indexOf(',');
      if (posdeb != -1) {
        posfin = texte.lastIndexOf('^');
        if (posfin == -1) {
          posfin = texte.lastIndexOf('\u00a8');
          if (posfin == -1) {
            JOptionPane.showMessageDialog(this, "Format du libellé incorrect : " + texte, "Erreur", JOptionPane.ERROR_MESSAGE);
            return;
          }
          trim = true;
        }
        longueur = Integer.parseInt(texte.substring(posdeb + 1, posfin));
      }
      
      // Ajout du rectangle après validation de ses infos
      if (texte.startsWith("@" + SelectionModeCaractere.LIB)) {
        listeRectangle.add(new SelectionModeCaractere(row, colonne, longueur, w_char, h_char, longueur == nbrcolonnes));
        listeRectangle.get(listeRectangle.size() - 1).setTrim(trim);
      }
    }
  }
  
  /**
   * Recalcule la taille et la position des selection suite au changement de la taille de la police
   * @param taille
   */
  public void setChgSizeFont(int taille) {
    // Modification de la taille de la police
    Font police = getFont();
    setFont(new Font(police.getName(), police.getStyle(), taille));
    // Récupération de la taille d'un caractère
    FontMetrics fm = getFontMetrics(getFont());
    // Modification de cette taille dans l'écouteur et dans la liste des sélections
    smcl.init(this);
    for (int i = 0; i < listeRectangle.size(); i++) {
      listeRectangle.get(i).setDimChar(fm.charWidth('M'), fm.getHeight());
    }
  }
  
  /**
   * @param creationAutoLabel the creationAutoLabel to set
   */
  public void setCreationAutoLabel(boolean creationAutoLabel) {
    this.creationAutoLabel = creationAutoLabel;
  }
  
  /**
   * Retourne le libellé de la sélection courante
   * @return
   */
  public String getLibelle() {
    if (indiceSelection == -1) {
      return "";
    }
    return listeRectangle.get(indiceSelection).getLibelle();
  }
  
  /**
   * @return the couleur
   */
  public Color getCouleur() {
    return couleur;
  }
  
  /**
   * @param couleur the couleur to set
   */
  public void setCouleur(Color couleur) {
    this.couleur = couleur;
    if (smcl != null) {
      smcl.setCouleur(couleur);
    }
  }
  
  /**
   * @return the couleurSelection
   */
  public Color getCouleurSelection() {
    return couleurSelection;
  }
  
  /**
   * @param couleurSelection the couleurSelection to set
   */
  public void setCouleurSelection(Color couleurSelection) {
    this.couleurSelection = couleurSelection;
    if (smcl != null) {
      smcl.setCouleurSelection(couleurSelection);
    }
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
    smcl.setPageEditor(pageEditor);
  }
  
  // ------------------------------------ Méthodes privées -------------------
  
  /**
   * Création du menu contextuel
   */
  protected void creationMenuContextuel() {
    pm_Btd = new JPopupMenu();
    mi_Suppimer = new JMenuItem();
    
    // setComponentPopupMenu(pm_Btd);
    pm_Btd.setName("pm_Btd");
    // ---- mi_Suppimer ----
    mi_Suppimer.setText("Supprimer");
    mi_Suppimer.setName("mi_Suppimer");
    mi_Suppimer.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mi_SuppimerActionPerformed(e);
      }
    });
    pm_Btd.add(mi_Suppimer);
  }
  
  /**
   * Grise les menuItems si on a pas cliqué sur une sélection
   * @param enable
   */
  protected void setEnabledChild(boolean enable) {
    Component[] composants = pm_Btd.getComponents();
    for (int i = 0; i < composants.length; i++) {
      composants[i].setEnabled(enable);
    }
  }
  
  // ------------------------------------ Evènements -------------------------
  
  /**
   * Action lors du relaché du bouton de la souris
   * @param e
   */
  private void thisMousePressed(MouseEvent e) {
    positionSouris = e.getPoint();
    selectionEncours = true;
    // On recherche si on a cliqué sur une sélection
    indiceSelection = findWhoIsSelectionned(e);
  }
  
  /**
   * Action lors du relaché du bouton de la souris
   * @param e
   */
  private void thisMouseReleased(MouseEvent e) {
    selectionEncours = false;
    // On ajoute la nouvelle sélection à la liste
    if (smcl.getRectangleSelection() != null) {
      for (int i = 0; i < smcl.getRectangleSelection().size(); i++) {
        listeRectangle.add(smcl.getRectangleSelection().get(i));
        if (creationAutoLabel) {
          pageEditor.creationAutoLabel(smcl.getRectangleSelection().get(i).getX(), smcl.getRectangleSelection().get(i).getY(),
              smcl.getRectangleSelection().get(i).getLibelle(), getPreferredSize());
        }
      }
    }
    indiceSelection = findWhoIsSelectionned(e);
    refreshText(null);
    repaint();
    
    // On met dans la liste le rectangle sélectionné (à améliorer) et on active le menu contextuel
    smcl.getRectangleSelection().clear();
    if (indiceSelection >= 0) {
      smcl.getRectangleSelection().add(listeRectangle.get(indiceSelection));
      setComponentPopupMenu(pm_Btd);
    }
    else {
      setComponentPopupMenu(null);
    }
  }
  
  /**
   * Action lors du déplacement de la souris
   * @param e
   */
  private void thisMouseMoved(MouseEvent e) {
    FontMetrics fm = getFontMetrics(getFont());
    int w_char = (e.getPoint().x / fm.charWidth('M')) + 1;
    int h_char = (e.getPoint().y / fm.getHeight()) + 1;
    
    ((JLabel) positionCurseur).setText("Curseur: " + h_char + " / " + w_char);
  }
  
  /**
   * Dessine dans le panel
   */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    
    // On dessine le rectangle en cours uniquement
    if ((smcl != null) && (selectionEncours)) {
      g.setColor(couleurSelection);
      g.drawRect(smcl.x, smcl.y, smcl.w, smcl.h);
    }
    
    // On dessine les rectangles déjà crées
    for (int i = 0; i < listeRectangle.size(); i++) {
      listeRectangle.get(i).paint(g);
    }
  }
  
  protected void mi_SuppimerActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    listeRectangle.remove(indiceSelection);
    repaint();
  }
  
}
