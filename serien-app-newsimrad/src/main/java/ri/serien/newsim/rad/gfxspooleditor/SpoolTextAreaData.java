/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JCheckBoxMenuItem;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.text.BadLocationException;

import ri.serien.newsim.description.DescriptionCodeBarre;
import ri.serien.newsim.description.DescriptionLabel;
import ri.serien.newsim.description.DescriptionPage;

/**
 * Description du textArea pour la sélection des données (liées aux Labels du PageEditor)
 */
public class SpoolTextAreaData extends SpoolTextArea {
  
  private JCheckBoxMenuItem chkmi_Trim;
  private JCheckBoxMenuItem chkmi_SelectionComplete;
  private JMenuItem mi_TrimageGaucheSelection;
  private JMenuItem mi_TrimageDroiteSelection;
  
  /**
   * Constructeur
   */
  public SpoolTextAreaData() {
    super();
    creationMenuContextuel();
    setCouleurSelection(new Color(255, 0, 255));
    smcl.setSelectionBloc(true);
  }
  
  /**
   * Importation de la description pour les données
   */
  public void importerDescription(DescriptionPage dp) {
    // Récupération des sélections (TODO à revoir faire une méthode/classe spécifique gestion des variables @???@)
    ArrayList<?> listeLigne = dp.getDetiquette().getDObject();
    for (int i = 0; i < listeLigne.size(); i++) {
      if (dp.getDetiquette().getDObject().get(i) instanceof DescriptionCodeBarre) {
        convertDescription2Selection(((DescriptionCodeBarre) listeLigne.get(i)).getTexte(), listeRectangle);
      }
      else {
        if (dp.getDetiquette().getDObject().get(i) instanceof DescriptionLabel) {
          convertDescription2Selection(((DescriptionLabel) listeLigne.get(i)).getTexte(), listeRectangle);
        }
      }
    }
    repaint();
  }
  
  /**
   * Création du menu contextuel
   */
  @Override
  protected void creationMenuContextuel() {
    super.creationMenuContextuel();
    
    pm_Btd.addPopupMenuListener(new PopupMenuListener() {
      @Override
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        pm_BtdPopupMenuWillBecomeVisible(e);
      }
      
      @Override
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
      }
      
      @Override
      public void popupMenuCanceled(PopupMenuEvent e) {
      }
    });
    
    chkmi_SelectionComplete = new JCheckBoxMenuItem();
    chkmi_Trim = new JCheckBoxMenuItem();
    mi_TrimageGaucheSelection = new JMenuItem();
    mi_TrimageDroiteSelection = new JMenuItem();
    
    // ---- chkmi_SelectionComplete ----
    chkmi_SelectionComplete.setText("S\u00e9lectionner toute la ligne");
    chkmi_SelectionComplete.setName("chkmi_SelectionComplete");
    chkmi_SelectionComplete.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        chkmi_SelectionCompleteActionPerformed(e);
      }
    });
    pm_Btd.add(chkmi_SelectionComplete);
    
    // ---- mi_TrimageGaucheSelection ----
    mi_TrimageGaucheSelection.setText("Trimage à gauche de la sélection");
    mi_TrimageGaucheSelection.setName("mi_TrimageGaucheSelection");
    mi_TrimageGaucheSelection.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mi_TrimageGaucheSelectionActionPerformed(e);
      }
    });
    pm_Btd.add(mi_TrimageGaucheSelection);
    
    // ---- mi_TrimageDroiteSelection ----
    mi_TrimageDroiteSelection.setText("Trimage à droite de la sélection");
    mi_TrimageDroiteSelection.setName("mi_TrimageDroiteSelection");
    mi_TrimageDroiteSelection.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mi_TrimageDroiteSelectionActionPerformed(e);
      }
    });
    pm_Btd.add(mi_TrimageDroiteSelection);
    
    // ---- Séparateur ----
    pm_Btd.add(new JSeparator());
    
    // ---- chkmi_Trim ----
    chkmi_Trim.setText("Enlever les espaces");
    chkmi_Trim.setName("chkmi_Trim");
    chkmi_Trim.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        chkmi_TrimActionPerformed(e);
      }
    });
    pm_Btd.add(chkmi_Trim);
    
  }
  
  /**
   * Recherche la colonne la plus à droite (1° caractère non blanc)
   * @return
   */
  private int getTrimageSelection(int indiceselection, boolean gauche) {
    int colonne = 0;
    String libelle = listeRectangle.get(indiceselection).getLibelle();
    int indice = getLigneSelection(libelle);
    
    try {
      int deb = getLineStartOffset(indice);
      int fin = getLineEndOffset(indice);
      String texte = getText(deb, fin - deb);
      if (gauche) {
        for (; colonne < texte.length(); colonne++) {
          if (texte.charAt(colonne) != ' ') {
            break;
          }
        }
      }
      else {
        colonne = texte.length();
        do {
          colonne--;
        }
        while ((colonne > 0) && (texte.charAt(colonne) == ' ') && (texte.charAt(colonne) == '\n'));
      }
    }
    catch (BadLocationException e1) {
    }
    
    return colonne;
  }
  
  /**
   * Action lors de l'apparition du menu contextuel
   * @param e
   */
  private void pm_BtdPopupMenuWillBecomeVisible(PopupMenuEvent e) {
    if (positionSouris == null) {
      return;
    }
    
    boolean valeur = false;
    for (int i = 0; i < listeRectangle.size(); i++) {
      valeur = listeRectangle.get(i).setIsIn(positionSouris.x, positionSouris.y);
      if (valeur) {
        break;
      }
    }
    setEnabledChild(valeur);
    // On initialise les valeurs des menuItems
    if ((true) && (indiceSelection != -1)) {
      chkmi_Trim.setSelected(listeRectangle.get(indiceSelection).isTrim());
      chkmi_SelectionComplete.setSelected(listeRectangle.get(indiceSelection).isLignecomplete());
    }
  }
  
  private void chkmi_TrimActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    listeRectangle.get(indiceSelection).setTrim(true);
    refreshText((listeRectangle.get(indiceSelection)));
  }
  
  private void chkmi_SelectionCompleteActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    listeRectangle.get(indiceSelection).setLignecomplete(true);
    listeRectangle.get(indiceSelection).setColonne(0);
    listeRectangle.get(indiceSelection).setLongueur(nbrcolonnes);
    repaint();
    refreshText((listeRectangle.get(indiceSelection)));
  }
  
  private void mi_TrimageGaucheSelectionActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    // listeRectangle.remove(indiceSelection);
    int colonne = getTrimageSelection(indiceSelection, true);
    chkmi_SelectionComplete.setSelected(false);
    listeRectangle.get(indiceSelection).setLignecomplete(colonne == 0);
    listeRectangle.get(indiceSelection).setColonne(colonne);
    listeRectangle.get(indiceSelection).setLongueur(nbrcolonnes - colonne);
    repaint();
    refreshText((listeRectangle.get(indiceSelection)));
  }
  
  private void mi_TrimageDroiteSelectionActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    // listeRectangle.remove(indiceSelection);
    int colonne = getTrimageSelection(indiceSelection, false);
    chkmi_SelectionComplete.setSelected(false);
    listeRectangle.get(indiceSelection).setLignecomplete(colonne == nbrcolonnes);
    listeRectangle.get(indiceSelection).setColonne(0);
    listeRectangle.get(indiceSelection).setLongueur(colonne - 1);
    repaint();
    refreshText((listeRectangle.get(indiceSelection)));
  }
  
}
