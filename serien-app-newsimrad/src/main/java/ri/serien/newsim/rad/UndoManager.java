/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad;

import java.io.File;
import java.io.FileFilter;

import ri.serien.libcommun.outils.encodage.XMLTools;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.newsim.description.DescriptionPage;
import ri.serien.newsim.rad.parametre.PreferencesManager;

/**
 * Gestion du Undo et du Redo
 */
public class UndoManager {
  // Constantes
  private static final String PREFIXE_NAME = "ddp_";
  
  // Variables
  private String tempo_radicalname = null;
  private int current = 0;
  private int maxcurrent = 0;
  
  /**
   * Constructeur
   * @param num
   */
  public UndoManager(int num) {
    tempo_radicalname = PREFIXE_NAME + String.format("%02d", num++) + '-';
    clearTemporyFiles();
  }
  
  /**
   * Returne si le bouton Undo doit être grisé ou non
   * @return
   */
  public boolean isEnableUndoButton() {
    return current > 1;
  }
  
  /**
   * Returne si le bouton Redo doit être grisé ou non
   * @return
   */
  public boolean isEnableRedoButton() {
    return current < maxcurrent;
  }
  
  /**
   * Copie un fichier ddp en temporaire pour le undo
   * @param fileori
   */
  public void copyOriginalFile(FileNG fileori) {
    if (fileori.exists()) {
      save();
      fileori.copyTo(getPathFileName().getAbsolutePath());
    }
  }
  
  /**
   * Gestion du curseur pour la sauvegarde d'une action
   */
  public void save() {
    current++;
    maxcurrent = current;
  }
  
  /**
   * Gestion du curseur sur le redo
   */
  public void next() {
    current++;
    if (current > maxcurrent) {
      current = maxcurrent;
    }
  }
  
  /**
   * Gestion du curseur sur le undo
   */
  public void back() {
    current--;
    if (current < 1) {
      current = 1;
    }
  }
  
  /**
   * Charge la description de la page pour le undo
   * @return
   */
  public DescriptionPage action(boolean undo) {
    File undofile;
    
    // On cherche la description précédente du undo
    if (undo) {
      back();
    }
    else {
      next();
    }
    
    DescriptionPage dpage = null;
    undofile = getPathFileName();
    if (undofile.exists()) {
      // Lecture du fichier ddp
      try {
        dpage = (DescriptionPage) XMLTools.decodeFromFile(undofile.getAbsolutePath());
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    return dpage;
  }
  
  /**
   * Retourne le chemin complet avec le nom du fichier pour la sauvegarde tempo
   * @return
   */
  public File getPathFileName() {
    return new File(PreferencesManager.DOSSIER_TMP + File.separatorChar + tempo_radicalname + String.format("%05d", current) + ".tmp");
  }
  
  /**
   * Retourne le nom du radical des fichiers temporaires
   * @return
   */
  public String getTemporyRadicalName() {
    return tempo_radicalname;
  }
  
  /**
   * Nettoie les fichiers temporaires du Undo une fois le travail terminé
   * @param nameradicalfiles
   */
  public void clearTemporyFiles() {
    File dossier = new File(PreferencesManager.DOSSIER_TMP);
    // Recherche des fichiers temporaires
    File[] listtemporyfiles = dossier.listFiles(new FileFilter() {
      public boolean accept(File pathname) {
        if (pathname.getName().startsWith(tempo_radicalname)) {
          return true;
        }
        return false;
      }
    });
    
    // Suppression des fichiers trouvés
    if (listtemporyfiles != null) {
      for (File file : listtemporyfiles) {
        file.delete();
      }
    }
  }
}
