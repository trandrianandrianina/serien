/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;

public class GfxPropertyPanel extends JPanel {
  
  // Variables
  protected GfxPageEditor pageEditor = null;
  protected String oldvalue = null;
  
  /**
   * Initialise l'éditeur de page
   * @param pageEditor
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * Gestion des focus pour les Spinner
   * @param composant
   */
  protected void gestionSpinnerFocus(final JSpinner composant) {
    ((JSpinner.DefaultEditor) composant.getEditor()).getTextField().addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        oldvalue = ((JSpinner.DefaultEditor) composant.getEditor()).getTextField().getText();
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        if (!oldvalue.equals(((JSpinner.DefaultEditor) composant.getEditor()).getTextField().getText())) {
          pageEditor.save(true);
        }
      }
    });
  }
  
  /**
   * Gestion des focus pour les TextField
   * @param composant
   */
  protected void gestionTextFieldFocus(final JTextField composant) {
    composant.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        oldvalue = composant.getText();
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        if (!oldvalue.equals(composant.getText())) {
          pageEditor.save(true);
        }
      }
    });
  }
  
  /**
   * Gestion des focus pour les Checkbox
   * @param composant
   */
  protected void gestionCheckboxFocus(final JCheckBox composant) {
    composant.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        if (composant.isSelected()) {
          oldvalue = "1";
        }
        else {
          oldvalue = "";
        }
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        if (!oldvalue.equals(composant.isSelected() ? "1" : "")) {
          pageEditor.save(true);
        }
      }
    });
  }
  
  /**
   * Gestion des focus pour les Combobox
   * @param composant
   */
  protected void gestionComboboxFocus(final JComboBox composant) {
    // final JTextField zone = (JTextField) composant.getEditor().getEditorComponent();
    composant.addFocusListener(new FocusAdapter() {
      @Override
      public void focusGained(FocusEvent e) {
        oldvalue = (String) composant.getSelectedItem();
      }
      
      @Override
      public void focusLost(FocusEvent e) {
        if (!oldvalue.equals(composant.getSelectedItem())) {
          pageEditor.save(true);
        }
      }
    });
  }
  
}
