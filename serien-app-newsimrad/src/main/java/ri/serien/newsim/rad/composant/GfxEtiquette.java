/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.AWTEvent;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.LineBorder;

import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;
import ri.serien.newsim.description.DescriptionCodeBarre;
import ri.serien.newsim.description.DescriptionEtiquette;
import ri.serien.newsim.description.DescriptionImage;
import ri.serien.newsim.description.DescriptionLabel;
import ri.serien.newsim.description.DescriptionObject;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.outils.CalculDPI;
import ri.serien.newsim.rad.outils.Clipboard;
import ri.serien.newsim.rad.outils.SelectionLocale;

/**
 * Description de la zone de création
 */
public class GfxEtiquette extends JPanel {
  // Constantes
  
  private static final int NOP = -1;
  
  private static final int ALIGNEH_GAUCHE = 0;
  private static final int ALIGNEH_CENTRE = 1;
  private static final int ALIGNEH_DROITE = 2;
  private static final int ALIGNEV_HAUT = 3;
  private static final int ALIGNEV_CENTRE = 4;
  private static final int ALIGNEV_BAS = 5;
  private static final int MEME_LARGEUR = 6;
  private static final int MEME_HAUTEUR = 7;
  private static final int MEME_ESPACEV = 8;
  private static final int PERSO_ESPACEV = 9;
  private static final int MEME_ESPACEH = 10;
  private static final int PERSO_ESPACEH = 11;
  
  public static final int MODE_DEPLACEMENT = 0;
  public static final int MODE_LABEL = 1;
  public static final int MODE_CODEBARRE = 2;
  public static final int MODE_IMAGE = 3;
  
  // Variables
  // private String dossierTravail="";
  private ArrayList<GfxObject> listeSelectionObject = new ArrayList<GfxObject>();
  
  private int mode = MODE_DEPLACEMENT;
  private GfxPageEditor pageEditor = null;
  private GfxPage page = null;
  // private Image imagefond=null;
  // private File cheminImageFond=null;
  private int dpi_aff = 72;
  private int dpi_img = 72;
  private float largeurEtiquette = ConstantesNewSim.A4_WIDTH;
  private float hauteurEtiquette = ConstantesNewSim.A4_HEIGHT;
  
  private boolean key_ctrl = false;
  private java.awt.datatransfer.Clipboard pressePapiers = new java.awt.datatransfer.Clipboard("local");
  
  /**
   * Constructeur
   */
  public GfxEtiquette(final GfxPageEditor pageEditor) {
    super();
    initComponents();
    this.pageEditor = pageEditor;
    setKeyControler();
  }
  
  /**
   * Constructeur
   * @param detiquette
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxEtiquette(DescriptionEtiquette detiquette, int dpi_aff, int dpi_img, final GfxPageEditor pageEditor) {
    this(pageEditor);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    setLargeurEtiquette(detiquette.getLargeur());
    setHauteurEtiquette(detiquette.getHauteur());
    for (int i = 0; i < detiquette.getDObject().size(); i++) {
      if (detiquette.getDObject().get(i) instanceof DescriptionLabel) {
        GfxLabel label = new GfxLabel((DescriptionLabel) detiquette.getDObject().get(i), dpi_aff, dpi_img, pageEditor);
        add(label);
        // label.setPlanTravail(pageEditor);
        // label.setComponentPopupMenu(popm_Label);
        setMode(MODE_DEPLACEMENT);
        if (pageEditor != null) {
          pageEditor.getPropertyObject().getPanelProperty(label);
        }
      }
      else {
        if (detiquette.getDObject().get(i) instanceof DescriptionImage) {
          GfxImage image = new GfxImage((DescriptionImage) detiquette.getDObject().get(i), dpi_aff, dpi_img, pageEditor);
          add(image);
          // image.setPlanTravail(pageEditor);
          // image.setComponentPopupMenu(popm_Image);
          setMode(MODE_DEPLACEMENT);
          if (pageEditor != null) {
            pageEditor.getPropertyObject().getPanelProperty(image);
          }
        }
        else {
          if (detiquette.getDObject().get(i) instanceof DescriptionCodeBarre) {
            GfxCodeBarre codebarre =
                new GfxCodeBarre((DescriptionCodeBarre) detiquette.getDObject().get(i), dpi_aff, dpi_img, pageEditor);
            add(codebarre);
            // image.setPlanTravail(pageEditor);
            // image.setComponentPopupMenu(popm_Image);
            setMode(MODE_DEPLACEMENT);
            if (pageEditor != null) {
              pageEditor.getPropertyObject().getPanelProperty(codebarre);
            }
          }
        }
      }
    }
  }
  
  /**
   * Met en place la surveillance de la touche control
   */
  private void setKeyControler() {
    Toolkit.getDefaultToolkit().addAWTEventListener(new AWTEventListener() {
      @Override
      public void eventDispatched(AWTEvent awte) {
        KeyEvent e = (KeyEvent) awte;
        key_ctrl = (e.isMetaDown() || e.isControlDown());
      }
    }, AWTEvent.KEY_EVENT_MASK);
  }
  
  /**
   * Initialise les infos pour le Rad
   * @param rad
   * @param pageEditor
   * @param zoneCreation
   */
  public void setInfos(final GfxPageEditor planTravail, final GfxPage page) {
    this.page = page;
    // if (planTravail != null)
    // setDossierTravail(planTravail.getDossierTravail());
    initPreparation();
  }
  
  /**
   * Initialise la création d'une nouvelle étiquette
   */
  public void initPreparation() {
    Dimension d =
        new Dimension(CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurEtiquette), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurEtiquette));
    setSize(d);
    setMinimumSize(d);
    setPreferredSize(d);
  }
  
  /**
   * @param mode the mode to set
   */
  public void setMode(int mode) {
    this.mode = mode;
    switch (mode) {
      case MODE_DEPLACEMENT:
        setCursor(Cursor.getDefaultCursor());
        break;
      case MODE_LABEL:
        setCursor(new Cursor(Cursor.TEXT_CURSOR)); // TODO trouver icone qui va bien
        break;
      case MODE_CODEBARRE:
        setCursor(new Cursor(Cursor.TEXT_CURSOR)); // TODO trouver icone qui va bien
        break;
      case MODE_IMAGE:
        setCursor(new Cursor(Cursor.TEXT_CURSOR)); // TODO trouver icone qui va bien
        break;
    }
    pageEditor.setMode(mode);
  }
  
  /**
   * @return the mode
   */
  public int getMode() {
    return mode;
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * @param dossierTravail the dossierTravail to set
   * 
   *          public void setDossierTravail(String dossierTravail)
   *          {
   *          this.dossierTravail = dossierTravail;
   *          }
   * 
   *          /**
   * @return the dossierTravail
   * 
   *         public String getDossierTravail()
   *         {
   *         return dossierTravail;
   *         }
   */
  
  /**
   * Retourne le menu contextuel des labels
   * @return
   */
  public JPopupMenu getPopMenuLabel() {
    return popm_Label;
  }
  
  /**
   * Retourne le menu contextuel des images
   * @return
   */
  public JPopupMenu getPopMenuImage() {
    return popm_Image;
  }
  
  /**
   * Retourne le menu contextuel des codes barres
   * @return
   */
  public JPopupMenu getPopMenuCodeBarre() {
    return popm_CodeBarre;
  }
  
  /**
   * Retourne le menu contextuel des Sélections Multiples
   * @return
   */
  public JPopupMenu getPopMenuMultiSelection() {
    boolean enable3 = (listeSelectionObject.size() >= 3);
    boolean enable2 = (listeSelectionObject.size() >= 2);
    mi_MemeEspaceVertical.setEnabled(enable3);
    m_EspaceVertical.setEnabled(enable2);
    mi_MemeEspaceHorizontal.setEnabled(enable3);
    m_EspaceHorizontal.setEnabled(enable2);
    return popm_MultiSelection;
  }
  
  /***
   * Création d'un objet label
   * @param x
   * @param y
   */
  public void creationLabel(int x, int y, String libelle) {
    if (libelle == null) {
      libelle = "@NOUVELLE_LIGNE@";
    }
    GfxLabel label = new GfxLabel(libelle, x, y, dpi_aff, dpi_img, pageEditor);
    add(label);
    label.setFocusBorder(true, false, false);
    
    setMode(MODE_DEPLACEMENT);
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(label);
      pageEditor.save(true);
    }
    repaint();
  }
  
  /***
   * Création d'un objet image
   * @param x
   * @param y
   */
  private void creationImage(int x, int y) {
    GfxImage image = new GfxImage(x, y, dpi_aff, dpi_img, pageEditor);
    add(image);
    image.setFocusBorder(true, false, false);
    
    setMode(MODE_DEPLACEMENT);
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(image);
      pageEditor.save(true);
    }
    repaint();
  }
  
  /***
   * Création d'un objet code barre
   * @param x
   * @param y
   */
  private void creationCodeBarre(int x, int y) {
    GfxCodeBarre codebarre = new GfxCodeBarre(x, y, dpi_aff, dpi_img, pageEditor);
    add(codebarre);
    codebarre.setFocusBorder(true, false, false);
    
    setMode(MODE_DEPLACEMENT);
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(codebarre);
      pageEditor.save(true);
    }
    repaint();
  }
  
  /**
   * Dessine une grille pour faciliter la positionenment des composants
   */
  private void dessineGrille(Graphics g) {
    // Dimensionnement de la zone de création
    Graphics2D g2d = (Graphics2D) g;
    
    g2d.setColor(new Color(210, 210, 210));
    // Création des lignes verticales
    int largeurCarreau = CalculDPI.getCmtoPx(dpi_aff, dpi_img, 0.5f);
    int largeurTotale = largeurCarreau;
    int largeurEtiquettePx = CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurEtiquette);
    while (largeurTotale < largeurEtiquettePx) {
      g2d.drawLine(CalculDPI.getCmtoPx(dpi_aff, dpi_img, page.getMargeXEtiquette()) + largeurTotale, 0,
          CalculDPI.getCmtoPx(dpi_aff, dpi_img, page.getMargeXEtiquette()) + largeurTotale,
          CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurEtiquette));
      largeurTotale += largeurCarreau;
    }
    // Création des lignes horizontales
    largeurTotale = largeurCarreau;
    int hauteurEtiquettePx = CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurEtiquette);
    while (largeurTotale < hauteurEtiquettePx) {
      g2d.drawLine(0, CalculDPI.getCmtoPx(dpi_aff, dpi_img, page.getMargeYEtiquette()) + largeurTotale,
          CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurEtiquette),
          CalculDPI.getCmtoPx(dpi_aff, dpi_img, page.getMargeYEtiquette()) + largeurTotale);
      largeurTotale += largeurCarreau;
    }
  }
  
  /**
   * Dessine dans le panel
   */
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    // if (imagefond != null)
    // g.drawImage(imagefond, CalculDPI.getCmtoPx(dpi_aff, dpi_img, margeXEtiquette), CalculDPI.getCmtoPx(dpi_aff,
    // dpi_img, margeYEtiquette), null);
    if (pageEditor.isDspGrid()) {
      dessineGrille(g);
    }
    dessineContourEtiquette(g);
  }
  
  /**
   * Dessine le contour de l'étiquette (les éléments figés)
   * @param parent
   */
  public void dessineContourEtiquette(Graphics g) {
    // Dimensionnement de la zone de création
    Graphics2D g2d = (Graphics2D) g;
    
    // On dessine le contour de l'étiquette
    g2d.setColor(Color.black);
    g2d.drawRect(0, 0, CalculDPI.getCmtoPx(dpi_aff, dpi_img, largeurEtiquette), CalculDPI.getCmtoPx(dpi_aff, dpi_img, hauteurEtiquette));
  }
  
  /**
   * Retourne l'image de fond de la page
   * @return
   * 
   *         public Image getFondPage()
   *         {
   *         if (cheminImageFond == null) return null;
   * 
   *         // On affiche le fond s'il y en a un
   *         if (cheminImageFond.exists())
   *         if (cheminImageFond.getName().toLowerCase().endsWith(".svg"))
   *         {
   *         try
   *         {
   *         URL urlFond = cheminImageFond.toURI().toURL();
   *         return SVGImage.getImageFromSvg(urlFond, CalculDPI.DPI_ECRAN);
   *         }
   *         catch(Exception e){}
   *         }
   *         else
   *         return new ImageIcon(cheminImageFond.getAbsolutePath()).getImage();
   * 
   *         return null;
   *         }
   */
  /**
   * Recherche le fichier à ouvrir
   * 
   * public String ouvrir(String[] extension, String fichier)
   * {
   * String chemin = null;
   * final JFileChooser choixFichier = new JFileChooser();
   * //choixFichier.getIcon(new URI(getClass().getResource("/images/m.gif").getFile()).getPath());
   * 
   * choixFichier.addChoosableFileFilter(new GestionFiltre(extension, fichier));
   * // On se place dans le dernier dossier courant s'il y en a un
   * if (dernierPath != null)
   * choixFichier.setCurrentDirectory(dernierPath);
   * 
   * // Recuperation du chemin
   * if (choixFichier.showOpenDialog(this) == JFileChooser.APPROVE_OPTION)
   * chemin = choixFichier.getSelectedFile().getPath();
   * else
   * return null;
   * 
   * // On stocke comme le dossier courant
   * dernierPath = choixFichier.getCurrentDirectory();
   * 
   * return chemin;
   * }
   */
  
  /**
   * Retourne la liste des objets sélectionnés
   */
  public ArrayList<GfxObject> getSelectionGfxObjet() {
    listeSelectionObject.clear();
    Component[] listeComposant = getComponents();
    for (int i = 0; i < listeComposant.length; i++) {
      if ((listeComposant[i] instanceof GfxObject) && (((GfxObject) listeComposant[i]).isFocusBorder())) {
        listeSelectionObject.add((GfxObject) listeComposant[i]);
      }
    }
    return listeSelectionObject;
  }
  
  /**
   * Déselectionne tous les objets
   */
  public void setDeselectionGfxObjet() {
    Component[] listeComposant = getComponents();
    for (int i = 0; i < listeComposant.length; i++) {
      if ((listeComposant[i] instanceof GfxObject) && (((GfxObject) listeComposant[i]).isFocusBorder())) {
        ((GfxObject) listeComposant[i]).setFocusBorder(false, false, false);
      }
    }
    listeSelectionObject.clear();
  }
  
  /**
   * Recherche et met à jour le libellé d'un objet
   * @param libelleactuel
   * @param nouveaulibelle
   */
  public void modifieObjet(String libelleactuel, String nouveaulibelle) {
    // Evite le bug qui met tous les labels avec la même variable
    if (libelleactuel.equals("")) {
      return;
    }
    
    Component[] listeComposant = getComponents();
    for (int i = 0; i < listeComposant.length; i++) {
      if (listeComposant[i] instanceof GfxLabel) {
        if (((GfxLabel) listeComposant[i]).getText().indexOf(libelleactuel) != -1) {
          // libelleactuel+"|");
          ((GfxLabel) listeComposant[i]).setText(nouveaulibelle);
        }
      }
    }
  }
  
  /**
   * Gestion clic souris
   * @param e
   */
  private void thisMouseClicked(MouseEvent e) {
    // Si on clique dans la page alors cela désélectionne les objets
    setDeselectionGfxObjet();
    
    // Action en fonction du mode en cours
    switch (mode) {
      case MODE_DEPLACEMENT:
        // selectionGfxObjet(e);
        break;
      case MODE_LABEL:
        creationLabel(e.getX(), e.getY(), null);
        break;
      case MODE_IMAGE:
        creationImage(e.getX(), e.getY());
        break;
      case MODE_CODEBARRE:
        creationCodeBarre(e.getX(), e.getY());
        break;
    }
  }
  
  /**
   * @param cheminImageFond the cheminImageFond to set
   * 
   *          public void setCheminImageFond(File cheminImageFond)
   *          {
   *          this.cheminImageFond = cheminImageFond;
   *          imagefond = getFondPage();
   *          repaint();
   *          }
   */
  
  /**
   * @return the cheminImageFond
   * 
   *         public File getCheminImageFond()
   *         {
   *         return cheminImageFond;
   *         }
   */
  
  /**
   * @param largeurEtiquette the largeurEtiquette to set
   */
  public void setLargeurEtiquette(float largeurEtiquette) {
    this.largeurEtiquette = largeurEtiquette;
  }
  
  /**
   * @return the largeurEtiquette
   */
  public float getLargeurEtiquette() {
    return largeurEtiquette;
  }
  
  /**
   * @param hauteurEtiquette the hauteurEtiquette to set
   */
  public void setHauteurEtiquette(float hauteurEtiquette) {
    this.hauteurEtiquette = hauteurEtiquette;
  }
  
  /**
   * @return the hauteurEtiquette
   */
  public float getHauteurEtiquette() {
    return hauteurEtiquette;
  }
  
  /**
   * Retourne la description de la page courante
   * @return
   */
  public DescriptionEtiquette getDescriptionEtiquette() {
    // Données pour une étiquette
    DescriptionEtiquette detiquette = new DescriptionEtiquette();
    // detiquette.setMarge_x(margeXEtiquette);
    // detiquette.setMarge_y(margeYEtiquette);
    detiquette.setHauteur(hauteurEtiquette);
    detiquette.setLargeur(largeurEtiquette);
    
    // Les composants
    ArrayList<DescriptionObject> listeObjet = detiquette.getDObject();
    Component[] listeObjetPage = getComponents();
    for (int i = 0; i < listeObjetPage.length; i++) {
      if (listeObjetPage[i] instanceof GfxLabel) {
        listeObjet.add(((GfxLabel) listeObjetPage[i]).getDescription());
      }
      else {
        if (listeObjetPage[i] instanceof GfxImage) {
          listeObjet.add(((GfxImage) listeObjetPage[i]).getDescription());
        }
        else {
          if (listeObjetPage[i] instanceof GfxCodeBarre) {
            listeObjet.add(((GfxCodeBarre) listeObjetPage[i]).getDescription());
          }
        }
      }
    }
    
    return detiquette;
  }
  
  /**
   * @return the dpi
   */
  public int getDpiAff() {
    return dpi_aff;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiAff(int dpi) {
    this.dpi_aff = dpi;
    Component[] listeobjet = getComponents();
    for (int i = 0; i < listeobjet.length; i++) {
      ((GfxObject) listeobjet[i]).setDpiAff(dpi_aff);
    }
    
    initPreparation();
    repaint();
  }
  
  /**
   * @return the dpi
   */
  public int getDpiImg() {
    return dpi_img;
  }
  
  /**
   * @param dpi the dpi to set
   */
  public void setDpiImg(int dpi) {
    this.dpi_img = dpi;
    Component[] listeobjet = getComponents();
    for (int i = 0; i < listeobjet.length; i++) {
      ((GfxObject) listeobjet[i]).setDpiImg(dpi_img);
    }
    
    initPreparation();
    repaint();
  }
  
  /**
   * Gère l'alignement des objets sélectionnés
   * @param e
   * @param aligne
   */
  private void setAlignObjet(ActionEvent e, int ope, int valeur) {
    GfxObject composant = null;
    int val = 0;
    int data[] = null;
    boolean excluComposant = true;
    
    // On récupère le composant référence et ses données qui nous intéresse
    if (valeur == NOP) {
      composant = (GfxObject) ((JPopupMenu) ((JMenuItem) e.getSource()).getParent()).getInvoker();
    }
    else {
      composant = (GfxObject) ((JPopupMenu) ((JMenu) ((JPopupMenu) ((JMenuItem) e.getSource()).getParent()).getInvoker()).getParent())
          .getInvoker();
    }
    // ((JPopupMenu)((JMenu)((JPopupMenu)((JMenuItem)e.getSource()).getParent()).getInvoker()).getParent()).getInvoker());
    
    switch (ope) {
      // Alignement horizontal
      case ALIGNEH_GAUCHE:
        val = composant.getLocation().x;
        break;
      case ALIGNEH_CENTRE:
        val = composant.getLocation().x + composant.getWidth() / 2;
        break;
      case ALIGNEH_DROITE:
        val = composant.getLocation().x + composant.getWidth();
        break;
      // Alignement vertical
      case ALIGNEV_HAUT:
        val = composant.getLocation().y;
        break;
      case ALIGNEV_CENTRE:
        val = composant.getLocation().y + composant.getHeight() / 2;
        break;
      case ALIGNEV_BAS:
        val = composant.getLocation().y + composant.getHeight();
        break;
      // Dimension
      case MEME_LARGEUR:
        val = composant.getWidth();
        break;
      case MEME_HAUTEUR:
        val = composant.getHeight();
        break;
      // Espacement vertical
      case MEME_ESPACEV:
        data = getDeltaVertical(NOP);
        excluComposant = false;
        break;
      case PERSO_ESPACEV:
        data = getDeltaVertical(valeur);
        excluComposant = false;
        break;
      // Espacement horizontal
      case MEME_ESPACEH:
        data = getDeltaHorizontal(NOP);
        excluComposant = false;
        break;
      case PERSO_ESPACEH:
        data = getDeltaHorizontal(valeur);
        excluComposant = false;
        break;
    }
    
    // On modifie la sélection
    // for (int i=0; i<listeSelectionObject.size(); i++)
    for (int i = listeSelectionObject.size(); --i >= 0;) {
      if (excluComposant && listeSelectionObject.get(i).equals(composant)) {
        continue;
      }
      switch (ope) {
        // Alignement horizontal
        case ALIGNEH_GAUCHE:
          listeSelectionObject.get(i).setLocation(val, listeSelectionObject.get(i).getLocation().y);
          break;
        case ALIGNEH_CENTRE:
          listeSelectionObject.get(i).setLocation(val - listeSelectionObject.get(i).getWidth() / 2,
              listeSelectionObject.get(i).getLocation().y);
          break;
        case ALIGNEH_DROITE:
          listeSelectionObject.get(i).setLocation(val - listeSelectionObject.get(i).getWidth(),
              listeSelectionObject.get(i).getLocation().y);
          break;
        // Alignement vertical
        case ALIGNEV_HAUT:
          listeSelectionObject.get(i).setLocation(listeSelectionObject.get(i).getLocation().x, val);
          break;
        case ALIGNEV_CENTRE:
          listeSelectionObject.get(i).setLocation(listeSelectionObject.get(i).getLocation().x,
              val - listeSelectionObject.get(i).getHeight() / 2);
          break;
        case ALIGNEV_BAS:
          listeSelectionObject.get(i).setLocation(listeSelectionObject.get(i).getLocation().x,
              val - listeSelectionObject.get(i).getHeight());
          break;
        // Dimension
        case MEME_LARGEUR:
          listeSelectionObject.get(i).setSize(val, listeSelectionObject.get(i).getHeight());
          break;
        case MEME_HAUTEUR:
          listeSelectionObject.get(i).setSize(listeSelectionObject.get(i).getWidth(), val);
          break;
        // Espacement vertical
        case MEME_ESPACEV:
        case PERSO_ESPACEV:
          if (data != null) {
            listeSelectionObject.get(i).setLocation(listeSelectionObject.get(i).getLocation().x, data[i]);
          }
          break;
        // Espacement horizontal
        case MEME_ESPACEH:
        case PERSO_ESPACEH:
          if (data != null) {
            listeSelectionObject.get(i).setLocation(data[i], listeSelectionObject.get(i).getLocation().y);
          }
          break;
      }
    }
  }
  
  /**
   * Retourne le delta vertical à appliquer (doit calculer l'espacement entre les objets, à confirmer)
   * @return
   */
  private int[] getDeltaVertical(int delta) {
    int ymin = 999999;
    int ymax = 0;
    int epaisseur = 0;
    int[] data = new int[listeSelectionObject.size()];
    
    ymin = listeSelectionObject.get(0).getLocation().y;
    ymax = listeSelectionObject.get(listeSelectionObject.size() - 1).getLocation().y;
    // On parcourt la liste de manière croissante
    for (int i = 0; i < listeSelectionObject.size() - 1; i++) {
      epaisseur += listeSelectionObject.get(i).getHeight();
      listeSelectionObject.get(i).setYtrie(true);
    }
    
    // Calcul du delta
    if (delta == NOP) {
      delta = (ymax - ymin - epaisseur) / (listeSelectionObject.size() - 1);
      if (delta < 0) {
        delta = 0;
      }
    }
    // Trie de la liste par ordre croissant
    Collections.sort(listeSelectionObject);
    
    // Recalcul des coordonnées
    data[0] = listeSelectionObject.get(0).getLocation().y;
    epaisseur = listeSelectionObject.get(0).getHeight();
    for (int i = 1; i < listeSelectionObject.size(); i++) {
      data[i] = data[i - 1] + epaisseur + delta;
      epaisseur = listeSelectionObject.get(i).getHeight();
    }
    
    return data;
  }
  
  /**
   * Retourne le delta horizontal à appliquer (doit calculer l'espacement entre les objets, à confirmer)
   * @return
   */
  private int[] getDeltaHorizontal(int delta) {
    int xmin = 999999;
    int xmax = 0;
    int largeur = 0;
    int[] data = new int[listeSelectionObject.size()];
    
    xmin = listeSelectionObject.get(0).getLocation().x;
    xmax = listeSelectionObject.get(listeSelectionObject.size() - 1).getLocation().x;
    // On parcourt la liste de manière croissante
    for (int i = 0; i < listeSelectionObject.size() - 1; i++) {
      largeur += listeSelectionObject.get(i).getWidth();
      listeSelectionObject.get(i).setYtrie(false);
    }
    
    // Calcul du delta
    if (delta == NOP) {
      delta = (xmax - xmin - largeur) / (listeSelectionObject.size() - 1);
      if (delta < 0) {
        delta = 0;
      }
    }
    // Trie de la liste par ordre croissant
    Collections.sort(listeSelectionObject);
    
    // Recalcul des coordonnées
    data[0] = listeSelectionObject.get(0).getLocation().x;
    largeur = listeSelectionObject.get(0).getWidth();
    for (int i = 1; i < listeSelectionObject.size(); i++) {
      data[i] = data[i - 1] + largeur + delta;
      largeur = listeSelectionObject.get(i).getWidth();
    }
    
    return data;
  }
  
  /**
   * Sélection avec la souris de plusieurs objets
   * @param e
   */
  private void selectionMultiObjet(MouseEvent e) {
    if (key_ctrl == false) {
      return;
    }
    
    int mx = e.getX();
    int my = e.getY();
    int x = 0;
    int w = 0;
    int y = 0;
    int h = 0;
    // listeSelectionObject.clear();
    Component[] listeComposant = getComponents();
    for (int i = 0; i < listeComposant.length; i++) {
      x = listeComposant[i].getX();
      y = listeComposant[i].getY();
      w = listeComposant[i].getWidth();
      h = listeComposant[i].getHeight();
      if ((listeComposant[i] instanceof GfxObject) && (!((GfxObject) listeComposant[i]).isFocusBorder())
          && ((mx >= x) && (mx <= (x + w) && (my >= y) && (my <= (y + h))))) {
        if ((listeSelectionObject.size() > 0) && (!listeComposant[i].getClass().equals(listeSelectionObject.get(0).getClass()))) {
          continue;
        }
        listeSelectionObject.add((GfxObject) listeComposant[i]);
        ((GfxObject) listeComposant[i]).setFocusBorder(true);
        if (pageEditor != null) {
          pageEditor.getPropertyObject().getPanelProperty((GfxObject) listeComposant[i]);
        }
      }
    }
  }
  
  /**
   * Copie l'objet dans le presse papier
   * @param objet
   */
  private void copierObjet(GfxObject objet) {
    // On récupère la description
    DescriptionObject dobj = null;
    if (objet instanceof GfxLabel) {
      dobj = ((GfxLabel) objet).getDescription();
    }
    else {
      if (objet instanceof GfxImage) {
        dobj = ((GfxImage) objet).getDescription();
      }
      else {
        if (objet instanceof GfxCodeBarre) {
          dobj = ((GfxCodeBarre) objet).getDescription();
        }
      }
    }
    
    // On le met dans le presse papier
    SelectionLocale locale = new SelectionLocale(dobj);
    pressePapiers.setContents(locale, null);
  }
  
  /**
   * Colle l'objet dans l'Etiquette
   */
  private void collerObjet(int x, int y) {
    // Controle les cordonnées de collage
    if (x < 0) {
      x = 0;
    }
    if (y < 0) {
      y = 0;
    }
    
    Transferable contenu = pressePapiers.getContents(null);
    if (contenu == null) {
      // setTitle("Presse-papiers vide");
      return;
    }
    String typeMIME = "application/x-java-jvm-local-objectref;class=" + DescriptionObject.class.getName();
    try {
      DataFlavor flavor = new DataFlavor(typeMIME);
      if (contenu.isDataFlavorSupported(flavor)) {
        DescriptionObject dobj = (DescriptionObject) contenu.getTransferData(flavor);
        dobj.setXPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, x));
        dobj.setYPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, y));
        
        if (dobj instanceof DescriptionLabel) {
          GfxLabel objet = new GfxLabel((DescriptionLabel) dobj, dpi_aff, dpi_img, pageEditor);
          add(objet);
        }
        else {
          if (dobj instanceof DescriptionImage) {
            GfxImage objet = new GfxImage((DescriptionImage) dobj, dpi_aff, dpi_img, pageEditor);
            add(objet);
          }
          else {
            if (dobj instanceof DescriptionCodeBarre) {
              GfxCodeBarre objet = new GfxCodeBarre((DescriptionCodeBarre) dobj, dpi_aff, dpi_img, pageEditor);
              add(objet);
            }
          }
        }
      }
    }
    catch (Exception ex) {
      // setTitle("Problème de transfert");
    }
  }
  
  /**
   * Retourne si la touche contrôle est enfoncée ou pas
   */
  public boolean isKey_ctrl() {
    return key_ctrl;
  }
  
  /**
   * Affiche le menu contextuel
   * @param e
   * @param btd
   */
  private void maybeShowPopup(MouseEvent e, JPopupMenu btd) {
    btd.show(e.getComponent(), e.getX(), e.getY());
  }
  
  // ---> Evènements <--------------------------------------------------------
  
  private void thisMouseMoved(MouseEvent e) {
    page.setPositionSouris(getX() + e.getX(), getY() + e.getY());
  }
  
  private void mi_SupprimerImageActionPerformed(ActionEvent e) {
    // modele.setModif(true);
    remove(popm_Image.getInvoker());
    repaint();
  }
  
  private void mi_SupprimerActionPerformed(ActionEvent e) {
    // modele.setModif(true);
    remove(popm_Label.getInvoker());
    repaint();
  }
  
  private void mi_CollerNomActionPerformed(ActionEvent e) {
    ((GfxLabel) popm_Label.getInvoker()).setText(Clipboard.getTextFromClipboard());
    // Ajout de la mise à jour des propriétés
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty((GfxLabel) popm_Label.getInvoker());
    }
  }
  
  private void mi_ChgImageActionPerformed(ActionEvent e) {
    ((GfxImage) popm_Image.getInvoker()).parcourirImage();
  }
  
  private void mi_AlignHGaucheActionPerformed(ActionEvent e) {
    setAlignObjet(e, ALIGNEH_GAUCHE, NOP);
  }
  
  private void mi_AlignHCentreActionPerformed(ActionEvent e) {
    setAlignObjet(e, ALIGNEH_CENTRE, NOP);
  }
  
  private void mi_AlignHDroiteActionPerformed(ActionEvent e) {
    setAlignObjet(e, ALIGNEH_DROITE, NOP);
  }
  
  private void mi_AlignVHautActionPerformed(ActionEvent e) {
    setAlignObjet(e, ALIGNEV_HAUT, NOP);
  }
  
  private void mi_AlignVCentreActionPerformed(ActionEvent e) {
    setAlignObjet(e, ALIGNEV_CENTRE, NOP);
  }
  
  private void mi_AlignVBasActionPerformed(ActionEvent e) {
    setAlignObjet(e, ALIGNEV_BAS, NOP);
  }
  
  private void mi_MmLargeurActionPerformed(ActionEvent e) {
    setAlignObjet(e, MEME_LARGEUR, NOP);
  }
  
  private void mi_MmHauteurActionPerformed(ActionEvent e) {
    setAlignObjet(e, MEME_HAUTEUR, NOP);
  }
  
  private void mi_MemeEspaceVerticalActionPerformed(ActionEvent e) {
    setAlignObjet(e, MEME_ESPACEV, NOP);
  }
  
  private void mi_V1pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 1);
  }
  
  private void mi_V2pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 2);
  }
  
  private void mi_V3pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 3);
  }
  
  private void mi_V4pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 4);
  }
  
  private void mi_V5pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 5);
  }
  
  private void mi_V6pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 6);
  }
  
  private void mi_V7pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 7);
  }
  
  private void mi_V8pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 8);
  }
  
  private void mi_V9pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 9);
  }
  
  private void mi_V10pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 10);
  }
  
  private void mi_MemeEspaceHorizontalActionPerformed(ActionEvent e) {
    setAlignObjet(e, MEME_ESPACEH, NOP);
  }
  
  private void mi_H1pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 1);
  }
  
  private void mi_H2pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 2);
  }
  
  private void mi_H3pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 3);
  }
  
  private void mi_H4pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 4);
  }
  
  private void mi_H5pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 5);
  }
  
  private void mi_H6pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 6);
  }
  
  private void mi_H7pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 7);
  }
  
  private void mi_H8pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 8);
  }
  
  private void mi_H9pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 9);
  }
  
  private void mi_H10pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 10);
  }
  
  private void mi_V0pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEV, 0);
  }
  
  private void mi_H0pxActionPerformed(ActionEvent e) {
    setAlignObjet(e, PERSO_ESPACEH, 0);
  }
  
  private void thisMouseDragged(MouseEvent e) {
    selectionMultiObjet(e);
  }
  
  private void mi_CopierObjetLabelActionPerformed(ActionEvent e) {
    copierObjet((GfxObject) popm_Label.getInvoker());
    mi_CollerObjet.setEnabled(true);
  }
  
  private void mi_CollerObjetActionPerformed(ActionEvent e) {
    collerObjet(getMousePosition().x - mi_CollerObjet.getX(), getMousePosition().y - mi_CollerObjet.getY());
    mi_CollerObjet.setEnabled(false);
  }
  
  private void mi_CopierObjetImageActionPerformed(ActionEvent e) {
    copierObjet((GfxObject) popm_Image.getInvoker());
    mi_CollerObjet.setEnabled(true);
  }
  
  private void mi_SupprimerCodeBarreActionPerformed(ActionEvent e) {
    remove(popm_CodeBarre.getInvoker());
    repaint();
  }
  
  private void mi_CopierObjetCodeBarreActionPerformed(ActionEvent e) {
    copierObjet((GfxObject) popm_CodeBarre.getInvoker());
    mi_CollerObjet.setEnabled(true);
  }
  
  private void thisMouseReleased(MouseEvent e) {
    if (SwingUtilities.isRightMouseButton(e)) {
      maybeShowPopup(e, popm_Etiquette);
    }
  }
  
  private void mi_OriginSizeActionPerformed(ActionEvent e) {
    ((GfxImage) popm_Image.getInvoker()).setOriginalSize();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    popm_Label = new JPopupMenu();
    mi_CollerNom = new JMenuItem();
    mi_Supprimer = new JMenuItem();
    mi_CopierObjetLabel = new JMenuItem();
    popm_Image = new JPopupMenu();
    mi_ChgImage = new JMenuItem();
    mi_OriginSize = new JMenuItem();
    mi_SupprimerImage = new JMenuItem();
    mi_CopierObjetImage = new JMenuItem();
    popm_CodeBarre = new JPopupMenu();
    mi_SupprimerCodeBarre = new JMenuItem();
    mi_CopierObjetCodeBarre = new JMenuItem();
    popm_MultiSelection = new JPopupMenu();
    mi_AlignHGauche = new JMenuItem();
    mi_AlignHCentre = new JMenuItem();
    mi_AlignHDroite = new JMenuItem();
    mi_AlignVHaut = new JMenuItem();
    mi_AlignVCentre = new JMenuItem();
    mi_AlignVBas = new JMenuItem();
    mi_MmLargeur = new JMenuItem();
    mi_MmHauteur = new JMenuItem();
    mi_MemeEspaceVertical = new JMenuItem();
    m_EspaceVertical = new JMenu();
    mi_V0px = new JMenuItem();
    mi_V1px = new JMenuItem();
    mi_V2px = new JMenuItem();
    mi_V3px = new JMenuItem();
    mi_V4px = new JMenuItem();
    mi_V5px = new JMenuItem();
    mi_V6px = new JMenuItem();
    mi_V7px = new JMenuItem();
    mi_V8px = new JMenuItem();
    mi_V9px = new JMenuItem();
    mi_V10px = new JMenuItem();
    mi_MemeEspaceHorizontal = new JMenuItem();
    m_EspaceHorizontal = new JMenu();
    mi_H0px = new JMenuItem();
    mi_H1px = new JMenuItem();
    mi_H2px = new JMenuItem();
    mi_H3px = new JMenuItem();
    mi_H4px = new JMenuItem();
    mi_H5px = new JMenuItem();
    mi_H6px = new JMenuItem();
    mi_H7px = new JMenuItem();
    mi_H8px = new JMenuItem();
    mi_H9px = new JMenuItem();
    mi_H10px = new JMenuItem();
    popm_Etiquette = new JPopupMenu();
    mi_CollerObjet = new JMenuItem();
    
    // ======== this ========
    setBorder(LineBorder.createBlackLineBorder());
    setOpaque(false);
    setComponentPopupMenu(null);
    setName("this");
    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        thisMouseClicked(e);
      }
      
      @Override
      public void mouseReleased(MouseEvent e) {
        thisMouseReleased(e);
      }
    });
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseDragged(MouseEvent e) {
        thisMouseDragged(e);
      }
      
      @Override
      public void mouseMoved(MouseEvent e) {
        thisMouseMoved(e);
      }
    });
    setLayout(null);
    
    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    
    // ======== popm_Label ========
    {
      popm_Label.setName("popm_Label");
      
      // ---- mi_CollerNom ----
      mi_CollerNom.setText("Coller le nom");
      mi_CollerNom.setName("mi_CollerNom");
      mi_CollerNom.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_CollerNomActionPerformed(e);
        }
      });
      popm_Label.add(mi_CollerNom);
      
      // ---- mi_Supprimer ----
      mi_Supprimer.setText("Supprimer");
      mi_Supprimer.setName("mi_Supprimer");
      mi_Supprimer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_SupprimerActionPerformed(e);
        }
      });
      popm_Label.add(mi_Supprimer);
      
      // ---- mi_CopierObjetLabel ----
      mi_CopierObjetLabel.setText("Copier l'objet");
      mi_CopierObjetLabel.setName("mi_CopierObjetLabel");
      mi_CopierObjetLabel.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_CopierObjetLabelActionPerformed(e);
        }
      });
      popm_Label.add(mi_CopierObjetLabel);
    }
    
    // ======== popm_Image ========
    {
      popm_Image.setName("popm_Image");
      
      // ---- mi_ChgImage ----
      mi_ChgImage.setText("Changer l'image");
      mi_ChgImage.setName("mi_ChgImage");
      mi_ChgImage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_ChgImageActionPerformed(e);
        }
      });
      popm_Image.add(mi_ChgImage);
      
      // ---- mi_OriginSize ----
      mi_OriginSize.setText("Mettre \u00e0 la taille originale");
      mi_OriginSize.setName("mi_OriginSize");
      mi_OriginSize.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_OriginSizeActionPerformed(e);
        }
      });
      popm_Image.add(mi_OriginSize);
      
      // ---- mi_SupprimerImage ----
      mi_SupprimerImage.setText("Supprimer");
      mi_SupprimerImage.setName("mi_SupprimerImage");
      mi_SupprimerImage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_SupprimerImageActionPerformed(e);
        }
      });
      popm_Image.add(mi_SupprimerImage);
      
      // ---- mi_CopierObjetImage ----
      mi_CopierObjetImage.setText("Copier l'objet");
      mi_CopierObjetImage.setName("mi_CopierObjetImage");
      mi_CopierObjetImage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_CopierObjetImageActionPerformed(e);
        }
      });
      popm_Image.add(mi_CopierObjetImage);
    }
    
    // ======== popm_CodeBarre ========
    {
      popm_CodeBarre.setName("popm_CodeBarre");
      
      // ---- mi_SupprimerCodeBarre ----
      mi_SupprimerCodeBarre.setText("Supprimer");
      mi_SupprimerCodeBarre.setName("mi_SupprimerCodeBarre");
      mi_SupprimerCodeBarre.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_SupprimerCodeBarreActionPerformed(e);
        }
      });
      popm_CodeBarre.add(mi_SupprimerCodeBarre);
      
      // ---- mi_CopierObjetCodeBarre ----
      mi_CopierObjetCodeBarre.setText("Copier l'objet");
      mi_CopierObjetCodeBarre.setName("mi_CopierObjetCodeBarre");
      mi_CopierObjetCodeBarre.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_CopierObjetCodeBarreActionPerformed(e);
        }
      });
      popm_CodeBarre.add(mi_CopierObjetCodeBarre);
    }
    
    // ======== popm_MultiSelection ========
    {
      popm_MultiSelection.setName("popm_MultiSelection");
      
      // ---- mi_AlignHGauche ----
      mi_AlignHGauche.setText("Aligner \u00e0 gauche");
      mi_AlignHGauche.setName("mi_AlignHGauche");
      mi_AlignHGauche.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_AlignHGaucheActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_AlignHGauche);
      
      // ---- mi_AlignHCentre ----
      mi_AlignHCentre.setText("Aligner  au centre");
      mi_AlignHCentre.setName("mi_AlignHCentre");
      mi_AlignHCentre.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_AlignHCentreActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_AlignHCentre);
      
      // ---- mi_AlignHDroite ----
      mi_AlignHDroite.setText("Aligner \u00e0 droite");
      mi_AlignHDroite.setName("mi_AlignHDroite");
      mi_AlignHDroite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_AlignHDroiteActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_AlignHDroite);
      popm_MultiSelection.addSeparator();
      
      // ---- mi_AlignVHaut ----
      mi_AlignVHaut.setText("Aligner en haut");
      mi_AlignVHaut.setName("mi_AlignVHaut");
      mi_AlignVHaut.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_AlignVHautActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_AlignVHaut);
      
      // ---- mi_AlignVCentre ----
      mi_AlignVCentre.setText("Aligner au centre");
      mi_AlignVCentre.setName("mi_AlignVCentre");
      mi_AlignVCentre.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_AlignVCentreActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_AlignVCentre);
      
      // ---- mi_AlignVBas ----
      mi_AlignVBas.setText("Aligner en bas");
      mi_AlignVBas.setName("mi_AlignVBas");
      mi_AlignVBas.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_AlignVBasActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_AlignVBas);
      popm_MultiSelection.addSeparator();
      
      // ---- mi_MmLargeur ----
      mi_MmLargeur.setText("M\u00eame largeur");
      mi_MmLargeur.setName("mi_MmLargeur");
      mi_MmLargeur.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MmLargeurActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_MmLargeur);
      
      // ---- mi_MmHauteur ----
      mi_MmHauteur.setText("M\u00eame hauteur");
      mi_MmHauteur.setName("mi_MmHauteur");
      mi_MmHauteur.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MmHauteurActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_MmHauteur);
      popm_MultiSelection.addSeparator();
      
      // ---- mi_MemeEspaceVertical ----
      mi_MemeEspaceVertical.setText("Egaliser l'espacement vertical");
      mi_MemeEspaceVertical.setName("mi_MemeEspaceVertical");
      mi_MemeEspaceVertical.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MemeEspaceVerticalActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_MemeEspaceVertical);
      
      // ======== m_EspaceVertical ========
      {
        m_EspaceVertical.setText("Mettre l'espacement vertical suivant");
        m_EspaceVertical.setName("m_EspaceVertical");
        
        // ---- mi_V0px ----
        mi_V0px.setText("0 pixel");
        mi_V0px.setName("mi_V0px");
        mi_V0px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V0pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V0px);
        
        // ---- mi_V1px ----
        mi_V1px.setText("1 pixel");
        mi_V1px.setName("mi_V1px");
        mi_V1px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V1pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V1px);
        
        // ---- mi_V2px ----
        mi_V2px.setText("2 pixels");
        mi_V2px.setName("mi_V2px");
        mi_V2px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V2pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V2px);
        
        // ---- mi_V3px ----
        mi_V3px.setText("3 pixels");
        mi_V3px.setName("mi_V3px");
        mi_V3px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V3pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V3px);
        
        // ---- mi_V4px ----
        mi_V4px.setText("4 pixels");
        mi_V4px.setName("mi_V4px");
        mi_V4px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V4pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V4px);
        
        // ---- mi_V5px ----
        mi_V5px.setText("5 pixels");
        mi_V5px.setName("mi_V5px");
        mi_V5px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V5pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V5px);
        
        // ---- mi_V6px ----
        mi_V6px.setText("6 pixels");
        mi_V6px.setName("mi_V6px");
        mi_V6px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V6pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V6px);
        
        // ---- mi_V7px ----
        mi_V7px.setText("7 pixels");
        mi_V7px.setName("mi_V7px");
        mi_V7px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V7pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V7px);
        
        // ---- mi_V8px ----
        mi_V8px.setText("8 pixels");
        mi_V8px.setName("mi_V8px");
        mi_V8px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V8pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V8px);
        
        // ---- mi_V9px ----
        mi_V9px.setText("9 pixels");
        mi_V9px.setName("mi_V9px");
        mi_V9px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V9pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V9px);
        
        // ---- mi_V10px ----
        mi_V10px.setText("10 pixels");
        mi_V10px.setName("mi_V10px");
        mi_V10px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_V10pxActionPerformed(e);
          }
        });
        m_EspaceVertical.add(mi_V10px);
      }
      popm_MultiSelection.add(m_EspaceVertical);
      
      // ---- mi_MemeEspaceHorizontal ----
      mi_MemeEspaceHorizontal.setText("Egaliser l'espacement horizontal");
      mi_MemeEspaceHorizontal.setName("mi_MemeEspaceHorizontal");
      mi_MemeEspaceHorizontal.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MemeEspaceHorizontalActionPerformed(e);
        }
      });
      popm_MultiSelection.add(mi_MemeEspaceHorizontal);
      
      // ======== m_EspaceHorizontal ========
      {
        m_EspaceHorizontal.setText("Mettre l'espacement horizontal suivant");
        m_EspaceHorizontal.setName("m_EspaceHorizontal");
        
        // ---- mi_H0px ----
        mi_H0px.setText("0 pixel");
        mi_H0px.setName("mi_H0px");
        mi_H0px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H0pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H0px);
        
        // ---- mi_H1px ----
        mi_H1px.setText("1 pixel");
        mi_H1px.setName("mi_H1px");
        mi_H1px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H1pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H1px);
        
        // ---- mi_H2px ----
        mi_H2px.setText("2 pixels");
        mi_H2px.setName("mi_H2px");
        mi_H2px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H2pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H2px);
        
        // ---- mi_H3px ----
        mi_H3px.setText("3 pixels");
        mi_H3px.setName("mi_H3px");
        mi_H3px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H3pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H3px);
        
        // ---- mi_H4px ----
        mi_H4px.setText("4 pixels");
        mi_H4px.setName("mi_H4px");
        mi_H4px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H4pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H4px);
        
        // ---- mi_H5px ----
        mi_H5px.setText("5 pixels");
        mi_H5px.setName("mi_H5px");
        mi_H5px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H5pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H5px);
        
        // ---- mi_H6px ----
        mi_H6px.setText("6 pixels");
        mi_H6px.setName("mi_H6px");
        mi_H6px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H6pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H6px);
        
        // ---- mi_H7px ----
        mi_H7px.setText("7 pixels");
        mi_H7px.setName("mi_H7px");
        mi_H7px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H7pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H7px);
        
        // ---- mi_H8px ----
        mi_H8px.setText("8 pixels");
        mi_H8px.setName("mi_H8px");
        mi_H8px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H8pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H8px);
        
        // ---- mi_H9px ----
        mi_H9px.setText("9 pixels");
        mi_H9px.setName("mi_H9px");
        mi_H9px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H9pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H9px);
        
        // ---- mi_H10px ----
        mi_H10px.setText("10 pixels");
        mi_H10px.setName("mi_H10px");
        mi_H10px.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            mi_H10pxActionPerformed(e);
          }
        });
        m_EspaceHorizontal.add(mi_H10px);
      }
      popm_MultiSelection.add(m_EspaceHorizontal);
    }
    
    // ======== popm_Etiquette ========
    {
      popm_Etiquette.setName("popm_Etiquette");
      
      // ---- mi_CollerObjet ----
      mi_CollerObjet.setText("Coller l'objet");
      mi_CollerObjet.setName("mi_CollerObjet");
      mi_CollerObjet.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_CollerObjetActionPerformed(e);
        }
      });
      popm_Etiquette.add(mi_CollerObjet);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPopupMenu popm_Label;
  private JMenuItem mi_CollerNom;
  private JMenuItem mi_Supprimer;
  private JMenuItem mi_CopierObjetLabel;
  private JPopupMenu popm_Image;
  private JMenuItem mi_ChgImage;
  private JMenuItem mi_OriginSize;
  private JMenuItem mi_SupprimerImage;
  private JMenuItem mi_CopierObjetImage;
  private JPopupMenu popm_CodeBarre;
  private JMenuItem mi_SupprimerCodeBarre;
  private JMenuItem mi_CopierObjetCodeBarre;
  private JPopupMenu popm_MultiSelection;
  private JMenuItem mi_AlignHGauche;
  private JMenuItem mi_AlignHCentre;
  private JMenuItem mi_AlignHDroite;
  private JMenuItem mi_AlignVHaut;
  private JMenuItem mi_AlignVCentre;
  private JMenuItem mi_AlignVBas;
  private JMenuItem mi_MmLargeur;
  private JMenuItem mi_MmHauteur;
  private JMenuItem mi_MemeEspaceVertical;
  private JMenu m_EspaceVertical;
  private JMenuItem mi_V0px;
  private JMenuItem mi_V1px;
  private JMenuItem mi_V2px;
  private JMenuItem mi_V3px;
  private JMenuItem mi_V4px;
  private JMenuItem mi_V5px;
  private JMenuItem mi_V6px;
  private JMenuItem mi_V7px;
  private JMenuItem mi_V8px;
  private JMenuItem mi_V9px;
  private JMenuItem mi_V10px;
  private JMenuItem mi_MemeEspaceHorizontal;
  private JMenu m_EspaceHorizontal;
  private JMenuItem mi_H0px;
  private JMenuItem mi_H1px;
  private JMenuItem mi_H2px;
  private JMenuItem mi_H3px;
  private JMenuItem mi_H4px;
  private JMenuItem mi_H5px;
  private JMenuItem mi_H6px;
  private JMenuItem mi_H7px;
  private JMenuItem mi_H8px;
  private JMenuItem mi_H9px;
  private JMenuItem mi_H10px;
  private JPopupMenu popm_Etiquette;
  private JMenuItem mi_CollerObjet;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
