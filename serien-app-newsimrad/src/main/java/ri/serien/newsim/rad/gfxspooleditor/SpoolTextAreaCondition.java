/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import ri.serien.newsim.description.DescriptionCondition;

/**
 * Description du textArea pour la sélection des Variables
 */
public class SpoolTextAreaCondition extends SpoolTextArea {
  
  // Variables
  private JMenuItem mi_Condition;
  private ArrayList<DescriptionCondition> listeCondition = null; // = new ArrayList<DescriptionCondition>();
  private JLabel texteCondition = null;
  
  /**
   * Constructeur
   */
  public SpoolTextAreaCondition() {
    super();
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        ta_PageConditionMouseMoved(e);
      }
    });
    creationMenuContextuel();
  }
  
  /**
   * Initialise la liste des conditions
   * @return
   */
  public void setListeCondition(ArrayList<DescriptionCondition> listeCondition) {
    this.listeCondition = listeCondition;
  }
  
  /**
   * Création du menu contextuel
   */
  @Override
  protected void creationMenuContextuel() {
    super.creationMenuContextuel();
    
    mi_Condition = new JMenuItem();
    
    // ---- mi_TrimageDroiteSelection ----
    mi_Condition.setText("Editeur de condition");
    mi_Condition.setName("mi_Condition");
    mi_Condition.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mi_ConditionActionPerformed(e);
      }
    });
    pm_Btd.add(mi_Condition);
  }
  
  /**
   * @param texteCondition the texteCondition to set
   */
  public void setTexteCondition(JLabel texteCondition) {
    this.texteCondition = texteCondition;
  }
  
  /**
   * @return the texteCondition
   */
  public JLabel getTexteCondition() {
    return texteCondition;
  }
  
  /**
   * Importation de la description pour les conditions
   */
  public void importerDescription() {
    if (listeCondition == null) {
      return;
    }
    
    // Récupération des sélections
    for (int i = listeCondition.size(); --i >= 0;) {
      convertDescription2Selection(listeCondition.get(i).getVariable(), listeRectangle);
    }
    repaint();
  }
  
  // ------------------------------------ Méthodes privées -------------------
  
  private void ta_PageConditionMouseMoved(MouseEvent e) {
    indiceSelection = findWhoIsSelectionned(e);
    
    if (indiceSelection == -1) {
      return;
    }
    String selection = listeRectangle.get(indiceSelection).getLibelle();
    DescriptionCondition condition = getCondition(selection);
    if ((condition != null) && (texteCondition != null)) {
      texteCondition.setText(condition.getLibelleCondition());
    }
  }
  
  /**
   * Retourne la condition à partir de son nom
   */
  private DescriptionCondition getCondition(String variable) {
    for (int i = 0; i < listeCondition.size(); i++) {
      if (listeCondition.get(i).getVariable().equals(variable)) {
        return listeCondition.get(i);
      }
    }
    return null;
  }
  
  /**
   * Supprime la condition à partir de son nom
   */
  private void removeCondition(String variable) {
    for (int i = 0; i < listeCondition.size(); i++) {
      if (listeCondition.get(i).getVariable().equals(variable)) {
        listeCondition.remove(i);
        return;
      }
    }
  }
  
  // ------------------------------------ Evènements -------------------------
  
  @Override
  protected void mi_SuppimerActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    
    String selection = listeRectangle.get(indiceSelection).getLibelle();
    removeCondition(selection);
    super.mi_SuppimerActionPerformed(e);
  }
  
  private void mi_ConditionActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    DescriptionCondition condition = null;
    
    // Recherche la condition à partir de la sélection (variable)
    String selection = listeRectangle.get(indiceSelection).getLibelle();
    condition = getCondition(selection);
    if (condition == null) {
      condition = new DescriptionCondition();
      listeCondition.add(condition);
      condition.setVariable(selection);
    }
    
    // Affichage de la boite de dialogue
    GfxConditionEditor dialog_cond = new GfxConditionEditor((JFrame) this.getTopLevelAncestor());
    dialog_cond.setCondition(condition);
    dialog_cond.setVisible(true);
    texteCondition.setText(condition.getLibelleCondition());
    
    repaint();
  }
}
