/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.Color;
import java.awt.Component;
import java.awt.FontMetrics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;

import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;

/**
 * Description des évènements d'une sélection
 */
public class SelectionModeCaractereListener implements MouseListener, MouseMotionListener, KeyListener {
  // Variables
  public int x = 0;
  public int y = 0;
  public int w = 0;
  public int h = 0;
  public int x_ori = 0;
  public int y_ori = 0;
  private FontMetrics fm = null;
  private Component composant = null;
  private int largeurLettre = 0;
  public int hauteurLettre = 0;
  private ArrayList<SelectionModeCaractere> listeRectangle = new ArrayList<SelectionModeCaractere>();
  private Color couleur = Color.blue;
  private Color couleurSelection = Color.red;
  // Permet soit de sélectionner une seule ligne ou un bloc qu'il va découper en plusieurs lignes
  private boolean selectionBloc = false;
  private boolean createRectangle = false;
  private GfxPageEditor pageEditor = null;
  
  /**
   * Retourne la colonne du premier caractère de la chaine sélectionnée (indice premère colonne: 0)
   * @return
   */
  public int getColonneChaine() {
    return x / largeurLettre;
  }
  
  /**
   * Retourne la longueur de la chaine sélectionnée
   * @return
   */
  public int getLongueurChaine() {
    return w / largeurLettre;
  }
  
  /**
   * Retourne la ligne de la chaine sélectionnée (indice premère ligne: 0)
   * @return
   */
  public int getLigneChaine() {
    return y / hauteurLettre;
  }
  
  /**
   * Retourne le rectangle
   * @return
   */
  public ArrayList<SelectionModeCaractere> getRectangleSelection() {
    return listeRectangle;
  }
  
  /**
   * Initialisation des différentes variables
   * @param arg0
   */
  public void init(Component acomposant) {
    composant = acomposant;
    fm = composant.getFontMetrics(composant.getFont()); // A améilorer avec g.getFontMetrics(); (à voir)
    largeurLettre = fm.charWidth('M');
    hauteurLettre = fm.getHeight();
  }
  
  /**
   * @return the couleur
   */
  public Color getCouleur() {
    return couleur;
  }
  
  /**
   * @param couleur the couleur to set
   */
  public void setCouleur(Color couleur) {
    this.couleur = couleur;
    // if (rs != null) rs.setCouleur(couleur);
    if (listeRectangle.size() != 0) {
      listeRectangle.get(0).setCouleur(couleur);
    }
  }
  
  /**
   * @return the couleurSelection
   */
  public Color getCouleurSelection() {
    return couleurSelection;
  }
  
  /**
   * @param couleurSelection the couleurSelection to set
   */
  public void setCouleurSelection(Color couleurSelection) {
    this.couleurSelection = couleurSelection;
    // if (rs != null) rs.setCouleurSelection(couleurSelection);
    if (listeRectangle.size() != 0) {
      listeRectangle.get(0).setCouleurSelection(couleurSelection);
    }
  }
  
  // ------------------------------------ Evènements -------------------------
  
  /**
   * @param selectionBloc the selectionBloc to set
   */
  public void setSelectionBloc(boolean selectionBloc) {
    this.selectionBloc = selectionBloc;
  }
  
  /**
   * @return the selectionBloc
   */
  public boolean isSelectionBloc() {
    return selectionBloc;
  }
  
  /**
   * @param pageEditor the pageEditor to set
   */
  public void setPageEditor(GfxPageEditor pageEditor) {
    this.pageEditor = pageEditor;
  }
  
  /**
   * @return the pageEditor
   */
  public GfxPageEditor getPageEditor() {
    return pageEditor;
  }
  
  @Override
  public void mouseClicked(MouseEvent arg0) {
  }
  
  @Override
  public void mouseEntered(MouseEvent arg0) {
  }
  
  @Override
  public void mouseExited(MouseEvent arg0) {
  }
  
  @Override
  public void mousePressed(MouseEvent arg0) {
    listeRectangle.clear();
    if (composant == null) {
      init(arg0.getComponent());
    }
    x = (arg0.getX() / largeurLettre) * largeurLettre;
    y = (arg0.getY() / hauteurLettre) * hauteurLettre;
    x_ori = x;
    y_ori = y;
    w = 0;
    h = 0;
  }
  
  @Override
  public void mouseReleased(MouseEvent arg0) {
    if (createRectangle) {
      if (selectionBloc) {
        // Création d'une ou plusieurs sélection (si hauteur > à une ligne)
        int nbrligne = h / hauteurLettre;
        for (int i = 0; i < nbrligne; i++) {
          SelectionModeCaractere rs =
              new SelectionModeCaractere(x, y + (i * hauteurLettre), w, hauteurLettre, largeurLettre, hauteurLettre);
          listeRectangle.add(rs);
          rs.setLigne(i + getLigneChaine());
          rs.setColonne(getColonneChaine());
          rs.setLongueur(getLongueurChaine());
          rs.setSelection(true);
          rs.setDimChar(largeurLettre, hauteurLettre);
          rs.setCouleur(couleur);
          rs.setCouleurSelection(couleurSelection);
        }
      }
      else {
        // Pour une seule ligne
        SelectionModeCaractere rs = new SelectionModeCaractere(x, y, w, h, largeurLettre, hauteurLettre);
        listeRectangle.add(rs);
        rs.setLigne(getLigneChaine());
        rs.setColonne(getColonneChaine());
        rs.setLongueur(getLongueurChaine());
        rs.setSelection(true);
        rs.setDimChar(largeurLettre, hauteurLettre);
        rs.setCouleur(couleur);
        rs.setCouleurSelection(couleurSelection);
      }
    }
    composant.repaint();
    createRectangle = false;
  }
  
  @Override
  public void mouseDragged(MouseEvent e) {
    createRectangle = true;
    
    // Déplacement vers la droite
    if (e.getX() >= x_ori) {
      w = ((e.getX() / largeurLettre) * largeurLettre) - x + largeurLettre;
    }
    else {
      // Déplacement vers la gauche
      x = (e.getX() / largeurLettre) * largeurLettre;
      w = x_ori - x;
    }
    
    // Déplacement vers le bas
    if (e.getY() >= y_ori) {
      // Permet de faire du multiligne
      if (selectionBloc) {
        h = ((e.getY() / hauteurLettre) * hauteurLettre) - y + hauteurLettre;
      }
      else {
        // Permet de faire du monoligne
        h = hauteurLettre;
      }
    }
    else {
      // Déplacement vers le haut
      y = (e.getY() / hauteurLettre) * hauteurLettre;
      // Permet de faire du multiligne
      if (selectionBloc) {
        h = y_ori - y + hauteurLettre;
      }
      else {
        // Permet de faire du monoligne
        h = hauteurLettre;
      }
    }
    
    composant.repaint();
  }
  
  @Override
  public void mouseMoved(MouseEvent arg0) {
  }
  
  @Override
  public void keyPressed(KeyEvent e) {
    // Si Shift enfoncé alors on est en mode aggrandissement ou déplacement
    if (e.isShiftDown() && listeRectangle.size() > 0) {
      SelectionModeCaractere rs = listeRectangle.get(listeRectangle.size() - 1);
      // Mode déplacemement
      if (e.isControlDown() || e.isMetaDown()) {
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
          rs.setX(rs.getX() + largeurLettre);
        }
        else {
          if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            rs.setX(rs.getX() - largeurLettre);
          }
        }
      }
      else {
        // Mode Aggrandissement
        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
          rs.setW(rs.getW() + largeurLettre);
        }
        else {
          if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            rs.setW(rs.getW() - largeurLettre);
          }
        }
        // Mise à jour dans l'éditeur de page
        if (pageEditor != null) {
          pageEditor.getGfxPage().getEtiquette().modifieObjet(rs.getSauveLibelle(), rs.getLibelle());
        }
      }
    }
    composant.repaint();
  }
  
  @Override
  public void keyReleased(KeyEvent e) {
  }
  
  @Override
  public void keyTyped(KeyEvent e) {
  }
  
}
