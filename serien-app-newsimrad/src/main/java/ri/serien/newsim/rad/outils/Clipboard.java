/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.outils;

import java.awt.Image;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Gestion du clipboard.
 */
public class Clipboard {
  
  /**
   * Envoi une image dans le presse papier.
   */
  public static void sendImageToClipBoard(Image img) {
    // if (img == null) return;
    ImageSelection imgSel = new ImageSelection(img);
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(imgSel, null);
  }
  
  /**
   * Retourne une image stocké dans le presse papier.
   */
  public static Image getImageFromClipboard() {
    Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    try {
      if (t != null && t.isDataFlavorSupported(DataFlavor.imageFlavor)) {
        Image text = (Image) t.getTransferData(DataFlavor.imageFlavor);
        return text;
      }
    }
    catch (UnsupportedFlavorException e) {
      // @todo A gérer
    }
    catch (IOException e) {
      // @todo A gérer
    }
    return null;
  }
  
  /**
   * Envoi un texte dans le presse papier.
   */
  public static void sendTextToClipBoard(String text) {
    // if (text == null) return;
    StringSelection data = new StringSelection(text);
    // Toolkit.getDefaultToolkit().getSystemClipboard().setContents(data, data);
    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(data, null);
  }
  
  /**
   * Retourne un texte stocké dans le presse papier.
   */
  public static String getTextFromClipboard() {
    Transferable contents = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
    if (contents == null) {
      return null;
    }
    
    boolean hasTransferableText = contents.isDataFlavorSupported(DataFlavor.stringFlavor);
    if (hasTransferableText) {
      try {
        return (String) contents.getTransferData(DataFlavor.stringFlavor);
      }
      catch (UnsupportedFlavorException e) {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
    return null;
  }
  
}
