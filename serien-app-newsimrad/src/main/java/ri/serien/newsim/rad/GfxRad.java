/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.exploitation.edition.SpoolOld;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.collection.HashMapManager;
import ri.serien.libcommun.outils.collection.HashMapManager.Listener;
import ri.serien.libcommun.outils.encodage.XMLTools;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.newsim.rad.gfxdocumenteditor.GfxDocumentEditor;
import ri.serien.newsim.rad.gfxeditioneditor.GfxEditionEditor;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.gfxspooleditor.GfxSpoolEditor;
import ri.serien.newsim.rad.outils.RiFileChooser;
import ri.serien.newsim.rad.parametre.GfxParametres;
import ri.serien.newsim.rad.parametre.Preferences;
import ri.serien.newsim.rad.parametre.PreferencesManager;

/**
 * RAD (génération des fond de pages)
 */
public class GfxRad extends JFrame {
  // Constantes
  
  public static final String VERSION = "3.6";
  
  // Variables
  private RiFileChooser selectionFichier = null;
  private GfxGestionOutq gestionOutq = null;
  private HashMapManager listeSpoolEditor = null;
  private ArrayList<GfxPageEditor> listePageEditor = new ArrayList<GfxPageEditor>();
  private PreferencesManager prefsManager = new PreferencesManager();
  private Preferences prefs = prefsManager.getPreferences();
  
  /**
   * Constructeur
   * 
   */
  public GfxRad() {
    initComponents();
    selectionFichier = new RiFileChooser(this, prefs.getDernierPath());
    PreferencesManager.createTempFolder();
    setTitle(VERSION);
    // Les boutons concernants les descriptions d'édition sont cachés car pour l'instant ils ne sont plus utilisés
    p_Edition.setVisible(false);
    setVisible(true);
    pack();
  }
  
  /**
   * Retourne les préferences du rad
   * @return
   */
  public Preferences getPreferences() {
    return prefs;
  }
  
  /**
   * Redéfinition de la classe
   */
  @Override
  public void dispose() {
    // On sauve les préférences
    prefs.setDernierPath(selectionFichier.getDossier());
    prefsManager.sauvePreferencesUser();
    PreferencesManager.removeTempfolder();
    
    // On ferme la outq si besoin
    if (gestionOutq != null) {
      gestionOutq.dispose();
    }
    super.dispose();
  }
  
  /**
   * Initialise le HashMapManager des outq si besoin
   */
  private void initListeSpoolEditor() {
    if (listeSpoolEditor == null) {
      // On instancie la liste des éditeurs de spool
      listeSpoolEditor = new HashMapManager();
      listeSpoolEditor.addListener(new Listener() {
        @Override
        public void onDataCleared() {
        }
        
        @Override
        public void onDataAdded(Object cle, Object val) {
          for (int i = 0; i < listePageEditor.size(); i++) {
            listePageEditor.get(i).initListeSpoolEditor();
            listePageEditor.get(i).enableActionSpoolEditor();
          }
          // Positionnement de la fenêtre par rapport à l'éditeur de page
          Dimension dimension = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
          int pos_x = 0;
          if (listePageEditor.size() > 0) {
            pos_x = listePageEditor.get(0).getX() + listePageEditor.get(0).getWidth() + 10;
          }
          else {
            pos_x = getWidth() + 10;
          }
          // Uniquement dans le cas où les écrans sont assez larges
          if ((int) dimension.getWidth() > (pos_x * 1.2)) {
            ((GfxSpoolEditor) listeSpoolEditor.getKeyAtIndex(listeSpoolEditor.getHashMap().size() - 1)).setLocation(pos_x, 0);
          }
        }
        
        @Override
        public void onDataRemoved(Object cle) {
          for (int i = 0; i < listePageEditor.size(); i++) {
            listePageEditor.get(i).initListeSpoolEditor();
            listePageEditor.get(i).enableActionSpoolEditor();
          }
        }
      });
    }
  }
  
  /**
   * Charge le fichier spool (spl)
   * @return
   */
  private void chargementSPL(String fichier) {
    SpoolOld spoolOld = null;
    // Lecture du fichier spl
    initListeSpoolEditor();
    try {
      // Dé-sérialisation du fichier spool
      spoolOld = (SpoolOld) XMLTools.decodeFromFile(fichier);
    }
    catch (
    
    Exception e) {
      if (!XMLTools.getListeMsgExceptionDecoder().isEmpty()) {
        // Conversion des fichiers SPL pour la 2.14
        try {
          Constantes.convert213to214(fichier, "ri.serien.libcommun.exploitation.");
          spoolOld = (SpoolOld) XMLTools.decodeFromFile(fichier);
        }
        catch (Exception e1) {
          JOptionPane.showMessageDialog(null, e1.getMessage(), "Conversion fichier spool", JOptionPane.ERROR_MESSAGE);
        }
      }
    }
    if (spoolOld != null) {
      File ffichier = new File(fichier);
      GfxSpoolEditor sp = new GfxSpoolEditor(spoolOld.getNom(), spoolOld, listeSpoolEditor);
      sp.setDossierTravail(ffichier.getParent());
      listeSpoolEditor.addObject(sp, spoolOld.getNom());
    }
  }
  
  /**
   * Création d'un nouveau éditeur de page
   */
  private GfxPageEditor createPageEditor(String titre, String fichierddp) {
    setCursor(new Cursor(Cursor.WAIT_CURSOR));
    
    GfxPageEditor fenetre = null;
    
    // Création de la page
    if (fichierddp == null) {
      fenetre = new GfxPageEditor(this, titre, null, prefs);
    }
    else {
      fenetre = new GfxPageEditor(this, titre, new FileNG(fichierddp), prefs);
    }
    fenetre.pack();
    // fenetre.majPanneau();
    fenetre.initListeSpoolEditor();
    listePageEditor.add(fenetre);
    
    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    
    return fenetre;
  }
  
  /**
   * @return the listeSpoolEditor
   */
  public HashMapManager getListeSpoolEditor() {
    return listeSpoolEditor;
  }
  
  /**
   * @param listeSpoolEditor the listeSpoolEditor to set
   */
  public void setListeSpoolEditor(HashMapManager listeSpoolEditor) {
    this.listeSpoolEditor = listeSpoolEditor;
  }
  
  /**
   * Programme de lancment
   * @param args
   */
  public static void main(String[] args) {
    // Utilisation du Look & Feel Nimbus
    try {
      for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
      // UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (Exception e) {
    }
    
    // Démarrage du programme
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        // PreferencesManager.removeFile();
        new GfxRad();
      }
    });
  }
  
  private void bt_NouveauActionPerformed(ActionEvent e) {
    createPageEditor(GfxPageEditor.TITRE, null);
  }
  
  private void bt_OpenActionPerformed(ActionEvent e) {
    String fichierddp = selectionFichier.choisirFichier("ddp", "Fichier description de page (*.ddp)");
    if (fichierddp != null) {
      createPageEditor(fichierddp, fichierddp);
    }
  }
  
  private void bt_OutqActionPerformed(ActionEvent e) {
    // Création de la fenêtre si besoin
    if (gestionOutq == null) {
      initListeSpoolEditor();
      
      // On instancie la gestion des Outqs
      gestionOutq = new GfxGestionOutq();
      pnlConnexion.add(gestionOutq, BorderLayout.CENTER);
      gestionOutq.setListeSpoolEditor(listeSpoolEditor);
      gestionOutq.setPreference(prefsManager);
    }
    
    if (tbt_Outq.isSelected()) {
      tbt_Outq.setToolTipText("Cache le panel de connexion aux OUTQs");
      gestionOutq.lectureInfosConnexion();
    }
    else {
      tbt_Outq.setToolTipText("Affiche le panel de connexion aux OUTQs");
    }
    gestionOutq.setVisible(tbt_Outq.isSelected());
    pack();
  }
  
  private void bt_OpenSpoolFileActionPerformed(ActionEvent e) {
    String chemin = selectionFichier.choisirFichier("spl", "Fichier spool (*.spl)");
    if (chemin != null) {
      chargementSPL(chemin);
    }
  }
  
  private void thisWindowClosing(WindowEvent e) {
    dispose();
  }
  
  private void bt_ParametresActionPerformed(ActionEvent e) {
    new GfxParametres(this, prefsManager);
  }
  
  private void bt_OuvrirDocumentActionPerformed(ActionEvent e) {
    String fichierddd = selectionFichier.choisirFichier("ddd", "Fichier description de document (*.ddd)");
    if (fichierddd != null) {
      new GfxDocumentEditor(fichierddd, prefs);
    }
  }
  
  private void bt_NouveauDocumentActionPerformed(ActionEvent e) {
    new GfxDocumentEditor(null, prefs);
  }
  
  private void bt_OuvrirEditionActionPerformed(ActionEvent e) {
    String fichierdde = selectionFichier.choisirFichier("dde", "Fichier description d'édition (*.dde)");
    if (fichierdde != null) {
      new GfxEditionEditor(fichierdde, prefs);
    }
  }
  
  private void bt_NouvelleEditionActionPerformed(ActionEvent e) {
    new GfxEditionEditor(null, prefs);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_Spool = new JPanel();
    bt_OuvrirSpool = new JButton();
    tbt_Outq = new JToggleButton();
    pnlConnexion = new JPanel();
    p_Page = new JPanel();
    bt_OuvrirPage = new JButton();
    bt_NouvellePage = new JButton();
    p_Document = new JPanel();
    bt_OuvrirDocument = new JButton();
    bt_NouveauDocument = new JButton();
    p_Edition = new JPanel();
    bt_OuvrirEdition = new JButton();
    bt_NouvelleEdition = new JButton();
    p_Parametres = new JPanel();
    bt_Parametres = new JButton();
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    setTitle("Edition");
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new GridBagLayout());
    ((GridBagLayout) contentPane.getLayout()).columnWidths = new int[] { 0, 0, 0 };
    ((GridBagLayout) contentPane.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
    ((GridBagLayout) contentPane.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    ((GridBagLayout) contentPane.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
    
    // ======== p_Spool ========
    {
      p_Spool.setBorder(new TitledBorder("Spool"));
      p_Spool.setMinimumSize(new Dimension(163, 96));
      p_Spool.setPreferredSize(new Dimension(163, 96));
      p_Spool.setName("p_Spool");
      p_Spool.setLayout(new FlowLayout());
      
      // ---- bt_OuvrirSpool ----
      bt_OuvrirSpool.setToolTipText("Ouvrir un fichier spool");
      bt_OuvrirSpool.setIcon(new ImageIcon(getClass().getResource("/images/dossier-kde-ouvrez-icone-5425-32.png")));
      bt_OuvrirSpool.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_OuvrirSpool.setName("bt_OuvrirSpool");
      bt_OuvrirSpool.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_OpenSpoolFileActionPerformed(e);
        }
      });
      p_Spool.add(bt_OuvrirSpool);
      
      // ---- tbt_Outq ----
      tbt_Outq.setIcon(new ImageIcon(getClass().getResource("/images/as400_720qe9.png")));
      tbt_Outq.setPreferredSize(new Dimension(50, 50));
      tbt_Outq.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      tbt_Outq.setToolTipText("Affiche le panel de connexion aux OUTQs");
      tbt_Outq.setName("tbt_Outq");
      tbt_Outq.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_OutqActionPerformed(e);
        }
      });
      p_Spool.add(tbt_Outq);
    }
    contentPane.add(p_Spool, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ======== pnlConnexion ========
    {
      pnlConnexion.setName("pnlConnexion");
      pnlConnexion.setLayout(new BorderLayout());
    }
    contentPane.add(pnlConnexion,
        new GridBagConstraints(1, 0, 1, 5, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    
    // ======== p_Page ========
    {
      p_Page.setBorder(new TitledBorder("Page"));
      p_Page.setName("p_Page");
      p_Page.setLayout(new FlowLayout());
      
      // ---- bt_OuvrirPage ----
      bt_OuvrirPage.setIcon(new ImageIcon(getClass().getResource("/images/dossier-kde-ouvrez-icone-5425-32.png")));
      bt_OuvrirPage.setToolTipText("Ouvrir une description de page");
      bt_OuvrirPage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_OuvrirPage.setName("bt_OuvrirPage");
      bt_OuvrirPage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_OpenActionPerformed(e);
        }
      });
      p_Page.add(bt_OuvrirPage);
      
      // ---- bt_NouvellePage ----
      bt_NouvellePage.setIcon(new ImageIcon(getClass().getResource("/images/nouveau-icone-6242-32.png")));
      bt_NouvellePage.setToolTipText("Nouvelle description de page");
      bt_NouvellePage.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_NouvellePage.setName("bt_NouvellePage");
      bt_NouvellePage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_NouveauActionPerformed(e);
        }
      });
      p_Page.add(bt_NouvellePage);
    }
    contentPane.add(p_Page, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ======== p_Document ========
    {
      p_Document.setBorder(new TitledBorder("Document"));
      p_Document.setName("p_Document");
      p_Document.setLayout(new FlowLayout());
      
      // ---- bt_OuvrirDocument ----
      bt_OuvrirDocument.setIcon(new ImageIcon(getClass().getResource("/images/dossier-kde-ouvrez-icone-5425-32.png")));
      bt_OuvrirDocument.setToolTipText("Ouvrir une description de document");
      bt_OuvrirDocument.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_OuvrirDocument.setName("bt_OuvrirDocument");
      bt_OuvrirDocument.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_OuvrirDocumentActionPerformed(e);
        }
      });
      p_Document.add(bt_OuvrirDocument);
      
      // ---- bt_NouveauDocument ----
      bt_NouveauDocument.setIcon(new ImageIcon(getClass().getResource("/images/nouveau-icone-6242-32.png")));
      bt_NouveauDocument.setToolTipText("Nouvelle description de document");
      bt_NouveauDocument.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_NouveauDocument.setName("bt_NouveauDocument");
      bt_NouveauDocument.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_NouveauDocumentActionPerformed(e);
        }
      });
      p_Document.add(bt_NouveauDocument);
    }
    contentPane.add(p_Document, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ======== p_Edition ========
    {
      p_Edition.setBorder(new TitledBorder("Edition"));
      p_Edition.setName("p_Edition");
      p_Edition.setLayout(new FlowLayout());
      
      // ---- bt_OuvrirEdition ----
      bt_OuvrirEdition.setToolTipText("Ouvrir une description d'\u00e9dition");
      bt_OuvrirEdition.setIcon(new ImageIcon(getClass().getResource("/images/dossier-kde-ouvrez-icone-5425-32.png")));
      bt_OuvrirEdition.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_OuvrirEdition.setName("bt_OuvrirEdition");
      bt_OuvrirEdition.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_OuvrirEditionActionPerformed(e);
        }
      });
      p_Edition.add(bt_OuvrirEdition);
      
      // ---- bt_NouvelleEdition ----
      bt_NouvelleEdition.setToolTipText("Nouvelle description d'\u00e9dition");
      bt_NouvelleEdition.setIcon(new ImageIcon(getClass().getResource("/images/nouveau-icone-6242-32.png")));
      bt_NouvelleEdition.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_NouvelleEdition.setName("bt_NouvelleEdition");
      bt_NouvelleEdition.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_NouvelleEditionActionPerformed(e);
        }
      });
      p_Edition.add(bt_NouvelleEdition);
    }
    contentPane.add(p_Edition, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    
    // ======== p_Parametres ========
    {
      p_Parametres.setBorder(new TitledBorder("Param\u00e8tres"));
      p_Parametres.setName("p_Parametres");
      p_Parametres.setLayout(new FlowLayout());
      
      // ---- bt_Parametres ----
      bt_Parametres.setIcon(new ImageIcon(getClass().getResource("/images/options-parametres-icone-5311-32.png")));
      bt_Parametres.setToolTipText("Param\u00e8tres");
      bt_Parametres.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Parametres.setName("bt_Parametres");
      bt_Parametres.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_ParametresActionPerformed(e);
        }
      });
      p_Parametres.add(bt_Parametres);
    }
    contentPane.add(p_Parametres, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
        new Insets(0, 0, 0, 0), 0, 0));
    pack();
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_Spool;
  private JButton bt_OuvrirSpool;
  private JToggleButton tbt_Outq;
  private JPanel pnlConnexion;
  private JPanel p_Page;
  private JButton bt_OuvrirPage;
  private JButton bt_NouvellePage;
  private JPanel p_Document;
  private JButton bt_OuvrirDocument;
  private JButton bt_NouveauDocument;
  private JPanel p_Edition;
  private JButton bt_OuvrirEdition;
  private JButton bt_NouvelleEdition;
  private JPanel p_Parametres;
  private JButton bt_Parametres;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
