/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.parametre;

import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import ri.serien.newsim.rad.GfxRad;
import ri.serien.newsim.rad.outils.RiFileChooser;

/**
 * Fenêtre des paramètres
 */
public class GfxParametres extends JDialog {
  // Constantes
  
  // Variables
  private PreferencesManager prefsManager = null;
  // private Preferences prefs = null;
  
  /**
   * Constructeur
   * @param owner
   * @param prefsManager
   */
  public GfxParametres(Frame owner, PreferencesManager prefsManager) {
    super(owner);
    initComponents();
    initPreferences(prefsManager);
    setVisible(true);
  }
  
  /**
   * Constructeur
   * @param owner
   * @param prefsManager
   */
  public GfxParametres(Dialog owner, PreferencesManager prefsManager) {
    super(owner);
    initComponents();
    initPreferences(prefsManager);
    setVisible(true);
  }
  
  /**
   * Initialise les préférences avec les données du fichier
   * @param prefsManager
   */
  private void initPreferences(PreferencesManager prefsManager) {
    if (prefsManager == null) {
      return;
    }
    this.prefsManager = prefsManager;
    // prefs = prefsManager.getPreferences();
    
    l_NumVersion.setText(GfxRad.VERSION);
    // tf_EspaceTravail.setText(prefs.getEspaceTravail());
  }
  
  /**
   * Sauve les préférences dans le fichier
   * @return
   */
  private boolean sauvePreference() {
    if (prefsManager == null) {
      return false;
    }
    // prefs.setEspaceTravail(tf_EspaceTravail.getText().trim());
    
    prefsManager.sauvePreferencesUser();
    return true;
  }
  
  /**
   * Permet de sélectionner le dossier de travail
   */
  private void setDossierTravail() {
    RiFileChooser selectionDossier = new RiFileChooser(null, tf_EspaceTravail.getText());
    File dossier = selectionDossier.choisirDossier(null);
    if (dossier != null) {
      tf_EspaceTravail.setText(dossier.getAbsolutePath());
    }
  }
  
  // -------------------------------- Evènementiel -----------------------------------------
  
  private void bt_ValiderActionPerformed(ActionEvent e) {
    sauvePreference();
    dispose();
  }
  
  private void bt_AnnulerActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void bt_ParcourirEspaceTravailActionPerformed(ActionEvent e) {
    setDossierTravail();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bt_Valider = new JButton();
    bt_Annuler = new JButton();
    l_EspaceTravail = new JLabel();
    tf_EspaceTravail = new JTextField();
    bt_ParcourirEspaceTravail = new JButton();
    l_Version = new JLabel();
    l_NumVersion = new JLabel();
    
    // ======== this ========
    setTitle("Param\u00e8tres");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(null);
    
    // ---- bt_Valider ----
    bt_Valider.setText("Valider");
    bt_Valider.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_Valider.setName("bt_Valider");
    bt_Valider.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_ValiderActionPerformed(e);
      }
    });
    contentPane.add(bt_Valider);
    bt_Valider.setBounds(new Rectangle(new Point(290, 375), bt_Valider.getPreferredSize()));
    
    // ---- bt_Annuler ----
    bt_Annuler.setText("Annuler");
    bt_Annuler.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_Annuler.setName("bt_Annuler");
    bt_Annuler.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_AnnulerActionPerformed(e);
      }
    });
    contentPane.add(bt_Annuler);
    bt_Annuler.setBounds(new Rectangle(new Point(370, 375), bt_Annuler.getPreferredSize()));
    
    // ---- l_EspaceTravail ----
    l_EspaceTravail.setText("Espace de travail");
    l_EspaceTravail.setEnabled(false);
    l_EspaceTravail.setName("l_EspaceTravail");
    contentPane.add(l_EspaceTravail);
    l_EspaceTravail.setBounds(15, 45, 115, l_EspaceTravail.getPreferredSize().height);
    
    // ---- tf_EspaceTravail ----
    tf_EspaceTravail.setEnabled(false);
    tf_EspaceTravail.setName("tf_EspaceTravail");
    contentPane.add(tf_EspaceTravail);
    tf_EspaceTravail.setBounds(135, 40, 235, tf_EspaceTravail.getPreferredSize().height);
    
    // ---- bt_ParcourirEspaceTravail ----
    bt_ParcourirEspaceTravail.setText("Parcourir");
    bt_ParcourirEspaceTravail.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    bt_ParcourirEspaceTravail.setEnabled(false);
    bt_ParcourirEspaceTravail.setName("bt_ParcourirEspaceTravail");
    bt_ParcourirEspaceTravail.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        bt_ParcourirEspaceTravailActionPerformed(e);
      }
    });
    contentPane.add(bt_ParcourirEspaceTravail);
    bt_ParcourirEspaceTravail.setBounds(new Rectangle(new Point(380, 40), bt_ParcourirEspaceTravail.getPreferredSize()));
    
    // ---- l_Version ----
    l_Version.setText("Version");
    l_Version.setName("l_Version");
    contentPane.add(l_Version);
    l_Version.setBounds(15, 10, 100, l_Version.getPreferredSize().height);
    
    // ---- l_NumVersion ----
    l_NumVersion.setText("version");
    l_NumVersion.setName("l_NumVersion");
    contentPane.add(l_NumVersion);
    l_NumVersion.setBounds(new Rectangle(new Point(135, 10), l_NumVersion.getPreferredSize()));
    
    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for (int i = 0; i < contentPane.getComponentCount(); i++) {
        Rectangle bounds = contentPane.getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = contentPane.getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      contentPane.setMinimumSize(preferredSize);
      contentPane.setPreferredSize(preferredSize);
    }
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JButton bt_Valider;
  private JButton bt_Annuler;
  private JLabel l_EspaceTravail;
  private JTextField tf_EspaceTravail;
  private JButton bt_ParcourirEspaceTravail;
  private JLabel l_Version;
  private JLabel l_NumVersion;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
