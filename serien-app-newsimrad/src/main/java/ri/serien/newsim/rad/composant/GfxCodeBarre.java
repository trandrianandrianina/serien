/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;

import ri.serien.newsim.description.DescriptionCodeBarre;
import ri.serien.newsim.rad.gfxpageeditor.GfxPageEditor;
import ri.serien.newsim.rad.outils.CalculDPI;

/**
 * Description de l'objet image pour RAD (génération Etiquette)
 */
public class GfxCodeBarre extends GfxObject {
  // Constantes
  
  // Variables
  private String texte = null;
  private String typeCodeBarre = null;
  private String positionMessage = DescriptionCodeBarre.BAS; // Position du texte du code barre
  private float largeurModule = -1; // Largeur d'une barre (pas du code barre en entier)
  private int rotation = 0; // Angle de rotation du code barre
  
  /**
   * Constructeur
   * @param x
   * @param y
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxCodeBarre(int x, int y, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(x, y, planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    setIcon(new ImageIcon(getClass().getResource("/images/code-a-barres-id-icone-8260-32.png")));
    setSize(getIcon().getIconWidth(), getIcon().getIconHeight());
    setHorizontalAlignment(CENTER);
    setBackground(Color.lightGray);
  }
  
  /**
   * Constructeur
   * @param dlabel
   * @param dpi_aff
   * @param dpi_img
   */
  public GfxCodeBarre(DescriptionCodeBarre dcodebarre, int dpi_aff, int dpi_img, final GfxPageEditor planTravail) {
    super(dcodebarre, dpi_aff, dpi_img, planTravail);
    setDpiAff(dpi_aff);
    setDpiImg(dpi_img);
    
    // Avoir si judicieux
    setIcon(new ImageIcon(getClass().getResource("/images/code-a-barres-id-icone-8260-32.png")));
    setHorizontalAlignment(CENTER);
    setBackground(Color.lightGray);
    
    setSize(CalculDPI.getCmtoPx(dpi_aff, dpi_img, dcodebarre.getLargeur()),
        CalculDPI.getCmtoPx(dpi_aff, dpi_img, dcodebarre.getHauteur()));
    setTypeCodeBarre(dcodebarre.getTypeCodeBarre());
    setTexte(dcodebarre.getTexte());
    setPositionMessage(dcodebarre.getPositionMessage());
    setLargeurModule(dcodebarre.getLargeurModule());
    setRotation(dcodebarre.getRotation());
    
    setFocusBorder(false, false, false);
    setCondition(dcodebarre.getCondition());
  }
  
  /**
   * Retourne une icone redimensionnée
   * @param ii
   * @param ratio
   * @return
   */
  public ImageIcon getResizeIcon(ImageIcon origine, int w, int h) {
    if (origine == null) {
      return null;
    }
    float rw = 1;
    float rh = 1;
    
    // Calcul du ratio
    // La référence est la hauteur
    if (w == -1) {
      rh = ((float) origine.getIconHeight() / h);
      rw = rh;
    }
    else {
      // La référence est la largeur
      if (h == -1) {
        rw = ((float) origine.getIconWidth() / w);
        rh = rw;
      }
      else {
        rw = ((float) origine.getIconWidth() / w);
        rh = ((float) origine.getIconHeight() / h);
      }
    }
    
    // Calcul des nouvelles dimensions
    w = (int) (origine.getIconWidth() / rw);
    h = (int) (origine.getIconHeight() / rh);
    
    BufferedImage buf = new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
    // On dessine sur le Graphics de l'image bufferisée.
    Graphics2D g = buf.createGraphics();
    g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
    g.drawImage(origine.getImage(), 0, 0, w, h, null);
    g.dispose();
    return new ImageIcon(buf);
  }
  
  /**
   * @return the ligne
   */
  public DescriptionCodeBarre getDescription() {
    DescriptionCodeBarre dcodebarre = new DescriptionCodeBarre();
    dcodebarre.setHauteur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getHeight()));
    dcodebarre.setLargeur(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getWidth()));
    dcodebarre.setXPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().x));
    dcodebarre.setYPos_cm(CalculDPI.getPxtoCm(dpi_aff, dpi_img, getLocation().y));
    dcodebarre.setTypeCodeBarre(getTypeCodeBarre());
    dcodebarre.setTexte(getTexte());
    dcodebarre.setPositionMessage(getPositionMessage());
    dcodebarre.setLargeurModule(getLargeurModule());
    dcodebarre.setRotation(getRotation());
    
    dcodebarre.setCondition(condition);
    
    return dcodebarre;
  }
  
  /**
   * @param texte the texte to set
   */
  public void setTexte(String texte) {
    this.texte = texte;
  }
  
  /**
   * @return the texte
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * @param positionMessage the positionMessage to set
   */
  public void setPositionMessage(String positionMessage) {
    this.positionMessage = positionMessage;
  }
  
  /**
   * @param typeCodeBarre the typeCodeBarre to set
   */
  public void setTypeCodeBarre(String typeCodeBarre) {
    this.typeCodeBarre = typeCodeBarre;
  }
  
  /**
   * @return the typeCodeBarre
   */
  public String getTypeCodeBarre() {
    return typeCodeBarre;
  }
  
  /**
   * @return the positionMessage
   */
  public String getPositionMessage() {
    return positionMessage;
  }
  
  /**
   * @param largeurModule the largeurModule to set
   */
  public void setLargeurModule(float largeurModule) {
    this.largeurModule = largeurModule;
  }
  
  /**
   * @return the largeurModule
   */
  public float getLargeurModule() {
    return largeurModule;
  }
  
  /**
   * @param rotation the rotation to set
   */
  public void setRotation(int rotation) {
    this.rotation = rotation;
  }
  
  /**
   * @return the rotation
   */
  public int getRotation() {
    return rotation;
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  @Override
  protected void labelMouseReleased(MouseEvent e) {
    // Gestion du redimensionnement de l'image
    if (redimensionne) {
      // setSize(getIcon().getIconWidth(), getIcon().getIconHeight());
    }
    
    // On effectue les opérations d'origine
    super.labelMouseReleased(e);
    
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if (pageEditor != null) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
  
  /**
   * Action lors du clic souris sur label (bouton laché)
   * @param e
   */
  @Override
  protected void labelMouseDragged(MouseEvent e) {
    super.labelMouseDragged(e);
    // On met à jour les propriétés de l'objet dans le panel des propriétés
    // Mise à jour des attributs
    if ((clic_gauche) && (pageEditor != null)) {
      pageEditor.getPropertyObject().getPanelProperty(this);
    }
  }
}
