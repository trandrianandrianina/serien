/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.parametre;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import javax.swing.JOptionPane;

import org.apache.commons.io.FileUtils;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.encodage.XMLTools;
import ri.serien.libcommun.outils.fichier.FileNG;

/**
 * Permet de gérer (enregistrement, lecture) les préférences de l'utilisateur
 */
public class PreferencesManager {
  // Constantes non enregistrées
  public transient static final String ANCIEN_DOSSIER_PREFS = System.getProperty("user.home") + File.separator + ".RadOndeEdition";
  public transient static final String DOSSIER_PREFS = System.getProperty("user.home") + File.separator + ".RadNewSim";
  public transient static final String FICHIER_PREFS = DOSSIER_PREFS + File.separator + "preferences.xml";
  
  public transient static final String DOSSIER_TMP =
      DOSSIER_PREFS + File.separatorChar + "tmp" + File.separatorChar + UUID.randomUUID().toString();
  
  // Variables
  private Preferences prefsUser = null; // Préférences diverses
  
  /**
   * Constructeur.
   */
  public PreferencesManager() {
    // Renomme le dossier des préférences si nécessaire
    renommerDossierPreferences();
  }
  
  /**
   * Retourne les préférences de l'utilisateur
   * @return the prefsUser
   */
  public Preferences getPreferences() {
    if (prefsUser == null) {
      if (!chargePreferencesUser()) {
        prefsUser = new Preferences();
      }
    }
    
    return prefsUser;
  }
  
  /**
   * Sauvegarde les préférences de l'utilisateur sur disque
   */
  public void sauvePreferencesUser() {
    File dossier = new File(DOSSIER_PREFS);
    if (!dossier.exists()) {
      dossier.mkdirs();
    }
    
    // Sauvegarde du fichier sur disque
    try {
      XMLTools.encodeToFile(getPreferences(), FICHIER_PREFS);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Supprime le fichier des préférences
   */
  public static void removeFile() {
    File fichier = new File(FICHIER_PREFS);
    fichier.delete();
  }
  
  /**
   * Création du dossier temporaire dans le home du user
   */
  public static void createTempFolder() {
    File dossier = new File(DOSSIER_TMP);
    if (!dossier.exists()) {
      dossier.mkdirs();
    }
  }
  
  /**
   * Supprime le dossier temporaire
   */
  public static void removeTempfolder() {
    FileNG dossier = new FileNG(DOSSIER_TMP);
    if (dossier.exists()) {
      dossier.remove();
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Charge les préférences de l'utilisateur à partir d'un fichier
   */
  private boolean chargePreferencesUser() {
    try {
      prefsUser = (Preferences) XMLTools.decodeFromFile(FICHIER_PREFS);
      if (!XMLTools.getListeMsgExceptionDecoder().isEmpty()) {
        // Conversion du fichier des préférences pour la 2.14
        try {
          Constantes.convert213to214(FICHIER_PREFS, "ri.serien.newsim.");
          prefsUser = (Preferences) XMLTools.decodeFromFile(FICHIER_PREFS);
        }
        catch (Exception e1) {
          JOptionPane.showMessageDialog(null, "Erreur pendant la conversion du fichier " + FICHIER_PREFS,
              "Conversion fichier des préférences", JOptionPane.ERROR_MESSAGE);
          return false;
        }
      }
    }
    catch (Exception e) {
      return false;
    }
    return true;
  }
  
  /**
   * Renomme le dossier des préférences si nécessaire.
   */
  private void renommerDossierPreferences() {
    File dossierNouveauNom = new File(DOSSIER_PREFS);
    if (!dossierNouveauNom.exists()) {
      File dossierAncienNom = new File(ANCIEN_DOSSIER_PREFS);
      if (dossierAncienNom.exists()) {
        try {
          FileUtils.copyDirectory(dossierAncienNom, dossierNouveauNom);
        }
        catch (IOException e) {
        }
      }
    }
  }
}
