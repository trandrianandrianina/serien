/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.newsim.description.DescriptionVariable;

/**
 * Boite de dialogue permettant la saisie d'une condition
 */
public class GfxVariableEditor extends JDialog {
  
  // Variables
  private DescriptionVariable variable = null;
  
  /**
   * Constructeur
   * @param owner
   */
  public GfxVariableEditor(Frame owner) {
    super(owner);
    initComponents();
  }
  
  /**
   * Constructeur
   * @param owner
   */
  public GfxVariableEditor(Dialog owner) {
    super(owner);
    initComponents();
  }
  
  public void setVariable(DescriptionVariable variable) {
    this.variable = variable;
    initVariable();
  }
  
  // ------------------------------------ Méthodes privées -------------------
  
  private void initVariable() {
    if (variable == null) {
      return;
    }
    
    tf_Nom.setText(variable.getNom());
    l_Variable.setText(variable.getVariable());
    chk_Trim.setSelected(variable.isTrimValeur());
  }
  
  private void creationVariable() {
    if (variable == null) {
      return;
    }
    
    variable.setNom(tf_Nom.getText().trim());
    variable.setVariable(l_Variable.getText());
    variable.setTrimValeur(chk_Trim.isSelected());
  }
  
  // ------------------------------------ Evènements -------------------------
  
  private void cancelButtonActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void okButtonActionPerformed(ActionEvent e) {
    creationVariable();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    label2 = new JLabel();
    tf_Nom = new JTextField();
    chk_Trim = new JCheckBox();
    l_Variable = new JLabel();
    label3 = new JLabel();
    buttonBar = new JPanel();
    okButton = new JButton();
    cancelButton = new JButton();
    
    // ======== this ========
    setTitle("Editeur de variable");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setName("dialogPane");
      dialogPane.setLayout(new BorderLayout());
      
      // ======== contentPanel ========
      {
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(new BorderLayout());
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- label2 ----
          label2.setText("Nom de la variable");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(new Rectangle(new Point(20, 40), label2.getPreferredSize()));
          
          // ---- tf_Nom ----
          tf_Nom.setName("tf_Nom");
          panel1.add(tf_Nom);
          tf_Nom.setBounds(160, 35, 170, tf_Nom.getPreferredSize().height);
          
          // ---- chk_Trim ----
          chk_Trim.setText("On supprime les blancs");
          chk_Trim.setName("chk_Trim");
          panel1.add(chk_Trim);
          chk_Trim.setBounds(new Rectangle(new Point(165, 70), chk_Trim.getPreferredSize()));
          
          // ---- l_Variable ----
          l_Variable.setText("text");
          l_Variable.setName("l_Variable");
          panel1.add(l_Variable);
          l_Variable.setBounds(160, 15, 130, l_Variable.getPreferredSize().height);
          
          // ---- label3 ----
          label3.setText("Variable");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(new Rectangle(new Point(20, 15), label3.getPreferredSize()));
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        contentPanel.add(panel1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      
      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setName("buttonBar");
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 85, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0 };
        
        // ---- okButton ----
        okButton.setText("OK");
        okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        okButton.setName("okButton");
        okButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cancelButton ----
        cancelButton.setText("Annuler");
        cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cancelButton.setName("cancelButton");
        cancelButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            cancelButtonActionPerformed(e);
          }
        });
        buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel label2;
  private JTextField tf_Nom;
  private JCheckBox chk_Trim;
  private JLabel l_Variable;
  private JLabel label3;
  private JPanel buttonBar;
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
