/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.outils;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

/**
 * Cette classe est une enveloppe pour le transfert de données des références
 * d'objet transférées dans la même machine virtuelle
 */
public class SelectionLocale implements Transferable {
  private Object selection;
  
  /**
   * Construit la sélection
   * 
   * @param sélection
   *          : n'importe quel type d'objet
   */
  public SelectionLocale(Object selection) {
    this.selection = selection;
  }
  
  public DataFlavor[] getTransferDataFlavors() {
    DataFlavor[] flavors = new DataFlavor[1];
    Class<? extends Object> type = selection.getClass();
    String typeMIME = "application/x-java-jvm-local-objectref;class=" + type.getName();
    try {
      flavors[0] = new DataFlavor(typeMIME);
      return flavors;
    }
    catch (ClassNotFoundException ex) {
      return new DataFlavor[0];
    }
  }
  
  public boolean isDataFlavorSupported(DataFlavor flavor) {
    return "application".equals(flavor.getPrimaryType()) && "x-java-jvm-local-objectref".equals(flavor.getSubType())
        && flavor.getRepresentationClass().isAssignableFrom(selection.getClass());
  }
  
  public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException, IOException {
    if (!isDataFlavorSupported(flavor)) {
      throw new UnsupportedFlavorException(flavor);
    }
    return selection;
  }
}
