/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.newsim.description.DescriptionCondition;

/**
 * Boite de dialogue permettant la saisie d'une condition
 */
public class GfxConditionEditor extends JDialog {
  
  // Variables
  private DescriptionCondition condition = null;
  
  /**
   * Constructeur
   * @param owner
   */
  public GfxConditionEditor(Frame owner) {
    super(owner);
    initComponents();
  }
  
  /**
   * Constructeur
   * @param owner
   */
  public GfxConditionEditor(Dialog owner) {
    super(owner);
    initComponents();
  }
  
  public void setCondition(DescriptionCondition condition) {
    this.condition = condition;
    initCondition();
  }
  
  // ------------------------------------ Méthodes privées -------------------
  
  private void initCondition() {
    if (condition == null) {
      return;
    }
    
    tf_Nom.setText(condition.getNom());
    l_Variable.setText(condition.getVariable());
    tf_Valeur.setText(condition.getValeur());
    chk_Trim.setSelected(condition.isTrimVariable());
    chk_DistinctionMAJmin.setSelected(condition.isDistinctionMAJmin());
    
    switch (condition.getOperation()) {
      case DescriptionCondition.EQUALS:
        rb_Egal.setSelected(true);
        break;
      case DescriptionCondition.CONTAINS:
        rb_Contient.setSelected(true);
        break;
      case DescriptionCondition.STARTWITH:
        rb_Debute.setSelected(true);
        break;
      case DescriptionCondition.ENDWITH:
        rb_Fini.setSelected(true);
        break;
      case DescriptionCondition.NOT_EQUALS:
        rb_NotEgal.setSelected(true);
        break;
      case DescriptionCondition.NOT_CONTAINS:
        rb_NotContient.setSelected(true);
        break;
      case DescriptionCondition.NOT_STARTWITH:
        rb_NotDebute.setSelected(true);
        break;
      case DescriptionCondition.NOT_ENDWITH:
        rb_NotFini.setSelected(true);
        break;
    }
  }
  
  private void creationCondition() {
    if (condition == null) {
      return;
    }
    
    condition.setNom(tf_Nom.getText().trim());
    condition.setValeur(tf_Valeur.getText());
    condition.setTrimVariable(chk_Trim.isSelected());
    condition.setDistinctionMAJmin(chk_DistinctionMAJmin.isSelected());
    
    if (rb_Egal.isSelected()) {
      condition.setOperation(DescriptionCondition.EQUALS);
    }
    else {
      if (rb_Contient.isSelected()) {
        condition.setOperation(DescriptionCondition.CONTAINS);
      }
      else {
        if (rb_Debute.isSelected()) {
          condition.setOperation(DescriptionCondition.STARTWITH);
        }
        else {
          if (rb_Fini.isSelected()) {
            condition.setOperation(DescriptionCondition.ENDWITH);
          }
          else {
            if (rb_NotEgal.isSelected()) {
              condition.setOperation(DescriptionCondition.NOT_EQUALS);
            }
            else {
              if (rb_NotContient.isSelected()) {
                condition.setOperation(DescriptionCondition.NOT_CONTAINS);
              }
              else {
                if (rb_NotDebute.isSelected()) {
                  condition.setOperation(DescriptionCondition.NOT_STARTWITH);
                }
                else {
                  if (rb_NotFini.isSelected()) {
                    condition.setOperation(DescriptionCondition.NOT_ENDWITH);
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  // ------------------------------------ Evènements -------------------------
  
  private void cancelButtonActionPerformed(ActionEvent e) {
    dispose();
  }
  
  private void okButtonActionPerformed(ActionEvent e) {
    creationCondition();
    dispose();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    dialogPane = new JPanel();
    contentPanel = new JPanel();
    panel1 = new JPanel();
    label2 = new JLabel();
    tf_Nom = new JTextField();
    label1 = new JLabel();
    rb_Egal = new JRadioButton();
    rb_Contient = new JRadioButton();
    rb_Debute = new JRadioButton();
    rb_Fini = new JRadioButton();
    tf_Valeur = new JTextField();
    chk_DistinctionMAJmin = new JCheckBox();
    chk_Trim = new JCheckBox();
    rb_NotContient = new JRadioButton();
    rb_NotDebute = new JRadioButton();
    rb_NotEgal = new JRadioButton();
    rb_NotFini = new JRadioButton();
    label3 = new JLabel();
    l_Variable = new JLabel();
    label4 = new JLabel();
    buttonBar = new JPanel();
    okButton = new JButton();
    cancelButton = new JButton();
    
    // ======== this ========
    setTitle("Editeur de condition");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== dialogPane ========
    {
      dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
      dialogPane.setName("dialogPane");
      dialogPane.setLayout(new BorderLayout());
      
      // ======== contentPanel ========
      {
        contentPanel.setName("contentPanel");
        contentPanel.setLayout(new BorderLayout());
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("La condition est vrai si"));
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- label2 ----
          label2.setText("Nom de la condition");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(new Rectangle(new Point(20, 35), label2.getPreferredSize()));
          
          // ---- tf_Nom ----
          tf_Nom.setName("tf_Nom");
          panel1.add(tf_Nom);
          tf_Nom.setBounds(315, 30, 170, tf_Nom.getPreferredSize().height);
          
          // ---- label1 ----
          label1.setText("La variable");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(20, 65, 75, label1.getPreferredSize().height);
          
          // ---- rb_Egal ----
          rb_Egal.setText("est \u00e9gale \u00e0");
          rb_Egal.setSelected(true);
          rb_Egal.setName("rb_Egal");
          panel1.add(rb_Egal);
          rb_Egal.setBounds(55, 90, 135, rb_Egal.getPreferredSize().height);
          
          // ---- rb_Contient ----
          rb_Contient.setText("contient");
          rb_Contient.setName("rb_Contient");
          panel1.add(rb_Contient);
          rb_Contient.setBounds(55, 115, 135, rb_Contient.getPreferredSize().height);
          
          // ---- rb_Debute ----
          rb_Debute.setText("commence  par");
          rb_Debute.setName("rb_Debute");
          panel1.add(rb_Debute);
          rb_Debute.setBounds(55, 140, 135, rb_Debute.getPreferredSize().height);
          
          // ---- rb_Fini ----
          rb_Fini.setText("fini par");
          rb_Fini.setName("rb_Fini");
          panel1.add(rb_Fini);
          rb_Fini.setBounds(55, 165, 135, rb_Fini.getPreferredSize().height);
          
          // ---- tf_Valeur ----
          tf_Valeur.setName("tf_Valeur");
          panel1.add(tf_Valeur);
          tf_Valeur.setBounds(160, 195, 325, tf_Valeur.getPreferredSize().height);
          
          // ---- chk_DistinctionMAJmin ----
          chk_DistinctionMAJmin.setText("Distinction MAJ/min");
          chk_DistinctionMAJmin.setName("chk_DistinctionMAJmin");
          panel1.add(chk_DistinctionMAJmin);
          chk_DistinctionMAJmin.setBounds(20, 230, 220, chk_DistinctionMAJmin.getPreferredSize().height);
          
          // ---- chk_Trim ----
          chk_Trim.setText("trim\u00e9e)");
          chk_Trim.setName("chk_Trim");
          panel1.add(chk_Trim);
          chk_Trim.setBounds(new Rectangle(new Point(105, 63), chk_Trim.getPreferredSize()));
          
          // ---- rb_NotContient ----
          rb_NotContient.setText("ne contient pas");
          rb_NotContient.setName("rb_NotContient");
          panel1.add(rb_NotContient);
          rb_NotContient.setBounds(200, 115, 169, rb_NotContient.getPreferredSize().height);
          
          // ---- rb_NotDebute ----
          rb_NotDebute.setText("ne commence pas par");
          rb_NotDebute.setName("rb_NotDebute");
          panel1.add(rb_NotDebute);
          rb_NotDebute.setBounds(new Rectangle(new Point(200, 140), rb_NotDebute.getPreferredSize()));
          
          // ---- rb_NotEgal ----
          rb_NotEgal.setText("n'est pas \u00e9gale \u00e0");
          rb_NotEgal.setName("rb_NotEgal");
          panel1.add(rb_NotEgal);
          rb_NotEgal.setBounds(200, 90, 169, rb_NotEgal.getPreferredSize().height);
          
          // ---- rb_NotFini ----
          rb_NotFini.setText("ne fini pas par");
          rb_NotFini.setName("rb_NotFini");
          panel1.add(rb_NotFini);
          rb_NotFini.setBounds(200, 165, 169, rb_NotFini.getPreferredSize().height);
          
          // ---- label3 ----
          label3.setText("au/le contenu suivant");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(new Rectangle(new Point(20, 200), label3.getPreferredSize()));
          
          // ---- l_Variable ----
          l_Variable.setText("Variable");
          l_Variable.setFont(l_Variable.getFont().deriveFont(l_Variable.getFont().getStyle() | Font.BOLD));
          l_Variable.setName("l_Variable");
          panel1.add(l_Variable);
          l_Variable.setBounds(180, 65, 300, l_Variable.getPreferredSize().height);
          
          // ---- label4 ----
          label4.setText("(");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(100, 65, 10, label4.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        contentPanel.add(panel1, BorderLayout.CENTER);
      }
      dialogPane.add(contentPanel, BorderLayout.CENTER);
      
      // ======== buttonBar ========
      {
        buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
        buttonBar.setName("buttonBar");
        buttonBar.setLayout(new GridBagLayout());
        ((GridBagLayout) buttonBar.getLayout()).columnWidths = new int[] { 0, 85, 80 };
        ((GridBagLayout) buttonBar.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0 };
        
        // ---- okButton ----
        okButton.setText("OK");
        okButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        okButton.setName("okButton");
        okButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            okButtonActionPerformed(e);
          }
        });
        buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cancelButton ----
        cancelButton.setText("Annuler");
        cancelButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cancelButton.setName("cancelButton");
        cancelButton.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            cancelButtonActionPerformed(e);
          }
        });
        buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      dialogPane.add(buttonBar, BorderLayout.SOUTH);
    }
    contentPane.add(dialogPane, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rb_Egal);
    buttonGroup1.add(rb_Contient);
    buttonGroup1.add(rb_Debute);
    buttonGroup1.add(rb_Fini);
    buttonGroup1.add(rb_NotContient);
    buttonGroup1.add(rb_NotDebute);
    buttonGroup1.add(rb_NotEgal);
    buttonGroup1.add(rb_NotFini);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel dialogPane;
  private JPanel contentPanel;
  private JPanel panel1;
  private JLabel label2;
  private JTextField tf_Nom;
  private JLabel label1;
  private JRadioButton rb_Egal;
  private JRadioButton rb_Contient;
  private JRadioButton rb_Debute;
  private JRadioButton rb_Fini;
  private JTextField tf_Valeur;
  private JCheckBox chk_DistinctionMAJmin;
  private JCheckBox chk_Trim;
  private JRadioButton rb_NotContient;
  private JRadioButton rb_NotDebute;
  private JRadioButton rb_NotEgal;
  private JRadioButton rb_NotFini;
  private JLabel label3;
  private JLabel l_Variable;
  private JLabel label4;
  private JPanel buttonBar;
  private JButton okButton;
  private JButton cancelButton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
