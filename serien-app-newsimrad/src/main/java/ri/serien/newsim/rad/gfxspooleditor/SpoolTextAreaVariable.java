/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.gfxspooleditor;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;

import ri.serien.newsim.description.DescriptionVariable;

/**
 * Description du textArea pour la sélection des Variables
 */
public class SpoolTextAreaVariable extends SpoolTextArea {
  
  // Variables
  private JMenuItem mi_Variable;
  private ArrayList<DescriptionVariable> listeVariable = null;
  private JLabel texteVariable = null;
  
  /**
   * Constructeur
   */
  public SpoolTextAreaVariable() {
    super();
    addMouseMotionListener(new MouseMotionAdapter() {
      @Override
      public void mouseMoved(MouseEvent e) {
        ta_PageVariableMouseMoved(e);
      }
    });
    creationMenuContextuel();
    setCouleurSelection(new Color(0, 255, 0));
  }
  
  /**
   * Initialise la liste des variables
   * @return
   */
  public void setListeVariable(ArrayList<DescriptionVariable> listeVar) {
    this.listeVariable = listeVar;
  }
  
  /**
   * Importation de la description pour les variables
   */
  public void importerDescription() {
    if (listeVariable == null) {
      return;
    }
    
    // Récupération des sélections
    for (int i = listeVariable.size(); --i >= 0;) {
      convertDescription2Selection(listeVariable.get(i).getVariable(), listeRectangle);
    }
    repaint();
  }
  
  /**
   * Création du menu contextuel
   */
  @Override
  protected void creationMenuContextuel() {
    super.creationMenuContextuel();
    
    mi_Variable = new JMenuItem();
    
    // ---- mi_TrimageDroiteSelection ----
    mi_Variable.setText("Editeur de variable");
    mi_Variable.setName("mi_Variable");
    mi_Variable.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        mi_VariableActionPerformed(e);
      }
    });
    pm_Btd.add(mi_Variable);
  }
  
  /**
   * @param texteVariable the texteVariable to set
   */
  public void setTexteVariable(JLabel texteVariable) {
    this.texteVariable = texteVariable;
  }
  
  /**
   * @return the texteVariable
   */
  public JLabel getTexteVariable() {
    return texteVariable;
  }
  
  // ------------------------------------ Méthodes privées -------------------
  
  private void ta_PageVariableMouseMoved(MouseEvent e) {
    indiceSelection = findWhoIsSelectionned(e);
    
    if (indiceSelection == -1) {
      return;
    }
    String selection = listeRectangle.get(indiceSelection).getLibelle();
    DescriptionVariable variable = getVariable(selection);
    if ((variable != null) && (texteVariable != null)) {
      texteVariable.setText(variable.getNom());
    }
  }
  
  /**
   * Retourne la variable à partir de son nom
   */
  private DescriptionVariable getVariable(String variable) {
    for (int i = 0; i < listeVariable.size(); i++) {
      if (listeVariable.get(i).getVariable().equals(variable)) {
        return listeVariable.get(i);
      }
    }
    return null;
  }
  
  /**
   * Supprime la variable à partir de son nom
   */
  private void removeVariable(String variable) {
    for (int i = 0; i < listeVariable.size(); i++) {
      if (listeVariable.get(i).getVariable().equals(variable)) {
        listeVariable.remove(i);
        return;
      }
    }
  }
  
  // ------------------------------------ Evènements -------------------------
  
  @Override
  protected void mi_SuppimerActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    
    String selection = listeRectangle.get(indiceSelection).getLibelle();
    removeVariable(selection);
    super.mi_SuppimerActionPerformed(e);
  }
  
  private void mi_VariableActionPerformed(ActionEvent e) {
    if (indiceSelection == -1) {
      return;
    }
    DescriptionVariable variable = null;
    
    // Recherche la condition à partir de la sélection (variable)
    String selection = listeRectangle.get(indiceSelection).getLibelle();
    variable = getVariable(selection);
    if (variable == null) {
      variable = new DescriptionVariable();
      listeVariable.add(variable);
      variable.setVariable(selection);
    }
    
    // Affichage de la boite de dialogue
    GfxVariableEditor dialog_var = new GfxVariableEditor((JFrame) this.getTopLevelAncestor());
    dialog_var.setVariable(variable);
    dialog_var.setVisible(true);
    // texteCondition.setText(condition.getLibelleCondition());
    
    repaint();
  }
}
