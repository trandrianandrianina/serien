/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.composant;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JComponent;
import javax.swing.JTextField;

public class SaisieVolatile extends JTextField {
  // Variables
  private JComponent composant = null;
  private String oldtext = null;
  
  /**
   * Constructeur
   */
  public SaisieVolatile(Point position, Dimension taille, final JComponent composant) {
    setOpaque(true);
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
          valideSaisie();
        }
      }
    });
    setAffiche(position, taille, composant);
  }
  
  /**
   * Appel pour la saisie
   * @param position
   * @param taille
   * @param texte
   */
  public void setAffiche(Point position, Dimension taille, final JComponent composant) {
    setLocation(position);
    setSize(taille);
    this.composant = composant;
    if (composant != null) {
      composant.getParent().add(this);
      if (composant instanceof GfxLabel) {
        setText(((GfxLabel) composant).getText());
        oldtext = getText();
      }
    }
    requestFocus();
    selectAll();
    setVisible(true);
  }
  
  /**
   * Valide la saisie
   */
  private void valideSaisie() {
    if (composant != null) {
      if (composant instanceof GfxLabel) {
        ((GfxLabel) composant).setText(getText());
        if (!oldtext.equals(getText())) {
          ((GfxLabel) composant).pageEditor.save(true);
        }
      }
    }
    setVisible(false);
  }
}
