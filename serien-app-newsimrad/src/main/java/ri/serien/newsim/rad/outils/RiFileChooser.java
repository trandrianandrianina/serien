/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rad.outils;

import java.awt.Component;
import java.awt.Container;
import java.io.File;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import ri.serien.libcommun.outils.fichier.FiltreFichierParExtension;

public class RiFileChooser {
  // Variables
  private File dernierChemin = null;
  private JFrame frame = null;
  private JFileChooser choixFichier = null;
  
  /**
   * Constructeur
   * @param derchemin
   */
  public RiFileChooser(JFrame frame, File derchemin) {
    setFrame(frame);
    setDossier(derchemin);
  }
  
  /**
   * Constructeur
   * @param derchemin
   */
  public RiFileChooser(JFrame frame, String derchemin) {
    setFrame(frame);
    if (derchemin == null) {
      return;
    }
    setDossier(new File(derchemin));
  }
  
  /**
   * Initialise la fenêtre maitresse
   * @param frame
   */
  private void setFrame(JFrame frame) {
    this.frame = frame;
  }
  
  /**
   * Initialise le dossier par défaut
   * @param derchemin
   */
  public void setDossier(File derchemin) {
    dernierChemin = derchemin;
    // if (dernierChemin != null)
    // choixFichier.setCurrentDirectory(dernierChemin);
  }
  
  /**
   * Retourne le dossier par défaut
   * @return
   */
  public File getDossierFile() {
    return dernierChemin;
  }
  
  /**
   * Retourne le dossier par défaut
   * @return
   */
  public String getDossier() {
    return dernierChemin.getAbsolutePath();
  }
  
  /**
   * Recherche le fichier à ouvrir
   */
  public String choisirFichier(String extension, String extdescription) {
    return choisirFichier(extension, extdescription, false);
  }
  
  /**
   * Recherche le fichier à ouvrir
   */
  public String choisirFichier(String extension, String extdescription, boolean nochangefolder) {
    String fichier = null;
    
    choixFichier = new JFileChooser();
    UIManager.put("FileChooser.openDialogTitleText", "Ouvrir");
    SwingUtilities.updateComponentTreeUI(choixFichier);
    choixFichier.setCurrentDirectory(dernierChemin);
    // choixFichier.addChoosableFileFilter(new GestionFiltre(new String[]{extension}, extdescription));
    choixFichier.setFileFilter(new FiltreFichierParExtension(new String[] { extension }, extdescription));
    choixFichier.setAcceptAllFileFilterUsed(false);
    if (nochangefolder) {
      disableUpFolderButton(choixFichier);
    }
    
    // Récupération du chemin
    if (choixFichier.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
      fichier = choixFichier.getSelectedFile().getPath();
    }
    else {
      return null;
    }
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
  
  /**
   * Recherche le fichier à ouvrir
   */
  public String choisirFichier(String[] extension, String extdescription) {
    return choisirFichier(extension, extdescription, false);
  }
  
  /**
   * Recherche le fichier à ouvrir
   */
  public String choisirFichier(String[] extension, String extdescription, boolean nochangefolder) {
    String fichier = null;
    
    choixFichier = new JFileChooser();
    UIManager.put("FileChooser.openDialogTitleText", "Ouvrir");
    SwingUtilities.updateComponentTreeUI(choixFichier);
    choixFichier.setCurrentDirectory(dernierChemin);
    // choixFichier.addChoosableFileFilter(new GestionFiltre(extension, extdescription));
    choixFichier.setFileFilter(new FiltreFichierParExtension(extension, extdescription));
    choixFichier.setAcceptAllFileFilterUsed(false);
    if (nochangefolder) {
      disableUpFolderButton(choixFichier);
    }
    
    // Récupération du chemin
    if (choixFichier.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {
      fichier = choixFichier.getSelectedFile().getAbsolutePath();
    }
    else {
      return null;
    }
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
  
  /**
   * Ouvre une boite de dialogue afin de sélectionner un dossier
   */
  public File choisirDossier(String origine) {
    if ((origine == null) && (dernierChemin != null)) {
      origine = dernierChemin.getAbsolutePath();
    }
    choixFichier = new JFileChooser(origine);
    UIManager.put("FileChooser.openDialogTitleText", "Sélection du dossier de travail");
    SwingUtilities.updateComponentTreeUI(choixFichier);
    choixFichier.setApproveButtonText("Sélectionner");
    choixFichier.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    if (choixFichier.showOpenDialog(frame) == JFileChooser.CANCEL_OPTION) {
      return null;
    }
    dernierChemin = choixFichier.getSelectedFile();
    
    return dernierChemin;
  }
  
  /**
   * Recherche le fichier pour enregistrer
   * @return
   */
  public File enregistreFichier(File fichier, String extension, String extdescription) {
    choixFichier = new JFileChooser();
    choixFichier.setCurrentDirectory(dernierChemin);
    choixFichier.addChoosableFileFilter(new FiltreFichierParExtension(new String[] { extension }, extdescription));
    choixFichier.setSelectedFile(fichier);
    
    // Récupération du chemin
    if (choixFichier.showSaveDialog(choixFichier) == JFileChooser.APPROVE_OPTION) {
      fichier = choixFichier.getSelectedFile();
    }
    else {
      return null;
    }
    
    // On stocke le dossier courant
    dernierChemin = choixFichier.getSelectedFile().getParentFile();
    
    return fichier;
  }
  
  /**
   * Permet de griser les composants de la fenêtre permettant de changer de dossier
   * @param c
   */
  private static void disableUpFolderButton(Container c) {
    int len = c.getComponentCount();
    for (int i = 0; i < len; i++) {
      Component comp = c.getComponent(i);
      if (comp instanceof JButton) {
        JButton b = (JButton) comp;
        Icon icon = b.getIcon();
        if (icon != null && (icon == UIManager.getIcon("FileChooser.upFolderIcon")
            || icon == UIManager.getIcon("FileChooser.newFolderIcon") || icon == UIManager.getIcon("FileChooser.homeFolderIcon"))) {
          b.setEnabled(false);
        }
      }
      else {
        if (comp instanceof JComboBox) {
          if (!(((JComboBox) comp).getSelectedItem() instanceof FiltreFichierParExtension)) {
            comp.setEnabled(false);
          }
        }
        else {
          if (comp instanceof Container) {
            disableUpFolderButton((Container) comp);
          }
        }
      }
    }
  }
}
