/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.util.HashMap;

import ri.serien.libas400.database.record.GenericRecord;

public abstract class PanierArticle {
  // Variables
  protected String a1art = null;
  protected String a1etb = null;
  
  public abstract void addRcd_pinjbdvdsl(GenericRecord pinjbdvdsl, HashMap<String, Object> entete, int numligne);
  
  // -- Accesseurs ----------------------------------------------------------
  /**
   * @return le a1art
   */
  public String getA1art() {
    return a1art;
  }
  
  /**
   * @param a1art le a1art à définir
   */
  public void setA1art(String a1art) {
    this.a1art = a1art;
  }
  
  public String getA1etb() {
    return a1etb;
  }
  
  public void setA1etb(String a1etb) {
    this.a1etb = a1etb;
  }
  
}
