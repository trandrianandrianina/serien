/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

public class PanierArticleNormal extends PanierArticle {
  // Variables
  private BigDecimal quantite = BigDecimal.ZERO;
  private BigDecimal tarifHTUnitaire = BigDecimal.ZERO;
  private BigDecimal tarifHTUnitaireCalcul = BigDecimal.ZERO;
  private BigDecimal totalPourQuantite = BigDecimal.ZERO;
  private BigDecimal totalEcotaxe = BigDecimal.ZERO;
  private String typeArticle = "C";
  private String libelleArt = null;
  private String uniteArt = null;
  private Float stockDispo = null;
  private int conditionnM = 1;
  private int nbDecimalesQ = 0;
  private String refFournisseur = null;
  private boolean isLot = false;
  private UtilisateurWebshop utilisateur = null;
  private ArrayList<GenericRecord> listeTravailArticle = null;
  
  private GenericRecord rcd_pgvmartm = null; // A déterminer son utilité mais
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Constructeur de PanierArticleNormal
   */
  public PanierArticleNormal(UtilisateurWebshop util) {
    utilisateur = util;
  }
  
  /**
   * Ajoute les champs de l'article pour la future injection
   * 
   * @param pinjbdvdsl
   */
  @Override
  public void addRcd_pinjbdvdsl(GenericRecord pinjbdvdsl, HashMap<String, Object> entete, int numligne) {
    if (pinjbdvdsl == null || utilisateur.getEtablissementEnCours() == null) {
      return;
    }
    
    // On écrit les champs pour cette ligne article (notamment la clef)
    for (Entry<String, Object> entry : entete.entrySet()) {
      if (entry.getKey().startsWith("BL")) {
        // champ pour la ligne
        pinjbdvdsl.setField(entry.getKey(), entry.getValue());
      }
    }
    
    // On écrit les infos génériques
    pinjbdvdsl.setField("BLNLI", numligne);
    pinjbdvdsl.setField("BLERL", "L");
    
    // Et le reste des informations propre à l'article
    pinjbdvdsl.setField("BLART", a1art);
    
    // Transformation du prix avec déciamle pour mise à jour PINJBDV
    String tarifBrut = tarifHTUnitaire.toString();
    if (tarifBrut.length() > utilisateur.getEtablissementEnCours().getDecimales_client()) {
      tarifBrut = tarifBrut.substring(0, tarifBrut.length() - utilisateur.getEtablissementEnCours().getDecimales_client()) + "."
          + tarifBrut.substring(tarifBrut.length() - utilisateur.getEtablissementEnCours().getDecimales_client());
    }
    else if (tarifBrut.length() == utilisateur.getEtablissementEnCours().getDecimales_client()) {
      tarifBrut = "0." + tarifBrut;
    }
    else {
      tarifBrut = tarifBrut + ".00";
    }
    
    Trace.debug("addRcd_pinjbdvdsl() Article AVANT PINJ: " + a1art);
    Trace.debug("addRcd_pinjbdvdsl() tarifBrut AVANT PINJ: " + tarifBrut);
    
    pinjbdvdsl.setField("BLPRX", tarifHTUnitaire);
    
    if (nbDecimalesQ > 2) {
      String quantiteFormatee = Outils.afficherQuantiteDIGITS(quantite, 11, nbDecimalesQ);
      pinjbdvdsl.setField("BLQTE3", quantiteFormatee);
      // pinjbdvdsl.setField("BLQTE", quantite);
    }
    else {
      // pinjbdvdsl.setField("BLQTE3", "");
      pinjbdvdsl.setField("BLQTE", quantite);
    }
  }
  
  /**
   * Calculer le montant de la ligne pour l'article et la quantité choisie.
   */
  public void calculerTotalLigne() {
    // Gestion de l'ecotaxe
    majEcotaxe();
    
    // MODE ON ADDITIONNE L'ECOTAXE DANS LA LIGNE
    totalPourQuantite = tarifHTUnitaireCalcul.multiply(quantite).add(totalEcotaxe);
    Trace.debug("calculTotalQuantite() totalPourQuantite: " + totalPourQuantite.toString() + " -> tarifHTUnitaireCalcul: "
        + tarifHTUnitaireCalcul.toString() + " -> quantite: " + quantite.toString() + " -> ecotaxe: " + totalEcotaxe.toString());
  }
  
  /**
   * On calcule l'ecotaxe de cet article afin de le rajouter à la ligne
   */
  private void majEcotaxe() {
    if (utilisateur == null || utilisateur.getEtablissementEnCours() == null) {
      return;
    }
    
    if (utilisateur.getEtablissementEnCours().isGestion_ecotaxe()) {
      if (quantite != null && a1art != null && a1etb != null) {
        listeTravailArticle = utilisateur.getAccesDB2()
            .select(" SELECT ATP01,TENBR FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMTEEM " + " LEFT JOIN "
                + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMTARM ON ATETB = TEETB AND ATART = TEARTT " + " WHERE TEETB = '" + a1etb.trim()
                + "' AND TEARTD ='" + a1art.trim() + "' ORDER BY ATDAP DESC FETCH FIRST 1 ROWS ONLY");
        
        if (listeTravailArticle != null && listeTravailArticle.size() > 0) {
          if (listeTravailArticle.get(0).isPresentField("ATP01") && listeTravailArticle.get(0).isPresentField("TENBR")) {
            totalEcotaxe = quantite.multiply(((BigDecimal) listeTravailArticle.get(0).getField("TENBR"))
                .multiply((BigDecimal) listeTravailArticle.get(0).getField("ATP01")));
          }
          
          Trace.debug("[majEcotaxe()] totalEcotaxe: " + totalEcotaxe);
        }
        else {
          Trace.debug("[majEcotaxe()] Pas d'ecotaxe pour cet article !!");
        }
      }
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le quantite
   */
  public BigDecimal getQuantite() {
    return quantite;
  }
  
  /**
   * @return le quantite
   */
  public BigDecimal getQuantiteFormatee() {
    if (quantite != null) {
      quantite = quantite.setScale(nbDecimalesQ, RoundingMode.HALF_UP);
    }
    return quantite;
  }
  
  /**
   * @param quantite
   *          le quantite à définir
   */
  public void setQuantite(int quantite) {
    if (quantite >= 0) {
      this.quantite = new BigDecimal(quantite);
    }
  }
  
  /**
   * @param quantite
   *          le quantite à définir
   */
  public void setQuantite(BigDecimal quantite) {
    if (quantite.compareTo(BigDecimal.ZERO) >= 0) {
      this.quantite = quantite;
    }
  }
  
  /**
   * @return le tarifHTUnitaire
   */
  public BigDecimal getTarifHTUnitaire() {
    return tarifHTUnitaire;
  }
  
  /**
   * Définie le tarif HT unitaire
   * @param pTarif
   * 
   */
  public void setTarifHTUnitaire(BigDecimal pTarif) {
    // Si ce n'est pas un article à rajouter au panier
    if (typeArticle == null || !typeArticle.equals("C")) {
      return;
    }
    
    this.tarifHTUnitaireCalcul = pTarif;
    
    this.tarifHTUnitaire = pTarif;
  }
  
  /**
   * @return le rcd_pgvmartm
   */
  public GenericRecord getRcd_pgvmartm() {
    return rcd_pgvmartm;
  }
  
  /**
   * @param rcd_pgvmartm
   *          le rcd_pgvmartm à définir
   */
  public void setRcd_pgvmartm(GenericRecord rcd_pgvmartm) {
    this.rcd_pgvmartm = rcd_pgvmartm;
  }
  
  public String getLibelleArt() {
    return libelleArt;
  }
  
  public void setLibelleArt(String libelleArt) {
    this.libelleArt = libelleArt;
  }
  
  public String getUniteArt() {
    return uniteArt;
  }
  
  public void setUniteArt(String uniteArt) {
    this.uniteArt = uniteArt;
  }
  
  public Float getStockDispo() {
    return stockDispo;
  }
  
  public void setStockDispo(Float stockDispo) {
    this.stockDispo = stockDispo;
  }
  
  public int getConditionnM() {
    return conditionnM;
  }
  
  public int getNbDecimalesQ() {
    return nbDecimalesQ;
  }
  
  public void setNbDecimalesQ(int nbDecimalesQ) {
    this.nbDecimalesQ = nbDecimalesQ;
  }
  
  public void setConditionnM(String conditionnM) {
    try {
      double nbBrut = Double.parseDouble(conditionnM);
      this.conditionnM = (int) nbBrut;
    }
    catch (Exception e) {
    }
  }
  
  public String getRefFournisseur() {
    return refFournisseur;
  }
  
  public void setRefFournisseur(String refFournisseur) {
    this.refFournisseur = refFournisseur;
  }
  
  public BigDecimal getTarifHTUnitaireCalcul() {
    return tarifHTUnitaireCalcul;
  }
  
  public void setTarifHTUnitaireCalcul(BigDecimal tarifHTUnitaireCalcul) {
    this.tarifHTUnitaireCalcul = tarifHTUnitaireCalcul;
  }
  
  public BigDecimal getTotalPourQuantite() {
    return totalPourQuantite;
  }
  
  public void setTotalPourQuantite(BigDecimal totalPourQuantite) {
    this.totalPourQuantite = totalPourQuantite;
  }
  
  public String getTypeArticle() {
    return typeArticle;
  }
  
  public void setTypeArticle(String typeArticle) {
    this.typeArticle = typeArticle;
  }
  
  public void setConditionnM(int conditionnM) {
    this.conditionnM = conditionnM;
  }
  
  public BigDecimal getTotalEcotaxe() {
    return totalEcotaxe;
  }
  
  public void setTotalEcotaxe(BigDecimal totalEcotaxe) {
    this.totalEcotaxe = totalEcotaxe;
  }
  
  public boolean isLot() {
    return isLot;
  }
  
  public void setLot(boolean isLot) {
    this.isLot = isLot;
  }
  
}
