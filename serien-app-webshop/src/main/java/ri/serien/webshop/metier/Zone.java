/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

public class Zone {
  private String fieldName = "";
  // private int taillePdf= 0;
  // private int row = 0;
  private int align = 0;
  // private Font font;
  private int colspan = 0;
  
  /*public Zone (String name, int alignTexte, Font police, int sizeCol)//setRowspan
  {
    fieldName=name;
  //  row=nbLigne;
    align = alignTexte;
    font = police;
    colspan = sizeCol;
  
  }*/
  
  // retourne le nom
  public String getFieldName() {
    return fieldName;
  }
  
  // définit le nom
  public void setFieldName(String fieldName) {
    this.fieldName = fieldName;
  }
  
  /*public int getTaillePdf() {
    return taillePdf;
  }
  public void setTaillePdf(int taillePdf) {
    this.taillePdf = taillePdf;
  }*/
  /*public int getRow() {
    return row;
  }
  public void setRow(int row) {
    this.row = row;
  }*/
  
  public int getAlign() {
    return align;
  }
  
  public void setAlign(int align) {
    this.align = align;
  }
  
  public int getColspan() {
    return colspan;
  }
  
  public void setColspan(int colspan) {
    this.colspan = colspan;
  }
  
}
