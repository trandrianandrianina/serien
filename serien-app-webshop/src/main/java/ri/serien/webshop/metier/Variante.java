/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

public class Variante {
  
  private String PARZ2 = null;
  private int nbCodeArticle = 0;
  private int nbMotClass1 = 0;
  private int nbMotClass2 = 0;
  private int nbCodeFamille = 0;
  private int nbCodeTarif = 0;
  private int nbRefFourniss = 0;
  
  private boolean isMemeFournisseur = false;
  
  private boolean proposerAvente = false;
  private boolean proposerAfabrication = false;
  private boolean obligerAvente = false;
  private boolean obligerAfabrication = false;
  
  public Variante(String chaine) {
    if (chaine == null) {
      return;
    }
    
    PARZ2 = chaine;
    decoupageChaine();
  }
  
  private void decoupageChaine() {
    if (PARZ2 == null || PARZ2.length() < 100) {
      return;
    }
    
    nbCodeArticle = Integer.parseInt(PARZ2.substring(30, 32));
    nbMotClass1 = Integer.parseInt(PARZ2.substring(32, 34));
    nbMotClass2 = Integer.parseInt(PARZ2.substring(34, 36));
    nbCodeFamille = Integer.parseInt(PARZ2.substring(36, 37));
    nbCodeTarif = Integer.parseInt(PARZ2.substring(37, 38));
    nbRefFourniss = Integer.parseInt(PARZ2.substring(38, 40));
    
    isMemeFournisseur = PARZ2.substring(40, 43).equals("OUI");
    proposerAvente = PARZ2.substring(49, 52).equals("OUI");
    proposerAfabrication = PARZ2.substring(52, 55).equals("OUI");
    obligerAvente = PARZ2.substring(43, 46).equals("OUI");
    obligerAfabrication = PARZ2.substring(46, 49).equals("OUI");
  }
  
  public int getNbMotClass1() {
    return nbMotClass1;
  }
  
  public int getNbMotClass2() {
    return nbMotClass2;
  }
  
  public int getNbCodeArticle() {
    return nbCodeArticle;
  }
  
  public int getNbCodeFamille() {
    return nbCodeFamille;
  }
  
  public int getNbCodeTarif() {
    return nbCodeTarif;
  }
  
  public int getNbRefFourniss() {
    return nbRefFourniss;
  }
  
  public boolean isMemeFournisseur() {
    return isMemeFournisseur;
  }
  
  public boolean isProposerAvente() {
    return proposerAvente;
  }
  
  public boolean isProposerAfabrication() {
    return proposerAfabrication;
  }
  
  public boolean isObligerAvente() {
    return obligerAvente;
  }
  
  public boolean isObligerAfabrication() {
    return obligerAfabrication;
  }
  
}
