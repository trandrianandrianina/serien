/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

/**
 * Fournisseur d'un article Série M
 */
public class Fournisseur {
  // collectif du forunisseur
  private String FRCOL = null;
  // code du fournisseur
  private String FRFRS = null;
  // nom du fournisseur
  private String FRNOM = null;
  
  /**
   * Constructeur par défaut
   */
  public Fournisseur(String coll, String fourni, String lib) {
    FRCOL = coll;
    FRFRS = fourni;
    FRNOM = lib;
  }
  
  public String getFRCOL() {
    return FRCOL;
  }
  
  public void setFRCOL(String fRCOL) {
    FRCOL = fRCOL;
  }
  
  public String getFRFRS() {
    return FRFRS;
  }
  
  public void setFRFRS(String fRFRS) {
    FRFRS = fRFRS;
  }
  
  public String getFRNOM() {
    return FRNOM;
  }
  
  public void setFRNOM(String fRNOM) {
    FRNOM = fRNOM;
  }
  
}
