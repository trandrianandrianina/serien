/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.util.HashMap;
import java.util.Map.Entry;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;

public class PanierArticleCommentaire extends PanierArticle {
  // Constantes
  public static final int TAILLE_MAX_ZONE = 30;
  public static final int NOMBRE_ZONES = 4;
  
  // Variables
  // private String BlocTexte = null;
  private String bllib1 = null;
  private String bllib2 = null;
  private String bllib3 = null;
  private String bllib4 = null;
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Ajoute les champs de l'article pour la future injection
   * @param pinjbdvdsl
   */
  @Override
  public void addRcd_pinjbdvdsl(GenericRecord pinjbdvdsl, HashMap<String, Object> entete, int numligne) {
    if (pinjbdvdsl == null) {
      return;
    }
    
    // On écrit les champs pour cette ligne article (notamment la clef)
    for (Entry<String, Object> entry : entete.entrySet()) {
      if (entry.getKey().startsWith("BL")) {
        pinjbdvdsl.setField(entry.getKey(), entry.getValue());
      }
    }
    
    // On écrit les infos génériques
    pinjbdvdsl.setField("BLNLI", numligne);
    pinjbdvdsl.setField("BLERL", "L");
    
    // Et le reste des informations propre à l'article
    // Ajouter ici les champs associés aux variables que vous avez rajouté à la classe
    if ((bllib1 != null) && !bllib1.trim().equals("")) {
      pinjbdvdsl.setField("BLLIB1", bllib1);
    }
    if ((bllib2 != null) && !bllib2.trim().equals("")) {
      pinjbdvdsl.setField("BLLIB2", bllib2);
    }
    if ((bllib3 != null) && !bllib3.trim().equals("")) {
      pinjbdvdsl.setField("BLLIB3", bllib3);
    }
    if ((bllib4 != null) && !bllib4.trim().equals("")) {
      pinjbdvdsl.setField("BLLIB4", bllib4);
    }
    
    // On supprime le code article (au cas où) car un article commentaire n'en a pas
    if (pinjbdvdsl.isPresentField("BLART")) {
      pinjbdvdsl.removeField("BLART");
    }
    
    Trace.debug("-addRcd_pinjbdvdsl() ->" + getBllib1() + " / " + getBllib2() + " / " + getBllib2());
  }
  
  /**
   * Initialise le champ à partir de son numéro
   * @param lib
   * @param indice
   */
  public void setBllibX(String lib, int indice) {
    switch (indice) {
      case 1:
        setBllib1(lib);
        break;
      case 2:
        setBllib2(lib);
        break;
      case 3:
        setBllib3(lib);
        break;
      case 4:
        setBllib4(lib);
        break;
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le bllib1
   */
  public String getBllib1() {
    return bllib1;
  }
  
  /**
   * @param bllib1 le bllib1 à définir
   */
  public void setBllib1(String bllib1) {
    this.bllib1 = bllib1;
  }
  
  /**
   * @return le bllib2
   */
  public String getBllib2() {
    return bllib2;
  }
  
  /**
   * @param bllib2 le bllib2 à définir
   */
  public void setBllib2(String bllib2) {
    this.bllib2 = bllib2;
  }
  
  /**
   * @return le bllib3
   */
  public String getBllib3() {
    return bllib3;
  }
  
  /**
   * @param bllib3 le bllib3 à définir
   */
  public void setBllib3(String bllib3) {
    this.bllib3 = bllib3;
  }
  
  /**
   * @return le bllib4
   */
  public String getBllib4() {
    return bllib4;
  }
  
  /**
   * @param bllib4 le bllib4 à définir
   */
  public void setBllib4(String bllib4) {
    this.bllib4 = bllib4;
  }
  
}
