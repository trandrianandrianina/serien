/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe qui liste un ensemble de paramètres propres aux établissements Série N
 */
public class Etablissement {
  private IdEtablissement idEtablissement = null;
  private UtilisateurWebshop utilisateur = null;
  private String libelleETB = null;
  // Cet établissement est il paramétré correctement pour fonctionner ?
  private boolean parametresSontDefinis = false;
  
  // Cet établissement contient t il un seul magasin ?
  private boolean is_mono_magasin = false;
  
  // limite d'affichage d'une liste sur un écran
  private int limit_liste = 15;
  // limite de recherche des requetes articles
  private int limit_requete_article = 150;
  // Le serveur autorise-til d'envoyer des mails
  private boolean ok_envois_mails = true;
  // mail de réception contact de l'ETB
  private String mail_contact = "";
  // mail de réception des commandes WS
  private String mail_commandes = "";
  // mail de réception des devis WS
  private String mail_devis = "";
  // Mail de récpetion des inscriptions client
  private String mail_inscription = "";
  
  // le code du magasin siège s'il existe
  private String magasin_siege = null;
  // la zone de référence affichée et recherchée (soit CAREF soit A1ART)
  private String zone_reference_article = "CAREF";
  // Le stock affiché est la somme du magasin siège et le magasin sélectionné
  private boolean retrait_is_siege_et_mag = false;
  // voir le tarif public dans la fiche article
  private boolean voir_prix_public = false;
  // le fournisseur doit il apparaitre dans la liste
  private boolean fourni_liste_articles = true;
  // Chemin de stockage des images
  private String chemin_images_articles = "";
  // Le type de référence article qu'il faut chercher en tant que nom d'image correspondant
  private String zone_affichage_photos = "CAREF";
  // Quel type d'extensions photo est attendue si c'est le cas
  private String extensions_photos = "JPG";
  // adresse du site public
  private String adresse_site_public = "";
  // adresse site facebook
  private String adresse_site_facebook = "";
  // NB de décimales Série N pour cet ETB
  private int decimales_client = 2;
  // NB de décimales à afficher pour cet ETB
  private int decimales_a_voir = 2;
  // Doit on voir le bloc fournisseur dans la fiche article
  private boolean voir_bloc_fournis_fiche = true;
  // Dans le cas d'une gestion multi Magasins doit on voir le stock des autres magasins (liste et fiche)
  private boolean voir_stocks_ts_mags = false;
  // voir la famille et la sous famille de l'article dans la fiche
  private boolean voir_familles_fiche = false;
  // RANG DE LA FICHE TECHNIQUE DANS LE SURAN de PSEMURLM -> LE RESTE EST DE LA PHOTO
  private int rang_fiche_technique = 1;
  // On autorise la gestion des articles favoris de l'utilisateur
  private boolean voir_favoris = true;
  // On autorise la gestion de l'historique des articles achetés par l'utilisateur
  private boolean voir_art_historique = true;
  // On autorise la gestion des articles récemment consultés par l'utilisateur
  private boolean voir_art_consulte = true;
  // La qte maximum d'articles intelligents affichés (favoris, consultés et achetés)
  private int qte_max_historique = 150;
  // Détermine si cet article est vendable ou non malgré son prix à zéro
  private boolean vend_articles_a_zero = false;
  // Cet établissement gère l'écotaxe ou non
  private boolean gestion_ecotaxe = true;
  
  // doit on voir la date de réassort en cas de rupture de stocks ?
  private boolean voir_date_reassort = false;
  // Le réassort vient il du siège
  private boolean is_reassort_siege = false;
  // Délai minimum de sécurité rajoutée lors de l'affichage d'une date de réappro prévue (en nb de jours)
  private int delai_jours_securite = 0;
  // Délai minimum de sécurité lors d'une demande de livraison dans la zone de saisie de "date souhaitée" (en nb de
  // jours)
  private int delai_mini_livraison = 0;
  // Doit on voir le stock avec sa valeur ou une icone représentative
  private int mode_stocks = MarbreEnvironnement.STOCK_AV_VALEUR;
  // Affiche t on directement les stocks ou sur demande ? ATTENTION AUX PERFORMANCES
  private boolean visu_auto_des_stocks = false;
  // Doit on filtrer la liste des articles automatiquement sur le stock positif
  private boolean filtre_stock = false;
  // Voir les articles de substitution (par défaut oui)
  private boolean voirSubstitutions = true;
  // Les variables dessous ne sont interprétées que si "voirSubstitutions" est actif
  private boolean voir_subst_rupture = true;
  private boolean voir_subst_aiguill = true;
  private boolean voir_subst_remplac = true;
  private boolean voir_subst_equival = true;
  private boolean voir_subst_variant = true;
  private int limite_liste_variantes = 10;
  // doit on voir le stock attendu dans les listes et la fiche article
  private boolean voir_stock_attendu = false;
  // Permet d'afficher le stock total de tous les magasins par défaut
  private boolean voir_stock_total = false;
  
  // 6 taux TVA de la DG. Chaque article a un taux de tva correspondant
  private BigDecimal taux_tva_1 = null;
  private BigDecimal taux_tva_2 = null;
  private BigDecimal taux_tva_3 = null;
  private BigDecimal taux_tva_4 = null;
  private BigDecimal taux_tva_5 = null;
  private BigDecimal taux_tva_6 = null;
  
  /**
   * Constructeur complet Etablissement
   */
  public Etablissement(UtilisateurWebshop pUtilisateur, IdEtablissement pIdEtablissement, String pLibelleEtablissement) {
    utilisateur = pUtilisateur;
    idEtablissement = pIdEtablissement;
    libelleETB = pLibelleEtablissement;
    
    // divers paramètres etb
    majParametresDivers();
  }
  
  /**
   * Constructeur Etablissement sans le libellé. Ce dernier sera récupéré à l'intérieur de ce constructeur
   */
  public Etablissement(UtilisateurWebshop pUtilisateur, IdEtablissement pIdEtablissement) {
    utilisateur = pUtilisateur;
    idEtablissement = pIdEtablissement;
    
    BigDecimal mille = new BigDecimal(1000);
    
    if (utilisateur != null && utilisateur.getAccesDB2() != null && idEtablissement != null) {
      // on complète le libellé
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
          .select("" + " SELECT SUBSTR(PARZ2, 1, 24) AS LIBETB, " + " substr(hex(substring(parz2 , 51, 4)), 1, 5) as TVA1, "
              + " substr(hex(substring(parz2 , 54, 4)), 1, 5) as TVA2, " + " substr(hex(substring(parz2 , 57, 4)), 1, 5) as TVA3, "
              + " substr(hex(substring(parz2 , 60, 4)), 1, 5) as TVA4, " + " substr(hex(substring(parz2 , 63, 4)), 1, 5) as TVA5, "
              + " substr(hex(substring(parz2 , 66, 4)), 1, 5) as TVA6 " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM "
              + " WHERE PARTYP = 'DG' AND PARETB = '" + idEtablissement.getCodeEtablissement() + "' ");
      if (liste != null && liste.size() > 0) {
        if (liste.get(0).isPresentField("LIBETB")) {
          libelleETB = Constantes.convertirObjetEnTexte(liste.get(0).getField("LIBETB"));
        }
        if (liste.get(0).isPresentField("TVA1")) {
          try {
            taux_tva_1 = new BigDecimal(Constantes.convertirObjetEnTexte(liste.get(0).getField("TVA1")));
            taux_tva_1 = taux_tva_1.divide(mille);
          }
          catch (Exception e) {
            taux_tva_1 = new BigDecimal(0);
          }
        }
        else {
          taux_tva_1 = new BigDecimal(0);
        }
        
        if (liste.get(0).isPresentField("TVA2")) {
          try {
            taux_tva_2 = new BigDecimal(Constantes.convertirObjetEnTexte(liste.get(0).getField("TVA2")));
            taux_tva_2 = taux_tva_2.divide(mille);
            
          }
          catch (Exception e) {
            taux_tva_2 = new BigDecimal(0);
          }
        }
        else {
          taux_tva_2 = new BigDecimal(0);
        }
        
        if (liste.get(0).isPresentField("TVA3")) {
          try {
            taux_tva_3 = new BigDecimal(Constantes.convertirObjetEnTexte(liste.get(0).getField("TVA3")));
            taux_tva_3 = taux_tva_3.divide(mille);
            
          }
          catch (Exception e) {
            taux_tva_3 = new BigDecimal(0);
          }
        }
        else {
          taux_tva_3 = new BigDecimal(0);
        }
        
        if (liste.get(0).isPresentField("TVA4")) {
          try {
            taux_tva_4 = new BigDecimal(Constantes.convertirObjetEnTexte(liste.get(0).getField("TVA4")));
            taux_tva_4 = taux_tva_4.divide(mille);
            
          }
          catch (Exception e) {
            taux_tva_4 = new BigDecimal(0);
          }
        }
        else {
          taux_tva_4 = new BigDecimal(0);
        }
        
        if (liste.get(0).isPresentField("TVA5")) {
          try {
            taux_tva_5 = new BigDecimal(Constantes.convertirObjetEnTexte(liste.get(0).getField("TVA5")));
            taux_tva_5 = taux_tva_5.divide(mille);
          }
          catch (Exception e) {
            taux_tva_5 = new BigDecimal(0);
          }
        }
        else {
          taux_tva_5 = new BigDecimal(0);
        }
        
        if (liste.get(0).isPresentField("TVA6")) {
          try {
            taux_tva_6 = new BigDecimal(Constantes.convertirObjetEnTexte(liste.get(0).getField("TVA6")));
            taux_tva_6 = taux_tva_6.divide(mille);
          }
          catch (Exception e) {
            taux_tva_6 = new BigDecimal(0);
          }
        }
        else {
          taux_tva_6 = new BigDecimal(0);
        }
      }
      
      // divers paramètres etb
      majParametresDivers();
    }
  }
  
  /**
   * Mise à jour des paramètres d'environnement propres à l'établissement
   */
  private void majParametresDivers() {
    if (idEtablissement == null || utilisateur == null || utilisateur.getAccesDB2() == null) {
      return;
    }
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select("SELECT EN_CLE,EN_VAL FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_ETB = '" + idEtablissement.getCodeEtablissement() + "' ");
    if (liste != null && liste.size() > 0) {
      parametresSontDefinis = true;
      for (int i = 0; i < liste.size(); i++) {
        if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("LIMIT_LISTE")) {
          try {
            limit_liste = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("LIMIT_REQUETE_ARTICLE")) {
          try {
            limit_requete_article = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("OK_ENVOIS_MAILS")) {
          ok_envois_mails = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("DECIMALES_A_VOIR")) {
          decimales_a_voir = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("DECIMALES_CLIENT")) {
          decimales_client = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("MAIL_CONTACT")) {
          mail_contact = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("MAIL_COMMANDES")) {
          mail_commandes = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("MAIL_DEVIS")) {
          mail_devis = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("MAIL_INSCRIPTION")) {
          mail_inscription = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("MAGASIN_SIEGE")) {
          magasin_siege = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("ZONE_REFERENCE_ARTICLE")) {
          zone_reference_article = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("RETRAIT_IS_SIEGE_ET_MAG")) {
          retrait_is_siege_et_mag = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_PRIX_PUBLIC")) {
          voir_prix_public = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("FOURNI_LISTE_ARTICLES")) {
          fourni_liste_articles = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("CHEMIN_IMAGES_ARTICLES")) {
          chemin_images_articles = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("ADRESSE_SITE_PUBLIC")) {
          adresse_site_public = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("ADRESSE_SITE_FACEBOOK")) {
          adresse_site_facebook = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("ZONE_AFFICHAGE_PHOTOS")) {
          zone_affichage_photos = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("EXTENSIONS_PHOTOS")) {
          extensions_photos = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_BLOC_FOURNIS_FICHE")) {
          voir_bloc_fournis_fiche = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_STOCKS_TS_MAGS")) {
          voir_stocks_ts_mags = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_FAMILLES_FICHE")) {
          voir_familles_fiche = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VISUSTOCKS")) {
          visu_auto_des_stocks = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("RANG_FICHE_TECHNIQUE")) {
          try {
            rang_fiche_technique = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_FAVORIS")) {
          voir_favoris = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_ART_HISTORIQUE")) {
          voir_art_historique = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_ART_CONSULTE")) {
          voir_art_consulte = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("QTE_MAX_HISTORIQUE")) {
          try {
            qte_max_historique = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("DELAI_JOURS_SECURITE")) {
          try {
            delai_jours_securite = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("DELAI_MINI_LIVRAISON")) {
          try {
            delai_mini_livraison = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("LIMITE_VARIANTE")) {
          try {
            limite_liste_variantes = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_DATE_REASSORT")) {
          voir_date_reassort = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("IS_REASSORT_SIEGE")) {
          is_reassort_siege = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VEND_ARTICLES_A_ZERO")) {
          vend_articles_a_zero = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("IS_MONO_MAGASIN")) {
          is_mono_magasin = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("GESTION_ECOTAXE")) {
          gestion_ecotaxe = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("MODE_STOCKS")) {
          try {
            mode_stocks = Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")));
          }
          catch (NumberFormatException e) {
            Trace.erreur(e.getMessage());
          }
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("AFFICHAGE_ATTENDU")) {
          voir_stock_attendu = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_STOCK_TOTAL")) {
          voir_stock_total = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_SUBSTITUT")) {
          voirSubstitutions = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("FILTRE_STOCK")) {
          filtre_stock = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_SUB_RUPT")) {
          voir_subst_rupture = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_SUB_AIGU")) {
          voir_subst_aiguill = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_SUB_REMP")) {
          voir_subst_remplac = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_SUB_EQUI")) {
          voir_subst_equival = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
        else if (Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_CLE")).equals("VOIR_SUB_VARIAN")) {
          voir_subst_variant = Constantes.convertirObjetEnTexte(liste.get(i).getField("EN_VAL")).equals("1");
        }
      }
    }
  }
  
  @Override
  public String toString() {
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Retourner de manière statique la liste complète des établissements actifs
   */
  public static ArrayList<GenericRecord> retournerListeDesEtablissementsValides(UtilisateurWebshop pUtilisateur) {
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      return null;
    }
    
    return pUtilisateur.getAccesDB2().select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".ETABLISS WHERE ETB_FM='"
        + MarbreEnvironnement.BIBLI_CLIENTS + "' AND ETB_ACTIF = '1' ORDER BY ETB_LIB ");
  }
  
  // ++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++
  
  /**
   * Retourne l'ID de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Retourne le code de l'établissement.
   */
  public String getCodeETB() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * Définie l'ID de l'établissement.
   */
  public void setIdEtablissement(IdEtablissement pIdEtablissement) {
    idEtablissement = pIdEtablissement;
  }
  
  /**
   * Retourne le libellé de l'établissement.
   */
  public String getLibelleETB() {
    return libelleETB;
  }
  
  /**
   * Définie le libellé de l'établissement.
   */
  public void setLibelleETB(String libelleETB) {
    this.libelleETB = libelleETB;
  }
  
  /**
   * Retourne l'état de paramétrage de l'établissement.
   */
  public boolean isParametresSontDefinis() {
    return parametresSontDefinis;
  }
  
  /**
   * Définie l'état de paramétrage de l'établissement.
   */
  public void setParametresSontDefinis(boolean parametresSontDefinis) {
    this.parametresSontDefinis = parametresSontDefinis;
  }
  
  /**
   * Retourne la limite d'affichage d'une liste sur l'écran.
   */
  public int getLimit_liste() {
    return limit_liste;
  }
  
  /**
   * Définie la limite d'affichage d'une liste sur l'écran.
   */
  public void setLimit_liste(int limit_liste) {
    this.limit_liste = limit_liste;
  }
  
  /**
   * Retourne la limite des résultats de recherches par requête des articles.
   */
  public int getLimit_requete_article() {
    return limit_requete_article;
  }
  
  /**
   * Définie la limite des résultats de recherches par requête des articles.
   */
  public void setLimit_requete_article(int limit_requete_article) {
    this.limit_requete_article = limit_requete_article;
  }
  
  /**
   * Retourne le droit d'autorisation du serveur pour l'envoie de mails.
   */
  public boolean isOk_envois_mails() {
    return ok_envois_mails;
  }
  
  /**
   * Définie le droit d'autorisation du serveur pour l'envoie de mails.
   */
  public void setOk_envois_mails(boolean ok_envois_mails) {
    this.ok_envois_mails = ok_envois_mails;
  }
  
  /**
   * Retourne l'adresse mail de réception du mail venant d'un contact.
   */
  public String getMail_contact() {
    return mail_contact;
  }
  
  /**
   * Définie l'adresse mail de réception du mail venant d'un contact.
   */
  public void setMail_contact(String mail_contact) {
    this.mail_contact = mail_contact;
  }
  
  /**
   * Retourne l'adresse mail de réception des commandes du Webshop.
   */
  public String getMail_commandes() {
    return mail_commandes;
  }
  
  /**
   * Définie l'adresse mail de réception des commandes du Webshop.
   */
  public void setMail_commandes(String mail_commandes) {
    this.mail_commandes = mail_commandes;
  }
  
  /**
   * Retourne l'adresse mail de réception des devis du Webshop.
   */
  public String getMail_devis() {
    return mail_devis;
  }
  
  /**
   * Définie l'adresse mail de réception des devis du Webshop.
   */
  public void setMail_devis(String mail_devis) {
    this.mail_devis = mail_devis;
  }
  
  /**
   * Retourne le code du magasin siège.
   */
  public String getMagasin_siege() {
    return magasin_siege;
  }
  
  /**
   * Définie le code du magasin siège.
   */
  public void setMagasin_siege(String magasin_siege) {
    this.magasin_siege = magasin_siege;
  }
  
  /**
   * Retourne le nombre de décimales SérieN pour cet établissement.
   */
  public int getDecimales_client() {
    return decimales_client;
  }
  
  /**
   * Définie le nombre de décimales SérieN pour cet établissement.
   */
  public void setDecimales_client(int decimales_client) {
    this.decimales_client = decimales_client;
  }
  
  /**
   * Retourne le nombre de décimales à afficher pour cet établissement.
   */
  public int getDecimales_a_voir() {
    return decimales_a_voir;
  }
  
  /**
   * Définie le nombre de décimales à afficher pour cet établissement.
   */
  public void setDecimales_a_voir(int decimales_a_voir) {
    this.decimales_a_voir = decimales_a_voir;
  }
  
  /**
   * Retourne la zone de référence affichée pour les articles.
   */
  public String getZone_reference_article() {
    return zone_reference_article;
  }
  
  /**
   * Définie la zone de référence affichée pour les articles.
   */
  public void setZone_reference_article(String zone_reference_article) {
    this.zone_reference_article = zone_reference_article;
  }
  
  /**
   * Retourne le droit d'affichage de stock comme la somme de celui du magasin siège et le magasin sélectionné.
   */
  public boolean isRetrait_is_siege_et_mag() {
    return retrait_is_siege_et_mag;
  }
  
  /**
   * Définie le droit d'affichage de stock comme la somme de celui du magasin siège et le magasin sélectionné.
   */
  public void setRetrait_is_siege_et_mag(boolean retrait_is_siege_et_mag) {
    this.retrait_is_siege_et_mag = retrait_is_siege_et_mag;
  }
  
  /**
   * Retourne le droit de voir le prix public dans la fiche article.
   */
  public boolean isVoir_prix_public() {
    return voir_prix_public;
  }
  
  /**
   * Définie le droit de voir le prix public dans la fiche article.
   */
  public void setVoir_prix_public(boolean voir_prix_public) {
    this.voir_prix_public = voir_prix_public;
  }
  
  /**
   * Retourne le droit d'afficher le fournisseur dans la fiche article.
   */
  public boolean isFourni_liste_articles() {
    return fourni_liste_articles;
  }
  
  /**
   * Définie le droit d'afficher le fournisseur dans la fiche article.
   */
  public void setFourni_liste_articles(boolean fourni_liste_articles) {
    this.fourni_liste_articles = fourni_liste_articles;
  }
  
  /**
   * Retourne le chemin des images des articles.
   */
  public String getChemin_images_articles() {
    return chemin_images_articles;
  }
  
  /**
   * Définie le chemin des images des articles.
   */
  public void setChemin_images_articles(String chemin_images_articles) {
    this.chemin_images_articles = chemin_images_articles;
  }
  
  /**
   * Retourne la zone d'affichage d'une photo.
   */
  public String getZone_affichage_photos() {
    return zone_affichage_photos;
  }
  
  /**
   * Définie la zone d'affichage d'une photo.
   */
  public void setZone_affichage_photos(String zone_affichage_photos) {
    this.zone_affichage_photos = zone_affichage_photos;
  }
  
  /**
   * Retourne l'extension de l'image attendue.
   */
  public String getExtensions_photos() {
    return extensions_photos;
  }
  
  /**
   * Définie l'extension de l'image attendue.
   */
  public void setExtensions_photos(String extensions) {
    extensions_photos = extensions;
  }
  
  /**
   * Retourne le droit de voir le bloc fournisseur dans la fiche article.
   */
  public boolean isVoir_bloc_fournis_fiche() {
    return voir_bloc_fournis_fiche;
  }
  
  /**
   * Définie le droit de voir le bloc fournisseur dans la fiche article.
   */
  public void setVoir_bloc_fournis_fiche(boolean voir_bloc_fournis_fiche) {
    this.voir_bloc_fournis_fiche = voir_bloc_fournis_fiche;
  }
  
  /**
   * Retourne le droit de voir les stocks articles de tous les magasins.
   */
  public boolean isVoir_stocks_ts_mags() {
    return voir_stocks_ts_mags;
  }
  
  /**
   * Définie le droit de voir les stocks articles de tous les magasins.
   */
  public void setVoir_stocks_ts_mags(boolean voir_stocks_ts_mags) {
    this.voir_stocks_ts_mags = voir_stocks_ts_mags;
  }
  
  /**
   * Retourne le droit de voir les familles et les sous familles de l'article dans sa fiche.
   */
  public boolean isVoir_familles_fiche() {
    return voir_familles_fiche;
  }
  
  /**
   * Définie le droit de voir les familles et les sous familles de l'article dans sa fiche.
   */
  public void setVoir_familles_fiche(boolean voir_familles_fiche) {
    this.voir_familles_fiche = voir_familles_fiche;
  }
  
  /**
   * Retourne le rang de la fiche technique.
   */
  public int getRang_fiche_technique() {
    return rang_fiche_technique;
  }
  
  /**
   * Définie le rang de la fiche technique.
   */
  public void setRang_fiche_technique(int rang_fiche_technique) {
    this.rang_fiche_technique = rang_fiche_technique;
  }
  
  /**
   * Retourne le droit de gérer les articles favories par l'utilisateur.
   */
  public boolean isVoir_favoris() {
    return voir_favoris;
  }
  
  /**
   * Définie le droit de gérer les articles favories par l'utilisateur.
   */
  public void setVoir_favoris(boolean voir_favoris) {
    this.voir_favoris = voir_favoris;
  }
  
  /**
   * Retourne le droit de gérer l'historique des articles achetés par l'utilisateur.
   */
  public boolean isVoir_art_historique() {
    return voir_art_historique;
  }
  
  /**
   * Définie le droit de gérer l'historique des articles achetés par l'utilisateur.
   */
  public void setVoir_art_historique(boolean voir_art_historique) {
    this.voir_art_historique = voir_art_historique;
  }
  
  /**
   * Retourne le droit de gérer les articles récemment consultés par l'utilisateur.
   */
  public boolean isVoir_art_consulte() {
    return voir_art_consulte;
  }
  
  /**
   * Définie le droit de gérer les articles récemment consultés par l'utilisateur.
   */
  public void setVoir_art_consulte(boolean voir_art_consulte) {
    this.voir_art_consulte = voir_art_consulte;
  }
  
  /**
   * Retourne la quantité maximum d'articles affichés.
   */
  public int getQte_max_historique() {
    return qte_max_historique;
  }
  
  /**
   * Définie la quantité maximum d'articles affichés.
   */
  public void setQte_max_historique(int qte_max) {
    qte_max_historique = qte_max;
  }
  
  /**
   * Retourne le droit de voir la date de réassort.
   */
  public boolean isVoir_date_reassort() {
    return voir_date_reassort;
  }
  
  /**
   * Définie le droit de voir la date de réassort.
   */
  public void setVoir_date_reassort(boolean voir_date_reassort) {
    this.voir_date_reassort = voir_date_reassort;
  }
  
  /**
   * Retourne l'état du réassort s'il vient du siège.
   */
  public boolean isIs_reassort_siege() {
    return is_reassort_siege;
  }
  
  /**
   * Définie l'état du réassort s'il vient du siège.
   */
  public void setIs_reassort_siege(boolean is_reassort_siege) {
    this.is_reassort_siege = is_reassort_siege;
  }
  
  /**
   * Retoune le délais minimum de sécurité pour une date de réapprovisionnement prévue.
   */
  public int getDelai_jours_securite() {
    return delai_jours_securite;
  }
  
  /**
   * Définie le délais minimum de sécurité pour une date de réapprovisionnement prévue.
   */
  public void setDelai_jours_securite(int delai_jours_securite) {
    this.delai_jours_securite = delai_jours_securite;
  }
  
  /**
   * Retoune le délais minimum de sécurité lors d'une demande de livraison.
   */
  public int getDelai_mini_livraison() {
    return delai_mini_livraison;
  }
  
  /**
   * Définie le délais minimum de sécurité lors d'une demande de livraison.
   */
  public void setDelai_mini_livraison(int delai_mini_livraison) {
    this.delai_mini_livraison = delai_mini_livraison;
  }
  
  /**
   * Retoune le droit de vendre l'article malgré son prix à zéro.
   */
  public boolean isVend_articles_a_zero() {
    return vend_articles_a_zero;
  }
  
  /**
   * Définie le droit de vendre l'article malgré son prix à zéro.
   */
  public void setVend_articles_a_zero(boolean vend_articles_a_zero) {
    this.vend_articles_a_zero = vend_articles_a_zero;
  }
  
  /**
   * Retoune le mode d'affichage de stock d'un article (par icône ou par valeur).
   */
  public int getMode_stocks() {
    return mode_stocks;
  }
  
  /**
   * Définie le mode d'affichage de stock d'un article.
   */
  public void setMode_stocks(int mode_stocks) {
    this.mode_stocks = mode_stocks;
  }
  
  /**
   * Retoune le droit d'afficher automatiquement les stocks.
   */
  public boolean isVisu_auto_des_stocks() {
    return visu_auto_des_stocks;
  }
  
  /**
   * Définie le droit d'afficher automatiquement les stocks.
   */
  public void setVisu_auto_des_stocks(boolean visu_auto_des_stocks) {
    this.visu_auto_des_stocks = visu_auto_des_stocks;
  }
  
  /**
   * Retoune l'état d'un établissement s'il n'a qu'un seul magasin.
   */
  public boolean is_mono_magasin() {
    return is_mono_magasin;
  }
  
  /**
   * Définie l'état d'un établissement s'il n'a qu'un seul magasin.
   */
  public void setIs_mono_magasin(boolean is_mono_magasin) {
    this.is_mono_magasin = is_mono_magasin;
  }
  
  /**
   * Retourne le droit de gérer l'écotaxe par l'établissement.
   */
  public boolean isGestion_ecotaxe() {
    return gestion_ecotaxe;
  }
  
  /**
   * Définie le droit de gérer l'écotaxe par l'établissement.
   */
  public void setGestion_ecotaxe(boolean gestion_ecotaxe) {
    this.gestion_ecotaxe = gestion_ecotaxe;
  }
  
  /**
   * Retoune l'adresse du site public.
   */
  public String getAdresse_site_public() {
    return adresse_site_public;
  }
  
  /**
   * Définie l'adresse du site public.
   */
  public void setAdresse_site_public(String adresse_site_public) {
    this.adresse_site_public = adresse_site_public;
  }
  
  /**
   * Retoune l'adresse d'un site facebook.
   */
  public String getAdresse_site_facebook() {
    return adresse_site_facebook;
  }
  
  /**
   * Définie l'adresse d'un site facebook.
   */
  public void setAdresse_site_facebook(String adresse_site_facebook) {
    this.adresse_site_facebook = adresse_site_facebook;
  }
  
  /**
   * Retoune le taux TVA correspondant au code TVA 1.
   */
  public BigDecimal getTaux_tva_1() {
    return taux_tva_1;
  }
  
  /**
   * Retoune le taux TVA correspondant au code TVA 2.
   */
  public BigDecimal getTaux_tva_2() {
    return taux_tva_2;
  }
  
  /**
   * Retoune le taux TVA correspondant au code TVA 3.
   */
  public BigDecimal getTaux_tva_3() {
    return taux_tva_3;
  }
  
  /**
   * Retoune le taux TVA correspondant au code TVA 4.
   */
  public BigDecimal getTaux_tva_4() {
    return taux_tva_4;
  }
  
  /**
   * Retoune le taux TVA correspondant au code TVA 5.
   */
  public BigDecimal getTaux_tva_5() {
    return taux_tva_5;
  }
  
  /**
   * Retoune le taux TVA correspondant au code TVA 6.
   */
  public BigDecimal getTaux_tva_6() {
    return taux_tva_6;
  }
  
  /**
   * Retoune le droit de voir le stock attendu.
   */
  public boolean isVoir_stock_attendu() {
    return voir_stock_attendu;
  }
  
  /**
   * Retoune le droit d'afficher le stock total de tous les magagsins.
   */
  public boolean voir_stock_total() {
    return voir_stock_total;
  }
  
  /**
   * Retoune le droit de voir les articles de substitution.
   */
  public boolean voirSubstitutions() {
    return voirSubstitutions;
  }
  
  /**
   * Retoune le droit de filtrer la liste des articles sur le stock positif.
   */
  public boolean is_filtre_stock() {
    return filtre_stock;
  }
  
  /**
   * Définie le droit de filtrer la liste des articles sur le stock positif.
   */
  public void setFiltre_stock(boolean pFiltre) {
    this.filtre_stock = pFiltre;
  }
  
  /**
   * Retoune le droit d'afficher les articles en rupture.
   */
  public boolean voir_Subst_Rupture() {
    return voir_subst_rupture;
  }
  
  /**
   * Retoune le droit d'afficher les articles en aiguillage.
   */
  public boolean voir_Subst_Aiguill() {
    return voir_subst_aiguill;
  }
  
  /**
   * Retoune le droit d'afficher les articles de remplacement.
   */
  public boolean voir_Subst_Remplac() {
    return voir_subst_remplac;
  }
  
  /**
   * Retoune le droit d'afficher les articles équivalents.
   */
  public boolean voir_Subst_Equival() {
    return voir_subst_equival;
  }
  
  /**
   * Retoune le mail de réception des inscriptions de clients.
   */
  public String getMail_inscription() {
    return mail_inscription;
  }
  
  /**
   * Définie le mail de réception des inscriptions de clients.
   */
  public void setMail_inscription(String mail_inscription) {
    this.mail_inscription = mail_inscription;
  }
  
  /**
   * Retoune le droit de voir les variantes de l'article.
   */
  public boolean voir_Subst_Variante() {
    return voir_subst_variant;
  }
  
  /**
   * Retoune la limite de la liste de variante de l'article.
   */
  public int getLimite_liste_variantes() {
    return limite_liste_variantes;
  }
}
