/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import ri.serien.webshop.environnement.UtilisateurWebshop;

public class Filtres {
  
  private boolean stock_positif = false;
  private UtilisateurWebshop utilisateur = null;
  
  public Filtres(UtilisateurWebshop pUtil) {
    utilisateur = pUtil;
  }
  
  public void init() {
    if (utilisateur == null || utilisateur.getEtablissementEnCours() == null) {
      return;
    }
    
    stock_positif = utilisateur.getEtablissementEnCours().is_filtre_stock();
  }
  
  public boolean isStock_positif() {
    return stock_positif;
  }
  
  public void setStock_positif(boolean stock_positif) {
    this.stock_positif = stock_positif;
  }
}
