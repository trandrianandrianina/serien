/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.AlmostQtemp;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.TableBDD;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.modeles.InjecteurBdVetF;
import ri.serien.webshop.outils.Outils;

/**
 * Cette classe représente le panier de l'utilisateur ainsi que tous ses
 * attributs
 */
public class Panier {
  // Constantes
  public static final int NOMBRE_MAX_LIGNES_COMMENTAIRES = 5;
  public static final int TAILLE_MAX_LIGNE_COMMENTAIRE = PanierArticleCommentaire.TAILLE_MAX_ZONE * PanierArticleCommentaire.NOMBRE_ZONES;
  public static final String CODE_COMMENTAIRE = "$#$#COM%d";
  
  // Variables
  private int idPanier = 0;
  // private LinkedHashMap<String, PanierArticle> listeArticles = new LinkedHashMap<String, PanierArticle>();
  private ArrayList<PanierArticle> listePanierArticle = new ArrayList<PanierArticle>();
  private BigDecimal totalHT = BigDecimal.ZERO;
  private BigDecimal totalEcotaxe = BigDecimal.ZERO;
  private UtilisateurWebshop utilisateur = null;
  private Magasin magasinPanier = null;
  // Modes de récupération du panier 1 ENLEVEMENT 2 LIVRAISON 3 DEVIS (Voir MarbreEnvironnement
  private int modeRecup = MarbreEnvironnement.MODE_NON_CHOISI;
  private String codeVendeur = null;
  // date de création du panier
  private int datePanier = 0;
  // date saisie par l'utililsateur quant à la date souhaitée
  private int dateRecuperation = 0;
  private String numeroCommande = null;
  // bloc notes
  private String detailsRecuperation = null;
  // Référence longue du bon de commande
  private String E1RCC = null;
  // Zones de livraison BLOC ADRESSE
  private String CLNOM = null;
  private String CLCPL = null;
  private String CLRUE = null;
  private String CLLOC = null;
  private String CLVIL = null;
  private String CLPOS = null;
  
  protected String msgErreur = "";
  
  /**
   * Constructeur
   * 
   * @param utilisateur
   */
  public Panier(UtilisateurWebshop utilisateur) {
    
    this.utilisateur = utilisateur;
    // on attribue par défaut le magasin de l'utilisateur au panier
    if (this.utilisateur != null) {
      // on checke si l'utilisateur a pas un panier en cours qui traine
      if (utilisateur.getAccesDB2() != null) {
        ArrayList<GenericRecord> liste = null;
        
        liste = utilisateur.getAccesDB2().select(
            "SELECT PA_ID, PA_US, PA_DTC, PA_DTS, PA_MOD, PA_NOM, PA_CPL, PA_RUE, PA_LOC, PA_VIL, PA_CPO, PA_INF, PA_RCC, PARIND AS MAG "
                + "FROM " + MarbreEnvironnement.BIBLI_WS + "." + TableBDD.TABLE_WS_PANIER + " LEFT JOIN "
                + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_PARAMETRES_GVM + " ON PA_MAG = PARIND "
                + "WHERE PA_ETA = '0' AND PARTYP = 'MA' AND PA_US = '" + utilisateur.getIdentifiant() + "' "
                + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
        
        // Si on a déjà un panier qui trainouille par là
        if (liste != null && liste.size() == 1) {
          if (isPanierGarni(liste.get(0))) {
            regenererUnPanier(liste.get(0));
          }
          else {
            videPanier(true);
            creerNouveauPanierDB2();
          }
        }
        // On va créer un panier vide DB2
        else if (liste != null && liste.size() == 0) {
          creerNouveauPanierDB2();
        }
      }
      
      codeVendeur = MarbreEnvironnement.CODE_VENDEUR_WS;
    }
  }
  
  /**
   * Régénerer le panier de l'utilisateur s'il existe dans DB2 et que son état est non validé = 0
   */
  private int regenererUnPanier(GenericRecord record) {
    if (record == null) {
      return 0;
    }
    
    try {
      if (record.isPresentField("PA_ID")) {
        idPanier = Integer.parseInt(record.getField("PA_ID").toString().trim());
      }
      if (record.isPresentField("PA_DTC")) {
        datePanier = Integer.parseInt(record.getField("PA_DTC").toString().trim());
      }
      if (record.isPresentField("PA_DTS")) {
        dateRecuperation = Integer.parseInt(record.getField("PA_DTS").toString().trim());
      }
      if (record.isPresentField("PA_MOD")) {
        modeRecup = Integer.parseInt(record.getField("PA_MOD").toString().trim());
      }
      // zones bloc adresses propres à la livraison
      if (modeRecup == MarbreEnvironnement.MODE_LIVRAISON) {
        if (record.isPresentField("PA_NOM")) {
          CLNOM = record.getField("PA_NOM").toString().trim();
        }
        else {
          CLNOM = "";
        }
        if (record.isPresentField("PA_CPL")) {
          CLCPL = record.getField("PA_CPL").toString().trim();
        }
        else {
          CLCPL = "";
        }
        if (record.isPresentField("PA_RUE")) {
          CLRUE = record.getField("PA_RUE").toString().trim();
        }
        else {
          CLRUE = "";
        }
        if (record.isPresentField("PA_LOC")) {
          CLLOC = record.getField("PA_LOC").toString().trim();
        }
        else {
          CLLOC = "";
        }
        if (record.isPresentField("PA_VIL")) {
          CLVIL = record.getField("PA_VIL").toString().trim();
        }
        else {
          CLVIL = "";
        }
        if (record.isPresentField("PA_CPO")) {
          CLPOS = record.getField("PA_CPO").toString().trim();
        }
        else {
          CLPOS = "";
        }
      }
      
      if (record.isPresentField("PA_INF")) {
        detailsRecuperation = record.getField("PA_INF").toString().trim();
      }
      else {
        detailsRecuperation = "";
      }
      
      if (record.isPresentField("PA_RCC")) {
        E1RCC = record.getField("PA_RCC").toString().trim();
      }
      else {
        E1RCC = "";
      }
      
      // rajout de commentaire
      if (record.isPresentField("MAG")) {
        magasinPanier = new Magasin(utilisateur, record.getField("MAG").toString().trim());
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
    
    // On liste les articles liés au panier et on les rajoute au panier
    if (idPanier > 0) {
      ArrayList<GenericRecord> liste = null;
      liste = utilisateur.getAccesDB2().select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + idPanier
          + "'" + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      // si il existe bien des articles dans ce panier
      if (liste != null && liste.size() > 0) {
        for (int i = 0; i < liste.size(); i++) {
          ajouterArticle(liste.get(i).getField("ART_ETB").toString().trim(), liste.get(i).getField("ART_ID").toString().trim(),
              liste.get(i).getField("ART_REF").toString().trim(), new BigDecimal(liste.get(i).getField("ART_QTE").toString().trim()),
              new BigDecimal(liste.get(i).getField("ART_TAR").toString().trim()), liste.get(i).getField("ART_CDT").toString().trim(),
              liste.get(i).getField("ART_UNI").toString().trim(), "1", new BigDecimal(liste.get(i).getField("ART_QTE").toString().trim()),
              false);
        }
      }
    }
    
    return idPanier;
  }
  
  // -- Méthodes publiques --------------------------------------------------
  /**
   * Validation de la commande (contrôle + injection)
   * 
   * @param berbv
   * @param rapport
   * @return
   */
  // si on utilise d'autres zones du PINJBDVDSE et PINJBDVDSL
  public boolean valideCommande(String typeBon, String berbv, LinkedHashMap<String, String> rapport) {
    if (utilisateur == null) {
      msgErreur += "La classe Utilisateur est à null.\n";
      return false;
    }
    
    // Création de la "QTEMP" à partir de l'ID utilisateur ici valeur aléatoire 123
    AlmostQtemp qtemp = new AlmostQtemp(utilisateur.getSysteme().getSystem(), utilisateur.getIdentifiant());
    qtemp.create();
    
    InjecteurBdVetF sgvmiv =
        new InjecteurBdVetF(utilisateur.getSysteme(), qtemp.getName(), utilisateur.getBibli(), MarbreEnvironnement.LETTRE_ENV);
    boolean retour = sgvmiv.init();
    if (!retour) {
      msgErreur += "Initialisation de InjecteurBdVetF en échec.\n";
      return retour;
    }
    
    sgvmiv.setGestionComplementEntete(true);
    // Nettoyage des fichiers physiques PINJBDV*
    retour = sgvmiv.clearAndCreateAllFilesInWrkLib();
    if (!retour) {
      msgErreur += "Nettoyage et/ou création fichiers pour l'injection en erreur.\n";
      return retour;
    }
    
    // Début - Améliorations possibles
    ArrayList<GenericRecord> listeRcdArticles = new ArrayList<GenericRecord>();
    
    if (berbv == null) {
      berbv = "";
    }
    if (typeBon == null) {
      typeBon = "ATT"; // Pour un bon mis en attente
    }
    // // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ ENTETE BON
    listeRcdArticles.add(creerLigneEnteteBon(typeBon, berbv));
    
    GenericRecord enteteBonComplementaire = creerLigneEnteteBonComplementaire(sgvmiv, berbv);
    if (enteteBonComplementaire != null) {
      listeRcdArticles.add(enteteBonComplementaire);
    }
    
    // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ LIGNES ARTICLES
    // Préparation de la clef pour les lignes du futur bon
    HashMap<String, Object> clefLigne = preparerClefFuturBon(berbv);
    
    // On traite les observations: on les transforme en PanierArticleCommentaire que l'on insère en tête du panier
    traitementDetailsRecuperation();
    
    // Préparation des genericrecord pour chaque articles différents du panier
    int numligne = 1;
    for (PanierArticle panierArticle : listePanierArticle) {
      GenericRecord article = new GenericRecord();
      panierArticle.addRcd_pinjbdvdsl(article, clefLigne, numligne++);
      listeRcdArticles.add(article);
    }
    
    // On insère les enregistrements dans les PINJBDVDSE et PINJBDVDSL
    retour = sgvmiv.insertRecordOld(listeRcdArticles);
    if (!retour) {
      msgErreur += "Insertion en erreur:\n" + sgvmiv.getMsgError() + '\n';
      return retour;
    }
    
    // On injecte le PINJBDV avec le rapport mis à jour: soit avec des erreurs soit avec les infos du bon créé
    retour = sgvmiv.injectFileOld(rapport);
    // Si on a un souci
    if (!retour) {
      msgErreur += "Injection en erreur (lire le rapport pour le détail):\n" + sgvmiv.getMsgError() + '\n';
    }
    else {
      // on change l'état du panier dans DB2
      sauverPanier("PA_ETA", "1");
      // On réinitialise le panier car tout c'est bien passé
      videPanier(false);
    }
    // On supprime la bibliothèque temporaire si est pas en mode DEBUG
    if (MarbreEnvironnement.MODE_DEBUG) {
      Trace.debug("[valideCommande()] Je ne supprime pas la qtemp: " + qtemp.getName());
    }
    else {
      qtemp.delete();
    }
    // Les fichiers PINJBDVDSE et PINJBDVDSL sont dans des bibli commencant par TMP chercher les TMP* dans WRKLIB
    // A ne faire qu'en toute fin (à la fin de la session utilisateur - ça permet d'éviter de la recréer/supprimer à
    // chaque injection) A commenter si vous voulez voir le fichier FCHLOG avec le 5250 de la dernière injection
    return retour;
  }
  
  /**
   * Prépare la cléf pour les lignes du futur bon
   */
  private HashMap<String, Object> preparerClefFuturBon(String pBerbv) {
    HashMap<String, Object> clefLigne = new HashMap<String, Object>();
    clefLigne.put("BLETB", utilisateur.getEtablissementEnCours().getCodeETB());
    
    // Si mode livraison, on passe le magasin siége - Si mode retrait, on passe le magasin du panier
    if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON
        || utilisateur.getEtablissementEnCours().is_mono_magasin()) {
      clefLigne.put("BLMAGE", utilisateur.getMagasinSiege().getCodeMagasin());
    }
    else {
      clefLigne.put("BLMAGE", magasinPanier.getCodeMagasin());
    }
    
    clefLigne.put("BLDAT", Integer.parseInt(retournerDatePanier()));
    clefLigne.put("BLRBV", pBerbv);
    
    return clefLigne;
  }
  
  /**
   * Crée l'entête du Bon
   */
  private GenericRecord creerLigneEnteteBon(String typeBon, String berbv) {
    // Création de la ligne d'entête du bon
    GenericRecord enteteBon = new GenericRecord();
    
    // Type le mode d'expédition
    String BEMEX = gererModeExpedition();
    enteteBon.setField("BEMEX", BEMEX);
    enteteBon.setField("BEETB", utilisateur.getEtablissementEnCours().getCodeETB());
    enteteBon.setField("BEMAG", retournerMagasin());
    enteteBon.setField("BEDAT", retournerDatePanier());
    enteteBon.setField("BERBV", berbv);
    enteteBon.setField("BEERL", "E");
    enteteBon.setField("BENLI", 0);
    enteteBon.setField("BENCLP", utilisateur.getClient().getNumeroClient());
    enteteBon.setField("BEVDE", codeVendeur);
    
    // Adresse de livraison
    if (modeRecup == MarbreEnvironnement.MODE_LIVRAISON) {
      enteteBon.setField("BENOML", CLNOM);
      enteteBon.setField("BECPLL", CLCPL);
      enteteBon.setField("BERUEL", CLRUE);
      enteteBon.setField("BELOCL", CLLOC);
      
      // On compléte le code postal par des blanc si null ou inférieur à 5 caractéres
      String codePostal = gererCodePostal();
      enteteBon.setField("BECDPL", codePostal);
      enteteBon.setField("BEVILL", CLVIL);
    }
    
    // Référence longue
    if (E1RCC != null && !E1RCC.trim().equals("")) {
      enteteBon.setField("BERCC", E1RCC);
    }
    
    // si commande SerieM renseigné, alors on vient de modifier un commande. On passe l'ancien n° pour annulation dans le SGVMIV
    if (numeroCommande != null) {
      deverouilleCommande(numeroCommande);
      
      if ((numeroCommande.startsWith("E") || numeroCommande.startsWith("D")) && numeroCommande.length() > 1) {
        enteteBon.setField("BETRT", "A");
        enteteBon.setField("BEBON", numeroCommande.substring(1));
      }
    }
    enteteBon.setField("BEOPT", typeBon);
    // DEV pour devis ou " " pour mise en attente (voir QDDSFCH de PINJBDVDSE pour plus de contexte)
    
    dateRecuperation = gererDateRecuperation();
    
    // On vire le siècle pour l'injecteur
    if (String.valueOf(dateRecuperation).length() == 7) {
      enteteBon.setField("BEDLS", String.valueOf(dateRecuperation).substring(1, 7));
    }
    
    return enteteBon;
  }
  
  /**
   * Gère le code postal
   */
  private String gererCodePostal() {
    String codePostal = null;
    if (CLPOS == null || CLPOS == "") {
      codePostal = "00000 ";
    }
    else if (CLPOS.length() < 5) {
      codePostal = String.format("%-6s", CLPOS);
    }
    else if (CLPOS.length() == 5) {
      codePostal = CLPOS + " ";
    }
    
    return codePostal;
  }
  
  /**
   * Gère le mode d'expédition
   */
  private String gererModeExpedition() {
    String modeExpedition = null;
    if (modeRecup == MarbreEnvironnement.MODE_RETRAIT) {
      modeExpedition = MarbreEnvironnement.EXP_RETRAIT;
    }
    else if (modeRecup == MarbreEnvironnement.MODE_LIVRAISON) {
      modeExpedition = MarbreEnvironnement.EXP_LIVRAI;
    }
    else {
      modeExpedition = MarbreEnvironnement.EXP_RETRAIT;
    }
    
    return modeExpedition;
  }
  
  /**
   * Gère la date de récuperation
   */
  private int gererDateRecuperation() {
    // Pas de date inférieure à la date du jour
    if (modeRecup == MarbreEnvironnement.MODE_RETRAIT) {
      if (Outils.recupererDateCouranteInt() > dateRecuperation) {
        dateRecuperation = Outils.recupererDateCouranteInt();
      }
    }
    else if (modeRecup == MarbreEnvironnement.MODE_LIVRAISON) {
      if (Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
          utilisateur.getEtablissementEnCours().getDelai_mini_livraison())) > dateRecuperation) {
        dateRecuperation = Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
            utilisateur.getEtablissementEnCours().getDelai_mini_livraison()));
      }
    }
    else {
      if (Outils.recupererDateCouranteInt() > dateRecuperation) {
        dateRecuperation = Outils.recupererDateCouranteInt();
      }
    }
    
    return dateRecuperation;
  }
  
  /**
   * Crée le complément de l'entête du Bon si c'est gérable
   */
  private GenericRecord creerLigneEnteteBonComplementaire(InjecteurBdVetF pSgvmiv, String berbv) {
    GenericRecord enteteBonComplementaire = null;
    if (pSgvmiv.isGestionComplementEntete()) {
      // Création de la ligne d'entête du bon complémentaire
      enteteBonComplementaire = new GenericRecord();
      
      enteteBonComplementaire.setField("BFETB", utilisateur.getEtablissementEnCours().getCodeETB());
      enteteBonComplementaire.setField("BFMAG", retournerMagasin());
      enteteBonComplementaire.setField("BFDAT", retournerDatePanier());
      enteteBonComplementaire.setField("BFRBV", berbv);
      enteteBonComplementaire.setField("BFERL", "F");
      enteteBonComplementaire.setField("BFNLI", 0);
      // Numéro de contact
      if (utilisateur.getIdContact() != null) {
        enteteBonComplementaire.setField("BFCNUM", utilisateur.getIdContact());
      }
    }
    
    return enteteBonComplementaire;
  }
  
  /**
   * Retourne le code magasin selon le mode de livraison
   */
  private String retournerMagasin() {
    String magasin = "";
    // Si mode livraison ou mono magasin, on passe le magasin siége - Si mode retrait, on passe le magasin du panier
    if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON
        || utilisateur.getEtablissementEnCours().is_mono_magasin()) {
      magasin = utilisateur.getMagasinSiege().getCodeMagasin();
    }
    else {
      magasin = magasinPanier.getCodeMagasin();
    }
    
    return magasin;
  }
  
  /**
   * Retourne la date du Panier
   */
  private String retournerDatePanier() {
    String datePanierPINJ = "";
    // attention aux nouvelles et anciennes dates de Série M
    if (String.valueOf(datePanier).length() == 7) {
      datePanierPINJ = String.valueOf(datePanier).substring(1, 7);
    }
    
    return datePanierPINJ;
  }
  
  /**
   * Retourne le nombre d'articles du panier
   * 
   * @return
   */
  public int getNombreArticle() {
    return listePanierArticle.size();
  }
  
  /**
   * On supprime un article du panier
   * 
   * @param a1art
   * @return
   */
  public boolean supprimeArticle(String a1etb, String a1art, String quantite) {
    if ((a1art == null) || (a1art.trim().equals("")) || listePanierArticle.isEmpty()) {
      return false;
    }
    
    if (supprimerUnArticleDuPanier(a1etb, a1art, quantite)) {
      if (supprimeArticleDB2(a1etb, a1art, quantite)) {
        // On recalcule le total du panier
        calulerTotalHT();
        return true;
      }
    }
    return false;
  }
  
  /**
   * Supprimer un article de la liste des articles
   */
  private boolean supprimerUnArticleDuPanier(String pEtb, String pCode, String quant) {
    if (pEtb == null || pCode == null || quant == null || listePanierArticle == null) {
      return false;
    }
    
    int i = 0;
    BigDecimal quantite = null;
    try {
      quantite = new BigDecimal(quant);
    }
    catch (Exception e) {
      return false;
    }
    for (PanierArticle article : listePanierArticle) {
      if (article instanceof PanierArticleNormal) {
        if (article.getA1etb().equals(pEtb) && article.getA1art().equals(pCode)
            && (((PanierArticleNormal) article).getQuantite().compareTo(quantite) == 0)) {
          listePanierArticle.remove(i);
          return true;
        }
      }
      i++;
    }
    
    return false;
  }
  
  /**
   * Supprimer un article de la base de DB2
   */
  private boolean supprimeArticleDB2(String a1etb, String a1art, String pQuantite) {
    if (numeroCommande != null) {
      return true;
    }
    if (a1art == null || a1etb == null || pQuantite == null || utilisateur == null || utilisateur.getAccesDB2() == null) {
      return false;
    }
    
    return (utilisateur.getAccesDB2()
        .requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + idPanier + "' AND TRIM(ART_ID) = '"
            + a1art.trim() + "' AND TRIM(ART_ETB) = '" + a1etb.trim() + "' AND ART_QTE = '" + pQuantite.trim() + "' ") > 0);
  }
  
  /**
   * Ajoute un article au panier en mode reprise de commande
   * 
   * @param pEtablissement
   * @param pArticle
   * @param pQuantite
   * @param pTarifHTUnitaire
   * @param pConditionnement
   * @param pUniteArticle
   * @param pType
   * @return
   */
  public boolean ajouterArticle(String pEtablissement, String pArticle, BigDecimal pQuantite, BigDecimal pTarifHTUnitaire,
      String pConditionnement, String pUniteArticle, String pType) {
    if ((pArticle == null) || (pArticle.trim().equals(""))) {
      return false;
    }
    
    PanierArticleNormal article = new PanierArticleNormal(utilisateur);
    article.setA1etb(pEtablissement);
    article.setA1art(pArticle);
    article.setUniteArt(pUniteArticle);
    article.setTypeArticle(pType);
    article.setQuantite(pQuantite);
    article.setTarifHTUnitaire(pTarifHTUnitaire);
    article.setConditionnM(pConditionnement);
    article.setLot(true);
    
    return ajouterArticle(article, true, null);
  }
  
  /**
   * Ajouter un article au panier.
   * 
   * @param pCodeEtablissement Code établissement.
   * @param pCodeArticle Code article.
   * @param pReferenceFournisseurArticle Référence fournisseur de l'article.
   * @param pQuantiteArticle Quantité à venre.
   * @param pTarifUnitaireHT Tarif unitaire HT.
   * @param pConditionnement Conditionnement.
   * @param pUniteVente Unité de vente.
   * @param pIsLot true=en lot, false=sinon.
   * @param pAncienneQuantite Ancienne valeur de la quantité avant modification.
   * @param pSauvegardeBDD true=sauver le nouvel article dans la bdd, false=ne pas sauver le nouvel article en bdd.
   * @return true=ok, false=erreur.
   */
  public boolean ajouterArticle(String pCodeEtablissement, String pCodeArticle, String pReferenceFournisseurArticle,
      BigDecimal pQuantiteArticle, BigDecimal pTarifUnitaireHT, String pConditionnement, String pUniteVente, String pIsLot,
      BigDecimal pAncienneQuantite, boolean pSauvegardeBDD) {
    // Contrôler les paramètres
    if (pCodeArticle == null || pCodeArticle.trim().equals("")) {
      throw new MessageErreurException("Impossible d'ajouter l'article au panier car l'article est invalide");
    }
    
    // Créer un article pour le panier
    PanierArticleNormal panierArticleNormal = new PanierArticleNormal(utilisateur);
    panierArticleNormal.setA1etb(pCodeEtablissement);
    panierArticleNormal.setA1art(pCodeArticle);
    panierArticleNormal.setRefFournisseur(pReferenceFournisseurArticle);
    panierArticleNormal.setUniteArt(pUniteVente);
    panierArticleNormal.setQuantite(pQuantiteArticle);
    panierArticleNormal.setTarifHTUnitaire(pTarifUnitaireHT);
    panierArticleNormal.setConditionnM(pConditionnement);
    if (pIsLot != null && pIsLot.trim().equals("1")) {
      panierArticleNormal.setLot(true);
    }
    
    // Ajouter l'article au panier
    return ajouterArticle(panierArticleNormal, pSauvegardeBDD, pAncienneQuantite);
  }
  
  /**
   * Ajouter un article au panier..
   * 
   * @param pPanierArticleNormal Article à ajouter.
   * @param pSauvegardeBDD true=sauver le nouvel article dans la bdd, false=ne pas sauver le nouvel article en bdd.
   * @param pAncienneQuantite Ancienne valeur de la quantité avant modification.
   * @return true=ok, false=erreur.
   */
  private boolean ajouterArticle(PanierArticleNormal pPanierArticleNormal, boolean pSauvegardeBDD, BigDecimal pAncienneQuantite) {
    // Contrôler les paramètres
    if (pPanierArticleNormal == null) {
      return false;
    }
    
    // Vérifier si l'article est déjà présent dans le panier
    boolean existe = existeDansListeArticles(pPanierArticleNormal.getA1etb(), pPanierArticleNormal.getA1art());
    
    // Ajouter l'article s'il n'est pas déjà présent ou si c'est un article loti
    if (!existe || pPanierArticleNormal.isLot()) {
      // Calculer le total de la ligne
      pPanierArticleNormal.calculerTotalLigne();
      
      // Sauvegarder l'article dans la base de données
      if (pSauvegardeBDD) {
        sauverArticlePanier(pPanierArticleNormal);
      }
      
      // Ajouter l'article au panier
      listePanierArticle.add(pPanierArticleNormal);
      
      // Calculer le total HT du panier
      calulerTotalHT();
    }
    else {
      // Ajouter la quantité nouvelle saisie à l'ancienne si l'article existe déjà
      modifierArticle(pPanierArticleNormal, pAncienneQuantite);
    }
    
    return true;
  }
  
  /**
   * Modifie la quantité d'un article (on l'écrase par la nouvelle valeur)
   * @param a1art
   * @param qte
   * @return
   */
  private boolean modifierArticle(PanierArticleNormal pNouvelArticle, BigDecimal ancienneQte) {
    if (pNouvelArticle == null || listePanierArticle.isEmpty()) {
      return false;
    }
    
    // On modifie la quantité de l'article
    PanierArticleNormal articleExistant = retournerArticleListe(pNouvelArticle, ancienneQte);
    if (articleExistant == null) {
      return false;
    }
    articleExistant.setQuantite(articleExistant.getQuantite().add(pNouvelArticle.getQuantite()));
    articleExistant.calculerTotalLigne();
    modificationLignesArticlesDB2();
    // On recalcule le total du panier
    calulerTotalHT();
    return true;
  }
  
  /**
   * Modifie la quantité d'un article (on l'écrase par la nouvelle valeur)
   * 
   * @param a1art
   * @param qte
   * @return
   */
  public boolean modificationArticle(Etablissement pEtb, String a1art, BigDecimal qte, BigDecimal ancienneQte) {
    if (pEtb == null || (a1art == null) || (a1art.trim().equals("")) || listePanierArticle.isEmpty() || ancienneQte == null) {
      return false;
    }
    
    PanierArticleNormal articleExistant = retournerArticleListe(pEtb.getCodeETB(), a1art, ancienneQte);
    if (articleExistant == null) {
      return false;
    }
    // On modifie la quantité de l'article
    articleExistant.setQuantite(qte);
    articleExistant.calculerTotalLigne();
    modificationLignesArticlesDB2();
    // On recalcule le total du panier
    calulerTotalHT();
    return true;
  }
  
  /**
   * On modifie directement surla liste d'articles : on supprime et on pet à jour
   */
  private boolean modificationLignesArticlesDB2() {
    if (numeroCommande != null) {
      return true;
    }
    
    boolean retour = false;
    String requete = "DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + idPanier + "' ";
    if (utilisateur.getAccesDB2().requete(requete) > 0) {
      for (PanierArticle article : listePanierArticle) {
        if (article instanceof PanierArticleNormal) {
          PanierArticleNormal articleNormal = (PanierArticleNormal) article;
          retour = sauverArticlePanier(articleNormal);
          if (retour == false) {
            return false;
          }
        }
      }
    }
    
    return true;
  }
  
  /**
   * Modifier la quantité d'un article en DB2
   */
  private boolean modificationDB2Article(String a1art, int qte) {
    if (numeroCommande != null) {
      return true;
    }
    if (a1art == null || this.utilisateur == null || utilisateur.getAccesDB2() == null || qte == 0) {
      return false;
    }
    
    return (utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN SET ART_QTE = '" + qte
        + "' WHERE ART_PAN = '" + idPanier + "' AND ART_ID = '" + a1art + "' ") == 1);
  }
  
  /**
   * Vide le panier de tous les articles
   * Si le paramètre transmis est true le panier doit même être supprimé de DB2
   */
  public void videPanier(boolean clearDB2) {
    listePanierArticle.clear();
    
    if (utilisateur.getClient().getMagasinClient() != null) {
      magasinPanier = this.utilisateur.getClient().getMagasinClient();
    }
    else {
      magasinPanier = this.utilisateur.getMagasinSiege();
    }
    
    totalHT = BigDecimal.ZERO;
    datePanier = Outils.recupererDateCouranteInt();
    detailsRecuperation = "";
    E1RCC = "";
    modeRecup = MarbreEnvironnement.MODE_NON_CHOISI;
    CLNOM = null;
    CLCPL = null;
    CLRUE = null;
    CLLOC = null;
    CLVIL = null;
    CLPOS = null;
    // Si en mode reprise de commande SerieM et qu'on veut supprimer panier, alors on dévérouille la commande
    if (numeroCommande != null && clearDB2) {
      deverouilleCommande(numeroCommande);
      numeroCommande = null;
    }
    
    if (clearDB2) {
      if (viderPanierDB2()) {
        idPanier = 0;
      }
    }
    else {
      idPanier = 0;
    }
  }
  
  /**
   * Vide le panier dans DB2
   */
  private boolean viderPanierDB2() {
    if (numeroCommande != null) {
      return true;
    }
    if (this.utilisateur == null || utilisateur.getAccesDB2() == null || idPanier == 0) {
      return false;
    }
    
    if (utilisateur.getAccesDB2()
        .requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + idPanier + "'") > -1) {
      return (utilisateur.getAccesDB2()
          .requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".PANIER WHERE PA_ID = '" + idPanier + "'") == 1);
    }
    else {
      return false;
    }
  }
  
  /**
   * Modifier le magasin de retrait du panier
   */
  public boolean deverouilleCommande(String cde) {
    String numCde = null;
    String numSuf = null;
    String typeCde = null;
    
    if (cde != null && cde.length() == 8 && utilisateur != null) {
      
      typeCde = cde.substring(0, 1);
      numCde = cde.substring(1, 7);
      numSuf = cde.substring(7, 8);
      
      return (utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM SET E1TOP=0 WHERE E1NUM = "
          + numCde + " AND E1SUF=" + numSuf + " AND E1COD='" + typeCde + "'") > 0);
    }
    else {
      return false;
    }
  }
  
  /**
   * Modifier le magasin de retrait du panier
   */
  public boolean modifierLeMagasinRetrait(String codemag) {
    if (codemag == null || codemag.trim().equals("")) {
      return false;
    }
    
    if (codemag.equals(Magasin.TOUS_MAGS)) {
      setModeRecup(MarbreEnvironnement.MODE_NON_CHOISI);
    }
    else {
      setMagasinPanier(new Magasin(utilisateur, codemag));
    }
    
    return true;
  }
  
  /**
   * Retourne le message d'erreur
   * 
   * @return
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Calcul le montant total HT du contenu du panier
   */
  private void calulerTotalHT() {
    // Réinit des valeurs
    totalHT = BigDecimal.ZERO;
    totalEcotaxe = BigDecimal.ZERO;
    if (listePanierArticle.isEmpty()) {
      return;
    }
    
    // On réattribue les totaux
    for (PanierArticle articlePanier : listePanierArticle) {
      if (articlePanier instanceof PanierArticleNormal) {
        totalHT = totalHT.add(((PanierArticleNormal) articlePanier).getTotalPourQuantite());
      }
      // MODE ON ADDITIONNE PAS ET ON AFFICHE DANS UNE LIGNE SUPPL //TODO A PARAMETRER
      if (utilisateur.getEtablissementEnCours().isGestion_ecotaxe()) {
        totalEcotaxe = totalEcotaxe.add(((PanierArticleNormal) articlePanier).getTotalEcotaxe());
      }
    }
    
    Trace.debug("[calculTotalHT()] totalHT: " + totalHT);
    Trace.debug("[calculTotalHT()] totalEcotaxe: " + totalEcotaxe);
  }
  
  /**
   * @param pPanierArticle Article du panier à sauver.
   * @return true=ok, false=erreur.
   */
  private boolean sauverArticlePanier(PanierArticleNormal pPanierArticle) {
    // Sortir de suite si le panier est déjà associé à un numéro de commande
    if (numeroCommande != null) {
      return true;
    }
    
    // Vérifier les paramètres
    if (pPanierArticle == null) {
      throw new MessageErreurException("Impossible de sauver un article du panier car l'article est invalide");
    }
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      throw new MessageErreurException("Impossible de sauver un article du panier car l'utilisateur est invalide");
    }
    
    // Créer le panier si nécessaire
    if (!sauverPanier(null, null)) {
      throw new MessageErreurException("Impossible de sauver le panier de l'utilisateur");
    }
    
    // Insérer l'article
    return (utilisateur.getAccesDB2().requete(
        "INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN (ART_PAN,ART_ID,ART_REF,ART_ETB,ART_QTE,ART_TAR,ART_CDT,ART_UNI) "
            + "VALUES ('" + idPanier + "','" + pPanierArticle.getA1art() + "','" + pPanierArticle.getRefFournisseur() + "','"
            + utilisateur.getEtablissementEnCours().getCodeETB() + "','" + pPanierArticle.getQuantite().toString() + "','"
            + pPanierArticle.getTarifHTUnitaire().toString() + "','" + pPanierArticle.getConditionnM() + "','"
            + pPanierArticle.getUniteArt() + "') ") > 0);
  }
  
  /**
   * Sauver un couple clé/valeur pour le panier.
   * 
   * Deux utilisations possibles de cette méthode :
   * - Si les deux parmaètres sont null, permet de créer le panier en base de données si celui-ci n'existe pas.
   * - Si les parmaètres sont renseigné, cette méthode permet de sauver une information pour le panier.
   * 
   * @param pCle Clé de l'information à sauvegarder pour le panier (si null, juste création du panier).
   * @param pValeur Valeur de l'information à sauvegarder pour le panier (remplacé par "" si null).
   * @return true=ok, false=erreur.
   */
  public boolean sauverPanier(String pCle, String pValeur) {
    // Sortir de suite si le panier est déjà associé à un numéro de commande
    if (numeroCommande != null) {
      return true;
    }
    
    // Charger le(s) panier(s) de l'utilisateur
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select("SELECT PA_ID FROM " + MarbreEnvironnement.BIBLI_WS
        + ".PANIER WHERE PA_US = '" + utilisateur.getIdentifiant() + "' AND PA_ETA = '0' ORDER BY PA_ID DESC ");
    if (liste == null) {
      throw new MessageErreurException("Impossible de charger le panier de l'utilisateur");
    }
    
    // Récupérer l'identifiant du panier
    idPanier = 0;
    try {
      if (liste.size() >= 1) {
        idPanier = Integer.parseInt(liste.get(0).getField("PA_ID").toString().trim());
      }
    }
    catch (Exception e) {
      // RAS
    }
    
    // Tester si le panier est déjà sauvé dans la BDD
    if (idPanier > 0) {
      // Renseigner la vlaeur si elle est null
      if (pValeur == null) {
        pValeur = "";
      }
      
      // Mettre à jour une information du panier
      if (pCle != null) {
        return (utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".PANIER SET " + pCle + " = '"
            + traiterCaracteresSpeciauxSQL(pValeur) + "' WHERE PA_ID = '" + idPanier + "'") > 0);
      }
      else {
        return (idPanier > 0);
      }
    }
    // Si il n'est pas dans DB2
    else {
      creerNouveauPanierDB2();
    }
    
    return (idPanier > 0);
  }
  
  /**
   * Créer un noueau panier DB2 et lui attribuer l'ID DB2
   */
  private boolean creerNouveauPanierDB2() {
    
    if (this.utilisateur.getClient().getMagasinClient() != null) {
      magasinPanier = this.utilisateur.getClient().getMagasinClient();
    }
    else {
      magasinPanier = this.utilisateur.getMagasinSiege();
    }
    
    datePanier = Outils.recupererDateCouranteInt();
    detailsRecuperation = "";
    E1RCC = "";
    
    if (utilisateur.getAccesDB2()
        .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".PANIER (PA_US,PA_ETA,PA_DTC,PA_MOD,PA_DTS,PA_MAG,PA_SESS)"
            + " VALUES ('" + utilisateur.getIdentifiant() + "','0','" + datePanier + "','" + modeRecup + "','" + dateRecuperation + "','"
            + magasinPanier.getCodeMagasin() + "','" + utilisateur.getSessionEnCours() + "') ") > 0) {
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select("SELECT PA_ID FROM " + MarbreEnvironnement.BIBLI_WS
          + ".PANIER WHERE PA_US = '" + utilisateur.getIdentifiant() + "' AND PA_ETA = '0' ");
      if (liste != null && liste.size() == 1) {
        if (liste.get(0).isPresentField("PA_ID")) {
          try {
            idPanier = Integer.parseInt(liste.get(0).getField("PA_ID").toString().trim());
            return true;
          }
          catch (Exception e) {
            Trace.erreur(e.getMessage());
          }
        }
      }
    }
    return false;
  }
  
  public String traiterCaracteresSpeciauxSQL(String chaine) {
    if (chaine == null) {
      return null;
    }
    
    return chaine.replace("'", "''");
  }
  
  /**
   * On ajoute un article commentaire au panier
   * @param nouvelarticle
   * @return
   */
  private boolean ajouteArticleCommentaire(int indice, PanierArticleCommentaire pNouvelarticle) {
    if (pNouvelarticle == null) {
      return false;
    }
    
    listePanierArticle.add(0, pNouvelarticle);
    
    return true;
  }
  
  /**
   * Traitement du commentaire que l'on va transformer en
   * PanierArticleCommentaire On sait à la base que l'on a max 600 caractères
   */
  private void traitementDetailsRecuperation() {
    GestionBlocCommentaire blocCommentaire =
        new GestionBlocCommentaire(detailsRecuperation.replaceAll("'", "''"), TAILLE_MAX_LIGNE_COMMENTAIRE, TAILLE_MAX_LIGNE_COMMENTAIRE);
    ArrayList<String> lignes = blocCommentaire.getFormateCommentaire();
    if (lignes == null) {
      return;
    }
    
    // On termine en insèrant les lignes du commentaire dans le panier
    for (int indice = lignes.size(); --indice >= 0;) {
      PanierArticleCommentaire article = new PanierArticleCommentaire();
      ArrayList<String> zones = blocCommentaire.getZones(lignes.get(indice), PanierArticleCommentaire.TAILLE_MAX_ZONE);
      for (int i = 0; i < zones.size(); i++) {
        article.setBllibX(zones.get(i), i + 1);
      }
      ajouteArticleCommentaire(indice, article);
    }
    lignes.clear();
  }
  
  /**
   * vérifier si le panier est garni. C'est à dire qu'il contient des articles
   */
  public boolean isPanierGarni(GenericRecord pRecord) {
    boolean retour = false;
    if (pRecord == null) {
      return retour;
    }
    
    if (pRecord.isPresentField("PA_ID")) {
      idPanier = Integer.parseInt(pRecord.getField("PA_ID").toString().trim());
    }
    
    // chercher les articles liés à ce panier
    if (idPanier > 0) {
      ArrayList<GenericRecord> liste = null;
      liste = utilisateur.getAccesDB2()
          .select("SELECT ART_PAN FROM " + MarbreEnvironnement.BIBLI_WS + ".ARTI_PAN WHERE ART_PAN = '" + idPanier + "'");
      // Si il existe bien des articles dans ce panier
      retour = (liste != null && liste.size() > 0);
    }
    
    return retour;
  }
  
  /**
   * retourner un articlePanier sur la base de son identifiant.
   */
  private boolean existeDansListeArticles(String etbArticle, String codeArticle) {
    if (listePanierArticle == null || listePanierArticle.size() == 0) {
      return false;
    }
    
    for (PanierArticle articlePanier : listePanierArticle) {
      if (articlePanier.getA1etb().equals(etbArticle) && articlePanier.getA1art().equals(codeArticle)) {
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Retourner l'article du panier déjà présent dans la liste correspondant à celui que l'on passe en paramètre
   */
  private PanierArticleNormal retournerArticleListe(PanierArticle pArticle, BigDecimal ancienneQte) {
    if (pArticle == null) {
      return null;
    }
    
    PanierArticleNormal articleNormal = null;
    for (PanierArticle articlePanier : listePanierArticle) {
      if (articlePanier instanceof PanierArticleNormal) {
        articleNormal = (PanierArticleNormal) articlePanier;
        if (articleNormal.getA1etb().equals(pArticle.getA1etb()) && articleNormal.getA1art().equals(pArticle.getA1art())) {
          if (ancienneQte != null && articleNormal.getQuantite().compareTo(ancienneQte) == 0) {
            return articleNormal;
          }
          else {
            return articleNormal;
          }
        }
      }
    }
    
    return articleNormal;
  }
  
  /**
   * Retourner l'article du panier déjà présent dans la liste correspondant à celui que l'on passe en paramètre
   */
  private PanierArticleNormal retournerArticleListe(String pEtb, String pCodeArticle, BigDecimal qte) {
    if (pEtb == null || pCodeArticle == null) {
      return null;
    }
    
    PanierArticleNormal articleNormal = null;
    for (PanierArticle articlePanier : listePanierArticle) {
      if (articlePanier instanceof PanierArticleNormal) {
        articleNormal = (PanierArticleNormal) articlePanier;
        if (articleNormal.getA1etb().trim().equals(pEtb.trim()) && articleNormal.getA1art().trim().equals(pCodeArticle.trim())
            && articleNormal.getQuantite().compareTo(qte) == 0) {
          return articleNormal;
        }
      }
    }
    
    return articleNormal;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  /**
   * @return le totalHT
   */
  public BigDecimal getTotalHT() {
    return totalHT;
  }
  
  public ArrayList<PanierArticle> getListeArticles() {
    return listePanierArticle;
  }
  
  public void setListeArticles(ArrayList<PanierArticle> listeArticles) {
    this.listePanierArticle = listeArticles;
  }
  
  public Magasin getMagasinPanier() {
    return magasinPanier;
  }
  
  public void setMagasinPanier(Magasin magasinPanier) {
    this.magasinPanier = magasinPanier;
    // DB2
    if (magasinPanier != null) {
      sauverPanier("PA_MAG", magasinPanier.getCodeMagasin());
    }
  }
  
  public void setTotalHT(BigDecimal totalHT) {
    this.totalHT = totalHT;
  }
  
  public int getModeRecup() {
    return modeRecup;
  }
  
  public void setModeRecup(int modeRecup) {
    this.modeRecup = modeRecup;
    // DB2
    sauverPanier("PA_MOD", "" + modeRecup);
  }
  
  public String getCodeVendeur() {
    return codeVendeur;
  }
  
  public void setCodeVendeur(String codeVendeur) {
    this.codeVendeur = codeVendeur;
  }
  
  public int getDatePanier() {
    return datePanier;
  }
  
  public void setDatePanier(int datePanier) {
    this.datePanier = datePanier;
  }
  
  public int getDateRecuperation() {
    return dateRecuperation;
  }
  
  public void setDateRecuperation(int dateRecuperation) {
    this.dateRecuperation = dateRecuperation;
    // DB2
    sauverPanier("PA_DTS", "" + dateRecuperation);
  }
  
  public String getDetailsRecuperation() {
    return detailsRecuperation;
  }
  
  public void setDetailsRecuperation(String detailsRecuperation) {
    this.detailsRecuperation = detailsRecuperation;
    sauverPanier("PA_INF", detailsRecuperation);
  }
  
  public String getCLNOM() {
    return CLNOM;
  }
  
  public void setCLNOM(String cLNOM) {
    CLNOM = cLNOM;
    sauverPanier("PA_NOM", cLNOM);
  }
  
  public String getCLCPL() {
    return CLCPL;
  }
  
  public void setCLCPL(String cLCPL) {
    CLCPL = cLCPL;
    sauverPanier("PA_CPL", cLCPL);
  }
  
  public String getCLRUE() {
    return CLRUE;
  }
  
  public void setCLRUE(String cLRUE) {
    CLRUE = cLRUE;
    sauverPanier("PA_RUE", cLRUE);
  }
  
  public String getCLLOC() {
    return CLLOC;
  }
  
  public void setCLLOC(String cLLOC) {
    CLLOC = cLLOC;
    sauverPanier("PA_LOC", cLLOC);
  }
  
  public String getCLVIL() {
    return CLVIL;
  }
  
  public void setCLVIL(String cLVIL) {
    CLVIL = cLVIL;
    sauverPanier("PA_VIL", cLVIL);
  }
  
  public String getCLPOS() {
    return CLPOS;
  }
  
  public void setCLPOS(String cLPOS) {
    CLPOS = cLPOS;
    sauverPanier("PA_CPO", cLPOS);
  }
  
  public String getE1RCC() {
    return E1RCC;
  }
  
  public void setE1RCC(String e1rcc) {
    E1RCC = e1rcc;
    sauverPanier("PA_RCC", e1rcc);
  }
  
  public String getNumCommandeSerieM() {
    return numeroCommande;
  }
  
  public void setNumCommandeSerieM(String numCommandeSerieM) {
    this.numeroCommande = numCommandeSerieM;
  }
  
  public int getID_DB2() {
    return idPanier;
  }
  
  public void setID_DB2(int iD_DB2) {
    idPanier = iD_DB2;
  }
  
  public BigDecimal getTotalEcotaxe() {
    return totalEcotaxe;
  }
  
  public void setTotalEcotaxe(BigDecimal totalEcotaxe) {
    this.totalEcotaxe = totalEcotaxe;
  }
  
}
