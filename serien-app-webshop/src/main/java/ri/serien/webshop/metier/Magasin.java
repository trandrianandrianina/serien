/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class Magasin {
  private String codeMagasin = "";
  private String libelleMagasin = "";
  private String mailDevis = null;
  private String mailCommandes = null;
  private String mailInscriptions = null;
  private UtilisateurWebshop utilisateur = null;
  public static final String TOUS_MAGS = "*TOUS*";
  
  /**
   * Constructeur basique, Sans le libellé
   */
  public Magasin(UtilisateurWebshop utilisateur, String code) {
    if (code == null || code.trim().equals("") || utilisateur == null) {
      Trace.erreur("Magasin1() Utilisateur ou code à NULL");
      return;
    }
    
    this.utilisateur = utilisateur;
    
    codeMagasin = code;
    
    majInfosMagasin();
    
    loggerMonMagasin();
  }
  
  /**
   * Constructeur lorsqu'on connait déjà le libellé
   */
  public Magasin(UtilisateurWebshop utilisateur, String code, String libelle) {
    if (code == null || code.trim().equals("") || utilisateur == null) {
      Trace.erreur("Magasin2() Utilisateur ou code à NULL");
      return;
    }
    
    this.utilisateur = utilisateur;
    
    codeMagasin = code;
    
    libelleMagasin = libelle;
    
    loggerMonMagasin();
    
  }
  
  /**
   * On logge les attributs du magasin
   */
  private void loggerMonMagasin() {
    Trace.debug("-------------------------");
    Trace.debug("loggerMonMagasin() codeMagasin: " + codeMagasin);
    Trace.debug("loggerMonMagasin() libelleMagasin: " + libelleMagasin);
    Trace.debug("-------------------------");
  }
  
  /**
   * mettre à jour le libellé du magasin à partir de son code
   */
  public void majInfosMagasin() {
    if (codeMagasin == null || codeMagasin.trim().equals("")) {
      return;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
        .select("SELECT MG_NAME, MG_MAIL_CD, MG_MAIL_DV, MG_MAIL_IN FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS WHERE MG_COD = '"
            + codeMagasin + "' AND US_ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB() + "' ");
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("MG_NAME")) {
        libelleMagasin = liste.get(0).getField("MG_NAME").toString().trim();
      }
      
      if (liste.get(0).isPresentField("MG_MAIL_DV")) {
        mailDevis = liste.get(0).getField("MG_MAIL_DV").toString().trim();
      }
      
      if (liste.get(0).isPresentField("MG_MAIL_CD")) {
        mailCommandes = liste.get(0).getField("MG_MAIL_CD").toString().trim();
      }
      
      if (liste.get(0).isPresentField("MG_MAIL_IN")) {
        mailInscriptions = liste.get(0).getField("MG_MAIL_IN").toString().trim();
      }
    }
  }
  
  public String getCodeMagasin() {
    return codeMagasin;
  }
  
  public void setCodeMagasin(String code) {
    this.codeMagasin = code;
  }
  
  public String getLibelleMagasin() {
    return libelleMagasin;
  }
  
  public void setLibelleMagasin(String libelleMagasin) {
    this.libelleMagasin = libelleMagasin;
  }
  
  public String getMailDevis() {
    return mailDevis;
  }
  
  public String getMailCommandes() {
    return mailCommandes;
  }
  
  public String getMailInscriptions() {
    return mailInscriptions;
  }
  
}
