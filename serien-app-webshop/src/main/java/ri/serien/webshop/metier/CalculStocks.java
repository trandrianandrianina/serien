/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe de calcul de stocks d'un article Série M/N
 */
public class CalculStocks {
  public CalculStocks() {
  }
  
  /**
   * Calcul du stock d'un article sur la base de deux magasins (en général SIEGE + MAGASIN RETRAIT)
   */
  public String retournerStock(UtilisateurWebshop utilisateur, String etb, String article, String magasin1, String magasin2) {
    if (utilisateur == null || etb == null || article == null) {
      return null;
    }
    BigDecimal qte = new BigDecimal(0);
    
    ArrayList<GenericRecord> liste = null;
    String requete = null;
    String rechercheMagasins = "";
    String optimise = "";
    
    if (magasin1 != null && magasin2 == null) {
      rechercheMagasins = " AND S1MAG = '" + magasin1 + "' ";
      optimise = " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ";
    }
    else if (magasin1 != null && magasin2 != null) {
      rechercheMagasins = " AND S1MAG IN ('" + magasin1 + "' , '" + magasin2 + "') ";
      optimise = " FETCH FIRST 2 ROWS ONLY OPTIMIZE FOR 2 ROWS ";
    }
    
    Trace.debug("[retournerStock()] pour " + etb + "/" + article.trim() + " magasin1: " + magasin1 + " magasin 2: " + magasin2);
    
    // Calcul en mode traitement du stock réservé avec les ALA
    if (MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE) {
      requete = "SELECT SUM(CASE WHEN QTEAFF IS NOT NULL THEN  "
          + " ((S1STD+ S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM + S1QES + S1QSS + S1QDS)-QTEAFF) "
          + "ELSE (S1STD+ S1QEE+ S1QSE+ S1QDE+ S1QEM+ S1QSM+S1QDM+ S1QES+ S1QSS+ S1QDS)   END  ) AS STK, S1MAG  " + "FROM "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMSTKM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
          + ".VUE_STK_RE on S1MAG=AAMAG AND S1ART= AAART   " + " WHERE S1ETB='" + etb + "' AND S1ART='" + article + "' "
          + rechercheMagasins + " GROUP BY S1MAG,S1ART,S1ETB " + optimise + " " + MarbreEnvironnement.CLAUSE_OPTIMIZE;
      // Mode de calcul du stock classique
    }
    else {
      requete = "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM+ S1QES + S1QSS + S1QDS- S1RES) AS STK, S1MAG " + "FROM "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMSTKM " + " WHERE S1ETB='" + etb + "' AND S1ART='" + article + "' "
          + rechercheMagasins + " GROUP BY S1MAG,S1ART,S1ETB " + optimise + " " + MarbreEnvironnement.CLAUSE_OPTIMIZE;
    }
    
    liste = utilisateur.getAccesDB2().select(requete);
    
    if (liste != null) {
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("STK") && liste.get(i).getField("STK") != null) {
          Trace.debug(" Magasin " + liste.get(i).getField("S1MAG") + " Article " + article + ": -> STK: "
              + liste.get(i).getField("STK").toString());
          qte = qte.add(liste.get(i).getDecimal("STK"));
        }
        else {
          Trace.debug(" Magasin " + liste.get(i).getField("S1MAG") + " Article " + article + ": -> STK n'est pas présent");
          qte = qte.add(new BigDecimal(0));
        }
      }
    }
    
    Trace.info("TOTAL stocksArticle " + article + ": -> STK: " + qte.toString());
    Trace.info("----------------------------");
    
    return qte.toString();
  }
  
  /**
   * Redéfinition du calcul de stock avec un seul magasin passé en paramètre
   */
  public String retournerStock(UtilisateurWebshop utilisateur, String etb, String article, String magasin1) {
    return retournerStock(utilisateur, etb, article, magasin1, null);
  }
}
