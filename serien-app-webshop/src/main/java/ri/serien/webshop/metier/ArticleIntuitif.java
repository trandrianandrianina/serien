/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.math.BigDecimal;

public class ArticleIntuitif {
  
  public ArticleIntuitif(String pArticle) {
    codeArticle = pArticle;
  }
  
  private String etb = null;
  private String codeArticle = null;
  private String referenceFourn = null;
  private String libelle = null;
  private String unite = null;
  
  private BigDecimal tarifGeneral = null;
  
  private int ordre = -1;
  
  public String getCodeArticle() {
    return codeArticle;
  }
  
  public void setCodeArticle(String codeArticle) {
    this.codeArticle = codeArticle;
  }
  
  public int getOrdre() {
    return ordre;
  }
  
  public void setOrdre(int ordre) {
    this.ordre = ordre;
  }
  
  public String getEtb() {
    return etb;
  }
  
  public void setEtb(String etb) {
    this.etb = etb;
  }
  
  public String getReferenceFourn() {
    return referenceFourn;
  }
  
  public void setReferenceFourn(String referenceFourn) {
    this.referenceFourn = referenceFourn;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  public String getUnite() {
    return unite;
  }
  
  public void setUnite(String unite) {
    this.unite = unite;
  }
  
  public BigDecimal getTarifGeneral() {
    return tarifGeneral;
  }
  
  public void setTarifGeneral(BigDecimal tarifGeneral) {
    this.tarifGeneral = tarifGeneral;
  }
}
