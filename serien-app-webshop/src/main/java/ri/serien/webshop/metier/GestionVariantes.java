/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreAffichage;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionCatalogue;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionVariantes {
  
  /**
   * Retourne la liste articles variantes liés à un article maitre
   */
  public ArrayList<GenericRecord> retournerArticlesVariantes(UtilisateurWebshop pUtil, String pEtb, String pArticle, GenericRecord pRecord) {
    if (pUtil == null || pEtb == null || pArticle == null || pRecord == null) {
      return null;
    }
    
    ArrayList<GenericRecord> listeArticles = null;
    
    if (pRecord.isPresentField("A1ASB")) {
      String requete = " SELECT PARZ2 FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM " + " WHERE PARTYP = 'TV' AND PARETB = '"
          + pUtil.getEtablissementEnCours() + "' AND PARIND = '" + pRecord.getField("A1ASB").toString().trim() + "' ";
      
      ArrayList<GenericRecord> liste = pUtil.getAccesDB2().select(requete);
      if (liste != null && liste.size() == 1 && liste.get(0).isPresentField("PARZ2")) {
        Variante variante = new Variante(liste.get(0).getField("PARZ2").toString());
        
        if (variante.isProposerAvente()) {
          listeArticles = recupererLesArticlesVariantes(pUtil, variante, pRecord);
        }
      }
    }
    
    return listeArticles;
  }
  
  /**
   * Retourner la liste des articles associés à une variante et ses conditions de recherche et d'affichage
   */
  private ArrayList<GenericRecord> recupererLesArticlesVariantes(UtilisateurWebshop pUtil, Variante pVariante, GenericRecord pRecord) {
    if (pVariante == null) {
      return null;
    }
    
    String conditionsVariante = traiterConditionsSQLVariante(pVariante, pRecord);
    
    if (conditionsVariante == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    String requete = "SELECT A1ETB, A1ART, A1LIB, A1CL1, A1CL2, A1ABC, A1FAM, CAREF, FRAWS ,FRNWS, FRNOM " + " FROM "
        + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '" + pUtil.getEtablissementEnCours().getCodeETB() + "' "
        + MarbreAffichage.retournerFiltreSqlStatutArticle(null) + " " + MarbreAffichage.retournerFiltreSqlVieArticle(null)
        + conditionsVariante + " ORDER BY A1ABC, A1LIB " + " FETCH FIRST " + pUtil.getEtablissementEnCours().getLimite_liste_variantes()
        + " ROWS ONLY " + " OPTIMIZE FOR " + pUtil.getEtablissementEnCours().getLimite_liste_variantes() + " ROWS "
        + MarbreEnvironnement.CLAUSE_OPTIMIZE;
    
    liste = pUtil.getAccesDB2().select(requete);
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        GestionCatalogue.traiterNomFournisseur(record);
      }
    }
    
    return liste;
  }
  
  /**
   * Récupérer les paramètres de la variante et en extraire les conditions SQL
   */
  private String traiterConditionsSQLVariante(Variante pVariante, GenericRecord pRecord) {
    if (pVariante == null || pRecord == null) {
      return "";
    }
    
    String retour = null;
    String comparaison = "";
    
    // On ne fait des recherches sur les articles variantes que si on est en mode proposition à la vente
    if (pVariante.isProposerAvente()) {
      // Code article
      if (pVariante.getNbCodeArticle() > 0 && pRecord.isPresentField("A1ART")) {
        comparaison = pRecord.getField("A1ART").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbCodeArticle()) {
            comparaison = comparaison.substring(0, pVariante.getNbCodeArticle());
          }
          
          retour = " AND A1ART LIKE '" + comparaison + "%' ";
        }
      }
      
      // Mot de classement 1
      if (pVariante.getNbMotClass1() > 0 && pRecord.isPresentField("A1CL1")) {
        comparaison = pRecord.getField("A1CL1").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbMotClass1()) {
            comparaison = comparaison.substring(0, pVariante.getNbMotClass1());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND A1CL1 LIKE '" + comparaison + "%' ";
        }
      }
      
      // Mot de classement 2
      if (pVariante.getNbMotClass2() > 0 && pRecord.isPresentField("A1CL2")) {
        comparaison = pRecord.getField("A1CL2").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbMotClass2()) {
            comparaison = comparaison.substring(0, pVariante.getNbMotClass2());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND A1CL2 LIKE '" + comparaison + "%' ";
        }
      }
      
      // Code famille
      if (pVariante.getNbCodeFamille() > 0 && pRecord.isPresentField("A1FAM")) {
        comparaison = pRecord.getField("A1FAM").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbCodeFamille()) {
            comparaison = comparaison.substring(0, pVariante.getNbCodeFamille());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND A1FAM LIKE '" + comparaison + "%' ";
        }
      }
      
      // Référence article
      if (pVariante.getNbRefFourniss() > 0 && pRecord.isPresentField("CAREF")) {
        comparaison = pRecord.getField("CAREF").toString().trim();
        if (!comparaison.equals("")) {
          if (comparaison.length() > pVariante.getNbRefFourniss()) {
            comparaison = comparaison.substring(0, pVariante.getNbRefFourniss());
          }
          
          if (retour == null) {
            retour = "";
          }
          retour += " AND CAREF LIKE '" + comparaison + "%' ";
        }
      }
      
      if (retour != null) {
        retour += " AND A1ART <> '" + pRecord.getField("A1ART").toString().trim() + "' ";
      }
    }
    
    return retour;
  }
  
}
