/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.metier;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Client Série N
 */
public class Client {
  private UtilisateurWebshop utilisateur = null;
  private Etablissement etablissement = null;
  private String numeroClient = null;
  private String suffixeClient = null;
  private String nomClient = null;
  private Magasin magasinClient = null;
  private String complementClient = null;
  private String rueClient = null;
  private String localiteClient = null;
  private String codePostalClient = null;
  private String villeClient = null;
  private BigDecimal francoPort = BigDecimal.ZERO;
  // Ce client est-il autorisé à agir sur le Web Shop
  private boolean isAutoriseActions = true;
  
  /**
   * Constructeur de base
   */
  public Client(UtilisateurWebshop util, Etablissement etb, String numero, String suffixe) {
    utilisateur = util;
    etablissement = etb;
    numeroClient = numero;
    suffixeClient = suffixe;
    // Maj du bloc adresse
    if (etb != null && numero != null && suffixe != null) {
      majInfosClient(etb, numero, suffixe);
    }
    
    loggerClient();
  }
  
  /**
   * Mise à jour du client de cet utilisateur
   */
  public void majInfosClient(Etablissement etb, String numero, String suffixe) {
    if (utilisateur == null || etb == null || numero == null || suffixe == null) {
      return;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
        .select("SELECT CLNOM,CLCPL,CLRUE,CLLOC,CLCDP1,CLVIL,CLMAG,CLTOP,CLTNS,CLFRP, SUBSTR(PARZ2, 1, 30) AS LIBMAG " + " FROM "
            + utilisateur.getBibli() + ".PGVMCLIM " + " LEFT JOIN " + utilisateur.getBibli()
            + ".PGVMPARM ON PARTYP='MA' AND PARIND=CLMAG " + " WHERE CLETB = '" + etb.getCodeETB() + "' AND CLCLI = '" + numero
            + "' AND CLLIV = '" + suffixe + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("CLNOM")) {
        nomClient = liste.get(0).getField("CLNOM").toString().trim();
      }
      
      if (liste.get(0).isPresentField("CLCPL")) {
        complementClient = liste.get(0).getField("CLCPL").toString().trim();
      }
      
      if (liste.get(0).isPresentField("CLRUE")) {
        rueClient = liste.get(0).getField("CLRUE").toString().trim();
      }
      
      if (liste.get(0).isPresentField("CLLOC")) {
        localiteClient = liste.get(0).getField("CLLOC").toString().trim();
      }
      
      if (liste.get(0).isPresentField("CLCDP1")) {
        codePostalClient = liste.get(0).getField("CLCDP1").toString().trim();
      }
      
      if (liste.get(0).isPresentField("CLVIL")) {
        villeClient = liste.get(0).getField("CLVIL").toString().trim();
      }
      
      if (liste.get(0).isPresentField("CLFRP")) {
        francoPort = (BigDecimal) liste.get(0).getField("CLFRP");
      }
      
      // Si on est pas mono magasin, on prend le mag du client
      if (liste.get(0).isPresentField("CLMAG") && !utilisateur.getEtablissementEnCours().is_mono_magasin()) {
        if (!liste.get(0).getField("CLMAG").toString().trim().equals("")) {
          Trace.debug(
              "+++ On met à jour le magasin client avec son magasin Série M : " + liste.get(0).getField("CLMAG").toString().trim());
          magasinClient = new Magasin(utilisateur, liste.get(0).getField("CLMAG").toString().trim());
        }
      }
      isAutoriseActions = controlerSiClientAutorise(liste.get(0));
    }
    
    if (utilisateur.getMagasinSiege() != null && magasinClient == null) {
      magasinClient = utilisateur.getMagasinSiege();
      Trace.debug("+++ On met à jour le magasin client avec le magasin siege  : " + utilisateur.getMagasinSiege().getCodeMagasin());
    }
  }
  
  /**
   * Contrôler si ce client est autorisé à produire des actions
   * Ce contrôle s'effectue sur les conditions suivantes
   * Client erroné , annulé ou inexistant : CLTOP > 0
   * Client interdit ou désactivé : CLTNS = 2 ou 9
   */
  private boolean controlerSiClientAutorise(GenericRecord pRecord) {
    if (pRecord == null) {
      return false;
    }
    boolean retour = true;
    int cltop = 0;
    if (pRecord.isPresentField("CLTOP")) {
      try {
        cltop = Integer.parseInt(pRecord.getField("CLTOP").toString().trim());
        // Clients érronés, annulés ou inexistants
        if (cltop > 0) {
          retour = false;
        }
      }
      catch (Exception e) {
        Trace.erreur(e.getMessage());
        return false;
      }
    }
    else {
      return false;
    }
    
    int cltns = 0;
    if (pRecord.isPresentField("CLTNS")) {
      try {
        cltns = Integer.parseInt(pRecord.getField("CLTNS").toString().trim());
        // Clients érronés, annulés ou inexistants
        if (cltns == 2 || cltns == 9) {
          retour = false;
        }
      }
      catch (Exception e) {
        Trace.erreur(e.getMessage());
        return false;
      }
    }
    
    return retour;
  }
  
  /**
   * logger les infos clients
   */
  private void loggerClient() {
    Trace.debug("------------------------------");
    Trace.debug("[loggerClient()] etb: " + etablissement);
    Trace.debug("[loggerClient()] numero: " + numeroClient);
    Trace.debug("[loggerClient()] suffixe: " + suffixeClient);
    Trace.debug("[loggerClient()] nomClient: " + nomClient);
    // adresse
    Trace.debug("[loggerClient()] complementClient: " + complementClient);
    Trace.debug("[loggerClient()] rueClient: " + rueClient);
    Trace.debug("[loggerClient()] localiteClient: " + localiteClient);
    Trace.debug("[loggerClient()] codePostalClient: " + codePostalClient);
    Trace.debug("[loggerClient()] villeClient: " + villeClient);
    if (magasinClient != null) {
      Trace.debug("[loggerClient()] COD magasinClient: " + magasinClient.getCodeMagasin());
      Trace.debug("[loggerClient()] lib magasinClient: " + magasinClient.getLibelleMagasin());
    }
    else {
      Trace.debug("[loggerClient()] Pas de magasinClient");
    }
    Trace.debug("------------------------------");
  }
  
  public String getNumeroClient() {
    return numeroClient;
  }
  
  public void setNumeroClient(String numeroClient) {
    this.numeroClient = numeroClient;
  }
  
  public String getSuffixeClient() {
    return suffixeClient;
  }
  
  public void setSuffixeClient(String suffixeClient) {
    this.suffixeClient = suffixeClient;
  }
  
  public String getNomClient() {
    return nomClient;
  }
  
  public void setNomClient(String nomClient) {
    this.nomClient = nomClient;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  public void setEtablissement(Etablissement etablissement) {
    this.etablissement = etablissement;
  }
  
  public Magasin getMagasinClient() {
    return magasinClient;
  }
  
  public void setMagasinClient(Magasin magasinClient) {
    this.magasinClient = magasinClient;
  }
  
  public String getComplementClient() {
    return complementClient;
  }
  
  public void setComplementClient(String complementClient) {
    this.complementClient = complementClient;
  }
  
  public String getRueClient() {
    return rueClient;
  }
  
  public void setRueClient(String rueClient) {
    this.rueClient = rueClient;
  }
  
  public String getLocaliteClient() {
    return localiteClient;
  }
  
  public void setLocaliteClient(String localiteClient) {
    this.localiteClient = localiteClient;
  }
  
  public String getCodePostalClient() {
    return codePostalClient;
  }
  
  public void setCodePostalClient(String codePostalClient) {
    this.codePostalClient = codePostalClient;
  }
  
  public String getVilleClient() {
    return villeClient;
  }
  
  public void setVilleClient(String villeClient) {
    this.villeClient = villeClient;
  }
  
  public boolean isAutoriseActions() {
    return isAutoriseActions;
  }
  
  public BigDecimal getFrancoPort() {
    return francoPort;
  }
  
}
