/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

//==>                                                                       11/06/2014 - 03/11/2014
//==> Permet l'injection des commandes
//==> On utilise le programme CL SGVMIVWSCL de SGVMAS car il effectue un controle des données avant l'injection
//==> A faire:
//==> Exemple:  voir test12 ou test13 (surtout) pour une injection de base (fait main)
//==>       voir test14 pour l'injection à partir d'un panier
//==> Attention, ne pas travailler dans la QTEMP car SQL et CallPrograme n'ont pas la même QTEMP
//==> Notes:
//==>   Si le java ne vous rend pas la main c'est que le CL est planté chercher un job QPWFSERVSO
//==> dans le sous-système QSERVER (avec votre profil) en MSGW
//==>   Soit vous tenez le fichier FCHLOG de la bib TMP???????
//=================================================================================================

package ri.serien.webshop.modeles;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.webshop.constantes.MarbreEnvironnement;

public class InjecteurBdVetF {
  // Variables
  private SystemeManager system = null;
  private ContexteProgrammeRPG programmeAS = null;
  private QueryManager queryManager = null;
  
  private char letter = 'W';
  private String curlib = null;
  private String wrklib = "QTEMP";
  private boolean gestionComplementEntete = false;
  
  // Liste des champs obligatoires
  private String[] requiredFieldDBE = { "BEETB", "BEMAG", "BEDAT", "BERBV", "BEERL", "BENLI" };
  private String[] requiredFieldDBF = { "BFETB", "BFMAG", "BFDAT", "BFRBV", "BFERL", "BFNLI" };
  private String[] requiredFieldDBL = { "BLETB", "BLMAGE", "BLDAT", "BLRBV", "BLERL", "BLNLI", "BLQTE" };
  private String[] requiredFieldDBLDEC = { "BLETB", "BLMAGE", "BLDAT", "BLRBV", "BLERL", "BLNLI", "BLQTE3" };
  
  private StringBuilder sbk = new StringBuilder(1024);
  private StringBuilder sbv = new StringBuilder(1024);
  private ArrayList<String> log = new ArrayList<String>();
  
  protected String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur
   * Si la bibliothèque de travail est la curlib renseigner qd même wrklib
   * @param sys
   * @param wlib
   * @param clib
   * @param let
   */
  public InjecteurBdVetF(SystemeManager sys, String wlib, String clib, char let) {
    this(sys, wlib, clib);
    letter = let;
  }
  
  /**
   * Constructeur
   * Si la bibliothèque de travail est la curlib renseigner qd même wrklib
   * @param sys
   * @param wlib
   * @param clib
   */
  public InjecteurBdVetF(SystemeManager sys, String wlib, String clib) {
    system = sys;
    setWrklib(wlib);
    setCurlib(clib);
  }
  
  /**
   * Initialise le début des opérations
   * @return
   */
  public boolean init() {
    if (curlib == null) {
      return false;
    }
    
    programmeAS = new ContexteProgrammeRPG(system.getSystem());
    queryManager = new QueryManager(system.getDatabaseManager().getConnection());
    boolean retour = programmeAS.executerCommande("CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + letter + "')");
    if (!retour) {
      return false;
    }
    retour = programmeAS.ajouterBibliotheque(MarbreEnvironnement.BIBLI_SYSTEME, false);
    retour = programmeAS.ajouterBibliotheque(MarbreEnvironnement.ENVIRONNEMENT, false);
    retour = programmeAS.ajouterBibliotheque(letter + "EXMAS", false);
    retour = programmeAS.ajouterBibliotheque(letter + "EXPAS", false);
    retour = programmeAS.ajouterBibliotheque(letter + "GVMAS", false);
    retour = programmeAS.ajouterBibliotheque(letter + "GVMX", false); // <- commenter cette ligne pour planter le programme
    // volontairement
    retour = programmeAS.ajouterBibliotheque(curlib, true);
    return retour;
  }
  
  /**
   * Nettoie et créer les fichiers d'injection PINJBDV* dans la bibliothèque de travail
   * @return
   */
  public boolean clearAndCreateAllFilesInWrkLib() {
    return clearAndCreateAllFiles(wrklib);
  }
  
  /**
   * Nettoie et créer les fichiers d'injection PINJBDV* dans la curlib
   * @return
   */
  public boolean clearAndCreateAllFilesInCurLib() {
    return clearAndCreateAllFiles(wrklib);
  }
  
  /**
   * Nettoie et créer les fichiers d'injection PINJBDV* dans la bibliothèque voulue
   * @param bib
   * @return
   */
  public boolean clearAndCreateAllFiles(String bib) {
    boolean retour = clearAndCreateFile(bib, "PINJBDVDSE");
    if (retour) {
      if (gestionComplementEntete) {
        retour = clearAndCreateFile(bib, "PINJBDVDSF");
      }
      if (retour) {
        retour = clearAndCreateFile(bib, "PINJBDVDSL");
        if (retour) {
          retour = clearAndCreateFile(bib, "PINJBDV");
        }
      }
    }
    return retour;
  }
  
  /**
   * Lancement du programme d'injection (SGVMIVWSCL qui effectue un contrôle des données à injecter)
   * @return
   * 
   */
  public boolean injectFile(LinkedHashMap<String, String> rapport) {
    // On crée le paramètre pour l'appel au RPG
    ProgramParameter[] parameterList = new ProgramParameter[2];
    AS400Text XNBON = new AS400Text(11);
    AS400Text BIBTMP = new AS400Text(10);
    parameterList[0] = new ProgramParameter(XNBON.toBytes(String.format("%11s", " ")), 11);
    parameterList[1] = new ProgramParameter(BIBTMP.toBytes(wrklib), 10);
    
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(letter + "GVMAS");
    Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.PROGRAMMES_SERIEN);
    boolean retour = programmeAS.executerProgramme("SGVMIVWSCL", bibliotheque, parameterList);
    if (!retour) {
      msgErreur += "Erreur lors du lancement du programme d'injection du PINJBDV.";
      return retour;
    }
    
    // Analyse des erreurs possibles
    retour = analyseErreurInjection(rapport);
    if (!retour) {
      msgErreur += log.size() + " erreurs(s) détectée(s), consulter la hashmap qui contient le détail des erreurs.\n";
    }
    // On récupère les informations du bon généré (numéro, etb)
    else {
      recupereInfosBon(rapport, (String) XNBON.toObject(parameterList[0].getOutputData()));
    }
    
    return retour;
  }
  
  // --> Méthodes privées <--------------------------------------------------
  
  /**
   * Analyse le paramètre XNBON du CL SGVMIVWSCL afin de récupérer les informations sur le bon créé
   * @param unbon
   * @param xnbon
   */
  private void recupereInfosBon(LinkedHashMap<String, String> rapport, String xnbon) {
    // rapport.clear();
    if ((xnbon == null) || (xnbon.trim().equals(""))) {
      return;
    }
    
    rapport.put("TYPE", "" + xnbon.charAt(0)); // D pour Devis, ...
    rapport.put("ETB", xnbon.substring(1, 4));
    rapport.put("NUMERO", xnbon.substring(4, 10));
    rapport.put("SUFFIXE", xnbon.substring(10));
  }
  
  /**
   * Analyse des erreurs possibles lors de l'injection (enregistrements du FCHLOG)
   * Détail des champs (CSV) du FCHLOG
   * 1: PINJBDV
   * 2: Date d'injecttion
   * 3: Heure d'injection
   * 4: Code erreur
   * 5: Clé de l'enregistrement en erreur
   * 6: Détail de l'erreur
   * @param detailErreurs
   * @return
   * 
   */
  private boolean analyseErreurInjection(LinkedHashMap<String, String> detailErreurs) {
    boolean retour = true;
    
    // Lecture du fichier de log généré par le RPG
    // lectureFchlog();
    
    // Analyse des données
    int indice = 0;
    detailErreurs.clear();
    for (String chaine : log) {
      String[] champs = chaine.split(";");
      if (!champs[3].trim().equals("")) {
        retour = false;
        String ch_indice = String.format("%02d", ++indice);
        detailErreurs.put(ch_indice + "_CLEF", champs[4]);
        detailErreurs.put(ch_indice + "_CODE_ERREUR", champs[3]);
        detailErreurs.put(ch_indice + "_LIBELLE_ERREUR", champs[5]);
      }
    }
    
    return retour;
  }
  
  /**
   * Copie les fichiers PINJBDVDSE et PINJBDVDSL dans le PINJBDV
   * @return
   */
  private boolean copy2Pinjbdv() {
    boolean retour = programmeAS
        .executerCommande("CPYF FROMFILE(" + wrklib + "/PINJBDVDSE) TOFILE(" + wrklib + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
    String codeErreur = programmeAS.getCodeRetour();
    if (retour && (codeErreur.equals("CPC2955") || codeErreur.equals("CPF4011"))) {
      retour = programmeAS
          .executerCommande("CPYF FROMFILE(" + wrklib + "/PINJBDVDSL) TOFILE(" + wrklib + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)");
      codeErreur = programmeAS.getCodeRetour();
      if (!(retour && (codeErreur.equals("CPC2955") || codeErreur.equals("CPF4011")))) {
        msgErreur += "Code erreur:" + codeErreur + " pour " + "CPYF FROMFILE(" + wrklib + "/PINJBDVDSL) TOFILE(" + wrklib
            + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)";
        return false;
      }
    }
    else {
      msgErreur += "Code erreur:" + codeErreur + " pour " + "CPYF FROMFILE(" + wrklib + "/PINJBDVDSE) TOFILE(" + wrklib
          + "/PINJBDV) MBROPT(*ADD) FMTOPT(*NOCHK)";
      return false;
    }
    
    return true;
  }
  
  /**
   * Injecte le fichier PINJBDV
   * @return
   */
  private boolean injectePinjbdv() {
    boolean retour = true;
    String codeErreur = "";
    
    if (wrklib.equals(curlib)) {
      // On travaille dans la CURLIB
      retour = programmeAS.executerCommande("CALL PGM(SGVMIVCL) PARM('X')"); // <- Ca plante chez nous (msgo CPF4103) ca cache un
      // truc !! (10/01/2013) // Attention peut planter
      // sur le SNDRCVFM (*REQUESTER) car le fichier
      // PSEMSEC n'est pas là où il faut... Et M_GPL doit
      // être en ligne (sinon ça marche moins bien)
      codeErreur = programmeAS.getCodeRetour();
      if (!retour) {
        msgErreur += "Code erreur:" + codeErreur + " pour " + "CALL PGM(SGVMIVCL) PARM('X')";
      }
    }
    else {
      retour = programmeAS.executerCommande("CALL PGM(SGVMIVCM) PARM('X' '" + wrklib + "')");
      codeErreur = programmeAS.getCodeRetour();
      if (!retour) {
        msgErreur += "Code erreur:" + codeErreur + " pour " + "CALL PGM(SGVMIVCM) PARM('X' '" + wrklib + "')";
      }
    }
    
    return retour;
  }
  
  /**
   * Nettoie et créer le fichier d'injection PINJBDV
   * @param bib
   * @param pinjbdv
   * @return
   */
  private boolean clearAndCreateFile(String bib, String pinjbdv) {
    if (programmeAS == null) {
      return false;
    }
    
    // On cleare le fichier
    boolean retour = programmeAS.executerCommande("CLRPFM FILE(" + bib + "/" + pinjbdv + ")");
    if (!retour) {
      // Il y a eu une erreur, il est possible que le fichier n'existe pas
      if (!programmeAS.getCodeRetour().equals("CPF3101")) {
        retour = programmeAS.executerCommande("CRTPF FILE(" + bib + "/" + pinjbdv + ") SRCFILE(" + letter
            + "GVMAS/QDDSFCH) SRCMBR(*FILE) OPTION(*NOSRC *NOLIST) SIZE(*NOMAX) WAITFILE(*CLS) WAITRCD(*NOMAX)");
        
      }
    }
    
    return retour;
  }
  
  // --> Accesseurs <--------------------------------------------------------
  
  /**
   * @return le wrklib
   */
  public String getWrklib() {
    return wrklib;
  }
  
  /**
   * @param wlib la library de travail
   */
  public void setWrklib(String wlib) {
    this.wrklib = wlib;
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le detailErreurs
   * 
   *         public LinkedHashMap<String, String> getDetailErreurs()
   *         {
   *         return detailErreurs;
   *         }
   */
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  public boolean isGestionComplementEntete() {
    return gestionComplementEntete;
  }
  
  public void setGestionComplementEntete(boolean gestionComplementEntete) {
    this.gestionComplementEntete = gestionComplementEntete;
  }
  
  /**
   * Enregistre une liste d'enregistrements de type E, F ou L dans le pinjbdvdse, pinjbdvsf ou pinjbdvdsl
   * @param data
   * @return
   * 
   */
  public boolean insertRecordOld(ArrayList<GenericRecord> data) {
    if ((data == null) || (data.size() == 0)) {
      return false;
    }
    
    String table = null;
    boolean erreur = false;
    int i = 0;
    for (GenericRecord rcd : data) {
      i++;
      // On détermine le fichier concerné
      // ENTETE DE BON
      if (rcd.isPresentField("BEERL")) {
        table = "PINJBDVDSE";
        rcd.setRequiredField(requiredFieldDBE);
      }
      // ENTETE COMPLEMENTAIRE
      else if (rcd.isPresentField("BFERL")) {
        table = "PINJBDVDSF";
        rcd.setRequiredField(requiredFieldDBF);
      }
      else if (rcd.isPresentField("BLERL")) {
        table = "PINJBDVDSL";
        if (rcd.isPresentField("BLART")) {
          if (rcd.isPresentField("BLQTE3")) {
            rcd.setRequiredField(requiredFieldDBLDEC);
          }
          else {
            rcd.setRequiredField(requiredFieldDBL);
          }
        }
      }
      if (table == null) {
        msgErreur += "Le record n°" + i + " n'appartient ni au PINJBDVSE, PINJBDVSF ou PINJBDVSL.\n";
        continue;
      }
      
      // Insertion de l'enregistrement
      String requete = rcd.getInsertSQL(table, wrklib);
      if (requete != null) {
        int retour = queryManager.requete(requete);
        if (retour == QueryManager.ERREUR) {
          msgErreur += "La requête (" + requete + ") n'a pas pu être exécuté.\n" + queryManager.getMsgError() + '\n';
          erreur = true;
        }
      }
      else {
        msgErreur += "La requête pour le record n°" + i + " n'a pas pu être générée.\n" + rcd.getMsgError() + '\n';
        erreur = true;
      }
    }
    return !erreur;
  }
  
  /**
   * Lancement du programme d'injection (SGVMIVWSCL qui effectue un contrôle des données à injecter)
   * @return
   * 
   */
  public boolean injectFileOld(LinkedHashMap<String, String> rapport) {
    // On crée le paramètre pour l'appel au RPG
    ProgramParameter[] parameterList = new ProgramParameter[2];
    AS400Text XNBON = new AS400Text(11);
    AS400Text BIBTMP = new AS400Text(10);
    parameterList[0] = new ProgramParameter(XNBON.toBytes(String.format("%11s", " ")), 11);
    parameterList[1] = new ProgramParameter(BIBTMP.toBytes(wrklib), 10);
    
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(letter + "GVMAS");
    Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.PROGRAMMES_SERIEN);
    boolean retour = programmeAS.executerProgramme("SGVMIVWSCL", bibliotheque, parameterList);
    if (!retour) {
      msgErreur += "Erreur lors du lancement du programme d'injection du PINJBDV.";
      return retour;
    }
    
    // Analyse des erreurs possibles
    retour = analyseErreurInjectionOld(rapport);
    if (!retour) {
      msgErreur += log.size() + " erreurs(s) détectée(s), consulter la hashmap qui contient le détail des erreurs.\n";
    }
    // On récupère les informations du bon généré (numéro, etb)
    else {
      recupereInfosBon(rapport, (String) XNBON.toObject(parameterList[0].getOutputData()));
    }
    
    return retour;
  }
  
  private boolean analyseErreurInjectionOld(LinkedHashMap<String, String> detailErreurs) {
    boolean retour = true;
    
    // Lecture du fichier de log généré par le RPG
    lectureFchlogOld();
    
    // Analyse des données
    int indice = 0;
    detailErreurs.clear();
    for (String chaine : log) {
      String[] champs = chaine.split(";");
      if (!champs[3].trim().equals("")) {
        retour = false;
        String ch_indice = String.format("%02d", ++indice);
        detailErreurs.put(ch_indice + "_CLEF", champs[4]);
        detailErreurs.put(ch_indice + "_CODE_ERREUR", champs[3]);
        detailErreurs.put(ch_indice + "_LIBELLE_ERREUR", champs[5]);
      }
    }
    
    return retour;
  }
  
  private void lectureFchlogOld() {
    log.clear();
    ArrayList<GenericRecord> fchlog = queryManager.select("select * from " + wrklib + ".FCHLOG");
    if (fchlog == null) {
      return;
    }
    
    for (GenericRecord rcd : fchlog) {
      log.add((String) rcd.getField(0));
    }
  }
  
}
