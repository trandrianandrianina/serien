/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.modeles;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.database.field.FieldDecimal;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.webshop.constantes.MarbreEnvironnement;

/**
 * Permet la récupération d'un tarif pour un article
 */
public class DuplicDevis {
  // Constantes
  private static final int LG_PARAM_ENVOI = 21;
  //
  private static final int LG_DATASTRUCT = 35;
  
  private static final int LG_PICOD = 1;
  private static final int LG_PIETB = 3;
  private static final int LG_PINUM = 6;
  private static final int LG_SUF = 1;
  private static final int LG_ERREURS = 3;
  
  // Variables entrée spécifiques du programme RPG
  private char[] indicateurs = { '2', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
  private FieldAlpha PICOD = new FieldAlpha(LG_PICOD);
  private FieldAlpha PIETB = new FieldAlpha(LG_PIETB);
  private FieldDecimal PINUM = new FieldDecimal(LG_PINUM, 0);
  private FieldDecimal PISUF = new FieldDecimal(new BigDecimal(0), LG_SUF, 0);
  
  // paramètres retour
  private String RETOUR_PGM = null;
  private String TYP_RETOUR = null;
  private String ETB_RETOUR = null;
  private String CDE_RETOUR = null;
  private int ERREURS = 0;
  
  // 001 Duplication effectuée
  public final static int PAS_ERREUR = 1;
  // 002 Bon d'origine inexistant
  public final static int ERREUR_BON = 2;
  // 003 L'état du devis doit être validé
  public final static int ERREUR_VALID = 3;
  // 004 Date de validité supérieur à la date du jour
  public final static int ERREUR_DATE = 4;
  // Erreur de contenu de retour
  public final static int ERREUR_CONT = 99;
  
  // Variables
  private boolean init = false;
  private SystemeManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
  private char lettre = 'W';
  private String curlib = null;
  private ContexteProgrammeRPG rpg = null;
  private StringBuilder sb = new StringBuilder();
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public DuplicDevis(SystemeManager sys, char let, String curlib) {
    system = sys;
    setLettre(let);
    setCurlib(curlib);
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init) {
      return true;
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
    rpg = new ContexteProgrammeRPG(system.getSystem());
    init = rpg.initialiserLettreEnvironnement(lettre);
    if (!init) {
      return false;
    }
    
    rpg.ajouterBibliotheque(curlib, true);
    rpg.ajouterBibliotheque(MarbreEnvironnement.ENVIRONNEMENT, false);
    rpg.ajouterBibliotheque(lettre + "EXPAS", false);
    rpg.ajouterBibliotheque(lettre + "GVMAS", false);
    init = rpg.ajouterBibliotheque(lettre + "GVMX", false);
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @param pCode
   * @param pEtablissement
   * @param pNumero
   * @param pSuffixe
   * @return
   */
  public boolean execute(String pCode, String pEtablissement, BigDecimal pNumero, BigDecimal pSuffixe) {
    this.PICOD.setValue(formateVar(pCode, LG_PICOD));
    this.PIETB.setValue(formateVar(pEtablissement, LG_PIETB));
    this.PINUM.setValue(pNumero);
    this.PISUF.setValue(pSuffixe);
    
    return execute();
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  private boolean execute() {
    if (rpg == null) {
      return false;
    }
    
    RETOUR_PGM = null;
    TYP_RETOUR = null;
    ETB_RETOUR = null;
    CDE_RETOUR = null;
    ERREURS = 0;
    
    // Construction du paramètre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(PICOD.toFormattedString()).append(PIETB.toFormattedString()).append(PINUM.toFormattedString())
        .append(PISUF.toFormattedString());
    try {
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      // Appel du programme RPG
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(lettre + "GVMAS");
      Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.PROGRAMMES_SERIEN);
      if (rpg.executerProgramme("VGVM25WS", bibliotheque, parameterList)) {
        RETOUR_PGM = (String) ppar1.toObject(parameterList[0].getOutputData());
        if (RETOUR_PGM != null) {
          return traiterRetour() == DuplicDevis.PAS_ERREUR;
        }
      }
    }
    catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  /**
   * ON découpe la chaine de retour en variables métier: Type, etb, commande, erreurs. On retourne le code erreur après vérifications du
   * découpage
   */
  private int traiterRetour() {
    if (RETOUR_PGM == null || RETOUR_PGM.length() != LG_DATASTRUCT) {
      ERREURS = ERREUR_CONT;
      return ERREURS;
    }
    
    int debut = LG_PARAM_ENVOI;
    int fin = debut + LG_PICOD;
    TYP_RETOUR = RETOUR_PGM.substring(debut, fin);
    debut = fin;
    fin = debut + LG_PIETB;
    ETB_RETOUR = RETOUR_PGM.substring(debut, fin);
    debut = fin;
    fin = debut + LG_PINUM + LG_SUF;
    CDE_RETOUR = RETOUR_PGM.substring(debut, fin);
    
    try {
      debut = fin;
      fin = debut + LG_ERREURS;
      ERREURS = Integer.parseInt(RETOUR_PGM.substring(debut, fin));
    }
    catch (NumberFormatException e) {
      ERREURS = ERREUR_CONT;
      return ERREURS;
    }
    
    // On contrôle le contenu du découpage
    if ((TYP_RETOUR == null || TYP_RETOUR.length() != LG_PICOD) || (ETB_RETOUR == null || ETB_RETOUR.length() != LG_PIETB)
        || (CDE_RETOUR == null || CDE_RETOUR.length() != LG_PINUM + LG_SUF)) {
      ERREURS = ERREUR_CONT;
    }
    
    return ERREURS;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage à droite, notamment pour l'établissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null) {
      return String.format("%" + lg + "." + lg + "s", "");
    }
    else {
      return String.format("%" + lg + "." + lg + "s", valeur);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre à définir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ') {
      this.lettre = lettre;
    }
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs à définir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  public String getTYP_RETOUR() {
    return TYP_RETOUR;
  }
  
  public String getETB_RETOUR() {
    return ETB_RETOUR;
  }
  
  public String getCDE_RETOUR() {
    return CDE_RETOUR;
  }
  
  public int getERREURS() {
    return ERREURS;
  }
  
}
