/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.modeles;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.database.field.Field;
import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.database.field.FieldDecimal;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;

public class GestionCommandes {
  // Constantes
  private static final int LG_DATASTRUCT = 64;
  
  private static final int LG_TYPEBON = 1;
  private static final int LG_ETB = 3;
  private static final int LG_NUMBON = 6;
  private static final int LG_SUF = 1;
  
  // Variables spécifiques du programme RPG
  private char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', ' ', ' ' };
  private FieldAlpha typeBon = new FieldAlpha(LG_TYPEBON);
  private FieldAlpha etablissement = new FieldAlpha(LG_ETB);
  private FieldDecimal numeroBon = new FieldDecimal(LG_NUMBON, 0);
  private FieldDecimal suffixeBon = new FieldDecimal(new BigDecimal(0), LG_SUF, 0);
  
  // Variables
  private boolean init = false;
  private SystemeManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
  private char lettre = 'W';
  private String curlib = null;
  private ContexteProgrammeRPG rpg = null;
  private StringBuilder sb = new StringBuilder();
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public GestionCommandes(SystemeManager sys, char let, String curlib) {
    system = sys;
    setLettre(let);
    setCurlib(curlib);
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init) {
      return true;
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
    // rpg = new CallProgram(system.getSystem(), "/QSYS.LIB/"+lettre+"GVMAS.LIB/SGVMCCA1.PGM", null);
    rpg = new ContexteProgrammeRPG(system.getSystem());
    init = rpg.initialiserLettreEnvironnement(lettre);
    if (!init) {
      return false;
    }
    
    rpg.ajouterBibliotheque(curlib, true);
    rpg.ajouterBibliotheque("M_GPL", false);
    rpg.ajouterBibliotheque(lettre + "EXPAS", false);
    rpg.ajouterBibliotheque(lettre + "GVMAS", false);
    init = rpg.ajouterBibliotheque(lettre + "GVMX", false);
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @param pType
   * @param pEtablissement
   * @param pNumeroBon
   * @param pSuffixeBon
   * @return
   */
  public boolean execute(String pType, String pEtablissement, BigDecimal pNumeroBon, BigDecimal pSuffixeBon) {
    setValueTypeBon(pType);
    setValueEtablissement(pEtablissement);
    setValueNumeroBon(pNumeroBon);
    setValueSuffixeBon(pSuffixeBon);
    
    return execute();
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  private boolean execute() {
    if (rpg == null) {
      return false;
    }
    
    // Construction du paramètre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(typeBon.toFormattedString()).append(etablissement.toFormattedString())
        .append(numeroBon.toFormattedString()).append(suffixeBon.toFormattedString());
    try {
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      // Appel du programme RPG
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(lettre + "GVMAS");
      Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.PROGRAMMES_SERIEN);
      if (rpg.executerProgramme("BONDL0", bibliotheque, parameterList)) {
        return true;
      }
    }
    catch (PropertyVetoException e) {
      e.printStackTrace();
    }
    return false;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage à droite, notamment pour l'établissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null) {
      return String.format("%" + lg + "." + lg + "s", "");
    }
    else {
      return String.format("%" + lg + "." + lg + "s", valeur);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre à définir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ') {
      this.lettre = lettre;
    }
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs à définir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  public FieldAlpha getTypeBon() {
    return typeBon;
  }
  
  public void setValueTypeBon(String typeBon) {
    this.typeBon.setValue(typeBon);
  }
  
  /**
   * Retourne le champ Etablissement
   * @return le etablissement
   */
  public Field<String> getEtablissement() {
    return etablissement;
  }
  
  /**
   * Initialise la valeur de l'établissement
   * @param etablissement le etablissement à définir
   */
  public void setValueEtablissement(String etablissement) {
    this.etablissement.setValue(formateVar(etablissement, LG_ETB));
  }
  
  public FieldDecimal getNumeroBon() {
    return numeroBon;
  }
  
  public void setValueNumeroBon(BigDecimal numeroBon) {
    this.numeroBon.setValue(numeroBon);
  }
  
  public FieldDecimal getSuffixeBon() {
    return suffixeBon;
  }
  
  public void setValueSuffixeBon(BigDecimal suffixeBon) {
    this.suffixeBon.setValue(suffixeBon);
  }
  
}
