/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.modeles;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.database.record.GenericRecordManager;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

/**
 * Classe Modèle permettant d'accéder aux données de DB2 Serie M et XWEBSHOP
 */
public class AccesBDDWS extends GenericRecordManager {
  
  UtilisateurWebshop utilisateur = null;
  Connection db2 = null;
  
  /**
   * Constructeur de la classe AccesBDDWS
   */
  public AccesBDDWS(Connection database, UtilisateurWebshop util) {
    super(database);
    db2 = database;
    utilisateur = util;
  }
  
  /**
   * Ajouter un évènement dans la table LOGSW.
   * Les évènements concernent les connexions et déconnexion des utilisateurs et les saisies de devis ou de commandes.
   * 
   * @param pIdUtilisateur : Identifiant Webshop de l'utilsiateur.
   * @param pIdSession : Identifiant de la session en cours.
   * @param pTypeTrace : Type d'évènement (C=connexion, F=déconnexion, D=devis saisi, E=commande saisie).
   * @param pMessage : Message complémentaire pour décrire l'évènement tracé en français.
   * @param pClasse : Classe Java ayant générée l'évènement.
   */
  public int sauverEvenement(int pIdUtilisateur, String pIdSession, String pTypeTrace, String pMessage, String pClasse) {
    return requete(
        "INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".LOGSW (LO_USER,LO_SESS,LO_TYPE,LO_MESS,LO_CLASS,LO_DATE,LO_HEURE) VALUES ('"
            + pIdUtilisateur + "','" + pIdSession + "','" + pTypeTrace + "','" + pMessage.trim() + "','" + pClasse + "','"
            + Outils.recupererDateCouranteInt() + "','" + Outils.recupererHeureCouranteInt() + "')");
  }
  
  /**
   * Récupération de la méthode de queryManager afin de logger de manière générique le SELECT
   */
  @Override
  public ArrayList<GenericRecord> select(String requete) {
    ArrayList<GenericRecord> liste = super.select(requete);
    
    return liste;
  }
  
  /**
   * Récupération de la méthode de queryManager afin de logger de manière générique l'UPDATE, INSERT ou CREATE
   */
  @Override
  public int requete(String requete) {
    int retour = super.requete(requete);
    
    return retour;
  }
  
  /**
   * Récupération de la méthode de queryManager afin de logger de manière générique l'UPDATE, INSERT ou CREATE
   */
  @Override
  public int requete(String requete, GenericRecord record) {
    int retour = super.requete(requete, record);
    
    return retour;
  }
  
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS
  
  public Connection getDb2() {
    return db2;
  }
  
  public void setDb2(Connection db2) {
    this.db2 = db2;
  }
  
  public UtilisateurWebshop getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(UtilisateurWebshop utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
