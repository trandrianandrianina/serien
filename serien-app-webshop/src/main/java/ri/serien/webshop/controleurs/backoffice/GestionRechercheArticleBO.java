/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionRechercheArticleBO extends Gestion {
  
  /**
   * Mettre à jour les critères de recherche article du moteur de recherche
   * dans la table des paramètres du WebShop
   */
  public int majCriteresRechercheArticle(UtilisateurWebshop pUtilisateur, HttpServletRequest pRequest) {
    // Code retour -1 erreur de mise à jour
    // 1 mise à jour effectuée
    
    if (pUtilisateur == null || pRequest == null) {
      return -1;
    }
    
    // tableau correspondance paramètre formulaire / zone de la bdd
    HashMap<String, String> tableauCleValeur = new HashMap<String, String>();
    tableauCleValeur.put("codeArticle", "A1ART");
    tableauCleValeur.put("referenceFour", "CAREF");
    tableauCleValeur.put("libelleArticle", "A1LIB");
    tableauCleValeur.put("classement1", "A1CL1");
    tableauCleValeur.put("classement2", "A1CL2");
    
    String requete = null;
    String cleSQL = "";
    String valeurSQL = "0";
    Set cles = tableauCleValeur.keySet();
    Iterator it = cles.iterator();
    while (it.hasNext()) {
      valeurSQL = "0";
      String paramForm = (String) it.next();
      cleSQL = tableauCleValeur.get(paramForm);
      if (pRequest.getParameter(paramForm) != null && pRequest.getParameter(paramForm).trim().equals("1")) {
        valeurSQL = "1";
      }
      
      requete =
          "UPDATE " + MarbreEnvironnement.BIBLI_WS + ".RECHARTICL SET RE_VAL = '" + valeurSQL + "' WHERE RE_CODE ='" + cleSQL + "' ";
      int resultat = pUtilisateur.getAccesDB2().requete(requete);
      if (resultat < 1) {
        Trace.erreur("Erreur de mise à jour du paramètre " + paramForm + " dans la zone " + cleSQL);
        return -1;
      }
    }
    
    return 1;
  }
  
  /**
   * Retourner la liste des critères de recherche article pour le moteur de recherche
   */
  public ArrayList<GenericRecord> retournerListeCriteres(UtilisateurWebshop pUtilisateur) {
    if (pUtilisateur == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    String requete = " SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".RECHARTICL ORDER BY RE_ID ";
    liste = pUtilisateur.getAccesDB2().select(requete);
    if (liste == null || liste.size() == 0) {
      Trace.erreur("Erreur récupération des critères de recherche article ");
    }
    
    return liste;
  }
  
}
