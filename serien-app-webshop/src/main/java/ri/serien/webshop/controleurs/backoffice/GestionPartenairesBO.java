/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionPartenairesBO extends Gestion {
  
  /**
   * Retourne tous les partenaires
   */
  public ArrayList<GenericRecord> tousLesPartenaires(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[tousLesPartenaires()] Utilisateur à NUll ");
      return null;
    }
    
    return (utilisateur.getAccesDB2()
        .select("SELECT PAR_ID, PAR_NOM, PAR_ETB, PAR_IMAGE, PAR_WEB FROM " + MarbreEnvironnement.BIBLI_WS + ".PARTENAIRE"));
  }
  
  /**
   * Retourne un partenaire
   */
  public GenericRecord unPartenaire(UtilisateurWebshop utilisateur, String unPartenaire, String etb) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[unPartenaire()] Utilisateur à NUll ");
      return null;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select("SELECT PAR_ID, PAR_NOM, PAR_ETB, PAR_IMAGE, PAR_WEB FROM "
        + MarbreEnvironnement.BIBLI_WS + ".PARTENAIRE WHERE PAR_ID='" + unPartenaire + "' AND PAR_ETB = '" + etb + "' ");
    
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Mise à jour ou ajout d'un partenaires
   */
  public int majCreaPartenaires(UtilisateurWebshop utilisateur, HttpServletRequest request, int id) {
    int res = -1;
    if (utilisateur == null || request == null) {
      Trace.erreur("[majCreaPartenaires]Utilisateur à NUll ");
      return res;
    }
    
    // on est modifi de partenaire
    if (request.getParameter("idPart") != null) {
      res = utilisateur.getAccesDB2()
          .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".PARTENAIRE SET " + "PAR_NOM ='"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomPART").toString().trim().toUpperCase()) + "' " + ", PAR_IMAGE ='"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomIMAGE").toString()) + "' " + " , PAR_WEB= '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomWEB").toString()) + "' " + "WHERE PAR_ID ="
              + request.getParameter("idPart"));
      
    } // on est en creation de partenaire
    else {
      
      res = utilisateur.getAccesDB2()
          .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".PARTENAIRE (PAR_NOM, PAR_ETB, PAR_IMAGE, PAR_WEB)VALUES" + " ('"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomPART").toString().trim().toUpperCase()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("etbPart").toString().trim().toUpperCase()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomIMAGE").toString()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nomWEB").toString()) + "')");
    }
    
    return res;
  }
  
  /**
   * Supprimer un partenaire sur la base de son id
   */
  public int suppressionPartenaire(UtilisateurWebshop pUtil, int pIdPart) {
    int res = -1;
    if (pUtil == null) {
      Trace.erreur("[suppressionPartenaire] Utilisateur à NUll ");
      return res;
    }
    
    res = pUtil.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".PARTENAIRE WHERE  PAR_ID =" + pIdPart);
    
    return res;
  }
  
}
