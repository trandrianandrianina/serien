/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionAccesBO extends Gestion {
  
  /**
   * Retourne la liste des menus du BO
   */
  public ArrayList<GenericRecord> recupererMenuBO(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("utilisateur à NULL");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT BTN_ID, BTN_NAME FROM " + MarbreEnvironnement.BIBLI_WS + ".ACCUEILBO " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
  /**
   * retourne une ligne de menu avec ses accès (30,40..)
   * 
   */
  public ArrayList<GenericRecord> recupereParMenu(UtilisateurWebshop utilisateur, String id, String acces) {
    if (utilisateur == null || id == null || acces == null) {
      Trace.erreur("utilisateur à NULL");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT AB_ID, AB_ACC FROM " + MarbreEnvironnement.BIBLI_WS + ".accesBO " + " WHERE AB_ID="
        + id + " AND AB_ACC='" + acces + "'" + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
  /**
   * Traitement des données dans la base accesbo
   * 
   */
  public int ajoutAccesBO(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (utilisateur == null) {
      Trace.erreur("utilisateur à NULL");
      return res;
    }
    
    // Lister les paramètres à mettre à jour
    Enumeration<String> menus = request.getParameterNames();
    
    // On supprime le contenu de la table ACCESBO avant d'inserer avec les nouvelles données
    res = utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".ACCESBO ");
    if (res < 0) {
      return res;
    }
    
    // Liste des paramètres passés
    Object elementMenu = null;
    while (menus.hasMoreElements()) {
      elementMenu = menus.nextElement();
      String param = (String) elementMenu;
      String[] valeurs = request.getParameterValues(param);
      for (int i = 0; i < valeurs.length; i++) {
        // j'exclus le paramètre MAJ
        if (!elementMenu.equals("Maj")) {
          // on insert avec les nouvelles valeurs
          res = utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".ACCESBO (AB_ID, AB_ACC) VALUES ('"
              + elementMenu + "','" + valeurs[i] + "')");
        }
      }
    }
    
    if (res < 0) {
      return res;
    }
    
    return res;
  }
}

/**************************************************** FIN *********************************************************/
