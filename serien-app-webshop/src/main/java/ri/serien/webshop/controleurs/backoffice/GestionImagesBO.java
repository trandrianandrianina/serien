/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionImagesBO extends Gestion {
  
  /** Liste les informations de la page d'accueil par code langue **/
  public ArrayList<GenericRecord> recupererTexteAccueil(UtilisateurWebshop utilisateur, String langue) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT ACC_ID, ACC_NAME, ACC_LAN  FROM " + MarbreEnvironnement.BIBLI_WS + ".INFOACC WHERE ACC_LAN = '" + langue + "' ");
  }
  
  /**
   * Mise à jour dans la base xwebshop.INFOACC
   */
  @SuppressWarnings("unchecked")
  public int miseAjourAccueil(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null || request == null) {
      return res;
    }
    
    // Lister les paramètres à mettre à jour dans toutes les langues
    Enumeration<String> listeParam = request.getParameterNames();
    ArrayList<String> idsTextesAccueil = null;
    
    // Liste de paramètres passés
    String elementParcouru = null;
    while (listeParam.hasMoreElements()) {
      elementParcouru = listeParam.nextElement().toString().trim();
      // Si c'est un texte d'accueil de type linguistique
      if (elementParcouru.startsWith("infoAccueil_")) {
        if (idsTextesAccueil == null) {
          idsTextesAccueil = new ArrayList<String>();
        }
        // On rajoute l'ID à modifier avec le nouveau texte
        idsTextesAccueil.add(elementParcouru.replaceFirst("infoAccueil_", ""));
      }
    }
    
    // ON SUPPRIME LES EVENTUELS ENREGISTREMENTS
    res = utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".INFOACC ");
    if (res < 0) {
      return res;
    }
    
    // ON INSERT les IDS passés en paramètres avec les nouvelles valeurs
    if (idsTextesAccueil != null && idsTextesAccueil.size() > 0 && res > -1) {
      for (int i = 0; i < idsTextesAccueil.size(); i++) {
        res = utilisateur.getAccesDB2()
            .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".INFOACC (ACC_NAME,ACC_LAN) VALUES ('"
                + traiterCaracteresSpeciauxSQL(request.getParameter("infoAccueil_" + idsTextesAccueil.get(i))) + "','"
                + idsTextesAccueil.get(i) + "')");
      }
    }
    
    return res;
    
  }
  
  /** Traitement des images images **/
  public int uploadI(UtilisateurWebshop utilisateur, HttpServletRequest request, String filename) {
    //
    String uploadDir = MarbreEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separator;
    
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    
    int res = 0;
    
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploadé*/
    
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requête*/
    
    /*construction du repertoire ou sera stocker le fichier uploadé. */
    String uploadPath = uploadDir;
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(request);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recupérés
      while (iter.hasNext()) {
        FileItem item = iter.next();
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          filename = item.getName();
          String filePath = uploadPath + filename; // chemin du fichier avec le nom du fichier
          File storeFile = new File(filePath);
          item.write(storeFile);
          res = 1;
        }
      }
      
    }
    catch (Exception ex) {
      res = -1;
      Trace.erreur(ex.getMessage());
    }
    
    if (res < 0) {
      Trace.erreur("uploadI(): L'upload de fichier ");
    }
    
    return res;
  }
}
