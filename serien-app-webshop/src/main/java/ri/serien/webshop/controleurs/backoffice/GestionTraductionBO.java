/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionTraductionBO extends Gestion {
  
  /**
   * Affiche liste moteur de recherche
   */
  public ArrayList<GenericRecord> retourneMotCleduMoteur(UtilisateurWebshop utilisateur, String expression) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    if (expression == null) {
      return null;
    }
    
    return utilisateur.getAccesDB2().select(" SELECT TR_CLE, TR_VALEUR FROM " + MarbreEnvironnement.BIBLI_WS + ".TRADUCTION "
        + " WHERE TR_CODE = 'fr' AND LCASE(TR_CLE) LIKE '%" + expression.toLowerCase() + "%' " + " ORDER BY TR_CLE ASC ");
  }
  
  public ArrayList<GenericRecord> retourUnMotCleParLangue(UtilisateurWebshop utilisateur, String mot, String langue) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    return utilisateur.getAccesDB2()
        .select("SELECT TR_ID,TR_CLE, TR_VALEUR, TR_CODE, b.la_id FROM " + MarbreEnvironnement.BIBLI_WS + ".TRADUCTION a" + " left join "
            + MarbreEnvironnement.BIBLI_WS + ".LANGUES b ON a.TR_CODE = b.LA_ID" + " WHERE TR_CLE = '" + mot + "' AND TR_CODE= '" + langue
            + "'");
  }
  
  /**
   * Mise à jour d'un mot clé
   * C'est vraiment pas bo ce DELETE INSERT mais on changera plus tard
   */
  @SuppressWarnings("unchecked")
  public int miseAjourMotCle(UtilisateurWebshop utilisateur, HttpServletRequest request, String motCle) {
    int res = -1;
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NULL");
      return res;
    }
    
    // Lister les paramètres à mettre à jour
    Enumeration<String> motCles = request.getParameterNames();
    
    // Liste des paramètres passés
    ArrayList<String> moscleSelectionne = null;
    String motCleParcouru = null;
    while (motCles.hasMoreElements()) {
      motCleParcouru = motCles.nextElement();
      
      if (motCleParcouru.startsWith("motCle_")) {
        if (moscleSelectionne == null) {
          moscleSelectionne = new ArrayList<String>();
        }
        
        moscleSelectionne.add(motCleParcouru.replaceFirst("motCle_", ""));
      }
    }
    
    // On supprime le mot cle selectionné et ses traductions avant d'inserer avec les nouvelles données
    res = utilisateur.getAccesDB2()
        .requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".TRADUCTION WHERE TR_CLE = '" + request.getParameter("motCle") + "'");
    if (res < 0) {
      return res;
    }
    
    // ON INSERT les donnees en paramètres avec les nouvelles valeurs
    if (moscleSelectionne != null && moscleSelectionne.size() > 0) {
      for (int i = 0; i < moscleSelectionne.size(); i++) {
        res = utilisateur.getAccesDB2()
            .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".TRADUCTION (TR_CODE, TR_CLE,TR_VALEUR) VALUES( '"
                + moscleSelectionne.get(i) + "','" + request.getParameter("motCle") + "','"
                + (traiterCaracteresSpeciauxSQL(request.getParameter("motCle_" + moscleSelectionne.get(i)))) + "')");
      }
    }
    
    return res;
  }
  
  /**
   * Permet de retourner la liste des traductions
   */
  public ArrayList<GenericRecord> retournerListeTraductions(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select(
        "SELECT TR_CLE, TR_VALEUR FROM " + MarbreEnvironnement.BIBLI_WS + ".TRADUCTION WHERE TR_CODE = 'fr' ORDER BY TR_CLE ASC ");
  }
}
