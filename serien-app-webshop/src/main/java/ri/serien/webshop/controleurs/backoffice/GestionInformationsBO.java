/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe permettant de modifier les informations du magasin pour le site institutionnel
 * 
 * 
 */
public class GestionInformationsBO extends Gestion {
  
  /**
   * Recuperer les données du magasin que l'on veut modifier
   */
  public ArrayList<GenericRecord> recupererUneInformation(UtilisateurWebshop utilisateur, String langue) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || langue == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT INF_ID,INF_TXT,INF_DES,INF_IMG,INF_IMG2,INF_TXT2,INF_DES2 FROM "
        + MarbreEnvironnement.BIBLI_WS + ".INFOSW WHERE INF_LAN = '" + langue + "'");
  }
  
  /**
   * Mise à jour de la table INFORMATION de XWEBSHOP
   */
  public int miseajourInformation(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return -1;
    }
    
    // On supprime d'éventuels enregistrements pour ce code langue
    if (request.getParameter("langueInfo") != null) {
      res = utilisateur.getAccesDB2().requete(
          "DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".INFOSW WHERE INF_LAN = '" + request.getParameter("langueInfo") + "'");
    }
    
    // On rajoute un nouvel enregistrement pour ce code langue
    if (res > -1) {
      res = utilisateur.getAccesDB2()
          .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
              + ".INFOSW (INF_TXT,INF_DES,INF_IMG,INF_IMG2,INF_TXT2,INF_DES2,INF_LAN) VALUES " + "('"
              + traiterCaracteresSpeciauxSQL(request.getParameter("texte1")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("des1")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("img1")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("img2")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("texte2")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("des2")) + "','" + (request.getParameter("langueInfo")) + "')");
    }
    
    return res;
  }
  
  /** traite l'upload des images informations */
  public int upload(UtilisateurWebshop utilisateur, HttpServletRequest request, String filename) {
    String uploadDirectory = MarbreEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separator;
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    int res = -1;
    
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploader*/
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requête*/
    
    /*construction du repertoire ou sera stocker le fichier uploader. */
    String uploadPath = uploadDirectory;
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(request);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recupérés
      while (iter.hasNext()) {
        FileItem item = iter.next();
        
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          filename = item.getName();
          String filePath = uploadPath + filename; // chemin du fichier avec le nom du fichier
          File storeFile = new File(filePath);
          // Enregistrer le fichier sur le disque
          item.write(storeFile);
          res = 1;
        }
      }
      
    }
    catch (Exception ex) {
      res = -1;
      Trace.erreur(ex.getMessage());
    }
    return res;
  }
  
}
