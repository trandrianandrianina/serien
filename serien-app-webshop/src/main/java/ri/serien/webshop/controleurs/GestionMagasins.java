/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Etablissement;

/**
 * Classe permettant d'afficher les informations par magasin
 * Table : Magasin
 * @author Déborah
 * 
 */
public class GestionMagasins extends Gestion {
  
  /**
   * Retourne la liste des magasins par filiale
   */
  public ArrayList<GenericRecord> retourneMagasinsParEtbs(UtilisateurWebshop utilisateur, Etablissement etablissement) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    // Si on a pas d'établissements on va tous les récupérer et trier les magasins par ETB
    if (utilisateur.getEtablissementEnCours() == null) {
      return utilisateur.getAccesDB2()
          .select("SELECT MG_NAME, MG_ADR, MG_TEL, MG_CARTE, US_ETB, ETB_LIB, LI_MESS, LI_HOR, LI_EQUI " + " FROM "
              + MarbreEnvironnement.BIBLI_WS + ".ETABLISS LEFT JOIN " + MarbreEnvironnement.BIBLI_WS
              + ".MAGASINS ON US_ETB = ETB_ID LEFT JOIN " + MarbreEnvironnement.BIBLI_WS + ".LIENSMAG "
              + " ON MG_ID = LI_MAG AND LI_LAN = '" + utilisateur.getLanguage() + "' WHERE MG_ACTIF = '1' AND ETB_FM = '"
              + MarbreEnvironnement.BIBLI_CLIENTS + "' ORDER BY US_ETB ASC,MG_COD ASC " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      
    }
    else if (utilisateur.getEtablissementEnCours() != null && utilisateur.getEtablissementEnCours().getCodeETB() != null) {
      return utilisateur.getAccesDB2()
          .select("SELECT MG_NAME, MG_ADR, MG_TEL, MG_CARTE, US_ETB, LI_MESS, LI_HOR, LI_EQUI " + " FROM " + MarbreEnvironnement.BIBLI_WS
              + ".MAGASINS LEFT JOIN " + MarbreEnvironnement.BIBLI_WS + ".LIENSMAG " + " ON MG_ID = LI_MAG AND LI_LAN = '"
              + utilisateur.getLanguage() + "' WHERE US_ETB ='" + etablissement.getCodeETB() + "' AND MG_ACTIF = '1' ORDER BY MG_COD ASC "
              + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    else {
      Trace.erreur("Etablissement à NUll");
      return null;
    }
  }
}
