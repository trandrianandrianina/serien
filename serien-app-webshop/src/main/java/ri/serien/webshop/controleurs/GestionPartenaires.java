/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;
//import ri.serien.libas400.database.record.GenericRecord;

public class GestionPartenaires extends Gestion {
  
  /**
   * retourne la liste de partenaires de cet établissement
   */
  public ArrayList<GenericRecord> retournerListePartenaires(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    if (utilisateur.getEtablissementEnCours() != null) {
      return utilisateur.getAccesDB2()
          .select("SELECT PAR_NOM, PAR_IMAGE, PAR_WEB FROM " + MarbreEnvironnement.BIBLI_WS + ".PARTENAIRE WHERE PAR_ETB='"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' ORDER BY PAR_NOM " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    else {
      return utilisateur.getAccesDB2().select("SELECT PAR_NOM, PAR_IMAGE, PAR_WEB FROM " + MarbreEnvironnement.BIBLI_WS
          + ".PARTENAIRE ORDER BY PAR_NOM " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
  }
}
