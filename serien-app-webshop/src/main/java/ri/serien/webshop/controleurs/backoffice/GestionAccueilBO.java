/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe permettant d'organiser les menus du BO
 * Table : BTN_ACCUEILBO
 * @author Déborah
 * 
 */
public class GestionAccueilBO extends Gestion {
  
  /**
   * retourne la liste de la table accesbo
   */
  
  public ArrayList<GenericRecord> retourneAccesBO(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT AB_ID, AB_ACC FROM " + MarbreEnvironnement.BIBLI_WS + ".ACCESBO ");
  }
  
}
