/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

public class GestionUtilisateursBO extends Gestion {
  
  /**
   * Retourne une liste d'utilisateurs triés par ACCES prioritaire
   */
  public ArrayList<GenericRecord> retournerListeUtilis(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    return utilisateur.getAccesDB2()
        .select("SELECT US_ID,US_LOGIN,US_ACCES,US_SERIEM,US_ETB,US_DATC, US_DATM, RWACC, RENUM, RENET, CLCLI, CLNOM FROM "
            + MarbreEnvironnement.BIBLI_WS + ".USERW " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM "
            + " ON CAST(RLNUMT AS CHAR(15)) = US_SERIEM AND US_ETB = RLETB AND RLCOD = 'C' " + " LEFT JOIN "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMCLIM ON DIGITS(CLCLI)||DIGITS(CLLIV) = RLIND " + " LEFT JOIN "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM ON RLNUMT = RENUM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTWM ON RLNUMT = RWNUM ORDER BY US_ACCES, RWACC, US_ETB, US_LOGIN ");
  }
  
  /**
   * Retourne les infos d'un utilisateur
   */
  public ArrayList<GenericRecord> retournerUnUtilisateur(UtilisateurWebshop utilisateur, int user_id) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select(
        "SELECT US_ID,US_LOGIN,US_ACCES,US_SERIEM,US_ETB FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW where US_ID= " + user_id);
  }
  
  /**
   * ajout d'un nouveau responsable dans la table userw
   */
  public int ajoutProfilAs400(UtilisateurWebshop utilisateur, HttpServletRequest request, int acces) {
    int res = -1;
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return res;
    }
    
    String login = request.getParameter("loginUser").toString().trim().toUpperCase();
    
    // D'abord on teste si ce profil existe ou non dans le WebShop
    ArrayList<GenericRecord> liste = null;
    liste = utilisateur.getAccesDB2().select("" + " SELECT US_ID FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW "
        + " WHERE UPPER(US_LOGIN) = '" + login.toUpperCase() + "' AND US_ETB = '" + utilisateur.getEtablissementEnCours() + "' ");
    
    if (liste == null) {
      return res;
    }
    // On retourne le nombre d'enregistrements afin de retourner un message adapté puisqu'il en a trop
    if (liste.size() > 1) {
      return liste.size();
    }
    
    // Ensuite on teste si ce profil est valide Série M -2 est le code retour pour un profil non autorisé
    if (!controlerUnProfilAs400(utilisateur, login)) {
      return -2;
    }
    
    if (liste.size() == 1 && liste.get(0).isPresentField("US_ID")) {
      res = utilisateur.getAccesDB2()
          .requete("" + " UPDATE " + MarbreEnvironnement.BIBLI_WS + ".USERW " + " SET US_ACCES = '" + acces + "',  US_DATM = '"
              + Outils.recupererDateCouranteInt() + "'  " + " WHERE US_ID = '" + liste.get(0).getField("US_ID").toString().trim()
              + "' AND US_ETB = '" + utilisateur.getEtablissementEnCours() + "' ");
    }
    else if (liste.size() == 0) {
      res = utilisateur.getAccesDB2()
          .requete("" + " INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".USERW  "
              + " (US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,US_DATC,US_DATM) VALUES " + " ('" + login.toUpperCase() + "'," + " '',"
              + " '" + acces + "'," + " ''," + " '" + utilisateur.getEtablissementEnCours() + "'," + "'"
              + Outils.recupererDateCouranteInt() + "'," + "'" + Outils.recupererDateCouranteInt() + "'" + ") ");
    }
    
    return res;
  }
  
  /**
   * Contrôler si le profil passé est un profil Série N valide
   */
  private boolean controlerUnProfilAs400(UtilisateurWebshop pUtil, String pProfil) {
    return true;
  }
  
  /**
   * supprimer un responsable dans la table userw
   */
  public int suppressionResponsable(UtilisateurWebshop utilisateur, int userId) {
    int res = 0;
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return res;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2()
        .select("SELECT US_ID,US_LOGIN,US_ACCES,US_SERIEM,US_ETB FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW where US_ID= " + userId);
    
    if (liste != null && liste.size() == 1 && utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_RESPON_WS) {
      res = utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW WHERE  US_ID =" + userId);
    }
    
    return res;
  }
  
}
