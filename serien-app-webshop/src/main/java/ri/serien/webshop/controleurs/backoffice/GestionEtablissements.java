/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionEtablissements extends Gestion {
  
  /**
   * Retourne la liste de tous les établissements de Série N via PGVMPARM
   */
  public ArrayList<GenericRecord> recupereListeETBsPGVMPARM(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[recupereListeETBsPGVMPARM()] Utilisateur à NUll ");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT PARETB,SUBSTR(PARZ2, 1, 24) AS LIBETB FROM " + MarbreEnvironnement.BIBLI_CLIENTS
        + ".PGVMPARM " + " WHERE PARTYP='DG' AND PARETB <> '' ");
  }
  
  /**
   * Retourne la liste de tous les établissements de ETABLISS
   */
  public ArrayList<GenericRecord> recupereListeTousETBs(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[recupereListeTousETBs()] Utilisateur à NUll ");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT ETB_ID, ETB_LIB, ETB_ACTIF FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ETABLISS  WHERE ETB_FM = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' ORDER BY ETB_ID ");
  }
  
  /**
   * Retourne la liste d'établissements actifs de ETABLISS !!
   */
  public ArrayList<GenericRecord> recupereListeETBsActifs(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[recupereListeETBsActifs()] Utilisateur à NUll ");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT ETB_ID, ETB_LIB, ETB_ACTIF FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ETABLISS  WHERE ETB_FM = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' AND ETB_ACTIF='1' ORDER BY ETB_ID ");
  }
  
  /**
   * Retourner un seul établissement
   */
  public GenericRecord recupereUnEtB(UtilisateurWebshop utilisateur, String etb) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || etb == null) {
      Trace.erreur("[recupereUnEtB()] Utilisateur ou ETB à NUll ");
      return null;
    }
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT ETB_ID, ETB_LIB, ETB_ACTIF FROM " + MarbreEnvironnement.BIBLI_WS
            + ".ETABLISS WHERE ETB_FM = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' AND ETB_ID='" + etb + "' ");
    
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Mis à jour de l'établissement avec de nouveaux paramètres
   */
  public int majEtablissement(UtilisateurWebshop utilisateur, String etb, String libelle, String etat) {
    return utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ETABLISS SET ETB_LIB = '" + libelle
        + "', ETB_ACTIF = '" + etat + "' WHERE ETB_ID = '" + etb + "' AND ETB_FM ='" + MarbreEnvironnement.BIBLI_CLIENTS + "' ");
  }
  
}
