/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionLogsBO extends Gestion {
  /**
   * modifier la varaible DEBUG
   */
  public boolean activeLogsDebug(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return false;
    }
    
    // si la variable est activée on la dsactive sinon l'inverse
    if (MarbreEnvironnement.MODE_DEBUG) {
      if (utilisateur.getAccesDB2()
          .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '0' WHERE EN_CLE='MODE_DEBUG'") > 0) {
        MarbreEnvironnement.MODE_DEBUG = false;
      }
    }
    else {
      if (utilisateur.getAccesDB2()
          .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '1' WHERE EN_CLE='MODE_DEBUG'") > 0) {
        MarbreEnvironnement.MODE_DEBUG = true;
      }
    }
    
    return MarbreEnvironnement.MODE_DEBUG;
  }
  
  /**
   * Liste tous les logs DEBUG
   * @return
   */
  public ArrayList<String> listeFileLogDebug() {
    String fileLog = null;
    FileNG logDebug = new FileNG(MarbreEnvironnement.DOSSIER_RACINE_TOMCAT + File.separator + "logs" + File.separator);
    File[] list = logDebug.listFiles();
    ArrayList<String> allFilesDebug = new ArrayList<String>();
    Comparator<String> comparator = Collections.reverseOrder();
    if (logDebug.exists()) {
      for (int i = 0; i < list.length; i++) {
        fileLog = list[i].getName();
        if (fileLog.startsWith("webshop")) {
          allFilesDebug.add(fileLog);
        }
      }
      
    }
    Collections.sort(allFilesDebug, comparator);
    return allFilesDebug;
  }
  
  // retourne le détail du fichier
  public ArrayList<String> detailFichierLog(String fileLog) {
    
    File fileModif = new File(MarbreEnvironnement.DOSSIER_RACINE_TOMCAT + File.separator + "logs" + File.separator + fileLog);
    
    GestionFichierTexte gestionFichier = new GestionFichierTexte(fileModif);
    ArrayList<String> allContenuFile = new ArrayList<String>();
    // contenu de nom fichier
    allContenuFile = gestionFichier.getContenuFichier();
    
    // tri de nom fichier
    Comparator<String> comparator = Collections.reverseOrder();
    Collections.sort(allContenuFile, comparator);
    
    return allContenuFile;
    
  }
  
}
