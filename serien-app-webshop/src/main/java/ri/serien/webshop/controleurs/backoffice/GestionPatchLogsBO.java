/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

public class GestionPatchLogsBO extends Gestion {
  
  private String versionInstallee = null;
  private GestionFichierTexte gestionFichier = null;
  
  // constantes
  private static final String VERSIONACTUELLE = "versionActuelle.txt";
  private static final String DIRVERSION = MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR + "WEB-INF"
      + MarbreEnvironnement.SEPARATEUR + "versions" + MarbreEnvironnement.SEPARATEUR;
  
  /**
   * Recupere tous les patchs
   * 
   */
  public ArrayList<GenericRecord> recuperePatchs(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT PA_ID,PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV, PA_APP FROM "
        + MarbreEnvironnement.BIBLI_WS + ".PATCHSW order by PA_ID desc");
  }
  
  /**
   * liste un patch
   * 
   */
  public ArrayList<GenericRecord> recupereUnPatch(UtilisateurWebshop utilisateur, int id) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT PA_ID,PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP FROM  "
        + MarbreEnvironnement.BIBLI_WS + ".PATCHSW where PA_ID= " + id);
  }
  
  /**
   * ajout patch dans la table patch
   * 
   */
  public int miseajourPatch(UtilisateurWebshop utilisateur, HttpServletRequest request, int id) {
    /*On verifie qu'on est dans la même version sql sinon pas de modification dans le patch*/
    int res = 0;
    GenericRecord rcd = new GenericRecord();
    String[] requiredfield = { "PA_CLAS", "PA_DETA", "PA_DATE", "PA_ETAT", "PA_VISI", "PA_DEV", "PA_APP" };
    rcd.setRequiredField(requiredfield);
    
    int paDate = Outils.transformerDateHumaineEnSeriem(request.getParameter("date"));
    String datePa = "" + paDate;
    rcd.setField("PA_CLAS", request.getParameter("classeJava"), true);
    rcd.setField("PA_DETA", request.getParameter("detail"), true);
    rcd.setField("PA_DATE", datePa, true);
    rcd.setField("PA_ETAT", request.getParameter("etat"), true);
    rcd.setField("PA_VISI", request.getParameter("visibilite"), true);
    rcd.setField("PA_DEV", request.getParameter("dev"), true);
    rcd.setField("PA_APP", request.getParameter("application"), true);
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT PA_ID,PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_NOM,PA_APP,PA_SQL,PA_DEV FROM  "
            + MarbreEnvironnement.BIBLI_WS + ".PATCHSW order by PA_ID desc");
    
    if (liste != null && liste.size() > 0) {
      if (request.getParameter("idPatch") != null) {
        String req = rcd.createSQLRequestUpdate("PATCHSW", MarbreEnvironnement.BIBLI_WS, "PA_ID=" + (request.getParameter("idPatch")));
        res = utilisateur.getAccesDB2().requete(req, rcd);
      }
      else {
        scanVersion();
        
        // et je mets l'insertion du patch dans la table patchsw
        res = utilisateur.getAccesDB2()
            .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
                + ".PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES " + "('"
                + traiterCaracteresSpeciauxSQL(request.getParameter("classeJava")) + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("detail")) + "','"
                + (Outils.transformerDateHumaineEnSeriem(request.getParameter("date"))) + "','" + versionInstallee + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("visibilite")) + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("dev")) + "','" + request.getParameter("application") + "','"
                + (request.getParameter("instSql") + "')"));
      }
    }
    return res;
  }
  
  /**
   * lister tous les .class du projet WS
   * @param path
   * @param allFiles
   * @param request
   * @param utilisateur
   * @return
   */
  public ArrayList<String> listerClassesJava(File path, ArrayList<String> allFiles, HttpServletRequest request,
      UtilisateurWebshop utilisateur) {
    
    File[] list = path.listFiles();
    if (path.isDirectory()) {
      list = path.listFiles();
      if ((list != null) && (list.length > 0)) {
        for (int i = 0; i < list.length; i++) {
          // Appel récursif sur les sous-répertoires
          listerClassesJava(list[i], allFiles, request, utilisateur);
        }
      }
    }
    else {
      String currentFilePath = path.getName();
      
      if (currentFilePath.endsWith(".java") || (currentFilePath.endsWith(".class"))) {
        allFiles.add(currentFilePath);
      }
      
    }
    Collections.sort(allFiles);
    return allFiles;
  }
  
  /**
   * récupere la version installée
   * @return
   */
  public boolean scanVersion() {
    boolean res = false;
    File versionActuelle = new File(DIRVERSION + VERSIONACTUELLE);
    /*si le fichier existe, on lit son contenu et on recupere le n° de version*/
    if (versionActuelle.exists()) {
      if (gestionFichier == null) {
        gestionFichier = new GestionFichierTexte(versionActuelle);
      }
      
      versionInstallee = gestionFichier.getContenuFichierString(true).trim();
      res = true;
    }
    return res;
  }
  
}
