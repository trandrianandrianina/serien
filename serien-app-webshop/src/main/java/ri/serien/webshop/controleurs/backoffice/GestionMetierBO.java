/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionMetierBO extends Gestion {
  
  /**
   * Retourne la liste des paramétres d'un ETB de la table ENVIRONM de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererParametresEtb(UtilisateurWebshop utilisateur, String etb) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || etb == null) {
      Trace.erreur("[recupererParametresEtb()] Utilisateur ou ETB à NUll");
      return null;
    }
    // EN_GEST == 1 c'est pour les variables de type CONTEXT et EN_GEST == 2 de type ETABLISSEMENT
    return utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_VAL, EN_TYP FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_GEST > 1 AND EN_ETB = '" + etb + "' ORDER BY EN_CLE ");
  }
  
  /**
   * Retourne la liste des paramètres types ETB de la table ENVIRONM de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererPatternParametresEtb(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[recupererParametresEtb()] Utilisateur ou ETB à NUll");
      return null;
    }
    
    // EN_GEST == 1 c'est pour les variables de type CONTEXT et EN_GEST == 2 de type ETABLISSEMENT
    return utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_TYP FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_GEST > 1 GROUP BY EN_CLE,EN_VAL,EN_TYP ORDER BY EN_CLE ");
  }
  
  /**
   * Mise à jour de la table ENVIRONM de XWEBSHOP
   */
  public boolean miseajourParametresEtb(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    boolean retour = false;
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[miseajourParametreclient()] Utilisateur ou ETB à NUll");
      return retour;
    }
    
    if (request.getParameter("etbCours") == null) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste = null;
    int res = 0;
    // EN_GEST == 1 c'est pour les variables de type CONTEXT et EN_GEST == 2 de type ETABLISSEMENT
    liste = recupererParametresEtb(utilisateur, request.getParameter("etbCours"));
    
    if (liste != null && liste.size() > 0) {
      int nbModif = 0;
      
      for (int i = 0; i < liste.size(); i++) {
        if ((liste.get(i).getField("EN_TYP").toString().trim().equals("MAJUSCULE"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim().toUpperCase() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE").toString().trim() + "' AND EN_ETB = '" + request.getParameter("etbCours") + "' ");
          if (res >= -1) {
            nbModif++;
          }
        }
        else if ((liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '" + Base64Coder
                  .encodeString(Base64Coder.encodeString(request.getParameter(liste.get(i).getField("EN_CLE").toString().trim()))).trim()
                  + "' WHERE EN_CLE = '" + liste.get(i).getField("EN_CLE").toString().trim() + "' AND EN_ETB = '"
                  + request.getParameter("etbCours") + "'  ");
          if (res >= -1) {
            nbModif++;
          }
        }
        else {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE").toString().trim() + "' AND EN_ETB = '" + request.getParameter("etbCours") + "' ");
          if (res >= -1) {
            nbModif++;
          }
        }
      }
      
      if (liste.size() == nbModif) {
        retour = true;
      }
    }
    // Ils n'existent pas encore pour cet établissement
    else if (liste != null && liste.size() == 0) {
      liste = recupererPatternParametresEtb(utilisateur);
      if (liste != null && liste.size() > 0) {
        for (int i = 0; i < liste.size(); i++) {
          if ((liste.get(i).getField("EN_TYP").toString().trim().equals("MAJUSCULE"))) {
            if (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim()) != null) {
              res = utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
                  + ".ENVIRONM (EN_CLE,EN_VAL,EN_TYP,EN_GEST,EN_ETB,EN_LIB) VALUES ('" + liste.get(i).getField("EN_CLE").toString().trim()
                  + "','" + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim().toUpperCase() + "','"
                  + liste.get(i).getField("EN_TYP").toString().trim() + "','2','" + request.getParameter("etbCours") + "', '') ");
            }
          }
          else if ((liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))) {
            if (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim()) != null) {
              res = utilisateur.getAccesDB2()
                  .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
                      + ".ENVIRONM (EN_CLE,EN_VAL,EN_TYP,EN_GEST,EN_ETB,EN_LIB) VALUES ('"
                      + liste.get(i).getField("EN_CLE").toString().trim() + "','"
                      + Base64Coder
                          .encodeString(Base64Coder.encodeString(request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())))
                          .trim()
                      + "','" + liste.get(i).getField("EN_TYP").toString().trim() + "','2','" + request.getParameter("etbCours")
                      + "', '') ");
            }
          }
          else {
            res = utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
                + ".ENVIRONM (EN_CLE,EN_VAL,EN_TYP,EN_GEST,EN_ETB,EN_LIB) VALUES ('" + liste.get(i).getField("EN_CLE").toString().trim()
                + "','" + (request.getParameter(liste.get(i).getField("EN_CLE").toString().trim())).trim() + "','"
                + liste.get(i).getField("EN_TYP").toString().trim() + "','2','" + request.getParameter("etbCours") + "', '') ");
          }
        }
        
        return true;
      }
    }
    
    return retour;
    
  }
  
}
