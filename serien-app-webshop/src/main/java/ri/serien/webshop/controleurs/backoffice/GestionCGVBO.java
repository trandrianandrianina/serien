/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionCGVBO extends Gestion {
  
  /**
   * Afficher la Récuperation des GCV des filiales
   */
  public ArrayList<GenericRecord> recupererCgv(UtilisateurWebshop utilisateur, String langue) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || langue == null) {
      Trace.erreur("Utilisateur ou LANGUE à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT CGV_NAME FROM " + MarbreEnvironnement.BIBLI_WS + ".CGV WHERE CGV_LAN = '" + langue + "'");
  }
  
  /**
   * Mise à jour de la table des conditions générales de vente [CGV]
   */
  public int miseAjourCgv(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null || request == null) {
      Trace.erreur("Utilisateur Request à NUll");
      return res;
    }
    
    // On supprime d'éventuels enregistrements pour ce code langue
    if (request.getParameter("langueInfo") != null) {
      res = utilisateur.getAccesDB2()
          .requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".CGV WHERE CGV_LAN = '" + request.getParameter("langueInfo") + "'");
    }
    
    // On rajoute un nouvel enregsitrement pour ce code langue
    if (res > -1) {
      res = utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".CGV (cgv_name,CGV_LAN) VALUES ('"
          + traiterCaracteresSpeciauxSQL(request.getParameter("conditions")) + "','" + request.getParameter("langueInfo") + "')");
    }
    
    return res;
  }
}
