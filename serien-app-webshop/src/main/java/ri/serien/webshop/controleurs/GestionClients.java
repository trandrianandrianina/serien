/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Client;
import ri.serien.webshop.metier.Panier;

/**
 * Classe permettant de gérer les données propores à la vue "CLients"
 * 
 * @author David
 * 
 */
public class GestionClients extends Gestion {
  /**
   * retourner les clients par mot clé ou par défaut sur les clients du représentant
   */
  public ArrayList<GenericRecord> retournerListeClients(UtilisateurWebshop utilisateur, String expression) {
    if (utilisateur == null) {
      Trace.erreur("retournerListeClients() utilisateur à null ");
      return null;
    }
    if (expression != null && !expressionIsOk(expression)) {
      return null;
    }
    
    // Si on est en saisie d'expression
    if (expression != null) {
      expression = traiterCaracteresSpeciauxSQL(expression.toUpperCase());
      return utilisateur.getAccesDB2()
          .select("" + "SELECT CLCLI,CLLIV,CLETB,CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM " + " WHERE CLETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND UPPER(CLNOM) LIKE '%" + expression + "%' OR CLCLI LIKE '"
              + expression + "%' " + " ORDER BY CLNOM FETCH FIRST " + utilisateur.getEtablissementEnCours().getLimit_requete_article()
              + " ROWS ONLY OPTIMIZE FOR " + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS "
              + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    // Si on est en mode clients du représentant
    else if (utilisateur.getUS_REPR() != null) {
      return utilisateur.getAccesDB2()
          .select("" + "SELECT CLCLI,CLLIV,CLETB,CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM " + " WHERE CLETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND (CLREP='" + utilisateur.getUS_REPR() + "' OR CLREP2='"
              + utilisateur.getUS_REPR() + "') " + " ORDER BY CLNOM FETCH FIRST "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      // Sinon Pas de code représentant (c'est bizarre quand même...)
    }
    else {
      return utilisateur.getAccesDB2()
          .select("" + "SELECT CLCLI,CLLIV,CLETB,CLNOM FROM " + utilisateur.getBibli() + ".PGVMCLIM " + " WHERE CLETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' " + " ORDER BY CLNOM FETCH FIRST "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
  }
  
  /**
   * Attribuer un client au représentant
   */
  public boolean attribuerUnClient(UtilisateurWebshop utilisateur, String client, String suffixe) {
    if (utilisateur == null || client == null || suffixe == null) {
      return false;
    }
    
    // On checke si on a bien un code représentant associé à cet uitlisateur
    if (utilisateur.getUS_REPR() != null) {
      // utilisateur.setClient(new Utilisateur(utilisateur.getSessionEnCours()));
      utilisateur.setClient(new Client(utilisateur, utilisateur.getEtablissementEnCours(), client, suffixe));
      if (utilisateur.getClient() != null) {
        utilisateur.setMonPanier(new Panier(utilisateur));
        
        return true;
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
  }
  
  /**
   * Retirer le client à la session du représentant
   */
  public boolean retirerLeClient(UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      return false;
    }
    
    utilisateur.setClient(null);
    utilisateur.setMonPanier(null);
    
    return true;
  }
  
  /**
   * Contrôle la validité de l'expression
   */
  public boolean expressionIsOk(String expression) {
    int saisieMinimum = 3;
    int saisieMaximum = 30;
    boolean isOk = false;
    
    if (expression != null && expression.trim().length() >= saisieMinimum && expression.trim().length() <= saisieMaximum) {
      isOk = true;
    }
    
    return isOk;
  }
}
