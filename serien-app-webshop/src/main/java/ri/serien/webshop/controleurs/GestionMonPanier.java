/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.TableBDD;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.CalculStocks;
import ri.serien.webshop.metier.PanierArticleNormal;
import ri.serien.webshop.outils.Outils;

public class GestionMonPanier extends Gestion {
  private CalculStocks calculDuStock = null;
  
  /**
   * Mettre à jour les informations de récupération propres aux panier de
   * l'utilisateur
   */
  public boolean majModeRecuperation(UtilisateurWebshop utilisateur, String valeur) {
    if (utilisateur == null || utilisateur.getMonPanier() == null || valeur == null) {
      return false;
    }
    try {
      utilisateur.getMonPanier().setModeRecup(Integer.parseInt(valeur));
      if (Integer.parseInt(valeur) == MarbreEnvironnement.MODE_RETRAIT) {
        if (utilisateur.getClient().getMagasinClient() != null) {
          utilisateur.getMonPanier().setMagasinPanier(utilisateur.getClient().getMagasinClient());
        }
        else {
          utilisateur.getMonPanier().setMagasinPanier(utilisateur.getMagasinSiege());
        }
        utilisateur.getMonPanier().setDateRecuperation(Outils.recupererDateCouranteInt());
      }
      else if (Integer.parseInt(valeur) == MarbreEnvironnement.MODE_LIVRAISON) {
        utilisateur.getMonPanier().setMagasinPanier(utilisateur.getMagasinSiege());
        utilisateur.getMonPanier()
            .setDateRecuperation(Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
                utilisateur.getEtablissementEnCours().getDelai_mini_livraison())));
      }
      
      return true;
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
      return false;
    }
  }
  
  /**
   * Permet de récupérer la liste des magasins de la table MAGASINS de la bibli XWEBSHOP (extraction commerciale de
   * celle de la bibli client)
   */
  public ArrayList<GenericRecord> recupererListeMagasins(UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    ArrayList<GenericRecord> liste = null;
    
    liste =
        utilisateur.getAccesDB2().select("SELECT MG_ID, MG_COD, MG_NAME FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB='"
            + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND MG_ACTIF = '1' " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    return liste;
  }
  
  /**
   * Retourne le stock pour un article
   */
  public float retourneStockPourUnArticle(UtilisateurWebshop utilisateur, String etb, String article, String magasin) {
    etb = traiterCaracteresSpeciauxSQL(etb);
    article = traiterCaracteresSpeciauxSQL(article);
    
    if (utilisateur == null || etb == null || article == null || magasin == null) {
      return Float.parseFloat("0");
    }
    
    if (calculDuStock == null) {
      calculDuStock = new CalculStocks();
    }
    
    if (calculDuStock != null) {
      try {
        return Float.parseFloat(calculDuStock.retournerStock(utilisateur, etb.trim(), article.trim(), magasin));
      }
      catch (NumberFormatException e) {
        Trace.erreur(e.getMessage());
        return Float.parseFloat("0");
      }
    }
    else {
      return Float.parseFloat("0");
    }
  }
  
  /**
   * retourne le libellé d'un article
   */
  public boolean majInfosArticle(UtilisateurWebshop utilisateur, PanierArticleNormal article) {
    boolean retour = false;
    
    if (utilisateur == null || article == null) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste = null;
    // récupère le libellé de l'article
    liste = utilisateur.getAccesDB2()
        .select("SELECT A1LIB, A1UNV, CAREF, SUBSTR(PARZ2, 82, 1) as DECIMQ " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "."
            + TableBDD.VUE_ART_UT + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_PARAMETRES_GVM
            + " ON PARTYP ='UN' AND PARIND=A1UNV " + " WHERE A1ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB()
            + "' AND A1ART ='" + article.getA1art() + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE
            + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    // On met à jour l'article
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("A1LIB")) {
        article.setLibelleArt(liste.get(0).getField("A1LIB").toString().trim());
      }
      if (liste.get(0).isPresentField("A1UNV")) {
        article.setUniteArt(liste.get(0).getField("A1UNV").toString().trim());
      }
      if (liste.get(0).isPresentField("CAREF")) {
        article.setRefFournisseur(liste.get(0).getField("CAREF").toString().trim());
      }
      if (liste.get(0).isPresentField("DECIMQ")) {
        int nbDecimalesQ = 0;
        try {
          nbDecimalesQ = Integer.parseInt(liste.get(0).getField("DECIMQ").toString().trim());
        }
        catch (NumberFormatException e) {
          nbDecimalesQ = 0;
        }
        article.setNbDecimalesQ(nbDecimalesQ);
      }
    }
    
    return retour;
  }
  
  /**
   * Mettre à jour le variables Client du panier avec les infos du bloc adresse
   */
  public boolean majVariablesPanier(UtilisateurWebshop utilisateur) {
    boolean isOk = false;
    
    if (utilisateur == null || utilisateur.getClient() == null) {
      return isOk;
    }
    
    if (utilisateur.getClient().getNomClient() != null) {
      utilisateur.getMonPanier().setCLNOM(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getNomClient()));
    }
    if (utilisateur.getClient().getComplementClient() != null) {
      utilisateur.getMonPanier().setCLCPL(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getComplementClient()));
    }
    if (utilisateur.getClient().getRueClient() != null) {
      utilisateur.getMonPanier().setCLRUE(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getRueClient()));
    }
    if (utilisateur.getClient().getLocaliteClient() != null) {
      utilisateur.getMonPanier().setCLLOC(traiterCaracteresSpeciauxSQL(utilisateur.getClient().getLocaliteClient()));
    }
    if (utilisateur.getClient().getCodePostalClient() != null) {
      if (!utilisateur.getClient().getCodePostalClient().trim().equals("")) {
        utilisateur.getMonPanier().setCLPOS(utilisateur.getClient().getCodePostalClient());
      }
      else if (utilisateur.getClient().getVilleClient() != null && utilisateur.getClient().getVilleClient().length() >= 5) {
        utilisateur.getMonPanier().setCLPOS(utilisateur.getClient().getVilleClient().substring(0, 5).trim());
      }
    }
    if (utilisateur.getClient().getVilleClient() != null && utilisateur.getClient().getVilleClient().length() >= 6) {
      utilisateur.getMonPanier().setCLVIL(utilisateur.getClient().getVilleClient().substring(6).trim());
    }
    
    return isOk;
  }
  
  /**
   * Mettre à jour le variables Client du panier avec les infos du bloc adresse
   */
  public boolean majVariablesPanier(UtilisateurWebshop utilisateur, String cle, String valeur) {
    if (utilisateur == null || cle == null || valeur == null) {
      return false;
    }
    
    valeur = valeur.trim();
    
    if (cle.equals("idCLNOM")) {
      utilisateur.getMonPanier().setCLNOM(valeur);
    }
    else if (cle.equals("idCLCPL")) {
      utilisateur.getMonPanier().setCLCPL(valeur);
    }
    else if (cle.equals("idCLRUE")) {
      utilisateur.getMonPanier().setCLRUE(valeur);
    }
    else if (cle.equals("idCLLOC")) {
      utilisateur.getMonPanier().setCLLOC(valeur);
    }
    else if (cle.equals("idCLVIL")) {
      utilisateur.getMonPanier().setCLVIL(valeur);
    }
    else if (cle.equals("idCLPOS")) {
      utilisateur.getMonPanier().setCLPOS(valeur);
    }
    else if (cle.equals("idDetailsRetrait")) {
      utilisateur.getMonPanier().setDetailsRecuperation(valeur);
    }
    else if (cle.equals("idDateRetrait")) {
      utilisateur.getMonPanier().setDateRecuperation(Outils.transformerDateHumaineEnSeriem(valeur));
    }
    else if (cle.equals("idReferenceLongue")) {
      utilisateur.getMonPanier().setE1RCC(valeur.toUpperCase());
    }
    
    return true;
  }
}
