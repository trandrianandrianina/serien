/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionGoogleAnalyticsBO extends Gestion {
  
  /**
   * Retourne le code de suivi présent dans la table Google
   */
  
  public ArrayList<GenericRecord> recupereCodeSuivi(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("[recupereCodeSuivi()] utilisateur à NULL");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT GO_ID,GO_ANALY FROM " + MarbreEnvironnement.BIBLI_WS + ".GOOGLE");
  }
  
  /**
   * Mise à jour de la table Google
   */
  
  public int miseaJourGoogle(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    int res = 0;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return -1;
    }
    GenericRecord rcd = new GenericRecord();
    String[] requiredfield = { "GO_ANALY" };
    rcd.setRequiredField(requiredfield);
    
    rcd.setField("GO_ANALY", request.getParameter("code_google"), true);
    
    // on delete l'enregistrement et on me recrée
    res = utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".GOOGLE");
    // si le delete c'est bien passé on ajoute le nouvel enregistrement
    
    if (res > -1) {
      res = utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + " .GOOGLE (GO_ANALY) VALUES" + "('"
          + traiterCaracteresSpeciauxSQL(request.getParameter("code_google")) + "')");
      
    }
    return res;
    
  }
}
