/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.exploitation.mail.EnumNomVariableMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.EnumTypeDocument;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.TableBDD;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Magasin;
import ri.serien.webshop.modeles.DuplicDevis;
import ri.serien.webshop.outils.Outils;
import ri.serien.webshop.outils.mail.ParametreMail;

public class GestionMesDonnees extends Gestion {
  private static final String ENTETE_LISTE =
      "height:26px; font-size:14px; font-weight:bold; text-align:center; color:white; background-color:#7C7C7C;";
  private static final String TABLE_LISTE_ARTICLE = "width:100%; max-width:1200px;";
  private static final String REF_ARTICLE_MAIL = "height:26px; width:20%; font-size:14px;";
  private static final String LIB_ARTICLE_MAIL = "height:26px; width:35%; font-size:14px;";
  private static final String QTE_ARTICLE_MAIL = "height:26px; width:8%; font-size:14px; text-align:right;";
  private static final String MULTIPLE_MAIL = "height:26px; width:5%; font-size:14px; text-align:right;";
  private static final String TARIF_ARTICLE_MAIL = "height:26px; width:16%; font-size:14px; text-align:right;";
  private static final String TOTAL_COMMANDE_MAIL =
      "height:26px; font-size:14px; font-weight:bold; text-align:right; border-top:1px solid #7C7C7C;";
  
  // Messages
  private static final String MESSAGE_GRAS = "font-size:14px; font-weight:bold;";
  private static final String MESSAGE = "font-size:14px;";
  
  /**
   * Retourne les commandes du clients
   */
  public ArrayList<GenericRecord> recupererCommandesClient(UtilisateurWebshop pUtilisateur, String pNumeroClient, String pNumeroLivraison,
      String pTypeCommande) {
    
    if (pUtilisateur == null || pUtilisateur.getEtablissementEnCours() == null || pUtilisateur.getAccesDB2() == null) {
      Trace.erreur("L'établissement en cours ou l'accès en DB2 est invalide.");
      return null;
    }
    if (pNumeroClient == null || pNumeroLivraison == null) {
      Trace.erreur("Information Client invalide.");
      return null;
    }
    
    pUtilisateur.setDernierTypeBon(pTypeCommande);
    String multiplicateur = "1";
    // Franc pacifique
    if (pUtilisateur.getEtablissementEnCours().getDecimales_client() == 0) {
      multiplicateur = "100";
    }
    
    if (pUtilisateur.getClient() != null) {
      String requete = "SELECT E1COD, DIGITS(E1NUM) AS E1NUM , E1SUF, E1CRE, E1DLP, (E1THTL*" + multiplicateur
          + ") AS E1THTL, E1ETA, E1TDV, E1DAT2, E1IN18, E1TOP, E1NCC, E1RCC " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS
          + ".PGVMEBCM " + " WHERE E1ETB = '" + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND E1CLFP = " + pNumeroClient
          + " AND E1CLFS=" + pNumeroLivraison + " AND E1COD='" + pTypeCommande + "' " + " ORDER BY E1CRE DESC,E1NUM DESC ,E1SUF DESC "
          + " FETCH FIRST " + pUtilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
          + pUtilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE;
      
      // Si notre utilisateur ne peut voir que les commandes passées par lui-même (paramétré dans la fiche client)
      if (!pUtilisateur.isCommandesEtablissementVisible()) {
        requete = "SELECT E1COD, DIGITS(E1NUM) AS E1NUM , E1SUF, E1CRE, E1DLP, (E1THTL*" + multiplicateur
            + ") AS E1THTL, E1ETA, E1TDV, E1DAT2, E1IN18, E1TOP, E1NCC, E1RCC " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMXLIM "
            + " ON XICOD = E1COD AND XIETB = E1ETB AND XINUM = E1NUM AND XISUF = E1SUF " + " WHERE E1ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' " + " AND E1COD = '" + pTypeCommande + "' AND XITYP = '72' "
            + " AND XILIB = " + pUtilisateur.getIdContact() + " " + " ORDER BY E1CRE DESC,E1NUM DESC ,E1SUF DESC " + " FETCH FIRST "
            + pUtilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
            + pUtilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE;
      }
      
      return pUtilisateur.getAccesDB2().select(requete);
    }
    
    return null;
  }
  
  /**
   * Retourne le detail d'une commande
   */
  public ArrayList<GenericRecord> recupererDetailCommande(UtilisateurWebshop pUtilisateur, String pReferenceCommande,
      boolean pModeReprise) {
    String numeroCommande = null;
    String suffixeCommande = null;
    String typeCommande = null;
    
    if (pReferenceCommande == null || pUtilisateur == null || pUtilisateur.getEtablissementEnCours() == null
        || pUtilisateur.getAccesDB2() == null) {
      return null;
    }
    
    if (pReferenceCommande.length() == 8) {
      typeCommande = pReferenceCommande.substring(0, 1);
      numeroCommande = pReferenceCommande.substring(1, 7);
      suffixeCommande = pReferenceCommande.substring(7, 8);
    }
    
    ArrayList<GenericRecord> liste = null;
    String multiplicateur = "1";
    // Franc pacifique
    if (pUtilisateur.getEtablissementEnCours().getDecimales_client() == 0) {
      multiplicateur = "100";
    }
    
    liste = pUtilisateur.getAccesDB2()
        .select("SELECT L1ETB, L1ART, CAREF, A1LIB, L1UNV, SUBSTR(PARZ2, 82, 1) as DECIMQ, L1QTE*L1KSV as L1QTE, CND, (L1PVC*"
            + multiplicateur + ") AS L1PVC, (L1MHT*" + multiplicateur + ") AS L1MHT, L1ERL, L1NLI, E1ETA , E1TDV, E1DAT2, (E1THTL*"
            + multiplicateur + ") AS E1THTL, E1RCC, E1NCC, E1IN18 " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMLBCM "
            + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_PARAMETRES_GVM
            + " ON PARTYP ='UN' AND PARIND=L1UNV " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".VUE_ART_UT ON L1ETB=A1ETB AND L1ART=A1ART " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM ON L1ETB=E1ETB AND L1NUM=E1NUM AND L1SUF=E1SUF AND L1COD=E1COD " + " WHERE L1ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND L1COD='" + typeCommande + "' AND L1NUM = " + numeroCommande
            + " AND L1SUF=" + suffixeCommande + " AND L1ERL IN ('C' , 'S') " + " ORDER BY L1NLI " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    GestionCatalogue gestionCatalogue = null;
    if (liste != null) {
      if (pModeReprise) {
        gestionCatalogue = new GestionCatalogue();
        pUtilisateur.controleGestionTarif();
      }
      
      for (GenericRecord ligne : liste) {
        if (ligne.isPresentField("L1ART")) {
          if (pModeReprise && gestionCatalogue != null) {
            // Tarif recalculé
            ligne.setField("TARIF",
                gestionCatalogue.retournerTarifClient(pUtilisateur, pUtilisateur.getEtablissementEnCours().getIdEtablissement(),
                    Constantes.convertirObjetEnTexte(ligne.getField("L1ART")),
                    Constantes.convertirObjetEnTexte(ligne.getField("L1UNV"))));
            
            Trace.debug("[recupererDetailCommande] Article: " + Constantes.convertirObjetEnTexte(ligne.getField("L1ART")) + " -> TARIF: |"
                + Constantes.convertirObjetEnTexte(ligne.getField("TARIF")) + "|");
          }
          
          // Tarif L1PVC en mode reprise de commande
          if (ligne.isPresentField("L1PVC")) {
            // Cas de l'unité * il faut diviser par 100 pour l'affichage
            BigDecimal prix = null;
            if (ligne.isPresentField("L1UNV") && Constantes.convertirObjetEnTexte(ligne.getField("L1UNV")).endsWith("*")) {
              prix = ((BigDecimal) ligne.getField("L1PVC")).divide(new BigDecimal(100));
              ligne.setField("L1PVC", prix);
            }
          }
          
          Trace.debug("[recupererDetailCommande] Article: " + Constantes.convertirObjetEnTexte(ligne.getField("L1ART")) + " -> L1PVC: |"
              + Constantes.convertirObjetEnTexte(ligne.getField("L1PVC")) + "|");
          
          // Tarif L1QTE en mode reprise de commande
          if (ligne.isPresentField("L1QTE")) {
            
            BigDecimal quantite = new BigDecimal(Constantes.convertirObjetEnTexte(ligne.getField("L1QTE")));
            int nbDecimales = 0;
            if (ligne.isPresentField("DECIMQ")) {
              nbDecimales = Constantes.convertirTexteEnInteger((String) ligne.getField("DECIMQ"));
            }
            quantite = quantite.setScale(nbDecimales, RoundingMode.HALF_UP);
            Trace.info("Quantite recupérée: " + quantite.toString());
            ligne.setField("L1QTE", quantite);
          }
        }
      }
    }
    
    return liste;
  }
  
  /**
   * Reprend la commande et l'attribue au panier
   * On récupère la commande ou le devis sur la base de son numéro
   * On vide le panier actuel et remplit un nouveau panier avec ces données
   * Si on est en mode isReprise on vérouille le bon initial sinon il s'agit d'un nouveau bon
   */
  public boolean repriseCommande(UtilisateurWebshop utilisateur, String numCde, boolean isReprise) {
    // TODO IL FAUDRAIT VERIFIER D'ABORD QUI FAIT CETTE REPRISE
    BigDecimal quantite = BigDecimal.ZERO;
    
    boolean retour = false;
    if (utilisateur == null || numCde == null) {
      return retour;
    }
    
    // Récup des données de l'entête du bon
    GenericRecord enTete = recupererEnteteCommande(utilisateur, numCde);
    if (enTete != null) {
      if (isReprise) {
        utilisateur.getMonPanier().setNumCommandeSerieM(numCde);
      }
      utilisateur.getMonPanier().videPanier(false);
      
      if (enTete.isPresentField("E1MEX")) {
        if (enTete.isPresentField("TYPECDE")) {
          // Mode devis
          if (Constantes.convertirObjetEnTexte(enTete.getField("TYPECDE")).equals("D")) {
            utilisateur.getMonPanier().setModeRecup(MarbreEnvironnement.MODE_DEVIS);
          }
          else {
            if (Constantes.convertirObjetEnTexte(enTete.getField("E1MEX")).equals("WE")) {
              utilisateur.getMonPanier().setModeRecup(MarbreEnvironnement.MODE_RETRAIT);
            }
            if (Constantes.convertirObjetEnTexte(enTete.getField("E1MEX")).equals("WL")) {
              utilisateur.getMonPanier().setModeRecup(MarbreEnvironnement.MODE_LIVRAISON);
            }
          }
        }
      }
      
      if (enTete.isPresentField("E1MAG") && !utilisateur.getEtablissementEnCours().is_mono_magasin()) {
        utilisateur.getMonPanier().setMagasinPanier(new Magasin(utilisateur, Constantes.convertirObjetEnTexte(enTete.getField("E1MAG"))));
      }
      
      if (isReprise) {
        if (enTete.isPresentField("E1DLS")) {
          utilisateur.getMonPanier().setDateRecuperation(Integer.parseInt(Constantes.convertirObjetEnTexte(enTete.getField("E1DLS"))));
        }
        
        // référence courte
        if (enTete.isPresentField("E1RCC")) {
          utilisateur.getMonPanier().setE1RCC(Constantes.convertirObjetEnTexte(enTete.getField("E1RCC")));
        }
      }
      else {
        int dateRecuperation = 0;
        if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
          if (Outils.recupererDateCouranteInt() > dateRecuperation) {
            dateRecuperation = Outils.recupererDateCouranteInt();
          }
        }
        else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
          if (Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
              utilisateur.getEtablissementEnCours().getDelai_mini_livraison())) > dateRecuperation) {
            dateRecuperation = Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
                utilisateur.getEtablissementEnCours().getDelai_mini_livraison()));
          }
        }
        else {
          if (Outils.recupererDateCouranteInt() > dateRecuperation) {
            dateRecuperation = Outils.recupererDateCouranteInt();
          }
        }
        
        utilisateur.getMonPanier().setDateRecuperation(dateRecuperation);
      }
      
      if (enTete.isPresentField("CLINOM")) {
        utilisateur.getMonPanier().setCLNOM(Constantes.convertirObjetEnTexte(enTete.getField("CLINOM")));
      }
      if (enTete.isPresentField("CLICPL")) {
        utilisateur.getMonPanier().setCLCPL(Constantes.convertirObjetEnTexte(enTete.getField("CLICPL")));
      }
      if (enTete.isPresentField("CLIRUE")) {
        utilisateur.getMonPanier().setCLRUE(Constantes.convertirObjetEnTexte(enTete.getField("CLIRUE")));
      }
      if (enTete.isPresentField("CLILOC")) {
        utilisateur.getMonPanier().setCLLOC(Constantes.convertirObjetEnTexte(enTete.getField("CLILOC")));
      }
      
      // Code postal + ville
      if (enTete.isPresentField("CLICDP")) {
        utilisateur.getMonPanier().setCLPOS(Outils.affichageHTML_speciaux(Constantes.convertirObjetEnTexte(enTete.getField("CLICDP"))));
      }
      if (enTete.isPresentField("CLIVIL")) {
        utilisateur.getMonPanier().setCLVIL(Outils.affichageHTML_speciaux(Constantes.convertirObjetEnTexte(enTete.getField("CLIVIL"))));
      }
    }
    // Récup du détail du bon avec la liste d'articles concernés
    ArrayList<GenericRecord> listeDetailCommande = recupererDetailCommande(utilisateur, numCde, true);
    
    if (listeDetailCommande != null && listeDetailCommande.size() > 0) {
      for (GenericRecord record : listeDetailCommande) {
        quantite = new BigDecimal(Constantes.convertirBigDecimalEnTexte((BigDecimal) record.getField("L1QTE")));
        // On vérifie l'existence d'un véritable article dans cette liste
        if (record.isPresentField("L1ETB") && record.isPresentField("L1ART") && record.isPresentField("TARIF")
            && record.isPresentField("CND") && record.isPresentField("L1UNV") && record.isPresentField("L1ERL")) {
          utilisateur.getMonPanier().ajouterArticle(Constantes.convertirObjetEnTexte(record.getField("L1ETB")),
              Constantes.convertirObjetEnTexte(record.getField("L1ART")), quantite,
              new BigDecimal(Constantes.convertirObjetEnTexte(record.getField("TARIF"))),
              Constantes.convertirObjetEnTexte(record.getField("CND")), Constantes.convertirObjetEnTexte(record.getField("L1UNV")),
              Constantes.convertirObjetEnTexte(record.getField("L1ERL")));
        }
      }
      
      if (isReprise) {
        verouilleCommande(utilisateur, numCde);
      }
      
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Annule le document dont la référence est passée en paramètre
   * .
   * 
   * @param pUtilisateur Information sur l'utilisateur connecté
   * @param pNumeroDocumentAvecCode Référence du document à annuler
   */
  public void annulationCommande(UtilisateurWebshop pUtilisateur, String pNumeroDocumentAvecCode) {
    if (pUtilisateur == null || pNumeroDocumentAvecCode == null) {
      return;
    }
    // TODO IL FAUDRAIT VERIFIER D'ABORD QUI FAIT CETTE ANNULATION
    pUtilisateur.controleGestionCommandes();
    
    if (pUtilisateur.getGestionCommandes() != null && pNumeroDocumentAvecCode.length() == 8) {
      try {
        String numeroDocument = pNumeroDocumentAvecCode.substring(1, 7);
        String suffixeDocument = pNumeroDocumentAvecCode.substring(7, 8);
        String typeDocument = pNumeroDocumentAvecCode.substring(0, 1);
        
        if (pUtilisateur.getGestionCommandes().execute(typeDocument, pUtilisateur.getEtablissementEnCours().getCodeETB(),
            new BigDecimal(numeroDocument), new BigDecimal(suffixeDocument))) {
          if (pUtilisateur.getEtablissementEnCours().isOk_envois_mails()) {
            if (typeDocument.equals("D")) {
              typeDocument = EnumTypeDocument.DEVIS.getLibelle();
            }
            else {
              typeDocument = EnumTypeDocument.COMMANDE.getLibelle();
            }
            
            // Préparation des paramètres du mail d'annulation
            ParametreMail parametreMail = preparerParametreMailAnnulationDocument(pNumeroDocumentAvecCode.substring(1, 8), typeDocument);
            // Envoyer le mail d'annulation de document
            pUtilisateur.envoyerMail(pUtilisateur.getLogin(), parametreMail, EnumTypeMail.MAIL_ANNULATION_COMMANDE_HTML);
          }
        }
      }
      catch (Exception e) {
        Trace.erreur(e.getMessage());
      }
    }
  }
  
  /**
   * Retourne l'entête de ma commande
   */
  public GenericRecord recupererEnteteCommande(UtilisateurWebshop pUtilisateur, String pReferenceCommande) {
    if (pUtilisateur == null || pUtilisateur.getEtablissementEnCours() == null || pUtilisateur.getAccesDB2() == null) {
      return null;
    }
    
    if (pReferenceCommande == null || pReferenceCommande.length() != 8) {
      return null;
    }
    
    String typeCommande = pReferenceCommande.substring(0, 1);
    String numeroCommande = pReferenceCommande.substring(1, 7);
    String suffixeCommande = pReferenceCommande.substring(7, 8);
    
    ArrayList<GenericRecord> listeEnTeteCommande = null;
    String multiplicateur = "1";
    // Franc pacifique
    if (pUtilisateur.getEtablissementEnCours().getDecimales_client() == 0) {
      multiplicateur = "100";
    }
    
    listeEnTeteCommande = pUtilisateur.getAccesDB2()
        .select("SELECT E1COD, E1MAG, E1MEX, E1DLS, E1DLP, E1ETA, E1TDV, E1DAT2, E1CRE, (E1THTL*" + multiplicateur
            + ") AS E1THTL, E1RCC, E1NCC, E1IN18, " + " CASE WHEN AVNOM IS NOT NULL THEN AVNOM ELSE CLNOM END AS CLINOM, "
            + " CASE WHEN AVCPL IS NOT NULL THEN AVCPL ELSE CLCPL END AS CLICPL, "
            + " CASE WHEN AVRUE IS NOT NULL THEN AVRUE ELSE CLRUE END AS CLIRUE, "
            + " CASE WHEN AVLOC IS NOT NULL THEN AVLOC ELSE CLLOC END AS CLILOC, "
            + " CASE WHEN AVCDP IS NOT NULL THEN AVCDP ELSE SUBSTR(CLVIL, 1, 5) END AS CLICDP, "
            + " CASE WHEN AVVIL IS NOT NULL THEN AVVIL ELSE SUBSTR(CLVIL, 6, 24)  END AS CLIVIL " + " FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMCLIM ON E1ETB=CLETB AND E1CLFP=CLCLI AND E1CLFS=CLLIV" + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMADVM ON E1ETB=AVETB AND E1NUM=AVNUM AND E1SUF=AVSUF AND AVCOD = 'L' " + " WHERE E1ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND E1COD='" + typeCommande + "' AND E1NUM = " + numeroCommande
            + " AND E1SUF=" + suffixeCommande + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (listeEnTeteCommande != null && listeEnTeteCommande.size() > 0) {
      listeEnTeteCommande.get(0).setField("TYPECDE", typeCommande);
      return listeEnTeteCommande.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourne l'entête de ma commande
   */
  public ArrayList<GenericRecord> recupererCommentaireCommande(UtilisateurWebshop pUtilisateur, String pReferenceCommande) {
    if (pReferenceCommande == null || pReferenceCommande.length() != 8 || pUtilisateur == null || pUtilisateur.getAccesDB2() == null
        || pUtilisateur.getEtablissementEnCours() == null) {
      return null;
    }
    
    String typeCommande = pReferenceCommande.substring(0, 1);
    String numeroCommande = pReferenceCommande.substring(1, 7);
    String suffixeCommande = pReferenceCommande.substring(7, 8);
    
    ArrayList<GenericRecord> listeCommentaire = null;
    
    listeCommentaire = pUtilisateur.getAccesDB2()
        .select("SELECT XITYP, XILIB " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMLBCM " + " LEFT JOIN "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMXLIM ON L1ETB=XIETB AND L1NUM=XINUM AND L1SUF=XISUF AND L1NLI=XINLI "
            + " WHERE L1ETB = '" + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND L1COD='" + typeCommande + "' AND L1NUM = "
            + numeroCommande + " AND l1SUF=" + suffixeCommande + " AND L1ERL='*'" + " ORDER BY XINLI, XITYP "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    return listeCommentaire;
  }
  
  /**
   * Verrouille une commande reprise dans le panier
   */
  public int verouilleCommande(UtilisateurWebshop pUtilisateur, String pReferenceCommande) {
    if (pReferenceCommande == null || pReferenceCommande.length() != 8 || pUtilisateur == null || pUtilisateur.getAccesDB2() == null
        || pUtilisateur.getEtablissementEnCours() == null) {
      return -1;
    }
    
    String typeCommande = pReferenceCommande.substring(0, 1);
    String numeroCommande = pReferenceCommande.substring(1, 7);
    String suffixeCommande = pReferenceCommande.substring(7, 8);
    
    return pUtilisateur.getAccesDB2()
        .requete("UPDATE " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + "SET E1TOP=E1ETA+1 " + "WHERE E1ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND E1NUM = " + numeroCommande + " AND E1SUF=" + suffixeCommande
            + " AND E1COD='" + typeCommande + "'");
  }
  
  /**
   * Vérifie si la commande est en attente
   */
  public boolean isCommandeEnAttente(UtilisateurWebshop pUtilisateur, String pReferenceCommande) {
    boolean isEnAttente = false;
    
    if (pReferenceCommande == null || pReferenceCommande.length() != 8 || pUtilisateur == null
        || pUtilisateur.getEtablissementEnCours() == null || pUtilisateur.getAccesDB2() == null) {
      return isEnAttente;
    }
    
    String typeCommande = pReferenceCommande.substring(0, 1);
    String numeroCommande = pReferenceCommande.substring(1, 7);
    String suffixeCommande = pReferenceCommande.substring(7, 8);
    
    ArrayList<GenericRecord> listeCommande = null;
    
    listeCommande = pUtilisateur.getAccesDB2()
        .select("SELECT E1ETA " + "FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + "WHERE E1ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND E1COD='" + typeCommande + "' AND E1NUM = " + numeroCommande
            + " AND E1SUF=" + suffixeCommande + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (listeCommande != null && listeCommande.size() == 1) {
      isEnAttente = Constantes.convertirObjetEnTexte(listeCommande.get(0).getField("E1ETA")).equals("0");
    }
    
    return isEnAttente;
  }
  
  /**
   * On vérifie si le bon transmis est un devis validé !!
   */
  public boolean isDevisValide(UtilisateurWebshop pUtilisateur, String pReferenceBon) {
    boolean isValide = false;
    
    if (pReferenceBon == null || pReferenceBon.length() != 8 || pUtilisateur == null || pUtilisateur.getAccesDB2() == null
        || pUtilisateur.getEtablissementEnCours() == null) {
      return isValide;
    }
    String typeCommande = pReferenceBon.substring(0, 1);
    String numeroCommande = pReferenceBon.substring(1, 7);
    String suffixeCommande = pReferenceBon.substring(7, 8);
    
    ArrayList<GenericRecord> listeDevis = null;
    
    // On vérifie que le bon soit un devis (E1COD = 'D') et que le bon soit dans un état de type validé ou envoyé (1 ou 2)
    listeDevis = pUtilisateur.getAccesDB2()
        .select("SELECT E1TDV, E1DAT2, E1COD " + "FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBCM " + "WHERE E1ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND E1COD='" + typeCommande + "' AND E1NUM = " + numeroCommande
            + " AND E1SUF=" + suffixeCommande + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (listeDevis != null && listeDevis.size() == 1) {
      isValide = (Constantes.convertirObjetEnTexte(listeDevis.get(0).getField("E1TDV")).equals("1")
          || Constantes.convertirObjetEnTexte(listeDevis.get(0).getField("E1TDV")).equals("2"))
          && Constantes.convertirObjetEnTexte(listeDevis.get(0).getField("E1COD")).equals("D");
      if (isValide) {
        isValide = verifierDateValiditeDevis(pUtilisateur, Constantes.convertirObjetEnTexte(listeDevis.get(0).getField("E1DAT2")));
      }
    }
    
    return isValide;
    
  }
  
  /**
   * On vérifie si la date du devis est encore valable
   */
  public boolean verifierDateValiditeDevis(UtilisateurWebshop pUtilisateur, String pDateDevis) {
    boolean isValable = false;
    
    if (pDateDevis == null || pUtilisateur == null) {
      return isValable;
    }
    
    int dateDevisNumerique = -1;
    int dateJour = 0;
    
    try {
      dateDevisNumerique = Integer.parseInt(pDateDevis);
      dateJour = Outils.recupererDateCouranteInt();
      
      isValable = (dateDevisNumerique >= dateJour) || (dateDevisNumerique == 0);
      
    }
    catch (Exception e) {
      
    }
    
    return isValable;
  }
  
  /**
   * Transformer un devis en commande
   */
  public String transformerEnCommande(UtilisateurWebshop pUtilisateur, String pReferenceBon) {
    String CdeRetour = null;
    if (pUtilisateur == null || pReferenceBon == null) {
      return null;
    }
    
    pUtilisateur.controleDuplicationDevis();
    
    if (pUtilisateur.getDuplicationDevis() != null && pReferenceBon.length() == 8) {
      try {
        DuplicDevis duplicDevis = pUtilisateur.getDuplicationDevis();
        if (duplicDevis.execute(pReferenceBon.substring(0, 1), pUtilisateur.getEtablissementEnCours().getCodeETB(),
            new BigDecimal(pReferenceBon.substring(1, 7)), new BigDecimal(pReferenceBon.substring(7, 8)))) {
          if (duplicDevis.getERREURS() == DuplicDevis.PAS_ERREUR) {
            CdeRetour = duplicDevis.getCDE_RETOUR();
          }
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    return CdeRetour;
  }
  
  /**
   * Si on passe un devis on retourne l'éventuelle commande générée à partir du devis
   * Si on passe une commande on retourne l'éventuel devis lié si il existe
   */
  public String retournerDocumentLie(UtilisateurWebshop pUtilisateur, String pReferenceDocument) {
    if (pUtilisateur == null || pUtilisateur.getEtablissementEnCours() == null || pUtilisateur.getAccesDB2() == null
        || pReferenceDocument == null || pReferenceDocument.length() != 8) {
      return null;
    }
    
    String valeurDocRetour = null;
    String valeurType = pReferenceDocument.substring(0, 1);
    String valeurDocument = valeurType + pUtilisateur.getEtablissementEnCours().getCodeETB() + pReferenceDocument.substring(1, 8);
    String zoneRecherche = null;
    String zoneDocument = null;
    
    final String ZONE_DEVIS = "IXREF1";
    final String ZONE_CDE = "IXREF2";
    
    if (valeurType.equals("E")) {
      zoneRecherche = ZONE_DEVIS;
      zoneDocument = ZONE_CDE;
    }
    else if (valeurType.equals("D")) {
      zoneRecherche = ZONE_CDE;
      zoneDocument = ZONE_DEVIS;
    }
    else {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    String requete = "SELECT " + zoneRecherche + " " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMIDXM "
        + " WHERE IXTYP = 'DD' AND IXETB = '" + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND " + zoneDocument + " = '"
        + valeurDocument + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE;
    
    liste = pUtilisateur.getAccesDB2().select(requete);
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField(zoneRecherche)) {
        valeurDocRetour = Constantes.convertirObjetEnTexte(liste.get(0).getField(zoneRecherche));
      }
    }
    
    return valeurDocRetour;
  }
  
  /**
   * Prépare les paramètres du mail d'annulation de commande.
   * 
   * @param pNumeroDocument Numéro du document à annuler
   * @param pTypeDocument Type du document à annuler
   * @return
   */
  private ParametreMail preparerParametreMailAnnulationDocument(String pNumeroDocument, String pTypeDocument) {
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.INDICATIF_DOCUMENT, StringEscapeUtils.escapeHtml4(pNumeroDocument));
    parametreMail.ajouterParametre(EnumNomVariableMail.TYPE_DOCUMENT, StringEscapeUtils.escapeHtml4(pTypeDocument));
    
    return parametreMail;
  }
  
  /**
   * Prépare les paramètres du mail pour les commandes.
   * 
   * @param pUtilisateur Information sur l'utilisateur
   * @param pNumeroDocument Numéro du document
   * @param pSuffixeDocument Suffixe du document
   * @param pCodeDocument Code du document
   * @param pEnumTypeDocument Type du document
   * @param pIsRapport Indicatif si c'est un rapport ou une confirmation de document
   * @return
   */
  public ParametreMail preparerParametreMailPourCommande(UtilisateurWebshop pUtilisateur, String pNumeroDocument, String pSuffixeDocument,
      String pCodeDocument, EnumTypeDocument pEnumTypeDocument, boolean pIsRapport) {
    // En-Tête du document contenant les informations complémentaires du le client
    GenericRecord enTeteDocument = recupererEnteteCommande(pUtilisateur, pCodeDocument + pNumeroDocument + pSuffixeDocument);
    // Liste des articles qui compose le document
    ArrayList<GenericRecord> listeArticle =
        recupererDetailCommande(pUtilisateur, pCodeDocument + pNumeroDocument + pSuffixeDocument, false);
    // Phrase à afficher pour le mode de récupération
    String modeRecuperation =
        genererModeRecuperationCommande(pUtilisateur, retournerModeRecuperation(enTeteDocument, pCodeDocument), pIsRapport);
    String numeroClient = pUtilisateur.getClient().getNumeroClient();
    String mailClient = pUtilisateur.getLogin();
    String complementNomClient = Constantes.convertirObjetEnTexte(enTeteDocument.getField("CLICPL"));
    String rueClient = Constantes.convertirObjetEnTexte(enTeteDocument.getField("CLIRUE"));
    String localiteClient = Constantes.convertirObjetEnTexte(enTeteDocument.getField("CLILOC"));
    String commune = getCommuneClient(enTeteDocument);
    String referenceDocument = getReferenceCommande(enTeteDocument);
    String suffixeSujet = "d'un montant de  " + getMontantCommande(pUtilisateur, enTeteDocument);
    // Tableau HTML contenant la liste des articles
    String tableauArticle = genererTableauArticleDocument(pUtilisateur, enTeteDocument, listeArticle,
        retournerModeRecuperation(enTeteDocument, pCodeDocument));
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.TYPE_DOCUMENT, pEnumTypeDocument.getLibelle());
    parametreMail.ajouterParametre(EnumNomVariableMail.NUMERO_SUFFIXE_FORMATE,
        StringEscapeUtils.escapeHtml4(pNumeroDocument + "/" + pSuffixeDocument));
    parametreMail.ajouterParametre(EnumNomVariableMail.NOM_CLIENT,
        StringEscapeUtils.escapeHtml4(pUtilisateur.getClient().getNomClient()));
    parametreMail.ajouterParametre(EnumNomVariableMail.MODE_RECUPERATION, StringEscapeUtils.escapeHtml4(modeRecuperation));
    parametreMail.ajouterParametre(EnumNomVariableMail.COMPLEMENT_NOM_CLIENT, StringEscapeUtils.escapeHtml4(complementNomClient));
    parametreMail.ajouterParametre(EnumNomVariableMail.RUE_CLIENT, StringEscapeUtils.escapeHtml4(rueClient));
    parametreMail.ajouterParametre(EnumNomVariableMail.LOCALITE_CLIENT, StringEscapeUtils.escapeHtml4(localiteClient));
    parametreMail.ajouterParametre(EnumNomVariableMail.COMMUNE, StringEscapeUtils.escapeHtml4(commune));
    parametreMail.ajouterParametre(EnumNomVariableMail.REFERENCE_COMMANDE, StringEscapeUtils.escapeHtml4(referenceDocument));
    parametreMail.ajouterParametre(EnumNomVariableMail.TABLE_ARTICLE, tableauArticle);
    // Si c'est un rapport pour le gestionnaire, on a besoin de ses informations supplémentaires
    if (pIsRapport) {
      parametreMail.ajouterParametre(EnumNomVariableMail.NUMERO_CLIENT, StringEscapeUtils.escapeHtml4(numeroClient));
      parametreMail.ajouterParametre(EnumNomVariableMail.MAIL_CLIENT, StringEscapeUtils.escapeHtml4(mailClient));
      parametreMail.ajouterParametre(EnumNomVariableMail.SUFFIXE_SUJET, StringEscapeUtils.escapeHtml4(suffixeSujet));
    }
    
    return parametreMail;
  }
  
  /**
   * Retourne le montant de la commande.
   * 
   * @param pUtilisateur Information sur l'utilisateur
   * @param pEnTeteCommande En-Tête de la commande
   * @return
   */
  private String getMontantCommande(UtilisateurWebshop pUtilisateur, GenericRecord pEnTeteCommande) {
    String prix = "";
    if (pEnTeteCommande != null && pEnTeteCommande.isPresentField("E1THTL")) {
      prix = Outils.formaterUnTarifArrondi(pUtilisateur, pEnTeteCommande.getField("E1THTL"));
    }
    
    // Extraire le tarif
    String[] tarif = prix.split(" &euro;");
    // Déterminer le suffixe du sujet pour les rapports
    String montant = tarif[0];
    if (!pUtilisateur.getUS_DEV().equals("XPF")) {
      montant += " euros";
    }
    else {
      montant += " F";
    }
    
    return montant;
  }
  
  /**
   * Retourne le mode de récupération d'un document.
   * 
   * @param pEnTeteDocument En-Tête du document
   * @param pCodeDocument Code du document
   * @return
   */
  private int retournerModeRecuperation(GenericRecord pEnTeteDocument, String pCodeDocument) {
    int modeRecuperation = -1;
    
    // Tester la présence des informations de l'entête de la commande
    if (pEnTeteDocument != null && pEnTeteDocument.isPresentField("E1MEX")) {
      // Récupérer le mode de livraison
      String modeRecupString = Constantes.normerTexte((String) pEnTeteDocument.getField("E1MEX"));
      if (modeRecupString.equals("WL")) {
        modeRecuperation = MarbreEnvironnement.MODE_LIVRAISON;
      }
      else if (modeRecupString.equals("WE") && pCodeDocument.equals("E")) {
        modeRecuperation = MarbreEnvironnement.MODE_RETRAIT;
      }
      else if (modeRecupString.equals("WE") && pCodeDocument.equals("D")) {
        modeRecuperation = MarbreEnvironnement.MODE_DEVIS;
      }
    }
    
    return modeRecuperation;
  }
  
  /**
   * Génère le contenu du mode de récupération d'un document.
   * 
   * @param pUtilisateur Information sur l'utilisateur connecté
   * @param pModeRecuperation Mode de récupération d'un document
   * @return
   */
  private String genererModeRecuperationCommande(UtilisateurWebshop pUtilisateur, int pModeRecuperation, boolean pIsRapport) {
    String contenu = "";
    
    // Si le mode de livraison est le retrait en magasin
    if (pModeRecuperation == MarbreEnvironnement.MODE_RETRAIT) {
      // Si le document est un rapport pour le gestionnaire
      if (pIsRapport) {
        contenu += StringEscapeUtils.escapeHtml4("Le client a choisi de retirer la commande en magasin.");
      }
      // Si le document est un confirmation pour le client
      else {
        contenu += StringEscapeUtils.escapeHtml4("Vous avez choisi de retirer votre commande en magasin.");
      }
    }
    // Si le mode de livraison est l'expédition
    else if (pModeRecuperation == MarbreEnvironnement.MODE_LIVRAISON) {
      // Si le document est un rapport pour le gestionnaire
      if (pIsRapport) {
        contenu += StringEscapeUtils.escapeHtml4("Le client a choisi de se faire livrer la marchandise aux coordonnées ci-dessous :");
      }
      // Si le document est un confirmation pour le client
      else {
        contenu += StringEscapeUtils.escapeHtml4("Vous avez choisi de vous faire livrer la marchandise aux coordonnées ci-dessous :");
      }
    }
    
    return contenu;
  }
  
  /**
   * Extrait l'information sur la commune où se trouve le client.
   * 
   * @param pEnTeteCommande En-Tête du document
   * @return
   */
  private String getCommuneClient(GenericRecord pEnTeteCommande) {
    if (pEnTeteCommande == null) {
      return "";
    }
    
    String codePostalClient = Constantes.normerTexte(pEnTeteCommande.getField("CLICDP").toString());
    String villeClient = Constantes.normerTexte((String) pEnTeteCommande.getField("CLIVIL"));
    String commune = "";
    if (pEnTeteCommande.isPresentField("CLICDP") && pEnTeteCommande.isPresentField("CLIVIL")) {
      commune += StringEscapeUtils.escapeHtml4(codePostalClient + " " + villeClient);
    }
    else if (pEnTeteCommande.isPresentField("CLICDP")) {
      commune += StringEscapeUtils.escapeHtml4(codePostalClient);
    }
    else if (pEnTeteCommande.isPresentField("CLIVIL")) {
      commune += StringEscapeUtils.escapeHtml4(villeClient);
    }
    
    return commune;
  }
  
  /**
   * Extrait l'information sur la référence commande.
   * 
   * @param pEnTeteCommande En-Tête du document
   * @return
   */
  private String getReferenceCommande(GenericRecord pEnTeteCommande) {
    if (pEnTeteCommande == null) {
      return "";
    }
    
    String referenceCommande = "";
    // Ajouter la référence de la commande
    if (pEnTeteCommande.isPresentField("E1RCC")) {
      referenceCommande = Constantes.normerTexte((String) pEnTeteCommande.getField("E1RCC"));
      if (!referenceCommande.equals("")) {
        return "Référence commande : " + referenceCommande;
      }
    }
    return "";
  }
  
  /**
   * Génère le tableau sur les articles d'un document.
   * 
   * @param pUtilisateur Information sur l'utilisateur
   * @param pModeRecuperation Mode de récupération d'un document
   * @return
   */
  private String genererTableauArticleDocument(UtilisateurWebshop pUtilisateur, GenericRecord pEnTeteCommande,
      ArrayList<GenericRecord> pListeArticle, int pModeRecuperation) {
    String contenu = "";
    // Vérifier la présence d'une liste d'articles
    if (pListeArticle != null && pListeArticle.size() > 0) {
      // Tableau avec la liste des articles
      contenu += "<table style='" + TABLE_LISTE_ARTICLE + "'>";
      
      // Ajouter une ligne de titre au tableau
      contenu += "<tr>";
      contenu += "<td style='" + ENTETE_LISTE + "'>" + StringEscapeUtils.escapeHtml4("Code") + "</td>";
      contenu += "<td style='" + ENTETE_LISTE + "'>" + StringEscapeUtils.escapeHtml4("Libellé article") + "</td>";
      contenu += "<td style='" + ENTETE_LISTE + "'>" + StringEscapeUtils.escapeHtml4("Qté") + "</td>";
      contenu += "<td style='" + ENTETE_LISTE + "'></td>";
      contenu += "<td style='" + ENTETE_LISTE + "'>" + StringEscapeUtils.escapeHtml4("PU HT") + "</td>";
      contenu += "<td style='" + ENTETE_LISTE + "'>" + StringEscapeUtils.escapeHtml4("Montant HT") + "</td>";
      contenu += "</tr>";
      
      // Parcourir les articles pour ajouter une ligne à chaque article
      for (int i = 0; i < pListeArticle.size(); i++) {
        // Début de ligne
        contenu += "<tr>";
        
        // Colonne référence ou code article
        contenu += "<td style='" + REF_ARTICLE_MAIL + "'>";
        if (pListeArticle.get(i).isPresentField(pUtilisateur.getEtablissementEnCours().getZone_reference_article()) && !Constantes
            .convertirObjetEnTexte(pListeArticle.get(i).getField(pUtilisateur.getEtablissementEnCours().getZone_reference_article()))
            .equals("")) {
          contenu += StringEscapeUtils.escapeHtml4(Constantes
              .convertirObjetEnTexte(pListeArticle.get(i).getField(pUtilisateur.getEtablissementEnCours().getZone_reference_article())));
        }
        else if (pListeArticle.get(i).isPresentField("L1ART")) {
          contenu += StringEscapeUtils.escapeHtml4(Constantes.convertirObjetEnTexte(pListeArticle.get(i).getField("L1ART")));
        }
        contenu += "</td>";
        
        // Colonne libellé article
        if (!pListeArticle.get(i).isPresentField("A1LIB") && pListeArticle.get(i).isPresentField("L1ART")) {
          pListeArticle.get(i).setField("A1LIB", Constantes.convertirObjetEnTexte(pListeArticle.get(i).getField("L1ART")));
        }
        contenu += "<td style='" + LIB_ARTICLE_MAIL + "'>"
            + StringEscapeUtils.escapeHtml4(Constantes.convertirObjetEnTexte(pListeArticle.get(i).getField("A1LIB"))) + "</td>";
        
        // Colonne quantité
        if (pListeArticle.get(i).isPresentField("L1QTE")) {
          contenu += "<td style='" + QTE_ARTICLE_MAIL + "'>" + StringEscapeUtils.escapeHtml4(
              Outils.afficherValeurCorrectement(pUtilisateur, Constantes.convertirObjetEnTexte(pListeArticle.get(i).getField("L1QTE"))))
              + "</td>";
        }
        
        // Colonne mail multiple
        contenu += "<td style='" + MULTIPLE_MAIL + "'> x </td>";
        
        // Colonne prix unitaire HT
        if (pListeArticle.get(i).isPresentField("L1PVC") && pListeArticle.get(i).isPresentField("L1UNV")) {
          contenu += "<td style='" + TARIF_ARTICLE_MAIL + "'>"
              + Outils.formaterUnTarifArrondi(pUtilisateur, pListeArticle.get(i).getField("L1PVC")) + "</td>";
        }
        
        // Colonne montant HT
        if (pListeArticle.get(i).isPresentField("L1MHT") && pListeArticle.get(i).isPresentField("L1UNV")) {
          contenu += "<td style='" + TARIF_ARTICLE_MAIL + "'>"
              + Outils.formaterUnTarifArrondi(pUtilisateur, pListeArticle.get(i).getField("L1MHT")) + "</td>";
        }
        
        // Fin de ligne
        contenu += "</tr>";
      }
      
      // Vérifier s'il y a un montant total de la commande
      if (pEnTeteCommande != null && pEnTeteCommande.isPresentField("E1THTL")) {
        /// Récupérer le montant total de la commande
        BigDecimal montantCde = (BigDecimal) pEnTeteCommande.getField("E1THTL");
        
        // Ajouter une ligne avec le montant total de la commande
        contenu += "<tr>";
        contenu += "<td  colspan='6' style='" + TOTAL_COMMANDE_MAIL + "'>" + StringEscapeUtils.escapeHtml4("Total HT : ")
            + Outils.formaterUnTarifArrondi(pUtilisateur, pEnTeteCommande.getField("E1THTL")) + " </td>";
        contenu += "</tr>";
        
        // Afficher le message d'avertissement concernant le franco Frais de port
        // Si le montant HT de la commande est inférieur au montant Franco alors on affiche le message d'avertissement
        if (pModeRecuperation != MarbreEnvironnement.MODE_RETRAIT && pUtilisateur.getClient() != null
            && pUtilisateur.getClient().getFrancoPort() != null) {
          BigDecimal francoMini = pUtilisateur.getClient().getFrancoPort();
          String messageSpecifique = " (Des frais de port peuvent être appliqués pour tout montant inférieur)";
          
          if (francoMini.compareTo(montantCde) > 0) {
            contenu += "<tr>";
            contenu += "<td  colspan='6'>" + StringEscapeUtils.escapeHtml4("Franco = ")
                + Outils.formaterUnTarifArrondi(pUtilisateur, francoMini) + StringEscapeUtils.escapeHtml4(messageSpecifique) + "</td>";
            contenu += "</tr>";
          }
        }
      }
      
      contenu += "</table>";
    }
    
    return contenu;
  }
  
  /**
   * Prépare les paramètres du mail pour les devis.
   * 
   * @param pUtilisateur Information sur l'utilisateur
   * @param pNumeroDocument Numéro du document
   * @param pSuffixeDocument Suffixe du document
   * @param pCodeDocument Code du document
   * @param pEnumTypeDocument Type du document
   * @param pIsRapport Indicatif si c'est un rapport ou une confirmation de document
   * @return
   */
  public ParametreMail preparerParametreMailPourDevis(UtilisateurWebshop pUtilisateur, String pNumeroDocument, String pSuffixeDocument,
      String pCodeDocument, EnumTypeDocument pEnumTypeDocument, boolean pIsRapport) {
    // En-Tête du document contenant les informations complémentaires du le client
    GenericRecord enTeteDocument = recupererEnteteCommande(pUtilisateur, pCodeDocument + pNumeroDocument + pSuffixeDocument);
    // Liste des articles qui compose le document
    ArrayList<GenericRecord> listeArticle =
        recupererDetailCommande(pUtilisateur, pCodeDocument + pNumeroDocument + pSuffixeDocument, false);
    String numeroClient = pUtilisateur.getClient().getNumeroClient();
    String mailClient = pUtilisateur.getLogin();
    String suffixeSujet = "d'un montant de  " + getMontantCommande(pUtilisateur, enTeteDocument);
    // Tableau HTML contenant la liste des articles
    String tableauArticle = genererTableauArticleDocument(pUtilisateur, enTeteDocument, listeArticle,
        retournerModeRecuperation(enTeteDocument, pCodeDocument));
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.TYPE_DOCUMENT, pEnumTypeDocument.getLibelle());
    parametreMail.ajouterParametre(EnumNomVariableMail.NUMERO_SUFFIXE_FORMATE,
        StringEscapeUtils.escapeHtml4(pNumeroDocument + "/" + pSuffixeDocument));
    parametreMail.ajouterParametre(EnumNomVariableMail.NOM_CLIENT,
        StringEscapeUtils.escapeHtml4(pUtilisateur.getClient().getNomClient()));
    parametreMail.ajouterParametre(EnumNomVariableMail.TABLE_ARTICLE, tableauArticle);
    // Si c'est un rapport pour le gestionnaire, on a besoin de ses informations supplémentaires
    if (pIsRapport) {
      parametreMail.ajouterParametre(EnumNomVariableMail.NUMERO_CLIENT, StringEscapeUtils.escapeHtml4(numeroClient));
      parametreMail.ajouterParametre(EnumNomVariableMail.MAIL_CLIENT, StringEscapeUtils.escapeHtml4(mailClient));
      parametreMail.ajouterParametre(EnumNomVariableMail.SUFFIXE_SUJET, StringEscapeUtils.escapeHtml4(suffixeSujet));
    }
    
    return parametreMail;
  }
}
