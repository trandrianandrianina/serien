/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionClientBO extends Gestion {
  
  /**
   * Retourne la liste des paramétre de la table ENVIRONM de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererParametreclient(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_VAL, EN_TYP  FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ENVIRONM WHERE EN_GEST='1' AND EN_ETB = '' ORDER BY EN_CLE");
  }
  
  /**
   * Mise à jour de la table ENVIRONM de XWEBSHOP
   */
  public boolean miseajourParametreclient(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    boolean retour = false;
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return retour;
    }
    
    ArrayList<GenericRecord> liste = null;
    int res = 0;
    liste = utilisateur.getAccesDB2()
        .select("SELECT EN_CLE, EN_VAL, EN_TYP  FROM " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_GEST = '1' AND EN_ETB = '' ");
    
    if (liste != null && liste.size() > 0) {
      int nbModif = 0;
      
      for (int i = 0; i < liste.size(); i++) {
        if ((liste.get(i).getField("EN_TYP").toString().trim().equals("MAJUSCULE"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim().toUpperCase() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE") + "'");
          if (res >= -1) {
            nbModif++;
          }
        }
        else if ((liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))) {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE "
                  + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '" + Base64Coder
                      .encodeString(Base64Coder.encodeString(request.getParameter(liste.get(i).getField("EN_CLE").toString()))).trim()
                  + "' WHERE EN_CLE = '" + liste.get(i).getField("EN_CLE") + "'");
          if (res >= -1) {
            nbModif++;
          }
          if (liste.get(i).getField("EN_CLE").toString().trim().equals("BIBLI_CLIENTS")) {
            MarbreEnvironnement.BIBLI_CLIENTS = (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim().toUpperCase();
            // permet de mettre à jour la table etabliss quand la FM est modifié dans la table environm.
            res = utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ETABLISS SET ETB_FM = '"
                + MarbreEnvironnement.BIBLI_CLIENTS + "' WHERE ETB_ID = '" + utilisateur.getEtablissementEnCours() + "'");
          }
        }
        else {
          res = utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE") + "'");
          if (res >= -1) {
            nbModif++;
          }
        }
      }
      
      if (liste.size() == nbModif) {
        retour = true;
      }
    }
    
    return retour;
    
  }
  
}
