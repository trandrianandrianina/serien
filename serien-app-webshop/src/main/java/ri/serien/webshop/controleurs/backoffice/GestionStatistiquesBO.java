/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Cette classe gère les accés base de données pour la page web dédiée aux statistiques.
 *
 * Procédure :
 * 1. Afficher le Webshop.
 * 2. Se connecter en tant que gestionnaire.
 * 3. Cliquer sur l’onglet "Gestion".
 * 4. Cliquer sur le bouton "Statistiques".
 * 5. Cliquer sur le bouton "Statistiques CA" ou "Statistiques connexions clients".
 */
public class GestionStatistiquesBO extends Gestion {
  
  /**
   * Charger la liste des CA des commandes Webshop par mois pour l'année précisée en paramètre.
   * 
   * @param pUtilisateur Utilisateur connecté au Webshop.
   * @param pAnnee Année dont il faut récupérer les statistiques de connexions mensuelles.
   * @return Liste des CA mensuels.
   */
  public ArrayList<GenericRecord> chargerListeCAParMois(UtilisateurWebshop pUtilisateur, int pAnnee) {
    // Contôler les paramètres
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      Trace.erreur("L'utilisateur est invalide");
      return null;
    }
    
    // Définir la colonne contenant le montant
    // Si on est en franc pacifique c'est à dire à 0 décimales
    String montant = "E1THTL";
    if (pUtilisateur.getEtablissementEnCours().getDecimales_client() == 0) {
      montant = "E1THTL*100";
    }
    
    // Retourner le résultat de la requête
    return pUtilisateur.getAccesDB2()
        .select("SELECT SUBSTR(DIGITS(E1CRE), 4, 2) as MOIS, SUM(" + montant + ") as CA " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM " + " WHERE E1COD='E' AND E1IN18='W' AND SUBSTR(DIGITS(E1CRE), 1, 3)+1900 = " + pAnnee
            + " GROUP BY SUBSTR(DIGITS(E1CRE), 4, 2)");
  }
  
  /**
   * Charger la liste du nombre de connexions par mois pour l'année précisée en paramètre.
   * 
   * @param pUtilisateur Utilisateur connecté au Webshop.
   * @param pAnnee Année dont il faut récupérer les statistiques de connexions mensuelles.
   * @return Liste des connexions mensuelles.
   */
  public ArrayList<GenericRecord> chargerListeConnexionParMois(UtilisateurWebshop pUtilisateur, int pAnnee) {
    // Contôler les paramètres
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      Trace.erreur("L'utilisateur est invalide");
      return null;
    }
    
    // Retourner le résultat de la requête
    return pUtilisateur.getAccesDB2()
        .select("SELECT SUBSTR(DIGITS(LO_DATE), 7, 2) as MOIS, COUNT(*) as CNX" + " FROM " + MarbreEnvironnement.BIBLI_WS + ".LOGSW "
            + " WHERE LO_TYPE ='C' AND SUBSTR(DIGITS(LO_DATE), 4, 3)+1900 = " + pAnnee + " GROUP BY SUBSTR(DIGITS(LO_DATE), 7, 2)");
  }
  
  /**
   * Charger l'ensemble des données de connexions par mois pour l'année précisée en paramètre.
   * 
   * @param pUtilisateur Utilisateur connecté au Webshop.
   * @param pAnnee Année dont il faut récupérer les statistiques de connexions mensuelles.
   * @return Liste des connexions mensuelles.
   */
  public ArrayList<GenericRecord> chargerListeCompleteConnexionParMois(UtilisateurWebshop pUtilisateur, int pAnnee) {
    // Contôler les paramètres
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      Trace.erreur("L'utilisateur est invalide");
      return null;
    }
    
    // Retourner le résultat de la requête
    return pUtilisateur.getAccesDB2()
        .select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".LOGSW LEFT OUTER JOIN " + MarbreEnvironnement.BIBLI_WS
            + ".USERW ON US_ID = LO_USER" + " WHERE (LO_TYPE ='C' OR LO_TYPE ='F') AND SUBSTR(DIGITS(LO_DATE), 4, 3)+1900 = " + pAnnee);
  }
  
}
