/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionCatalogueBO extends Gestion {
  
  /**
   * Retourne la liste des paramétres d'affichage de la table AFFICHAGE de XWEBSHOP
   */
  public ArrayList<GenericRecord> recupererParametreAffichage(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT EN_CLE, EN_LIB, EN_VAL, EN_TYPE  FROM " + MarbreEnvironnement.BIBLI_WS + ".AFFICHAGE" + " WHERE EN_GEST='1'");
  }
  
  /**
   * Mise à jour de la table AFFICHAGE de XWEBSHOP et par conséquent l'affichage du catalogue
   */
  public ArrayList<GenericRecord> miseajourParametreAffichage(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2().select("SELECT EN_CLE, EN_VAL  FROM " + MarbreEnvironnement.BIBLI_WS + ".AFFICHAGE");
    
    if (liste != null && liste.size() > 0) {
      for (int i = 0; i < liste.size(); i++) {
        if (!liste.get(i).getField("EN_VAL").toString().trim()
            .equals((request.getParameter(liste.get(i).getField("EN_CLE").toString()).trim()))) {
          utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".AFFICHAGE SET EN_VAL  = '"
                  + (request.getParameter(liste.get(i).getField("EN_CLE").toString())).trim() + "' WHERE EN_CLE = '"
                  + liste.get(i).getField("EN_CLE") + "'");
        }
      }
    }
    
    return liste;
  }
  
  /**
   * traitement de l'upload
   */
  public int upload(UtilisateurWebshop utilisateur, HttpServletRequest request, String filename) {
    String uploadDirectory = MarbreEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separator;
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    
    int res = 0;
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploader*/
    
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requête*/
    
    /*construction du repertoire ou sera stocker le fichier uploader. */
    String uploadPath = uploadDirectory;
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(request);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recupérés
      while (iter.hasNext()) {
        FileItem item = iter.next();
        
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          filename = item.getName();
          
          String filePath = uploadPath + filename;
          File storeFile = new File(filePath);
          // Enregistrer le fichier sur le disque
          item.write(storeFile);
          res = 1;
          Trace.debug("[upload()] L'UPLOAD à correctement fonctionné");
        }
      }
      
    }
    catch (Exception ex) {
      Trace.erreur("[upload()] L'upload à échoué car fichier non selectionné, ou repertoire de destination non créé: " + ex.getMessage());
      res = -1;
    }
    return res;
  }
  
}
