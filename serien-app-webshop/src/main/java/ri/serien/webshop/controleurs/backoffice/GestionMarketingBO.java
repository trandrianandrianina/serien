/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

public class GestionMarketingBO extends Gestion {
  
  /**
   * récupérer la liste de promotions qui répondent aux critères transmis
   **/
  public static ArrayList<GenericRecord> recupererListePromotions(UtilisateurWebshop utilisateur, String pFiltreStt) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return null;
    }
    
    String conditions = " ";
    if (pFiltreStt != null) {
      int dateJour = Outils.recupererDateCouranteInt();
      if (pFiltreStt.equals("0")) {
        conditions = " WHERE PR_DATED > " + dateJour + " ";
      }
      else if (pFiltreStt.equals("1")) {
        conditions = " WHERE PR_DATED <= " + dateJour + " AND PR_DATEF >= " + dateJour + " ";
      }
      else if (pFiltreStt.equals("2")) {
        conditions = " WHERE PR_DATEF < " + dateJour + " ";
      }
    }
    
    return utilisateur.getAccesDB2().select("SELECT PR_ID,PR_A1ETB,PR_A1ART,PR_A1LIB,PR_TEXTE,PR_DATED,PR_DATEF,PR_PRIX " + " FROM "
        + MarbreEnvironnement.BIBLI_WS + ".PROMOTION " + conditions + " " + " ORDER BY PR_DATEF DESC ");
  }
  
  /**
   * Récupère le détail d'une promotion
   **/
  public static ArrayList<GenericRecord> recupererUnepromotion(UtilisateurWebshop utilisateur, String pId) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || pId == null) {
      return null;
    }
    
    return utilisateur.getAccesDB2().select(" SELECT PR_ID,PR_A1ETB,PR_A1ART,PR_A1LIB,PR_TEXTE,PR_DATED,PR_DATEF,PR_PRIX " + " FROM "
        + MarbreEnvironnement.BIBLI_WS + ".PROMOTION WHERE PR_ID= " + pId);
  }
  
  /**
   * Mise à jour de la table promotion (type de donnée classique
   **/
  public static int miseaJourUnePromotion(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    if (utilisateur == null || request == null) {
      return -1;
    }
    int res = 0;
    
    if (request.getParameter("insertPromo") != null) {
      res = utilisateur.getAccesDB2()
          .requete(" INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".PROMOTION "
              + " (PR_A1ETB,PR_A1ART,PR_A1LIB,PR_TEXTE,PR_DATED,PR_DATEF,PR_PRIX)" + " VALUES" + "('"
              + (request.getParameter("etbArticle").toString().trim()) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("codeArticle")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("libelleArt")) + "','"
              + traiterCaracteresSpeciauxSQL(request.getParameter("libPromotion")) + "','"
              + Outils.transformerDateHumaineEnSeriem((request.getParameter("dateDebut").trim())) + "','"
              + Outils.transformerDateHumaineEnSeriem((request.getParameter("dateFin").trim())) + "','"
              + traiterLesMontantsSql(request.getParameter("prixArticle")) + "')");
      
      // On récupère l'identifiant que l'on vient d'insérer si cela a fonctionné
      if (res == 1) {
        ArrayList<GenericRecord> liste =
            utilisateur.getAccesDB2().select(" SELECT MAX(PR_ID) AS MONID " + " FROM " + MarbreEnvironnement.BIBLI_WS + ".PROMOTION ");
        if (liste != null && liste.size() == 1) {
          try {
            res = Integer.parseInt(liste.get(0).getField("MONID").toString());
          }
          catch (Exception e) {
            res = -2;
          }
        }
      }
    }
    else if (request.getParameter("idAmodifier") != null) {
      res = utilisateur.getAccesDB2()
          .requete(" UPDATE " + MarbreEnvironnement.BIBLI_WS + ".PROMOTION " + " SET PR_A1ART = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("codeArticle")) + "' " + ", PR_A1LIB  = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("libelleArt")) + "', PR_TEXTE = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("libPromotion")) + "', PR_PRIX = '"
              + traiterLesMontantsSql(request.getParameter("prixArticle")) + "', PR_DATED = '"
              + (Outils.transformerDateHumaineEnSeriem(request.getParameter("dateDebut"))) + "', PR_DATEF = '"
              + (Outils.transformerDateHumaineEnSeriem(request.getParameter("dateFin"))) + "' WHERE PR_ID = "
              + (request.getParameter("idAmodifier")));
      
      if (res == 1) {
        try {
          res = Integer.parseInt(request.getParameter("idAmodifier"));
        }
        catch (NumberFormatException e) {
          res = -2;
        }
      }
    }
    
    return res;
  }
  
  /**
   * retourner une valeur d'insertion pour les champ numériques de montants
   */
  private static String traiterLesMontantsSql(String pValeur) {
    if (pValeur == null) {
      return "0";
    }
    BigDecimal montant = null;
    
    try {
      montant = new BigDecimal(pValeur);
    }
    catch (Exception e) {
      montant = BigDecimal.ZERO;
    }
    
    return montant.toString();
  }
  
  /** Traitement des images Promotion **/
  public static int uploadPromotion(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    String uploadDirectory = MarbreEnvironnement.DOSSIER_SPECIFIQUE + "promotions" + File.separator;
    
    int thresholdSize = 1024 * 1024 * 3;/*3MB*/
    int maxFileSize = 1024 * 1024 * 2;/*2MB*/
    int maxrequestSize = 1024 * 1024 * 10;/*10MB*/
    
    int res = -1;
    int idPromo = 0;
    
    /*Configuration de l'objet upload*/
    DiskFileItemFactory fileItemFactory = new DiskFileItemFactory();/*objet upload*/
    fileItemFactory.setSizeThreshold(thresholdSize);/*taille en memoire*/
    fileItemFactory.setRepository(new File(System.getProperty("java.io.tmpdir")));
    
    ServletFileUpload upload = new ServletFileUpload(fileItemFactory);
    upload.setFileSizeMax(maxFileSize);/*definition de la taille max du fichier uploader*/
    upload.setSizeMax(maxrequestSize);/*definition de la taille max de la requête*/
    
    String nomFichier = "promotion.jpg";
    
    try {
      /*cas ou il y a un champ dans le formulaire*/
      List<FileItem> formItems = upload.parseRequest(request);
      Iterator<FileItem> iter = formItems.iterator();
      
      // on parcourt les champs recupérés
      while (iter.hasNext()) {
        FileItem item = iter.next();
        
        // traite seulement les champs qui ne sont pas des champs formulaire donc c'est notre cas
        if (!item.isFormField()) {
          String filePath = uploadDirectory + nomFichier; // chemin du fichier avec le nom du fichier
          File storeFile = new File(filePath);
          // Enregistrer le fichier sur le disque
          item.write(storeFile);
          res = idPromo;
        }
        else {
          if (item.getFieldName().equals("idPromo")) {
            idPromo = Integer.parseInt(item.getString().trim());
            nomFichier = "promotion_" + item.getString().trim() + ".jpg";
          }
        }
      }
      
    }
    catch (Exception ex) {
      res = -1;
    }
    return res;
  }
  
  /**
   * Traitement des erreurs dans le formulaire de saisie de la promotion
   **/
  public static int[] gestionDesErreurs(HttpServletRequest request) {
    int[] tabRetour = null;
    String retour = null;
    
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    
    String codeArticle = request.getParameter("codeArticle").trim();
    String etbArticle = request.getParameter("etbArticle").trim();
    String libPromo = request.getParameter("libPromotion").trim();
    String prix = request.getParameter("prixArticle").trim();
    String dateDebut = request.getParameter("dateDebut").trim();
    String dateFin = request.getParameter("dateFin").trim();
    boolean articlePresent = false;
    
    if (codeArticle != null && codeArticle.trim().length() != 0) {
      articlePresent = true;
      if (!verifierExistenceArticle(utilisateur, etbArticle, codeArticle)) {
        retour = "-1";
      }
    }
    
    if (prix != null && prix.trim().length() != 0) {
      try {
        BigDecimal decimal = new BigDecimal(prix);
        if (decimal.compareTo(BigDecimal.ZERO) < 0) {
          if (retour == null) {
            retour = "-2";
          }
          else {
            retour += ",-2";
          }
        }
      }
      catch (Exception e) {
        if (retour == null) {
          retour = "-2";
        }
        else {
          retour += ",-2";
        }
      }
    }
    else {
      if (articlePresent) {
        if (retour == null) {
          retour = "-2";
        }
        else {
          retour += ",-2";
        }
      }
    }
    
    if (libPromo == null || libPromo.length() < 3) {
      if (retour == null) {
        retour = "-3";
      }
      else {
        retour += ",-3";
      }
    }
    
    if (dateFin == null || dateDebut == null) {
      if (retour == null) {
        retour = "-4";
      }
      else {
        retour += ",-4";
      }
    }
    else {
      int dateD = Outils.transformerDateHumaineEnSeriem(dateDebut);
      int dateF = Outils.transformerDateHumaineEnSeriem(dateFin);
      
      if (dateD == 0 || dateF == 0 || dateD > dateF) {
        if (retour == null) {
          retour = "-4";
        }
        else {
          retour += ",-4";
        }
      }
    }
    
    if (etbArticle == null || etbArticle.length() == 0) {
      if (retour == null) {
        retour = "-5";
      }
      else {
        retour += ",-5";
      }
    }
    
    if (retour != null) {
      String[] tab = retour.split(",");
      tabRetour = new int[tab.length];
      for (int i = 0; i < tab.length; i++) {
        tabRetour[i] = Integer.parseInt(tab[i]);
      }
    }
    
    return tabRetour;
  }
  
  /**
   * On vérifie si la saisie d'un article existe en base de données
   */
  private static boolean verifierExistenceArticle(UtilisateurWebshop pUtil, String pEtb, String pCode) {
    if (pEtb == null || pCode == null) {
      return false;
    }
    
    ArrayList<GenericRecord> liste = pUtil.getAccesDB2().select(" SELECT A1ART " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS
        + ".PGVMARTM " + " WHERE A1ETB = '" + pEtb + "' AND A1ART = '" + pCode + "' ");
    
    return (liste != null && liste.size() == 1);
  }
  
  /**
   * Permet de retourner le statut de la promo sur la base de la date de début, la date courante et la date de fin déclarées
   */
  public static int retournerStatutPromo(UtilisateurWebshop utilisateur, String pDateD, String pDateF) {
    if (pDateD == null || pDateF == null) {
      return -1;
    }
    
    try {
      int dateDebut = Integer.parseInt(pDateD);
      int dateFin = Integer.parseInt(pDateF);
      int dateJour = Outils.recupererDateCouranteInt();
      // impossible
      if (dateDebut > dateFin) {
        return -1;
      }
      // avant la promo
      if (dateJour < dateDebut) {
        return 0;
      }
      // après la promo
      if (dateJour > dateFin) {
        return 2;
      }
      // date active;
      return 1;
      
    }
    catch (NumberFormatException e) {
      return -1;
    }
  }
}
