/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreAffichage;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.TableBDD;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.CalculStocks;
import ri.serien.webshop.metier.Etablissement;
import ri.serien.webshop.metier.GestionVariantes;
import ri.serien.webshop.metier.Magasin;
import ri.serien.webshop.outils.Outils;

public class GestionCatalogue extends Gestion {
  private CalculStocks calculDeStocks = null;
  private final static BigDecimal ZERO = new BigDecimal(0);
  private final static BigDecimal CENT = new BigDecimal(100);
  private final static BigDecimal DIXMILLE = new BigDecimal(10000);
  private GestionVariantes gestionVariantes = null;
  private String MEMO_WS = "WS";
  
  /**
   * Retourner une liste d'articles
   */
  public ArrayList<GenericRecord> retournerListeArticlesMoteur(UtilisateurWebshop utilisateur, String expression) {
    String filtreFrs = " ";
    
    expression = traiterCaracteresSpeciauxSQL(expression.trim().toUpperCase());
    
    // On dégage si on est pas bon sur la saisie de l'expression
    if (utilisateur == null) {
      return null;
    }
    if (!expressionIsOk(expression)) {
      utilisateur.setDerniereListeArticle(null);
      return null;
    }
    
    if (utilisateur.getDernierFiltreFournisseur() != null) {
      filtreFrs = " AND A1COF = " + utilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
          + utilisateur.getDernierFiltreFournisseur().substring(1, 7);
    }
    
    int A1ART = 0;
    int CAREF = 0;
    int A1LIB = 0;
    int A1CL1 = 0;
    int A1CL2 = 0;
    
    ArrayList<GenericRecord> listeRecherche = utilisateur.getAccesDB2()
        .select("SELECT RE_CODE FROM " + MarbreEnvironnement.BIBLI_WS + "." + TableBDD.TABLE_WS_RECHARTICL + " WHERE RE_VAL = '1' ");
    if (listeRecherche != null) {
      for (GenericRecord record : listeRecherche) {
        if (record.getField("RE_CODE").toString().trim().equals("A1ART")) {
          A1ART = 1;
        }
        if (record.getField("RE_CODE").toString().trim().equals("CAREF")) {
          CAREF = 1;
        }
        if (record.getField("RE_CODE").toString().trim().equals("A1LIB")) {
          A1LIB = 1;
        }
        if (record.getField("RE_CODE").toString().trim().equals("A1CL1")) {
          A1CL1 = 1;
        }
        if (record.getField("RE_CODE").toString().trim().equals("A1CL2")) {
          A1CL2 = 1;
        }
      }
    }
    
    ArrayList<GenericRecord> liste = null;
    String zonePrioritaire = null;
    String zoneSecondaire = null;
    if (A1ART > 0 && CAREF > 0) {
      // Zones référence de recherche A1ART ou CAREF en fonction du paramétrage
      zonePrioritaire = MarbreAffichage.REF_REF_FOURNISS;
      zoneSecondaire = MarbreAffichage.REF_CODE_ARTICLE;
      
      // Si le paramétrage le réclame c'est le code article qui devient prioritaire et non la référence fournisseur
      if (utilisateur.getEtablissementEnCours().getZone_reference_article() != null
          && utilisateur.getEtablissementEnCours().getZone_reference_article().equals(MarbreAffichage.REF_CODE_ARTICLE)) {
        zonePrioritaire = MarbreAffichage.REF_CODE_ARTICLE;
        zoneSecondaire = MarbreAffichage.REF_REF_FOURNISS;
      }
    }
    else if (A1ART > 0) {
      zonePrioritaire = MarbreAffichage.REF_CODE_ARTICLE;
    }
    else {
      zonePrioritaire = MarbreAffichage.REF_REF_FOURNISS;
    }
    
    int ordre = 1;
    String zonesSELECTprioritaires = " CASE " + " WHEN " + zonePrioritaire + " LIKE '" + expression + "%'" + "THEN '" + ordre + "' ";
    
    if (A1CL1 > 0) {
      ordre += 1;
      zonesSELECTprioritaires += " WHEN A1CL1 = '" + expression + "' THEN '" + ordre + "' ";
    }
    
    if (A1CL2 > 0) {
      ordre += 1;
      zonesSELECTprioritaires += " WHEN A1CL2 = '" + expression + "' THEN '" + ordre + "' ";
    }
    
    if (A1LIB > 0) {
      ordre += 1;
      zonesSELECTprioritaires += " WHEN A1LIB LIKE '" + expression + "%' THEN '" + ordre + "' ";
    }
    
    ordre += 1;
    zonesSELECTprioritaires += " WHEN " + zonePrioritaire + " LIKE '%" + expression + "%' THEN '" + ordre + "' ";
    
    if (A1LIB > 0) {
      ordre += 1;
      zonesSELECTprioritaires += " WHEN A1LIB  " + " LIKE '%" + expression + "%' THEN '" + ordre + "' ";
    }
    
    zonesSELECTprioritaires += " END as TRI ";
    
    // Conditions de recherches
    
    String zonesWHEREprioritaires = " AND (" + zonePrioritaire + " LIKE '%" + expression + "%' ";
    
    if (A1CL1 > 0) {
      zonesWHEREprioritaires += " OR A1CL1 = '" + expression + "' ";
    }
    
    if (A1CL2 > 0) {
      zonesWHEREprioritaires += " OR A1CL2 = '" + expression + "' ";
    }
    
    if (A1LIB > 0) {
      zonesWHEREprioritaires += " OR A1LIB like '%" + expression + "%' ";
    }
    
    zonesWHEREprioritaires += " ) ";
    
    liste = utilisateur.getAccesDB2()
        .select(" SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND, A1TVA, " + zonesSELECTprioritaires
            + ", A1COF, A1FRS, FRAWS ,FRNWS, FRNOM " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.VUE_ART_UT + " "
            + " WHERE A1ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB() + "' "
            + MarbreAffichage.retournerFiltreSqlStatutArticle(null) + " " + MarbreAffichage.retournerFiltreSqlVieArticle(null) + " "
            + filtreFrs + zonesWHEREprioritaires + " ORDER BY TRI FETCH FIRST "
            + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
            + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    // Si on a pas trouvé de résultat, on cherche dans la zone secondaire de recherche article de Série N
    if (zoneSecondaire != null && liste != null && liste.size() == 0) {
      liste = utilisateur.getAccesDB2()
          .select(" SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND, A1TVA, CASE " + " WHEN " + zoneSecondaire
              + " LIKE '" + expression + "%'" + "THEN '1'" + " WHEN " + zoneSecondaire + " LIKE '%" + expression
              + "%' THEN '2' END as TRI ,A1COF, A1FRS, FRAWS ,FRNWS, FRNOM " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "."
              + TableBDD.VUE_ART_UT + " " + " WHERE A1ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB() + "' "
              + MarbreAffichage.retournerFiltreSqlStatutArticle(null) + " " + MarbreAffichage.retournerFiltreSqlVieArticle(null) + " "
              + filtreFrs + " AND " + zoneSecondaire + " LIKE '%" + expression + "%' " + " ORDER BY TRI FETCH FIRST "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    if (utilisateur.getDernierFiltreFournisseur() == null) {
      utilisateur.setDerniereListeArticleMoteur(liste);
    }
    
    utilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Contrôle la validité de l'expression
   */
  public boolean expressionIsOk(String expression) {
    int saisieMinimum = 3;
    int saisieMaximum = 30;
    boolean isOk = false;
    
    if (expression != null && expression.trim().length() >= saisieMinimum && expression.trim().length() <= saisieMaximum) {
      isOk = true;
    }
    
    return isOk;
  }
  
  /**
   * Retourne la liste des groupes familles.
   */
  public ArrayList<GenericRecord> recupereGroupesFamilles(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur ou AccesDB2 à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT CA_TYP as TYPE, CA_IND as IND, CA_LIB as LIB FROM " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB()
            + "' AND CA_TYP <> 'SF' ORDER BY IND  " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
  /**
   * Retourne la liste des groupes d'articles
   */
  public ArrayList<GenericRecord> retourneListeGroupes(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur ou AccesDB2 à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT CA_TYP AS TYPE, CA_IND AS IND, CA_LIB AS LIB FROM " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB()
            + "' AND CA_TYP ='GR' ORDER BY IND  " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
  /**
   * Retourne la liste des articles d'une famille ou d'une sous famille.
   */
  public ArrayList<GenericRecord> retourneListeArticlesFamille(UtilisateurWebshop utilisateur, String famille) {
    String filtreFrs = " ";
    
    if (famille == null || utilisateur == null) {
      Trace.erreur("retourneListeArticlesFamille() Utilisateur ou famille à NUll");
      return null;
    }
    else {
      famille = traiterCaracteresSpeciauxSQL(famille);
    }
    
    String codFam = null;
    String fieldFam = null;
    // Si il s'agit d'une famille -> A1FAM
    if (famille.length() > 4 && famille.substring(0, 2).equals("FA")) {
      codFam = famille.substring(2, 5);
      fieldFam = "A1FAM";
    }
    // Si il s'agit d'une sous famille -> A1SFA
    else if (famille.length() > 6 && famille.substring(0, 2).equals("SF")) {
      codFam = famille.substring(2, 7);
      fieldFam = "A1SFA";
    }
    
    // Filtre sur le fournisseur
    if (utilisateur.getDernierFiltreFournisseur() != null) {
      filtreFrs = " AND A1COF = " + utilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
          + utilisateur.getDernierFiltreFournisseur().substring(1, 7);
    }
    
    ArrayList<GenericRecord> liste = null;
    
    // Si on a bien récupéré le code et la valeur correspondante on execute la demande d'articles
    if (codFam != null && fieldFam != null) {
      liste = utilisateur.getAccesDB2()
          .select(" SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND, CND , A1TVA, A1COF, A1FRS, FRAWS ,FRNWS, FRNOM "
              + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND " + fieldFam + "='" + codFam + "' "
              + MarbreAffichage.retournerFiltreSqlStatutArticle(null) + " " + MarbreAffichage.retournerFiltreSqlVieArticle(null) + " "
              + filtreFrs + " ORDER BY A1ABC, A1SAI DESC, A1LIB" + " FETCH FIRST "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    utilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Retourne la liste des fournisseurs issue de la liste des articles.
   */
  public HashMap<String, String[]> retourneListeFournisseur(UtilisateurWebshop utilisateur) {
    ArrayList<GenericRecord> liste = new ArrayList<GenericRecord>();
    
    if (utilisateur == null) {
      return null;
    }
    
    if (utilisateur.getDernierGroupeArticles() == null && utilisateur.getDerniereFamilleArticles() == null
        && utilisateur.getDerniereExpression() == null && utilisateur.getDerniersFavoris() == null
        && utilisateur.getDernierHistorique() == null && utilisateur.getDerniersConsultes() == null
        && utilisateur.getDernierPaniersIntuitifs() == null) {
      return null;
    }
    
    // Gestion de l'historique
    if (utilisateur.getDernierHistorique() != null) {
      if (utilisateur.getDerniereListeArticleHistorique() != null && utilisateur.getDerniereListeArticleHistorique().size() > 0) {
        liste = utilisateur.getDerniereListeArticleHistorique();
      }
    }
    // Gestion des favoris
    else if (utilisateur.getDerniersFavoris() != null) {
      if (utilisateur.getDerniereListeArticleFavoris() != null && utilisateur.getDerniereListeArticleFavoris().size() > 0) {
        liste = utilisateur.getDerniereListeArticleFavoris();
      }
    }
    // Gestion des paniers intuitifs
    else if (utilisateur.getDernierPaniersIntuitifs() != null) {
      if (utilisateur.getDerniereListePanierIntuitif() != null && utilisateur.getDerniereListePanierIntuitif().size() > 0) {
        liste = utilisateur.getDerniereListePanierIntuitif();
      }
    }
    // Gestion des consultés
    else if (utilisateur.getDerniersConsultes() != null) {
      if (utilisateur.getDerniereListeArticleConsultee() != null && utilisateur.getDerniereListeArticleConsultee().size() > 0) {
        liste = utilisateur.getDerniereListeArticleConsultee();
      }
    }
    // Si derniere expression du client renseigné, alors on charge la liste de l'utilisateur,
    else if (utilisateur.getDerniereExpression() != null) {
      if (utilisateur.getDerniereListeArticleMoteur() != null && utilisateur.getDerniereListeArticleMoteur().size() > 0) {
        liste = utilisateur.getDerniereListeArticleMoteur();
      }
    }
    // sinon, on recherche tous les fournisseurs pour le groupe ou la famille
    else {
      String recherche = "";
      if (utilisateur.getDernierGroupeArticles() != null) {
        recherche = utilisateur.getDernierGroupeArticles();
      }
      
      if (utilisateur.getDerniereFamilleArticles() != null) {
        if (utilisateur.getDerniereFamilleArticles().length() > 6
            && utilisateur.getDerniereFamilleArticles().substring(0, 2).equals("SF")) {
          recherche = utilisateur.getDerniereFamilleArticles().substring(2, 7);
        }
        else if (utilisateur.getDerniereFamilleArticles().length() > 4
            && utilisateur.getDerniereFamilleArticles().substring(0, 2).equals("FA")) {
          recherche = utilisateur.getDerniereFamilleArticles().substring(2, 5);
        }
      }
      
      liste = utilisateur.getAccesDB2()
          .select("SELECT FO_COL AS A1COF, DIGITS(FO_FRS) AS A1FRS, FO_NOM AS FRNOM FROM " + MarbreEnvironnement.BIBLI_WS
              + ".FOURNISS WHERE FO_BIB = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' AND FO_ETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND FO_CA = '" + recherche + "' "
              + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    
    // Construction de la liste des fournisseurs
    HashMap<String, String[]> listeFrs = new HashMap<String, String[]>();
    if (liste.size() == 0) {
      return listeFrs;
    }
    
    for (GenericRecord record : liste) {
      traiterNomFournisseur(record);
      if (record.isPresentField("FRNOM") && !record.getField("FRNOM").toString().trim().isEmpty() && record.isPresentField("A1COF")
          && record.isPresentField("A1FRS")) {
        // CAS DU A1FRS CODE SUR PLUS DE 6 DIGITS
        if (record.getField("A1FRS").toString().length() > 6) {
          record.setField("A1FRS", record.getField("A1FRS").toString().substring(4));
        }
        
        // Ajoute le fournisseur à la liste des fournisseurs
        if (!listeFrs.containsKey(
            record.getField("FRNOM").toString() + record.getField("A1COF").toString() + record.getField("A1FRS").toString())) {
          listeFrs.put(record.getField("FRNOM").toString() + record.getField("A1COF").toString() + record.getField("A1FRS").toString(),
              new String[] { record.getField("FRNOM").toString(), record.getField("A1COF").toString(),
                  record.getField("A1FRS").toString() });
        }
      }
    }
    
    // Stockage de la derniére liste des fournisseurs
    utilisateur.setDerniereListeFournisseur(listeFrs);
    return listeFrs;
  }
  
  /**
   * Retourne la liste des articles d'un groupe.
   */
  public ArrayList<GenericRecord> retourneListeArticlesGroupe(UtilisateurWebshop utilisateur, String groupe) {
    String filtreFrs = " ";
    
    if (groupe == null || utilisateur == null) {
      return null;
    }
    else {
      groupe = traiterCaracteresSpeciauxSQL(groupe);
    }
    
    if (utilisateur.getDernierFiltreFournisseur() != null) {
      filtreFrs = " AND A1COF = " + utilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
          + utilisateur.getDernierFiltreFournisseur().substring(1, 7);
    }
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2()
        .select(" SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND , A1TVA, A1COF, A1FRS, FRAWS ,FRNWS, FRNOM "
            + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT " + " WHERE A1ETB = '"
            + utilisateur.getEtablissementEnCours().getCodeETB() + "' " + MarbreAffichage.retournerFiltreSqlStatutArticle(null) + " "
            + MarbreAffichage.retournerFiltreSqlVieArticle(null) + " " + filtreFrs + " AND A1FAM LIKE '" + groupe + "%' "
            + " ORDER BY A1ABC, A1SAI DESC, A1LIB FETCH FIRST " + utilisateur.getEtablissementEnCours().getLimit_requete_article()
            + " ROWS ONLY OPTIMIZE FOR " + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    utilisateur.setDerniereListeArticle(liste);
    return liste;
  }
  
  /**
   * Retourne le libellé concernant la famille.
   */
  public String retourneInfoFamille(UtilisateurWebshop utilisateur, String famille) {
    String codFam = null;
    
    if (famille == null || utilisateur == null) {
      return null;
    }
    else {
      famille = traiterCaracteresSpeciauxSQL(famille);
    }
    
    if (famille.length() > 4) {
      codFam = famille.substring(2, 5);
    }
    else {
      codFam = famille;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
        .select("SELECT CA_LIB AS LIB FROM  " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB= '" + utilisateur.getEtablissementEnCours().getCodeETB()
            + "' AND CA_IND = '" + codFam + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    if (liste != null && liste.size() > 0) {
      codFam = liste.get(0).getField("LIB").toString().trim();
    }
    else {
      codFam = "";
    }
    
    return codFam;
  }
  
  /**
   * Retourne le libellé concernant la sous-famille.
   */
  public String retourneInfoSousFamille(UtilisateurWebshop utilisateur, String famille) {
    String codFam = null;
    
    if (famille == null || utilisateur == null) {
      return null;
    }
    else {
      famille = traiterCaracteresSpeciauxSQL(famille);
    }
    
    if (famille.length() > 6) {
      codFam = famille.substring(2, 7);
    }
    else {
      codFam = famille;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
        .select("SELECT CA_LIB AS LIB FROM  " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB= '" + utilisateur.getEtablissementEnCours().getCodeETB()
            + "' AND CA_IND = '" + codFam + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    if (liste != null && liste.size() > 0) {
      codFam = liste.get(0).getField("LIB").toString().trim();
    }
    else {
      codFam = "";
    }
    
    return codFam;
  }
  
  /**
   * Retourne information concernant le groupe
   */
  public ArrayList<GenericRecord> retourneInfoGroupe(UtilisateurWebshop utilisateur, String groupe) {
    
    if (groupe == null || utilisateur == null) {
      return null;
    }
    else {
      groupe = traiterCaracteresSpeciauxSQL(groupe);
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT CA_LIB AS LIB FROM  " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB= '" + utilisateur.getEtablissementEnCours().getCodeETB()
            + "' AND CA_IND = '" + groupe + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
  /**
   * Retourne la liste des groupes familles.
   */
  public ArrayList<GenericRecord> retournerFamillesPourUnGroupe(UtilisateurWebshop utilisateur, String groupe) {
    if (groupe == null || utilisateur == null) {
      return null;
    }
    else {
      groupe = traiterCaracteresSpeciauxSQL(groupe);
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT CA_TYP as PARTYP, CA_IND as PARIND, CA_LIB as LIBELLEPAR FROM " + MarbreEnvironnement.BIBLI_WS
            + ".CATALOGUE WHERE CA_BIB = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' AND CA_ETB = '"
            + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND CA_TYP <> 'GR' AND SUBSTR(CA_IND, 1, 1) = '" + groupe
            + "' ORDER BY PARIND  " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
  /**
   * VERSION PAGINATION A JOUR Retourne une extraction de la liste totale d'articles en
   * fonction d'un indice de parcours et du nombre d'articles dans un morceau
   */
  public ArrayList<GenericRecord> retourneMorceauListe(UtilisateurWebshop utilisateur, int indice) {
    // long td = 0;
    if (utilisateur == null) {
      return null;
    }
    if (utilisateur.getDerniereListeArticle() == null) {
      return null;
    }
    if (utilisateur.getDerniereListeArticle().size() == 0) {
      return utilisateur.getDerniereListeArticle();
    }
    
    // INIT DE LA LISTE PARTIELLE DE L'UTILISATEUR
    if (utilisateur.getDerniereListePartiellePagination() == null) {
      utilisateur.setDerniereListePartiellePagination(new ArrayList<GenericRecord>());
    }
    else {
      utilisateur.getDerniereListePartiellePagination().clear();
    }
    
    // SI ON EST UN CLIENT OU UN REPRES AVC UN CLIENT ASSOCIE
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT
        || utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CONSULTATION
        || (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS && utilisateur.getClient() != null)
            && utilisateur.isVisuStock()) {
      String dispo = null;
      String attendu = null;
      utilisateur.controleGestionTarif();
      
      for (int i = indice; i < utilisateur.getDerniereListeArticle().size()
          && i < indice + utilisateur.getEtablissementEnCours().getLimit_liste(); i++) {
        if ((utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI
            || utilisateur.getEtablissementEnCours().voir_stock_total()) && utilisateur.isVisuStock()
            && utilisateur.isStockDisponibleVisible()) {
          // On ne recalcule pas les stocks alors qu'on l'a déjà fait
          if (!utilisateur.getEtablissementEnCours().is_filtre_stock()) {
            if (utilisateur.getEtablissementEnCours().voir_stock_total()) {
              dispo =
                  retourneDispoArticleSQLunMagasin(utilisateur, utilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                      utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(), null);
            }
            else {
              dispo = retourneDispoArticleSQL(utilisateur, utilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                  utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString());
            }
            // Si le stock est null ou < à 0
            if (dispo == null || dispo.startsWith("-")) {
              dispo = "0.000";
            }
            // On rajoute le stock à l'article
            utilisateur.getDerniereListeArticle().get(i).setField("STOCK", dispo);
          }
          // Gestion du stock attendu dans la liste... BEEUUUUURK
          if (utilisateur.getMonPanier() != null && utilisateur.getEtablissementEnCours().isVoir_stock_attendu()
              && utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI
              && utilisateur.getMonPanier().getMagasinPanier() != null) {
            attendu = retournerStockAttendu(utilisateur, utilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
                utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
                utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
            if (attendu != null) {
              utilisateur.getDerniereListeArticle().get(i).setField("ATTEN", attendu);
            }
          }
        }
        
        // Associer les tarifs au record déjà présent
        if (!utilisateur.getDerniereListeArticle().get(i).isPresentField("TARIF") && utilisateur.isPrixVisible()) {
          utilisateur.getDerniereListeArticle().get(i).setField("TARIF",
              retournerTarifClient(utilisateur, utilisateur.getEtablissementEnCours().getIdEtablissement(),
                  utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
                  utilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString().trim()));
        }
        
        // Gestion des promotions
        if (estEnPromotion(utilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString().trim(),
            utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString().trim(), utilisateur)) {
          utilisateur.getDerniereListeArticle().get(i).setField("PROMO", "1");
        }
        
        utilisateur.getDerniereListePartiellePagination().add(utilisateur.getDerniereListeArticle().get(i));
      }
    }
    // Pour les modes au dessus de clients (représentant, boutique, gestionnaire, admin)
    else if ((utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_BOUTIQUE && utilisateur.getClient() == null)
        || utilisateur.peutAccederAuCatalogue()) {
      GenericRecord record = null;
      for (int i = indice; i < utilisateur.getDerniereListeArticle().size()
          && i < indice + utilisateur.getEtablissementEnCours().getLimit_liste(); i++) {
        // Associer les tarifs au record déjà présent
        record = retourneTarifGeneralArticle(utilisateur, utilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
            utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString().trim(),
            utilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString().trim());
        if (record != null && record.isPresentField("ATP01") && utilisateur.getDerniereListeArticle().get(i).isPresentField("A1TVA")) {
          if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_BOUTIQUE) {
            BigDecimal montantTTC =
                retournerTTC(utilisateur, record.getField("ATP01"), utilisateur.getDerniereListeArticle().get(i).getField("A1TVA"));
            utilisateur.getDerniereListeArticle().get(i).setField("TARIF", montantTTC);
          }
          else {
            utilisateur.getDerniereListeArticle().get(i).setField("TARIF", record.getField("ATP01"));
          }
        }
        else {
          utilisateur.getDerniereListeArticle().get(i).setField("TARIF", new BigDecimal(0));
        }
        
        // Gestion des promotions
        if (estEnPromotion(utilisateur.getDerniereListeArticle().get(i).getField("A1ETB").toString().trim(),
            utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString().trim(), utilisateur)) {
          utilisateur.getDerniereListeArticle().get(i).setField("PROMO", "1");
        }
        
        utilisateur.getDerniereListePartiellePagination().add(utilisateur.getDerniereListeArticle().get(i));
      }
    }
    // Clients mais sans visuStock //UN PEU BIZARRE CETTE MANIERE DE FAIRE MAIS... JE REPASSERAI DESSUS
    else {
      utilisateur.controleGestionTarif();
      for (int i = indice; i < utilisateur.getDerniereListeArticle().size()
          && i < indice + utilisateur.getEtablissementEnCours().getLimit_liste(); i++) {
        if (utilisateur.getClient() != null) {
          // Associer les tarifs au record déjà présent
          utilisateur.getDerniereListeArticle().get(i).setField("TARIF",
              retournerTarifClient(utilisateur, utilisateur.getEtablissementEnCours().getIdEtablissement(),
                  utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString(),
                  utilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString().trim()));
          
          Trace.debug(
              "retourneMorceauListe()2 Tarif liste Article " + utilisateur.getDerniereListeArticle().get(i).getField("A1ART").toString()
                  + "/ Unité: " + utilisateur.getDerniereListeArticle().get(i).getField("A1UNV").toString() + ":|"
                  + utilisateur.getGestionTarif().getTarif().getValue() + "|");
        }
        else {
          utilisateur.getDerniereListeArticle().get(i).setField("TARIF", new BigDecimal(0));
        }
        
        utilisateur.getDerniereListePartiellePagination().add(utilisateur.getDerniereListeArticle().get(i));
      }
    }
    
    return utilisateur.getDerniereListePartiellePagination();
  }
  
  /**
   * Retourne le tarif général (T1) d'un article
   */
  public GenericRecord retourneTarifGeneralArticle(UtilisateurWebshop utilisateur, String etb, String codArt, String unite) {
    if (unite == null || etb == null || codArt == null || utilisateur == null) {
      return null;
    }
    else {
      unite = traiterCaracteresSpeciauxSQL(unite).trim();
      etb = traiterCaracteresSpeciauxSQL(etb).trim();
      codArt = traiterCaracteresSpeciauxSQL(codArt).trim();
    }
    
    ArrayList<GenericRecord> liste = null;
    String requete = null;
    // Gestion décimales
    if (utilisateur.getEtablissementEnCours().getDecimales_client() > 0) {
      requete = "SELECT DECIMAL((CASE WHEN SUBSTR('" + unite + "', 2, 1)='*' THEN ATP01/100 ELSE ATP01 END), 9, 4) AS ATP01 " + " FROM "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_TARIFS " + " WHERE ATETB = '" + etb + "' AND ATART = '" + codArt + "' "
          + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE;
    }
    // Francs Pacifiques
    else {
      requete = "SELECT (ATP01 * 100) AS ATP01 " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_TARIFS " + " WHERE ATETB = '"
          + etb + "' AND ATART = '" + codArt + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE;
    }
    
    liste = utilisateur.getAccesDB2().select(requete);
    
    if (liste != null && liste.size() > 0) {
      Trace.debug("[retourneTarifGeneralArticle()] " + codArt + " : " + liste.get(0).getField("ATP01").toString());
      
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Permet de vérifier si un article est en promotion
   */
  private static boolean estEnPromotion(String pEtb, String pCodeArticle, UtilisateurWebshop pUtil) {
    if (pEtb == null || pCodeArticle == null || pUtil == null) {
      return false;
    }
    
    boolean estEnpromo = false;
    
    if (pUtil.getListePromotionsActives() != null) {
      int i = 0;
      while (estEnpromo == false && i < pUtil.getListePromotionsActives().size()) {
        if (pUtil.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
            && pUtil.getListePromotionsActives().get(i).getField("PR_A1ART") != null
            && pUtil.getListePromotionsActives().get(i).isPresentField("PR_A1ETB")
            && pUtil.getListePromotionsActives().get(i).getField("PR_A1ETB") != null) {
          estEnpromo = (pUtil.getListePromotionsActives().get(i).getField("PR_A1ART").toString().trim().equals(pCodeArticle)
              && pUtil.getListePromotionsActives().get(i).getField("PR_A1ETB").toString().trim().equals(pEtb));
        }
        
        i++;
      }
    }
    
    return estEnpromo;
  }
  
  /**
   * retourner le montant TTC d'un tarif HT en fonction du code tva de l'article et des taux associés à l'utilisateur et son établissement
   */
  public BigDecimal retournerTTC(UtilisateurWebshop pUtil, Object pHt, Object pCode) {
    if (pUtil == null || pHt == null || pCode == null) {
      return null;
    }
    
    BigDecimal montant = (BigDecimal) pHt;
    String codeTVA = pCode.toString().trim();
    BigDecimal tauxTVA = null;
    
    if (montant.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }
    
    if (codeTVA.equals("0")) {
      codeTVA = "1";
    }
    
    if (codeTVA.equals("1")) {
      tauxTVA = pUtil.getEtablissementEnCours().getTaux_tva_1();
    }
    else if (codeTVA.equals("2")) {
      tauxTVA = pUtil.getEtablissementEnCours().getTaux_tva_2();
    }
    else if (codeTVA.equals("3")) {
      tauxTVA = pUtil.getEtablissementEnCours().getTaux_tva_3();
    }
    else if (codeTVA.equals("4")) {
      tauxTVA = pUtil.getEtablissementEnCours().getTaux_tva_4();
    }
    else if (codeTVA.equals("5")) {
      tauxTVA = pUtil.getEtablissementEnCours().getTaux_tva_5();
    }
    else if (codeTVA.equals("6")) {
      tauxTVA = pUtil.getEtablissementEnCours().getTaux_tva_6();
    }
    else {
      tauxTVA = BigDecimal.ZERO;
    }
    
    if (tauxTVA == null || tauxTVA.compareTo(BigDecimal.ZERO) == 0) {
      return montant;
    }
    
    montant =
        montant.add((montant.multiply(tauxTVA).divide(Constantes.VALEUR_CENT))).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    return montant;
  }
  
  /**
   * Vérifie qu'il existe des articles en promotion dans DB2
   */
  public boolean existeDesPromos(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return false;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2().select(
        " SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".PROMOTION " + " WHERE PR_DATED <= '" + Outils.recupererDateCouranteInt()
            + "' AND PR_DATEF >= '" + Outils.recupererDateCouranteInt() + "' ORDER BY PR_DATEF FETCH FIRST 3 ROWS ONLY ");
    
    return (liste != null && liste.size() > 0);
  }
  
  /**
   * Retourne le libellé du fournisseur
   */
  public GenericRecord retourneNomFournisseur(UtilisateurWebshop utilisateur, String codFrs) {
    if (codFrs == null || utilisateur == null) {
      return null;
    }
    else {
      codFrs = traiterCaracteresSpeciauxSQL(codFrs);
    }
    
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2()
        .select(" SELECT FRAWS ,FRNWS, FRNOM FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMFRSM " + " WHERE FRETB = '"
            + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND DIGITS(FRCOL) CONCAT DIGITS(FRFRS) = '" + codFrs + "' "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste != null && liste.size() > 0) {
      traiterNomFournisseur(liste.get(0));
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourne le détail pour un article
   */
  public GenericRecord retourneDetailArticle(UtilisateurWebshop utilisateur, String etb, String article) {
    ArrayList<GenericRecord> liste = null;
    if (etb == null || article == null || utilisateur == null) {
      return null;
    }
    else {
      etb = traiterCaracteresSpeciauxSQL(etb);
      article = traiterCaracteresSpeciauxSQL(article);
    }
    
    String zonesSupplementairesRechercheArticle = null;
    ArrayList<GenericRecord> listeRecherche =
        utilisateur.getAccesDB2().select("SELECT RE_CODE FROM " + MarbreEnvironnement.BIBLI_WS + ".RECHARTICL WHERE RE_VAL = '1' ");
    if (listeRecherche != null) {
      for (GenericRecord record : listeRecherche) {
        if (record.getField("RE_CODE").toString().trim().equals("A1CL1")) {
          zonesSupplementairesRechercheArticle = " A1CL1 ,";
        }
        if (record.getField("RE_CODE").toString().trim().equals("A1CL2")) {
          if (zonesSupplementairesRechercheArticle == null) {
            zonesSupplementairesRechercheArticle = " A1CL2 ,";
          }
          else {
            zonesSupplementairesRechercheArticle += " A1CL2 ,";
          }
        }
      }
      
      if (zonesSupplementairesRechercheArticle == null) {
        zonesSupplementairesRechercheArticle = "";
      }
    }
    else {
      return null;
    }
    
    liste = utilisateur.getAccesDB2()
        .select(" SELECT A1ETB, A1ART, A1LIB, A1FAM, A1SFA, FRAWS ,FRNWS, FRNOM, CAREF, A1UNV, DECIMQ, A1IN2, CARFC, "
            + zonesSupplementairesRechercheArticle + " A1NPU, A1TVA, LIBCND as LIBUNV , CND AS "
            + MarbreEnvironnement.ZONE_CONDITIONNEMENT + " " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT "
            + " WHERE A1ETB = '" + etb + "' " + " AND A1ART = '" + article + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE
            + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    // traitement des tarifs et des stocks
    if (liste != null && liste.size() == 1) {
      GenericRecord record = liste.get(0);
      traiterNomFournisseur(record);
      // Clients ou représentant un client
      if ((utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT
          || utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CONSULTATION
          || utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) && utilisateur.getClient() != null) {
        if (utilisateur.isPrixVisible()) {
          IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getField("A1ETB").toString());
          BigDecimal tarif = retournerTarifClient(utilisateur, idEtablissement, record.getField("A1ART").toString().trim(),
              record.getField("A1UNV").toString().trim());
          if (tarif == null || tarif.compareTo(ZERO) == 0) {
            tarif = ZERO;
          }
          
          record.setField("TARIF", tarif);
        }
        else {
          record.setField("TARIF", ZERO);
        }
      }
      // en mode tarif public (admin, gestionnaire, boutique et représentants sans client)
      else {
        GenericRecord recordTarif = retourneTarifGeneralArticle(utilisateur, record.getField("A1ETB").toString(),
            record.getField("A1ART").toString().trim(), record.getField("A1UNV").toString().trim());
        if (recordTarif != null && recordTarif.isPresentField("ATP01") && record.isPresentField("A1TVA")) {
          if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_BOUTIQUE) {
            BigDecimal montantTTC = retournerTTC(utilisateur, recordTarif.getField("ATP01"), record.getField("A1TVA"));
            record.setField("TARIF", montantTTC);
          }
          else {
            record.setField("TARIF", recordTarif.getField("ATP01"));
          }
        }
        else {
          record.setField("TARIF", new BigDecimal(0));
        }
      }
      // Stocks
      if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() > MarbreEnvironnement.MODE_NON_CHOISI
          && utilisateur.isStockDisponibleVisible()) {
        record.setField("STOCK", retourneDispoArticleSQL(utilisateur, etb, article));
      }
      
      // Gestion des promotions
      if (estEnPromotion(record.getField("A1ETB").toString().trim(), record.getField("A1ART").toString().trim(), utilisateur)) {
        record.setField("PROMO", "1");
      }
      
      String memo =
          retournerMemoArticleWS(utilisateur, record.getField("A1ETB").toString().trim(), record.getField("A1ART").toString().trim());
      if (memo != null) {
        record.setField("MEMO", memo);
      }
      
      return record;
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourne le stock dispo pour un article à partir d'un programme GAP
   * 
   */
  public String retourneDispoArticle(UtilisateurWebshop utilisateur, String etb, String article) {
    BigDecimal dispoStock = null;
    String dispo = null;
    
    String magasinRetrait = retourneMagasinRetrait(utilisateur);
    
    if (utilisateur.getGestionStock().execute(etb, article, utilisateur.getMagasinSiege().getCodeMagasin(), magasinRetrait,
        new BigDecimal(0), new BigDecimal(0))) {
      dispoStock = utilisateur.getGestionStock().getStock1().getValue().add(utilisateur.getGestionStock().getStock2().getValue());
      Trace.debug("retourneDispoArticle() " + etb + "/" + article + " dispoStock Brut: " + dispoStock.toString());
    }
    
    if (dispoStock == null) {
      return "";
    }
    else if (dispoStock.toString().length() > 1) {
      dispo = dispoStock.toString().substring(0, dispoStock.toString().length() - 3) + "."
          + dispoStock.toString().substring(dispoStock.toString().length() - 3);
    }
    else {
      dispo = dispoStock.toString();
    }
    
    Trace.debug("retourneDispoArticle() " + etb + "/" + article + " dispo traitée: " + dispo);
    return dispo;
  }
  
  /**
   * Retourner le mémo type WebShop "WS" d'un article
   */
  private String retournerMemoArticleWS(UtilisateurWebshop pUtilisateur, String pEtb, String pArticle) {
    if (pUtilisateur == null || pEtb == null || pArticle == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select(" SELECT PONUM FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMOBPM " + " WHERE POCOD = 'A' AND POETB ='" + pEtb
            + "' AND POTYP = '" + MEMO_WS + "' " + " AND POIND = '" + pArticle + "' ORDER BY POCRE DESC "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste == null || liste.size() == 0) {
      return null;
    }
    
    String ponum = null;
    if (liste.get(0).isPresentField("PONUM")) {
      ponum = liste.get(0).getField("PONUM").toString().trim();
    }
    else {
      return null;
    }
    
    String obind = retournerInDiceObs(pArticle, ponum);
    
    String retour = "";
    
    liste = pUtilisateur.getAccesDB2().select("SELECT OBZON FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMOBSM "
        + " WHERE OBCOD = 'a' " + " AND OBETB ='" + pEtb + "' AND OBIND = '" + obind + "' ORDER BY OBSUF ");
    
    if (liste == null || liste.size() == 0) {
      return null;
    }
    
    for (GenericRecord record : liste) {
      if (record.isPresentField("OBZON")) {
        retour += record.getField("OBZON").toString().trim();
      }
    }
    
    return retour;
  }
  
  /**
   * Retourner l'indice qui permet de retourner le bon mémo en fonction de l'article et son numéro de mémo
   */
  private String retournerInDiceObs(String pCodeArticle, String pNumero) {
    if (pCodeArticle == null || pNumero == null) {
      return null;
    }
    int nbCodeArticleMax = 26;
    int nbIndiceMax = 4;
    int nbCodeArticle = pCodeArticle.length();
    int nbNumero = pNumero.length();
    
    StringBuilder retour = new StringBuilder();
    retour.append(pCodeArticle);
    for (int i = 0; i < nbCodeArticleMax - nbCodeArticle; i++) {
      retour.append(" ");
    }
    
    String indiceFinal = "";
    for (int i = 0; i < nbIndiceMax - nbNumero; i++) {
      indiceFinal += "0";
    }
    indiceFinal += pNumero;
    retour.append(indiceFinal);
    
    return retour.toString();
  }
  
  /**
   * Permet de retourner la quantité du lot dont la quantité est la plus importante
   */
  public BigDecimal retournerLotMaxArticle(UtilisateurWebshop pUtilisateur, String pEtb, String pArticle) {
    BigDecimal retour = BigDecimal.ZERO;
    if (pUtilisateur == null || pEtb == null || pArticle == null) {
      return retour;
    }
    // Au stade deconnecté l'utilisateur n'a pas encore de magasin siège associé. Il faut prendre celui déterminé par l'établissement
    Etablissement etablissement = pUtilisateur.getEtablissementEnCours();
    // Pas normal
    if (etablissement == null) {
      return BigDecimal.ZERO;
    }
    String magasin = etablissement.getMagasin_siege();
    if (pUtilisateur.getMonPanier() != null && pUtilisateur.getMonPanier().getMagasinPanier() != null) {
      magasin = pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select(" SELECT (SLQTE-SLQTS) AS DIFF FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_LOTS + " WHERE SLETB = '"
            + pEtb + "' AND SLMAG ='" + magasin + "' AND SLART = '" + pArticle + "' ORDER BY DIFF DESC "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste == null) {
      return retour;
    }
    
    if (liste.size() == 0) {
      return retour;
    }
    
    try {
      retour = (BigDecimal) liste.get(0).getField("DIFF");
    }
    catch (Exception e) {
      return BigDecimal.ZERO;
    }
    
    return retour;
  }
  
  /**
   * Permet de retourner la quantité des 3 lots dont la quantité est la plus importante
   */
  public String retournerLotsArticle(UtilisateurWebshop pUtilisateur, String pEtb, String pArticle, int nbdecimales) {
    String retour = null;
    if (pUtilisateur == null || pEtb == null || pArticle == null) {
      return retour;
    }
    
    String magasin = pUtilisateur.getMagasinSiege().getCodeMagasin();
    if (pUtilisateur.getMonPanier() != null && pUtilisateur.getMonPanier().getMagasinPanier() != null) {
      magasin = pUtilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
    }
    
    ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2()
        .select(" SELECT (SLQTE-SLQTS) AS DIFF FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_LOTS + " WHERE SLETB = '"
            + pEtb + "' AND SLMAG ='" + magasin + "' AND SLART = '" + pArticle + "' ORDER BY DIFF DESC FETCH FIRST 3 ROWS ONLY "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste == null) {
      return null;
    }
    
    if (liste.size() == 0) {
      return null;
    }
    
    retour = "";
    for (GenericRecord record : liste) {
      if (!retour.isEmpty()) {
        retour += ",";
      }
      try {
        BigDecimal valeur = (BigDecimal) record.getField("DIFF");
        valeur = valeur.setScale(nbdecimales, RoundingMode.HALF_UP);
        retour += valeur.toString().trim();
      }
      catch (Exception e) {
        retour += BigDecimal.ZERO.toString();
      }
    }
    return retour;
  }
  
  /**
   * NEW Retourne la magasin de retrait
   * 
   * @param utilisateur
   * @return
   */
  private String retourneMagasinRetrait(UtilisateurWebshop utilisateur) {
    String magasinRetrait = " ";
    if ((utilisateur.getMonPanier() != null) && (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT)
        && !utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin().trim()
            .equals(utilisateur.getMagasinSiege().getCodeMagasin().trim())) {
      magasinRetrait = utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
    }
    
    return magasinRetrait;
  }
  
  /**
   * Permet de retourner le tarif d'un article avec la condition du client
   */
  protected BigDecimal retournerTarifClient(UtilisateurWebshop pUtil, IdEtablissement pIdEtablissement, String pArticle, String pUnite) {
    if (pUtil == null || pIdEtablissement == null || pArticle == null) {
      return null;
    }
    
    BigDecimal valeurBrute = null;
    pUtil.controleGestionTarif();
    if (pUtil.getGestionTarif().execute(pIdEtablissement, new BigDecimal(pUtil.getClient().getNumeroClient()),
        new BigDecimal(pUtil.getClient().getSuffixeClient()), pArticle, new BigDecimal(1))) {
      valeurBrute = pUtil.getGestionTarif().getTarif().getValue();
      Trace.debug("Article : " + pIdEtablissement.getCodeEtablissement() + "/" + pArticle);
      Trace.debug(" Decimales client: " + pUtil.getEtablissementEnCours().getDecimales_client());
      Trace.debug(" retournerTarifClient AV: " + valeurBrute);
      if (valeurBrute != null && valeurBrute.intValue() != 0) {
        if (pUtil.getEtablissementEnCours().getDecimales_client() == 4
            || (pUnite != null && pUnite.toString().endsWith(MarbreEnvironnement.CODE_QUATRE_DECIMALE))) {
          valeurBrute = valeurBrute.divide(DIXMILLE);
        }
        else if (pUtil.getEtablissementEnCours().getDecimales_client() == 2) {
          valeurBrute = valeurBrute.divide(CENT);
        }
      }
      Trace.debug(" retournerTarifClient AP: " + valeurBrute);
    }
    return valeurBrute;
  }
  
  /**
   * Retourne le stock dispo pour un article (via SQL) en fonction du mode de récupération, multi-magasins et type de
   * stock de retrait
   */
  public String retourneDispoArticleSQL(UtilisateurWebshop utilisateur, String etb, String article) {
    if (utilisateur == null || etb == null || article == null) {
      return null;
    }
    
    etb = traiterCaracteresSpeciauxSQL(etb);
    article = traiterCaracteresSpeciauxSQL(article);
    
    if (calculDeStocks == null) {
      calculDeStocks = new CalculStocks();
    }
    
    if (calculDeStocks != null && utilisateur.getMonPanier() != null) {
      // En fonction du mode de livraison
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        // Si est pas mono magasin
        if (!utilisateur.getEtablissementEnCours().is_mono_magasin()) {
          return calculDeStocks.retournerStock(utilisateur, etb, article, utilisateur.getMagasinSiege().getCodeMagasin());
        }
        else {
          return calculDeStocks.retournerStock(utilisateur, etb, article, utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
        }
      }
      else {
        // Si le stock retrait est un stock SIEGE + MAGASIN et qu'on est un établissement multi Magasins
        if (utilisateur.getEtablissementEnCours().isRetrait_is_siege_et_mag()
            && !utilisateur.getEtablissementEnCours().is_mono_magasin()) {
          return calculDeStocks.retournerStock(utilisateur, etb, article, utilisateur.getMagasinSiege().getCodeMagasin(),
              utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
        }
        else {
          return calculDeStocks.retournerStock(utilisateur, etb, article, utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
        }
      }
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourne le stock dispo d'un article SQL d'un magasin
   */
  public String retourneDispoArticleSQLunMagasin(UtilisateurWebshop utilisateur, String etb, String article, String codeMag) {
    if (utilisateur == null || etb == null || article == null) {
      return null;
    }
    
    etb = traiterCaracteresSpeciauxSQL(etb);
    article = traiterCaracteresSpeciauxSQL(article);
    if (calculDeStocks == null) {
      calculDeStocks = new CalculStocks();
    }
    
    if (calculDeStocks != null) {
      // pas de magasin transmis
      if (codeMag == null) {
        // C'est normal on veut le stock de tous les magasins
        if (utilisateur.getEtablissementEnCours().voir_stock_total()) {
          if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI
              && utilisateur.getMonPanier().getMagasinPanier() != null) {
            codeMag = utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin();
          }
          return calculDeStocks.retournerStock(utilisateur, etb, article, codeMag);
        }
        else {
          return null;
        }
      }
      // Si le stock retrait est un stock SIEGE + MAGASIN et qu'on est un établissement multi Magasins
      else if (utilisateur.getEtablissementEnCours().isRetrait_is_siege_et_mag()
          && !utilisateur.getEtablissementEnCours().is_mono_magasin()) {
        return calculDeStocks.retournerStock(utilisateur, etb, article, utilisateur.getMagasinSiege().getCodeMagasin(), codeMag);
      }
      else {
        return calculDeStocks.retournerStock(utilisateur, etb, article, codeMag);
      }
    }
    else {
      return null;
    }
  }
  
  // Permet de retourner le lien d'une photo par sa référence fournisseur dans l'IFS ou sur un système d'hébergement
  public String recupererIFSphoto(UtilisateurWebshop utilisateur, String nomImage) {
    String retour = null;
    
    if (utilisateur.getEtablissementEnCours().getChemin_images_articles() != null) {
      retour = "src='" + utilisateur.getEtablissementEnCours().getChemin_images_articles() + nomImage + "."
          + utilisateur.getEtablissementEnCours().getExtensions_photos() + "'";
    }
    
    return retour;
  }
  
  /**
   * Permet de retourner le lien d'une photo par sa référence fournisseur dans
   * une URL fournisseur saisie dans Série M
   * 
   * @param refFournisseur
   * @return
   */
  public String recupererURLphoto(String refFournisseur, String etb, String article, UtilisateurWebshop utilisateur) {
    if (verifierPresencePSEMURLM(utilisateur)) {
      return recupererPhotoURL(utilisateur, etb, article);
    }
    else {
      return null;
    }
  }
  
  /**
   * Récupérer l'URL d'une image article si elle existe dans PSEMURLM
   */
  private String recupererPhotoURL(UtilisateurWebshop utilisateur, String etb, String article) {
    String retour = null;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return retour;
    }
    else {
      etb = traiterCaracteresSpeciauxSQL(etb);
      article = traiterCaracteresSpeciauxSQL(article);
    }
    
    // Si le PSEMURLM existe
    if (MarbreEnvironnement.PSEMURLM_IS_OK) {
      ArrayList<GenericRecord> liste = null;
      
      liste = utilisateur.getAccesDB2()
          .select("SELECT URADR FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMURNM WHERE URETB = '" + etb.trim()
              + "' AND URCOD = 'A'  AND URARG = '" + article.trim() + "' AND URTYP = 'PHW'  " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      
      if (liste != null && liste.size() > 0 && liste.get(0).isPresentField("SUADR")) {
        retour = "src='" + liste.get(0).getField("SUADR") + "' ";
      }
    }
    
    return retour;
  }
  
  /**
   * Récupérer l'URL d'une fiche technique article si elle existe dans PSEMURLM
   */
  public String recupererFicheTechniqueURL(UtilisateurWebshop utilisateur, String etb, String article) {
    String retour = null;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return retour;
    }
    else {
      etb = traiterCaracteresSpeciauxSQL(etb);
      article = traiterCaracteresSpeciauxSQL(article);
    }
    ArrayList<GenericRecord> liste = null;
    
    liste = utilisateur.getAccesDB2().select(
        " SELECT FTIND,FTURL FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_FICHES_TECHNIQUES + " WHERE FTETB = '"
            + etb.trim() + "' AND FTART = '" + article.trim() + "' AND FTWEB = '1'  " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE);
    if (liste != null && liste.size() > 0 && liste.get(0).isPresentField("FTURL")) {
      retour = liste.get(0).getField("FTURL").toString().trim();
    }
    
    return retour;
  }
  
  /**
   * Permet de vérifier la présence du PSEMURLM une seule fois
   */
  private boolean verifierPresencePSEMURLM(UtilisateurWebshop utilisateur) {
    // Si on a pas déjà checké le bouzin
    if (!MarbreEnvironnement.CHECK_PSEMURLM) {
      ArrayList<GenericRecord> liste = null;
      if (utilisateur == null || utilisateur.getAccesDB2() == null) {
        return false;
      }
      liste = utilisateur.getAccesDB2().select(
          "SELECT 1 FROM QSYS2.SYSTABLES WHERE TABLE_SCHEMA = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' AND TABLE_NAME = 'PSEMURNM' ");
      
      MarbreEnvironnement.CHECK_PSEMURLM = true;
      MarbreEnvironnement.PSEMURLM_IS_OK = (liste != null && liste.size() == 1);
    }
    
    return MarbreEnvironnement.PSEMURLM_IS_OK;
  }
  
  /**
   * Permet de vérifier si cet article est vendable ou non: dans un premier
   * temps par son tarif
   */
  public boolean verifierSiVendable(BigDecimal tarif, boolean pOnVendAzero) {
    
    if (tarif == null) {
      return false;
    }
    
    if (tarif.compareTo(ZERO) == 0) {
      return pOnVendAzero;
    }
    else {
      return true;
    }
  }
  
  /**
   * Retourner La liste de tous les magasins
   */
  public ArrayList<Magasin> retournerTousLesMagasins(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return null;
    }
    
    ArrayList<Magasin> liste = null;
    
    ArrayList<GenericRecord> listePartielle =
        utilisateur.getAccesDB2().select("SELECT MG_NAME,MG_COD FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB='"
            + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND MG_ACTIF = '1' ");
    if (listePartielle != null && listePartielle.size() > 0) {
      liste = new ArrayList<Magasin>();
      
      for (int i = 0; i < listePartielle.size(); i++) {
        liste.add(new Magasin(utilisateur, listePartielle.get(i).getField("MG_COD").toString().trim(),
            listePartielle.get(i).getField("MG_NAME").toString().trim()));
      }
    }
    
    return liste;
  }
  
  /**
   * Enlever ou mettre un article dans ses favoris
   */
  public int majFavoriArticle(UtilisateurWebshop utilisateur, String etb, String article) {
    int retour = 0;
    
    if (utilisateur == null || etb == null || article == null) {
      return retour;
    }
    
    retour = isEnFavori(utilisateur, etb, article);
    if (retour == 1) {
      if (utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".FAVORIS (FV_UTIL,FV_ETB,FV_ARTI) VALUES ('"
          + utilisateur.getIdentifiant() + "','" + etb + "','" + article + "') ") == 1) {
        retour = 2;
      }
      else {
        retour = 0;
      }
    }
    else if (retour == 2) {
      if (utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".FAVORIS WHERE FV_UTIL = '"
          + utilisateur.getIdentifiant() + "' AND FV_ETB = '" + etb + "' AND FV_ARTi = '" + article + "'") == 1) {
        retour = 1;
      }
      else {
        retour = 0;
      }
    }
    
    return retour;
  }
  
  /**
   * Récupérer l'état de l'article: Favori ou non
   */
  public int isEnFavori(UtilisateurWebshop utilisateur, String etb, String article) {
    int retour = 0;
    
    if (utilisateur == null || etb == null || article == null) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select("SELECT FV_ID FROM " + MarbreEnvironnement.BIBLI_WS
        + ".FAVORIS WHERE FV_UTIL = '" + utilisateur.getIdentifiant() + "' AND FV_ETB = '" + etb + "' AND FV_ARTI = '" + article + "' ");
    if (liste != null) {
      if (liste.size() > 0) {
        retour = 2;
      }
      else {
        retour = 1;
      }
    }
    
    return retour;
  }
  
  /**
   * Récupérer les articles favoris de l'utilisateur sur l'établisssement courant
   */
  public ArrayList<GenericRecord> retournerListeArticlesFavoris(UtilisateurWebshop utilisateur) {
    ArrayList<GenericRecord> liste = null;
    
    if (utilisateur == null) {
      return liste;
    }
    
    String filtreFrs = " ";
    
    liste = utilisateur.getAccesDB2().select("SELECT FV_ARTI FROM " + MarbreEnvironnement.BIBLI_WS + ".FAVORIS WHERE FV_UTIL = '"
        + utilisateur.getIdentifiant() + "' AND FV_ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB() + "' ");
    
    // Il existe des articles dans les favoris de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (utilisateur.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + utilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + utilisateur.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      for (int i = 0; i < liste.size(); i++) {
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        
        arguments += liste.get(i).getField("FV_ARTI").toString().trim() + "'";
      }
      arguments += ")";
      
      Trace.erreur("Arguments ARTICLES FAVORIS:" + arguments);
      
      liste = utilisateur.getAccesDB2().select(
          "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND , A1ART, A1COF, A1FRS, A1TVA, FRAWS ,FRNWS, FRNOM "
              + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND A1ART IN " + arguments + filtreFrs
              + " ORDER BY A1ABC, A1SAI DESC, A1LIB FETCH FIRST " + utilisateur.getEtablissementEnCours().getLimit_requete_article()
              + " ROWS ONLY OPTIMIZE FOR " + utilisateur.getEtablissementEnCours().getLimit_requete_article() + " ROWS "
              + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      
    }
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    if (utilisateur.getDernierFiltreFournisseur() == null) {
      utilisateur.setDerniereListeArticleFavoris(liste);
    }
    
    // attribuer la liste à l'utilisateur
    utilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Récupérer les articles favoris de l'utilisateur sur l'établisssement courant
   */
  public ArrayList<GenericRecord> retournerListeArticlesAchats(UtilisateurWebshop utilisateur) {
    ArrayList<GenericRecord> liste = null;
    
    if (utilisateur == null) {
      return liste;
    }
    
    String filtreFrs = " ";
    
    liste = utilisateur.getAccesDB2().select("SELECT DISTINCT ART_ID,MAX(PA_DTC) AS DTMAX, MAX(PA_ID) AS IDMAX FROM "
        + MarbreEnvironnement.BIBLI_WS + "." + TableBDD.TABLE_WS_PANIER + ", " + MarbreEnvironnement.BIBLI_WS + "."
        + TableBDD.TABLE_WS_ARTI_PAN + " WHERE ART_PAN = PA_ID AND PA_US = '" + utilisateur.getIdentifiant() + "' AND ART_ETB = '"
        + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND PA_ETA > 0 " + " GROUP BY ART_ID ORDER BY DTMAX DESC, IDMAX DESC ");
    
    // Il existe des articles dans les favoris de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (utilisateur.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + utilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + utilisateur.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      String triSpecial = " ORDER BY CASE ";
      for (int i = 0; i < liste.size(); i++) {
        // arguments de recherche
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        
        arguments += liste.get(i).getField("ART_ID").toString().trim() + "'";
        
        // arguments de tri
        triSpecial += " WHEN A1ART='" + liste.get(i).getField("ART_ID").toString().trim() + "' THEN " + (i + 1) + " ";
      }
      arguments += ")";
      triSpecial += " END ";
      
      Trace.debug("Arguments ARTICLES Historique:" + arguments);
      
      liste = utilisateur.getAccesDB2().select(
          "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND , A1ART, A1COF, A1FRS, A1TVA, FRAWS ,FRNWS, FRNOM "
              + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.VUE_ART_UT + " WHERE A1ETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND A1ART IN " + arguments + filtreFrs + " " + triSpecial
              + "  FETCH FIRST " + utilisateur.getEtablissementEnCours().getQte_max_historique() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getEtablissementEnCours().getQte_max_historique() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      
    }
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    if (utilisateur.getDernierFiltreFournisseur() == null) {
      utilisateur.setDerniereListeArticleHistorique(liste);
    }
    
    // attribuer la liste à l'utilisateur
    utilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Ajouter un article aux articles consultés de l'utilisateur
   */
  public void ajouterAuxConsultes(UtilisateurWebshop utilisateur, String etb, String article) {
    ArrayList<GenericRecord> liste = null;
    
    if (utilisateur == null || etb == null || article == null) {
      return;
    }
    
    liste = utilisateur.getAccesDB2().select("SELECT CON_ID FROM " + MarbreEnvironnement.BIBLI_WS + ".CONSULTES WHERE CON_UTIL = '"
        + utilisateur.getIdentifiant() + "' AND CON_ETB = '" + etb + "' AND CON_ARTI = '" + article + "' ");
    
    // Si l'article n'est pas encore dans la table
    if (liste != null && liste.size() == 0) {
      utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".CONSULTES (CON_UTIL,CON_ETB,CON_ARTI) VALUES ('"
          + utilisateur.getIdentifiant() + "','" + etb + "','" + article + "')");
    }
    else if (liste != null && liste.size() == 1) {
      if (utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".CONSULTES WHERE CON_UTIL = '"
          + utilisateur.getIdentifiant() + "' AND CON_ETB = '" + etb + "' AND CON_ARTI = '" + article + "' ") == 1) {
        utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
            + ".CONSULTES (CON_UTIL,CON_ETB,CON_ARTI) VALUES ('" + utilisateur.getIdentifiant() + "','" + etb + "','" + article + "')");
      }
    }
  }
  
  /**
   * Retourner la liste des articles récemment consultés par un utilisateur
   */
  public ArrayList<GenericRecord> retournerListeArticlesConsultes(UtilisateurWebshop utilisateur) {
    ArrayList<GenericRecord> liste = null;
    
    if (utilisateur == null) {
      return liste;
    }
    
    String filtreFrs = " ";
    
    liste = utilisateur.getAccesDB2()
        .select("SELECT CON_ARTI FROM " + MarbreEnvironnement.BIBLI_WS + ".CONSULTES WHERE CON_UTIL = '" + utilisateur.getIdentifiant()
            + "' AND CON_ETB = '" + utilisateur.getEtablissementEnCours().getCodeETB() + "'  ORDER BY CON_ID DESC FETCH FIRST "
            + utilisateur.getEtablissementEnCours().getLimit_liste() + " ROWS ONLY ");
    
    // Il existe des articles dans les consultés de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (utilisateur.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + utilisateur.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + utilisateur.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      String triSpecial = " ORDER BY CASE ";
      for (int i = 0; i < liste.size(); i++) {
        // arguments de recherche
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        
        arguments += liste.get(i).getField("CON_ARTI").toString().trim() + "'";
        
        // arguments de tri
        triSpecial += "WHEN A1ART='" + liste.get(i).getField("CON_ARTI").toString().trim() + "' THEN " + (i + 1) + " ";
      }
      arguments += ")";
      triSpecial += " END ";
      
      Trace.debug("Arguments ARTICLES consultes:" + arguments);
      
      liste = utilisateur.getAccesDB2().select(
          "SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND , A1ART, A1COF, A1FRS, A1TVA, FRAWS ,FRNWS, FRNOM "
              + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '"
              + utilisateur.getEtablissementEnCours().getCodeETB() + "' AND A1ART IN " + arguments + filtreFrs + " " + triSpecial
              + "  FETCH FIRST " + utilisateur.getEtablissementEnCours().getQte_max_historique() + " ROWS ONLY OPTIMIZE FOR "
              + utilisateur.getEtablissementEnCours().getQte_max_historique() + " ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    if (utilisateur.getDernierFiltreFournisseur() == null) {
      utilisateur.setDerniereListeArticleConsultee(liste);
    }
    
    // attribuer la liste à l'utilisateur
    utilisateur.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Retourner la date d'attendu d'un article pour un magasin donné
   */
  public String retournerDateAttenduMagasin(UtilisateurWebshop utilisateur, String etb, String article, String codeMagasin) {
    String retour = "NR";
    if (utilisateur == null || etb == null || article == null || codeMagasin == null) {
      return retour;
    }
    
    String requete = null;
    String magSouhaite = null;
    
    if (utilisateur.getEtablissementEnCours().isIs_reassort_siege() && !utilisateur.getEtablissementEnCours().is_mono_magasin()
        && utilisateur.getMagasinSiege() != null) {
      magSouhaite = utilisateur.getMagasinSiege().getCodeMagasin();
    }
    else {
      magSouhaite = codeMagasin;
    }
    
    if (magSouhaite == null) {
      return retour;
    }
    
    // On récupère l'entête des bons de commande fournisseurs ainsi que les lignes
    // Si il existe une date de livraison prévue dans la ligne on la retourne en priorité sinon on retourne la date de livraison prévue de
    // l'entête. Dans la liste des retours on choisit la date la plus proche
    
    // Dans le cas du traitement de la commande propre au stock reservé
    if (MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE) {
      requete = "SELECT CASE WHEN LADLP<>0 then LADLP ELSE EADLP END AS DISPO," + " LAQTS,(SELECT SUM(AAQTE) AS QTERES FROM "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMALAM " + " WHERE lb.LAETB=AAETB AND lb.LAART=AAART AND lb.LANUM=AANOE "
          + " AND lb.LASUF=AASOE AND lb.LANLI=AALOE AND AATOS='V' AND AATOE IN ('G' , 'A') ) AS RESERVE " + " FROM "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMLBFM lb LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
          + ".PGVMEBFM ON LAETB=EAETB " + " AND LACOD=EACOD AND LANUM=EANUM AND EASUF=LASUF " + " WHERE LACOD='E' AND LAETB = '" + etb
          + "' AND LAART='" + article + "' AND LAMAG = '" + magSouhaite + "' AND EAREC=0 AND EAETA=1 AND EAIN9 <> 'I' "
          + " ORDER BY DISPO ASC ";
    }
    // Le cas classique sans traitment du stock reservé
    else {
      requete = " SELECT CASE WHEN LADLP<>0 THEN LADLP ELSE EADLP END AS DISPO FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMLBFM "
          + " INNER JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBFM "
          + " ON LAETB=EAETB AND LACOD=EACOD AND LANUM=EANUM AND EASUF=LASUF " + " WHERE LACOD='E' AND LAETB = '" + etb + "' AND LAART='"
          + article + "' " + " AND LAMAG = '" + magSouhaite + "' AND EAREC=0 AND EAETA=1 AND EAIN9 <> 'I' " + " ORDER BY DISPO ASC";
    }
    
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(requete);
    
    if (liste != null && liste.size() > 0) {
      GenericRecord record = null;
      
      if (MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE) {
        int i = 0;
        while (i < liste.size() && record == null) {
          if (!liste.get(i).isPresentField("RESERVE")) {
            liste.get(i).setField("RESERVE", "0");
          }
          if (liste.get(i).isPresentField("LAQTS")) {
            if (Float.valueOf(liste.get(i).getField("LAQTS").toString().trim())
                - Float.valueOf(liste.get(i).getField("RESERVE").toString().trim()) > Float.valueOf("0")) {
              record = liste.get(i);
            }
          }
          i++;
        }
      }
      else {
        record = liste.get(0);
      }
      
      if (record == null) {
        record = new GenericRecord();
      }
      
      if (!record.isPresentField("DISPO")) {
        record.setField("DISPO", "0");
      }
      
      int delaiDispo = Integer.valueOf(record.getField("DISPO").toString().trim());
      int delaiTransfert = 0;
      // Si on tient compte du stock du siège dans la date du réassort de ce magasin
      if (utilisateur.getEtablissementEnCours().isIs_reassort_siege() && !utilisateur.getEtablissementEnCours().is_mono_magasin()
          && delaiDispo > 0) {
        // On va lire le délai moyen qu'il existe entre le siège et ce magasin
        liste = utilisateur.getAccesDB2().select("SELECT DEL_JOURS FROM " + MarbreEnvironnement.BIBLI_WS + ".DELAISMOY WHERE DEL_ETB ='"
            + etb + "' AND DEL_MGDP = '" + magSouhaite + "' AND DEL_MGAR = '" + codeMagasin + "' ");
        if (liste != null && liste.size() == 1 && liste.get(0).isPresentField("DEL_JOURS")) {
          delaiTransfert = Integer.valueOf(liste.get(0).getField("DEL_JOURS").toString().trim());
        }
      }
      // addition et mise en forme
      retour =
          Outils.mettreAjourUneDateSerieM(delaiDispo, (utilisateur.getEtablissementEnCours().getDelai_jours_securite() + delaiTransfert));
      if (retour == null) {
        retour = "NR";
      }
    }
    
    return retour;
  }
  
  /**
   * Récupérer le stock attendu d'un article dans un magasisn
   */
  public String retournerStockAttendu(UtilisateurWebshop pUtilisateur, String pEtb, String pArticle, String pMag) {
    String retour = "0";
    if (pUtilisateur == null || pEtb == null || pArticle == null || pMag == null) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste = null;
    String requete = null;
    String magSouhaite = null;
    BigDecimal attendu = null;
    BigDecimal reserve = BigDecimal.ZERO;
    
    // Choix du magasin à scanner
    if (pUtilisateur.getEtablissementEnCours().isIs_reassort_siege() && !pUtilisateur.getEtablissementEnCours().is_mono_magasin()
        && pUtilisateur.getMagasinSiege() != null) {
      magSouhaite = pUtilisateur.getMagasinSiege().getCodeMagasin();
    }
    else {
      magSouhaite = pMag;
    }
    
    if (magSouhaite == null) {
      return retour;
    }
    
    requete = " SELECT S1ATT FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMSTKM " + " WHERE S1ETB = '" + pEtb + "' AND S1MAG = '"
        + magSouhaite + "' AND S1ART = '" + pArticle + "' ";
    
    liste = pUtilisateur.getAccesDB2().select(requete);
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("S1ATT")) {
        attendu = (BigDecimal) liste.get(0).getField("S1ATT");
      }
    }
    else {
      return retour;
    }
    
    if (MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE && attendu != null) {
      requete = " SELECT SUM(AAQTE) AS RESERVE FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMALAM " + " LEFT JOIN "
          + MarbreEnvironnement.BIBLI_CLIENTS
          + ".PGVMLBFM ON LASUF=AASOE AND LANLI=AALOE AND LAETB=AAETB AND LAART=AAART AND LANUM=AANOE " + " LEFT JOIN "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMEBFM ON LAETB=EAETB AND LACOD=EACOD AND LANUM=EANUM AND EASUF=LASUF "
          + " WHERE AATOS='V' AND AATOE IN ('G' , 'A') AND EAREC=0 AND EAETA=1 AND EAIN9 <> 'I' AND LACOD='E' " + " AND LAETB = '" + pEtb
          + "' AND LAART='" + pArticle + "' AND LAMAG = '" + magSouhaite + "' ";
      
      liste = pUtilisateur.getAccesDB2().select(requete);
      
      if (liste != null && liste.size() == 1) {
        if (liste.get(0).isPresentField("RESERVE")) {
          reserve = (BigDecimal) liste.get(0).getField("RESERVE");
        }
      }
    }
    
    if (attendu == null) {
      return BigDecimal.ZERO.toString();
    }
    
    attendu = attendu.subtract(reserve);
    retour = attendu.toString();
    return retour;
  }
  
  /**
   * Retourne l'article de substitution de l'article transmis s'il en existe un.
   * Nous récupérons les types de substitution suivants:
   * Rupture de stock A1IN4 = ' ' A1ASB = [code article valide]
   * Aiguillage A1IN4 = 'A' A1ASB = [code article valide]
   * Equivalent A1IN4 = 'E' A1ASB = [code article valide]
   * Remplacement A1IN4 = 'R' A1ASB = [code article valide]
   */
  public GenericRecord retourneArticleSubstitution(UtilisateurWebshop pUtil, String pEtb, String pArticle) {
    GenericRecord retour = null;
    
    String requete = " SELECT A1IN4,A1ASB FROM " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.TABLE_ARTICLE + " WHERE A1ETB = '"
        + pEtb + "' AND A1ART = '" + pArticle + "' ";
    
    ArrayList<GenericRecord> liste = pUtil.getAccesDB2().select(requete);
    
    if (liste != null && liste.size() == 1) {
      GenericRecord recTemp = liste.get(0);
      // On vérifie qu'on ait un code article de substitution
      if (recTemp.isPresentField("A1ASB") && !recTemp.getField("A1ASB").toString().trim().equals("")) {
        GenericRecord recArticle = retourneDetailArticle(pUtil, pEtb, recTemp.getField("A1ASB").toString().trim());
        if (recArticle != null && recArticle.isPresentField("A1ART")) {
          // On vérifie que le type de substitution existe et soit dans le pattern défini
          if (recTemp.isPresentField("A1IN4") && recTemp.getField("A1IN4") != null) {
            String codeSub = recTemp.getField("A1IN4").toString().trim();
            recArticle.setField("CODESUB", codeSub);
            if (codeSub.equals("") && pUtil.getEtablissementEnCours().voir_Subst_Rupture()) {
              recArticle.setField("LIBSUB", pUtil.getTraduction().traduire("$$infoRupture"));
            }
            else if (codeSub.equals("A") && pUtil.getEtablissementEnCours().voir_Subst_Aiguill()) {
              recArticle.setField("LIBSUB", pUtil.getTraduction().traduire("$$infoAiguillage"));
            }
            else if (codeSub.equals("E") && pUtil.getEtablissementEnCours().voir_Subst_Equival()) {
              recArticle.setField("LIBSUB", pUtil.getTraduction().traduire("$$infoEquivalent"));
            }
            else if (codeSub.equals("R") && pUtil.getEtablissementEnCours().voir_Subst_Remplac()) {
              recArticle.setField("LIBSUB", pUtil.getTraduction().traduire("$$infoRemplacement"));
            }
            
            retour = recArticle;
          }
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Retourner les articles de substitution de type variante d'un article passé en paramètre
   */
  public ArrayList<GenericRecord> retournerVarianteDunArticle(UtilisateurWebshop pUtil, String pEtb, String pArticle) {
    if (pUtil == null || pEtb == null || pArticle == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    String requete = " SELECT A1ETB, A1ART, A1IN4, A1ASB, A1CL1, A1CL2, A1FAM, CAREF FROM " + MarbreEnvironnement.BIBLI_CLIENTS
        + ".VUE_ART_UT " + " WHERE A1ETB = '" + pEtb + "' AND A1ART = '" + pArticle + "' AND A1IN4 = 'V' ";
    
    ArrayList<GenericRecord> listeSub = pUtil.getAccesDB2().select(requete);
    if (listeSub != null && listeSub.size() == 1) {
      // On est dans le cas d'un article qui possède une variante déclarée
      if (listeSub.get(0).isPresentField("A1ASB") && !listeSub.get(0).getField("A1ASB").toString().trim().equals("")) {
        if (gestionVariantes == null) {
          gestionVariantes = new GestionVariantes();
        }
        
        return gestionVariantes.retournerArticlesVariantes(pUtil, pEtb, pArticle, listeSub.get(0));
      }
    }
    
    return liste;
  }
  
  /**
   * En fonction des paramètres transmis, on trie la liste d'article déjà présente
   */
  public void filtrerUneListeArticles(UtilisateurWebshop pUtil) {
    if (pUtil.getDerniereListeArticle() == null) {
      return;
    }
    
    if (pUtil.getDerniereListeArticle().size() <= 1) {
      return;
    }
    
    String dispo = null;
    String attendu = null;
    
    // Tri par stock
    if (pUtil.getFiltres().isStock_positif()) {
      
      ArrayList<GenericRecord> listeFiltree = new ArrayList<GenericRecord>();
      ArrayList<GenericRecord> listeRebus = new ArrayList<GenericRecord>();
      BigDecimal valeur = null;
      
      for (int i = 0; i < pUtil.getDerniereListeArticle().size(); i++) {
        dispo = null;
        attendu = null;
        
        if (pUtil.getEtablissementEnCours().voir_stock_total()) {
          dispo = retourneDispoArticleSQLunMagasin(pUtil, pUtil.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtil.getDerniereListeArticle().get(i).getField("A1ART").toString(), null);
        }
        else {
          dispo = retourneDispoArticleSQL(pUtil, pUtil.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtil.getDerniereListeArticle().get(i).getField("A1ART").toString());
        }
        // Si le stock est null ou < à 0
        if (dispo == null || dispo.startsWith("-")) {
          dispo = "0.000";
        }
        // On rajoute le stock à l'article
        pUtil.getDerniereListeArticle().get(i).setField("STOCK", dispo);
        
        // Gestion du stock attendu dans la liste... BEEUUUUURK
        if (pUtil.getMonPanier() != null && pUtil.getEtablissementEnCours().isVoir_stock_attendu()
            && pUtil.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI
            && pUtil.getMonPanier().getMagasinPanier() != null) {
          attendu = retournerStockAttendu(pUtil, pUtil.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtil.getDerniereListeArticle().get(i).getField("A1ART").toString(),
              pUtil.getMonPanier().getMagasinPanier().getCodeMagasin());
          if (attendu != null) {
            pUtil.getDerniereListeArticle().get(i).setField("ATTEN", attendu);
          }
        }
        
        try {
          valeur = new BigDecimal(dispo);
          if (valeur.compareTo(BigDecimal.ZERO) == 0) {
            listeRebus.add(pUtil.getDerniereListeArticle().get(i));
          }
          else {
            listeFiltree.add(pUtil.getDerniereListeArticle().get(i));
          }
        }
        catch (Exception e) {
          Trace.erreur(e.getMessage());
          listeRebus.add(pUtil.getDerniereListeArticle().get(i));
        }
      }
      
      pUtil.setDerniereListeArticle(listeFiltree);
    }
    else {
      for (int i = 0; i < pUtil.getDerniereListeArticle().size(); i++) {
        dispo = null;
        if (pUtil.getEtablissementEnCours().voir_stock_total()) {
          dispo = retourneDispoArticleSQLunMagasin(pUtil, pUtil.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtil.getDerniereListeArticle().get(i).getField("A1ART").toString(), null);
        }
        else {
          dispo = retourneDispoArticleSQL(pUtil, pUtil.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtil.getDerniereListeArticle().get(i).getField("A1ART").toString());
        }
        // Si le stock est null ou < à 0
        if (dispo == null || dispo.startsWith("-")) {
          dispo = "0.000";
        }
        // On rajoute le stock à l'article
        pUtil.getDerniereListeArticle().get(i).setField("STOCK", dispo);
        
        // Gestion du stock attendu dans la liste... BEEUUUUURK
        if (pUtil.getMonPanier() != null && pUtil.getEtablissementEnCours().isVoir_stock_attendu()
            && pUtil.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI
            && pUtil.getMonPanier().getMagasinPanier() != null) {
          attendu = retournerStockAttendu(pUtil, pUtil.getDerniereListeArticle().get(i).getField("A1ETB").toString(),
              pUtil.getDerniereListeArticle().get(i).getField("A1ART").toString(),
              pUtil.getMonPanier().getMagasinPanier().getCodeMagasin());
          if (attendu != null) {
            pUtil.getDerniereListeArticle().get(i).setField("ATTEN", attendu);
          }
        }
      }
    }
  }
  
  /**
   * Retourner les paniers intuitifs actifs dans l'établissement passé en paramètre
   * Si pas d'établissement: tous les établissements seront pris
   */
  public static ArrayList<GenericRecord> recupererPanierIntuitifsActifs(UtilisateurWebshop pUtil) {
    
    if (pUtil == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    String requete = " SELECT IN_ID,IN_LIBELLE FROM " + MarbreEnvironnement.BIBLI_WS + ".INTUITIF WHERE IN_BIB = '" + pUtil.getBibli()
        + "' AND IN_ETB  = '" + pUtil.getEtablissementEnCours() + "' AND IN_STATUT = '1' ";
    
    if (pUtil.getEtablissementEnCours() == null) {
      requete = " SELECT IN_ID,IN_LIBELLE FROM " + MarbreEnvironnement.BIBLI_WS + ".INTUITIF WHERE IN_BIB = '" + pUtil.getBibli()
          + "' AND IN_STATUT = '1' ";
    }
    
    try {
      liste = pUtil.getAccesDB2().select(requete);
    }
    catch (MessageErreurException e) {
      Trace.erreur(e.getMessage());
    }
    
    if (liste != null && liste.size() == 0) {
      liste = null;
    }
    
    return liste;
  }
  
  public ArrayList<GenericRecord> retournerListeArticlesPanierIntuitif(UtilisateurWebshop pUtil, String pIdPanier) {
    
    ArrayList<GenericRecord> liste = null;
    
    if (pUtil == null || pIdPanier == null) {
      return liste;
    }
    
    String filtreFrs = " ";
    
    liste = pUtil.getAccesDB2()
        .select(" SELECT ART_CODE FROM " + MarbreEnvironnement.BIBLI_WS + ".ART_INTUIT WHERE ART_IN_ID = '" + pIdPanier + "' ");
    
    // Il existe des articles dans les favoris de l'utilisateur
    if (liste != null && liste.size() > 0) {
      // Filtre sur le fournisseur
      if (pUtil.getDernierFiltreFournisseur() != null) {
        filtreFrs = " AND A1COF = " + pUtil.getDernierFiltreFournisseur().substring(0, 1) + " AND A1FRS = "
            + pUtil.getDernierFiltreFournisseur().substring(1, 7);
      }
      
      String arguments = "(";
      for (int i = 0; i < liste.size(); i++) {
        if (i == 0) {
          arguments += "'";
        }
        else {
          arguments += ",'";
        }
        
        arguments += liste.get(i).getField("ART_CODE").toString().trim() + "'";
      }
      arguments += ")";
      
      liste = pUtil.getAccesDB2()
          .select(" SELECT A1ETB, A1ART, A1LIB, CAREF, A1UNV, DECIMQ, A1IN2, LIBCND , CND , A1TVA, A1COF, A1FRS, FRAWS ,FRNWS, FRNOM "
              + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT WHERE A1ETB = '"
              + pUtil.getEtablissementEnCours().getCodeETB() + "' AND A1ART IN " + arguments + filtreFrs
              + " ORDER BY A1ABC, A1SAI DESC, A1LIB FETCH FIRST " + pUtil.getEtablissementEnCours().getLimit_requete_article()
              + " ROWS ONLY OPTIMIZE FOR " + pUtil.getEtablissementEnCours().getLimit_requete_article() + " ROWS "
              + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    }
    
    if (liste != null) {
      for (GenericRecord record : liste) {
        traiterNomFournisseur(record);
      }
    }
    
    // attribuer la liste à l'utilisateur
    if (pUtil.getDernierFiltreFournisseur() == null) {
      pUtil.setDerniereListePanierIntuitif(liste);
    }
    
    // attribuer la liste à l'utilisateur
    pUtil.setDerniereListeArticle(liste);
    
    return liste;
  }
  
  /**
   * Récupérer les promotions liées à une sélection par groupe, par familles ou par sous famille
   */
  public static ArrayList<GenericRecord> recupererPromotionsParCatalogue(UtilisateurWebshop pUtil, int pNbPromosMax) {
    
    if (pUtil == null || pUtil.getListePromotionsActives() == null
        || (pUtil.getDerniereFamilleArticles() == null && pUtil.getDernierGroupeArticles() == null)) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = new ArrayList<GenericRecord>();
    String valeur = null;
    
    // On recherche par famille ou sous famille
    if (pUtil.getDerniereFamilleArticles() != null) {
      valeur = pUtil.getDerniereFamilleArticles();
      // famille
      if (valeur.startsWith("FA")) {
        valeur = valeur.replaceFirst("FA", "");
        int i = 0;
        while (liste.size() <= pNbPromosMax && i < pUtil.getListePromotionsActives().size()) {
          if (pUtil.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
              && pUtil.getListePromotionsActives().get(i).isPresentField("A1FAM")) {
            if (pUtil.getListePromotionsActives().get(i).getField("A1FAM").toString().trim().equals(valeur)) {
              liste.add(pUtil.getListePromotionsActives().get(i));
            }
          }
          i++;
        }
      }
      // sous famille
      else if (valeur.startsWith("SF")) {
        valeur = valeur.replaceFirst("SF", "");
        int i = 0;
        while (liste.size() <= pNbPromosMax && i < pUtil.getListePromotionsActives().size()) {
          if (pUtil.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
              && pUtil.getListePromotionsActives().get(i).isPresentField("A1SFA")) {
            if (pUtil.getListePromotionsActives().get(i).getField("A1SFA").toString().trim().equals(valeur)) {
              liste.add(pUtil.getListePromotionsActives().get(i));
            }
          }
          i++;
        }
      }
    }
    // On recherche par groupe
    else {
      valeur = pUtil.getDernierGroupeArticles();
      int i = 0;
      while (liste.size() < pNbPromosMax && i < pUtil.getListePromotionsActives().size()) {
        if (pUtil.getListePromotionsActives().get(i).isPresentField("PR_A1ART")
            && pUtil.getListePromotionsActives().get(i).isPresentField("A1FAM")) {
          if (pUtil.getListePromotionsActives().get(i).getField("A1FAM").toString().trim().startsWith(valeur)) {
            liste.add(pUtil.getListePromotionsActives().get(i));
          }
        }
        i++;
      }
    }
    
    if (liste.size() == 0) {
      return null;
    }
    else {
      return liste;
    }
  }
  
  /**
   * Traiter l'affichage du nom du fournisseur
   * En effet dans certains cas le fournisseur ne doit pas être affiché ou nommé d'une manière spécifique au Web Shop
   */
  public static void traiterNomFournisseur(GenericRecord pGenericRecord) {
    if (pGenericRecord == null) {
      return;
    }
    
    if (!pGenericRecord.isPresentField("FRNOM") || !pGenericRecord.isPresentField("FRAWS") || !pGenericRecord.isPresentField("FRNWS")) {
      return;
    }
    
    String typeAffichage = pGenericRecord.getField("FRAWS").toString().trim();
    
    if (typeAffichage.equals("0")) {
      return;
    }
    else if (typeAffichage.equals("1")) {
      pGenericRecord.setField("FRNOM", "");
    }
    else if (typeAffichage.equals("2")) {
      pGenericRecord.setField("FRNOM", pGenericRecord.getField("FRNWS").toString().trim());
    }
  }
  
}
