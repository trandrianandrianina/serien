/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe permettant de remplir les condiftions générales de vente des magasins
 * 
 * table CGV
 * @author Déborah
 * 
 */

public class GestionConditionsGenerales extends Gestion {
  
  /**
   * Récuperer les GCV du Web Shop
   */
  public ArrayList<GenericRecord> RecupererCgv(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT CGV_NAME FROM " + MarbreEnvironnement.BIBLI_WS + ".CGV WHERE CGV_LAN = '"
        + utilisateur.getLanguage() + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
}
