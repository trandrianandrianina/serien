/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.TableBDD;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Etablissement;

public class Gestion {
  public Gestion() {
  }
  
  /**
   * Cette méthode retourne l'ensemble des langues utilisées pour le WS par ce serveur
   */
  public ArrayList<GenericRecord> retournerLanguesWS(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT LA_ID, LA_LIB FROM " + MarbreEnvironnement.BIBLI_WS + "." + TableBDD.TABLE_WS_LANGUES + " ORDER BY LA_ID ");
  }
  
  /**
   * Echapper les caractères spéciaux
   */
  public String traiterCaracteresSpeciauxAffichage(String chaine) {
    if (chaine == null) {
      return null;
    }
    
    return StringEscapeUtils.escapeHtml4(chaine);
  }
  
  /**
   * Echapper les caractères spéciaux
   */
  public static String traiterCaracteresSpeciauxSQL(String chaine) {
    if (chaine == null) {
      return null;
    }
    
    return chaine.replace("'", "''");
  }
  
  /**
   * permet d'initier une liste visible via une pagination à partir d'une liste complète
   */
  public static void initDerniereListePagination(UtilisateurWebshop utilisateur, ArrayList<GenericRecord> liste) {
    if (utilisateur == null) {
      return;
    }
    // on init la liste complète à paginer
    utilisateur.setDerniereListePagination(liste);
    // si pas de problème on met à jour la liste partielle
    if (utilisateur.getDerniereListePagination() != null) {
      majListePartiellePagination(utilisateur, "0");
    }
    else {
      utilisateur.setDerniereListePartiellePagination(null);
    }
  }
  
  /**
   * Met à jour la liste vivible via une pagination à partir de la liste complète,
   * de l'indice de début de lecture et de la limite d'élements visibles dans une liste
   */
  public static void majListePartiellePagination(UtilisateurWebshop utilisateur, String indiceDeb) {
    if (utilisateur == null || indiceDeb == null) {
      return;
    }
    
    // Si la liste complète est null ou vide on vide la liste partielle à l'identique
    if (utilisateur.getDerniereListePagination() == null && utilisateur.getDerniereListePagination().size() == 0) {
      utilisateur.setDerniereListePartiellePagination(null);
      return;
    }
    
    // on type l'indice de de début récéptionné
    int indiceDebut = 0;
    try {
      indiceDebut = Integer.parseInt(indiceDeb);
    }
    catch (Exception e) {
      indiceDebut = 0;
    }
    
    // on initialise/reinit la liste partielle
    if (utilisateur.getDerniereListePartiellePagination() == null) {
      utilisateur.setDerniereListePartiellePagination(new ArrayList<GenericRecord>());
    }
    else {
      utilisateur.getDerniereListePartiellePagination().clear();
    }
    // On rajoute les élements à sélectionner dans la liste complète à l'intérieur de la liste partielle
    for (int i = indiceDebut; i < utilisateur.getDerniereListePagination().size()
        && i < indiceDebut + utilisateur.getEtablissementEnCours().getLimit_liste(); i++) {
      utilisateur.getDerniereListePartiellePagination().add(utilisateur.getDerniereListePagination().get(i));
    }
  }
  
  /**
   * initialiser ces listes temporaires afin de ne pas récupérer une précendente liste
   */
  public static void initListesTemporaires(UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      return;
    }
    
    utilisateur.setListeDeTravail(null);
    utilisateur.setRecordTravail(null);
  }
  
  /**
   * On teste si la saisie passée est alphanumérique
   */
  public static boolean isAlphanumerique(String variableAtester) {
    return variableAtester.matches("\\p{Alnum}");
  }
  
  /**
   * On attribue un établissement à l'utilisateur
   */
  public static void attribuerUnETB(UtilisateurWebshop utilisateur, IdEtablissement pIdEtablissement) {
    if (utilisateur == null || pIdEtablissement == null) {
      return;
    }
    
    utilisateur.setEtablissementEnCours(new Etablissement(utilisateur, pIdEtablissement));
  }
  
}
