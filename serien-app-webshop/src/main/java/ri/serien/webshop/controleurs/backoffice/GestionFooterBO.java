/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionFooterBO extends Gestion {
  
  public final static int VARIABLES_MANQUANTES = -2;
  public final static int ERREUR_MAJ_SQL = -1;
  public final static int AUCUNE_MAJ_SQL = 0;
  public final static int OK_UPDATE = 1;
  public final static int OK_INSERT = 2;
  
  /**
   * Retourner la liste des footers
   */
  public ArrayList<GenericRecord> retournerListeFooters(UtilisateurWebshop pUtil) {
    if (pUtil == null || pUtil.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return pUtil.getAccesDB2().select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".FOOTER ORDER BY FT_LANG, FT_LIB ");
  }
  
  /**
   * Retourner le détail d'un footer dans le détail
   */
  public GenericRecord retournerUnFooter(UtilisateurWebshop pUtil, String pCode) {
    if (pUtil == null || pUtil.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    if (pCode == null) {
      Trace.erreur("pCode à NUll");
      return null;
    }
    
    ArrayList<GenericRecord> liste =
        pUtil.getAccesDB2().select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".FOOTER WHERE FT_ID = '" + pCode + "' ");
    
    if (liste == null || liste.size() == 0) {
      return null;
    }
    
    return liste.get(0);
  }
  
  /**
   * On met à jour un footer dans DB2
   * Si le code Footer est présent c'est une mise a jour sinon un insert
   */
  public int majFooter(UtilisateurWebshop pUtil, String pCode, String pLib, String pLang, String pLien, String pTarg) {
    if (pUtil == null || pUtil.getAccesDB2() == null || pLib == null || pLang == null || pLien == null || pTarg == null) {
      Trace.erreur("Utilisateur à NUll");
      return VARIABLES_MANQUANTES;
    }
    
    // Mise à jour d'un footer
    if (pCode != null) {
      return pUtil.getAccesDB2().requete("" + " UPDATE " + MarbreEnvironnement.BIBLI_WS + ".FOOTER " + " SET FT_LANG  = '" + pLang
          + "', FT_LIB = '" + pLib + "', FT_LIEN = '" + pLien + "', FT_TARG = '" + pTarg + "' " + " WHERE FT_ID = '" + pCode + "' ");
      
    }
    else {
      int ret = pUtil.getAccesDB2()
          .requete("" + " INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG) VALUES " + " ('"
              + pLang + "'," + " '" + pLib + "'," + " '" + pLien + "'," + " '" + pTarg + "' " + " )");
      
      if (ret == 1) {
        return OK_INSERT;
      }
      else {
        return ret;
      }
    }
  }
  
  /**
   * On supprime un footer sur la base de son id
   */
  public boolean supprimerFooter(UtilisateurWebshop pUtil, String pId) {
    if (pUtil == null || pUtil.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return false;
    }
    
    if (pId == null) {
      Trace.erreur("Id Footer à NULL");
      return false;
    }
    
    return (pUtil.getAccesDB2()
        .requete("" + " DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".FOOTER WHERE FT_ID = '" + pId + "' ") == 1);
  }
  
}
