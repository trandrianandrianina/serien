/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionMagasinsBO extends Gestion {
  
  /**
   * Recuperer tous les magasins
   */
  public ArrayList<GenericRecord> recupererAllMag(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT MG_ID, MG_NAME,MG_ADR, MG_TEL,MG_MESS,MG_HOR,MG_CARTE,MG_EQUI FROM "
        + MarbreEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB = '" + utilisateur.getEtablissementEnCours() + "' ");
  }
  
  /**
   * recuperer les données par magasins (id)
   */
  public ArrayList<GenericRecord> recupererDetailsMagasin(UtilisateurWebshop utilisateur, String id, String langue) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || id == null) {
      Trace.erreur("Utilisateur ou ID à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select(
        "SELECT MG_ID, MG_COD,MG_NAME,MG_ADR, MG_TEL,MG_CARTE,MG_ACTIF, MG_MAIL_CD, MG_MAIL_DV, MG_MAIL_IN, LI_MESS,LI_HOR,LI_EQUI "
            + "FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS LEFT JOIN " + MarbreEnvironnement.BIBLI_WS + ".LIENSMAG "
            + "ON MG_ID = LI_MAG AND LI_LAN = '" + langue + "' WHERE MG_ID= " + id);
  }
  
  /**
   * Mise à jour des données dans xwebshop.magasins
   */
  public String miseajourInformationMagasin(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    int res = 0;
    String idMagasin = null;
    
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    // Si on est en mode modification
    if (request.getParameter("idMagasin") != null) {
      idMagasin = request.getParameter("idMagasin");
      // On met à jour les "constantes" du magasins
      res = utilisateur.getAccesDB2()
          .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS SET " + " MG_NAME  = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("nameBO")) + "' , MG_ADR = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("adr")) + "' , MG_TEL = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("tel")) + "' , MG_CARTE = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("carte")) + "' , " + " MG_ACTIF = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("etatMG")) + "'," + " MG_MAIL_CD = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailCDE")) + "', " + " MG_MAIL_DV = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailDEV")) + "', " + " MG_MAIL_IN = '"
              + traiterCaracteresSpeciauxSQL(request.getParameter("mailINS")) + "' " + " WHERE MG_ID=" + idMagasin);
    }
    // En insert de nouveau magasin
    else {
      res = utilisateur.getAccesDB2().requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS "
          + "(MG_NAME,MG_ADR,MG_TEL,MG_MESS,MG_HOR,MG_CARTE,US_ETB,MG_EQUI,MG_COD,MG_MAIL_CD,MG_MAIL_DV, MG_MAIL_IN) VALUES " + "('"
          + traiterCaracteresSpeciauxSQL(request.getParameter("nameBO")) + "'," + "'"
          + traiterCaracteresSpeciauxSQL(request.getParameter("adr")) + "'," + "'"
          + traiterCaracteresSpeciauxSQL(request.getParameter("tel")) + "'," + "''," + "''," + "'"
          + request.getParameter("carte").toString().replace("'", "''") + "'," + "'" + utilisateur.getEtablissementEnCours().getCodeETB()
          + "'," + "''," + "'" + traiterCaracteresSpeciauxSQL(request.getParameter("code")) + "'," + "'"
          + traiterCaracteresSpeciauxSQL(request.getParameter("mailCDE")) + "'," + "'"
          + traiterCaracteresSpeciauxSQL(request.getParameter("mailDEV")) + "'," + "'"
          + traiterCaracteresSpeciauxSQL(request.getParameter("mailINS")) + "')");
      
      // Récuperer l'ID pour INSERT DANS LA TABLE LINGUISTIQUE
      if (res > 0) {
        utilisateur.recupererUnRecordTravail(
            utilisateur.getAccesDB2().select("SELECT MAX(MG_ID) AS ID FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS "));
        if (utilisateur.getRecordTravail() != null && utilisateur.getRecordTravail().isPresentField("ID")) {
          idMagasin = utilisateur.getRecordTravail().getField("ID").toString();
        }
      }
    }
    
    // On va s'occuper des "variables" linguistiques
    if (res > -1 && idMagasin != null) {
      // On supprime d'éventuels enregistrements pour ce code langue
      if (request.getParameter("langueInfo") != null && request.getParameter("idMagasin") != null) {
        res = utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".LIENSMAG WHERE LI_LAN = '"
            + request.getParameter("langueInfo") + "' AND LI_MAG = '" + idMagasin + "'");
      }
      
      // On rajoute un nouvel enregsitrement pour ce code langue
      if (res >= 0) {
        // la suppression s'est bien passée on log si la variable debug est activée
        res = utilisateur.getAccesDB2()
            .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".LIENSMAG " + "(LI_MAG,LI_LAN,LI_MESS,LI_HOR,LI_EQUI) VALUES "
                + "('" + idMagasin + "','" + request.getParameter("langueInfo") + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("mess")) + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("hor")) + "','"
                + traiterCaracteresSpeciauxSQL(request.getParameter("equi")) + "')");
        if (res < 0) {
          return null;
        }
      }
    }
    
    return idMagasin;
  }
  
}
