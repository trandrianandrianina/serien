/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.TableBDD;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

public class GestionAccueil extends Gestion {
  
  /**
   * Retourne la liste des promotions actives [PROMOTION]
   */
  public static ArrayList<GenericRecord> recupererPromotions(UtilisateurWebshop utilisateur) {
    ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
        .select(" SELECT PR_ID, PR_A1ETB, PR_A1ART, PR_A1LIB, PR_TEXTE, PR_DATED, PR_DATEF, PR_PRIX, A1FAM, A1SFA " + " FROM "
            + MarbreEnvironnement.BIBLI_WS + "." + TableBDD.TABLE_WS_PROMOTION + " " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM ON PR_A1ETB = A1ETB AND PR_A1ART = A1ART " + " WHERE PR_DATED <= '" + Outils.recupererDateCouranteInt()
            + "' AND PR_DATEF >= '" + Outils.recupererDateCouranteInt() + "' " + " ORDER BY PR_DATEF FETCH FIRST 80 ROWS ONLY ");
    
    if (liste != null && liste.size() > 0) {
      return liste;
    }
    else {
      return null;
    }
  }
  
  /**
   * retourne le texte d'info de l'accueil en fonction de la langue [INFOACC]
   */
  public static ArrayList<GenericRecord> recupereInfoAcc(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT ACC_NAME FROM " + MarbreEnvironnement.BIBLI_WS + "." + TableBDD.TABLE_WS_INFOACC + " WHERE ACC_LAN = '"
            + utilisateur.getLanguage() + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
}
