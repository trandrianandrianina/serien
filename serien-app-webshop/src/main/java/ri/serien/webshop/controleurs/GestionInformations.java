/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe permettant de retourner les informations des magasins par filiales
 * table : information
 * @author Déborah
 * 
 */

public class GestionInformations extends Gestion {
  /**
   * retourner les informations par filiale
   */
  public ArrayList<GenericRecord> retournerInformations(UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("select INF_TXT,INF_DES,INF_IMG,INF_TXT2,INF_DES2,INF_IMG2, INF_LAN FROM " + MarbreEnvironnement.BIBLI_WS + ".INFOSW "
            + " WHERE INF_LAN = '" + utilisateur.getLanguage() + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
  }
  
}
