/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.controleurs.backoffice;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class GestionBddBO extends Gestion {
  
  /**
   * Retourne la liste des fichiers pour la bibliothéque WebShop.
   */
  public ArrayList<GenericRecord> retournerListeFichier(UtilisateurWebshop utilisateur) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      Trace.erreur("Utilisateur à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2()
        .select("SELECT TABLE_NAME FROM SYSIBM.TABLES WHERE TABLE_SCHEMA='" + MarbreEnvironnement.BIBLI_WS.toUpperCase() + "'");
  }
  
  /**
   * Retourne la description d'un fichier
   */
  public ArrayList<GenericRecord> retournerDetailFichier(UtilisateurWebshop utilisateur, String fichier) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || fichier == null) {
      Trace.erreur("Utilisateur ou fichier à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select("SELECT COLUMN_NAME, DATA_TYPE  FROM SYSIBM.COLUMNS WHERE TABLE_SCHEMA = '"
        + MarbreEnvironnement.BIBLI_WS.toUpperCase() + "' AND TABLE_NAME = '" + fichier + "'");
  }
  
  /**
   * Retourne le resultat de la requete
   */
  public ArrayList<GenericRecord> retournerRequete(UtilisateurWebshop utilisateur, String requeteBDD) {
    if (utilisateur == null || utilisateur.getAccesDB2() == null || requeteBDD == null) {
      Trace.erreur("Utilisateur ou requeteBDD à NUll");
      return null;
    }
    
    return utilisateur.getAccesDB2().select(" " + requeteBDD + " ");
  }
  
}
