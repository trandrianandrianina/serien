/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.outils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.SaveFile;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.webshop.constantes.MarbreEnvironnement;

public class Installation {
  
  // Constantes
  public static final int NOP = 0;
  public static final int INSTALL = 1;
  public static final int UPDATE = 2;
  
  private static final String NAME_PARAMETER_FILE = "ENVIRONM";
  private static final String NAME_FOLDER_SPECIFIC_STANDARD = "specifique";
  private static final String NAME_VERSION_WAR = "versionActuelle.txt";
  private static final String NAME_VERSION_INSTALLED = "version_installed.txt";
  private static final String NAME_LIBRARY_SAVE = "SERIEMFTP";
  private static final String NAME_LOG4J = "log4j.properties";
  
  // Variables
  private SystemeManager systemeManager = null;
  private boolean statelibrary = false;
  private boolean statefolder = false;
  private String dossierPriveWebshop = null;
  private File dossierDeSauvegarde = null;
  private int versionInstalled = 0;
  
  protected String messageErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur
   */
  public Installation() {
    dossierPriveWebshop = MarbreEnvironnement.DOSSIER_RACINE_PRIVATE + MarbreEnvironnement.DOSSIER_WS + File.separatorChar;
    dossierDeSauvegarde = new File(dossierPriveWebshop + "Sauvegarde");
  }
  
  /**
   * Contrôler les paramètres afin de déterminer si on doit effectuer une installation ou pas.
   * @return NOP = ne rien faire, UPDATE = mettre à jour le Webshop, INSTALL = installer le Webshop.
   */
  public int controllerInstallation() {
    // Contrôler les paramètres
    if (!controlerParametres()) {
      // Arrêt un peu violent si tout n'est pas ok dés le départ
      System.exit(-1);
    }
    
    // Contrôler les fichiers de version installée et de version WAR
    controlerVersionInstallee();
    
    statelibrary = controllerBibliotheque();
    statefolder = controllerDossierSpecific();
    
    // On controle ce que l'on doit faire
    // 0: on fait rien, 1: on installe, 2: maj
    int resultat = NOP;
    if (statelibrary && statefolder) {
      if (controllerVersion()) {
        resultat = UPDATE;
      }
    }
    else {
      resultat = INSTALL;
    }
    
    return resultat;
  }
  
  /**
   * Contrôle l'existence du dossier spécifique
   * @return
   */
  private boolean controllerDossierSpecific() {
    boolean resultat = false;
    Trace.info("-- Contrôle du dossier : " + MarbreEnvironnement.DOSSIER_SPECIFIQUE);
    File dossierSpecific = new File(MarbreEnvironnement.DOSSIER_SPECIFIQUE);
    if (dossierSpecific.exists()) {
      Trace.info("\t--> Présent            : [ OK ]");
      resultat = (dossierSpecific.list().length > 0);
      if (resultat) {
        Trace.info("\t--> Contenu            : [ OK ]");
      }
      else {
        Trace.info("\t--> Contenu            : [ NO ]");
      }
    }
    else {
      Trace.info("\t--> Présent            : [ NO ]");
    }
    
    return resultat;
  }
  
  /**
   * Contrôle l'existence du fichier de version, le lit s'il existe et le compare à celui du WAR
   * @return
   */
  private boolean controllerVersion() {
    boolean resultat = false;
    
    File fichierVersionInstallee = new File(dossierPriveWebshop + NAME_VERSION_INSTALLED);
    if (fichierVersionInstallee.exists()) {
      GestionFichierTexte gestionFichierTexte = new GestionFichierTexte(fichierVersionInstallee);
      String version = gestionFichierTexte.getContenuFichierString(false);
      versionInstalled = convertVersion(version);
      gestionFichierTexte.dispose();
      
      resultat = true;
    }
    Trace.info("\t--> Installée            : " + versionInstalled);
    Trace.info("\t--> Du WAR               : " + VersionWar.getVersionNumerique());
    // true si on fait une mise à jour, false sinon (installation ou rien)
    resultat = ((versionInstalled < VersionWar.getVersionNumerique()) && (versionInstalled > 0));
    if (resultat) {
      Trace.info("\t--> Une mise à jour va être effectuée");
    }
    
    return resultat;
  }
  
  /**
   * Contrôle l'existence de la bibliothèque et du fichier paramètre
   * @return
   */
  private boolean controllerBibliotheque() {
    boolean resultat = false;
    Trace.info("-- Contrôle de la bibliothèque : " + MarbreEnvironnement.BIBLI_WS);
    
    SystemeManager system = new SystemeManager(MarbreEnvironnement.ADRESSE_AS400, MarbreEnvironnement.PROFIL_AS_ANONYME,
        Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_ANONYME)), false);
    
    if (system.isConnecte()) {
      Trace.info("\t--> Connexion          : [ OK ]");
      
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(MarbreEnvironnement.BIBLI_WS);
      Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.NON_DEFINI);
      BibliothequeAS400 bibliothequeAS400 = new BibliothequeAS400(system.getSystem(), bibliotheque);
      if (bibliothequeAS400.isExists()) {
        Trace.info("\t--> Bib présente       : [ OK ]");
        IFSFile fichierParametreIFS =
            new IFSFile(system.getSystem(), QSYSObjectPathName.toPath(bibliothequeAS400.getName(), NAME_PARAMETER_FILE, "FILE"));
        try {
          resultat = fichierParametreIFS.exists();
          if (resultat) {
            Trace.info("\t--> Fichier " + NAME_PARAMETER_FILE + "   : [ OK ]");
          }
          else {
            Trace.info("\t--> Fichier " + NAME_PARAMETER_FILE + "   : [ NO ]");
          }
        }
        catch (IOException e) {
          Trace.erreur(e.getMessage());
        }
      }
      else {
        Trace.info("\t--> Bib présente       : [ NO ]");
      }
    }
    else {
      Trace.erreur("\t--> Connexion          : [ ER ]\n" + system.getMsgError());
    }
    system.deconnecterJDBC();
    
    return resultat;
  }
  
  /**
   * Contrôler les paramètres obligatoires issus du fichier "context.xml".
   * 
   * @return true = les paramètres sont corrects, false = erreur dans les paramètres.
   */
  private boolean controlerParametres() {
    boolean resultat = true;
    String etatAdresse = "[ OK ]";
    String etatProfile = "[ OK ]";
    String etatMotDePasse = "[ OK ]";
    String etatDossierTomcat = "[ OK ]";
    String etatDossierPublic = "[ OK ]";
    String etatDossierPrive = "[ OK ]";
    String etatDossierWebshop = "[ OK ]";
    
    if ((MarbreEnvironnement.ADRESSE_AS400 == null) || MarbreEnvironnement.ADRESSE_AS400.trim().equals("")) {
      etatAdresse = "[ ER ]";
      resultat = false;
    }
    if ((MarbreEnvironnement.PROFIL_AS_ANONYME == null) || MarbreEnvironnement.PROFIL_AS_ANONYME.trim().equals("")) {
      etatAdresse = "[ ER ]";
      resultat = false;
    }
    if ((MarbreEnvironnement.MP_AS_ANONYME == null) || MarbreEnvironnement.MP_AS_ANONYME.trim().equals("")) {
      etatAdresse = "[ ER ]";
      resultat = false;
    }
    if (!testerValiditeDossier(MarbreEnvironnement.DOSSIER_RACINE_TOMCAT)) {
      etatDossierTomcat = "[ ER ]";
      resultat = false;
    }
    if (!testerValiditeDossier(MarbreEnvironnement.DOSSIER_RACINE_PUBLIC)) {
      etatDossierPublic = "[ ER ]";
      resultat = false;
    }
    if (!testerValiditeDossier(MarbreEnvironnement.DOSSIER_RACINE_PRIVATE)) {
      etatDossierPrive = "[ ER ]";
      resultat = false;
    }
    if (!testerValiditeDossier(MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP)) {
      etatDossierWebshop = "[ ER ]";
      resultat = false;
    }
    
    Trace.info("Paramètres issus du fichier context.xml :");
    Trace.info("\tAdresse AS400      : " + etatAdresse);
    Trace.info("\tProfil             : " + etatProfile);
    Trace.info("\tMot de passe       : " + etatMotDePasse);
    Trace.info("\tDossier tomcat     : " + etatDossierTomcat);
    Trace.info("\tDossier public     : " + etatDossierPublic);
    Trace.info("\tDossier private    : " + etatDossierPrive + " - " + MarbreEnvironnement.DOSSIER_RACINE_PRIVATE);
    Trace.info("\tDossier webshop    : " + etatDossierWebshop);
    
    if (!resultat) {
      Trace.erreur("Il y a des erreurs de paramètrage, veuillez corriger votre fichier context.xml.");
    }
    return resultat;
  }
  
  /**
   * Teste la validité d'un dossier
   * @param pDossier
   * @return
   */
  private boolean testerValiditeDossier(String pDossier) {
    if ((pDossier == null) || pDossier.trim().equals("")) {
      return false;
    }
    else {
      File dossier = new File(pDossier);
      if (!dossier.exists()) {
        return false;
      }
    }
    return true;
  }
  
  /**
   * Copie le dossier spécifique du WAR vers sa destination (et créer l'arborescence si besoin)
   */
  private boolean copierFichierVersDossierSpecific() {
    boolean resultat = true;
    
    File dossierPublic = new File(MarbreEnvironnement.DOSSIER_TRAVAIL_PUBLIC);
    if (!dossierPublic.exists()) {
      dossierPublic.mkdirs();
    }
    File dossierSpecificACopier =
        new File(MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR + NAME_FOLDER_SPECIFIC_STANDARD);
    
    if (dossierSpecificACopier.exists()) {
      File dossierSpecificDeDestination = new File(FileNG.cleanNameFolder(MarbreEnvironnement.DOSSIER_SPECIFIQUE));
      try {
        FileNG.copyDirectory(dossierSpecificACopier, dossierSpecificDeDestination);
      }
      catch (IOException e) {
        resultat = false;
        messageErreur += "\nLa copie de " + dossierSpecificACopier.getAbsolutePath() + " vers " + dossierSpecificDeDestination
            + " n'a pas pu se faire.";
      }
    }
    
    return resultat;
  }
  
  /**
   * Créer la bibliothèque
   * @param system
   * @return
   */
  private boolean creerBibliotheque(SystemeManager system) {
    boolean resultat = true;
    
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(MarbreEnvironnement.BIBLI_WS);
    Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.NON_DEFINI);
    BibliothequeAS400 bibliothequeAS400 = new BibliothequeAS400(system.getSystem(), bibliotheque);
    resultat = bibliothequeAS400.create();
    if (!resultat) {
      messageErreur += "\n" + bibliothequeAS400.getMsgError();
    }
    
    return resultat;
  }
  
  /**
   * Execute les syntaxes SQL des fichiers .sql
   */
  public boolean executeSQLFiles() {
    File dossierDesVersions = new File(
        MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR + "WEB-INF" + File.separatorChar + "versions");
    
    // Listage des fichiers SQL dans le WAR
    File[] listeFichierSQL = dossierDesVersions.listFiles();
    
    if (listeFichierSQL == null || listeFichierSQL.length == 0) {
      messageErreur += "\nAucun fichier SQL trouvé dans le WAR";
      return false;
    }
    
    // Elimination des fichiers SQL non nécessaires (basé sur le numéro de version)
    for (int i = 0; i < listeFichierSQL.length; i++) {
      FileNG file = new FileNG(listeFichierSQL[i].getAbsolutePath());
      if (!file.getName().endsWith(".sql")
          || ((versionInstalled > 0) && (versionInstalled >= convertVersion(file.getNamewoExtension(false))))) {
        
        listeFichierSQL[i] = null;
      }
    }
    
    // Execution des fichiers restants
    QueryManager gestionDeRequete = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    boolean resultat = true;
    for (File file : listeFichierSQL) {
      if (file != null) {
        Trace.info("---Fichier SQL éxécutés--->" + file.getAbsolutePath());
        gestionDeRequete.executeSQLFile(file.getAbsolutePath(), MarbreEnvironnement.BIBLI_WS);
        Trace.info("--->" + resultat);
        if (!resultat) {
          Trace.erreur("Erreur: " + gestionDeRequete.getMsgError());
          
          return resultat;
        }
      }
    }
    
    return resultat;
  }
  
  /**
   * Convertit un numéro de version de String en int
   * @param version
   * @return
   */
  public int convertVersion(String version) {
    try {
      return Integer.parseInt(version.trim());
    }
    catch (Exception e) {
      return 0;
    }
  }
  
  /**
   * Connexion à l'AS400
   * @return
   */
  private SystemeManager connexion() {
    if (systemeManager == null) {
      systemeManager = new SystemeManager(MarbreEnvironnement.ADRESSE_AS400, MarbreEnvironnement.PROFIL_AS_ANONYME,
          Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_ANONYME)), true);
      if (!systemeManager.isConnecte()) {
        return null;
      }
    }
    return systemeManager;
  }
  
  /**
   * Procède à l'installation d'une partie ou toute l'application
   */
  public void installation() {
    boolean resultat = false;
    
    if (!statefolder) {
      if (copierFichierVersDossierSpecific()) {
        Trace.info("\t--> Installation dans l'IFS [ OK ]");
      }
      else {
        Trace.erreur("\t--> Installation dans l'IFS [ ER ]" + getMessageErreur());
      }
    }
    
    if (!statelibrary) {
      connexion();
      if (systemeManager != null) {
        resultat = creerBibliotheque(systemeManager);
        Trace.info("résultat creation biblio-------->" + resultat);
        if (resultat) {
          resultat = executeSQLFiles();
          Trace.info("résultat des executions sql----------->" + resultat);
          if (resultat) {
            Trace.info("\t--> Installation dans 5250 [ OK ]");
            if (!mettreAJourVersion()) {
              Trace.erreur("\t--> Mise à jour du numéro de version [ ER ]");
              Trace.erreur("\t\t- " + getMessageErreur());
            }
            else {
              Trace.info("\t--> Mise à jour du numéro de version [ OK ]");
            }
          }
          Trace.info("pas de mise à jour de numéro de version" + resultat);
        }
        systemeManager.deconnecterJDBC();
      }
      if (!resultat) {
        Trace.erreur("\t--> Installation dans 5250 [ ER ]" + getMessageErreur());
      }
    }
  }
  
  /**
   * Mettre à jour le WebShop
   */
  public void mettreAJour() {
    // Se connecter à l'AS400
    connexion();
    
    // Sauvegarde du dossier xweb dans un SAVF
    Trace.info("\t--> Sauvegarde de la bibliothèque");
    if (!sauverBibliotheque(systemeManager)) {
      Trace.erreur("\t\t" + getMessageErreur());
      return;
    }
    // Copie du SAVF dans le dossier sauvegarde dans l'IFS aaaammjj_hhmm_name
    Trace.info("\t--> Copie du fichier SAVF dans le dossier Sauvegarde");
    if (!copierFichierSavfVersPrivate(NAME_LIBRARY_SAVE, MarbreEnvironnement.BIBLI_WS)) {
      Trace.erreur("\t\t" + getMessageErreur());
      return;
    }
    // A partir de la version du war rechercher tous les fichiers sql à executer dans l'ordre
    Trace.info("\t--> Execution des mises à jour SQL si nécessaire");
    if (!executeSQLFiles()) {
      Trace.erreur("\t\t" + getMessageErreur());
      return;
    }
    
    // Mettre à jour le fichier version dans le dossier private
    Trace.info("\t--> Mise à jour du fichier de version");
    if (!mettreAJourVersion()) {
      Trace.erreur("\t\t" + getMessageErreur());
      return;
    }
    // Mettre à jour le repertoire specifique/images/decoration
    Trace.info("\t--> Mise à jour du repertoire decoration");
    if (!copierDossierImagesDecoration()) {
      Trace.erreur("\t\t- " + getMessageErreur());
      return;
    }
    // Mettre à jour le repertoire specifique/images
    Trace.info("\t--> Mise à jour du repertoire images");
    if (!copierNouvellesImagesSpecifiques()) {
      Trace.erreur("\t\t- " + getMessageErreur());
      return;
    }
    systemeManager.deconnecterJDBC();
  }
  
  /**
   * Sauvegarde de la bibliothèque
   * @param system
   * @return
   */
  private boolean sauverBibliotheque(SystemeManager system) {
    boolean resultat = false;
    
    SaveFile fichierSauve = new SaveFile(system.getSystem(), "SERIEMFTP", MarbreEnvironnement.BIBLI_WS);
    try {
      fichierSauve.delete();
      fichierSauve.create();
      fichierSauve.save(MarbreEnvironnement.BIBLI_WS);
      resultat = true;
    }
    catch (Exception e) {
      messageErreur += "\nErreur lors de la sauvegarde de la biblilothèque " + MarbreEnvironnement.BIBLI_WS + "\n" + e;
    }
    
    return resultat;
  }
  
  /**
   * Copie le fichier savf vers le dossier sauvegarde dans l'IFS
   * @param lib
   * @param file
   * @return
   */
  private boolean copierFichierSavfVersPrivate(String lib, String file) {
    if ((lib == null) || (file == null)) {
      return false;
    }
    boolean resultat = false;
    
    try {
      // On vérifie l'existence de la bibliothèque
      String cheminDuFichier = QSYSObjectPathName.toPath(lib, file, "FILE");
      IFSFile fichierIFS = new IFSFile(systemeManager.getSystem(), cheminDuFichier);
      if (!fichierIFS.exists()) {
        messageErreur += "\nErreur le fichier " + cheminDuFichier + " n'a pas été trouvé";
        return false;
      }
      
      // On vérifie l'existence du dossier private
      if (!dossierDeSauvegarde.exists()) {
        dossierDeSauvegarde.mkdirs();
      }
      
      // Construction du nom du fichier stocké
      Date dateCourante = new Date();
      String nomDuFichier = dossierDeSauvegarde.getAbsolutePath() + File.separatorChar
          + String.format("%1$tY%1$tm%1$td_%1$tH%1$tM_%2$s_v%3$d.savf", dateCourante, file, versionInstalled);
      
      // On copie le SAVF
      StringBuilder messageFinal = new StringBuilder();
      CommandCall cmd = new CommandCall(systemeManager.getSystem(), "QSYS/CPYTOSTMF FROMMBR('" + cheminDuFichier + "') TOSTMF('"
          + nomDuFichier + "') STMFOPT(*REPLACE) CVTDTA(*NONE) DBFCCSID(*FILE) ENDLINFMT(*FIXED)");
      resultat = cmd.run();
      if (!resultat) {
        AS400Message[] messages = cmd.getMessageList();
        if (messages != null) {
          Trace.info("-copySavfToPrivate->msg:" + messages.length);
          for (int i = 0; i < messages.length; i++) {
            messageFinal.append(messages[i].getText()).append('\n');
          }
          messageErreur += "\nErreur lors de la copie du fichier SAVF dans l'IFS:\n" + messageFinal.toString();
        }
      }
      else {
        fichierIFS.delete();
      }
    }
    catch (Exception e) {
      messageErreur += "\nErreur lors de la copie du fichier SAVF dans l'IFS:\n" + e;
      resultat = false;
    }
    
    return resultat;
  }
  
  /**
   * Mettre à jour la version installée à partir de celle du fichier WAR.
   * @return true=mise à jour réussie, false=erreur.
   */
  private boolean mettreAJourVersion() {
    // Localiser le fichier version en local
    File versionDuFichier = new File(dossierPriveWebshop + NAME_VERSION_INSTALLED);
    
    // Sauvegarder le fichier version actuelle (s'il est présent)
    File fileSauvegardeVersion = new File(dossierPriveWebshop + NAME_VERSION_INSTALLED.replaceFirst(".txt", ".old"));
    versionDuFichier.renameTo(fileSauvegardeVersion);
    
    // Localiser le fichier version du WAR
    FileNG versionDuWar = new FileNG(MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR + "WEB-INF"
        + File.separatorChar + "versions" + File.separatorChar + NAME_VERSION_WAR);
    
    // Tracer
    Trace.info("Mettre à jour la version installée à partir de celle du fichier WAR.");
    
    // Copier le fichier version du WAR en local
    boolean resultat = versionDuWar.copyTo(versionDuFichier.getAbsolutePath());
    if (resultat) {
      // Effacer la sauvegarde
      fileSauvegardeVersion.delete();
    }
    else {
      Trace.erreur("Erreur lors de la mise à jour de la version installée : " + versionDuFichier.getAbsolutePath());
    }
    
    return resultat;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMessageErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = messageErreur;
    messageErreur = "";
    
    return chaine;
  }
  
  /**
   * Copie du repertoire images/decoration dans le dossier public/WebShop/specifique
   * @throws IOException
   */
  public boolean copierDossierImagesDecoration() {
    boolean resultat = true;
    
    // repertoire decoration dans webcontent
    File folderDecoration = new File(MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR
        + NAME_FOLDER_SPECIFIC_STANDARD + File.separatorChar + "images" + File.separatorChar + "decoration" + File.separatorChar);
    // repertoire decoration dans specifique
    File folderdestinationdecoration =
        new File(MarbreEnvironnement.DOSSIER_SPECIFIQUE + "images" + File.separatorChar + "decoration" + File.separatorChar);
    
    if (!folderdestinationdecoration.exists()) {
      folderdestinationdecoration.mkdir();
    }
    
    try {
      FileNG.copyDirectory(folderDecoration, folderdestinationdecoration);
      resultat = true;
      
    }
    catch (IOException e) {
      messageErreur += "\nErreur lors de la copie du repertoire decoration dans l'IFS:\n" + e;
      resultat = false;
    }
    
    return resultat;
  }
  
  /**
   * Copie du fichier log4j.porperties dans .classes
   */
  public boolean copierFichierProprietesLog4j() {
    boolean resultat = true;
    
    FileNG dossierSource = new FileNG(
        MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR + "WEB-INF" + File.separator + NAME_LOG4J);
    File dossierDestination = new File(MarbreEnvironnement.DOSSIER_RACINE_TOMCAT + "webapps" + File.separatorChar
        + MarbreEnvironnement.DOSSIER_WS + File.separatorChar + "WEB-INF" + File.separatorChar + "classes");
    
    resultat = dossierSource.copyTo(dossierDestination.getAbsolutePath() + File.separatorChar + NAME_LOG4J);
    Trace.info("copie du fichier Log4j.properties : dans le dossier classes : " + resultat);
    if (!resultat) {
      messageErreur += "\nErreur lors de la copie du fichier de Log4j.properties : " + dossierDestination.getAbsolutePath();
    }
    
    return resultat;
  }
  
  /**
   * Copie les nouvelles images spécifiques
   */
  public boolean copierNouvellesImagesSpecifiques() {
    boolean resultat = true;
    String currentPictureTo = null;
    FileNG folderSpecifiqueImageTo = new FileNG(
        MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + NAME_FOLDER_SPECIFIC_STANDARD + File.separatorChar + "images" + File.separatorChar);
    File folderSpecifiqueImageFrom =
        new File(MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + MarbreEnvironnement.SEPARATEUR + "images" + File.separatorChar);
    FileNG fichierAcopier = null;
    
    // liste les images dans le respertoire image xwebshop
    File[] listeImagesTo = folderSpecifiqueImageTo.listFiles();
    
    // liste les images dans le repertoire image sur le serveeur
    File[] listeImagesFrom = folderSpecifiqueImageFrom.listFiles();
    
    // recupere les images To dans une arraylist
    if (listeImagesFrom == null || listeImagesTo == null) {
      return false;
    }
    
    for (int i = 0; i < listeImagesTo.length; i++) {
      currentPictureTo = listeImagesTo[i].getName();
      for (int j = 0; j < listeImagesFrom.length; j++) {
        if (!currentPictureTo.equals(listeImagesFrom[j].getName())) {
          fichierAcopier = new FileNG(folderSpecifiqueImageTo + File.separator + currentPictureTo);
          
          resultat = fichierAcopier.copyTo(folderSpecifiqueImageFrom.getAbsolutePath() + File.separator + currentPictureTo);
        }
      }
    }
    Trace.info("copie d'une nouvelle image effectuée: " + resultat);
    if (!resultat) {
      messageErreur += "\nErreur lors de la copie de l'image : " + folderSpecifiqueImageFrom.getAbsolutePath();
    }
    
    return resultat;
  }
  
  /**
   * Comparer la version déjà installée et la version présente dans le fichier WAR.
   */
  private void controlerVersionInstallee() {
    // Tester si le fichier avec la version installée est présent
    File fileVersionInstallee = new File(dossierPriveWebshop + NAME_VERSION_INSTALLED);
    if (fileVersionInstallee.exists()) {
      // Tracer
      Trace.info("Le fichier avec la version installée existe : " + fileVersionInstallee.getAbsolutePath());
      
      // Récuperer le numéro de version installée
      GestionFichierTexte gestionFichierTexte = new GestionFichierTexte(fileVersionInstallee);
      String version = gestionFichierTexte.getContenuFichierString(false);
      versionInstalled = convertVersion(version);
      gestionFichierTexte.dispose();
      
      // Récuperer le numéro de version du WAR
      int versionWar = VersionWar.getVersionNumerique();
      
      // Tracer les versions
      Trace.info("Version installée      : " + versionInstalled);
      Trace.info("Version du fichier WAR : " + versionWar);
      
      // Comparer avec la version du fichier WAR
      if (VersionWar.getVersionNumerique() != versionInstalled) {
        Trace.erreur("La version du WAR est différente de la version installée.");
      }
    }
    else {
      // Tracer
      Trace.erreur("Le fichier avec la version installée n'existe pas : " + fileVersionInstallee.getAbsolutePath());
      if (MarbreEnvironnement.isEclipse) {
        Trace.info(
            "Sur Eclipse, le dossier racine de Tomcat est situé : Workspace\\.metadata\\.plugins\\org.eclipse.wst.server.core\\tmp0");
      }
      
      // Déterminer l'emplacement du nouveau dossier du fichier version
      File fileNouveauDossierVersion = new File(dossierPriveWebshop);
      
      // Regarder si le fichier version est situé dans l'ancien dossier
      String nomAncienDossierVersion = MarbreEnvironnement.DOSSIER_RACINE_PRIVATE + "WebShop" + File.separatorChar;
      File fileAncierDossierVersion = new File(nomAncienDossierVersion);
      File fileAncienFichierVersion = new File(nomAncienDossierVersion + NAME_VERSION_INSTALLED);
      if (fileAncienFichierVersion.exists() && fileAncierDossierVersion.isDirectory()) {
        // Indiquer qu'on a trouvé le fichier version dans l'ancien dossier
        Trace.erreur(
            "Le fichier avec la version installée est présent dans l'ancien dossier : " + fileAncierDossierVersion.getAbsolutePath());
        
        // Renommer l'ancien dossier version
        if (fileAncierDossierVersion.renameTo(fileNouveauDossierVersion)) {
          // Code dangereux, ...réentrance
          controlerVersionInstallee();
        }
        else {
          Trace.erreur("Erreur pour renommer l'ancien dossier version : " + fileAncierDossierVersion.getAbsolutePath());
        }
      }
      else {
        // Créer le nouveau dossier version
        fileNouveauDossierVersion.mkdirs();
        
        // Créer le nouveau fichier version
        mettreAJourVersion();
      }
    }
  }
  
  /**
   * Cette méthode statique retourne la version INSTALLEE du webshop
   */
  public static String recupererVersionInstallee() {
    String version = "0";
    File fversioninstalled = new File(
        MarbreEnvironnement.DOSSIER_RACINE_PRIVATE + MarbreEnvironnement.DOSSIER_WS + File.separatorChar + NAME_VERSION_INSTALLED);
    if (fversioninstalled.isFile()) {
      GestionFichierTexte gft = new GestionFichierTexte(
          MarbreEnvironnement.DOSSIER_RACINE_PRIVATE + MarbreEnvironnement.DOSSIER_WS + File.separatorChar + NAME_VERSION_INSTALLED);
      version = gft.getContenuFichierString(false);
      if (version != null) {
        version = version.trim();
      }
    }
    
    return version;
  }
  
}
