/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.outils;

import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.UtilisateurWebshop;

public class Traduction {
  private HashMap<String, String> listeEquivalence = null;
  private UtilisateurWebshop utilisateur = null;
  
  /**
   * Constructeur Traduction
   */
  public Traduction(UtilisateurWebshop util) {
    utilisateur = util;
    listeEquivalence = new HashMap<String, String>();
  }
  
  /**
   * Charger une première fois cette liste puis l'attribuer à l'objet
   */
  public void chargerListeEquivalence(String code) {
    if (utilisateur != null && utilisateur.getAccesDB2() != null && listeEquivalence != null) {
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
          .select("SELECT TR_CLE,TR_VALEUR FROM " + MarbreEnvironnement.BIBLI_WS + ".TRADUCTION WHERE TR_CODE = '" + code + "'");
      if (liste != null && liste.size() > 0) {
        for (int i = 0; i < liste.size(); i++) {
          if (liste.get(i).isPresentField("TR_CLE") && liste.get(i).isPresentField("TR_VALEUR")) {
            listeEquivalence.put(liste.get(i).getField("TR_CLE").toString(), liste.get(i).getField("TR_VALEUR").toString());
          }
        }
      }
    }
  }
  
  /**
   * Chercher une équivalence à un mot clé
   */
  public String traduire(String cle) {
    if (cle == null || listeEquivalence == null) {
      return cle;
    }
    
    if (listeEquivalence.containsKey(cle)) {
      return listeEquivalence.get(cle);
    }
    else {
      return cle;
    }
  }
  
  public UtilisateurWebshop getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(UtilisateurWebshop utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public HashMap<String, String> getListeEquivalence() {
    return listeEquivalence;
  }
  
  public void setListeEquivalence(HashMap<String, String> listeEquivalence) {
    this.listeEquivalence = listeEquivalence;
  }
  
}
