/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.outils;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.webshop.constantes.MarbreEnvironnement;

/**
 * Version du fichier Webshop.war.
 * 
 * La version est récupérée dans le fichier "WebContent\WEB-INF\versions\versionActuelle.txt" du fichier WAR.
 * Elle est mise à disposition au format numérique et au format textuel.
 **/
public class VersionWar {
  private static String versionTextuelle = null;
  private static int versionNumerique = 0;
  
  /**
   * Version du logiciel formatée sous forme de chaine de caractères.
   *
   * @return vide si le numéro de version n'a pas été trouvée, sinon valeur de la version présentée au format texte.
   */
  public static String getVersionTextuelle() {
    if (versionTextuelle == null) {
      lireVersion();
    }
    
    return versionTextuelle;
  }
  
  /**
   * Version du logiciel sous forme numérique.
   * 
   * @return 0 = si le numéro de version n'a pas été trouvée, sinon valeur de la version au format numérique.
   */
  public static int getVersionNumerique() {
    if (versionNumerique == 0) {
      lireVersion();
    }
    return versionNumerique;
  }
  
  /**
   * Lire le fichier version situé dans le fichier WAR.
   */
  private static void lireVersion() {
    // Mettre les valeurs par défaut en cas d'erreur
    versionTextuelle = "";
    versionNumerique = 0;
    
    // Tester si le fichier avec la version du WAR existe
    GestionFichierTexte gestionFichierTexte =
        new GestionFichierTexte(MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP + MarbreEnvironnement.SEPARATEUR + "WEB-INF"
            + MarbreEnvironnement.SEPARATEUR + "versions" + MarbreEnvironnement.SEPARATEUR + "versionActuelle.txt");
    if (gestionFichierTexte.isPresent()) {
      // Tracer
      Trace.info("Le fichier avec la version du WAR existe : " + gestionFichierTexte.getNomFichier());
      
      // Lire la version dans le fichier
      String version = gestionFichierTexte.getContenuFichierString(false);
      if (version != null) {
        version = version.trim();
        
        // Formater la version au format textuel
        try {
          if (version.length() == 3) {
            versionTextuelle = version.substring(0, 1) + "." + version.substring(1, 3);
          }
          else if (version.length() == 4) {
            versionTextuelle = version.substring(0, 2) + "." + version.substring(2, 4);
          }
          else if (version.length() == 5) {
            versionTextuelle = version.substring(0, 1) + "." + version.substring(1, 3) + "/" + version.substring(3, 5);
          }
          else if (version.length() > 8) {
            versionTextuelle =
                version.substring(0, 4) + "." + version.substring(4, 6) + "/" + version.substring(6, 8) + " " + version.substring(8);
          }
          else if (version.length() == 8) {
            versionTextuelle = version.substring(0, 4) + "." + version.substring(4, 6) + "/" + version.substring(6, 8);
          }
        }
        catch (NumberFormatException e) {
          Trace.erreur(e, "Erreur lors de la conversion de la version au format textuel");
        }
        
        // Convertir la version en version numérique
        try {
          versionNumerique = Integer.parseInt(version);
        }
        catch (NumberFormatException e) {
          Trace.erreur(e, "Erreur lors de la conversion de la version au format numérique");
        }
      }
      
    }
    else {
      Trace.erreur("Le fichier avec la version du WAR n'existe pas : " + gestionFichierTexte.getNomFichier());
    }
  }
}
