/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.outils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;

/**
 * Classe permettant le mappage des dossiers contenant des doc (images, ...) devant être accessibles par une URL
 */
public class FilterFile implements Filter {
  private static final String DOSSIER_VIRTUEL_SPECIFIQUE = MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE.substring(1);
  
  /**
   * Appelée lors de la construction du filtre par le serveur d'applications.
   */
  @Override
  public void init(FilterConfig config) throws ServletException {
  }
  
  /**
   * Appelée lors de la destruction du filtre par le serveur d'applications.
   */
  @Override
  public void destroy() {
  }
  
  /**
   * Appelée sur les requêtes HTTP avant leur traitement classique par doGet ou doPost par le serveur d'applications.
   * 
   * C'est un pré-processeur de la requête HTTP, c'est à dire que cette méthode s'exécute avant qu'on arrive dans le doGet où le doPost
   * de la servlet. Quant à l'ordre d'exécution des filtres, c'est celui du fichier web.xml.
   */
  @Override
  public void doFilter(ServletRequest pRequest, ServletResponse pResponse, FilterChain pFilterChain)
      throws IOException, ServletException {
    final String methode = getClass().getSimpleName() + ".doFilter";
    
    try {
      HttpServletRequest httpRequest = (HttpServletRequest) pRequest;
      HttpServletResponse httpResponse = (HttpServletResponse) pResponse;
      
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(httpRequest));
      
      // Convertir le chemin virtuel en chemin réel
      String chemin = httpRequest.getRequestURI();
      chemin = convertirCheminVirtuel(chemin);
      
      // Tester l'existence du fichier et sortir de suite s'il n'existe pas
      File file = new File(chemin);
      if (!file.exists()) {
        Trace.alerte("Le fichier correspondant à l'URI suivante n'existe pas : " + chemin);
      }
      else {
        // Modifier la date de dernière modification de l'entête de la réponse afin de gérer les problèmes de cache
        httpResponse.setDateHeader("Last-Modified", file.lastModified());
        
        // Set content type
        ServletContext sc = httpRequest.getSession().getServletContext();
        String mime = sc.getMimeType(chemin);
        httpResponse.setContentType(mime);
        
        // Set content size
        httpResponse.setContentLength((int) file.length());
        
        // Open the file and output streams
        FileInputStream in = new FileInputStream(file);
        ServletOutputStream out = httpResponse.getOutputStream();
        
        // Copy the contents of the file to the output stream
        byte[] buf = new byte[2048];
        int count = 0;
        while ((count = in.read(buf)) >= 0) {
          out.write(buf, 0, count);
        }
        in.close();
        out.close();
      }
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Convertir un chemin virtuel en chemin réel.
   * 
   * Un chemin virtuel est identifié par la présence du mot "virtual" dans le chemin. Si le chemin ne contient pas le mot "virtual", il
   * est retourné sans conversion.
   * 
   * @param cheminVirtuel Chemin virtuel à convertir.
   * @return Chemin réel.
   */
  private String convertirCheminVirtuel(String cheminVirtuel) {
    String cheminReel;
    
    // Remplacer la racine du chemin virtuel des spécifiques afin de mettre le chemin réel
    int positionSpecifique = cheminVirtuel.indexOf(DOSSIER_VIRTUEL_SPECIFIQUE);
    if (positionSpecifique >= 0) {
      cheminReel =
          MarbreEnvironnement.DOSSIER_SPECIFIQUE + cheminVirtuel.substring(positionSpecifique + DOSSIER_VIRTUEL_SPECIFIQUE.length());
    }
    else {
      cheminReel = cheminVirtuel;
    }
    
    // Utiliser le séparateur standard du système d'exploitation
    cheminReel = cheminReel.replace('/', MarbreEnvironnement.SEPARATEUR_CHAR);
    
    // Tracer le remplacement de l'URI
    Trace.info("Convertir l'URI virtuel en chemin réel : " + cheminVirtuel + " => " + cheminReel);
    
    // Retourner le chemin réel
    return cheminReel;
  }
}
