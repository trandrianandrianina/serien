/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.outils.mail;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ri.serien.libas400.dao.sql.exploitation.mail.SqlMail;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.mail.DestinataireMail;
import ri.serien.libcommun.exploitation.mail.EnumNomVariableMail;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeDestinataireMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Classe de service utilisée pour la persistance des paramètres du mail pour le Webshop.
 */
public class ServiceMail {
  
  /**
   * Crée un mail dans la base de données.
   * 
   * @param pQueryManager Objet gestionnaire de requête
   * @return
   */
  public static IdMail creerMail(UtilisateurWebshop pUtilisateur, QueryManager pQueryManager, String pDestinataire,
      EnumTypeMail pEnumTypeMail) {
    if (pUtilisateur == null || pUtilisateur.getEtablissementEnCours() == null) {
      throw new MessageErreurException("L'établissement en cours n'est pas défini.");
    }
    
    if (pQueryManager == null) {
      throw new MessageErreurException("Le gestionnaire de requête n'est pas valide.");
    }
    
    IdEtablissement idEtablissement = pUtilisateur.getEtablissementEnCours().getIdEtablissement();
    Mail mail = getInstanceMail(idEtablissement, pEnumTypeMail);
    mail.setListeDestinataire(ListeDestinataireMail.preparerListeDestinataire(idEtablissement, Arrays.asList(pDestinataire)));
    
    QueryManager queryManager = pQueryManager;
    SqlMail operation = new SqlMail(queryManager);
    return operation.creerMail(mail);
  }
  
  /**
   * Charge le mail concerné.
   * 
   * @param pQueryManager Objet gestionnaire de requête
   * @param pIdMail Objet Id du mail concerné
   * @return
   */
  public static Mail chargerMail(QueryManager pQueryManager, IdMail pIdMail) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le gestionnaire de requête n'est pas valide.");
    }
    
    SqlMail operation = new SqlMail(pQueryManager);
    List<IdMail> listeIdMail = new ArrayList<IdMail>();
    listeIdMail.add(pIdMail);
    List<Mail> listeMail = operation.chargerListeMail(listeIdMail);
    if (listeMail.isEmpty()) {
      return null;
    }
    return listeMail.get(0);
  }
  
  /**
   * Persister la liste des variables Liée à mail.
   * 
   * @param pQueryManager Objet gestionnaire de requête
   * @param pListeVariableMail Liste des variables du mail
   */
  private static void creerListeVariableMail(QueryManager pQueryManager, ListeVariableMail pListeVariableMail) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le gestionnaire de requête n'est pas valide.");
    }
    
    SqlMail operation = new SqlMail(pQueryManager);
    operation.creerListeVariableMail(pListeVariableMail);
  }
  
  /**
   * Initialiser les variables d'un mail.
   * 
   * @param pQueryManager Objet gestionnaire de requête
   * @param pMail Objet mail concerné
   * @return
   */
  public static ListeVariableMail initialiserVariableMail(QueryManager pQueryManager, Mail pMail) {
    ListeVariableMail listeVariableMail = null;
    // Utiliser les variables définits avant l'appel de la méthode s'il en existe
    if (pMail.getListeVariableMail() != null) {
      listeVariableMail = pMail.getListeVariableMail();
    }
    else {
      listeVariableMail = new ListeVariableMail();
    }
    
    // Définition des variables par défaut
    // La date de création du mail
    listeVariableMail.ajouterNouvelleVariable(pMail.getId(), EnumNomVariableMail.DATE_CREATION.getNom(),
        DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, pMail.getDateCreation()));
    
    // Pour les types fax, la variable destinataire est initialisée avec le numéro de fax du destinataire du fax/mail
    if (pMail.getIdTypeMail().getEnumTypeMail() == EnumTypeMail.MAIL_ENVOI_FAX) {
      ListeDestinataireMail listeDestinataire = pMail.getListeDestinataire();
      if (!listeDestinataire.isEmpty()) {
        // Il faudra revoir cette initialisation lors de la suppression des champs destinataire de la table mail
        DestinataireMail destinataire = listeDestinataire.get(0);
        listeVariableMail.ajouterNouvelleVariable(pMail.getId(), EnumNomVariableMail.DESTINATAIRE.getNom(), destinataire.getEmail());
      }
    }
    
    // Sauvegarde des variables dans la base de données
    if (pQueryManager != null) {
      creerListeVariableMail(pQueryManager, listeVariableMail);
    }
    
    return listeVariableMail;
  }
  
  /**
   * Crée une instance de Mail par type de mail
   * 
   * @param pIdEtablissement Etablissement en cours
   * @param pEnumTypeMail Type de mail à créer
   */
  private static Mail getInstanceMail(IdEtablissement pIdEtablissement, EnumTypeMail pEnumTypeMail) {
    // Création d'instance de mail à partir de l'établissement
    Mail mail = new Mail(IdMail.getInstanceAvecCreationId(pIdEtablissement));
    // Création d'instance de type de mail
    IdTypeMail idTypeMail = IdTypeMail.getInstance(pIdEtablissement, pEnumTypeMail);
    // Précision du type de mail
    mail.setIdTypeMail(idTypeMail);
    // Les mails qui ne possède pas de pièce jointe sont statués "A envoyer" à leur création
    mail.setStatut(EnumStatutMail.A_ENVOYER);
    
    return mail;
  }
  
}
