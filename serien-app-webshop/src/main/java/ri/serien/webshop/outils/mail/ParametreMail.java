/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.outils.mail;

import java.util.HashMap;
import java.util.Map;

import ri.serien.libcommun.exploitation.mail.EnumNomVariableMail;

/**
 * Classe de préparation des paramètres dont un mail a besoin.
 */
public class ParametreMail {
  private Map<EnumNomVariableMail, String> mapParametre = null;
  
  /**
   * Constructeur
   */
  public ParametreMail() {
    mapParametre = new HashMap<EnumNomVariableMail, String>();
  }
  
  /**
   * Ajoute un paramètre du mail.
   * 
   * @param pEnumNomVariableMail Nom de la variable pour identifier le paramètre
   * @param pValeur Valeur du paramètre
   */
  public void ajouterParametre(EnumNomVariableMail pEnumNomVariableMail, String pValeur) {
    mapParametre.put(pEnumNomVariableMail, pValeur);
  }
  
  public Map<EnumNomVariableMail, String> retournerParametre() {
    return mapParametre;
  }
}
