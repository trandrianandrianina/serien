/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import ri.serien.webshop.constantes.MarbreEnvironnement;

/**
 * Adresse IP suspecte (adresse IP surveillée).
 * 
 * Le serveur Webshop maintient une liste des adresses IP qui s'y connectent et comptabilise leur nombre de requêtes et le délai entre
 * les connexions. Pour chaque adresse IP, on stocke également la liste des requêtes exécutées.
 * 
 * Pour qu'une adresse suspecte devienne "moisie", il faut 50 requêtes d'affilées avec un délai inférieur à 1 minute entre chaque requête.
 * De plus, si le nombre de requêtes maximum est effectué en moins d'une minute, on considère que c'est un robot et l'adresse IP est
 * ajoutée à la liste des adresses IP bloquées.
 * 
 * Note : c'est plus une adresse IP surveillée qu'une adresse IP suspecte, sinon c'est une manière de dire que chaque adresse IP qui
 * contacte le Webshop est d'office suspecte.
 */
public class AdresseSuspecte {
  private int nbRequete = 0;
  private long heurePremiereRequete = 0;
  private long heureDerniereRequete = 0;
  private String adresseIP = null;
  private boolean isMoisie = false;
  private ArrayList<HttpServletRequest> listeHttpServletRequest = null;
  
  /**
   * Constructeur.
   * 
   * @param pNbTentative Nombre de tentatives de requêtes déjà effectuées.
   * @param pHeurePremiereRequete Date et heure de la première requête.
   * @param pAdresseIP Adresse IP surveillée.
   */
  public AdresseSuspecte(int pNbTentative, long pHeurePremiereRequete, String pAdresseIP) {
    nbRequete = pNbTentative;
    heurePremiereRequete = pHeurePremiereRequete;
    adresseIP = pAdresseIP;
    listeHttpServletRequest = new ArrayList<HttpServletRequest>();
  }
  
  /**
   * Ajouter une tentative de requête.
   * 
   * Le nombre de requêtes maximum est de 50 (NB_SESSIONS_MAX_PAR_IP). Le compteur de requêtes est remis à 1 s'il n'y a aucune
   * requête pendant 60 000 ms (TEST_PAR_IP). Si le nombre de requêtes maximum est dépassé, l'adresse IP est marquée comme "moisie" et
   * les requêtes ne seront pas traitées.
   * 
   * Si le nombre de requêtes maximum (NB_SESSIONS_MAX_PAR_IP) est effectué en moins d'une minute (TEST_PAR_IP), on considère que c'est
   * un robot et l'adresse IP est ajoutée à la liste des adresses IP bloquées.
   * 
   * @param pHttpServletRequest Nouvelle requête HTTP effectuée par l'adresse IP.
   * @return true=traiter la requête, false=ne pas traiter la requête.
   */
  public boolean ajouterUneTentativeRequete(HttpServletRequest pHttpServletRequest) {
    if (listeHttpServletRequest == null) {
      return false;
    }
    
    // On modifie le dernier accès de cette
    heureDerniereRequete = pHttpServletRequest.getSession().getLastAccessedTime();
    
    // On teste si cette adresse mérite d'être réinitialisée
    if (listeHttpServletRequest != null && (heureDerniereRequete - heurePremiereRequete > MarbreEnvironnement.TEST_PAR_IP
        && nbRequete < MarbreEnvironnement.NB_SESSIONS_MAX_PAR_IP)) {
      nbRequete = 1;
      isMoisie = false;
      heurePremiereRequete = heureDerniereRequete;
      listeHttpServletRequest.clear();
      listeHttpServletRequest.add(pHttpServletRequest);
    }
    // ou alors on l'incrémente
    else {
      listeHttpServletRequest.add(pHttpServletRequest);
      nbRequete++;
    }
    
    // Si on dépasse le plafond de requêtes moisies pour une adresse IP
    if (nbRequete > MarbreEnvironnement.NB_SESSIONS_MAX_PAR_IP) {
      // Si on dépasse ce plafond dans un laps de temps qu'un humain ne peut atteindre
      if (heureDerniereRequete - heurePremiereRequete < MarbreEnvironnement.TEST_PAR_IP) {
        // Je bloque Dans le contexte
        if (SessionListener.listeIpBloquees == null) {
          SessionListener.listeIpBloquees = new HashMap<String, Integer>();
        }
        SessionListener.listeIpBloquees.put(pHttpServletRequest.getRemoteAddr(), 1);
      }
      
      isMoisie = true;
      
      return false;
    }
    else {
      return true;
    }
  }
  
  /**
   * Nombre de tentatives de requêtes de cette session.
   */
  public int getNbTentatives() {
    return nbRequete;
  }
  
  /**
   * Heure de la première tentative de requête en ms.
   */
  public long getHeurePremiereTentative() {
    return heurePremiereRequete;
  }
  
  /**
   * Heure de la dernière tentative de requête en ms.
   */
  public long getHeureDerniereTentative() {
    return heureDerniereRequete;
  }
  
  /**
   * Adresse IP surveillée.
   */
  public String getAdresseIP() {
    return adresseIP;
  }
  
  /**
   * Indique si l'adresse IP est "moisie".
   * Les requêtes des adresses IP moisies sont ignorées.
   */
  public boolean isMoisie() {
    return isMoisie;
  }
  
  /**
   * Liste des requêtes effectuées par l'adresse IP surveillée.
   */
  public ArrayList<HttpServletRequest> getListeHttpServletRequest() {
    return listeHttpServletRequest;
  }
}
