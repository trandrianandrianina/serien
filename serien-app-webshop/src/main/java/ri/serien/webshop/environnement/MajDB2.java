/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreAffichage;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.controleurs.GestionCatalogue;
import ri.serien.webshop.metier.Etablissement;

public class MajDB2 {
  private UtilisateurWebshop utilisateur = null;
  
  /**
   * Constructeur de MajDB2
   */
  public MajDB2(UtilisateurWebshop util) {
    utilisateur = util;
  }
  
  /**
   * Mettre à jour toutes les tables de DB2
   */
  public boolean mettreAJourDB2() {
    // NE PAS PASSER DANS CETTE PROCEDURE SI ON EST EN DEPLOIEMENT
    if ((MarbreEnvironnement.BIBLI_CLIENTS == null) || MarbreEnvironnement.BIBLI_WS == null || MarbreEnvironnement.ETB_DEFAUT == null) {
      return true;
    }
    
    // On récupère les PS nécessaires (A améliorer dans une fucking classe à terme)
    majPS();
    // maj des adresses moisies
    majAdressesIPmoisies();
    
    mettreAjourDivers();
    
    if (MarbreEnvironnement.isEclipse) {
      return true;
    }
    boolean retour = false;
    if (mettreAjourCatalogue()) {
      retour = mettreAjourVues();
    }
    
    return retour;
  }
  
  /**
   * Cette méthode récupère dans DB2 les adresses IP
   */
  private void majAdressesIPmoisies() {
    if (utilisateur == null || utilisateur.getAccesDB2() == null) {
      return;
    }
    
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT BL_ADR FROM " + MarbreEnvironnement.BIBLI_WS + ".BLOCAGE_IP ");
    
    if (liste != null && liste.size() > 0) {
      if (SessionListener.listeIpBloquees == null) {
        SessionListener.listeIpBloquees = new HashMap<String, Integer>();
      }
      
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("BL_ADR")) {
          SessionListener.listeIpBloquees.put(liste.get(i).getField("BL_ADR").toString().trim(),
              Integer.valueOf(MarbreEnvironnement.NB_SESSIONS_MAX));
          Trace.info("[MajDB2]-[majAdressesIPmoisies]- On rajoute dans la SessionListener.listeIpBloquees l'adresse "
              + liste.get(i).getField("BL_ADR"));
        }
      }
    }
  }
  
  /**
   * Mettre à jour les vues SQL spécifiques au WS
   */
  private boolean mettreAjourVues() {
    
    Trace.info("-------------- Mise à jour des vues / indexs");
    boolean retour = false;
    if (utilisateur != null && utilisateur.getAccesDB2() != null) {
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
      // +++++++++++++++++++++++++++++++++++++++++++ INDEXS +++++++++++++++++++++++++++++++++++++++++++++ //
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ //
      
      ArrayList<GenericRecord> liste = utilisateur.getAccesDB2()
          .select("SELECT INDEX_NAME,INDEX_SCHEMA FROM QSYS2.SYSINDEXES WHERE (INDEX_SCHEMA = '" + MarbreEnvironnement.BIBLI_CLIENTS
              + "' OR INDEX_SCHEMA = '" + MarbreEnvironnement.BIBLI_WS + "' OR INDEX_SCHEMA = '"
              + MarbreEnvironnement.BIBLI_WS.toUpperCase() + "')  AND (INDEX_NAME LIKE 'IN_%' OR INDEX_NAME LIKE 'IND_%')");
      
      if (liste == null) {
        return false;
      }
      HashMap<String, String> procedures = new HashMap<String, String>();
      Trace.info("On checke les indexs déjà présents");
      int result = -1;
      String requete = null;
      
      // DROPPER LES INDEXS BIBLI CLIENTS DYNAMIQUES
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("INDEX_SCHEMA") && liste.get(i).isPresentField("INDEX_NAME")) {
          Trace.info("index: " + i + " " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
              + liste.get(i).getField("INDEX_NAME").toString().trim());
        }
        else {
          Trace.info("index: " + i);
        }
        
        // Si l'index est statique (XWEB ou CLIENTS/IND_)
        if ((liste.get(i).isPresentField("INDEX_SCHEMA")
            && liste.get(i).getField("INDEX_SCHEMA").toString().trim().equals(MarbreEnvironnement.BIBLI_WS))
            || (liste.get(i).isPresentField("INDEX_SCHEMA") && liste.get(i).isPresentField("INDEX_NAME")
                && liste.get(i).getField("INDEX_NAME").toString().trim().startsWith("IND_"))) {
          // Si il n'est pas encore dans la liste des indexs
          if (!procedures.containsKey(liste.get(i).getField("INDEX_NAME").toString())) {
            procedures.put(liste.get(i).getField("INDEX_NAME").toString(), "1");
            Trace.info("  INDEX STATIQUE " + (i + 1) + " " + liste.get(i).getField("INDEX_NAME").toString());
          }
        }
        // Dynamique -> le dropper
        else if (liste.get(i).isPresentField("INDEX_SCHEMA") && liste.get(i).isPresentField("INDEX_NAME")) {
          if (MarbreEnvironnement.INDEXS_DYNAMIQUES) {
            result = utilisateur.getAccesDB2().requete("DROP INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
                + liste.get(i).getField("INDEX_NAME").toString().trim());
            Trace.info("  Suppression INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
                + liste.get(i).getField("INDEX_NAME").toString().trim() + " -> " + result);
          }
          // Si il n'est pas encore dans la liste des indexs
          else if (!procedures.containsKey(liste.get(i).getField("INDEX_NAME").toString())) {
            procedures.put(liste.get(i).getField("INDEX_NAME").toString(), "1");
            Trace.info("  INDEX STATIQUE " + (i + 1) + " " + liste.get(i).getField("INDEX_NAME").toString());
          }
        }
      }
      
      // ++++++++++++++++++++++++++++++++++ INDEXS STATIQUES +++++++++++++++++++++++++++++++++ //
      
      // -------------------------- INDEXS XWEB ---------------------------- //
      
      // INDEXS PROMOTIONS ++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_PRO_DTF")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_PRO_DTF ON " + MarbreEnvironnement.BIBLI_WS
            + ".PROMOTION (PR_DATEF ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_PRO_DTF: " + result);
      }
      else {
        Trace.info("INDEX IN_PRO_DTF DEJA PRESENT");
      }
      
      // INDEXS USERW +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_USE_LOG")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_USE_LOG ON " + MarbreEnvironnement.BIBLI_WS
            + ".USERW (US_LOGIN ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_USE_LOG: " + result);
      }
      else {
        Trace.info("INDEX IN_USE_LOG DEJA PRESENT");
      }
      
      // INDEXS MENUS +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_MEN_ORD")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_MEN_ORD ON " + MarbreEnvironnement.BIBLI_WS
            + ".MENU (MN_ORDRE ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_MEN_ORD: " + result);
      }
      else {
        Trace.info("INDEX IN_MEN_ORD DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_MEN_ACC")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_MEN_ACC ON " + MarbreEnvironnement.BIBLI_WS
            + ".MENU (US_ACC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_MEN_ACC: " + result);
      }
      else {
        Trace.info("INDEX IN_MEN_ACC DEJA PRESENT");
      }
      
      // MAGASINS
      if (!procedures.containsKey("IN_MAG_COD")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_MAG_COD ON " + MarbreEnvironnement.BIBLI_WS
            + ".MAGASINS (US_ETB ASC,MG_COD ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_MAG_COD: " + result);
      }
      else {
        Trace.info("INDEX IN_MAG_COD DEJA PRESENT");
      }
      
      // INDEXS CATALOGUE ++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_CAT_TYP")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_CAT_TYP ON " + MarbreEnvironnement.BIBLI_WS
            + ".CATALOGUE (CA_BIB ASC, CA_ETB ASC, CA_TYP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_CAT_TYP: " + result);
      }
      else {
        Trace.info("INDEX IN_CAT_TYP DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_CAT_IN")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_CAT_IN ON " + MarbreEnvironnement.BIBLI_WS
            + ".CATALOGUE (CA_BIB ASC, CA_ETB ASC, CA_IND ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_CAT_IN: " + result);
      }
      else {
        Trace.info("INDEX IN_CAT_IN DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_CAT_IN2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_CAT_IN2 ON " + MarbreEnvironnement.BIBLI_WS
            + ".CATALOGUE (CA_TYP ASC, CA_BIB ASC, CA_ETB ASC, CA_IND ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_CAT_IN2: " + result);
      }
      else {
        Trace.info("INDEX IN_CAT_IN2 DEJA PRESENT");
      }
      
      // INDEXS PANIER +++++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_PAN_MAG")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_PAN_MAG ON " + MarbreEnvironnement.BIBLI_WS
            + ".PANIER (PA_ETA ASC, PA_US ASC, PA_MAG ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_PAN_MAG: " + result);
      }
      else {
        Trace.info("INDEX IN_PAN_MAG DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_PAN_SES")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_PAN_SES ON " + MarbreEnvironnement.BIBLI_WS
            + ".PANIER (PA_ETA ASC, PA_US ASC, PA_SESS ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_PAN_SES: " + result);
      }
      else {
        Trace.info("INDEX IN_PAN_SES DEJA PRESENT");
      }
      
      // INDEXS FOURNISS +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_FOU_CA1")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_FOU_CA1 ON " + MarbreEnvironnement.BIBLI_WS
            + ".FOURNISS (FO_BIB ASC, FO_ETB ASC, FO_CA ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_FOU_CA1: " + result);
      }
      else {
        Trace.info("INDEX IN_FOU_CA1 DEJA PRESENT");
      }
      
      // INDEXS FAVORIS +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_FAVO_UT")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_FAVO_UT ON " + MarbreEnvironnement.BIBLI_WS
            + ".FAVORIS (FV_UTIL ASC, FV_ETB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_FAVO_UT: " + result);
      }
      else {
        Trace.info("INDEX IN_FAVO_UT DEJA PRESENT");
      }
      
      // INDEXS CONSULTES +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_CONS_UT")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_CONS_UT ON " + MarbreEnvironnement.BIBLI_WS
            + ".CONSULTES (CON_UTIL ASC, CON_ETB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_CONS_UT: " + result);
      }
      else {
        Trace.info("INDEX IN_CONS_UT DEJA PRESENT");
      }
      
      // INDEXS DELAISMOY +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IN_DEL_MG")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_WS + ".IN_DEL_MG ON " + MarbreEnvironnement.BIBLI_WS
            + ".DELAISMOY (DEL_ETB ASC, DEL_MGDP ASC, DEL_MGAR ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_DEL_MG: " + result);
      }
      else {
        Trace.info("INDEX IN_DEL_MG DEJA PRESENT");
      }
      
      // -------------------------- INDEXS BIBLI CLIENT ---------------------------- //
      
      // INDEXS PGVMPARM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_PAR_IN")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_PAR_IN ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMPARM (PARTYP ASC, PARIND ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_PAR_IN: " + result);
      }
      else {
        Trace.info("INDEX IND_PAR_IN DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_PAR_TY")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_PAR_TY ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMPARM (PARTOP ASC, PARETB ASC, PARTYP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_PAR_TY: " + result);
      }
      else {
        Trace.info("INDEX IND_PAR_TY DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_RTL_N2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_RTL_N2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTLM (RLETB ASC, RLCOD ASC, RLNUMT ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_RTL_N2: " + result);
      }
      else {
        Trace.info("INDEX IND_RTL_N2 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_RTL_IN")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_RTL_IN ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTLM (RLCOD ASC, RLNUMT ASC, RLIND ASC) UNIT ANY ";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_RTL_IN: " + result);
      }
      else {
        Trace.info("INDEX IND_RTL_IN DEJA PRESENT");
      }
      
      // INDEXS PSEMRTWM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_RTW_NU")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_RTW_NU ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTWM (RWACC ASC, RWNUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_RTW_NU: " + result);
      }
      else {
        Trace.info("INDEX IND_RTW_NU DEJA PRESENT");
      }
      
      // INDEXS PSEMRTEM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_RTE_NU")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_RTE_NU ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTEM (RENET ASC, RENUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_RTE_NU: " + result);
      }
      else {
        Trace.info("INDEX IND_RTE_NU DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_RTE_N2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_RTE_N2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PSEMRTEM (RENUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_RTE_N2: " + result);
      }
      else {
        Trace.info("INDEX IND_RTE_N2 DEJA PRESENT");
      }
      
      // INDEXS PGVMCLIM ++++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_CLI_CL")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_CLI_CL ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMCLIM (CLETB ASC, CLLIV ASC, CLCLI ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_CLI_CL: " + result);
      }
      else {
        Trace.info("INDEX IND_CLI_CL DEJA PRESENT");
      }
      
      // INDEXS PGVMADVM (adresses livraison) ++++++++++++
      
      if (!procedures.containsKey("IND_ADV_NU")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_ADV_NU ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMADVM (AVETB ASC, AVSUF ASC, AVNUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_ADV_NU: " + result);
      }
      else {
        Trace.info("INDEX IND_ADV_NU DEJA PRESENT");
      }
      
      // INDEXS PGVMXLIM (extensions lignes) ++++++++++++++
      
      if (!procedures.containsKey("IND_XLI_NL")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_XLI_NL ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMXLIM (XIETB ASC, XISUF ASC, XINUM ASC, XINLI ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_XLI_NL: " + result);
      }
      else {
        Trace.info("INDEX IND_XLI_NL DEJA PRESENT");
      }
      
      // INDEXS PGVMCNAM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_CNA_AR")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_CNA_AR ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAETB ASC, CAIN9 ASC, CACOL ASC, CAFRS ASC, CAART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_CNA_AR: " + result);
      }
      else {
        Trace.info("INDEX IND_CNA_AR DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_CNA_FR")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_CNA_FR ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAETB ASC, CAIN9 ASC, CAART ASC, CACOL ASC, CAFRS ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_CNA_FR: " + result);
      }
      else {
        Trace.info("INDEX IND_CNA_FR DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_CNA_AA")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_CNA_AA ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAIN9 ASC, CAETB ASC, CACOL ASC, CAFRS ASC, CAART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_CNA_AA: " + result);
      }
      else {
        Trace.info("INDEX IND_CNA_FR DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_CNA_A2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_CNA_A2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMCNAM (CAIN9 ASC, CACOL ASC, CAETB ASC, CAFRS ASC, CAART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_CNA_A2: " + result);
      }
      else {
        Trace.info("INDEX IND_CNA_A2 DEJA PRESENT");
      }
      
      // INDEXS PGVMTARM +++++++++++++++++++++++++++++++++
      
      if (!procedures.containsKey("IND_TAR_DA")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_TAR_DA ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMTARM (ATETB ASC, ATART ASC, ATDAP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_TAR_DA: " + result);
      }
      else {
        Trace.info("INDEX IND_TAR_DA DEJA PRESENT");
      }
      
      // INDEXS PGVMALAM +++++++++++++++++++++++++++++++++
      // Si on est en mode TRAITEMENT_STOCK_RESERVE on bosse avec les PGVMALAM (spécial CGED)
      if (MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE) {
        if (!procedures.containsKey("IND_ALA_NO")) {
          requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_ALA_NO ON " + MarbreEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AATOE ASC, AAART ASC, AAETB ASC, AASOS ASC, AALOS ASC, AANOS ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete);
          
          Trace.info("CREATE INDEX IND_ALA_NO: " + result);
        }
        else {
          Trace.info("INDEX IND_ALA_NO DEJA PRESENT");
        }
        
        if (!procedures.containsKey("IND_ALA_N2")) {
          requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_ALA_N2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AAMAG ASC, AATOE ASC, AAART ASC, AAETB ASC, AASOS ASC, AALOS ASC, AANOS ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete);
          
          Trace.info("CREATE INDEX IND_ALA_N2: " + result);
        }
        else {
          Trace.info("INDEX IND_ALA_N2 DEJA PRESENT");
        }
        
        if (!procedures.containsKey("IND_ALA_ET")) {
          requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_ALA_ET ON " + MarbreEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AAMAG ASC, AATOE ASC, AAART ASC, AAETB ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete);
          
          Trace.info("CREATE INDEX IND_ALA_ET: " + result);
        }
        else {
          Trace.info("INDEX IND_ALA_ET DEJA PRESENT");
        }
        
        if (!procedures.containsKey("IND_ALA_E2")) {
          requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_ALA_E2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
              + ".PGVMALAM (AACOS ASC, AATOE ASC, AAART ASC, AAMAG ASC, AAETB ASC) UNIT ANY";
          result = utilisateur.getAccesDB2().requete(requete);
          
          Trace.info("CREATE INDEX IND_ALA_E2: " + result);
        }
        else {
          Trace.info("INDEX IND_ALA_E2 DEJA PRESENT");
        }
      }
      
      // INDEXS PGVMEBCM (ENTETES de lignes) ++++++++++++++
      if (!procedures.containsKey("IND_EBC_FP")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_EBC_FP ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM (E1ETB ASC, E1CLFS ASC, E1COD ASC, E1CLFP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_EBC_FP: " + result);
      }
      else {
        Trace.info("INDEX IND_EBC_FP DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_EBC_NU")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_EBC_NU ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMEBCM (E1ETB ASC, E1SUF ASC, E1NUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_EBC_NU: " + result);
      }
      else {
        Trace.info("INDEX IND_EBC_NU DEJA PRESENT");
      }
      
      // INDEXS PGVMLBCM +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IND_LBC_TO")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_LBC_TO ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ERL ASC, L1COD ASC, L1TOP ASC) UNIT ANY ";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_LBC_TO: " + result);
      }
      else {
        Trace.info("INDEX IND_LBC_TO DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_LBC_T2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_LBC_T2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ERL ASC, L1COD ASC, L1ETB ASC, L1SUF ASC, L1NLI ASC, L1NUM ASC, L1TOP ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_LBC_T2: " + result);
      }
      else {
        Trace.info("INDEX IND_LBC_T2 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_LBC_NU")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_LBC_NU ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ETB ASC, L1ERL ASC, L1COD ASC, L1SUF ASC, L1NUM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_LBC_NU: " + result);
      }
      else {
        Trace.info("INDEX IND_LBC_NU DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IND_LBC_LI")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_LBC_LI ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMLBCM (L1ETB ASC, L1COD ASC, L1SUF ASC, L1NUM ASC, L1ERL ASC, L1NLI ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_LBC_LI: " + result);
      }
      else {
        Trace.info("INDEX IND_LBC_LI DEJA PRESENT");
      }
      
      // INDEXS PGVMFRSM (fournisseurs ++++++++++++++++++++
      if (!procedures.containsKey("IND_FRS_FR")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IND_FRS_FR ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMFRSM (FRETB ASC, FRCOL ASC, FRFRS ASC) UNIT ANY ";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IND_FRS_FR: " + result);
      }
      else {
        Trace.info("INDEX IND_FRS_FR DEJA PRESENT");
      }
      
      // ++++++++++++++++++++++++++++++++++ INDEXS DYNAMIQUES +++++++++++++++++++++++++++++++++ //
      
      // -------------------------- INDEXS BIBLI CLIENT ---------------------------- //
      
      // INDEXS PGVMARTM +++++++++++++++++++++++++++++++++
      if (!procedures.containsKey("IN_ART_ABC")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_ABC ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_ABC: " + result);
      }
      else {
        Trace.info("INDEX IND_FRS_FR DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_AB2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_AB2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ART ASC, A1ABC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_AB2: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_AB2 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_LIB")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_LIB ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1FAM ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_LIB: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_LIB DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_LI2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_LI2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_LI2: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_LI2 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_FAM")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_FAM ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1FAM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_FAM: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_FAM DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_SFA")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_SFA ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1SFA ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_SFA: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_SFA DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_SF2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_SF2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1IN15 ASC, A1NPU ASC, A1SFA ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_SF2: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_SF2 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_FRS")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_FRS ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1COF ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1FAM ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_FRS: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_FRS DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_FR2")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_FR2 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1SAI ASC, A1COF ASC, A1IN15 ASC, A1NPU ASC, A1FAM ASC, A1ABC ASC, A1LIB ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_FR2: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_FR2 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_FR3")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_FR3 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1COF ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_FR3: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_FR3 DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_A1A")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_A1A ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1IN15 ASC, A1NPU ASC, A1ART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_A1A: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_A1A DEJA PRESENT");
      }
      
      if (!procedures.containsKey("IN_ART_A12")) {
        requete = "CREATE INDEX " + MarbreEnvironnement.BIBLI_CLIENTS + ".IN_ART_A12 ON " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMARTM (A1ETB ASC, A1IN15 ASC, A1NPU ASC, A1ABC ASC, A1ART ASC) UNIT ANY";
        result = utilisateur.getAccesDB2().requete(requete);
        
        Trace.info("CREATE INDEX IN_ART_A12: " + result);
      }
      else {
        Trace.info("INDEX IN_ART_A12 DEJA PRESENT");
      }
      
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++ VUES ++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
      liste = utilisateur.getAccesDB2()
          .select("SELECT TABLE_NAME, TABLE_SCHEMA FROM QSYS2.SYSVIEWS WHERE TABLE_SCHEMA = '" + MarbreEnvironnement.BIBLI_CLIENTS + "'");
      procedures = new HashMap<String, String>();
      Trace.info("On checke les vues présentes");
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("TABLE_NAME") && liste.get(i).isPresentField("TABLE_SCHEMA")
            && liste.get(i).getField("TABLE_SCHEMA").toString().trim().equals(MarbreEnvironnement.BIBLI_CLIENTS)) {
          // Si il n'est pas encore dans la liste des indexs
          if (!procedures.containsKey(liste.get(i).getField("TABLE_NAME").toString().trim())) {
            procedures.put(liste.get(i).getField("TABLE_NAME").toString().trim(), "1");
            Trace.info("  VUE EXISTANTE : " + liste.get(i).getField("TABLE_NAME").toString());
          }
        }
      }
      result = -1;
      
      // VUE STOCKS
      if (!procedures.containsKey("VUE_STK_RE")) {
        if (MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE) {
          result = utilisateur.getAccesDB2().requete(
              "CREATE VIEW " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_STK_RE AS SELECT AAMAG, AAETB, AAART, SUM(AAQTE) as QTEAFF "
                  + "FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMALAM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                  + ".PGVMLBCM " + "ON AAETB=L1ETB AND AACOS=L1COD AND AANOS=L1NUM AND AASOS=L1SUF AND AALOS = L1NLI "
                  + "WHERE AATOE='S' AND AACOS = 'E' AND L1ERL = 'C' AND L1TOP < 4 AND L1QTE > 0 GROUP BY AAMAG, AAETB, AAART");
          Trace.info("CREATE VUE VUE_STK_RE: " + result);
        }
        else {
          Trace.info("Pas de gestion du reservé. Vue VUE_STK_RE inutile");
        }
      }
      else {
        Trace.info("VUE_STK_RE déjà présente");
      }
      
      // VUE CNA
      if (!procedures.containsKey("VUE_CNA_9")) {
        result = utilisateur.getAccesDB2().requete("CREATE VIEW " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".VUE_CNA_9 AS SELECT MAX(CACRE) AS DATEM, CAETB, CAART, CAFRS, CACOL," + "CAREF, CARFC FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMCNAM WHERE CAIN9 = '1' " + "GROUP BY CAETB, CAART, CAFRS,CACOL, CAREF, CARFC");
        
        Trace.info("CREATE VUE VUE_CNA_9: " + result);
      }
      else {
        Trace.info("VUE_CNA_9 déjà présente");
      }
      
      // VUE TARIFS
      if (!procedures.containsKey("VUE_TARIFS")) {
        result = utilisateur.getAccesDB2()
            .requete("CREATE VIEW " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_TARIFS AS SELECT " + "ATETB, ATART, ATDAP, ATP01 "
                + "FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMTARM a WHERE a.ATDAP = " + "(SELECT MAX(ATDAP) FROM "
                + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMTARM b " + "WHERE a.ATETB = b.ATETB AND a.ATART = b.ATART "
                + "AND b.ATDAP <= " + "(SELECT '1' CONCAT SUBSTR(YEAR(CURRENT DATE), 3, 2) "
                + "CONCAT SUBSTR(DIGITS(MONTH(CURRENT DATE)), 9, 2) " + "CONCAT SUBSTR(DIGITS(DAY(CURRENT DATE)), 9, 2) "
                + "FROM SYSIBM.SYSDUMMY1))");
        Trace.info("CREATE VUE VUE_TARIFS: " + result);
      }
      else {
        Trace.info("VUE_TARIFS déjà présente");
      }
      
      // VUE ARTICLES
      if (!procedures.containsKey("VUE_ART_UT")) {
        String coeffMultiple = "1";
        if (MarbreEnvironnement.ZONE_CONDITIONNEMENT != null && MarbreEnvironnement.ZONE_CONDITIONNEMENT.equals("A1CND")) {
          coeffMultiple = "100";
        }
        result = utilisateur.getAccesDB2()
            .requete("CREATE VIEW " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_ART_UT AS SELECT A1ETB, A1ART, "
                + "A1LIB, A1ABC, A1SAI, A1FAM, A1SFA, CAREF, CARFC, A1NPU, A1UNV, A1CL1, A1CL2, A1ASB, A1IN2, A1IN4, "
                + "SUBSTR(PARZ2, 82, 1) as DECIMQ, " + "SUBSTR(PARZ2, 1, 15) as LIBCND , CASE WHEN " + "SUBSTR(A1UNV, 2, 1)='*' THEN "
                + MarbreEnvironnement.ZONE_CONDITIONNEMENT + "*" + coeffMultiple + " WHEN " + MarbreEnvironnement.ZONE_CONDITIONNEMENT
                + " = '0' THEN 1 " + "ELSE " + MarbreEnvironnement.ZONE_CONDITIONNEMENT
                + " END AS CND, A1TVA, A1COF, DIGITS(A1FRS) AS A1FRS, FRAWS ,FRNWS, FRNOM " + "FROM " + MarbreEnvironnement.BIBLI_CLIENTS
                + ".PGVMARTM " + "LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".VUE_CNA_9 ON CAETB=A1ETB AND "
                + " CAART=A1ART AND A1FRS=CAFRS AND A1COF=CACOL " + "LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                + ".PGVMPARM ON PARTYP='UN' AND PARIND=A1UNV " + "LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                + ".PGVMFRSM ON FRETB=A1ETB AND FRCOL=A1COF " + "AND FRFRS=A1FRS WHERE A1IN15=' ' AND FRIN5 = ' '  ");
        
        Trace.info("CREATE VUE VUE_ART_UT: " + result);
      }
      else {
        Trace.info("VUE_ART_UT déjà présente");
      }
      
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Mettre à jour les groupes, familles et sous familles du catalogue
   */
  private boolean mettreAjourCatalogue() {
    boolean retour = false;
    Trace.info("-------------- Mise à jour du catalogue");
    if (utilisateur != null && utilisateur.getAccesDB2() != null) {
      int result = 0;
      result = utilisateur.getAccesDB2().requete(
          "DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".FOURNISS WHERE FO_BIB = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' ");
      if (result > -1) {
        if (utilisateur.getAccesDB2().requete("DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE WHERE CA_BIB = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "' ") < 0) {
          Trace.erreur("[mettreAjourCatalogue()] PB de suppression des catalogues");
        }
      }
      
      if (MarbreEnvironnement.ETB_DEFAUT != null) {
        String requete = "SELECT a.PARTYP as TYPE, a.PARIND as IND, SUBSTR(a.PARZ2, 1, 30) as LIB FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM a " + " WHERE a.PARETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
            + "' AND PARTYP='GR' AND a.PARTOP='0' AND a.PARIND <> ' ' " + " AND EXISTS (SELECT c.PARTYP FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM c WHERE c.PARETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
            + "' AND SUBSTR(c.PARIND, 1, 1) "
            + "=substr(a.PARIND, 1, 1) AND c.PARTYP='FA' AND SUBSTR(c.PARZ2, 138, 1)=' ' AND c.PARTOP='0' AND EXISTS(SELECT e.A1ART FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMARTM e WHERE e.A1ETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
            + "' AND SUBSTR(c.PARIND, 1, 3) = e.A1FAM " + MarbreAffichage.retournerFiltreSqlStatutArticle("e")
            + MarbreAffichage.retournerFiltreSqlVieArticle("e") + " AND e.A1IN15= ' ' " + ")) "
            + "UNION SELECT b.PARTYP as TYPE, b.PARIND as IND, SUBSTR(b.PARZ2, 1, 30) as LIB FROM " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMPARM b WHERE b.PARETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' AND b.PARTYP='FA' "
            + "AND SUBSTR(b.PARZ2, 138, 1)=' ' AND b.PARTOP='0' AND b.PARIND <> ' ' AND EXISTS (SELECT d.A1ART FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMARTM d WHERE d.A1ETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
            + "' AND SUBSTR(b.PARIND, 1, 3) = d.A1FAM " + MarbreAffichage.retournerFiltreSqlStatutArticle("d")
            + MarbreAffichage.retournerFiltreSqlVieArticle("d") + " AND d.A1IN15=' ' " + ") "
            + " UNION SELECT f.PARTYP as TYPE, f.PARIND as IND, SUBSTR(f.PARZ2, 1, 30) as LIB FROM " + MarbreEnvironnement.BIBLI_CLIENTS
            + ".PGVMPARM f " + "WHERE f.PARETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
            + "' AND f.PARTYP='SF' AND f.PARTOP='0' AND f.PARIND <> ' ' AND EXISTS (SELECT g.PARTYP FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM g WHERE " + " g.PARETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
            + "' AND SUBSTR(g.PARIND, 1, 3) "
            + "=substr(f.PARIND, 1, 3) AND g.PARTYP='FA' AND SUBSTR(g.PARZ2, 138, 1)=' ' AND g.PARTOP='0' AND EXISTS(SELECT h.A1ART "
            + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMARTM h " + " WHERE h.A1ETB = '"
            + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' " + " AND SUBSTR(f.PARIND, 1, 5) = h.A1SFA "
            + MarbreAffichage.retournerFiltreSqlStatutArticle("h") + MarbreAffichage.retournerFiltreSqlVieArticle("h")
            + " AND h.A1IN15=' ' " + ")) " + "ORDER BY IND FOR READ ONLY";
        
        ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(requete);
        if (liste != null && liste.size() > 0) {
          result = 0;
          String condition = "";
          ArrayList<GenericRecord> listeFrs = new ArrayList<GenericRecord>();
          for (int i = 0; i < liste.size(); i++) {
            result = utilisateur.getAccesDB2()
                .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".CATALOGUE (CA_BIB,CA_ETB,CA_TYP,CA_IND,CA_LIB) VALUES ('"
                    + MarbreEnvironnement.BIBLI_CLIENTS + "','" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "','"
                    + liste.get(i).getField(0) + "','" + liste.get(i).getField(1) + "','"
                    + (liste.get(i).getField(2).toString().replace("'", "''")) + "')");
            if (result > 0) {
              if (liste.get(i).getField("TYPE").toString().trim().equals("GR")) {
                condition = " SUBSTR(A1FAM, 1, 1) = '" + liste.get(i).getField("IND").toString() + "'";
              }
              else if (liste.get(i).getField("TYPE").toString().trim().equals("FA")) {
                condition = " A1FAM = '" + liste.get(i).getField("IND").toString() + "'";
              }
              else if (liste.get(i).getField("TYPE").toString().trim().equals("SF")) {
                condition = " A1SFA = '" + liste.get(i).getField("IND").toString() + "'";
              }
              
              listeFrs = utilisateur.getAccesDB2()
                  .select("SELECT A1ETB, A1COF, A1FRS, FRAWS ,FRNWS, FRNOM " + " FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMARTM "
                      + " JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMFRSM "
                      + " ON A1ETB = FRETB AND A1COF = FRCOL AND A1FRS = FRFRS " + " WHERE A1ETB = '"
                      + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' " + " AND " + condition
                      + MarbreAffichage.retournerFiltreSqlStatutArticle(null) + MarbreAffichage.retournerFiltreSqlVieArticle(null)
                      + " AND A1IN15=' ' AND FRIN5 = ' ' " + " GROUP BY A1ETB, A1COF, A1FRS, FRAWS ,FRNWS, FRNOM ");
              if (listeFrs != null && listeFrs.size() > 0) {
                for (GenericRecord record : listeFrs) {
                  GestionCatalogue.traiterNomFournisseur(record);
                  if (record.isPresentField("FRNOM") && !record.getField("FRNOM").toString().trim().isEmpty()) {
                    result = utilisateur.getAccesDB2()
                        .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
                            + ".FOURNISS (FO_BIB,FO_ETB,FO_CA,FO_COL,FO_FRS,FO_NOM) VALUES ('" + MarbreEnvironnement.BIBLI_CLIENTS + "','"
                            + record.getField("A1ETB").toString() + "','" + liste.get(i).getField("IND").toString() + "','"
                            + record.getField("A1COF").toString() + "','" + record.getField("A1FRS").toString() + "','"
                            + Gestion.traiterCaracteresSpeciauxSQL(record.getField("FRNOM").toString().trim()) + "')");
                  }
                }
              }
            }
          }
        }
      }
      
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Mettre à jour des éléments divers liés en général aux versions
   */
  private boolean mettreAjourDivers() {
    Trace.info("-------------- Mise à jour divers");
    if (MarbreEnvironnement.ETB_DEFAUT == null) {
      return false;
    }
    
    boolean retour = true;
    ArrayList<GenericRecord> liste = null;
    int resultat = -1;
    String nbColonnesVueArticle = "25";
    // On supprime la vue VUE_ART_UT (articles) si elle n'a pas la bonne structure
    // A changer en scannant les bonnes colonnes si ca venait à évoluer..
    liste = utilisateur.getAccesDB2().select("SELECT COUNT(*) AS NBCL FROM INFORMATION_SCHEMA.COLUMNS WHERE table_schema = '"
        + MarbreEnvironnement.BIBLI_CLIENTS + "' AND TABLE_NAME = '" + TableBDD.VUE_ART_UT + "' ");
    if (liste != null && liste.size() == 1) {
      GenericRecord record = liste.get(0);
      if (record.isPresentField("NBCL")) {
        if (!record.getField("NBCL").toString().equals(nbColonnesVueArticle)) {
          utilisateur.getAccesDB2().requete("DROP VIEW " + MarbreEnvironnement.BIBLI_CLIENTS + "." + TableBDD.VUE_ART_UT);
        }
      }
    }
    
    liste =
        utilisateur.getAccesDB2().select("SELECT EN_VAL FROM " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_CLE = 'ETB_FORCE' ");
    if (liste != null && liste.size() == 1) {
      GenericRecord record = liste.get(0);
      if (record.isPresentField("EN_VAL") && record.getField("EN_VAL").toString().trim().isEmpty()) {
        if (MarbreEnvironnement.LISTE_ETBS != null) {
          Etablissement etbForce = MarbreEnvironnement.LISTE_ETBS.get(0);
          if (etbForce != null) {
            resultat = utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL='"
                + etbForce.getCodeETB().trim() + " ' WHERE EN_CLE = 'ETB_FORCE' ");
            if (resultat < 0) {
              Trace.erreur(
                  "Probleme mettre à jour l'établissement par défaut dans ETB_FORCE " + MarbreEnvironnement.ETB_DEFAUT.getCodeETB());
              retour = false;
            }
          }
        }
      }
    }
    
    // QUE SI ON EST EN MONO ETB
    // On met à jour l'établissement dans ENVIRONM à partir du temporaire *TP 1.49 QUE SI ON EST EN MONO ETB
    liste = utilisateur.getAccesDB2().select("SELECT EN_CLE FROM " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_ETB = '*TP' ");
    
    if (liste != null && liste.size() > 0) {
      resultat = utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_ETB ='"
          + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' WHERE EN_ETB = '*TP' ");
      if (resultat < 0) {
        Trace.erreur(
            "Probleme mettre à jour l'établissement par défaut dans les zones *TP " + MarbreEnvironnement.ETB_DEFAUT.getCodeETB());
        retour = false;
      }
      
      // on met à jour les variables metiers
      resultat =
          utilisateur.getAccesDB2().requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_GEST=2 WHERE EN_ETB <>'' ");
    }
    
    // On fait ce contrôle pour initialiser la variable MAGASIN SIEGE dans tous les etablissements si elle n'existe pas
    if (MarbreEnvironnement.ETB_DEFAUT.getCodeETB() != null) {
      // On contrôle que la variable magasin siège n'est pas déjà correctement init dans le WebShop
      ArrayList<GenericRecord> listeVariables =
          utilisateur.getAccesDB2().select("SELECT EN_CLE FROM " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_ETB = '"
              + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' AND EN_CLE = 'MAGASIN_SIEGE' AND EN_VAL <> '' ");
      // Si la variable n'est pas correctement initialisée
      if (listeVariables != null && (listeVariables.size() == 0)) {
        // Il faut récupérer le bon magasin siège dans les paramètres
        String magasinSiegeDefaut = "";
        ArrayList<GenericRecord> listeMagsSiege = utilisateur.getAccesDB2()
            .select("SELECT SUBSTR(PARIND, 1, 2) AS MAG " + "FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM "
                + " WHERE PARETB ='" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB()
                + "' AND PARTOP = '0' AND PARTYP = 'MA' AND SUBSTR(PARZ3, 1, 1) <>' ' ");
        // S'il en existe un
        if (listeMagsSiege != null && listeMagsSiege.size() > 0) {
          if (listeMagsSiege.get(0).isPresentField("MAG")) {
            magasinSiegeDefaut = listeMagsSiege.get(0).getField("MAG").toString().trim();
          }
        }
        
        // Lors de la création d'un site Webshop, le code magasin doit être mis à la main.
        // Ensuite, le code magasin doit être ajouté.
        // Donc, il y a des cas où le magasin siège peut être vide
        // Le champ de la table ENVIRONM qui reçoit cette valeur accèpte uniquement les valeurs non null
        // Le vide est considéré comme null d'où l'ajout d'un espace avant l'ajout
        if (Constantes.normerTexte(magasinSiegeDefaut).isEmpty()) {
          magasinSiegeDefaut = " ";
        }
        
        // On vérifie que le magasin siege est en bdd
        listeVariables = utilisateur.getAccesDB2().select("SELECT EN_CLE FROM " + MarbreEnvironnement.BIBLI_WS
            + ".ENVIRONM WHERE EN_ETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' AND EN_CLE = 'MAGASIN_SIEGE' ");
        // La variable n'existe pas en base de donnée
        if (listeVariables != null && (listeVariables.size() == 0)) {
          resultat = utilisateur.getAccesDB2()
              .requete("" + "INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM " + " VALUES ('MAGASIN_SIEGE', '"
                  + magasinSiegeDefaut + "', 'DONNEES','2','" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "','')");
          if (resultat <= 0) {
            Trace.erreur("Probleme pour insérer le magasin siège dans les variables d'environnement de l'etb : "
                + MarbreEnvironnement.ETB_DEFAUT.getCodeETB());
          }
          
          Trace.info("[Utilisateur] mettreAjourDonneesLieesETB() magasinSiege 1 : " + magasinSiegeDefaut);
        }
        // Si elle existe que la variable est érronée on la met à jour
        else if (listeVariables != null && (listeVariables.size() == 1)) {
          resultat = utilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM " + " SET EN_VAL = '" + magasinSiegeDefaut
                  + "' WHERE EN_CLE = 'MAGASIN_SIEGE' AND EN_ETB = '" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' ");
          if (resultat <= 0) {
            Trace.erreur("Probleme pour insérer le magasin siège dans les variables d'environnement de l'etb : "
                + MarbreEnvironnement.ETB_DEFAUT.getCodeETB());
          }
          
          Trace.info("[Utilisateur] mettreAjourDonneesLieesETB() magasinSiege 1 : " + magasinSiegeDefaut);
        }
        else {
          return false;
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Mettre à jour les PS nécessaires aux traitements du Web Shop
   */
  public boolean majPS() {
    ArrayList<GenericRecord> liste = null;
    String PS = null;
    
    if (utilisateur != null && utilisateur.getAccesDB2() != null) {
      if (MarbreEnvironnement.ETB_DEFAUT == null) {
        Trace.erreur("ETB_DEFAUT NULL");
        return false;
      }
      // PS qui contrôle si on tient compte du réservé dans les stocks (spécial CGED)
      PS = "67";
      liste = utilisateur.getAccesDB2()
          .select("SELECT SUBSTR(PARZ2, " + PS + ", 1) AS PS FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM WHERE PARETB ='"
              + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' AND PARTYP = 'PS' AND PARIND = ' ' "
              + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      if (liste != null && liste.size() == 1) {
        if (liste.get(0).isPresentField("PS")) {
          MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE = liste.get(0).getField("PS").toString().trim().equals("5");
          Trace.info("[MajDB2] PS 67 à 5 " + MarbreEnvironnement.TRAITEMENT_STOCK_RESERVE);
        }
        else {
          Trace.erreur("[MajDB2] PB de PS");
        }
      }
      else {
        Trace.erreur("[MajDB2] PB de récupération de paramètres");
      }
    }
    
    // On verra plus tard pour stopper les autres traitements si celui-là déconne
    return true;
  }
  
  /**
   * Supprime tous les indexs
   * Même si elle ne sert plus je préfère la garder car on pourra regénérer les indexs comme sur beaucoup de sites Web
   */
  public boolean supprimerIndexs() {
    
    if (MarbreEnvironnement.isEclipse) {
      return true;
    }
    
    Trace.info("-------------- Suppression des vues / indexs");
    boolean isOk = false;
    int retourRequete = -1;
    
    // ON SUPPRIME LES INDEXS ++++++++++++++++++++++++++++++++++++++++++
    ArrayList<GenericRecord> liste =
        utilisateur.getAccesDB2().select("SELECT INDEX_NAME,INDEX_SCHEMA FROM QSYS2.SYSINDEXES WHERE INDEX_SCHEMA = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "'  AND (INDEX_NAME LIKE 'IN_%' OR INDEX_NAME LIKE 'IND_%')");
    
    if (liste == null) {
      return isOk;
    }
    
    for (int i = 0; i < liste.size(); i++) {
      retourRequete = utilisateur.getAccesDB2().requete("DROP INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("INDEX_NAME").toString().trim());
      Trace.info("  Suppression INDEX " + liste.get(i).getField("INDEX_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("INDEX_NAME").toString().trim() + " -> " + retourRequete);
    }
    
    // ON SUPPRIME LES VUES +++++++++++++++++++++++++++++++++++++++++++++
    
    liste = utilisateur.getAccesDB2()
        .select("SELECT TABLE_NAME, TABLE_SCHEMA FROM QSYS2.SYSVIEWS WHERE TABLE_SCHEMA = '" + MarbreEnvironnement.BIBLI_CLIENTS + "'");
    
    if (liste == null) {
      Trace.erreur("[supprimerIndexs] Problème de requete SELECT VIEWS ");
      return isOk;
    }
    
    for (int i = 0; i < liste.size(); i++) {
      
      retourRequete = utilisateur.getAccesDB2().requete("DROP VIEW " + liste.get(i).getField("TABLE_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("TABLE_NAME").toString().trim());
      Trace.info("  Suppression VIEW OK " + liste.get(i).getField("TABLE_SCHEMA").toString().trim() + "."
          + liste.get(i).getField("TABLE_NAME").toString().trim() + " -> " + retourRequete);
    }
    
    // A faire plus propre pour tenir compte des enregistrements moisis
    if (retourRequete > -1) {
      isOk = true;
    }
    
    return isOk;
  }
  
  /********************** ACCESSEURS ******************************/
  
  public UtilisateurWebshop getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(UtilisateurWebshop utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
