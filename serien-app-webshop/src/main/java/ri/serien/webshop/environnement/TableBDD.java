/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

public class TableBDD {
  
  // Table des paramètres de la GESCOM
  public static final String TABLE_PARAMETRES_GVM = "PGVMPARM";
  public final static String TABLE_FICHES_TECHNIQUES = "PSEMFTCM";
  public final static String TABLE_ARTICLE = "PGVMARTM";
  // Table de gestion des lots
  public static final String TABLE_LOTS = "PGVMSTLM";
  
  // table Panier du WebShop
  public static final String TABLE_WS_PANIER = "PANIER";
  // table qui contient les articles liés aux paniers du WebShop
  public static final String TABLE_WS_ARTI_PAN = "ARTI_PAN";
  // table des différentes langues utilisées par le WebShop
  public static final String TABLE_WS_LANGUES = "LANGUES";
  // table qui stocke les informations propres au promotions du WebShop
  public static final String TABLE_WS_PROMOTION = "PROMOTION";
  // table qui stocke le contenu du texte de l'accueil dans le WebShop en fonction de la langue
  public static final String TABLE_WS_INFOACC = "INFOACC";
  // Vue qui rassemble toutes les informations liées aux articles nécessaire dans le WebShop
  public static final String VUE_ART_UT = "VUE_ART_UT";
  // Table liée à la recherche des articles nécessaires dans le WebShop
  public static final String TABLE_WS_RECHARTICL = "RECHARTICL";
  
}
