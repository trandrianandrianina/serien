/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.exploitation.mail.EnumNomVariableMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.metier.Etablissement;
import ri.serien.webshop.metier.Panier;
import ri.serien.webshop.outils.Outils;
import ri.serien.webshop.outils.mail.ParametreMail;

public class GestionConnexion extends Gestion {
  
  /**
   * Mettre à jour la connexion Utilisateur si on lui passe un profil et un mot de passe.
   * 
   * @param pProtocoleHttps true=protocole HTTPS, false=protocole HTTP.
   */
  public int majConnexion(HttpSession pSession, String pLogin, String pMotDePasse, String pConfirmationMotDePasse, String pClient,
      String pProvenance, boolean pProtocoleHttps) {
    if (controlerLaSaisie(pLogin, pMotDePasse, pConfirmationMotDePasse)) {
      return controlerProfil(pSession, pLogin, pMotDePasse, pConfirmationMotDePasse, pClient, pProvenance, pProtocoleHttps);
    }
    else {
      Trace.erreur("Problème de login et / ou mot de passe : " + pLogin);
      ((UtilisateurWebshop) pSession.getAttribute("utilisateur"))
          .setTentativesConnexions(((UtilisateurWebshop) pSession.getAttribute("utilisateur")).getTentativesConnexions() + 1);
      return MarbreEnvironnement.ACCES_ER_SAISIE;
    }
  }
  
  /**
   * Contrôler la saisie des données passées
   */
  private boolean controlerLaSaisie(String pLogin, String pMotDePasse, String pConfirmationMotDePasse) {
    int longueurSecurite = 3;
    
    return (Constantes.normerTexte(pLogin).length() >= longueurSecurite
        && (Constantes.normerTexte(pMotDePasse).length() >= longueurSecurite));
  }
  
  /**
   * Contrôler si le profil existe dans la BDD
   * 
   * @param pProtocoleHttps true=protocole HTTPS, false=protocole HTTP.
   */
  private int controlerProfil(HttpSession pSession, String pLogin, String pMotDePasse, String pConfirmationMotDePasse, String pClient,
      String pProvenance, boolean pProtocoleHttps) {
    if (pSession.getAttribute("utilisateur") == null) {
      pSession.setAttribute("utilisateur", new UtilisateurWebshop(pSession.getId()));
    }
    
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) pSession.getAttribute("utilisateur");
    
    if (utilisateur.getAccesDB2() == null) {
      utilisateur.majConnexionDB2(MarbreEnvironnement.PROFIL_AS_ANONYME,
          Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_ANONYME)));
    }
    
    // Si l'utilisateur a bien l'accès à DB2
    if (utilisateur.getAccesDB2() != null) {
      utilisateur.setListeDeTravail(
          selectSecurite(utilisateur, "SELECT US_ID,US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB FROM " + MarbreEnvironnement.BIBLI_WS
              + ".USERW WHERE UPPER(US_LOGIN) = ? " + MarbreEnvironnement.CLAUSE_OPTIMIZE, pLogin.toUpperCase()));
      
      // Si le profil existe dans la base de données
      if ((utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() == 1)
          || (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 1 && pClient != null)) {
        if (pConfirmationMotDePasse != null) {
          return MarbreEnvironnement.ACCES_DEJA_EXISTANT;
        }
        int accesWS = MarbreEnvironnement.ACCES_REFUSE;
        GenericRecord record = utilisateur.getListeDeTravail().get(0);
        // récup de l'accès de l'utilisateur
        try {
          accesWS = Integer.parseInt(Constantes.convertirObjetEnTexte(record.getField("US_ACCES")));
        }
        catch (Exception e) {
          Trace.erreur(e.getMessage());
        }
        
        // Si son accès est celui d'un client ou d'un client en mdoe consultation
        if (accesWS == MarbreEnvironnement.ACCES_CLIENT || accesWS == MarbreEnvironnement.ACCES_CONSULTATION) {
          // On contrôle quand même si l'utilisateur est encore existant dans la base Série M et autorisé en un seul exemplaire
          int autorisation = controlerClientAutorise(utilisateur, pLogin, Constantes.convertirObjetEnTexte(record.getField("US_ETB")));
          // autorisé et un seul exemplaire
          if (autorisation == 1 || (autorisation > 1 && pClient != null)) {
            if (pClient != null) {
              pClient = Constantes.normerTexte(pClient);
              if (pClient.length() < 6) {
                int boucle = 6 - pClient.length();
                String ajout = "";
                for (int i = 0; i < boucle; i++) {
                  ajout += "0";
                }
                
                pClient = ajout + pClient;
              }
              
              utilisateur.setListeDeTravail(selectSecurite(utilisateur,
                  " SELECT US_ID,US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5" + " FROM "
                      + MarbreEnvironnement.BIBLI_WS + ".USERW " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM "
                      + " ON CAST(RLNUMT AS CHAR(15)) = US_SERIEM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTEM ON RLNUMT = RENUM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTWM ON RLNUMT = RWNUM " + " WHERE RLCOD ='C' AND RWACC = 'W' AND UPPER(RENET) = ? "
                      + " AND SUBSTR(RLIND, 1, 6) = '" + pClient + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE,
                  pLogin.toUpperCase()));
            }
            else {
              utilisateur.setListeDeTravail(selectSecurite(utilisateur,
                  " SELECT US_ID,US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5" + " FROM "
                      + MarbreEnvironnement.BIBLI_WS + ".USERW " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTLM "
                      + " ON CAST(RLNUMT AS CHAR(15)) = US_SERIEM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTEM ON RLNUMT = RENUM " + " LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                      + ".PSEMRTWM ON RLNUMT = RWNUM " + " WHERE RLCOD ='C' AND RWACC = 'W' AND UPPER(RENET) = ? "
                      + MarbreEnvironnement.CLAUSE_OPTIMIZE,
                  pLogin.toUpperCase()));
            }
            
            // on re contrôle la liste au cas ou on l'ait remplie à nouveau
            if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0) {
              // TODO c'est ici qu'on va contrôler qu'il y ait un seul contact Série M associé dans ce cas il faut
              // supprimer
              if (utilisateur.getListeDeTravail().size() > 1) {
                String codeContact = Constantes.convertirObjetEnTexte(utilisateur.getListeDeTravail().get(0).getField("US_SERIEM"));
                String requete = "SELECT US_ID,US_LOGIN,US_SERIEM FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW WHERE US_SERIEM = '"
                    + codeContact + "' AND UPPER(US_LOGIN) <> '" + pLogin.toUpperCase() + "' ";
                ArrayList<GenericRecord> liste = utilisateur.getAccesDB2().select(requete);
                if (liste != null) {
                  for (GenericRecord record2 : liste) {
                    String requeteDelete = "DELETE FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW WHERE US_ID = '"
                        + Constantes.convertirObjetEnTexte(record2.getField("US_ID")) + "' ";
                    int retour = utilisateur.getAccesDB2().requete(requeteDelete);
                    if (retour < 1) {
                      Trace.erreur(
                          "Probleme pour supprimer l'utilisateur  : " + Constantes.convertirObjetEnTexte(record2.getField("US_ID")));
                    }
                  }
                }
              }
              
              record = utilisateur.getListeDeTravail().get(0);
              // Contrôle Mot de passe
              if (Constantes.convertirObjetEnTexte(record.getField("US_PASSW"))
                  .equals(Constantes.convertirObjetEnTexte(Base64Coder.encodeString(Base64Coder.encodeString(pMotDePasse))))) {
                Etablissement etb = null;
                if (record.isPresentField("US_ETB") && !Constantes.convertirObjetEnTexte(record.getField("US_ETB")).equals("")) {
                  IdEtablissement idEtablissement =
                      IdEtablissement.getInstance(Constantes.convertirObjetEnTexte(record.getField("US_ETB")));
                  etb = new Etablissement(utilisateur, idEtablissement);
                }
                
                utilisateur.setInfos(record, etb);
                utilisateur.setMonPanier(new Panier(utilisateur));
                
                // Ajouter un évènement pour la connexion de l'utilisateur
                utilisateur.getAccesDB2().sauverEvenement(utilisateur.getIdentifiant(), pSession.getId(), "C",
                    "Connexion " + utilisateur.getLogin(), this.getClass().getSimpleName());
              }
              else {
                utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
                utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_ER_MDP);
                Trace.erreur("Problème de login et / ou mot de passe : " + pLogin);
              }
            }
          }
          // si plusieurs contacts du même logins sont présents et autorisés
          else if (autorisation == 2 && pClient == null) {
            utilisateur.setLogin(pLogin);
            utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_MULTIPLE_CONNEX);
          }
          // si pas autorisé ou pas présent
          else {
            utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
            utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_REFUSE);
            Trace.erreur("Problème de login et / ou mot de passe : " + pLogin);
          }
        }
        // si son accès est celui d'un profil boutique, représentant , responsable ou d'un admin
        else if (accesWS > MarbreEnvironnement.ACCES_CONSULTATION) {
          // Si l'accès est OK
          if (utilisateur.majConnexionDB2(pLogin, pMotDePasse)) {
            // On checke la provenance de la connexion (WS ou mobilite +)
            utilisateur.setMobilitePlus(pProvenance != null && pProvenance.equals("1"));
            // On ajoute les infos à l'utilisateur
            Etablissement etb = null;
            if (record.isPresentField("US_ETB") && !Constantes.convertirObjetEnTexte(record.getField("US_ETB")).equals("")) {
              IdEtablissement idEtablissement = IdEtablissement.getInstance(Constantes.convertirObjetEnTexte(record.getField("US_ETB")));
              etb = new Etablissement(utilisateur, idEtablissement);
            }
            utilisateur.setInfos(record, etb);
            utilisateur.getAccesDB2().sauverEvenement(utilisateur.getIdentifiant(), pSession.getId(), "C",
                "Connexion " + utilisateur.getLogin(), this.getClass().getSimpleName());
            // On renomme les profils de type boutique
            if (accesWS == MarbreEnvironnement.ACCES_BOUTIQUE) {
              utilisateur.setLogin("BOUTIQUE");
              // Attribuer un temps max d'inactivité à la session
              pSession.setMaxInactiveInterval(50000);
            }
          }
          // Si la saisie du profil n'est pas bonne
          else {
            utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
            utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_ER_MDP);
          }
        }
        // Si l'accès est un profil en attente de validation
        else if (accesWS == MarbreEnvironnement.ACCES_AT_VALID) {
          utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_AT_VALID);
        }
      }
      // Le client existe en double dans USERW
      else if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 1) {
        utilisateur.setLogin(pLogin);
        utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_MULTIPLE_CONNEX);
      }
      // Si le profil n'existe pas encore dans WS
      else {
        int autorisation = controlerClientAutorise(utilisateur, pLogin, null);
        
        // Si il est autorisé à accéder au WS
        if (autorisation == 1 || (autorisation == 2 && pClient != null)) {
          utilisateur.setLogin(pLogin);
          // si on est dans un cas d'inscription
          if (pConfirmationMotDePasse != null) {
            // Si le contrôle de mot de passe est bon
            if (pMotDePasse.equals(pConfirmationMotDePasse)) {
              int idUtilisateur = insererNouveauClient(utilisateur, pMotDePasse, pClient, null);
              // si le client est bien inséré dans la base de données
              if (idUtilisateur > 0) {
                // Basculer le profil en attente de validation
                utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_AT_VALID);
                
                // Préparer un mail de validation d'inscription
                ParametreMail parametreMail = preparerParametreMailValidationInscription(idUtilisateur, pProtocoleHttps);
                utilisateur.envoyerMail(utilisateur.getLogin(), parametreMail, EnumTypeMail.MAIL_VALIDATION_INSCRIPTION_HTML);
              }
            }
            else {
              utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_ER_CONF_MDP);
            }
          }
          else {
            utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_AT_MDP);
          }
        }
        // si plusieurs contacts du même logins sont présents et autorisés
        else if (autorisation == 2 && pClient == null) {
          utilisateur.setLogin(pLogin);
          utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_MULTIPLE_INSCRI);
        }
        // S'il n'est pas autorisé
        else {
          Trace.erreur("Ce profil n'est pas autorisé à utiliser le WebSHop, veuillez contacter votre responsable WebShop :" + pLogin);
          utilisateur.setTentativesConnexions(((UtilisateurWebshop) pSession.getAttribute("utilisateur")).getTentativesConnexions() + 1);
          utilisateur.setTypeAcces(MarbreEnvironnement.ACCES_INEXISTANT);
        }
      }
      
      return utilisateur.getTypeAcces();
    }
    else {
      return MarbreEnvironnement.ACCES_NON_DEPLOYE;
    }
  }
  
  /**
   * Retourne si le login passé en paramètre est un login client autorisé à accéder au WS
   */
  private int controlerClientAutorise(UtilisateurWebshop pUtilisateur, String pLogin, String pEtablissement) {
    if (pUtilisateur == null) {
      Trace.erreur("Utilisateur à NULL");
      return 0;
    }
    
    ArrayList<GenericRecord> liste = null;
    // AVEC PASSAGE D'ETB
    if (!Constantes.normerTexte(pEtablissement).equals("")) {
      liste = selectSecurite(pUtilisateur,
          "SELECT * FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
              + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
              + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND RLETB ='" + pEtablissement
              + "' AND UPPER(RENET) = ?  FETCH FIRST 2 ROWS ONLY OPTIMIZE FOR 2 ROWS ",
          pLogin.toUpperCase());
    }
    else {
      // METHODE SANS ETB pour LE CAS DU MULTI ETB
      liste = selectSecurite(pUtilisateur, "SELECT * FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
          + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
          + ".PSEMRTLM ON RENUM = RLNUMT  WHERE RWACC = 'W' AND RLCOD ='C' AND UPPER(RENET) = ?  FETCH FIRST 2 ROWS ONLY OPTIMIZE FOR 2 ROWS ",
          pLogin.toUpperCase());
    }
    
    // Si le profil existe et est autorisé dans l'AS et si il est unique ou non
    if (liste != null && liste.size() == 1) {
      return 1;
    }
    else if (liste != null && liste.size() > 1) {
      return 2;
    }
    else {
      return 0;
    }
  }
  
  /**
   * Insérer un nouveau client dans la base de donnée du Web Shop
   */
  private int insererNouveauClient(UtilisateurWebshop pUtilisateur, String pMotDePasse, String pClient, String pEtablissement) {
    int idUtilisateur = 0;
    
    if (pUtilisateur != null) {
      if (pUtilisateur.getAccesDB2() != null) {
        // Vérifier client et récupérer les données Série M propres à ce contact
        ArrayList<GenericRecord> liste1 = null;
        // Si on passe la sélection d'un client en paramètre
        if (pClient != null) {
          pClient = Constantes.normerTexte(pClient);
          if (pClient.length() < 6) {
            int boucle = 6 - pClient.length();
            String ajout = "";
            for (int i = 0; i < boucle; i++) {
              ajout += "0";
            }
            
            pClient = ajout + pClient;
          }
          
          if (pEtablissement != null) {
            liste1 = selectSecurite(pUtilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                    + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND RLETB ='" + pEtablissement
                    + "' AND UPPER(RENET) = ? AND SUBSTR(RLIND, 1, 6) = '" + pClient + "' FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS "
                    + MarbreEnvironnement.CLAUSE_OPTIMIZE,
                pUtilisateur.getLogin().toUpperCase());
          }
          else {
            liste1 = selectSecurite(pUtilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                    + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND UPPER(RENET) = ? AND SUBSTR(RLIND, 1, 6) = '"
                    + pClient + "' FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE,
                pUtilisateur.getLogin().toUpperCase());
          }
        }
        // Contact d'un seul client unique
        else {
          if (pEtablissement != null) {
            liste1 = selectSecurite(pUtilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                    + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND RLETB ='" + pEtablissement
                    + "' AND UPPER(RENET) = ? FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE,
                pUtilisateur.getLogin().toUpperCase());
          }
          else {
            liste1 = selectSecurite(pUtilisateur,
                "SELECT RLNUMT,RLETB,RWIN1,RWIN2,RWIN3,RWIN4,RWIN5 FROM " + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTEM LEFT JOIN "
                    + MarbreEnvironnement.BIBLI_CLIENTS + ".PSEMRTWM ON RENUM = RWNUM LEFT JOIN " + MarbreEnvironnement.BIBLI_CLIENTS
                    + ".PSEMRTLM ON RENUM = RLNUMT WHERE RWACC = 'W' AND RLCOD ='C' AND UPPER(RENET) = ? "
                    + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE,
                pUtilisateur.getLogin().toUpperCase());
          }
        }
        
        if (liste1 != null && liste1.size() == 1) {
          if (liste1.get(0).isPresentField("RLNUMT")) {
            pUtilisateur.setIdContact(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RLNUMT")));
          }
          if (liste1.get(0).isPresentField("RLETB")) {
            IdEtablissement idEtablissement =
                IdEtablissement.getInstance(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RLETB")));
            pUtilisateur.setEtablissementEnCours(new Etablissement(pUtilisateur, idEtablissement));
          }
          if (liste1.get(0).isPresentField("RWIN1")) {
            pUtilisateur.setStockDisponibleVisible(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RWIN1")).equals("1"));
          }
          if (liste1.get(0).isPresentField("RWIN2")) {
            pUtilisateur.setPrixVisible(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RWIN2")).equals("1"));
          }
          if (liste1.get(0).isPresentField("RWIN3")) {
            pUtilisateur.setSaisieCommandeAutorisee(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RWIN3")).equals("1"));
          }
          if (liste1.get(0).isPresentField("RWIN4")) {
            pUtilisateur.setCommandesEtablissementVisible(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RWIN4")).equals("1"));
          }
          if (liste1.get(0).isPresentField("RWIN5")) {
            pUtilisateur.setSaisieDevisAutorisee(Constantes.convertirObjetEnTexte(liste1.get(0).getField("RWIN5")).equals("1"));
          }
          
          liste1 = null;
          // Insertion du contact dans la base DB2 du Web Shop
          if (pUtilisateur.getAccesDB2()
              .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".USERW "
                  + "(US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB,US_DATC,US_DATM) VALUES " + "('" + pUtilisateur.getLogin() + "'" + ",'"
                  + Base64Coder.encodeString(Base64Coder.encodeString(pMotDePasse)) + "'" + ",'" + MarbreEnvironnement.ACCES_AT_VALID
                  + "'" + ",'" + pUtilisateur.getIdContact() + "'" + ",'" + pUtilisateur.getEtablissementEnCours().getCodeETB() + "'"
                  + ",'" + Outils.recupererDateCouranteInt() + "'" + ",'" + Outils.recupererDateCouranteInt() + "')") == 1) {
            liste1 = pUtilisateur.getAccesDB2().select("SELECT US_ID FROM " + MarbreEnvironnement.BIBLI_WS
                + ".USERW WHERE UPPER(US_LOGIN) = '" + pUtilisateur.getLogin().toUpperCase() + "' ");
            if (liste1 != null && liste1.size() == 1) {
              try {
                idUtilisateur = Integer.parseInt(Constantes.convertirObjetEnTexte(liste1.get(0).getField("US_ID")));
              }
              catch (Exception e) {
                Trace.erreur(e.getMessage());
              }
            }
          }
        }
      }
    }
    
    return idUtilisateur;
  }
  
  /**
   * Prépare un mail de notification d'inscription
   **/
  private boolean envoyerMailNotification(UtilisateurWebshop pUtilisateur) {
    if (pUtilisateur == null || pUtilisateur.getClient() == null) {
      return false;
    }
    
    boolean isSucces = true;
    String mailDestinataire = null;
    if (pUtilisateur.getClient().getMagasinClient() != null) {
      if (pUtilisateur.getClient().getMagasinClient().getMailInscriptions() != null
          && !Constantes.normerTexte(pUtilisateur.getClient().getMagasinClient().getMailInscriptions()).equals("")) {
        mailDestinataire = pUtilisateur.getClient().getMagasinClient().getMailInscriptions();
      }
      else {
        if (pUtilisateur.getEtablissementEnCours() != null) {
          mailDestinataire = pUtilisateur.getEtablissementEnCours().getMail_inscription();
        }
      }
    }
    else if (pUtilisateur.getEtablissementEnCours() != null) {
      mailDestinataire = pUtilisateur.getEtablissementEnCours().getMail_inscription();
    }
    
    if (mailDestinataire != null) {
      ParametreMail parametreMail = preparerParametreMailNotificationInscription(pUtilisateur);
      pUtilisateur.envoyerMail(mailDestinataire, parametreMail, EnumTypeMail.MAIL_NOTIFICATION_INSCRIPTION_HTML);
    }
    
    return isSucces;
  }
  
  /**
   * Retourne l'état du profil après l'avoir basculé en CLIENT
   */
  public int validationProfil(UtilisateurWebshop pUtilisateur, String pIdUtilisateur) {
    int validation = MarbreEnvironnement.ACCES_INEXISTANT;
    
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      return validation;
    }
    
    try {
      // On teste si l'utlisateur existe dans la base et on récupère ses accès
      int idDecode = Integer.parseInt(pIdUtilisateur) / MarbreEnvironnement.ENCODE_INT;
      ArrayList<GenericRecord> liste = pUtilisateur.getAccesDB2().select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS
          + ".USERW WHERE US_ID = '" + idDecode + "' FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      // Si l'utilisateur existe
      if (liste != null && liste.size() == 1) {
        // Si l'utilisateur est bien en attente de validation
        if (Integer.parseInt(Constantes.convertirObjetEnTexte(liste.get(0).getField("US_ACCES"))) == MarbreEnvironnement.ACCES_AT_VALID) {
          // Mettre à jour ses droits d'accès
          if (pUtilisateur.getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".USERW SET US_ACCES = '" + MarbreEnvironnement.ACCES_CLIENT
                  + "', US_DATM = '" + Outils.recupererDateCouranteInt() + "' WHERE US_ID = '" + idDecode + "'") == 1) {
            if (liste.get(0).isPresentField("US_LOGIN")) {
              pUtilisateur.setLogin(Constantes.convertirObjetEnTexte(liste.get(0).getField("US_LOGIN")));
            }
            
            if (liste.get(0).isPresentField("US_ETB")) {
              pUtilisateur.setEtablissementEnCours(MarbreEnvironnement.ETB_DEFAUT);
              
              if (liste.get(0).isPresentField("US_SERIEM")) {
                pUtilisateur.setIdContact(Constantes.convertirObjetEnTexte(liste.get(0).getField("US_SERIEM")));
                
                pUtilisateur.majDesInfosAS400();
                envoyerMailNotification(pUtilisateur);
              }
            }
            validation = MarbreEnvironnement.ACCES_VAL_PROFIL;
            pUtilisateur.setTypeAcces(MarbreEnvironnement.ACCES_PUBLIC);
          }
        }
        else {
          validation = MarbreEnvironnement.ACCES_DEJA_EXISTANT;
        }
      }
      
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
    }
    
    return validation;
  }
  
  /**
   * Permet de déployer les paramètres du client au sein même du serveur
   */
  public boolean traiterLeDeploiement(HttpServletRequest pRequest, UtilisateurWebshop pUtilisateur) {
    // Tester si le profil admin est bien le bon
    if (!testerProfilAdmin(pRequest, pUtilisateur)) {
      return false;
    }
    Trace.erreur("testerProfilAdmin : TRUE");
    // tester si les paramètres passés sont corrects
    if (pRequest.getParameter("ADRESSE_PUBLIC_SERVEUR") == null
        || !Constantes.isAdresseIpV4(pRequest.getParameter("ADRESSE_PUBLIC_SERVEUR"))) {
      return false;
    }
    if (Constantes.normerTexte(pRequest.getParameter("LETTRE_ENV")).isEmpty()) {
      return false;
    }
    if (Constantes.normerTexte(pRequest.getParameter("BIBLI_CLIENTS")).isEmpty()) {
      return false;
    }
    if (Constantes.normerTexte(pRequest.getParameter("ETB_FORCE")).isEmpty()) {
      return false;
    }
    
    boolean isDeployee = false;
    
    if (pUtilisateur.majConnexionDB2(pRequest.getParameter("PROFIL_ADMIN"), pRequest.getParameter("MP_ADMIN"))) {
      isDeployee = ajouterProfilAdmin(pRequest, pUtilisateur);
      if (!isDeployee) {
        return false;
      }
      isDeployee = ajouterParametres(pRequest, pUtilisateur);
    }
    
    return isDeployee;
  }
  
  /**
   * Insertion dans DB2 du nouveau profil administrateur WebShop
   */
  private boolean ajouterProfilAdmin(HttpServletRequest pRequest, UtilisateurWebshop pUtilisateur) {
    if (Constantes.normerTexte(pRequest.getParameter("PROFIL_ADMIN")).isEmpty() || pUtilisateur == null) {
      return false;
    }
    
    String requete = " UPDATE " + MarbreEnvironnement.BIBLI_WS + ".USERW SET US_LOGIN = '"
        + Constantes.normerTexte(pRequest.getParameter("PROFIL_ADMIN")).toUpperCase() + "' WHERE US_LOGIN = 'SERIEMAD' ";
    int resultat = pUtilisateur.getAccesDB2().requete(requete);
    return resultat == 1;
  }
  
  /**
   * Insertion dans DB2 des paramètres de déploiement propre au client
   */
  private boolean ajouterParametres(HttpServletRequest pRequest, UtilisateurWebshop pUtilisateur) {
    if (pUtilisateur == null || pUtilisateur.getAccesDB2() == null) {
      return false;
    }
    int resultat = -1;
    int nbInsertion = 0;
    String arguments = "";
    
    String requeteInsert = "INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM (EN_CLE, EN_VAL, EN_TYP,EN_GEST,EN_ETB) VALUES ";
    // VARIABLES DE SERVEUR
    if (pRequest.getParameter("ADRESSE_PUBLIC_SERVEUR") != null
        && Constantes.isAdresseIpV4(pRequest.getParameter("ADRESSE_PUBLIC_SERVEUR"))) {
      arguments += isVirguleNecessaire(arguments) + " ('ADRESSE_PUBLIC_SERVEUR', '"
          + Constantes.normerTexte(pRequest.getParameter("ADRESSE_PUBLIC_SERVEUR")) + "','DONNEES','1','')";
      nbInsertion++;
    }
    if (!Constantes.normerTexte(pRequest.getParameter("PORT_SERVEUR")).isEmpty()) {
      arguments += isVirguleNecessaire(arguments) + " ('PORT_SERVEUR', '" + Constantes.normerTexte(pRequest.getParameter("PORT_SERVEUR"))
          + "','DONNEES','1','')";
      nbInsertion++;
    }
    if (!Constantes.normerTexte(pRequest.getParameter("LETTRE_ENV")).isEmpty()) {
      arguments += isVirguleNecessaire(arguments) + " ('LETTRE_ENV', '"
          + Constantes.normerTexte(pRequest.getParameter("LETTRE_ENV")).toUpperCase() + "','MAJUSCULE','1','')";
      nbInsertion++;
    }
    if (!Constantes.normerTexte(pRequest.getParameter("BIBLI_CLIENTS")).isEmpty()) {
      arguments += isVirguleNecessaire(arguments) + " ('BIBLI_CLIENTS', '"
          + Constantes.normerTexte(pRequest.getParameter("BIBLI_CLIENTS")).toUpperCase() + "','MAJUSCULE','1','')";
      nbInsertion++;
    }
    arguments += isVirguleNecessaire(arguments) + " ('MAIL_IS_MODE_TEST', '1','DONNEES','1','')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('MAIL_TESTS', '" + MarbreEnvironnement.MAIL_TESTS + "','DONNEES','1','')";
    nbInsertion++;
    
    // VARIABLES D'ETB
    arguments += isVirguleNecessaire(arguments) + " ('LIMIT_LISTE', '15','DONNEES','1','*TP')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('LIMIT_REQUETE_ARTICLE', '150','DONNEES','1','*TP')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('OK_ENVOIS_MAILS', '1','DONNEES','1','*TP')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('MODE_STOCKS', '0','DONNEES','1','*TP')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('MAIL_CONTACT', '','DONNEES','1','*TP')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('IS_MONO_MAGASIN', '0','DONNEES','1','*TP')";
    nbInsertion++;
    arguments += isVirguleNecessaire(arguments) + " ('VISUSTOCKS', '0','DONNEES','1','*TP')";
    nbInsertion++;
    
    resultat = pUtilisateur.getAccesDB2().requete(requeteInsert + arguments);
    if (nbInsertion != resultat) {
      return false;
    }
    
    // Mises à jour de variables existantes
    if (!Constantes.normerTexte(pRequest.getParameter("ETB_FORCE")).isEmpty()) {
      String requeteUpdate = "UPDATE " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM SET EN_VAL = '"
          + Constantes.normerTexte(pRequest.getParameter("ETB_FORCE")) + "' WHERE EN_CLE = 'ETB_FORCE' ";
      int resul = pUtilisateur.getAccesDB2().requete(requeteUpdate);
      resultat += resul;
      nbInsertion++;
    }
    
    return nbInsertion == resultat;
  }
  
  /**
   * Teste si une virgule est nécessaire ou non
   */
  private String isVirguleNecessaire(String arguments) {
    if (!Constantes.normerTexte(arguments).isEmpty()) {
      return ",";
    }
    else {
      return " ";
    }
  }
  
  /**
   * Permet de tester si le profil Admin correspond bien à celui qui est attendu lors du déploiement
   */
  private boolean testerProfilAdmin(HttpServletRequest pRequest, UtilisateurWebshop pUtilisateur) {
    
    if (Constantes.normerTexte(pRequest.getParameter("PROFIL_ADMIN")).isEmpty()
        || Constantes.normerTexte(pRequest.getParameter("MP_ADMIN")).isEmpty()) {
      return false;
    }
    String profil = Constantes.normerTexte(pRequest.getParameter("PROFIL_ADMIN")).toUpperCase();
    String motPasse = Constantes.normerTexte(pRequest.getParameter("MP_ADMIN")).toUpperCase();
    
    return pUtilisateur.majConnexionDB2(profil, motPasse);
  }
  
  /**
   * Prépare un mail avec un lien de changement de mot de passe si le profil est correct.
   * 
   * @param pUtilisateur Information sur l'utilisateur.
   * @param pDestinataire Adresse mail à laquelle le mail sera envoyé.
   * @param pProtocoleHttps true=protocole HTTPS, false=protocole HTTP.
   * @return Type d'accès accordé à l'utilisateur.
   */
  public int changerMotDePasseProfil(UtilisateurWebshop pUtilisateur, String pDestinataire, boolean pProtocoleHttps) {
    int retour = MarbreEnvironnement.ACCES_INEXISTANT;
    pDestinataire = Constantes.normerTexte(pDestinataire);
    // Vérifier les paramètres
    if (pUtilisateur == null || pDestinataire.equals("")) {
      return retour;
    }
    
    // Lire le mode d'accès de l'utilisateur dans la base de données
    ArrayList<GenericRecord> liste = selectSecurite(pUtilisateur, "SELECT US_ID, US_ACCES FROM " + MarbreEnvironnement.BIBLI_WS
        + ".USERW WHERE UPPER(US_LOGIN) = ? " + MarbreEnvironnement.CLAUSE_OPTIMIZE, pDestinataire.toUpperCase());
    
    // Tester si l'utilisateur existe
    if (liste != null && liste.size() == 1) {
      // Tester si l'utilisateur possède un accès au Webshop
      if (liste.get(0).isPresentField("US_ACCES")) {
        try {
          int accesUtilisateur = Constantes.convertirTexteEnInteger(Constantes.convertirObjetEnTexte(liste.get(0).getField("US_ACCES")));
          // Tester si c'est un administrateur
          if (accesUtilisateur > MarbreEnvironnement.ACCES_CLIENT) {
            retour = MarbreEnvironnement.ACCES_NV_MP_ADMIN;
          }
          else {
            // Dans le cas d'un utilisateur standard
            int idUtilisateur = Constantes.convertirTexteEnInteger(Constantes.convertirObjetEnTexte(liste.get(0).getField("US_ID")));
            if (idUtilisateur > 0) {
              retour = MarbreEnvironnement.ACCES_DE_NV_MP;
              
              // Préparer le mail de changement de mot de passe
              ParametreMail parametreMail = preparerParametreMailChangementMotDePasse(idUtilisateur, pProtocoleHttps);
              pUtilisateur.envoyerMail(pDestinataire, parametreMail, EnumTypeMail.MAIL_CHANGEMENT_MOT_DE_PASSE_HTML);
            }
          }
        }
        catch (Exception e) {
          Trace.erreur(e.getMessage());
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Vérifie l'utilisateur et l'ID de modification de MP avant de modifier le mot de passe
   */
  public int validerChgtMP(HttpServletRequest pRequest) {
    int retour = MarbreEnvironnement.ACCES_ER_MDP;
    if (pRequest == null || pRequest.getParameter("UtilnvMP") == null || pRequest.getParameter("UtilnvMP").equals("")) {
      return retour;
    }
    
    ArrayList<GenericRecord> liste = null;
    int idChgt = 0;
    
    try {
      idChgt = Integer.parseInt(Constantes.normerTexte(pRequest.getParameter("UtilnvMP"))) / MarbreEnvironnement.ENCODE_INT;
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
      return retour;
    }
    
    // on teste si l'ID de changement de mot de passe correspond à l'utilisateur saisi
    if (pRequest.getSession().getAttribute("utilisateur") != null
        && controlerLaSaisie(Constantes.normerTexte(pRequest.getParameter("loginUser")),
            Constantes.normerTexte(pRequest.getParameter("passUser")), Constantes.normerTexte(pRequest.getParameter("passUserCtrl")))) {
      // Mots de passes identiques
      if (Constantes.normerTexte(pRequest.getParameter("passUser"))
          .equals(Constantes.normerTexte(pRequest.getParameter("passUserCtrl")))) {
        // Vérifier que l'uilisateur est bien l'ID de changement décrypté
        liste = ((UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur")).getAccesDB2()
            .select("SELECT US_ID FROM " + MarbreEnvironnement.BIBLI_WS + ".USERW WHERE US_ID = '" + idChgt + "' AND UPPER(US_LOGIN) = '"
                + Constantes.normerTexte(pRequest.getParameter("loginUser")).toUpperCase() + "' ");
        if (liste != null && liste.size() == 1) {
          if (((UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur")).getAccesDB2()
              .requete("UPDATE " + MarbreEnvironnement.BIBLI_WS + ".USERW SET US_PASSW = '"
                  + Base64Coder.encodeString(Base64Coder.encodeString(Constantes.normerTexte(pRequest.getParameter("passUser"))))
                  + "', US_DATM = '" + Outils.recupererDateCouranteInt() + "' WHERE US_ID ='" + idChgt + "' ") == 1) {
            return MarbreEnvironnement.ACCES_VAL_NV_MP;
          }
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Excécute les requêtes SELECT avec une sécurité utilisateur
   */
  public ArrayList<GenericRecord> selectSecurite(UtilisateurWebshop pUtilisateur, String pRequete, String pParametre) {
    if (pUtilisateur == null) {
      return null;
    }
    
    Connection dataBase = pUtilisateur.getMaconnectionAS();
    if (dataBase == null) {
      return null;
    }
    
    final ArrayList<GenericRecord> listeRecord = new ArrayList<GenericRecord>();
    int i = 0;
    GenericRecord record = null;
    PreparedStatement select = null;
    ResultSet rs = null;
    try {
      // Tracer la requête
      // Préfixé SQL WEBSHOP car cette requête devrait être géré par le QueryManager. Changement à faire.
      Trace.info("[SQL WEBSHOP] " + pRequete);
      long debut = System.currentTimeMillis();
      
      // Prépare le statement
      select = dataBase.prepareStatement(pRequete);
      select.setString(1, pParametre);
      rs = select.executeQuery();
      if (rs != null) {
        while (rs.next()) {
          record = new GenericRecord();
          for (i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            record.setField(rs.getMetaData().getColumnName(i), rs.getObject(i), rs.getMetaData().getPrecision(i),
                rs.getMetaData().getScale(i));
          }
          listeRecord.add(record);
        }
        rs.close();
      }
      
      select.close();
      
      // Tracer le résultat et la durée de la requête
      // Préfixé SQL WEBSHOP car cette requête devrait être géré par le QueryManager. Changement à faire.
      long duree = System.currentTimeMillis() - debut;
      Trace.info("[SQL WEBSHOP] " + listeRecord.size() + " lignes (" + duree + " ms)");
    }
    catch (SQLException exc) {
      // Encapsulation de l'exception retournée par le JT400 car celle-ci n'est pas connue sur le client.
      // Par exemple, le JT400 peut retourner l'exception AS400JDBCSQLSyntaxErrorException. Si celle-ci était intégrée dans
      // MessageErreurException, nous aurions une erreur UnmarshalException côté client car celui-ci n'a pas accès à jt400.jar.
      throw new MessageErreurException("Une requête SQL a provoqué une erreur, merci de contacter le support.");
    }
    
    return listeRecord;
  }
  
  /**
   * Prépare les paramètres du mail de notification d'inscription.
   * 
   * @param pUtilisateur Information sur l'utilisateur
   * @return
   */
  private ParametreMail preparerParametreMailNotificationInscription(UtilisateurWebshop pUtilisateur) {
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.CIVILITE_CONTACT,
        StringEscapeUtils.escapeHtml4(pUtilisateur.getCiviliteContact()));
    parametreMail.ajouterParametre(EnumNomVariableMail.NOM_CONTACT, StringEscapeUtils.escapeHtml4(pUtilisateur.getNomContact()));
    parametreMail.ajouterParametre(EnumNomVariableMail.MAIL_CLIENT, StringEscapeUtils.escapeHtml4(pUtilisateur.getLogin()));
    parametreMail.ajouterParametre(EnumNomVariableMail.NOM_CLIENT,
        StringEscapeUtils.escapeHtml4(pUtilisateur.getClient().getNomClient()));
    parametreMail.ajouterParametre(EnumNomVariableMail.REFERENCE_CLIENT,
        StringEscapeUtils.escapeHtml4(pUtilisateur.getClient().getNumeroClient() + "/" + pUtilisateur.getClient().getSuffixeClient()));
    
    return parametreMail;
  }
  
  /**
   * Génère l'url de changement de mot de passe.
   * 
   * @param pIdUtilisateur ID de l'utilisateur
   * @param pProtocoleHttps Indicateur de protocole à utiliser
   * @return
   */
  private String genererLienChangementMotDePasse(int pIdUtilisateur, boolean pProtocoleHttps) {
    String urlChangeMotDePasse = "";
    // Utiliser le protocole https si c'est indiqué
    if (pProtocoleHttps) {
      urlChangeMotDePasse += "https://";
    }
    // Sinon, appliquer le protocole http
    else {
      urlChangeMotDePasse += "http://";
    }
    
    urlChangeMotDePasse += MarbreEnvironnement.ADRESSE_PUBLIC_SERVEUR + ":" + MarbreEnvironnement.PORT_SERVEUR + "/"
        + MarbreEnvironnement.DOSSIER_WS + "/connexion?saisieNVMP=" + pIdUtilisateur * MarbreEnvironnement.ENCODE_INT;
    
    return urlChangeMotDePasse;
  }
  
  /**
   * Prépare les paramètres du mail de changement de mot de passe.
   * 
   * @param pIdUtilisateur ID de l'utilisateur
   * @param pProtocoleHttps Indicateur de protocole à utiliser
   * @return
   */
  private ParametreMail preparerParametreMailChangementMotDePasse(int pIdUtilisateur, boolean pProtocoleHttps) {
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.URL_CHANGE_MOTDEPASSE,
        StringEscapeUtils.escapeHtml4(genererLienChangementMotDePasse(pIdUtilisateur, pProtocoleHttps)));
    
    return parametreMail;
  }
  
  /**
   * Génère l'url d'inscription d'un nouveau utilisateur
   * .
   * 
   * @param pIdUtilisateur ID de l'utilisateur
   * @param pProtocoleHttps Indicateur de protocole à utiliser
   * @return
   */
  private String genererLienInscription(int pIdUtilisateur, boolean pProtocoleHttps) {
    String urlInscription = "";
    // Utiliser le protocole https si c'est indiqué
    if (pProtocoleHttps) {
      urlInscription += "https://";
    }
    // Sinon, appliquer le protocole http
    else {
      urlInscription += "http://";
    }
    
    urlInscription += MarbreEnvironnement.ADRESSE_PUBLIC_SERVEUR + ":" + MarbreEnvironnement.PORT_SERVEUR + "/"
        + MarbreEnvironnement.DOSSIER_WS + "/connexion?validation=" + pIdUtilisateur * MarbreEnvironnement.ENCODE_INT;
    
    return urlInscription;
  }
  
  /**
   * Prépare les paramètres du mail de validation d'inscription.
   * 
   * @param pIdUtilisateur ID de l'utilisateur
   * @param pProtocoleHttps Indicateur de protocole à utiliser
   * @return
   */
  private ParametreMail preparerParametreMailValidationInscription(int pIdUtilisateur, boolean pProtocoleHttps) {
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.LIEN_INSCRIPTION,
        StringEscapeUtils.escapeHtml4(genererLienInscription(pIdUtilisateur, pProtocoleHttps)));
    
    return parametreMail;
  }
}
