/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;

/**
 * Permet de gérer l'ensemble des requetes rentrant sur le serveur afin de gérer la sécurité du serveur
 */
public class GestionRequetes {
  // liste d'adresses IP suspectes tentant de se connceter au serveur
  private static HashMap<String, AdresseSuspecte> listeAdressesSuspectes = null;
  private static UtilisateurWebshop utilisateur = null;
  
  /**
   * Teste une requête rentrante sur certains critères de sécurité
   */
  public static boolean testerRequete(HttpServletRequest request) {
    
    if (request == null || request.getRemoteAddr() == null) {
      return false;
    }
    
    // On bloque la requête si l'adresse IP fait partie de la liste noire des adresses
    if (SessionListener.listeIpBloquees != null && SessionListener.listeIpBloquees.containsKey(request.getRemoteAddr())) {
      // On ajoute une tentative par requête sur cette adresse
      SessionListener.listeIpBloquees.put(request.getRemoteAddr(), SessionListener.listeIpBloquees.get(request.getRemoteAddr()) + 1);
      // Si on dépasse le quota on bloque en BDD
      if (SessionListener.listeIpBloquees.get(request.getRemoteAddr()) > MarbreEnvironnement.NB_SESSIONS_MAX_PAR_IP) {
        // Si l'adresse est déjà bloquée de manière définitive
        if (SessionListener.listeIpBloquees.get(request.getRemoteAddr()) < MarbreEnvironnement.NB_SESSIONS_MAX) {
          // RENTRER EN BASE l'ADRESSE IP
          if (utilisateur == null) {
            utilisateur = new UtilisateurWebshop();
          }
          if (utilisateur != null && utilisateur.getAccesDB2() != null) {
            ArrayList<GenericRecord> liste = null;
            liste = utilisateur.getAccesDB2().select(
                "SELECT BL_ADR FROM " + MarbreEnvironnement.BIBLI_WS + ".BLOCAGE_IP WHERE BL_ADR = '" + request.getRemoteAddr() + "' ");
            if (liste == null || liste.size() == 0) {
              if (utilisateur.getAccesDB2()
                  .requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".BLOCAGE_IP  (BL_ADR,BL_NB) VALUES ('"
                      + request.getRemoteAddr() + "'," + MarbreEnvironnement.NB_SESSIONS_MAX + ")") > 0) {
                SessionListener.listeIpBloquees.put(request.getRemoteAddr(), MarbreEnvironnement.NB_SESSIONS_MAX);
              }
            }
          }
        }
      }
      
      return false;
    }
    
    // On va contrôler la fréquence des requêtes sur la base de leur provenance
    if (listeAdressesSuspectes == null) {
      listeAdressesSuspectes = new HashMap<String, AdresseSuspecte>();
    }
    
    // Si l'adresse est déjà repérée
    if (listeAdressesSuspectes.containsKey(request.getRemoteAddr())) {
      // if(MarbreEnvironnement.MODE_DEBUG)
      // Si l'adresse est considérée comme moisie
      if (listeAdressesSuspectes.get(request.getRemoteAddr()).isMoisie()) {
        return false;
      }
      else {
        return listeAdressesSuspectes.get(request.getRemoteAddr()).ajouterUneTentativeRequete(request);
      }
    }
    // Si nouvelle adresse
    else {
      listeAdressesSuspectes.put(request.getRemoteAddr(),
          new AdresseSuspecte(1, request.getSession().getLastAccessedTime(), request.getRemoteAddr()));
    }
    
    // On vire les adresses IP qui ne sont plus suspectes
    nettoyerLesAdressesNonSuspectes(request);
    
    return true;
  }
  
  /**
   * On retire les adresses suspectes qui ne le sont plus
   */
  private static void nettoyerLesAdressesNonSuspectes(HttpServletRequest request) {
    if (request == null) {
      return;
    }
    
    try {
      for (Map.Entry<String, AdresseSuspecte> indice : listeAdressesSuspectes.entrySet()) {
        // On ne prend que les adresses IP qui ne concernent pas cette requête
        if (!indice.getKey().equals(request.getRemoteAddr())) {
          if ((request.getSession().getLastAccessedTime()
              - indice.getValue().getHeurePremiereTentative()) > MarbreEnvironnement.TEST_PAR_IP && !indice.getValue().isMoisie()
              && indice.getValue().getNbTentatives() < MarbreEnvironnement.NB_SESSIONS_MAX_PAR_IP) {
            // Je vire les requêtes suspectes de cette adresse
            indice.getValue().getListeHttpServletRequest().clear();
            // On vire l'adresse de la liste
            listeAdressesSuspectes.remove(indice.getKey());
          }
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
    }
  }
  
  /**
   * Afficher la liste des adresses IP suspectes
   */
  public static void afficherListeAdressessuspectes() {
  }
  
  // ++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++++//
  
  public static HashMap<String, AdresseSuspecte> getListeAdressesSuspectes() {
    return listeAdressesSuspectes;
  }
  
  public static void setListeAdressesSuspectes(HashMap<String, AdresseSuspecte> listeAdressesSuspectes) {
    GestionRequetes.listeAdressesSuspectes = listeAdressesSuspectes;
  }
  
}
