/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.dao.gvm.programs.GestionTarif;
import ri.serien.libas400.dao.gvx.database.PgvmsecmManager;
import ri.serien.libas400.dao.gvx.programs.GestionStock;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.webshop.constantes.MarbreAffichage;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.controleurs.GestionAccueil;
import ri.serien.webshop.controleurs.GestionCatalogue;
import ri.serien.webshop.metier.Client;
import ri.serien.webshop.metier.Etablissement;
import ri.serien.webshop.metier.Filtres;
import ri.serien.webshop.metier.Magasin;
import ri.serien.webshop.metier.Panier;
import ri.serien.webshop.metier.PanierIntuitif;
import ri.serien.webshop.modeles.AccesBDDWS;
import ri.serien.webshop.modeles.DuplicDevis;
import ri.serien.webshop.modeles.GestionCommandes;
import ri.serien.webshop.outils.Traduction;
import ri.serien.webshop.outils.mail.ParametreMail;
import ri.serien.webshop.outils.mail.ServiceMail;

/**
 * Contient les informations d'un utilisateur.
 */
public class UtilisateurWebshop {
  private AccesBDDWS accesDB2 = null;
  private String sessionEnCours = null;
  private int id = MarbreEnvironnement.PROFIL_INVITE;
  private String pageEnCours = "";
  
  private String login = null;
  private int typeAcces = MarbreEnvironnement.ACCES_PUBLIC;
  private Etablissement etablissementEnCours = null;
  // code devise de l'utilisateur
  private String US_DEV = null;
  // code représentant
  private String US_REPR = null;
  // civilite de ce contact
  private String idContact = null;
  private String civiliteContact = null;
  private String nomContact = null;
  private String telContact = null;
  // client associé
  private Client client = null;
  
  // Droits
  private boolean stockDisponibleVisible = true;
  private boolean prixVisible = true;
  private boolean commandeEtablissementVisible = true;
  private boolean saisieDevisAutorisee = true;
  private boolean saisieCommandeAutorisee = true;
  
  private int tentativesConnexions = 0;
  private String bibli = null;
  // liste temporaire pour bosser avec des listes de GenericRecord
  private ArrayList<GenericRecord> listeDeTravail = null;
  private GenericRecord recordTravail = null;
  private ArrayList<GenericRecord> paniersIntuitifsActifs = null;
  // AHAHAHHHHH ENFIN UNE LISTE COMMUNE POUR TOUTES LES PAGINATIONS
  private ArrayList<GenericRecord> derniereListePagination = null;
  private ArrayList<GenericRecord> derniereListePartiellePagination = null;
  // Les listes pour pagination et mémorisation des articles
  private ArrayList<GenericRecord> derniereListeArticle = null;
  private ArrayList<GenericRecord> derniereListeArticleMoteur = null;
  private ArrayList<GenericRecord> derniereListeArticleFavoris = null;
  private ArrayList<GenericRecord> derniereListeArticleHistorique = null;
  private ArrayList<GenericRecord> derniereListeArticleConsultee = null;
  private ArrayList<GenericRecord> listeAccesBackOffice = null;
  private ArrayList<GenericRecord> derniereListePanierIntuitif = null;
  private ArrayList<GenericRecord> listePromotionsActives = null;
  
  private String dernierTypeBon = null;
  private SystemeManager systeme = null;
  private Connection maconnectionAS = null;
  private PgvmsecmManager mesSecurites = null;
  
  private String derniereRequete = null;
  private GestionTarif gestionTarif = null;
  private GestionStock gestionStock = null;
  private GestionCommandes gestionCommandes = null;
  private DuplicDevis duplicationDevis = null;
  private String derniereExpression = null;
  private String dernierGroupeArticles = null;
  private String derniereFamilleArticles = null;
  private String dernierFiltreFournisseur = null;
  private Filtres filtres = null;
  private String derniersFavoris = null;
  private String dernierHistorique = null;
  private String derniersConsultes = null;
  private String dernierPaniersIntuitifs = null;
  private String indiceDebut = "0";
  private String parametresEnCours = null;
  private boolean isMobilitePlus = false;
  
  private HashMap<String, String[]> derniereListeFournisseur = null;
  private boolean visuStock = false;
  
  // language de l'utilisateur
  private String language = null;
  // Classe de traduction de tous les textes
  private Traduction traduction = null;
  // Mes pieds de page dynamiques
  private ArrayList<GenericRecord> mesFooters = null;
  // Mes menus dynamiques
  private ArrayList<GenericRecord> mesMenus = null;
  
  // Variables nécessaires au panier
  private Panier monPanier = null;
  private Magasin magasinSiege = null;
  private String typeDonneesEncours = "E";
  private PanierIntuitif panierIntuitif = null;
  
  /**
   * Constructeur d'un utilisateur
   */
  public UtilisateurWebshop(String session) {
    sessionEnCours = session;
    // si la récupération des variables d'environnements ne fonctionne pas correctement
    if (!majConnexionDB2(MarbreEnvironnement.PROFIL_AS_ANONYME,
        Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_ANONYME)))) {
      typeAcces = MarbreEnvironnement.ACCES_PAS_DB2;
    }
    else {
      if (!majVariablesEnvironnement()) {
        typeAcces = MarbreEnvironnement.ACCES_NON_DEPLOYE;
      }
      
      if (MarbreEnvironnement.ETB_DEFAUT != null) {
        etablissementEnCours = MarbreEnvironnement.ETB_DEFAUT;
      }
      
      language = System.getProperty("user.language");
      traduction = new Traduction(this);
      traduction.chargerListeEquivalence(language);
      // Charger mes footers
      mesFooters = retournerFooters(language);
      
      filtres = new Filtres(this);
      filtres.init();
      
      // si on doit revenir sur une page par défaut c'est l'accueil bien sûr
      parametresEnCours = "accueil";
    }
    
    if (typeAcces >= MarbreEnvironnement.ACCES_PUBLIC) {
      // on charge automatiquement les promotions
      listePromotionsActives = GestionAccueil.recupererPromotions(this);
    }
  }
  
  /**
   * Constructeur anonyme
   * 
   */
  public UtilisateurWebshop() {
    if (majConnexionDB2(MarbreEnvironnement.PROFIL_AS_ANONYME,
        Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_ANONYME)))) {
      majVariablesEnvironnement();
    }
    
    if (MarbreEnvironnement.ETB_DEFAUT != null) {
      etablissementEnCours = MarbreEnvironnement.ETB_DEFAUT;
    }
  }
  
  /**
   * Permet de mettre à jour toutes les variables d'environnement à partir de
   * DB2
   */
  protected boolean majVariablesEnvironnement() {
    // marbreEnvironnement bien mises à jour
    if (MarbreEnvironnement.BIBLI_CLIENTS != null && MarbreEnvironnement.ETB_DEFAUT != null) {
      return true;
    }
    else {
      // Sortir si l'accès DB2 est pourrave
      if (accesDB2 == null) {
        return false;
      }
      
      // Tracer
      Trace.titre("LECTURE DES PARAMETRES SITUES DANS LA BASE DE DONNEES WEBSHOP");
      
      listeDeTravail = accesDB2.select(
          "SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM WHERE EN_CLE='LETTRE_ENV' " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
      // si pb requête ou table environm est vide de cette variable indispensable
      if (listeDeTravail == null || listeDeTravail.size() <= 0) {
        return false;
      }
      
      return remplirVariableEnvironnementDepuisTable();
    }
    
  }
  
  /**
   * Remplie les variables d'environnement par les valeurs de la table
   * 
   */
  private boolean remplirVariableEnvironnementDepuisTable() {
    listeDeTravail =
        accesDB2.select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".ENVIRONM " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (listeDeTravail == null || listeDeTravail.size() <= 0) {
      return false;
    }
    
    // On définit les variables d'environnement pour chaque élément de la table
    identifierVariablesEnvironnement();
    
    if (MarbreEnvironnement.LETTRE_ENV == ' ') {
      return false;
    }
    
    // On met à jour l'information environnement
    if (!majEnvironnement()) {
      return false;
    }
    
    // On va vérifier les différents établissements dans la DG et on met à jour l'environnement ETABLISS si nécessaire
    if (!gererEtablissements()) {
      return false;
    }
    
    // On met à jour l'information Google Analytics
    majGoogleAnalytics();
    
    // On met à jour les variables d'affichage
    majVariablesAffichage();
    
    return true;
  }
  
  /**
   * Identifie les valeurs de la table pour chaque variable d'environnement
   */
  private void identifierVariablesEnvironnement() {
    for (int i = 0; i < listeDeTravail.size(); i++) {
      if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("ADRESSE_PUBLIC_SERVEUR")) {
        MarbreEnvironnement.ADRESSE_PUBLIC_SERVEUR = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL"));
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("PORT_SERVEUR")) {
        MarbreEnvironnement.PORT_SERVEUR = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL"));
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("LETTRE_ENV")) {
        MarbreEnvironnement.LETTRE_ENV = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).charAt(0);
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("BIBLI_CLIENTS")) {
        MarbreEnvironnement.BIBLI_CLIENTS = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL"));
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("ETB_FORCE")) {
        MarbreEnvironnement.ETB_FORCE = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL"));
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("MAIL_IS_MODE_TEST")) {
        MarbreEnvironnement.MAIL_IS_MODE_TEST = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).equals("1");
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("MAIL_TESTS")) {
        MarbreEnvironnement.MAIL_TESTS = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL"));
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("TEMPS_SESSION_INACTIVE")) {
        try {
          MarbreEnvironnement.TEMPS_SESSION_INACTIVE =
              Integer.parseInt(Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")));
        }
        catch (NumberFormatException e) {
          Trace.erreur(e.getMessage());
        }
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("ZONE_CONDITIONNEMENT")) {
        MarbreEnvironnement.ZONE_CONDITIONNEMENT = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL"));
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("MODE_DEBUG")) {
        MarbreEnvironnement.MODE_DEBUG = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).equals("1");
        Trace.setModeDebug(MarbreEnvironnement.MODE_DEBUG);
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("IS_MODE_SSL")) {
        MarbreEnvironnement.IS_MODE_SSL = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).equals("1");
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("IS_MODE_LANGUAGES")) {
        MarbreEnvironnement.IS_MODE_LANGUAGES = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).equals("1");
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("CATALOGUE_MODE_PUBLIC")) {
        MarbreEnvironnement.CATALOGUE_MODE_PUBLIC =
            Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).equals("1");
      }
      else if (Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_CLE")).equals("IS_GESTION_LOTS")) {
        MarbreEnvironnement.IS_GESTION_LOTS = Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("EN_VAL")).equals("1");
      }
    }
  }
  
  /**
   * Met à jour l'information sur l'environnement
   */
  private boolean majEnvironnement() {
    listeDeTravail =
        accesDB2.select("SELECT ENVCLI FROM " + MarbreEnvironnement.BIBLI_SYSTEME + "." + MarbreEnvironnement.TABLE_ENVIRONNEMENT
            + " WHERE ENVVER = '" + MarbreEnvironnement.LETTRE_ENV + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    if (listeDeTravail != null && listeDeTravail.size() == 1) {
      if (listeDeTravail.get(0).isPresentField("ENVCLI")) {
        MarbreEnvironnement.ENVIRONNEMENT = Constantes.convertirObjetEnTexte(listeDeTravail.get(0).getField("ENVCLI"));
        Trace.info("Récupération de l'environnement du WebShop " + MarbreEnvironnement.ENVIRONNEMENT + " pour la lettre "
            + MarbreEnvironnement.LETTRE_ENV + " et la bibliothèque WS " + MarbreEnvironnement.BIBLI_WS + " et la bibliothèque clients "
            + MarbreEnvironnement.BIBLI_CLIENTS);
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
    return true;
  }
  
  /**
   * Gère les informations sur les établissements
   */
  private boolean gererEtablissements() {
    if (MarbreEnvironnement.BIBLI_CLIENTS != null) {
      listeDeTravail = accesDB2.select("SELECT PARETB, SUBSTR(PARZ2, 1, 24) AS LIBETB FROM " + MarbreEnvironnement.BIBLI_CLIENTS
          + ".PGVMPARM WHERE PARTYP = 'DG' AND PARETB <> '' ");
    }
    
    if (listeDeTravail == null || listeDeTravail.size() <= 0) {
      return false;
    }
    
    ArrayList<GenericRecord> listeEtablissements = null;
    ArrayList<GenericRecord> listeMagasins = null;
    
    for (int i = 0; i < listeDeTravail.size(); i++) {
      listeEtablissements = null;
      listeMagasins = null;
      if (listeDeTravail.get(i).isPresentField("PARETB")) {
        listeEtablissements = accesDB2.select("SELECT ETB_ID, ETB_LIB FROM " + MarbreEnvironnement.BIBLI_WS + ".ETABLISS WHERE ETB_ID = '"
            + Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("PARETB")) + "' AND ETB_FM = '"
            + MarbreEnvironnement.BIBLI_CLIENTS + "'  ");
      }
      
      if (listeEtablissements == null) {
        Trace.erreur("[majVariablesEnvironnement()] la liste ETBS de ETABLISS est NULL !!");
        break;
      }
      // Si l'établissement PGVMPARM n'existe pas dans la base ETABLISS
      else if (listeEtablissements.size() <= 0) {
        int nombreResultat = -1;
        Trace.info("[majVariablesEnvironnement()] l'ETb " + Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("PARETB"))
            + " n'existe pas dans ETABLISS");
        
        if (listeDeTravail.get(i).isPresentField("PARETB") && listeDeTravail.get(i).isPresentField("LIBETB")) {
          nombreResultat =
              accesDB2.requete("" + " INSERT INTO " + MarbreEnvironnement.BIBLI_WS + ".ETABLISS (ETB_ID,ETB_LIB,ETB_ACTIF,ETB_FM) "
                  + " VALUES ('" + Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("PARETB")) + "','"
                  + Gestion.traiterCaracteresSpeciauxSQL(Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("LIBETB")))
                  + "','1','" + MarbreEnvironnement.BIBLI_CLIENTS + "')");
        }
        
        Trace.info("[majVariablesEnvironnement()] INSERT d'ETB "
            + Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("PARETB")) + " dans ETABLISS resultat: " + nombreResultat);
      }
      // S'il existe déjà dans ETABLISS
      else if (listeEtablissements.size() == 1) {
        Trace.info("[majVariablesEnvironnement()] l'ETB " + Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("PARETB"))
            + " existe déjà dans ETABLISS");
      }
      
      // TRAITEMENT DES MAGASINS DE CET ETABLISSEMENT
      if (listeDeTravail.get(i).isPresentField("PARETB")) {
        listeMagasins = accesDB2.select("SELECT PARETB AS ETBMG,PARIND AS CODMG,SUBSTR(PARZ2, 1, 24) AS LIBMG FROM "
            + MarbreEnvironnement.BIBLI_CLIENTS + ".PGVMPARM WHERE PARTYP = 'MA' AND PARETB = '"
            + Constantes.convertirObjetEnTexte(listeDeTravail.get(i).getField("PARETB")) + "' ");
      }
      traiterMagasinsEtablissement(listeMagasins);
    }
    
    // Maintenant on met à jour la liste d'établissements de l'environnement à partir d'ETABLISS
    MarbreEnvironnement.LISTE_ETBS = new ArrayList<Etablissement>();
    listeEtablissements = accesDB2.select("SELECT ETB_ID, ETB_LIB FROM " + MarbreEnvironnement.BIBLI_WS
        + ".ETABLISS WHERE ETB_ACTIF = '1' AND ETB_FM = '" + MarbreEnvironnement.BIBLI_CLIENTS + "' ");
    gererListeEtablissements(listeEtablissements);
    if (MarbreEnvironnement.LISTE_ETBS != null && MarbreEnvironnement.ETB_DEFAUT == null) {
      MarbreEnvironnement.ETB_DEFAUT = MarbreEnvironnement.LISTE_ETBS.get(0);
    }
    
    return true;
  }
  
  /**
   * Traite les magasins de l'établissement
   */
  private void traiterMagasinsEtablissement(ArrayList<GenericRecord> pListeMagasins) {
    if (pListeMagasins != null && pListeMagasins.size() > 0) {
      // On a bien listé des MGS pour cet ETB
      ArrayList<GenericRecord> listeMagasins = null;
      for (int j = 0; j < pListeMagasins.size(); j++) {
        listeMagasins = null;
        if (pListeMagasins.get(j).isPresentField("ETBMG") && pListeMagasins.get(j).isPresentField("CODMG")) {
          listeMagasins = accesDB2.select("SELECT MG_COD FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB = '"
              + Constantes.convertirObjetEnTexte(pListeMagasins.get(j).getField("ETBMG")) + "' AND MG_COD = '"
              + Constantes.convertirObjetEnTexte(pListeMagasins.get(j).getField("CODMG")) + "' ");
          if (listeMagasins != null && listeMagasins.size() > 0) {
            Trace.debug("[majVariablesEnvironnement()] le MG " + Constantes.convertirObjetEnTexte(listeMagasins.get(0).getField("MG_COD"))
                + " existe déjà dans MAGASINS");
          }
          else if (listeMagasins != null && listeMagasins.size() == 0) {
            // FAUT QU'ON FASSE NOTRE INSERT IN THE TABLE MAGASINS
            accesDB2.requete("INSERT INTO " + MarbreEnvironnement.BIBLI_WS
                + ".MAGASINS (MG_NAME,US_ETB,MG_COD,MG_ADR,MG_TEL,MG_MESS,MG_HOR,MG_CARTE,MG_EQUI,MG_ACTIF)  " + "VALUES ('"
                + Constantes.convertirObjetEnTexte(pListeMagasins.get(j).getField("LIBMG")) + "','"
                + Constantes.convertirObjetEnTexte(pListeMagasins.get(j).getField("ETBMG")) + "','"
                + Constantes.convertirObjetEnTexte(pListeMagasins.get(j).getField("CODMG")) + "','','','','','','','1')");
          }
          else {
            Trace.erreur("[majVariablesEnvironnement()] la listeMagasinsWS est NULL !!");
          }
        }
      }
    }
  }
  
  /**
   * Gère la liste des établissements
   */
  private void gererListeEtablissements(ArrayList<GenericRecord> pListeEtablissements) {
    for (GenericRecord record : pListeEtablissements) {
      if (record.isPresentField("ETB_ID") && record.isPresentField("ETB_LIB")) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(Constantes.convertirObjetEnTexte(record.getField("ETB_ID")));
        Etablissement etablissement =
            new Etablissement(this, idEtablissement, Constantes.convertirObjetEnTexte(record.getField("ETB_LIB")));
        MarbreEnvironnement.LISTE_ETBS.add(etablissement);
        if (MarbreEnvironnement.ETB_FORCE != null
            && Constantes.normerTexte(MarbreEnvironnement.ETB_FORCE).equals(idEtablissement.getCodeEtablissement())) {
          MarbreEnvironnement.ETB_DEFAUT = etablissement;
        }
      }
    }
  }
  
  /**
   * Met à jour l'information Google Analytics
   */
  private void majGoogleAnalytics() {
    // On met à jour google analytics
    listeDeTravail = accesDB2.select("SELECT GO_ANALY FROM " + MarbreEnvironnement.BIBLI_WS + ".GOOGLE");
    if (listeDeTravail != null && listeDeTravail.size() == 1) {
      if (listeDeTravail.get(0).isPresentField("GO_ANALY")
          && !Constantes.convertirObjetEnTexte(listeDeTravail.get(0).getField("GO_ANALY")).equals("")) {
        MarbreEnvironnement.GOOGLE_ANALYTICS = Constantes.convertirObjetEnTexte(listeDeTravail.get(0).getField("GO_ANALY"));
      }
    }
  }
  
  /**
   * Permet de mettre à jour toutes les variables d'affichage à partir de DB2
   */
  protected boolean majVariablesAffichage() {
    if (accesDB2 == null) {
      return false;
    }
    
    ArrayList<GenericRecord> listeAffichage = null;
    listeAffichage = accesDB2.select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".AFFICHAGE ");
    if (listeAffichage != null && listeAffichage.size() > 0) {
      for (int i = 0; i < listeAffichage.size(); i++) {
        if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_CODE_A")) {
          MarbreAffichage.AFFICHE_CODE_A = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_CODE_B")) {
          MarbreAffichage.AFFICHE_CODE_B = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_CODE_C")) {
          MarbreAffichage.AFFICHE_CODE_C = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_CODE_D")) {
          MarbreAffichage.AFFICHE_CODE_D = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_CODE_R")) {
          MarbreAffichage.AFFICHE_CODE_R = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_CODE_N")) {
          MarbreAffichage.AFFICHE_CODE_N = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHE_PAS_CODE")) {
          MarbreAffichage.AFFICHE_PAS_CODE = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_SANS_STK")) {
          MarbreAffichage.AFFICHE_SANS_STK = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_ETAT_1")) {
          MarbreAffichage.AFFICHAGE_STATUT_DESACTIVE = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_ETAT_2")) {
          MarbreAffichage.AFFICHAGE_STATUT_EPUISE = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_ETAT_3")) {
          MarbreAffichage.AFFICHAGE_STATUT_SYSV = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_ETAT_4")) {
          MarbreAffichage.AFFICHAGE_STATUT_SYSV2 = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_ETAT_5")) {
          MarbreAffichage.AFFICHAGE_STATUT_PREFIN = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
        else if (Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_CLE")).equals("AFFICHAGE_ETAT_6")) {
          MarbreAffichage.AFFICHAGE_STATUT_FIN = Constantes.convertirObjetEnTexte(listeAffichage.get(i).getField("EN_VAL"));
        }
      }
      return true;
    }
    else {
      return false;
    }
  }
  
  /**
   * Mise à jour de la connexion AS400/DB2
   */
  public boolean majConnexionDB2(String pLogin, String pMotDePasse) {
    accesDB2 = null;
    if (Constantes.normerTexte(pLogin).isEmpty() || Constantes.normerTexte(pMotDePasse).isEmpty()) {
      pLogin = MarbreEnvironnement.PROFIL_AS_CLIENTS;
      pMotDePasse = Base64Coder.decodeString(Base64Coder.decodeString(MarbreEnvironnement.MP_AS_CLIENTS));
    }
    
    pLogin = Constantes.normerTexte(pLogin).toUpperCase();
    pMotDePasse = Constantes.normerTexte(pMotDePasse).toUpperCase();
    
    systeme = new SystemeManager(MarbreEnvironnement.ADRESSE_AS400, pLogin, pMotDePasse, true);
    
    if (systeme != null && systeme.getSystem() != null) {
      maconnectionAS = systeme.getDatabaseManager().getConnection();
      if (maconnectionAS != null) {
        accesDB2 = new AccesBDDWS(maconnectionAS, this);
      }
      else {
        Trace.erreur("[majConnexionDB2] ECHEC Connexion DB2 !!");
      }
    }
    else {
      Trace.erreur("[majConnexionDB2] ECHEC Systeme !!");
    }
    
    return accesDB2 != null;
  }
  
  /**
   * On déconnecte tous les jobs sur l'AS400 (QZRCSRVS et QZDASOINIT)
   */
  public void deconnecterAS400() {
    if (systeme != null) {
      systeme.deconnecterJDBC();
      systeme = null;
      Trace.debug("[deconnecterAS400] Systeme déconnecté");
    }
  }
  
  /**
   * Mettre à jour toutes les informations propres à l'utilisateur à partir de la base de données.
   */
  public void setInfos(GenericRecord pRecord, Etablissement pEtb) {
    if (pRecord == null) {
      return;
    }
    
    // Renseigner l'identifiant
    try {
      if (pRecord.isPresentField("US_ID") && pRecord.getField("US_ID") != null) {
        id = Integer.parseInt(Constantes.convertirObjetEnTexte(pRecord.getField("US_ID")));
      }
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
    }
    
    // Renseigner le type d'accès de l'utilisateur
    try {
      
      if (pRecord.isPresentField("US_ACCES") && pRecord.getField("US_ACCES") != null) {
        typeAcces = Integer.parseInt(Constantes.convertirObjetEnTexte(pRecord.getField("US_ACCES")));
      }
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
    }
    
    // Renseigner le login
    if (pRecord.isPresentField("US_LOGIN") && pRecord.getField("US_LOGIN") != null) {
      login = Constantes.convertirObjetEnTexte(pRecord.getField("US_LOGIN"));
    }
    
    // s'il s'agit d'un contact client
    if (pRecord.isPresentField("US_SERIEM") && pRecord.getField("US_SERIEM") != null) {
      idContact = Constantes.convertirObjetEnTexte(pRecord.getField("US_SERIEM"));
    }
    
    // Renseigner le droit de voir le stock disponible
    if (pRecord.isPresentField("RWIN1") && pRecord.getField("RWIN1") != null) {
      stockDisponibleVisible = Constantes.convertirObjetEnTexte(pRecord.getField("RWIN1")).equals("1");
    }
    
    // Renseigner le droit de voir les prix
    if (pRecord.isPresentField("RWIN2") && pRecord.getField("RWIN2") != null) {
      prixVisible = Constantes.convertirObjetEnTexte(pRecord.getField("RWIN2")).equals("1");
    }
    
    // Renseigner le droit de voir les commandes de l'établissement en plus des siennes
    if (pRecord.isPresentField("RWIN4") && pRecord.getField("RWIN4") != null) {
      commandeEtablissementVisible = Constantes.convertirObjetEnTexte(pRecord.getField("RWIN4")).equals("1");
    }
    
    // Renseigner le droit de saisir des devis
    if (pRecord.isPresentField("RWIN5") && pRecord.getField("RWIN5") != null) {
      saisieDevisAutorisee = Constantes.convertirObjetEnTexte(pRecord.getField("RWIN5")).equals("1");
    }
    
    // Renseigner le droit de passer des commandes
    if (pRecord.isPresentField("RWIN3") && pRecord.getField("RWIN3") != null) {
      saisieCommandeAutorisee = Constantes.convertirObjetEnTexte(pRecord.getField("RWIN3")).equals("1");
    }
    
    // Renseigner l'établissement de l'utilisateur
    if (pEtb != null) {
      etablissementEnCours = pEtb;
    }
    
    // Attribuer un accès temporaire en consultation au client :
    // - s'il ne peut pas voir les prix,
    // - ou s'il ne peut ni faire des devis ni commander.
    if (typeAcces == MarbreEnvironnement.ACCES_CLIENT && (!prixVisible || (!saisieDevisAutorisee && !saisieCommandeAutorisee))) {
      typeAcces = MarbreEnvironnement.ACCES_CONSULTATION;
    }
    
    majDesInfosAS400();
    
    // on change de profil on change les menus !!
    recupererMenus();
    
    // On attribue les acces back office
    recupererAccesBackOffice();
    
    // paniers intuitifs
    paniersIntuitifsActifs = GestionCatalogue.recupererPanierIntuitifsActifs(this);
    
    // Afficher les informations de l'utilisateur dans les traces
    Trace.info("Chargement des données de l'utilisateur : " + login);
    if (etablissementEnCours != null) {
      Trace.info("Etablissement : " + etablissementEnCours.getCodeETB());
    }
    else {
      Trace.info("Etablissement : ");
    }
    Trace.info("Identifiant utilisateur : " + id);
    Trace.info("Identifiant contact associé à l'utilisateur : " + idContact);
    Trace.info("Login : " + login);
    Trace.info("Type d'accès : " + typeAcces);
    Trace.info("Droit de voir le stock disponible : " + stockDisponibleVisible);
    Trace.info("Droit de voir les prix : " + prixVisible);
    Trace.info("Droit de voir les commandes de l'établissement : " + commandeEtablissementVisible);
    Trace.info("Droit de saisir des devis : " + saisieDevisAutorisee);
    Trace.info("Droit de saisir des commandes : " + saisieCommandeAutorisee);
  }
  
  /**
   * Mise à jour des informations dispos dans DB2
   */
  public void majDesInfosAS400() {
    // Mise à jour de la bibli
    
    // Finalement on file la bibli clients à tout le monde
    bibli = MarbreEnvironnement.BIBLI_CLIENTS;
    // NE METTRE A JOUR QUE SI ON A UN ETB ASSOCIE ( c'est à dire en mode CLIENT ) ATTENTION IL FAUT UNE METHODE QUI
    // METTE A JOUR TOUT LE BOUZIN APRES SELECTION D'UN ETB
    if (etablissementEnCours != null) {
      mettreAjourDonneesLieesETB(etablissementEnCours.getCodeETB());
    }
    else {
      Trace.erreur("+++++++++ ON A UN UTILISATEUR SANS ETB ASSOCIE (PUBLIC OU AS400)");
      return;
    }
    
    // On va mettre les infos à jour propres au contact Série M -> contact et Client concerné
    if (idContact != null && etablissementEnCours != null && !idContact.equals("") && etablissementEnCours.getCodeETB() != null) {
      if (this.getAccesDB2() != null) {
        ArrayList<GenericRecord> liste = this.getAccesDB2()
            .select(" SELECT RENUM,RECIV,REPAC,RETEL,CLCLI,CLLIV,CLNOM " + " FROM " + bibli + ".PSEMRTEM " + " LEFT JOIN " + bibli
                + ".PSEMRTLM " + " ON RENUM = RLNUMT " + " LEFT JOIN " + bibli + ".PGVMCLIM ON DIGITS(CLCLI)||DIGITS(CLLIV) = RLIND "
                + " WHERE RLETB = '" + etablissementEnCours.getCodeETB() + "' AND RLCOD = 'C' AND RLNUMT = '" + idContact + "' "
                + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
        // Si on trouve bien un profil dans la base de Série M/N
        if (liste != null && liste.size() == 1) {
          if (liste.get(0).isPresentField("RECIV")) {
            civiliteContact = Constantes.convertirObjetEnTexte(liste.get(0).getField("RECIV"));
          }
          if (liste.get(0).isPresentField("REPAC")) {
            nomContact = Constantes.convertirObjetEnTexte(liste.get(0).getField("REPAC"));
          }
          if (liste.get(0).isPresentField("RETEL")) {
            telContact = Constantes.convertirObjetEnTexte(liste.get(0).getField("RETEL"));
          }
          
          Trace.debug("[majDesInfosAS400] civiliteContact: " + civiliteContact);
          Trace.debug("[majDesInfosAS400] nomContact: " + nomContact);
          
          // Si on a bien un client associé à ce contact on lui attribue
          if (liste.get(0).isPresentField("CLCLI") && liste.get(0).isPresentField("CLLIV") && liste.get(0).isPresentField("CLNOM")) {
            client = new Client(this, etablissementEnCours, Constantes.convertirObjetEnTexte(liste.get(0).getField("CLCLI")),
                Constantes.convertirObjetEnTexte(liste.get(0).getField("CLLIV")));
          }
        }
      }
    }
    // RECUP DU CODE REPRESENTANT SI ON EN EST UN
    else if (etablissementEnCours != null && login != null && typeAcces == MarbreEnvironnement.ACCES_REPRES_WS && maconnectionAS != null
        && US_REPR == null) {
      if (mesSecurites == null) {
        mesSecurites = new PgvmsecmManager(maconnectionAS);
      }
      
      mesSecurites.setLibrary(this.getBibli());
      
      ExtendRecord record = mesSecurites.getRecordsbyUSERandETB(login, etablissementEnCours.getIdEtablissement());
      if (record != null && record.getField("SERPUS") != null
          && !Constantes.convertirObjetEnTexte(record.getField("SERPUS")).equals("")) {
        US_REPR = Constantes.convertirObjetEnTexte(record.getField("SERPUS"));
        Trace.debug("Je récupère un code REPRESENTANT: " + US_REPR);
        
        ArrayList<GenericRecord> liste = this.getAccesDB2()
            .select("SELECT RPCIV,RPNOM FROM " + bibli + ".PGVMREPM " + " WHERE RPETB = '" + etablissementEnCours.getCodeETB()
                + "' AND RPREP = '" + US_REPR + "' " + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
        // Si on trouve bien un profil dans la base de Série M/N
        if (liste != null && liste.size() == 1) {
          if (liste.get(0).isPresentField("RPCIV")) {
            civiliteContact = Constantes.convertirObjetEnTexte(liste.get(0).getField("RPCIV"));
          }
          Trace.debug("[majDesInfosAS400] REPR civiliteContact: " + civiliteContact);
          if (liste.get(0).isPresentField("RPNOM")) {
            nomContact = Constantes.convertirObjetEnTexte(liste.get(0).getField("RPNOM"));
          }
          Trace.debug("[majDesInfosAS400] REPR nomContact: " + nomContact);
        }
      }
    }
  }
  
  /**
   * Met à jour les variables stockées par l'utilisateur propres à la
   * recherche articles dans le catalogue
   */
  public void setInfosRechercheArticles(String expression, String groupe, String famille, String frs, String favoris, String historique,
      String consulte, String panierInt, String indicePagination) {
    if (indicePagination == null) {
      if (expression != null) {
        derniereExpression = Constantes.normerTexte(expression).toUpperCase();
      }
      else {
        derniereExpression = expression;
      }
      // On ne met pas à jour le groupe si on change de famille ou sous famille
      dernierGroupeArticles = groupe;
      derniereFamilleArticles = famille;
      if (frs != null && frs.equals("")) {
        frs = null;
      }
      dernierFiltreFournisseur = frs;
      derniersFavoris = favoris;
      dernierHistorique = historique;
      derniersConsultes = consulte;
      dernierPaniersIntuitifs = panierInt;
    }
  }
  
  /**
   * Controler l'état de la gestion de tarif et l'initialiser si nécessaire
   **/
  public boolean controleDuplicationDevis() {
    if (duplicationDevis == null) {
      duplicationDevis = new DuplicDevis(this.getSysteme(), MarbreEnvironnement.LETTRE_ENV, MarbreEnvironnement.BIBLI_CLIENTS);
    }
    return duplicationDevis.init();
  }
  
  /**
   * Controler l'état de la gestion de tarif et l'initialiser si nécessaire
   */
  public boolean controleGestionTarif() {
    if (gestionTarif == null) {
      
      // Initialisation de l'objet BibliothèqueEnvironnement
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(MarbreEnvironnement.BIBLI_WS);
      Bibliotheque bibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.NON_DEFINI);
      
      gestionTarif = new GestionTarif(this.getSysteme(), MarbreEnvironnement.LETTRE_ENV, MarbreEnvironnement.BIBLI_CLIENTS, bibliotheque);
    }
    
    return gestionTarif.init();
  }
  
  /**
   * Controler l'état de la gestion de tarif et l'initialiser si nécessaire
   */
  public boolean controleGestionCommandes() {
    if (gestionCommandes == null) {
      gestionCommandes = new GestionCommandes(this.getSysteme(), MarbreEnvironnement.LETTRE_ENV, MarbreEnvironnement.BIBLI_CLIENTS);
    }
    
    return gestionCommandes.init();
  }
  
  /**
   * Controler l'état de la gestion des stocks et l'initialiser si nécessaire
   */
  public boolean controleGestionStock() {
    boolean isOk = false;
    if (gestionStock == null) {
      gestionStock = new GestionStock(this.getSysteme(), MarbreEnvironnement.LETTRE_ENV, MarbreEnvironnement.BIBLI_CLIENTS);
      isOk = gestionStock.init();
    }
    else {
      isOk = true;
    }
    
    return isOk;
  }
  
  /**
   * Faire un RAZ du panier de l'utilisateur
   */
  public boolean razPanier() {
    // On vide le panier courant
    if (getMonPanier() != null) {
      getMonPanier().videPanier(true);
    }
    // On en recrée un autre
    this.monPanier = new Panier(this);
    
    return true;
  }
  
  /**
   * Retourner la liste des footers (bas de page) de l'utilisateur dans le language paramétré
   */
  private ArrayList<GenericRecord> retournerFooters(String language) {
    if (accesDB2 == null) {
      return null;
    }
    
    return this.getAccesDB2().select("SELECT * FROM " + MarbreEnvironnement.BIBLI_WS + ".FOOTER WHERE FT_LANG = '" + language + "'");
  }
  
  /**
   * Mettre à jour la liste des footers (bas de page) de l'utilisateur dans le language paramétré
   */
  public void majFooters(String language) {
    if (language == null) {
      return;
    }
    
    mesFooters = retournerFooters(language);
  }
  
  /**
   * mettre à jour la page courante de l'utilisateur
   */
  public void majPageEnCours(String nomPage, HttpServletRequest request) {
    pageEnCours = nomPage;
    
    if (nomPage != null && !nomPage.equals("clients") && !nomPage.equals("monPanier")) {
      parametresEnCours = nomPage;
      Enumeration<String> parametres = request.getParameterNames();
      String parametre = null;
      while (parametres.hasMoreElements()) {
        if (parametre == null) {
          parametresEnCours += "?";
        }
        else {
          parametresEnCours += "&";
        }
        
        parametre = Constantes.normerTexte(parametres.nextElement());
        parametre += "=" + request.getParameter(parametre);
        
        parametresEnCours += parametre;
      }
    }
    else if (nomPage != null && nomPage.equals("monPanier")) {
      parametresEnCours = "catalogue";
    }
  }
  
  /**
   * Recupérer les menus en fonction de l'accès de l'utilisateur
   */
  public void recupererMenus() {
    if (accesDB2 == null) {
      return;
    }
    
    String specialCatalogue = " ";
    if (MarbreEnvironnement.CATALOGUE_MODE_PUBLIC) {
      specialCatalogue = " OR MN_NAME= 'Catalogue' ";
    }
    
    mesMenus = accesDB2.select("SELECT MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES FROM " + MarbreEnvironnement.BIBLI_WS
        + ".MENU WHERE (US_ACC <= " + typeAcces + " AND " + typeAcces + " <= MN_LIM) " + specialCatalogue + " ORDER BY MN_ORDRE ");
  }
  
  /**
   * Recupérer les acces back office de l'utilisateur en fonction de l'accès de l'utilisateur
   */
  private void recupererAccesBackOffice() {
    if (accesDB2 == null) {
      return;
    }
    
    if (typeAcces == MarbreEnvironnement.ACCES_ADMIN_WS) {
      listeAccesBackOffice = accesDB2.select("SELECT BTN_NAME,BTN_LINK FROM " + MarbreEnvironnement.BIBLI_WS + ".ACCUEILBO ");
    }
    else if (typeAcces > MarbreEnvironnement.ACCES_CLIENT) {
      listeAccesBackOffice = accesDB2.select("SELECT BTN_NAME,BTN_LINK FROM " + MarbreEnvironnement.BIBLI_WS + ".ACCUEILBO JOIN "
          + MarbreEnvironnement.BIBLI_WS + ".ACCESBO a ON a.AB_ID = BTN_ID WHERE AB_ACC = '" + typeAcces + "'");
    }
    else {
      listeAccesBackOffice = null;
    }
  }
  
  /**
   * Récuperer un seul enregistrement de la liste de Record
   */
  public void recupererUnRecordTravail(ArrayList<GenericRecord> liste) {
    if (liste != null && liste.size() > 0) {
      this.recordTravail = liste.get(0);
    }
    else {
      this.recordTravail = null;
    }
  }
  
  /**
   * On met à jour toutes les variables d'environnement qui dépendaient d'un choix d'établissement
   */
  public void mettreAjourDonneesLieesETB(String etb) {
    if (etb == null) {
      return;
    }
    
    // Récup de la devise de la DG
    ArrayList<GenericRecord> rcd = this.getAccesDB2()
        .select("SELECT SUBSTR(PARZ2, 121, 3) AS DEV FROM " + bibli + ".PGVMPARM " + " WHERE PARETB ='" + etb + "' AND PARTYP = 'DG'");
    if (rcd != null && rcd.size() > 0) {
      US_DEV = Constantes.convertirObjetEnTexte(rcd.get(0).getField("DEV"));
    }
    
    // Récup du magasin siége
    if (etablissementEnCours.getMagasin_siege() != null) {
      magasinSiege = new Magasin(this, etablissementEnCours.getMagasin_siege());
    }
    
    visuStock = etablissementEnCours.isVisu_auto_des_stocks();
  }
  
  /**
   * permet de savoir si cet utilisateur peut accéder ou non au catalogue
   */
  public boolean peutAccederAuCatalogue() {
    return (getTypeAcces() >= MarbreEnvironnement.ACCES_CLIENT || MarbreEnvironnement.CATALOGUE_MODE_PUBLIC);
  }
  
  /**
   * Fournie le gestionnaire de requête
   * .
   * 
   * @return
   */
  public QueryManager getQueryManager() {
    if (getSysteme() == null) {
      throw new MessageErreurException("L'information sur le système n'est pas valide");
    }
    
    if (getSysteme().getDatabaseManager() == null || getSysteme().getDatabaseManager().getConnection() == null) {
      throw new MessageErreurException("Le paramètre de connexion n'est pas valide.");
    }
    QueryManager queryManager = new QueryManager(getSysteme().getDatabaseManager().getConnection());
    queryManager.setLibrary(MarbreEnvironnement.BIBLI_CLIENTS);
    
    return queryManager;
  }
  
  /**
   * Crée le mail à envoyer.
   * 
   * @param pDestinataire Adresse mail du destinataire
   * @param pParametreMail Liste des paramètres dont le mail a besoin
   * @param pEnumTypeMail Type de mail à envoyer
   */
  public void envoyerMail(String pDestinataire, ParametreMail pParametreMail, EnumTypeMail pEnumTypeMail) {
    IdMail idMail = ServiceMail.creerMail(this, getQueryManager(), pDestinataire, pEnumTypeMail);
    Mail mail = ServiceMail.chargerMail(getQueryManager(), idMail);
    mail.setListeVariableMail(ListeVariableMail.preparerListeVariableMail(idMail, pParametreMail.retournerParametre()));
    ServiceMail.initialiserVariableMail(getQueryManager(), mail);
  }
  
  // +++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++ //
  
  /**
   * Identifiant unique de l'utilisateur dans la base de données Webshop.
   * 
   * Cette information est stockée dans le champ USERW.US_ID de la base de données Webshop.
   * L'identifiant '1' est réservé aux invités.
   */
  public int getIdentifiant() {
    return id;
  }
  
  /**
   * Identifiant du contact associé à l'utilisateur.
   * 
   * Cette information est stockée dans le champ USERW.US_SERIEM de la base de données Webshop.
   * Cela correspond au numéro de contact RLNUMT dans la table des liens contacts PSEMRTLM. Ce numéro permet de faire le lien avec la
   * table PSEMRTEM (champ RENUM).
   */
  public String getIdContact() {
    return idContact;
  }
  
  /**
   * Modifier l'identifiant du contact associé à l'utilisateur.
   */
  public void setIdContact(String uS_SERIEM) {
    idContact = uS_SERIEM;
  }
  
  /**
   * Compte de l'utilisateur.
   * 
   * Cette information est stockée dans le champ USERW.US_LOGIN de la base de données Webshop.
   * Cela peut-être un profil pour un administrateur ou un gestionnaire (RIDEVAA par exemple) ou une adresse mail pour un contact client
   * (aa@resolution-informatique.com par exemple).
   */
  public String getLogin() {
    return login;
  }
  
  /**
   * Modifier le compte de l'utilisateur.
   */
  public void setLogin(String pLogin) {
    login = pLogin;
  }
  
  /**
   * Type d'accès au Webshop dont bénéficie l'utilisateur.
   *
   * Cette information est stockée dans le champ USERW.US_ACCES de la base de données Webshop.
   * La liste ci-dessous décrit les cas possibles d'accès utilisateur. La liste n'est pas tout à fait exhaustive. En gros, cette
   * information permet au Webvshop de savoir ce qu'il doit afficher et permettre à l'utilisateur connecté.
   * 
   * Cas d'erreur :
   * -2 = ACCES_PAS_DB2 = L'accès à la base de données ne focntionne pas correctement.
   * -1 = ACCES_NON_DEPLOYE = La récupération des variables d'environnements ne fonctionne pas correctement
   *
   * Accès public :
   * 1 = ACCES_PUBLIC
   *
   * Accès en cours de saisie du compte :
   * 2 = ACCES_INEXISTANT = Le profil n'est pas autorisé à utiliser le Webshop.
   * 3 = ACCES_AT_MDP = Accès en attente de saisie d'un mot de passe.
   * 4 = ACCES_ER_CONF_MDP = Erreur de confirmation de mot de passe lors de l'inscription de l'utilisateur.
   * 6 = ACCES_AT_VALID = Profil en attente de validation
   * 8 = ACCES_ER_MDP = Si les informations d'identification ne sont pas les bonnes (login ou mot de passe invalide).
   * 10 = ACCES_REFUSE = Accès interdit pour cet utilisateur.
   * 
   * Gestion des logins multiples :
   * 16 = ACCES_MULTIPLE_CONNEX = L'utilisateur existe en double dans la table des utilisateurs.
   * 17 = ACCES_MULTIPLE_INSCRI = Si plusieurs contact avec le même login sont présents est autorisés.
   *
   * Accès clients autorisés :
   * 20 = ACCES_CLIENT = Accès client standard.
   * 21 = ACCES_CONSULTATION = Accès en consultation seulement (pas de devis ou de commande possible).
   * 25 = ACCES_REPRES_WS = Accès pour un représentant.
   * 30 = ACCES_RESPON_WS = Accès responsable Webshop.
   * 40 = ACCES_ADMIN_WS = Accès pour un adminsitrateur.
   */
  public int getTypeAcces() {
    return typeAcces;
  }
  
  /**
   * Modifier le type d'accès au Webshop de l'utilisateur.
   */
  public void setTypeAcces(int pTypeAcces) {
    typeAcces = pTypeAcces;
  }
  
  /**
   * Droit de voir le stock disponible.
   *
   * Cette information est stockée dans le champ PSEMRTWM.RWIN1 de la base de données.
   */
  public boolean isStockDisponibleVisible() {
    return stockDisponibleVisible;
  }
  
  /**
   * Modifier le droit de voir le stock disponible.
   */
  public void setStockDisponibleVisible(boolean pStockDisponibleVisible) {
    stockDisponibleVisible = pStockDisponibleVisible;
  }
  
  /**
   * Droit de voir les prix.
   * 
   * Cette information est stockée dans le champ PSEMRTWM.RWIN2 de la base de données.
   */
  public boolean isPrixVisible() {
    return prixVisible;
  }
  
  /**
   * Modifier le droit de voir les prix.
   */
  public void setPrixVisible(boolean pPrixVisible) {
    prixVisible = pPrixVisible;
  }
  
  /**
   * Droit de voir les commandes de l'établissement autres que les siennes.
   * 
   * Cette information est stockée dans le champ PSEMRTWM.RWIN4 de la base de données.
   */
  public boolean isCommandesEtablissementVisible() {
    return commandeEtablissementVisible;
  }
  
  /**
   * Modifier le droit de voir les commandes de l'établissement autres que les siennes.
   * 
   * Cette information est stockée dans le champ PSEMRTWM.RWIN5 de la base de données.
   */
  public void setCommandesEtablissementVisible(boolean pCommandesEtablissementVisible) {
    commandeEtablissementVisible = pCommandesEtablissementVisible;
  }
  
  /**
   * Droit de saisir des devis.
   */
  public boolean isSaisieDevisAutorisee() {
    return saisieDevisAutorisee;
  }
  
  /**
   * Modifier le droit de saisir des devis.
   * 
   * Cette information est stockée dans le champ PSEMRTWM.RWIN3 de la base de données.
   */
  public void setSaisieDevisAutorisee(boolean pSaisieDevisAutorisee) {
    saisieDevisAutorisee = pSaisieDevisAutorisee;
  }
  
  /**
   * Droit de saisir des commandes.
   */
  public boolean isSaisieCommandeAutorisee() {
    return saisieCommandeAutorisee;
  }
  
  /**
   * Modifier le droit de saisir des commandes.
   */
  public void setSaisieCommandeAutorisee(boolean pSaisieCommandeAutorisee) {
    saisieCommandeAutorisee = pSaisieCommandeAutorisee;
  }
  
  public GestionCommandes getGestionCommandes() {
    return gestionCommandes;
  }
  
  public void setGestionCommandes(GestionCommandes gestionCommandes) {
    this.gestionCommandes = gestionCommandes;
  }
  
  public ArrayList<GenericRecord> getMesFooters() {
    return mesFooters;
  }
  
  public void setMesFooters(ArrayList<GenericRecord> mesFooters) {
    this.mesFooters = mesFooters;
  }
  
  public AccesBDDWS getAccesDB2() {
    return accesDB2;
  }
  
  public void setAccesDB2(AccesBDDWS accesDB2) {
    this.accesDB2 = accesDB2;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticle() {
    return derniereListeArticle;
  }
  
  public void setDerniereListeArticle(ArrayList<GenericRecord> derniereListeArticle) {
    this.derniereListeArticle = derniereListeArticle;
  }
  
  public String getDernierTypeBon() {
    return dernierTypeBon;
  }
  
  public void setDernierTypeBon(String dernierTypeBon) {
    this.dernierTypeBon = dernierTypeBon;
  }
  
  public String getUS_DEV() {
    return US_DEV;
  }
  
  public void setUS_DEV(String uS_DEV) {
    US_DEV = uS_DEV;
  }
  
  public int getTentativesConnexions() {
    return tentativesConnexions;
  }
  
  public void setTentativesConnexions(int tentativesConnexions) {
    this.tentativesConnexions = tentativesConnexions;
  }
  
  public String getNomContact() {
    return nomContact;
  }
  
  public void setNomContact(String nomContact) {
    this.nomContact = nomContact;
  }
  
  public String getBibli() {
    return bibli;
  }
  
  public void setBibli(String bibli) {
    this.bibli = bibli;
  }
  
  public SystemeManager getSysteme() {
    return systeme;
  }
  
  public void setSysteme(SystemeManager systeme) {
    this.systeme = systeme;
  }
  
  public Connection getMaconnectionAS() {
    return maconnectionAS;
  }
  
  public void setMaconnectionAS(Connection maconnectionAS) {
    this.maconnectionAS = maconnectionAS;
  }
  
  public String getDerniereRequete() {
    return derniereRequete;
  }
  
  public void setDerniereRequete(String derniere) {
    derniereRequete = derniere;
  }
  
  public String getDernierFiltreFournisseur() {
    return dernierFiltreFournisseur;
  }
  
  public void setDernierFiltreFournisseur(String dernierFiltreFournisseur) {
    this.dernierFiltreFournisseur = dernierFiltreFournisseur;
  }
  
  public HashMap<String, String[]> getDerniereListeFournisseur() {
    return derniereListeFournisseur;
  }
  
  public void setDerniereListeFournisseur(HashMap<String, String[]> derniereListeFournisseur) {
    this.derniereListeFournisseur = derniereListeFournisseur;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleMoteur() {
    return derniereListeArticleMoteur;
  }
  
  public void setDerniereListeArticleMoteur(ArrayList<GenericRecord> derniereListeArticleMoteur) {
    this.derniereListeArticleMoteur = derniereListeArticleMoteur;
  }
  
  public String getCiviliteContact() {
    return civiliteContact;
  }
  
  public void setCiviliteContact(String civiliteContact) {
    this.civiliteContact = civiliteContact;
  }
  
  public GestionTarif getGestionTarif() {
    return gestionTarif;
  }
  
  public GestionStock getGestionStock() {
    return gestionStock;
  }
  
  public String getDerniereExpression() {
    return derniereExpression;
  }
  
  public void setDerniereExpression(String derniereExpression) {
    this.derniereExpression = derniereExpression;
  }
  
  public String getDernierGroupeArticles() {
    return dernierGroupeArticles;
  }
  
  public void setDernierGroupeArticles(String dernierGroupeArticles) {
    this.dernierGroupeArticles = dernierGroupeArticles;
  }
  
  public String getDerniereFamilleArticles() {
    return derniereFamilleArticles;
  }
  
  public void setDerniereFamilleArticles(String derniereFamilleArticles) {
    this.derniereFamilleArticles = derniereFamilleArticles;
  }
  
  public Panier getMonPanier() {
    return monPanier;
  }
  
  public void setMonPanier(Panier monPanier) {
    this.monPanier = monPanier;
  }
  
  public String getIndiceDebut() {
    return indiceDebut;
  }
  
  public void setIndiceDebut(String indiceDebut) {
    this.indiceDebut = indiceDebut;
  }
  
  public void setGestionTarif(GestionTarif gestionTarif) {
    this.gestionTarif = gestionTarif;
  }
  
  public void setGestionStock(GestionStock gestionStock) {
    this.gestionStock = gestionStock;
  }
  
  public boolean isVisuStock() {
    return visuStock;
  }
  
  public void setVisuStock(boolean visuStock) {
    this.visuStock = visuStock;
  }
  
  public Magasin getMagasinSiege() {
    return magasinSiege;
  }
  
  public String getSessionEnCours() {
    return sessionEnCours;
  }
  
  public void setSessionEnCours(String sessionEnCours) {
    this.sessionEnCours = sessionEnCours;
  }
  
  public String getLanguage() {
    return language;
  }
  
  public void setLanguage(String language) {
    this.language = language;
  }
  
  public Traduction getTraduction() {
    return traduction;
  }
  
  public String getDerniersFavoris() {
    return derniersFavoris;
  }
  
  public void setDerniersFavoris(String derniersFavoris) {
    this.derniersFavoris = derniersFavoris;
  }
  
  public String getDernierHistorique() {
    return dernierHistorique;
  }
  
  public void setDernierHistorique(String dernierHistorique) {
    this.dernierHistorique = dernierHistorique;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleFavoris() {
    return derniereListeArticleFavoris;
  }
  
  public void setDerniereListeArticleFavoris(ArrayList<GenericRecord> derniereListeArticleFavoris) {
    this.derniereListeArticleFavoris = derniereListeArticleFavoris;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleHistorique() {
    return derniereListeArticleHistorique;
  }
  
  public void setDerniereListeArticleHistorique(ArrayList<GenericRecord> derniereListeArticleHistorique) {
    this.derniereListeArticleHistorique = derniereListeArticleHistorique;
  }
  
  public ArrayList<GenericRecord> getDerniereListeArticleConsultee() {
    return derniereListeArticleConsultee;
  }
  
  public void setDerniereListeArticleConsultee(ArrayList<GenericRecord> derniereListeArticleConsultee) {
    this.derniereListeArticleConsultee = derniereListeArticleConsultee;
  }
  
  public String getDerniersConsultes() {
    return derniersConsultes;
  }
  
  public void setDerniersConsultes(String derniersConsultes) {
    this.derniersConsultes = derniersConsultes;
  }
  
  public Client getClient() {
    return client;
  }
  
  public void setClient(Client client) {
    this.client = client;
  }
  
  public String getUS_REPR() {
    return US_REPR;
  }
  
  public void setUS_REPR(String uS_REPR) {
    US_REPR = uS_REPR;
  }
  
  public String getTelContact() {
    return telContact;
  }
  
  public void setTelContact(String telContact) {
    this.telContact = telContact;
  }
  
  public String getParametresEnCours() {
    return parametresEnCours;
  }
  
  public void setParametresEnCours(String parametresEnCours) {
    this.parametresEnCours = parametresEnCours;
  }
  
  public String getPageEnCours() {
    return pageEnCours;
  }
  
  public void setPageEnCours(String pageEnCours) {
    this.pageEnCours = pageEnCours;
  }
  
  public ArrayList<GenericRecord> getListeDeTravail() {
    return listeDeTravail;
  }
  
  public void setListeDeTravail(ArrayList<GenericRecord> listeDeTravail) {
    this.listeDeTravail = listeDeTravail;
  }
  
  public ArrayList<GenericRecord> getMesMenus() {
    return mesMenus;
  }
  
  public void setMesMenus(ArrayList<GenericRecord> mesMenus) {
    this.mesMenus = mesMenus;
  }
  
  public GenericRecord getRecordTravail() {
    return recordTravail;
  }
  
  public void setRecordTravail(GenericRecord recordTravail) {
    this.recordTravail = recordTravail;
  }
  
  public PgvmsecmManager getMesSecurites() {
    return mesSecurites;
  }
  
  public void setMesSecurites(PgvmsecmManager mesSecurites) {
    this.mesSecurites = mesSecurites;
  }
  
  public ArrayList<GenericRecord> getDerniereListePagination() {
    return derniereListePagination;
  }
  
  public void setDerniereListePagination(ArrayList<GenericRecord> derniereListePagination) {
    this.derniereListePagination = derniereListePagination;
  }
  
  public ArrayList<GenericRecord> getDerniereListePartiellePagination() {
    return derniereListePartiellePagination;
  }
  
  public void setDerniereListePartiellePagination(ArrayList<GenericRecord> derniereListePartiellePagination) {
    this.derniereListePartiellePagination = derniereListePartiellePagination;
  }
  
  public boolean isMobilitePlus() {
    return isMobilitePlus;
  }
  
  public void setMobilitePlus(boolean isMobilitePlus) {
    this.isMobilitePlus = isMobilitePlus;
  }
  
  public Etablissement getEtablissementEnCours() {
    return etablissementEnCours;
  }
  
  public void setEtablissementEnCours(Etablissement pEtablissement) {
    etablissementEnCours = pEtablissement;
  }
  
  public ArrayList<GenericRecord> getListeAccesBackOffice() {
    return listeAccesBackOffice;
  }
  
  public DuplicDevis getDuplicationDevis() {
    return duplicationDevis;
  }
  
  public String getTypeDonneesEncours() {
    return typeDonneesEncours;
  }
  
  public void setTypeDonneesEncours(String typeDonneesEncours) {
    this.typeDonneesEncours = typeDonneesEncours;
  }
  
  public Filtres getFiltres() {
    return filtres;
  }
  
  public PanierIntuitif getPanierIntuitif() {
    return panierIntuitif;
  }
  
  public void setPanierIntuitif(PanierIntuitif panierIntuitif) {
    this.panierIntuitif = panierIntuitif;
  }
  
  public ArrayList<GenericRecord> getDerniereListePanierIntuitif() {
    return derniereListePanierIntuitif;
  }
  
  public void setDerniereListePanierIntuitif(ArrayList<GenericRecord> derniereListePanierIntuitif) {
    this.derniereListePanierIntuitif = derniereListePanierIntuitif;
  }
  
  public String getDernierPaniersIntuitifs() {
    return dernierPaniersIntuitifs;
  }
  
  public void setDernierPaniersIntuitifs(String dernierPaniersIntuitifs) {
    this.dernierPaniersIntuitifs = dernierPaniersIntuitifs;
  }
  
  public ArrayList<GenericRecord> getPaniersIntuitifsActifs() {
    return paniersIntuitifsActifs;
  }
  
  public void setPaniersIntuitifsActifs(ArrayList<GenericRecord> paniersIntuitifsActifs) {
    this.paniersIntuitifsActifs = paniersIntuitifsActifs;
  }
  
  public ArrayList<GenericRecord> getListePromotionsActives() {
    return listePromotionsActives;
  }
}
