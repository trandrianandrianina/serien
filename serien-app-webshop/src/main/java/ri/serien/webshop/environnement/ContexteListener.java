/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.io.File;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.outils.Installation;

/**
 * Ecouter les changements d'état du ServletContext, qui représente l'application Web.
 * 
 * Le conteneur Web avertit l'application de la création du ServletContext grâce à la méthode contextInitialized et de la destruction
 * du ServletContext grâce à la méthode contextDestroyed. Donc un ServletContextListener est un moyen de réaliser des traitements au
 * moment du lancement de l'application Web et/ou au moment de son arrêt.
 */
public class ContexteListener implements ServletContextListener {
  // Variables
  private ServletContext servletContext = null;
  
  /**
   * Appelée par le serveur d'application lors du lancement du Webshop.
   */
  @Override
  public void contextInitialized(ServletContextEvent pServletContextEvent) {
    try {
      // Mémoriser le ServletContext (pour obtenir des informations sur le contexte d'exécution de la servlet)
      servletContext = pServletContextEvent.getServletContext();
      
      // Charger les paramètres du fichier "context.xml"
      chargerParametresContexte();
      
      // Vérifier la présence des paramètres obligatoires pour le fichier trace
      if (MarbreEnvironnement.DOSSIER_RACINE_PRIVATE == null || MarbreEnvironnement.DOSSIER_RACINE_PRIVATE.isEmpty()) {
        Trace.erreur("Le paramètre DOSSIER_RACINE_PRIVATE doit être renseigné dans le fichier context.xml.");
        throw new RuntimeException();
      }
      
      // Tracer le démarrage du logiciel
      // Après le chargement des paramètres car c'est grâce à eux que l'on connaît l'emplacement du fichier de trace
      Trace.demarrerLogiciel(MarbreEnvironnement.DOSSIER_RACINE_PRIVATE + MarbreEnvironnement.DOSSIER_WS + File.separatorChar, "webshop",
          "WebShop");
      
      // Vérifier le statut d'installation du Webshop
      Installation installation = new Installation();
      switch (installation.controllerInstallation()) {
        case Installation.INSTALL:
          // Tracer le début de l'installation du WebShop
          Trace.titre("INSTALLATION DU WEBSHOP");
          
          // Effectuer l'installation initiale du Webshop
          installation.installation();
          break;
        
        case Installation.UPDATE:
          // Tracer le début de la mise à jour du WebShop
          Trace.titre("MISE A JOUR DU WEBSHOP");
          
          // Mettre à jour le Webshop
          installation.mettreAJour();
          break;
      }
      
      // Mettre à jour la base de données si nécessaire
      UtilisateurWebshop utilisateur = new UtilisateurWebshop();
      if (utilisateur.getAccesDB2() != null) {
        // Tracer le début de la mise à jour de la base de données Webshop
        Trace.titre("MISE A JOUR DE LA BASE DE DONNEES WEBSHOP");
        
        // Mettre à jour la base de données Webshop (?WEB)
        MajDB2 majDB2 = new MajDB2(utilisateur);
        majDB2.mettreAJourDB2();
      }
      
      // Tracer la fin du démarrage du Webshop
      Trace.titre("FIN DU DEMARRAGE DU WEBSHOP");
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "Erreur lors du démarrage du Webshop");
      
      // Remonter l'erreur au serveur d'application
      throw new RuntimeException(e);
    }
  }
  
  /**
   * Appelée par le serveur d'application lors de l'arrêt du Webshop.
   */
  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    // Tracer le début de l'arrêt du Webshop
    Trace.titre("DEBUT DE L'ARRET DU WEBSHOP");
    
    try {
      Enumeration<Driver> drivers = null;
      
      drivers = DriverManager.getDrivers();
      while (drivers.hasMoreElements()) {
        DriverManager.deregisterDriver(drivers.nextElement());
      }
      
      // Tracer l'arrêt du logiciel
      Trace.arreterLogiciel();
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "Erreur lors de l'arrêt du Webshop");
      
      // Remonter l'erreur au serveur d'application
      throw new RuntimeException(e);
    }
  }
  
  /**
   * Lire les paramètres du fichier context.xml pour initialiser certaines variables d'environnement.
   */
  private void chargerParametresContexte() {
    boolean refresh = false;
    
    String chaine = servletContext.getInitParameter("ADRESSE_AS400");
    if (chaine != null) {
      MarbreEnvironnement.ADRESSE_AS400 = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("PROFIL_AS_ANONYME");
    if (chaine != null) {
      MarbreEnvironnement.PROFIL_AS_ANONYME = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("MP_AS_ANONYME");
    if (chaine != null) {
      MarbreEnvironnement.MP_AS_ANONYME = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("DOSSIER_RACINE_TOMCAT");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_RACINE_TOMCAT = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("DOSSIER_RACINE_PUBLIC");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_RACINE_PUBLIC = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("DOSSIER_RACINE_PRIVATE");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_RACINE_PRIVATE = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("DOSSIER_RACINE_XWEBSHOP");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP = chaine;
      if (MarbreEnvironnement.versionInstallee == null && MarbreEnvironnement.DOSSIER_RACINE_XWEBSHOP != null) {
        MarbreEnvironnement.versionInstallee = Installation.recupererVersionInstallee();
      }
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("DOSSIER_SRC");
    if (chaine != null) {
      MarbreEnvironnement.DOSSIER_SRC = chaine;
      refresh = true;
    }
    
    chaine = servletContext.getInitParameter("LIBELLE_SERVEUR");
    if (chaine != null) {
      MarbreEnvironnement.LIBELLE_SERVEUR = chaine;
    }
    else {
      MarbreEnvironnement.LIBELLE_SERVEUR = "";
    }
    
    chaine = servletContext.getInitParameter("IS_ECLIPSE");
    if (chaine != null) {
      MarbreEnvironnement.isEclipse = chaine.trim().equalsIgnoreCase("true");
    }
    
    chaine = servletContext.getInitParameter("LIB_RACINE");
    if (chaine != null) {
      MarbreEnvironnement.BIBLI_WS = chaine;
    }
    
    // Relaculer certaines variables d'environnement si des paramètres ont été lus
    if (refresh) {
      MarbreEnvironnement.refreshValue();
    }
  }
}
