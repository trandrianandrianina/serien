/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.environnement;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import ri.serien.webshop.constantes.MarbreEnvironnement;

/**
 * Permet de gérer l'ensemble des sessions créées sur le web shop
 */
public class SessionListener implements HttpSessionListener {
  public static ArrayList<HttpSession> listeSessions = null;
  
  // liste des IP bloquees
  public static HashMap<String, Integer> listeIpBloquees = null;
  
  // permet de gérer la sécurité des requêtes rentrantes
  public static GestionRequetes gestionDesRequetes = null;
  
  /**
   * lancée à la création d'une session
   */
  @Override
  public void sessionCreated(HttpSessionEvent moteur) {
    // Créer un utilisateur si il n'existe pas
    if (moteur.getSession().getAttribute("utilisateur") == null) {
      moteur.getSession().setAttribute("utilisateur", new UtilisateurWebshop(moteur.getSession().getId()));
    }
    
    if ((UtilisateurWebshop) moteur.getSession().getAttribute("utilisateur") != null) {
      // Attribuer un temps max d'inactivité à la session
      moteur.getSession().setMaxInactiveInterval(MarbreEnvironnement.TEMPS_SESSION_INACTIVE);
      // on crée la liste de sessions si elle est nulle
      if (listeSessions == null) {
        listeSessions = new ArrayList<HttpSession>();
      }
      listeSessions.add(moteur.getSession());
    }
    else {
      moteur.getSession().invalidate();
    }
    
    if (MarbreEnvironnement.MODE_DEBUG) {
      afficherListeDesSessions();
    }
  }
  
  /**
   * lancée à la destruction d'une session
   */
  @Override
  public void sessionDestroyed(HttpSessionEvent moteur) {
    UtilisateurWebshop utilisateurWebshop = (UtilisateurWebshop) moteur.getSession().getAttribute("utilisateur");
    // si il s'agit d'une session utilisateur, enregistrer l'événement
    if (utilisateurWebshop != null) {
      if (utilisateurWebshop.getTypeAcces() > MarbreEnvironnement.ACCES_PUBLIC) {
        // Ajouter un évènement pour la déconnexion de l'utilisateur
        utilisateurWebshop.getAccesDB2().sauverEvenement(utilisateurWebshop.getIdentifiant(), moteur.getSession().getId(), "F",
            "Fin de connexion " + utilisateurWebshop.getLogin(), this.getClass().getSimpleName());
      }
      utilisateurWebshop.deconnecterAS400();
    }
    
    // Virer la session de la liste
    if (listeSessions != null && listeSessions.size() > 0) {
      int i = 0;
      boolean trouve = false;
      
      while (i < listeSessions.size() && !trouve) {
        if (listeSessions.get(i).getId().equals(moteur.getSession().getId())) {
          listeSessions.remove(i);
          trouve = true;
        }
        i++;
      }
    }
    
    if (MarbreEnvironnement.MODE_DEBUG) {
      afficherListeDesSessions();
    }
  }
  
  /**
   * affiche la liste des sessions connectées au Web Shop
   */
  public static void afficherListeDesSessions() {
  }
  
  /**
   * Affiche la liste des adresses IP bloquées dans le contexte
   */
  public static void afficherListeIpBloquees() {
  }
}
