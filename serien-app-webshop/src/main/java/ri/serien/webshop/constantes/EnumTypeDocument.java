/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.constantes;

import ri.serien.libcommun.outils.MessageErreurException;

/*
 * Enumération des types de document (commande ou devis)
 */
public enum EnumTypeDocument {
  COMMANDE(1, "commande"),
  DEVIS(2, "demande de devis");
  
  private final Integer numero;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumTypeDocument(Integer pNumero, String pLibelle) {
    numero = pNumero;
    libelle = pLibelle;
  }
  
  /**
   * Le numéro sous lequel la variable est associé
   */
  public Integer getNumero() {
    return numero;
  }
  
  /**
   * Le libellé associée au numéro.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le libellé associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet énum par son numéro.
   */
  static public EnumTypeDocument valueOfByNumero(Integer pNumero) {
    for (EnumTypeDocument value : values()) {
      if (pNumero.equals(value.getNumero())) {
        return value;
      }
    }
    throw new MessageErreurException("Le type de document est invalide : " + pNumero);
  }
}
