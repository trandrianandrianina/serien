/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.constantes;

import java.util.ArrayList;

import ri.serien.webshop.metier.Etablissement;

public class MarbreEnvironnement {
  public static boolean MODE_DEBUG = false;
  public static final String SEPARATEUR = System.getProperty("file.separator");
  public static final char SEPARATEUR_CHAR = SEPARATEUR.charAt(0);
  public static final String SERIE = "N";
  public static final String TABLE_ENVIRONNEMENT = "PSEMENVM";
  public static final String BIBLI_SYSTEME = "QGPL";
  
  // SESSIONS + SECURITE
  // Temps au bout du quel la session du client se deconnecte si pas d'action
  public static int TEMPS_SESSION_INACTIVE = 300; // 5mn
  // nb de sessions actives maximum pour une adresse Ip
  public static int NB_SESSIONS_MAX_PAR_IP = 50;
  // Temps imparti en ms pour tester le nb de sessions actives max par adresse IP
  public static long TEST_PAR_IP = 60000; // 1mn
  // Nb de sessions actives tout court
  public static int NB_SESSIONS_MAX = 500;
  // Le mode SSL est requis pour le Web Shop de ce client
  public static boolean IS_MODE_SSL = false;
  
  // On est mode développeur sur nos postes
  public static boolean isEclipse = false;
  // On gère les indexs dynamiques ?
  public static boolean INDEXS_DYNAMIQUES = false;
  
  public static String ADRESSE_AS400 = null;
  public static String PROFIL_AS_ANONYME = null;
  public static String MP_AS_ANONYME = null;
  public static String LIBELLE_SERVEUR = "";
  
  public static String DOSSIER_RACINE_XWEBSHOP = null;// dossier racine du Webshop
  public static String versionInstallee = null;
  
  public static String ADRESSE_PUBLIC_SERVEUR = null;
  public static String PORT_SERVEUR = null;
  public static String ENVIRONNEMENT = null;
  public static char LETTRE_ENV = ' ';
  public static String PROFIL_AS_CLIENTS = null;
  public static String MP_AS_CLIENTS = null;
  public static String BIBLI_CLIENTS = null;
  public static String ETB_FORCE = null;
  public static Etablissement ETB_DEFAUT = null;
  public static ArrayList<Etablissement> LISTE_ETBS = null;
  
  public static final String DOSSIER_WS = "WebShop";
  
  // optimisation pour une lecture classique
  public static final String CLAUSE_OPTIMIZE = "  FOR READ ONLY WITH UR ";
  // optimisation supplémentaire pour une lecture d'un seul enregistrement
  public static final String CLAUSE_OPTIMIZE_UNIQUE = "   FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS  ";
  
  public static final String CODE_VENDEUR_WS = "*W";
  public static final String EXP_RETRAIT = "WE";
  public static final String EXP_LIVRAI = "WL";
  
  public static final String DOSSIER_IMAGES_DECO = "images" + SEPARATEUR + "decoration" + SEPARATEUR;
  public static final String DOSSIER_CSS = "css/";
  
  // Variables initialisées dans le context.xml
  // Dossier racine de Tomcat
  public static String DOSSIER_RACINE_TOMCAT = null;
  // Dossier racine de la partie publique du site
  public static String DOSSIER_RACINE_PUBLIC = null;
  // Dossier racine de la partie privée du site
  public static String DOSSIER_RACINE_PRIVATE = null;
  public static String BIBLI_WS = "xwebshop";
  // Dossier contenant les .java en dev
  public static String DOSSIER_SRC = null;
  
  // Variables initialisées dans la méthode refreshValue de cette classe
  public static String DOSSIER_TRAVAIL_PUBLIC = null;
  public static String DOSSIER_TRAVAIL_PRIVATE = null;
  // Dossier spécifique du client contenant ses css et images
  public static String DOSSIER_SPECIFIQUE = null;
  // Dossier des docs de l'IFS du client
  public static String DOSSIER_IFS = null;
  // Dossier local dev
  public static String DOSSIER_DEV = null;
  public static boolean CATALOGUE_MODE_PUBLIC = false;
  
  // Dossier à utiliser dans les URLs spécifiques du client(sera remappé automatiquement par FilterFile)
  public static final String DOSSIER_VIRTUEL_SPECIFIQUE = "./virtual/specific";
  
  // GOOGLE ANALYTICS
  public static String GOOGLE_ANALYTICS = null;
  
  // Nb de tentatives de connexion maximum avant bloquage de la session
  public static final int MAX_TENT_CONNEX = 5;
  
  // Tous les types d'accès de l'utilisateur
  public static final int PROFIL_INVITE = 1;
  public static final int ACCES_PAS_DB2 = -2;
  public static final int ACCES_NON_DEPLOYE = -1;
  public static final int ACCES_MAINTENANCE = 0;
  public static final int ACCES_PUBLIC = 1;
  
  // Suite à une saisie de MDP
  public static final int ACCES_INEXISTANT = 2;
  public static final int ACCES_AT_MDP = 3;
  public static final int ACCES_ER_CONF_MDP = 4;
  public static final int ACCES_DEJA_EXISTANT = 5;
  public static final int ACCES_AT_VALID = 6;
  public static final int ACCES_ER_SAISIE = 7;
  public static final int ACCES_ER_MDP = 8;
  public static final int BLOC_SAISIE_MAX = 9;
  public static final int ACCES_REFUSE = 10;
  // Etats intermédiaires de vérification de profil
  public static final int ACCES_ATT_NV_MP = 11;
  public static final int ACCES_DE_NV_MP = 12;
  public static final int ACCES_NV_MP_ADMIN = 13;
  public static final int ACCES_VAL_NV_MP = 14;
  public static final int ACCES_VAL_PROFIL = 15;
  public static final int ACCES_MULTIPLE_CONNEX = 16;
  public static final int ACCES_MULTIPLE_INSCRI = 17;
  
  // ACCES UTILISATEURS VALIDES
  public static final int ACCES_CLIENT = 20;
  // accès utilisateur en consultation
  public static final int ACCES_CONSULTATION = 21;
  public static final int ACCES_BOUTIQUE = 22;
  public static final int ACCES_REPRES_WS = 25;
  public static final int ACCES_RESPON_WS = 30;
  public static final int ACCES_ADMIN_WS = 40;
  
  // modes de récupération du panier
  public static final int MODE_NON_CHOISI = 0;
  public static final int MODE_RETRAIT = 1;
  public static final int MODE_LIVRAISON = 2;
  public static final int MODE_DEVIS = 3;
  
  public static boolean MAIL_IS_MODE_TEST = true;
  public static String MAIL_TESTS = "";
  public static String MAIL_WEBMASTER = "";
  
  public static final int ENCODE_INT = 365;
  
  // conditionne l'affiche des stocks, 0=Avec valeur, 1=Sans valeur
  public static final int STOCK_AV_VALEUR = 0;
  public static final int STOCK_SANS_VALEUR = 1;
  
  // Gestion des logs
  public static final int NB_MAX_LOGS = 20000;
  
  // on gardera les 30 derniers jours de logs.
  public static final int NB_JOURS_MAX_LOGS = 30;
  
  public static String CODE_QUATRE_DECIMALE = "*";
  
  // PS_67 à 5 - Gestion du stock réservé
  public static boolean TRAITEMENT_STOCK_RESERVE = false; // JEN SUIS ICI
  
  // Est ce qu'on affiche les délais de livraisons/disponibilité
  public static boolean VOIR_TABLE_DELAIS = false;
  public static final String PARAM_FILROUGE = "PARAMETRE_FILROUGE_PARAMETRE";
  
  // Zone qui gère le conditionnement des articles dans cette entreprise
  // A VOIR PLUS TARD POUR LES METTRE DANS l'ETABLISSEMENT ET TYPER LE VUES ET LES INDEX
  public static String ZONE_CONDITIONNEMENT = "A1CND";
  
  // Variable qui checke si on a déjà scanné la présence du fichier PSEMURLM
  public static boolean CHECK_PSEMURLM = false;
  
  // Variable qui checke si on a déjà scanné la présence du fichier PSEMURLM
  public static boolean PSEMURLM_IS_OK = false;
  
  // variable qui rend le site accessible en plusieurs langues
  public static boolean IS_MODE_LANGUAGES = false;
  
  // variable qui autorise de gérer les lots Série N dans le WebShop
  public static boolean IS_GESTION_LOTS = false;
  
  /**
   * Met à jour les variables si besoin
   */
  public static void refreshValue() {
    DOSSIER_TRAVAIL_PRIVATE = DOSSIER_RACINE_PRIVATE + DOSSIER_WS + SEPARATEUR;
    DOSSIER_TRAVAIL_PUBLIC = DOSSIER_RACINE_PUBLIC + DOSSIER_WS + SEPARATEUR;
    DOSSIER_SPECIFIQUE = DOSSIER_TRAVAIL_PUBLIC + "specific" + SEPARATEUR;
    DOSSIER_DEV = DOSSIER_SRC + "src" + SEPARATEUR;
  }
  
}
