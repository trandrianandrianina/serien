/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionConditionsGenerales;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Servlet ConditionsGenerales
 */
public class ConditionsGenerales extends MouleVues {
  
  public ConditionsGenerales() {
    super(
        new GestionConditionsGenerales(), "conditionsGenerales", "<link href='" + MarbreEnvironnement.DOSSIER_CSS
            + "conditionsGenerales.css?" + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("conditionsGenerales", "ConditionsGenerales", "Conditions générales", "Terms and conditions"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficherConditions((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
          "<h2><span id='titrePage'>"
              + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$CGV")
              + "</span></h2>"),
          "conditionsGenerales", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le formulaire des CGV
   */
  private String afficherConditions(UtilisateurWebshop utilisateur, String titre) {
    if (utilisateur == null || titre == null) {
      return "";
    }
    
    String retour = titre;
    retour += "<div class='blocContenu'>";
    
    utilisateur.recupererUnRecordTravail(((GestionConditionsGenerales) maGestion).RecupererCgv(utilisateur));
    
    if (utilisateur.getRecordTravail() != null && utilisateur.getRecordTravail().isPresentField("CGV_NAME")) {
      retour += "<div class='CGV_MESS'>" + utilisateur.getRecordTravail().getField("CGV_NAME").toString().trim() + "</div>";
    }
    else {
      retour += "<div class='CGV_MESS'>Pas de conditions générales</div>";
    }
    
    retour += "</div>";
    
    return retour;
  }
}
