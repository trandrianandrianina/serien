/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionBddBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class BddBO
 */
public class BddBO extends MouleVues {
  
  public BddBO() {
    super(new GestionBddBO(), "BddBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "bddBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("BddBO", "BddBO", "Saisie Requête", "Enter a query"));
    listeFilRouge.add(new FilRouge("requeteBDD",
        "<a href='BddBO?fichier=" + MarbreEnvironnement.PARAM_FILROUGE + "' class='filRougeText'>Un fichier</a>", "Résultat", "Result"));
    // listeFilRouge.add(new FilRouge("logId", "<a href='logsBO?logId=" + MarbreEnvironnement.PARAM_FILROUGE + "'
    // class='filRougeText'>Détail log</a>"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("requeteBDD") != null) {
        out.println(afficherContenu(afficherRetourRequete(request.getSession(), request.getParameter("requeteBDD"), request),
            "requeteBDD", null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(afficherFormulaire(null, request, request.getSession()), null, null, null, request.getSession()));
      }
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Formulaire de saisie de la requete SQL
   */
  
  private String afficherFormulaire(String requete, HttpServletRequest request, HttpSession sess) {
    StringBuilder retour = new StringBuilder();
    
    if (requete == null) {
      requete = "";
    }
    
    retour.append("<h2><span id='titrePage'>Saisie de requête</span></h2>");
    
    retour.append("<div class='blocContenu'>");// id='blocContenu'
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Requête SQL sur " + MarbreEnvironnement.BIBLI_WS + "</h3>");
    retour.append("<form action='BddBO' method='POST' name='formRequeteBDD' id='formRequeteBDD'>");
    retour.append("<textarea name='requeteBDD' rows='5' class='zone'>" + requete + "</textarea>");
    retour.append("<input type='submit' class='BTNenvoyer' value='Traiter' id='EnvoyerSQLITE'/>");
    retour.append("</form>");
    retour.append("</div>");
    // affiche la liste des tables présent dans xwebshop
    if (request.getParameter("requeteBDD") != null) {
      retour.append(afficherRetourRequete(sess, request.getParameter("requeteBDD"), request));
    }
    
    retour.append(afficherListeFichier(sess, "", request));
    if (request.getParameter("fichier") != null) {
      retour.append(afficherDetailFichier(sess, "", request.getParameter("fichier"), request));
    }
    return retour.toString();
  }
  
  /**
   * Afficher la liste des fichiers de la bibliothéque WebShop
   */
  private String afficherListeFichier(HttpSession sess, String titre, HttpServletRequest request) {
    String retour = titre;
    
    retour += "<div class='blocContenu'>";
    
    retour += ("<h3><span class='puceH3'>&nbsp;</span>Liste des tables </h3>");
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ArrayList<GenericRecord> listeFichiers =
          ((GestionBddBO) maGestion).retournerListeFichier((UtilisateurWebshop) sess.getAttribute("utilisateur"));
      if (listeFichiers != null) {
        for (int i = 0; i < listeFichiers.size(); i++) {
          retour += "<a class='listeTables' href='bddBO?fichier=" + listeFichiers.get(i).getField("TABLE_NAME") + "'>"
              + listeFichiers.get(i).getField("TABLE_NAME");
        }
      }
      else {
        retour += "<p>Pas de description à afficher</p>";
      }
    }
    retour += "</div>";
    return retour;
  }
  
  /**
   * Afficher la description d'un fichier
   */
  private String afficherDetailFichier(HttpSession sess, String titre, String fichier, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ArrayList<GenericRecord> listeFields =
          ((GestionBddBO) maGestion).retournerDetailFichier((UtilisateurWebshop) sess.getAttribute("utilisateur"), fichier);
      if (listeFields != null) {
        // entêtes de colonnes
        retour.append("<div  class='listes' id='enTeteListes'>" + "<span id='titreNom'>Nom du champ </span>"
            + "<span id='titreChamp'>type du champ</span></div>");
        for (int i = 0; i < listeFields.size(); i++) {
          retour.append("<div class='listes'>");
          retour.append("<a class='COLUMN_NAME'// href='#'>");
          if (listeFields.get(i).isPresentField("COLUMN_NAME")) {
            retour.append(listeFields.get(i).getField("COLUMN_NAME").toString().trim());
          }
          else {
            retour.append("aucune");
            retour.append("</a>");
          }
          retour.append("<a class='DATA_TYPE'// href='#'>");
          if (listeFields.get(i).isPresentField("DATA_TYPE")) {
            retour.append(listeFields.get(i).getField("DATA_TYPE").toString().trim());
          }
          else {
            retour.append("aucune");
            retour.append("</a>");
          }
          retour.append("</div>");
        }
        
      }
      else {
        retour.append("<p>Pas de champs disponibles</p>");
      }
    }
    retour.append("</div>");
    return retour.toString();
  }
  
  private String afficherRetourRequete(HttpSession sess, String requete, HttpServletRequest request) {
    
    StringBuilder chaine = new StringBuilder();
    int nFields = 0;
    
    // chaine.append(afficherFormulaire(requete, request, sess));
    chaine.append("<div class='blocContenu'>");
    
    chaine.append("<h3><span class='puceH3'>&nbsp;</span>résultat de la requête </h3>");
    ArrayList<GenericRecord> listeResultat = null;
    listeResultat = ((GestionBddBO) maGestion).retournerRequete((UtilisateurWebshop) sess.getAttribute("utilisateur"), requete);
    
    if (listeResultat != null && listeResultat.size() > 0) {
      // recupere le champs
      nFields = listeResultat.get(0).getNumberOfFields();
      chaine.append("<table  class='table' >");// construction de la table pour le résultat de la requête
      for (int i = 0; i < listeResultat.size(); i++) {
        chaine.append("<TR>");
        for (int j = 0; j < nFields; j++) {
          chaine.append("<TD class='cellule'>" + listeResultat.get(i).getField(j) + "</TD>");
        }
        
        chaine.append("</TR>");
      }
    }
    else {
      chaine.append("<p id='retourSQLITE'>pas d'enregistrement</p>");
    }
    
    chaine.append("</table>");
    chaine.append("</div>");
    
    return chaine.toString();
  }
  
}
