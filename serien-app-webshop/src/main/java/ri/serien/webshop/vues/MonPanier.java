/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.EnumTypeDocument;
import ri.serien.webshop.constantes.MarbreAffichage;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionMesDonnees;
import ri.serien.webshop.controleurs.GestionMonPanier;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.PanierArticle;
import ri.serien.webshop.metier.PanierArticleNormal;
import ri.serien.webshop.outils.Outils;
import ri.serien.webshop.outils.mail.ParametreMail;

/**
 * Servlet MonPanier c'est l'affichage du panier de l'utilisateur
 */
public class MonPanier extends MouleVues {
  
  /**
   * Constructeur du panier
   */
  public MonPanier() {
    super(new GestionMonPanier(), "monPanier",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "monPanier.css?" + MarbreEnvironnement.versionInstallee
            + "' rel='stylesheet'/>" + "<link rel='stylesheet' href='jquery/jquery-ui.css'>"
            + "<script src='jquery/external/jquery/jquery.js'></script>" + "<script src='jquery/jquery-ui.js'></script>"
            + "<script src='jquery/datepicker-fr.js'></script>" + "<script src='scripts/monPanier.js?"
            + MarbreEnvironnement.versionInstallee + "'></script>",
        MarbreEnvironnement.ACCES_CLIENT);
    
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("catalogue", "catalogue?retour=1", "Catalogue", "Catalog"));
    listeFilRouge.add(new FilRouge("monPanier", "catalogue", "Mon panier", "My basket"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
      
      // Mode AJAX pour mettre à jour les infos de livraison/retrait dans le panier de l'utilisateur
      if (request.getParameter("modificationBlocAdresse") != null) {
        String zone = gererZone(request);
        
        // Mettre à jour les variables en fonction du mode dans lequel on est REPRESENTANT OU CLIENT
        ((GestionMonPanier) maGestion).majVariablesPanier(utilisateur, zone, request.getParameter(zone));
      }
      else {
        
        // Supprimer un article du panier avant d'afficher le panier dans la navigation persistante
        if (request.getParameter("SupArticle") != null) {
          utilisateur.getMonPanier().supprimeArticle(request.getParameter("SupEtbArt"), request.getParameter("SupArticle"),
              request.getParameter("supQuantite"));
        }
        
        // mettre à jour le mode de récupération de l'utilisateur
        if (request.getParameter("modeRecup") != null) {
          ((GestionMonPanier) maGestion).majModeRecuperation(utilisateur, request.getParameter("modeRecup"));
        }
        
        // Modifier la quantité du panier
        if (request.getParameter("panierArticle") != null && request.getParameter("panierQuantite") != null) {
          
          utilisateur.getMonPanier().modificationArticle(utilisateur.getEtablissementEnCours(), request.getParameter("panierArticle"),
              new BigDecimal(request.getParameter("panierQuantite")), new BigDecimal(request.getParameter("ancienneQte")));
        }
        
        if (request.getParameter("nvMagasin") != null) {
          utilisateur.getMonPanier().modifierLeMagasinRetrait(request.getParameter("nvMagasin"));
        }
        
        ServletOutputStream out = response.getOutputStream();
        String connexion = pattern.afficherConnexion(nomPage, request, response);
        
        redirectionSecurite(request, response);
        
        // Affichage d'en-tête
        out.println(pattern.majDuHead(request, cssSpecifique, null));
        out.println(pattern.afficherPresentation(nomPage, request, connexion));
        // traitement de la validation du panier en commande ou devis
        if (request.getParameter("validation") != null) {
          if (request.getParameter("validation").equals("1") || request.getParameter("validation").equals("2")) {
            out.println(afficherContenu(afficherValidationPanier(utilisateur, request), "monPanier", null, null, request.getSession()));
          }
          else if (request.getParameter("validation").equals("0")) {
            traitementAnnulationPanier(utilisateur, request, response);
          }
        }
        // Affichage simple du panier
        else {
          if (utilisateur != null) {
            out.println(
                afficherContenu(afficherPanier(utilisateur, "<h2><span id='titrePage'>" + gererTitrePanier(utilisateur) + "</span></h2>"),
                    "monPanier", null, null, request.getSession()));
          }
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur produite dans MonPanier.traiterPOSTouGET()");
    }
  }
  
  /**
   * Gère la zone
   */
  private String gererZone(HttpServletRequest pRequest) {
    String zone = null;
    
    if (pRequest.getParameter("idCLNOM") != null) {
      zone = "idCLNOM";
    }
    else if (pRequest.getParameter("idCLCPL") != null) {
      zone = "idCLCPL";
    }
    else if (pRequest.getParameter("idCLRUE") != null) {
      zone = "idCLRUE";
    }
    else if (pRequest.getParameter("idCLLOC") != null) {
      zone = "idCLLOC";
    }
    else if (pRequest.getParameter("idCLVIL") != null) {
      zone = "idCLVIL";
    }
    else if (pRequest.getParameter("idCLPOS") != null) {
      zone = "idCLPOS";
    }
    else if (pRequest.getParameter("idDetailsRetrait") != null) {
      zone = "idDetailsRetrait";
    }
    else if (pRequest.getParameter("idDateRetrait") != null) {
      zone = "idDateRetrait";
    }
    else if (pRequest.getParameter("idReferenceLongue") != null) {
      zone = "idReferenceLongue";
    }
    
    return zone;
  }
  
  /**
   * Gère le titre du Panier
   */
  private String gererTitrePanier(UtilisateurWebshop pUtilisateur) {
    String titre = "";
    if (pUtilisateur.getMonPanier() != null) {
      if (pUtilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT
          || pUtilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        titre = "Ma commande";
      }
      else if (pUtilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_DEVIS) {
        titre = "Mon devis";
      }
      else if (pUtilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_NON_CHOISI) {
        titre = pUtilisateur.getTraduction().traduire("$$monPanier");
      }
    }
    
    return titre;
  }
  
  /**
   * L'utilisateur annule panier courant. Il faut donc tout supprimer et
   * afficher l'ancienne liste d'articles
   */
  private void traitementAnnulationPanier(UtilisateurWebshop utilisateur, HttpServletRequest request, HttpServletResponse reponse) {
    if (utilisateur == null || request == null) {
      return;
    }
    
    if (utilisateur.razPanier()) {
      try {
        getServletContext().getRequestDispatcher("/catalogue?retour=1").forward(request, reponse);
      }
      catch (Exception e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * retour du passage du panier à la commande dans Série M
   */
  private String afficherValidationPanier(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    String retour = "";
    if (request == null) {
      return "";
    }
    
    retour += "<div class='blocContenu'>";
    
    if (utilisateur != null) {
      LinkedHashMap<String, String> rapport = new LinkedHashMap<String, String>();
      
      String typeBon = "ATT";
      EnumTypeDocument typeDemande = EnumTypeDocument.COMMANDE;
      
      if (request.getParameter("validation") != null) {
        if (request.getParameter("validation").equals("1")) {
          // DEA pour les devis en attente et DEV pour les devis validés
          typeBon = "DEA";
          typeDemande = EnumTypeDocument.DEVIS;
        }
      }
      
      // Si le panier contient des articles
      if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getListeArticles() != null
          && utilisateur.getMonPanier().getListeArticles().size() > 0) {
        // On valide la commande et on checke le résultat
        if (utilisateur.getMonPanier().valideCommande(typeBon, null, rapport)) {
          // si on bien injecté le bon
          if (rapport.containsKey("TYPE") && rapport.containsKey("ETB") && rapport.containsKey("NUMERO")
              && rapport.containsKey("SUFFIXE")) {
            if (typeBon.equals("ATT")) {
              typeBon = "E";
            }
            else if (typeBon.equals("DEA")) {
              typeBon = "D";
            }
            else {
              typeBon = "M";
            }
            
            retour += "<h3><span class='puceH3'>&nbsp;</span>Validation de votre " + typeDemande.getLibelle() + "</h3>";
            retour += "<p class='messageRubrique'>Votre " + typeDemande.getLibelle() + " N° " + rapport.get("NUMERO")
                + rapport.get("SUFFIXE")
                + " a bien été transmise à nos services.<br/>Vous recevrez dans les plus brefs délais, la confirmation de disponibilité de la part de notre agent Web Shop</p>";
            
            String numero = rapport.get("TYPE") + rapport.get("NUMERO") + rapport.get("SUFFIXE");
            GestionMesDonnees mesDonnees = new GestionMesDonnees();
            GenericRecord enTeteCommande = mesDonnees.recupererEnteteCommande(utilisateur, numero);
            
            // Passage au mails
            // Magasin de la commande
            String magasin = null;
            if (enTeteCommande != null && enTeteCommande.isPresentField("E1MAG")) {
              magasin = enTeteCommande.getField("E1MAG").toString().trim();
            }
            if (utilisateur.razPanier()) {
              retour += pattern.razDuPanier(utilisateur);
              // Mettre dans un thread
              
              // On gère l'envoi par mail de la confirmation
              if (utilisateur.getEtablissementEnCours().isOk_envois_mails()) {
                ParametreMail parametreMail = null;
                // Préparer le mail à envoyer au Client
                if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT) {
                  // Pour le cas de confirmation de commande
                  if (typeDemande == EnumTypeDocument.COMMANDE) {
                    parametreMail = ((GestionMesDonnees) getMaGestion()).preparerParametreMailPourCommande(utilisateur,
                        rapport.get("NUMERO"), rapport.get("SUFFIXE"), rapport.get("TYPE"), EnumTypeDocument.COMMANDE, false);
                    utilisateur.envoyerMail(utilisateur.getLogin(), parametreMail, EnumTypeMail.MAIL_CONFIRMATION_COMMANDE_HTML);
                  }
                  // Pour le cas de confirmation de devis
                  else if (typeDemande == EnumTypeDocument.DEVIS) {
                    parametreMail = ((GestionMesDonnees) getMaGestion()).preparerParametreMailPourDevis(utilisateur,
                        rapport.get("NUMERO"), rapport.get("SUFFIXE"), rapport.get("TYPE"), EnumTypeDocument.DEVIS, false);
                    utilisateur.envoyerMail(utilisateur.getLogin(), parametreMail, EnumTypeMail.MAIL_CONFIRMATION_DEVIS_HTML);
                  }
                }
                
                // Préparer le mail à envoyer au gestionnaire
                // Cas d'une commande
                if (typeDemande == EnumTypeDocument.COMMANDE) {
                  parametreMail = ((GestionMesDonnees) getMaGestion()).preparerParametreMailPourCommande(utilisateur,
                      rapport.get("NUMERO"), rapport.get("SUFFIXE"), rapport.get("TYPE"), EnumTypeDocument.COMMANDE, true);
                  utilisateur.envoyerMail(retournerAdresseMailConfirmation(magasin, typeBon, utilisateur), parametreMail,
                      EnumTypeMail.MAIL_RAPPORT_COMMANDE_HTML);
                }
                // Cas d'un devis
                else if (typeDemande == EnumTypeDocument.DEVIS) {
                  parametreMail = ((GestionMesDonnees) getMaGestion()).preparerParametreMailPourDevis(utilisateur, rapport.get("NUMERO"),
                      rapport.get("SUFFIXE"), rapport.get("TYPE"), EnumTypeDocument.DEVIS, true);
                  utilisateur.envoyerMail(retournerAdresseMailConfirmation(magasin, typeBon, utilisateur), parametreMail,
                      EnumTypeMail.MAIL_RAPPORT_DEVIS_HTML);
                }
              }
              
              // Ajouter un évènement pour le devis ou la commande saisie
              utilisateur.getAccesDB2().sauverEvenement(utilisateur.getIdentifiant(), request.getSession().getId(), typeBon,
                  rapport.get("NUMERO") + rapport.get("SUFFIXE"), "MonPanier");
            }
          }
          // si on a pas bien injecté le bouzin
          else {
            retour += "<p class='messageAlerte'>";
            retour += "Injection de votre " + typeDemande.getLibelle() + " en erreur 2. Veuillez contacter votre agence.<br/>";
            for (Entry<String, String> entry : rapport.entrySet()) {
              if (entry.getKey().trim().contains("_LIBELLE_ERREUR")) {
                retour += (String.format("%-20s", entry.getValue())) + "<br/>";
              }
            }
            retour += "</p>";
          }
        }
        else {
          retour += "<p class='messageAlerte'>";
          retour += "Injection de votre " + typeDemande.getLibelle() + " en erreur. Veuillez contacter votre agence.<br/>";
          for (Entry<String, String> entry : rapport.entrySet()) {
            if (entry.getKey().trim().contains("_LIBELLE_ERREUR")) {
              retour += (String.format("%-20s", entry.getValue())) + "<br/>";
            }
          }
          retour += "</p>";
        }
      }
      else {
        retour += "<p class='messageAlerte'>Votre panier est vide.</p>";
      }
    }
    
    retour += "</div>";
    return retour;
  }
  
  /**
   * Cette méthode permet d'afficher le panier de l'utilisateur dans son
   * ensemble
   */
  private String afficherPanier(UtilisateurWebshop utilisateur, String titre) {
    String retour = titre;
    retour += "<div class='blocContenu'>";
    
    if (utilisateur != null) {
      // Gestion du mode récupération
      retour += afficherModeDeRecuperation(utilisateur, "MonPanier");
      
      // afficher les articles avec les bons stocks et dans les bonnes rubriques
      retour += afficherArticles(utilisateur);
      
      retour += afficherFinDeBon(utilisateur);
    }
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Cette méthode permet de gérer le mode récupération de la commande
   * (indispensable à la gestion des stocks)
   */
  public String afficherModeDeRecuperation(UtilisateurWebshop utilisateur, String page) {
    boolean modePanier = true;
    String retour = "";
    String parametres = "?modeRecup=";
    String ajoutBtRetour = "";
    String lienRetrait = "";
    String lienLivraison = "";
    String lienDevis = "";
    String debutSpec = "";
    String finSpec = "";
    String titreModeRecup = "$$disponibilite";
    
    // Afficher le mode récupération de la commande dans la liste des articles du catalogue.
    // Ce choix s'affiche lorsque la quantité en stock disponible n'est pas affichée dans la liste des articles. Un icône ressemblant à
    // un icône rafraichir est affiché dans la colonne stock. Si on clique sur cet icône, le logiciel propose les modes de récupérations.
    if (page.equals("Catalogue")) {
      modePanier = false;
      parametres = "?visuStock=1&modeRecup=";
      ajoutBtRetour =
          "<div class='divDeBoutons'  id='blocDeRetour'><a class='btRecuperation' href='catalogue?retour=1'><span class='deco_bt' id='deco_bt_retour'>&nbsp;</span><span class='texte_bt'>"
              + utilisateur.getTraduction().traduire("$$retour") + "</span></a></div>";
      
      // Afficher les boutons "Retrait en magasin" et "Livraison".
      if (utilisateur.isSaisieCommandeAutorisee()) {
        lienRetrait =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\"  onClick='traitementStockDispo(\""
                + page + parametres + MarbreEnvironnement.MODE_RETRAIT
                + "\");'><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$retraitMagasin") + "</span></a></div>";
        lienLivraison =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\"  onClick='traitementStockDispo(\""
                + page + parametres + MarbreEnvironnement.MODE_LIVRAISON
                + "\");'><span class='deco_bt' id='deco_bt_livraison'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$livraison") + "</span></a></div>";
      }
      
      // Afficher le bouton "Simple devis".
      if (utilisateur.isSaisieDevisAutorisee()) {
        lienDevis =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\"  onClick='traitementStockDispo(\""
                + page + parametres + MarbreEnvironnement.MODE_DEVIS
                + "\");'><span class='deco_bt' id='deco_bt_devis'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$simpleDevis") + "</span></a></div>";
      }
    }
    // Afficher le mode récupération de la commande dans la fiche article.
    // Cela s'affiche lorsqu'on clique sur le bouton "Stocks" (icône rafraichir) de la fiche article.
    else if (page.startsWith("catalogue?A1ETB=")) {
      modePanier = false;
      parametres = "&modeRecup=";
      ajoutBtRetour = "<div class='divDeBoutons'  id='blocDeRetour'><a class='btRecuperation' href='" + page
          + "'><span class='deco_bt' id='deco_bt_retour'>&nbsp;</span><span class='texte_bt'>"
          + utilisateur.getTraduction().traduire("$$retour") + "</span></a></div>";
      
      // Afficher les boutons "Retrait en magasin" et "Livraison".
      if (utilisateur.isSaisieCommandeAutorisee()) {
        lienRetrait =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\" onClick='traitementStockDispo(\""
                + page + parametres + MarbreEnvironnement.MODE_RETRAIT
                + "\");'><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$retraitMagasin") + "</span></a></div>";
        lienLivraison =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\" onClick='traitementStockDispo(\""
                + page + parametres + MarbreEnvironnement.MODE_LIVRAISON
                + "\");'><span class='deco_bt' id='deco_bt_livraison'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$livraison") + "</span></a></div>";
      }
      
      // Afficher le bouton "Simple devis".
      if (utilisateur.isSaisieDevisAutorisee()) {
        lienDevis =
            "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href=\"javascript:void(0)\" onClick='traitementStockDispo(\""
                + page + parametres + MarbreEnvironnement.MODE_DEVIS
                + "\");'><span class='deco_bt' id='deco_bt_devis'>&nbsp;</span><span class='texte_bt'>"
                + utilisateur.getTraduction().traduire("$$simpleDevis") + "</span></a></div>";
      }
    }
    // Afficher le mode récupération de la commande dans le panier (mode le plus courant).
    else {
      debutSpec =
          "<div class='accordeon2' id='changerMode'><a class='ouvrirAccordeonG' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChangerMode');\">&nbsp;</a><a class='titreAccordeon' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChangerMode');\">Changer votre mode de récupération"
              + "</a> <a class='ouvrirAccordeonD' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChangerMode');\">&nbsp;</a></div>";
      debutSpec += "<div class='contenuAccordeon2' id='contenuChangerMode'>";
      
      // Afficher les boutons "Retrait en magasin" et "Livraison".
      if (utilisateur.isSaisieCommandeAutorisee()) {
        lienRetrait = "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href='" + page + parametres
            + MarbreEnvironnement.MODE_RETRAIT + "'><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>"
            + utilisateur.getTraduction().traduire("$$retraitMagasin") + "</span></a></div>";
        lienLivraison = "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href='" + page + parametres
            + MarbreEnvironnement.MODE_LIVRAISON + "'><span class='deco_bt' id='deco_bt_livraison'>&nbsp;</span><span class='texte_bt'>"
            + utilisateur.getTraduction().traduire("$$livraison") + "</span></a></div>";
      }
      
      // Afficher le bouton "Simple devis".
      if (utilisateur.isSaisieDevisAutorisee()) {
        lienDevis = "<div class='divDeBoutons'  id='blocDeRecuperation'><a class='btRecuperation' href='" + page + parametres
            + MarbreEnvironnement.MODE_DEVIS + "'><span class='deco_bt' id='deco_bt_devis'>&nbsp;</span><span class='texte_bt'>"
            + utilisateur.getTraduction().traduire("$$simpleDevis") + "</span></a></div>";
      }
      
      finSpec = "</div>";
      // titreModeRecup = "$$modeRecuperation";
      titreModeRecup = "Mode de récupération";
    }
    
    if (utilisateur.getMonPanier() == null) {
      return retour;
    }
    
    // liens ci-dessous
    retour += "<div class='rubriquePanier' id='modeRecup'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>" + utilisateur.getTraduction().traduire(titreModeRecup) + "</h3>";
    retour += "<p class='messageRubrique'>";
    // Le mode récupération n'est pas choisi
    if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_NON_CHOISI) {
      retour += utilisateur.getTraduction().traduire("$$choisirModeRecuperation");
      retour += "</p>";
      retour += lienRetrait;
      retour += lienLivraison;
      retour += lienDevis;
    }
    // Le mode de récupération est le "retrait en magasin"
    else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
      if (utilisateur.getMonPanier().getMagasinPanier() == null) {
        retour += "Vous n'avez pas encore choisi votre magasin de retrait";
      }
      else {
        retour += "Vous avez choisi de retirer votre commande au magasin - "
            + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin() + " -";
      }
      retour += "</p>";
      retour += afficherChoixMagasins(utilisateur);
      if (modePanier) {
        retour += afficherInfosRecup(utilisateur);
      }
      
      // Si multi magasin, on affiche le bouton de changement du magasin
      if (!utilisateur.getEtablissementEnCours().is_mono_magasin()) {
        retour +=
            "<div class='divDeBoutons' id='blocDeRecuperation'><a class='btRecuperation' id='chgMonMag' href='javascript:void(0)' onClick=\"montrerAccordeon('choixMagasin','contenuChoixMagasin'); cacherUnElement('chgMonMag');\" ><span class='deco_bt' id='deco_bt_retrait'>&nbsp;</span><span class='texte_bt'>Changer de magasin</span></a></div>";
      }
      retour += debutSpec;
      retour += lienLivraison;
      retour += lienDevis;
      retour += finSpec;
      
    }
    // le mode de récupération est la livraison
    else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
      retour +=
          "Vous avez choisi de vous faire livrer votre marchandise.<br/>Veuillez remplir ci-dessous les renseignements nécessaires à la livraison.";
      retour += "</p>";
      if (modePanier) {
        retour += afficherInfosRecup(utilisateur);
      }
      
      retour += debutSpec;
      retour += lienRetrait;
      retour += lienDevis;
      retour += finSpec;
    }
    // simple devis
    else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_DEVIS) {
      retour += "Vous avez choisi une demande de devis";
      retour += "</p>";
      if (modePanier) {
        retour += afficherInfosRecup(utilisateur);
      }
      retour += debutSpec;
      retour += lienRetrait;
      retour += lienLivraison;
      retour += finSpec;
    }
    
    retour += ajoutBtRetour;
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher les informations propres au retrait de la marchandise en magasin ou en livraison
   */
  private String afficherInfosRecup(UtilisateurWebshop utilisateur) {
    StringBuilder retour = new StringBuilder(5000);
    
    if (utilisateur != null) {
      // TRAITEMENT
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // mettre à jour le bloc adresse du client en cas de livraison
      if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        if (utilisateur.getMonPanier().getCLNOM() == null && utilisateur.getMonPanier().getCLCPL() == null
            && utilisateur.getMonPanier().getCLRUE() == null && utilisateur.getMonPanier().getCLLOC() == null) {
          ((GestionMonPanier) maGestion).majVariablesPanier(utilisateur);
        }
      }
      
      // Mise à jour du bloc commentaires du panier
      if (utilisateur.getMonPanier().getDetailsRecuperation() == null) {
        utilisateur.getMonPanier().setDetailsRecuperation("");
      }
      
      // GESTION DE LA DATE DE RECUPERATION DU PANIER
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
        if (Outils.recupererDateCouranteInt() > utilisateur.getMonPanier().getDateRecuperation()) {
          utilisateur.getMonPanier().setDateRecuperation(Outils.recupererDateCouranteInt());
        }
      }
      else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        if (Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(Outils.recupererDateCouranteInt(),
            utilisateur.getEtablissementEnCours().getDelai_mini_livraison())) > utilisateur.getMonPanier().getDateRecuperation()) {
          utilisateur.getMonPanier().setDateRecuperation(Outils.transformerDateHumaineEnSeriem(Outils.mettreAjourUneDateSerieM(
              Outils.recupererDateCouranteInt(), utilisateur.getEtablissementEnCours().getDelai_mini_livraison())));
        }
      }
      
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // AFFICHAGE
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      
      // Affichage du bloc d'informations supplémentaires
      String typeRetrait = "retrait";
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_DEVIS) {
        typeRetrait = "devis";
      }
      
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        typeRetrait = "livraison";
      }
      
      retour.append("<div class='accordeon2' id='infosRetrait'>"
          + "<a class='ouvrirAccordeonG' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuInfosRetrait');\">&nbsp;</a>"
          + "<a class='titreAccordeon' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuInfosRetrait');\">Informations de "
          + typeRetrait + "</a>"
          + "<a class='ouvrirAccordeonD' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuInfosRetrait');\">&nbsp;</a>"
          + "</div>");
      retour.append("<div class='contenuAccordeon2' id='contenuInfosRetrait'>");
      retour.append(
          "<script type=\"text/javascript\">if(document.getElementById('contenuInfosRetrait').style.display=='') document.getElementById('contenuInfosRetrait').style.display='block';</script>"
              + "<form name='formInfosRetrait' id='formInfosRetrait' action ='MonPanier' method ='POST' >");
      
      if (!(utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_DEVIS)) {
        
        // Date de retrait/livraison souhaitée
        retour.append("<label class='labelsModeRecup' for='idDateRetrait' id='lbDateRetrait'>Date de " + typeRetrait
            + " souhaitée</label>" + "<input type='text' name='dateRetrait' id='idDateRetrait' readonly='readonly' value = '"
            + Outils.transformerDateSeriemEnHumaine(utilisateur.getMonPanier().getDateRecuperation())
            + "' onClick=\"openCalendarRetrait()\" onChange='majInfosPanier(\"idDateRetrait\");' /><br/>");
      }
      // Référence longue avec libellé paramétrable
      retour.append("<label  class='labelsModeRecup' for='idCLNOM' id='lbCLNOM' >"
          + utilisateur.getTraduction().traduire("$$referenceLongue") + "</label>"
          + "<input  class='zonesBlocAdresse' type='text' name='REFLONGUE' id='idReferenceLongue' maxlength ='25' value=\""
          + utilisateur.getMonPanier().getE1RCC().trim() + "\" onBlur='majInfosPanier(\"idReferenceLongue\");'/><br/>");
      // Bloc adresse spécifique aux livraisons
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        retour.append("<br/><br/>" + "<label  class='labelsModeRecup' for='idCLNOM' id='lbCLNOM' >Nom ou raison sociale</label>"
            + "<input class='zonesBlocAdresse' type='text' name='CLNOM' id='idCLNOM' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLNOM() + "\" onBlur='majInfosPanier(\"idCLNOM\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLCPL' id='lbCLCPL'>Nom du responsable</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLCPL' id='idCLCPL' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLCPL() + "\" onBlur='majInfosPanier(\"idCLCPL\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLRUE' id='lbCLRUE'>Adresse - rue</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLRUE' id='idCLRUE' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLRUE() + "\" onBlur='majInfosPanier(\"idCLRUE\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLLOC' id='lbDateRetrait'>Adresse - localité</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLLOC' id='idCLLOC' maxlength ='30' value=\""
            + utilisateur.getMonPanier().getCLLOC() + "\" onBlur='majInfosPanier(\"idCLLOC\");'/><br/>"
            + "<label  class='labelsModeRecup' for='idCLVIL' id='lbDateRetrait'>Ville</label>"
            + "<input  class='zonesBlocAdresse' type='text' name='CLPOS' id='idCLPOS' maxlength ='5' minlength ='5' onKeyPress='only_numeric(event)' value=\""
            + utilisateur.getMonPanier().getCLPOS() + "\" onBlur='majInfosPanier(\"idCLPOS\")'/>&nbsp;"
            + "<input  class='zonesBlocAdresse' type='text' name='CLVIL' id='idCLVIL' maxlength ='24' value=\""
            + utilisateur.getMonPanier().getCLVIL() + "\" onBlur='majInfosPanier(\"idCLVIL\");'/><br/>");
      }
      // commentaires
      retour.append("<label  class='labelsModeRecup' for='idDetailsRetrait' id='lbDateRetrait'>Commentaires</label>"
          + "<textarea id='idDetailsRetrait' maxlength ='360' name='detailsRetrait'  " + "onBlur='majInfosPanier(\"idDetailsRetrait\");'>"
          + utilisateur.getMonPanier().getDetailsRecuperation() + "</textarea>" + "</form>");
      retour.append("</div>");
    }
    else {
      return "";
    }
    
    return retour.toString();
  }
  
  /**
   * Afficher la fenêtre qui permet de choisir/changer le magasin de retrait
   */
  private String afficherChoixMagasins(UtilisateurWebshop utilisateur) {
    String retour = "";
    
    if (utilisateur != null) {
      ArrayList<GenericRecord> liste = ((GestionMonPanier) maGestion).recupererListeMagasins(utilisateur);
      if (liste != null) {
        retour +=
            "<div class='accordeon2' id='choixMagasin'><a class='ouvrirAccordeonG' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChoixMagasin');\">&nbsp;</a><a class='titreAccordeon' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChoixMagasin');\">Changement du magasin de retrait</a> <a class='ouvrirAccordeonD' href=\"javascript:void(0)\" onClick=\"gererOuvertureAccordeon(this,'contenuChoixMagasin');\">&nbsp;</a></div>";
        retour += "<div class='contenuAccordeon2' id='contenuChoixMagasin'>";
        
        for (int i = 0; i < liste.size(); i++) {
          retour += "<a class='ligneAccordeon' href='MonPanier?nvMagasin=" + liste.get(i).getField("MG_COD").toString()
              + "'><span class='libMagasinPanier'>Magasin - <span class='magImportant'>" + liste.get(i).getField("MG_NAME")
              + "</span> -</span><span class='btMagasinPanier'>&nbsp;</span></a>";
        }
        
        retour += "</div>";
      }
    }
    else {
      return "";
    }
    
    return retour;
  }
  
  /**
   * afficher les articles avec les bons stocks dans les bonnes rubriques de
   * disponibilité
   */
  private String afficherArticles(UtilisateurWebshop utilisateur) {
    String retour = "";
    Float stockDispo = null;
    Float stockRetrait = null;
    Float stockSiege = null;
    
    if (utilisateur == null || utilisateur.getMonPanier() == null) {
      return retour;
    }
    
    if (utilisateur.getMonPanier().getModeRecup() > MarbreEnvironnement.MODE_NON_CHOISI
        && utilisateur.getMonPanier().getNombreArticle() > 0) {
      retour += "<div class='rubriquePanier'>";
      
      String typeDemande = "";
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT
          || utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
        typeDemande = "commande";
      }
      else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_DEVIS) {
        typeDemande = "devis";
      }
      
      retour += "<h3 id='ancreArticles'><span class='puceH3'>&nbsp;</span>Les articles de votre " + typeDemande + "</h3>";
      
      // Si cette liste n'est pas vide
      if (!utilisateur.getMonPanier().getListeArticles().isEmpty()) {
        // entêtes de colonnes
        retour += "<div class='listesPanier' id='enTeteListes'>" + "<span id='titreCAREF2'>Référence</span>"
            + "<span id='titreA1LIB2'>Libellé article</span>" + "<span id='titreSTOCK2'>Dispo.</span>" + "<span id='titreTARIF2'>Tarif("
            + utilisateur.getUS_DEV() + ")</span>"
            + "<span id='titreActionsP'>Quantité</span><span id='titreTOTALLIGNE'>Total H.T</span></div>";
        
        ArrayList<PanierArticle> listeDispo = null;
        ArrayList<PanierArticle> listePartiel = null;
        ArrayList<PanierArticle> listeIndispo = null;
        
        /************************ On attribue les bonnes catégories ***********************************************/
        /* DLC */
        for (PanierArticle articlePanier : utilisateur.getMonPanier().getListeArticles()) {
          if (articlePanier instanceof PanierArticleNormal && ((PanierArticleNormal) articlePanier).getTypeArticle().equals("C")) {
            PanierArticleNormal panierarticle = (PanierArticleNormal) articlePanier;
            stockDispo = 0.0f;
            stockRetrait = 0.0f;
            stockSiege = 0.0f;
            
            // ON CALCULE LES STOCKS QUAND NECESSAIRE
            // le stock de retrait est monté systématiquement quand on est en mode retrait
            if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
              stockRetrait = ((GestionMonPanier) maGestion).retourneStockPourUnArticle(utilisateur, panierarticle.getA1etb(),
                  panierarticle.getA1art(), utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
            }
            // Le stock du magasin siège est monté si MODE LIVRAISON ou (MODE RETRAIT et PAS MONO MAG et MAG <> SIEGE)
            if (utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_RETRAIT
                || ((utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT)
                    && (!utilisateur.getEtablissementEnCours().is_mono_magasin()) && (!utilisateur.getMonPanier().getMagasinPanier()
                        .getCodeMagasin().toString().equals(utilisateur.getMagasinSiege().getCodeMagasin())))) {
              stockSiege = ((GestionMonPanier) maGestion).retourneStockPourUnArticle(utilisateur, panierarticle.getA1etb(),
                  panierarticle.getA1art(), utilisateur.getMagasinSiege().getCodeMagasin());
            }
            
            // ON EN DEDUIT LE DISPONIBLE EN FONCTION DES CAS Retrait
            if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
              // si le retrait est (MAG + SIEGE)
              if (utilisateur.getEtablissementEnCours().isRetrait_is_siege_et_mag()) {
                stockDispo = stockRetrait + stockSiege;
              }
              else {
                stockDispo = stockRetrait;
              }
            }
            // livraison ou devis
            else {
              stockDispo = stockSiege;
            }
            
            Trace.debug("+++++++++++++++++++++ Panier ");
            Trace.debug("++ magasinRetrait: " + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin());
            Trace.debug("++ stockRetrait: " + stockRetrait.toString());
            Trace.debug("++ magasinSiege: " + utilisateur.getMagasinSiege().getLibelleMagasin());
            Trace.debug("++ stockSiege: " + stockSiege.toString());
            Trace.debug("++ stockDispo: " + stockDispo.toString());
            
            if (stockDispo < 0) {
              stockDispo = Float.valueOf("0");
            }
            panierarticle.setStockDispo(stockDispo);
            
            ((GestionMonPanier) maGestion).majInfosArticle(utilisateur, panierarticle);
            
            if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
              // Articles directement disponibles
              if (Float.parseFloat(panierarticle.getQuantite().toString()) <= stockRetrait) {
                if (listeDispo == null) {
                  listeDispo = new ArrayList<PanierArticle>();
                }
                // On rajoute cet article dans cette catégorie
                listeDispo.add(articlePanier);
              }
              // Articles insuffisemment disponibles en magasin de retrait
              else if ((Float.parseFloat(panierarticle.getQuantite().toString()) > stockRetrait)
                  && (Float.parseFloat(panierarticle.getQuantite().toString()) <= (stockRetrait + stockSiege))) {
                if (listePartiel == null) {
                  listePartiel = new ArrayList<PanierArticle>();
                }
                
                listePartiel.add(articlePanier);
              }
              // Articles indisponibles
              else if (stockDispo == 0 || Float.parseFloat(panierarticle.getQuantite().toString()) > (stockRetrait + stockSiege)) {
                if (listeIndispo == null) {
                  listeIndispo = new ArrayList<PanierArticle>();
                }
                
                listeIndispo.add(articlePanier);
              }
            }
            else {
              // Articles directement disponibles
              if (Float.parseFloat(panierarticle.getQuantite().toString()) <= stockSiege) {
                if (listeDispo == null) {
                  listeDispo = new ArrayList<PanierArticle>();
                }
                // On rajoute cet article dans cette catégorie
                listeDispo.add(articlePanier);
              }
              // Articles indisponibles
              else if (stockSiege == 0 || Float.parseFloat(panierarticle.getQuantite().toString()) > stockSiege) {
                if (listeIndispo == null) {
                  listeIndispo = new ArrayList<PanierArticle>();
                }
                
                listeIndispo.add(articlePanier);
              }
            }
          }
        }
        
        /************************
         * On affiche les articles et leurs commentaires
         ******************************************/
        if (listeDispo != null) {
          if (utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_LIVRAISON) {
            retour +=
                "<div class='listesCommentaires'><span class='txtCommentaires'>Ces articles sont directement disponibles dans votre magasin</span></div>";
          }
          else {
            retour +=
                "<div class='listesCommentaires'><span class='txtCommentaires'>Ces articles vous seront livrés directement</span></div>";
          }
          // }
          retour += afficherUneLigneDarticle(listeDispo, utilisateur);
        }
        if (listePartiel != null) {
          if (utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_LIVRAISON) {
            if (MarbreEnvironnement.VOIR_TABLE_DELAIS && !utilisateur.getEtablissementEnCours().is_mono_magasin()) {
              // Ne rien faire
            }
            else {
              retour +=
                  "<div class='listesCommentaires'><span class='txtCommentaires' id='spanRouge'>La quantité demandée pour ces articles n'est que partiellement disponible</span></div>";
            }
          }
          else {
            retour +=
                "<div class='listesCommentaires'><span class='txtCommentaires' id='spanRouge'>La quantité demandée pour ces articles n'est que partiellement disponible</span></div>";
          }
          
          retour += afficherUneLigneDarticle(listePartiel, utilisateur);
        }
        if (listeIndispo != null) {
          retour +=
              "<div class='listesCommentaires'><span class='txtCommentaires'  id='spanRouge'>La quantité demandée pour ces articles est indisponible</span></div>";
          retour += afficherUneLigneDarticle(listeIndispo, utilisateur);
        }
      }
      retour += "</div>";
    }
    return retour;
  }
  
  /**
   * Affiche le détail d'une ligne à partir d'une liste d'articles
   */
  private String afficherUneLigneDarticle(ArrayList<PanierArticle> liste, UtilisateurWebshop utilisateur) {
    StringBuilder retour = new StringBuilder(2000);
    PanierArticleNormal panierarticle = null;
    String blocEcoTaxe = null;
    int nbFinalQ = 0;
    for (int i = 0; i < liste.size(); i++) {
      if (liste.get(i) instanceof PanierArticleNormal) {
        panierarticle = (PanierArticleNormal) liste.get(i);
        blocEcoTaxe = null;
        // Liste articles
        retour.append("<div class='listesPanier'>");
        if (utilisateur.getEtablissementEnCours().getZone_reference_article().equals("CAREF") && panierarticle.getRefFournisseur() != null
            && !panierarticle.getRefFournisseur().trim().equals("")) {
          retour.append("<a class='CAREF2' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + liste.get(i).getA1etb().trim() + "\",\"A1ART\",\"" + liste.get(i).getA1art() + "\");'>"
              + panierarticle.getRefFournisseur() + "</a>");
        }
        else {
          retour.append("<a class='CAREF2' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + liste.get(i).getA1etb().trim() + "\",\"A1ART\",\"" + liste.get(i).getA1art() + "\");'>" + panierarticle.getA1art()
              + "</a>");
        }
        
        retour.append("<a class='A1LIB2' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
            + liste.get(i).getA1etb().trim() + "\",\"A1ART\",\"" + liste.get(i).getA1art() + "\");'>" + panierarticle.getLibelleArt()
            + "</a>");
        
        if (utilisateur.isStockDisponibleVisible()) {
          retour.append("<span class='STOCK2'>"
              + afficherStockCorrectement(utilisateur, panierarticle.getStockDispo().toString(), panierarticle.getNbDecimalesQ())
              + "</span>");
        }
        
        nbFinalQ = MarbreAffichage.SAISIE_MAX_QTE_ART;
        if (panierarticle.getNbDecimalesQ() > 0) {
          nbFinalQ = MarbreAffichage.SAISIE_MAX_QTE_ART + panierarticle.getNbDecimalesQ() - 1;
        }
        retour.append(
            "<span class='TARIF2'>" + Outils.formaterUnTarifArrondi(utilisateur, panierarticle.getTarifHTUnitaireCalcul()) + "</span>");
        retour.append("<span class='quantiteArticleP'><input id='saisieQ" + panierarticle.getA1art() + i
            + "' type ='text' class='saisieQuantiteP' onClick='this.select();' maxlength = '" + nbFinalQ
            + "' onKeyPress=\"recupererZoneInitiale(event,'saisieQ" + panierarticle.getA1art() + i
            + "');\" onKeyup=\"controle_quantiteDecimales(event,'saisieQ" + panierarticle.getA1art() + i + "', "
            + panierarticle.getNbDecimalesQ() + "," + nbFinalQ + ")\" onChange=\"modifQuantite('" + panierarticle.getA1art()
            + "','saisieQ" + panierarticle.getA1art() + i + "','" + panierarticle.getConditionnM() + "','" + panierarticle.getQuantite()
            + "'," + panierarticle.getNbDecimalesQ() + ");\"  tabindex=" + (i + 1) + " value=" + panierarticle.getQuantiteFormatee()
            + "  />");
        retour.append("</span>");
        retour.append(
            "<span class='TOTALLIGNE'>" + Outils.formaterUnTarifArrondi(utilisateur, panierarticle.getTotalPourQuantite()) + "</span>");
        retour.append("<a class='suppPanier'  href='MonPanier?SupArticle=" + panierarticle.getA1art() + "&SupEtbArt="
            + panierarticle.getA1etb() + "&supQuantite=" + panierarticle.getQuantite() + "#ancreArticles'>&nbsp;</a>");
        
        // on prépare le bloc ECOTAXE
        if (utilisateur.getEtablissementEnCours().isGestion_ecotaxe()
            && panierarticle.getTotalEcotaxe().compareTo(BigDecimal.ZERO) == 1) {
          blocEcoTaxe = "<div class='ecotaxeP'>dont écotaxe: "
              + Outils.formaterUnTarifArrondi(utilisateur, panierarticle.getTotalEcotaxe()) + "</div>";
        }
        // CONDITIONNEMENT
        if (panierarticle.getConditionnM() > 1 || blocEcoTaxe != null) {
          retour.append(afficherConditionnement(utilisateur, String.valueOf(panierarticle.getConditionnM())));
        }
        // ON affiche l'ECOTAXE APRES le conditionnement
        if (blocEcoTaxe != null) {
          retour.append(blocEcoTaxe);
        }
        
        retour.append("</div>");
      }
    }
    
    return retour.toString();
  }
  
  /**
   * afficher les informations fins de bon
   */
  private String afficherFinDeBon(UtilisateurWebshop utilisateur) {
    String retour = "";
    String typeDemande = "panier";
    int typeValidation = 2;
    if (utilisateur == null || utilisateur.getMonPanier() == null) {
      return retour;
    }
    
    // Si le mode de récupération est choisi
    if (utilisateur.getMonPanier().getModeRecup() > MarbreEnvironnement.MODE_NON_CHOISI) {
      // Tester si c'est un devis ou une commande
      String texteBoutonValidation = "";
      String texteBoutonAnnulation = "";
      if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_DEVIS) {
        typeDemande = "devis";
        texteBoutonValidation = utilisateur.getTraduction().traduire("$$validerDevis");
        texteBoutonAnnulation = utilisateur.getTraduction().traduire("$$annulerDevis");
        typeValidation = 1;
      }
      else {
        typeDemande = "commande";
        texteBoutonValidation = utilisateur.getTraduction().traduire("$$validerCommande");
        texteBoutonAnnulation = utilisateur.getTraduction().traduire("$$annulerCommande");
      }
      
      retour += "<h3><span class='puceH3'>&nbsp;</span>Fin de " + typeDemande + "</h3>";
      retour += "<div class='rubriquePanier' id='finDeBon'>";
      
      if (utilisateur.getMonPanier().getNombreArticle() > 0) {
        retour += "<p class='messageRubrique'>Bilan de votre " + typeDemande + "</p>";
        retour += "<div class='listesPanier' id='bilanPanier'>";
        retour += "<span id='totalHTPanier'>Total HT : "
            + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalHT()) + " </span>";
        if (utilisateur.getEtablissementEnCours().isGestion_ecotaxe()
            && utilisateur.getMonPanier().getTotalEcotaxe().compareTo(BigDecimal.ZERO) == 1) {
          retour += "<span id='totalECOTAX'>Dont écotaxe : "
              + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalEcotaxe()) + " </span>";
        }
        retour += "</div>";
        retour += afficherFrancoPort(utilisateur);
        
        // Ajouter le bouton "Valider"
        retour += "<div class='divDeBoutons'>"
            + "<a class='btRecuperation' id='validerbon' href='javascript:void(0)' onClick=\"traitementEnCours('MonPanier?validation="
            + typeValidation + "');\" >" + "<span class='deco_bt' id='deco_bt_valider'>&nbsp;</span><span class='texte_bt'>"
            + texteBoutonValidation + "</span>" + "</a>" + "</div>";
      }
      else {
        retour += "<p class='messageRubrique'>Votre " + typeDemande + " ne contient aucun article</p>";
      }
      
      retour +=
          "<div class='divDeBoutons'><a class='btRecuperation' id='plusArticles' href='catalogue?retour=1'><span class='deco_bt' id='deco_bt_plusArt'>&nbsp;</span><span class='texte_bt'>Ajouter des articles</span></a></div>";
      
      // Ajouter le bouton "Annuler"
      retour += "<div class='divDeBoutons'>" + "<a class='btRecuperation' id='annulerBon' href='MonPanier?validation=0'>"
          + "<span class='deco_bt' id='deco_bt_annuler'>&nbsp;</span>" + "<span class='texte_bt'>" + texteBoutonAnnulation
          + "</span></a></div>";
      retour += "</div>";
    }
    
    return retour;
  }
  
  /**
   * Afficher la ligne de conditionnement si nécessaire
   */
  private String afficherConditionnement(UtilisateurWebshop utilisateur, String conditionnement) {
    String retour = "";
    retour += "<div class='conditionnement'>";
    try {
      if (Double.parseDouble(String.valueOf(conditionnement)) > 1) {
        retour += "Vendu par conditionnement de " + Outils.afficherValeurCorrectement(utilisateur, conditionnement);
      }
      else {
        retour += "&nbsp;";
      }
    }
    catch (Exception e) {
      retour += "&nbsp;";
    }
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher le message d'avertissement concernant le franco Frais de port :
   * Si le montant HT de la commande est inférieur au montant Franco alors on affiche le message d'avertissement
   */
  private String afficherFrancoPort(UtilisateurWebshop utilisateur) {
    String messageRetour = "";
    
    if (utilisateur == null || utilisateur.getClient() == null || utilisateur.getMonPanier() == null) {
      return messageRetour;
    }
    
    if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
      return messageRetour;
    }
    
    BigDecimal montantHT = utilisateur.getMonPanier().getTotalHT();
    BigDecimal francoMini = utilisateur.getClient().getFrancoPort();
    
    if (montantHT == null || francoMini == null) {
      return messageRetour;
    }
    
    String messageSpecifique = utilisateur.getTraduction().traduire("$$francoPort");
    
    if (francoMini.compareTo(montantHT) > 0 && (messageSpecifique != null && !messageSpecifique.trim().isEmpty())) {
      messageRetour += "<div id='francoPanier'>";
      messageRetour += "Franco = " + Outils.formaterUnTarifArrondi(utilisateur, francoMini);
      messageRetour += messageSpecifique;
      messageRetour += "</div>";
    }
    
    return messageRetour;
  }
  
}
