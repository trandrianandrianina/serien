/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionCatalogueBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class catalogueBO
 */

public class catalogueBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public catalogueBO() {
    super(new GestionCatalogueBO(), "catalogueBO",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "catalogueBO.css' rel='stylesheet'/>", MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (!ServletFileUpload.isMultipartContent(request)) {
        if (request.getParameter("Maj") != null) {
          out.println(afficherContenu(
              traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Paramètres du catalogue</span></h2>"), null, null,
              null, request.getSession()));
        }
        else {
          out.println(afficherContenu(
              afficherAccueilAffichage(request.getSession(), "<h2><span id='titrePage'>Paramètres du catalogue</span></h2>", request),
              null, null, null, request.getSession()));
        }
      }
      else {
        out.println(
            afficherContenu(traiterUpload(request.getSession(), request, "<h2><span id='titrePage'>Paramètres du catalogue</span></h2>",
                request.getParameter("fileUpload")), null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le formulaire pour modifier l'affichage du catalogue
   * 
   */
  private String afficherAccueilAffichage(HttpSession session, String titre, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    String selOui = "";
    String selNon = "";
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ArrayList<GenericRecord> liste =
          ((GestionCatalogueBO) maGestion).recupererParametreAffichage((UtilisateurWebshop) session.getAttribute("utilisateur"));
      if (liste != null) {
        retour.append("<form action='CatalogueBO' method='POST'>");
        for (int i = 0; i < liste.size(); i++) {
          retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_LIB").toString().trim()
              + "</label>");
          retour.append("<select name='" + liste.get(i).getField("EN_CLE") + "' id='choixan' size='1' >");
          
          if (liste.get(i).getField("EN_VAL").toString().trim().equals("1")) {
            selOui = "selected=*selected";
            selNon = " ";
          }
          else {
            selOui = " ";
            selNon = "selected=*selected";
          }
          
          retour.append("<option " + selOui + " value='1'>" + "Oui" + "</option>");
          retour.append("<option " + selNon + " value='0'>" + "Non" + "</option>");
          
          retour.append("</select>");
          retour.append("</div>");
          
        }
      }
      retour.append("<input type='hidden'  id='Maj' name='Maj' value='1'>");
      retour.append("<input class='btnMAJ' type='submit' value='Mettre à jour' id='Maj'/>");
      retour.append("</form>");
      retour.append(formulaireUpload("<h2><span id='titrePage'>Catalogue BO </span></h2>", request));
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * traiter la mise à jour de l'affichage du catalogue
   */
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    StringBuilder retour = new StringBuilder();
    
    if (titre != null) {
      retour.append(titre);
    }
    retour.append("<div class='blocContenu'>");
    if (((GestionCatalogueBO) maGestion).miseajourParametreAffichage((UtilisateurWebshop) session.getAttribute("utilisateur"),
        request) != null) {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    retour.append(afficherAccueilAffichage(session, "", request));
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * formulaire d'upload
   * @param titre
   * @param request
   * 
   * @return
   */
  private String formulaireUpload(String titre, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String filename = "";
    if (request.getParameter("fileUpload") != null) {
      filename = request.getParameter("fileUpload");
    }
    
    /*formulaire d'upload*/
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Images à Uploader</h3>");
    // envoie des données binaires
    retour.append("<form action='catalogueBO' method='post' enctype='multipart/form-data'>"); // enctype='multipart/form-data'>");
    retour.append("<label for = 't1' class='textLabelImage'> Modifier les images du catalogue</label>");
    retour.append("<label for = 't1' class='textLabel'>Image à uploader :  </label>");
    retour.append("<input type='file' name='fileUpload' class='fileUpload' value='" + filename + "'/>");
    retour.append("<input type='submit' value='Uploader' class='btnMAJ'/>");
    
    retour.append("</form>");
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * traitement de l'upload
   * 
   */
  private String traiterUpload(HttpSession session, HttpServletRequest request, String titre, String name) {
    String chaine = "";
    
    chaine += "<div class='blocContenu'>";
    
    if (((GestionCatalogueBO) maGestion).upload((UtilisateurWebshop) session.getAttribute("utilisateur"), request, name) == -1) {
      // chaine+=("<script> alert(\"L'upload s'est correctement déroulé\")</script>");
      chaine += ("<div id='messagePrincipal'>Upload du fichier en erreur</div>");
    }
    else {
      chaine += ("<div id='messagePrincipal'>Upload du fichier effectué avec succès</div>");
      // chaine+=("<script> alert(\"L'upload à échoué. Merci de recommencer\")</script>");
    }
    
    chaine += "</div>";
    
    return chaine;
    
  }
  
}
