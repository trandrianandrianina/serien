/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionRechercheArticleBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class RechercheArticleBO
 */
public class RechercheArticleBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public RechercheArticleBO() {
    super(new GestionRechercheArticleBO(), "rechercheArticleBO",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "rechercheArticleBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("rechercheArticleBO", "rechercheArticleBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficherListeCriteres(request, "<h2><span id='titrePage'>Gestion du Web Shop</span></h2>"),
          "rechercheArticleBO", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des critères de recherches à sélectionner pour trouver des articles dans le moteur de recherche articles
   */
  private String afficherListeCriteres(HttpServletRequest request, String pTitre) {
    
    UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
    StringBuilder retour = new StringBuilder();
    retour.append(pTitre);
    
    String message = null;
    if (request.getParameter("majForm") != null) {
      message = afficherResultatMajCriteres(utilisateur, request);
    }
    
    String checkA1ART = " ";
    String checkCAREF = " ";
    String checkA1LIB = " ";
    String checkA1CL1 = " ";
    String checkA1CL2 = " ";
    
    ArrayList<GenericRecord> listeCriteres = ((GestionRechercheArticleBO) maGestion).retournerListeCriteres(utilisateur);
    if (listeCriteres != null) {
      for (GenericRecord record : listeCriteres) {
        if (record.isPresentField("RE_CODE")) {
          if (record.getField("RE_CODE").toString().trim().equals("A1ART")) {
            if (record.isPresentField("RE_VAL") && record.getField("RE_VAL").toString().trim().equals("1")) {
              checkA1ART = " checked ";
            }
          }
          else if (record.getField("RE_CODE").toString().trim().equals("CAREF")) {
            if (record.isPresentField("RE_VAL") && record.getField("RE_VAL").toString().trim().equals("1")) {
              checkCAREF = " checked ";
            }
          }
          else if (record.getField("RE_CODE").toString().trim().equals("A1LIB")) {
            if (record.isPresentField("RE_VAL") && record.getField("RE_VAL").toString().trim().equals("1")) {
              checkA1LIB = " checked ";
            }
          }
          else if (record.getField("RE_CODE").toString().trim().equals("A1CL1")) {
            if (record.isPresentField("RE_VAL") && record.getField("RE_VAL").toString().trim().equals("1")) {
              checkA1CL1 = " checked ";
            }
          }
          else if (record.getField("RE_CODE").toString().trim().equals("A1CL2")) {
            if (record.isPresentField("RE_VAL") && record.getField("RE_VAL").toString().trim().equals("1")) {
              checkA1CL2 = " checked ";
            }
          }
        }
      }
    }
    
    retour.append("<div class='blocContenu'>");
    if (message != null) {
      retour.append(message);
    }
    retour.append("" + "<form id='formCriteres' name='criteresRecherche' action='RechercheArticleBO' method='POST'>"
        + "<p id='texteRechercheArticle'>Sélectionnez les notions article qui doivent être scannées par le moteur de recherche du WebShop : </p>"
        + "<label>Code article de Série N</Label><input type='checkbox' name='codeArticle' class ='chkRechArticle' value='1' "
        + checkA1ART + " /><br/>"
        + "<label>Code article du fournisseur</Label><input type='checkbox' name='referenceFour' class ='chkRechArticle'  value='1' "
        + checkCAREF + "/><br/>"
        + "<label>1er libellé article</Label><input type='checkbox' name='libelleArticle' class ='chkRechArticle'  value='1' "
        + checkA1LIB + "/><br/>"
        + "<label>Mot de classement 1 article</Label><input type='checkbox' name='classement1' class ='chkRechArticle'  value='1' "
        + checkA1CL1 + "/><br/>"
        + "<label>Mot de classement 2 article</Label><input type='checkbox' name='classement2' class ='chkRechArticle'  value='1' "
        + checkA1CL2 + "/><br/>" + "<input type='hidden'  id='Maj' name='majForm' value='1'>"
        + "<input type='button' class='btnMAJ' name='validerRech' id='validerUneRech' value='valider les criteres' onClick=\"traitementForm(document.criteresRecherche);\" /><br/>"
        + "</form>");
    
    return retour.toString();
  }
  
  /**
   * Afficher le resultat de la mis à jour deu formulaire après son execution
   */
  private String afficherResultatMajCriteres(UtilisateurWebshop pUtilisateur, HttpServletRequest pRequest) {
    String retour = "<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>";
    
    if (pRequest.getParameter("codeArticle") == null && pRequest.getParameter("referenceFour") == null
        && pRequest.getParameter("libelleArticle") == null && pRequest.getParameter("classement1") == null
        && pRequest.getParameter("classement2") == null) {
      retour = "<div class='messageAlerte'>Vous devez choisir au moins un critère</div>";
    }
    
    int codeRetour = ((GestionRechercheArticleBO) maGestion).majCriteresRechercheArticle(pUtilisateur, pRequest);
    
    if (codeRetour != 1) {
      retour = "<div class='messageAlerte'>Erreur de mise à jour des critères de recherche</div>";
    }
    
    return retour;
  }
  
}
