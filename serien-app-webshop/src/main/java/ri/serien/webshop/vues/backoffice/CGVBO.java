/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionCGVBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class CGVBO
 */
public class CGVBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public CGVBO() {
    super(new GestionCGVBO(), "cgvBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "cgvBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      // * si mes paramètres sont vides j'affiche le formulaire sinon je traite les mises à jours
      if (request.getParameter("conditions") != null) {
        out.println(
            afficherContenu(traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Conditions générales</span></h2>"),
                null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(
            afficherFormulaire(request.getSession(), "<h2><span id='titrePage'>Conditions générales</span></h2>", request), null, null,
            null, request.getSession()));
        
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le formulaire de saisir des CGV
   * 
   */
  private String afficherFormulaire(HttpSession session, String titre, HttpServletRequest request) {
    // String retour = titre;
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      // On affiche les codes langues utilisés pour ce serveur
      ((UtilisateurWebshop) session.getAttribute("utilisateur"))
          .setListeDeTravail(maGestion.retournerLanguesWS((UtilisateurWebshop) session.getAttribute("utilisateur")));
      if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail() != null
          && ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().size() > 0) {
        String langue = "fr";
        if (request.getParameter("codeLangue") != null) {
          langue = request.getParameter("codeLangue");
        }
        
        retour.append("<form action='CGVBO' method='post' name='nvChoixLangue' id='formChoixLangue'>");
        retour.append("<select name='codeLangue' onChange='document.nvChoixLangue.submit();'>"); // onChange='document.choixLangue.submit();
        String selectedOpt = "";
        for (int i = 0; i < ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().size(); i++) {
          if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
              .equals(langue)) {
            selectedOpt = "selected='selected'";
          }
          else {
            selectedOpt = "";
          }
          retour.append("<option value='"
              + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID") + "' "
              + selectedOpt + ">"
              + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB") + "</option>");
        }
        retour.append("</select>");
        retour.append("</form>");
        
        ((UtilisateurWebshop) session.getAttribute("utilisateur")).recupererUnRecordTravail(
            ((GestionCGVBO) maGestion).recupererCgv((UtilisateurWebshop) session.getAttribute("utilisateur"), langue));
        retour.append("<form action='CGVBO' method='post'>");
        String contenu = null;
        if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail() != null
            && ((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail().isPresentField("CGV_NAME")) {
          contenu = ((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail().getField("CGV_NAME").toString().trim();
        }
        
        retour.append("<label class='textLabel'>Modifier vos conditions Générales " + langue + "  </label>");
        retour.append("<textarea name='conditions' class='zone' rows='30' > " + contenu + "</textarea>");
        retour.append("<input type='hidden' name='langueInfo' value='" + langue + "' />");
        retour.append(
            "<input class='btnMAJ' type='submit' value='Mettre à jour' id='conditions' href='javascript:void(0)' onClick=\"traitementEnCours('CGVBO');\"/>");
        retour.append("</form>");
      }
      retour.append("</div>");
    }
    return retour.toString();
  }
  
  /**
   * Traitement de la mise à jour des données
   * 
   */
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    String chaine = "";
    if (titre != null) {
      chaine += titre;
    }
    
    chaine += "<div class='blocContenu'>";
    
    if (((GestionCGVBO) maGestion).miseAjourCgv((UtilisateurWebshop) session.getAttribute("utilisateur"), request) > 0) {
      chaine += ("<div id='messagePrincipal'>CG de vente mises à jour avec succès.</div>");
    }
    else {
      chaine += ("<div id='messagePrincipal'>Erreur dans la mise à jour des Conditions générales</div>");
    }
    
    chaine += (afficherFormulaire(session, "", request));
    
    chaine += "</div>";
    
    return chaine;
    
  }
  
}
