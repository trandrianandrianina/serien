/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionEtablissements;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class Etablissements
 */
public class Etablissements extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Etablissements() {
    super(new GestionEtablissements(), "etablissements",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "etablissements.css' rel='stylesheet'/>", MarbreEnvironnement.ACCES_RESPON_WS);
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("Etablissements", "Etablissements", "Les établissements", ""));
    listeFilRouge.add(new FilRouge("etbId", "Etablissements?etbId=" + MarbreEnvironnement.PARAM_FILROUGE, "Un établissement", ""));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if (request.getParameter("etbId") != null) {
        out.println(afficherContenu(
            afficherUnETB((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Modification d'un mot clé</span></h2>", request, request.getParameter("etbId")),
            "etbId", null, null, request.getSession()));
      }
      else if (request.getParameter("codeETB") != null) {
        out.println(afficherContenu(traiterMiseajour((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), request),
            null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(afficherListeETB((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
            "<h2><span id='titrePage'>Etablissements</span></h2>", request, null), null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des établissements
   */
  private String afficherListeETB(UtilisateurWebshop user, String titre, HttpServletRequest request, String message) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String lienEtb = "";
    
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Les établissements &nbsp;&nbsp;</h3>");
    
    if (message != null) {
      retour.append("<p id='messagePrincipal'>" + message + "</p>");
    }
    
    user.setListeDeTravail(((GestionEtablissements) maGestion).recupereListeTousETBs(user));
    if (user.getListeDeTravail() != null && user.getListeDeTravail().size() > 0) {
      // entête
      retour.append("<div class='listes' id='enTeteListes'>"
          + "<span id='codEtb'>CODE ETB</span><span id='libellEtb'>LIBELLE</span><span id='etatEtb'>ETAT</span></div>");
      
      for (int i = 0; i < user.getListeDeTravail().size(); i++) {
        lienEtb = "Etablissements?etbId=" + user.getListeDeTravail().get(i).getField("ETB_ID").toString();
        retour.append("<div class='listes'>");
        
        // affiche le code etablissement
        retour.append("<a class='codEtb' href='" + lienEtb + "'>");
        if (user.getListeDeTravail().get(i).isPresentField("ETB_ID")) {
          retour.append(user.getListeDeTravail().get(i).getField("ETB_ID"));
        }
        else {
          retour.append("aucune");
        }
        retour.append("</a>");
        // affiche le libellé
        retour.append("<a class='libellEtb' href='" + lienEtb + "'>");
        if (user.getListeDeTravail().get(i).isPresentField("ETB_LIB")) {
          retour.append(user.getListeDeTravail().get(i).getField("ETB_LIB").toString().trim());
        }
        else {
          retour.append("aucune");
        }
        retour.append("</a>");
        // Afiiche l'état de l'établissement
        retour.append("<a class='etatEtb' href='" + lienEtb + "'>");
        if (user.getListeDeTravail().get(i).isPresentField("ETB_ACTIF")) {
          if (user.getListeDeTravail().get(i).getField("ETB_ACTIF").toString().trim().equals("1")) {
            retour.append("Actif");
          }
          else {
            retour.append("Inactif");
          }
        }
        else {
          retour.append("aucun");
        }
        retour.append("</a>");
        
        retour.append("</div>");
      }
      
    }
    else {
      retour.append("<p>Pas d'établissement</p>");
    }
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Méthode qui permet de traiter une mise à jour de fiche d'établissement
   */
  private String traiterMiseajour(UtilisateurWebshop utilisateur, HttpServletRequest request) {
    String retour = null;
    String message = null;
    int intMaj = -1;
    
    if (request.getParameter("codeETB") != null && request.getParameter("libETB") != null && request.getParameter("etatETB") != null) {
      // if(maGestion.isAlphanumerique(request.getParameter("libETB")))
      intMaj = ((GestionEtablissements) maGestion).majEtablissement(utilisateur, request.getParameter("codeETB"),
          request.getParameter("libETB"), request.getParameter("etatETB"));
      // else intMaj = -2;
    }
    
    if (intMaj == -2) {
      message = "Votre saisie est incorrecte [" + request.getParameter("libETB") + "]";
    }
    else if (intMaj == -1) {
      message = "Problème de passage de paramètre de l'établissement " + request.getParameter("codeETB");
    }
    else if (intMaj == 0) {
      message = "Problème de mise à jour de l'établissement " + request.getParameter("codeETB");
    }
    else if (intMaj == 1) {
      message = "Succès de la mise à jour de l'établissement " + request.getParameter("codeETB");
    }
    
    retour += afficherListeETB(utilisateur, "<h2><span id='titrePage'>Etablissements</span></h2>", request, message);
    
    return retour;
  }
  
  /**
   * Modifier un etablissement
   */
  private String afficherUnETB(UtilisateurWebshop user, String titre, HttpServletRequest request, String codeEtb) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Un établissement &nbsp;&nbsp;</h3>");
    GenericRecord unEtb = ((GestionEtablissements) maGestion).recupereUnEtB(user, codeEtb);
    
    if (unEtb != null && unEtb.isPresentField("ETB_ID") && unEtb.isPresentField("ETB_LIB") && unEtb.isPresentField("ETB_ACTIF")) {
      String actif = "";
      String inactif = "";
      
      retour.append("<form action='etablissements' method ='POST'>");
      
      retour.append("<label class='labelCODE'>Code ETB</label>");
      retour.append("<input type='text' id='fc_codeETB' name='codeETB'  maxlength='3' readonly=\"readonly\"  value='"
          + unEtb.getField("ETB_ID").toString().trim() + "' ><br/>");
      retour.append("<label class='labelCODE'>Libellé ETB</label>");
      retour.append("<input type='text' id='fc_libEtb' name='libETB'  maxlength='30' required value='"
          + unEtb.getField("ETB_LIB").toString().trim() + "' ><br/>");
      retour.append("<label class='labelCODE'>Visibilité ETB</label>");
      retour.append("<select id='fc_etatETB' name='etatETB'>");
      if (unEtb.getField("ETB_ACTIF").toString().trim().equals("0")) {
        inactif = "selected";
      }
      else {
        actif = "selected";
      }
      retour.append("<option value='0' " + inactif + ">Inactif</option>");
      retour.append("<option value='1' " + actif + ">Actif</option>");
      retour.append("</select><br/>");
      
      retour.append("<input class='btnMAJ' id='btModifETB' TYPE='submit' value='Modifier' />");
      
      retour.append("</form>");
    }
    else {
      retour.append("Pas d'établissement valide");
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
}
