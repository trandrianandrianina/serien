/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionMetierBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class MetierBO
 */
public class MetierBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public MetierBO() {
    super(new GestionMetierBO(), "metierBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "clientBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("Maj") != null) {
        out.println(afficherContenu(traiterMiseajour((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), request,
            "<h2><span id='titrePage'>Paramètres métier</span></h2>"), null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(
            afficherFormEtb((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Paramètres métier</span></h2>", request.getParameter("choixEtb")),
            null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des paramètres du client (XWEBSHOP/ENVIRONM)
   */
  private String afficherFormEtb(UtilisateurWebshop utilisateur, String titre, String etb) {
    if (utilisateur == null) {
      return "";
    }
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    // String retour = titre;
    retour.append("<div class='blocContenu'>");
    
    if (etb == null && utilisateur.getEtablissementEnCours() != null) {
      etb = utilisateur.getEtablissementEnCours().getCodeETB();
    }
    
    if (MarbreEnvironnement.ETB_DEFAUT != null && etb != null) {
      String selected = "";
      retour.append("<form action='MetierBO' method='POST' name='choixEtbs' id='choixEtbs'>");
      retour.append(
          "<select id='choixETBParams' name='choixEtb' onChange=\"document.location.href = 'MetierBO?choixEtb=' + this.value ;\">");
      if (MarbreEnvironnement.ETB_DEFAUT.getCodeETB().equals(etb)) {
        selected = " selected ";
      }
      else {
        selected = "";
      }
      retour.append("<option value='" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' " + selected + ">"
          + MarbreEnvironnement.ETB_DEFAUT.getLibelleETB() + "</option>");
      
      retour.append("</select>");
      retour.append("</form>");
      
      ArrayList<GenericRecord> liste = null;
      ArrayList<GenericRecord> listeMonETB = null;
      HashMap<String, String> listeValeurs = null;
      
      // ON travaille le pattern puis les valeurs déjà saisies pour les comparer
      liste = ((GestionMetierBO) maGestion).recupererPatternParametresEtb(utilisateur);
      listeMonETB = ((GestionMetierBO) maGestion).recupererParametresEtb(utilisateur, etb);
      if (listeMonETB != null) {
        listeValeurs = new HashMap<String, String>();
        
        for (int i = 0; i < listeMonETB.size(); i++) {
          if (listeMonETB.get(i).isPresentField("EN_CLE") && listeMonETB.get(i).isPresentField("EN_VAL")) {
            listeValeurs.put(listeMonETB.get(i).getField("EN_CLE").toString().trim(),
                listeMonETB.get(i).getField("EN_VAL").toString().trim());
          }
        }
      }
      
      if (liste != null && listeMonETB != null) {
        String typeDonnee = null;
        String valeur = "";
        
        retour.append(
            "<form action='MetierBO' method='POST' name='paramETB'>" + "<input type='hidden' name='etbCours' value = '" + etb + "'/>");
        for (int i = 0; i < liste.size(); i++) {
          // Conditionnement du champs type selon valeur de la zone EN_TYP pour conditionner le input
          if (liste.get(i).isPresentField("EN_TYP") && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD")) {
            // typeDonnee = "text" ;
            typeDonnee = (liste.get(i).getField("EN_TYP").toString().trim());
          }
          else {
            typeDonnee = "text";
          }
          
          if (liste.get(i).isPresentField("EN_CLE")) {
            if (listeValeurs != null && listeValeurs.containsKey(liste.get(i).getField("EN_CLE").toString().trim())) {
              if (liste.get(i).isPresentField("EN_TYP") && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD")) {
                valeur = Base64Coder
                    .decodeString(Base64Coder.decodeString(listeValeurs.get(liste.get(i).getField("EN_CLE").toString().trim())));
              }
              else {
                valeur = listeValeurs.get(liste.get(i).getField("EN_CLE").toString().trim());
              }
            }
            else {
              valeur = "";
            }
            
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + typeDonnee + "' class='valparam' name='" + liste.get(i).getField("EN_CLE").toString().trim()
                + "' value='" + valeur + "'/></div>");
          }
        }
        
        /*//forcer les 4 paramètres en Majuscule (environm, lettre_env, bibli et etb
        if(liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("ENVIRONNEMENT"))
        {
          environm = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
          retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + environm + "'/></div>");
        }
        else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("LETTRE_ENV"))
        {
          lettreTyp = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
          retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + lettreTyp + "'/></div>");
        }
        
        else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("BIBLI_CLIENTS"))
        {
          bibli = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
          retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + bibli + "'/></div>");
        }
        else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("ETB_CLIENTS"))
        {
          etb = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
          retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + etb + "'/></div>");
        }
        //forcer l'affichage du mot de passe en crypté
        else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD"))
        {
          password = (liste.get(i).getField("EN_VAL").toString().trim());
          if (password.length()<=10)
          {
            password = (liste.get(i).getField("EN_VAL").toString().trim());
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + password + "'/></div>");
          }
          else
          {
            password = Base64Coder.decodeString(Base64Coder.decodeString(liste.get(i).getField("EN_VAL").toString().trim()));
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + password + "'/></div>");
          }
        }
        else
          retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim() + "</label><input type='" + type + "' class='valparam' name='" + liste.get(i).getField("EN_CLE")+ "' value='" + liste.get(i).getField("EN_VAL").toString().trim() + "'/></div>");
        }*/
      }
      retour.append("<input type='hidden'  id='Maj' name='Maj' value='1'>");
      retour.append(
          "<input class='bouton' type='submit' value='Mettre à jour' id='MajP' href='javascript:void(0)' onClick=\"traitementEnCours('MetierBO');\"/>");
      retour.append("</form>");
    }
    
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * Traiter la mise à jour des paramètres du client
   */
  private String traiterMiseajour(UtilisateurWebshop utilisateur, HttpServletRequest request, String titre) {
    StringBuilder retour = new StringBuilder();
    
    if (titre != null) {
      retour.append(titre);
    }
    
    retour.append("<div class='blocContenu'>");
    
    if (((GestionMetierBO) maGestion).miseajourParametresEtb(utilisateur, request)) {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>Erreur dans la mise à jour des paramètres</div>");
    }
    retour
        .append(afficherFormEtb(utilisateur, "<h2><span id='titrePage'>Paramètres métier</span></h2>", request.getParameter("etbCours")));
    retour.append("</div>");
    
    return retour.toString();
  }
}
