/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionSonepar;

/**
 * Servlet implementation class Sonepar
 */
public class Sonepar extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Sonepar() {
    super(new GestionSonepar(), "sonepar", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "sonepar.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_PUBLIC);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      out.println(afficherContenu(afficherSonepar(request, "<h2><span id='titrePage'>Groupe Sonepar</span></h2>"), "sonepar", null, null,
          request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affiche l'ensemble des élements de la page Sonepar
   */
  private String afficherSonepar(HttpServletRequest request, String titre) {
    String retour = titre;
    retour += "<div class='blocContenu'>";
    
    retour += "</div>";
    
    return retour;
  }
  
}
