/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.math.BigDecimal;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionAccueil;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

/**
 * Servlet Accueil
 */
public class Accueil extends MouleVues {
  
  private static final int NB_PROMOS_PAR_PAGE = 8;
  
  public Accueil() {
    super(new GestionAccueil(), "accueil",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "accueil.css?" + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_PUBLIC);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      out.println(
          afficherContenu(afficherAccueil(request, "<h2><span id='titrePage'>" + MarbreEnvironnement.LIBELLE_SERVEUR + "</span></h2>"),
              "accueil", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affiche l'ensemble des élements de l'accueil
   */
  private String afficherAccueil(HttpServletRequest request, String titre) {
    if (request == null || request.getSession() == null) {
      return "";
    }
    
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String retour = titre;
    retour += "<div class='blocContenu' id='blocContenuAccueil'>";
    
    if (utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_REPRES_WS) {
      retour += afficherBlocIntroduction(utilisateur);
    }
    
    if (utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_REPRES_WS) {
      retour += afficherBlocPromotions(utilisateur, request);
    }
    
    retour += afficherBlocLiensGraphiques(utilisateur);
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Affiche les liens graphiques du WS
   */
  private String afficherBlocLiensGraphiques(UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      return "";
    }
    String retour = "";
    
    retour += "<div id='blocLiensGraphiques'>";
    retour += "<a href='catalogue' class='liensGraphiques' id='lienCatalogue'><img class='decoLienGraphique' src='"
        + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
        + utilisateur.getTraduction().traduire("$$catalogueArticles") + "</span><img src='"
        + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/decoration/lienCatalogue.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
        + utilisateur.getTraduction().traduire("$$catalogueArticles") + "</span></span></a>";
    if (utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_REPRES_WS) {
      retour += "<a href='magasins' class='liensGraphiques' id='lienMagasins'><img class='decoLienGraphique' src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
          + utilisateur.getTraduction().traduire("$$votreMagasin") + "</span><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/lienMagasin.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
          + utilisateur.getTraduction().traduire("$$votreMagasin") + "</span></span></a>";
    }
    if (utilisateur.getTypeAcces() < MarbreEnvironnement.ACCES_CLIENT) {
      retour += "<a href='connexion' class='liensGraphiques' id='lienConnexion'><img class='decoLienGraphique' src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
          + utilisateur.getTraduction().traduire("Connexion")
          + "</span><img src='images/decoration/lienConnexion.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
          + utilisateur.getTraduction().traduire("Connexion") + "</span></span></a>";
    }
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) {
      retour += "<a href='clients' class='liensGraphiques' id='lienClients'><img class='decoLienGraphique' src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
          + utilisateur.getTraduction().traduire("Clients")
          + "</span><img src='images/decoration/lienClients.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
          + utilisateur.getTraduction().traduire("Vos clients") + "</span></span></a>";
    }
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT
        || utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) {
      retour += "<a href='mesDonnees' class='liensGraphiques' id='lienHistorique'><img class='decoLienGraphique' src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
          + utilisateur.getTraduction().traduire("Historique")
          + "</span><img src='images/decoration/lienHistorique.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
          + utilisateur.getTraduction().traduire("Historique") + "</span></span></a>";
    }
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CONSULTATION) {
      retour += "<a class='liensGraphiques' id='lienHistoriqueC'><img class='decoLienGraphique' src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
          + utilisateur.getTraduction().traduire("Historique")
          + "</span><img src='images/decoration/lienHistorique.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
          + utilisateur.getTraduction().traduire("Historique") + "</span></span></a>";
    }
    if (utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_RESPON_WS) {
      retour += "<a href='accueilBO' class='liensGraphiques' id='lienBO'><img class='decoLienGraphique' src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>"
          + utilisateur.getTraduction().traduire("Gestion")
          + "</span><img src='images/decoration/lienGestion.png' class='imgliensGraphiques' /><span class='textLiensGraphiques'>"
          + utilisateur.getTraduction().traduire("Gestion") + "</span></span></a>";
    }
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Affiche les promotions s'il y en a
   */
  private String afficherBlocPromotions(UtilisateurWebshop utilisateur, HttpServletRequest pRequest) {
    String retour = "";
    String page = null;
    if (utilisateur == null) {
      return retour;
    }
    
    if (utilisateur.getListePromotionsActives() != null) {
      page = pRequest.getParameter("indice");
      retour += afficherLesPromotions(utilisateur, page);
    }
    else {
      retour += afficherImageAccueil(utilisateur);
    }
    
    return retour;
  }
  
  /**
   * Affiche la présentation du Web Shop
   */
  private String afficherBlocIntroduction(UtilisateurWebshop utilisateur) {
    String retour = "";
    String texteSpecifique = "";
    
    if (utilisateur == null) {
      return retour;
    }
    
    retour += "<div id='blocIntroduction'>";
    retour += "<div id='blocDecoBleue'></div>";
    
    if (!MarbreEnvironnement.LIBELLE_SERVEUR.equals("")) {
      texteSpecifique += "<span id='indelebile'>" + MarbreEnvironnement.LIBELLE_SERVEUR + "</span>";
    }
    
    retour += "<h4><span id='lookWeb'>Web</span> <span id='lookShop'>SHOP</span> " + texteSpecifique + "</h4>";
    utilisateur.recupererUnRecordTravail(GestionAccueil.recupereInfoAcc(utilisateur));
    
    if (utilisateur.getRecordTravail() != null) {
      retour += "<p id='texteIntroduction'>" + utilisateur.getRecordTravail().getField("ACC_NAME").toString().trim() + "</p>";
    }
    
    retour += "</div>";
    return retour;
  }
  
  /**
   * Affiche les promotions en cours
   */
  private String afficherLesPromotions(UtilisateurWebshop utilisateur, String pIndice) {
    String retour = "";
    String pagination = "&nbsp;";
    if (utilisateur == null) {
      return retour;
    }
    
    retour += "<div id='blocPromotions'>";
    retour += "<img class='decoLienGraphique' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>" + utilisateur.getTraduction().traduire("$$promos")
        + "</span>";
    
    if (utilisateur.getListePromotionsActives() != null) {
      // Gestion de la pagination
      int indMax = utilisateur.getListePromotionsActives().size();
      int indDebut = 0;
      int indFin = NB_PROMOS_PAR_PAGE;
      if (pIndice != null) {
        try {
          indDebut = Integer.parseInt(pIndice);
        }
        catch (NumberFormatException e) {
          indDebut = 0;
        }
      }
      
      indFin = indDebut + NB_PROMOS_PAR_PAGE;
      if (indFin > indMax) {
        indFin = indMax;
      }
      
      pagination = afficherPaginationPromos(indDebut, indMax);
      
      ArrayList<GenericRecord> listePartielle = new ArrayList<GenericRecord>();
      for (int i = indDebut; i < indFin; i++) {
        listePartielle.add(utilisateur.getListePromotionsActives().get(i));
      }
      
      String lienArticle = null;
      
      for (int i = 0; i < listePartielle.size(); i++) {
        lienArticle = null;
        
        if (utilisateur.getTypeAcces() < MarbreEnvironnement.ACCES_CLIENT) {
          lienArticle = "connexion";
        }
        else if (listePartielle.get(i).isPresentField("PR_A1ART")
            && !listePartielle.get(i).getField("PR_A1ART").toString().trim().equals("")) {
          lienArticle = "catalogue?A1ETB=" + listePartielle.get(i).getField("PR_A1ETB").toString().trim() + "&A1ART="
              + listePartielle.get(i).getField("PR_A1ART").toString().trim();
        }
        
        retour += "<div class='unePromo'>";
        
        if (listePartielle.get(i).isPresentField("PR_PRIX")) {
          if (((BigDecimal) listePartielle.get(i).getField("PR_PRIX")).compareTo(BigDecimal.ZERO) > 0) {
            retour += "<div class='prixPromo'>";
            retour += Outils.formaterUnTarifArrondi(utilisateur, listePartielle.get(i).getField("PR_PRIX"));
            retour += "</div>";
          }
        }
        
        // Les images doivent au mieux être en longu: 270px et larg: 160px
        if (lienArticle != null) {
          retour += "<a class='panierPromo' href='" + lienArticle + "'><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/panier.png'/></a>";
          retour += "<a class='imagePromotion' href='" + lienArticle + "'>";
        }
        else {
          retour += "<div class='imagePromotion'>";
        }
        
        retour += "<img class='imagePromo' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/promotions/promotion_"
            + listePartielle.get(i).getField("PR_ID") + ".jpg'/>";
        
        if (lienArticle != null) {
          retour += "</a>";
        }
        else {
          retour += "</div>";
        }
        
        retour += "<div class='detailsPromotion'>";
        if (listePartielle.get(i).isPresentField("PR_A1LIB")
            && !listePartielle.get(i).getField("PR_A1LIB").toString().trim().equals("")) {
          retour += "<span class='decoLibPromo'>&nbsp;</span><span class='detailLibPromo'>"
              + listePartielle.get(i).getField("PR_A1LIB").toString().trim() + "</span>";
        }
        retour += "<span class='textePromo'>" + listePartielle.get(i).getField("PR_TEXTE").toString().trim() + "</span>";
        retour += "<span class='validPromo'>Jusqu'au "
            + Outils.transformerDateSeriemEnHumaine(listePartielle.get(i).getField("PR_DATEF").toString().trim()) + "</span>";
        retour += "</div>";
        retour += "</div>";
      }
    }
    retour += "<div class='marginBas_Promo'>" + pagination + "</div>";
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Affiche une image de remplacement si il n'y a pas de promotion
   */
  private String afficherImageAccueil(UtilisateurWebshop utilisateur) {
    String retour = "";
    
    retour += "<div id='blocImageAccueil'>";
    retour += "<img class='decoLienGraphique' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/decoration/puceH3.png'/><span class='titreLienGraphique'>" + utilisateur.getTraduction().traduire("$$infosSociete")
        + "</span><a href='informations' id='lienInformations'><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/accueil.jpg' id='imgAccueil'/></a>";
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * afficher la pagination propre à la liste de pagination de l'utilisateur
   */
  protected String afficherPaginationPromos(int indiceDebut, int pTotal) {
    int nbPage = -1;
    int numPage = -1;
    int debItem = 0;
    Double parPage = Double.valueOf(NB_PROMOS_PAR_PAGE);
    Double totalDouble = Double.valueOf(pTotal);
    StringBuilder retour = new StringBuilder(400);
    nbPage = (int) Math.ceil(totalDouble / parPage);
    
    if (nbPage > 1) {
      retour.append("<div class='blocPagination'>");
      for (int p = 0; p < nbPage; p++) {
        numPage = p + 1;
        if (indiceDebut >= debItem && indiceDebut < debItem + NB_PROMOS_PAR_PAGE) {
          if (indiceDebut > 0) {
            retour.append("<span class='spanPagination'> < </span>");
          }
          
          retour.append("<span id='spanPageEnCours'>Page " + numPage + "</span>");
        }
        else {
          if (indiceDebut > debItem && debItem > 0) {
            retour.append("<span class='spanPagination'> < </span>");
          }
          else if (debItem != 0) {
            retour.append("<span> > </span>");
          }
          
          retour.append("<a class='aPagination' href='accueil?indice=" + debItem + "'>" + numPage + "</a>");
        }
        
        debItem += NB_PROMOS_PAR_PAGE;
      }
      retour.append("</div>");
    }
    else {
      retour.append("&nbsp;");
    }
    
    return retour.toString();
  }
  
}
