/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.GestionConnexion;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Servlet Connexion
 */
public class Connexion extends MouleVues {
  
  public Connexion() {
    super(new GestionConnexion(), "connexion",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "connexion.css?" + MarbreEnvironnement.versionInstallee
            + "' rel='stylesheet'/><script src='scripts/connexion.js?" + MarbreEnvironnement.versionInstallee + "'></script>",
        MarbreEnvironnement.ACCES_NON_DEPLOYE);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      
      UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
      // par défaut afficher un accès public
      int etatConnexion = MarbreEnvironnement.ACCES_PUBLIC;
      String retourMessage = "";
      
      ServletOutputStream out = response.getOutputStream();
      
      // Si on passe un message d'état de connexion
      if (utilisateur.getTypeAcces() < MarbreEnvironnement.ACCES_CLIENT) {
        if (utilisateur.getTentativesConnexions() > MarbreEnvironnement.MAX_TENT_CONNEX) {
          // Bloquer l'adresse IP que si on est dans une nouvelle session
          if (utilisateur.getTentativesConnexions() == MarbreEnvironnement.MAX_TENT_CONNEX + 1) {
            // NE pas repasser dans cette boucle avant une nouvelle session
            utilisateur.setTentativesConnexions(utilisateur.getTentativesConnexions() + 1);
          }
          etatConnexion = MarbreEnvironnement.BLOC_SAISIE_MAX;
        }
        else if (request.getParameter("UtilnvMP") != null) {
          etatConnexion = ((GestionConnexion) maGestion).validerChgtMP(request);
        }
        else if (request.getParameter("saisieNVMP") != null) {
          etatConnexion = MarbreEnvironnement.ACCES_ATT_NV_MP;
        }
        else if (request.getParameter("verifProfil") != null) {
          etatConnexion = ((GestionConnexion) maGestion).changerMotDePasseProfil(utilisateur, request.getParameter("verifProfil"),
              request.isSecure());
        }
        else if (request.getParameter("echec") != null) {
          etatConnexion = Integer.parseInt(request.getParameter("echec"));
        }
        else if (request.getParameter("validation") != null) {
          etatConnexion = ((GestionConnexion) maGestion).validationProfil(utilisateur, request.getParameter("validation"));
        }
        else if (request.getParameter("deploiement") != null) {
          etatConnexion = MarbreEnvironnement.ACCES_NON_DEPLOYE;
        }
        else if (request.getParameter("passUser") != null && request.getParameter("loginUser") != null) {
          etatConnexion = ((GestionConnexion) maGestion).majConnexion(request.getSession(), request.getParameter("loginUser"),
              request.getParameter("passUser"), request.getParameter("passUserCtrl"), request.getParameter("numeroClient"),
              request.getParameter("mobilitePluch"), request.isSecure());
        }
        // Capturer l'événement sur l'état d'une connexion
        // cet événement se produit lors de l'échec de connexion sur la barre de login de la page Accueil
        else if (request.getParameter("etatConnexion") != null) {
          etatConnexion = Constantes.convertirTexteEnInteger(request.getParameter("etatConnexion"));
        }
        else {
          etatConnexion = MarbreEnvironnement.ACCES_PUBLIC;
        }
        
        // En fonction de l'état d'accès de l'utilisateur afficher l'interface adaptée
        switch (etatConnexion) {
          case MarbreEnvironnement.ACCES_PAS_DB2:
            retourMessage =
                afficher_BLOQUAGE_MESSAGE(request, "WebShop inaccessible", "Pas d'accès DB2. Veuillez contacter votre agence");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_NON_DEPLOYE:
            retourMessage = afficher_NON_DEPLOYE(request, "Processus de déploiement", request.getParameter("deploiement"), utilisateur);
            break;
          case MarbreEnvironnement.ACCES_MAINTENANCE:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, "Web Shop inaccessible",
                "Le Web Shop est actuellement en maintenance. Merci de tenter ultérieurement.<br/>Pour plus d'informations, contactez votre <a href='contact'>magasin</a>.");
            break;
          // afficher_ACCES_INEXISTANT(request,((Utilisateur)request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$ACCES_INEXISTANT"));
          // DEMANDE DE CHANGEMENT DE MESSAGE SECURITE SONEPAR
          case MarbreEnvironnement.ACCES_INEXISTANT:
            retourMessage = afficher_ACCES_ER_MDP(request, "Ce profil n'est pas autorisé à utiliser le WebShop. Accès inexistant");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_AT_MDP:
            retourMessage = afficher_ACCES_AT_MDP(request, null);
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_ER_CONF_MDP:
            retourMessage = afficher_ACCES_AT_MDP(request, "Erreur confirmation mot de passe");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_DEJA_EXISTANT:
            retourMessage =
                afficher_ACCES_ER_MDP(request, "Votre profil existe déjà dans la base de données.<br/>Merci de saisir vos identifiants");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_AT_VALID:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, "Confirmation de votre inscription",
                "Vous devez valider votre profil sur votre boite mails avant d'accéder à votre espace client");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_ER_SAISIE:
            retourMessage = afficher_ACCES_ER_MDP(request, "Erreur de saisie");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_ER_MDP:
            retourMessage = afficher_ACCES_ER_MDP(request, null);
            razProfil(request);
            break;
          case MarbreEnvironnement.BLOC_SAISIE_MAX:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, utilisateur.getTraduction().traduire("$$ACCES_BLOQUE"),
                "Vous avez dépassé le nombre maximum de tentatives");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_REFUSE:
            retourMessage = afficher_BLOQUAGE_MESSAGE(request, utilisateur.getTraduction().traduire("$$ACCES_BLOQUE"),
                "Votre profil n'est plus actif sur le Web Shop. Veuillez vous adresser à votre <a href='contact'>magasin</a>.");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_PUBLIC:
            retourMessage = afficher_pagePrincipale(utilisateur, utilisateur.getTraduction().traduire("$$connexionH3"),
                utilisateur.getTraduction().traduire("$$connexionH3_2"));
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_VAL_PROFIL:
            retourMessage = afficher_pagePrincipale(utilisateur,
                "<span class='superflus'>Vous avez validé votre profil: </span>veuillez saisir vos identifiants",
                "<span class='superflus'>Vous n'avez pas encore de compte Web Shop: </span>inscrivez vous");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_MULTIPLE_CONNEX:
            retourMessage = afficherSaisieNumeroClient(request);
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_MULTIPLE_INSCRI:
            retourMessage = afficherSaisieNumeroClient(request);
            razProfil(request);
            break;
          
          // Les cas de demande de nouveau mot de passe
          case MarbreEnvironnement.ACCES_DE_NV_MP:
            retourMessage = afficherConf_NV_MP("Changement de mot de passe",
                "Afin de finaliser votre demande concernant le changement de votre mot passe, veuillez consulter votre boite mail et suivre la procédure indiquée.");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_NV_MP_ADMIN:
            retourMessage = afficherConf_NV_MP("Changement de mot de passe",
                "Votre mot de passe ne peut être changé sur le Web Shop. Merci de consulter votre administrateur.");
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_ATT_NV_MP:
            retourMessage = afficherSaisieNVMP(utilisateur, "Changement de mot de passe",
                "Veuillez saisir votre identifiant ainsi que vos nouveaux mots de passes", request.getParameter("saisieNVMP"));
            razProfil(request);
            break;
          case MarbreEnvironnement.ACCES_VAL_NV_MP:
            retourMessage = afficher_ACCES_ER_MDP(request,
                "Votre mot de passe a été modifié avec succès. Vous pouvez vous connecter avec vos nouveaux identifiants.");
            razProfil(request);
            break;
          // utilisateurs "connectés"
          case MarbreEnvironnement.ACCES_CLIENT:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case MarbreEnvironnement.ACCES_CONSULTATION:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case MarbreEnvironnement.ACCES_BOUTIQUE:
            getServletContext().getRequestDispatcher("/catalogue").forward(request, response);
            break;
          case MarbreEnvironnement.ACCES_REPRES_WS:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case MarbreEnvironnement.ACCES_RESPON_WS:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          case MarbreEnvironnement.ACCES_ADMIN_WS:
            getServletContext().getRequestDispatcher("/accueil").forward(request, response);
            break;
          
          default:
            break;
        }
      }
      else if (request.getParameter("choixETB") != null) {
        retourMessage = afficherChoixEtb(request);
      }
      else if (request.getParameter("etbChoisi") != null) {
        IdEtablissement idEtablissement = IdEtablissement.getInstance(request.getParameter("etbChoisi"));
        Gestion.attribuerUnETB(utilisateur, idEtablissement);
        if (utilisateur.getEtablissementEnCours() != null) {
          getServletContext().getRequestDispatcher("/accueil").forward(request, response);
        }
        else {
          getServletContext().getRequestDispatcher("/connexion").forward(request, response);
        }
      }
      else {
        getServletContext().getRequestDispatcher("/accueil").forward(request, response);
      }
      
      // Affichage
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, null));
      out.println(afficherContenu(
          "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$connexionH2") + "</span></h2>" + retourMessage,
          "connexion", null, null, request.getSession()));
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Saisir un nouveau mot de passe
   */
  private String afficherSaisieNVMP(UtilisateurWebshop utilisateur, String titre, String message, String mp) {
    String retour = "";
    if (utilisateur == null) {
      return retour;
    }
    
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span><span class='superflus'>" + titre + "</h3>";
    if (message != null) {
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    }
    
    retour += "<form name ='nvMP' id='nvMP1' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='' type='text' placeholder='"
        + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='nvPassUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour +=
        "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$confirmMotDePasse")
            + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='mot de passe' required /></div>";
    retour += "<input type='hidden' name='UtilnvMP' value='" + mp + "' />";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerInscription' value='"
        + utilisateur.getTraduction().traduire("$$valider") + "'/></div>";
    retour += "</form>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher en cas de tentative de changement de mot de passe
   */
  private String afficherConf_NV_MP(String titre, String contenu) {
    String retour = "";
    retour += "<div class='blocContenu' id='blocConnexion'>";
    if (titre != null) {
      retour += "<h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>";
    }
    if (contenu != null) {
      retour += "<p id='messagePrincipal'>" + contenu + "</p>";
    }
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher la page principale de connexion
   */
  private String afficher_pagePrincipale(UtilisateurWebshop utilisateur, String pTitreConnexion, String pTitreInscription) {
    StringBuilder builder = new StringBuilder();
    String loginSaisi = "";
    
    if (utilisateur == null) {
      return "";
    }
    
    // Récupérer le login saisi par l'utilisateur si c'est le cas
    if (utilisateur.getLogin() != null) {
      loginSaisi = utilisateur.getLogin();
    }
    
    builder.append("<div class='blocContenu' id='blocConnexion'>");
    if (pTitreConnexion != null) {
      builder.append("<h3><span class='puceH3'>&nbsp;</span>" + pTitreConnexion + "</h3>");
    }
    
    builder.append("<form name ='seConnecter' id='seConnecter' action='connexion' method='POST' accept-charset=\"utf-8\">");
    builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>"
        + utilisateur.getTraduction().traduire("$$adresseEmail") + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi
        + "' type='text' placeholder='" + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>");
    builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='passUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>");
    builder.append("<div class='lignesFormulaires'><input type ='submit' class='mesBoutons' id='validerConnexion' value='"
        + utilisateur.getTraduction().traduire("Connexion") + "'/></div>");
    builder.append("<div class='lignesFormulaires'><a href='#' id='mpOublie' onClick=\"verifierProfilSaisi('loginUser')\" >"
        + utilisateur.getTraduction().traduire("$$motPasseOublie") + "</a></div>");
    builder.append("</form>");
    builder.append(
        "<img id='imageConnexion' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/imageConnexion.png'/>");
    builder.append("</div>");
    
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_PUBLIC) {
      builder.append("<div class='blocContenu' id='blocSinscrire'>");
      if (pTitreInscription != null) {
        builder.append("<h3><span class='puceH3'>&nbsp;</span>" + pTitreInscription + "</h3>");
      }
      builder.append("<p id='messagePrincipal'>" + utilisateur.getTraduction().traduire("$$alerteInscription1") + " "
          + MarbreEnvironnement.LIBELLE_SERVEUR + " " + utilisateur.getTraduction().traduire("$$alerteInscription2") + "</p>");
      builder.append("<form name ='sInscrire' id='sInscrire' action='connexion' method='POST' accept-charset=\"utf-8\">");
      builder.append("<div class='lignesFormulaires'><label class='labelConnexion'>"
          + utilisateur.getTraduction().traduire("$$adresseEmail") + "</label><input name='loginUser' id='nvLoginUser' value ='"
          + loginSaisi + "' type='text' placeholder='" + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>");
      builder
          .append("<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
              + "</label><input name='passUser' id='nvPassUser' type='password' placeholder='"
              + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>");
      builder.append(
          "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$confirmMotDePasse")
              + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='"
              + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>");
      builder.append("<div class='lignesFormulaires'><input type ='submit'  class='mesBoutons' id='validerInscription' value=\""
          + utilisateur.getTraduction().traduire("$$sinscrire") + "\"/></div>");
      builder.append("</form>");
      builder.append("<img id='imageInscription' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/imageInscription.png'/>");
      builder.append("</div>");
    }
    
    return builder.toString();
  }
  
  /**
   * Virer toutes les propriétés de l'utilisateur -> réinitialiser la totale
   */
  private void razProfil(HttpServletRequest request) {
    if (request.getSession().getAttribute("utilisateur") != null
        && request.getSession().getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).setLogin(null);
      ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).setTypeAcces(MarbreEnvironnement.ACCES_PUBLIC);
    }
  }
  
  /**
   * Interface de déploiement du Web Shop. C'est ici qu'on saisit toutes les variables propres au déploiement de
   * l'application
   */
  private String afficher_NON_DEPLOYE(HttpServletRequest request, String titre, String deploiement, UtilisateurWebshop utilisateur) {
    String retour = "<div class='blocContenu'>";
    if (titre != null) {
      retour += "<h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>";
    }
    Trace.info("+++++++++++ deploiement : " + deploiement);
    
    if (deploiement == null || deploiement.equals("2")) {
      // Affichage du formulaire de déploiement
      retour += "<form name='traitementDeploiement' action='connexion?deploiement=1' method='POST' accept-charset=\"utf-8\">";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>Profil administrateur</label>"
          + "<input id='PROFIL_ADMIN' name='PROFIL_ADMIN' value='' type='text' maxlength='10' required/></div>";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>MP administrateur</label>"
          + "<input id='MP_ADMIN' name='MP_ADMIN' value='' type='password' maxlength='20' required/></div>";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>Adresse publique AS400</label>"
          // L'adresse public doit être une adresse IP valide
          + "<input id='ADRESSE_PUBLIC_SERVEUR' name='ADRESSE_PUBLIC_SERVEUR' value='' type='text' maxlength='50' required/></div>";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>Port publique AS400</label>"
          // Uniquement les valeurs numériques sont autorisées pour le port
          + "<input id='PORT_SERVEUR' name='PORT_SERVEUR' value='' type='text' maxlength='4' onkeypress='return /[0-9]/i.test(event.key)'/></div>";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>Lettre environnement</label>"
          + "<input id='LETTRE_ENV' name='LETTRE_ENV' value='' type='text' maxlength='1' required/></div>";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>Bibliothèque clients</label>"
          + "<input id='BIBLI_CLIENTS' name='BIBLI_CLIENTS' value='' type='text' maxlength='10' required/></div>";
      retour += "<div class='lignesFormulaires'><label class='labelDeploi'>Etablissement clients</label>"
          + "<input id='ETB_FORCE' name='ETB_FORCE' value='' type='text' maxlength='3' required/></div>";
      retour += "<div class='lignesFormulaires'><input type ='submit' id='validerParametres' value='Valider les données'/></div>";
      retour += "</form>";
    }
    else {
      if (((GestionConnexion) maGestion).traiterLeDeploiement(request, utilisateur)) {
        retour += "<p>Je suis déployé correctement</p>";
        request.getSession().invalidate();
      }
      else {
        retour += "<p>Problème de déploiement</p>";
      }
    }
    
    retour += "</div>";
    return retour;
  }
  
  /**
   * Formulaire d'inscription
   */
  private String afficher_ACCES_AT_MDP(HttpServletRequest request, String message) {
    String retour = "";
    String loginSaisi = "";
    
    UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
    if (utilisateur == null) {
      return retour;
    }
    
    // Récupérer le login saisi par l'utilisateur si c'est le cas
    if (utilisateur.getLogin() != null) {
      loginSaisi = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getLogin();
    }
    
    retour += "<div class='blocContenu'>";
    retour +=
        "<h3><span class='puceH3'>&nbsp;</span><span class='superflus'>Vous n'avez pas encore de compte client: </span>inscrivez vous</h3>";
    if (message != null) {
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    }
    
    retour += "<form name ='sInscrire' id='sInscrire2' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi
        + "' type='text' placeholder='adresse mail' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='nvPassUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour +=
        "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$confirmMotDePasse")
            + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='"
            + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerInscription' value=\""
        + utilisateur.getTraduction().traduire("$$sinscrire") + "\"/></div>";
    retour += "</form>";
    retour += "<img id='imageInscription' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/decoration/imageInscription.png'/>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Interface d'inscription client au Web Shop. C'est ici qu'on saisit toutes les variables propres au déploiement de
   * l'application
   */
  private String afficher_ACCES_ER_MDP(HttpServletRequest request, String message) {
    String retour = "";
    String loginSaisi = "";
    
    UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
    if (utilisateur == null) {
      return retour;
    }
    // Récupérer le login saisi par l'utilisateur si c'est le cas
    if (utilisateur.getLogin() != null) {
      loginSaisi = utilisateur.getLogin();
    }
    
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>Vos informations <span class='superflus'>de connexion</span> sont incorrectes</h3>";
    
    if (message != null) {
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    }
    retour += "<form name ='seConnecter' id='seConnecter' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi + "' type='text' placeholder='"
        + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='passUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerConnexion' value='"
        + utilisateur.getTraduction().traduire("Connexion") + "'/></div>";
    retour += "<div class='lignesFormulaires'><a href='#' id='mpOublie' onClick=\"verifierProfilSaisi('loginUser')\" >"
        + utilisateur.getTraduction().traduire("$$motPasseOublie") + "</a></div>";
    retour += "</form>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher le message de bloquage de la sasie utilisateurs
   */
  private String afficher_BLOQUAGE_MESSAGE(HttpServletRequest request, String titre, String message) {
    String retour = "";
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>";
    
    if (message != null) {
      retour += "<p id='messagePrincipal'>" + message + "</p>";
    }
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher l'interface qui permet de préciser le numéro du client
   */
  private String afficherSaisieNumeroClient(HttpServletRequest request) {
    String retour = "";
    String loginSaisi = "";
    boolean isInscription = false;
    
    UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
    if (utilisateur == null) {
      return retour;
    }
    
    // Récupérer le login saisi par l'utilisateur si c'est le cas
    if (utilisateur.getLogin() != null) {
      loginSaisi = utilisateur.getLogin();
      isInscription = utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_MULTIPLE_INSCRI;
    }
    
    retour += "<div class='blocContenu'>";
    retour += "<h3><span class='puceH3'>&nbsp;</span>";
    if (isInscription) {
      retour += "Formulaire d'inscription";
    }
    else {
      retour += "Connexion utilisateur";
    }
    retour += "</h3>";
    
    retour += "<p id='messagePrincipal'>" + utilisateur.getTraduction().traduire("$$txtPlusieursClients") + "</p>";
    
    retour += "<form name ='seConnecter' id='seConnecter' action='connexion' method='POST' accept-charset=\"utf-8\">";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$adresseEmail")
        + "</label><input name='loginUser' id='loginUser' value ='" + loginSaisi + "' type='text' placeholder='"
        + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required /></div>";
    retour +=
        "<div class='lignesFormulaires'><label class='labelConnexion'>Numéro client</label><input name='numeroClient' id='numeroClient' maxlength= '6' type='text' placeholder='client' required /></div>";
    retour += "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$motDePasse")
        + "</label><input name='passUser' id='passUser' type='password' placeholder='"
        + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    if (isInscription) {
      retour +=
          "<div class='lignesFormulaires'><label class='labelConnexion'>" + utilisateur.getTraduction().traduire("$$confirmMotDePasse")
              + "</label><input name='passUserCtrl' id='passUserCtrl' type='password' placeholder='"
              + utilisateur.getTraduction().traduire("$$motDePasse") + "' required /></div>";
    }
    retour += "<div class='lignesFormulaires'><input type ='submit' id='validerConnexion' value='"
        + utilisateur.getTraduction().traduire("Connexion") + "'/></div>";
    retour += "</form>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * afficher l'interface qui permet de choisir l'établissement
   */
  private String afficherChoixEtb(HttpServletRequest request) {
    StringBuilder builder = new StringBuilder(1000);
    
    builder.append("<div class='blocContenu'>");
    
    builder.append("<h3><span class='puceH3'>&nbsp;</span>Choix de l'établissement de travail</h3>");
    builder.append(
        "<p id='messagePrincipal'>Afin d'accéder aux différents modules, vous devez choisir un établissement de travail pour votre session</p>");
    
    if (MarbreEnvironnement.LISTE_ETBS != null && MarbreEnvironnement.LISTE_ETBS.size() > 0) {
      for (int i = 0; i < MarbreEnvironnement.LISTE_ETBS.size(); i++) {
        builder.append("<a class = 'liensBoutons' href='connexion?etbChoisi=" + MarbreEnvironnement.LISTE_ETBS.get(i).getCodeETB() + "'>"
            + MarbreEnvironnement.LISTE_ETBS.get(i).getLibelleETB() + "</a>");
      }
    }
    builder.append("<a class = 'liensBoutons' href='accueil?deconnecter=1'>Annuler</a>");
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
}
