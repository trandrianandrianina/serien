/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.text.DateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.environnement.GestionConnexion;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

/**
 * Cette classe est le pattern du look global du Web Shop. La structure de chaque page repose sur ce modèle
 */
public class PatternErgonomie {
  
  private DateFormat mediumDateFormat = null;
  
  /**
   * Constructeur par défaut
   */
  public PatternErgonomie() {
    mediumDateFormat = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM);
  }
  
  /**
   * Afficher l'entête de la page Web
   */
  public String majDuHead(HttpServletRequest request, String metaSpecifiques, String ancrage) {
    String chaine = "";
    UtilisateurWebshop utilisateurWebshop = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    if (utilisateurWebshop == null) {
      return "";
    }
    chaine += "<!DOCTYPE html>";
    chaine += "<html lang='fr'>";
    chaine += "<head>";
    chaine += "<meta charset='windows-1252'>";
    chaine +=
        "<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi' />";
    chaine +=
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "webShop.css?" + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>";
    chaine += "<link rel=\"shortcut icon\" href=\"" + MarbreEnvironnement.DOSSIER_IMAGES_DECO + "favicon.png\" />";
    chaine += "<script src='scripts/general.js?" + MarbreEnvironnement.versionInstallee + "'></script>";
    
    if (metaSpecifiques != null) {
      chaine += metaSpecifiques;
    }
    
    // Relooker en mode REPRESENTANT
    if (utilisateurWebshop.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) {
      chaine += "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "representant.css?" + MarbreEnvironnement.versionInstallee
          + "' rel='stylesheet'/>";
    }
    
    chaine += "<link href='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/css/CSSspecifique.css?"
        + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>";
    
    // Google analytics
    if (MarbreEnvironnement.GOOGLE_ANALYTICS != null) {
      chaine += MarbreEnvironnement.GOOGLE_ANALYTICS;
    }
    else {
      chaine += "<meta name='ROBOTS' content='NONE'> ";
      chaine += "<meta name='GOOGLEBOT' content='NOARCHIVE'>";
    }
    chaine += "</head>";
    
    if (ancrage == null) {
      ancrage = "";
    }
    chaine += "<body " + ancrage + ">";
    return chaine;
  }
  
  /**
   * Afficher le panneau de présentation
   */
  public String afficherPresentation(String page, HttpServletRequest request, String connexion) {
    if (request == null || (UtilisateurWebshop) request.getSession().getAttribute("utilisateur") == null) {
      return "";
    }
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    
    StringBuilder chaine = new StringBuilder(2000);
    chaine.append("<div id='presentationHeader'>");
    chaine.append("<header>");
    if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
      chaine.append("<div id='alerteClient'>" + "<div class='puceAlerte'>&nbsp;</div>"
          + "<div id='contenuAlerteClient'>Votre compte n'est pas autorisé à commander sur le Web Shop</div>" + "</div>");
    }
    chaine.append("<h1 id='h1Web'>Web</h1>");
    chaine.append("<h1 id='h1Shop'>SHOP</h1>");
    chaine.append("<h1 id='h1Client'>Nom du client</h1>");
    if (connexion != null) {
      chaine.append(connexion);
    }
    chaine.append(afficherNavigation(page, request.getSession()));
    // le logo point sur le site public de la filiale .
    
    if (utilisateur.getEtablissementEnCours() != null && utilisateur.getEtablissementEnCours().getAdresse_site_public() != null
        && (!utilisateur.getEtablissementEnCours().getAdresse_site_public().equals(""))) {
      chaine.append("<a href='" + utilisateur.getEtablissementEnCours().getAdresse_site_public() + "' target='_blank' id='lienAccueil'>");
      chaine.append("<img id='logoClient' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/logoSociete.png' alt=''>");
      chaine.append("</a>");
    }
    else {
      chaine.append("<a href='accueil' id='lienAccueil'>");
      chaine.append("<img id ='logoClient'  src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/logoSociete.png' alt=''>");
      chaine.append("</a>");
    }
    
    chaine.append(
        "<img id='image_deco' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/image_decoration.jpg' alt='Web Shop'>");
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) {
      chaine.append("<div id='titreRepresentant'>Mobilité <span id='plusRepresentant'>+</span></div>");
    }
    chaine.append("</header>");
    chaine.append("</div>");
    
    return chaine.toString();
  }
  
  /**
   * ---------------------------GESTION DU PANNEAU DE NAVIGATION-----------
   */
  
  /**
   * Afficher le panneau de navigation
   */
  public String afficherNavigation(String page, HttpSession session) {
    if (page == null || session == null) {
      return "";
    }
    
    StringBuilder chaine = new StringBuilder(2000);
    
    chaine.append("<nav id='navPersistante'>");
    
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) session.getAttribute("utilisateur");
    if (utilisateur == null) {
      return "";
    }
    
    if (utilisateur.getPanierIntuitif() == null) {
      // Aller chercher la liste des menus si nécessaire
      if (utilisateur.getMesMenus() == null) {
        utilisateur.recupererMenus();
        
        if (utilisateur.getMesMenus() == null) {
          return "";
        }
      }
      
      String classe = "";
      for (GenericRecord menu : utilisateur.getMesMenus()) {
        if (page.equals(Constantes.convertirObjetEnTexte(menu.getField("MN_LINK")))) {
          classe = "class='courantNav'";
        }
        else {
          classe = "class='lienNav'";
        }
        chaine.append(
            "<a " + classe + " href='" + Constantes.convertirObjetEnTexte(menu.getField("MN_LINK")) + "'>" + "<span class='navModeTexte'>"
                + utilisateur.getTraduction().traduire(Constantes.convertirObjetEnTexte(menu.getField("MN_NAME"))) + " </span>"
                + "<img src='" + Constantes.convertirObjetEnTexte(menu.getField("MN_IMG")) + "' alt='"
                + Constantes.convertirObjetEnTexte(menu.getField("MN_DES")) + "' 'title='"
                + Constantes.convertirObjetEnTexte(menu.getField("MN_DES")) + "' class='navModeGraphique'/>" + "</a>");
      }
    }
    // Moteur de recherche
    if (utilisateur.peutAccederAuCatalogue()) {
      chaine.append(
          "<form onSubmit='traitementEnCours(null);' action='catalogue' method='POST' id='moteurRecherche' name='moteurRecherche' accept-charset=\"utf-8\">"
              + "<input type='search' name='champRecherche' id='champRecherche' placeholder='"
              + utilisateur.getTraduction().traduire("$$Rechercher") + "' required/>"
              + "<a id='validMoteurRecherche' href='#' onClick=\"traitementEnCoursForm(document.moteurRecherche);\">r</a></form>");
    }
    
    chaine.append("</nav>");
    
    return chaine.toString();
  }
  
  /**
   * Afficher la panneau d'état de connexion
   */
  public String afficherConnexion(String page, HttpServletRequest pRequest, HttpServletResponse pResponse) {
    if (pRequest == null || pRequest.getSession() == null) {
      return "";
    }
    
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur");
    StringBuilder chaine = new StringBuilder(500);
    
    // Si on a passé une tentative de connexion
    if (pRequest.getParameter("loginUser") != null && pRequest.getParameter("passUser") != null) {
      if (utilisateur == null || (utilisateur.getTypeAcces() < MarbreEnvironnement.ACCES_CLIENT)) {
        GestionConnexion connexion = new GestionConnexion();
        int etatConnexion = connexion.majConnexion(pRequest.getSession(), pRequest.getParameter("loginUser"),
            pRequest.getParameter("passUser"), pRequest.getParameter("passUserCtrl"), pRequest.getParameter("numeroClient"),
            pRequest.getParameter("mobilitePluch"), pRequest.isSecure());
        // Rediriger l'utilisateur vers la page de connexion si son accès est incorrecte
        if (etatConnexion < MarbreEnvironnement.ACCES_CLIENT) {
          try {
            pResponse.sendRedirect(pRequest.getContextPath() + "/connexion?etatConnexion=" + etatConnexion);
          }
          catch (Exception e) {
            Trace.erreur(e, "Erreur lors de la redirection vers la page de connexion.");
            return "";
          }
        }
      }
    }
    // si on passe une tentative de déconnexion
    else if (Constantes.normerTexte(pRequest.getParameter("deconnecter")).equals("1")) {
      pRequest.getSession().invalidate();
    }
    
    // toujours passer un utilisateur à la session
    if (pRequest.getSession().getAttribute("utilisateur") == null) {
      pRequest.getSession().setAttribute("utilisateur", new UtilisateurWebshop(pRequest.getSession().getId()));
    }
    // Rafraîchir l'information de l'utilisateur après connexion ou déconnexion
    else {
      utilisateur = (UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur");
    }
    
    chaine.append("<aside id='etatConnexion'>");
    
    // affichage si on est connecté ou pas
    if (pRequest.getSession().getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      // on change la langue de l'utilisateur
      if (utilisateur != null && pRequest.getParameter("chgtLangue") != null) {
        utilisateur.setLanguage(pRequest.getParameter("chgtLangue"));
        utilisateur.getTraduction().chargerListeEquivalence(pRequest.getParameter("chgtLangue"));
        utilisateur.majFooters(pRequest.getParameter("chgtLangue"));
      }
      
      // On est en mode connecté
      if (utilisateur != null && utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_CLIENT) {
        
        chaine.append("<div id='connecte'>");
        chaine.append("<span id='monLogin'>" + afficherPortionLogin(utilisateur.getLogin()) + "</span>");
        chaine.append(afficherModeConnexion(utilisateur));
        if (!page.equals("monPanier")) {
          // Si on est connecté via la Mobilité Pluch
          if (utilisateur.isMobilitePlus()) {
            chaine.append("<a id='deconnecter' href='../SerieNmobilite/affichagePrincipal?menu=G'>Suite Série N mobilité</a>");
            chaine.append("<a id='imgDeconnexion' href='../SerieNmobilite/affichagePrincipal?menu=G'><img src='"
                + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/deconnexion.png' id='monImgDeconnexion'/></a>");
          }
          else {
            chaine.append(
                "<a id='deconnecter' href='" + page + "?deconnecter=1'>" + utilisateur.getTraduction().traduire("Deconnexion") + "</a>");
            chaine.append("<a id='imgDeconnexion' href='" + page + "?deconnecter=1'><img src='"
                + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/deconnexion.png' id='monImgDeconnexion'/></a>");
          }
        }
        chaine.append("</div>");
        chaine.append("</aside>");
        if (utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_CONSULTATION) {
          if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
            chaine.append("");
          }
          else {
            chaine.append(afficherPanier(utilisateur, false));
          }
        }
      }
      // On est en mode pas connecté
      else if (utilisateur != null) {
        chaine.append("<form id='pasConnecte' method='POST' name ='connexionUser' action='" + page + "' accept-charset=\"utf-8\">");
        chaine.append("<input name='loginUser' id='loginUser' type='text' placeholder='"
            + utilisateur.getTraduction().traduire("$$adresseEmail") + "' required />");
        chaine.append("<input name='passUser' id='passUser' type='password' placeholder='"
            + utilisateur.getTraduction().traduire("$$motDePasse") + "' required />");
        chaine.append("<input type ='submit' id='validerConnexionPattern' />");
        chaine.append("<a id='validConnexion' href='#' onClick=\"document.getElementById('validerConnexionPattern').click();\"><img src='"
            + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fleche.png' id='imgValidConnexion'/></a>");
        
        chaine.append("</form>");
        chaine.append("</aside>");
      }
    }
    
    return chaine.toString();
  }
  
  /**
   * Afficher une portion réduite du login de l'utilisateur
   */
  private String afficherPortionLogin(String login) {
    String retour = "";
    if (login == null) {
      return "";
    }
    else {
      retour = Constantes.normerTexte(login);
      login = Constantes.normerTexte(login);
    }
    
    if (login.length() > 25 && login.split("@").length == 2) {
      retour = login.split("@")[0];
    }
    
    if (retour.length() > 25) {
      retour = retour.substring(0, 24) + "..";
    }
    
    return retour;
  }
  
  /**
   * Afficher le mode de connexion : Nom du client ou Gestionnaire ou Administrateur....etc
   */
  private String afficherModeConnexion(UtilisateurWebshop utilisateur) {
    String retour = "";
    if (utilisateur == null) {
      return retour;
    }
    
    if (utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_CLIENT
        || utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_CONSULTATION) {
      retour += "<span id='modeConnexion'>(";
      if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT
          || utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CONSULTATION && utilisateur.getClient().getNomClient() != null) {
        if (Constantes.normerTexte(utilisateur.getClient().getNomClient()).length() > 30) {
          retour += Constantes.normerTexte(utilisateur.getClient().getNomClient()).substring(0, 29) + "..";
        }
        else {
          retour += Constantes.normerTexte(utilisateur.getClient().getNomClient());
        }
      }
      else if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) {
        retour += "Représentant";
      }
      else if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_RESPON_WS) {
        retour += "Gestionnaire";
      }
      else if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_ADMIN_WS) {
        retour += "Administrateur";
      }
      
      retour += ")</span>";
    }
    
    return retour;
  }
  
  /**
   * permet d'afficher le résumé du panier de l'utilisateur sur toutes les
   * pages
   */
  protected String afficherPanier(UtilisateurWebshop utilisateur, boolean isAjax) {
    String chaine = "";
    if (utilisateur == null) {
      return chaine;
    }
    
    if (!isAjax) {
      chaine += "<div id='monPanier'>";
    }
    
    if (utilisateur.getMonPanier() != null) {
      chaine += "<a id='lienPanier' href='monPanier'><img src='images/decoration/panier2.png'/></a>";
      chaine += "<a id='quantitePanier' href='monPanier'>(" + utilisateur.getMonPanier().getNombreArticle() + ")</a>";
      chaine += "<a id='totalMonPanier' href='monPanier'>"
          + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalHT()) + "</a>";
    }
    else if (utilisateur.getPanierIntuitif() != null) {
      chaine += "<a id='lienPanier' href='PanierIntuitifBO'><img src='images/decoration/panier2.png'/></a>";
      chaine += "<a id='quantitePanier' href='PanierIntuitifBO'>(" + utilisateur.getPanierIntuitif().getNombreArticle() + ")</a>";
      chaine += "<a id='totalMonPanier' href='PanierIntuitifBO'>&nbsp;</a>";
    }
    
    if (!isAjax) {
      chaine += "</div>";
    }
    
    return chaine;
  }
  
  /**
   * Afficher le pied de page
   */
  public String afficherPiedPage(String pop, HttpSession session, String page) {
    if (session == null) {
      return "";
    }
    
    UtilisateurWebshop utilisateurWebshop = (UtilisateurWebshop) session.getAttribute("utilisateur");
    if (utilisateurWebshop == null || utilisateurWebshop.getPanierIntuitif() != null) {
      return "";
    }
    
    // Infos sessions
    String infos = "- Serveur " + MarbreEnvironnement.LIBELLE_SERVEUR + "\\n- Web Shop " + MarbreEnvironnement.versionInstallee
        + " \\n- Bibliothèque " + MarbreEnvironnement.BIBLI_CLIENTS;
    if (utilisateurWebshop.getEtablissementEnCours() != null) {
      infos += "\\n- Etb " + utilisateurWebshop.getEtablissementEnCours().getLibelleETB() + " ("
          + utilisateurWebshop.getEtablissementEnCours().getCodeETB() + ")";
    }
    
    StringBuilder chaine = new StringBuilder(1000);
    if (utilisateurWebshop.getTypeAcces() != MarbreEnvironnement.ACCES_REPRES_WS) {
      chaine.append("<footer>");
      chaine.append("<a class='liensFooter' href='conditionsGenerales'>" + utilisateurWebshop.getTraduction().traduire("$$CGV") + "</a>");
      chaine.append("<a  class='liensFooter' href='magasins'>" + utilisateurWebshop.getTraduction().traduire("$$votreMagasin") + "</a>");
      chaine.append(afficherFooterDynamique(utilisateurWebshop));
      chaine.append("<a  class='liensFooter' href='contact'>Contact</a>");
      chaine.append("<a  class='liensFooter' href='#' onClick='alert(\"" + infos + "\");'>?</a>");
      // lien Facebook
      if (utilisateurWebshop.getEtablissementEnCours() != null
          && utilisateurWebshop.getEtablissementEnCours().getAdresse_site_facebook() != null
          && !Constantes.normerTexte(utilisateurWebshop.getEtablissementEnCours().getAdresse_site_facebook()).equals("")) {
        chaine
            .append("<a class='liensFooter' id='lienFB' href='" + utilisateurWebshop.getEtablissementEnCours().getAdresse_site_facebook()
                + "' target='_blank'><span class='DrapeauInvisible'>FB</span></a>");
      }
      chaine.append("<span  class='liensFooter' id='copyright'>&copy 2018 Résolution Informatique</span>");
      if (page == null || (!page.equals("accueil") && !page.equals("informations") && !page.equals("conditionsGenerales")
          && !page.equals("magasins") && !page.equals("contact"))) {
        page = "accueil";
      }
      if (MarbreEnvironnement.IS_MODE_LANGUAGES) {
        chaine
            .append("<a  class='liensFooter' id='lienFR' href='" + page + "?chgtLangue=fr'><span class='DrapeauInvisible'>fr</span></a>");
        chaine.append(
            "<a  class='liensFooter' id='lienEN'  href='" + page + "?chgtLangue=en'><span class='DrapeauInvisible'>en</span></a>");
      }
      chaine.append("</footer>");
    }
    chaine.append("</article>");
    chaine.append(afficherPopUp(utilisateurWebshop, pop));
    chaine.append(afficherFocus("champRecherche"));
    chaine.append("</body>");
    chaine.append("</html>");
    return chaine.toString();
  }
  
  /**
   * Afficher des liens dynamiques dans le footer
   */
  private String afficherFooterDynamique(UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      return null;
    }
    StringBuilder chaine = new StringBuilder(1000);
    if (utilisateur.getMesFooters() == null) {
      utilisateur.majFooters(utilisateur.getLanguage());
    }
    String target = "";
    if (utilisateur.getMesFooters() != null && utilisateur.getMesFooters().size() > 0) {
      for (int i = 0; i < utilisateur.getMesFooters().size(); i++) {
        if (Constantes.convertirObjetEnTexte(utilisateur.getMesFooters().get(i).getField("FT_TARG")).equals("BL")) {
          target = "Target='_blank'";
        }
        else {
          target = "";
        }
        chaine.append("<a  class='liensFooter' href='"
            + Constantes.convertirObjetEnTexte(utilisateur.getMesFooters().get(i).getField("FT_LIEN")) + "' " + target + " >"
            + Constantes.convertirObjetEnTexte(utilisateur.getMesFooters().get(i).getField("FT_LIB")) + "</a>");
      }
    }
    
    return chaine.toString();
  }
  
  /**
   * Donner le focus à un objet sinon au formulaire de connexion
   */
  private String afficherFocus(String idObjet) {
    if (idObjet == null) {
      return "";
    }
    String chaine = "";
    chaine = "<script type=\"text/javascript\">" + "if(document.getElementById('" + idObjet.trim() + "')!= null) "
        + " document.getElementById('" + idObjet.trim() + "').focus(); " + "else if (document.getElementById('loginUser')!= null) "
        + " document.getElementById('loginUser').focus(); " + "</script>";
    
    return chaine;
  }
  
  /**
   * Afficher Pop up générale
   */
  private String afficherPopUp(UtilisateurWebshop utilisateur, String pop) {
    String retour = "";
    retour = "<div id='filtreTotal'></div>";
    
    retour += "<div id='popUpGenerale'>";
    retour += pop;
    retour += "</div>";
    
    retour += "<div id='traitementEnCours'>" + utilisateur.getTraduction().traduire("$$traitementCours") + "</div>";
    
    retour += afficherPopUpLots(utilisateur);
    
    return retour;
  }
  
  /**
   * Permet d'afficher la pop-up de gestion des lots
   */
  private String afficherPopUpLots(UtilisateurWebshop pUtilisateur) {
    StringBuilder retour = new StringBuilder();
    retour.append("<div id='popUpLots'>");
    retour.append("<div class='entetePopUpLot' >Article</div>");
    retour.append("<div id='articlePopUpLot'>" + "<div id='nomZone'>nomZone</div>"
        + "<p class='labelDonnee'>Référence article</p><p class='zoneDonnee' id='refArticle'><span class='paddingInterne'>Reference article</span></p><br/>"
        + "<p class='labelDonnee'>Libellé article</p><p class='zoneDonnee' id='libArticle'><span class='paddingInterne'>Libelle article</span></p><br/>"
        + "</div>");
    retour.append("<div class='entetePopUpLot' >Information sur la disponibilité des lots</div>");
    retour.append("<div id='infosPopUpLot'>" + pUtilisateur.getTraduction().traduire("$$infoGestionLots") + "</div>"
        + "<div id='disposPopUpLot'><div class='uneDispoLot'>- 500 metres</div><div class='uneDispoLot'>- 500 metres</div><div class='uneDispoLot'>- 250 metres</div></div>");
    retour.append("<div class='entetePopUpLot' >Découpe souhaitée</div>");
    retour.append("" + "<div id='decoupePopUpLot'>"
        + "<p class='labelDonnee'>Longueur souhaitée</p><p class='zoneDonnee' id='longueurSouhaitee'><span class='paddingInterne'>650</span></p><p id='uniteDecoupe'>metres</p><br/>"
        + "<div id='textSouhaitDecoupe'>Précisez la découpe que vous souhaitez :</div>" + "<div id='zonesDecoup'>" + "</div>"
        + "<div id='navigationLots'>" + "<div class='divDeBoutons' id='btValidLots'>"
        + "<a class='btRecuperation' id='validerDecoupe' href='#' onClick ='gererDecoupes(true);' >"
        + "<span class='deco_bt' id='deco_bt_valid_Lot'>&nbsp;</span>" + "<span class='texte_bt'>Valider la découpe</span>" + "</a>"
        + "</div>" + "<div class='divDeBoutons'>"
        + "<a class='btRecuperation' id='annulerDecoupe' href='#' onClick='fermerPopGestionLots();'>"
        + "<span class='deco_bt' id='deco_bt_annul_Lot'>&nbsp;</span>" + "<span class='texte_bt'>Annuler</span>" + "</a>" + "</div>"
        + "</div>");
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * Vider le panier de l'utilisateur de toutes ses informations
   */
  public String razDuPanier(UtilisateurWebshop utilisateur) {
    String retour = "";
    
    retour += "<script type=\"text/javascript\">" + "razPanier(\""
        + Outils.formaterUnTarifArrondi(utilisateur, utilisateur.getMonPanier().getTotalHT()) + "\");" + "</script>";
    
    return retour;
  }
  
}
