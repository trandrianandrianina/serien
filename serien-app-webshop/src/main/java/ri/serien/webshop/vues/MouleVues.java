/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.environnement.GestionRequetes;
import ri.serien.webshop.environnement.SessionListener;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;

/**
 * Classe de base de tous les Servlet du WebShop.
 * 
 * Cette classe implémente le comportement commun, souhaité pour le WebShop, lors du traitement des servlet.
 */
public abstract class MouleVues extends HttpServlet {
  
  protected PatternErgonomie pattern = null;
  protected String nomPage = null;
  protected Gestion maGestion = null;
  protected String cssSpecifique = null;
  protected int accesPage;
  protected ArrayList<FilRouge> listeFilRouge = null;
  protected FilRouge filAccueil = null;
  
  /**
   * Constructeur par défaut.
   */
  public MouleVues() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Constructeur permettant de passer des paramètres typique au servlet.
   */
  public MouleVues(Gestion gestionSpec, String pNomPage, String css, int accesMini) {
    super();
    pattern = new PatternErgonomie();
    maGestion = gestionSpec;
    nomPage = pNomPage;
    cssSpecifique = css;
    accesPage = accesMini;
    filAccueil = new FilRouge("accueil", "accueil", "Accueil", "Home");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      gererLesCasMoisis(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (ServletException e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
    catch (IOException e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      gererLesCasMoisis(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (ServletException e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
    catch (IOException e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * gérer les cas moisis de requêtes moisies
   * @throws IOException
   * @throws ServletException
   */
  private void gererLesCasMoisis(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
    // Vérifier les paramètres
    if (response == null) {
      Trace.erreur("Traitement de la requête impossible car HttpServletResponse est invalide.");
      return;
    }
    if (request == null) {
      Trace.erreur("Traitement de la requête impossible car HttpServletRequest est invalide.");
      response.sendError(HttpServletResponse.SC_FORBIDDEN);
      return;
    }
    
    if (MarbreEnvironnement.IS_MODE_SSL) {
      if (!request.isSecure()) {
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        return;
      }
    }
    
    if (controlerRequetesHTTP(request, response)) {
      traiterPOSTouGET(request, response);
    }
    else {
      // Renouvellement de session obsolète
      if (request.getSession() == null || ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur") == null)) {
        gererSessionUtilisateur(request, response);
      }
      // requête moisie
      else {
        response.sendError(HttpServletResponse.SC_FORBIDDEN);
        request.getSession().invalidate();
      }
    }
  }
  
  /**
   * Gère la session utilisateur
   */
  private void gererSessionUtilisateur(HttpServletRequest pRequest, HttpServletResponse pResponse) throws ServletException, IOException {
    // Renouveller la session
    if (pRequest.getSession() == null) {
      pRequest.getSession(true);
      pRequest.getSession().setAttribute("utilisateur", new UtilisateurWebshop(pRequest.getSession().getId()));
      getServletContext().getRequestDispatcher("/connexion").forward(pRequest, pResponse);
    }
    // Créer un utilisateur si il n'existe pas dans la session
    else if (pRequest.getSession().getAttribute("utilisateur") == null) {
      pRequest.getSession().setAttribute("utilisateur", new UtilisateurWebshop(pRequest.getSession().getId()));
      getServletContext().getRequestDispatcher("/connexion").forward(pRequest, pResponse);
    }
  }
  
  /**
   * Afficher un contenu au sein même de la page spécifique
   */
  protected String afficherContenu(String pContenu, String pPage, String pParametre, String pPop, HttpSession pSession) {
    if (pSession == null) {
      return "";
    }
    
    String chaine = "<article id='contenu'>";
    
    chaine += retournerBlocRepresentant(pSession);
    
    chaine += traitementFilRouge(pPage, pParametre, pSession);
    
    chaine += pContenu;
    
    chaine += pattern.afficherPiedPage(pPop, pSession, pPage);
    
    return chaine;
  }
  
  /**
   * Permet de traiter l'affichage du fil rouge ne fonction de la page passée
   */
  public String traitementFilRouge(String page, String parametre, HttpSession session) {
    String retour = "";
    String lienGraph = null;
    if (session == null || session.getAttribute("utilisateur") == null) {
      return retour;
    }
    
    // On n'affiche pas de fil rouge si on est en mode panier intuitif
    if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getPanierIntuitif() != null) {
      return retour;
    }
    
    retour = "<div id='filRouge'>";
    
    if (page != null) {
      String partiel = "";
      int i = 0;
      boolean trouve = false;
      while (i < listeFilRouge.size() && !trouve) {
        listeFilRouge.get(i).majValeurLangue(((UtilisateurWebshop) session.getAttribute("utilisateur")).getLanguage());
        trouve = listeFilRouge.get(i).getCleFil().equals(page);
        // si on a trouvé le fil on change ses paramètres
        if (trouve) {
          listeFilRouge.get(i).setParametreLien(parametre);
        }
        else {
          if (i != 0) {
            partiel += " <span class='flecheFilRouge'><</span> ";
          }
          partiel +=
              "<a href='" + listeFilRouge.get(i).getLienFil() + "' class='filRougeText'>" + listeFilRouge.get(i).getValeurFil() + "</a>";
          lienGraph = listeFilRouge.get(i).getLienFil();
        }
        
        i++;
      }
      if (!trouve || (trouve && i == 1)) {
        partiel = "";
      }
      
      retour += partiel;
    }
    else {
      filAccueil.majValeurLangue(((UtilisateurWebshop) session.getAttribute("utilisateur")).getLanguage());
      retour += "<a href='" + filAccueil.getLienFil() + "' class='filRougeText'>" + filAccueil.getValeurFil() + "</a>";
    }
    
    // On passe au lien graphique en mode Smartphone
    if (page != null && lienGraph != null) {
      retour += "<a href='" + lienGraph + "' class='filGraphique'><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/flecheGauche.png' alt='retour' /></a>";
    }
    else {
      retour += "<a href='" + filAccueil.getLienFil() + "' class='filGraphique'><img src='"
          + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/flecheGauche.png' alt='retour' /></a>";
    }
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Permet d'afficher le mode représentant sur chaque interface
   */
  private String retournerBlocRepresentant(HttpSession session) {
    String retour = "";
    
    if (session.getAttribute("utilisateur") == null) {
      return retour;
    }
    
    if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS) {
      if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getClient() != null) {
        retour += "<div id='surBlocClient'>";
        retour += "<div id='blocrepresentant'><span class='superflusClient'>Vous avez associé le client </span><span id='clientEncours'>"
            + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getClient().getNomClient() + " ("
            + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getClient().getNumeroClient() + "/"
            + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getClient().getSuffixeClient()
            + ")</span><span class='superflusClient'> à votre session</span></div>";
        if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getMonPanier() != null
            && ((UtilisateurWebshop) session.getAttribute("utilisateur")).getMonPanier().getListeArticles().size() == 0) {
          retour += "<a id='blocModifClient' href='clients'><img class='imagesClients' src='images/decoration/modifClient.png'/></a>";
          retour +=
              "<a id='blocSupprClient' href='clients?reinit=1'><img class='imagesClients' src='images/decoration/supprClient.png'/></a>";
        }
        retour += "</div>";
      }
      else {
        retour += "<div id='surBlocClient'>";
        retour +=
            "<div id='blocrepresentant'><span class='superflusClient'>Vous n'avez associé </span><span id='clientEncours'>aucun client à votre session</span></div>";
        retour += "<a id='blocAjoutCLient' href='clients'><img class='imagesClients' src='images/decoration/ajoutClient.png'/></a>";
        retour += "</div>";
      }
    }
    
    return retour;
  }
  
  /**
   * Méthode générique de contrôle d'accès à la page en question
   */
  protected void redirectionSecurite(HttpServletRequest request, HttpServletResponse response) {
    try {
      int accesUtilisateur = MarbreEnvironnement.ACCES_REFUSE;
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof UtilisateurWebshop) {
        // Cas ou la session dépasse le nombre maximum de tentatives de connexion à un compte
        if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
            .getTentativesConnexions() > MarbreEnvironnement.MAX_TENT_CONNEX) {
          accesUtilisateur = MarbreEnvironnement.BLOC_SAISIE_MAX;
          if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
              .getTentativesConnexions() == MarbreEnvironnement.MAX_TENT_CONNEX + 1) {
            // Ne pas repasser dans cette boucle avant une nouvelle session
            ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).setTentativesConnexions(
                ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getTentativesConnexions() + 1);
          }
        }
        else {
          accesUtilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getTypeAcces();
        }
      }
      
      // Si on est pas autorisé ou on doit saisir un numéro client avant d'accéder à la page
      if (accesInterdit(accesUtilisateur, ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")))
          || accesUtilisateur == MarbreEnvironnement.ACCES_MULTIPLE_CONNEX
          || accesUtilisateur == MarbreEnvironnement.ACCES_MULTIPLE_INSCRI) {
        getServletContext().getRequestDispatcher("/connexion?echec=" + accesUtilisateur).forward(request, response);
      }
      
      if (accesUtilisateur > MarbreEnvironnement.ACCES_REPRES_WS
          && ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getEtablissementEnCours() == null) {
        if (request.getParameter("choixETB") == null) {
          getServletContext().getRequestDispatcher("/connexion?choixETB=1").forward(request, response);
        }
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * On vérifie que l'utilisateur ait un accés légal à la page
   */
  private boolean accesInterdit(int accesUtil, UtilisateurWebshop utilisateur) {
    boolean isInterdit = true;
    
    // On vérifie que l'utilisateur n'ait pas un accès inférieur à celui requis
    if (nomPage.equals("catalogue")) {
      isInterdit = (accesUtil < accesPage) && !MarbreEnvironnement.CATALOGUE_MODE_PUBLIC;
    }
    else {
      isInterdit = accesUtil < accesPage;
    }
    if (isInterdit) {
      return isInterdit;
    }
    
    // Pour les accès back office Si on est autorisé
    if (accesPage > MarbreEnvironnement.ACCES_CLIENT) {
      isInterdit = true;
      if (utilisateur.getListeAccesBackOffice() == null || utilisateur.getListeAccesBackOffice().size() == 0) {
        return true;
      }
      
      if (nomPage.equals("accueilBO")) {
        return false;
      }
      
      // On contrôle si la page fait partie des pages autorisées pour cet utilisateur
      for (int i = 0; i < utilisateur.getListeAccesBackOffice().size(); i++) {
        if (utilisateur.getListeAccesBackOffice().get(i).isPresentField("BTN_LINK")
            && utilisateur.getListeAccesBackOffice().get(i).getField("BTN_LINK").toString().trim().equals(nomPage)) {
          isInterdit = false;
          break;
        }
      }
    }
    
    return isInterdit;
  }
  
  /**
   * Méthode qui regroupe le traitement par GET ou par POST
   */
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    if (request != null && request.getSession() != null && request.getSession().getAttribute("utilisateur") != null) {
      // maj de la page en cours de l'utilisateur
      ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).majPageEnCours(nomPage, request);
      // nettoyage des listes temporaires
      Gestion.initListesTemporaires((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
      // Sécuriser le cookie de SESSION en sortie
      response.setHeader("Set-Cookie", "JSESSIONID=" + request.getSession().getId() + "; Path=/WebShop; HttpOnly");
    }
    else {
      try {
        Trace.erreur("********** Session ou utilisateur à NULL");
        getServletContext().getRequestDispatcher("/connexion").forward(request, response);
      }
      catch (ServletException e) {
        e.printStackTrace();
      }
      catch (IOException e) {
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Affiche l'info stock en fonction du paramètre filiale ou des décimales
   * choisies
   */
  public String afficherStockCorrectement(UtilisateurWebshop pUtilisateur, String pQuantite, int pNombreDeDecimales) {
    if (pQuantite == null || pQuantite.equals("") || pUtilisateur == null || pUtilisateur.getEtablissementEnCours() == null) {
      return "";
    }
    String retour = "";
    BigDecimal stock = new BigDecimal(pQuantite);
    stock = stock.setScale(pNombreDeDecimales, RoundingMode.HALF_DOWN);
    
    // Si on est en mode affichage graphique avec les icône
    if (pUtilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_SANS_VALEUR) {
      
      if (stock.compareTo(BigDecimal.ZERO) > 0) {
        retour = "<span class='dispoStock'>&nbsp;</span>";
      }
      else {
        retour = "<span class='pasDispoStock'>&nbsp;</span>";
      }
    }
    // On formate l'affichage la valeur du stock
    else {
      // Si on est en négatif on cache le négatif
      if (stock.compareTo(BigDecimal.ZERO) < 0) {
        stock = BigDecimal.ZERO;
        stock = stock.setScale(pNombreDeDecimales, RoundingMode.HALF_DOWN);
      }
      retour = stock.toString();
    }
    
    return retour;
  }
  
  /**
   * On échappe les simples quotes
   */
  public String traiterCaracteresINPUT(String brut) {
    if (brut == null) {
      return null;
    }
    
    return StringEscapeUtils.escapeHtml4(brut);
  }
  
  /**
   * Afficher un icone représentatif du stock saisi
   */
  public String afficherStockFiche(UtilisateurWebshop utilisateur, String donnee, String taille) {
    if (donnee == null || donnee.trim().equals("")) {
      return null;
    }
    String retour = donnee;
    
    if (utilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_SANS_VALEUR) {
      float stock = 0;
      try {
        stock = Float.parseFloat(donnee);
        
        if (stock > 0) {
          retour =
              "<img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_stockOK" + taille + ".png'/>";
        }
        else {
          retour =
              "<img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_stockNUL" + taille + ".png'/>";
        }
      }
      catch (NumberFormatException e) {
        e.printStackTrace();
        retour = "&nbsp;";
      }
    }
    
    return retour;
  }
  
  /**
   * Contrôler la requête saisi par le client sur un
   */
  public boolean controlerRequetesHTTP(HttpServletRequest request, HttpServletResponse response) {
    if (request == null || request.getSession() == null
        || ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur") == null)) {
      return false;
    }
    // Si on a affaire à une adresse déjà rentrée en bDD pas la peine d'aller plus loin
    if (SessionListener.listeIpBloquees != null && SessionListener.listeIpBloquees.containsKey(request.getRemoteAddr())
        && SessionListener.listeIpBloquees.get(request.getRemoteAddr()) >= MarbreEnvironnement.NB_SESSIONS_MAX) {
      return false;
    }
    // On teste l'adresse IP de cette requête ainsi que sa fréquence de demandes de requêtes pour éviter les robots
    else if (!request.isRequestedSessionIdValid() && request.getRequestedSessionId() == null) {
      return GestionRequetes.testerRequete(request);
    }
    else {
      if (MarbreEnvironnement.MODE_DEBUG) {
        SessionListener.afficherListeDesSessions();
      }
      return true;
    }
  }
  
  /**
   * Afficher la pagination d'une liste
   */
  protected String afficherPaginationArticles(UtilisateurWebshop utilisateur, float total, int indice, String parametres) {
    
    int nbPage = -1;
    int numPage = -1;
    int debItem = 0;
    
    if (parametres == null) {
      parametres = "";
    }
    else {
      parametres += "&";
    }
    
    StringBuilder retour = new StringBuilder(400);
    nbPage = (int) Math.ceil(total / utilisateur.getEtablissementEnCours().getLimit_liste());
    
    if (nbPage > 1) {
      retour.append("<div class='blocPagination'>");
      for (int p = 0; p < nbPage; p++) {
        numPage = p + 1;
        if (indice >= debItem && indice < debItem + utilisateur.getEtablissementEnCours().getLimit_liste()) {
          if (indice > 0) {
            retour.append("<span class='spanPagination'> < </span>");
          }
          
          retour.append("<span id='spanPageEnCours'>" + utilisateur.getTraduction().traduire("$$page") + " " + numPage + "</span>");
        }
        else {
          if (indice > debItem && debItem > 0) {
            retour.append("<span class='spanPagination'> < </span>");
          }
          else if (debItem != 0) {
            retour.append("<span> > </span>");
          }
          
          retour
              .append("<a class='aPagination' href='" + nomPage + "?" + parametres + "indiceDebut=" + debItem + "'>" + numPage + "</a>");
        }
        
        debItem += utilisateur.getEtablissementEnCours().getLimit_liste();
      }
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * afficher la pagination propre à la liste de pagination de l'utilisateur
   */
  protected String afficherPagination(UtilisateurWebshop utilisateur, String indiceString, String parametres) {
    if (utilisateur == null || utilisateur.getDerniereListePagination() == null) {
      return "";
    }
    
    float total = utilisateur.getDerniereListePagination().size();
    
    int nbPage = -1;
    int numPage = -1;
    int debItem = 0;
    int indice = 0;
    
    try {
      if (indiceString != null) {
        indice = Integer.parseInt(indiceString);
      }
    }
    catch (Exception e) {
      indice = 0;
      Trace.erreur("afficherPagination() " + e.getMessage());
    }
    
    if (parametres == null) {
      parametres = "";
    }
    else {
      parametres += "&";
    }
    
    StringBuilder retour = new StringBuilder(400);
    nbPage = (int) Math.ceil(total / utilisateur.getEtablissementEnCours().getLimit_liste());
    
    if (nbPage > 1) {
      retour.append("<div class='blocPagination'>");
      for (int p = 0; p < nbPage; p++) {
        numPage = p + 1;
        if (indice >= debItem && indice < debItem + utilisateur.getEtablissementEnCours().getLimit_liste()) {
          if (indice > 0) {
            retour.append("<span class='spanPagination'> < </span>");
          }
          
          retour.append("<span id='spanPageEnCours'>" + utilisateur.getTraduction().traduire("$$page") + " " + numPage + "</span>");
        }
        else {
          if (indice > debItem && debItem > 0) {
            retour.append("<span class='spanPagination'> < </span>");
          }
          else if (debItem != 0) {
            retour.append("<span> > </span>");
          }
          
          retour
              .append("<a class='aPagination' href='" + nomPage + "?" + parametres + "indiceDebut=" + debItem + "'>" + numPage + "</a>");
        }
        
        debItem += utilisateur.getEtablissementEnCours().getLimit_liste();
      }
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Retourner la bonne adresse Mail de confirmation de commande Négociant.<br>
   * Si une adresse est déclarée sur le magasin de retrait la prendre sinon prendre l'adresse de l'établissement en cours.
   */
  protected String retournerAdresseMailConfirmation(String pMag, String pTypeBon, UtilisateurWebshop pUtilisateur) {
    String retour = "";
    
    // Si on a pas de magasin associé
    if (pTypeBon == null || pUtilisateur == null) {
      return null;
    }
    
    if (pMag != null) {
      retour = retournerAdresseMailMagasin(pUtilisateur, pTypeBon, pMag);
    }
    
    // Si il n'y a aucune adresse mail magasin
    if (retour == null) {
      if (pTypeBon.equals("E")) {
        retour = pUtilisateur.getEtablissementEnCours().getMail_commandes();
      }
      else if (pTypeBon.equals("D")) {
        retour = pUtilisateur.getEtablissementEnCours().getMail_devis();
      }
    }
    
    return retour;
  }
  
  /**
   * Retourner l'adresse mail d'une commande ou d'un devis déclarée pour un magasin
   */
  private String retournerAdresseMailMagasin(UtilisateurWebshop pUtilisateur, String pTypeBon, String pMag) {
    if (pMag == null || pUtilisateur == null || pTypeBon == null) {
      return null;
    }
    
    String retour = null;
    String zoneMail = "MG_MAIL_CD";
    
    if (pTypeBon.equals("E")) {
      zoneMail = "MG_MAIL_CD";
    }
    else if (pTypeBon.equals("D")) {
      zoneMail = "MG_MAIL_DV";
    }
    
    ArrayList<GenericRecord> liste = null;
    liste = pUtilisateur.getAccesDB2()
        .select("SELECT " + zoneMail + " FROM " + MarbreEnvironnement.BIBLI_WS + ".MAGASINS WHERE US_ETB = '"
            + pUtilisateur.getEtablissementEnCours().getCodeETB() + "' AND MG_COD ='" + pMag + "' "
            + MarbreEnvironnement.CLAUSE_OPTIMIZE_UNIQUE + MarbreEnvironnement.CLAUSE_OPTIMIZE);
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).getField(zoneMail) != null && !liste.get(0).getField(zoneMail).toString().trim().equals("")) {
        retour = liste.get(0).getField(zoneMail).toString().trim();
      }
    }
    
    return retour;
  }
  
  /*++++++++++++++++++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
  public PatternErgonomie getPattern() {
    return pattern;
  }
  
  public void setPattern(PatternErgonomie pattern) {
    this.pattern = pattern;
  }
  
  public String getNomPage() {
    return nomPage;
  }
  
  public void setNomPage(String nomPage) {
    this.nomPage = nomPage;
  }
  
  public Gestion getMaGestion() {
    return maGestion;
  }
  
  public void setMaGestion(Gestion maGestion) {
    this.maGestion = maGestion;
  }
  
  public String getCssSpecifique() {
    return cssSpecifique;
  }
  
  public void setCssSpecifique(String cssSpecifique) {
    this.cssSpecifique = cssSpecifique;
  }
  
  public int getAccesPage() {
    return accesPage;
  }
  
  public void setAccesPage(int accesPage) {
    this.accesPage = accesPage;
  }
  
}
