/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.EnumTypeDocument;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionMesDonnees;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Magasin;
import ri.serien.webshop.outils.Outils;
import ri.serien.webshop.outils.mail.ParametreMail;

/**
 * Afficher les pages "Historique".
 * 
 * Cette classe se charge d'afficher les données du client. Il s'agit des coordonnées du client et de son historique de devis et de
 * commandes. Il est possible d'afficher le détail d'un devis ou d'une commande. A partir de ce détail, l'utilisateur peut intervenir
 * sur le devis ou la commande sous réserve qu'il ait les droits correspondants.
 * 
 * Actions possibles
 * - Commande en attente : Modifier / Annuler / Dupliquer
 * - Commande validée : Dupliquer
 * 
 * - Devis en attente : Modifier / Annuler / Dupliquer
 * - Devis validé : Commander / Dupliquer
 * - Devis envoyé : Commander / Dupliquer
 */
public class MesDonnees extends MouleVues {
  private final static String DEVIS = "Devis";
  private final static String COMMANDE = "Commande";
  
  /**
   * Constructeur.
   */
  public MesDonnees() {
    super(new GestionMesDonnees(), "mesDonnees",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "mesDonnees.css?" + MarbreEnvironnement.versionInstallee
            + "' rel='stylesheet'/><script src='scripts/MesDonnes.js?" + MarbreEnvironnement.versionInstallee + "'></script>",
        MarbreEnvironnement.ACCES_CLIENT);
    
    // Compléter le fil rouge
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("mesDonnées", "MesDonnees?retour=1", "Mes données", "My data"));
    listeFilRouge.add(new FilRouge("uneCde", "MesDonnees?cde", "Détails", "Detail"));
    listeFilRouge.add(new FilRouge("modifierCde", "MesDonnees?reprise", "Modifier ma commande", "Modify my order"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      
      // Récupérer les informations utiles
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
      
      // Contrôler l'accès à la page
      redirectionSecurite(request, response);
      
      // Générer la balise HEAD de la page
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      
      // Générer le bandeau supérieur de la page
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      // Tester si on doit afficher le détail d'un devis ou d'une commande
      if (request.getParameter("cde") != null) {
        out.println(afficherContenu(
            afficherDetailCommande(utilisateur, "<h2><span id='titrePage'>Détail commande</span></h2>", request.getParameter("cde")),
            "uneCde", request.getParameter("cde"), null, request.getSession()));
      }
      // Tester si on doit modifier un devis ou une commande
      else if (request.getParameter("reprise") != null) {
        // On checke d'abord qu'elle soit bien en attente
        if (((GestionMesDonnees) getMaGestion()).isCommandeEnAttente(utilisateur, request.getParameter("reprise"))) {
          ((GestionMesDonnees) getMaGestion()).repriseCommande(utilisateur, request.getParameter("reprise"), true);
          
          getServletContext().getRequestDispatcher("/monPanier").forward(request, response);
        }
        // Sinon retour à la case départ
        else {
          out.println(
              afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
                  "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
                  request.getParameter("type"), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
        }
      }
      // Tester si on doit dupliquer un devis ou une commande
      else if (request.getParameter("duplication") != null) {
        ((GestionMesDonnees) getMaGestion()).repriseCommande(utilisateur, request.getParameter("duplication"), false);
        
        getServletContext().getRequestDispatcher("/monPanier").forward(request, response);
      }
      // Tester si on doit annuler un devis ou une commande
      else if (request.getParameter("annulation") != null) {
        // On checke d'abord qu'elle soit bien en attente
        if (((GestionMesDonnees) getMaGestion()).isCommandeEnAttente(utilisateur, request.getParameter("annulation"))) {
          ((GestionMesDonnees) getMaGestion()).annulationCommande(utilisateur, request.getParameter("annulation"));
          
          out.println(
              afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
                  "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
                  request.getParameter("type"), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
        }
        else {
          out.println(
              afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
                  "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
                  request.getParameter("type"), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
        }
      }
      // Tester si on doit transformer un devis en commande
      else if (request.getParameter("transform") != null) {
        String retour = request.getParameter("transform");
        String magasin = request.getParameter("magasin");
        // On checke d'abord que le devis soit bien adapté à la transformation
        if (((GestionMesDonnees) getMaGestion()).isDevisValide(utilisateur, request.getParameter("transform"))) {
          retour = ((GestionMesDonnees) getMaGestion()).transformerEnCommande(utilisateur, request.getParameter("transform"));
          if (retour != null && retour.length() == 7) {
            
            // Tester si l'utilisateur veut recevoir des mails
            if (utilisateur.getEtablissementEnCours().isOk_envois_mails()) {
              // Préparer et mettre en évidence les paramètres du document
              String codeDocument = "E";
              String numeroDocument = retour.substring(0, 6);
              String suffixeDocument = retour.substring(6, 7);
              
              // Préparation du mail de confirmation de commande au client
              ParametreMail parametreMail = ((GestionMesDonnees) getMaGestion()).preparerParametreMailPourCommande(utilisateur,
                  numeroDocument, suffixeDocument, codeDocument, EnumTypeDocument.COMMANDE, false);
              utilisateur.envoyerMail(utilisateur.getLogin(), parametreMail, EnumTypeMail.MAIL_CONFIRMATION_COMMANDE_HTML);
              
              // Préparation du mail de rapport de commande au gestionnaire
              parametreMail = ((GestionMesDonnees) getMaGestion()).preparerParametreMailPourCommande(utilisateur, numeroDocument,
                  suffixeDocument, codeDocument, EnumTypeDocument.COMMANDE, true);
              utilisateur.envoyerMail(retournerAdresseMailConfirmation(magasin, codeDocument, utilisateur), parametreMail,
                  EnumTypeMail.MAIL_RAPPORT_COMMANDE_HTML);
            }
          }
        }
        
        out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), retour,
            "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
            request.getParameter("type"), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
      }
      else if (request.getParameter("retour") != null) {
        out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
            "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
            utilisateur.getTypeDonneesEncours(), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
      }
      // On affiche les données et les commandes du client
      else {
        out.println(afficherContenu(afficherMesDonnees(utilisateur, request.getParameter("annulation"), request.getParameter("transform"),
            "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("Historique") + "</span></h2>",
            request.getParameter("type"), request.getParameter("indiceDebut")), null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la génaration de la page");
    }
  }
  
  /**
   * Afficher détails du contact et client
   */
  private String afficherMesDonnees(UtilisateurWebshop utilisateur, String annulation, String transformation, String titre,
      String typeCde, String indiceDebut) {
    if (utilisateur == null) {
      return "";
    }
    StringBuilder retour = new StringBuilder(10000);
    
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (annulation != null) {
      String det = "la";
      String typeDoc = "commande";
      if (annulation.startsWith("D")) {
        det = "le";
        typeDoc = "devis";
      }
      retour.append("<div id='messagePrincipal'>Vous avez annulé avec succés " + det + " " + typeDoc + " N° " + annulation + "</div>");
    }
    if (transformation != null) {
      retour.append("<div id='messagePrincipal'>Vous avez créé avec succés la commande N° " + transformation + "</div>");
    }
    
    // Récup des infos du clients
    // ---------------------------
    retour.append("<h3><span class='puceH3'>&nbsp;</span>" + utilisateur.getTraduction().traduire("$$monCompte") + "</h3>");
    
    if (utilisateur.getClient() != null && utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_CONSULTATION) {
      retour.append("<div class='sousBlocs'>");
      retour.append("<p class='decoH5'>&nbsp;</p>");
      retour.append("<h5>" + utilisateur.getTraduction().traduire("$$contact") + "</h5>");
      retour.append("<span id='RECIV' placeholder='cv'>");
      
      if (utilisateur.getCiviliteContact() != null && !utilisateur.getCiviliteContact().equals("")) {
        retour.append(utilisateur.getCiviliteContact());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_RECIV'><p class='paddingInterne'>Civ.</p></p>");
      }
      retour.append("</span>");
      retour.append("<span id='REPAC' placeholder='" + utilisateur.getTraduction().traduire("$$nom") + "'>");
      
      if (utilisateur.getNomContact() != null && !utilisateur.getNomContact().trim().equals("")) {
        retour.append(utilisateur.getNomContact().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_REPAC'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$nom") + "</p></p>");
      }
      retour.append("</span>");
      
      retour.append("<span id='RETEL' placeholder='téléphone'>tel: ");
      
      if (utilisateur.getTelContact() != null && !utilisateur.getTelContact().trim().equals("")) {
        retour.append(utilisateur.getTelContact().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_RETEL'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$telephone") + "</p></p>");
      }
      retour.append("</span>");
      retour.append("</div>");
      
      retour.append("<div class='sousBlocs'>");
      retour.append("<p class='decoH5'>&nbsp;</p>");
      retour.append("<h5>" + utilisateur.getTraduction().traduire("$$societe") + "</h5>");
      retour.append("<span id='CLCLI'>N° ");
      if (utilisateur.getClient().getNumeroClient() != null && (!utilisateur.getClient().getNumeroClient().trim().equalsIgnoreCase(""))) {
        retour.append(utilisateur.getClient().getNumeroClient().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_CLCLI'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$numero") + "</p></p>");
      }
      retour.append("</span>");
      
      retour.append("<span id='CLNOM' placeholder='" + utilisateur.getTraduction().traduire("$$societe") + "'>");
      if (utilisateur.getClient().getNomClient() != null && (!utilisateur.getClient().getNomClient().trim().equalsIgnoreCase(""))) {
        retour.append(utilisateur.getClient().getNomClient().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_CLNOM'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$societe") + "</p></p>");
      }
      retour.append("</span>");
      
      retour.append("<span id='CLCPL' placeholder='" + utilisateur.getTraduction().traduire("$$complement") + "'>");
      if (utilisateur.getClient().getComplementClient() != null
          && (!utilisateur.getClient().getComplementClient().trim().equalsIgnoreCase(""))) {
        retour.append(utilisateur.getClient().getComplementClient().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_CLCPL'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$complement") + "</p></p>");
      }
      retour.append("</span>");
      
      retour.append("<span id='CLRUE' placeholder='" + utilisateur.getTraduction().traduire("$$adresse") + " 1'>");
      if (utilisateur.getClient().getRueClient() != null && !utilisateur.getClient().getRueClient().trim().equalsIgnoreCase("")) {
        retour.append(utilisateur.getClient().getRueClient().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_CLRUE'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$adresse") + " 1</p></p>");
      }
      retour.append("</span>");
      
      retour.append("<span id='CLLOC'  placeholder='" + utilisateur.getTraduction().traduire("$$adresse") + " 2'>");
      if (utilisateur.getClient().getLocaliteClient() != null
          && (!utilisateur.getClient().getLocaliteClient().trim().equalsIgnoreCase(""))) {
        retour.append(utilisateur.getClient().getLocaliteClient().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_CLLOC'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$adresse") + " 2</p></p>");
      }
      retour.append("</span>");
      retour.append("<span id='CLVIL' placeholder='" + utilisateur.getTraduction().traduire("$$ville") + "'>");
      if (utilisateur.getClient().getVilleClient() != null && (!utilisateur.getClient().getVilleClient().trim().equalsIgnoreCase(""))) {
        retour.append(utilisateur.getClient().getVilleClient().trim());
      }
      else {
        retour.append("<p class='placeholderSpan' id='fi_CLVIL'><p class='paddingInterne'>"
            + utilisateur.getTraduction().traduire("$$ville") + "</p></p>");
      }
      retour.append("</span>");
      retour.append("</div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>vous n'êtes pas autorisé à modifier des commandes</div>");
    }
    retour.append("</div>");
    
    // Affiche les commandes/devis du client
    // si type de commande est null, alors on le force à "E" pour extraire les commandes en priorité
    if (typeCde == null) {
      typeCde = "E";
    }
    
    // si on pagine pas
    if (utilisateur.getClient() != null && utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_CONSULTATION) {
      
      if (indiceDebut == null) {
        GestionMesDonnees.initDerniereListePagination(utilisateur, ((GestionMesDonnees) getMaGestion()).recupererCommandesClient(
            utilisateur, utilisateur.getClient().getNumeroClient(), utilisateur.getClient().getSuffixeClient(), typeCde));
        // si on pagine
      }
      else {
        GestionMesDonnees.majListePartiellePagination(utilisateur, indiceDebut);
      }
      
      String texteTitre = utilisateur.getTraduction().traduire("$$vosCommandes");
      if (utilisateur.getDerniereListePartiellePagination() == null || utilisateur.getDernierTypeBon() == null) {
        retour.append("<p>Aucune donnée</p>");
      }
      else {
        
        if (utilisateur.getDernierTypeBon().equals("D")) {
          texteTitre = utilisateur.getTraduction().traduire("$$vosDevis");
        }
        
        retour.append("<div class='blocContenu' id='listeCommandes'>");
        retour.append("<h3><span class='puceH3'>&nbsp;</span>" + texteTitre + "</h3>");
        if (utilisateur.getDernierTypeBon().equals("D")) {
          retour.append(
              "<a href='MesDonnees?type=E' class='mesBoutons'><< " + utilisateur.getTraduction().traduire("$$vosCommandes") + " >></a>");
        }
        else {
          retour.append(
              "<a href='MesDonnees?type=D' class='mesBoutons'><< " + utilisateur.getTraduction().traduire("$$vosDevis") + " >></a>");
        }
        
        if (utilisateur.getDerniereListePartiellePagination().size() > 0) {
          // entêtes de colonnes
          retour.append("<div class='listes' id='enTeteListes'>");
          
          retour.append("<span id='titreE1NUM'>" + utilisateur.getTraduction().traduire("$$numero") + "</span>");
          retour.append("<span id='titreE1RCC'>" + utilisateur.getTraduction().traduire("$$referenceLongue") + "</span>");
          
          retour.append("" + "<span id='titreE1CRE'>" + utilisateur.getTraduction().traduire("$$date") + "</span>"
              + "<span id='titreE1DLP'>" + utilisateur.getTraduction().traduire("$$prevue") + "</span>" + "<span id='titreE1THTL'>"
              + utilisateur.getTraduction().traduire("$$montant") + "</span>" + "<span id='titreSTATUT'>"
              + utilisateur.getTraduction().traduire("$$etat") + "</span>" + "<span id='titreTYPCDE'>"
              + utilisateur.getTraduction().traduire("$$type") + "</span>" + "</div>");
          
          // Affiche de la liste
          String refCde = null;
          boolean isDateValide = false;
          for (GenericRecord commande : utilisateur.getDerniereListePartiellePagination()) {
            String look = "";
            String verCde = "";
            isDateValide = false;
            // commande qui n'est pas en attente: on ne peut pas la modifier
            if (commande.isPresentField("E1ETA") && !commande.getField("E1ETA").toString().trim().equals("0") && typeCde.equals("E")) {
              look = "op";
            }
            if (typeCde.equals("D")) {
              isDateValide = ((GestionMesDonnees) getMaGestion()).verifierDateValiditeDevis(utilisateur,
                  commande.getField("E1DAT2").toString().trim());
              int etatDevis = -1;
              try {
                etatDevis = Integer.parseInt(commande.getField("E1TDV").toString().trim());
              }
              catch (Exception e) {
                
              }
              
              if (etatDevis > 2 || !isDateValide) {
                look = "op";
              }
            }
            // Si top different de 0, la commande est verouillé
            if (!commande.getField("E1TOP").toString().trim().equals("0")) {
              look = "op";
              verCde = "(v)";
            }
            else {
              verCde = "";
            }
            // Si commande verouillé, pas de lien
            if (!verCde.equals("(v)")) {
              retour.append("<a class='listes' href='MesDonnees?cde=" + commande.getField("E1COD") + commande.getField("E1NUM")
                  + commande.getField("E1SUF") + "'>");
            }
            else {
              retour.append("<a class='listes' >");
            }
            
            retour.append("<span class='E1NUM" + look + "'>" + commande.getField("E1NUM").toString().trim()
                + commande.getField("E1SUF").toString().trim() + "</span>");
            refCde = null;
            if (!commande.getField("E1NCC").toString().trim().equals("")) {
              refCde = commande.getField("E1NCC").toString().trim();
            }
            if (!commande.getField("E1RCC").toString().trim().equals("")) {
              if (refCde == null) {
                refCde = commande.getField("E1RCC").toString().trim();
              }
              else {
                refCde += "/" + commande.getField("E1RCC").toString().trim();
              }
            }
            if (refCde == null) {
              refCde = "";
            }
            retour.append("<span class='E1RCC" + look + "'>" + refCde + "</span>");
            retour.append("<span class='E1CRE" + look + "'>"
                + Outils.transformerDateSeriemEnHumaine(commande.getField("E1CRE").toString()) + "</span>" + "<span class='E1DLP" + look
                + "'>" + Outils.transformerDateSeriemEnHumaine(commande.getField("E1DLP").toString()) + "</span>" + "<span class='E1THTL"
                + look + "'>" + Outils.formaterUnTarifArrondi(utilisateur, commande.getField("E1THTL")) + "</span><span class='STATUT"
                + look + "'>" + afficherEtatBon(commande, isDateValide) + verCde + "</span>");
            retour
                .append("<span class='TYPCDE" + look + "'>" + afficherTypeCde(commande.getField("E1IN18").toString().trim()) + "</span>");
            
            retour.append("</a>");
          }
        }
      }
    }
    
    // Pagination
    retour.append(afficherPagination(utilisateur, indiceDebut, null));
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Afficher le détail d'un devis ou d'une commande.
   * 
   * @param pUtilisateur Utilisateur connecté.
   * @param pTitre Titre de la page à afficher (ce titre est modifié dans le cas d'un devis).
   * @param pNumero Numéor du devis ou de la commande à afficher.
   * @return Corps de la page.
   */
  private String afficherDetailCommande(UtilisateurWebshop pUtilisateur, String pTitre, String pNumero) {
    // Tester les paramètres
    if (pUtilisateur == null) {
      return "";
    }
    
    // Ne pas afficher le document si l'utilisateur n'a ni le droit de saisir des devis ni celui de saisir des commandes
    if (!pUtilisateur.isSaisieDevisAutorisee() && !pUtilisateur.isSaisieCommandeAutorisee()) {
      return "";
    }
    
    // Préparer le corps de la page
    StringBuilder retour = new StringBuilder(10000);
    
    // Tester si c'est un devis ou une commande
    String codeDocument = pNumero.substring(0, 1);
    pUtilisateur.setTypeDonneesEncours(codeDocument);
    
    String typeDocument = null;
    if (codeDocument.equals("E")) {
      typeDocument = COMMANDE;
    }
    else {
      typeDocument = DEVIS;
      
      // Modifier le titre si c'est un devis
      pTitre = "<h2><span id='titrePage'>Détail devis</span></h2>";
    }
    
    // Afficher le titre
    retour.append(pTitre);
    retour.append("<div class='blocContenu' id='detailCommande'>");
    
    // Récupérer les lignes du document et les définir comme lignes en cours pour l'utilisateur
    ArrayList listeLigne = ((GestionMesDonnees) getMaGestion()).recupererDetailCommande(pUtilisateur, pNumero, false);
    pUtilisateur.setListeDeTravail(listeLigne);
    
    // Tester si des lignes sont en cours (on n'affiche rien sinon)
    if (pUtilisateur.getListeDeTravail() != null) {
      String documentLie = null;
      boolean isDateValide = false;
      Magasin magasin = null;
      
      // Récupérer l'entête du document et le définir comme en cours pour l'utilisateur
      GenericRecord record = ((GestionMesDonnees) getMaGestion()).recupererEnteteCommande(pUtilisateur, pNumero);
      pUtilisateur.setRecordTravail(record);
      
      // Vérifier qu'un entête est présent
      if (pUtilisateur.getRecordTravail() != null) {
        
        // Récupérer le document de vente lié (devis pour commande, commande pour devis)
        documentLie = ((GestionMesDonnees) getMaGestion()).retournerDocumentLie(pUtilisateur, pNumero);
        String typeDocumentLie = null;
        if (typeDocument.equals(DEVIS)) {
          isDateValide = ((GestionMesDonnees) getMaGestion()).verifierDateValiditeDevis(pUtilisateur,
              pUtilisateur.getRecordTravail().getField("E1DAT2").toString().trim());
          typeDocumentLie = COMMANDE;
        }
        else if (typeDocument.equals(COMMANDE)) {
          typeDocumentLie = DEVIS;
        }
        
        // Afficher le numéro du devis ou de la commande (format "Commande N°356595/0")
        retour.append("<h3><span class='puceH3'>&nbsp;</span><span class='zwwwiip'>" + typeDocument + " </span>N° "
            + pNumero.substring(1, 7) + "/" + pNumero.substring(7, 8) + "</h3>");
        
        // Afficher un message si la date limite du devis est dépassé
        if (typeDocument.equals(DEVIS) && !isDateValide) {
          retour.append("<div id='messagePrincipal'>" + pUtilisateur.getTraduction().traduire("$$validiteDepassee") + "</div>");
        }
        
        // AFFICHER L'ENCART ENTETE
        retour.append("<div class='sousBlocs' id='enTeteCde'>");
        retour.append("<p class='decoH5'>&nbsp;</p>");
        retour.append("<h5>Entête</h5>");
        
        // Afficher le document lié
        if (documentLie != null) {
          retour.append(
              "<label class='labelsZones'>" + typeDocumentLie + "</label><span id='DOCLIE'><a class='docLie' href='mesDonnees?cde="
                  + documentLie.substring(0, 1) + documentLie.substring(4, 10) + documentLie.substring(10, 11) + "'>"
                  + documentLie.substring(4, 10) + "/" + documentLie.substring(10, 11) + "</a></span>");
        }
        
        // Afficher le statut
        retour.append("<label class='labelsZones'>Statut</label><span id='E1ETA'>"
            + afficherEtatBon(pUtilisateur.getRecordTravail(), isDateValide) + "</span>");
        
        // Afficher la référence courte
        if (pUtilisateur.getRecordTravail().isPresentField("E1NCC")
            && !pUtilisateur.getRecordTravail().getField("E1NCC").toString().trim().equals("")) {
          retour.append("<label class='labelsZones'>" + pUtilisateur.getTraduction().traduire("$$referenceCourte")
              + "</label><span id='E1NCC'>" + pUtilisateur.getRecordTravail().getField("E1NCC").toString().trim() + "</span>");
        }
        
        // Afficher la référence longue
        if (pUtilisateur.getRecordTravail().isPresentField("E1RCC")
            && !pUtilisateur.getRecordTravail().getField("E1RCC").toString().trim().equals("")) {
          retour.append("<label class='labelsZones'>" + pUtilisateur.getTraduction().traduire("$$referenceLongue")
              + "</label><span id='E1RCC'>" + pUtilisateur.getRecordTravail().getField("E1RCC").toString().trim() + "</span>");
        }
        
        // Afficher la date de création
        if (pUtilisateur.getRecordTravail().isPresentField("E1CRE")) {
          retour.append("<label class='labelsZones'>Date de création</label><span id='E1CREd'>"
              + Outils.transformerDateSeriemEnHumaine(pUtilisateur.getRecordTravail().getField("E1CRE").toString()) + "</span>");
        }
        
        // Afficher le mode de récupération (livraison ou un enlèvement)
        if (pUtilisateur.getRecordTravail().isPresentField("E1MEX")) {
          if (pUtilisateur.getRecordTravail().getField("E1MEX").toString().trim().equals("WE")) {
            // Afficher le mode de récupération retrait
            retour.append("<label class='labelsZones'>Mode de récupération</label><span id='E1MEX'>Retrait en magasin</span>");
            
            // Afficher le magasin de retrait
            if (pUtilisateur.getRecordTravail().isPresentField("E1MAG")
                && !pUtilisateur.getRecordTravail().getField("E1MAG").toString().trim().equals("")) {
              magasin = new Magasin(pUtilisateur, pUtilisateur.getRecordTravail().getField("E1MAG").toString().trim());
              retour.append(
                  "<label class='labelsZones'>Magasin de retrait</label><span id='E1MAG'>" + magasin.getLibelleMagasin() + "</span>");
            }
          }
          else if (pUtilisateur.getRecordTravail().getField("E1MEX").toString().trim().equals("WL")) {
            // Afficher le mode de récupération livraison
            retour.append("<label class='labelsZones'>Mode de récupération</label><span id='E1MEX'>Livraison</span> ");
          }
        }
        
        // Affichée la date souhaitée (commande), la date prévue (commande) ou la date de validité (devis)
        if (typeDocument.equals(COMMANDE)) {
          if (pUtilisateur.getRecordTravail().isPresentField("E1DLS")) {
            retour.append("<label class='labelsZones'>Date souhaitée</label><span id='E1DLS'>"
                + Outils.transformerDateSeriemEnHumaine(pUtilisateur.getRecordTravail().getField("E1DLS").toString()) + "</span>");
          }
          if (pUtilisateur.getRecordTravail().isPresentField("E1DLP")) {
            retour.append("<label class='labelsZones'>Date prévue</label><span id='E1DLP'>"
                + Outils.transformerDateSeriemEnHumaine(pUtilisateur.getRecordTravail().getField("E1DLP").toString()) + "</span>");
          }
        }
        else if (typeDocument.equals(DEVIS)) {
          if (pUtilisateur.getRecordTravail().isPresentField("E1DAT2")) {
            retour.append("<label class='labelsZones'>Date de validité</label><span id='E1DLS'>"
                + Outils.transformerDateSeriemEnHumaine(pUtilisateur.getRecordTravail().getField("E1DAT2").toString()) + "</span>");
          }
        }
        
        // FIN ENTETE
        retour.append("</div>");
        
        // Afficher l'encart livraison si c'est une livraison
        if (pUtilisateur.getRecordTravail().isPresentField("E1MEX")
            && pUtilisateur.getRecordTravail().getField("E1MEX").toString().trim().equals("WL")) {
          // Commencer l'encart livraison
          retour.append("<div class='sousBlocs'>");
          retour.append("<p class='decoH5'>&nbsp;</p>");
          retour.append("<h5>Adresse de livraison</h5>");
          
          // Afficher le nom
          retour.append("<span id='CLINOM'>");
          if (pUtilisateur.getRecordTravail().isPresentField("CLINOM")
              && !pUtilisateur.getRecordTravail().getField("CLINOM").toString().trim().equals("")) {
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(pUtilisateur.getRecordTravail().getField("CLINOM").toString().trim()));
          }
          else {
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Nom ou raison sociale</p></p>");
          }
          retour.append("</span>");
          
          // Afficher le complément de nom
          retour.append("<span id='CLICPL'>");
          if (pUtilisateur.getRecordTravail().isPresentField("CLICPL")
              && !pUtilisateur.getRecordTravail().getField("CLICPL").toString().trim().equals("")) {
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(pUtilisateur.getRecordTravail().getField("CLICPL").toString().trim()));
          }
          else {
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Nom du responsable</p></p>");
          }
          retour.append("</span>");
          
          // Afficher la rue
          retour.append("<span id='CLIRUE'>");
          if (pUtilisateur.getRecordTravail().isPresentField("CLIRUE")
              && !pUtilisateur.getRecordTravail().getField("CLIRUE").toString().trim().equals("")) {
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(pUtilisateur.getRecordTravail().getField("CLIRUE").toString().trim()));
          }
          else {
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Adresse - rue</p></p>");
          }
          retour.append("</span>");
          
          // Afficher la localité
          retour.append("<span id='CLILOC'>");
          if (pUtilisateur.getRecordTravail().isPresentField("CLILOC")
              && !pUtilisateur.getRecordTravail().getField("CLILOC").toString().trim().equals("")) {
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(pUtilisateur.getRecordTravail().getField("CLILOC").toString().trim()));
          }
          else {
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Adresse - localité</p></p>");
          }
          retour.append("</span>");
          
          // Afficher le code postal et la ville
          retour.append("<span id='CLIVIL'>");
          if (pUtilisateur.getRecordTravail().isPresentField("CLICDP")
              && !pUtilisateur.getRecordTravail().getField("CLICDP").toString().trim().equals("")) {
            retour.append(getMaGestion()
                .traiterCaracteresSpeciauxAffichage(pUtilisateur.getRecordTravail().getField("CLICDP").toString().trim() + " "));
          }
          if (pUtilisateur.getRecordTravail().isPresentField("CLIVIL")
              && !pUtilisateur.getRecordTravail().getField("CLIVIL").toString().trim().equals("")) {
            retour.append(
                getMaGestion().traiterCaracteresSpeciauxAffichage(pUtilisateur.getRecordTravail().getField("CLIVIL").toString().trim()));
          }
          else {
            retour.append("<p class='placeholderSpan'><p class='paddingInterne'>Ville</p></p>");
          }
          retour.append("</span>");
          
          // Terminer l'encart livraison
          retour.append("</div>");
        }
      }
      
      // Récuperer les commentaires du document
      ArrayList<GenericRecord> listeCommentaire =
          ((GestionMesDonnees) getMaGestion()).recupererCommentaireCommande(pUtilisateur, pNumero);
      if (listeCommentaire != null && listeCommentaire.size() > 0 && listeCommentaire.get(0).isPresentField("XILIB")
          && !listeCommentaire.get(0).getField("XILIB").toString().trim().equals("")) {
        // Afficher les commentaires
        retour.append("<div class='sousBlocs' id='blocCommentaires'>");
        retour.append("<p class='decoH5'>&nbsp;</p>");
        retour.append("<h5>Commentaires</h5>");
        retour.append("<div id='XILIB'>");
        for (int i = 0; i < listeCommentaire.size(); i++) {
          if (listeCommentaire.get(i).isPresentField("XILIB")) {
            retour.append(getMaGestion().traiterCaracteresSpeciauxAffichage(listeCommentaire.get(i).getField("XILIB").toString().trim()));
          }
        }
        retour.append("</div>");
        retour.append("</div>");
      }
      
      // Afficher les boutons permettant d'agir sur le document
      // Tester si le document a été saisi sur le Webshop et si l'utilisateur a le droit de faire des actions
      if (pUtilisateur.getRecordTravail().getField("E1IN18").toString().equals("W") && pUtilisateur.getClient() != null
          && pUtilisateur.getClient().isAutoriseActions()) {
        // Commencer l'encart boutons
        retour.append("<div id='optionsCommande'>");
        
        // Tester si c'est une commande
        // Afficher les boutons uniquement si l'utilisateur a le droit de saisir des commandes
        if (typeDocument.equals(COMMANDE) && pUtilisateur.isSaisieCommandeAutorisee()) {
          // Tester si c'est une commande en attente sans document lié
          if (pUtilisateur.getRecordTravail().getField("E1ETA").toString().equals("0") && documentLie == null) {
            // Ajouter le bouton "Modifier"
            retour.append("<a id='modifCommande' onClick = \"confirmerUnLien('MesDonnees?reprise=" + pNumero
                + "','Voulez vous vraiment modifier cette commande ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Modifier >></a>");
            
            // Ajouter le bouton "Annuler"
            retour.append("<a id='annulCommande' onClick = \"confirmerUnLien('MesDonnees?annulation=" + pNumero
                + "','Voulez vous vraiment annuler cette commande ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Annuler >></a>");
          }
          
          // Ajouter le bouton "Dupliquer"
          retour.append("<a id='duplicCommande' onClick = \"confirmerUnLien('MesDonnees?duplication=" + pNumero
              + "','Voulez vous vraiment dupliquer ce " + typeDocument
              + " ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Dupliquer >></a>");
        }
        // Tester si c'est un devis valide avec magasin
        else if (typeDocument.equals(DEVIS) && isDateValide && magasin != null) {
          // Tester si c'est un devis en attente
          // Afficher les boutons uniquement si l'utilisateur a le droit de saisir des devis
          if (pUtilisateur.getRecordTravail().getField("E1TDV").toString().equals("0") && pUtilisateur.isSaisieDevisAutorisee()) {
            // Ajouter le bouton "Modifier"
            retour.append("<a id='modifCommande' onClick = \"confirmerUnLien('MesDonnees?reprise=" + pNumero + "&magasin="
                + magasin.getCodeMagasin()
                + "','Voulez vous vraiment modifier ce devis ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Modifier >></a>");
            
            // Ajouter le bouton "Annuler"
            retour.append("<a id='annulCommande' onClick = \"confirmerUnLien('MesDonnees?annulation=" + pNumero
                + "','Voulez vous vraiment annuler ce devis ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Annuler >></a>");
            
            // Ajouter le bouton "Dupliquer"
            retour.append("<a id='duplicCommande' onClick = \"confirmerUnLien('MesDonnees?duplication=" + pNumero
                + "','Voulez vous vraiment dupliquer ce " + typeDocument
                + " ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Dupliquer >></a>");
          }
          // Tester si c'est un devis validé ou envoyé
          else if ((pUtilisateur.getRecordTravail().getField("E1TDV").toString().equals("1")
              || pUtilisateur.getRecordTravail().getField("E1TDV").toString().equals("2"))) {
            // Afficher le bouton uniquement si l'utilisateur a le droit de saisir des commandes
            if (pUtilisateur.isSaisieCommandeAutorisee()) {
              // Ajouter le bouton "Commander"
              retour.append("<a id='annulCommande' onClick = \"confirmerUnLien('MesDonnees?transform=" + pNumero + "&magasin="
                  + magasin.getCodeMagasin() + "','Voulez vous vraiment transformer ce devis (" + typeDocument.toLowerCase()
                  + ") en commande ?')\" href=\"javascript:void(0)\" class='mesBoutons'>< Commander ></a>");
            }
            
            // Afficher le bouton uniquement si l'utilisateur a le droit de saisir des devis
            if (pUtilisateur.isSaisieDevisAutorisee()) {
              // Ajouter le bouton "Dupliquer"
              retour.append("<a id='duplicCommande' onClick = \"confirmerUnLien('MesDonnees?duplication=" + pNumero
                  + "','Voulez vous vraiment dupliquer ce " + typeDocument
                  + " ?')\" href=\"javascript:void(0)\" class='mesBoutons'><< Dupliquer >></a>");
            }
          }
        }
        
        // Terminer l'encart boutons
        retour.append("</div>");
      }
      
      // entêtes de colonnes
      retour.append("<div class='listes' id='enTeteListes'>" + "<span id='titreA1ART'>Article</span>"
          + "<span id='titreA1LIB'>Désignation</span>" + "<span id='titreL1PVC'>Tarif H.T</span>"
          + "<span id='titreL1QTE'>Quantité</span>" + "<span id='titreL1MHT'>Total H.T</span>" + "</div>");
      
      // Afficher la liste des articles du document
      String totalEcotaxe = null;
      for (GenericRecord ligne : pUtilisateur.getListeDeTravail()) {
        // Déterminer la référence fournisseur ou le code article Série M
        String referenceAffichee = "Aucune";
        if (pUtilisateur.getEtablissementEnCours().getZone_reference_article().equals("CAREF") && ligne.isPresentField("CAREF")
            && !ligne.getField("CAREF").toString().trim().equals("")) {
          referenceAffichee = ligne.getField("CAREF").toString().trim();
        }
        else if (ligne.isPresentField("L1ART")) {
          referenceAffichee = ligne.getField("L1ART").toString().trim();
        }
        else {
          referenceAffichee = "Aucune";
        }
        
        // Vérifier les données de l'article de la ligne
        if (!ligne.isPresentField("A1LIB")) {
          if (ligne.isPresentField("L1ERL") && ligne.getField("L1ERL").toString().trim().equals("S")
              && pUtilisateur.getEtablissementEnCours().isGestion_ecotaxe() && ligne.isPresentField("L1NLI")
              && ligne.getField("L1NLI").toString().trim().equals("9770")) {
            ligne.setField("A1LIB", "Ecotaxe");
            totalEcotaxe = Outils.formaterUnTarifArrondi(pUtilisateur, ligne.getField("L1MHT"));
          }
          else {
            ligne.setField("A1LIB", referenceAffichee);
          }
        }
        
        // Ajouter l'article au tableau
        retour.append("<a class='listes' href='#'>" + "<span class='A1ART'>" + referenceAffichee + "</span>" + "<span class='A1LIB'>"
            + ligne.getField("A1LIB") + "</span>" + "<span class='L1PVC'>"
            + Outils.formaterUnTarifArrondi(pUtilisateur, ligne.getField("L1PVC")) + "</span>" + "<span class='L1QTE'>"
            + ligne.getField("L1QTE").toString().trim() + "</span>" + "<span class='L1MHT'>"
            + Outils.formaterUnTarifArrondi(pUtilisateur, ligne.getField("L1MHT")) + "</span>" + "</a>");
      }
      
      // Afficher le total du document
      if (pUtilisateur.getListeDeTravail().size() > 0 && pUtilisateur.getListeDeTravail().get(0).isPresentField("E1THTL")) {
        retour.append("<p class='messageRubrique'>Total de votre commande</p>");
        retour.append("<div id='piedBon'>");
        
        retour.append("<span class='TOTALHT'>Total HT : "
            + Outils.formaterUnTarifArrondi(pUtilisateur, pUtilisateur.getListeDeTravail().get(0).getField("E1THTL")) + " </span>");
        if (totalEcotaxe != null) {
          retour.append("<span class='TOTALECO'>Dont écotaxe : " + totalEcotaxe + " </span>");
        }
        
        retour.append("</div>");
      }
      else {
        retour.append("<p>Problème dans la commande, veuillez contacter votre agence.</p>");
      }
    }
    
    // Terminer le corps
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * affiche l'info de la commande Web
   */
  private String afficherTypeCde(String donnee) {
    if (donnee == null || donnee.trim().equals("")) {
      donnee = "<p class='Nvu'>Comptoir</p><p class='Ncache'>Com</p>";
    }
    else if (donnee.equals("M")) {
      donnee = "<p class='Wvu'>Représentant</p><p class='Wcache'>Rep</p>";
    }
    else if (donnee.equals("W")) {
      donnee = "<p class='Wvu'>Web <strong>Shop</strong></p><p class='Wcache'>Web</p>";
    }
    
    return donnee;
  }
  
  /**
   * Permet de formater l'état du bon en fonction de son état et de sa validité
   */
  private String afficherEtatBon(GenericRecord pRecord, boolean pIsDateValide) {
    String retour = "";
    
    if (pRecord == null || !pRecord.isPresentField("E1ETA") || !pRecord.isPresentField("E1TDV") || !pRecord.isPresentField("E1COD")) {
      return retour;
    }
    
    String typeBon = pRecord.getField("E1COD").toString().trim();
    String etatBon = null;
    if (typeBon.equals("E")) {
      etatBon = pRecord.getField("E1ETA").toString().trim();
    }
    else if (typeBon.equals("D")) {
      if (!pIsDateValide) {
        return "Validité dépassée";
      }
      etatBon = pRecord.getField("E1TDV").toString().trim();
    }
    if (etatBon == null) {
      return retour;
    }
    
    if (typeBon.equals("E")) {
      if (etatBon.equals("0")) {
        retour = "En attente";
      }
      else if (etatBon.equals("1")) {
        retour = "Validée";
      }
      else if (etatBon.equals("3")) {
        retour = "Validée";
      }
      // retour = "Reservée"; ON REMPLACE RESERVEE PAR VALIDEE
      else if (etatBon.equals("4")) {
        retour = "Expédiée";
      }
      else if (etatBon.equals("6") || etatBon.equals("7")) {
        retour = "Facturée";
      }
    }
    else if (typeBon.equals("D")) {
      if (etatBon.equals("0")) {
        retour = "En attente";
      }
      else if (etatBon.equals("1")) {
        retour = "Validé";
      }
      else if (etatBon.equals("2")) {
        retour = "Envoyé";
      }
      else if (etatBon.equals("3")) {
        retour = "Signé";
      }
      else if (etatBon.equals("4")) {
        retour = "Validité dépassée";
      }
      else if (etatBon.equals("5")) {
        retour = "Perdu";
      }
      else if (etatBon.equals("6")) {
        retour = "Clôturé";
      }
    }
    
    return retour;
  }
  
}
