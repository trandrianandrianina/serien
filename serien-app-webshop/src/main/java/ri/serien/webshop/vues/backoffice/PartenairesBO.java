/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionPartenairesBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class PartenairesBO
 */
public class PartenairesBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public PartenairesBO() {
    super(
        new GestionPartenairesBO(), "partenairesBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS
            + "partenairesBO.css' rel='stylesheet'/>" + "<script src='scripts/partenairesBO.js'></script>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("PartenairesBO", "PartenairesBO", "Les partenaires", ""));
    listeFilRouge.add(new FilRouge("partId", "PartenairesBO?partId=" + MarbreEnvironnement.PARAM_FILROUGE, "Un partenaire", ""));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if (request.getParameter("majPartenaire") != null) {
        out.println(afficherContenu(miseAjourPartenaire(request, request.getSession(), "<h2><span id='titrePage'>Partenaires</span></h2>",
            request.getParameter("idPart")), null, null, null, request.getSession()));
      }
      else if (request.getParameter("partId") != null || request.getParameter("Ajout") != null) {
        out.println(afficherContenu(
            unPartenaire((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), request.getParameter("partId"), request,
                "<h2><span id='titrePage'>Modifier un partenaire</span></h2>", request.getParameter("choixEtb")),
            "partId", null, null, request.getSession()));
      }
      else if (request.getParameter("supprimer") != null) {
        out.println(afficherContenu(supprimerPartenaire(request, request.getSession(), "<h2><span id='titrePage'>Partenaires</span></h2>",
            request.getParameter("supprimer")), null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(listePartenaires(request, (UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
            "<h2><span id='titrePage'>Partenaires</span></h2>"), null, null, null, request.getSession()));
      }
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affichage de tous les partenaires
   */
  
  private String listePartenaires(HttpServletRequest request, UtilisateurWebshop user, String titre) {
    if (request == null || ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")) == null) {
      return "";
    }
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String lienPart = "";
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Les partenaires &nbsp;&nbsp;</h3>");
    
    user.setListeDeTravail(((GestionPartenairesBO) maGestion).tousLesPartenaires(user));
    if (user.getListeDeTravail() != null && user.getListeDeTravail().size() > 0) {
      retour.append("<a class='ajouter' href='PartenairesBO?Ajout=1' >Ajouter un Partenaire</a>");
      
      // entête
      retour.append("<div class='listes' id='enTeteListes'>" + "<span id='NomPart'>NOM DU PARTENAIRE</span></div>");
      for (int i = 0; i < user.getListeDeTravail().size(); i++) {
        lienPart = "PartenairesBO?partId=" + user.getListeDeTravail().get(i).getField("PAR_ID").toString();
        retour.append("<div class='listes'>");
        // affiche le nom du partenaire
        retour.append("<a class='nomPart' href='" + lienPart + "'>");
        if (user.getListeDeTravail().get(i).isPresentField("PAR_ID")) {
          retour.append(user.getListeDeTravail().get(i).getField("PAR_NOM"));
        }
        else {
          retour.append("aucune");
        }
        retour.append("</a>");
        
        retour.append("<a href='#'  class='supprPartenaire' onClick = \"supprimerPartenaire("
            + user.getListeDeTravail().get(i).getField("PAR_ID").toString()
            + ");\" alt='supprimer un partenaire' title='supprimer un partenaire'><img class ='imageSuppr' src='"
            + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/poubelle.png'/></a>");
        
        retour.append("</div>");
        
      }
      
    }
    else {
      retour.append("<div id='messagePrincipal'>Pas de partenaire à afficher</div>");
    }
    retour.append("</div>");
    return retour.toString();
    
  }
  
  /**
   * Affiche un partenaire
   */
  
  private String unPartenaire(UtilisateurWebshop user, String idPart, HttpServletRequest request, String titre, String etbPart) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    String partId = null;
    if (idPart != null) {
      partId = idPart;
    }
    
    // recuperer etablissement
    retour.append("<div class='blocContenu'>");
    if (etbPart == null && user.getEtablissementEnCours() != null) {
      etbPart = user.getEtablissementEnCours().getCodeETB();
    }
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Un partenaire &nbsp;&nbsp;</h3>");
    if (MarbreEnvironnement.ETB_DEFAUT != null && etbPart != null) {
      String selected = "";
      retour.append("<form action='MetierBO' method='POST' name='choixEtbs' id='choixEtbs'>");
      retour.append(
          "<select id='choixETBParams' name='choixEtb' class='inputETB' onChange=\"document.location.href = 'MetierBO?choixEtb=' + this.value ;\">");
      retour.append("<option value='" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' " + selected + ">"
          + MarbreEnvironnement.ETB_DEFAUT.getLibelleETB() + "</option>");
      
      retour.append("</select>");
      retour.append("</form>");
      
      // je suis en creation d'un partenaire
      if (idPart == null) {
        retour.append("<form action='partenairesBO' method ='POST'>");
        retour.append("<input type=hidden name='etbPart'  value='" + etbPart + "' ><br/>");
        
        retour.append("<label class='labelNOM'>Son nom  </label>");
        retour.append("<input type='text'  class='inputImage' name='nomPART'  maxlength='20'  value='' required><br/>");
        retour.append("<label class='labelNOM'>Son logo </label>");
        retour.append("<input type='text' class='inputImage' name='nomIMAGE'  value='' required><br/>");
        retour.append("<label class='labelNOM'>Son Site </label>");
        retour.append("<input type='text' class='inputImage' name='nomWEB'  value='' required><br/>");
        retour.append("<input type='hidden'  id='majPartenaire' name='majPartenaire' value='1'>");
        retour.append("<input class='btnMAJ' TYPE='submit' value='Créer' />");
        retour.append("</form>");
      }
      else {
        GenericRecord unPart = ((GestionPartenairesBO) maGestion).unPartenaire(user, partId, etbPart);
        if (unPart != null && unPart.isPresentField("PAR_ID") && unPart.isPresentField("PAR_NOM")) {
          retour.append("<form action='partenairesBO' method ='POST'>");
          retour.append("<input type=hidden  name='etbPart'   value='" + unPart.getField("PAR_ETB").toString().trim() + "' ><br/>");
          retour.append("<label class='labelNOM'>Son nom </label>");
          retour.append("<input type='text' class='inputImage' name='nomPART'  maxlength='20'  value='"
              + unPart.getField("PAR_NOM").toString().trim().toUpperCase() + "' ><br/>");
          retour.append("<label class='labelNOM'>Son logo </label>");
          retour.append("<input type='text' class='inputImage' name='nomIMAGE'  value='" + unPart.getField("PAR_IMAGE").toString().trim()
              + "' ><br/>");
          retour.append("<label class='labelNOM'>Son site </label>");
          retour.append(
              "<input type='text' class='inputImage' name='nomWEB'  value='" + unPart.getField("PAR_WEB").toString().trim() + "' ><br/>");
        }
        retour.append("<input type='hidden'  id='idPart' name='idPart' value='" + idPart + "'>");
        retour.append("<input type='hidden'  id='majPartenaire' name='majPartenaire' value='1'>");
        retour.append("<input class='btnMAJ' TYPE='submit' value='Modifier' />");
        retour.append("</form>");
      }
    }
    retour.append("</div>");
    
    return retour.toString();
  }
  
  private String miseAjourPartenaire(HttpServletRequest request, HttpSession session, String titre, String idPart) {
    
    int numPart = 0;
    if (idPart != null) {
      numPart = Integer.parseInt(idPart);
    }
    
    StringBuilder retour = new StringBuilder();
    
    if (titre != null) {
      retour.append(titre);
    }
    
    retour.append("<div class='blocContenu'>");
    
    if (((GestionPartenairesBO) maGestion).majCreaPartenaires((UtilisateurWebshop) session.getAttribute("utilisateur"), request,
        numPart) == -1) {
      retour.append("<div id='messagePrincipal'>Erreur dans la mise à jour des partenaires </div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    
    retour.append(listePartenaires(request, (UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), ""));
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Affichage du résultat de la suppression d'un partenaire
   */
  private String supprimerPartenaire(HttpServletRequest request, HttpSession session, String titre, String idPart) {
    int numPart = 0;
    if (idPart != null) {
      numPart = Integer.parseInt(idPart);
    }
    
    StringBuilder retour = new StringBuilder();
    
    if (titre != null) {
      retour.append(titre);
    }
    
    retour.append("<div class='blocContenu'>");
    
    if (((GestionPartenairesBO) maGestion).suppressionPartenaire((UtilisateurWebshop) session.getAttribute("utilisateur"),
        numPart) == -1) {
      retour.append("<div id='messagePrincipal'>Erreur dans la suppression du partenaire " + idPart + "</div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>Suppression effectuée avec succès</div>");
    }
    
    retour.append(listePartenaires(request, (UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), ""));
    
    retour.append("</div>");
    return retour.toString();
  }
}
