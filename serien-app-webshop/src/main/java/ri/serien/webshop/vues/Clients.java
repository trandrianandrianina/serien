/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.Gestion;
import ri.serien.webshop.controleurs.GestionClients;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Servlet implementation class Clients
 */
public class Clients extends MouleVues {
  
  /**
   * @see MouleVues#MouleVues(Gestion, String, String, int)
   */
  public Clients() {
    super(new GestionClients(), "clients",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "clients.css?" + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_REPRES_WS);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("clients", "clients", "Clients", "Customers"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      boolean traitementIsOk = false;
      UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
      if (utilisateur == null) {
        return;
      }
      // TRAITEMENTS A EFFECTUER AVANT L'AFFICHAGE
      if (request.getParameter("client") != null && request.getParameter("suffixe") != null) {
        traitementIsOk =
            ((GestionClients) maGestion).attribuerUnClient(utilisateur, request.getParameter("client"), request.getParameter("suffixe"));
      }
      else if (request.getParameter("reinit") != null) {
        traitementIsOk = ((GestionClients) maGestion).retirerLeClient(utilisateur);
      }
      
      if (traitementIsOk && utilisateur.getParametresEnCours() != null) {
        getServletContext().getRequestDispatcher("/" + utilisateur.getParametresEnCours()).forward(request, response);
      }
      else {
        super.traiterPOSTouGET(request, response);
        ServletOutputStream out = response.getOutputStream();
        String connexion = pattern.afficherConnexion(nomPage, request, response);
        
        redirectionSecurite(request, response);
        
        out.println(pattern.majDuHead(request, cssSpecifique, null));
        out.println(pattern.afficherPresentation(nomPage, request, connexion));
        out.println(afficherContenu(afficherAccueil(request, "<h2><span id='titrePage'>Recherche clients</span></h2>", traitementIsOk),
            "clients", null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      Trace.erreur(e.getMessage());
    }
  }
  
  /**
   * afficher la page d'accueil du module de choix clients
   */
  private String afficherAccueil(HttpServletRequest request, String titre, boolean traitementIsOk) {
    if (request == null || (UtilisateurWebshop) request.getSession().getAttribute("utilisateur") == null) {
      return "";
    }
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    
    String retour = titre;
    retour += "<div class='blocContenu' >";
    if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getListeArticles() != null
        && utilisateur.getMonPanier().getListeArticles().size() > 0) {
      retour +=
          "<div  id='messagePrincipal'>Vous ne pouvez pas changer de client tant que le panier ne cours n'est pas validé ou annulé</div>";
    }
    
    if (request.getParameter("client") != null && request.getParameter("suffixe") != null) {
      retour += afficherMessageAttributionClient(request, traitementIsOk);
    }
    else if (request.getParameter("reinit") != null) {
      retour += afficherMessageRetraitClient(request, traitementIsOk);
    }
    retour += afficherFormulaireRecherche(request);
    retour += afficherListeClients(request, request.getParameter("champClients"));
    
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * afficher le message de confirmation du retrait du client à la session du représentant
   */
  private String afficherMessageRetraitClient(HttpServletRequest request, boolean traitementIsOk) {
    if (request == null || request.getSession().getAttribute("utilisateur") == null) {
      return "";
    }
    
    String retour = "";
    
    if (traitementIsOk) {
      retour = "<div id='messagePrincipal'>Vous avez retiré ce client de votre session</div>";
    }
    else {
      retour = "<div class='messageAlerte'>Problème de retrait du client associé à votre session</div>";
    }
    
    return retour;
  }
  
  /**
   * Afficher le message de retour d'attribution d'un client
   */
  private String afficherMessageAttributionClient(HttpServletRequest request, boolean traitementIsOk) {
    if (request == null || request.getSession().getAttribute("utilisateur") == null) {
      return "";
    }
    
    String retour = "";
    
    if (request.getSession().getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      if (traitementIsOk) {
        retour = "<div id='messagePrincipal'>Client attribué avec Succès</div>";
      }
      else {
        retour = "<div class='messageAlerte'>Problème d'attribution du client " + request.getParameter("client") + "/"
            + request.getParameter("suffixe") + "</div>";
      }
    }
    
    return retour;
  }
  
  /**
   * afficher la liste des clients à partir de l'expression saisie ou par défaut les clients du représentant
   */
  private String afficherListeClients(HttpServletRequest request, String expression) {
    if (request == null || ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")) == null) {
      return "";
    }
    
    UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
    StringBuilder retour = null;
    
    retour = new StringBuilder(15000);
    
    // si on pagine pas
    if (request.getParameter("indiceDebut") == null) {
      GestionClients.initDerniereListePagination(utilisateur,
          ((GestionClients) maGestion).retournerListeClients(utilisateur, expression));
      // si on pagine
    }
    else {
      GestionClients.majListePartiellePagination(utilisateur, request.getParameter("indiceDebut"));
    }
    
    if (utilisateur.getDerniereListePartiellePagination() != null && utilisateur.getDerniereListePartiellePagination().size() > 0) {
      retour.append("   <div class='listes' id='enTeteListes'>" + "<span id='titre_CLCLI'>Numéro client</span>"
          + "<span id='titre_CLNOM'>Nom client</span>" + "<span id='titre_action'>Action</span>" + "</div>");
      for (int i = 0; i < utilisateur.getDerniereListePartiellePagination().size(); i++) {
        if (utilisateur.getDerniereListePartiellePagination().get(i).isPresentField("CLNOM")
            && utilisateur.getDerniereListePartiellePagination().get(i).isPresentField("CLCLI")
            && utilisateur.getDerniereListePartiellePagination().get(i).isPresentField("CLLIV")) {
          retour.append("<div class='listes'>" + "<span class='CLCLI'>"
              + utilisateur.getDerniereListePartiellePagination().get(i).getField("CLCLI").toString().trim() + "/"
              + utilisateur.getDerniereListePartiellePagination().get(i).getField("CLLIV").toString().trim() + "</span>"
              + "<span class='CLNOM'>" + utilisateur.getDerniereListePartiellePagination().get(i).getField("CLNOM") + "</span>");
          if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getListeArticles() != null
              && utilisateur.getMonPanier().getListeArticles().size() > 0) {
            retour.append("<span class='choisirMonClient'>Pas d'action possible</span>");
          }
          else {
            retour.append("<a class='choisirMonClient' href='clients?client="
                + utilisateur.getDerniereListePartiellePagination().get(i).getField("CLCLI").toString().trim() + "&suffixe="
                + utilisateur.getDerniereListePartiellePagination().get(i).getField("CLLIV").toString().trim() + "'>Choisir</a>");
          }
          retour.append("</div>");
        }
      }
      
      retour.append(afficherPagination(utilisateur, request.getParameter("indiceDebut"), null));
    }
    else {
      retour.append("<div class='listesCommentaires'><span class='txtCommentaires'>Pas de clients pour vos critères de saisie ["
          + expression + "]</span></div>");
    }
    
    return retour.toString();
  }
  
  /**
   * Afficher le formulaire de recherche d'un client
   */
  private String afficherFormulaireRecherche(HttpServletRequest request) {
    if (request == null || request.getSession() == null) {
      return "";
    }
    String retour = "";
    String valeur = "";
    if (request.getParameter("champClients") != null && !request.getParameter("champClients").trim().equals("")) {
      valeur = request.getParameter("champClients").trim();
    }
    
    retour += "<form name='saisieClients' id='saisieClients' method='POST' action='clients' accept-charset=\"utf-8\">";
    retour += " <label id='lbRechercheClient'>Rechercher un client</label>";
    retour += "<div id='blocMoteurClients'>";
    retour += " <input type='search' name='champClients' id='champClients' placeholder='"
        + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("Un client") + "' value='"
        + valeur + "' required/>" + "<a id='validRechClients' href='#' onClick=\"traitementEnCoursForm(document.saisieClients);\">r</a>";
    retour += "</div>";
    retour += " <a href='clients' id='reinitMoteurClients'>Réinitialiser la liste</a>";
    retour += "</form>";
    
    return retour;
  }
}
