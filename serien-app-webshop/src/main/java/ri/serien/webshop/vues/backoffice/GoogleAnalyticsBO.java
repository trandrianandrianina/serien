/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionGoogleAnalyticsBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class GoogleAnalyticsBO
 */
public class GoogleAnalyticsBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public GoogleAnalyticsBO() {
    super(new GestionGoogleAnalyticsBO(), "googleAnalyticsBO",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "googleAnalyticsBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("codeGoogle") != null) {
        out.println(
            afficherContenu(miseAjourCodeGoogle("<h2><span id='titrePage'>Google analytics</span></h2>", request, request.getSession()),
                null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(afficherGoogle(((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")),
            "<h2><span id='titrePage'>Google analytics</span></h2>", request), null, null, null, request.getSession()));
      }
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Liste le contenu de la table Google
   * @param utilisateur
   * @param titre
   * @return
   */
  private String afficherGoogle(UtilisateurWebshop user, String titre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Google Analytics &nbsp;&nbsp;</h3>");
    
    user.recupererUnRecordTravail(((GestionGoogleAnalyticsBO) maGestion).recupereCodeSuivi(user));
    
    // String contenu="";
    boolean isEnModif = request.getParameter("Modif") != null;
    
    retour.append("<form action='GoogleAnalyticsBO' method='post'>");
    
    // je suis en modif ou consult
    if (user.getRecordTravail() != null) {
      retour.append("<a class='modif' href='GoogleAnalyticsBO?Modif=1' name='modification'>Modifier le code </a>");
      // je suis en consult
      if (!isEnModif) {
        // contenu=(user).getRecordTravail().getField("GO_ANALY").toString().trim();
        retour.append("<textarea name='code_google' class='zone' readonly>"
            + (user).getRecordTravail().getField("GO_ANALY").toString().trim() + "</textarea>");
      }
      // je suis en modif
      else {
        // contenu=(user).getRecordTravail().getField("GO_ANALY").toString().trim();
        retour.append("<textarea name='code_google' class='zone'>" + (user).getRecordTravail().getField("GO_ANALY").toString().trim()
            + "</textarea>");
        retour.append("<input type='hidden'  id='codeGoogle' name='codeGoogle' value='1'>");
        retour.append("<div><input class='btnMAJ' type='submit' value='Modifier' /></div>");
      }
    }
    // je suis en creat
    else {
      retour.append("<label class='textLabel'>Code google à renseigner :  </label>");
      // contenu="";
      retour.append("<textarea name='code_google' class='zone'></textarea>");
      retour.append("<input type='hidden'  id='codeGoogle' name='codeGoogle' value='1'>");
      retour.append("<div><input class='btnMAJ' type='submit' value='Création' id='codeGoogle' /></div>");
    }
    
    retour.append("</form>");
    
    retour.append("</div>");
    return retour.toString();
  }
  
  /*  private String retournerTexteModifOuCrea(boolean isModif, GenericRecord record, String zone)
  {
    if(isModif && record.isPresentField(zone))
      return record.getField(zone).toString().trim();
    else return "";
  }*/
  
  private String miseAjourCodeGoogle(String titre, HttpServletRequest request, HttpSession session) {
    String chaine = "";
    if (titre != null) {
      chaine += titre;
    }
    
    // on contrôle si le formulaire est mis à jour ou pas
    chaine += "<div class='blocContenu'>";
    if (((GestionGoogleAnalyticsBO) maGestion).miseaJourGoogle((UtilisateurWebshop) session.getAttribute("utilisateur"), request) > 0) {
      
      chaine += ("<div id='messagePrincipal'>Le code de suivi Google a bien été mis à jour.</div>");
    }
    else {
      chaine += ("<div id='messagePrincipal'>Erreur lors de la mis à jour du code de suivi Google.</div>");
    }
    chaine += afficherGoogle((UtilisateurWebshop) request.getSession(true).getAttribute("utilisateur"), titre, request);
    chaine += "</div>";
    return chaine;
  }
  
}
