/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionMagasinsBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class MagasinsBO
 */
public class MagasinsBO extends MouleVues {
  
  public MagasinsBO() {
    super(new GestionMagasinsBO(), "magasinsBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "magasinsBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    
    listeFilRouge.add(new FilRouge("MagasinsBO", "MagasinsBO", "Les magasins", "All stores"));
    
    listeFilRouge.add(new FilRouge("unMag", "MagasinsBO?creaMag=1", "créer ", "A store"));
    listeFilRouge.add(new FilRouge("modifierMag", "MagasinsBO?modifMag=modif", "modifier", "A store"));
    
    listeFilRouge.add(new FilRouge("magId", "MagasinsBO?magId=" + MarbreEnvironnement.PARAM_FILROUGE, "un magasin", "A store"));
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      /******* affichage ****/
      
      // mode creation
      if (request.getParameter("creaMag") != null) {
        out.println(afficherContenu(afficherMagParId(request, "<h2><span id='titrePage'>Insérer un magasin</span></h2>", null), "unMag",
            null, null, request.getSession()));
      }
      else if (request.getParameter("modifMag") != null) {
        out.println(afficherContenu(
            afficherMags(request, request.getSession(), "<h2><span id='titrePage'>Modification d'un magasin</span></h2>", null), "unMag",
            request.getParameter("modifMag"), null, request.getSession()));
      }
      else if (request.getParameter("magId") != null) {
        out.println(afficherContenu(
            afficherMagParId(request, "<h2><span id='titrePage'>Modifier un magasin</span></h2>", request.getParameter("magId")), "unMag",
            request.getParameter("magId"), null, request.getSession()));
      }
      else if (request.getParameter("maj") != null) {
        out.println(afficherContenu(traiterMiseajour(request.getSession(), request), null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(affichageAccueilMags(request.getSession(), "<h2><span id='titrePage'>Gestion Magasins</span></h2>"),
            "MagasinsBO", null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher l'accueil des magasins
   */
  private String afficherBouton() {
    
    String chaine = "";
    chaine += "<div class='blocContenu'>";
    
    chaine += "<div id='centrageBoutons'>";
    // Affichage des bontons
    chaine += "<a class='magasinsBO' href='MagasinsBO?modifMag=modif'>Modifier un magasin</a>";
    chaine += "</div>";
    
    chaine += "</div>";
    
    return chaine;
    
  }
  
  /**
   * Affiche les boutons
   */
  private String affichageAccueilMags(HttpSession session, String titre) {
    String chaine = "";
    chaine += "<div class='blocContenu'>";
    chaine += ("<h2><span id='titrePage'>Gestion des magasins</span></h2>");
    chaine += afficherBouton();
    chaine += "</div>";
    return chaine;
  }
  
  /**
   * Afficher tous les magasins
   */
  private String afficherMags(HttpServletRequest request, HttpSession session, String titre, String page) {
    page = "modif";
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    String selectedOpt = "selected=\"selected\"";
    
    retour.append("<div class='blocContenu'>");
    
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ArrayList<GenericRecord> liste =
          ((GestionMagasinsBO) maGestion).recupererAllMag((UtilisateurWebshop) session.getAttribute("utilisateur"));
      
      retour.append("<h3><span class='puceH3'>&nbsp;</span>liste des magasins </h3>");
      retour.append("<div id='centrageChoixLangues'>");
      retour.append("<select name='mag' size='1' class='nomMag' onChange='location = this.options[this.selectedIndex].value;'>");
      
      if (liste != null && liste.size() > 0) {
        retour.append("<option value=''>Tous les magasins</option>");
        for (int i = 0; i < liste.size(); i++) {
          if (liste.get(i).isPresentField("MG_ID") && request.getParameter("magId") != null
              && liste.get(i).getField("MG_ID").toString().trim().equals(request.getParameter("magId").toString().trim())) {
            selectedOpt = "selected=\"selected\"";
          }
          else {
            selectedOpt = "";
          }
          
          retour.append("<option value='MagasinsBO?magId=" + liste.get(i).getField("MG_ID") + "' " + selectedOpt + ">"
              + liste.get(i).getField("MG_NAME") + "</option>");
        }
      }
      else {
        retour.append("<option value=''>pas de mag</option>");
      }
      
      retour.append("</select>");
      
      retour.append("</div>");
    }
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * Afficher le formulaire pour modifier les informations magasins
   */
  private String afficherMagParId(HttpServletRequest request, String titre, String magId) {
    if (request == null) {
      return null;
    }
    boolean isEnCreation = request.getParameter("creaMag") != null;
    String mode = "Modifier";
    if (isEnCreation) {
      mode = "Insérer";
    }
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    ArrayList<GenericRecord> langues =
        maGestion.retournerLanguesWS((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
    if (langues != null && langues.size() > 0) {
      String langue = "fr";
      if (request.getParameter("codeLangue") != null) {
        langue = request.getParameter("codeLangue");
      }
      
      if (magId != null) {
        retour.append("<form action='MagasinsBO' method='post' name='nvChoixLangue' id='formChoixLangue'>");
        retour.append("<select name='codeLangue' onChange=\"document.forms['nvChoixLangue'].submit();\">");
        
        String selectedOpt = "";
        for (int i = 0; i < langues.size(); i++) {
          if (langues.get(i).getField("LA_ID").toString().equals(langue)) {
            selectedOpt = "selected='selected'";
          }
          else {
            selectedOpt = "";
          }
          retour.append("<option value='" + langues.get(i).getField("LA_ID") + "' " + selectedOpt + ">"
              + langues.get(i).getField("LA_LIB") + "</option>");
        }
        retour.append("</select>");
        retour.append("<input type='hidden' name='magId' value='" + magId + "' />");
        retour.append("</form>");
      }
      
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof UtilisateurWebshop) {
        if (magId != null) {
          ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).recupererUnRecordTravail(((GestionMagasinsBO) maGestion)
              .recupererDetailsMagasin((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), magId, langue));
        }
        else {
          GenericRecord magasin = new GenericRecord();
          magasin.setField("MG_COD", "");
          magasin.setField("MG_NAME", "");
          magasin.setField("MG_ADR", "");
          magasin.setField("MG_TEL", "");
          magasin.setField("MG_CARTE", "");
          magasin.setField("MG_MAIL_CD", "");
          magasin.setField("MG_MAIL_DV", "");
          magasin.setField("MG_MAIL_IN", "");
          
          ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).setRecordTravail(magasin);
        }
        
        // Si pas de données linguistiques (cré ou language pas encore saisi)
        if (!((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().isPresentField("LI_MESS")) {
          ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().setField("LI_MESS", "");
        }
        if (!((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().isPresentField("LI_HOR")) {
          ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().setField("LI_HOR", "");
        }
        if (!((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().isPresentField("LI_EQUI")) {
          ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().setField("LI_EQUI", "");
        }
        
        retour.append("<form action='MagasinsBO' method='post' name='formMagasin' id='formMagasin'>");
        retour.append("<h3><span class='puceH3'>&nbsp;</span> " + mode + " les constantes du magasin</h3>");
        String actif = "";
        String inactif = "";
        String isEditable = "readonly";
        if (isEnCreation) {
          isEditable = "";
        }
        retour.append("<label class='textLabel'>" + mode + " le code Série M du magasin:  </label>");
        retour.append("<input name='code' id='MG_CODE' value='" + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
            .getRecordTravail().getField("MG_COD").toString().trim() + "' " + isEditable + " maxlength='2'/><br/>");
        
        retour.append("<label class='textLabel'>" + mode + " le nom du magasin:  </label>");
        retour
            .append("<input name='nameBO' id='MG_NAME' value='" + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
                .getRecordTravail().getField("MG_NAME").toString().trim() + "' maxlength='50'/><br/>");
        
        retour.append("<label class='textLabel'>" + mode + " le téléphone du magasin:  </label>");
        retour.append("<input name='tel' id='MG_TEL' value='" + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
            .getRecordTravail().getField("MG_TEL").toString().trim() + "' maxlength='20'/><br/>");
        
        retour.append("<label class='textLabel'>" + mode + " l'adresse du magasin:  </label>");
        retour.append("<textarea name='adr' class='zone'>" + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
            .getRecordTravail().getField("MG_ADR").toString().trim() + "</textarea>");
        
        retour.append("<label class='textLabel'>" + mode + " la localisation:  </label>");
        retour.append("<textarea name='carte' class='zone' id='geolocalisation'> "
            + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_CARTE").toString()
                .trim()
            + "</textarea>");
        
        // Les mails attribués aux gestionnaires du négociant du magasin: Ceux-ci servent à alerter le bon magasin de la commande ou du
        // devis passé par le client
        String valeur = "";
        if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_MAIL_CD") != null) {
          valeur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_MAIL_CD")
              .toString().trim();
        }
        else {
          valeur = "";
        }
        retour.append("<label class='textLabel'>" + mode + " le mail \"commandes client\":  </label>");
        retour.append("<input name='mailCDE' id='MG_MAIL_CD' value='" + valeur + "' maxlength='250'/><br/>");
        valeur = "";
        if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_MAIL_DV") != null) {
          valeur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_MAIL_DV")
              .toString().trim();
        }
        else {
          valeur = "";
        }
        retour.append("<label class='textLabel'>" + mode + " le mail \"devis client\":  </label>");
        retour.append("<input name='mailDEV' id='MG_MAIL_DV' value='" + valeur + "' maxlength='250'/><br/>");
        
        valeur = "";
        if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_MAIL_IN") != null) {
          valeur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_MAIL_IN")
              .toString().trim();
        }
        else {
          valeur = "";
        }
        retour.append("<label class='textLabel'>" + mode + " le mail \"inscription clients\":  </label>");
        retour.append("<input name='mailINS' id='MG_MAIL_IN' value='" + valeur + "' maxlength='250'/><br/>");
        
        retour.append("<h3><span class='puceH3'>&nbsp;</span> " + mode + " les textes linguistiques du magasin (" + langue + ")</h3>");
        
        retour.append("<label class='textLabel'>" + mode + " le message d'accueil:  </label>");
        retour.append("<textarea name='mess' class='zone'>"
            + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("LI_MESS")
            + "</textarea>");
        
        retour.append("<label class='textLabel'>" + mode + " vos horaires:  </label>");
        retour.append("<textarea name='hor' class='zone'>" + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
            .getRecordTravail().getField("LI_HOR").toString().trim() + "</textarea>");
        
        retour.append("<label class='textLabel'>" + mode + " les informations :  </label>");
        retour.append("<textarea name='equi' class='zone'>" + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"))
            .getRecordTravail().getField("LI_EQUI").toString().trim() + "</textarea>");
        
        retour.append("<label class='textLabel'>Visibilité du magasin</label>");
        retour.append("<select id='fc_etatETB' name='etatMG'>");
        if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getRecordTravail().getField("MG_ACTIF").toString()
            .trim().equals("0")) {
          inactif = "selected";
        }
        else {
          actif = "selected";
        }
        retour.append("<option value='0' " + inactif + ">Inactif</option>");
        retour.append("<option value='1' " + actif + ">Actif</option>");
        retour.append("</select><br/>");
        
        retour.append("<input type='hidden'  id='langueInfo' name='langueInfo' value='" + langue + "'>");
        
        if (magId != null) {
          retour.append("<input type='hidden'  id='magId' name='idMagasin' value='" + magId + "'>");
        }
        
        retour.append("<input type='hidden'  id='maj' name='maj' value='1'>");
        retour.append("<input class='btnMAJ' type='submit' value='" + mode
            + "' id='majrr' href='javascript:void(0)' onClick=\"traitementEnCours('MagasinsBO');\"/>");
        
        retour.append("</form>");
      }
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Mise à jour dans la table magasin + liensMag
   */
  private String traiterMiseajour(HttpSession session, HttpServletRequest request) {
    String chaine = "";
    
    chaine += "<div class='blocContenu'>";
    chaine += "<h2><span id='titrePage'>Validation de magasin</span></h2>";
    
    if (((GestionMagasinsBO) maGestion).miseajourInformationMagasin((UtilisateurWebshop) session.getAttribute("utilisateur"),
        request) != null) {
      chaine += "<div id='messagePrincipal'>Magasin mis à jour avec succès.</div>";
    }
    else {
      chaine += "<div id='messagePrincipal'>Il y a eu une erreur pendant la mise à jour de vos données.</div>";
    }
    
    chaine += afficherBouton();
    
    chaine += "<div/>";
    
    return chaine;
  }
  
}
