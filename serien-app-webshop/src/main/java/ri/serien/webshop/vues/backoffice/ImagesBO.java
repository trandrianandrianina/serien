/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionImagesBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class ImagesBO
 */
public class ImagesBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public ImagesBO() {
    super(new GestionImagesBO(), "imagesBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "imagesBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (!ServletFileUpload.isMultipartContent(request)) {
        if (request.getParameter("maj") != null) {
          out.println(afficherContenu(traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Accueil </span></h2>"),
              null, null, null, request.getSession()));
        }
        else {
          out.println(afficherContenu(
              afficherInfoAccueil(request.getSession(), "<h2><span id='titrePage'>Modifier l'accueil</span></h2>", "accBO", request),
              null, request.getParameter("accBO"), null, request.getSession()));
        }
        
      }
      else {
        out.println(afficherContenu(traiterUpload(request.getSession(), request, "<h2><span id='titrePage'>Accueil BO</span></h2>",
            request.getParameter("fileUpload")), null, null, null, request.getSession()));
      }
      
    }
    
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * afficher le formulaire du texte de l'accueil
   */
  private String afficherInfoAccueil(HttpSession session, String titre, String langue, HttpServletRequest request) {
    // String retour = titre;
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      // Codes langues du WS sur ce serveur
      ((UtilisateurWebshop) session.getAttribute("utilisateur"))
          .setListeDeTravail(maGestion.retournerLanguesWS((UtilisateurWebshop) session.getAttribute("utilisateur")));
      
      /*affiche le titre de la page*/
      retour.append("<h3><span class='puceH3'>&nbsp;</span> Accueil </h3>");
      
      if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail() != null
          && ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().size() > 0) {
        retour.append("<form action='ImagesBO' method='post' name ='formTextesAccueil'>");
        String texteAmodifier = "";
        // GenericRecord texteLangue = null;
        // On scanne chaque langue du serveur
        for (int i = 0; i < ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().size(); i++) {
          // On récupère le texte existant en BDD si c'est la cas et on remplit la textarea avec son contenu
          ((UtilisateurWebshop) session.getAttribute("utilisateur")).recupererUnRecordTravail(
              ((GestionImagesBO) maGestion).recupererTexteAccueil((UtilisateurWebshop) session.getAttribute("utilisateur"),
                  ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()));
          if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail() != null
              && ((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail().isPresentField("ACC_NAME")) {
            texteAmodifier =
                ((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail().getField("ACC_NAME").toString().trim();
          }
          else {
            texteAmodifier = "";
          }
          retour.append("<label for = 't1' class='textLabel'>Modifier texte D'accueil :  "
              + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB").toString().trim()
              + "</label>");
          retour.append("<textarea name='infoAccueil_"
              + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
              + "' class='zone' rows='10' >" + texteAmodifier + "</textarea>");
        }
        retour.append("<input type='hidden'  id='maj' name='maj' value='1'>");
        retour.append(
            "<input class='btnMAJ' type='submit' value='Mettre à jour' id='majp' href='javascript:void(0)' onClick=\"traitementEnCours('ImagesBO');\"/>");
        retour.append("</form>");
        retour.append(formulaireUpload("<h2><span id='titrePage'>Modification de l'accueil</span></h2>", request));
      }
      else {
        retour.append("<div>Codes langues manquants (table LANGUES)</div>");
      }
      
      retour.append("</div>");
    }
    return retour.toString();
  }
  
  /** traitement de le mise à jour du formulaire accueilBO **/
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    String chaine = "";
    if (titre != null) {
      chaine += titre;
    }
    
    chaine += "<div class='blocContenu'>";
    if (((GestionImagesBO) maGestion).miseAjourAccueil((UtilisateurWebshop) session.getAttribute("utilisateur"), request) > 0) {
      chaine += ("<div id='messagePrincipal'>Le texte de l'accueil mis à jour correctement. </div>");
    }
    else {
      chaine += ("<div id='messagePrincipal'>Erreur lors de la mise à jour de vos données.</div>");
    }
    
    chaine += afficherInfoAccueil(request.getSession(), "", "accBO", request);
    chaine += "</div>";
    
    return chaine;
  }
  
  /** Afficher le formulaire d'upload */
  private String formulaireUpload(String titre, HttpServletRequest request) {
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    String filename = "";
    if (request.getParameter("fileUpload") != null) {
      filename = request.getParameter("fileUpload");
    }
    
    /*formulaire d'upload*/
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Images à Uploader</h3>");
    // envoie des données binaires
    retour.append("<form action='ImagesBO' method='post' enctype='multipart/form-data'>"); // enctype='multipart/form-data'>");
    retour.append("<label for = 't1' class='textLabelImagel'> Modifier une image</label>");
    retour.append("<label for = 't1' class='textLabel'>Image de l'accueil à uploader :  </label>");
    retour.append("<input type='file' name='fileUpload' class='fileUpload' value='" + filename + "'/>");
    retour.append("<input type='submit' value='Uploader' class='btnMAJ'/>");
    
    retour.append("</form>");
    retour.append("</div>");
    
    return retour.toString();
    
  }
  
  /** traiter l'upload des images de l'accueil */
  private String traiterUpload(HttpSession session, HttpServletRequest request, String titre, String name) {
    String chaine = "";
    
    chaine += "<div class='blocContenu'>";
    
    if (((GestionImagesBO) maGestion).uploadI((UtilisateurWebshop) session.getAttribute("utilisateur"), request, name) == -1) {
      chaine += ("<div id='messagePrincipal'>Upload du fichier en erreur</div>");
    }
    else {
      chaine += ("<div id='messagePrincipal'>Upload du fichier effectué avec succès</div>");
    }
    
    chaine += "</div>";
    
    return chaine;
    
  }
}
