/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.encodage.Base64Coder;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionClientBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class ClientBO
 */
public class ClientBO extends MouleVues {
  
  public ClientBO() {
    super(new GestionClientBO(), "clientBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "clientBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("Maj") != null) {
        out.println(
            afficherContenu(traiterMiseajour(request.getSession(), request, "<h2><span id='titrePage'>Paramètres du serveur</span></h2>"),
                null, null, null, request.getSession()));
      }
      else {
        out.println(
            afficherContenu(afficherAccueilClient(request.getSession(), "<h2><span id='titrePage'>Paramètres serveur</span></h2>"), null,
                null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la liste des paramètres du client (XWEBSHOP/ENVIRONM)
   */
  private String afficherAccueilClient(HttpSession session, String titre) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    // String retour = titre;
    retour.append("<div class='blocContenu'>");
    String type = " ";
    String environm = " ";
    String lettreTyp = " ";
    String bibli = " ";
    String etb = " ";
    String password = " ";
    
    if (session.getAttribute("utilisateur") != null && session.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ArrayList<GenericRecord> liste =
          ((GestionClientBO) maGestion).recupererParametreclient((UtilisateurWebshop) session.getAttribute("utilisateur"));
      if (liste != null) {
        retour.append("<form action='ClientBO' method='POST' name='formClientBO'>");
        for (int i = 0; i < liste.size(); i++) {
          // Conditionnement du champs type selon valeur de la zone EN_TYP pour conditionner le input
          if (liste.get(i).getField("EN_TYP") != null && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD")) {
            type = (liste.get(i).getField("EN_TYP").toString().trim());
          }
          else {
            type = "text";
          }
          // forcer les 4 paramètres en Majuscule (environm, lettre_env, bibli et etb
          if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("ENVIRONNEMENT")) {
            environm = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='"
                + liste.get(i).getField("EN_CLE") + "' value='" + environm + "'/></div>");
          }
          else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("LETTRE_ENV")) {
            lettreTyp = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='"
                + liste.get(i).getField("EN_CLE") + "' value='" + lettreTyp + "'/></div>");
          }
          
          else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("BIBLI_CLIENTS")) {
            bibli = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='"
                + liste.get(i).getField("EN_CLE") + "' value='" + bibli + "'/></div>");
          }
          else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_CLE").toString().trim().equals("ETB_CLIENTS")) {
            etb = liste.get(i).getField("EN_VAL").toString().trim().toUpperCase();
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='"
                + liste.get(i).getField("EN_CLE") + "' value='" + etb + "'/></div>");
          }
          // forcer l'affichage du mot de passe en crypté
          else if (liste.get(i).getField("EN_CLE") != null && liste.get(i).getField("EN_TYP").toString().trim().equals("PASSWORD")) {
            password = (liste.get(i).getField("EN_VAL").toString().trim());
            if (password.length() <= 10) {
              password = (liste.get(i).getField("EN_VAL").toString().trim());
              retour
                  .append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                      + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='"
                      + liste.get(i).getField("EN_CLE") + "' value='" + password + "'/></div>");
            }
            else {
              password = Base64Coder.decodeString(Base64Coder.decodeString(liste.get(i).getField("EN_VAL").toString().trim()));
              retour
                  .append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                      + "</label><input type='" + liste.get(i).getField("EN_TYP").toString().trim() + "' class='valparam' name='"
                      + liste.get(i).getField("EN_CLE") + "' value='" + password + "'/></div>");
            }
          }
          else {
            retour.append("<div class='lignesFormulaires'><label class='labelparam'>" + liste.get(i).getField("EN_CLE").toString().trim()
                + "</label><input type='" + type + "' class='valparam' name='" + liste.get(i).getField("EN_CLE") + "' value='"
                + liste.get(i).getField("EN_VAL").toString().trim() + "'/></div>");
          }
        }
      }
      retour.append("<input type='hidden'  id='Maj' name='Maj' value='1'>");
      retour.append(
          "<input class='bouton' type='submit' value='Mettre à jour' id='MajP' href='javascript:void(0)' onClick=\"traitementEnCours('ClientBO');\"/>");
      retour.append("</form>");
    }
    
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * Traiter la mise à jour des paramètres du client
   */
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String titre) {
    StringBuilder retour = new StringBuilder();
    
    if (titre != null) {
      retour.append(titre);
    }
    
    retour.append("<div class='blocContenu'>");
    
    if (((GestionClientBO) maGestion).miseajourParametreclient((UtilisateurWebshop) session.getAttribute("utilisateur"),
        request) == true) {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>Erreur dans la mise à jour des paramètres du client</div>");
    }
    
    retour.append(afficherAccueilClient(session, ""));
    retour.append("</div>");
    
    return retour.toString();
    
  }
}
