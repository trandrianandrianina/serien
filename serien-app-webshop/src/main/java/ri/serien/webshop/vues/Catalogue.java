/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreAffichage;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionCatalogue;
import ri.serien.webshop.controleurs.GestionMonPanier;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Magasin;
import ri.serien.webshop.outils.Outils;

/**
 * Servlet implementation class Commandes
 */
public class Catalogue extends MouleVues {
  private ArrayList<Magasin> listeMagasin = null;
  private MonPanier monPanier = null;
  private final int NB_PROMOS_MAX_NAVIGATION = 2;
  
  /**
   * Constructeur par défaut du catalogue
   */
  public Catalogue() {
    super(new GestionCatalogue(), "catalogue",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "monPanier.css?" + MarbreEnvironnement.versionInstallee
            + "' rel='stylesheet'/><link href='" + MarbreEnvironnement.DOSSIER_CSS + "catalogue.css?"
            + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/><script src='scripts/catalogue.js?"
            + MarbreEnvironnement.versionInstallee + "'></script>",
        MarbreEnvironnement.ACCES_CLIENT);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("catalogue", "catalogue", "Catalogue", "Catalog"));
    listeFilRouge.add(new FilRouge("listeArticles", "catalogue?retour=1", "Liste d'articles", "Items list"));
    listeFilRouge.add(new FilRouge("unArticle", "catalogue?unArticle=1", "Un article", "Item"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest pRequest, HttpServletResponse pResponse) {
    try {
      super.traiterPOSTouGET(pRequest, pResponse);
      
      // Récupérer l'utilisateur
      UtilisateurWebshop utilisateur = null;
      if ((UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur") != null) {
        utilisateur = ((UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur"));
      }
      
      // Modifier le mode de récupération dans le panier de l'utilisateur
      if (pRequest.getParameter("modeRecup") != null) {
        GestionMonPanier panier = new GestionMonPanier();
        panier.majModeRecuperation(utilisateur, pRequest.getParameter("modeRecup"));
      }
      
      // Ajouter un article au panier
      // Les informations, collectées en AJAX, sont transmises dans l'URL. Exemple :
      // http://localhost:8080/SerieNWebShop/catalogue?etbArticle=%20ELE&panierArticle=CNPT2402SI&tarifArticle=7032.4&panierQuantite=1&refFournisseur=CNPT/240-2-MV&conditionnement=1&uniteVente=U
      if (pRequest.getParameter("panierArticle") != null && pRequest.getParameter("tarifArticle") != null
          && pRequest.getParameter("panierQuantite") != null) {
        if (utilisateur != null) {
          // Récupérer l'ancienne quantité s'il y en avait une
          BigDecimal ancienneQte = null;
          if (pRequest.getParameter("ancienneQte") != null) {
            ancienneQte = new BigDecimal(pRequest.getParameter("ancienneQte"));
          }
          
          // Ajouter un article au panier
          utilisateur.getMonPanier().ajouterArticle(pRequest.getParameter("etbArticle"), pRequest.getParameter("panierArticle"),
              pRequest.getParameter("refFournisseur"), new BigDecimal(pRequest.getParameter("panierQuantite")),
              new BigDecimal(pRequest.getParameter("tarifArticle")), pRequest.getParameter("conditionnement"),
              pRequest.getParameter("uniteVente"), pRequest.getParameter("isLot"), ancienneQte, true);
          
          // Afficher le résumé du panier en LIVE
          pResponse.getWriter().write(getPattern().afficherPanier(utilisateur, true));
        }
      }
      // Ajouter un article dans un panier intuitif
      else if (pRequest.getParameter("intuitif") != null && pRequest.getParameter("codeArticle") != null) {
        if (utilisateur != null && utilisateur.getPanierIntuitif() != null) {
          utilisateur.getPanierIntuitif().ajouterUnArticle(utilisateur, pRequest.getParameter("codeArticle"));
          // afficher en LIVE
          pResponse.getWriter().write(getPattern().afficherPanier(utilisateur, true));
        }
      }
      // Ajouter un article aux favoris
      else if (pRequest.getParameter("favoriETB") != null && pRequest.getParameter("favoriArticle") != null) {
        int retour = 0;
        if ((UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur") != null) {
          retour = ((GestionCatalogue) maGestion).majFavoriArticle(utilisateur, pRequest.getParameter("favoriETB"),
              pRequest.getParameter("favoriArticle"));
        }
        if (retour == 1) {
          pResponse.getWriter().write("<em id='artPasFavori'>Cet article ne fait pas partie de vos favoris</em>");
        }
        else if (retour == 2) {
          pResponse.getWriter().write("<em id='artFavori'>Cet article fait partie de vos favoris</em>");
        }
        else {
          pResponse.getWriter().write("<em id='artPasFavori'>pb</em>");
        }
      }
      else {
        ServletOutputStream out = pResponse.getOutputStream();
        
        String connexion = pattern.afficherConnexion(nomPage, pRequest, pResponse);
        redirectionSecurite(pRequest, pResponse);
        
        out.println(pattern.majDuHead(pRequest, cssSpecifique, " onLoad=\"majTailleNavig();\""));
        
        out.println(pattern.afficherPresentation(nomPage, pRequest, connexion));
        // Mes articles : favoris , consultés ou achats...etc
        if (pRequest.getParameter("favoris") != null || pRequest.getParameter("historique") != null
            || pRequest.getParameter("consulte") != null || pRequest.getParameter("unPanierIntuitif") != null) {
          out.println(afficherContenu(
              afficherUneListeArticles(pRequest, "<h2><span id='titrePage'>Mes articles</span></h2>",
                  pRequest.getParameter("indiceDebut")),
              "listeArticles", null, afficherPopUpRecup(utilisateur, false), pRequest.getSession()));
        }
        else if (utilisateur != null
            && (pRequest.getParameter("champRecherche") != null || pRequest.getParameter("groupeArticles") != null
                || pRequest.getParameter("familleArticles") != null || pRequest.getParameter("indiceDebut") != null)) {
          out.println(afficherContenu(
              afficherUneListeArticles(pRequest,
                  "<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$rechercheArticles") + "</span></h2>",
                  pRequest.getParameter("indiceDebut")),
              "listeArticles", null, afficherPopUpRecup(utilisateur, false), pRequest.getSession()));
        }
        else if ((pRequest.getParameter("retour") != null
            && ((UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur")).getDerniereListeArticle() != null)
            || (pRequest.getParameter("modeRecup") != null && pRequest.getParameter("A1ETB") == null)) {
          out.println(afficherContenu(afficherUneListeArticles(pRequest, null, null), "listeArticles", null,
              afficherPopUpRecup(utilisateur, false), pRequest.getSession()));
        }
        else if (utilisateur != null && pRequest.getParameter("A1ETB") != null && pRequest.getParameter("A1ART") != null) {
          out.println(
              afficherContenu(
                  afficherUnArticle(pRequest,
                      "<h2 id='h2Catalogue'><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$ficheArticle")
                          + "</span></h2>",
                      pRequest.getParameter("A1ETB"), pRequest.getParameter("A1ART"), "catalogue?retour=1"),
                  "unArticle", null,
                  afficherPopUpRecup(utilisateur, pRequest.getParameter("A1ETB"), pRequest.getParameter("A1ART"), false),
                  pRequest.getSession()));
        }
        // afficher le catalogue en mode conception de panier intuitif
        else if (pRequest.getParameter("intuitif") != null) {
          out.println(afficherContenu(
              afficherLeCatalogue(pRequest.getSession(), "<h2 id='h2Catalogue'><span id='titrePage'>Panier intuitif</span></h2>"), null,
              null, null, pRequest.getSession()));
        }
        // Consulter la liste des paniers intuitifs
        else if (pRequest.getParameter("panierIntuitif") != null) {
          out.println(afficherContenu(
              afficherLeCatalogueIntuitif(pRequest, "<h2 id='h2Catalogue'><span id='titrePage'>Paniers intuitifs</span></h2>"), null,
              null, null, pRequest.getSession()));
        }
        // Sinon l'accueil du catalogue
        else if (utilisateur != null) {
          out.println(afficherContenu(
              afficherLeCatalogue(pRequest.getSession(),
                  "<h2 id='h2Catalogue'><span id='titrePage'>" + utilisateur.getTraduction().traduire("Catalogue") + "</span></h2>"),
              null, null, null, pRequest.getSession()));
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du traitement de la page catalogue");
    }
  }
  
  /**
   * Afficher le détail d'un article
   */
  private String afficherUnArticle(HttpServletRequest request, String titre, String etb, String article, String lienRetour) {
    if (request == null || article == null || etb == null) {
      return "";
    }
    UtilisateurWebshop utilisateur = null;
    StringBuilder retour = new StringBuilder();
    
    retour.append(titre);
    
    if (request.getSession().getAttribute("utilisateur") != null
        && request.getSession().getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
      // Pop up verte de succès d'ajout au panier
      retour.append(afficherPopUpAjout());
      
      retour.append("<div class='blocContenu' id='detailArticle' >");
      
      GenericRecord rcdArt = null;
      rcdArt = ((GestionCatalogue) getMaGestion()).retourneDetailArticle(utilisateur, etb, article);
      GenericRecord tarifArt = null;
      GenericRecord substitution = null;
      ArrayList<GenericRecord> listeVariantes = null;
      
      String CND = "1";
      String CAREF = "";
      String UNITE = "";
      String REF_AFFICHEE = "";
      String DECIMQ = "0";
      int nbDecimalesQ = 0;
      
      if (rcdArt != null) {
        ((GestionCatalogue) getMaGestion()).ajouterAuxConsultes(utilisateur, etb, article);
        if (utilisateur.getEtablissementEnCours().voirSubstitutions()) {
          substitution = ((GestionCatalogue) getMaGestion()).retourneArticleSubstitution(utilisateur, etb, article);
          if (utilisateur.getEtablissementEnCours().voir_Subst_Variante()) {
            listeVariantes = ((GestionCatalogue) getMaGestion()).retournerVarianteDunArticle(utilisateur, etb, article);
          }
        }
        // attribution de variables
        if (rcdArt.isPresentField("CAREF") && !rcdArt.getField("CAREF").toString().trim().equals("")) {
          CAREF = rcdArt.getField("CAREF").toString().trim();
        }
        // Affichage de la référence soit article soit fournisseur
        if (utilisateur.getEtablissementEnCours().getZone_reference_article() != null) {
          if (utilisateur.getEtablissementEnCours().getZone_reference_article().equals("CAREF") && !CAREF.equals("")) {
            REF_AFFICHEE = CAREF;
          }
          else {
            REF_AFFICHEE = article;
          }
        }
        
        if (rcdArt.isPresentField("A1UNV")) {
          UNITE = rcdArt.getField("A1UNV").toString().trim();
        }
        
        if (rcdArt.isPresentField("DECIMQ") && !rcdArt.getField("DECIMQ").toString().trim().equals("")) {
          DECIMQ = rcdArt.getField("DECIMQ").toString().trim();
          nbDecimalesQ = Integer.parseInt(DECIMQ);
        }
        
        // N'afficher le tarif public que si il est adapté (profils) et paramétré
        if (utilisateur.getEtablissementEnCours().isVoir_prix_public()
            || utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_REPRES_WS) {
          tarifArt = ((GestionCatalogue) getMaGestion()).retourneTarifGeneralArticle(utilisateur, etb, article, UNITE);
          if (tarifArt != null && tarifArt.isPresentField("ATP01") && rcdArt.isPresentField("A1TVA")) {
            if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_BOUTIQUE && utilisateur.isPrixVisible()) {
              BigDecimal montantTTC =
                  ((GestionCatalogue) getMaGestion()).retournerTTC(utilisateur, tarifArt.getField("ATP01"), rcdArt.getField("A1TVA"));
              tarifArt.setField("ATP01", montantTTC);
            }
          }
        }
        
        boolean isVendable = false;
        
        if (rcdArt.isPresentField("PROMO")) {
          if (rcdArt.isPresentField("A1LIB")) {
            retour.append("<div id='estEnPromo'><span id='libPromo1'>Article en promotion</span><span id='libPromo2'>"
                + rcdArt.getField("A1LIB").toString().trim() + "</span></div>");
          }
        }
        else {
          retour.append("<h3><span class='puceH3'>&nbsp;</span>");
          if (rcdArt.isPresentField("A1LIB")) {
            retour.append(rcdArt.getField("A1LIB").toString().trim());
          }
          retour.append("</h3>");
          if (rcdArt.isPresentField("MEMO")) {
            retour.append("<div id='memoFicheArticle'><span>" + rcdArt.getField("MEMO").toString().trim() + "</span></div>");
          }
        }
        // Article de substitution
        if (substitution != null) {
          if (substitution.isPresentField("LIBSUB") && substitution.getField("LIBSUB") != null) {
            retour.append("<p id='messagePrincipal'>" + substitution.getField("LIBSUB").toString().trim()
                + "<br/><a class='lienSub' href='catalogue?A1ETB=" + etb + "&A1ART=" + substitution.getField("A1ART").toString().trim()
                + "'>" + substitution.getField("A1ART").toString().trim() + " / " + substitution.getField("A1LIB").toString().trim()
                + "</a>" + "</p>");
          }
        }
        
        retour.append("<div id='blocFiche'>");
        retour.append("<div class='unBloc' id='blocGauche'>");
        
        // <<<<<<<<<< Bloc detail article >>>>>>>>>>
        retour.append("<div class='sousBlocs' id='fi_details'>");
        retour.append("<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$detailsArticle") + "</h5>");
        // code article
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$codeArticle") + "</p>");
        if (rcdArt.isPresentField("A1ART") && !rcdArt.getField("A1ART").toString().trim().equals("")) {
          retour.append("<p class='zoneDonnee' id='fi_A1ART'><span class='paddingInterne'>" + rcdArt.getField("A1ART").toString().trim()
              + "</span></p><br/>");
        }
        else {
          retour.append("<p class='placeholderSpan' id='fi_A1ART'><span class='paddingInterne'>"
              + utilisateur.getTraduction().traduire("$$reference") + "</span></p><br/>");
        }
        
        // Mots de classements
        if (rcdArt.isPresentField("A1CL1") && !rcdArt.getField("A1CL1").toString().trim().equals("")) {
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$cl1Article") + "</p>");
          retour.append("<p class='zoneDonnee' id='fi_A1CL1'><span class='paddingInterne'>" + rcdArt.getField("A1CL1").toString().trim()
              + "</span></p><br/>");
        }
        if (rcdArt.isPresentField("A1CL2") && !rcdArt.getField("A1CL2").toString().trim().equals("")) {
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$cl2Article") + "</p>");
          retour.append("<p class='zoneDonnee' id='fi_A1CL2'><span class='paddingInterne'>" + rcdArt.getField("A1CL2").toString().trim()
              + "</span></p><br/>");
        }
        
        // libelle article
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$libelle") + "</p>");
        if (rcdArt.isPresentField("A1LIB") && !rcdArt.getField("A1LIB").toString().trim().equals("")) {
          retour.append("<p class='zoneDonnee' id='fi_A1LIB'><span class='paddingInterne'>" + rcdArt.getField("A1LIB").toString().trim()
              + "</span></p><br/>");
        }
        else {
          retour.append("<p class='placeholderSpan' id='fi_A1LIB'><span class='paddingInterne'>"
              + utilisateur.getTraduction().traduire("$$libelle") + "</span></p><br/>");
        }
        
        // Gestion des familles et sous familles
        if (utilisateur.getEtablissementEnCours().isVoir_familles_fiche()) {
          // famille article
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$famille") + "</p>");
          if (rcdArt.isPresentField("A1FAM") && !rcdArt.getField("A1FAM").toString().trim().equals("")) {
            retour.append("<p class='zoneDonnee' id='fi_A1FAM'><span class='paddingInterne'>"
                + ((GestionCatalogue) getMaGestion()).retourneInfoFamille(utilisateur, rcdArt.getField("A1FAM").toString().trim())
                + "</span></p><br/>");
          }
          else {
            retour.append("<p class='placeholderSpan' id='fi_A1FAM'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$famille") + "</span></p><br/>");
          }
          
          // sous famille article
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$sousFamille") + "</p>");
          if (rcdArt.isPresentField("A1SFA") && !rcdArt.getField("A1SFA").toString().trim().equals("")) {
            retour.append("<p class='zoneDonnee' id='fi_A1SFA'><span class='paddingInterne'>"
                + ((GestionCatalogue) getMaGestion()).retourneInfoSousFamille(utilisateur, rcdArt.getField("A1SFA").toString().trim())
                + "</span></p><br/>");
          }
          else {
            retour.append("<p class='placeholderSpan' id='fi_A1SFA'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$sousFamille") + "</span></p><br/>");
          }
        }
        
        String taxes = "HT";
        if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_BOUTIQUE) {
          taxes = "TTC";
        }
        // tarif de base
        if (tarifArt != null && tarifArt.isPresentField("ATP01") && !tarifArt.getField("ATP01").toString().trim().equals("")) {
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$tarifPublic") + " " + taxes + "</p>");
          retour.append("<p class='zoneDonnee'  id='fi_ATP01'><span class='paddingInterne'>"
              + Outils.formaterUnTarifArrondi(utilisateur, tarifArt.getField("ATP01")) + "</span></p><br/>");
        }
        
        // CONDITIONNER LE TARIF CLIENT SUR L'ACCES UNIQUEMENT CLIENT
        if (utilisateur.getClient() != null && utilisateur.isPrixVisible()) {
          // tarif du client
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$votreTarif") + " " + taxes + "</p>");
          if (rcdArt.isPresentField("TARIF") && !rcdArt.getField("TARIF").toString().trim().equals("")) {
            isVendable = ((GestionCatalogue) getMaGestion()).verifierSiVendable((BigDecimal) rcdArt.getField("TARIF"),
                utilisateur.getEtablissementEnCours().isVend_articles_a_zero());
            retour.append("<p class='zoneDonnee'  id='fi_TARIF'><span class='paddingInterne'>"
                + Outils.formaterUnTarifArrondi(utilisateur, rcdArt.getField("TARIF")) + "</span></p><br/>");
          }
          else {
            retour.append("<p class='placeholderSpan'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$votreTarif") + "</span></p><br/>");
          }
        }
        retour.append(afficherLienFicheFournisseur(utilisateur, etb, article));
        
        retour.append("</div>");
        
        // <<<<<<<<<< Bloc detail fournisseur >>>>>>>>>>
        if (utilisateur.getEtablissementEnCours().isVoir_bloc_fournis_fiche() && rcdArt.isPresentField("FRNOM")
            && !rcdArt.getField("FRNOM").toString().trim().equals("")) {
          retour.append("<div class='sousBlocs'  id='fi_fourniss'>");
          retour.append("<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$fournisseur") + "</h5>");
          // nom du fournisseur
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$fournisseur") + "</p>");
          if (rcdArt.isPresentField("FRNOM") && !rcdArt.getField("FRNOM").toString().trim().equals("")) {
            retour.append("<p class='zoneDonnee' id='fi_FRNOM'><span class='paddingInterne'>" + rcdArt.getField("FRNOM").toString().trim()
                + "</span></p><br/>");
          }
          else {
            retour.append("<p class='placeholderSpan'  id='fi_FRNOM'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$fournisseur") + "</span></p>");
          }
          
          // reference fournisseur
          retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$reference") + "</p>");
          if (rcdArt.isPresentField("CAREF") && !rcdArt.getField("CAREF").toString().trim().equals("")) {
            retour.append("<p class='zoneDonnee' id='fi_CAREF'><span class='paddingInterne'>" + rcdArt.getField("CAREF").toString().trim()
                + "</span></p><br/>");
          }
          else {
            retour.append("<p class='placeholderSpan' id='fi_CAREF'><span class='paddingInterne'>"
                + utilisateur.getTraduction().traduire("$$reference") + "</span></p>");
          }
          
          retour.append("</div>");
        }
        
        // <<<<<<<<<< Bloc stock et unité >>>>>>>>>>
        boolean gestionLots = false;
        String lesLots = "";
        BigDecimal lotMax = BigDecimal.ZERO;
        retour.append("<div class='sousBlocs' id= 'fi_stock'>");
        retour.append("<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$disponibilite") + "</h5>");
        if (rcdArt.getField("A1IN2") != null && rcdArt.getField("A1IN2").toString().trim().equals("1")
            && MarbreEnvironnement.IS_GESTION_LOTS) {
          retour.append("<p class='labelDonnee'>Article géré par lot</p><br/>");
          lotMax = ((GestionCatalogue) maGestion).retournerLotMaxArticle(utilisateur, etb, article);
          if (lotMax.compareTo(BigDecimal.ZERO) > 0 && utilisateur.getMonPanier() != null) {
            gestionLots = true;
            lesLots = ((GestionCatalogue) maGestion).retournerLotsArticle(utilisateur, etb, article, nbDecimalesQ);
          }
        }
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$unite") + "</p>");
        // unité de vente
        if (rcdArt.isPresentField("LIBUNV") && !rcdArt.getField("LIBUNV").toString().trim().equals("")) {
          retour.append("<p class='zoneDonnee' id='fi_LIBUNV'><span class='paddingInterne'>" + rcdArt.getField("LIBUNV").toString().trim()
              + "</span></p><br/>");
        }
        else {
          retour.append("<p class='placeholderSpan' id='fi_LIBUNV'><span class='paddingInterne'>"
              + utilisateur.getTraduction().traduire("$$unite") + "</span></p>");
        }
        // conditionnement
        retour.append("<p class='labelDonnee'>" + utilisateur.getTraduction().traduire("$$conditionnement") + "</p>");
        if (rcdArt.isPresentField(MarbreEnvironnement.ZONE_CONDITIONNEMENT)
            && !rcdArt.getField(MarbreEnvironnement.ZONE_CONDITIONNEMENT).toString().trim().equals("")) {
          CND =
              Outils.afficherValeurCorrectement(utilisateur, rcdArt.getField(MarbreEnvironnement.ZONE_CONDITIONNEMENT).toString().trim());
          // Si le conditionnement est à 0 afficher 1
          if (CND != null && CND.equals("0")) {
            CND = "1";
          }
          retour.append("<p class='zoneDonnee' id='fi_A1CND'><span class='paddingInterne'>" + CND + "</span></p><br/>");
        }
        else {
          retour.append("<p class='placeholderSpan' id='fi_A1CND'><span class='paddingInterne'>Condit.</span></p>");
        }
        
        // ++++ On a un mode de récupération dans le magasin
        if (utilisateur.isStockDisponibleVisible()) {
          if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() > MarbreEnvironnement.MODE_NON_CHOISI
              || utilisateur.getEtablissementEnCours().is_filtre_stock()) {
            // +++ On veut voir le stock de tous les magasins
            if (utilisateur.getEtablissementEnCours().isVoir_stocks_ts_mags()) {
              if (listeMagasin == null) {
                listeMagasin = ((GestionCatalogue) maGestion).retournerTousLesMagasins(utilisateur);
              }
              
              String stock = null;
              String livraison = null;
              String dateReassort = null;
              String dateLivraison = null;
              retour.append("<div id='titreMultiStocks'>Stocks disponibles</div>");
              if (listeMagasin != null && listeMagasin.size() > 0) {
                for (int i = 0; i < listeMagasin.size(); i++) {
                  dateReassort = null;
                  stock = ((GestionCatalogue) maGestion)
                      .retourneDispoArticleSQLunMagasin(utilisateur, etb, article, listeMagasin.get(i).getCodeMagasin()).trim();
                  retour.append("<p class='labelDonnee'>- " + listeMagasin.get(i).getLibelleMagasin() + "</p>");
                  // on affiche la valeur du stock ou une icone dispo/pas dispo
                  if (utilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_AV_VALEUR) {
                    String attendu = null;
                    if (utilisateur.getEtablissementEnCours().isVoir_stock_attendu()) {
                      attendu = ((GestionCatalogue) maGestion).retournerStockAttendu(utilisateur, etb, article,
                          listeMagasin.get(i).getCodeMagasin());
                    }
                    
                    if (attendu == null || attendu.trim().equals("")) {
                      attendu = "";
                    }
                    else {
                      attendu = "/" + afficherStockCorrectement(utilisateur, attendu, nbDecimalesQ);
                    }
                    
                    if (listeMagasin.get(i).getCodeMagasin().trim().equals(utilisateur.getMagasinSiege().getCodeMagasin())) {
                      livraison = "<p class='zoneStock'><span class='paddingInterne'>"
                          + afficherStockCorrectement(utilisateur, stock, nbDecimalesQ) + attendu + "</span></p>";
                    }
                    
                    retour.append("<p class='zoneStock'><span class='paddingInterne'>"
                        + afficherStockCorrectement(utilisateur, stock, nbDecimalesQ) + attendu + "</span></p>");
                  }
                  // Si on affiche l'image et non le stock
                  else {
                    // Affichage mono magasin ou magasin du panier uniquement
                    if (utilisateur.getEtablissementEnCours().is_mono_magasin()
                        || !utilisateur.getEtablissementEnCours().isVoir_stocks_ts_mags()) {
                      retour.append("<p id='lienStock'>" + afficherStockFiche(utilisateur, stock, "") + "</p>");
                    }
                    else {
                      if (listeMagasin.get(i).getCodeMagasin().trim().equals(utilisateur.getMagasinSiege().getCodeMagasin())) {
                        livraison = "<p class='liensMultiStocks'>" + afficherStockFiche(utilisateur, stock, "mini") + "</p>";
                      }
                      retour.append("<p class='liensMultiStocks'>" + afficherStockFiche(utilisateur, stock, "mini") + "</p>");
                    }
                  }
                  
                  // On récupère la date de réassort pour les ruptures
                  if (stock != null && Float.parseFloat(stock) <= 0 && utilisateur.getEtablissementEnCours().isVoir_date_reassort()) {
                    retour.append("<p class='reassort' onMouseOver=\"afficherInfoBulle('infoB" + i
                        + "');\" onMouseOut=\"fermerInfoBulle('infoB" + i + "');\">");
                    dateReassort = ((GestionCatalogue) maGestion).retournerDateAttenduMagasin(utilisateur, etb, article,
                        listeMagasin.get(i).getCodeMagasin());
                    // Stocker la donnée pour la livraison à partir du magasin siège
                    if (listeMagasin.get(i).getCodeMagasin().trim().equals(utilisateur.getMagasinSiege().getCodeMagasin())) {
                      livraison +=
                          "<p class='reassort' onMouseOver=\"afficherInfoBulle('infoX');\" onMouseOut=\"fermerInfoBulle('infoX');\">"
                              + dateReassort + "</p>";
                      dateLivraison = dateReassort;
                    }
                    retour.append(dateReassort);
                    
                    retour.append("</p>");
                    retour.append("<p class='lienReassort' onMouseOver=\"afficherInfoBulle('infoB" + i
                        + "');\" onMouseOut=\"fermerInfoBulle('infoB" + i + "');\">&nbsp;</p>");
                    retour.append("<div class='infoBulle' id='infoB" + i + "'>Date estimée de la disponibilité de cet article: "
                        + dateReassort + "</div>");
                  }
                  
                  retour.append("</br>");
                }
              }
              // ++ Cas de la livraison (magasin siège)
              if (livraison != null) {
                retour.append("<p class='labelDonnee'><i>* Pour livraison</i></p>");
                retour.append(livraison);
                // Si on a demandé une date de réassort on affiche ce qu'il faut
                if (dateLivraison != null) {
                  retour.append(
                      "<p class='lienReassort' onMouseOver=\"afficherInfoBulle('infoX');\" onMouseOut=\"fermerInfoBulle('infoX');\">&nbsp;</p>");
                  retour.append(
                      "<div class='infoBulle' id='infoX'>Date estimée de la disponibilité de cet article: " + dateLivraison + "</div>");
                }
              }
            }
            // +++ On ne veut voir que le stock du magasin choisi dans le panier
            else {
              // on affiche la valeur du stock
              if (utilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_AV_VALEUR) {
                retour.append("<p class='labelDonnee'>Stock disponible</p>");
                if (rcdArt.isPresentField("STOCK") && !rcdArt.getField("STOCK").toString().trim().equals("")) {
                  retour.append("<p class='zoneDonnee' id='fi_STOCK'><span class='paddingInterne'>"
                      + afficherStockCorrectement(utilisateur, rcdArt.getField("STOCK").toString().trim(), nbDecimalesQ) + "</span></p>");
                }
                else {
                  retour.append("<p class='placeholderSpan' id='fi_STOCK'><span class='paddingInterne'>Stock</span></p>");
                }
              }
              // on affiche une image
              else {
                if (rcdArt.isPresentField("STOCK") && !rcdArt.getField("STOCK").toString().trim().equals("")) {
                  retour.append(
                      "<p id='lienStock'>" + afficherStockFiche(utilisateur, rcdArt.getField("STOCK").toString().trim(), "") + "</p>");
                }
              }
              
              // On récupère la date de réassort pour les ruptures
              if (rcdArt.isPresentField("STOCK") && Float.parseFloat(rcdArt.getField("STOCK").toString().trim()) <= 0
                  && utilisateur.getEtablissementEnCours().isVoir_date_reassort()) {
                String dateReassort = "NR";
                // Si on affiche la valeur du stock
                if (utilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_SANS_VALEUR) {
                  retour.append("<p class='labelDonnee'>Date disponibilité</p>");
                }
                if (utilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_SANS_VALEUR) {
                  retour.append(
                      "<p class='zoneDonnee' id='fi_dateReassort' onMouseOver=\"afficherInfoBulle('infoB1');\" onMouseOut=\"fermerInfoBulle('infoB1');\"><span class='paddingInterne'>");
                }
                else {
                  retour.append(
                      "<p class='reassort' onMouseOver=\"afficherInfoBulle('infoB1');\" onMouseOut=\"fermerInfoBulle('infoB1');\">");
                }
                
                dateReassort = ((GestionCatalogue) maGestion).retournerDateAttenduMagasin(utilisateur, etb, article,
                    utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin());
                retour.append(dateReassort);
                
                if (utilisateur.getEtablissementEnCours().getMode_stocks() == MarbreEnvironnement.STOCK_SANS_VALEUR) {
                  retour.append("</span>");
                }
                retour.append("</p>");
                retour.append(
                    "<p class='lienReassort' onMouseOver=\"afficherInfoBulle('infoB1');\" onMouseOut=\"fermerInfoBulle('infoB1');\">&nbsp;</p>");
                retour.append(
                    "<div class='infoBulle' id='infoB1'>Date estimée de la disponibilité de cet article: " + dateReassort + "</div>");
              }
              
              retour.append("<br/>");
              
              String recup = " <b>la récupération</b> de ";
              String lieurecup = " au magasin - <b>" + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin() + "</b> -";
              
              if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
                recup = " <b>la livraison</b> de ";
                lieurecup = "";
              }
              
              retour.append(
                  "<p id='texteStock'><i id='paddTexteStock'><i id='alertTexteStock'>&nbsp;</i>Cette disponibilité n'est valable que pour "
                      + recup + " votre commande " + lieurecup + "</i></p>");
            }
          }
          // ++++ On a pas de mode de récup
          else {
            if (utilisateur.getTypeAcces() < MarbreEnvironnement.ACCES_RESPON_WS && utilisateur.getClient() != null) {
              retour.append("<a id='lienStock' href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\" ><img src='"
                  + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_recup.png'/><span>"
                  + utilisateur.getTraduction().traduire("$$stocks") + "</span></a>");
              retour.append("<p id='texteStock'><i id='paddTexteStock'><i id='alertMajStock'>&nbsp;</i>"
                  + utilisateur.getTraduction().traduire("$$choisirModeRecuperation") + "</i></p>");
            }
          }
        }
        
        retour.append("</div>");
        retour.append("</div>");
        
        retour.append("<div class='unBloc' id='blocDroit'>");
        
        // <<<<<<<<<< Bloc photo >>>>>>>>>>
        retour.append("<div class='sousBlocs' id='fi_photo'>" + "<p class='decoH5'>&nbsp;</p><h5>"
            + utilisateur.getTraduction().traduire("$$photo") + "</h5>");
        
        retour.append("</div>");
        
        // <<<<<<<<<< Ajouter au panier >>>>>>>>>>
        retour.append("<div class='sousBlocs'  id='fi_actions'>");
        retour.append("<p class='decoH5'>&nbsp;</p><h5>Actions</h5>");
        if (utilisateur.getMonPanier() != null) {
          String texteSituation = "";
          if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON) {
            texteSituation =
                "<b class='spanTextePourPanier'>Votre panier est actuellement configuré pour <i id='spanGras'>une livraison</i>. <a href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\">Changer pour un retrait en magasin</a></b>";
          }
          else if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT) {
            texteSituation = "<b class='spanTextePourPanier'>Votre panier est actuellement attribué au magasin : <i id='spanGras'>"
                + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin()
                + "</i>. <a href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\">Changer pour une livraison ou changer de magasin</a></b>";
          }
          else if (utilisateur.getMonPanier() != null
              && utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_NON_CHOISI) {
            texteSituation =
                "<b class='spanTextePourPanier'>Votre panier n'est pas encore attribué à un magasin ou une livraison. <a href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\">Attribuer</a></b>";
          }
          retour.append("<p id='textePourPanier'>" + texteSituation + "</p>");
        }
        // Mettre au panier
        if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
          retour.append("<p class='btActions' ><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/><span>" + utilisateur.getTraduction().traduire("$$panier") + "</span></p>");
        }
        else if (utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_CONSULTATION
            && utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_BOUTIQUE && rcdArt.isPresentField("A1ART")
            && !rcdArt.getField("A1ART").toString().trim().equals("") && rcdArt.isPresentField("TARIF")
            && !rcdArt.getField("TARIF").toString().trim().equals("") && isVendable) {
          
          // Formater le libellé article
          String libelleArticle = rcdArt.getField("A1LIB").toString().trim();
          libelleArticle = libelleArticle.replace("'", "\\'");
          libelleArticle = libelleArticle.replace('"', ' ');
          
          // Construire l'appel Javascript à la méthode qui ajoute un article au panier
          // Formater la référence fournisseur avec StringEscapeUtils.escapeHtml4()
          // Utiliser le caractère échapement \ n'est pas suffisant dans un context HTML, il faut utiliser &quot;
          String javascriptPanier =
              "\"ajoutPanier(0,'" + REF_AFFICHEE + "','" + etb + "','" + article + "','" + rcdArt.getField("TARIF").toString().trim()
                  + "','miniqteSaisie','" + StringEscapeUtils.escapeHtml4(CAREF) + "','" + CND + "','" + UNITE + "',true);\"";
          
          if (gestionLots && utilisateur.getMonPanier() != null) {
            javascriptPanier = "\"controlerLots('" + lesLots + "','miniqteSaisie'," + lotMax + ",'" + REF_AFFICHEE + "','" + etb + "','"
                + article + "','" + rcdArt.getField("TARIF").toString().trim() + "','miniqteSaisie','" + CAREF + "','" + CND + "','"
                + rcdArt.getField("A1LIB").toString().trim() + "','" + UNITE + "'," + nbDecimalesQ + ",'"
                + rcdArt.getField("LIBUNV").toString().trim() + "',true);\"";
          }
          
          retour.append("<a class='btActions' href=\"javascript:void(0)\"  onClick=\"ouvrirPopPanierComplet('"
              + Outils.afficherValeurCorrectement(utilisateur, CND) + "','" + REF_AFFICHEE + "','" + libelleArticle + "','"
              + Outils.formaterUnTarifArrondi(utilisateur, rcdArt.getField("TARIF")) + "','" + UNITE + "','"
              + rcdArt.getField("LIBUNV").toString().trim() + "','" + DECIMQ + "');\"><img src='"
              + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_panier.png'/><span>"
              + utilisateur.getTraduction().traduire("$$panier") + "</span></a>");
          
          retour.append("<a id='onMetAuPanierInvisible' href=\"javascript:void(0)\"  onClick=" + javascriptPanier + "><img src='"
              + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_panier.png'/><span>"
              + utilisateur.getTraduction().traduire("$$panier") + "</span></a>");
        }
        else if (utilisateur.getPanierIntuitif() != null) {
          retour.append("<a class='btActions' href=\"javascript:void(0)\" onClick=\"ajouterPanierIntuitif('"
              + rcdArt.getField("A1ART").toString().trim() + "');\" >" + "<img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/>" + "<span>Panier</span></a>");
        }
        else {
          retour.append("<p class='btActions' ><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_panier.png'/><span>" + utilisateur.getTraduction().traduire("$$panier") + "</span></p>");
        }
        // Favoris
        if (utilisateur.getEtablissementEnCours().isVoir_favoris() && utilisateur.getTypeAcces() >= MarbreEnvironnement.ACCES_CLIENT) {
          retour.append("<a class='btActions' href=\"javascript:void(0)\" onClick=\"majFavori('" + etb + "','" + article
              + "');\" ><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_favori.png'/><span>"
              + utilisateur.getTraduction().traduire("$$favori") + "</span></a>");
        }
        else {
          retour.append("<p class='btActions' id='favoriOpaque'><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
              + "/images/decoration/fiche_favori.png'/><span>" + utilisateur.getTraduction().traduire("$$favori") + "</span></p>");
        }
        
        retour.append("<a class='btActions' href=\"" + lienRetour + "\"><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/decoration/fiche_retour.png'/><span>" + utilisateur.getTraduction().traduire("$$retour") + "</span></a>");
        
        retour.append("<div id='etatFavori'>" + afficherFavoriArticle(utilisateur, etb, article) + "</div>");
        
        retour.append("</div>");
        retour.append("</div>");
        retour.append("</div>");
        if (listeVariantes != null) {
          retour.append(afficherVariantes(utilisateur, listeVariantes));
        }
        
        // Chargement de l'image après le chargement de la page on récupère le code RUBIS s'il existe
        String retourImage = "<p class='decoH5'>&nbsp;</p><h5>" + utilisateur.getTraduction().traduire("$$photo") + "</h5>";
        
        retourImage += afficherPhoto(rcdArt, etb, article, utilisateur);
        
        retour.append("<script type=\"text/javascript\">");
        retour.append("window.onload= document.getElementById(\"fi_photo\").innerHTML= \"" + retourImage + "\";");
        retour.append("</script> ");
        
      }
      else {
        retour.append("Pas de détail article");
      }
      
      retour.append("</div>");
      retour.append(afficherMajTaillesDiv());
    }
    
    return retour.toString();
  }
  
  /**
   * Afficher le bloc propre au statut Favori de l'article
   */
  private String afficherFavoriArticle(UtilisateurWebshop utilisateur, String etb, String article) {
    String retour = "<em id='artPasFavori'>Cet article ne fait pas partie de vos favoris</em>";
    
    if (utilisateur == null || etb == null || article == null) {
      return retour;
    }
    
    if (((GestionCatalogue) maGestion).isEnFavori(utilisateur, etb, article) == 2) {
      retour = "<em id='artFavori'>Cet article fait partie de vos favoris</em>";
    }
    
    return retour;
  }
  
  /**
   * Afficher la pop up spécifique à la mise au panier avec quantité et conditionnement
   */
  private String afficherPopUpPanier(UtilisateurWebshop utilisateur) {
    String retour = "</div>" + "<div id='popUpPanier'>"
        + "<div id='titreMiniPanier'><span id='presentReference'>Ajout d'un article au panier: </span><span id='miniReference'></span>"
        + "<span id='miniLibelle'></span></div>"
        + "<div id='textMiniCondition'>Cet article est conditionné par <span id='miniCondition'></span> <span id='miniUnite'></span></div>"
        + "<div id='ligneMiniPanier'>"
        + "<a id='miniMoins' href=\"javascript:void(0)\" onClick= \"majQuantiteGlobal('miniqteSaisie','-1'); majCndGlobal('miniqteSaisie');\">-</a>"
        + "<div id='divMiniSaisie'><input id='miniqteSaisie' type='text' name='fuckingQuantite' value='1 onClick='this.select();' "
        // + MarbreAffichage.SAISIE_MAX_QTE_ART
        + "' onKeyPress=\"recupererZoneInitiale(event,'miniqteSaisie');\"  onKeyup=\"controle_quantiteDecimalesGlob(event,'miniqteSaisie');\" onChange=\"majCndGlobal('miniqteSaisie');\" /></div>"
        + "<a id='miniPlus' href=\"javascript:void(0)\" onClick= \"majQuantiteGlobal('miniqteSaisie','1'); majCndGlobal('miniqteSaisie');\">+</a>"
        + "<span id='miniMultipl'>x</span>" + "<div id='miniTarif'></div>"
        + "<a id='miniBtPanier' href=\"javascript:void(0)\" onClick='mettreAuPanierFiche();' ><img id='miniImgPanier' src='"
        + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_panier.png' alt='Mise au panier'/></a>"
        + "<a id='miniBtRetour' href=\"javascript:void(0)\" onClick ='fermerPopPanierComplet();'><img id='miniImgRetour' src='"
        + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/fiche_retour.png' alt='Retour'/></a>" + "</div>";
        
    return retour;
  }
  
  /**
   * *Afficher le lien fiche article du fournisseur s'il existe
   * @param refFournisseur
   * @return
   */
  private String afficherLienFicheFournisseur(UtilisateurWebshop utilisateur, String etb, String article) {
    String lienFiche = ((GestionCatalogue) maGestion).recupererFicheTechniqueURL(utilisateur, etb, article);
    if (lienFiche != null) {
      return "<div id='divFicheFournisseur'><p id='labelFiche'>Fiche technique</p>" + "<a class='btActions' id='ficheTechnique' href='"
          + lienFiche + "' target = '_BLANK'><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
          + "/images/decoration/fiche_fourniss.png'/><span>" + utilisateur.getTraduction().traduire("$$fiche") + "</span></a></div>";
    }
    else {
      return "";
    }
  }
  
  /**
   * *Afficher la photo de l'article
   * @param refFournisseur
   * @return
   */
  private String afficherPhoto(GenericRecord record, String etb, String article, UtilisateurWebshop utilisateur) {
    String retour = "";
    String nomImage = null;
    String srcImage = null;
    if (record == null) {
      return retour;
    }
    
    if (utilisateur.getEtablissementEnCours().getZone_affichage_photos().equals("CAREF") && record.isPresentField("CARFC")
        && !record.getField("CARFC").toString().trim().equals("")) {
      nomImage = record.getField("CARFC").toString().trim();
    }
    else if (utilisateur.getEtablissementEnCours().getZone_affichage_photos().equals("CAREF") && record.isPresentField("CAREF")
        && !record.getField("CAREF").toString().trim().equals("")) {
      nomImage = record.getField("CAREF").toString().trim();
    }
    else if (utilisateur.getEtablissementEnCours().getZone_affichage_photos().equals("A1ART")) {
      nomImage = article;
    }
    else {
      return retour;
    }
    
    retour += "<img id='imageArticle' ";
    // récupérer par une méthode ou une autre la photo de l'article
    // URL
    srcImage = ((GestionCatalogue) maGestion).recupererURLphoto(nomImage, etb, article, utilisateur);
    // IFS
    if (srcImage == null) {
      srcImage = ((GestionCatalogue) maGestion).recupererIFSphoto(utilisateur, nomImage);
    }
    // Si on a pas récupéré d'URL
    if (srcImage == null) {
      srcImage = "src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/imageVide.png'";
    }
    
    retour = "<img id='imageArticle' " + srcImage + " alt='reférence " + nomImage + "' title='reférence " + nomImage + "' />";
    
    return retour;
  }
  
  /**
   * pop up générale de la liste factoriser plus tard
   */
  private String afficherPopUpRecup(UtilisateurWebshop utilisateur, boolean isRepresentant) {
    String retour = "";
    
    if (monPanier == null) {
      monPanier = new MonPanier();
    }
    
    retour += "<div>";
    retour += monPanier.afficherModeDeRecuperation(utilisateur, "Catalogue");
    retour += "</div>";
    
    retour += afficherPopUpPanier(utilisateur);
    
    return retour;
  }
  
  /**
   * pop up générale de la fiche article factoriser plus tard
   */
  private String afficherPopUpRecup(UtilisateurWebshop utilisateur, String etbArticle, String codeArticle, boolean isRepresentant) {
    String retour = "";
    
    if (monPanier == null) {
      monPanier = new MonPanier();
    }
    
    retour += "<div>";
    retour += monPanier.afficherModeDeRecuperation(utilisateur,
        "catalogue?A1ETB=" + etbArticle.toString().trim() + "&A1ART=" + codeArticle.toString().trim());
    retour += "</div>";
    
    retour += afficherPopUpPanier(utilisateur);
    
    return retour;
  }
  
  /**
   * Affiche un morceau de la liste d'articles trouvée
   */
  private String afficherUneListeArticles(HttpServletRequest request, String titre, String indiceDebut) {
    if (request == null) {
      return "";
    }
    UtilisateurWebshop utilisateur = null;
    StringBuilder retour = new StringBuilder(15000);
    float nbArt = -1;
    int idDeb = 0;
    String url = null;
    
    // Gestion de session
    HttpSession sess = request.getSession();
    if (sess == null) {
      return "";
    }
    
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      utilisateur = (UtilisateurWebshop) sess.getAttribute("utilisateur");
      // Gestion du filtre sur le stock positif
      if (request.getParameter("stockPositif") != null) {
        utilisateur.getFiltres().setStock_positif(request.getParameter("stockPositif").equals("1"));
      }
      
      // Mettre à jour le filtre sur les fournisseurs si nécessaire
      if (request.getParameter("filtreFrs") != null && !request.getParameter("filtreFrs").trim().equals("")) {
        utilisateur.setDernierFiltreFournisseur(request.getParameter("filtreFrs"));
      }
      
      // passer le paramètre de visualisation des stocks si nécessaire
      if (!utilisateur.getEtablissementEnCours().isVisu_auto_des_stocks()) {
        utilisateur.setVisuStock(request.getParameter("visuStock") != null);
      }
      
      // Si on a passé un changement de magasin de retrait
      if (request.getParameter("chgMagasin") != null && utilisateur.getMonPanier() != null) {
        utilisateur.getMonPanier().modifierLeMagasinRetrait(request.getParameter("chgMagasin"));
      }
      
      // maj des infos de recherche si on est pas dans le rechargement d'une liste existante
      if (titre != null) {
        utilisateur.setInfosRechercheArticles(request.getParameter("champRecherche"), request.getParameter("groupeArticles"),
            request.getParameter("familleArticles"), request.getParameter("filtreFrs"), request.getParameter("favoris"),
            request.getParameter("historique"), request.getParameter("consulte"), request.getParameter("unPanierIntuitif"), indiceDebut);
        retour.append(titre);
      }
      else {
        retour.append("<h2><span id='titrePage'>" + utilisateur.getTraduction().traduire("$$rechercheArticles") + "</span></h2>");
      }
      
      // aficher le bloc de navigation du catalogue
      retour.append(afficherNavigationCatalogue(utilisateur));
      retour.append(afficherPopUpAjout());
      
      retour.append("<div class='blocContenu' id='listeArticles' >");
      ArrayList<GenericRecord> liste = null;
      
      // CHERCHER LES ARTICLES A AFFICHER
      // ++++++++++++++++++++++++++++++++++++++++++++++++++
      // Si il s'agit d'une nouvelle requête
      if (indiceDebut == null) {
        // Favoris
        if (request.getParameter("favoris") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesFavoris(utilisateur);
        }
        else if (request.getParameter("historique") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesAchats(utilisateur);
        }
        else if (request.getParameter("consulte") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesConsultes(utilisateur);
        }
        else if (request.getParameter("unPanierIntuitif") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesPanierIntuitif(utilisateur,
              request.getParameter("unPanierIntuitif"));
        }
        else if (request.getParameter("champRecherche") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retournerListeArticlesMoteur(utilisateur, request.getParameter("champRecherche"));
        }
        else if (request.getParameter("familleArticles") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retourneListeArticlesFamille(utilisateur, request.getParameter("familleArticles"));
        }
        else if (request.getParameter("groupeArticles") != null) {
          liste = ((GestionCatalogue) getMaGestion()).retourneListeArticlesGroupe(utilisateur, request.getParameter("groupeArticles"));
        }
        else if (titre == null) {
          liste = utilisateur.getDerniereListeArticle();
          indiceDebut = utilisateur.getIndiceDebut();
        }
      }
      
      // si on pagine
      utilisateur.setIndiceDebut(indiceDebut);
      // déterminer l'indice de début
      try {
        if (indiceDebut == null) {
          idDeb = 0;
        }
        else {
          idDeb = Integer.parseInt(indiceDebut);
        }
      }
      catch (Exception e) {
        e.printStackTrace();
      }
      
      ((GestionCatalogue) getMaGestion()).filtrerUneListeArticles(utilisateur);
      
      // AFFICHER LES ARTICLES SELECTIONNES
      // ++++++++++++++++++++++++++++++++++++++++++++++++++
      retour.append(afficherTitreListe(request, utilisateur.getDerniereListeArticle()));
      
      // Bilan sur le nombre d'articles total
      if (utilisateur.getDerniereListeArticle() != null) {
        nbArt = utilisateur.getDerniereListeArticle().size();
      }
      
      // Si la liste a un souci ou ne possède pas d'éléments
      if (nbArt <= 0) {
        if (nbArt == -1) {
          retour.append(
              "<div class='listesCommentaires'><span class='txtCommentaires'>Vos critères de saisie sont incorrects</span></div>");
        }
        else if (nbArt == 0) {
          retour.append(retournerBandeauFiltres(utilisateur, url));
          retour.append("<div class='listesCommentaires'><span class='txtCommentaires'>"
              + utilisateur.getTraduction().traduire("$$pasDarticles") + "</span></div>");
        }
      }
      // Si la liste ne comporte qu'un seul article et que nous ne sommes pas en mode filtre...
      else if (nbArt == 1 && request.getParameter("filtreFrs") == null) {
        retour = new StringBuilder(15000);
        retour.append(afficherUnArticle(request,
            "<h2 id='h2Catalogue'><span id='titrePage'>"
                + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$ficheArticle")
                + "</span></h2>",
            utilisateur.getDerniereListeArticle().get(0).getField("A1ETB").toString().trim(),
            utilisateur.getDerniereListeArticle().get(0).getField("A1ART").toString().trim(), "catalogue"));
        return retour.toString();
      }
      else {
        retour.append(retournerBandeauFiltres(utilisateur, url));
      }
      
      // Extraire un morceau de la liste afin d'en afficher des pages
      liste = ((GestionCatalogue) getMaGestion()).retourneMorceauListe(utilisateur, idDeb);
      
      // Afficher la liste
      if (liste != null && nbArt > 0) {
        retour.append(afficherLignesArticles(liste, utilisateur, nbArt, idDeb));
      }
      
      retour.append("</div>");
      
      retour.append(afficherMajTaillesDiv());
    }
    
    return retour.toString();
    
  }
  
  /**
   * afficher le bandeau des filtres de la liste des articles sélectionnés
   */
  private String retournerBandeauFiltres(UtilisateurWebshop pUtil, String url) {
    StringBuilder retour = new StringBuilder();
    HashMap<String, String[]> listeFrs = null;
    
    // Construction de l'url en fonction des derniers critéres de sélection
    if (pUtil.getDerniersFavoris() != null) {
      url = "Catalogue?favoris=" + pUtil.getDerniersFavoris();
    }
    else if (pUtil.getDernierHistorique() != null) {
      url = "Catalogue?historique=" + pUtil.getDernierHistorique();
    }
    else if (pUtil.getDerniersConsultes() != null) {
      url = "Catalogue?consulte=" + pUtil.getDerniersConsultes();
    }
    else if (pUtil.getDernierPaniersIntuitifs() != null) {
      url = "Catalogue?unPanierIntuitif=" + pUtil.getDernierPaniersIntuitifs();
    }
    else if (pUtil.getDerniereExpression() != null) {
      url = "Catalogue?champRecherche=" + pUtil.getDerniereExpression();
    }
    else if (pUtil.getDernierGroupeArticles() != null) {
      url = "Catalogue?groupeArticles=" + pUtil.getDernierGroupeArticles();
    }
    
    if (pUtil.getDerniereFamilleArticles() != null) {
      url += "&familleArticles=" + pUtil.getDerniereFamilleArticles();
    }
    
    String checked = "";
    if (pUtil.getFiltres().isStock_positif()) {
      checked = "checked";
    }
    
    String selected = "";
    
    retour.append("<div id='blocDesFiltres'>");
    
    retour.append("<input type='checkbox' name='stockPositif' value = '1' id='stockPositif' " + checked
        + " onChange = \"rajouterUnFiltreURL('" + url + "');\" /><label id='lb_stockPositif'>Articles en stock</label>");
    
    retour.append("<select name = 'filtreFrs' id='choixFournisseur' onChange = \"rajouterUnFiltreURL('" + url + "');\" >");
    
    if (pUtil.getDernierFiltreFournisseur() == null) {
      selected = "selected='selected'";
    }
    
    retour.append("<option value = '' " + selected + ">Tous les fournisseurs</option>");
    listeFrs = ((GestionCatalogue) getMaGestion()).retourneListeFournisseur(pUtil);
    if (listeFrs != null && listeFrs.size() > 0) {
      for (Entry<String, String[]> entry : listeFrs.entrySet()) {
        selected = "";
        if (pUtil.getDernierFiltreFournisseur() != null) {
          if (pUtil.getDernierFiltreFournisseur().equals(entry.getValue()[1] + entry.getValue()[2])) {
            selected = "selected='selected'";
          }
        }
        retour.append(
            "<option value = '" + entry.getValue()[1] + entry.getValue()[2] + "' " + selected + ">" + entry.getValue()[0] + "</option>");
      }
    }
    
    retour.append("</select>");
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Affiche le détail de chaque ligne article de la liste récupérée
   */
  private String afficherLignesArticles(ArrayList<GenericRecord> liste, UtilisateurWebshop utilisateur, float nbArt, int idDeb) {
    if (liste == null || utilisateur == null) {
      return "";
    }
    StringBuilder retour = new StringBuilder(10000);
    boolean vendable = false;
    
    String lienFicheArticle = "#";
    String A1ETB = null;
    String A1ART = null;
    String TARIF = null;
    String CAREF = "";
    String REF_AFFICHEE = "Aucune";
    String CND = "1";
    String A1LIB = null;
    String UNITE = "1";
    String LIBCND = null;
    String enTetesClients = "";
    int nbFinalQ = 5;
    int nbDecimalesQ = 0;
    
    if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
      enTetesClients = "";
    }
    else if (utilisateur.getMonPanier() != null && utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_CONSULTATION) {
      enTetesClients = "<span id='titreActions'>Quantité</span><a href='#' onClick = \"ajouterToutAuPanier(" + liste.size()
          + ");\" id='panierGlobal' alt='Ajouter tous les articles sélectionnés au panier' title='Ajouter tous les articles sélectionnés au panier'></a>";
    }
    
    // Entête de liste
    retour
        .append("<div class='listes' id='enTeteListes'>" + "<span id='titreCAREF'>" + utilisateur.getTraduction().traduire("$$reference")
            + "</span>" + "<span id='titreA1LIB'>" + utilisateur.getTraduction().traduire("$$libelle") + "</span>"
            + "<span id='titreSTOCK'>" + utilisateur.getTraduction().traduire("$$dispo") + "</span>" + "<span id='titreTARIF'>");
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_BOUTIQUE) {
      retour.append("Tarif TTC");
    }
    else if (utilisateur.getClient() != null) {
      retour.append("Tarif HT");
    }
    else {
      retour.append("Tarif HT");
    }
    retour.append(
        "</span>" + "<span id='titreLIBCND'>" + utilisateur.getTraduction().traduire("$$unite") + "</span>" + enTetesClients + "</div>");
    
    // Affichage des autres magasins pour affiner ses stocks
    if (utilisateur.getEtablissementEnCours().isVoir_stocks_ts_mags()
        && ((utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT) || (utilisateur.getMonPanier() != null
            && utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI && utilisateur.isVisuStock()))) {
      // On est un client
      if (utilisateur.getMonPanier() != null) {
        if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_RETRAIT
            && utilisateur.getMonPanier().getMagasinPanier() != null) {
          retour.append(
              "<a href=\"javascript:void(0)\" id='gestionDesStocks'  onClick=\"gererOuvertureStocks();\"><span id='spanEtatStock'>Stocks pour un retrait à <b>"
                  + utilisateur.getMonPanier().getMagasinPanier().getLibelleMagasin()
                  + "</b></span><span id='spanAutresStocks'>Voir les autres stocks</span></a>");
        }
        else if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON
            && utilisateur.getMagasinSiege() != null) {
          retour.append(
              "<a href=\"javascript:void(0)\" id='gestionDesStocks'  onClick=\"gererOuvertureStocks();\"><span id='spanEtatStock'>Stocks disponibles à la livraison</span><span id='spanAutresStocks'>Voir les autres stocks</span></a>");
        }
      }
      else {
        retour.append(
            "<a href=\"javascript:void(0)\" id='gestionDesStocks' onClick=\"gererOuvertureStocks();\" ><span>Voir les stocks</span></a>");
      }
      
      retour.append("" + "<div id='magasinsPourStocks' >" + afficherListeMagasinsStock(utilisateur) + "</div>");
    }
    
    String separateur = "";
    // scanner la liste d'articles
    for (int i = 0; i < liste.size(); i++) {
      // les init
      lienFicheArticle = "#";
      A1ETB = null;
      A1ART = null;
      TARIF = null;
      CAREF = "";
      CND = "1";
      A1LIB = "";
      UNITE = "UN";
      LIBCND = "";
      vendable = false;
      
      // liens fiche article et lien mise au panier
      if (liste.get(i).isPresentField("A1ART") && !liste.get(i).getField("A1ART").toString().trim().equals("")) {
        A1ART = liste.get(i).getField("A1ART").toString().trim();
        A1ETB = liste.get(i).getField("A1ETB").toString().trim();
        lienFicheArticle = "catalogue?A1ETB=" + A1ETB + "&amp;A1ART=" + liste.get(i).getField("A1ART").toString().trim();
      }
      // Gestion tarif
      if (liste.get(i).isPresentField("TARIF")) {
        TARIF = liste.get(i).getField("TARIF").toString().trim();
      }
      // Gestion unite
      if (liste.get(i).isPresentField("TARIF")) {
        UNITE = liste.get(i).getField("A1UNV").toString().trim();
      }
      
      // Gestion des décimales de la quantité de saisie
      nbDecimalesQ = 0;
      if (liste.get(i).getField("DECIMQ") != null && !liste.get(i).getField("DECIMQ").toString().trim().equals("")) {
        nbDecimalesQ = Integer.valueOf(liste.get(i).getField("DECIMQ").toString().trim());
      }
      
      // Lignes d'articles
      retour.append("<div class='listes'>");
      // Affiche si l'article est en promotion ou non
      if (liste.get(i).isPresentField("PROMO")) {
        retour.append("<div class='pastillePromo'>Promotion</div>");
      }
      retour.append(
          "<a class='CAREF' title='Référence article' alt='Référence article' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      
      if (liste.get(i).isPresentField("CAREF") && !liste.get(i).getField("CAREF").toString().trim().equals("")) {
        CAREF = liste.get(i).getField("CAREF").toString().trim();
      }
      
      // Affichage de la référence soit article soit fournisseur
      if (utilisateur.getEtablissementEnCours().getZone_reference_article() != null) {
        if (utilisateur.getEtablissementEnCours().getZone_reference_article().equals("CAREF") && !CAREF.equals("")) {
          REF_AFFICHEE = CAREF;
        }
        else if (A1ART != null) {
          REF_AFFICHEE = A1ART;
        }
        else {
          REF_AFFICHEE = "Aucune";
        }
      }
      
      retour.append(surlignerExpressionSaisie(utilisateur, REF_AFFICHEE));
      
      retour.append("</a>");
      
      // ++++++++++++++++ libellé article
      retour.append(
          "<a class='A1LIB' title='Libellé article' alt='Libellé article'  href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (liste.get(i).isPresentField("A1LIB") && !liste.get(i).getField("A1LIB").toString().trim().equals("")) {
        A1LIB = liste.get(i).getField("A1LIB").toString().trim();
        retour.append(surlignerExpressionSaisie(utilisateur, A1LIB));
      }
      else {
        retour.append("aucun");
      }
      retour.append("</a>");
      
      // ++++++++++++++++ Stock articles
      // Si on doit intégrer la notion de stock
      if (utilisateur.getMonPanier() != null && utilisateur.isStockDisponibleVisible()) {
        if (utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_NON_CHOISI
            && !utilisateur.getEtablissementEnCours().voir_stock_total()) {
          retour.append(
              "<a class='STOCK' href=\"javascript:void(0)\" onClick=\"ouvrirPopUpGenerale();\"  title='Affichage du stock de cet article' alt='Affichage du stock de cet article'>");
          retour.append("<span class='pasMagPanier'>&nbsp;</span>");
        }
        else {
          if (utilisateur.isVisuStock() || utilisateur.getEtablissementEnCours().is_filtre_stock()) {
            retour.append("<a class='STOCK' href='" + lienFicheArticle + "'>");
            if (liste.get(i).isPresentField("STOCK") && !liste.get(i).getField("STOCK").toString().trim().equals("")) {
              retour.append(afficherStockCorrectement(utilisateur, liste.get(i).getField("STOCK").toString().trim(), nbDecimalesQ));
              // on n'affiche l'attendu que si on ne montre pas le stock total sinon l'attendu n'est pas pertinent
              if (liste.get(i).isPresentField("ATTEN") && !liste.get(i).getField("ATTEN").toString().trim().equals("")
                  && utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_NON_CHOISI) {
                retour
                    .append("/" + afficherStockCorrectement(utilisateur, liste.get(i).getField("ATTEN").toString().trim(), nbDecimalesQ));
              }
            }
            else {
              retour.append("<span>NC</span>");
            }
          }
          else {
            retour.append(
                "<a class='STOCK' href='javascript:void(0)' onClick=\"traitementEnCours('catalogue?retour=1&visuStock=1');\" title='visulaliser les stocks' alt='visulaliser les stocks'>");
            retour.append("<span class='pasMagPanier'>&nbsp;</span>");
          }
          retour.append("</a>");
        }
      }
      else {
        retour.append("<a class='STOCK' href=\"javascript:void(0)\" >&nbsp;</a>");
      }
      separateur = "";
      // ++++++++++++++++ Tarif article
      retour.append("<a class='TARIF' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\"" + A1ETB
          + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (liste.get(i).isPresentField("TARIF") && !liste.get(i).getField("TARIF").toString().trim().equals("")) {
        vendable = ((GestionCatalogue) getMaGestion()).verifierSiVendable((BigDecimal) liste.get(i).getField("TARIF"),
            utilisateur.getEtablissementEnCours().isVend_articles_a_zero());
        retour.append(Outils.formaterUnTarifArrondi(utilisateur, liste.get(i).getField("TARIF")));
        separateur = "/ ";
      }
      else {
        retour.append("<span class='aucunStock'>aucun</span>");
      }
      retour.append("</a>");
      
      // L'unité de vente
      retour.append("<a class='LIBCND' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\"" + A1ETB
          + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (liste.get(i).isPresentField("LIBCND") && !liste.get(i).getField("LIBCND").toString().trim().equals("")) {
        LIBCND = liste.get(i).getField("LIBCND").toString().trim().toLowerCase();
        retour.append(separateur + LIBCND);
      }
      else {
        retour.append("&nbsp;");
      }
      retour.append("</a>");
      
      // ++++++++++++++++ Actions de mise au panier
      boolean gestionLots = false;
      String lesLots = "";
      BigDecimal lotMax = BigDecimal.ZERO;
      if (liste.get(i).getField("A1IN2") != null && liste.get(i).getField("A1IN2").toString().equals("1")
          && MarbreEnvironnement.IS_GESTION_LOTS) {
        lotMax = ((GestionCatalogue) maGestion).retournerLotMaxArticle(utilisateur, A1ETB, A1ART);
        
        if (lotMax.compareTo(BigDecimal.ZERO) > 0) {
          gestionLots = true;
          if (utilisateur.getMonPanier() != null) {
            lesLots = ((GestionCatalogue) maGestion).retournerLotsArticle(utilisateur, A1ETB, A1ART, nbDecimalesQ);
          }
        }
      }
      
      // On conditionne l'option mettre au panier que si: un magasin est associé au panier, on est un client et que l'article est vendable
      if (utilisateur.getClient() != null && !utilisateur.getClient().isAutoriseActions()) {
        retour.append("");
      }
      else if (utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_CONSULTATION
          && utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_BOUTIQUE && utilisateur.getMonPanier() != null && vendable) {
        if (liste.get(i).getField("CND") != null && !liste.get(i).getField("CND").toString().trim().equals("")
            && !liste.get(i).getField("CND").toString().trim().equals("0")) {
          CND = liste.get(i).getField("CND").toString().trim();
        }
        
        // Bouton "décrémenter d'une unité de conditionnement"
        retour.append("<a class='moinsArticle' href=\"javascript:void(0)\" onClick= \"majQuantiteZone('saisieQ" + i + "','-1','"
            + Outils.afficherValeurCorrectement(utilisateur, CND) + "'); majCnd('saisieQ" + i + "','"
            + Outils.afficherValeurCorrectement(utilisateur, CND) + "'," + nbDecimalesQ + ");\">&nbsp;</a>");
        // Zone typée en fonction de notre gestion de décimales/unité
        nbFinalQ = MarbreAffichage.SAISIE_MAX_QTE_ART;
        if (nbDecimalesQ > 0) {
          nbFinalQ = MarbreAffichage.SAISIE_MAX_QTE_ART + nbDecimalesQ - 1;
          retour.append("<span class='quantiteArticle'><input id='saisieQ" + i + "' type ='text' maxlength= '" + nbFinalQ
              + "' class='saisieQuantite' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'saisieQ" + i
              + "')\" onKeyup=\"controle_quantiteDecimales(event,'saisieQ" + i + "'," + nbDecimalesQ + "," + nbFinalQ + ")\" tabindex="
              + (i + 1) + " onChange=\"majCnd('saisieQ" + i + "','" + Outils.afficherValeurCorrectement(utilisateur, CND) + "',"
              + nbDecimalesQ + ");\" /></span>");
        }
        else {
          retour.append("<span class='quantiteArticle'><input id='saisieQ" + i + "' type ='text' class='saisieQuantite' maxlength = '"
              + nbFinalQ + "' onKeyPress='only_numeric(event)' onChange=\"majCnd('saisieQ" + i + "','"
              + Outils.afficherValeurCorrectement(utilisateur, CND) + "'," + nbDecimalesQ + ");\" tabindex=" + (i + 1) + " /></span>");
        }
        // Bouton "incrémenter d'une unité de conditionnement"
        retour.append("<a class='plusArticle' href=\"javascript:void(0)\" onClick= \"majQuantiteZone('saisieQ" + i + "','1','"
            + Outils.afficherValeurCorrectement(utilisateur, CND) + "'); majCnd('saisieQ" + i + "','"
            + Outils.afficherValeurCorrectement(utilisateur, CND) + "'," + nbDecimalesQ + ");\">&nbsp;</a>");
        
        // Formater la référence fournisseur avec StringEscapeUtils.escapeHtml4()
        // Utiliser le caractère échapement \ n'est pas suffisant dans un context HTML, il faut utiliser &quot;
        String javascriptPanier = "\"ajoutPanier(0,'" + REF_AFFICHEE + "','" + A1ETB + "','" + A1ART + "','" + TARIF + "','saisieQ" + i
            + "','" + StringEscapeUtils.escapeHtml4(CAREF) + "','" + CND + "','" + UNITE + "',true);\"";
        
        if (gestionLots && utilisateur.getMonPanier() != null) {
          javascriptPanier = "\"controlerLots('" + lesLots + "','saisieQ" + i + "'," + lotMax + ",'" + REF_AFFICHEE + "','" + A1ETB
              + "','" + A1ART + "','" + TARIF + "','saisieQ" + i + "','" + CAREF + "','" + CND + "','" + A1LIB + "','" + UNITE + "',"
              + nbDecimalesQ + ",'" + LIBCND + "',true);\"";
        }
        retour.append(
            "<a class='ajoutPanier' title=\"Ajouter l'article au panier\" alt=\"Ajouter l'article au panier\" href=\"javascript:void(0)\" onClick="
                + javascriptPanier + ">&nbsp;</a>");
        retour.append("<textarea class='donneesCachees' id='" + i + "'>" + A1ETB + "$$$" + A1ART + "$$$" + TARIF + "$$$" + CAREF + "$$$"
            + CND + "$$$" + UNITE + "$$$saisieQ" + i + "</textarea>");
      }
      else if (utilisateur.getPanierIntuitif() != null) {
        retour.append(
            "<a class='ajoutPanier' title=\"Ajouter l'article au panier\" alt=\"Ajouter l'article au panier intuitif\" href=\"javascript:void(0)\""
                + " onClick=\"ajouterPanierIntuitif('" + A1ART + "');\">&nbsp;</a>");
      }
      
      // Afficher le nom du fournisseur
      if (utilisateur.getEtablissementEnCours().isFourni_liste_articles()) {
        if (liste.get(i).isPresentField("FRNOM") && !liste.get(i).getField("FRNOM").toString().trim().equals("")) {
          retour.append("<div class='libDuFournisseur'>" + liste.get(i).getField("FRNOM").toString().trim() + "</div>");
        }
      }
      
      if (liste.get(i).getField("A1IN2") != null && liste.get(i).getField("A1IN2").toString().equals("1")
          && MarbreEnvironnement.IS_GESTION_LOTS) {
        retour.append(afficherGestionLots());
      }
      // Afficher le conditionnement si il est différent de 1
      else if (liste.get(i).getField("CND") != null && liste.get(i).getField("LIBCND") != null) {
        retour.append(afficherConditionnement(utilisateur, CND, liste.get(i).getField("LIBCND").toString().trim()));
      }
      
      retour.append("</div>");
    }
    
    // Paramètre performances: montrer les stocks à chaque fois ou pas à la solution de me faire chiiiiier
    utilisateur.setVisuStock(utilisateur.getEtablissementEnCours().isVisu_auto_des_stocks());
    // Pagination
    retour.append(afficherPaginationArticles(utilisateur, nbArt, idDeb, null));
    
    return retour.toString();
  }
  
  /**
   * Afficher la ligne de conditionnement si nécessaire
   */
  private String afficherConditionnement(UtilisateurWebshop utilisateur, String conditionnement, String unite) {
    String retour = "";
    try {
      if (Double.parseDouble(conditionnement) > 1) {
        retour += "<div class='conditionnement'>Vendu par " + unite + " et conditionné par "
            + Outils.afficherValeurCorrectement(utilisateur, conditionnement) + "</div>";
      }
    }
    catch (Exception e) {
    }
    
    return retour;
  }
  
  /**
   * Afficher la gestion des lots
   */
  private String afficherGestionLots() {
    String retour = "";
    retour += "<div class='conditionnement'>Article géré par lot</div>";
    return retour;
  }
  
  /**
   * Contenu de la pop up d'ajout au panier
   */
  private String afficherPopUpAjout() {
    StringBuilder builder = new StringBuilder();
    
    builder.append("<div id='popUpAjout'>");
    
    builder.append("Article ajouté au panier");
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Afficher le script de mise à jour des tailles de Div catalogue/listes
   * articles
   */
  private String afficherMajTaillesDiv() {
    String retour = "";
    
    retour += "<script type='text/javascript'>" + "majTailleNavig();" + "</script>";
    
    return retour;
  }
  
  /**
   * Affiche le catalogue
   */
  private String afficherLeCatalogue(HttpSession sess, String titre) {
    if (titre == null || sess == null) {
      return "";
    }
    StringBuilder builder = new StringBuilder(5000);
    String lien = null;
    String groupe = null;
    String id = null;
    String classe = null;
    String plusDeFamilles = "";
    String image = null;
    String puce = null;
    int compteurFamilles = 0;
    int compteurMax = 6;
    
    builder.append(titre);
    builder.append("<div class='blocContenu' id='blocCatalogue'>");
    if (sess.getAttribute("utilisateur") != null && sess.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      ArrayList<GenericRecord> liste =
          ((GestionCatalogue) getMaGestion()).recupereGroupesFamilles((UtilisateurWebshop) sess.getAttribute("utilisateur"));
      if (liste != null) {
        if ((((UtilisateurWebshop) sess.getAttribute("utilisateur")).getEtablissementEnCours().isVoir_favoris()
            || ((UtilisateurWebshop) sess.getAttribute("utilisateur")).getEtablissementEnCours().isVoir_art_historique()
            || ((UtilisateurWebshop) sess.getAttribute("utilisateur")).getEtablissementEnCours().isVoir_art_consulte())
            && ((UtilisateurWebshop) sess.getAttribute("utilisateur")).getTypeAcces() != MarbreEnvironnement.ACCES_BOUTIQUE) {
          builder.append("<div class='blocGroupe'>");
          
          builder.append(
              "<a class='groupeCatalogue' id='groupeMesArticles' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?favoris=1');\" >Mes articles</a>"
                  + "<a class='imagegroupeCatalogue' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?favoris=1');\"><img class='imageGroupe' src='"
                  + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/groupeMesImages.png' alt=''/></a>");
          if (((UtilisateurWebshop) sess.getAttribute("utilisateur")).getEtablissementEnCours().isVoir_favoris()) {
            builder.append(
                "<a class='familleCatalogue' id='lienMesFavoris' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?favoris=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Mes favoris</a>");
          }
          if (((UtilisateurWebshop) sess.getAttribute("utilisateur")).getEtablissementEnCours().isVoir_art_historique()
              && (((UtilisateurWebshop) sess.getAttribute("utilisateur")).getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT
                  || ((UtilisateurWebshop) sess.getAttribute("utilisateur")).getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS)) {
            builder.append(
                "<a class='familleCatalogue' id='lienMonHistorique' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?historique=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Mes achats</a>");
          }
          if (((UtilisateurWebshop) sess.getAttribute("utilisateur")).getEtablissementEnCours().isVoir_art_consulte()) {
            builder.append(
                "<a class='familleCatalogue' id='lienMesConsultes' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?consulte=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Récemment consultés</a>");
          }
          if (((UtilisateurWebshop) sess.getAttribute("utilisateur")).getPaniersIntuitifsActifs() != null) {
            builder.append(
                "<a class='familleCatalogue' id='lienPanierIntuitif' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?panierIntuitif=1');\" ><span class='puceFamillesMesArticles'>&nbsp;</span>Paniers intuitifs</a>");
          }
          builder.append("</div>");
        }
        for (int i = 0; i < liste.size(); i++) {
          // on sort de la boucle si souci de fields
          if (!liste.get(i).isPresentField("TYPE") || !liste.get(i).isPresentField("LIB") || !liste.get(i).isPresentField("IND")
              || liste.get(i).getField("IND").toString().trim().length() < 1) {
            break;
          }
          // Type le lien en fonction de groupe ou famille
          if (liste.get(i).getField("TYPE").toString().trim().equals("GR")) {
            // Si c'est pas le premier groupe de la liste on cloture son affichage
            if (i > 0) {
              // si plus de familles que l'affichage le peut
              if (compteurFamilles > compteurMax) {
                builder.append(plusDeFamilles);
              }
              
              builder.append("</div>");
            }
            
            // nouveau groupe
            builder.append("<div class='blocGroupe'>");
            plusDeFamilles = "";
            compteurFamilles = 0;
            
            lien = "groupeArticles=" + liste.get(i).getField("IND").toString().substring(0, 1);
            groupe = lien;
            classe = "groupeCatalogue";
            id = "GR" + liste.get(i).getField("IND").toString().substring(0, 1);
            image =
                "<img class='imageGroupe' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/" + id + ".png' alt=''/>";
            puce = "";
          }
          else {
            compteurFamilles++;
            
            lien = groupe + "&amp;familleArticles=" + liste.get(i).getField("TYPE") + liste.get(i).getField("IND");
            classe = "familleCatalogue";
            id = "FAM" + liste.get(i).getField("IND").toString().trim();
            image = "";
            puce = "<span class='puceFamilles'>&nbsp;</span>";
          }
          // Si on peut afficher la famille
          if (compteurFamilles <= compteurMax) {
            builder.append("<a class='" + classe + "' id='" + id + "' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?"
                + lien + "');\" >" + puce + liste.get(i).getField("LIB").toString().trim() + "</a>");
          }
          
          // s'il s'agit d'un groupe afficher et stocker ce qu'il faut
          if (!image.trim().equals("")) {
            builder.append("<a class='image" + classe + "' id='image" + id
                + "' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?" + lien + "');\" >" + image + "</a>");
            plusDeFamilles =
                "<a class='familleCatalogue'  href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?" + lien + "');\" >[+] "
                    + ((UtilisateurWebshop) sess.getAttribute("utilisateur")).getTraduction().traduire("$$autreFamille") + "</a>";
          }
          // fin de liste cloturer l'affichage
          if (i == liste.size() - 1) {
            if (compteurFamilles > compteurMax) {
              builder.append(plusDeFamilles);
            }
            builder.append("</div>");
          }
        }
      }
      else {
        builder.append("<p>Pas de catalogue à afficher</p>");
      }
    }
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Affiche le catalogue des paniers intuitifs
   */
  private String afficherLeCatalogueIntuitif(HttpServletRequest pRequest, String pTitre) {
    UtilisateurWebshop pUtil = null;
    StringBuilder retour = new StringBuilder(1000);
    
    pUtil = (UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur");
    
    retour.append(pTitre);
    
    retour.append("<div class='blocContenu' id='blocCatalogue'>");
    
    if (pUtil.getPaniersIntuitifsActifs() != null) {
      for (GenericRecord record : pUtil.getPaniersIntuitifsActifs()) {
        retour.append("<div class='blocGroupePI'>");
        retour.append("<a class='groupeCatalogue' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?unPanierIntuitif="
            + record.getField("IN_ID").toString().trim() + "');\" >" + record.getField("IN_LIBELLE").toString().trim() + "</a>"
            + "<a class='imageCatalogueIntuitif' href='javascript:void(0)' onClick=\"traitementEnCours('Catalogue?unPanierIntuitif="
            + record.getField("IN_ID").toString().trim() + "');\">" + "<img class='imagePanierIntuitif' src='"
            + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/panierIntuitif_" + record.getField("IN_ID").toString().trim()
            + ".jpg' alt=''/></a>");
        retour.append("</div>");
      }
    }
    else {
      retour.append("<p>Pas de catalogue à afficher</p>");
    }
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Affiche le titre de la liste *
   */
  private String afficherTitreListe(HttpServletRequest request, ArrayList<GenericRecord> liste) {
    UtilisateurWebshop utilisateur = null;
    String retour = "";
    String nbArticles = "";
    
    utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    
    if (utilisateur.getDernierHistorique() != null) {
      retour = "Mes achats récents";
    }
    else if (utilisateur.getDerniersFavoris() != null) {
      retour = "Mes articles favoris";
    }
    else if (utilisateur.getDernierPaniersIntuitifs() != null) {
      retour = "Mon panier intuitif";
    }
    else if (utilisateur.getDerniersConsultes() != null) {
      retour = "Récemment consultés";
    }
    else if (utilisateur.getDerniereExpression() != null) {
      retour = utilisateur.getTraduction().traduire("$$motsCles") + " [ "
          + getMaGestion().traiterCaracteresSpeciauxAffichage(utilisateur.getDerniereExpression()) + " ]";
    }
    else if (utilisateur.getDerniereFamilleArticles() != null) {
      String grp = null;
      // cas famille
      if (utilisateur.getDerniereFamilleArticles().startsWith("FA")) {
        grp = ((GestionCatalogue) getMaGestion()).retourneInfoFamille(utilisateur, utilisateur.getDerniereFamilleArticles());
        retour = utilisateur.getTraduction().traduire("$$famille") + " ";
      }
      // cas sousfamille
      else {
        grp = ((GestionCatalogue) getMaGestion()).retourneInfoSousFamille(utilisateur, utilisateur.getDerniereFamilleArticles());
        retour = utilisateur.getTraduction().traduire("$$sousFamille") + " ";
      }
      
      if (grp != null) {
        retour += "[ " + grp + " ]";
      }
      else {
        retour = "";
      }
      
    }
    // si le type de recherche est basé sur le groupe
    else if (utilisateur.getDernierGroupeArticles() != null) {
      ArrayList<GenericRecord> grp =
          ((GestionCatalogue) getMaGestion()).retourneInfoGroupe(utilisateur, utilisateur.getDernierGroupeArticles());
      if (grp != null && grp.size() > 0) {
        retour = utilisateur.getTraduction().traduire("$$groupe") + " [ " + grp.get(0).getField("LIB") + " ]";
      }
    }
    
    if (liste != null) {
      String pluriel = "";
      if (liste.size() > 1) {
        pluriel = "s";
      }
      
      String plafond = "";
      if (liste.size() == utilisateur.getEtablissementEnCours().getLimit_requete_article()) {
        plafond = utilisateur.getTraduction().traduire("$$plafond");
      }
      
      nbArticles = "<span id='nbArticlesListe'>" + plafond + liste.size() + " " + utilisateur.getTraduction().traduire("$$article")
          + pluriel + "</span>";
    }
    
    retour = "<h3><span class='puceH3'>&nbsp;</span>" + retour + "<span class='aCacher''>" + nbArticles + "</span></h3>";
    
    return retour;
  }
  
  /**
   * Affiche une liste de familles/sous familles correspondant à un groupe
   * sélectionné
   */
  private String afficherNavigationCatalogue(UtilisateurWebshop utilisateur) {
    StringBuilder retour = new StringBuilder(1000);
    if (utilisateur == null) {
      return null;
    }
    
    retour.append("<div class='blocContenu' id='navigationGroupe'>");
    retour.append("<h2 class='navigCatalogue'>" + utilisateur.getTraduction().traduire("Catalogue") + "</h2>");
    
    // Afficher la liste des groupes si le groupe est null
    if (utilisateur.getDernierGroupeArticles() == null) {
      ArrayList<GenericRecord> lstGrp = ((GestionCatalogue) getMaGestion()).retourneListeGroupes(utilisateur);
      if (lstGrp != null) {
        for (int i = 0; i < lstGrp.size(); i++) {
          retour.append("<a class='h4Groupes' href=\"javascript:void(0)\" onClick='traitementEnCours(\"catalogue?groupeArticles="
              + lstGrp.get(i).getField("IND").toString().substring(0, 1)
              + "\");'><span class='decoGroupes'>+</span><span class='libGroupe'>" + lstGrp.get(i).getField("LIB") + "</span></a>");
        }
      }
    }
    else {
      // Retourne et affiche le libelle du groupe
      ArrayList<GenericRecord> grp =
          ((GestionCatalogue) getMaGestion()).retourneInfoGroupe(utilisateur, utilisateur.getDernierGroupeArticles());
      if (grp != null && grp.size() > 0) {
        retour.append("<div id='imgDuGroupe'><img src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/GR"
            + utilisateur.getDernierGroupeArticles() + ".png' alt=''/></div>");
        retour.append(
            "<h4><a class='decoGroupes' href='catalogue'>^</a><span class='libGroupe'>" + grp.get(0).getField("LIB") + "</span></h4>");
      }
      
      ArrayList<GenericRecord> liste =
          ((GestionCatalogue) getMaGestion()).retournerFamillesPourUnGroupe(utilisateur, utilisateur.getDernierGroupeArticles());
      if (liste != null) {
        String retourFamille = "";
        String retourPartiel = "";
        String classe = "";
        String idFamille = "";
        // String affichage = "";
        String rappel = "";
        int nbSF = 0;
        
        for (int i = 0; i < liste.size(); i++) {
          // Si l'élement est une famille
          if (liste.get(i).getField("PARTYP").toString().trim().equals("FA")) {
            // affichage de la dernière famille et ses sous familles
            if (i > 0) {
              if (nbSF > 0) {
                retourFamille = "<a class='accordeonFamille' href=\"javascript:void(0)\" onClick='ouvrirSousFamilles(\"" + idFamille
                    + "\",null,true);'>+</a>" + retourFamille;
              }
              else {
                retourFamille = "<span class='accordeonFamille'>&nbsp;</span>" + retourFamille;
              }
              
              retour.append(retourFamille + retourPartiel);
            }
            
            retourPartiel = "";
            nbSF = 0;
            
            classe = "familleArticle";
            idFamille = "Famille" + liste.get(i).getField("PARIND").toString().trim();
            rappel = idFamille;
            
            retourFamille =
                "<a class='" + classe + "' id='" + idFamille + "' href='#' onClick='traitementEnCours(\"catalogue?groupeArticles="
                    + utilisateur.getDernierGroupeArticles() + "&amp;familleArticles=" + liste.get(i).getField("PARTYP").toString().trim()
                    + liste.get(i).getField("PARIND").toString().trim()
                    + "\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>"
                    + liste.get(i).getField("LIBELLEPAR").toString().trim() + "</a>";
          }
          // Sous famille
          else if (liste.get(i).getField("PARTYP").toString().trim().equals("SF")) {
            nbSF++;
            classe = "classeSousFamille";
            retourPartiel += "<span class='accordeonSousFamille' id='" + "accordeon" + liste.get(i).getField("PARIND").toString().trim()
                + "rappel" + rappel + "' >&nbsp;</span>" + "      <a class='" + classe + "' id='" + "sf"
                + liste.get(i).getField("PARIND").toString().trim() + "rappel" + rappel
                + "' href='#' onClick='traitementEnCours(\"Catalogue?groupeArticles=" + utilisateur.getDernierGroupeArticles()
                + "&amp;familleArticles=" + liste.get(i).getField("PARTYP").toString().trim()
                + liste.get(i).getField("PARIND").toString().trim() + "\");'>" + liste.get(i).getField("LIBELLEPAR").toString().trim()
                + "</a>";
          }
          else {
            classe = "";
            idFamille = "";
            retourFamille = "";
            retourPartiel = "";
            nbSF = 0;
          }
          
          if (i == liste.size() - 1) {
            if (nbSF > 0) {
              retourFamille = "<a class='accordeonFamille' href=\"javascript:void(0)\" onClick='ouvrirSousFamilles(\"" + idFamille
                  + "\",null,true);'>+</a>" + retourFamille;
            }
            else {
              retourFamille = "<span class='accordeonFamille'>&nbsp;</span>" + retourFamille;
            }
            
            retour.append(retourFamille + retourPartiel);
          }
        }
      }
      else {
        retour.append("<p>Pas de famille pour le groupe</p>");
      }
    }
    
    // Afficher les favoris si paramétrés
    if ((utilisateur.getEtablissementEnCours().isVoir_favoris() || utilisateur.getEtablissementEnCours().isVoir_art_historique()
        || utilisateur.getEtablissementEnCours().isVoir_art_consulte())
        && utilisateur.getTypeAcces() != MarbreEnvironnement.ACCES_BOUTIQUE) {
      retour.append(afficherNavigationMesArticles(utilisateur));
    }
    retour.append(afficherPromotionsNavigation(utilisateur));
    retour.append("</div>");
    
    retour.append(afficherMajOuvertureFamilles(utilisateur));
    
    return retour.toString();
  }
  
  /**
   * Afficher le script de mise à jour des tailles de Div catalogue/listes
   * articles
   */
  private String afficherMajOuvertureFamilles(UtilisateurWebshop utilisateur) {
    String retour = "";
    
    retour += "<script type='text/javascript'>" + "majOuvertureFamilles(\"" + utilisateur.getDernierGroupeArticles() + "\",\""
        + utilisateur.getDerniereFamilleArticles() + "\");" + "</script>";
    
    return retour;
  }
  
  /**
   * Permet d'afficher les articles liés à l'utilisateur soit par favoris, ses précédents paniers ou d'autres critères
   */
  private String afficherNavigationMesArticles(UtilisateurWebshop utilisateur) {
    String retour = "";
    
    retour += "<h2 class='navigCatalogue' id='navigMesArticles'>Mes articles</h2>";
    // Accéder à ses favoris
    if (utilisateur.getEtablissementEnCours().isVoir_favoris()) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='mesFavoris' href='#' onClick='traitementEnCours(\"catalogue?favoris=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Mes articles favoris</a>";
    }
    // Accéder à son historique d'articles
    if (utilisateur.getEtablissementEnCours().isVoir_art_historique() && (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT
        || utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_REPRES_WS)) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='monHistorique' href='#' onClick='traitementEnCours(\"catalogue?historique=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Mes achats récents</a>";
    }
    // Accéder à ses récemment consutlés
    if (utilisateur.getEtablissementEnCours().isVoir_art_consulte()) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='mesConsultes' href='#' onClick='traitementEnCours(\"catalogue?consulte=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Récemment consultés</a>";
    }
    // Paniers intuitifs
    if (utilisateur.getPaniersIntuitifsActifs() != null) {
      retour += "<span class='accordeonMesArticles'>&nbsp;</span>";
      retour +=
          "<a class='familleArticle' id='paniersIntuitifs' href='#' onClick='traitementEnCours(\"catalogue?panierIntuitif=1\");' onMouseOver='this.style.background=\"#f1ede9\"' onMouseOut='this.style.background=\"none\"'>Paniers intuitifs</a>";
    }
    
    return retour;
  }
  
  /**
   * Afficher la liste complète des magasins pour changement de visiblité du stock (mags de retrait ou livraison)
   */
  private String afficherListeMagasinsStock(UtilisateurWebshop utilisateur) {
    String retour = "";
    // Si cette liste n'est pas initialisée
    if (listeMagasin == null) {
      listeMagasin = ((GestionCatalogue) maGestion).retournerTousLesMagasins(utilisateur);
    }
    
    retour += "<div id='divInterneMags'>"
        + "<a href=\"javascript:void(0)\" class='changeMags' onClick=\"gererOuvertureStocks();\" ><span class='libMagChoix'>Pas de changement</span><span class='choisirNvMag' id='annulerChangement'>&nbsp;</span></a>";
    if (listeMagasin != null && listeMagasin.size() > 0) {
      for (int i = 0; i < listeMagasin.size(); i++) {
        // Si il ne s'agit pas du magasin courant dans le panier
        if (utilisateur.getMonPanier() != null
            && (!utilisateur.getMonPanier().getMagasinPanier().getCodeMagasin().equals(listeMagasin.get(i).getCodeMagasin())
                || utilisateur.getMonPanier().getModeRecup() == MarbreEnvironnement.MODE_LIVRAISON)) {
          retour += "<a onClick=\"traitementEnCours('catalogue?visuStock=1&modeRecup=" + MarbreEnvironnement.MODE_RETRAIT + "&chgMagasin="
              + listeMagasin.get(i).getCodeMagasin()
              + "');\" href='javascript:void(0)' class='changeMags'><span class='libMagChoix'>Stocks pour un retrait à "
              + listeMagasin.get(i).getLibelleMagasin() + "</span><span class='choisirNvMag'>&nbsp;</span></a>";
        }
      }
    }
    if (utilisateur.getMonPanier() != null && utilisateur.getMonPanier().getModeRecup() != MarbreEnvironnement.MODE_LIVRAISON) {
      retour += "<a onClick=\"traitementEnCours('catalogue?visuStock=1&modeRecup=" + MarbreEnvironnement.MODE_LIVRAISON
          + "');\" href='javascript:void(0)' class='changeMags'><span class='libMagChoix'>Voir les stocks pour une livraison</span><span class='choisirNvMag'>&nbsp;</span></a>";
    }
    if (utilisateur.getEtablissementEnCours().voir_stock_total()) {
      retour += "<a onClick=\"traitementEnCours('catalogue?visuStock=1&modeRecup=" + MarbreEnvironnement.MODE_RETRAIT + "&chgMagasin="
          + Magasin.TOUS_MAGS + "');\" href='javascript:void(0)' class='changeMags'>"
          + "<span class='libMagChoix'>Stocks tous magasins</span><span class='choisirNvMag'>&nbsp;</span>" + "</a>";
    }
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * On affiche les promotions liées à un groupe ou une famille sélectionnée
   */
  private String afficherPromotionsNavigation(UtilisateurWebshop pUtil) {
    if (pUtil == null || pUtil.getListePromotionsActives() == null) {
      return "";
    }
    
    StringBuilder builder = new StringBuilder();
    
    ArrayList<GenericRecord> listePromos = null;
    
    // On recherche par Famille ou sous famille
    if (pUtil.getDerniereFamilleArticles() != null || pUtil.getDernierGroupeArticles() != null) {
      listePromos = GestionCatalogue.recupererPromotionsParCatalogue(pUtil, NB_PROMOS_MAX_NAVIGATION);
    }
    
    if (listePromos != null) {
      builder.append("<div id='promosNavigation'>");
      
      for (GenericRecord record : listePromos) {
        String lienArticle = "#";
        if (record.isPresentField("PR_A1ETB") && record.isPresentField("PR_A1ART")) {
          lienArticle = "catalogue?A1ETB=" + record.getField("PR_A1ETB").toString().trim() + "&A1ART="
              + record.getField("PR_A1ART").toString().trim();
        }
        
        builder.append("<div class='unePromo'>");
        
        if (record.isPresentField("PR_PRIX")) {
          if (((BigDecimal) record.getField("PR_PRIX")).compareTo(BigDecimal.ZERO) > 0) {
            builder.append("<div class='prixPromo'>");
            builder.append(Outils.formaterUnTarifArrondi(pUtil, record.getField("PR_PRIX")));
            builder.append("</div>");
          }
        }
        builder.append("<a class='imagePromotion' href='" + lienArticle + "'>");
        builder.append("<img class='imagePromo' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/promotions/promotion_"
            + record.getField("PR_ID") + ".jpg'/>");
        builder.append("</a>");
        
        builder.append("<div class='detailsPromotion'>");
        if (record.isPresentField("PR_A1LIB") && !record.getField("PR_A1LIB").toString().trim().equals("")) {
          builder.append("<span class='detailLibPromo'>" + record.getField("PR_A1LIB").toString().trim() + "</span>");
        }
        builder.append("<span class='textePromo'>" + record.getField("PR_TEXTE").toString().trim() + "</span>");
        builder.append("<span class='validPromo'>Jusqu'au "
            + Outils.transformerDateSeriemEnHumaine(record.getField("PR_DATEF").toString().trim()) + "</span>");
        builder.append("</div>");
        
        builder.append("</div>");
      }
      
      builder.append("</div>");
    }
    else {
      return "";
    }
    
    return builder.toString();
  }
  
  /**
   * On affiche la liste des articles en variantes
   */
  private String afficherVariantes(UtilisateurWebshop pUtil, ArrayList<GenericRecord> pVariantes) {
    if (pVariantes == null || pVariantes.size() == 0) {
      return "";
    }
    
    StringBuilder builder = new StringBuilder();
    String A1ETB = null;
    String A1ART = null;
    String CAREF = "";
    String REF_AFFICHEE = "Aucune";
    
    builder.append("<div class='sousBlocs'  id='fi_variantes'>");
    builder.append("<p class='decoH5'>&nbsp;</p><h5>Articles associés</h5>");
    
    for (GenericRecord article : pVariantes) {
      if (article.isPresentField("A1ETB") && !article.getField("A1ETB").toString().trim().equals("")) {
        A1ETB = article.getField("A1ETB").toString().trim();
      }
      if (article.isPresentField("A1ART") && !article.getField("A1ART").toString().trim().equals("")) {
        A1ART = article.getField("A1ART").toString().trim();
      }
      
      builder.append("<div class='listes'>");
      // ++++++++++++++++ REF fournisseur ou article
      builder.append(
          "<a class='CAREF' title='Référence article' alt='Référence article' href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      
      if (article.isPresentField("CAREF") && !article.getField("CAREF").toString().trim().equals("")) {
        CAREF = article.getField("CAREF").toString().trim();
      }
      
      // Affichage de la référence soit article soit fournisseur
      if (pUtil.getEtablissementEnCours().getZone_reference_article() != null) {
        if (pUtil.getEtablissementEnCours().getZone_reference_article().equals("CAREF") && !CAREF.equals("")) {
          REF_AFFICHEE = CAREF;
        }
        else if (A1ART != null) {
          REF_AFFICHEE = A1ART;
        }
        else {
          REF_AFFICHEE = "Aucune";
        }
      }
      
      builder.append(surlignerExpressionSaisie(pUtil, REF_AFFICHEE));
      
      builder.append("</a>");
      
      // ++++++++++++++++ libellé article
      builder.append(
          "<a class='A1LIB' title='Libellé article' alt='Libellé article'  href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (article.isPresentField("A1LIB") && !article.getField("A1LIB").toString().trim().equals("")) {
        builder.append(surlignerExpressionSaisie(pUtil, article.getField("A1LIB").toString().trim()));
      }
      else {
        builder.append("aucun");
      }
      builder.append("</a>");
      builder.append(
          "<a class='FRNOM' title='Fournisseur' alt='Fournisseur'  href=\"javascript:void(0)\" onClick='envoyerDonneesEnPOST(\"catalogue\",\"A1ETB\",\""
              + A1ETB + "\",\"A1ART\",\"" + A1ART + "\");'>");
      if (article.isPresentField("FRNOM") && !article.getField("FRNOM").toString().trim().equals("")) {
        builder.append(surlignerExpressionSaisie(pUtil, article.getField("FRNOM").toString().trim()));
      }
      else {
        builder.append("");
      }
      builder.append("</a>");
      builder.append("</div>");
    }
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Surligner l'expression saisie dans le moteur dans la chaine passée en paramètre
   */
  private String surlignerExpressionSaisie(UtilisateurWebshop utilisateur, String libelle) {
    if (utilisateur == null || libelle == null) {
      return libelle;
    }
    if (utilisateur.getDerniereExpression() != null && !utilisateur.getDerniereExpression().trim().equals("")) {
      // L'expression doit être niterprétée comme du texte et non une expression régulière
      String expression = "\\b" + utilisateur.getDerniereExpression().trim() + "\\b";
      Pattern patternRecherche = Pattern.compile(expression);
      Matcher matcher = patternRecherche.matcher(libelle);
      libelle = matcher.replaceAll("<u class='surligne'>" + utilisateur.getDerniereExpression() + "</u>");
    }
    
    return libelle;
  }
  
}
