/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.servlet.ServletFileUpload;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionPanierIntuitifBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.ArticleIntuitif;
import ri.serien.webshop.metier.PanierIntuitif;
import ri.serien.webshop.outils.Outils;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class PanierIntuitifBO
 */
public class PanierIntuitifBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public PanierIntuitifBO() {
    super(new GestionPanierIntuitifBO(), "panierIntuitifBO",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "panierIntuitifBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("paniers", "PanierIntuitifBO", "Les paniers intuitifs", "All the baskets"));
    listeFilRouge
        .add(new FilRouge("unPanier", "PanierIntuitifBO?idPanier=" + MarbreEnvironnement.PARAM_FILROUGE, "Un panier", "One basket"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      
      UtilisateurWebshop utilisateur = ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"));
      PanierIntuitif copie = null;
      
      if (request.getParameter("idPanier") != null) {
        PanierIntuitif panier = ((GestionPanierIntuitifBO) maGestion).retournerUnPanier(utilisateur, request.getParameter("idPanier"));
        utilisateur.setPanierIntuitif(panier);
      }
      else if (request.getParameter("annulation") != null || request.getParameter("retour") != null) {
        utilisateur.setPanierIntuitif(null);
      }
      else if (request.getParameter("idModif") != null) {
        copie = utilisateur.getPanierIntuitif().clone();
        utilisateur.setPanierIntuitif(null);
      }
      
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("creation") != null) {
        out.println(afficherContenu(validationNouveauPanier("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", request),
            "unPanier", null, null, request.getSession()));
      }
      else if (request.getParameter("supprArticle") != null) {
        out.println(afficherContenu(afficherRetraitArticle("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", request), "unPanier",
            null, null, request.getSession()));
      }
      else if (request.getParameter("idModif") != null) {
        out.println(afficherContenu(modificationPanier("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", request, copie),
            "unPanier", null, null, request.getSession()));
      }
      else if (ServletFileUpload.isMultipartContent(request)) {
        out.println(afficherContenu(afficherModificationImage("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", request),
            "modifImage", null, null, request.getSession()));
      }
      else if (request.getParameter("annulation") != null) {
        out.println(afficherContenu(annulationPanier("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", request), "unPanier", null,
            null, request.getSession()));
      }
      else if (((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getPanierIntuitif() != null
          || request.getParameter("nouveauP") != null) {
        out.println(afficherContenu(afficherSaisieUnPanier("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", null, request),
            "unPanier", null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(afficherAccueil("<h2><span id='titrePage'>Paniers intuitifs</span></h2>", null, request), null, null,
            null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher l'accueil de la gestion des paniers intuitifs
   */
  private String afficherAccueil(String titre, String pMessage, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    if (pMessage != null) {
      retour.append("<div id='messagePrincipal'>" + pMessage + "</div>");
    }
    
    retour.append(afficherCreerUnPanier());
    
    retour.append(afficherListeDesPaniers(request.getParameter("indiceDebut"),
        (UtilisateurWebshop) request.getSession().getAttribute("utilisateur")));
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Afficher l'appel de la création d'un nouveau panier
   */
  private String afficherCreerUnPanier() {
    StringBuilder retour = new StringBuilder();
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Créer un nouveau panier</h3>");
    
    retour.append("<a class='lesBoutons' href='PanierIntuitifBO?nouveauP=1' >Nouveau panier</a>");
    
    return retour.toString();
  }
  
  /**
   * Afficher la liste des paniers intuitifs
   */
  private String afficherListeDesPaniers(String pDebut, UtilisateurWebshop pUtil) {
    StringBuilder retour = new StringBuilder();
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Les paniers intuitifs</h3>");
    
    // TRAITEMENT + PAGINATION
    if (pDebut == null) {
      GestionPanierIntuitifBO.initDerniereListePagination(pUtil, ((GestionPanierIntuitifBO) maGestion).retournerListePaniers(pUtil));
    }
    else {
      GestionPanierIntuitifBO.majListePartiellePagination(pUtil, pDebut);
    }
    
    if (pUtil.getDerniereListePartiellePagination() != null && pUtil.getDerniereListePartiellePagination().size() > 0) {
      // entête de colonne
      retour.append(
          "<div class='listes' id='enTeteListes'>" + "<span class='etbPanier'>Etb</span>" + "<span class='libPanier'>Libellé</span>"
              + "<span class='statPanier'>Statut</span>" + "<span class='artiPanier'>Articles</span>"
              + "<span class='creaPanier'>Création</span>" + "<span class='modifPanier'>Modification</span>" + "</div>");
      
      for (GenericRecord record : pUtil.getDerniereListePartiellePagination()) {
        retour.append("<a class='listes' href='PanierIntuitifBO?idPanier=" + record.getField("IN_ID").toString().trim() + "'>"
            + "<span class='etbPanier'>" + record.getField("IN_ETB").toString().trim() + "</span>" + "<span class='libPanier'>"
            + record.getField("IN_LIBELLE").toString().trim() + "</span>" + "<span class='statPanier'>"
            + PanierIntuitif.traduireStatut(record.getField("IN_STATUT").toString().trim()) + "</span>" + "<span class='artiPanier'>"
            + record.getField("NMB").toString().trim() + "</span>" + "<span class='creaPanier'>"
            + Outils.transformerDateSeriemEnHumaine(record.getField("IN_CREA").toString().trim()) + "</span>"
            + "<span class='modifPanier'>" + Outils.transformerDateSeriemEnHumaine(record.getField("IN_MODIF").toString().trim())
            + "</span>" + "</a>");
      }
    }
    
    return retour.toString();
  }
  
  /**
   * Afficher la saisie d'un panier intuitif
   */
  private String afficherSaisieUnPanier(String pTitre, String pMessage, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    UtilisateurWebshop pUtil = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    
    retour.append(pTitre);
    
    String libelle = "";
    int statut = 1;
    String dateCreation = Outils.transformerDateSeriemEnHumaine(Outils.recupererDateCouranteInt());
    String dateModification = dateCreation;
    String selected = "";
    
    retour.append("<div class='blocContenu'>");
    
    if (pMessage != null) {
      retour.append("<div id='messagePrincipal'>" + pMessage + "</div>");
    }
    
    // Modification
    if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif().getIdPanier() != null) {
      pUtil.getPanierIntuitif().getEtb();
      libelle = pUtil.getPanierIntuitif().getLibelle();
      statut = pUtil.getPanierIntuitif().getStatut();
      dateCreation = Outils.transformerDateSeriemEnHumaine(pUtil.getPanierIntuitif().getDateCreation());
      dateModification = Outils.transformerDateSeriemEnHumaine(pUtil.getPanierIntuitif().getDateModification());
      
      retour.append("<h3><span class='puceH3'>&nbsp;</span>Modifier un panier intuitif</h3>");
    }
    // Création
    else {
      if (pUtil.getPanierIntuitif() == null) {
        pUtil.setPanierIntuitif(new PanierIntuitif());
      }
      
      retour.append("<h3><span class='puceH3'>&nbsp;</span>Créer un panier intuitif</h3>");
    }
    
    if (pUtil.getPanierIntuitif().getEtb() != null) {
      pUtil.getPanierIntuitif().getEtb();
    }
    else {
      if (MarbreEnvironnement.ETB_DEFAUT != null) {
        MarbreEnvironnement.ETB_DEFAUT.getCodeETB();
      }
    }
    libelle = pUtil.getPanierIntuitif().getLibelle();
    
    statut = pUtil.getPanierIntuitif().getStatut();
    
    retour.append("<form name='panierIntuitif' id='formPanier' action='PanierIntuitifBO'>");
    
    retour.append("<label class='lbForms'>Etablissement du panier</label>");
    retour.append("<select name ='etbPanier'>");
    
    retour.append("<option value ='" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "' " + selected + ">"
        + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "</option>");
    retour.append("</select><br/>");
    retour.append("<label class='lbForms'>Libellé du panier</label>");
    retour
        .append("<input name='libPanier' type ='text' id='libPanier' value=\"" + traiterCaracteresINPUT(libelle) + "\" required /><br/>");
    retour.append("<label class='lbForms'>Statut du panier</label>");
    retour.append("<select name='sttPanier' id='sttPanier' value=''/>");
    if (statut == 0) {
      selected = "selected";
    }
    retour.append("<option value ='0' " + selected + ">Inactif </option>");
    if (statut == 1) {
      selected = "selected";
    }
    else {
      selected = "";
    }
    retour.append("<option value ='1' " + selected + ">Actif</option>");
    retour.append("</select><br/>");
    retour.append("<label class='lbForms'>Date de création</label>");
    retour.append("<input name='creaPanier' type ='text' class='datesPanier' value='" + dateCreation + "' readonly /><br/>");
    retour.append("<label class='lbForms'>Dernière modification</label>");
    retour.append("<input name='modifPanier' type ='text' class='datesPanier' value='" + dateModification + "' readonly />");
    
    if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif().getIdPanier() != null) {
      retour.append("<input name='idModif' type='hidden' value ='" + pUtil.getPanierIntuitif().getIdPanier() + "' />");
    }
    else {
      retour.append("<input name='creation' type='hidden' value ='1' />");
    }
    
    retour.append("</form>");
    
    if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif().getIdPanier() != null) {
      retour.append("<h3 id='pouetImage'><span class='puceH3'>&nbsp;</span>Ajouter une image</h3>");
      retour.append(afficherFormulaireImage(request, pUtil.getPanierIntuitif().getIdPanier()));
      retour.append("<h3><span class='puceH3'>&nbsp;</span>Les articles du panier</h3>");
      
      if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif().getNombreArticle() > 0) {
        retour.append("<div class='listes' id='enTeteListes'>" + "<span class='intCodeArticle'>Code article</span>"
            + "<span class='intRefArticle'>Référence fournisseur</span>" + "<span class='intLibArticle'>Libellé</span>"
            + "<span class='intTarifArticle'>Tarif général</span>" + "<span class='supprArtPanier'>&nbsp;</span>" + "</div>");
        for (ArticleIntuitif article : pUtil.getPanierIntuitif().getListeArticles()) {
          retour.append("<div class='listes'>" + "<span class='intCodeArticle'>" + article.getCodeArticle() + "</span>"
              + "<span class='intRefArticle'>" + article.getReferenceFourn() + "</span>" + "<span class='intLibArticle'>"
              + article.getLibelle() + "</span>" + "<span class='intTarifArticle'>"
              + Outils.formaterUnTarifArrondi(pUtil, article.getTarifGeneral()) + "</span>" + "<a href='PanierIntuitifBO?supprArticle="
              + article.getCodeArticle() + "' class='supprArtPanier'>Supprimer</a>" + "</div>");
        }
      }
      else {
        retour.append("<p class='messageRubrique'>Votre panier ne contient aucun article</p>");
      }
      
    }
    
    retour.append("<h3 id='finP'><span class='puceH3'>&nbsp;</span>Actions</h3>");
    
    retour.append("<div class='rubriquePanier' id='finPanier'>");
    
    if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif().getIdPanier() != null) {
      
      retour.append(
          "<a class='btRecuperation' id='validerbon' href='javascript:void(0)' onClick=\"traitementEnCoursForm(document.panierIntuitif);\" >"
              + "<span class='texte_bt'>Valider le panier</span>" + "</a>");
      
      retour.append("<a class='btRecuperation' id='plusArticles' href='PanierIntuitifBO?retour=1'>"
          + "<span class='texte_bt'>Retour à la liste</span>" + "</a>");
      
      retour.append("<a class='btRecuperation' id='plusArticles' href='catalogue'>" + "<span class='texte_bt'>Ajouter des articles</span>"
          + "</a>");
      
      retour.append("<a class='btRecuperation' id='annulerBon' href='PanierIntuitifBO?annulation="
          + pUtil.getPanierIntuitif().getIdPanier() + "'>" + "<span class='texte_bt'>Supprimer le panier</span>" + "</a>");
    }
    else {
      retour.append(
          "<a class='btRecuperation' id='validerbon' href='javascript:void(0)' onClick=\"traitementEnCoursForm(document.panierIntuitif);\" >"
              + "<span class='texte_bt'>Créer le panier</span>" + "</a>");
      
      retour.append("<a class='btRecuperation' id='plusArticles' href='PanierIntuitifBO?retour=1'>"
          + "<span class='texte_bt'>Retour à la liste</span>" + "</a>");
    }
    retour.append("</div>");
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Afficher le retour suite à un retrait d'article du panier intuitif
   */
  private String afficherRetraitArticle(String pTitre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    UtilisateurWebshop pUtil = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String message = null;
    
    if (request.getParameter("supprArticle") != null) {
      if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif() != null) {
        if (!pUtil.getPanierIntuitif().retirerUnArticle(request.getParameter("supprArticle"))) {
          message = "Problème de suppression article 2";
        }
      }
    }
    else {
      message = "Problème de suppression article 1";
    }
    
    // Affichage du panier
    retour.append(afficherSaisieUnPanier(pTitre, message, request));
    
    return retour.toString();
  }
  
  /**
   * Afficher le retour de la validation d'un nouveau panier
   */
  private String validationNouveauPanier(String pTitre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    UtilisateurWebshop pUtil = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String message = "";
    
    PanierIntuitif panier = new PanierIntuitif();
    
    panier.setEtb(request.getParameter("etbPanier"));
    panier.setLibelle(request.getParameter("libPanier"));
    panier.majStatut(request.getParameter("sttPanier"));
    try {
      int date = Outils.transformerDateHumaineEnSeriem(request.getParameter("creaPanier"));
      panier.setDateCreation(String.valueOf(date));
      
      date = Outils.transformerDateHumaineEnSeriem(request.getParameter("modifPanier"));
      panier.setDateModification(String.valueOf(date));
      
    }
    catch (Exception e) {
      
    }
    
    // Traitement
    int idPanier = ((GestionPanierIntuitifBO) maGestion).creerNouveauPanierIntuitif(pUtil, panier);
    
    if (idPanier > 0) {
      panier.setIdPanier(String.valueOf(idPanier));
      pUtil.setPanierIntuitif(panier);
      message = "Panier créé avec succès";
      
      // traitement de l'image
      ((GestionPanierIntuitifBO) maGestion).creerImagePanierIntuitif(idPanier);
    }
    else {
      message = "problème à la création du panier";
      
    }
    
    // Affichage du panier
    retour.append(afficherSaisieUnPanier(pTitre, message, request));
    
    return retour.toString();
  }
  
  /**
   * Suppression d'un panier intuititf sur la base de son Id
   */
  private String annulationPanier(String pTitre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    UtilisateurWebshop pUtil = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String message = "Problème à la suppression du panier ";
    
    // Annulation en base si le panier existe
    if (request.getParameter("annulation") != null) {
      int code = ((GestionPanierIntuitifBO) maGestion).supprimerUnPanierIntuitif(pUtil, request.getParameter("annulation"));
      
      if (code == 1) {
        message = "Succès de la suppression du panier ";
      }
    }
    
    // Afficher l'accueil avec le message retour
    retour.append(afficherAccueil(pTitre, message, request));
    
    return retour.toString();
  }
  
  /**
   * Afficher le retour de la modification d'un nouveau panier
   */
  private String modificationPanier(String pTitre, HttpServletRequest request, PanierIntuitif pPanier) {
    StringBuilder retour = new StringBuilder();
    UtilisateurWebshop pUtil = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String message = "Problème à la modification du panier";
    
    // Traitement sur le contenu du panier intuitif on le modifie en base
    if (pPanier != null) {
      pPanier.setEtb(request.getParameter("etbPanier"));
      pPanier.setLibelle(request.getParameter("libPanier"));
      pPanier.majStatut(request.getParameter("sttPanier"));
      
      // Traitement
      int code = ((GestionPanierIntuitifBO) maGestion).modifierUnPanierIntuitif(pUtil, pPanier);
      
      if (code == 1) {
        message = "Panier modifié avec succès";
      }
    }
    
    // Afficher l'accueil avec le msessage retour
    retour.append(afficherAccueil(pTitre, message, request));
    
    return retour.toString();
  }
  
  /**
   * Afficher le formulaire de téléchargement d'une image pour le panier
   */
  private String afficherFormulaireImage(HttpServletRequest pRequest, String pId) {
    if (pRequest == null || pId == null) {
      return "";
    }
    
    StringBuilder retour = new StringBuilder();
    String filename = "";
    
    retour.append(
        "<form action='PanierIntuitifBO' name='formImagePanier' id='formImagePanier' method='POST' enctype='multipart/form-data'>");
    retour.append("<label for = 'fichierImage' class='textLabelImagel'>Modifier l'image du panier</label>");
    retour.append("<input type='file' name='fichierImage' class='fileUpload' value='" + filename + "'/>");
    retour.append("<input type='hidden' name='modifImage' value='" + pId + "'/>");
    retour.append(
        "<input type='button' value='Modifier l&#039;image' class='btnMAJ' onClick=\"traitementEnCoursForm(document.formImagePanier);\" />");
    retour.append("</form>");
    
    retour.append("<div id='imgEnCours'>" + "<img id='imagePanierIntuitif' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
        + "/images/panierIntuitif_" + pId + ".jpg' alt=''/>" + "</div>");
    
    return retour.toString();
  }
  
  /**
   * On affiche le retour de la modification d'une image de panier
   */
  private String afficherModificationImage(String pTitre, HttpServletRequest pRequest) {
    StringBuilder retour = new StringBuilder();
    UtilisateurWebshop pUtil = (UtilisateurWebshop) pRequest.getSession().getAttribute("utilisateur");
    
    if (pUtil.getPanierIntuitif() != null && pUtil.getPanierIntuitif().getIdPanier() != null) {
      
      String pMessage = "problème de modification de l'image du panier";
      
      // Traitement de l'image
      if (((GestionPanierIntuitifBO) maGestion).uploadImage(pRequest, pUtil.getPanierIntuitif().getIdPanier()) == 1) {
        pMessage = "Image modifiée avec succès";
      }
      
      retour.append(afficherSaisieUnPanier(pTitre, pMessage, pRequest));
    }
    
    return retour.toString();
  }
}
