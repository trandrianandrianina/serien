/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionAccueilBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet AccueilBO
 */
public class AccueilBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AccueilBO() {
    super(new GestionAccueilBO(), "accueilBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "accueilBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficheAccesBO(request.getSession(), "<h2><span id='titrePage'>Gestion du Web Shop</span></h2>"),
          "accueilBO", null, null, request.getSession()));
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur survenue lors de l'affichage de la Gestion du back office");
    }
  }
  
  /**
   * Affiche les menus du back office
   */
  private String afficheAccesBO(HttpSession session, String titre) {
    String retour = titre;
    retour += "<div class='blocContenu'>";
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) session.getAttribute("utilisateur");
    
    if (utilisateur != null) {
      retour += afficherMessagesAlerteParametrage();
      retour += organiserMenu(utilisateur);
    }
    return retour;
  }
  
  /**
   * Ordonne et crée les menus du back office
   */
  private String organiserMenu(UtilisateurWebshop pUtilisateur) {
    String sectionMenu = "";
    List<GenericRecord> listMenu = pUtilisateur.getListeAccesBackOffice();
    if (listMenu != null) {
      // Créer une Map à partir de la liste dont la clé est la valeur dans BTN_NAME et la valeur celle dans BTN_LINK
      // Les éléments dans TreeMap sont ordonnés automatiquement par Clé
      TreeMap<String, String> mapMenu = new TreeMap();
      for (int index = 0; index < listMenu.size(); index++) {
        GenericRecord record = listMenu.get(index);
        mapMenu.put(record.getField("BTN_NAME").toString().trim(), record.getField("BTN_LINK").toString().trim());
      }
      
      // Créer les menus à afficher
      for (Entry<String, String> entry : mapMenu.entrySet()) {
        sectionMenu += "<a class='liensBO' href='" + entry.getValue() + " '>" + entry.getKey() + "</a>";
      }
    }
    
    return sectionMenu;
  }
  
  /**
   * Affichage d'une alerte quand le paramétrage de l'établissement n'est pas défini
   */
  private String afficherMessagesAlerteParametrage() {
    String messageAlerte = "";
    
    if (MarbreEnvironnement.ETB_DEFAUT != null) {
      if (!MarbreEnvironnement.ETB_DEFAUT.isParametresSontDefinis()) {
        messageAlerte += "<p class='messageAlerte' >ATTENTION !! Les établissements suivants nécessitent un paramétrage: ";
        messageAlerte += "<a class='etbPourris' href='metierBO?choixEtb=" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "'>- "
            + MarbreEnvironnement.ETB_DEFAUT.getLibelleETB() + " (" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + ")</a>";
        messageAlerte += "</p>";
      }
    }
    
    return messageAlerte;
  }
  
}
