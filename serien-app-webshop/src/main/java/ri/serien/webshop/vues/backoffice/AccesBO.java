/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionAccesBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class AccesBO
 */
public class AccesBO extends MouleVues {
  
  /**
   * 
   * @see HttpServlet#HttpServlet()
   */
  public AccesBO() {
    super(new GestionAccesBO(), "accesBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "accesBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if ((request.getParameter("Maj") != null)) {
        out.println(afficherContenu(
            traiterMAJ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"), request,
                "<h2><span id='titrePage'>Gestion des Accès du BO</span></h2>", request.getParameter("menuId")),
            null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(afficherMenuBO((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
            "<h2><span id='titrePage'>Gestion des Accès du BO</span></h2>", request), null, null, null, request.getSession()));
      }
      
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private String afficherMenuBO(UtilisateurWebshop utilisateur, String titre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Accès par défaut du BO &nbsp;&nbsp;</h3>");
    
    utilisateur.setListeDeTravail(((GestionAccesBO) maGestion).recupererMenuBO(utilisateur));
    
    if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0) {
      retour.append("<form action='accesBO' method='post'>");
      // entêtes de colonnes
      retour.append("<label for='labelcheckbox' class='labelGestionnaire'>Gestionnaire</label>");
      String checked = "";
      String[] acces = { "30" };
      for (int i = 0; i < utilisateur.getListeDeTravail().size(); i++) {
        retour.append("<div class='list'>");
        retour.append(
            "<label for='labelmenu' class='labelmenu' >" + utilisateur.getListeDeTravail().get(i).getField("BTN_NAME") + "</label>");
        // on parcourt les droits
        String disabled = "";
        for (int j = 0; j < acces.length; j++) {
          // S'il s'agit d'un admin on ne modifie pas les droits
          if (acces[j].equals("40")) {
            disabled = "disabled=\"disabled\" ";
          }
          utilisateur.recupererUnRecordTravail(((GestionAccesBO) maGestion).recupereParMenu(utilisateur,
              utilisateur.getListeDeTravail().get(i).getField("BTN_ID").toString(), acces[j]));
          if (utilisateur.getRecordTravail() != null) {
            checked = "checked='checked'";
          }
          else {
            checked = "";
          }
          // affiche une ligne de menu avec ses accès
          retour.append("<input type='checkbox' class='checkbox1' " + checked + " name='"
              + utilisateur.getListeDeTravail().get(i).getField("BTN_ID") + "' value='" + acces[j] + "' " + disabled + " />");
          
        }
        retour.append("</div>");
      }
      
    }
    retour.append("<input type='hidden'  id='Maj' name='Maj' value='1'>");
    retour.append("<input type='submit' class='btnMAJ' value='Valider' id='accesBO'/>");
    retour.append("</form>");
    retour.append("</div>");
    
    return retour.toString();
  }
  
  private String traiterMAJ(UtilisateurWebshop utilisateur, HttpServletRequest request, String titre, String menuId) {
    StringBuilder retour = new StringBuilder();
    
    if (titre != null) {
      retour.append(titre);
    }
    retour.append("<div class='blocContenu'>");
    
    if (((GestionAccesBO) maGestion).ajoutAccesBO(utilisateur, request) == -1) {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    else {
      retour.append("<div id='messagePrincipal'>Mise à jour effectuée avec succès</div>");
    }
    retour.append(afficherMenuBO(utilisateur, "", request));
    retour.append("</div>");
    return retour.toString();
  }
}
