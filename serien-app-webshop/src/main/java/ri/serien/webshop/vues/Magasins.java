/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionMagasins;
import ri.serien.webshop.environnement.UtilisateurWebshop;

/**
 * Servlet Magasins
 */
public class Magasins extends MouleVues {
  
  public Magasins() {
    super(new GestionMagasins(), "magasins", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "magasins.css?"
        + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>", MarbreEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("magasins", "magasins", "Magasins", "Stores"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficherMagasinsFiliale((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
          "<h2><span id='titrePage'>"
              + ((UtilisateurWebshop) request.getSession().getAttribute("utilisateur")).getTraduction().traduire("$$votreMagasin")
              + "</span></h2>"),
          "magasins", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher les magasins par filiales
   **/
  private String afficherMagasinsFiliale(UtilisateurWebshop utilisateur, String titre) {
    if (utilisateur == null) {
      return "";
    }
    
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    retour.append("<div class='blocContenu'>");
    
    utilisateur
        .setListeDeTravail(((GestionMagasins) maGestion).retourneMagasinsParEtbs(utilisateur, utilisateur.getEtablissementEnCours()));
    
    if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 0
        && utilisateur.getListeDeTravail().size() <= 5) {
      retour.append(afficherListeDetaillee(utilisateur, utilisateur.getListeDeTravail()));
    }
    else if (utilisateur.getListeDeTravail() != null && utilisateur.getListeDeTravail().size() > 5) {
      retour.append(afficherListeReduite(utilisateur, utilisateur.getListeDeTravail()));
    }
    else {
      retour.append("Pas de magasin récupéré");
    }
    
    retour.append("</div>");
    return retour.toString();
  }
  
  /**
   * Afficher les magasins en mode détaillé
   */
  private String afficherListeDetaillee(UtilisateurWebshop utilisateur, ArrayList<GenericRecord> liste) {
    StringBuilder retour = new StringBuilder();
    
    if (liste != null && liste.size() > 0) {
      for (int i = 0; i < liste.size(); i++) {
        // Contrôles de présence
        if (!liste.get(i).isPresentField("MG_NAME")) {
          liste.get(i).setField("MG_NAME", "");
        }
        if (!liste.get(i).isPresentField("LI_MESS")) {
          liste.get(i).setField("LI_MESS", "");
        }
        if (!liste.get(i).isPresentField("LI_HOR")) {
          liste.get(i).setField("LI_HOR", "");
        }
        if (!liste.get(i).isPresentField("LI_EQUI")) {
          liste.get(i).setField("LI_EQUI", "");
        }
        
        retour.append("<h3><span class='puceH3'>&nbsp</span><span class='MG_NAME'>" + liste.get(i).getField("MG_NAME").toString().trim()
            + "</span><span class='MG_ADR'>" + liste.get(i).getField("MG_ADR").toString().trim()
            + "</span><span class='MG_TEL'><span class='superflus'>tel: </span>" + liste.get(i).getField("MG_TEL").toString().trim()
            + "</span></h3>");
        retour.append("<div class='MG_MESS'>" + liste.get(i).getField("LI_MESS").toString().trim() + "</div>");
        retour.append("<div class='blocArrangement'>");
        retour.append("<div class='MG_HOR'><img class='decoRubriques' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/decoration/puceH3.png'/><span class='titreRubriques'>" + utilisateur.getTraduction().traduire("$$horaires")
            + "</span><span class='lookTextes'>" + liste.get(i).getField("LI_HOR").toString().trim() + "</span></div>");
        retour.append("<div class='MG_EQUI'><img class='decoRubriques' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/decoration/puceH3.png'/><span class='titreRubriques'>" + utilisateur.getTraduction().traduire("Infos")
            + "</span><span class='lookTextes'>" + liste.get(i).getField("LI_EQUI").toString().trim() + "</span></div>");
        // TODO remplacer par du dynamique dans DB2
        retour.append("<div class='MG_CARTE'><img class='decoRubriques' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE
            + "/images/decoration/puceH3.png'/><span class='titreRubriques'>" + utilisateur.getTraduction().traduire("$$localisation")
            + "</span><iframe" + liste.get(i).getField("MG_CARTE") + "></iframe></div>");
        retour.append("</div>");
      }
    }
    
    return retour.toString();
  }
  
  /**
   * Puisqu'on veut afficher beaucoup d'élements on affiche de manière réduite l'ensemble des magasins
   */
  private String afficherListeReduite(UtilisateurWebshop utilisateur, ArrayList<GenericRecord> liste) {
    StringBuilder retour = new StringBuilder();
    
    // entêtes de colonnes
    String entete = "<div  class='listes' id='enTeteListes'>" + "<span class='TITRENOM'>Nom du Magasin </span>"
        + "<span class='TITREADRE'>Adresse du magasin</span>" + "<span Class='TITRETEL'>Téléphone</span></div>";
    
    if (liste != null && liste.size() > 0) {
      String etbEnCours = null;
      for (int i = 0; i < liste.size(); i++) {
        // nouvelle ligne
        if (etbEnCours == null
            || (liste.get(i).isPresentField("US_ETB") && !liste.get(i).getField("US_ETB").toString().trim().equals(etbEnCours))) {
          if (etbEnCours != null) {
            retour.append("<br/><br/>");
          }
          if (liste.get(i).isPresentField("ETB_LIB")) {
            retour.append("<h3><span class='puceH3'>&nbsp</span><span class='MG_NAME'>"
                + liste.get(i).getField("ETB_LIB").toString().trim() + "</span></h3>");
          }
          retour.append(entete);
          etbEnCours = liste.get(i).getField("US_ETB").toString().trim();
        }
        retour.append("<div class='listes'>");
        retour.append("<a class='TITRENOM'>");// href='"+lienpatchs+"'"
        if (liste.get(i).isPresentField("MG_NAME")) {
          retour.append((liste.get(i).getField("MG_NAME").toString().trim()));
        }
        else {
          retour.append("aucune");
        }
        retour.append("</a>");
        
        retour.append("<a class='TITREADRE'>");// " + lienpatchs + "'>
        if (liste.get(i).isPresentField("MG_ADR")) {
          retour.append(liste.get(i).getField("MG_ADR").toString().trim());
        }
        else {
          retour.append("aucune");
        }
        retour.append("</a>");
        
        retour.append("<a class='TITRETEL'>");//
        if (liste.get(i).isPresentField("MG_TEL")) {
          retour.append(liste.get(i).getField("MG_TEL").toString().trim());
        }
        else {
          retour.append("aucune");
        }
        
        retour.append("</a>");
        retour.append("</div>");
      }
    }
    
    return retour.toString();
    
  }
}
