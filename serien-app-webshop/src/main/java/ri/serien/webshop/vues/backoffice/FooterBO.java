/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionFooterBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class FooterBO
 */
public class FooterBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public FooterBO() {
    super(new GestionFooterBO(), "footerBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "footerBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("footerBO", "footerBO", "Pieds de page", "Footers"));
    listeFilRouge.add(new FilRouge("footer", "footerBO", "Pieds de page", "Footers"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      if (request.getParameter("formulaireFooter") != null) {
        out.println(afficherContenu(traiterMiseAjourFooter("<h2><span id='titrePage'>Gestion du pied de page</span></h2>", request), null,
            null, null, request.getSession()));
      }
      else if (request.getParameter("footer") != null) {
        out.println(afficherContenu(afficherAccueil("<h2><span id='titrePage'>Gestion du pied de page</span></h2>", request, null,
            request.getParameter("footer")), "footer", null, null, request.getSession()));
      }
      else if (request.getParameter("suppFooter") != null) {
        out.println(afficherContenu(traiterSuppressionFooter("<h2><span id='titrePage'>Gestion du pied de page</span></h2>", request),
            null, null, null, request.getSession()));
      }
      else {
        out.println(afficherContenu(afficherAccueil("<h2><span id='titrePage'>Gestion du pied de page</span></h2>", request, null, null),
            null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher l'accueil de la gestion du pied de page
   */
  private String afficherAccueil(String titre, HttpServletRequest request, String message, String pCode) {
    StringBuilder retour = new StringBuilder();
    
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    if (message != null) {
      retour.append("<div id='messagePrincipal'>" + message + "</div>");
    }
    
    if (pCode == null) {
      retour.append(afficherListeFooters(request.getSession()));
    }
    
    retour.append(afficherDetailFooter(request.getSession(), pCode));
    
    retour.append("</div>");
    
    return retour.toString();
  }
  
  /**
   * Afficher la liste des footers dispo
   */
  private String afficherListeFooters(HttpSession pSession) {
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) pSession.getAttribute("utilisateur");
    StringBuilder retour = new StringBuilder();
    
    ArrayList<GenericRecord> liste = (((GestionFooterBO) maGestion).retournerListeFooters(utilisateur));
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Tous les pieds de page</h3>");
    
    for (GenericRecord footer : liste) {
      retour.append("" + "<a class='listeFooter' href='footerBO?footer=" + footer.getField("FT_ID").toString().trim() + "'>"
          + "<span class='elementFootLibe'>" + footer.getField("FT_LIB").toString().trim() + "</span>" + "<span class='elementFootLang'>"
          + traduireCodeLangue(footer.getField("FT_LANG").toString().trim()) + "</span>" + "<span class='elementFootLien' >"
          + footer.getField("FT_LIEN").toString().trim() + "</span>" + "</a>");
      retour.append(
          "<a class='suppFooter' href='#' onClick=\"confirmerUnLien('footerBO?suppFooter=" + footer.getField("FT_ID").toString().trim()
              + "','Souhaitez vous vraiment supprimer ce pied de page ?' );\">" + "<img id ='imageSuppr' src='"
              + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/suppression.png'/>" + "</a>");
    }
    
    return retour.toString();
  }
  
  /**
   * Traiter la mise à jour d'un footer (création ou mise à jour)
   */
  private String traiterSuppressionFooter(String titre, HttpServletRequest request) {
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String message = null;
    boolean retour = (((GestionFooterBO) maGestion).supprimerFooter(utilisateur, request.getParameter("suppFooter")));
    
    if (retour) {
      message = "Succès de suppression du pied de page";
    }
    else {
      message = "Echec de la suppression du pied de page";
    }
    
    return afficherAccueil(titre, request, message, null);
  }
  
  /**
   * Traiter la mise à jour d'un footer (création ou mise à jour)
   */
  private String traiterMiseAjourFooter(String titre, HttpServletRequest request) {
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) request.getSession().getAttribute("utilisateur");
    String message = null;
    int maj =
        (((GestionFooterBO) maGestion).majFooter(utilisateur, request.getParameter("idFooter"), request.getParameter("libelleFooter"),
            request.getParameter("langueFooter"), request.getParameter("lienFooter"), request.getParameter("cibleFooter")));
    
    if (maj == GestionFooterBO.OK_UPDATE) {
      message = "Succès de la mise à jour du pied de page";
    }
    else if (maj == 2) {
      message = "Succès du création du pied de page";
    }
    else if (maj < 0) {
      message = "Echec de la mise à jour du pied de page";
    }
    
    return afficherAccueil(titre, request, message, null);
  }
  
  /**
   * Afficher le détail d'un footer en mode modification ou création
   */
  private String afficherDetailFooter(HttpSession pSession, String pCode) {
    UtilisateurWebshop utilisateur = (UtilisateurWebshop) pSession.getAttribute("utilisateur");
    StringBuilder retour = new StringBuilder();
    String titre = "Modification d'un pied de page";
    String texteMaj = "Mettre à jour";
    GenericRecord footer = null;
    boolean isCreation = false;
    String libelleFooter = "";
    String lienFooter = "";
    String langueFooter = null;
    String cibleFooter = null;
    
    if (pCode == null) {
      isCreation = true;
      titre = "Création d'un pied de page";
      texteMaj = "Créer";
    }
    else {
      footer = (((GestionFooterBO) maGestion).retournerUnFooter(utilisateur, pCode));
      if (footer != null) {
        if (footer.isPresentField("FT_LIB")) {
          libelleFooter = footer.getField("FT_LIB").toString().trim();
        }
        if (footer.isPresentField("FT_LIEN")) {
          lienFooter = footer.getField("FT_LIEN").toString().trim();
        }
        if (footer.isPresentField("FT_LANG")) {
          langueFooter = footer.getField("FT_LANG").toString().trim();
        }
        if (footer.isPresentField("FT_TARG")) {
          cibleFooter = footer.getField("FT_TARG").toString().trim();
        }
      }
    }
    
    retour.append("<br/><br/><h3><span class='puceH3'>&nbsp;</span>" + titre + "</h3>");
    
    if (isCreation || footer != null) {
      retour.append("<form name='formAjoutFooter' id='formFooter' action='footerBO' method='post'>");
      retour.append("<label class='labelFooter'>Libellé</label>");
      retour.append("<input name='libelleFooter' id='libFooter' value ='" + libelleFooter
          + "' type='text' placeholder='Libellé pied de page' required maxlength = '40' /><br/>");
      retour.append("<label class='labelFooter'>Langue</label>");
      retour.append("<select name='langueFooter'>");
      String selected = "";
      if (langueFooter != null && langueFooter.equals("fr")) {
        selected = " selected ";
      }
      else {
        selected = "";
      }
      retour.append("<option value ='fr' " + selected + ">Français</option>");
      if (langueFooter != null && langueFooter.equals("en")) {
        selected = " selected ";
      }
      else {
        selected = "";
      }
      retour.append("<option value ='en' " + selected + ">Anglais</option><br/>");
      retour.append("</select><br/>");
      retour.append("<label class='labelFooter'>Lien</label>");
      retour.append("<input name='lienFooter' id='lienFooter' value ='" + lienFooter
          + "' type='text' placeholder='Lien du pied de page' required maxlength = '300'/><br/>");
      retour.append("<label class='labelFooter'>Cible du lien</label>");
      retour.append("<select name='cibleFooter'>");
      if (cibleFooter != null && cibleFooter.equals("BL")) {
        selected = " selected ";
      }
      else {
        selected = "";
      }
      retour.append("<option value ='BL' " + selected + ">Nouvel onglet</option>");
      if (cibleFooter != null && cibleFooter.equals("")) {
        selected = " selected ";
      }
      else {
        selected = "";
      }
      retour.append("<option value ='' " + selected + ">Onglet courant</option><br/>");
      retour.append("</select><br/>");
      if (pCode != null) {
        retour.append("<input type='hidden' name='idFooter' value='" + pCode + "'>");
      }
      retour.append("<input type='hidden' name='formulaireFooter' value='1'>");
      retour.append("<div><input class='btnMAJ' type='submit' value='" + texteMaj + "' id='texte1' /></div>");
      retour.append("</form>");
    }
    
    return retour.toString();
  }
  
  /**
   * Traduire un code langue en libellé langue
   */
  private String traduireCodeLangue(String pCode) {
    String retour = "";
    if (pCode == null) {
      return retour;
    }
    
    if (pCode.equals("fr")) {
      retour = "Français";
    }
    else if (pCode.equals("en")) {
      retour = "Anglais";
    }
    else {
      retour = pCode;
    }
    
    return retour;
  }
  
}
