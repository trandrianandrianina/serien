/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionSecuriteBO;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class SecuriteBO
 */
public class SecuriteBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public SecuriteBO() {
    super(new GestionSecuriteBO(), "securiteBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "securiteBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      out.println(afficherContenu(afficherAccueilSecurite("<h2><span id='titrePage'>Gestion de la sécurité</span></h2>", request), null,
          null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private String afficherAccueilSecurite(String titre, HttpServletRequest request) {
    StringBuilder retour = new StringBuilder();
    
    retour.append(titre);
    
    retour.append("<div class='blocContenu'>");
    
    retour.append("je suis dans le contenu afficherAccueilSecurite");
    
    retour.append("</div>");
    
    return retour.toString();
  }
}
