/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionAffichBO;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class AffichBO
 */
public class AffichBO extends MouleVues {
  
  public AffichBO() {
    super(new GestionAffichBO(), "affichBO", "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "affichBO.css' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      out.println(
          afficherContenu(afficherAccueilAffichage(request.getSession(), "<h2><span id='titrePage'>Paramètres d'affichage</span></h2>",
              request.getParameter("cssFile"), request.getParameter("passe")), null, null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher l'accueil du module Paramètres du client
   * 
   * @throws IOException
   */
  private String afficherAccueilAffichage(HttpSession session, String titre, String cssFile, String passe) throws IOException {
    String fichier = "";
    fichier = MarbreEnvironnement.DOSSIER_SPECIFIQUE + "css" + File.separator + "CSSspecifique.css";
    
    StringBuilder builder = null;
    builder = new StringBuilder();
    
    GestionFichierTexte gestionFichier = new GestionFichierTexte(fichier);
    ArrayList<String> contenu = null;
    builder.append(titre);
    
    builder.append("<div class='blocContenu'>");
    builder.append("<form name='ModifCss' action='AffichBO' method='POST' id='ModifCss'>");
    
    int nbrLignes = 0;
    
    if (passe == "0" || passe == "" || passe == null) {
      // lecture du fichier CSS
      contenu = gestionFichier.getContenuFichier();
      nbrLignes += contenu.size();
      if (nbrLignes > 0) {
        builder.append("<input type='hidden' name='passe' value='1'>");
      }
      
      builder.append("<div><label for = 't1' class='textLabel'>Modifier votre css :  </label></div>");
      builder.append("<textarea name='cssFile' id='cssFile' rows='50' cols='100'>");
      
      for (int i = 0; i < contenu.size(); i++) {
        builder.append(contenu.get(i));
        builder.append(System.getProperty("line.separator"));
      }
      builder.append("</textarea>");
      builder.append("<div><input name='Enregistrer' class='bouton' id='css_enregistrer' type='submit' value = 'Enregistrer'></div>");
      builder.append("</div>");
      builder.append("</form>");
      
    }
    else {
      builder.append("<input type='hidden' name='passe' value='1'>");
      
      gestionFichier.setContenuFichier(cssFile.replaceAll("\n", ""));
      gestionFichier.ecritureFichier();
      
      builder.append("<div id='messagePrincipal'>Votre fichier CSS est enregistré</div>");
    }
    
    return builder.toString();
    
  }
  
}
