/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libcommun.exploitation.mail.EnumNomVariableMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.GestionContact;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.metier.Etablissement;
import ri.serien.webshop.outils.mail.ParametreMail;

/**
 * Servle Contact
 */
public class ContactWebshop extends MouleVues {
  
  public ContactWebshop() {
    super(new GestionContact(), "contact",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "contact.css?" + MarbreEnvironnement.versionInstallee + "' rel='stylesheet'/>",
        MarbreEnvironnement.ACCES_PUBLIC);
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("contact", "contact", "Contact", "Contact"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      super.traiterPOSTouGET(request, response);
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      IdEtablissement idEtablissement = null;
      if (request.getParameter("etb_contact") != null) {
        idEtablissement = IdEtablissement.getInstance(request.getParameter("etb_contact"));
      }
      out.println(afficherContenu(afficherContact("<h2><span id='titrePage'>Contact</span></h2>", request.getParameter("mail"),
          request.getParameter("sujet"), request.getParameter("message"), request.getParameter("prenom"), request.getParameter("nom"),
          request.getParameter("societe"), request.getParameter("phone"), idEtablissement,
          (UtilisateurWebshop) request.getSession().getAttribute("utilisateur")), "contact", null, null, request.getSession()));
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Affichage du formulaire d'envoi de mail à l'adresse de contact de l'établissement
   */
  private String afficherContact(String titre, String pMailContact, String pSujetContact, String pMessageContact, String pPrenomContact,
      String pNomContact, String pSocieteContact, String pTelephoneContact, IdEtablissement pIdEtablissement,
      UtilisateurWebshop utilisateur) {
    if (utilisateur == null) {
      return "";
    }
    
    // String retour = "";
    StringBuilder builder = new StringBuilder(2000);
    builder.append(titre);
    
    String prenom = "";
    String nom = "";
    String societe = "";
    String telephone = "";
    String emailExpediteur = "";
    
    builder.append("<div class='blocContenu'>");
    
    // On gère l'envoi par mail de la confirmation
    if (!Constantes.normerTexte(pMailContact).equals("") && !Constantes.normerTexte(pMessageContact).equals("")) {
      // Déterminer l'adresse mail de contact pour l'établissement en cours
      String emailDestinataire = "";
      if (utilisateur.getEtablissementEnCours() != null) {
        emailDestinataire = Constantes.normerTexte(utilisateur.getEtablissementEnCours().getMail_contact());
      }
      else if (pIdEtablissement != null) {
        Etablissement etablissementTemporaire = new Etablissement(utilisateur, pIdEtablissement);
        if (etablissementTemporaire.getMail_contact() != null) {
          emailDestinataire = Constantes.normerTexte(etablissementTemporaire.getMail_contact());
        }
      }
      
      // Tester si on a une adresse mail destinatrice
      if (!emailDestinataire.isEmpty()) {
        // Préparer les paramètres dont le mail de contact a besoin
        ParametreMail parametreMail = preparerParametreMailContact(pSujetContact, pNomContact, pPrenomContact, pSocieteContact,
            pTelephoneContact, pMailContact, pMessageContact);
        
        utilisateur.envoyerMail(emailDestinataire, parametreMail, EnumTypeMail.MAIL_CONTACT_HTML);
      }
      
      builder.append("<div id='messagePrincipal'>" + utilisateur.getTraduction().traduire("$$confMailContact") + "</div>");
    }
    
    builder.append("<div id='boiteFormulaire'>");
    builder.append("<h3><span class='puceH3'>&nbsp</span>" + utilisateur.getTraduction().traduire("$$formulaireContact") + "</h3>");
    // Les champs adresse mail et messages doivent être obligatoirement saisis
    builder.append("<form name='Contact' action='contact' method='POST' id='formContact'>");
    
    if (utilisateur.getTypeAcces() == MarbreEnvironnement.ACCES_CLIENT && utilisateur.getClient() != null) {
      if (utilisateur.getNomContact() != null) {
        prenom = utilisateur.getNomContact();
        nom = utilisateur.getNomContact();
      }
      
      if (utilisateur.getClient().getNomClient() != null) {
        societe = utilisateur.getClient().getNomClient();
      }
      
      if (utilisateur.getTelContact() != null) {
        telephone = utilisateur.getTelContact();
      }
      
      emailExpediteur = utilisateur.getLogin();
    }
    
    if (utilisateur.getEtablissementEnCours() == null) {
      builder.append("<div class='ligneContact'>");
      builder.append("<label id='etb_label'>" + utilisateur.getTraduction().traduire("$$etablissement") + "</label>"
          + "<select name='etb_contact' id='contact_etb'>");
      if (MarbreEnvironnement.ETB_DEFAUT != null) {
        builder.append("<option value='" + MarbreEnvironnement.ETB_DEFAUT.getCodeETB() + "'>"
            + MarbreEnvironnement.ETB_DEFAUT.getLibelleETB() + "</option>");
      }
      builder.append("</select>");
      builder.append("</div>");
    }
    else {
      builder.append("<input name='etb_contact' type='hidden' value='" + utilisateur.getEtablissementEnCours() + "'/>");
    }
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_prenom_label'>" + utilisateur.getTraduction().traduire("$$prenom") + "</label>"
        + "<input name='prenom' id='contact_prenom' type='text' required maxlength='100' value='" + prenom + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_nom_label'>" + utilisateur.getTraduction().traduire("$$nomFamille") + "</label>"
        + "<input name='nom' id='contact_nom' type='text' required maxlength='100' value='" + nom + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_socie_label'>" + utilisateur.getTraduction().traduire("$$societe") + "</label>"
        + "<input name='societe' id='contact_societe' type='text' required  maxlength='100' value='" + societe + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_phone_label'>T&eacute;l&eacute;phone</label>"
        + "<input name='phone' id='contact_phone' type='tel' maxlength='100' value='" + telephone + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_mail_label'>" + utilisateur.getTraduction().traduire("$$adresseEmail") + "</label>"
        + "<input name='mail' id='contact_mail' type='email' required='Champs obligatoire' maxlength='240' value='" + emailExpediteur
        + "'/>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(" <label id='contact_sujet_label'>" + utilisateur.getTraduction().traduire("$$sujet") + "</label>"
        + "<input name='sujet' id='contact_sujet' required type='text' maxlength='240' />");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact' id='divMessage'>");
    builder.append(" <label id='contact_message_label'>Message</label>"
        + "<textarea name='message' id='contact_message' required='Champs obligatoire'></textarea>");
    builder.append("</div>");
    
    builder.append("<div class='ligneContact'>");
    builder.append(
        "<input name='Envoyer' id='contact_valider' type='submit' value = '" + utilisateur.getTraduction().traduire("$$envoyer") + "' >");
    builder.append("</div>");
    
    builder.append("</form>");
    
    // Afficher un look graphique à la page
    builder.append("<div id='decoContact'>");
    builder.append(
        "<img id='imgDecoContact' src='" + MarbreEnvironnement.DOSSIER_VIRTUEL_SPECIFIQUE + "/images/decoration/decoContact.png' />");
    builder.append("</div>");
    
    builder.append("</div>");
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Prépare les paramètres du mail contact.
   * 
   * @param pSujet Sujet du mail
   * @param pNom Nom du contact
   * @param pPrenom Prénom du contact
   * @param pSociete Société du contact
   * @param pTelephone Téléphone du contact
   * @param pExpediteur Mail du contact
   * @param pMessage Message du contact
   * @return
   */
  private ParametreMail preparerParametreMailContact(String pSujet, String pNom, String pPrenom, String pSociete, String pTelephone,
      String pExpediteur, String pMessage) {
    String titreContact = "";
    
    // L'un de ces paramètres est nécéssaire pour contenu du mail
    if (Constantes.normerTexte(pExpediteur).isEmpty()) {
      throw new MessageErreurException("L'adresse mail du contact n'est pas renseignée.");
    }
    
    // Renseigne le prénom du contact s'il existe
    if (!Constantes.normerTexte(pPrenom).isEmpty()) {
      titreContact += pPrenom + " ";
    }
    
    // Renseigne le nom du contact s'il existe
    if (!Constantes.normerTexte(pNom).isEmpty()) {
      titreContact += pNom;
    }
    
    // Renseigne le nom de la société s'il existe
    if (!Constantes.normerTexte(pSociete).isEmpty()) {
      if (titreContact.isEmpty()) {
        titreContact += "La société " + pSociete;
      }
      else {
        titreContact += " de la societé " + pSociete;
      }
    }
    
    // Ajoute le nom des variables et leurs valeurs dans la liste
    ParametreMail parametreMail = new ParametreMail();
    parametreMail.ajouterParametre(EnumNomVariableMail.MAIL_SUJET, StringEscapeUtils.escapeHtml4(pSujet));
    parametreMail.ajouterParametre(EnumNomVariableMail.TITRE_CONTACT, StringEscapeUtils.escapeHtml4(titreContact));
    parametreMail.ajouterParametre(EnumNomVariableMail.EXPEDITEUR, StringEscapeUtils.escapeHtml4(pExpediteur));
    parametreMail.ajouterParametre(EnumNomVariableMail.TELEPHONE_CONTACT, StringEscapeUtils.escapeHtml4(pTelephone));
    parametreMail.ajouterParametre(EnumNomVariableMail.MAIL_CORPS, StringEscapeUtils.escapeHtml4(pMessage));
    
    return parametreMail;
  }
}
