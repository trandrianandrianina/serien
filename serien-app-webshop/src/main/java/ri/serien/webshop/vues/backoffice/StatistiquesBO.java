/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionStatistiquesBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.outils.Outils;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class StatistiquesBO
 */
public class StatistiquesBO extends MouleVues {
  // Caractère de délimitation de colonne
  private final static String DELIMITER = ",";
  // Caractère de fin de ligne
  private final static String SEPARATOR = "\n";
  // En-tête de colonnes
  private final static String HEADER = "Numero,Date,Heure,Utilisateur,Type,Evenement";
  // Séparateur date
  private final static char SLASH = '/';
  
  public StatistiquesBO() {
    super(new GestionStatistiquesBO(), "statistiquesBO",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "statistiquesBO.css' rel='stylesheet'/>", MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      
      // Tester s'il faut afficher la page avec les statistiques CA
      if (request.getParameter("ca") != null) {
        // Récupérer l'année à afficher initialement
        int annee = 0;
        try {
          annee = Integer.parseInt(request.getParameter("ca"));
        }
        catch (NumberFormatException e) {
          annee = 0;
        }
        
        // Afficher la page après clic sur bouton statistiques CA
        String page = afficherPageStatistiqueCA(request.getSession(), "<h2><span id='titrePage'>Statistiques du Web Shop</span></h2>",
            annee, request);
        page = afficherContenu(page, null, null, null, request.getSession());
        out.println(page);
      }
      // Tester s'il faut afficher la page avec les statistiques connexion
      else if (request.getParameter("cnx") != null) {
        // Récupérer l'année à afficher
        int annee = 0;
        try {
          annee = Integer.parseInt(request.getParameter("cnx"));
        }
        catch (NumberFormatException e) {
          annee = 0;
        }
        
        // Afficher la page après clic sur bouton statistiques connexion
        String page = afficherPageStatistiqueConnexion(request.getSession(),
            "<h2><span id='titrePage'>Statistiques du WebShop</span></h2>", annee, request);
        page = afficherContenu(page, null, null, null, request.getSession());
        out.println(page);
      }
      // Tester s'il faut exporter les statistiques connexion
      else if (request.getParameter("exp") != null) {
        // Récupérer l'année à afficher
        int annee = 0;
        try {
          annee = Integer.parseInt(request.getParameter("exp"));
        }
        catch (NumberFormatException e) {
          annee = 0;
        }
        
        // Afficher la page après clic sur bouton Exporter
        out.println("");
        String page = exporterStatistiqueConnexion(request.getSession(), "<h2><span id='titrePage'>Statistiques du WebShop</span></h2>",
            annee, request, response);
        page = afficherContenu(page, null, null, null, request.getSession());
        out.println(page);
      }
      // Afficher la page d'accueil des statistiques
      else {
        // Afficher la page
        String page =
            afficherPageAccueilStatistique(request.getSession(), "<h2><span id='titrePage'>Statistiques du Web Shop</span></h2>");
        page = afficherContenu(page, null, null, null, request.getSession());
        out.println(page);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher la page d'accueil des statistiques.
   * 
   * @param pHttpSession Session HTTP.
   * @param pTitre Titre de la page.
   * @return Page HTML.
   */
  private String afficherPageAccueilStatistique(HttpSession pHttpSession, String pTitre) {
    String retour = pTitre;
    
    retour += "<div class='blocContenu'>";
    retour += "<a class='statistiquesBO' href='StatistiquesBO?ca=0'>Afficher statistiques CA</a>";
    retour += "<a class='statistiquesBO' href='StatistiquesBO?cnx=0'>Afficher statistiques connexions</a>";
    retour += "</div>";
    
    return retour;
  }
  
  /**
   * Afficher la page contenant les statistiques de CA par mois.
   * 
   * @param pHttpSession Session HTTP.
   * @param pTitre Titre de la page.
   * @param pAnnee Année dont afficher les statistiques (0 pour l'année en cours).
   * @param pHttpServletRequest Requête HTTP.
   * @return Page HTML.
   */
  private String afficherPageStatistiqueCA(HttpSession pHttpSession, String pTitre, int pAnnee, HttpServletRequest pHttpServletRequest) {
    StringBuilder builder = new StringBuilder();
    
    // Ajouter le titre
    builder.append(pTitre);
    
    // Démarrer le bloc
    builder.append("<div class='blocContenu'>");
    
    // Afficher les boutons
    builder.append("<a class='statistiquesBO' href='StatistiquesBO?cnx=0'>Afficher statistiques connexions</a>");
    
    // Affichage des bontons statistiques
    if (pHttpSession.getAttribute("utilisateur") != null && pHttpSession.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      UtilisateurWebshop utilisateur = (UtilisateurWebshop) pHttpSession.getAttribute("utilisateur");
      
      // Mettre l'année en cours par défaut
      if (pAnnee <= 0) {
        pAnnee = Calendar.getInstance().get(Calendar.YEAR);
      }
      
      // Charger la liste des CA par mois
      ArrayList<GenericRecord> liste = ((GestionStatistiquesBO) getMaGestion()).chargerListeCAParMois(utilisateur, pAnnee);
      
      // affiche le titre
      builder.append("<h3><span class='puceH3'>&nbsp;</span>CA commande Web  " + pAnnee + "</h3>");
      // affiche le selecteur de date
      builder.append(afficherComposantSelectionAnnee("ca", pAnnee));
      if (liste != null) {
        
        // entêtes de colonnes
        builder.append(
            "<div class='listes' id='enTeteListes'>" + "<span id='titreMOIS'>Mois</span>" + "<span id='titreCA'>CA</span>" + "</div>");
        
        for (int i = 0; i < liste.size(); i++) {
          builder.append("<a class='listes'>");
          
          builder.append("<span id='MOIS'>");
          if (liste.get(i).isPresentField("MOIS")) {
            builder.append(Outils.retourneMois(liste.get(i).getField("MOIS").toString().trim()));
          }
          else {
            builder.append("&nbsp;");
          }
          builder.append("</span>");
          
          builder.append("<span id='CA'>");
          if (liste.get(i).isPresentField("CA")) {
            builder.append(Outils.formaterUnTarifArrondi(utilisateur, liste.get(i).getField("CA")));
          }
          else {
            builder.append("&nbsp;");
          }
          builder.append("</span>");
          
          builder.append("</a>");
        }
        
        builder.append("</div>");
      }
    }
    
    builder.append("</div>");
    
    return builder.toString();
  }
  
  /**
   * Afficher la page contenant les statistiques de connexions par mois.
   * 
   * @param pHttpSession Session HTTP.
   * @param pTitre Titre de la page.
   * @param pAnnee Année dont afficher les statistiques (0 pour l'année en cours).
   * @param pHttpServletRequest Requête HTTP.
   * @return Page HTML.
   */
  private String afficherPageStatistiqueConnexion(HttpSession pHttpSession, String pTitre, int pAnnee,
      HttpServletRequest pHttpServletRequest) {
    StringBuilder builder = new StringBuilder();
    
    // Ajouter le titre
    builder.append(pTitre);
    
    // Démarrer le bloc
    builder.append("<div class='blocContenu'>");
    
    // Afficher les boutons
    builder.append("<a class='statistiquesBO' href='StatistiquesBO?ca=0'>Afficher statistiques CA</a>");
    builder.append("<a class='statistiquesBO' href='StatistiquesBO?exp=" + pAnnee + "'>Exporter</a>");
    
    // Récupérer l'utilisateur
    if (pHttpSession.getAttribute("utilisateur") != null && pHttpSession.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      UtilisateurWebshop utilisateur = (UtilisateurWebshop) pHttpSession.getAttribute("utilisateur");
      
      // Mettre l'année en cours par défaut
      if (pAnnee <= 0) {
        pAnnee = Calendar.getInstance().get(Calendar.YEAR);
      }
      
      // Charger la liste des connexions par mois
      ArrayList<GenericRecord> liste = ((GestionStatistiquesBO) getMaGestion()).chargerListeConnexionParMois(utilisateur, pAnnee);
      
      // affiche le titre
      builder.append("<h3><span class='puceH3'>&nbsp;</span>Nombre de connexions clients " + pAnnee + "</h3>");
      // affiche le selecteur de date
      builder.append(afficherComposantSelectionAnnee("cnx", pAnnee));
      
      if (liste != null) {
        
        // entêtes de colonnes
        builder.append("<div class='listes' id='enTeteListes'>" + "<span id='titreMOIS'>Mois</span>"
            + "<span id='titreCNX'>Connexions clients</span>" + "</div>");
        for (int i = 0; i < liste.size(); i++) {
          
          builder.append("<a class='listes'>");
          
          builder.append("<span id='MOIS'>");
          if (liste.get(i).isPresentField("MOIS")) {
            builder.append(Outils.retourneMois(liste.get(i).getField("MOIS").toString().trim()));
          }
          else {
            builder.append("&nbsp;");
          }
          builder.append("</span>");
          
          builder.append("<span id='CNX'>");
          if (liste.get(i).isPresentField("CNX")) {
            builder.append(liste.get(i).getField("CNX").toString().trim());
          }
          else {
            builder.append("&nbsp;");
          }
          builder.append("</span>");
          
          builder.append("</a>");
        }
        builder.append("</div>");
      }
    }
    builder.append("</div>");
    return builder.toString();
  }
  
  /**
   * Exporter les statistiques de connexions par mois.
   * 
   * @param pHttpSession Session HTTP.
   * @param pTitre Titre de la page.
   * @param pAnnee Année dont afficher les statistiques (0 pour l'année en cours).
   * @param pHttpServletRequest Requête HTTP.
   * @return Page HTML.
   */
  private String exporterStatistiqueConnexion(HttpSession pHttpSession, String pTitre, int pAnnee, HttpServletRequest pHttpServletRequest,
      HttpServletResponse pResponse) {
    StringBuilder builder = new StringBuilder();
    
    // Ajouter le titre
    builder.append(pTitre);
    
    // Démarrer le bloc
    builder.append("<div class='blocContenu'>");
    
    // Afficher les boutons
    builder.append("<a class='statistiquesBO' href='StatistiquesBO?ca=0'>Afficher statistiques CA</a>");
    builder.append("<a class='statistiquesBO' href='StatistiquesBO?exp=" + pAnnee + "'>Exporter</a>");
    
    // Récupérer l'utilisateur
    if (pHttpSession.getAttribute("utilisateur") != null && pHttpSession.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      UtilisateurWebshop utilisateur = (UtilisateurWebshop) pHttpSession.getAttribute("utilisateur");
      // Mettre l'année en cours par défaut
      if (pAnnee <= 0) {
        pAnnee = Calendar.getInstance().get(Calendar.YEAR);
      }
      
      // Afficher le titre
      builder.append("<h3><span class='puceH3'>&nbsp;</span>Nombre de connexions clients " + pAnnee + "</h3>");
      
      // Afficher le sélecteur de date
      builder.append(afficherComposantSelectionAnnee("exp", pAnnee));
      
      // Charger la liste des connexions par mois
      ArrayList<GenericRecord> listeEvenement =
          ((GestionStatistiquesBO) getMaGestion()).chargerListeCompleteConnexionParMois(utilisateur, pAnnee);
      if (listeEvenement != null) {
        try {
          pResponse.resetBuffer();
          pResponse.setContentType("text/csv");
          pResponse.addHeader("Content-disposition", "attachment; filename=\"connexions.csv\"");
          pResponse.setCharacterEncoding("UTF-8");
          OutputStream fluxSortie = pResponse.getOutputStream();
          
          // Entêtes de colonnes
          fluxSortie.write(HEADER.getBytes());
          fluxSortie.write(SEPARATOR.getBytes());
          
          // Parcourir le résultat de la requête Pour l'exporter au format CSV
          for (GenericRecord record : listeEvenement) {
            // Intercepter les erreurs sur un évènement pour ne pas bloquer l'ensemble de l'exportation
            try {
              // Ajouter la colonne identifiant évènement
              fluxSortie.write(("" + record.getIntegerValue("LO_ID")).getBytes());
              fluxSortie.write(DELIMITER.getBytes());
              
              // Ajouter la colonne date
              String date = "" + record.getIntegerValue("LO_DATE");
              StringBuilder dateFormatee = new StringBuilder(9);
              dateFormatee.append(date.substring(5)).append(SLASH);
              dateFormatee.append(date.substring(3, 5)).append(SLASH);
              dateFormatee.append(date.substring(1, 3));
              fluxSortie.write(dateFormatee.toString().getBytes());
              fluxSortie.write(DELIMITER.getBytes());
              
              // Ajouter la colonne heure
              String heure = "" + record.getIntegerValue("LO_HEURE");
              StringBuilder heureFormatee = new StringBuilder(9);
              if (heure.trim().length() < 6) {
                heureFormatee.append("0").append(heure.substring(0, 1)).append(":");
                heureFormatee.append(heure.substring(1, 3)).append(":");
                heureFormatee.append(heure.substring(3));
              }
              else {
                heureFormatee.append(heure.substring(0, 2)).append(":");
                heureFormatee.append(heure.substring(2, 4)).append(":");
                heureFormatee.append(heure.substring(4));
              }
              fluxSortie.write(heureFormatee.toString().getBytes());
              fluxSortie.write(DELIMITER.getBytes());
              
              // Ajouter la colonne utilisateur
              fluxSortie.write(record.getStringValue("US_LOGIN").getBytes());
              fluxSortie.write(DELIMITER.getBytes());
              
              // Ajouter la colonne type d'évènement
              fluxSortie.write(record.getStringValue("LO_TYPE").getBytes());
              fluxSortie.write(DELIMITER.getBytes());
              
              // Ajouter la colonne évènement
              fluxSortie.write(record.getStringValue("LO_MESS").getBytes());
              fluxSortie.write(DELIMITER.getBytes());
              
              // Ajouter le retour à la ligne
              fluxSortie.write(SEPARATOR.getBytes());
            }
            catch (Exception e) {
              Trace.erreur(e, "Un des évènements utilisateur n'a pas pu être exporté");
            }
          }
          
          // Traitement du flux de sortie
          fluxSortie.flush();
          fluxSortie.close();
        }
        catch (Exception e) {
          Trace.erreur(e, "Erreur lors de l'exportation des évènements utilisateur");
        }
      }
    }
    builder.append("</div>");
    return builder.toString();
  }
  
  /**
   * Afficher le composant de sélection de l'année.
   */
  private String afficherComposantSelectionAnnee(String pPage, int pAnnee) {
    String chaine = "";
    String sel = "";
    
    int a = Calendar.getInstance().get(Calendar.YEAR);
    
    chaine += "<select name='choixan' id='choixan' size='1' onChange='location = this.options[this.selectedIndex].value;'>";
    for (int i = 1; i < 6; i++) {
      if (pAnnee == a) {
        sel = "selected=*selected";
      }
      else {
        sel = "";
      }
      
      chaine += "<option " + sel + "value='StatistiquesBO?" + pPage + "=" + a + "'>" + a + "</option>";// onClick=\"dataGraph()\"
      a = a - 1;
    }
    chaine += "</select>";
    
    return chaine;
  }
}
