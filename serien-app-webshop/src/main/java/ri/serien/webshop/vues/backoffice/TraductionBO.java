/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.webshop.vues.backoffice;

import java.util.ArrayList;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.text.StringEscapeUtils;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.webshop.constantes.MarbreEnvironnement;
import ri.serien.webshop.controleurs.backoffice.GestionTraductionBO;
import ri.serien.webshop.environnement.UtilisateurWebshop;
import ri.serien.webshop.vues.FilRouge;
import ri.serien.webshop.vues.MouleVues;

/**
 * Servlet implementation class TraductionBO
 */
public class TraductionBO extends MouleVues {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public TraductionBO() {
    super(new GestionTraductionBO(), "traductionBO",
        "<link href='" + MarbreEnvironnement.DOSSIER_CSS + "traductionBo.css' rel='stylesheet'/>", MarbreEnvironnement.ACCES_RESPON_WS);
    
    filAccueil = new FilRouge("accueilBO", "accueilBO", "Back office", "Back office");
    listeFilRouge = new ArrayList<FilRouge>();
    listeFilRouge.add(filAccueil);
    listeFilRouge.add(new FilRouge("TraductionBO", "TraductionBO", "Tous les mots clés", "All mots cles"));
    listeFilRouge.add(new FilRouge("TRCle", "TraductionBO?TRcle=" + MarbreEnvironnement.PARAM_FILROUGE, "Un mot clé", "mot Clé"));
  }
  
  @Override
  protected void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      ServletOutputStream out = response.getOutputStream();
      String connexion = pattern.afficherConnexion(nomPage, request, response);
      
      redirectionSecurite(request, response);
      
      out.println(pattern.majDuHead(request, cssSpecifique, null));
      out.println(pattern.afficherPresentation(nomPage, request, connexion));
      if (request.getParameter("moteurRechercheCle") != null) {
        out.println(afficherContenu(afficherResultatMoteur(request.getSession(), "<h2><span id='titrePage'>TRADUCTION</span></h2>",
            request, request.getParameter("moteurRechercheCle")), "TRCle", null, null, request.getSession()));
      }
      else if (request.getParameter("TRcle") != null || (request.getParameter("trad") != null)) {
        out.println(
            afficherContenu(modifierUnMotClé(request.getSession(), "<h2><span id='titrePage'>Modification d'un mot clé</span></h2>",
                request, request.getParameter("TRcle"), "", null), "TRCle", null, null, request.getSession()));
      }
      else if (request.getParameter("majMoCle") != null) {
        out.println(afficherContenu(traiterMiseajour(request.getSession(), request, request.getParameter("motCle")), null, null, null,
            request.getSession()));
      }
      else {
        out.println(afficherContenu(
            afficherlisteTraduction((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
                "<h2><span id='titrePage'>Traduction</span></h2>", request, request.getParameter("indiceDebut")),
            null, null, null, request.getSession()));
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * On affiche la liste des traductions
   */
  private String afficherlisteTraduction(UtilisateurWebshop utilisateur, String titre, HttpServletRequest request, String indiceDebut) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    
    // TRAITEMENT + PAGINATION
    // si on pagine pas
    if (request.getParameter("indiceDebut") == null) {
      GestionTraductionBO.initDerniereListePagination(utilisateur,
          ((GestionTraductionBO) maGestion).retournerListeTraductions(utilisateur));
    }
    // si on pagine
    else {
      GestionTraductionBO.majListePartiellePagination((UtilisateurWebshop) request.getSession().getAttribute("utilisateur"),
          request.getParameter("indiceDebut"));
    }
    
    retour.append("<div class='blocContenu'>");
    
    // bouton de creation
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Insérer une traduction &nbsp;&nbsp;</h3>");
    
    retour.append("<a class='traduire' href='TraductionBO?trad=1' >Nouvelle traduction</a>");
    
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Rechercher une traduction &nbsp;&nbsp;</h3>");
    
    // moteur de recherche
    retour.append(
        "<form onSubmit='traitementEnCours(null);' action='traductionBO' method='POST' name='moteurRechercheCle' accept-charset=\"utf-8\" >");
    retour.append("<div class='recherche' id='rechTraduK'>");
    retour.append("<label id='lbRechTraduK'>Rechercher une traduction</label>");
    retour.append("<input type='search' name='moteurRechercheCle' id='rechercheInput' placeholder='"
        + utilisateur.getTraduction().traduire("$$Rechercher") + "' required/>");
    retour.append("<a id='validRecherche' href='#' onClick=\"traitementEnCoursForm(document.moteurRechercheCle);\">r</a>");
    retour.append("</div>");
    retour.append("</form>");
    
    if (utilisateur.getDerniereListePartiellePagination() != null && utilisateur.getDerniereListePartiellePagination().size() > 0) {
      // entête de colonne
      retour.append("<div class='listes' id='enTeteListes'><span id='labelMotsCles'>Mots clés</span>");
      
      retour.append("</div>");
      
      String lien = null;
      String cle = null;
      String valeur = null;
      
      for (int j = 0; j < utilisateur.getDerniereListePartiellePagination().size(); j++) {
        cle = null;
        valeur = null;
        if (utilisateur.getDerniereListePartiellePagination().get(j).isPresentField("TR_CLE")) {
          cle = utilisateur.getDerniereListePartiellePagination().get(j).getField("TR_CLE").toString().trim();
          lien = "TraductionBO?TRcle=" + cle;
        }
        
        if (utilisateur.getDerniereListePartiellePagination().get(j).isPresentField("TR_VALEUR")) {
          valeur = StringEscapeUtils
              .escapeHtml4(utilisateur.getDerniereListePartiellePagination().get(j).getField("TR_VALEUR").toString().trim());
          lien = "TraductionBO?TRcle=" + cle;
        }
        
        if (cle != null) {
          retour.append("<div class='listes'>");
          
          // début de la ligne
          retour.append("<a class='cle' href='" + lien + "'>");
          retour.append(cle);
          retour.append("</a>");
          
          retour.append("<a class='valeurLangue' href='" + lien + "'>");
          retour.append(valeur);
          retour.append("</a>");
          
          retour.append("</div>");
        }
        
      }
      
      // Pagination
      retour.append(afficherPagination(utilisateur, request.getParameter("indiceDebut"), null));
      retour.append("</div>");
    }
    
    return retour.toString();
  }
  
  /**
   * Modifier un mot clé
   */
  private String modifierUnMotClé(HttpSession session, String titre, HttpServletRequest request, String motCle, String langue,
      String page) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    page = "modifCle";
    
    retour.append("<div class='blocContenu'>");
    
    if ((UtilisateurWebshop) session.getAttribute("utilisateur") != null
        && session.getAttribute("utilisateur") instanceof UtilisateurWebshop) {
      
      retour.append("<form action='traductionBO' method ='post'>");
      retour.append("<h2><span class='titrePage'>&nbsp;</span>Traduction</h2>");
      if (motCle == null) {
        retour.append("<h3><span class='puceH3'>&nbsp;</span>Insérer une nouvelle traduction</h3>");
      }
      else {
        retour.append("<h3><span class='puceH3'>&nbsp;</span>Modifier une traduction</h3>");
      }
      retour.append("<label for='motCle' class='labelTraduction'>Mot Clé :</label>");
      if (motCle == null) {
        retour.append("<input type ='text' name='motCle' class='motCle' value='' >");
      }
      else {
        retour.append("<input type ='text' name='motCle' class='motCle' value=" + motCle + " readonly>");
      }
      
      // Codes langues
      ((UtilisateurWebshop) session.getAttribute("utilisateur"))
          .setListeDeTravail(maGestion.retournerLanguesWS((UtilisateurWebshop) session.getAttribute("utilisateur")));
      
      String contenu = null;
      
      if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail() != null
          && ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().size() > 0) {
        for (int i = 0; i < ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().size(); i++) {
          // un enregistrement
          ((UtilisateurWebshop) session.getAttribute("utilisateur")).recupererUnRecordTravail(((GestionTraductionBO) maGestion)
              .retourUnMotCleParLangue(((UtilisateurWebshop) session.getAttribute("utilisateur")), motCle,
                  ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()));
          if (((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail() != null) {
            contenu =
                ((UtilisateurWebshop) session.getAttribute("utilisateur")).getRecordTravail().getField("TR_VALEUR").toString().trim();
            retour.append("<label for='motCle' class='labelTraduction'>"
                + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB").toString()
                + "</label>");
            retour.append("<textarea name='motCle_"
                + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
                + "' class='valeur' >" + contenu + "</textarea>");
          }
          else {
            contenu = "";
            retour.append("<label for='motCle' class='labelTraduction'>"
                + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_LIB").toString()
                + "</label>");
            retour.append("<textarea name='motCle_"
                + ((UtilisateurWebshop) session.getAttribute("utilisateur")).getListeDeTravail().get(i).getField("LA_ID").toString()
                + "' class='valeur' >" + contenu + "</textarea>");
          }
        }
        
        retour.append("<input type='hidden'  id='majMoCle' name='majMoCle' value='1'>");
        
        retour.append("<input type='submit' class='btnMAJ' value='MAJ' id='majMoCle'/>");
      }
      else {
        retour.append("<div>Codes langues manquants (table LANGUES)</div>");
      }
      
      retour.append("</div>");
    }
    return retour.toString();
  }
  
  private String traiterMiseajour(HttpSession session, HttpServletRequest request, String motCle) {
    String chaine = "";
    String TRcle = null;
    if (motCle != null) {
      TRcle = motCle;
    }
    
    if (TRcle != null) {
      chaine += "<div class='blocContenu'>";
    }
    
    chaine += "<h2><span id='titrePage'>Validation de la traduction</span></h2>";
    
    if (((GestionTraductionBO) maGestion).miseAjourMotCle((UtilisateurWebshop) session.getAttribute("utilisateur"), request,
        TRcle) != -1) {
      chaine += "<div id='messagePrincipal'>Mot clé mis à jour avec succès.</div>";
      chaine += afficherlisteTraduction(((UtilisateurWebshop) session.getAttribute("utilisateur")), "", request,
          request.getParameter("indiceDebut"));
    }
    else {
      chaine += "<div id='messagePrincipal'>Il y a eu une erreur pendant la mise à jour de vos données.</div>";
      chaine += modifierUnMotClé(session, "", request, request.getParameter("valeurMotCle"), request.getParameter("motCleCode"), null);
    }
    
    chaine += "<div/>";
    
    return chaine;
  }
  
  private String afficherResultatMoteur(HttpSession session, String titre, HttpServletRequest request, String resultat) {
    StringBuilder retour = new StringBuilder();
    retour.append(titre);
    ArrayList<GenericRecord> listeResultat =
        (((GestionTraductionBO) maGestion).retourneMotCleduMoteur((UtilisateurWebshop) session.getAttribute("utilisateur"), resultat));
    retour.append("<div class='blocContenu'>");
    retour.append("<h3><span class='puceH3'>&nbsp;</span>Résultat du moteur &nbsp;&nbsp;</h3>");
    String lien = null;
    if (listeResultat != null && listeResultat.size() > 0) {
      for (int i = 0; i < listeResultat.size(); i++) {
        lien = "TraductionBO?TRcle=" + listeResultat.get(i).getField("TR_CLE");
        
        retour.append("<div class='listes'>");
        retour.append("<a class='cle' href='" + lien + "'>");
        if (listeResultat.get(i).isPresentField("TR_CLE")) {
          retour.append(listeResultat.get(i).getField("TR_CLE").toString().trim());
        }
        retour.append("</a>");
        retour.append("</div>");
      }
      
    }
    else {
      retour.append("mauvais critère de recherche");
    }
    
    retour.append("</div>");
    return retour.toString();
  }
}
