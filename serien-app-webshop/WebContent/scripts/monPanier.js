//methode permettant d'afficher le calendrier
 $(function openCalendarRetrait () {
	
 $("#idDateRetrait").datepicker($.datepicker.regional["fr"]);
	
});

/*permet de g�rer les boutons d'incr�ments de quantit� d'articles*/
function majQuantiteZone(idZone, incr) {
	var init = document.getElementById(idZone).value;
	var total = 0;
	var increment = Number(incr);

	if (init == null || isNaN(init)) {
		init = 0;
		if (increment == -1)
			total = 0;
		else
			total = 1;
	} else
		total = Number(init) + Number(increment);

	if (total < 0)
		total = 0;

	document.getElementById(idZone).value = total;
}

/**
 *  Contr�ler que la saisie
 *  Va falloir factoriser toutes ces méthodes catalogue - panier
 *   */
function only_numeric(e) 
{
	var key = window.event ? e.keyCode : e.which;

	// autoriser les touches de suppr et de retour
	if (key != 0 && key != 8) {
		var valeur = String.fromCharCode(key);
		var regex = new RegExp("^[0-9]*$", "g");
		// sinon ne pas autoriser ce qui n'est pas num�rique
		if (!regex.test(valeur)) {
			e.returnValue = false;
			if (e.preventDefault)
				e.preventDefault();
		}
	}
}

//variable globale 
zoneInitiale ="";

/**
 * Si la saisie quantité n'est pas numérique ou ,/. on annule 
 * Va falloir factoriser toutes ces méthodes catalogue - panier
 * */
function recupererZoneInitiale(e,idZone)
{
	var key = window.event ? e.keyCode : e.which;

	// autoriser les touches de suppr et de retour
	if (key != 0 && key != 8)
	{
		var valeur = String.fromCharCode(key);
		var regex = new RegExp("^([0-9]|[,.])?$", "g");
		if (!regex.test(valeur))
		{
			e.returnValue = false;
			if (e.preventDefault) e.preventDefault();
		}
	}
	
	if(key==9 || key==46)
		zoneInitiale = "";
		
	zoneInitiale = document.getElementById(idZone).value;
}

/**
 * Si la saisie quantité n'est pas conforme aux entiers/décimales prévus 
 * Va falloir factoriser toutes ces méthodes catalogue - panier
 * */
function controle_quantiteDecimales(e,idZone,decimales,nbFinal)
{
	var Valeurinit = document.getElementById(idZone).value;
	var key = window.event ? e.keyCode : e.which;
	var entier = nbFinal-decimales-1;
	var chaineRegEx = "^[0-9]{1," + entier + "}[,.]?[0-9]{0," + decimales + "}$";
		var regexChamp = new RegExp(chaineRegEx, "g");
		if (key != 0 && key != 8 && key!=9 && key!=46)
		{
			if(!regexChamp.test(Valeurinit))
			{
				document.getElementById(idZone).value = zoneInitiale;
			}
			else
			{
				var chaineRegEx2 = "^[0-9]{" + entier + "," + (entier+1) + "}$";
				var regexChamp2 = new RegExp(chaineRegEx2, "g");
				//dans le cas précis ou on dépasse le nb d'entier
				if(regexChamp2.test(Valeurinit))
				{
					document.getElementById(idZone).value = Valeurinit + "," ;
				}
			}
		}
}

/**
 * Cette methode permet d'ajouter dynamiquement un article au panier 
 * Va falloir factoriser toutes ces méthodes catalogue - panier
 * */
function modifQuantite(id, zoneSaisie,cnd,ancienneQte,decimales) 
{
	majCnd(zoneSaisie,cnd,decimales);
	var quantite = document.getElementById(zoneSaisie).value.trim();
	
	if (quantite == null || quantite == "" || quantite == "0")
		quantite = "1";

	var url = "monPanier?panierArticle=" + id + "&panierQuantite=" + quantite + "&ancienneQte=" + ancienneQte + "#ancreArticles";
	
	document.location.href = url;
}

/**
 *  permet de modifier la quantit� selon le conditionnement 
 *  Va falloir factoriser toutes ces méthodes catalogue - panier
 *  */
function majCnd(idZone,cnd,decimales)
{
	var init = document.getElementById(idZone).value;
	//Les virgules en point
	var valeurPoint = init.replace(/,/g,'.');
	var total = 0;
	var conditionnement = Number(cnd);
	var rst = 0;
	var quot = 0;

	total = Number(valeurPoint);

	if (conditionnement > 1)
	{
		if (valeurPoint < Number(cnd))
		{
			total = Number(cnd);
		}

		rst = total % Number(cnd);
		if (rst != 0)
		{
			quot = total / Number(cnd);
			total = (parseInt(quot) + 1) * Number(cnd);
		}
	}

	if (total < 0) total = 0;
	
	//Formatage décimales du total
	if(total!=0)
		document.getElementById(idZone).value = total.toFixed(decimales);
	else 
		document.getElementById(idZone).value = "";
}

/*
 * Mise � jour des infos bloc adresse / livraison du panier 
 * */
function majInfosPanier(nomZone) 
{
	var valeurModif = document.getElementById(nomZone).value.trim();

	var url = "monPanier?modificationBlocAdresse=1&" + nomZone + "=" + valeurModif;

	if (window.XMLHttpRequest) 
	{
		requete = new XMLHttpRequest();
		if (requete) 
		{
			requete.open("POST", url, true);
			requete.send();
		}
	} 
	else if (window.ActiveXObject) {
		requete = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete) 
		{
			requete.open("POST", url, true);
			requete.send();
		} else
			alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} else {
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}


}
