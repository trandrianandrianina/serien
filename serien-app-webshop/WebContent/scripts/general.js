	

/**
 * Cette m�thode permet d'ouvrir cot� client un accordeon
 * des articles
 */
function gererOuvertureAccordeon(accordeon, idElement){
	var blocContenu = document.getElementById(idElement);
	var isAffiche = (blocContenu.style.display == "block");
	
	// Ouvrir ou fermer la boite de contenu
	if (isAffiche && blocContenu != null) {
		blocContenu.style.display = "none";
		accordeon.style.marginBottom = "8px";
	} else {
		blocContenu.style.display = "block";
		accordeon.style.marginBottom = "0px";
	}
}

/**
 * Cette m�thode permet de rendre visible un accordeon tout en cachant tous les
 * autres
 */
function montrerAccordeon(idElement, idContenu) {
	var blocAouvrir = document.getElementById(idElement);
	var blocContenus = document.getElementById(idContenu);

	liste = document.getElementsByClassName("accordeon");
	listeContenu = document.getElementsByClassName("contenuAccordeon");
	// on cache tous les autres accordeons
	if (liste != null && liste.length > 1) {
		for ( var i = 0; i < liste.length; i++)
			liste[i].style.display = "none";
	}
	// on cache tous les contenus d'accordeon
	if (listeContenu != null && listeContenu.length > 1) {
		for ( var i = 0; i < listeContenu.length; i++)
			listeContenu[i].style.display = "none";
	}

	// On ne montre que celui que l'on a s�lectionn�
	if (blocAouvrir != null)
		blocAouvrir.style.display = "block";
	if (blocContenus != null)
		blocContenus.style.display = "block";
}

function cacherUnElement(idElement)
{
	if(document.getElementById(idElement)!=null)
		document.getElementById(idElement).style.display="none";
}

/**
 * Permet de repositionner une pop dynamiquement afin qu'elle soit centr�e et ajust�e � la hauteur
 * */
function repositionnerPopUp(elementPop) 
{
	var larg = 0;
	var haut = 0;
	
	larg = (window.innerWidth);
	haut = (window.innerHeight);
	//SI le window n'existe pas
	if(larg==0 && hauteur==0 && document.body)
	{
		larg = (document.body.clientWidth);
		haut = (document.body.clientHeight);
	}
	
	// on met � jour la largeur de la pop-up
	if(elementPop.offsetWidth)
	{
		largeurPop = elementPop.offsetWidth;
		hauteurPop = elementPop.offsetHeight;
	}
	else if(elementPop.style.pixelWidth)
	{
		largeurPop = elementPop.style.pixelWidth;
		hauteurPop = elementPop.style.pixelHeight;
	}
			
	elementPop.style.left = parseInt(larg/2) - parseInt(largeurPop/2) + "px";
	elementPop.style.top = parseInt(haut/2) - parseInt(hauteurPop/2) + "px";
}

/**
 * fait monter la pop-up de traitement en cours en attendant que la page soit atteinte
 */
function traitementEnCours(url) 
{
	if (document.getElementById("filtreTotal") != null&& document.getElementById("traitementEnCours") != null) 
	{
		document.getElementById("filtreTotal").style.display = "block";
		document.getElementById("traitementEnCours").style.display = "block";
		repositionnerPopUp(document.getElementById("traitementEnCours"));
	}

	if(url!=null)
		document.location.href = url;
}

/**
 * fait monter la pop-up de traitement en cours en attendant que le formulaire
 *  ait fait son fucking job
 */
function traitementEnCoursForm(formulaire)
{
	traitementEnCours(null);
	formulaire.submit();
}

/**
 * Traiter un formaulaire simplement
 */
function traitementForm(formulaire)
{
	formulaire.submit();
}

/**
 * Fait un raz graphique du panier en haut � droite
 * */
function razPanier(montant)
{
	if (document.getElementById("quantitePanier") != null)
		document.getElementById("quantitePanier").innerHTML = "(0)";
	
	if (document.getElementById("totalMonPanier") != null)
		document.getElementById("totalMonPanier").innerHTML = montant;
}

/**
 * Envoyer les donn�es en POST plut�t qu'en GET (caract�res sp�ciaux)
 * */
function envoyerDonneesEnPOST(page,nameData1,data1,nameData2,data2)
{
	traitementEnCours(null);
    var form = document.createElement('form');
    form.setAttribute('action', page);
    form.setAttribute('method', 'post');
    form.setAttribute('accept-charset', 'utf-8');
    var inputvar1 = document.createElement('input');
    inputvar1.setAttribute('type', 'hidden');
    inputvar1.setAttribute('name', nameData1);
    inputvar1.setAttribute('value', data1);
    form.appendChild(inputvar1);
    var inputvar2 = document.createElement('input');
    inputvar2.setAttribute('type', 'hidden');
    inputvar2.setAttribute('name', nameData2);
    inputvar2.setAttribute('value', data2);
    form.appendChild(inputvar2);
    
    document.body.appendChild(form);
    form.submit();
}

/**
 * Permet de lancer un PDF du contenu affich�
 * */
function gestionEditions(page,mode) 
{
	var url = page + "?modeEdition=" + mode;

	if (window.XMLHttpRequest) 
	{
		requete = new XMLHttpRequest();
		if (requete) 
		{
			requete.open("POST", url, true);
			
			requete.onreadystatechange = majIHMEditions;
			requete.send();
		}
	} 
	else if (window.ActiveXObject) 
	{
		requete = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete) 
		{
			requete.open("POST", url, true);
			
			requete.onreadystatechange = majIHMEditions;
			requete.send();
		} 
		else alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} 
	else 
	{
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}
}

/**
 * Permet d'ouvrir cot� client le
 * */
function majIHMEditions() 
{
	if (requete.readyState == 4) 
	{
		if (requete.status == 200 || requete.status == 0)
		{
			window.open("pdf/" + requete.responseText); return false;
		}
		else
			alert('Une erreur est survenue lors l\'ouverture du PDF'
					+ '\n\nCode retour = ' + requete.statusText);
	}
}

/**
 * Envoyer une variable en POST
 * */
function post_en_url(url, cle, parametre) 
{
	//Création dynamique du formulaire
	var form = document.createElement('form');
	form.setAttribute('method', 'POST');
	form.setAttribute('action', url);
	//Ajout des param�tres sous forme de champs cach�s
	var champCache = document.createElement('input');
	champCache.setAttribute('type', 'hidden');
	champCache.setAttribute('name', cle);
	champCache.setAttribute('value', parametre);
	form.appendChild(champCache);
	
	//Ajout du formulaire � la page et soumission du formulaire
	document.body.appendChild(form);
	form.submit();
}

function confirmerUnLien(lien,message)
{
	if(confirm(message))
		window.location.href=lien;
}

