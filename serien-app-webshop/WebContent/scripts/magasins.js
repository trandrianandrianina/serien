function gererOuvertureFiltres(P1, P2)
{
	var contenu = document.getElementById(P1);
	var elementC = document.getElementById(P2);
	liste = document.getElementById(P2).getElementsByClassName("ouvrirFiltre");
	
	if(contenu.style.display=="block")
	{
		contenu.style.display="none";
		elementC.style.marginBottom ="8px";
		
		for(var i=0; i<liste.length; i++)
		{
			liste[i].style.backgroundImage="url(images/decoration/filtre.png)";
		}		
	}
	else
	{
	
		contenu.style.display="block";
		elementC.style.marginBottom ="0px";

		for(var i=0; i<liste.length; i++)
		{
			liste[i].style.backgroundImage="url(images/decoration/filtreF.png)";
			liste[i].style.backgroundRepeat = "no-repeat" ;
		}
		
	}
}