opaciteGlob = 170;
timer = null;
conditionnementEnCours = 1;
decimalesEnCours = 0;

/* Permet de mettre � jour les hauteurs de div en fonction de la plus grande */
function majTailleNavig()
{
	if (document.getElementById("listeArticles") != null
			&& document.getElementById("navigationGroupe") != null)
	{
		// On remet au minimum les tailles de DIV
		if (document.getElementById("listeArticles") != null) document
				.getElementById("listeArticles").style.height = "auto";
		if (document.getElementById("navigationGroupe") != null) document
				.getElementById("navigationGroupe").style.height = "auto";
		
		// Si le navigateur interpr�te ce mode
		if (document.getElementById("contenu").offsetHeight)
		{
			// maj du plus petit
			if (document.getElementById("listeArticles").offsetHeight > document
					.getElementById("navigationGroupe").offsetHeight) document
					.getElementById("navigationGroupe").style.height = (document
					.getElementById("listeArticles").offsetHeight - 30)
					+ "px";
			else if (document.getElementById("listeArticles").offsetHeight < document
					.getElementById("navigationGroupe").offsetHeight) document
					.getElementById("listeArticles").style.height = (document
					.getElementById("navigationGroupe").offsetHeight - 70)
					+ "px";
		}
		// Ou celui-l�
		else if (document.getElementById("listeArticles").style.pixelHeight)
		{
			// maj du plus petit
			if (document.getElementById("listeArticles").style.pixelHeight > document
					.getElementById("navigationGroupe").style.pixelHeight) document
					.getElementById("navigationGroupe").style.height = (document
					.getElementById("listeArticles").style.pixelHeight - 30)
					+ "px";
			else if (document.getElementById("listeArticles").style.pixelHeight < document
					.getElementById("navigationGroupe").style.pixelHeight) document
					.getElementById("listeArticles").style.height = (document
					.getElementById("navigationGroupe").offsetHeight - 70)
					+ "px";
		}
	}
	// Pour la fiche article il faut faire gaffe à la hauteur de tous les
	// composants
	else if (document.getElementById("detailArticle") != null)
	{
		document.getElementById("detailArticle").style.height = "auto";

		// Si on est plus en mode absolu
		if (document.getElementById("blocDroit").offsetTop > 200) { return; }

		var htGauche = 0;
		var htDroite = 0;

		// Navigateurs standards
		if (document.getElementById("contenu").offsetHeight)
		{

			if (document.getElementById("messagePrincipal") != null)
			{
				htGauche += document.getElementById("messagePrincipal").offsetHeight + 40;
				htDroite += document.getElementById("messagePrincipal").offsetHeight + 40;
			}
			
			if (document.getElementById("memoFicheArticle") != null)
			{
				htGauche += document.getElementById("memoFicheArticle").offsetHeight + 10;
				htDroite += document.getElementById("memoFicheArticle").offsetHeight + 10;
			}

			htGauche += document.getElementById("fi_details").offsetHeight;
			htDroite += document.getElementById("fi_photo").offsetHeight;

			if (document.getElementById("fi_fourniss") != null)
			{
				htGauche += document.getElementById("fi_fourniss").offsetHeight;
			}

			if (document.getElementById("fi_stock") != null)
			{
				htGauche += document.getElementById("fi_stock").offsetHeight;
			}

			htDroite += document.getElementById("fi_actions").offsetHeight;
		}
		// Autres navigateurs
		else if (document.getElementById("contenu").style.pixelHeight)
		{
			if (document.getElementById("messagePrincipal") != null)
			{
				htGauche += document.getElementById("messagePrincipal").style.pixelHeight + 40;
				htDroite += document.getElementById("messagePrincipal").style.pixelHeight + 40;
			}
			htGauche += document.getElementById("fi_details").style.pixelHeight;
			htDroite += document.getElementById("fi_photo").style.pixelHeight;

			if (document.getElementById("fi_fourniss") != null)
			{
				htGauche += document.getElementById("fi_fourniss").style.pixelHeight;
			}

			if (document.getElementById("fi_stock") != null)
			{
				htGauche += document.getElementById("fi_stock").style.pixelHeight;
			}

			htDroite += document.getElementById("fi_actions").style.pixelHeight;
		}

		// on retaille comme des sauvages
		if (htDroite > htGauche)
		{
			htDroite += 90;
			document.getElementById("blocFiche").style.height = htDroite + "px";

			if (document.getElementById("fi_variantes") != null)
			{
				htDroite += document.getElementById("fi_variantes").offsetHeight + 50;
			}
			document.getElementById("detailArticle").style.height = htDroite
					+ "px";
		}
		else
		{
			htGauche += 110;
			document.getElementById("blocFiche").style.height = htGauche + "px";

			if (document.getElementById("fi_variantes") != null)
			{
				htGauche += document.getElementById("fi_variantes").offsetHeight + 50;
			}

			document.getElementById("detailArticle").style.height = htGauche
					+ "px";
		}
	}
}

/* ouvrir les bonnes sous familles en fonction de la famille sélectionnée */
function ouvrirSousFamilles(idFamille, sousFamille, maj)
{
	liste = document.getElementsByClassName("classeSousFamille");
	var idElement = "";
	// le lien sous famille
	for (var i = 0; i < liste.length; i++)
	{
		idElement = liste[i].id;

		if (idElement.indexOf("rappel" + idFamille) >= 0)
		{
			if (liste[i].style.display == "inline-block") liste[i].style.display = "none";
			else liste[i].style.display = "inline-block";

			if (sousFamille != null)
			{
				if (liste[i].getAttribute("id") == sousFamille + "rappel"
						+ idFamille) liste[i].style.background = "#f1ede9";
				else liste[i].style.background = "none";
			}
		}
		else
		{
			liste[i].style.display = "none";
		}
	}

	// l'accordeon
	liste = document.getElementsByClassName("accordeonSousFamille");

	// le lien sous famille
	for (var i = 0; i < liste.length; i++)
	{
		idElement = liste[i].id;

		if (idElement.indexOf("rappel" + idFamille) >= 0)
		{
			if (liste[i].style.display == "inline-block") liste[i].style.display = "none";
			else liste[i].style.display = "inline-block";
		}
		else
		{
			liste[i].style.display = "none";
		}
	}

	// mise en �vidence de la famille
	if (sousFamille == null)
	{
		liste = document.getElementsByClassName("familleArticle");
		for (var i = 0; i < liste.length; i++)
		{
			idElement = liste[i].id;

			if (idElement.indexOf(idFamille) >= 0) liste[i].style.background = "#f1ede9";
			else liste[i].style.background = "none";
		}
	}

	if (maj) majTailleNavig();
}

/**
 *  ouvre dynamiquement une famille */
function majOuvertureFamilles(groupe, familleBrute)
{
	var sousFamille = null;
	var famille = null;

	if (familleBrute != null)
	{
		if (familleBrute.length == 5 && familleBrute.substring(0, 2) == "FA") famille = "Famille"
				+ familleBrute.substring(2, 5);
		else if (familleBrute.length == 7
				&& familleBrute.substring(0, 2) == "SF")
		{
			famille = "Famille" + familleBrute.substring(2, 5);
			sousFamille = "sf" + familleBrute.substring(2, 7);
		}
	}

	if (famille != null) ouvrirSousFamilles(famille, sousFamille, false);
}

/**
 * permet de g�rer les boutons d'incr�ments de quantit� d'articles
 */
function majQuantiteZone(idZone, incr, cnd)
{
	var init = document.getElementById(idZone).value;
	var total = 0;
	var increment = Number(incr);
	var conditionnement = Number(cnd);

	if (conditionnement != 1) increment = Number(increment) * Number(cnd);

	if (init == null || isNaN(init))
	{
		init = 0;
		if (increment == -1) total = 0;
		else total = 1;
	}
	else total = Number(init) + Number(increment);

	if (total < Number(cnd) && total != 0) total = Number(cnd);

	if (total < 0) total = 0;

	document.getElementById(idZone).value = total;
}

/**
 * permet de g�rer les boutons d'incr�ments de quantit� d'articles avec le
 * conditionnement global
 */
function majQuantiteGlobal(idZone, incr)
{
	majQuantiteZone(idZone, incr, conditionnementEnCours);
}

/**
 * permet de modifier la quantit� selon le conditionnement
 */
function majCnd(idZone,cnd,decimales)
{
	var init = document.getElementById(idZone).value;
	//Les virgules en point
	var valeurPoint = init.replace(/,/g,'.');
	var total = 0;
	var conditionnement = Number(cnd);
	var rst = 0;
	var quot = 0;

	total = Number(valeurPoint);

	if (conditionnement > 1)
	{
		if (valeurPoint < Number(cnd))
		{
			total = Number(cnd);
		}

		rst = total % Number(cnd);
		if (rst != 0)
		{
			quot = total / Number(cnd);
			total = (parseInt(quot) + 1) * Number(cnd);
		}
	}

	if (total < 0) total = 0;
	
	//Formatage décimales du total
	if(total!=0)
		document.getElementById(idZone).value = total.toFixed(decimales);
	else 
		document.getElementById(idZone).value = "";
}

/**
 * permet de modifier la quantit� selon le conditionnement sans modif de décimales pour transmission au panier
 * Va falloir feire une seule méthode propre
 */
function majCndPanier(idZone,cnd)
{
	var init = document.getElementById(idZone).value;
	//Les virgules en point
	var valeurPoint = init.replace(/,/g,'.');
	var total = 0;
	var conditionnement = Number(cnd);
	var rst = 0;
	var quot = 0;

	total = Number(valeurPoint);

	if (conditionnement > 1)
	{
		if (valeurPoint < Number(cnd))
		{
			total = Number(cnd);
		}

		rst = total % Number(cnd);
		if (rst != 0)
		{
			quot = total / Number(cnd);
			total = (parseInt(quot) + 1) * Number(cnd);
		}
	}

	if (total < 0) total = 0;
	
	//Formatage décimales du total
	if(total!=0)
		document.getElementById(idZone).value = total;
	else 
		document.getElementById(idZone).value = "";
}

/**
 * permet de modifier la quantit� d'une zone selon le conditionnement global
 */
function majCndGlobal(idZone)
{
	majCnd(idZone, conditionnementEnCours,decimalesEnCours);
}

/**
 * Contr�ler que la saisie
 */
function only_numeric(e)
{
	var key = window.event ? e.keyCode : e.which;

	// autoriser les touches de suppr et de retour
	if (key != 0 && key != 8)
	{
		var valeur = String.fromCharCode(key);
		var regex = new RegExp("^[0-9]*$", "g");
		if (!regex.test(valeur))
		{
			e.returnValue = false;
			if (e.preventDefault) e.preventDefault();
		}
	}
}

zoneInitiale ="";

/**
 * Si la saisie quantité n'est pas numérique ou ,/. on annule 
 * */
function recupererZoneInitiale(e,idZone)
{
	var key = window.event ? e.keyCode : e.which;

	// autoriser les touches de suppr et de retour
	if (key != 0 && key != 8)
	{
		var valeur = String.fromCharCode(key);
		var regex = new RegExp("^([0-9]|[,.])?$", "g");
		if (!regex.test(valeur))
		{
			e.returnValue = false;
			if (e.preventDefault) e.preventDefault();
		}
	}
	
	if(key==9 || key==46)
		zoneInitiale = "";
		
	zoneInitiale = document.getElementById(idZone).value;
}

/**
 * Controle quantite saisie globale (cad panier)
 * */
function controle_quantiteDecimalesGlob(e, idZone)
{
	//TODO modifier nbFinal
	controle_quantiteDecimales(e,idZone,decimalesEnCours,7);
}

/**
 * Si la saisie quantité n'est pas conforme aux entiers/décimales prévus 
 * */
function controle_quantiteDecimales(e,idZone,decimales,nbFinal)
{
	var Valeurinit = document.getElementById(idZone).value;
	var key = window.event ? e.keyCode : e.which;
	var entier = nbFinal-decimales-1;
	var chaineRegEx = "^[0-9]{1," + entier + "}[,.]?[0-9]{0," + decimales + "}$";
		var regexChamp = new RegExp(chaineRegEx, "g");
		if (key != 0 && key != 8 && key!=9 && key!=46)
		{
			if(!regexChamp.test(Valeurinit))
			{
				document.getElementById(idZone).value = zoneInitiale;
			}
			else
			{
				var chaineRegEx2 = "^[0-9]{" + entier + "," + (entier+1) + "}$";
				var regexChamp2 = new RegExp(chaineRegEx2, "g");
				//dans le cas précis ou on dépasse le nb d'entier
				if(regexChamp2.test(Valeurinit))
				{
					document.getElementById(idZone).value = Valeurinit + "," ;
				}
			}
		}
}

/**
 * Cette methode permet d'ajouter dynamiquement un article au panier
 */
function ajoutPanier(qteForcee,zoneAffichage, etb, id, tarif, zoneSaisie, refFourn, conditionnement, unite, montrePopUp)
{
	majCndPanier(zoneSaisie, conditionnement);
	
	var okCestFait = false;
	var quantite = 1;
	var isLot = "";
	if(qteForcee!=null && qteForcee>0)
	{
		quantite = qteForcee;
		isLot = "&isLot=1";
	}
	else
	{
		quantite = document.getElementById(zoneSaisie).value.trim();
		isLot = "";
	} 
	
	if (quantite == null || quantite == "" || quantite == "0") quantite = "1";

	var url = "catalogue?etbArticle= " + etb + "&panierArticle=" + id
			+ "&tarifArticle=" + tarif + "&panierQuantite=" + quantite
			+ "&refFournisseur=" + refFourn + "&conditionnement="
			+ conditionnement + "&uniteVente=" + unite + isLot;
	
	if (window.XMLHttpRequest)
	{
		requete = new XMLHttpRequest();
		if (requete)
		{
			requete.open("POST", url, true);

			if (montrePopUp) requete.onreadystatechange = majIHM;
			requete.send();
			document.getElementById(zoneSaisie).value = "";
			
			okCestFait = true;
		}
	}
	else if (window.ActiveXObject)
	{
		requete = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete)
		{
			requete.open("POST", url, true);
			if (montrePopUp) requete.onreadystatechange = majIHM;
			requete.send();
			document.getElementById(zoneSaisie).value = "";
			okCestFait = true;
		}
		else alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	}
	else
	{
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}
	
	if (okCestFait && montrePopUp) afficherPopUpAjout(zoneAffichage, zoneSaisie);
}

/**
 *  Cette méthode permet d'ajouter dynamiquement un article au panier intuitif */
function ajouterPanierIntuitif(id) 
{
	var okCestFait = false;
	var montrePopUp = true;
	var zoneAffichage = "";
	
	var url = "catalogue?intuitif=1&codeArticle=" + id;
	
	if (window.XMLHttpRequest) {
		requete2 = new XMLHttpRequest();
		if (requete2) {
			requete2.open("POST", url, true);
			
			if(montrePopUp)
				requete2.onreadystatechange = majIHM2;
			requete2.send();
			
			okCestFait = true;
		}
	} else if (window.ActiveXObject) {
		requete2 = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete2) {
			requete2.open("POST", url, true);
			if(montrePopUp)
				requete2.onreadystatechange = majIHM2;
			requete2.send();
	
			okCestFait = true;
		} else
			alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	} else {
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}

	if (okCestFait && montrePopUp)
		afficherPopUpAjout(zoneAffichage, null);
}

/* Ajouter tous les articles sélectionnés au panier */
function ajouterToutAuPanier(nbElements)
{
	var element = null;
	var contenu = "";
	var saisie = null;
	var tab = null;

	var A1ART = null;
	var A1ETB = null;
	var zoneSaisie = null;
	var CND = null;
	var CAREF = null;
	var UNITE = null;
	var TARIF = null;
	var nbArticles = 0;
	var indiceNbArticles = 0;
	var onMontreLaPop = false;

	// PAS BO A MODIFIER VIIIITE
	for (var i = 0; i < nbElements; i++)
	{
		saisie = "saisieQ" + i;
		if (document.getElementById(saisie) != null
				&& document.getElementById(saisie).value != ""
				&& document.getElementById(saisie).value != "0") nbArticles++;
	}
	// PAS BO A MODIFIER VIIIITE
	for (var i = 0; i < nbElements; i++)
	{
		saisie = "saisieQ" + i;
		if (document.getElementById(saisie) != null
				&& document.getElementById(saisie).value != ""
				&& document.getElementById(saisie).value != "0")
		{
			indiceNbArticles++;
			onMontreLaPop = (indiceNbArticles == nbArticles);
			element = document.getElementById(i).value;
			tab = element.split("$$$");

			if (tab.length == 7)
			{
				A1ETB = tab[0];
				A1ART = tab[1];
				TARIF = tab[2];
				CAREF = tab[3];
				CND = tab[4];
				UNITE = tab[5];
				zoneSaisie = tab[6];
				ajoutPanier(0,A1ART, A1ETB, A1ART, TARIF, zoneSaisie, CAREF, CND,
						UNITE, onMontreLaPop);
			}
		}
	}

	if (element != null)
	{
		// si on est en mode repr�sentant bloquer les modifs du client
		if (document.getElementById("surBlocClient") != null) document
				.getElementById("surBlocClient").innerHTML = "<div id='blocrepresentant'>Vous ne pouvez pas changer de client tant que le panier n'est pas validé ou supprimé</div>";
	}
}

/**
 * rajouter un filtre à la liste des articles sélectionnés Suivre l'URL passée
 * en paramètre
 */
function rajouterUnFiltreURL(url)
{
	if (url == null) return;

	if (document.getElementById("stockPositif") != null)
	{
		if (document.getElementById("stockPositif").checked)
		{
			url = url + "&stockPositif=1";
		}
		else
		{
			url = url + "&stockPositif=0";
		}
	}

	if (document.getElementById("choixFournisseur") != null)
	{
		select = document.getElementById("choixFournisseur");
		choice = select.selectedIndex;
		valeur_cherchee = select.options[choice].value;

		if (valeur_cherchee != null)
		{
			url = url + "&filtreFrs=" + valeur_cherchee;
		}
	}

	document.location.href = url;
}

function set_opacity(id)
{
	var valeurEffacement = 5;
	element = document.getElementById(id);
	
	if (opaciteGlob >= valeurEffacement) opaciteGlob += -valeurEffacement;
	else
	{
		opaciteGlob = 0;
		clearInterval(timer);
		timer = null;
	}

	if (element != null && opaciteGlob <= 100)
	{
		element.style["-moz-opacity"] = opaciteGlob / 100;
		element.style["-khtml-opacity"] = opaciteGlob / 100;
		element.style["opacity"] = opaciteGlob / 100;
	}
	else if (opaciteGlob == (180 - valeurEffacement))
	{
		element.style["-moz-opacity"] = 0.9;
		element.style["-khtml-opacity"] = 0.9;
		element.style["opacity"] = 0.9;
	}

	return true;
}

/**
 * afficher la pop up qui ajoute un article au panier
 * */
function afficherPopUpAjout(id, idZoneReplacement)
{
	opaciteGlob = 180;

	var element = document.getElementById("popUpAjout"); 
	
	if (document.getElementById("popUpAjout") != null)
	{
		if (id != null) document.getElementById("popUpAjout").innerHTML = "Article "
				+ id + " ajout&eacute; au panier";
		else document.getElementById("popUpAjout").innerHTML = "Articles ajout&eacute;s au panier";

		var larg = 0;
		var haut = 0;

		if (document.body)
		{
			larg = (document.body.clientWidth);
			haut = (document.body.clientHeight);
		}
		else
		{
			larg = (window.innerWidth);
			haut = (window.innerHeight);
		}

		document.getElementById("popUpAjout").style.left = (larg / 2) - 130
				+ "px";
		document.getElementById("popUpAjout").style.top = (haut / 3) + "px";
		document.getElementById("popUpAjout").style.display = "block";
		
		timer = setInterval("set_opacity(\"popUpAjout\")", 60);
	}
}

/**
 * Cette méthode permet d'ouvrir coté client la gestion des filtres de la liste
 * des articles
 */
function gererOuvertureFiltres()
{
	var contenu = document.getElementById("contenuFiltres");
	var elementC = document.getElementById("filtreArticles");

	if (contenu != null)
	{
		if (contenu.style.display == "block")
		{
			contenu.style.display = "none";
			if (elementC != null) elementC.style.marginBottom = "8px";
		}
		else
		{
			contenu.style.display = "block";
			if (elementC != null) elementC.style.marginBottom = "0px";
		}
	}

	majTailleNavig();
}

/**
 * Mise a jour de l'interface graphique avec AJAX
 */
function majIHM()
{
	if (requete.readyState == 4)
	{
		if (requete.status == 200 || requete.status == 0)
		{
			document.getElementById("monPanier").innerHTML = requete.responseText;
			if (document.getElementById("surBlocClient") != null) document
					.getElementById("surBlocClient").innerHTML = "<div id='blocrepresentant'>Vous ne pouvez pas changer de client tant que le panier n'est pas valid&eacute; ou supprim&eacute;</div>";
		}
		else alert('Une erreur est survenue lors de la mise à jour de la page.'
				+ '\n\nCode retour = ' + requete.statusText);
	}
}

/**
 * Mise à jour de l'interface graphique avec AJAX pour les paniers intuitifs
 */
function majIHM2() 
{
	if (requete2.readyState == 4) 
	{
		if (requete2.status == 200 || requete2.status == 0)
		{
			document.getElementById("monPanier").innerHTML = requete2.responseText;
		}
		else
			alert('Une erreur est survenue lors de la mise à jour de la page.'
					+ '\n\nCode retour = ' + requete2.statusText);
	}
}

/**
 * permet d'ouvrir la pop-up g�n�rale
 */
function ouvrirPopUpGenerale()
{
	document.getElementById("filtreTotal").style.display = "block";
	document.getElementById("popUpGenerale").style.display = "block";

	repositionnerPopUp(document.getElementById("popUpGenerale"));
}

/**
 * Appeller la pop up de traitement en cours sp�cial mode de recup�ration
 */
function traitementStockDispo(url)
{
	if (document.getElementById("filtreTotal") != null) document
			.getElementById("popUpGenerale").style.display = "none";
	if (document.getElementById("filtreTotal") != null) document
			.getElementById("popUpGenerale").style.display = "none";
	// traitement g�n�ral dans general.js
	traitementEnCours(url);
}

function gererOuvertureStocks()
{
	if (document.getElementById("magasinsPourStocks").style.display == "block") document
			.getElementById("magasinsPourStocks").style.display = "none";
	else document.getElementById("magasinsPourStocks").style.display = "block";
}

/**
 * En mode gestion des lots on va comparer la quantité saisie par l'utilisateur 
 * à la quantité du lot maximum
 * pour savoir si on ouvre la pop up de gestion des lots
 * */
function controlerLots(lots,id,lotM,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement,libelleArticle, unite, nbDecimales, libUnite, montrePopUp)
{
	if(id==null || lotM==null || lots==null)
	{
		return;
	}
	var idSaisie = document.getElementById(id);
	var quantite = 1;
	var lotMax = 0;
	lotMax = lotM;
	if(idSaisie!=null && idSaisie.value!="" && idSaisie.value!="0")
	{
		quantite = idSaisie.value;
	}
	
	if(quantite>lotMax)
	{
		ouvrirPopGestionLots(zoneAffichage,lots,id,lotM,etb,idArt,tarif,refFourn,libelleArticle,conditionnement,unite,nbDecimales,libUnite);
	}
	else
	{
		ajoutPanier(0,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, montrePopUp);
	}
}

/**
 * permet d'ouvrir la pop-up 
 */
function ouvrirPopGestionLots(zoneAffichage,lots,id,lotM,etb,idArt,tarif,refFourn,libelle,conditionnement,unite,nbDecimales,libUnite)
{
	var contenu = "lots: " + lots + " lotM:" + lotM + " etb: " + etb + " idArt: " + idArt;
	var longueur = document.getElementById(id).value;
	document.getElementById("refArticle").innerHTML = "<span class='paddingInterne'>" + idArt + "</span>";
	document.getElementById("libArticle").innerHTML = "<span class='paddingInterne'>" + libelle + "</span>";
	var liste = null;
	var texteLots = "";
	if(lots!=null)
	{
		liste = lots.split(",");
	}
	
	if(liste!=null)
	{
		for (var i = 0; i < liste.length; i++)
		{
			/*var texte = "";
			var sousTableau = liste[i].split(".");
			if(sousTableau!=null)
			{
				texte = sousTableau[0];
			}
			else
			{
				texte = liste[i];
			}*/
			texteLots += "<div class='uneDispoLot'>- " + liste[i] + " " + libUnite + "</div>";
		}
	}
	document.getElementById("disposPopUpLot").innerHTML = texteLots;
	document.getElementById("longueurSouhaitee").innerHTML = "<span class='paddingInterne'>" + longueur  + "</span>";
	document.getElementById("uniteDecoupe").innerHTML = libUnite ;
	document.getElementById("nomZone").innerHTML = id ;
	
	var nbFinalQ = 5;
	if (nbDecimales > 0) {
        nbFinalQ = nbFinalQ + nbDecimales - 1;
	}
	
	var contenuZonesSaisie = 
		  "<input type ='text' class='zoneDecoupe' id='longueurSouhaitee1' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'longueurSouhaitee1" 
        + "')\" onkeyup=\"controle_quantiteDecimales(event,'longueurSouhaitee1'," + nbDecimales + "," + nbFinalQ + "); afficherDecoupes();\" onChange=\"majCnd('longueurSouhaitee1','" + conditionnement + "'," + nbDecimales + ");\">" 
        + "<input type ='text' class='zoneDecoupe' id='longueurSouhaitee2' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'longueurSouhaitee2" 
        + "')\" onkeyup=\"controle_quantiteDecimales(event,'longueurSouhaitee2'," + nbDecimales + "," + nbFinalQ + "); afficherDecoupes();\" onChange=\"majCnd('longueurSouhaitee2','" + conditionnement + "'," + nbDecimales + ");\">"
        + "<input type ='text' class='zoneDecoupe' id='longueurSouhaitee3' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'longueurSouhaitee3" 
        + "')\" onkeyup=\"controle_quantiteDecimales(event,'longueurSouhaitee3'," + nbDecimales + "," + nbFinalQ + "); afficherDecoupes();\" onChange=\"majCnd('longueurSouhaitee3','" + conditionnement + "'," + nbDecimales + ");\">" 
        + "<input type ='text' class='zoneDecoupe' id='longueurSouhaitee4' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'longueurSouhaitee4" 
        + "')\" onkeyup=\"controle_quantiteDecimales(event,'longueurSouhaitee4'," + nbDecimales + "," + nbFinalQ + "); afficherDecoupes();\" onChange=\"majCnd('longueurSouhaitee4','" + conditionnement + "'," + nbDecimales + ");\">"
        + "<input type ='text' class='zoneDecoupe' id='longueurSouhaitee5' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'longueurSouhaitee5" 
        + "')\" onkeyup=\"controle_quantiteDecimales(event,'longueurSouhaitee5'," + nbDecimales + "," + nbFinalQ + "); afficherDecoupes();\" onChange=\"majCnd('longueurSouhaitee5','" + conditionnement + "'," + nbDecimales + ");\">" 
        + "<input type ='text' class='zoneDecoupe' id='longueurSouhaitee6' onClick='this.select();' onKeyPress=\"recupererZoneInitiale(event,'longueurSouhaitee6" 
        + "')\" onkeyup=\"controle_quantiteDecimales(event,'longueurSouhaitee6'," + nbDecimales + "," + nbFinalQ + "); afficherDecoupes();\" onChange=\"majCnd('longueurSouhaitee6','" + conditionnement + "'," + nbDecimales + ");\">" 
	document.getElementById("zonesDecoup").innerHTML = contenuZonesSaisie;
	
	var contenuValidation = 
		"<a class='btRecuperation' id='validerDecoupe' href='#' onClick =\"gererDecoupes('" + zoneAffichage + "', '" + etb + "', '" + idArt + "', '" + tarif + "', '" + id + "', '" + refFourn + "', '" + conditionnement + "', '" + unite +"',true);\" >" +
			"<span class='deco_bt' id='deco_bt_valid_Lot'>&nbsp;</span>" +
			"<span class='texte_bt'>Valider</span>" + 
		"</a>";
	document.getElementById("btValidLots").innerHTML = contenuValidation;
	document.getElementById("filtreTotal").style.display = "block";
	document.getElementById("popUpLots").style.display = "block";

	repositionnerPopUp(document.getElementById("popUpLots"));
}

function fermerPopGestionLots()
{
	document.getElementById("filtreTotal").style.display = "none";
	document.getElementById("popUpLots").style.display = "none";
}

/**
 * Permet de gérer les découpes de lot coté client sans faire appel au serveur
 * Si la découpe correspond au total on peut mettre au panier
 * */
function gererDecoupes(zoneAffichage,etb,idArt,tarif,zoneSaisie,refFourn,conditionnement,unite,onValide)
{
	var racine = "longueurSouhaitee";
	var zoneSaisieQ = document.getElementById("nomZone");
	var nomZone = zoneSaisieQ.innerText || zoneSaisieQ.textContent ;
	var total = document.getElementById(nomZone).value;

	var zoneModifiee1 = document.getElementById(racine + 1);
	var zoneModifiee2 = document.getElementById(racine + 2);
	var zoneModifiee3 = document.getElementById(racine + 3);
	var zoneModifiee4 = document.getElementById(racine + 4);
	var zoneModifiee5 = document.getElementById(racine + 5);
	var zoneModifiee6 = document.getElementById(racine + 6);
	
	var valeur1 = 0;
	var valeur2 = 0;
	var valeur3 = 0;
	var valeur4 = 0;
	var valeur5 = 0;
	var valeur6 = 0;
	
	valeur1 = parseFloat(zoneModifiee1.value);
	if(isNaN(valeur1))
	{
		valeur1 = 0;
		zoneModifiee1.value = valeur1;
	}
	valeur2 = parseFloat(zoneModifiee2.value);
	if(isNaN(valeur2))
	{
		valeur2 = 0;
		zoneModifiee2.value = valeur2;
	}
	valeur3 = parseFloat(zoneModifiee3.value);
	if(isNaN(valeur3))
	{
		valeur3 = 0;
		zoneModifiee3.value = valeur3;
	}
	valeur4 = parseFloat(zoneModifiee4.value);
	if(isNaN(valeur4))
	{
		valeur4 = 0;
		zoneModifiee4.value = valeur4;
	}
	valeur5 = parseFloat(zoneModifiee5.value);
	if(isNaN(valeur5))
	{
		valeur5 = 0;
		zoneModifiee5.value = valeur5;
	}
	valeur6 = parseFloat(zoneModifiee6.value);
	if(isNaN(valeur6))
	{
		valeur6 = 0;
		zoneModifiee6.value = valeur6;
	}
	
	var valeurTotale = valeur1 + valeur2 + valeur3 + valeur4 + valeur5 + valeur6;	
	if(valeurTotale < total)
	{
		document.getElementById("validerDecoupe").style.opacity = "0.5";
		return;
	}
	else if(valeurTotale > total)
	{
		document.getElementById("validerDecoupe").style.opacity = "0.5";
		return;
	}
	else
	{
		document.getElementById("validerDecoupe").style.opacity = "1";
		if(onValide)
		{
			var delai = 300;
		
			fermerPopGestionLots();
			if(valeur1>0){
				ajoutPanier(valeur1,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, true);
			}
			if(valeur2>0)
				setTimeout(ajoutPanier(valeur2,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, true),delai);
			if(valeur3>0)
				setTimeout(ajoutPanier(valeur3,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, true),delai);
			if(valeur4>0)
				setTimeout(ajoutPanier(valeur4,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, true),delai);
			if(valeur5>0)
				setTimeout(ajoutPanier(valeur5,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, true),delai);
			if(valeur6>0)
				setTimeout(ajoutPanier(valeur6,zoneAffichage, etb, idArt, tarif, zoneSaisie, refFourn, conditionnement, unite, true),delai);
		}
		else
		{
			return;
		}
	}
}

/**
 * Gérer les zones d'affichage de découpes et le bouton de validation de la découpe
 * */
function afficherDecoupes()
{
	var zoneSaisieQ = document.getElementById("nomZone");
	var nomZone = zoneSaisieQ.innerText || zoneSaisieQ.textContent ;
	var total = document.getElementById(nomZone).value;
	var racine = "longueurSouhaitee";
	var zoneModifiee1 = document.getElementById(racine + 1);
	var zoneModifiee2 = document.getElementById(racine + 2);
	var zoneModifiee3 = document.getElementById(racine + 3);
	var zoneModifiee4 = document.getElementById(racine + 4);
	var zoneModifiee5 = document.getElementById(racine + 5);
	var zoneModifiee6 = document.getElementById(racine + 6);
	
	var valeur1 = 0;
	var valeur2 = 0;
	var valeur3 = 0;
	var valeur4 = 0;
	var valeur5 = 0;
	var valeur6 = 0;
	
	valeur1 = parseFloat(zoneModifiee1.value);
	if(isNaN(valeur1))
	{
		valeur1 = 0;
		zoneModifiee1.value = valeur1;
	}
	valeur2 = parseFloat(zoneModifiee2.value);
	if(isNaN(valeur2))
	{
		valeur2 = 0;
		zoneModifiee2.value = valeur2;
	}
	valeur3 = parseFloat(zoneModifiee3.value);
	if(isNaN(valeur3))
	{
		valeur3 = 0;
		zoneModifiee3.value = valeur3;
	}
	valeur4 = parseFloat(zoneModifiee4.value);
	if(isNaN(valeur4))
	{
		valeur4 = 0;
		zoneModifiee4.value = valeur4;
	}
	valeur5 = parseFloat(zoneModifiee5.value);
	if(isNaN(valeur5))
	{
		valeur5 = 0;
		zoneModifiee5.value = valeur5;
	}
	if(isNaN(valeur6))
	{
		valeur6 = 0;
		zoneModifiee6.value = valeur6;
	}
	
	if(valeur1>0)
	{
		zoneModifiee2.style.display = "inline-block";
	}
	if(valeur2>0)
	{
		zoneModifiee2.style.display = "inline-block";
		zoneModifiee3.style.display = "inline-block";
	}
	if(valeur3>0)
	{
		zoneModifiee3.style.display = "inline-block";
		zoneModifiee4.style.display = "inline-block";
	}
	if(valeur4>0)
	{
		zoneModifiee4.style.display = "inline-block";
		zoneModifiee5.style.display = "inline-block";
	}
	if(valeur5>0)
	{
		zoneModifiee5.style.display = "inline-block";
		zoneModifiee6.style.display = "inline-block";
	}
	if(valeur6>0)
	{
		zoneModifiee6.style.display = "inline-block";
	}
	
	var valeurTotale = valeur1 + valeur2 + valeur3 + valeur4 + valeur5 + valeur6;	
	if(valeurTotale < total)
	{
		document.getElementById("validerDecoupe").style.opacity = "0.5";
		return;
	}
	else if(valeurTotale > total)
	{
		document.getElementById("validerDecoupe").style.opacity = "0.5";
		return;
	}
	else
	{
		document.getElementById("validerDecoupe").style.opacity = "1";
	}
}

function ouvrirPopPanierComplet(condition, referenceAffichee, libelle, tarif,
		unite, libUnite, decimales)
{
	conditionnementEnCours = condition;
	//gestion des décimales
	total = Number(condition);
	decimalesEnCours = decimales;
	document.getElementById("miniCondition").innerHTML = condition;
	document.getElementById("miniReference").innerHTML = referenceAffichee;
	document.getElementById("miniLibelle").innerHTML = libelle;
	document.getElementById("miniTarif").innerHTML = tarif;
	document.getElementById("miniUnite").innerHTML = libUnite;
	document.getElementById("miniqteSaisie").value = total.toFixed(decimales);
	document.getElementById("filtreTotal").style.display = "block";
	document.getElementById("popUpPanier").style.display = "block";

	repositionnerPopUp(document.getElementById("popUpPanier"));
}

function fermerPopPanierComplet()
{
	document.getElementById("filtreTotal").style.display = "none";
	document.getElementById("popUpPanier").style.display = "none";
}

/**
 * On ferme la pop up panier de la fiche et on met au panier
 */
function mettreAuPanierFiche()
{
	fermerPopPanierComplet();
	document.getElementById("onMetAuPanierInvisible").click();
}

/**
 * Cette méthode permet d'ajouter dynamiquement un article au panier
 */
function majFavori(etb, article)
{

	var okCestFait = false;

	var url = "catalogue?favoriETB=" + etb + "&favoriArticle=" + article;

	if (window.XMLHttpRequest)
	{
		requete1 = new XMLHttpRequest();
		if (requete1)
		{
			requete1.open("POST", url, true);
			requete1.onreadystatechange = majIHMParametre;
			requete1.send();
			okCestFait = true;
		}
	}
	else if (window.ActiveXObject)
	{
		requete1 = new ActiveXObject("Microsoft.XMLHTTP");
		if (requete1)
		{
			requete1.open("POST", url, true);
			requete1.onreadystatechange = majIHMParametre;
			requete1.send();
			okCestFait = true;
		}
		else alert('Une erreur est survenue lors de construction objet: ActiveXObject');
	}
	else
	{
		alert("Le navigateur ne supporte pas la technologie Ajax");
	}
}

/**
 * Mise a jour de l'interface graphique avec AJAX
 */
function majIHMParametre()
{
	if (requete1.readyState == 4)
	{
		if (requete1.status == 200 || requete1.status == 0) document
				.getElementById("etatFavori").innerHTML = requete1.responseText;
		else alert('Une erreur est survenue lors de la mise à jour de la page.'
				+ '\n\nCode retour = ' + requete.statusText);
	}
}

/**
 * 
 */
function afficherInfoBulle(id)
{
	document.getElementById(id).style.display = "block";
}

/**
 * 
 */
function fermerInfoBulle(id)
{
	document.getElementById(id).style.display = "none";
}
