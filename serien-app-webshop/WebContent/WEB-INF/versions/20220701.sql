/*========================================================================*/
/* Table : ACCUEILBO UPDATE												*/
/*========================================================================*/

UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Promotions' WHERE BTN_NAME = 'Gestion des promotions';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Utilisateurs' WHERE BTN_NAME = 'Gestion des utilisateurs';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Logs' WHERE BTN_NAME = 'Gestion des logs';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Fichiers' WHERE BTN_NAME = 'Gestion des fichiers';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Informations' WHERE BTN_NAME = 'Gestion informations';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Magasins' WHERE BTN_NAME = 'Gestion magasins';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'CGV' WHERE BTN_NAME = 'Gestion CGV';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Catalogue' WHERE BTN_NAME = 'Gestion catalogue';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Accueil' WHERE BTN_NAME = 'Gestion accueil';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Patchs' WHERE BTN_NAME = 'Gestion des patchs';;;
UPDATE ##CURLIB##.ACCUEILBO SET BTN_NAME = 'Accès back-office' WHERE BTN_NAME = 'Gestion des accès du BO';;;
DELETE FROM ##CURLIB##.ACCUEILBO WHERE BTN_LINK = 'securiteBO';;;