CREATE table ##CURLIB##.BLOCAGE_IP               
(                                              
   BL_ID INTEGER GENERATED ALWAYS AS IDENTITY, 
   BL_ADR CHARACTER(250) not null,             
   BL_NB INTEGER not null,                     
                                               
   PRIMARY KEY(BL_ID)                          
);;;                                              



