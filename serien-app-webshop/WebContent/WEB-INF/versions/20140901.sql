INSERT INTO ##CURLIB##.AFFICHAGE VALUES ('AFFICHE_PAS_CODE','Affiche des articles sans code','1',' ','1');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('FILTRE_STOCK', '0', 'DONNEES','2','*TP','');;;
INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$infoRupture','Cette r�f�rence est en rupture de stock. Voir l''article de remplacement : ');;;
INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$infoAiguillage','Cette r�f�rence est aiguill�e vers un autre article : ');;;
INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$infoEquivalent','Cette r�f�rence poss�de un article �quivalent : ');;;
INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$infoRemplacement','Cette r�f�rence a �t� remplac�e par l''article suivant : ');;;
INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','$$validiteDepassee','La date de validit� de ce devis est d�pass�e.');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('VOIR_SUB_RUPT', '1', 'DONNEES','2','*TP','');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('VOIR_SUB_AIGU', '1', 'DONNEES','2','*TP','');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('VOIR_SUB_REMP', '1', 'DONNEES','2','*TP','');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('VOIR_SUB_EQUI', '1', 'DONNEES','2','*TP','');;;
ALTER TABLE ##CURLIB##.USERW ADD US_DATC INTEGER;;;
ALTER TABLE ##CURLIB##.USERW ADD US_DATM INTEGER;;;
UPDATE ##CURLIB##.USERW SET US_DATC = 1170724;;; 
UPDATE ##CURLIB##.USERW SET US_DATM = 1170724;;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('MAIL_INSCRIPTION', 'noreply@resolution-informatique.com', 'DONNEES','2','*TP','');;;
ALTER TABLE ##CURLIB##.MAGASINS ADD MG_MAIL_IN varchar (500);;;
UPDATE ##CURLIB##.USERW SET US_ACCES = 1 WHERE US_ACCES = 10;;;
UPDATE ##CURLIB##.MENU SET US_ACC = 1 WHERE US_ACC = 10;;;
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Pieds de page','footerBO','40','50');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('VOIR_SUB_VARIAN', '1', 'DONNEES','2','*TP','');;;
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('LIMITE_VARIANTE', '10', 'DONNEES','2','*TP','');;;
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Panier intuitif','panierIntuitifBO','40','50');;;

CREATE TABLE ##CURLIB##.INTUITIF
(
   IN_ID INTEGER GENERATED ALWAYS AS IDENTITY,
   IN_BIB CHARACTER (5) not null,
   IN_ETB CHARACTER (3) not null,
   IN_LIBELLE VARCHAR (250),
   IN_STATUT INTEGER not null,
   IN_CREA INTEGER,
   IN_MODIF INTEGER,
   
   PRIMARY KEY(IN_ID)	
);;;

CREATE TABLE ##CURLIB##.ART_INTUIT
(
   ART_IN_ID INTEGER not null,
   ART_CODE VARCHAR(30) not null,
   ART_ORDRE INTEGER
);;;