DROP TABLE ##CURLIB##.ARTI_PAN;;;

CREATE table ##CURLIB##.ARTI_PAN
(
	ART_PAN integer not null,
	ART_ID character (30) not null,
	ART_REF character (30),
	ART_ETB character(3) not null,
	ART_QTE character (9),
	ART_TAR character (11),
	ART_CDT SMALLINT,
	ART_UNI character(2)
);;;

