ALTER TABLE ##CURLIB##.PATCHSW ADD PA_APP integer;;;
ALTER TABLE ##CURLIB##.PATCHSW ADD PA_SQL CLOB(32000) allocate(0);;;
ALTER TABLE ##CURLIB##.PATCHSW ADD PA_DEV varchar (255);;;

DROP TABLE ##CURLIB##.PANIER;;;

CREATE table ##CURLIB##.PANIER
(
	PA_ID integer GENERATED ALWAYS AS IDENTITY,
	PA_US integer not null, 
	PA_ETA smallint not null,
	PA_DTC character (7) not null,
	PA_MOD smallint not null,
	PA_DTS character (7) not null,
	PA_MAG character (2),
	PA_NOM character (30),
	PA_CPL character (30),
	PA_RUE character (30),
	PA_LOC character (30),
	PA_VIL character (30),
	PA_CPO varchar(5),
	PA_INF varchar (1000), 
	PA_SESS varchar(50),
	primary key (PA_ID)
);;;

CREATE INDEX ##CURLIB##.in_pa_et ON ##CURLIB##.PANIER (PA_ETA);;;

DROP TABLE ##CURLIB##.ARTI_PAN;;;

CREATE table ##CURLIB##.ARTI_PAN
(
	ART_PAN integer not null,
	ART_ID character (30) not null,
	ART_REF character (30),
	ART_ETB character(3) not null,
	ART_QTE smallint not null,
	ART_TAR character (11),
	ART_CDT SMALLINT,
	primary key (ART_PAN,ART_ID)
);;;

CREATE table ##CURLIB##.DELAI_MG
(
	DEL_MG1 integer not null,
	DEL_MG2 integer not null,
	DEL_NBJ smallint not null,
	primary key (DEL_MG1,DEL_MG2)
);;;

INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('DECIMALES_CLIENT','2', 'DONNEES','1');;;

INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('DECIMALES_A_VOIR','2', 'DONNEES','1');;;


