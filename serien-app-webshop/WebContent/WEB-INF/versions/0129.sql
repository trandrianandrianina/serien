INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('ZONE_CONDITIONNEMENT','A1CND', 'MAJUSCULE','1');;;

INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('ZONE_REFERENCE_ARTICLE','CAREF', 'MAJUSCULE','1');;;


ALTER TABLE ##CURLIB##.ARTI_PAN ADD COLUMN ART_UNI character (2);;;

INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('MarbreEnvironnement.java','Mise en place du place d''une zone de conditionnement param�trable: En effet A1CND peut-�tre A1QT1 dans d''autres �tablissements.
MarbreEnvironnement.ZONE_CONDITIONNEMENT','1150318','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('GestionCatalogue.java','Mise en place d''une variable MAGASIN_SIEGE dans MarbreEnvironnement uniquement pour les entreprises qui n''en ont pas dans S�rie M','1150320','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('Panier.java','Correctif de bug: Magasins inexistants dans la reprise du panier. Du coup on chope le magasin de S�rie M et non celui de la table MAGASINS','1150330','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_VISI,PA_DEV,PA_APP,PA_SQL) VALUES ('Catalogue.java','Correctifs li�s aux articles, sous familles et familles vides. On scanne correctement cet ensemble. A optimiser dans une table interm�diaire pour all�ger tout le bouzin.','1150330','0','1','2','1','');;;
