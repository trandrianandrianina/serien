

/*==============================================================*/
/* Table : USERW                                                 */
/*==============================================================*/

CREATE table ##CURLIB##.USERW
(
   US_ID INTEGER GENERATED ALWAYS AS IDENTITY,
   US_LOGIN CHARACTER(255) not null,
   US_PASSW CHARACTER(255),
   US_ACCES SMALLINT not null,
   US_SERIEM CHARACTER(15) not null,
   US_ETB CHARACTER(3) not null,		
   PRIMARY KEY(US_ID)	
);;;

CREATE INDEX ##CURLIB##.in_us_acc ON ##CURLIB##.USERW (US_ACCES);;;
CREATE INDEX ##CURLIB##.in_us_seri ON ##CURLIB##.USERW (US_SERIEM);;;
CREATE INDEX ##CURLIB##.in_us_etb ON ##CURLIB##.USERW (US_ETB);;;

INSERT INTO ##CURLIB##.USERW (US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB) VALUES ( 'INVITE',NULL,'10','','');;;
INSERT INTO ##CURLIB##.USERW (US_LOGIN,US_PASSW,US_ACCES,US_SERIEM,US_ETB) VALUES ( 'SERIEMAD',NULL,'40','','');;;

/*==============================================================*/
/* Table : LOGSW                                                 */
/*==============================================================*/

CREATE table ##CURLIB##.LOGSW
(
   LO_ID INTEGER GENERATED ALWAYS AS IDENTITY,
   LO_USER INTEGER,
   LO_SESS CHARACTER(255) not null,
   LO_TYPE CHARACTER(1),
   LO_MESS CHARACTER(255) not null,
   LO_CLASS CHARACTER(60) not null,
   LO_DATE INTEGER not null,
   LO_HEURE INTEGER not null,
  /* FOREIGN KEY (LO_USER) REFERENCES ##CURLIB##.USERW(US_ID),*/
   
   PRIMARY KEY(LO_ID)	
);;;

CREATE INDEX ##CURLIB##.in_lo_user ON ##CURLIB##.LOGSW (LO_USER);;;
CREATE INDEX ##CURLIB##.in_lo_sess ON ##CURLIB##.LOGSW (LO_SESS);;;
CREATE INDEX ##CURLIB##.in_lo_clas ON ##CURLIB##.LOGSW (LO_CLASS);;;
CREATE INDEX ##CURLIB##.in_lo_date ON ##CURLIB##.LOGSW (LO_DATE);;;
CREATE INDEX ##CURLIB##.in_lo_type ON ##CURLIB##.LOGSW (LO_TYPE);;;


/*==============================================================*/
/* Table : ENVIRONM                                                 */
/*==============================================================*/

CREATE table ##CURLIB##.ENVIRONM
(
	EN_CLE CHARACTER(30),
	EN_VAL CHARACTER(255) not null,
	EN_TYP CHARACTER (10) not null,
	EN_GEST CHARACTER (1) not null,
	
	PRIMARY KEY(EN_CLE)
);;;

/*INSERT INTO ##CURLIB##.ENVIRONM VALUES ('PROFIL_ADMIN','SERIEMAD', 'password','1');;;*/

/*==============================================================*/
/* Table : AFFICHAGE                                                 */
/*==============================================================*/

CREATE table ##CURLIB##.AFFICHAGE
(
	EN_CLE CHARACTER(30),
	EN_LIB CHARACTER(50),
	EN_VAL CHARACTER(255) not null,
	EN_TYPE CHARACTER(10),
	EN_GEST CHARACTER(1),
	
	PRIMARY KEY(EN_CLE)
);;;

INSERT INTO ##CURLIB##.AFFICHAGE VALUES ('AFFICHE_CODE_A','Affiche des codes A','1',' ','1');;;

/*==============================================================*/
/* Table : MENU                                                 	*/
/*==============================================================*/

CREATE table ##CURLIB##.MENU
(
	MN_ID INTEGER,
	US_ACC INTEGER,
	MN_NAME CHARACTER(255)not null,
	MN_LINK CHARACTER (255)not null,
	MN_IMG CHARACTER (255)not null,
	MN_ORDRE INTEGER not null,
	MN_DES CHARACTER (255),
	MN_LIM SMALLINT not null,
	
	PRIMARY KEY(MN_ID)
);;;

INSERT INTO ##CURLIB##.MENU(MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES,MN_LIM) VALUES ('1','10','Accueil','accueil','images/decoration/accueil.png','1','Accueil','50');
INSERT INTO ##CURLIB##.MENU(MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES,MN_LIM) VALUES ('2','10','Infos','informations','images/decoration/informations.png','2','Infos','50');
INSERT INTO ##CURLIB##.MENU(MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES,MN_LIM) VALUES ('3','20','Catalogue','catalogue','images/decoration/catalogue.png','3','Catalogue articles','50');
INSERT INTO ##CURLIB##.MENU(MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES,MN_LIM) VALUES ('4','20','Historique','mesdonnees','images/decoration/donnees.png','4','Mes donn�es','20');
INSERT INTO ##CURLIB##.MENU(MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES,MN_LIM) VALUES ('5','30','Gestion','accueilBO','images/decoration/gestion.png','5','Gestion Web Shop','50');
INSERT INTO ##CURLIB##.MENU(MN_ID,US_ACC,MN_NAME,MN_LINK,MN_IMG,MN_ORDRE,MN_DES,MN_LIM) VALUES ('6','10','Connexion','Connexion','images/decoration/connexion.png','3','Connexion client','10');

/*====================================================================*/
/* Table : INFOSW													   */
/*====================================================================*/

CREATE table ##CURLIB##.INFOSW
(
	INF_ID INTEGER GENERATED ALWAYS AS IDENTITY,
	INF_TXT CLOB (32000) allocate(0) not null,
	INF_DES CLOB (32000) allocate(0) not null,
	INF_IMG CHARACTER (255) not null,
	INF_IMG2 CHARACTER (255) not null,
	INF_TXT2 CLOB (32000) allocate(0) not null,
	INF_DES2 CLOB (32000) allocate(0) not null
);;;


/*====================================================================*/
/* Table : MAGASINS													   */
/*====================================================================*/

CREATE table ##CURLIB##.MAGASINS
(
	MG_ID integer GENERATED ALWAYS AS IDENTITY, 
	MG_NAME varchar (1000) not null, 
	MG_ADR varchar (1000) not null, 
	MG_TEL varchar (1000) not null,
	MG_MESS varchar (1000) not null,
	MG_HOR varchar (1000) not null,
	MG_CARTE varchar (1000)not null,
	US_ETB varchar (3) not null, 
	MG_EQUI varchar (1000)not null,
	MG_COD varchar (2)not null,
	
	primary key (MG_ID)
);;;

	
/*========================================================================*/
/* Table : ACCUEILBO													*/
/*========================================================================*/

CREATE table ##CURLIB##.ACCUEILBO
( 
	BTN_ID integer GENERATED ALWAYS AS IDENTITY,
	BTN_NAME character (100) not null,
	BTN_LINK character (255),
	BTN_ACCES character (2),
	BTN_LIM character(2),
	primary key (BTN_ID)
);;;

INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des promotions','marketingBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des utilisateurs','utilisateursBO','40','50');	
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des logs','logsBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des fichiers','BddBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Param�tres du client','clientBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Param�tres d''affichage','affichBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Statistiques','statistiquesBO','30','40');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion informations','informationsBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion magasins','magasinsBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion CGV','cgvBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion catalogue','catalogueBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des images de l''accueil','imagesBO','40','50');
INSERT INTO ##CURLIB##.ACCUEILBO (BTN_NAME,BTN_LINK,BTN_ACCES,BTN_LIM) VALUES ('Gestion des patchs','patchLogsBO','40','50');




/*========================================================================*/
/* Table : CGV			 		 					     	*/
/*========================================================================*/

CREATE table ##CURLIB##.CGV
(
	CGV_ID INTEGER GENERATED ALWAYS AS IDENTITY,
	CGV_NAME CLOB (32000)ALLOCATE(0),
	PRIMARY KEY (CGV_ID)

);;; 

// En erreur
//INSERT INTO ##CURLIB##.CGV (CGV_NAME) VALUES ('TEXTE CONDITIONS GENERALES DE VENTE')

/*========================================================================*/
/* Table : PROMOTION	           										*/
/*========================================================================*/

CREATE table ##CURLIB##.PROMOTION
(
	PR_ID integer GENERATED ALWAYS AS IDENTITY, 
	PR_A1ETB character (3) not null, 
	PR_A1ART character (20) not null,
	PR_A1LIB character (30) not null,  
	PR_TEXTE character (250) not null,
	PR_DATED character (7) not null,
	PR_DATEF character (7) not null,
	PR_PRIX DECIMAL (11, 4) not null,
	
	primary key (PR_ID)
);;;

CREATE INDEX ##CURLIB##.in_pr_dtd ON ##CURLIB##.PROMOTION (PR_DATED);;;
CREATE INDEX ##CURLIB##.in_pr_dtf ON ##CURLIB##.PROMOTION (PR_DATEF);;;

/*========================================================================*/
/* Table : INFOACC	           										*/
/*========================================================================*/

CREATE table ##CURLIB##.INFOACC
(
	ACC_ID integer GENERATED ALWAYS AS IDENTITY,
	ACC_NAME CLOB (10000)ALLOCATE(0) not null,
	PRIMARY KEY (ACC_ID)
);;;

/*CREATE INDEX ##CURLIB##.in_ACC_NAME ON ##CURLIB##.INFOACC (ACC_NAME);;;*/

INSERT INTO ##CURLIB##.INFOACC (ACC_NAME) VALUES ('TEXTE ACCUEIL');;;

/*========================================================================*/
/* Table : PANIER	           										*/
/*========================================================================*/

CREATE table ##CURLIB##.PANIER
(
	PA_ID integer GENERATED ALWAYS AS IDENTITY,
	PA_US integer not null, 
	PA_ETA smallint not null,
	PA_DTC character (7) not null,
	PA_MOD smallint not null,
	PA_DTS character (7) not null,
	PA_MAG integer,
	PA_NOM character (30),
	PA_CPL character (30),
	PA_RUE character (30),
	PA_LOC character (30),
	PA_VIL character (30),
	PA_INF varchar (1000), 

/*	FOREIGN KEY (PA_US) REFERENCES ##CURLIB##.USERW(US_ID),  */
/*	FOREIGN KEY (PA_MAG) REFERENCES ##CURLIB##.MAGASINS(MG_ID),*/
	primary key (PA_ID)
)

CREATE INDEX ##CURLIB##.in_pa_us ON ##CURLIB##.PANIER (PA_US)

/*========================================================================*/
/* Table : ARTI_PAN    lignes d'articles dans le panier					*/
/*========================================================================*/

CREATE table ##CURLIB##.ARTI_PAN
(
	ART_PAN integer not null,
	ART_ID character (30) not null,
	ART_ETB character(3) not null,
	ART_QTE smallint not null,
	
	/*FOREIGN KEY (ART_PAN) REFERENCES ##CURLIB##.PANIER(PA_ID),*/
	primary key (ART_PAN,ART_ID,ART_QTE)
)

/*CREATE INDEX ##CURLIB##.in_art_pa_us ON ##CURLIB##.ARTI_PAN (ART_ID,ART_ETB)*/

           

