CREATE table ##CURLIB##.CATALOGUE
(
   CA_ID INTEGER GENERATED ALWAYS AS IDENTITY,
   CA_BIB CHARACTER(5) not null,
   CA_ETB CHARACTER(3) not null,
   CA_TYP CHARACTER(2) not null,
   CA_IND CHARACTER(6) not null,
   CA_LIB CHARACTER(150) not null,
   
   PRIMARY KEY(CA_ID)	
);;;

CREATE INDEX ##CURLIB##.in_ca_fm ON ##CURLIB##.CATALOGUE (CA_BIB,CA_ETB);;;
CREATE INDEX ##CURLIB##.in_ca_typ ON ##CURLIB##.CATALOGUE (CA_TYP);;;

CREATE table ##CURLIB##.FOURNISS
(
   FO_ID INTEGER GENERATED ALWAYS AS IDENTITY,
   FO_BIB CHARACTER(5) not null,
   FO_ETB CHARACTER(3) not null,
   FO_CA CHARACTER(6) not null,
   FO_COL INTEGER not null,
   FO_FRS INTEGER not null,
   
   PRIMARY KEY(FO_ID)	
);;;

CREATE INDEX ##CURLIB##.in_fo_ca ON ##CURLIB##.FOURNISS (FO_CA);;;


INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_DEV,PA_VISI,PA_APP,PA_SQL) VALUES ('MonPanier.java','Correctif bug des d�cimales (2/4 d�cimales + centi�mes non affich�s)','1150407','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_DEV,PA_VISI,PA_APP,PA_SQL) VALUES ('MesDonnees.java','Correctif bug Code postal dans CLVIL CLPOS   (le code postal est int�gr� dans CLVIL de la fiche client mais dissoci� dans les bons de commande)','1150407','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_DEV,PA_VISI,PA_APP,PA_SQL) VALUES ('MajDB2.java','Mise en place d''une table interm�diaire "Catalogue" qui �vite les requ�tes poussives sur les articles / groupes / familles / sous familles. Cette table est r�g�n�r�e � la cr�ation du contexte dans MajDB2.java','1150410','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_DEV,PA_VISI,PA_APP,PA_SQL) VALUES ('GestionCatalogue.java','Suppression du rappel des tarifs lors de l''acc�s aux stocks -> plus rapide','1150413','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_DEV,PA_VISI,PA_APP,PA_SQL) VALUES ('GestionCatalogue.java','Nouvel appel des stocks en SQL au lieu du programme des tarifs -> plus rapide plus direct','1150413','0','1','2','1','');;;
INSERT INTO ##CURLIB##.PATCHSW (PA_CLAS,PA_DETA,PA_DATE,PA_ETAT,PA_DEV,PA_VISI,PA_APP,PA_SQL) VALUES ('GestionCatalogue.java','Mise en place d''une table FOURNISS. Cette table liste les fournisseurs par Famille/SF et groupe. Gain de temps consid�rable car on ne rappelle pas une requ�te articles','1150413','0','1','2','1','');;;


