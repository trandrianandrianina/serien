ALTER TABLE ##CURLIB##.INFOACC ADD ACC_LAN CHARACTER(2);;;

UPDATE ##CURLIB##.INFOACC SET ACC_LAN = 'fr';;;

INSERT INTO ##CURLIB##.INFOACC (ACC_NAME,ACC_LAN) VALUES ('ENGLISH TEXT','en');;;

CREATE table ##CURLIB##.TRADUCTION
(
   TR_ID INTEGER GENERATED ALWAYS AS IDENTITY,
   TR_CODE CHARACTER(4) not null,
   TR_CLE VARCHAR (1000) not null,
   TR_VALEUR VARCHAR (1000) not null,
   		
   PRIMARY KEY(TR_ID)	
);;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','Accueil','Accueil');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','Accueil','Home');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','Infos','Infos');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','Infos','About us');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','Connexion','Connexion');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','Connexion','Sign-in');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','Deconnexion','Deconnexion');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','Deconnexion','logout');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','Catalogue','Catalogue');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','Catalogue','Catalog');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('fr','Gestion','Gestion');;;

INSERT INTO ##CURLIB##.TRADUCTION 
(TR_CODE,TR_CLE,TR_VALEUR) 
VALUES ('en','Gestion','Management');;;

/***************************************************deb************************************/
INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$CGV','Conditions G�n�rales');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$CGV','Terms and Conditions');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$infosSociete','Informations sur la soci�t�');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$infosSociete','About us');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$catalogueArticles','Catalogue articles');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$catalogueArticles','Catalog');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$votreMagasin','Votre magasin');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$votreMagasin','Your store');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$groupeCGEDetSonepar','Groupe CGED/Sonepar');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$groupeCGEDetSonepar','CGED/Sonepar');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$notreGroupe','Notre Groupe');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$notreGroupe','Our group');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$adresseEmail','Adresse mail');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$adresseEmail','Email');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$motDePasse','Mot de passe');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$motDePasse','Password');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', 'Historique','Historique');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', 'Historique','Your data');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$Rechercher','Rechercher');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$Rechercher','Search item');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('fr', '$$connexionH2','Connexion au Web Shop');;;

INSERT INTO ##CURLIB##.TRADUCTION
(TR_CODE,TR_CLE,TR_VALEUR)
VALUES ('en', '$$connexionH2','Web Shop connexion');;;

INSERT INTO ##CURLIB##.TRADUCTION                                     
(TR_CODE,TR_CLE,TR_VALEUR)                                          
VALUES ('fr', '$$connexionH3','<span class="superflus">Vous avez un compte Web Shop: </span>veuillez saisir vos identifiants');;;

INSERT INTO ##CURLIB##.TRADUCTION                                     
(TR_CODE,TR_CLE,TR_VALEUR)                                          
VALUES ('en', '$$connexionH3','<span class="superflus">Vous avez un compte Web Shop: </span>veuillez saisir vos identifiants');;; 

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$connexionH3_2','<span class="superflus">Vous n''avez pas de compte: </span>Inscrivez vous');;;                                             

INSERT INTO ##CURLIB##.TRADUCTION                                 
(TR_CODE,TR_CLE,TR_VALEUR)                                      
VALUES ('en', '$$connexionH3_2','<span class="superflus">You still have no account: </span>Register here');;;   

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$formulaireContact','Formulaire de contact');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$formulaireContact','Contact form');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$prenom','Pr�nom');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$prenom','First name');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$nomFamille','Nom');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$nomFamille','Last name');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$societe','Soci�t�');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$societe','Company');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$telephone','T�l�phone');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$telephone','Phone number');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$sujet','Sujet');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$sujet','Subject');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$envoyer','Envoyer');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$envoyer','Send');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$confMailContact','Votre requ�te a �t� envoy�e avec succ�s � nos services.<br/>Un agent r�pondra � votre demande dans les plus brefs d�lais');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$confMailContact','Your request was successfully sent to our services.<br/>An agent will answer your demand as soon as possible');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$contactMagasin','Vous pouvez contacter votre magasin en consultant <a href="magasins">cette page</a>');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$contactMagasin','You can contact you store by consulting <a href="magasins">this page</a>');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$valider','Valider');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$valider','Confirm');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$motPasseOublie','Mot de passe oubli�');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$motPasseOublie','forgotten password');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$txtPlusieursClients','Votre profil existe chez plusieurs clients. Veuillez saisir votre num�ro de client.');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$txtPlusieursClients','Your profile exists at several customers. Please seize your customer''s number.');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$confirmMotDePasse','Confirm. mot de passe');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$confirmMotDePasse','Password confirm.');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$sinscrire','S''inscrire');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$sinscrire','Register');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$alerteInscription1','Attention, vous devez �tre client en compte chez ');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$alerteInscription1','Be careful, you have to be customer at ');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$alerteInscription2',' pour acc�der � ce service');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$alerteInscription2',' to reach this service');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$profilIndispo','Votre profil n''est pas disponible<span class="superflus"> sur le Web Shop</span>');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$profilIndispo','Your profile is not available<span class="superflus"> on the Web Shop</span>');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$ACCES_INEXISTANT','S''il s''agit d''une erreur de saisie, <a href="connexionEchec">connectez vous</a>.<br/> Si vous n''avez pas de profil, vous pouvez demander un acc�s aupr�s de votre <a href="contact">magasin</a>');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$ACCES_INEXISTANT','In the event of a typing error, try again to <a href="connexionEchec">sign-in</a>.<br/> If you do not have a profile, please ask your <a href="contact">store</a> for register you.');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('fr', '$$ACCES_BLOQUE','Votre acc�s au Web Shop a �t� bloqu�');;;

INSERT INTO ##CURLIB##.TRADUCTION  
(TR_CODE,TR_CLE,TR_VALEUR)                                   
VALUES ('en', '$$ACCES_BLOQUE','Your access to Web Shop has been blocked');;;
