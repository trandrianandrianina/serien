INSERT INTO ##CURLIB##.ENVIRONM VALUES 
('TEMPS_SESSION_INACTIVE','600', 'DONNEES','1');;;

INSERT INTO ##CURLIB##.ENVIRONM VALUES ('VOIR_PRIX_PUBLIC','0', 'DONNEES','1');;;

CREATE table ##CURLIB##.FOOTER            
(
	FT_ID INTEGER GENERATED ALWAYS AS IDENTITY,
	FT_LANG CHARACTER (2) not null,
 	FT_LIB CHARACTER (100) not null,
	FT_LIEN VARCHAR (1000) not null,
	FT_TARG CHAR(2) not null,
	
 	PRIMARY KEY(FT_ID)
);;;

INSERT INTO ##CURLIB##.FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG)
 VALUES ('fr','Notre groupe','pdf/planF.pdf','BL');;;

INSERT INTO ##CURLIB##.FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG)
 VALUES ('en','Our group','pdf/planF.pdf','BL');;;

INSERT INTO ##CURLIB##.FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG)
 VALUES ('fr','CGED','http://www.cge-distribution.com','BL');;;

INSERT INTO ##CURLIB##.FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG)
 VALUES ('en','CGED','http://www.cge-distribution.com','BL');;;

INSERT INTO ##CURLIB##.FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG)
 VALUES ('fr','Sonepar','http://www.sonepar.com/fr','BL');;;

INSERT INTO ##CURLIB##.FOOTER (FT_LANG,FT_LIB,FT_LIEN,FT_TARG)
 VALUES ('en','Sonepar','http://www.sonepar.com/en','BL');;;
 
INSERT INTO ##CURLIB##.ENVIRONM VALUES ('IS_MODE_LANGUAGES','0', 'DONNEES','1');;;