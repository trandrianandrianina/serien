/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.messages;

import com.google.gson.JsonObject;

import ri.serien.libas400.database.record.InterfaceRecord;

public class EnteteMsgRPG implements InterfaceRecord {
  // Constantes
  public static final int OFFSET_MSGIDM = 0;
  public static final int LONG_MSGIDM = 5;
  public static final int OFFSET_MSGNJB = 5;
  public static final int LONG_MSGNJB = 10;
  public static final int OFFSET_MSGRCD = 15;
  public static final int LONG_MSGRCD = 10;
  public static final int OFFSET_MSGUSR = 25;
  public static final int LONG_MSGUSR = 10;
  public static final int OFFSET_MSGMOD = 35;
  public static final int LONG_MSGMOD = 3;
  public static final int OFFSET_MSGFMT = 38;
  public static final int LONG_MSGFMT = 10;
  public static final int OFFSET_MSGPRG = 48;
  public static final int LONG_MSGPRG = 10;
  public static final int OFFSET_MSGBLC = 58;
  public static final int LONG_MSGBLC = 1;
  public static final int OFFSET_MSGFIC = 59;
  public static final int LONG_MSGFIC = 20;
  public static final int OFFSET_MSGRES = 79;
  public static final int LONG_MSGRES = 10;
  public static final int OFFSET_MSGFK1 = 89;
  public static final int LONG_MSGFK1 = 1;
  public static final int OFFSET_MSGFK2 = 90;
  public static final int LONG_MSGFK2 = 1;
  public static final int OFFSET_MSGIND = 91;
  public static final int LONG_MSGIND = 99;
  public static final String[] VAR_NAME =
  { "MSGIDM", "MSGNJB", "MSGRCD", "MSGUSR", "MSGMOD", "MSGFMT", "MSGPRG", "MSGBLC", "MSGFIC", "MSGRES", "MSGFK1", "MSGFK2", "MSGIND", };
  public static final int[] VAR_SIZE = { 5, 10, 10, 10, 3, 10, 10, 1, 20, 10, 1, 1, 99, };
  public static final boolean[] VAR_IS_ALPHA = { true, true, true, true, true, true, true, true, true, true, true, true, true, };
  public static final int SIZE_MAX = 190;
  public static final String spaces = String.format("%1$" + SIZE_MAX + "s", "");
  
  // Variables
  // 5
  private String msgidm = null;
  // 10
  private String msgnjb = null;
  // 10
  private String msgrcd = null;
  // 10
  private String msgusr = null;
  // 3
  private String msgmod = null;
  // 10
  private String msgfmt = null;
  // 10
  private String msrprg = null;
  // 1
  private String msgblc = null;
  // 20
  private String msgfic = null;
  // 10
  private String msgres = null;
  // 1
  private String msgfk1 = null;
  // 1
  private String msgfk2 = null;
  // 99
  private String msgind = null;
  
  // Variables de travail
  private StringBuffer buffer = null;
  
  /**
   * Constructeur.
   */
  public EnteteMsgRPG() {
  }
  
  /**
   * Constructeur.
   */
  public EnteteMsgRPG(String record) {
    setFlat(record);
  }
  
  /**
   * Renseigner MSGIDM.
   */
  public void setMSGIDM(String pmsgidm) {
    if ((pmsgidm == null) || (pmsgidm.length() <= LONG_MSGIDM)) {
      msgidm = pmsgidm;
    }
    else {
      msgidm = pmsgidm.substring(0, LONG_MSGIDM);
    }
  }
  
  public String getMSGIDM() {
    return msgidm;
  }
  
  /**
   * Renseigner MSGNJB en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGNJB(String pmsgnjb) {
    if ((pmsgnjb == null) || (pmsgnjb.length() <= LONG_MSGNJB)) {
      msgnjb = pmsgnjb;
    }
    else {
      msgnjb = pmsgnjb.substring(0, LONG_MSGNJB);
    }
  }
  
  public String getMSGNJB() {
    return msgnjb;
  }
  
  /**
   * Renseigner MSGRCD en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGRCD(String pmsgrcd) {
    if ((pmsgrcd == null) || (pmsgrcd.length() <= LONG_MSGRCD)) {
      msgrcd = pmsgrcd;
    }
    else {
      msgrcd = pmsgrcd.substring(0, LONG_MSGRCD);
    }
  }
  
  public String getMSGRCD() {
    return msgrcd;
  }
  
  /**
   * Renseigner MSGUSR en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGUSR(String pmsgusr) {
    if ((pmsgusr == null) || (pmsgusr.length() <= LONG_MSGUSR)) {
      msgusr = pmsgusr;
    }
    else {
      msgusr = pmsgusr.substring(0, LONG_MSGUSR);
    }
  }
  
  public String getMSGUSR() {
    return msgusr;
  }
  
  /**
   * Renseigner MSGMOD en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGMOD(String pmsgmod) {
    if ((pmsgmod == null) || (pmsgmod.length() <= LONG_MSGMOD)) {
      msgmod = pmsgmod;
    }
    else {
      msgmod = pmsgmod.substring(0, LONG_MSGMOD);
    }
  }
  
  public String getMSGMOD() {
    return msgmod;
  }
  
  /**
   * Renseigner MSGFMT en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGFMT(String pmsgfmt) {
    if ((pmsgfmt == null) || (pmsgfmt.length() <= LONG_MSGFMT)) {
      msgfmt = pmsgfmt;
    }
    else {
      msgfmt = pmsgfmt.substring(0, LONG_MSGFMT);
    }
  }
  
  public String getMSGFMT() {
    return msgfmt;
  }
  
  /**
   * Renseigner MSGPRG en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGPRG(String pmsgprg) {
    if ((pmsgprg == null) || (pmsgprg.length() <= LONG_MSGPRG)) {
      msrprg = pmsgprg;
    }
    else {
      msrprg = pmsgprg.substring(0, LONG_MSGPRG);
    }
  }
  
  public String getMSGPRG() {
    return msrprg;
  }
  
  /**
   * Renseigner MSGBLC en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGBLC(String pmsgblc) {
    if ((pmsgblc == null) || (pmsgblc.length() <= LONG_MSGBLC)) {
      msgblc = pmsgblc;
    }
    else {
      msgblc = pmsgblc.substring(0, LONG_MSGBLC);
    }
  }
  
  public String getMSGBLC() {
    return msgblc;
  }
  
  /**
   * Renseigner MSGFIC en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGFIC(String pmsgfic) {
    if ((pmsgfic == null) || (pmsgfic.length() <= LONG_MSGFIC)) {
      msgfic = pmsgfic;
    }
    else {
      msgfic = pmsgfic.substring(0, LONG_MSGFIC);
    }
  }
  
  public String getMSGFIC() {
    return msgfic;
  }
  
  /**
   * Renseigner MSGRES en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGRES(String pmsgres) {
    if ((pmsgres == null) || (pmsgres.length() <= LONG_MSGRES)) {
      msgres = pmsgres;
    }
    else {
      msgres = pmsgres.substring(0, LONG_MSGRES);
    }
  }
  
  public String getMSGRES() {
    return msgres;
  }
  
  /**
   * Renseigner MSGFK1 en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGFK1(String pmsgfk1) {
    if ((pmsgfk1 == null) || (pmsgfk1.length() <= LONG_MSGFK1)) {
      msgfk1 = pmsgfk1;
    }
    else {
      msgfk1 = pmsgfk1.substring(0, LONG_MSGFK1);
    }
  }
  
  public String getMSGFK1() {
    return msgfk1;
  }
  
  /**
   * Renseigner MSGFK2 en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGFK2(String pmsgfk2) {
    if ((pmsgfk2 == null) || (pmsgfk2.length() <= LONG_MSGFK2)) {
      msgfk2 = pmsgfk2;
    }
    else {
      msgfk2 = pmsgfk2.substring(0, LONG_MSGFK2);
    }
  }
  
  public String getMSGFK2() {
    return msgfk2;
  }
  
  /**
   * Renseigner MSGIND en tronquant la valeur à la taille du champs si nécessaire.
   */
  public void setMSGIND(String pmsgind) {
    if ((pmsgind == null) || (pmsgind.length() <= LONG_MSGIND)) {
      msgind = pmsgind;
    }
    else {
      msgind = pmsgind.substring(0, LONG_MSGIND);
    }
  }
  
  public String getMSGIND() {
    return msgind;
  }
  
  /**
   * Renseigner un champ du message à partir de son nom.
   */
  // @Override
  public void setField(String variable, Object valeur) {
    if (variable == null) {
      return;
    }
    
    if (variable.equals("MSGIDM")) {
      setMSGIDM((String) valeur);
    }
    else if (variable.equals("MSGNJB")) {
      setMSGNJB((String) valeur);
    }
    else if (variable.equals("MSGRCD")) {
      setMSGRCD((String) valeur);
    }
    else if (variable.equals("MSGUSR")) {
      setMSGUSR((String) valeur);
    }
    else if (variable.equals("MSGMOD")) {
      setMSGMOD((String) valeur);
    }
    else if (variable.equals("MSGFMT")) {
      setMSGFMT((String) valeur);
    }
    else if (variable.equals("MSGPRG")) {
      setMSGPRG((String) valeur);
    }
    else if (variable.equals("MSGBLC")) {
      setMSGBLC((String) valeur);
    }
    else if (variable.equals("MSGFIC")) {
      setMSGFIC((String) valeur);
    }
    else if (variable.equals("MSGRES")) {
      setMSGRES((String) valeur);
    }
    else if (variable.equals("MSGFK1")) {
      setMSGFK1((String) valeur);
    }
    else if (variable.equals("MSGFK2")) {
      setMSGFK2((String) valeur);
    }
    else if (variable.equals("MSGIND")) {
      setMSGIND((String) valeur);
    }
  }
  
  /**
   * Lire un champ du message à partir de son indice.
   */
  // @Override
  public Object getField(int indice) {
    if (indice < 0) {
      return null;
    }
    
    if (indice == 0) {
      return getMSGIDM();
    }
    else if (indice == 1) {
      return getMSGNJB();
    }
    else if (indice == 3) {
      return getMSGRCD();
    }
    else if (indice == 4) {
      return getMSGUSR();
    }
    else if (indice == 5) {
      return getMSGMOD();
    }
    else if (indice == 6) {
      return getMSGFMT();
    }
    else if (indice == 7) {
      return getMSGPRG();
    }
    else if (indice == 8) {
      return getMSGBLC();
    }
    else if (indice == 9) {
      return getMSGFIC();
    }
    else if (indice == 10) {
      return getMSGRES();
    }
    else if (indice == 11) {
      return getMSGFK1();
    }
    else if (indice == 12) {
      return getMSGFK2();
    }
    else if (indice == 13) {
      return getMSGIND();
    }
    else {
      return null;
    }
  }
  
  /**
   * Lire un champ du message à partir de son nom.
   */
  // @Override
  public Object getField(String variable) {
    if (variable == null) {
      return null;
    }
    
    if (variable.equals("MSGIDM")) {
      return getMSGIDM();
    }
    else if (variable.equals("MSGNJB")) {
      return getMSGNJB();
    }
    else if (variable.equals("MSGRCD")) {
      return getMSGRCD();
    }
    else if (variable.equals("MSGUSR")) {
      return getMSGUSR();
    }
    else if (variable.equals("MSGMOD")) {
      return getMSGMOD();
    }
    else if (variable.equals("MSGFMT")) {
      return getMSGFMT();
    }
    else if (variable.equals("MSGPRG")) {
      return getMSGPRG();
    }
    else if (variable.equals("MSGBLC")) {
      return getMSGBLC();
    }
    else if (variable.equals("MSGFIC")) {
      return getMSGFIC();
    }
    else if (variable.equals("MSGRES")) {
      return getMSGRES();
    }
    else if (variable.equals("MSGFK1")) {
      return getMSGFK1();
    }
    else if (variable.equals("MSGFK2")) {
      return getMSGFK2();
    }
    else if (variable.equals("MSGIND")) {
      return getMSGIND();
    }
    else {
      return null;
    }
  }
  
  /**
   * Retourne.
   */
  // @Override
  public StringBuffer getFlat() {
    if (buffer == null) {
      buffer = new StringBuffer(spaces);
    }
    else {
      // On initialise à blanc
      buffer.setLength(0);
      buffer.append(spaces);
    }
    
    if (msgidm != null) {
      buffer.replace(OFFSET_MSGIDM, OFFSET_MSGIDM + msgidm.length(), msgidm);
    }
    if (msgnjb != null) {
      buffer.replace(OFFSET_MSGNJB, OFFSET_MSGNJB + msgnjb.length(), msgnjb);
    }
    if (msgrcd != null) {
      buffer.replace(OFFSET_MSGRCD, OFFSET_MSGRCD + msgrcd.length(), msgrcd);
    }
    if (msgusr != null) {
      buffer.replace(OFFSET_MSGUSR, OFFSET_MSGUSR + msgusr.length(), msgusr);
    }
    if (msgmod != null) {
      buffer.replace(OFFSET_MSGMOD, OFFSET_MSGMOD + msgmod.length(), msgmod);
    }
    if (msgfmt != null) {
      buffer.replace(OFFSET_MSGFMT, OFFSET_MSGFMT + msgfmt.length(), msgfmt);
    }
    if (msrprg != null) {
      buffer.replace(OFFSET_MSGPRG, OFFSET_MSGPRG + msrprg.length(), msrprg);
    }
    if (msgblc != null) {
      buffer.replace(OFFSET_MSGBLC, OFFSET_MSGBLC + msgblc.length(), msgblc);
    }
    if (msgfic != null) {
      buffer.replace(OFFSET_MSGFIC, OFFSET_MSGFIC + msgfic.length(), msgfic);
    }
    if (msgres != null) {
      buffer.replace(OFFSET_MSGRES, OFFSET_MSGRES + msgres.length(), msgres);
    }
    if (msgfk1 != null) {
      buffer.replace(OFFSET_MSGFK1, OFFSET_MSGFK1 + msgfk1.length(), msgfk1);
    }
    if (msgfk2 != null) {
      buffer.replace(OFFSET_MSGFK2, OFFSET_MSGFK2 + msgfk2.length(), msgfk2);
    }
    if (msgind != null) {
      buffer.replace(OFFSET_MSGIND, OFFSET_MSGIND + msgind.length(), msgind);
    }
    return buffer;
  }
  
  /**
   * Initialise l'entête avec un enregistrement plat.
   */
  // @Override
  public boolean setFlat(String record) {
    if (record == null) {
      return false;
    }
    
    final int tailleRecord = record.length();
    int offsetDeb = 0;
    int offsetFin = 0;
    for (int i = 0; i < VAR_NAME.length; i++) {
      offsetDeb = offsetFin;
      offsetFin += VAR_SIZE[i];
      if (offsetFin <= tailleRecord) {
        setField(VAR_NAME[i], record.substring(offsetDeb, offsetFin).trim());
      }
      else {
        setField(VAR_NAME[i], record.substring(offsetDeb).trim());
      }
    }
    return true;
  }
  
  // @Override
  public JsonObject getJSON(boolean trim) {
    // TODO Auto-generated method stub
    return null;
  }
  
  // @Override
  public boolean setJSON(String record) {
    // TODO Auto-generated method stub
    return false;
  }
}
