/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400;

import ri.serien.libas400.dao.sql.exploitation.mail.SqlMail;
import ri.serien.libas400.dao.sql.exploitation.notification.SqlNotification;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeDestinataireMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.ParametreCreationMail;
import ri.serien.libcommun.exploitation.notification.ParametreCreationNotification;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe regroupe les méthodes permettant d'envoyer un mail ou une notification afin de prévenir des destinataires d'un problème.
 */
public class Avertissement {
  
  /**
   * Contrôle et envoi une notification aux destinataires des erreurs.
   */
  public static void envoyerNotification(SystemeManager pSystemeManager, QueryManager pQueryManager,
      ParametreCreationNotification pParametreCreationNotification) {
    if (pSystemeManager == null) {
      throw new MessageErreurException("Le systemeManager est invalide.");
    }
    if (pQueryManager == null) {
      throw new MessageErreurException("Le queryManager est invalide.");
    }
    if (pParametreCreationNotification == null) {
      throw new MessageErreurException("Les paramètres sont invalides.");
    }
    if (pParametreCreationNotification.getMessage() == null || pParametreCreationNotification.getMessage().isEmpty()) {
      throw new MessageErreurException("Le message est vide.");
    }
    
    // Création de la notification
    SqlNotification.creerNotification(pSystemeManager.getSystem(), pQueryManager, pParametreCreationNotification);
  }
  
  /**
   * Contrôle, prépare et envoi un mail aux destinataires des erreurs.
   */
  public static void envoyerMail(QueryManager pQueryManager, ParametreCreationMail pParametreCreationMail) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le queryManager est invalide.");
    }
    if (pParametreCreationMail == null) {
      throw new MessageErreurException("Les paramètres sont invalides.");
    }
    if (pParametreCreationMail.getMessage() == null || pParametreCreationMail.getMessage().isEmpty()) {
      throw new MessageErreurException("Le message est vide.");
    }
    if (pParametreCreationMail.getListeDestinataireMail() == null || pParametreCreationMail.getListeDestinataireMail().trim().isEmpty()) {
      throw new MessageErreurException("Aucun destinataire n'a été défini.");
    }
    
    // Préparation du mail
    IdEtablissement idEtablissement = pParametreCreationMail.getIdEtablissement();
    IdMail idMail = IdMail.getInstanceAvecCreationId(idEtablissement);
    Mail mail = new Mail(idMail);
    mail.setStatut(EnumStatutMail.A_ENVOYER);
    mail.setSujet(pParametreCreationMail.getSujet());
    mail.setCorpsMail(pParametreCreationMail.getMessage());
    IdTypeMail idTypeMail = IdTypeMail.getInstance(idEtablissement, EnumTypeMail.MAIL_AVERTISSEMENT_ERREUR);
    mail.setIdTypeMail(idTypeMail);
    // Contrôle de la présence de destinataires
    String listeDestinataire = pParametreCreationMail.getListeDestinataireMail();
    ListeDestinataireMail listeDestinataireMail = ListeDestinataireMail.convertirChaineEnListe(idMail, listeDestinataire);
    mail.setListeDestinataire(listeDestinataireMail);
    
    if (!mail.isValide()) {
      throw new MessageErreurException("Erreur lors de la construction du mail.");
    }
    SqlMail sqlMail = new SqlMail(pQueryManager);
    idMail = sqlMail.creerMail(mail);
    mail.setId(idMail);
    
    // Construction et sauvegarde de la liste des variables pour le mail
    ListeVariableMail listeVariableMail = ListeVariableMail.initialiserVariableMail(null, mail);
    sqlMail.creerListeVariableMail(listeVariableMail);
  }
  
}
