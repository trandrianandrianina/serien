/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.mail;

import com.ibm.as400.access.AS400;

import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.mail.DestinataireMail;
import ri.serien.libcommun.exploitation.mail.EnumProtocoleServeurMail;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeDestinataire;
import ri.serien.libcommun.exploitation.mail.EnvoiMail;
import ri.serien.libcommun.exploitation.mail.IdDestinataireMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdServeurMail;
import ri.serien.libcommun.exploitation.mail.ListeDestinataireMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.ServeurMail;
import ri.serien.libcommun.exploitation.notification.Notification;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe doit être utilisé dans le cadre d'une préparation et d'un envoi de mail à RI.
 * Il n'y a pas de stockage en base du mail envoyé et les données d'envoi sont stockées dans une datarea nommée RIDATA dans ?EXPAS.
 */
public class EnvoiMailRI {
  // Constantes
  public static final String NOM_DATA = "RIDATA";
  public static final int LONGUEUR_MINI_RIDATA = 401;
  
  // Variables
  private static IdEtablissement idEtablissement = IdEtablissement.getInstance("RIT");
  private static ServeurMail serveurMail = null;
  private static boolean chargee = false;
  private static String emailEmetteur = null;
  private static String emailDestinataire = null;
  
  /**
   * Lecture de la dataarea de ?EXPAS.
   */
  public static void chargerServeurMail(AS400 pSysteme) {
    try {
      // La dataarea pour la nouvelle gestion des licences
      Dataarea dataarea = new Dataarea();
      dataarea.setNom(NOM_DATA);
      dataarea.setBibliotheque(EnvironnementExecution.getExpas());
      
      // Lecture du contenu
      GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(pSysteme);
      String contenu = gestionDataareaAS400.chargerDataarea(dataarea);
      if (contenu == null) {
        throw new MessageErreurException("La dataarea " + EnvironnementExecution.getExpas().getNom() + '/' + NOM_DATA + " est invalide.");
      }
      if (contenu.length() < LONGUEUR_MINI_RIDATA) {
        throw new MessageErreurException("La dataarea " + EnvironnementExecution.getExpas().getNom() + '/' + NOM_DATA
            + " n'a pas la longueur minimun attendue qui est de " + LONGUEUR_MINI_RIDATA + '.');
      }
      
      // Découpage du contenu
      // Protocole
      Integer entier = Constantes.convertirTexteEnInteger("" + contenu.charAt(0));
      EnumProtocoleServeurMail protocoleServeurMail = EnumProtocoleServeurMail.valueOfByNumero(entier);
      // Port
      int portMail = Constantes.convertirTexteEnInteger(contenu.substring(1, 6));
      // Serveur de mail
      String serveurHostMail = Constantes.normerTexte(contenu.substring(6, 100));
      // Utilisateur
      String utilisateur = Constantes.normerTexte(contenu.substring(100, 200));
      // Mot de passe
      String motDePasse = Constantes.normerTexte(contenu.substring(200, 300));
      // Email émetteur
      emailEmetteur = utilisateur;
      // Email Destinataire
      emailDestinataire = Constantes.normerTexte(contenu.substring(300, 400));
      
      // Création de l'instance serveur (le numéro est de 999999 de manière arbitraire)
      IdServeurMail idServeurMail = IdServeurMail.getInstance(idEtablissement, 999999);
      serveurMail = new ServeurMail(idServeurMail, null);
      serveurMail.setActif(true);
      serveurMail.setHost(serveurHostMail);
      serveurMail.setPort(portMail);
      serveurMail.setProtocole(protocoleServeurMail);
      serveurMail.setUser(utilisateur);
      serveurMail.setPassword(motDePasse);
      
      chargee = true;
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Création d'un mail à partir d'une notification.
   */
  public static Mail creerMail(String pPrefixeSujet, Notification pNotification) {
    // Contrôle du chargement des informations du serveur de mail
    if (!chargee) {
      throw new MessageErreurException("Les données du serveur de mail n'ont pas été chargées.");
    }
    
    // Préparation du mail (le numéro est de 999999 de manière arbitraire)
    IdMail idMail = IdMail.getInstance(idEtablissement, 999999);
    Mail mail = new Mail(idMail);
    
    // Préparation du sujet
    String sujet = pNotification.getSujet();
    if (pPrefixeSujet != null) {
      sujet = pPrefixeSujet + sujet;
    }
    mail.setSujet(sujet);
    
    // Le corps est au format html
    String corpsHtml = "<html>" + pNotification.getMessage().replaceAll("\\n", "<BR>") + "</html>";
    mail.setCorpsMail(corpsHtml);
    
    ListeDestinataireMail listeDestinataireMail = new ListeDestinataireMail();
    IdDestinataireMail idDestinataire = IdDestinataireMail.getInstancePourTableMail(idMail);
    DestinataireMail destinataireMail = new DestinataireMail(idDestinataire);
    destinataireMail.setType(EnumTypeDestinataire.PRINCIPAL);
    destinataireMail.setEmail(emailDestinataire);
    listeDestinataireMail.add(destinataireMail);
    mail.setListeDestinataire(listeDestinataireMail);
    
    mail.setStatut(pNotification.getStatutMail());
    if (!mail.isValide()) {
      throw new MessageErreurException("Erreur lors de la construction du mail de la notification " + pNotification.getId() + '.');
    }
    
    return mail;
  }
  
  /**
   * Envoi un mail.
   */
  public static EnumStatutMail envoyerMail(Mail pMail) {
    try {
      if (EnvoiMail.envoyer(serveurMail, pMail, false, false)) {
        return EnumStatutMail.ENVOYE;
      }
      return EnumStatutMail.ERREUR_ENVOI;
    }
    catch (Exception e) {
      return EnumStatutMail.ERREUR_ENVOI;
    }
  }
}
