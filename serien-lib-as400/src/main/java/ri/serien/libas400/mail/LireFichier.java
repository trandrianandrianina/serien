/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.mail;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class LireFichier {
  String chaine = null;
  
  /**
   * Lire un fichier.
   */
  public LireFichier(String fichier) {
    if (fichier == null) {
      return;
    }
    
    File ffichier = new File(fichier);
    byte[] buffer = new byte[(int) ffichier.length()];
    chaine = "";
    
    // lecture du fichier texte
    try {
      InputStream ips = new FileInputStream(ffichier);
      ips.read(buffer);
      chaine = new String(buffer);
      ips.close();
    }
    catch (Exception e) {
      chaine = null;
    }
  }
  
  public String retournerChaine() {
    return chaine;
  }
  
}
