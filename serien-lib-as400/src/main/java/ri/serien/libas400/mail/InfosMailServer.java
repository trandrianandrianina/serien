/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.mail;

public class InfosMailServer {
  // Variables
  private String hosto = null; // Serveur sortant
  private int porto = 25;
  private String hosti = null; // Serveur entrant
  private int porti = 110;
  private String email = null; // Adresse email émetteur
  private String login = null; // identifiant du compte
  private String password = null; // mdp du compte
  
  public String getHosto() {
    return hosto;
  }
  
  public void setHosto(String hosto) {
    this.hosto = hosto;
  }
  
  public int getPorto() {
    return porto;
  }
  
  public void setPorto(int porto) {
    this.porto = porto;
  }
  
  public String getHosti() {
    return hosti;
  }
  
  public void setHosti(String hosti) {
    this.hosti = hosti;
  }
  
  public int getPorti() {
    return porti;
  }
  
  public void setPorti(int porti) {
    this.porti = porti;
  }
  
  public String getEmail() {
    return email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getLogin() {
    return login;
  }
  
  public void setLogin(String login) {
    this.login = login;
  }
  
  public String getPassword() {
    return password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
}
