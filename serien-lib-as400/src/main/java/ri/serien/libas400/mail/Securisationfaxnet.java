/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.mail;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.Signature;

public class Securisationfaxnet {
  String fichierkey = "";
  String alias = "";
  String passwordfichier = "";
  String passwordkey = "";
  FileWriter fwFichier = null;
  
  /**
   * Constructeur.
   */
  public Securisationfaxnet(String qfichierkey, String qpasswordstorage, String qalias, String qpasswordkey, FileWriter afwFichier) {
    fichierkey = qfichierkey;
    alias = qalias;
    passwordfichier = qpasswordstorage;
    passwordkey = qpasswordkey;
    
    fwFichier = afwFichier;
  }
  
  protected String secure(byte[] message) {
    FileInputStream fin = null;
    String retour = null;
    try {
      // if( DEBUG ) trace(message);
      
      fin = new FileInputStream(fichierkey);
      java.security.KeyStore ks = KeyStore.getInstance("JKS");
      
      ks.load(fin, passwordfichier.toCharArray());
      PrivateKey pk = (PrivateKey) ks.getKey(alias, passwordkey.toCharArray());
      ks.getCertificate(alias);
      Signature signature = Signature.getInstance("SHA1withRSA");
      // Signature signature = Signature.getInstance("SHA-1");
      signature.initSign(pk);
      // writeLog("secure, message|"+new String(message)+"|");
      signature.update(message);
      byte[] sigBytes = signature.sign();
      byte[] ba = org.apache.commons.codec.binary.Base64.encodeBase64(sigBytes);
      
      retour = new String(ba);
    }
    catch (Exception e) {
      // throw et;
      e.printStackTrace();
      retour = null;
    }
    finally {
      if (fin != null) {
        try {
          fin.close();
        }
        catch (Exception e) {
          // @todo Gérer correctemnet l'exppetion.
        }
      }
    }
    
    return retour;
  }
}
