/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.parameters;

/**
 * Classe Personnalisations ancienne mode (Mobilite, ...) à supprimer à terme.
 */
public class PersonnalisationsMobilite {
  
  // Variables
  public static int NBR_PS = 177;
  private char[] tableau = new char[NBR_PS];
  
  /**
   * @return le tableau.
   */
  public char[] getTableau() {
    return tableau;
  }
  
  /**
   * Renseigner la personnalisation à aprtir d'un tableau.
   */
  public void setTableau(char[] tableau) {
    this.tableau = tableau;
  }
  
  /**
   * Renseigner la personnalisation à aprtir d'une chaîne.
   */
  public void setTableau(String chaine) {
    if (chaine == null) {
      return;
    }
    if (chaine.length() > NBR_PS) {
      NBR_PS = chaine.length();
      tableau = new char[NBR_PS];
    }
    int delta = NBR_PS - chaine.length();
    
    // On alimente le tableau des PS avec les valeurs récupérées
    int index = 0;
    for (; index < chaine.length(); index++) {
      tableau[index] = chaine.charAt(index);
    }
    // On complète le tableau des PS avec du blancs si nécessaire
    for (int j = 0; j < delta; j++, index++) {
      tableau[index] = ' ';
    }
  }
  
  /**
   * Retourne la valeur de la PS numéro X.
   */
  public char getValue(int num) {
    if ((tableau == null) || (num > tableau.length)) {
      return ' ';
    }
    return tableau[num - 1];
  }
  
}
