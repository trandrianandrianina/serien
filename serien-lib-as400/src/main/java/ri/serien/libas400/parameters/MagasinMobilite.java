/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.parameters;

/**
 * Classe Magasin ancienne mode (Mobilite, ...) à supprimer à terme.
 */
public class MagasinMobilite {
  private String codeMag = null;
  private String maLib = null;
  
  // à compléter
  
  /**
   * @return le codeMag.
   */
  public String getCodeMag() {
    return codeMag != null ? codeMag.trim() : "";
  }
  
  /**
   * @param codeMag le codeMag à définir.
   */
  public void setCodeMag(String codeMag) {
    this.codeMag = codeMag;
  }
  
  /**
   * @return le mALIB.
   */
  public String getMALIB() {
    return maLib != null ? maLib.trim() : "";
  }
  
  /**
   * @param pmaLib le mALIB à définir.
   */
  public void setMALIB(String pmaLib) {
    maLib = pmaLib;
  }
  
}
