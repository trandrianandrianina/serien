/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.parameters;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;

/**
 * Classe Etablissement ancienne mode (Mobilite, ...) à supprimer à terme.
 */
public class EtablissementMobilite {
  // Variables
  private IdEtablissement idEtablissement = null;
  private String nom = "";
  private String complementNom = "";
  private IdMagasin magasinGeneral = null;
  private PersonnalisationsMobilite ps = null;
  
  /**
   * @return le nom.
   */
  public String getNom() {
    return nom;
  }
  
  /**
   * @param nom le nom à définir.
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  /**
   * @return le complementNom.
   */
  public String getComplementNom() {
    return complementNom;
  }
  
  /**
   * @param complementNom le complementNom à définir.
   */
  public void setComplementNom(String complementNom) {
    this.complementNom = complementNom;
  }
  
  /**
   * @return le codeEtablissement.
   */
  public IdEtablissement getId() {
    return idEtablissement;
  }
  
  /**
   * @param codeEtablissement le codeEtablissement à définir.
   */
  public void setId(IdEtablissement pIdEtablissement) {
    this.idEtablissement = pIdEtablissement;
  }
  
  /**
   * Code de l'établissement.
   */
  public String getCodeEtablissement() {
    if (idEtablissement == null) {
      return null;
    }
    return idEtablissement.getCodeEtablissement();
  }
  
  /**
   * @return le magasinGeneral.
   */
  public IdMagasin getMagasinGeneral() {
    return magasinGeneral;
  }
  
  /**
   * @param magasinGeneral le magasinGeneral à définir.
   */
  public void setMagasinGeneral(IdMagasin pMagasinGeneral) {
    if (pMagasinGeneral == null) {
      this.magasinGeneral = null;
    }
    else {
      this.magasinGeneral = pMagasinGeneral;
    }
  }
  
  /**
   * @return le ps.
   */
  public PersonnalisationsMobilite getPs() {
    if (ps == null) {
      ps = new PersonnalisationsMobilite();
    }
    return ps;
  }
  
  /**
   * @param ps le ps à définir.
   */
  public void setPs(PersonnalisationsMobilite ps) {
    this.ps = ps;
  }
  
}
