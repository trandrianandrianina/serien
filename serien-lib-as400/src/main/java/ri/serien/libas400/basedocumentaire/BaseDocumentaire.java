/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.basedocumentaire;

import java.io.File;

import ri.serien.libas400.dao.sql.exploitation.basedocumentaire.SqlBaseDocumentaire;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.DocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.EnumTypeDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.IdDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.ListeDocumentStocke;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;

/**
 * Cette classe regroupe l'ensemble des traitements qui concernent la base documentaire.
 */
public class BaseDocumentaire {
  // Constantes
  public static final char SEPARATEUR_SERVEUR = '/';
  // La racine de la base documentaire est unique pour un serveur physique
  public static final String CHEMIN_RACINE_BASE = "/SerieNBdDoc";
  // Longueur maximum du chemin complet d'un document stocké dans la base documentaire (racine + chemin relatif)
  public static final int LONGUEUR_CHEMIN_COMPLET = 255;
  
  // -- Méthodes publiques
  
  /**
   * Enregistre un fichier qui se situe dans l'IFS du serveur vers la base documentaire.
   * Le paramètre "pSupprimerFichierSource" indique si le fichier doit être supprimé après sa copie dans la base documentaire.
   * Note:
   * Cette méthode ne sauve pas les liens avec les documents de ventes ou avec les clients, il faut appeler explicitement les
   * méthodes dédiées à la création de ces liens.
   */
  public static DocumentStocke sauverDocumentStocke(QueryManager pQueryManager, FileNG pFichierAStocker, IdEtablissement pIdEtablissement,
      String pProfil, EnumTypeDocumentStocke pTypeDocumentStocke, String pDescription, boolean pSupprimerFichierSource) {
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'identifiant de l'établissement est invalide.");
    }
    pProfil = Constantes.normerTexte(pProfil);
    if (pProfil.isEmpty()) {
      throw new MessageErreurException("Le profil de l'utilisateur doit obligatoirement être renseigné.");
    }
    if (Constantes.equals(pTypeDocumentStocke, EnumTypeDocumentStocke.NON_DEFINI)) {
      throw new MessageErreurException("Le type du document à stocker est invalide.");
    }
    controlerFichierIfs(pFichierAStocker);
    
    // Création de l'objet DocumentStocke
    IdDocumentStocke id = IdDocumentStocke.getInstanceAvecCreationId(pIdEtablissement);
    DocumentStocke documentStocke = new DocumentStocke(id, pProfil, pTypeDocumentStocke);
    documentStocke.initialiserInformationBase(pFichierAStocker, pDescription);
    
    // Créer l'enregistrement dans la table afin de récupérer l'identifiant du document stocké
    SqlBaseDocumentaire sqlBaseDocumentaire = new SqlBaseDocumentaire(pQueryManager);
    sqlBaseDocumentaire.creerIdentifiantDocumentStocke(documentStocke);
    
    // Compléter les informations du document stocké
    String cheminRelatif = construireCheminRelatifDuDocument(documentStocke.getId(), pFichierAStocker);
    documentStocke.completerInformation(pFichierAStocker, cheminRelatif);
    
    // Construction du chemin où va être stocké le fichier
    String cheminCompletDestination = contruireCheminCompletDocumentStocke(pQueryManager.getLibrary(), documentStocke);
    File fichierDestination = new File(cheminCompletDestination);
    if (!pFichierAStocker.copyTo(fichierDestination)) {
      throw new MessageErreurException(
          "Erreur lors de la copie du fichier " + pFichierAStocker.getAbsolutePath() + " vers " + fichierDestination.getAbsolutePath());
    }
    // Suppression du fichier source si la demande en a été faite
    if (pSupprimerFichierSource) {
      if (!pFichierAStocker.delete()) {
        Trace.erreur("Erreur lors de la suppression du fichier " + pFichierAStocker.getAbsolutePath());
      }
      else {
        Trace.info("Le fichier " + pFichierAStocker.getAbsolutePath() + " a été supprimé.");
      }
    }
    
    // Mettre à jour l'enregistrement dans la table avec les données du fichiers et mise à jour du statut
    sqlBaseDocumentaire.sauverDocumentStocke(documentStocke);
    Trace.info("Le fichier " + pFichierAStocker.getAbsolutePath() + " a été stocké dans la base documentaire avec succès.");
    
    return documentStocke;
  }
  
  /**
   * Retourne un document stocké à partir de son identifiant.
   */
  public static DocumentStocke chargerDocumentStocke(QueryManager pQueryManager, IdDocumentStocke pIdDocumentStocke) {
    // Lecture en base de données des informations
    SqlBaseDocumentaire sqlBaseDocumentaire = new SqlBaseDocumentaire(pQueryManager);
    DocumentStocke documentStocke = sqlBaseDocumentaire.chargerDocumentStocke(pIdDocumentStocke);
    return documentStocke;
  }
  
  /**
   * Enregistre un lien entre un document stocké et un de document de vente.
   */
  public static void sauverLienDocumentStocke(QueryManager pQueryManager, IdDocumentStocke pIdDocumentStocke,
      IdDocumentVente pIdDocumentVente) {
    SqlBaseDocumentaire sqlBaseDocumentaire = new SqlBaseDocumentaire(pQueryManager);
    sqlBaseDocumentaire.sauverLienAvecDocumentVente(pIdDocumentStocke, pIdDocumentVente);
  }
  
  /**
   * Enregistre un lien entre un document stocké et un client.
   */
  public static void sauverLienDocumentStocke(QueryManager pQueryManager, IdDocumentStocke pIdDocumentStocke, IdClient pIdClient) {
    SqlBaseDocumentaire sqlBaseDocumentaire = new SqlBaseDocumentaire(pQueryManager);
    sqlBaseDocumentaire.sauverLienAvecClient(pIdDocumentStocke, pIdClient);
  }
  
  /**
   * Retourne une liste de documents stockés suivant les critères.
   */
  public static ListeDocumentStocke listerDocumentStocke(QueryManager pQueryManager, CritereDocumentStocke pCritere) {
    // Lecture en base de données des informations
    SqlBaseDocumentaire sqlBaseDocumentaire = new SqlBaseDocumentaire(pQueryManager);
    return sqlBaseDocumentaire.listerDocumentStocke(pCritere);
  }
  
  /**
   * Retourne le chemin complet qui mène au fichier du document stocké.
   * Attention à utiliser avec prudence, il s'agit d'une routine bas niveau.
   * Les accès doivent obligatoirement être fait en lecture seule.
   * Exemple d'utilisation: pour les mails on récupère le chemin de la pièce jointe de type document lié.
   */
  public static String getCheminCompletDocumentStocke(QueryManager pQueryManager, DocumentStocke pDocumentStocke) {
    return contruireCheminCompletDocumentStocke(pQueryManager.getLibrary(), pDocumentStocke);
  }
  
  // -- Méthodes privées
  
  /**
   * Controle que le fichier à stocker qui se trouve dans l'IFS existe et soit un fichier.
   */
  private static void controlerFichierIfs(FileNG pFichierIfsAStocker) {
    if (pFichierIfsAStocker == null) {
      throw new MessageErreurException("Le fichier à stocker est invalide.");
    }
    if (!pFichierIfsAStocker.exists()) {
      throw new MessageErreurException(
          "Le fichier à stocker n'a pas été trouvé à l'emplacement désigné " + pFichierIfsAStocker.getAbsolutePath());
    }
    if (!pFichierIfsAStocker.isFile()) {
      throw new MessageErreurException("Le chemin du fichier ne pointe pas vers un fichier.");
    }
  }
  
  /**
   * Contrôle que le chemin du fichier à stocker soit un fichier ifs.
   */
  private static FileNG controlerFichierTypeIfs(String pCheminFichierIfsAStocker) {
    pCheminFichierIfsAStocker = Constantes.normerTexte(pCheminFichierIfsAStocker);
    if (pCheminFichierIfsAStocker.isEmpty() || pCheminFichierIfsAStocker.indexOf(SEPARATEUR_SERVEUR) == -1
        || pCheminFichierIfsAStocker.indexOf('\\') != -1) {
      throw new MessageErreurException("Le chemin du fichier est invalide.");
    }
    return new FileNG(pCheminFichierIfsAStocker);
  }
  
  /**
   * Converti l'identifiant du document stocké en chemin relatif. Le nom du fichier et de son extension sont inclus.
   * L'identifiant est formaté sur 8 caractères puis découpé en segment de 2 caractères.
   * Le séparateur de dossier n'est jamais inséré au début du chemin.
   * Exemple: 00/01/10/15/00011015.pdf
   */
  private static String construireCheminRelatifDuDocument(IdDocumentStocke pIdDocumentStocke, FileNG pFichierAStocker) {
    if (pIdDocumentStocke == null || pIdDocumentStocke.getNumero() <= 0) {
      throw new MessageErreurException("L'identifiant du document stocké n'a pas été attribué.");
    }
    if (pFichierAStocker == null) {
      throw new MessageErreurException("Le fichier stocké n'est pas valide.");
    }
    // Vérification que la longueur de l'id soit un multiple de 2
    int reste = IdDocumentStocke.LONGUEUR_NUMERO % 2;
    if (reste != 0) {
      throw new MessageErreurException("La longueur de l'identifiant du document stocké doit être un multiple de 2.");
    }
    
    // Découpe l'identifiant en segment de 2 caractères
    String identifiant = String.format("%0" + IdDocumentStocke.LONGUEUR_NUMERO + "d", pIdDocumentStocke.getNumero());
    String[] listeDossier = identifiant.split("(?<=\\G.{2})");
    // Création du dossier
    StringBuilder cheminRelatif = new StringBuilder(LONGUEUR_CHEMIN_COMPLET);
    for (int i = 0; i < listeDossier.length - 1; i++) {
      cheminRelatif.append(listeDossier[i]).append(SEPARATEUR_SERVEUR);
    }
    // Création du nom du fichier
    cheminRelatif.append(identifiant).append('.').append(pFichierAStocker.getExtension(false));
    return cheminRelatif.toString();
  }
  
  /**
   * Contruit le chemin racine de la base documentaire pour un document à stocker.
   * Cette racine commence par le CHEMIN_RACINE suivit de la base de donnée suivit de l'établissement.
   * Il n'y a pas de séparateur en fin de chemin.
   */
  private static String contruireCheminCompletDocumentStocke(String pBaseDeDonnees, DocumentStocke pDocumentStocke) {
    if (pBaseDeDonnees == null) {
      throw new MessageErreurException("Le nom de la base de données est invalide.");
    }
    
    IdBibliotheque baseDeDonnees = IdBibliotheque.getInstance(pBaseDeDonnees);
    return contruireCheminCompletDocumentStocke(baseDeDonnees, pDocumentStocke);
  }
  
  /**
   * Contruit le chemin racine de la base documentaire pour un document à stocker.
   * Cette racine commence par le CHEMIN_RACINE suivit de la base de donnée suivit du code établissement trimé.
   * Il n'y a pas de séparateur en fin de chemin.
   */
  private static String contruireCheminCompletDocumentStocke(IdBibliotheque pBaseDeDonnees, DocumentStocke pDocumentStocke) {
    if (pBaseDeDonnees == null) {
      throw new MessageErreurException("L'identifiant de la base de données est invalide.");
    }
    if (pDocumentStocke == null) {
      throw new MessageErreurException("Le document stocké est invalide.");
    }
    String cheminRelatif = Constantes.normerTexte(pDocumentStocke.getCheminRelatif());
    if (cheminRelatif.isEmpty()) {
      throw new MessageErreurException("Le chemin relatif du document stocké est invalide.");
    }
    
    // Construction de la racine dans la base documentaire
    String racine = CHEMIN_RACINE_BASE + SEPARATEUR_SERVEUR + pBaseDeDonnees.getNom() + SEPARATEUR_SERVEUR
        + pDocumentStocke.getId().getCodeEtablissement().trim();
    
    // Construction du chemin complet
    StringBuilder cheminDansBaseDocumentaire = new StringBuilder(LONGUEUR_CHEMIN_COMPLET);
    cheminDansBaseDocumentaire.append(racine).append(SEPARATEUR_SERVEUR).append(cheminRelatif);
    
    return cheminDansBaseDocumentaire.toString();
  }
  
}
