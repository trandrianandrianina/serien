/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.dataarea;

import com.ibm.as400.access.LocalDataArea;

import ri.serien.libas400.system.SystemeManager;

/**
 * Gestion des informations contenues dans la *LDA.
 */
public class LdaManager {
  // Variables
  private SystemeManager systeme = null;
  private LocalDataArea lda = null;
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public LdaManager(SystemeManager psysteme) {
    setSystem(psysteme);
    lda = new LocalDataArea(systeme.getSystem());
  }
  
  /**
   * Lit et retourne le contenu de la LDA.
   */
  public String getData() {
    String data = null;
    
    try {
      data = lda.read();
    }
    catch (Exception e) {
      msgErreur = "[LDAOperation] (getData) Erreur : " + e;
    }
    
    return data;
  }
  
  /**
   * Ecrit le contenu de data dans la LDA.
   */
  public boolean setData(String data) {
    if (data == null) {
      return false;
    }
    try {
      if (data.length() > lda.getLength()) {
        return false;
      }
      lda.write(data);
    }
    catch (Exception e) {
      msgErreur = "[LDAOperation] (setData) Erreur : " + e;
      return false;
    }
    
    return true;
  }
  
  /**
   * Initialise la variable système.
   */
  public void setSystem(SystemeManager psysteme) {
    if (psysteme == null) {
      msgErreur += "\n[LDAOperation] (setSystem) Erreur : le paramètre psysteme est null";
    }
    else {
      this.systeme = psysteme;
    }
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
