/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.job;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;

public class ManagerFluxMagentoASurveiller {
  // Variables
  private ManagerFluxMagento managerFluxMagento = null;
  private boolean avertissementEnvoye = false;
  
  /**
   * Constructeur.
   */
  public ManagerFluxMagentoASurveiller(ManagerFluxMagento pManagerFluxMagento) {
    managerFluxMagento = pManagerFluxMagento;
  }
  
  // -- Méthodes publiques
  
  /**
   * Récupérer le message.
   */
  public String recupererMessage() {
    if (managerFluxMagento.getListeFluxMagentoEnCours() == null || managerFluxMagento.getListeFluxMagentoEnCours().isEmpty()) {
      return null;
    }
    
    String message = "Voici la liste des flux qui ont dépassé le temps d'exécution de "
        + (SurveillanceJob.TEMPS_EXECUTION_MAX_EN_SECONDE / 60) + " minutes : ";
    for (FluxMagento flux : managerFluxMagento.getListeFluxMagentoEnCours()) {
      message += "\n - le flux " + flux.getFLID() + " de type " + flux.getFLIDF();
    }
    
    return message;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le manager de flux.
   */
  public ManagerFluxMagento getManagerFluxMagento() {
    return managerFluxMagento;
  }
  
  /**
   * Retourne si un avertissement a été envoyé.
   */
  public boolean isAvertissementEnvoye() {
    return avertissementEnvoye;
  }
  
  /**
   * Initialise si l'avertissement a été envoyé.
   */
  public void setAvertissementEnvoye(boolean avertissementEnvoye) {
    this.avertissementEnvoye = avertissementEnvoye;
  }
  
}
