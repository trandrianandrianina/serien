/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.AS400;

import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.version.Version;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe regroupe les méthodes qui permettent de récupérer les numéros de version de Série N et des bases de données.
 */
public class VersionManager {
  
  /**
   * Retourne la version de Série N.
   */
  public static Version retournerVersionSerieN(AS400 pSysteme, char pLettre) {
    if (pSysteme == null) {
      throw new MessageErreurException("La connexion avec le serveur est invalide.");
    }
    if (pLettre == ' ') {
      throw new MessageErreurException("La lettre de l'envrionnement d'exécution est invalide.");
    }
    
    Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(pLettre + "EXPAS"), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
    Dataarea wexpdta = new Dataarea();
    wexpdta.setNom("WEXPDTA");
    wexpdta.setBibliotheque(bibliotheque);
    
    // Lecture de la dataarea
    GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(pSysteme);
    String valeur = gestionDataareaAS400.chargerDataarea(wexpdta);
    if (valeur == null || valeur.isEmpty()) {
      throw new MessageErreurException(
          "Le contenu de la dataarea WEXPDTA de la bibliothèque " + bibliotheque.getNom() + " n'est pas valide.");
    }
    if (valeur.length() < 24) {
      throw new MessageErreurException(
          "La longueur de la dataarea WEXPDTA de la bibliothèque " + bibliotheque.getNom() + " n'a pas la longueur attendue.");
    }
    // Récupération des valeurs dispersées dans la dataarea
    String annee = valeur.substring(19, 23);
    String mois = valeur.substring(9, 11);
    String release = valeur.substring(5, 7);
    
    // Création de l'objet Version pour Série N
    return new Version(annee, mois, release);
  }
  
  /**
   * Retourne la version d'une base de données c'est à dire d'une bibliothèque contenant les tables.
   */
  public static Version retournerVersionBaseDeDonnee(AS400 pSysteme, Bibliotheque pBibliotheque) {
    if (pSysteme == null) {
      throw new MessageErreurException("La connexion avec le serveur est invalide.");
    }
    if (pBibliotheque == null) {
      throw new MessageErreurException("La bibliothèque fichier est incorrecte.");
    }
    
    Dataarea wexpdta = new Dataarea();
    wexpdta.setNom("PSEMVERN");
    wexpdta.setBibliotheque(pBibliotheque);
    
    // Lecture de la dataarea
    GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(pSysteme);
    String valeur = gestionDataareaAS400.chargerDataarea(wexpdta);
    if (valeur == null || valeur.isEmpty()) {
      throw new MessageErreurException(
          "Le contenu de la dataarea PSEMVERN de la bibliothèque " + pBibliotheque.getNom() + " n'est pas valide.");
    }
    if (valeur.length() < 8) {
      throw new MessageErreurException(
          "La longueur de la dataarea PSEMVERN de la bibliothèque " + pBibliotheque.getNom() + " n'a pas la longueur attendue.");
    }
    
    // Il s'agit d'une bibliothèque de Série M avec la numérotation de type X.XX/XX
    // ou de Serie N avec la numérotation de type 2.XX/XX
    if (valeur.trim().length() < 6) {
      String annee = "000" + valeur.charAt(0);
      String mois = valeur.substring(1, 3);
      String release = valeur.substring(3, 6);
      // Création de l'objet Version pour la bibliothèque de fichiers
      return new Version(annee, mois, release);
    }
    // Il s'agit d'une bibliothèque de Série N avec la numérotation annee.mois/release
    else {
      String annee = valeur.substring(0, 4);
      String mois = valeur.substring(4, 6);
      String release = valeur.substring(6, 8);
      // Création de l'objet Version pour la bibliothèque de fichiers
      return new Version(annee, mois, release);
    }
  }
  
}
