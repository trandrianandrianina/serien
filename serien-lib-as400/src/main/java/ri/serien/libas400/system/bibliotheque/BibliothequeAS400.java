/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.bibliotheque;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileFilter;
import com.ibm.as400.access.ObjectDescription;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.VersionManager;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.commun.bdd.EnumStatutBDD;
import ri.serien.libcommun.commun.bdd.ListeBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.version.Version;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

public class BibliothequeAS400 {
  // Constantes
  private static final String TYPE_BIBLIOTHEQUE = "LIB";
  
  // Variables
  private AS400 systeme = null;
  private IFSFile objetIfs = null;
  private Bibliotheque bibliotheque = null;
  
  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public BibliothequeAS400(AS400 pSysteme) {
    systeme = pSysteme;
  }
  
  /**
   * Constructeur.
   */
  public BibliothequeAS400(AS400 pSysteme, Bibliotheque pBibliotheque) {
    this(pSysteme);
    setBibliotheque(pBibliotheque);
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifie l'existence de la bibliothèque.
   */
  public boolean isExists() {
    if (objetIfs == null) {
      return false;
    }
    
    try {
      return objetIfs.exists();
    }
    catch (IOException ioe) {
      return false;
    }
  }
  
  /**
   * Créer la bibliothèque si elle n'existe pas.
   */
  public boolean create() {
    if (objetIfs == null) {
      return false;
    }
    
    try {
      if (!objetIfs.exists()) {
        return objetIfs.mkdir();
        // ou CommandCall cmd = new CommandCall(sysAS400, "QSYS/CRTLIB LIB(" + library + ")");
      }
    }
    catch (IOException ioe) {
      return false;
    }
    return true;
  }
  
  /**
   * Créer la bibliothèque sans tester son l'existence auparavent.
   */
  public boolean createWOCheckExists() {
    if (objetIfs == null) {
      return false;
    }
    
    try {
      return objetIfs.mkdir();
    }
    catch (IOException ioe) {
      return false;
    }
  }
  
  /**
   * Supprime la bibliothèque.
   */
  public boolean delete() {
    if (objetIfs == null) {
      return false;
    }
    
    try {
      if (objetIfs.exists()) {
        // Contrôle si la bibliothèque est vide
        IFSFile[] contenu = objetIfs.listFiles();
        if (contenu.length > 0) {
          Bibliotheque baseDeDonnees = null;
          ContexteProgrammeRPG cmd = new ContexteProgrammeRPG(systeme, baseDeDonnees);
          return cmd.executerCommande("DLTLIB LIB(" + bibliotheque.getNom() + ")");
        }
        
        // Dans le cas où la bibliothèque est vide
        return objetIfs.delete();
      }
    }
    catch (IOException ioe) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  /**
   * Lit le texte de description d'une bibliothèque.
   */
  public void lireDescription() {
    if (bibliotheque == null) {
      return;
    }
    // On vérifie que la bilbiothèque existe
    if (!isExists()) {
      throw new MessageErreurException(String.format("La bibliothèque %s n'existe pas.", bibliotheque.getNom()));
    }
    
    ObjectDescription objectDescription = new ObjectDescription(systeme, bibliotheque.getCheminIFS());
    try {
      bibliotheque.setDescription(objectDescription.getValueAsString(ObjectDescription.TEXT_DESCRIPTION));
    }
    catch (Exception e) {
      bibliotheque = null;
      throw new MessageErreurException(e, "");
    }
  }
  
  // -- Méthodes statiques
  
  /**
   * Vérifie l'existence d'une bibliothèque donnée.
   */
  public static boolean verifierExistence(AS400 pSysteme, Bibliotheque pBibliotheque) {
    if (pSysteme == null) {
      throw new MessageErreurException("La configuration système est incorrecte.");
    }
    if (pBibliotheque == null) {
      throw new MessageErreurException("La bibliothèque est incorrecte.");
    }
    
    IFSFile objetIfs = new IFSFile(pSysteme, pBibliotheque.getCheminIFS());
    try {
      return objetIfs.exists();
    }
    catch (IOException e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Créé une bibliothèque.
   */
  public boolean creerBibliotheque(AS400 pSysteme, Bibliotheque pBibliotheque) {
    if (pBibliotheque == null) {
      throw new MessageErreurException("Le paramètre aBibliotheque est à null");
    }
    if ((pBibliotheque.getNom() == null) || (pBibliotheque.getNom().length() > 10)) {
      throw new MessageErreurException("Le nom de la bibliothèque est incorrect : " + pBibliotheque.getNom());
    }
    
    // On vérifie que la bilbiothèque n'existe pas déjà
    if (!verifierExistence(pSysteme, pBibliotheque)) {
      return true;
    }
    
    // Sinon on l'a créé
    CommandCall cmd =
        new CommandCall(systeme, String.format("QSYS/CRTLIB LIB(%s) TEXT(%s)", pBibliotheque.getNom(), pBibliotheque.getDescription()));
    try {
      return cmd.run();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Recherche les bibliothèques en fonction du critère donnée.
   * Note : la description n'est pas chargée car la nouvelle méthode ne le permet pas et les performances étaient parfois désastreuses
   * avec l'ancienne.
   */
  public static List<Bibliotheque> chargerListeBibliotheque(AS400 pSysteme, CritereBibliotheque pCriteres) {
    List<IdBibliotheque> listeIdBibliotheques = retournerListeBibliotheque(pSysteme, pCriteres);
    if (listeIdBibliotheques == null) {
      return null;
    }
    
    List<Bibliotheque> listeBibliotheques = new ArrayList<Bibliotheque>();
    for (IdBibliotheque id : listeIdBibliotheques) {
      Bibliotheque bibliotheque = new Bibliotheque(id, EnumTypeBibliotheque.NON_DEFINI);
      listeBibliotheques.add(bibliotheque);
    }
    return listeBibliotheques;
  }
  
  /**
   * Recherche les base de données en fonction du critère donnée.
   * Note : la description n'est pas chargée car la nouvelle méthode ne le permet pas et les performances étaient parfois désastreuses
   * avec l'ancienne.
   */
  public static ListeBDD chargerListeBaseDeDonnees(AS400 pSysteme, CritereBibliotheque pCriteres) {
    long td = System.currentTimeMillis();
    List<IdBibliotheque> listeIdBibliotheques = retournerListeBibliotheque(pSysteme, pCriteres);
    long tf = System.currentTimeMillis();
    Trace.debug("Temps pour lister des bases de données : " + (tf - td) + " ms");
    if (listeIdBibliotheques == null) {
      return null;
    }
    
    td = System.currentTimeMillis();
    ListeBDD listeBaseDeDonnees = new ListeBDD();
    for (IdBibliotheque id : listeIdBibliotheques) {
      BDD baseDeDonnees = new BDD(id, null);
      try {
        Version version = VersionManager.retournerVersionBaseDeDonnee(pSysteme, baseDeDonnees);
        baseDeDonnees.setVersion(version);
        listeBaseDeDonnees.add(baseDeDonnees);
      }
      catch (Exception e) {
        // Pas besoin d'afficher le message
      }
    }
    tf = System.currentTimeMillis();
    Trace.debug("Temps de recherche de la version des bases de données : " + (tf - td) + " ms");
    return listeBaseDeDonnees;
  }
  
  /**
   * Récupération de la description d'une bibliothèque.
   * Cette opération peut être très longue dans certains cas donc la méthode est appelée à la demande pour une bibliothèque.
   */
  public static String chargerDescription(AS400 pSysteme, IdBibliotheque pIdBibliotheque) {
    if (pIdBibliotheque == null || pIdBibliotheque.getNom() == null) {
      return null;
    }
    try {
      ObjectDescription od =
          new ObjectDescription(pSysteme, QSYSObjectPathName.toPath("QSYS", pIdBibliotheque.getNom(), TYPE_BIBLIOTHEQUE));
      od.refresh();
      return od.getValueAsString(ObjectDescription.TEXT_DESCRIPTION);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de récupération de la description de la bilbiothèque " + pIdBibliotheque.getNom());
    }
    return null;
  }
  
  /**
   * Contrôle le statut d'une base de données.
   * Si une anomalie est détectée et si des paramètres sont fournis alors une notification est générée.
   */
  public static EnumStatutBDD controlerStatut(AS400 pSysteme, QueryManager pQueryManager, StringBuilder pMessageErreurStatut) {
    if (pSysteme == null) {
      throw new MessageErreurException("La configuration système est incorrecte.");
    }
    if (pQueryManager == null) {
      throw new MessageErreurException("Le pQueryManager est incorrect.");
    }
    if (pQueryManager.getLibrary() == null) {
      throw new MessageErreurException("La bibliothèque est incorrecte.");
    }
    
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pQueryManager.getLibrary());
    Bibliotheque pBibliotheque = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    
    try {
      // Lecture de la dataarea
      Dataarea dataarea = new Dataarea();
      dataarea.setNom("PSEMDTA");
      dataarea.setBibliotheque(pBibliotheque);
      GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(pSysteme);
      String contenu = gestionDataareaAS400.chargerDataarea(dataarea);
      if (contenu == null) {
        throw new MessageErreurException("La dataarea " + pBibliotheque.getNom() + "/PSEMDTA est invalide.");
      }
      if (contenu.length() < 65) {
        throw new MessageErreurException(
            "La longueur de la dataarea " + pBibliotheque.getNom() + "/PSEMDTA est invalide : " + contenu.length() + '.');
      }
      
      // Récupération du statut
      EnumStatutBDD statut = EnumStatutBDD.valueOfByCode(contenu.charAt(0));
      
      // Mise à jour du message d'une notification si demandé
      if (pMessageErreurStatut != null) {
        if (statut != EnumStatutBDD.NORMAL && statut != EnumStatutBDD.NORMAL_ALTERNATIF) {
          // Récupération du message dans la dtaarea
          pMessageErreurStatut.setLength(0);
          pMessageErreurStatut.append(Constantes.normerTexte(contenu.substring(2)));
        }
      }
      
      return statut;
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne une liste d'identifiant de bibliothèque à partir des critères de recherche.
   */
  private static List<IdBibliotheque> retournerListeBibliotheque(AS400 pSysteme, CritereBibliotheque pCriteres) {
    List<IdBibliotheque> listeIdBibliotheques = new ArrayList<IdBibliotheque>();
    boolean testEgalite = false;
    String filtre = "";
    
    try {
      // Préparation du filtre pour le listage sur le nom des bibliothèques
      if (pCriteres != null) {
        filtre = Constantes.normerTexte(pCriteres.getFiltrePrefixe());
        int pos = filtre.lastIndexOf('*');
        if (pos != -1) {
          testEgalite = false;
          filtre = filtre.substring(0, pos);
        }
      }
      final boolean filtrage = !testEgalite;
      final String pattern = filtre.toUpperCase();
      IFSFile ifsFile = new IFSFile(pSysteme, "/QSYS.LIB");
      
      // Règles de filtrage sur le nom de la bibliothèque
      IFSFile[] liste = ifsFile.listFiles(new IFSFileFilter() {
        @Override
        public boolean accept(IFSFile object) {
          // Filtrage sur les bibliothèques uniquement
          if (!object.getName().endsWith(IdBibliotheque.EXTENSION)) {
            return false;
          }
          // Test d'égalité
          if (!filtrage) {
            return object.getName().equals(pattern);
          }
          // Sinon filtrage
          return object.getName().startsWith(pattern);
        }
      });
      
      // Construction d'une liste d'identifiant à partir de la liste des bibliothèques récupérées
      for (IFSFile library : liste) {
        String nom = library.getName();
        int pos = nom.lastIndexOf('.');
        if (pos != -1) {
          nom = nom.substring(0, pos);
        }
        listeIdBibliotheques.add(IdBibliotheque.getInstance(nom));
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    
    return listeIdBibliotheques;
  }
  
  // -- Accesseurs
  
  /**
   * Met à jour la bibliothèque.
   */
  public void setBibliotheque(Bibliotheque pBibliotheque) {
    if (pBibliotheque == null || systeme == null) {
      objetIfs = null;
      return;
    }
    bibliotheque = pBibliotheque;
    objetIfs = new IFSFile(systeme, QSYSObjectPathName.toPath("QSYS", bibliotheque.getNom(), TYPE_BIBLIOTHEQUE));
  }
  
  /**
   * Retourne le nom de la bibliothèque.
   */
  public String getName() {
    if (bibliotheque == null) {
      return null;
    }
    return bibliotheque.getNom();
  }
  
  /**
   * Met à jour le nom de la bibliothèque.
   */
  public void setName(String pNom) {
    pNom = Constantes.normerTexte(pNom);
    if (pNom.isEmpty()) {
      bibliotheque = null;
      objetIfs = null;
      return;
    }
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pNom);
    setBibliotheque(new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.NON_DEFINI));
  }
  
  public AS400 getSysteme() {
    return systeme;
  }
  
  public Bibliotheque getBibliotheque() {
    return bibliotheque;
  }
  
}
