/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.AS400;

import ri.serien.libas400.database.DatabaseManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Gère les connections au serveur AS400 et au serveur JDBC.
 */
public class SystemeManager {
  // Variables
  private AS400 systeme = null;
  private DatabaseManager databaseManager = null;
  private boolean connecterJDBC = false;
  private int qaqqini = DatabaseManager.NODEBUG;
  
  private Bibliotheque bibliothequeEnvironnement =
      new Bibliotheque(IdBibliotheque.getInstance("X_GPL"), EnumTypeBibliotheque.SYSTEME_SERIEN);
  private String curlib = "FMPRO";
  // Le profil de l'utilisateur en cours
  private String profilUtilisateur = null;
  
  // Conserve le dernier message d'erreur émit et non lu
  private String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public SystemeManager(boolean pConnexionJDBC, boolean pDriverNative) {
    connecterAS400();
    if (pConnexionJDBC) {
      connecterJDBC(pDriverNative);
    }
  }
  
  /**
   * Constructeur.
   */
  public SystemeManager(boolean pConnexionJDBC, boolean pDriverNative, int qaqqini) {
    connecterAS400();
    setQaqqini(qaqqini);
    if (pConnexionJDBC) {
      connecterJDBC(pDriverNative);
    }
  }
  
  /**
   * Constructeur.
   */
  public SystemeManager(AS400 pSysteme, boolean pConnexionJDBC, boolean pDriverNative) {
    setSystem(pSysteme);
    if (pConnexionJDBC) {
      connecterJDBC(pDriverNative);
    }
  }
  
  /**
   * Constructeur.
   */
  public SystemeManager(String pMachine, String pProfil, String pMdp, boolean pConnexionJDBC, boolean pDriverNative) {
    connecter(pMachine, pProfil, pMdp);
    if (pConnexionJDBC) {
      connecterJDBC(pProfil, pMdp, pDriverNative);
    }
  }
  
  /**
   * Constructeur.
   */
  public SystemeManager(boolean pConnexionJDBC) {
    this(pConnexionJDBC, true);
  }
  
  /**
   * Constructeur.
   */
  public SystemeManager(AS400 pSysteme, boolean pConnexionJDBC) {
    this(pSysteme, pConnexionJDBC, true);
  }
  
  /**
   * Constructeur.
   */
  public SystemeManager(String pMachine, String pProfil, String pMdp, boolean pConnexionJDBC) {
    this(pMachine, pProfil, pMdp, pConnexionJDBC, true);
  }
  
  // -- Méthodes publiques
  
  /**
   * Connexion du JDBC.
   */
  public void connecterJDBC(boolean pDriverNative) {
    Trace.debug("-connecterJDBC-> 2");
    if (connecterJDBC) {
      return;
    }
    if (systeme == null) {
      throw new MessageErreurException("La connexion au JDBC n'est pas possible car le système n'est pas connecté à l'AS400.");
    }
    
    // Connexion au JDBC
    databaseManager = new DatabaseManager(pDriverNative);
    databaseManager.setQaqqini(qaqqini);
    databaseManager.connexion();
    connecterJDBC = true;
  }
  
  /**
   * Connexion du JDBC.
   */
  public void connecterJDBC(String pProfil, String pMdp, boolean pDriverNative) {
    Trace.debug("-connecterJDBC-> 1");
    if (connecterJDBC) {
      return;
    }
    if (systeme == null) {
      throw new MessageErreurException("La connexion au JDBC n'est pas possible car le système n'est pas connecté à l'AS400.");
    }
    pProfil = Constantes.normerTexte(pProfil);
    if (pProfil.isEmpty()) {
      throw new MessageErreurException("Le profil pour la connexion au JDBC est invalide.");
    }
    pMdp = Constantes.normerTexte(pMdp);
    if (pMdp.isEmpty()) {
      throw new MessageErreurException("Le mot de passe pour la connexion au JDBC est invalide.");
    }
    
    // Connexion au JDBC
    databaseManager = new DatabaseManager(pDriverNative);
    databaseManager.setQaqqini(qaqqini);
    databaseManager.connexion(systeme.getSystemName(), pProfil, pMdp);
    connecterJDBC = true;
  }
  
  /**
   * Retourne si on est connecté au serveur.
   */
  public boolean isConnecte() {
    return systeme != null;
  }
  
  /**
   * Retourne si on est connecté au JDBC.
   */
  public boolean isJDBCconnecte() {
    if (databaseManager != null) {
      return connecterJDBC;
    }
    else {
      return false;
    }
  }
  
  /**
   * Déconnexion du serveur JDBC.
   */
  public void deconnecterJDBC() {
    // Déconnecte de la base de données si besoin
    if (databaseManager != null && connecterJDBC) {
      Trace.debug("-deconnecterJDBC()-> ");
      databaseManager.disconnect();
      connecterJDBC = false;
      databaseManager = null;
    }
  }
  
  /**
   * Déconnection du serveur.
   */
  public void deconnecter() {
    // Déconnexion du JDBC d'abord
    deconnecterJDBC();
    
    // Déconnection du serveur AS400
    if (systeme != null) {
      // Déconnecte l'ensemble des services
      systeme.disconnectAllServices();
      systeme = null;
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Connexion à l'AS400 sans identifiants.
   */
  private void connecterAS400() {
    systeme = new AS400();
  }
  
  /**
   * Connexion à l'AS/400.
   * Attention il peut y avoir des problèmes de lenteur de connexion sur certaines configurations AS/400-Réseaux
   * Par exemple chez Carrière de la Loire, si l'adresse vaut 127.0.0.1 c'est extrèmement lent donc on remplace avec localhost.
   * Voir SNC-5450
   */
  private void connecter(String pMachine, String pProfil, String pMdp) {
    // Contrôle de la validité des identifiants de l'utilisateur
    pMachine = Constantes.normerTexte(pMachine);
    if (pMachine.isEmpty()) {
      throw new MessageErreurException(
          "Le hostname ou l'adresse IP de la machine sur laquelle l'utilisateur tente de se connecter est invalide.");
    }
    pProfil = Constantes.normerTexte(pProfil);
    if (pProfil.isEmpty()) {
      throw new MessageErreurException("Le profil de l'utilisateur est invalide.");
    }
    pMdp = Constantes.normerTexte(pMdp);
    if (pMdp.isEmpty()) {
      throw new MessageErreurException("Le mot de passe de l'utilisateur est invalide.");
    }
    
    try {
      // Changement à la volée de l'adresse de la machine si elle vaut 127.0.0.1 par localhost
      // La raison exacte de la lenteur est ignorée mais ce correctif corrige le problème
      if (pMachine.equals("127.0.0.1") || pMachine.equalsIgnoreCase("*LOCAL")) {
        pMachine = "localhost";
      }
      // On tente de se connecter à l'AS/400
      Trace.debug("Début connexion machine:" + pMachine + " profil:" + pProfil);
      long t1 = System.currentTimeMillis();
      systeme = new AS400(pMachine, pProfil, pMdp);
      long t2 = System.currentTimeMillis() - t1;
      Trace.debug("Après instanciation de la classe AS400 (" + t2 + " ms).");
      
      // Contrôle la validité du profil/mot de passe
      t1 = System.currentTimeMillis();
      if (systeme.validateSignon()) {
        t2 = System.currentTimeMillis() - t1;
        Trace.debug("L'identification est valide (" + t2 + " ms).");
        if (t2 > 10000) {
          Trace.erreur(
              "Le contrôle de validité du profil a pris plus de 10 secondes. Il fort possible que la configuration réseau de l'AS400"
                  + " ne soit pas correcte, veuillez contrôler: ");
          Trace.erreur(" - La passerelle par défaut (CFGTCP option 2)");
          Trace.erreur(" - Le serveur DNS (CFGTCP option 12)");
          Trace.erreur(" - La configuration d'un nom de domaine (CFGTCP option 12)");
        }
      }
      else {
        systeme = null;
        throw new MessageErreurException("L'identification n'est pas valide.");
      }
    }
    catch (Exception e) {
      systeme = null;
      throw new MessageErreurException(e, "Erreur lors de la connexion.");
    }
  }
  
  // -- Accesseurs
  
  public void setSystem(AS400 pSysteme) {
    this.systeme = pSysteme;
  }
  
  public AS400 getSystem() {
    return systeme;
  }
  
  /**
   * Retourne le pointeur vers la base de données.
   */
  public void setDatabaseManager(DatabaseManager database) {
    this.databaseManager = database;
  }
  
  /**
   * Retourne le pointeur vers la base de données.
   */
  public DatabaseManager getDatabaseManager() {
    return databaseManager;
  }
  
  /**
   * Retourner la bibliothèque d'environnement.
   */
  public Bibliotheque getLibenv() {
    return bibliothequeEnvironnement;
  }
  
  /**
   * Initialiser la bibliothèque d'environnement.
   */
  public void setLibenv(Bibliotheque pBibliotheque) {
    this.bibliothequeEnvironnement = pBibliotheque;
  }
  
  /**
   * @return le curlib.
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir.
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * Initialise le Qaqqini (mode debug) à faire avant la connexion à la base.
   */
  public void setQaqqini(int qaqqini) {
    this.qaqqini = qaqqini;
  }
  
  public String getMsgError() {
    String chaine = msgErreur;
    msgErreur = "";
    return chaine;
  }
  
  public String getProfilUtilisateur() {
    if (profilUtilisateur == null) {
      return "";
    }
    return profilUtilisateur.toUpperCase();
  }
  
  public void setProfilUtilisateur(String profilUtilisateur) {
    this.profilUtilisateur = profilUtilisateur;
  }
  
}
