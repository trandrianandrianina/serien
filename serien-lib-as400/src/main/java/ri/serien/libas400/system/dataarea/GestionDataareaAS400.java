/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.dataarea;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.outils.MessageErreurException;

public class GestionDataareaAS400 {
  // Constantes
  public static final int TAILLE_MAX = 2000;
  // Variables
  private AS400 systeme = null;
  
  /**
   * Constructeur.
   */
  public GestionDataareaAS400(AS400 aSysteme) {
    systeme = aSysteme;
  }
  
  /**
   * Vérifie l'existence d'une dataarea donnée.
   */
  public boolean verifierExistenceDataarea(Dataarea pDataarea) {
    // Contrôle des variables
    if (pDataarea == null) {
      throw new MessageErreurException("Le paramètre aDataarea est à null.");
    }
    
    Bibliotheque bibliotheque =
        new Bibliotheque(IdBibliotheque.getInstance(pDataarea.getBibliotheque().getNom()), EnumTypeBibliotheque.NON_DEFINI);
    BibliothequeAS400 bibliothequeAS400 = new BibliothequeAS400(systeme, bibliotheque);
    if (!bibliothequeAS400.isExists()) {
      throw new MessageErreurException("La bibliothèque de la dataarea n'existe pas : " + bibliothequeAS400.getName());
    }
    
    final String racine = "/QSYS.LIB/" + pDataarea.getBibliotheque().getNom() + ".LIB/" + pDataarea.getNom() + ".DTAARA";
    final IFSFile dta = new IFSFile(systeme, racine);
    try {
      return dta.exists();
    }
    catch (IOException e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Créé une dataarea.
   */
  public boolean creerDataarea(Dataarea pDataarea) {
    // Vérification que la dataarea n'existe pas déjà
    if (verifierExistenceDataarea(pDataarea)) {
      return false;
    }
    
    // Sinon création
    CharacterDataArea dta =
        new CharacterDataArea(systeme, QSYSObjectPathName.toPath(pDataarea.getBibliotheque().getNom(), pDataarea.getNom(), "DTAARA"));
    try {
      if (pDataarea.getLongueur() < 0) {
        dta.create();
      }
      else {
        dta.create(pDataarea.getLongueur(), pDataarea.getValeur(), pDataarea.getDescription(), "*ALL");
      }
      return true;
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Lecture du texte de la dataarea.
   */
  public String chargerDataarea(Dataarea pDataarea) {
    // Vérification que la dataarea existe
    if (!verifierExistenceDataarea(pDataarea)) {
      return "";
    }
    
    CharacterDataArea dta =
        new CharacterDataArea(systeme, QSYSObjectPathName.toPath(pDataarea.getBibliotheque().getNom(), pDataarea.getNom(), "DTAARA"));
    try {
      pDataarea.setValeur(dta.read());
      return pDataarea.getValeur();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Ecriture du texte dans la dataarea.
   */
  public boolean ecrireDataarea(Dataarea pDataarea) {
    // Vérification que la dataarea existe
    if (!verifierExistenceDataarea(pDataarea)) {
      return false;
    }
    
    CharacterDataArea dta =
        new CharacterDataArea(systeme, QSYSObjectPathName.toPath(pDataarea.getBibliotheque().getNom(), pDataarea.getNom(), "DTAARA"));
    try {
      dta.write(pDataarea.getValeur());
      return true;
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
  }
}
