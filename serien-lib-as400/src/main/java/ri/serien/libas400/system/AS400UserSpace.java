/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.UserSpace;

/**
 * Gestion des UserSpace.
 */
public class AS400UserSpace extends UserSpace {
  // Constantes
  public static final int BLOCK_SIZE = 4096;
  
  // Variables
  private AS400 system = null;
  private String msgErreur = null;
  private String library = null;
  private String namespc = null;
  
  /**
   * Constructeur.
   */
  public AS400UserSpace(AS400 asystem, String lib, String aname) {
    super();
    system = asystem;
    setLibrary(lib);
    setNamespc(aname);
    QSYSObjectPathName sysobj = new QSYSObjectPathName(library, namespc, "USRSPC");
    try {
      setSystem(system);
      setPath(sysobj.getPath());
      // setMustUseNativeMethods(Constantes.isOs400()); // Commenté car plante si true (à voir plus
      // tard)
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
    }
  }
  
  /**
   * Création d'un user space.
   */
  public boolean create(int size, boolean replace) {
    if (library == null) {
      msgErreur += "\nLa bibliothèque est à null.";
      return false;
    }
    if (namespc == null) {
      msgErreur += "\nLe nom du userspace est à null.";
      return false;
    }
    if (system == null) {
      msgErreur += "\nLa variable system n'est pas initialisée.";
      return false;
    }
    
    try {
      create(size, replace, " ", (byte) 0x00, "UserSpace d'échange asynchrone", "*USE");
      return true;
    }
    catch (Exception e) {
      msgErreur += "\n" + e.getMessage();
    }
    
    return false;
  }
  
  /**
   * Lecture d'un bloc.
   */
  public String readBlock(int offset) {
    try {
      return read(offset, BLOCK_SIZE);
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
    }
    
    return null;
  }
  
  /**
   * Ecriture d'un bloc.
   */
  public boolean writeBlock(int offset, String data) {
    try {
      write(data, offset);
      return true;
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
    }
    
    return false;
  }
  
  /**
   * Redimensionne le userspace sans altérer le contenu.
   */
  public boolean resize(int newsize) {
    try {
      setLength(newsize);
      return true;
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
    }
    
    return false;
  }
  
  /**
   * @return le library.
   */
  public String getLibrary() {
    return library;
  }
  
  /**
   * @param library le library à définir.
   */
  public void setLibrary(String library) {
    this.library = library;
  }
  
  /**
   * @return le namespc.
   */
  public String getNamespc() {
    return namespc;
  }
  
  /**
   * @param namespc le namespc à définir.
   */
  public void setNamespc(String namespc) {
    this.namespc = namespc;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    // La récupération du message est à usage unique
    String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
