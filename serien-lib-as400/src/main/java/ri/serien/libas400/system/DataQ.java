/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.DataQueue;

import ri.serien.libcommun.outils.Trace;

/**
 * Gestion des Dataqueue.
 *
 * <p>La classe DataQ, qui hérite de la classe DataQueue du JT400, permet d'établir une connexion entre un AS400 et un
 * PC par le biais des objets AS400 "DataQ".
 */
public class DataQ extends DataQueue {
  // Constantes
  public static final String EXTENSION = "DTAQ";
  public static final int ETAT_LECTURE_ERREUR = -1;
  public static final int ETAT_LECTURE_NEUTRE = 0;
  public static final int ETAT_LECTURE_ATTENTE_DONNEES = 1;
  public static final int ETAT_LECTURE_DONNEES_LUES = 2;
  public static final int ETAT_LECTURE_DELAI_ATTENTE_ATTEINT = 3;
  
  // Variables
  private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
  private AS400 systeme;
  private String messagelu = null;
  private int etatLecture = ETAT_LECTURE_NEUTRE;
  
  /**
   * DataQ permet de construire l'objet DataQueue qui sera utilisé lors de la communication. Si la dataqueue spécifiée
   * n'existe pas elle est crée avec une taille maximale défini par longueur
   *
   * @param systeme Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilisée (l'appelant doit être
   *          cpnnecté).
   * @param pCheminIfs Chemin complet dans l'IFS de la DataQueue à creer (ou à utiliser). Le chemin doit donc imperativement
   *          se terminer par un objet de type DTAQ : mon_obj.DTAQ <BLOCKQUOTE><I>Note :</I> Si la Dataqueue spécifiée
   *          dans chemin n'existe pas elle sera crée.</BLOCKQUOTE>
   */
  public DataQ(AS400 pSysteme, String pCheminIfs) {
    super();
    try {
      systeme = pSysteme;
      this.setSystem(systeme);
      this.setPath(pCheminIfs);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la création d'une dataqueue");
    }
  }
  
  /**
   * Constructeur.
   */
  public DataQ(SystemeManager systememg, String chemin) {
    this(systememg.getSystem(), chemin);
  }
  
  /**
   * DataQ permet de construire l'objet DataQueue qui sera utilisé lors de la communication. Si la dataqueue spécifiée
   * n'existe pas elle est crée avec une taille maximale défini par longueur. Si la Dataqueue spécifiée dans chemin
   * existe elle sera vidée.
   *
   * @param pSysteme Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilisée (l'appelant doit être
   *          cpnnecté).
   * @param pCheminIfs Chemin complet dans l'IFS de la DataQueue à creer (ou à utiliser). Le chemin doit donc imperativement
   *          se terminer par un objet de type DTAQ : mon_obj.DTAQ
   * @param pLongueur Longueur de l'enregistrement de la DataQueue compris entre 1 et 64512
   * @param pDescription La description de l'objet
   * @param pDetruireSiExiste Si la dataqueue existe elle sera détruite si la valeur vaut true sinon elle sera clearée
   */
  public DataQ(AS400 pSysteme, String pCheminIfs, int pLongueur, String pDescription, boolean pDetruireSiExiste) {
    try {
      systeme = pSysteme;
      setSystem(systeme);
      setPath(pCheminIfs);
      // Si la dataq existe
      if (exists()) {
        // Soit elle est seulement vidée
        if (!pDetruireSiExiste) {
          clear();
          return;
        }
        // Soit elle est détruite
        delete();
      }
      // La dataq est créé
      create(pLongueur, "*ALL", false, true, true, pDescription);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la création d'une dataqueue");
    }
  }
  
  /**
   * Constructeur.
   */
  public DataQ(SystemeManager pSystemeManager, String pCheminIfs, int pLongueur, String pDescription, boolean pDetruireSiExiste) {
    this(pSystemeManager.getSystem(), pCheminIfs, pLongueur, pDescription, pDetruireSiExiste);
  }
  
  /**
   * DataQ permet de construire l'objet DataQueue qui sera utilisé lors de la communication. Si la dataqueue spécifiée
   * n'existe pas elle est crée avec une taille défini par longueur. Si la Dataqueue spécifiée dans chemin n'existe pas
   * elle sera crée.
   *
   * @param pHost Machine AS400 sur laquelle se trouve ou se trouvera la DataQueue utilisée..
   * @param pProfil Nom de l'utilisateur sur l'AS400.
   * @param pMotDePasse Mot de passe de l'utilisateur sur l'AS400.
   * @param pCheminIfs Chemin complet dans l'IFS de la DataQueue à creer (ou à utiliser). Le chemin doit donc imperativement
   *          se terminer par un objet de type DTAQ : mon_obj.DTAQ.
   * @param pLongueur Longueur de l'enregistrement de la DataQueue compris entre 1 et 64512.
   */
  public DataQ(String pHost, String pProfil, String pMotDePasse, String pCheminIfs, int pLongueur) {
    super();
    try {
      systeme = new AS400(pHost, pProfil, pMotDePasse);
      this.setSystem(systeme);
      this.setPath(pCheminIfs);
      if (this.exists()) {
        this.clear();
      }
      else {
        this.create(pLongueur);
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la construction d'une dataqueue");
    }
  }
  
  /**
   * writePackedDec permet d'écrire un nombre décimal dans la Dataqueue courante.
   *
   * @param taille contient le nombre de position de la valeur à écrire.<br> Par exemple 1956 => 4
   * @param nbdec contient le nombre de décimale de la valeur à écrire.<br> Par exemple 145.65 => 2
   * @param val contien la valeur à écrire dans la Dataqueue.
   */
  public boolean writePackedDec(int taille, int nbdec, double val) {
    boolean res;
    
    try {
      AS400PackedDecimal converter = new AS400PackedDecimal(taille, nbdec);
      byte[] tabBytes = converter.toBytes(val);
      this.write(tabBytes);
      res = true;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'écriture dans une dataqueue");
      res = false;
    }
    return res;
  }
  
  /**
   * readPackedDec permet de lire un nombre décimal dans la Dataqueue courante.
   *
   * <p>Important : les parmaètres 'taille' et 'nbdec' doivent correspondrent exactement à la structure de la zone
   * AS400.
   *
   * @param taille contient le nombre de position de la valeur à lire.<br> Par exemple 1956 => 4.
   * @param nbdec contient le nombre de décimale de la valeur à lire.<br> Par exemple 145.65 => 2.
   *
   */
  public double readPackedDec(int taille, int nbdec) {
    double ret;
    
    try {
      byte[] val = this.read().getData();
      AS400PackedDecimal converter = new AS400PackedDecimal(taille, nbdec);
      ret = converter.toDouble(val);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la lecture dans une dataqueue");
      ret = -1;
    }
    return ret;
  }
  
  /**
   * Permet de lire une chaine de caractères dans la Dataqueue courante.
   * https://stackoverflow.com/questions/16317554/jtopen-keyeddataqueue-read-timeout
   */
  public String readString(int attente) {
    etatLecture = ETAT_LECTURE_ATTENTE_DONNEES;
    try {
      String chaine = read(attente).getString();
      etatLecture = ETAT_LECTURE_DONNEES_LUES;
      return chaine;
    }
    catch (NullPointerException e) {
      // C'est ici que l'on attrape de dépassement du délai
      try {
        if (!exists()) {
          etatLecture = ETAT_LECTURE_ERREUR;
          Trace.erreur(e, "La dataqueue " + getName() + " n'existe pas");
        }
        else {
          etatLecture = ETAT_LECTURE_DELAI_ATTENTE_ATTEINT;
          Trace.info("Délai d'attente atteint lors de lecture de la dataqueue : " + attente + " sec");
        }
      }
      catch (Exception e1) {
        etatLecture = ETAT_LECTURE_ERREUR;
        Trace.erreur(e1, "Erreur lors de la lecture de la dataqueue");
      }
    }
    catch (Exception e) {
      etatLecture = ETAT_LECTURE_ERREUR;
      Trace.erreur(e, "Erreur lors de la lecture de la dataqueue");
    }
    return null;
  }
  
  /**
   * Permet de lire une chaine de caractères dans la Dataqueue courante.
   */
  public void readMessage(int attente) {
    setMessageLu(readString(attente));
  }
  
  /**
   * writeString permet d'écrire une chaine de caractères dans la Dataqueue courante.
   *
   * @param chaine chaine à écrire dans la Dataqueue. <BLOCKQUOTE><I>Note :</I> La chaine ne pourra pas dépasser 256
   *          caracteres.</BLOCKQUOTE>
   */
  public boolean writeString(String chaine) {
    if (chaine == null) {
      return false;
    }
    boolean retour = true;
    
    try {
      AS400Text converter = new AS400Text(chaine.length(), systeme);
      byte[] tab = converter.toBytes(chaine.trim());
      this.write(tab);
    }
    catch (Exception e) {
      Trace.alerte("Erreur lors de l'écriture dans la dataqueue");
      retour = false;
    }
    return retour;
  }
  
  /**
   * writeString permet d'écrire une chaine de caractères dans la Dataqueue courante en spécifiant la taille de la
   * chaine.
   *
   * @param chaine chaine à écrire dans la Dataqueue.
   * @param taille longueur de la chaine.
   */
  public boolean writeString(String chaine, int taille) {
    if (chaine == null) {
      return false;
    }
    try {
      AS400Text converter = new AS400Text(taille, systeme);
      byte[] tableau = converter.toBytes(chaine);
      this.write(tableau);
      return true;
    }
    catch (Exception e) {
      Trace.alerte("Erreur lors de l'écriture dans la dataqueue");
      return false;
    }
  }
  
  /**
   * write_fromfile permet d'écrire dans la dataqueue courante le contenu du fichier passé en paramamètre. Chaque ligne
   * du fichier constitue une entrée dans la dataqueue.
   *
   * @param chemin chemin du fichier sur votre disque dur. Le chemin doit comporter le séparateur final.
   * @param nomfichier nom du fichier qui doit etre copié dans la dataqueue.
   * @return si 0 tout c'est bien passé, erreur sinon.
   */
  public int write_fromfile(String chemin, String nomfichier) {
    int ret;
    BufferedReader in = null;
    try {
      String string;
      in = new BufferedReader(new FileReader(chemin + nomfichier));
      
      while ((string = in.readLine()) != null) {
        // Ecriture d'une chaine
        AS400Text converter = new AS400Text(256, systeme);
        byte[] tab = converter.toBytes(string.trim());
        this.write(tab);
      }
      ret = 0;
    }
    catch (FileNotFoundException fnfe) {
      Trace.erreur(fnfe, "Erreur lors de l'écriture dans une dataqueue");
      ret = 1;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'écriture dans une dataqueue");
      ret = 2;
    }
    finally {
      try {
        if (in != null) {
          in.close();
        }
      }
      catch (IOException e) {
        Trace.erreur(e, "Erreur lors de l'écriture dans une dataqueue");
      }
    }
    return ret;
  }
  
  /**
   * Methode qui ecrit toutes les entrées de la dataQ dans un fichier (une ligne par entrée).
   */
  public int read_tofile(String chemin, String nomfichier) {
    FileWriter sortie = null;
    int ret = 0;
    
    try {
      File destFile = new File(new File(chemin).getPath());
      if (!destFile.exists()) {
        destFile.mkdirs();
      }
      
      while (true) {
        sortie = new FileWriter(chemin + File.separator + nomfichier, true);
        String string = this.read().getString();
        sortie.write(string + "\n");
        sortie.flush();
        sortie.close();
      }
    }
    catch (NullPointerException npe) {
      Trace.erreur(npe, "Erreur lors de la lecture d'une dataqueue");
      try {
        if (sortie != null) {
          sortie.close();
        }
      }
      catch (IOException ioe) {
        Trace.erreur(ioe, "Erreur lors de la lecture d'une dataqueue");
      }
      ret = 0;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la lecture d'une dataqueue");
      try {
        if (sortie != null) {
          sortie.close();
        }
      }
      catch (IOException ioe) {
        Trace.erreur(ioe, "Erreur lors de la lecture d'une dataqueue");
      }
      ret = 2;
    }
    return ret;
  }
  
  /**
   * @param msglu the messagelu to set.
   */
  public void setMessageLu(String msglu) {
    String oldMsgLu = messagelu;
    messagelu = msglu;
    pcs.firePropertyChange("dqRead", oldMsgLu, messagelu);
  }
  
  public String getMessageLu() {
    return messagelu;
  }
  
  /**
   * Enlever les listener.
   */
  public void close() {
    PropertyChangeListener[] liste = pcs.getPropertyChangeListeners();
    if (liste != null) {
      for (PropertyChangeListener l : liste) {
        pcs.removePropertyChangeListener(l);
      }
    }
  }
  
  /**
   * Add a PropertyChangeListener for bound property using the services of the PropertyChangeSupport class.
   */
  @Override
  public void addPropertyChangeListener(PropertyChangeListener listener) {
    this.pcs.addPropertyChangeListener(listener);
  }
  
  /**
   * Remove a PropertyChangeListener using the services of the PropertyChangeSupport class.
   */
  @Override
  public void removePropertyChangeListener(PropertyChangeListener listener) {
    this.pcs.removePropertyChangeListener(listener);
  }
  
  public int getEtatLecture() {
    return etatLecture;
  }
}
