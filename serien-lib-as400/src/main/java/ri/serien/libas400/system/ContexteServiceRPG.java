/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe permet de gérer les appels aux programmes RPG qui sont "spécialisés" en tant que service.
 */
public class ContexteServiceRPG extends ContexteProgrammeRPG {
  // Variables
  private SystemeManager systemManager = null;
  
  /**
   * Constructeur.
   */
  public ContexteServiceRPG(SystemeManager pSystemManager) {
    super(pSystemManager.getSystem());
    systemManager = pSystemManager;
  }
  
  /**
   * Initialise l'environnement d'exécution avant d'exécuter le service.
   */
  public void initialiserEnvironnement(Bibliotheque... pBibliotheque) {
    if (systemManager == null) {
      throw new MessageErreurException("Erreur lors de l'initialisation de l'environnement");
    }
    initialiserLettreEnvironnement(EnvironnementExecution.getLettreEnvironnement());
    initialiserProfil(systemManager.getProfilUtilisateur());
    initialiserCodeAffichageSerieN();
    
    // On ajoute la bibliothèque contenant les fichiers (la curlib) en premier
    ajouterBibliotheque(systemManager.getCurlib(), true);
    // On ajoute la bibliothèque d'environnement en second
    ajouterBibliotheque(systemManager.getLibenv(), false);
    // Puis les bibliothèques "métiers"
    for (Bibliotheque bibliotheque : pBibliotheque) {
      ajouterBibliotheque(bibliotheque, false);
    }
  }
}
