/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.edition;

import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.Printer;
import com.ibm.as400.access.PrinterList;

import ri.serien.libcommun.exploitation.edition.imprimante.CritereImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.EnumEtatImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.IdImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.Imprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.ListeImprimante;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gestion des imprimantes sur un AS/400
 * @author Stephan Veneri
 *
 */
public class GestionImprimanteAS400 {
  // Variables
  private AS400 systeme = null;
  
  private String messageErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur
   */
  public GestionImprimanteAS400() {
    systeme = new AS400();
  }
  
  /**
   * Constructeur
   * @param pSysteme
   */
  public GestionImprimanteAS400(AS400 pSysteme) {
    if (pSysteme == null) {
      this.systeme = new AS400();
    }
    else {
      this.systeme = pSysteme;
    }
  }
  
  /**
   * Constructeur
   * @param pHost
   * @param pUser
   * @param pMotDePasse
   */
  public GestionImprimanteAS400(String pHost, String pUser, String pMotDePasse) {
    systeme = new AS400(pHost, pUser, pMotDePasse);
  }
  
  /**
   * Déconnecte la session
   */
  public void deconnecter() {
    if (systeme != null) {
      systeme.disconnectAllServices();
    }
    systeme = null;
  }
  
  /**
   * Retourne la valeur de système
   * @return
   */
  public AS400 getSystem() {
    if (systeme == null) {
      systeme = new AS400();
    }
    return systeme;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    // La récupération du message est à usage unique
    String chaine = messageErreur;
    messageErreur = "";
    
    return chaine;
  }
  
  /**
   * Charger une liste d'identifiants d'imprimante à partir d'un critère de recherche.
   * Actuellement aucun critère de recherche n'est implémenté.
   * 
   * @param pCritereImprimante Critère de recherche pour la liste d'IdImprimante.
   * @return Liste d'IdImprimante.
   */
  public List<IdImprimante> chargerListeIdImprimante(CritereImprimante pCritereImprimante) {
    PrinterList listePrinter = new PrinterList(systeme);
    List<IdImprimante> listeIdImprimante = new ArrayList<IdImprimante>();
    try {
      listePrinter.openSynchronously();
      for (int i = 0; i < listePrinter.size(); i++) {
        Printer printer = (Printer) listePrinter.getObject(i);
        if (printer != null) {
          IdImprimante idImprimante = IdImprimante.getInstance(printer.getName(), printer.getStringAttribute(Printer.ATTR_OUTPUT_QUEUE));
          listeIdImprimante.add(idImprimante);
        }
      }
    }
    catch (Exception e) {
      listeIdImprimante = null;
      throw new MessageErreurException(e, "Erreur lors de la récupération de la liste des imprimantes.");
    }
    finally {
      listePrinter.close();
    }
    return listeIdImprimante;
  }
  
  /**
   * Charger une liste d'Imprimante à partir d'une liste d'identifiant d'imprimante.
   * 
   * @param pListeIdImprimante Liste d'IdImprimante pour charger la liste d'Imprimante.
   * @return Liste d'Imprimante.
   */
  public ListeImprimante chargerListeImprimante(List<IdImprimante> pListeIdImprimante) {
    if (pListeIdImprimante == null) {
      return null;
    }
    ListeImprimante listeImprimante = new ListeImprimante();
    for (IdImprimante idImprimante : pListeIdImprimante) {
      if (idImprimante != null) {
        Printer printer = new Printer(systeme, idImprimante.getTexte());
        try {
          Imprimante imprimante = new Imprimante(idImprimante);
          imprimante.setEtatImprimante(EnumEtatImprimante.valueOfByValeur(printer.getStringAttribute(Printer.ATTR_WTRSTRTD)));
          
          listeImprimante.add(imprimante);
        }
        catch (Exception e) {
          throw new MessageErreurException(e, "Erreur lors de la récupération de l'imprimante " + idImprimante.getTexte() + ".");
        }
      }
    }
    return listeImprimante;
  }
}
