/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.systemrequest;

import java.io.IOException;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libcommun.outils.protocolemessage.DataareaRequest;
import ri.serien.libcommun.outils.protocolemessage.LibraryRequest;

public class DataareaRequestActions {
  private AS400 sysAS400 = null;
  private DataareaRequest rdta = null;
  
  /**
   * Constructeur.
   */
  public DataareaRequestActions(AS400 system, DataareaRequest ld) {
    sysAS400 = system;
    rdta = ld;
  }
  
  /**
   * Traitement des actions possibles.
   */
  public void actions() {
    // int theactions = actions;
    for (int i = LibraryRequest.ACTIONS.size(); --i > 0;) {
      if (rdta.getActions() >= LibraryRequest.ACTIONS.get(i)) {
        switch (LibraryRequest.ACTIONS.get(i)) {
          case DataareaRequest.ISEXISTS:
            dataareaExists();
            break;
          case DataareaRequest.CREATE:
            dataareaCreate();
            break;
          case DataareaRequest.READ:
            dataareaRead();
            break;
          case DataareaRequest.WRITE:
            dataareaWrite();
            break;
          default:
            break;
        }
        rdta.setActions(rdta.getActions() - LibraryRequest.ACTIONS.get(i));
      }
    }
  }
  
  /**
   * Traite la demande de vérification d'une dataarea.
   */
  private void dataareaExists() {
    // Contrôle des variables
    String dataarea = rdta.getDataarea();
    if ((dataarea == null) || (dataarea.length() > 10)) {
      rdta.setSuccess(false);
      rdta.setExist(false);
      rdta.setMsgError("Le nom de la dataarea est incorrect : " + dataarea);
      return;
    }
    String library = rdta.getLibrary();
    if ((library == null) || (library.length() > 10)) {
      rdta.setSuccess(false);
      rdta.setExist(false);
      rdta.setMsgError("Le nom de la bibliothèque est incorrect : " + library);
      return;
    }
    
    final String racine = "/QSYS.LIB/" + library + ".LIB/" + dataarea + ".DTAARA";
    final IFSFile dta = new IFSFile(sysAS400, racine);
    try {
      rdta.setSuccess(true);
      rdta.setExist(dta.exists());
      rdta.setMsgError("La dataarea " + dataarea + " existe.");
    }
    catch (IOException e) {
      rdta.setSuccess(false);
      rdta.setExist(false);
      rdta.setMsgError(e.getMessage());
    }
  }
  
  /**
   * Traite la création d'une bibliothèque.
   */
  private void dataareaCreate() {
    String dataarea = rdta.getDataarea();
    String library = rdta.getLibrary();
    
    // On vérifie que la library n'existe pas déjà
    dataareaExists();
    if (rdta.isExist()) {
      rdta.setSuccess(true);
      rdta.setMsgError("La dataarea " + dataarea + " existe déjà dans " + library);
      return;
    }
    
    // Sinon on l'a créé
    CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
    try {
      dta.create();
      rdta.setMsgError(null);
      rdta.setSuccess(true);
    }
    catch (Exception e) {
      rdta.setSuccess(false);
      rdta.setMsgError(e.getMessage());
    }
  }
  
  /**
   * Traite la demande de récupération du texte de la dataarea.
   */
  private void dataareaRead() {
    String dataarea = rdta.getDataarea();
    String library = rdta.getLibrary();
    
    // On vérifie que la library n'existe pas déjà
    dataareaExists();
    if (!rdta.isExist()) {
      rdta.setSuccess(true);
      rdta.setData(null);
      return;
    }
    
    CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
    try {
      rdta.setSuccess(true);
      rdta.setMsgError(null);
      rdta.setData(dta.read());
    }
    catch (Exception e) {
      rdta.setSuccess(false);
      rdta.setMsgError(e.getMessage());
      rdta.setData(null);
    }
  }
  
  /**
   * Traite la demande de récupération du texte de la dataarea.
   */
  private void dataareaWrite() {
    String dataarea = rdta.getDataarea();
    String library = rdta.getLibrary();
    
    // On vérifie que la library n'existe pas déjà
    dataareaExists();
    if (!rdta.isExist()) {
      rdta.setSuccess(true);
      return;
    }
    
    CharacterDataArea dta = new CharacterDataArea(sysAS400, QSYSObjectPathName.toPath(library, dataarea, "DTAARA"));
    try {
      dta.write(rdta.getData());
      rdta.setSuccess(true);
      rdta.setMsgError(null);
    }
    catch (Exception e) {
      rdta.setSuccess(false);
      rdta.setMsgError(e.getMessage());
    }
  }
}
