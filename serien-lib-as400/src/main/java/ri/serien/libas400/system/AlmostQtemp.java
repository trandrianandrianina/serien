/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.AS400;

import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;

/**
 * Gestion de la bibliothèque temporaire (équivalent de la QTEMP).
 */
public class AlmostQtemp extends BibliothequeAS400 {
  // Constantes
  private static final String PREFIX = "TMP";
  
  // Variables
  private int numero = 0;
  private int compteur = 0;
  
  /**
   * Constructeur.
   */
  public AlmostQtemp(AS400 pSysteme, int pNumero) {
    super(pSysteme);
    numero = pNumero;
    createName();
  }
  
  /**
   * Créer le nom de la bibliothèque.
   * Le numero est une valeur comprise entre 0 et 99999.
   */
  private void createName() {
    if (numero < 0) {
      numero = Math.abs(numero);
    }
    setName(String.format("%s%05d%02d", PREFIX, numero, compteur));
  }
  
  /**
   * Créer la bibliothèque si elle n'existe pas.
   */
  @Override
  public boolean create() {
    boolean stop = false;
    boolean ret = false;
    do {
      if (!isExists()) {
        ret = createWOCheckExists();
        stop = ret;
      }
      else {
        compteur++;
        if (compteur < 100) {
          createName();
        }
        else {
          stop = true;
        }
      }
    }
    while (!stop);
    
    return ret;
  }
  
}
