/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.job;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.ibm.as400.access.AS400Bin4;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.Job;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.Avertissement;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.mail.ParametreCreationMail;
import ri.serien.libcommun.exploitation.notification.EnumPrioriteNotification;
import ri.serien.libcommun.exploitation.notification.ListeDestinataireNotification;
import ri.serien.libcommun.exploitation.notification.ParametreCreationNotification;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe permet la surveillance de job lancés sur l'AS/400 et des temps de traitement du gestionnaire de flux.
 * Si un job est en MSGW ou LCKW :
 * - une notification est envoyé,
 * - un mail est envoyé si l'établissement peut être renseigné,
 * - un mail à RI est envoyé.
 * 
 * En théorie, tous les job peuvent être surveillés, ils ne doivent pas être obligatoirement liés au flux. Sauf qu'à l'heure actuelle, la
 * surveillance n'est lancé que par le gestionnaire de flux. Mais pour le rendre générique peu de modifications sont à envisager.
 */
public class SurveillanceJob extends Thread {
  // Constantes
  private final static int DELAI_ATTENTE_EN_SECONDE = 10;
  public final static int TEMPS_EXECUTION_MAX_EN_SECONDE = 5 * 60;
  
  // Variables
  private int delaiAttenteEnSeconde = DELAI_ATTENTE_EN_SECONDE;
  private Bibliotheque bibliothequeEnvironnement = null;
  // Liste des destinataires sous forme de chaine avec éléments séparés par virgule, point-virgule ou espace
  private HashMap<Bibliotheque, ListeDestinataireNotification> mapListeDestinataireNotification =
      new HashMap<Bibliotheque, ListeDestinataireNotification>();
  private HashMap<Bibliotheque, String> mapListeDestinataireMail = new HashMap<Bibliotheque, String>();
  // Liste contenant les jobs à surveiller
  private List<JobASurveiller> listeJobASurveiller = new ArrayList<JobASurveiller>();
  private List<ManagerFluxMagentoASurveiller> listeManagerFluxMagentoASurveiller = null;
  private SystemeManager systemeManager = null;
  private QueryManager queryManager = null;
  private ProgramCall programme = null;
  private boolean boucleSansFin = true;
  
  /**
   * Constructeur.
   */
  public SurveillanceJob(Bibliotheque pBibliothequeEnvironnement) {
    bibliothequeEnvironnement = pBibliothequeEnvironnement;
  }
  
  // -- Méthodes publiques
  
  /**
   * Démarrage de la surveillance.
   */
  public boolean demarrer() {
    systemeManager = new SystemeManager(true);
    systemeManager.setLibenv(bibliothequeEnvironnement);
    queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    queryManager.setLibrairieEnvironnement(bibliothequeEnvironnement.getNom());
    programme = new ProgramCall(systemeManager.getSystem());
    
    Trace.info("Démarrer la surveillance des jobs.");
    start();
    
    return true;
  }
  
  /**
   * Arrêt de la surveillance.
   */
  public void arreter() {
    if (systemeManager != null) {
      systemeManager.deconnecter();
    }
    Trace.info("Arrêter la surveillance des jobs.");
    // Thread.currentThread().interrupt(); <-- N'arrête pas le thread
    boucleSansFin = false;
  }
  
  /**
   * Traitements à effectuer.
   */
  @Override
  public void run() {
    String nomThread = String.format("SurveillanceJob(%03d)-surveiller-", Thread.currentThread().getId());
    setName(nomThread);
    boucleSansFin = true;
    
    while (boucleSansFin) {
      try {
        listerJob();
        // Parcourt la liste des jobs à surveiller
        Iterator<JobASurveiller> iJobASurveiller = listeJobASurveiller.iterator();
        while (iJobASurveiller.hasNext()) {
          JobASurveiller jobASurveiller = iJobASurveiller.next();
          
          // Contrôle du statut du job
          controlerStatutJob(jobASurveiller);
          
          // Le job est inactif
          if (!jobASurveiller.isActif()) {
            iJobASurveiller.remove();
            Trace.info("Le job " + jobASurveiller.getJob() + " a été enlevé de la liste de surveillance car il n'est plus actif.");
            continue;
          }
          
          // Problème détecté donc une notification est créée et un mail envoyé s'il n'a pas déjà été envoyé
          if (jobASurveiller.getStatut() != EnumStatutJob.NON_DEFINI && !jobASurveiller.isAvertissementEnvoye()) {
            if (listeManagerFluxMagentoASurveiller != null) {
              signalerProblemeJobFlux(jobASurveiller);
            }
            if (listeJobASurveiller != null) {
              signalerProblemeJobRPG(jobASurveiller);
              if (jobASurveiller.isAvertissementEnvoye()) {
                Thread.currentThread().interrupt();
                
                break;
              }
            }
          }
        }
        
        // Parcourt la liste des manager de flux afin de contrôler les temps de traitement
        controlerTempsTraitementFlux();
        
        // Pause durant le délai imparti
        Thread.sleep(delaiAttenteEnSeconde * 1000);
      }
      catch (InterruptedException ie) {
        Thread.currentThread().interrupt();
        Trace.alerte("Le thread de surveillance des jobs (" + getName() + ") a été arrêté.");
        break;
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
  }
  
  /**
   * Ajout d'un job à surveiller à la liste.
   */
  public void ajouterJob(JobASurveiller pJobASurveiller) {
    if (pJobASurveiller == null) {
      return;
    }
    Job job = pJobASurveiller.getJob();
    for (JobASurveiller jobASurveiller : listeJobASurveiller) {
      if (jobASurveiller.getJob().getNumber().equals(job.getNumber()) && jobASurveiller.getJob().getUser().equals(job.getUser())
          && jobASurveiller.getJob().getName().equals(job.getName())) {
        return;
      }
    }
    listeJobASurveiller.add(pJobASurveiller);
    Trace.info("Le job " + pJobASurveiller.getJob() + " a été ajouté à la liste de surveillance.");
  }
  
  /**
   * Enlève un job à surveiller de la liste.
   */
  public void enleverJob(Job pJob) {
    if (pJob == null) {
      return;
    }
    for (JobASurveiller jobASurveiller : listeJobASurveiller) {
      if (jobASurveiller.getJob().getNumber().equals(pJob.getNumber()) && jobASurveiller.getJob().getUser().equals(pJob.getUser())
          && jobASurveiller.getJob().getName().equals(pJob.getName())) {
        listeJobASurveiller.remove(jobASurveiller);
        Trace.info("Le job " + pJob + " a été enlevé de la liste de surveillance.");
        return;
      }
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Liste les jobs en cours de surveillance.
   */
  private void listerJob() {
    Trace.info("Liste des jobs en cours de surveillance (" + listeJobASurveiller.size() + ") : ");
    for (JobASurveiller jobASurveiller : listeJobASurveiller) {
      Trace.info(" - " + jobASurveiller.getJob());
    }
  }
  
  /**
   * Contrôler le statut du job.
   * En utilisant l'API AS400 directement car appeler les méthodes de la classe Job sur un job en erreur bloque le traitement.
   */
  private void controlerStatutJob(JobASurveiller pJobASurveiller) {
    Trace.info("Contrôle du job " + pJobASurveiller.getJob());
    
    // Create the various converters we need
    AS400Bin4 bin4Converter = new AS400Bin4();
    AS400Text text26Converter = new AS400Text(26, systemeManager.getSystem());
    AS400Text text16Converter = new AS400Text(16, systemeManager.getSystem());
    AS400Text text10Converter = new AS400Text(10, systemeManager.getSystem());
    AS400Text text8Converter = new AS400Text(8, systemeManager.getSystem());
    AS400Text text6Converter = new AS400Text(6, systemeManager.getSystem());
    AS400Text text4Converter = new AS400Text(4, systemeManager.getSystem());
    
    try {
      // The server program we call has five parameters
      ProgramParameter[] parmlist = new ProgramParameter[5];
      
      // The first parm is a byte array that holds the output data. We will allocate a 1k buffer for output data.
      parmlist[0] = new ProgramParameter(1024);
      
      // The second parm is the size of our output data buffer (1K).
      Integer iStatusLength = Integer.valueOf(1024);
      byte[] statusLength = bin4Converter.toBytes(iStatusLength);
      parmlist[1] = new ProgramParameter(statusLength);
      
      // The third parm is the name of the format of the data.
      // We will use format JOBI0200 because it has job status.
      byte[] statusFormat = text8Converter.toBytes("JOBI0200");
      parmlist[2] = new ProgramParameter(statusFormat);
      
      // The fourth parm is the job name is format "name user number".
      // Name must be 10 characters, user must be 10 characters and number must be 6 characters. We will use a text converter to do the
      // conversion and padding.
      Job job = pJobASurveiller.getJob();
      byte[] jobName = text26Converter.toBytes(job.getName());
      text10Converter.toBytes(job.getUser(), jobName, 10);
      text6Converter.toBytes(job.getNumber(), jobName, 20);
      
      parmlist[3] = new ProgramParameter(jobName);
      
      // The last paramter is job identifier. We will leave this blank.
      byte[] jobId = text16Converter.toBytes("                ");
      parmlist[4] = new ProgramParameter(jobId);
      
      // Run the program.
      if (programme.run("/QSYS.LIB/QUSRJOBI.PGM", parmlist) == false) {
        // if the program failed display the error message.
        AS400Message[] msgList = programme.getMessageList();
        Trace.erreur("Erreur lors du contrôle de " + job + " : " + msgList[0].getText());
      }
      else {
        // else the program worked. Output the status followed by the jobName.user.jobID
        byte[] as400Data = parmlist[0].getOutputData();
        String codeStatut = Constantes.normerTexte((String) text4Converter.toObject(as400Data, 107));
        // Le job est non actif (n'existe plus dans le WRKACTJOB)
        if (codeStatut.isEmpty()) {
          pJobASurveiller.setActif(false);
          pJobASurveiller.setStatut(EnumStatutJob.NON_DEFINI);
        }
        // Statut en erreur
        if (codeStatut.equals(EnumStatutJob.ERREUR.getCode())) {
          pJobASurveiller.setStatut(EnumStatutJob.ERREUR);
        }
        // Statut verrouillé
        else if (codeStatut.equals(EnumStatutJob.VERROUILLE.getCode())) {
          pJobASurveiller.setStatut(EnumStatutJob.VERROUILLE);
        }
        // Statut par défaut (tous les autres états et qui sont non gérés l'heure actuelle)
        else {
          pJobASurveiller.setStatut(EnumStatutJob.NON_DEFINI);
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  /**
   * Signale aux personnes désignées qu'un programme RPG/CL rencontre un problème.
   * Un mail sera envoyé, ce qui n'est pas le cas pour les autres jobs à cause de l'établisement manaquant.
   */
  private void signalerProblemeJobFlux(JobASurveiller pJobASurveiller) {
    if (pJobASurveiller == null) {
      Trace.erreur("Le job à surveiller est invalide, impossible de signaler le problème.");
      return;
    }
    
    // Initialisation de la base de données du queryManager
    Bibliotheque baseDeDonnees = null;
    if (pJobASurveiller.getBaseDeDonnees() != null) {
      queryManager.setLibrary(pJobASurveiller.getBaseDeDonnees().getId());
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(queryManager.getLibrary());
      baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    }
    
    // Préparer le sujet et le message
    String sujet = "Une anomalie sur un programme a été détectée";
    if (baseDeDonnees != null) {
      sujet += " (" + baseDeDonnees.getNom() + ')';
    }
    String message = pJobASurveiller.recupererMessage();
    
    // Préparation pour la notification
    try {
      // Initialisation des paramètres
      ParametreCreationNotification parametreCreationNotification = new ParametreCreationNotification();
      parametreCreationNotification.setPriorite(EnumPrioriteNotification.IMPORTANTE);
      parametreCreationNotification.setSujet(sujet);
      parametreCreationNotification.setMessage(message);
      ListeDestinataireNotification listeDestinataire = mapListeDestinataireNotification.get(baseDeDonnees);
      parametreCreationNotification.setListeDestinataire(listeDestinataire);
      
      // Envoi de la notification
      Avertissement.envoyerNotification(systemeManager, queryManager, parametreCreationNotification);
    }
    catch (Exception e) {
    }
    
    // Préparation pour le mail
    // Uniquement s'il s'agit d'un flux à cause de la récupération de l'établissement qui est "impossible lors de l'exécution normale",
    // une exception sera déclenché si non flux
    if (baseDeDonnees == null) {
      Trace.alerte(
          "Le mail d'avertissement concernant la surveillance des jobs ne peut pas être envoyé car la base de données n'est pas renseignée.");
      return;
    }
    try {
      // Récupération du code établissement
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.ajouter("select INETB from QGPL." + EnumTableBDD.FLUX_LIEN_INSTANCE + " where ");
      requeteSql.ajouterConditionAnd("INBIB", "=", queryManager.getLibrary());
      ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
      if (listeRecord == null || listeRecord.isEmpty()) {
        throw new MessageErreurException(
            "Aucun code établissement n'a été trouvé pour la base de données " + queryManager.getLibrary() + '.');
      }
      String codeEtablissement = Constantes.normerTexte(listeRecord.get(0).getStringValue("INETB"));
      IdEtablissement idEtablissement = IdEtablissement.getInstance(codeEtablissement);
      
      // Initialisation des paramètres
      ParametreCreationMail parametreCreationMail = new ParametreCreationMail();
      parametreCreationMail.setIdEtablissement(idEtablissement);
      parametreCreationMail.setSujet(sujet);
      parametreCreationMail.setMessage(message);
      // Contrôle de la présence de destinataires
      String listeDestinataire = mapListeDestinataireMail.get(baseDeDonnees);
      if (Constantes.normerTexte(listeDestinataire).isEmpty()) {
        throw new MessageErreurException("Le mail pour notifier que \"" + sujet
            + "\" ne peut pas être envoyé car aucun destinataire des mails n'a été paramétré dans la table " + EnumTableBDD.FLUX_PARAMETRE
            + '.');
      }
      parametreCreationMail.setListeDestinataireMail(listeDestinataire);
      
      // Envoi du mail
      Avertissement.envoyerMail(queryManager, parametreCreationMail);
    }
    catch (Exception e) {
    }
    
    pJobASurveiller.setAvertissementEnvoye(true);
  }
  
  /**
   * Signale aux personnes désignées qu'un programme RPG/CL rencontre un problème.
   *
   */
  private void signalerProblemeJobRPG(JobASurveiller pJobASurveiller) {
    if (pJobASurveiller == null) {
      Trace.erreur("Le job à surveiller est invalide, impossible de signaler le problème.");
      return;
    }
    // Préparer le sujet et le message
    String message = pJobASurveiller.recupererMessage();
    
    // Préparation pour la notification
    try {
      // Si le job est verrouillé
      if (pJobASurveiller.getStatut().equals(EnumStatutJob.VERROUILLE)) {
        pJobASurveiller.setAvertissementEnvoye(true);
        throw new MessageErreurException(message);
      }
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Contrôler les temps de traitement des flux.
   */
  private void controlerTempsTraitementFlux() {
    if (listeManagerFluxMagentoASurveiller == null || listeManagerFluxMagentoASurveiller.isEmpty()) {
      return;
    }
    
    for (ManagerFluxMagentoASurveiller managerFluxMagentoASurveiller : listeManagerFluxMagentoASurveiller) {
      ManagerFluxMagento managerFluxMagento = managerFluxMagentoASurveiller.getManagerFluxMagento();
      if (managerFluxMagento.controlerTempsTraitementFlux(TEMPS_EXECUTION_MAX_EN_SECONDE)) {
        // Tout est ok donc l'envoi de l'avertissement est remis à false
        managerFluxMagentoASurveiller.setAvertissementEnvoye(false);
        continue;
      }
      
      // Un problème a été détecté mais l'avertissement a déjà été envoyé
      if (managerFluxMagentoASurveiller.isAvertissementEnvoye()) {
        continue;
      }
      
      // Un problème a été détecté
      if (bibliothequeEnvironnement != null
          && Constantes.normerTexte(managerFluxMagento.getQueryManager().getLibrairieEnvironnement()).isEmpty()) {
        managerFluxMagento.getQueryManager().setLibrairieEnvironnement(bibliothequeEnvironnement.getNom());
      }
      String sujet = "Une anomalie sur un flux a été détectée";
      String nomBaseDeDonnees = managerFluxMagento.getQueryManager().getLibrary();
      if (nomBaseDeDonnees != null) {
        sujet += " sur la base de données " + nomBaseDeDonnees;
      }
      String message = managerFluxMagentoASurveiller.recupererMessage();
      
      managerFluxMagento.signalerProbleme(sujet, message, null);
      managerFluxMagentoASurveiller.setAvertissementEnvoye(true);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Initialise la liste des manager de flux à surveiller.
   */
  public void setListeManagerFluxMagentoASurveiller(List<ManagerFluxMagentoASurveiller> listeManagerFluxMagentoASurveiller) {
    this.listeManagerFluxMagentoASurveiller = listeManagerFluxMagentoASurveiller;
  }
  
  /**
   * Ajoute une liste des destinataires des notifications pour une base de données.
   */
  public void ajouterListeDestinataireNotification(Bibliotheque pNomBaseDeDonnees,
      ListeDestinataireNotification pListeDestinataireNotification) {
    mapListeDestinataireNotification.put(pNomBaseDeDonnees, pListeDestinataireNotification);
  }
  
  /**
   * Ajoute une liste de destinataires des mails pour une base de données.
   */
  public void ajouterListeDestinataireMail(Bibliotheque pNomBaseDeDonnees, String pListeDestinataireMail) {
    mapListeDestinataireMail.put(pNomBaseDeDonnees, pListeDestinataireMail);
  }
}
