/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.job;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Permet de déterminer si une période est spécifiée en nombre de jours, de mois ou d'années
 */
public enum EnumStatutJob {
  NON_DEFINI("", "Non défini"),
  ERREUR("MSGW", "Erreur avec message en attente"),
  VERROUILLE("LCKW", "Verrouillé");
  
  private final String code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumStatutJob(String pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public String getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return libelle;
  }
  
  /**
   * Retourner l'objet enum par son code.
   */
  public static EnumStatutJob valueOfByCode(String pCode) {
    pCode = Constantes.normerTexte(pCode);
    for (EnumStatutJob value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("Le statut du job est inconnu : " + pCode);
  }
}
