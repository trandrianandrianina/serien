/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import ri.serien.libcommun.outils.Constantes;

/**
 * Programme contenant les méthodes de base.
 */
public abstract class BaseProgram implements PropertyChangeListener {
  // Variables
  protected SystemeManager systeme = null;
  protected String profil = null;
  // Nom commun des DTAQs
  private String device = null;
  // Bibliothèque d'environnement
  private String bibenv = null;
  // Bibliothèque où s'execute les travaux
  private String bibjob = null;
  private char letasp = 'W';
  
  // private String dossierRacineServeur = null;
  // Dossier contenant les menus de Série N
  // private String dossierMenus = null;
  // Nom de la DTAQ émettrice (toOndeSrv)
  private String nomDtaqSender = null;
  // Nom de la DTAQ réceptrice(fromOndeSrv)
  private String nomDtaqRecepter = null;
  
  // Infos sur DataQ
  protected DataQManager fromOndeSrv = null;
  protected DataQManager toOndeSrv = null;
  protected boolean waitMsg = true;
  
  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";
  
  /**
   * Constructeur.
   */
  /*
  protected BaseProgram(SystemeManager psysteme, String profil, String device, String bibenv, char letasp) {
    setSystemManagement(psysteme);
    setProfil(profil);
    setDevice(device);
    setBibenv(bibenv);
    setLetasp(letasp);
  }*/
  
  /**
   * Constructeur.
   */
  /*
  protected BaseProgram(HashMap<String, Object> parametres) {
    if (parametres == null) {
      return;
    }
    setSystemManagement((SystemeManager) parametres.get("SYSTEM"));
    setProfil((String) parametres.get("PROFIL"));
    setDevice((String) parametres.get("DEVICE"));
    setBibenv((String) parametres.get("BIBENV"));
    setLetasp(((String) parametres.get("LETASP")).charAt(0));
    setBibjob((String) parametres.get("BIBJOB"));
    // setTypemenu((String)parametres.get("TYPEMENU"));
    setDossierRacineServeur((String) parametres.get("DRACINE"));
  }*/
  
  // ------- Gestion DTAQs ------------------------------------------------------
  
  /**
   * Met en place la gestion des DATQs.
   */
  protected boolean manageDataQueue() {
    String bib = getBibjob();
    if ((bib == null) || (bib.trim().equals(""))) {
      bib = getBibenv();
    }
    fromOndeSrv = new DataQManager(systeme, bib, getNameDtaqRecepter(), Constantes.DATAQLONG);
    toOndeSrv = new DataQManager(systeme, bib, getNameDtaqSender(), Constantes.DATAQLONG);
    if ((fromOndeSrv == null) || (fromOndeSrv.getDtaq() == null)) {
      return false;
    }
    if ((toOndeSrv == null) || (toOndeSrv.getDtaq() == null)) {
      return false;
    }
    
    fromOndeSrv.superviseDataQueue();
    fromOndeSrv.getDtaq().addPropertyChangeListener(this);
    
    return true;
  }
  
  /**
   * Déconnecte les DTAQs si besoin.
   */
  private void disconnectDataQueue() {
    if (fromOndeSrv != null) {
      fromOndeSrv.disconnectDataQueue();
    }
    if (toOndeSrv != null) {
      toOndeSrv.disconnectDataQueue();
    }
  }
  
  // ------- Accesseurs ------------------------------------------------------
  
  /**
   * Connection au serveur.
   */
  protected void setSystemManagement(SystemeManager psysteme) {
    if (psysteme == null) {
      systeme = new SystemeManager(true);
    }
    else {
      systeme = psysteme;
    }
  }
  
  /**
   * @param profil the profil to set.
   */
  public void setProfil(String profil) {
    if (profil != null) {
      this.profil = profil.toUpperCase();
    }
    else {
      this.profil = profil;
    }
  }
  
  public String getProfil() {
    return profil;
  }
  
  public String getDevice() {
    return device;
  }
  
  /**
   * @param device the device to set.
   */
  public void setDevice(String device) {
    this.device = device;
    setNameDtaqSender(this.device);
    setNameDtaqRecepter(this.device);
  }
  
  public String getBibenv() {
    return bibenv;
  }
  
  /**
   * @param bibenv the bibenv to set.
   */
  public void setBibenv(String bibenv) {
    this.bibenv = bibenv;
  }
  
  /**
   * @param letasp the letasp to set.
   */
  public void setLetasp(char letasp) {
    this.letasp = letasp;
  }
  
  public char getLetasp() {
    return letasp;
  }
  
  /**
   * @return le bibjob.
   */
  public String getBibjob() {
    return bibjob;
  }
  
  /**
   * @param bibjob le bibjob à définir.
   */
  public void setBibjob(String bibjob) {
    this.bibjob = bibjob;
  }
  
  /**
   * @param dossierRacine le dossierRacine à définir.
   */
  /*
  public void setDossierRacineServeur(String dossierRacineServeur) {
    this.dossierRacineServeur = dossierRacineServeur;
    setDossierMenus(
        this.dossierRacineServeur + File.separatorChar + Constantes.DOSSIER_CONFIG + File.separatorChar + Constantes.DOSSIER_MENUS);
  }*/
  
  /**
   * @return le dossierMenus.
   */
  /*
  public String getDossierMenus() {
    return dossierMenus;
  }*/
  
  /**
   * @param dossierMenus le dossierMenus à définir.
   */
  /*
  public void setDossierMenus(String dossierMenus) {
    this.dossierMenus = dossierMenus;
  }*/
  
  /**
   * Renseigner le nom de la DataQueue d'émission.
   */
  public void setNameDtaqSender(String prefixedtaq) {
    if (prefixedtaq == null) {
      nomDtaqSender = null;
    }
    else {
      prefixedtaq = prefixedtaq.trim();
      if (prefixedtaq.length() < 10) {
        nomDtaqSender = prefixedtaq.toUpperCase() + "S";
      }
      else {
        nomDtaqSender = prefixedtaq.substring(0, 10).toUpperCase() + "S";
      }
    }
  }
  
  public String getNameDtaqSender() {
    return nomDtaqSender;
  }
  
  /**
   * Renseigner le nom de la DataQueue de réception.
   */
  public void setNameDtaqRecepter(String prefixedtaq) {
    if (prefixedtaq == null) {
      nomDtaqRecepter = null;
    }
    else {
      prefixedtaq = prefixedtaq.trim();
      if (prefixedtaq.length() < 10) {
        nomDtaqRecepter = prefixedtaq.toUpperCase() + "R";
      }
      else {
        nomDtaqRecepter = prefixedtaq.substring(0, 10).toUpperCase() + "R";
      }
    }
  }
  
  public String getNameDtaqRecepter() {
    return nomDtaqRecepter;
  }
  
  /**
   * Traitement lors de la réception d'un message dans la Dataqueue.
   */
  protected abstract void receptionMessage(String message);
  
  // ------- Autres ----------------------------------------------------------
  
  /**
   * Traitement principal.
   */
  public boolean treatment() {
    return true;
  }
  
  /**
   * Traite l'évènement réception d'un message dans la DATQ.
   */
  // @Override
  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getPropertyName().equals("dqRead")) {
      try {
        receptionMessage((String) evt.getNewValue());
      }
      catch (Exception e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }
  
  /**
   * Ferme proprement le programme.
   */
  protected void destructor() {
    disconnectDataQueue();
    if (systeme != null) {
      systeme.deconnecter();
    }
    systeme = null;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
