/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import java.io.IOException;

import com.ibm.as400.access.FTP;

/**
 * Gestion du FTP (spécial AS400).
 */
public class AS400FTPManager {
  // Variables
  private FTP connexion = null;
  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public AS400FTPManager() {
  }
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Connexion au serveur.
   */
  public boolean connexion(String serveur, String user, String password) {
    if (!testStringVariable(serveur) || !testStringVariable(user) || !testStringVariable(password)) {
      return false;
    }
    try {
      connexion = new FTP(serveur, user, password);
      
      if ((connexion != null) && connexion.noop()) {
        msgErreur += "\nConnexion FTP établie";
        return true;
      }
      else {
        msgErreur += "\nConnexion FTP non établie";
        return false;
      }
    }
    catch (Exception e) {
      msgErreur += "\nErreur lors de la connexion : " + e;
      return false;
    }
  }
  
  /**
   * Lancement de commande distante.
   */
  public String remotecmd(String cmd) {
    try {
      return connexion.issueCommand(cmd);
    }
    catch (IOException e) {
      e.printStackTrace();
      msgErreur += "\nErreur lors du lancement de la commande " + cmd + " : " + e;
      return null;
    }
  }
  
  /**
   * Liste le contenu d'un dossier.
   */
  public String[] listFolder(String folder) {
    try {
      return connexion.ls(folder);
    }
    catch (IOException e) {
      msgErreur += "\n" + e;
    }
    return null;
  }
  
  /**
   * Liste le contenu d'un dossier.
   */
  public String[] listFolderWithAttribute(String folder) {
    try {
      return connexion.dir(folder);
    }
    catch (IOException e) {
      msgErreur += "\n" + e;
    }
    return null;
  }
  
  /**
   * Télécharge un fichier.
   */
  public boolean downloadFile(String remoteFile, String localFile, boolean isbinary) {
    boolean ret = false;
    try {
      if (isbinary) {
        connexion.setDataTransferType(com.ibm.as400.access.FTP.BINARY);
      }
      ret = connexion.get(remoteFile, localFile);
      // OutputStream output = new FileOutputStream(localFile);
      // ret = connexion.retrieveFile(remoteFile, output);
    }
    catch (Exception e) {
      msgErreur += "\n" + e;
    }
    return ret;
  }
  
  /**
   * Téléverse un fichier.
   */
  public boolean uploadFile(String localFile, String remoteFile, boolean isbinary) {
    boolean ret = false;
    try {
      if (isbinary) {
        connexion.setDataTransferType(com.ibm.as400.access.FTP.BINARY);
      }
      ret = connexion.put(localFile, remoteFile);
      // InputStream input = new FileInputStream(localFile);
      // ret = connexion.storeFile(remoteFile, input);
      // input.close();
    }
    catch (Exception e) {
      msgErreur += "\n" + e;
    }
    return ret;
  }
  
  /**
   * Déconnexion.
   */
  public void disconnect() {
    try {
      connexion.disconnect();
    }
    catch (Exception e) {
      msgErreur += "\nErreur déconnexion: " + e;
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Teste la validité des variables de type String.
   */
  private boolean testStringVariable(String varvalue) {
    if ((varvalue == null) || (varvalue.trim().equals(""))) {
      msgErreur += "\nUne ou plusieurs variables incorrectes.";
      return false;
    }
    return true;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  public FTP getConnexion() {
    return connexion;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
