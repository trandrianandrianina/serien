/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.edition;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Exception;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.AS400SecurityException;
import com.ibm.as400.access.ConnectionDroppedException;
import com.ibm.as400.access.ErrorCompletingRequestException;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.OutputQueue;
import com.ibm.as400.access.PrintObject;
import com.ibm.as400.access.PrintObjectTransformedInputStream;
import com.ibm.as400.access.PrintParameterList;
import com.ibm.as400.access.RequestNotSupportedException;
import com.ibm.as400.access.SpooledFile;
import com.ibm.as400.access.SpooledFileList;
import com.ibm.as400.access.SpooledFileOutputStream;

import ri.serien.libcommun.exploitation.edition.ListContains;
import ri.serien.libcommun.exploitation.edition.SpoolOld;
import ri.serien.libcommun.exploitation.edition.spool.CritereSpool;
import ri.serien.libcommun.exploitation.edition.spool.EnumEtatSpool;
import ri.serien.libcommun.exploitation.edition.spool.IdSpool;
import ri.serien.libcommun.exploitation.edition.spool.ListeSpool;
import ri.serien.libcommun.exploitation.edition.spool.Spool;
import ri.serien.libcommun.exploitation.utilisateur.IdUtilisateur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Gestion des fichiers spools pour une outq sur un AS/400.
 */
public class GestionSpoolAS400 {
  // ATTR_ALIGN - Align Page
  // ATTR_BACK_OVERLAY - Back Overlay Integrated File System Name
  // ATTR_BKOVL_DWN - Back Overlay Offset Down
  // ATTR_BKOVL_ACR - Back Overlay offset across
  // ATTR_COPIES - Copies
  // ATTR_ENDPAGE - Ending Page
  // ATTR_FILESEP - File Separators
  // ATTR_FORM_DEFINITION - Form definition Integrated File System Name
  // ATTR_FORMFEED - Form Feed
  // ATTR_FORMTYPE - Form Type
  // ATTR_FRONTSIDE_OVERLAY - Front overlay Integrated File System Name
  // ATTR_FTOVL_ACR - Front Overlay Offset Across
  // ATTR_FTOVL_DWN - Front Overlay Offset Down
  // ATTR_OUTPTY - Output Priority
  // ATTR_OUTPUT_QUEUE - Output Queue Integrated File System Name
  // ATTR_MULTIUP - Pages Per Side
  // ATTR_FIDELITY - Print Fidelity
  // ATTR_DUPLEX - Print on Both Sides
  // ATTR_PRTQUALITY - Print Quality
  // ATTR_PRTSEQUENCE - Print Sequence
  // ATTR_PRINTER - Printer
  // ATTR_RESTART - Restart Printing
  // ATTR_SAVE - Save Spooled File
  // ATTR_SCHEDULE - Spooled Output Schedule
  // ATTR_STARTPAGE - Starting Page
  // ATTR_USERDATA - User Data
  // ATTR_USRDEFOPT - User defined option(s)
  // ATTR_USER_DEFINED_OBJECT - User defined object Integrated File System Name
  
  // Constantes
  public static final String PLAIN_TEXT = "/QSYS.LIB/QWPDEFAULT.WSCST";
  public static final String PLAIN_GIF = "/QSYS.LIB/QWPGIF.WSCST";
  public static final String PLAIN_EURO = "/QSYS.LIB/QWPHPEURO.WSCST";
  
  private static final char CR = 0x0d;
  private static final char LF = 0x0a;
  private static final char FF = 0x0c;
  
  private static final String[] titres = new String[] { "Fichier", "Utilisateur", "R\u00e9f\u00e9rence", "Etat", "Pages", "Travail nom",
      "Travail num", "Spool num", "Date", "Heure" };
  private static final String[] types =
      new String[] { "String", "String", "String", "String", "Integer", "String", "String", "Integer", "Date", "String" };
  
  // Variables
  private AS400 systeme = null;
  private OutputQueue outq = null;
  private String nomOutq = null;
  private String codePage = "CP858";
  private SpooledFileList spooledFileList = null;
  private Gson gson = new Gson();
  private String messageErreur = "";
  
  private static SimpleDateFormat formatDate = new SimpleDateFormat("dd/MM/yy");
  
  /**
   * Constructeur.
   */
  public GestionSpoolAS400() {
    systeme = new AS400();
  }
  
  /**
   * Constructeur.
   */
  public GestionSpoolAS400(AS400 pSysteme) {
    if (pSysteme == null) {
      this.systeme = new AS400();
    }
    else {
      this.systeme = pSysteme;
    }
  }
  
  /**
   * Constructeur.
   */
  public GestionSpoolAS400(String pHost, String pUser, String pMotDePasse) {
    systeme = new AS400(pHost, pUser, pMotDePasse);
  }
  
  /**
   * Déconnecte la session.
   */
  public void deconnecter() {
    if (systeme != null) {
      systeme.disconnectAllServices();
    }
    systeme = null;
  }
  
  /**
   * Retourne la valeur de système.
   */
  public AS400 getSystem() {
    if (systeme == null) {
      systeme = new AS400();
    }
    return systeme;
  }
  
  /**
   * Retourne le nom du spool à partir de son indice (dans la outq).
   */
  public SpooledFile getSpooledFile(int pIndice) {
    if ((spooledFileList == null) || (pIndice < 0) || (spooledFileList.size() <= pIndice)) {
      return null;
    }
    return ((SpooledFile) spooledFileList.getObject(pIndice));
  }
  
  /**
   * Met le spool en premier dans sa outq.
   */
  public boolean moveTop(SpooledFile splf) {
    if (splf == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (moveTop) Spool null.";
      return false;
    }
    try {
      splf.moveToTop();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (moveTop) " + e;
      return false;
    }
    return true;
  }
  
  /**
   * Met le spool en premier dans sa outq.
   */
  public boolean move(SpooledFile pSpooledFile, OutputQueue pTargetOutputQueue, int pNombreExemplaires) {
    // Contrôle des paramètres
    if (pSpooledFile == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (move) Le spool est à null.";
      return false;
    }
    
    if (pTargetOutputQueue == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (move) La outqueue est à null.";
      return false;
    }
    if (pNombreExemplaires <= 0) {
      pNombreExemplaires = 1;
    }
    IFSFile outq = new IFSFile(systeme, pTargetOutputQueue.getPath());
    try {
      if (!outq.exists()) {
        messageErreur +=
            "\n" + GestionSpoolAS400.class.getSimpleName() + " (move) La outqueue " + outq.getAbsolutePath() + " n'existe pas.";
        return false;
      }
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (move) Problème lors de la vérification de la outqueue "
          + outq.getAbsolutePath() + ".";
      return false;
    }
    
    // Envoi le spool dans une outq avec le nombre d'exemplaire voulus
    try {
      pSpooledFile.move(pTargetOutputQueue);
      if (pNombreExemplaires > 1) {
        PrintParameterList parms = new PrintParameterList();
        parms.setParameter(PrintObject.ATTR_COPIES, pNombreExemplaires);
        pSpooledFile.setAttributes(parms);
      }
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (move) " + e;
      return false;
    }
    return true;
  }
  
  /**
   * Génère un spooledfile pour le premier spool de la OUTQ.
   */
  public SpooledFile creationSpoolF() throws AS400Exception, ConnectionDroppedException, AS400SecurityException,
      ErrorCompletingRequestException, InterruptedException, IOException, RequestNotSupportedException {
    SpooledFileList splfList = new SpooledFileList(systeme);
    splfList.openSynchronously();
    Enumeration<?> iter = splfList.getObjects();
    SpooledFile splf = (SpooledFile) iter.nextElement();
    splf = new SpooledFile(systeme, splf.getName(), splf.getNumber(), splf.getJobName(), splf.getJobUser(), splf.getJobNumber());
    return (splf);
    
  }
  
  /**
   * Suspend le spool typhold. Peut avoir 2 valeurs: * '*IMMED' ou '*PAGEEND' Le fichier doit être suspendu au plus tôt
   * -> *IMMED Le fichier doit être suspendu à une limite de page -> *PAGEEND.
   */
  public boolean suspendSpool(SpooledFile pSpooledFile, String pTyphold) {
    if (pSpooledFile == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (suspendSpool) Spool null.";
      return false;
    }
    try {
      pSpooledFile.hold(pTyphold);
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (suspendSpool) " + e;
      return false;
    }
    return true;
  }
  
  /**
   * Libère le spool.
   */
  public boolean libereSpool(SpooledFile splf) {
    if (splf == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (libereSpool) Spool null.";
      return false;
    }
    try {
      splf.release();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (libereSpool) " + e;
      return false;
    }
    return true;
  }
  
  /**
   * Supprime le spool.
   */
  public boolean supprimeSpool(SpooledFile pSpooledFile, String pTyphold) {
    if (pSpooledFile == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (supprimeSpool) Spool null.";
      return false;
    }
    try {
      pSpooledFile.delete();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (supprimeSpool) " + e;
      return false;
    }
    return true;
  }
  
  /**
   * Lecture Message d'un spool.
   */
  public String messageSpool(SpooledFile pSpooledFile, boolean pIsReply, String pReply) {
    String message = null;
    try {
      if (pIsReply) {
        pSpooledFile.answerMessage(pReply);
        message = "";
      }
      else {
        AS400Message msg = pSpooledFile.getMessage();
        message = pSpooledFile.getJobNumber() + "/" + pSpooledFile.getJobUser() + "/" + pSpooledFile.getJobName()
            + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getID() + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getSeverity()
            + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getText() + Constantes.SEPARATEUR_CHAINE_CHAR + msg.getHelp()
            + Constantes.SEPARATEUR_CHAINE_CHAR + DateHeure.getJourHeure(0, msg.getDate().getTime());
      }
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (messageSpool) " + e;
    }
    return message;
  }
  
  /**
   * Copie un fichier dans un spool.
   *
   * <p>Attributs ici :
   * http://publib.boulder.ibm.com/iseries/v5r1/ic2924/index.htm?info/rzahh/javadoc/PrintAttributes.html
   */
  public SpooledFile cpyFichier2Spool(String pFichier, String pNomSpool, int pNombreCopies, boolean pIsSuspendre, String pQualite,
      String pPriorite, String pScriptAvant, String pScriptApres) {
    byte[] buf = new byte[65536];
    int bytesRead;
    SpooledFileOutputStream fileout = null;
    FileInputStream in = null;
    
    // Contrôle variables
    if (outq == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (cpyFichier2Spool) La outq n'est pas initialisée.";
      return null;
    }
    if (pFichier == null) {
      return null;
    }
    
    // Initialisation des paramètres du spool
    PrintParameterList parms = new PrintParameterList();
    if (pNomSpool != null) {
      parms.setParameter(PrintObject.ATTR_SPOOLFILE, pNomSpool);
    }
    parms.setParameter(PrintObject.ATTR_COPIES, pNombreCopies <= 0 ? 1 : pNombreCopies);
    parms.setParameter(PrintObject.ATTR_HOLD, pIsSuspendre ? "*YES" : "*NO");
    parms.setParameter(PrintObject.ATTR_OUTPUT_QUEUE, outq.getPath());
    // parms.setParameter(PrintObject.ATTR_DUPLEX, rectoverso?"*YES":"*NO"); // Recto Verso
    // parms.setParameter(PrintObject.ATTR_SRCDRWR, tiroirEntree); // Tiroir entrée
    if (pQualite != null) {
      // Qualité d'impression : *STD,
      parms.setParameter(PrintObject.ATTR_PRTQUALITY, pQualite);
    }
    // *DRAFT, *NLQ, *FASTDRAFT
    if (pPriorite != null) {
      // Priorité : 1 (haute) à 9 (basse)
      parms.setParameter(PrintObject.ATTR_OUTPTY, pPriorite);
    }
    
    // Création du fichier spool
    try {
      // PrinterFile pf = new PrinterFile(systeme, "/QSYS.LIB/SVAS.LIB/BONA4.FILE");
      fileout = new SpooledFileOutputStream(systeme, parms, null, outq);
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName()
          + " (cpyFichier2Spool) Erreur lors de l'ouverture du fichier spool : " + e.getMessage();
      return null;
    }
    
    // Copie du contenu du fichier dans le spool
    try {
      in = new FileInputStream(pFichier);
      if (pScriptAvant != null) {
        fileout.write(pScriptAvant.getBytes());
      }
      do {
        bytesRead = in.read(buf);
        if (bytesRead != -1) {
          fileout.write(buf, 0, bytesRead);
        }
      }
      while (bytesRead != -1);
      if (pScriptApres != null) {
        fileout.write(pScriptApres.getBytes());
      }
      fileout.close();
      in.close();
      return fileout.getSpooledFile();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (cpyFichier2Spool) Erreur lors de la copie du fichier spool : "
          + e.getMessage();
      try {
        fileout.close();
        if (in != null) {
          in.close();
        }
      }
      catch (Exception exceptionNested) {
        // @todo Exception à traiter correctement.
      }
      return null;
    }
  }
  
  /**
   * Copie une liste d'objet dans un spool.
   *
   * @param pTiroirEntree - Non utilisé pour l'instant car non pris en compte si pdf ou ps.
   * @param isRectoVerso - Non utilisé pour l'instant car non pris en compte si pdf ou ps.
   */
  public SpooledFile cpyFichier2Spool(ArrayList<?> pListeBuffer, String pNomSpool, int pNombreCopies, boolean pIsSuspendre,
      String pQualite, String pPriorite, int pTiroirEntree, boolean isRectoVerso) {
    SpooledFileOutputStream fileout = null;
    
    // Contrôle variables
    if (outq == null) {
      return null;
    }
    if ((pListeBuffer == null) || (pListeBuffer.size() == 0)) {
      return null;
    }
    
    // Initialisation des paramètres du spool
    PrintParameterList parms = new PrintParameterList();
    if (pNomSpool != null) {
      parms.setParameter(PrintObject.ATTR_SPOOLFILE, pNomSpool);
    }
    parms.setParameter(PrintObject.ATTR_COPIES, pNombreCopies <= 0 ? 1 : pNombreCopies);
    parms.setParameter(PrintObject.ATTR_HOLD, pIsSuspendre ? "*YES" : "*NO");
    parms.setParameter(PrintObject.ATTR_OUTPUT_QUEUE, outq.getPath());
    // parms.setParameter(PrintObject.ATTR_DUPLEX, rectoverso?"*YES":"*NO"); // Recto Verso
    // parms.setParameter(PrintObject.ATTR_SRCDRWR, tiroirEntree); // Tiroir entrée
    if (pQualite != null) {
      // Qualité d'impression : *STD,
      parms.setParameter(PrintObject.ATTR_PRTQUALITY, pQualite);
    }
    // *DRAFT, *NLQ, *FASTDRAFT
    if (pPriorite != null) {
      // Priorité : 1 (haute) à 9 (basse)
      parms.setParameter(PrintObject.ATTR_OUTPTY, pPriorite);
    }
    
    // Création du fichier spool
    try {
      fileout = new SpooledFileOutputStream(systeme, parms, null, outq);
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName()
          + " (cpyFichier2Spool) Erreur lors de l'ouverture du fichier spool : " + e.getMessage();
      return null;
    }
    
    // Copie du contenu du fichier dans le spool
    try {
      for (int i = 0; i < pListeBuffer.size(); i++) {
        fileout.write((byte[]) pListeBuffer.get(i));
      }
      fileout.close();
      return fileout.getSpooledFile();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (cpyFichier2Spool) Erreur lors de la copie du fichier spool : "
          + e.getMessage();
      return null;
    }
  }
  
  /**
   * Retourne le spool dans un tableau de StringBuffer.
   */
  public String[] cpySpool2TabStringBuffer(SpooledFile pSpooledFile) {
    StringBuffer sb = new StringBuffer();
    StringBuffer page = new StringBuffer();
    int dim = 0;
    String[] pages = null;
    String[] ligne = null;
    String[] tabsurimpression = null;
    
    // Contrôle variables
    if (pSpooledFile == null) {
      return null;
    }
    
    sb = recupSpool(pSpooledFile);
    if (sb != null) {
      // Suppression systématique du premier caractère NULL
      if (sb.length() > 0) {
        sb.deleteCharAt(0);
      }
      pages = sb.toString().split("" + FF);
    }
    if (sb == null || pages == null) {
      return null;
    }
    
    // On parcourt la liste afin de corriger les surimpressions et les sauts de page
    for (int p = 0; p < pages.length; p++) {
      // Suppression des caractères de contrôle de SIM : µG
      pages[p] = pages[p].replaceAll("µG", "  ");
      
      page.setLength(0);
      ligne = pages[p].split("" + LF);
      for (int i = 0; i < ligne.length; i++) {
        dim = ligne[i].length();
        if (dim == 0) {
          page.append(Constantes.crlf);
          continue;
        }
        
        // On supprime le dernier caractère si c'est CR
        if (ligne[i].charAt(dim - 1) == CR) {
          if (dim > 1) {
            ligne[i] = ligne[i].substring(0, dim - 1);
          }
          else {
            ligne[i] = "";
          }
        }
        
        // On recherche des caractères CR dans la chaine, cas de la surimpression
        tabsurimpression = ligne[i].split("" + CR);
        if (tabsurimpression.length > 1) {
          sb.setLength(0);
          sb.append(tabsurimpression[0]);
          for (int j = 1; j < tabsurimpression.length; j++) {
            for (int k = 0; k < tabsurimpression[j].length(); k++) {
              if (sb.length() <= k) {
                sb.append(tabsurimpression[j].charAt(k));
              }
              else if (tabsurimpression[j].charAt(k) != ' ') {
                sb.setCharAt(k, tabsurimpression[j].charAt(k));
              }
            }
          }
          ligne[i] = sb.toString();
        }
        page.append(ligne[i]).append(Constantes.crlf);
      }
      pages[p] = page.toString();
    }
    
    return pages;
  }
  
  /**
   * Retourne le spool avec les informations principales.
   */
  public SpoolOld getSpool(SpooledFile pSpooledFile) {
    String[] pages = cpySpool2TabStringBuffer(pSpooledFile);
    if (pages == null) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getSpool) Pas de données sur le spool.";
      return null;
    }
    
    SpoolOld spoolOld = new SpoolOld();
    try {
      spoolOld.setNom(pSpooledFile.getStringAttribute(PrintObject.ATTR_SPOOLFILE));
      spoolOld.setNbrColonnes(pSpooledFile.getFloatAttribute(PrintObject.ATTR_PAGEWIDTH).intValue());
      spoolOld.setNbrLignes(pSpooledFile.getFloatAttribute(PrintObject.ATTR_PAGELEN).intValue());
      spoolOld.setPortrait(!(pSpooledFile.getIntegerAttribute(PrintObject.ATTR_PAGRTT).intValue() == 90));
      spoolOld.setNbrLigneInch(pSpooledFile.getFloatAttribute(PrintObject.ATTR_LPI).intValue());
      spoolOld.setNbrPages(pSpooledFile.getIntegerAttribute(PrintObject.ATTR_PAGES).intValue());
      spoolOld.setPages(pages);
      return spoolOld;
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getSpool) Erreur : " + e.getMessage();
      return null;
    }
  }
  
  /**
   * Copie un spool dans un fichier (ici gif).
   */
  public boolean cpySpool2Fichier(SpooledFile pSpooledFile, String pFichier) {
    PrintObjectTransformedInputStream in = null;
    int count = 0;
    byte[] buffer = new byte[32767];
    FileOutputStream out = null;
    
    // Contrôle variableswrk
    if (pSpooledFile == null) {
      return false;
    }
    if (pFichier == null) {
      return false;
    }
    
    PrintParameterList printParms = new PrintParameterList();
    printParms.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, PLAIN_GIF);
    printParms.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");
    
    try {
      in = pSpooledFile.getTransformedInputStream(printParms);
      out = new FileOutputStream(pFichier);
      count = 0;
      do {
        out.write(buffer, 0, count);
        // IBM00858
        count = in.read(buffer, 0, buffer.length);
      }
      while (count != -1);
      in.close();
      out.close();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (cpySpool2Fichier) Erreur : " + e.getMessage();
      return false;
    }
    return true;
  }
  
  /**
   * Retourne la liste des spools d'une outq.
   */
  public SpooledFileList listeSpool(String pFiltre, String pNomOutQ, boolean pIsTraitementSynchrone) {
    // Contrôle variables
    try {
      // if (splfList == null) --> Erreur : Object cannot be in an open state.
      spooledFileList = new SpooledFileList(systeme);
      // Filtre sur les users
      spooledFileList.setUserFilter(pFiltre.toUpperCase());
      // splfList.setFormTypeFilter(arg0)
      if ((pNomOutQ != null) && (pNomOutQ.trim().length() > 0) && (pNomOutQ.toUpperCase().startsWith("/QSYS.LIB"))) {
        spooledFileList.setQueueFilter(pNomOutQ);
      }
      if (pIsTraitementSynchrone) {
        spooledFileList.openSynchronously();
      }
      else {
        spooledFileList.openAsynchronously();
        // Risque de blocage si coupure de l'as400, il faut tester le service FILE
        spooledFileList.waitForListToComplete();
      }
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (listeSpool) Erreur : " + e.getMessage();
      Trace.erreur(e, "");
      return null;
    }
    
    return spooledFileList;
  }
  
  /**
   * Retourne un model de table avec les données de la liste des spools d'une outq.
   */
  public ListContains getContainslListeSpool(SpooledFileList pSpooledFileList) {
    final int NBRCOL = 10;
    // final String[] titres = new String[] {"Fichier", "Utilisateur", "R\u00e9f\u00e9rence",
    // "Etat", "Pages", "Travail nom", "Travail num", "Spool num", "Date", "Heure"};
    if (pSpooledFileList == null) {
      return new ListContains(new String[0][NBRCOL], titres);
    }
    
    String[][] data = new String[pSpooledFileList.size()][NBRCOL];
    for (int i = 0; i < pSpooledFileList.size(); i++) {
      PrintObject pol = pSpooledFileList.getObject(i);
      try {
        // pol.getFloatAttribute(PrintObject.ATTR_PAGELEN) + " "+
        // pol.getFloatAttribute(PrintObject.ATTR_PAGEWIDTH) + " " +
        // pol.getIntegerAttribute(PrintObject.ATTR_PAGRTT));
        // pol.getStringAttribute(PrintObject.ATTR_DATE) + " " +
        // pol.getStringAttribute(PrintObject.ATTR_TIME));
        data[i][0] = pol.getStringAttribute(PrintObject.ATTR_SPOOLFILE);
        data[i][1] = pol.getStringAttribute(PrintObject.ATTR_JOBUSER);
        data[i][2] = pol.getStringAttribute(PrintObject.ATTR_USERDATA);
        data[i][3] = pol.getStringAttribute(PrintObject.ATTR_SPLFSTATUS);
        data[i][4] = String.valueOf(pol.getIntegerAttribute(PrintObject.ATTR_PAGES));
        
        data[i][5] = pol.getStringAttribute(PrintObject.ATTR_JOBNAME);
        data[i][6] = pol.getStringAttribute(PrintObject.ATTR_JOBNUMBER);
        data[i][7] = String.valueOf(pol.getIntegerAttribute(PrintObject.ATTR_SPLFNUM));
        
        data[i][8] = getFormateDate(pol.getStringAttribute(PrintObject.ATTR_DATE));
        data[i][9] = getFormateHeure(pol.getStringAttribute(PrintObject.ATTR_TIME));
      }
      catch (Exception e) {
        messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getContainslListeSpool) Erreur : " + e.getMessage();
        Trace.erreur(e, "");
        return null;
      }
    }
    return new ListContains(data, titres);
  }
  
  /**
   * Retourne les données de la liste des spools d'une outq sous forme JSon.
   */
  public String getJSONListeSpool(SpooledFileList pSpooledFileList) {
    // Création de l'objet JSON
    // Le type
    JsonArray objtypes = new JsonArray();
    for (int i = 0; i < types.length; i++) {
      objtypes.add(new JsonPrimitive(types[i]));
    }
    JsonObject objet = new JsonObject();
    objet.add("TYPE", objtypes);
    
    // Le titre
    JsonArray objtitres = new JsonArray();
    for (int i = 0; i < titres.length; i++) {
      objtitres.add(new JsonPrimitive(Constantes.lettreori2code(titres[i])));
    }
    // JSONObject objet = new JSONObject();
    objet.add("TITLE", objtitres);
    
    if (pSpooledFileList == null) {
      objet.add("DATA", null);
      return gson.toJson(objet);
    }
    
    // Les données
    JsonArray objdata = new JsonArray();
    for (int i = 0; i < pSpooledFileList.size(); i++) {
      JsonArray objligne = new JsonArray();
      PrintObject pol = pSpooledFileList.getObject(i);
      try {
        objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_SPOOLFILE)));
        objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_JOBUSER)));
        objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_USERDATA)));
        objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_SPLFSTATUS)));
        objligne.add(new JsonPrimitive(pol.getIntegerAttribute(PrintObject.ATTR_PAGES)));
        
        objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_JOBNAME)));
        objligne.add(new JsonPrimitive(pol.getStringAttribute(PrintObject.ATTR_JOBNUMBER)));
        objligne.add(new JsonPrimitive(pol.getIntegerAttribute(PrintObject.ATTR_SPLFNUM)));
        
        objligne.add(new JsonPrimitive(getFormateDate(pol.getStringAttribute(PrintObject.ATTR_DATE))));
        objligne.add(new JsonPrimitive(getFormateHeure(pol.getStringAttribute(PrintObject.ATTR_TIME))));
        
      }
      catch (Exception e) {
        messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getContainslListeSpool) Erreur : " + e.getMessage();
        Trace.erreur(e, "");
        return null;
      }
      objdata.add(objligne);
    }
    objet.add("DATA", objdata);
    return gson.toJson(objet);
  }
  
  /**
   * Récupère le spool dans un StringBuffer.
   */
  private StringBuffer recupSpool(SpooledFile pSpooledFile) {
    InputStreamReader in = null;
    StringBuffer spool = new StringBuffer();
    char[] buffer = new char[32767];
    int bytesRead = 0;
    
    // Contrôle variables
    if (pSpooledFile == null) {
      return spool;
    }
    
    PrintParameterList printParms = new PrintParameterList();
    printParms.setParameter(PrintObject.ATTR_WORKSTATION_CUST_OBJECT, PLAIN_TEXT);
    printParms.setParameter(PrintObject.ATTR_MFGTYPE, "*WSCST");
    
    // Chargement du spool
    try {
      if (codePage == null) {
        in = new InputStreamReader(pSpooledFile.getTransformedInputStream(printParms));
      }
      else {
        in = new InputStreamReader(pSpooledFile.getTransformedInputStream(printParms), codePage);
      }
      if (in.ready()) {
        bytesRead = in.read(buffer, 0, buffer.length);
        while (bytesRead > 0) {
          spool.append(buffer, 0, bytesRead);
          bytesRead = in.read(buffer, 0, buffer.length);
        }
      }
      in.close();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (recupSpool) Erreur : " + e.getMessage();
      return null;
    }
    
    return spool;
  }
  
  /**
   * Enregistre le spool dans le StringBuffer dans un fichier.
   */
  public boolean enregistreSpool2Fichier(StringBuffer pSpool, String pFichier) {
    FileWriter out = null;
    
    // On écrit le fichier
    try {
      out = new FileWriter(pFichier);
      out.write(pSpool.toString());
      out.close();
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (enregistreSpool2Fichier) Erreur : " + e.getMessage();
      return false;
    }
    return true;
  }
  
  public String getCodepage() {
    return codePage;
  }
  
  /**
   * @param pCodePage the codepage to set.
   */
  public void setCodepage(String pCodePage) {
    this.codePage = pCodePage;
  }
  
  /**
   * @return outq.
   */
  public OutputQueue getOutq() {
    return outq;
  }
  
  /**
   * @param pOutQ outq à définir.
   */
  public void setOutq(OutputQueue pOutQ) {
    this.outq = pOutQ;
    nomOutq = pOutQ.getPath();
  }
  
  /**
   * @param pOutQ à définir.
   */
  public void setOutq(String pOutQ) {
    nomOutq = pOutQ.toUpperCase();
    if (systeme != null) {
      this.outq = new OutputQueue(systeme, pOutQ);
    }
    else {
      this.outq = new OutputQueue(new AS400(), pOutQ);
    }
  }
  
  /**
   * @return nomoutq.
   */
  public String getNomOutq() {
    return nomOutq;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgErreur() {
    // La récupération du message est à usage unique
    String chaine = messageErreur;
    messageErreur = "";
    
    return chaine;
  }
  
  // Formatte une date SAAMMJJ en JJ/MM/AA
  private String getFormateDate(String pStringDate) {
    if ((pStringDate == null) || (pStringDate.length() != 7)) {
      return pStringDate;
    }
    
    return pStringDate.substring(5) + '/' + pStringDate.substring(3, 5) + '/' + (pStringDate.charAt(0) == '1' ? "20" : "19")
        + pStringDate.substring(1, 3);
  }
  
  // Formatte une heure HHMMSS en HH:MM:SS
  private String getFormateHeure(String heure) {
    if ((heure == null) || (heure.length() != 6)) {
      return heure;
    }
    
    return heure.substring(0, 2) + ':' + heure.substring(2, 4) + ':' + heure.substring(4);
  }
  
  /**
   * Charger une liste d'objets Spool à partir d'une liste d'IdSpool
   * 
   * @param pListeIdSpool Liste d'IdSpool.
   * 
   * @see ri.serien.libcommun.exploitation.edition.spool.Spool
   * @see ListeSpool
   * @see IdSpool
   */
  public ListeSpool chargerListeSpool(List<IdSpool> pListeIdSpool) {
    if (pListeIdSpool == null) {
      return null;
    }
    ListeSpool listeSpool = new ListeSpool();
    for (IdSpool idSpool : pListeIdSpool) {
      if (idSpool != null) {
        SpooledFile splf = new SpooledFile(systeme, idSpool.getTexte(), idSpool.getNumeroSpool(), idSpool.getNomJob(),
            idSpool.getIdUtilisateur().getTexte(), String.valueOf(idSpool.getNumeroJob()));
        Spool spool = new Spool(idSpool);
        try {
          // Nombre de colonnes
          Float nombreColonnes = splf.getFloatAttribute(PrintObject.ATTR_PAGEWIDTH);
          if (nombreColonnes != null) {
            spool.setNombreColonnes(nombreColonnes.intValue());
          }
          
          // Nombre de lignes
          Float nombreLignes = splf.getFloatAttribute(PrintObject.ATTR_PAGELEN);
          if (nombreLignes != null) {
            spool.setNombreLignes(nombreLignes.intValue());
          }
          
          // Nombre de lignes par inch
          Float nombreLignesInch = splf.getFloatAttribute(PrintObject.ATTR_LPI);
          if (nombreLignesInch != null) {
            spool.setNombreLigneInch(nombreLignesInch.intValue());
          }
          
          // Nombre de pages
          Integer nombrePages = splf.getIntegerAttribute(PrintObject.ATTR_PAGES);
          if (nombrePages != null) {
            spool.setNombrePages(nombrePages);
          }
          
          // Format d'affichage
          Integer orientationPage = splf.getIntegerAttribute(PrintObject.ATTR_PAGRTT);
          if (orientationPage != null) {
            spool.setFormatPortrait(!(orientationPage.intValue() == 90));
          }
          
          // Etat du spool
          String etatSpool = splf.getStringAttribute(PrintObject.ATTR_SPLFSTATUS);
          if (etatSpool != null) {
            spool.setEtatSpool(EnumEtatSpool.valueOfByValeur(etatSpool));
          }
          
          // Référence du spool
          String referenceSpool = splf.getStringAttribute(PrintObject.ATTR_USERDATA);
          if (referenceSpool != null) {
            spool.setReferenceSpool(referenceSpool);
          }
          
          // Date de création
          String date = getFormateDate(splf.getStringAttribute(PrintObject.ATTR_DATE));
          if (date != null) {
            Date d = formatDate.parse(date);
            spool.setDate(d);
          }
          
          // Heure de création
          String heure = splf.getStringAttribute(PrintObject.ATTR_TIME);
          if (heure != null) {
            spool.setHeure(getFormateHeure(heure));
          }
          
          listeSpool.add(spool);
        }
        catch (Exception e) {
          messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getContainslListeSpool) Erreur : " + e.getMessage();
          Trace.erreur(e, "");
          return null;
        }
      }
    }
    return listeSpool;
  }
  
  /**
   * Charger une liste d'IdSpool à partir de critères de recherche de spool.
   * 
   * @param pCritereSpool Criteres de recherche d'un spool.
   * 
   * @see IdSpool
   */
  public List<IdSpool> chargerListeIdSpool(CritereSpool pCritereSpool) {
    if (pCritereSpool == null) {
      throw new MessageErreurException("Le paramètre pCritere du service est à null.");
    }
    List<IdSpool> listeIdSpool = new ArrayList<IdSpool>();
    pCritereSpool.setTraitementSynchrone(false);
    SpooledFileList splfListe = chargerListeSpool(pCritereSpool);
    for (int i = 0; i < splfListe.size(); i++) {
      PrintObject pol = splfListe.getObject(i);
      try {
        // On vérifie que le nom du spool n'est pas à null
        if (pol.getStringAttribute(PrintObject.ATTR_SPOOLFILE) != null) {
          pol = filtrerSpool(pol, pCritereSpool);
          if (pol != null) {
            // On complète l'IdSpool avec les données nécessaires, et on l'ajoute à la liste.
            IdSpool idSpool = completerIdSpool(pol);
            listeIdSpool.add(idSpool);
          }
        }
      }
      catch (Exception e) {
        messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getContainslListeSpool) Erreur : " + e.getMessage();
        Trace.erreur(e, "");
        return null;
      }
    }
    return listeIdSpool;
  }
  
  /**
   * Retourner un objet AS400 SpooledFileList à partir de critères de recherche de spool.
   * 
   * @param pCritereSpool Critères de recherche d'un objet spool.
   */
  public SpooledFileList chargerListeSpool(CritereSpool pCritereSpool) {
    // Contrôle variables
    try {
      // if (splfList == null) --> Erreur : Object cannot be in an open state.
      spooledFileList = new SpooledFileList(systeme);
      // Filtre sur les users
      spooledFileList.setUserFilter(pCritereSpool.getIdUtiliateur().getTexte().toUpperCase());
      // Filtre sur l'outQ
      if ((pCritereSpool.getNomOutQueue() != null) && (pCritereSpool.getNomOutQueue().trim().length() > 0)
          && (pCritereSpool.getNomOutQueue().toUpperCase().startsWith("/QSYS.LIB"))) {
        spooledFileList.setQueueFilter(pCritereSpool.getNomOutQueue());
      }
      if (pCritereSpool.getDebutPlageDateCreationSpool() == null && pCritereSpool.getFinPlageDateCreationSpool() == null) {
        spooledFileList.setStartDateFilter("*ALL");
      }
      else {
        // Filtre sur la date de début de plage.
        if (pCritereSpool.getDebutPlageDateCreationSpool() != null) {
          spooledFileList.setStartDateFilter(DateHeure.getJourHeure(DateHeure.SAAMMJJ, pCritereSpool.getDebutPlageDateCreationSpool()));
        }
        else {
          spooledFileList.setStartDateFilter("*FIRST");
        }
        // Filtre sur la date de fin de plage.
        if (pCritereSpool.getFinPlageDateCreationSpool() != null) {
          spooledFileList.setEndDateFilter(DateHeure.getJourHeure(DateHeure.SAAMMJJ, pCritereSpool.getFinPlageDateCreationSpool()));
        }
        else {
          spooledFileList.setEndDateFilter("*LAST");
        }
      }
      // Filtre sur le traitement.
      if (pCritereSpool.isTraitementSynchrone()) {
        spooledFileList.openSynchronously();
      }
      else {
        spooledFileList.openAsynchronously();
        // Risque de blocage si coupure de l'as400, il faut tester le service FILE
        spooledFileList.waitForListToComplete();
      }
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (listeSpool) Erreur : " + e.getMessage();
      Trace.erreur(e, "");
      return null;
    }
    
    return spooledFileList;
  }
  
  /**
   * Construire un objet IdSpool à partir d'un objet PrintObject
   * 
   * @param pPol Objet AS400 de type PrintObject.
   */
  private IdSpool completerIdSpool(PrintObject pPol) {
    if (pPol == null) {
      return null;
    }
    try {
      IdSpool idSpool = IdSpool.getInstance(pPol.getStringAttribute(PrintObject.ATTR_SPOOLFILE));
      
      // Profil utilisateur
      String profilUtilisateur = pPol.getStringAttribute(PrintObject.ATTR_JOBUSER);
      if (profilUtilisateur != null) {
        idSpool.setIdUtilisateur(IdUtilisateur.getInstance(profilUtilisateur));
      }
      
      // Numéro du spool
      Integer numeroSpool = pPol.getIntegerAttribute(PrintObject.ATTR_SPLFNUM);
      if (numeroSpool != null) {
        idSpool.setNumeroSpool(numeroSpool);
      }
      
      // Nom du job
      String nomJob = pPol.getStringAttribute(PrintObject.ATTR_JOBNAME);
      if (nomJob != null) {
        idSpool.setNomJob(nomJob);
      }
      
      // Numéro du job
      String numeroJob = pPol.getStringAttribute(PrintObject.ATTR_JOBNUMBER);
      if (numeroJob != null) {
        idSpool.setNumeroJob(numeroJob);
      }
      return idSpool;
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getContainslListeSpool) Erreur : " + e.getMessage();
      Trace.erreur(e, "");
      return null;
    }
  }
  
  /**
   * Filter le spool suivant les critères de recherche.
   * 
   * @param pPol Objet à filtrer.
   * @param pCritereSpool Critères de recherche du spool.
   * @return L'objet PrintObject si les caractéristiques correspondent à l'objet attendu, null sinon.
   */
  private PrintObject filtrerSpool(PrintObject pPol, CritereSpool pCritereSpool) {
    if (pPol == null) {
      return null;
    }
    if (pCritereSpool == null) {
      return null;
    }
    
    String critereEtatSpool = pCritereSpool.getEtatSpool().getValeur();
    String critereRechercheGenerique = pCritereSpool.getRechercheGenerique().getTexteFinal().toUpperCase();
    try {
      // Si le critère de recherche sur l'état du spool existe,
      if (critereEtatSpool != null && !critereEtatSpool.trim().isEmpty()) {
        // On observe l'état du spool à analyser.
        String etatSpool = pPol.getStringAttribute(PrintObject.ATTR_SPLFSTATUS);
        // Si l'état du spool à analyser ne correspond pas au critère attendu,
        if (etatSpool != null && !Constantes.equals(etatSpool, critereEtatSpool)) {
          // On retourne null.
          return null;
        }
      }
      // Si le critère de recherche générique existe,
      if (critereRechercheGenerique != null) {
        // On observe les valeurs du nom du spool et de la référence du spool
        String nomSpool = pPol.getStringAttribute(PrintObject.ATTR_SPOOLFILE);
        String referenceSpool = pPol.getStringAttribute(PrintObject.ATTR_USERDATA);
        // La recherche générique filtre les spools dont la valeur recherchée COMMENCE PAR le critère.
        // Si le nom du spool existe et correspond au critère de recherche,
        // OU
        // Si la référence du spool existe et correspon au critère de recherche,
        if ((nomSpool != null && nomSpool.startsWith(critereRechercheGenerique))
            || (referenceSpool != null && referenceSpool.startsWith(critereRechercheGenerique))) {
          // On retourne le spool.
          return pPol;
        }
        // Sinon on retourne null.
        else {
          return null;
        }
      }
      return pPol;
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionSpoolAS400.class.getSimpleName() + " (getContainslListeSpool) Erreur : " + e.getMessage();
      Trace.erreur(e, "");
      return null;
    }
  }
  
  /**
   * Imprimer le spool.
   * 
   * Déplace le spool d'une outq à une autre générant l'impression.
   * 
   * @param pIdSpool Id du spool à imprimer.
   * @param pNomOutq Nom de l'OutQ correspondant à l'imprimante sélectionnée.
   * @param pNombreExemplaire Nombre d'exemplaire à imprimer.
   * @return true tout s'est déroulé sans problème, false sinon.
   */
  public boolean imprimerSpool(IdSpool pIdSpool, String pNomOutq, int pNombreExemplaire) {
    if (pIdSpool == null) {
      throw new MessageErreurException("Impression impossible : spool non renseigné");
    }
    if (pNomOutq == null) {
      throw new MessageErreurException("Impression impossible : outq non renseignée");
    }
    if (pNombreExemplaire < 1) {
      throw new MessageErreurException("Impression impossible : nombre d'exemplaires renseigné inférieur à 1");
    }
    SpooledFile splf = new SpooledFile(systeme, pIdSpool.getTexte(), pIdSpool.getNumeroSpool(), pIdSpool.getNomJob(),
        pIdSpool.getIdUtilisateur().getTexte(), String.valueOf(pIdSpool.getNumeroJob()));
    OutputQueue outq = new OutputQueue(systeme, pNomOutq);
    return move(splf, outq, pNombreExemplaire);
  }
  
  /**
   * Suspend un spool donné.
   * 
   * @param pIdSpool Id du spool à suspendre.
   */
  public boolean suspendreSpool(IdSpool pIdSpool) {
    if (pIdSpool == null) {
      throw new MessageErreurException("Suspension impossible : spool non renseigné");
    }
    SpooledFile splf = new SpooledFile(systeme, pIdSpool.getTexte(), pIdSpool.getNumeroSpool(), pIdSpool.getNomJob(),
        pIdSpool.getIdUtilisateur().getTexte(), String.valueOf(pIdSpool.getNumeroJob()));
    return suspendSpool(splf, "*IMMED");
  }
  
  /**
   * Libère un spool donné.
   * 
   * @param pIdSpool Id du spool à libérer.
   */
  public boolean libererSpool(IdSpool pIdSpool) {
    if (pIdSpool == null) {
      throw new MessageErreurException("Libération impossible : spool non renseigné");
    }
    SpooledFile splf = new SpooledFile(systeme, pIdSpool.getTexte(), pIdSpool.getNumeroSpool(), pIdSpool.getNomJob(),
        pIdSpool.getIdUtilisateur().getTexte(), String.valueOf(pIdSpool.getNumeroJob()));
    return libereSpool(splf);
  }
  
  /**
   * Supprime un spool donné.
   * 
   * @param pIdSpool Id du spool à supprimer.
   */
  public boolean supprimerSpool(IdSpool pIdSpool) {
    if (pIdSpool == null) {
      throw new MessageErreurException("Supression impossible : spool non renseigné");
    }
    SpooledFile splf = new SpooledFile(systeme, pIdSpool.getTexte(), pIdSpool.getNumeroSpool(), pIdSpool.getNomJob(),
        pIdSpool.getIdUtilisateur().getTexte(), String.valueOf(pIdSpool.getNumeroJob()));
    return supprimeSpool(splf, "*IMMED");
  }
  
  /**
   * Charge le contenu d'un spool donné.
   * 
   * @param pSpool Spool dont il faut charger le contenu.
   */
  public Spool chargerContenuSpool(Spool pSpool) {
    if (pSpool == null) {
      return null;
    }
    IdSpool pIdSpool = pSpool.getId();
    List<String> contenuSpool = new ArrayList<String>();
    SpooledFile splf = new SpooledFile(systeme, pIdSpool.getTexte(), pIdSpool.getNumeroSpool(), pIdSpool.getNomJob(),
        pIdSpool.getIdUtilisateur().getTexte(), String.valueOf(pIdSpool.getNumeroJob()));
    for (String page : cpySpool2TabStringBuffer(splf)) {
      contenuSpool.add(page);
    }
    pSpool.setContenuSpool(contenuSpool);
    return pSpool;
  }
}
