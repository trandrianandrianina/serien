/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.QSYSObjectPathName;

/**
 * Gestion des connexions aux DTAQs.
 */
public class DataQManager {
  // Variables
  private SystemeManager systeme = null;
  private DataQ dtaq = null;
  private int longueurdtaq = 1024;
  private String commentaire = "";
  private Thread threadDataQueue = null;
  
  // Conserve le dernier message d'erreur émit et non lu
  protected String msgErreur = "";
  
  /**
   * Constructeur.
   */
  public DataQManager(SystemeManager pSystemeManager, String pNomBibliotheque, String pNomDataqueue, int pLongueur) {
    setSysteme(pSystemeManager);
    setLongueurdtaq(pLongueur);
    connexionDataQueue(pNomBibliotheque, pNomDataqueue);
  }
  
  /**
   * Connexion aux DataQueues.
   */
  private void connexionDataQueue(String bib, String nomdtaq) {
    final String DataQRJ = QSYSObjectPathName.toPath(bib, nomdtaq, DataQ.EXTENSION);
    try {
      dtaq = new DataQ(systeme.getSystem(), DataQRJ, getLongueurdtaq(), getCommentaire(), false);
    }
    catch (Exception e) {
      msgErreur += "\n" + DataQManager.class.getSimpleName() + " (connexionDataQueue) Erreur:" + e.getMessage();
    }
  }
  
  /**
   * Réceptionne un message du programme RPG On teste l'existance de la DTAQ afin d'éviter un bouclage et un remplissage
   * de la file d'attente avec le même message.
   */
  public void superviseDataQueue() {
    threadDataQueue = new Thread() {
      @Override
      public void run() {
        try {
          while (isAlive() && (dtaq.exists())) {
            // DELAI_ATTENTE
            dtaq.readMessage(-1);
          }
        }
        catch (Exception e) {
          msgErreur += "\nException sur écoute de la DTAQ : " + e;
        }
      }
    };
    threadDataQueue.start();
  }
  
  /**
   * Envoi un message avec entête dissociée au programme RPG Remplit avec des messages entiers.
   */
  public boolean sendMessageDataQueue(StringBuffer head, StringBuffer message, final int sizemsg) {
    // On vérifie la longeur du message à envoyer
    final int taillehead = head.length();
    int offset = 0;
    int taillerest = message.length();
    while (taillerest > sizemsg) {
      head.append(message.substring(offset, offset + sizemsg));
      sendMessageDataQueue(head.toString());
      offset += sizemsg;
      taillerest -= sizemsg;
      head.setLength(taillehead);
    }
    if (taillerest > 0) {
      head.append(message.substring(offset));
      sendMessageDataQueue(head.toString());
    }
    return true;
  }
  
  /**
   * Envoi un message avec entête dissociée au programme RPG Remplit avec des messages entiers.
   */
  public boolean sendMessageDataQueue(StringBuffer head, String message, final int sizemsg) {
    // On vérifie la longeur du message à envoyer
    final int taillehead = head.length();
    int offset = 0;
    int taillerest = message.length();
    while (taillerest > sizemsg) {
      head.append(message.substring(offset, offset + sizemsg));
      sendMessageDataQueue(head.toString());
      offset += sizemsg;
      taillerest -= sizemsg;
      head.setLength(taillehead);
    }
    if (taillerest > 0) {
      head.append(message.substring(offset));
      sendMessageDataQueue(head.toString());
    }
    return true;
  }
  
  /**
   * Envoi un message au programme RPG.
   */
  public synchronized boolean sendMessageDataQueue(String message) {
    if (!dtaq.writeString(message, longueurdtaq)) {
      msgErreur += "\nProbl\u00e8me lors de l'envoi du message dans la dataqueue.";
      return false;
    }
    return true;
  }
  
  /**
   * Déconnecte les DTAQs si besoin.
   */
  public void disconnectDataQueue() {
    if (dtaq != null) {
      // if ((threadDataQueue != null) && threadDataQueue.isAlive()) threadDataQueue.interrupt();
      // threadDataQueue = null;
      dtaq.close();
      dtaq = null;
    }
  }
  
  // -------------------------------------------------------------------------
  // ------- Accesseurs ------------------------------------------------------
  // -------------------------------------------------------------------------
  
  /**
   * @param systeme the systeme to set.
   */
  public void setSysteme(SystemeManager systeme) {
    this.systeme = systeme;
  }
  
  public SystemeManager getSysteme() {
    return systeme;
  }
  
  /**
   * @param dtaq the dtaq to set.
   */
  public void setDtaq(DataQ dtaq) {
    this.dtaq = dtaq;
  }
  
  public DataQ getDtaq() {
    return dtaq;
  }
  
  /**
   * @param longueurdtaq the longueurdtaq to set.
   */
  public void setLongueurdtaq(int longueurdtaq) {
    this.longueurdtaq = longueurdtaq;
  }
  
  public int getLongueurdtaq() {
    return longueurdtaq;
  }
  
  /**
   * @param commentaire the commentaire to set.
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }
  
  public String getCommentaire() {
    return commentaire;
  }
  
  /**
   * Retourne le message d'erreur.
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
}
