/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.valeursysteme;

import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.SystemValue;

import ri.serien.libcommun.exploitation.IdServeurPower;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe contient les valeurs systèmes du serveur AS400.
 * Toutes les valeurs systèmes ne sont pas chargées car c'est inutile, seules les valeurs systèmes nécessaires à Série N sont chargés.
 */
public class ValeurSysteme {
  // Variables
  private static AS400 systeme = null;
  private static List<Bibliotheque> listeBibliothequeUtilisateur = null;
  private static IdServeurPower idServeurPower = null;
  
  /**
   * Constructeur.
   */
  public ValeurSysteme(AS400 pSysteme) {
    systeme = pSysteme;
    charger();
  }
  
  // -- Méthodes privées
  
  /**
   * Charge toutes les variables systèmes nécesssaires.
   */
  private static void charger() {
    Trace.info("Début du chargement de l'ensemble des variables systèmes nécessaires.");
    if (systeme == null) {
      throw new MessageErreurException("La connexion au serveur n'est pas valide.");
    }
    
    // Chargement de la valeur système QUSRLIBL (liste des bibliothèques utilisateur)
    chargerListeBibliothequeUtilisateur();
    
    // Chargement du numéro de série du serveur
    chargerNumeroSerieServeur();
    
    Trace.info("Fin du chargement de l'ensemble des variables systèmes nécessaires.");
  }
  
  /**
   * Charge la liste des bibliothèques utilisateur.
   */
  private static void chargerListeBibliothequeUtilisateur() {
    Trace.info("\tChargement de la valeur système QUSRLIBL :");
    SystemValue qusrlibl = new SystemValue(systeme, "QUSRLIBL");
    
    try {
      String[] tableauValeurs = (String[]) qusrlibl.getValue();
      listeBibliothequeUtilisateur = new ArrayList<Bibliotheque>();
      for (String element : tableauValeurs) {
        IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(element);
        Trace.info("\t- " + idBibliotheque.getNom());
        listeBibliothequeUtilisateur.add(new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.NON_DEFINI));
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Problème rencontré lors du chargement de la liste des bibliothèques utilisateur (QUSRLIBL)");
    }
  }
  
  /**
   * Charge le numéro de série du serveur.
   */
  private static void chargerNumeroSerieServeur() {
    Trace.info("\tChargement de la valeur système QSRLNBR :");
    SystemValue qsrlnbr = new SystemValue(systeme, "QSRLNBR");
    
    try {
      String valeur = (String) qsrlnbr.getValue();
      idServeurPower = IdServeurPower.getInstance(valeur);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Problème rencontré lors du chargement du numéro de série du serveur (QSRLNBR)");
    }
  }
  
  // -- Accesseurs
  
  public static List<Bibliotheque> getListeBibliothequeUtilisateur() {
    return listeBibliothequeUtilisateur;
  }
  
  public static IdServeurPower getIdServeurPower() {
    return idServeurPower;
  }
  
}
