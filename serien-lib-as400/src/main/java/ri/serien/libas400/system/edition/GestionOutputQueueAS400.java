/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.edition;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.ibm.as400.access.AS400;
import com.ibm.as400.access.OutputQueue;
import com.ibm.as400.access.OutputQueueList;
import com.ibm.as400.access.Printer;

import ri.serien.libcommun.exploitation.edition.OutQ;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gestion des imprimantes sur un AS/400
 */
public class GestionOutputQueueAS400 {
  // Variables
  private AS400 systeme = null;
  
  private String messageErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur
   */
  public GestionOutputQueueAS400() {
    systeme = new AS400();
  }
  
  /**
   * Constructeur
   * @param pSysteme
   */
  public GestionOutputQueueAS400(AS400 pSysteme) {
    if (pSysteme == null) {
      this.systeme = new AS400();
    }
    else {
      this.systeme = pSysteme;
    }
  }
  
  /**
   * Constructeur
   * 
   * @param pHost
   * @param pUser
   * @param pMotDePasse
   */
  public GestionOutputQueueAS400(String pHost, String pUser, String pMotDePasse) {
    systeme = new AS400(pHost, pUser, pMotDePasse);
  }
  
  /**
   * Déconnecte la session
   */
  public void deconnecter() {
    if (systeme != null) {
      systeme.disconnectAllServices();
    }
    systeme = null;
  }
  
  /**
   * Retourne la valeur de système
   * @return
   */
  public AS400 getSystem() {
    if (systeme == null) {
      systeme = new AS400();
    }
    return systeme;
  }
  
  /**
   * Retourne la liste des outq avec leur description et leur outq au format json.
   * 
   * @return liste des outq
   */
  public JsonArray getListeOutputQueues() {
    JsonArray tabjson = new JsonArray();
    OutputQueueList liste = new OutputQueueList(systeme);
    
    try {
      liste.setQueueFilter("/QSYS.LIB/%ALL%.LIB/%ALL%.OUTQ");
      liste.openSynchronously();
      for (int i = 0; i < liste.size(); i++) {
        OutputQueue prt = (OutputQueue) liste.getObject(i);
        JsonObject objjson = new JsonObject();
        objjson.add("NAME", new JsonPrimitive(prt.getName()));
        objjson.add("DESCRIPTION", new JsonPrimitive(prt.getStringAttribute(Printer.ATTR_DESCRIPTION)));
        objjson.add("OUTQ", new JsonPrimitive(prt.getStringAttribute(Printer.ATTR_OUTPUT_QUEUE)));
        tabjson.add(objjson);
        // prt.getStringAttribute(prt.ATTR_OUTPUT_QUEUE));
      }
    }
    catch (Exception e) {
      messageErreur += "\n" + GestionOutputQueueAS400.class.getSimpleName() + " (getListeOutputQueues) " + e;
      tabjson = null;
    }
    finally {
      liste.close();
    }
    
    return tabjson;
  }
  
  /**
   * Retourne la liste des outq avec leur description et leur outq au format tableau.
   * @return la liste des outq
   */
  public OutQ[] chargerListeOutQ() {
    OutputQueueList liste = new OutputQueueList(systeme);
    OutQ[] listeOutQs = null;
    
    try {
      liste.setQueueFilter("/QSYS.LIB/%ALL%.LIB/%ALL%.OUTQ");
      liste.openSynchronously();
      listeOutQs = new OutQ[liste.size()];
      for (int i = 0; i < liste.size(); i++) {
        OutputQueue prt = (OutputQueue) liste.getObject(i);
        OutQ outq = new OutQ();
        outq.setNom(prt.getName());
        outq.setDescription(prt.getStringAttribute(Printer.ATTR_DESCRIPTION));
        outq.setOutq(prt.getStringAttribute(Printer.ATTR_OUTPUT_QUEUE));
        listeOutQs[i] = outq;
      }
    }
    catch (Exception e) {
      listeOutQs = null;
      throw new MessageErreurException(e, "Erreur lors de la récupération de la liste des outqs.");
    }
    finally {
      liste.close();
    }
    
    return listeOutQs;
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMessageErreur() {
    // La récupération du message est à usage unique
    String chaine = messageErreur;
    messageErreur = "";
    
    return chaine;
  }
  
}
