/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.DataQueueEvent;
import com.ibm.as400.access.DataQueueListener;

/**
 * Gestion des évènements sur les DTAQs (pas très utile car déclenché juste avant l'action).
 */
public class DataQListener implements DataQueueListener {
  
  // @Override
  public void cleared(DataQueueEvent event) {
  }
  
  // @Override
  public void peeked(DataQueueEvent event) {
  }
  
  // @Override
  public void read(DataQueueEvent event) {
  }
  
  // @Override
  public void written(DataQueueEvent event) {
  }
  
}
