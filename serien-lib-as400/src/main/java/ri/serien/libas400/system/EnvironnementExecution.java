/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.outils.EnumBibliothequeProgramme;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe regroupe l'ensemble des données propre à un environnement d'exécution. C'est une classe statique.
 * Cette classe est appelée et initialisée après la lecture de la lettre d'environnement.
 */
public class EnvironnementExecution {
  // Variables
  private static Character lettreEnvironnement = null;
  
  private static Bibliotheque expas = null;
  private static Bibliotheque gvmas = null;
  private static Bibliotheque gvmx = null;
  private static Bibliotheque gamas = null;
  private static Bibliotheque cgmas = null;
  private static Bibliotheque gemas = null;
  private static Bibliotheque gimas = null;
  private static Bibliotheque glfas = null;
  private static Bibliotheque gmmas = null;
  private static Bibliotheque gpmas = null;
  private static Bibliotheque gtmas = null;
  private static Bibliotheque prmas = null;
  private static Bibliotheque spmas = null;
  private static Bibliotheque tbmas = null;
  private static Bibliotheque telm = null;
  private static Bibliotheque timas = null;
  private static Bibliotheque trmas = null;
  private static Bibliotheque wbsas = null;
  
  // -- Méthodes publiques
  
  /**
   * Initialisation du nom des bibliothèques métiers avec la lettre d'environnement.
   */
  public static void initialiserNomBibliotheques(Character pLettreEnvironnement) {
    if (pLettreEnvironnement == null || pLettreEnvironnement.charValue() == ' ') {
      throw new MessageErreurException("La lettre d'environnement est invalide.");
    }
    lettreEnvironnement = pLettreEnvironnement;
    
    expas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.EXPLOITATION_EXP, null);
    gvmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_COMMERCIALE_GVM, null);
    gvmx = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_COMMERCIALE_GVX, null);
    gamas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_COMMERCIALE_GAM, null);
    cgmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.COMPTABILITE_CGM, null);
    gemas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_EFFETS_GEM, null);
    gimas =
        EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_IMMOBILISATION_GIM, null);
    glfas =
        EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_LIASSE_FISCALE_GLF, null);
    gmmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_MAINTENANCE_GMM, null);
    gpmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_PRODUCTION_GPM, null);
    gtmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_TRESORERIE_GTM, null);
    prmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_PROSPECTION_PRM, null);
    spmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.SPECIFIQUE_SERIEN_SPM, null);
    tbmas =
        EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_TABLEAU_BORD_TBM, null);
    telm =
        EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_TELEMAINTENANCE_TLM, null);
    timas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_IMPAYES_TIM, null);
    trmas = EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_TRANSPORTS_TRM, null);
    wbsas =
        EnumBibliothequeProgramme.retournerBibliotheque(lettreEnvironnement, EnumBibliothequeProgramme.GESTION_WEB_SERVICES_WBS, null);
  }
  
  // -- Accesseurs
  
  public static Character getLettreEnvironnement() {
    return lettreEnvironnement;
  }
  
  public static Bibliotheque getExpas() {
    return expas;
  }
  
  public static Bibliotheque getGvmas() {
    return gvmas;
  }
  
  public static Bibliotheque getGvmx() {
    return gvmx;
  }
  
  public static Bibliotheque getGamas() {
    return gamas;
  }
  
  public static Bibliotheque getCgmas() {
    return cgmas;
  }
  
  public static Bibliotheque getGemas() {
    return gemas;
  }
  
  public static Bibliotheque getGimas() {
    return gimas;
  }
  
  public static Bibliotheque getGlfas() {
    return glfas;
  }
  
  public static Bibliotheque getGmmas() {
    return gmmas;
  }
  
  public static Bibliotheque getGpmas() {
    return gpmas;
  }
  
  public static Bibliotheque getGtmas() {
    return gtmas;
  }
  
  public static Bibliotheque getPrmas() {
    return prmas;
  }
  
  public static Bibliotheque getSpmas() {
    return spmas;
  }
  
  public static Bibliotheque getTbmas() {
    return tbmas;
  }
  
  public static Bibliotheque getTelm() {
    return telm;
  }
  
  public static Bibliotheque getTimas() {
    return timas;
  }
  
  public static Bibliotheque getTrmas() {
    return trmas;
  }
  
  public static Bibliotheque getWbsas() {
    return wbsas;
  }
  
}
