/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.objet;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.SaveFile;
import com.ibm.as400.access.SaveFileEntry;

import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class ObjetSavfAS400 {
  // Constantes
  public static final String EXTENSION = ".SAVF";
  
  // -- Méthodes statiques
  
  /**
   * Copie un fichier Stream de type SAVF de l'IFS vers le 5250.
   */
  public static void copierStreamSavfVers5250(AS400 pSysteme, String pCheminCompletStream, Bibliotheque pBibliotheque) {
    if (pSysteme == null) {
      throw new MessageErreurException("La configuration système est incorrecte.");
    }
    pCheminCompletStream = Constantes.normerTexte(pCheminCompletStream).toUpperCase();
    if (pCheminCompletStream.isEmpty()) {
      throw new MessageErreurException("Le fichier stream est invalide.");
    }
    if (!pCheminCompletStream.endsWith(EXTENSION)) {
      throw new MessageErreurException("Le fichier stream n'est pas un fichier SAVF.");
    }
    if (pBibliotheque == null) {
      throw new MessageErreurException("La bibliothèque de destination est invalide.");
    }
    
    // Récupération du nom du fichier
    int posd = pCheminCompletStream.lastIndexOf('/');
    if (posd == -1) {
      throw new MessageErreurException("Le chemin du fichier stream est invalide.");
    }
    String nomFichier5250 = pCheminCompletStream.substring(posd + 1, pCheminCompletStream.lastIndexOf(EXTENSION)).toUpperCase();
    
    // Vérification de l'existence de la bibliothèque
    BibliothequeAS400 bibliothequeAS400 = new BibliothequeAS400(pSysteme, pBibliotheque);
    if (!bibliothequeAS400.isExists()) {
      bibliothequeAS400.create();
    }
    if (!bibliothequeAS400.isExists()) {
      throw new MessageErreurException("La bibliothèque " + bibliothequeAS400.getName() + " n'a pas pu être créé.");
    }
    
    String targetPath = QSYSObjectPathName.toPath(bibliothequeAS400.getName(), nomFichier5250, "FILE");
    // Copie du fichier SAVF
    CommandCall cmd = new CommandCall(pSysteme, "QSYS/CPYFRMSTMF FROMSTMF('" + pCheminCompletStream + "') TOMBR('" + targetPath
        + "') CVTDTA(*NONE) ENDLINFMT(*FIXED) TABEXPN(*NO)");
    try {
      cmd.run();
    }
    catch (Exception e) {
      StringBuilder sb = new StringBuilder();
      AS400Message[] msg = cmd.getMessageList();
      if (msg != null) {
        for (int i = 0; i < msg.length; i++) {
          sb.append(msg[i].getText()).append('\n');
        }
      }
      throw new MessageErreurException(e, sb.toString());
    }
  }
  
  /**
   * Restaure le contenu du fichier.
   */
  public static void restaurer(AS400 pSysteme, Bibliotheque pBibliotheque, String pNomFichierSavf) {
    if (pSysteme == null) {
      throw new MessageErreurException("La configuration système est incorrecte.");
    }
    if (pBibliotheque == null) {
      throw new MessageErreurException("La biliothèque est invalide.");
    }
    if (pNomFichierSavf == null || pNomFichierSavf.isEmpty()) {
      throw new MessageErreurException("Le nom du fichier SAVF est invalide.");
    }
    
    // Controle qu'il n'y ait pas l'extension
    pNomFichierSavf = pNomFichierSavf.toUpperCase();
    if (pNomFichierSavf.endsWith(EXTENSION)) {
      pNomFichierSavf = pNomFichierSavf.substring(0, pNomFichierSavf.length() - EXTENSION.length());
    }
    try {
      SaveFile savf = new SaveFile(pSysteme, pBibliotheque.getNom(), pNomFichierSavf);
      if (!savf.exists()) {
        throw new MessageErreurException("Le fichier SAVF est introuvable.");
      }
      // Analyse du SAVF et restauration du savf
      // ObjectDescription od = savf.getObjectDescription();
      // if (od.getValue(ObjectDescription.SAVE_COMMAND).equals("SAVOBJ"))
      // {
      SaveFileEntry[] liste = savf.listEntries();
      String[] listeFile = new String[liste.length];
      int index = 0;
      for (SaveFileEntry fichier : liste) {
        listeFile[index++] = fichier.getName();
      }
      savf.restore(pNomFichierSavf, listeFile, pNomFichierSavf);
      // }
      // else
      // savf.restore(bib_savf[1]);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de la restauration du SAVF.");
    }
  }
  
}
