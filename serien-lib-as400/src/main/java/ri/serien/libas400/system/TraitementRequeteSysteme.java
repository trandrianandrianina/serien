/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.AS400;

import ri.serien.libas400.system.systemrequest.DataareaRequestActions;
import ri.serien.libas400.system.systemrequest.Db2RequestActions;
import ri.serien.libas400.system.systemrequest.LibraryRequestActions;
import ri.serien.libcommun.outils.protocolemessage.DataareaRequest;
import ri.serien.libcommun.outils.protocolemessage.Db2Request;
import ri.serien.libcommun.outils.protocolemessage.LibraryRequest;
import ri.serien.libcommun.outils.protocolemessage.MessageHead;
import ri.serien.libcommun.outils.protocolemessage.MessageManager;

/**
 * Regroupe toutes les requêtes pour le système.
 */
public class TraitementRequeteSysteme {
  // Constantes
  private static final String REQUETE_INCORRECTE = "Requête incorrecte.";
  private static final String REQUETE_NON_ANALYSABLE = "Requête non analysable.";
  
  // Variables
  private AS400 systeme;
  protected MessageManager mc = new MessageManager(0);
  protected String message;
  protected String reponse;
  
  /**
   * Constructeur.
   */
  public TraitementRequeteSysteme(AS400 system, String amessage) {
    systeme = system;
    message = amessage;
  }
  
  /**
   * Analyse le message reçu.
   */
  protected void analyseMessage() {
    reponse = null;
    if ((message == null) && (message.trim().equals(""))) {
      reponse = REQUETE_INCORRECTE;
      return;
    }
    
    if (!mc.getMessageReceived(message, 0)) {
      reponse = REQUETE_NON_ANALYSABLE;
      return;
    }
    
    if (mc.getHead().getBodycode() == MessageHead.BODY_JSON) {
      if (mc.getBody() instanceof Db2Request) {
        traitementDb2Request();
      }
      else if (mc.getBody() instanceof LibraryRequest) {
        traitementLibraryRequest();
      }
      else if (mc.getBody() instanceof DataareaRequest) {
        traitementDataareaRequest();
      }
      // else if (mc.getBody() instanceof UserspaceRequest) {
      // traitementUserspaceRequest();
      // }
    }
  }
  
  /**
   * Execute une requête sur la base DB2.
   */
  private void traitementDb2Request() {
    Db2Request db2 = (Db2Request) mc.getBody();
    Db2RequestActions tdb2 = new Db2RequestActions(systeme, db2);
    tdb2.actions();
    // mc.createMessage(mc.getHead().getId(), true, db2);
    mc.createMessage(true, db2);
    reponse = mc.getMessageToSend();
  }
  
  /**
   * Execute une requête sur la Library.
   */
  private void traitementLibraryRequest() {
    LibraryRequest rlibrary = (LibraryRequest) mc.getBody();
    LibraryRequestActions tlib = new LibraryRequestActions(systeme, rlibrary);
    tlib.actions();
    // mc.createMessage(mc.getHead().getId(), rlibrary);
    mc.createMessage(rlibrary);
    reponse = mc.getMessageToSend();
  }
  
  /**
   * Execute une requête sur le stream.
   */
  /*
  private void traitementStreamRequest() {
    StreamRequest rstream = (StreamRequest) mc.getBody();
    StreamRequestActions tstream = new StreamRequestActions(systeme, rstream);
    tstream.actions();
    mc.createMessage(rstream);
    reponse = mc.getMessageToSend();
  }*/
  
  /**
   * Execute une requête sur le savf.
   */
  /*
  private void traitementSavfRequest() {
    SavfRequest rsavf = (SavfRequest) mc.getBody();
    SavfRequestActions tsavf = new SavfRequestActions(systeme, rsavf);
    tsavf.actions();
    mc.createMessage(rsavf);
    reponse = mc.getMessageToSend();
  }*/
  
  /**
   * Execute une requête sur les dataarea.
   */
  private void traitementDataareaRequest() {
    DataareaRequest rdta = (DataareaRequest) mc.getBody();
    DataareaRequestActions tdta = new DataareaRequestActions(systeme, rdta);
    tdta.actions();
    mc.createMessage(rdta);
    reponse = mc.getMessageToSend();
  }
  
  /**
   * Execute une requête sur les userspace.
   */
  /*
  private void traitementUserspaceRequest() {
    UserspaceRequest rupc = (UserspaceRequest) mc.getBody();
    UserspaceRequestActions tupc = new UserspaceRequestActions(systeme, rupc);
    tupc.actions();
    mc.createMessage(rupc);
    reponse = mc.getMessageToSend();
  }*/
  
  /**
   * Retourne la réponse à la requête soumise.
   */
  public String getResponse() {
    analyseMessage();
    
    return reponse;
  }
  
  /**
   * Libère la mémoire.
   */
  public void dispose() {
    systeme = null;
    mc.dispose();
  }
}
