/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.job;

import com.ibm.as400.access.Job;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;

public class JobASurveiller {
  // Variables
  private Job job = null;
  private Bibliotheque baseDeDonnees = null;
  private boolean actif = true;
  private EnumStatutJob statut = EnumStatutJob.NON_DEFINI;
  private boolean avertissementEnvoye = false;
  
  /**
   * Constructeur.
   */
  public JobASurveiller(Job pJob, String pNomBaseDeDonnees) {
    job = pJob;
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pNomBaseDeDonnees);
    baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
  }
  
  /**
   * Constructeur.
   */
  public JobASurveiller(Job pJob, Bibliotheque pBaseDeDonnees) {
    job = pJob;
    baseDeDonnees = pBaseDeDonnees;
  }
  
  // -- Méthodes publiques
  
  /**
   * Récupérer le message en fonction du statut.
   */
  public String recupererMessage() {
    String message = "";
    
    // Récupération du message dans la file d'attente
    switch (statut) {
      case ERREUR:
        message = "Le travail " + job + " est en erreur.";
        break;
      
      case VERROUILLE:
        message =
            "Le travail " + job + " est verrouillé." + '\n' + "L'enregistrement sélectionné doit être tenu par un autre utilisateur.";
        break;
      
      default:
        break;
    }
    return message;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne le job.
   */
  public Job getJob() {
    return job;
  }
  
  /**
   * Retourne la base de données du contexte d'exécution du job.
   */
  public Bibliotheque getBaseDeDonnees() {
    return baseDeDonnees;
  }
  
  /**
   * Retourne le statut.
   */
  public EnumStatutJob getStatut() {
    return statut;
  }
  
  /**
   * Initialise le statut.
   */
  public void setStatut(EnumStatutJob statut) {
    this.statut = statut;
  }
  
  /**
   * Retourne si le job est actif.
   * Existe quelque soit son statut.
   */
  public boolean isActif() {
    return actif;
  }
  
  /**
   * Initialise si le job est actif.
   */
  public void setActif(boolean pActif) {
    this.actif = pActif;
  }
  
  /**
   * Retourne si un avertissement a été envoyé.
   */
  public boolean isAvertissementEnvoye() {
    return avertissementEnvoye;
  }
  
  /**
   * Initialise si l'avertissement a été envoyé.
   */
  public void setAvertissementEnvoye(boolean avertissementEnvoye) {
    this.avertissementEnvoye = avertissementEnvoye;
  }
}
