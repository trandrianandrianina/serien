/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.Job;
import com.ibm.as400.access.ProgramCall;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.system.job.JobASurveiller;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Permet d'exécuter un programme RPG ou de lancer une commande.
 * Note (fait chez SMN en V7R3)
 * Il y a eu des problèmes de réutilisation de job ce qui a provoqué des problèmes avec les programme RPG qui testent les dates de la
 * session. La doc suivante https://www.nicklitten.com/ibm-i-server-job-qzrcsrvs-is-a-mouthful-of-wiggly-worms/ recommande de modifier le
 * paramètre MAXUSE comme ceci :
 * CHGPJE SBSD(QUSRWRK) PGM(QSYS/QZRCSRVS) MAXUSE(1)
 */
public class ContexteProgrammeRPG {
  // Constantes
  private static final String PREFIXE_RPG = "[RPG] ";
  
  // Variables
  private AS400 systeme = null;
  private ProgramCall programCall = null;
  private CommandCall commandCall = null;
  private SurveillanceJob surveillanceJob = null;
  private Bibliotheque baseDeDonnees = null;
  private String codeRetour = "";
  
  /**
   * Constructeur.
   * La base de données permet de renseigner un contexte utilisée uniquement pour la surveillance des jobs.
   */
  public ContexteProgrammeRPG(AS400 pSysteme, Bibliotheque pBaseDeDonnees) {
    setSysteme(pSysteme);
    baseDeDonnees = pBaseDeDonnees;
  }
  
  /**
   * Constructeur.
   * A n'utiliser que si la surveillance des jobs n'est pas nécessaire.
   */
  public ContexteProgrammeRPG(AS400 pSysteme) {
    setSysteme(pSysteme);
    baseDeDonnees = null;
  }
  
  /**
   * Constructeur.
   */
  public ContexteProgrammeRPG(AS400 pSysteme, String pNomBaseDeDonnees) {
    setSysteme(pSysteme);
    if (pNomBaseDeDonnees != null) {
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pNomBaseDeDonnees);
      baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Ajout d'une bibliothèque.
   */
  public boolean ajouterBibliotheque(String pNomBibliotheque, boolean pEstCurLib) {
    if (pNomBibliotheque == null) {
      return false;
    }
    
    String chaine;
    if (pEstCurLib) {
      chaine = "CHGCURLIB CURLIB(" + pNomBibliotheque.trim() + ")";
    }
    else {
      chaine = "ADDLIBLE LIB(" + pNomBibliotheque.trim() + ")";
    }
    
    try {
      // Initialer les données d'appel
      if (commandCall == null) {
        commandCall = new CommandCall(systeme);
      }
      commandCall.setCommand(chaine);
      ajouterJobEnSurveillance();
      commandCall.run();
      enleverJobEnSurveillance();
    }
    catch (Exception e) {
      // Générer une exception en cas d'erreur d'exécution fatale du programme RPG
      throw new MessageErreurException(e, "Erreur lors de l'exécution de la commande CL");
    }
    
    return true;
  }
  
  /**
   * Ajout d'une bibliothèque.
   */
  public boolean ajouterBibliotheque(Bibliotheque pBibliotheque, boolean pEstCurLib) {
    if (pBibliotheque == null) {
      return false;
    }
    return ajouterBibliotheque(pBibliotheque.getNom(), pEstCurLib);
  }
  
  /**
   * Initialise la dataarea *LDA avec la lettre d'environnement de Série N.
   */
  public boolean initialiserLettreEnvironnement(char pLettre) {
    String ordre = "CHGDTAARA DTAARA(*LDA (1024 1)) VALUE('" + pLettre + "')";
    try {
      // Initialiser les données d'appel
      if (commandCall == null) {
        commandCall = new CommandCall(systeme);
      }
      commandCall.setCommand(ordre);
      ajouterJobEnSurveillance();
      boolean retour = commandCall.run();
      enleverJobEnSurveillance();
      return retour;
    }
    catch (Exception e) {
      // Générer une exception en cas d'erreur d'exécution fatale du programme RPG
      throw new MessageErreurException(e, "Erreur lors de l'exécution de la commande CL");
    }
  }
  
  /**
   * Initialise la dataarea *LDA avec le profil utilisateur.
   */
  public boolean initialiserProfil(String pProfil) {
    pProfil = Constantes.normerTexte(pProfil);
    String ordre = "CHGDTAARA DTAARA(*LDA (29 10)) VALUE(" + String.format("'%-10s'", pProfil) + ")";
    try {
      // Initialiser les données d'appel
      if (commandCall == null) {
        commandCall = new CommandCall(systeme);
      }
      commandCall.setCommand(ordre);
      ajouterJobEnSurveillance();
      boolean retour = commandCall.run();
      enleverJobEnSurveillance();
      return retour;
    }
    catch (Exception e) {
      // Générer une exception en cas d'erreur d'exécution fatale du programme RPG
      throw new MessageErreurException(e, "Erreur lors de l'exécution de la commande CL");
    }
  }
  
  /**
   * Initialise la dataarea *LDA avec le code d'affichage pour un profil Série N.
   */
  public boolean initialiserCodeAffichageSerieN() {
    String ordre = "CHGDTAARA DTAARA(*LDA (39 1)) VALUE('N')";
    try {
      // Initialiser les données d'appel
      if (commandCall == null) {
        commandCall = new CommandCall(systeme);
      }
      commandCall.setCommand(ordre);
      ajouterJobEnSurveillance();
      boolean retour = commandCall.run();
      enleverJobEnSurveillance();
      return retour;
    }
    catch (Exception e) {
      // Générer une exception en cas d'erreur d'exécution fatale du programme RPG
      throw new MessageErreurException(e, "Erreur lors de l'exécution de la commande CL");
    }
  }
  
  /**
   * Exécuter un programme RPG sur l'AS400.
   * Le nom du programme est sous la forme VERSYS.
   */
  public boolean executerProgramme(String pNomProgramme, Bibliotheque pBibliotheque, ProgramParameter[] pListeParametres) {
    pNomProgramme = Constantes.normerTexte(pNomProgramme);
    if (pBibliotheque == null || pNomProgramme.isEmpty()) {
      throw new MessageErreurException("Erreur lors de la contruction du chemin du programme RPG à exécuter.");
    }
    return executerProgramme(pBibliotheque.getCheminIFS() + "/" + pNomProgramme + ".PGM", pListeParametres);
  }
  
  /**
   * Executer un programme RPG sur l'AS400.
   * Le nom du programme est sous la forme /QSYS.LIb/XEXPAS.LIB/VERSYS.PGM.
   */
  private boolean executerProgramme(String pNomProgramme, ProgramParameter[] pListeParametres) {
    Trace.info(PREFIXE_RPG + "Appel : job=" + getJob().getName() + " user=" + getJob().getUser() + " numero=" + getJob().getNumber()
        + " prg=" + pNomProgramme);
    try {
      // Initialiser le code retour
      codeRetour = "";
      
      // Renseigner les paramètres du programme
      if (pListeParametres == null) {
        programCall.setProgram(pNomProgramme);
      }
      else {
        programCall.setProgram(pNomProgramme, pListeParametres);
      }
      
      // Lancer le programme RPG
      long debut = System.currentTimeMillis();
      ajouterJobEnSurveillance();
      boolean retour = programCall.run();
      enleverJobEnSurveillance();
      long duree = System.currentTimeMillis() - debut;
      
      // Récupérer le code retour
      AS400Message[] listeAS400Message = programCall.getMessageList();
      if (listeAS400Message.length > 0) {
        codeRetour = listeAS400Message[0].getID();
      }
      
      // Vérifier si le programme a retourné une erreur
      if (!retour) {
        String erreur = "";
        for (AS400Message as400Message : listeAS400Message) {
          erreur += as400Message + ", ";
        }
        Trace.alerte(PREFIXE_RPG + "Le programme RPG a retourné un message : " + erreur);
        // On déclenche une exception lorsque l'on détecte l'erreur RNX9998
        // A terme il faudra voir s'il ne vaut pas mieux la déclencher pour tous les id message CEE9901 (mais faut voir les conséquences)
        if (erreur.contains("RNX9998")) {
          Exception e = new Exception(
              "Message : " + erreur + ", Erreur provoquée généralement par un problème de paramètres avec un sous programme appelé.");
          throw new MessageErreurException(e, "");
        }
        
        return false;
      }
      
      // Tracer la fin du programme
      Trace.info(PREFIXE_RPG + "Succès (" + duree + " ms)");
      return true;
    }
    catch (Exception e) {
      // Générer une exception en cas d'erreur d'exécution fatale du programme RPG
      throw new MessageErreurException(e, "Erreur lors de l'exécution du programme RPG " + pNomProgramme);
    }
  }
  
  /**
   * Executer la commande CL sur l'AS400.
   */
  public boolean executerCommande(String pCommande) {
    if ((pCommande == null) || pCommande.trim().equals("")) {
      throw new MessageErreurException("Le nom de la commande CL a exécuter n'est pas renseigné.");
    }
    
    // Tracer le début de l'appel
    Trace.info(PREFIXE_RPG + "Appel : job=" + getJob().getName() + " user=" + getJob().getUser() + " numero=" + getJob().getNumber()
        + " cmd=" + pCommande);
    
    try {
      // Initialiser le code retour
      codeRetour = "";
      
      // Initialer les données d'appel
      if (commandCall == null) {
        commandCall = new CommandCall(systeme);
      }
      commandCall.setCommand(pCommande);
      
      // Exécuter la commande
      long debut = System.currentTimeMillis();
      ajouterJobEnSurveillance();
      boolean retour = commandCall.run();
      enleverJobEnSurveillance();
      long duree = System.currentTimeMillis() - debut;
      
      // Récupérer le code retour
      AS400Message[] listeAS400Message = commandCall.getMessageList();
      if (listeAS400Message.length > 0) {
        codeRetour = listeAS400Message[0].getID();
      }
      
      // Vérifier si la commande a retourné une erreur
      if (!retour) {
        String erreur = "";
        for (AS400Message as400Message : listeAS400Message) {
          erreur += as400Message + ", ";
        }
        Trace.info(PREFIXE_RPG + "La commande CL a retourné un message (code erreur " + codeRetour + ") : " + erreur);
        return false;
      }
      
      // Tracer la fin du programme
      Trace.info(PREFIXE_RPG + "Succès (" + duree + " ms)");
      return true;
    }
    catch (Exception e) {
      // Générer une exception en cas d'erreur d'exécution fatale du programme RPG
      throw new MessageErreurException(e, "Erreur lors de l'exécution de la commande CL " + pCommande);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Obtenir le job du programme.
   */
  private Job getJob() {
    try {
      if (programCall != null) {
        return programCall.getServerJob();
      }
      if (commandCall != null) {
        return commandCall.getServerJob();
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution de la commande CL");
    }
    return null;
  }
  
  /**
   * Ajoute un job à surveiller à la liste.
   */
  private void ajouterJobEnSurveillance() {
    // Si surveillanceJob n'est pas initialisé alors rien n'est fait
    if (surveillanceJob == null) {
      return;
    }
    JobASurveiller jobASurveiller = new JobASurveiller(getJob(), baseDeDonnees);
    surveillanceJob.ajouterJob(jobASurveiller);
  }
  
  /**
   * Supprime un job à surveiller de la liste.
   */
  private void enleverJobEnSurveillance() {
    // Si surveillanceJob n'est pas initialisé alors rien n'est fait
    if (surveillanceJob == null) {
      return;
    }
    surveillanceJob.enleverJob(getJob());
  }
  
  // -- Accesseurs
  
  /**
   * @param pSysteme le systeme à définir.
   */
  private void setSysteme(AS400 pSysteme) {
    this.systeme = pSysteme;
    if (pSysteme != null) {
      programCall = new ProgramCall(pSysteme);
      executerCommande("CHGJOB RUNPTY(10)");
    }
  }
  
  /**
   * Initialise la thread de surveillance des jobs.
   */
  public void setSurveillanceJob(SurveillanceJob pSurveillanceJob) {
    surveillanceJob = pSurveillanceJob;
  }
  
  /**
   * Retourne la base de données pour ce contexte d'exécution.
   */
  public Bibliotheque getBaseDeDonnees() {
    return baseDeDonnees;
  }
  
  /**
   * Code retour du programme RPG ou de la commande CL.
   * Le code retour correspond a un code d'erreur si l'exécution a retournée false.
   */
  public String getCodeRetour() {
    return codeRetour;
  }
}
