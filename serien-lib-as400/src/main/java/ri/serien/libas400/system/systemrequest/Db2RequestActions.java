/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.system.systemrequest;

import java.util.ArrayList;

import com.ibm.as400.access.AS400;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.protocolemessage.Db2Request;

public class Db2RequestActions {
  private AS400 sysAS400 = null;
  private Db2Request rdb2 = null;

  /**
   * Constructeur.
   */
  public Db2RequestActions(AS400 system, Db2Request ldb) {
    sysAS400 = system;
    rdb2 = ldb;
  }

  // --> Méthodes publiques <------------------------------------------------

  /**
   * Traitement des actions possibles.
   */
  public void actions() {
    // int theactions = actions;
    for (int i = Db2Request.ACTIONS.size(); --i > 0;) {
      if (rdb2.getActions() >= Db2Request.ACTIONS.get(i)) {
        switch (Db2Request.ACTIONS.get(i)) {
          case Db2Request.EXECUTE:
            executeRequest();
            break;
          default:
            break;
        }
        rdb2.setActions(rdb2.getActions() - Db2Request.ACTIONS.get(i));
      }
    }
  }

  // --> Méthodes privées <--------------------------------------------------

  /**
   * Execute une requête sur la base DB2 A affiner si select sans resultat mais pas en erreur alors faudrait au moins
   * les titres de colonnes.
   */
  private void executeRequest() {
    SystemeManager manager = new SystemeManager(sysAS400, true);
    QueryManager query = new QueryManager(manager.getDatabaseManager().getConnection());

    if (rdb2.isSelect()) {
      ArrayList<GenericRecord> liste = query.select(rdb2.getRequest());
      // liste==null?"null":liste.size());
      if (liste == null) {
        rdb2.setSuccess(false);
      }
      else if (liste.isEmpty()) {
        rdb2.setSuccess(true);
        rdb2.setTData(null);
      }
      else {
        rdb2.setSuccess(true);
        rdb2.setTTitle(liste.get(0).getArrayTitle());
        Object[][] data = new Object[liste.size()][];
        /*
         * for (int i=0; i<liste.size(); i++) { data[i] = liste.get(i).getArrayValue(true); }
         * liste.clear();
         */
        int index = 0;
        while (liste.size() > 0) {
          data[index++] = liste.get(0).getArrayValue(true);
          liste.remove(0);
        }
        rdb2.setTData(data);
      }
    }
    else if (query.requete(rdb2.getRequest()) == -1) {
      rdb2.setSuccess(false);
    }
    else {
      rdb2.setSuccess(true);
    }

    rdb2.setMsgError(query.getMsgError());
    query.deconnecter();
  }

}
