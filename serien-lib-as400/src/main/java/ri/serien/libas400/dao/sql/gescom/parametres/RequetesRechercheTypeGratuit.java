/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;

public class RequetesRechercheTypeGratuit {
  
  /**
   * Retourne la requête en fonction du type de recherche.
   */
  public static String getRequete(String curlib, CriteresRechercheTypeGratuit criteres) {
    String requete = null;
    if (criteres == null) {
      return requete;
    }
    
    switch (criteres.getTypeRecherche()) {
      case CriteresRechercheTypeGratuit.RECHERCHE_TYPE_GRATUIT:
        criteres.setNumeroPage(1);
        criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
        requete = getRequeteRechercheToutTypeGratuit(curlib, criteres);
        break;
    }
    return ajouterPagination(requete, criteres);
  }
  
  /**
   * Retourner la liste des types de gratuit pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   */
  private static String getRequeteRechercheToutTypeGratuit(String curlib, CriteresRechercheTypeGratuit aCriteres) {
    return "Select * from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTYP = 'TG' ORDER BY PARZ2";
  }
  
  /**
   * Construit la requête qui permet de rechercher une expression dans le nom de la section analytique.
   */
  private static String getRequeteRechercheDansLibelle(String curlib, CriteresRechercheTypeGratuit criteres) {
    String conditions;
    String critere = criteres.getCriterePourRequete();
    if (criteres.isIgnoreCasse()) {
      conditions = "UPPER(SUBSTR(PARZ2, 1, 40)) like '" + critere + "'";
    }
    else {
      conditions = "SUBSTR(PARZ2, 1, 40) like '" + critere + "'";
    }
    return "select * from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where PARTYP = 'TG' and (" + conditions + ")";
  }
  
  /**
   * Ajoute les ordres afin de récupérer qu'une quantité limité de lignes pour la pagination.
   */
  private static String ajouterPagination(String requete, CriteresBaseRecherche criteres) {
    if (criteres.getNbrLignesParPage() != CriteresBaseRecherche.TOUTES_LES_LIGNES) {
      int nbreltpage = criteres.getNbrLignesParPage();
      int indexdeb = (criteres.getNumeroPage() * nbreltpage) - (nbreltpage - 1);
      int indexfin = (indexdeb + nbreltpage) - 1;
      requete = "with vue as (" + requete + ") select * from vue where numrow between " + indexdeb + " and " + indexfin;
    }
    return requete;
  }
}
