/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.standard;

/**
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */
import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgam0002i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICOL = 1;
  public static final int DECIMAL_PICOL = 0;
  public static final int SIZE_PIFRS = 6;
  public static final int DECIMAL_PIFRS = 0;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PIA01 = 20;
  public static final int SIZE_PIA02 = 20;
  public static final int SIZE_PIA03 = 20;
  public static final int SIZE_PIA04 = 20;
  public static final int SIZE_PIA05 = 20;
  public static final int SIZE_PIA06 = 20;
  public static final int SIZE_PIA07 = 20;
  public static final int SIZE_PIA08 = 20;
  public static final int SIZE_PIA09 = 20;
  public static final int SIZE_PIA10 = 20;
  public static final int SIZE_PIA11 = 20;
  public static final int SIZE_PIA12 = 20;
  public static final int SIZE_PIA13 = 20;
  public static final int SIZE_PIA14 = 20;
  public static final int SIZE_PIA15 = 20;
  public static final int SIZE_PIA16 = 20;
  public static final int SIZE_PIA17 = 20;
  public static final int SIZE_PIA18 = 20;
  public static final int SIZE_PIA19 = 20;
  public static final int SIZE_PIA20 = 20;
  public static final int SIZE_PIA21 = 20;
  public static final int SIZE_PIA22 = 20;
  public static final int SIZE_PIA23 = 20;
  public static final int SIZE_PIA24 = 20;
  public static final int SIZE_PIA25 = 20;
  public static final int SIZE_PIA26 = 20;
  public static final int SIZE_PIA27 = 20;
  public static final int SIZE_PIA28 = 20;
  public static final int SIZE_PIA29 = 20;
  public static final int SIZE_PIA30 = 20;
  public static final int SIZE_PIA31 = 20;
  public static final int SIZE_PIA32 = 20;
  public static final int SIZE_PIA33 = 20;
  public static final int SIZE_PIA34 = 20;
  public static final int SIZE_PIA35 = 20;
  public static final int SIZE_PIA36 = 20;
  public static final int SIZE_PIA37 = 20;
  public static final int SIZE_PIA38 = 20;
  public static final int SIZE_PIA39 = 20;
  public static final int SIZE_PIA40 = 20;
  public static final int SIZE_PIA41 = 20;
  public static final int SIZE_PIA42 = 20;
  public static final int SIZE_PIA43 = 20;
  public static final int SIZE_PIA44 = 20;
  public static final int SIZE_PIA45 = 20;
  public static final int SIZE_PIA46 = 20;
  public static final int SIZE_PIA47 = 20;
  public static final int SIZE_PIA48 = 20;
  public static final int SIZE_PIA49 = 20;
  public static final int SIZE_PIA50 = 20;
  public static final int SIZE_PIA51 = 20;
  public static final int SIZE_PIA52 = 20;
  public static final int SIZE_PIA53 = 20;
  public static final int SIZE_PIA54 = 20;
  public static final int SIZE_PIA55 = 20;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 1123;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICOL = 2;
  public static final int VAR_PIFRS = 3;
  public static final int VAR_PIMAG = 4;
  public static final int VAR_PIA01 = 5;
  public static final int VAR_PIA02 = 6;
  public static final int VAR_PIA03 = 7;
  public static final int VAR_PIA04 = 8;
  public static final int VAR_PIA05 = 9;
  public static final int VAR_PIA06 = 10;
  public static final int VAR_PIA07 = 11;
  public static final int VAR_PIA08 = 12;
  public static final int VAR_PIA09 = 13;
  public static final int VAR_PIA10 = 14;
  public static final int VAR_PIA11 = 15;
  public static final int VAR_PIA12 = 16;
  public static final int VAR_PIA13 = 17;
  public static final int VAR_PIA14 = 18;
  public static final int VAR_PIA15 = 19;
  public static final int VAR_PIA16 = 20;
  public static final int VAR_PIA17 = 21;
  public static final int VAR_PIA18 = 22;
  public static final int VAR_PIA19 = 23;
  public static final int VAR_PIA20 = 24;
  public static final int VAR_PIA21 = 25;
  public static final int VAR_PIA22 = 26;
  public static final int VAR_PIA23 = 27;
  public static final int VAR_PIA24 = 28;
  public static final int VAR_PIA25 = 29;
  public static final int VAR_PIA26 = 30;
  public static final int VAR_PIA27 = 31;
  public static final int VAR_PIA28 = 32;
  public static final int VAR_PIA29 = 33;
  public static final int VAR_PIA30 = 34;
  public static final int VAR_PIA31 = 35;
  public static final int VAR_PIA32 = 36;
  public static final int VAR_PIA33 = 37;
  public static final int VAR_PIA34 = 38;
  public static final int VAR_PIA35 = 39;
  public static final int VAR_PIA36 = 40;
  public static final int VAR_PIA37 = 41;
  public static final int VAR_PIA38 = 42;
  public static final int VAR_PIA39 = 43;
  public static final int VAR_PIA40 = 44;
  public static final int VAR_PIA41 = 45;
  public static final int VAR_PIA42 = 46;
  public static final int VAR_PIA43 = 47;
  public static final int VAR_PIA44 = 48;
  public static final int VAR_PIA45 = 49;
  public static final int VAR_PIA46 = 50;
  public static final int VAR_PIA47 = 51;
  public static final int VAR_PIA48 = 52;
  public static final int VAR_PIA49 = 53;
  public static final int VAR_PIA50 = 54;
  public static final int VAR_PIA51 = 55;
  public static final int VAR_PIA52 = 56;
  public static final int VAR_PIA53 = 57;
  public static final int VAR_PIA54 = 58;
  public static final int VAR_PIA55 = 59;
  public static final int VAR_PIARR = 60;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private BigDecimal picol = BigDecimal.ZERO; // Collectif fournisseur
  private BigDecimal pifrs = BigDecimal.ZERO; // Code fournisseur
  private String pimag = ""; // Magasin ou Blanc pour Stock
  private String pia01 = ""; //
  private String pia02 = ""; //
  private String pia03 = ""; //
  private String pia04 = ""; //
  private String pia05 = ""; //
  private String pia06 = ""; //
  private String pia07 = ""; //
  private String pia08 = ""; //
  private String pia09 = ""; //
  private String pia10 = ""; //
  private String pia11 = ""; //
  private String pia12 = ""; //
  private String pia13 = ""; //
  private String pia14 = ""; //
  private String pia15 = ""; //
  private String pia16 = ""; //
  private String pia17 = ""; //
  private String pia18 = ""; //
  private String pia19 = ""; //
  private String pia20 = ""; //
  private String pia21 = ""; //
  private String pia22 = ""; //
  private String pia23 = ""; //
  private String pia24 = ""; //
  private String pia25 = ""; //
  private String pia26 = ""; //
  private String pia27 = ""; //
  private String pia28 = ""; //
  private String pia29 = ""; //
  private String pia30 = ""; //
  private String pia31 = ""; //
  private String pia32 = ""; //
  private String pia33 = ""; //
  private String pia34 = ""; //
  private String pia35 = ""; //
  private String pia36 = ""; //
  private String pia37 = ""; //
  private String pia38 = ""; //
  private String pia39 = ""; //
  private String pia40 = ""; //
  private String pia41 = ""; //
  private String pia42 = ""; //
  private String pia43 = ""; //
  private String pia44 = ""; //
  private String pia45 = ""; //
  private String pia46 = ""; //
  private String pia47 = ""; //
  private String pia48 = ""; //
  private String pia49 = ""; //
  private String pia50 = ""; //
  private String pia51 = ""; //
  private String pia52 = ""; //
  private String pia53 = ""; //
  private String pia54 = ""; //
  private String pia55 = ""; //
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PICOL, DECIMAL_PICOL), // Collectif fournisseur
      new AS400ZonedDecimal(SIZE_PIFRS, DECIMAL_PIFRS), // Code fournisseur
      new AS400Text(SIZE_PIMAG), // Magasin ou Blanc pour Stock
      new AS400Text(SIZE_PIA01), //
      new AS400Text(SIZE_PIA02), //
      new AS400Text(SIZE_PIA03), //
      new AS400Text(SIZE_PIA04), //
      new AS400Text(SIZE_PIA05), //
      new AS400Text(SIZE_PIA06), //
      new AS400Text(SIZE_PIA07), //
      new AS400Text(SIZE_PIA08), //
      new AS400Text(SIZE_PIA09), //
      new AS400Text(SIZE_PIA10), //
      new AS400Text(SIZE_PIA11), //
      new AS400Text(SIZE_PIA12), //
      new AS400Text(SIZE_PIA13), //
      new AS400Text(SIZE_PIA14), //
      new AS400Text(SIZE_PIA15), //
      new AS400Text(SIZE_PIA16), //
      new AS400Text(SIZE_PIA17), //
      new AS400Text(SIZE_PIA18), //
      new AS400Text(SIZE_PIA19), //
      new AS400Text(SIZE_PIA20), //
      new AS400Text(SIZE_PIA21), //
      new AS400Text(SIZE_PIA22), //
      new AS400Text(SIZE_PIA23), //
      new AS400Text(SIZE_PIA24), //
      new AS400Text(SIZE_PIA25), //
      new AS400Text(SIZE_PIA26), //
      new AS400Text(SIZE_PIA27), //
      new AS400Text(SIZE_PIA28), //
      new AS400Text(SIZE_PIA29), //
      new AS400Text(SIZE_PIA30), //
      new AS400Text(SIZE_PIA31), //
      new AS400Text(SIZE_PIA32), //
      new AS400Text(SIZE_PIA33), //
      new AS400Text(SIZE_PIA34), //
      new AS400Text(SIZE_PIA35), //
      new AS400Text(SIZE_PIA36), //
      new AS400Text(SIZE_PIA37), //
      new AS400Text(SIZE_PIA38), //
      new AS400Text(SIZE_PIA39), //
      new AS400Text(SIZE_PIA40), //
      new AS400Text(SIZE_PIA41), //
      new AS400Text(SIZE_PIA42), //
      new AS400Text(SIZE_PIA43), //
      new AS400Text(SIZE_PIA44), //
      new AS400Text(SIZE_PIA45), //
      new AS400Text(SIZE_PIA46), //
      new AS400Text(SIZE_PIA47), //
      new AS400Text(SIZE_PIA48), //
      new AS400Text(SIZE_PIA49), //
      new AS400Text(SIZE_PIA50), //
      new AS400Text(SIZE_PIA51), //
      new AS400Text(SIZE_PIA52), //
      new AS400Text(SIZE_PIA53), //
      new AS400Text(SIZE_PIA54), //
      new AS400Text(SIZE_PIA55), //
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, picol, pifrs, pimag, pia01, pia02, pia03, pia04, pia05, pia06, pia07, pia08, pia09, pia10, pia11,
          pia12, pia13, pia14, pia15, pia16, pia17, pia18, pia19, pia20, pia21, pia22, pia23, pia24, pia25, pia26, pia27, pia28, pia29,
          pia30, pia31, pia32, pia33, pia34, pia35, pia36, pia37, pia38, pia39, pia40, pia41, pia42, pia43, pia44, pia45, pia46, pia47,
          pia48, pia49, pia50, pia51, pia52, pia53, pia54, pia55, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picol = (BigDecimal) output[2];
    pifrs = (BigDecimal) output[3];
    pimag = (String) output[4];
    pia01 = (String) output[5];
    pia02 = (String) output[6];
    pia03 = (String) output[7];
    pia04 = (String) output[8];
    pia05 = (String) output[9];
    pia06 = (String) output[10];
    pia07 = (String) output[11];
    pia08 = (String) output[12];
    pia09 = (String) output[13];
    pia10 = (String) output[14];
    pia11 = (String) output[15];
    pia12 = (String) output[16];
    pia13 = (String) output[17];
    pia14 = (String) output[18];
    pia15 = (String) output[19];
    pia16 = (String) output[20];
    pia17 = (String) output[21];
    pia18 = (String) output[22];
    pia19 = (String) output[23];
    pia20 = (String) output[24];
    pia21 = (String) output[25];
    pia22 = (String) output[26];
    pia23 = (String) output[27];
    pia24 = (String) output[28];
    pia25 = (String) output[29];
    pia26 = (String) output[30];
    pia27 = (String) output[31];
    pia28 = (String) output[32];
    pia29 = (String) output[33];
    pia30 = (String) output[34];
    pia31 = (String) output[35];
    pia32 = (String) output[36];
    pia33 = (String) output[37];
    pia34 = (String) output[38];
    pia35 = (String) output[39];
    pia36 = (String) output[40];
    pia37 = (String) output[41];
    pia38 = (String) output[42];
    pia39 = (String) output[43];
    pia40 = (String) output[44];
    pia41 = (String) output[45];
    pia42 = (String) output[46];
    pia43 = (String) output[47];
    pia44 = (String) output[48];
    pia45 = (String) output[49];
    pia46 = (String) output[50];
    pia47 = (String) output[51];
    pia48 = (String) output[52];
    pia49 = (String) output[53];
    pia50 = (String) output[54];
    pia51 = (String) output[55];
    pia52 = (String) output[56];
    pia53 = (String) output[57];
    pia54 = (String) output[58];
    pia55 = (String) output[59];
    piarr = (String) output[60];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicol(BigDecimal pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = pPicol.setScale(DECIMAL_PICOL, RoundingMode.HALF_UP);
  }
  
  public void setPicol(Integer pPicol) {
    if (pPicol == null) {
      return;
    }
    picol = BigDecimal.valueOf(pPicol);
  }
  
  public Integer getPicol() {
    return picol.intValue();
  }
  
  public void setPifrs(BigDecimal pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = pPifrs.setScale(DECIMAL_PIFRS, RoundingMode.HALF_UP);
  }
  
  public void setPifrs(Integer pPifrs) {
    if (pPifrs == null) {
      return;
    }
    pifrs = BigDecimal.valueOf(pPifrs);
  }
  
  public Integer getPifrs() {
    return pifrs.intValue();
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPia01(String pPia01) {
    if (pPia01 == null) {
      return;
    }
    pia01 = pPia01;
  }
  
  public String getPia01() {
    return pia01;
  }
  
  public void setPia02(String pPia02) {
    if (pPia02 == null) {
      return;
    }
    pia02 = pPia02;
  }
  
  public String getPia02() {
    return pia02;
  }
  
  public void setPia03(String pPia03) {
    if (pPia03 == null) {
      return;
    }
    pia03 = pPia03;
  }
  
  public String getPia03() {
    return pia03;
  }
  
  public void setPia04(String pPia04) {
    if (pPia04 == null) {
      return;
    }
    pia04 = pPia04;
  }
  
  public String getPia04() {
    return pia04;
  }
  
  public void setPia05(String pPia05) {
    if (pPia05 == null) {
      return;
    }
    pia05 = pPia05;
  }
  
  public String getPia05() {
    return pia05;
  }
  
  public void setPia06(String pPia06) {
    if (pPia06 == null) {
      return;
    }
    pia06 = pPia06;
  }
  
  public String getPia06() {
    return pia06;
  }
  
  public void setPia07(String pPia07) {
    if (pPia07 == null) {
      return;
    }
    pia07 = pPia07;
  }
  
  public String getPia07() {
    return pia07;
  }
  
  public void setPia08(String pPia08) {
    if (pPia08 == null) {
      return;
    }
    pia08 = pPia08;
  }
  
  public String getPia08() {
    return pia08;
  }
  
  public void setPia09(String pPia09) {
    if (pPia09 == null) {
      return;
    }
    pia09 = pPia09;
  }
  
  public String getPia09() {
    return pia09;
  }
  
  public void setPia10(String pPia10) {
    if (pPia10 == null) {
      return;
    }
    pia10 = pPia10;
  }
  
  public String getPia10() {
    return pia10;
  }
  
  public void setPia11(String pPia11) {
    if (pPia11 == null) {
      return;
    }
    pia11 = pPia11;
  }
  
  public String getPia11() {
    return pia11;
  }
  
  public void setPia12(String pPia12) {
    if (pPia12 == null) {
      return;
    }
    pia12 = pPia12;
  }
  
  public String getPia12() {
    return pia12;
  }
  
  public void setPia13(String pPia13) {
    if (pPia13 == null) {
      return;
    }
    pia13 = pPia13;
  }
  
  public String getPia13() {
    return pia13;
  }
  
  public void setPia14(String pPia14) {
    if (pPia14 == null) {
      return;
    }
    pia14 = pPia14;
  }
  
  public String getPia14() {
    return pia14;
  }
  
  public void setPia15(String pPia15) {
    if (pPia15 == null) {
      return;
    }
    pia15 = pPia15;
  }
  
  public String getPia15() {
    return pia15;
  }
  
  public void setPia16(String pPia16) {
    if (pPia16 == null) {
      return;
    }
    pia16 = pPia16;
  }
  
  public String getPia16() {
    return pia16;
  }
  
  public void setPia17(String pPia17) {
    if (pPia17 == null) {
      return;
    }
    pia17 = pPia17;
  }
  
  public String getPia17() {
    return pia17;
  }
  
  public void setPia18(String pPia18) {
    if (pPia18 == null) {
      return;
    }
    pia18 = pPia18;
  }
  
  public String getPia18() {
    return pia18;
  }
  
  public void setPia19(String pPia19) {
    if (pPia19 == null) {
      return;
    }
    pia19 = pPia19;
  }
  
  public String getPia19() {
    return pia19;
  }
  
  public void setPia20(String pPia20) {
    if (pPia20 == null) {
      return;
    }
    pia20 = pPia20;
  }
  
  public String getPia20() {
    return pia20;
  }
  
  public void setPia21(String pPia21) {
    if (pPia21 == null) {
      return;
    }
    pia21 = pPia21;
  }
  
  public String getPia21() {
    return pia21;
  }
  
  public void setPia22(String pPia22) {
    if (pPia22 == null) {
      return;
    }
    pia22 = pPia22;
  }
  
  public String getPia22() {
    return pia22;
  }
  
  public void setPia23(String pPia23) {
    if (pPia23 == null) {
      return;
    }
    pia23 = pPia23;
  }
  
  public String getPia23() {
    return pia23;
  }
  
  public void setPia24(String pPia24) {
    if (pPia24 == null) {
      return;
    }
    pia24 = pPia24;
  }
  
  public String getPia24() {
    return pia24;
  }
  
  public void setPia25(String pPia25) {
    if (pPia25 == null) {
      return;
    }
    pia25 = pPia25;
  }
  
  public String getPia25() {
    return pia25;
  }
  
  public void setPia26(String pPia26) {
    if (pPia26 == null) {
      return;
    }
    pia26 = pPia26;
  }
  
  public String getPia26() {
    return pia26;
  }
  
  public void setPia27(String pPia27) {
    if (pPia27 == null) {
      return;
    }
    pia27 = pPia27;
  }
  
  public String getPia27() {
    return pia27;
  }
  
  public void setPia28(String pPia28) {
    if (pPia28 == null) {
      return;
    }
    pia28 = pPia28;
  }
  
  public String getPia28() {
    return pia28;
  }
  
  public void setPia29(String pPia29) {
    if (pPia29 == null) {
      return;
    }
    pia29 = pPia29;
  }
  
  public String getPia29() {
    return pia29;
  }
  
  public void setPia30(String pPia30) {
    if (pPia30 == null) {
      return;
    }
    pia30 = pPia30;
  }
  
  public String getPia30() {
    return pia30;
  }
  
  public void setPia31(String pPia31) {
    if (pPia31 == null) {
      return;
    }
    pia31 = pPia31;
  }
  
  public String getPia31() {
    return pia31;
  }
  
  public void setPia32(String pPia32) {
    if (pPia32 == null) {
      return;
    }
    pia32 = pPia32;
  }
  
  public String getPia32() {
    return pia32;
  }
  
  public void setPia33(String pPia33) {
    if (pPia33 == null) {
      return;
    }
    pia33 = pPia33;
  }
  
  public String getPia33() {
    return pia33;
  }
  
  public void setPia34(String pPia34) {
    if (pPia34 == null) {
      return;
    }
    pia34 = pPia34;
  }
  
  public String getPia34() {
    return pia34;
  }
  
  public void setPia35(String pPia35) {
    if (pPia35 == null) {
      return;
    }
    pia35 = pPia35;
  }
  
  public String getPia35() {
    return pia35;
  }
  
  public void setPia36(String pPia36) {
    if (pPia36 == null) {
      return;
    }
    pia36 = pPia36;
  }
  
  public String getPia36() {
    return pia36;
  }
  
  public void setPia37(String pPia37) {
    if (pPia37 == null) {
      return;
    }
    pia37 = pPia37;
  }
  
  public String getPia37() {
    return pia37;
  }
  
  public void setPia38(String pPia38) {
    if (pPia38 == null) {
      return;
    }
    pia38 = pPia38;
  }
  
  public String getPia38() {
    return pia38;
  }
  
  public void setPia39(String pPia39) {
    if (pPia39 == null) {
      return;
    }
    pia39 = pPia39;
  }
  
  public String getPia39() {
    return pia39;
  }
  
  public void setPia40(String pPia40) {
    if (pPia40 == null) {
      return;
    }
    pia40 = pPia40;
  }
  
  public String getPia40() {
    return pia40;
  }
  
  public void setPia41(String pPia41) {
    if (pPia41 == null) {
      return;
    }
    pia41 = pPia41;
  }
  
  public String getPia41() {
    return pia41;
  }
  
  public void setPia42(String pPia42) {
    if (pPia42 == null) {
      return;
    }
    pia42 = pPia42;
  }
  
  public String getPia42() {
    return pia42;
  }
  
  public void setPia43(String pPia43) {
    if (pPia43 == null) {
      return;
    }
    pia43 = pPia43;
  }
  
  public String getPia43() {
    return pia43;
  }
  
  public void setPia44(String pPia44) {
    if (pPia44 == null) {
      return;
    }
    pia44 = pPia44;
  }
  
  public String getPia44() {
    return pia44;
  }
  
  public void setPia45(String pPia45) {
    if (pPia45 == null) {
      return;
    }
    pia45 = pPia45;
  }
  
  public String getPia45() {
    return pia45;
  }
  
  public void setPia46(String pPia46) {
    if (pPia46 == null) {
      return;
    }
    pia46 = pPia46;
  }
  
  public String getPia46() {
    return pia46;
  }
  
  public void setPia47(String pPia47) {
    if (pPia47 == null) {
      return;
    }
    pia47 = pPia47;
  }
  
  public String getPia47() {
    return pia47;
  }
  
  public void setPia48(String pPia48) {
    if (pPia48 == null) {
      return;
    }
    pia48 = pPia48;
  }
  
  public String getPia48() {
    return pia48;
  }
  
  public void setPia49(String pPia49) {
    if (pPia49 == null) {
      return;
    }
    pia49 = pPia49;
  }
  
  public String getPia49() {
    return pia49;
  }
  
  public void setPia50(String pPia50) {
    if (pPia50 == null) {
      return;
    }
    pia50 = pPia50;
  }
  
  public String getPia50() {
    return pia50;
  }
  
  public void setPia51(String pPia51) {
    if (pPia51 == null) {
      return;
    }
    pia51 = pPia51;
  }
  
  public String getPia51() {
    return pia51;
  }
  
  public void setPia52(String pPia52) {
    if (pPia52 == null) {
      return;
    }
    pia52 = pPia52;
  }
  
  public String getPia52() {
    return pia52;
  }
  
  public void setPia53(String pPia53) {
    if (pPia53 == null) {
      return;
    }
    pia53 = pPia53;
  }
  
  public String getPia53() {
    return pia53;
  }
  
  public void setPia54(String pPia54) {
    if (pPia54 == null) {
      return;
    }
    pia54 = pPia54;
  }
  
  public String getPia54() {
    return pia54;
  }
  
  public void setPia55(String pPia55) {
    if (pPia55 == null) {
      return;
    }
    pia55 = pPia55;
  }
  
  public String getPia55() {
    return pia55;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
