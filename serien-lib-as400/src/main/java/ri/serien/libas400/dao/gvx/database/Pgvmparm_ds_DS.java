/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_DS pour les DS
 */
public class Pgvmparm_ds_DS extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "DSLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "DSR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "DSI"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "DSS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "DSCDP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DSPE1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DSPE2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "DST"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(20), "DSJ"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "DSK"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(40), "DSR4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(60), "DSK4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DSPE3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "DSREA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "DSMAG"));
    
    length = 300;
  }
}
