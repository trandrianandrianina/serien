/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.client.modification;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgSauverClient extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0023";
  
  /**
   * Appel du programme RPG qui va modifier un client.
   */
  public boolean sauverClient(SystemeManager pSysteme, Client pClient) {
    if (pSysteme == null) {
      throw new MessageErreurException("Impossible de sauver le client.");
    }
    if (pClient == null) {
      throw new MessageErreurException("Impossible de sauver le client car il est invalide.");
    }
    if (pClient.getContactPrincipal() == null) {
      throw new MessageErreurException("Impossible de sauver le client car il n'a pas de contact principal.");
    }
    
    try {
      // Préparation des paramètres du programme RPG
      Svgvm0023i entree = new Svgvm0023i();
      Svgvm0023o sortie = new Svgvm0023o();
      ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
          sortie, // Paramètres de sortie
          erreur // Paramètres d'erreur
      };
      
      // Initialisation des paramètres d'entrée
      // Si PiIND1('1', l'erl client passé par paramètre (de PiNOM à PiIN30) remplace toutes les zones de PGVMCLIM en block.
      // Si PiIND2 <> '1', voici la liste des zones mises à jour
      entree.setPiind("1000000000");
      entree.setPietb(pClient.getId().getCodeEtablissement());
      entree.setPicli(pClient.getId().getNumero());
      entree.setPiliv(pClient.getId().getSuffixe());
      entree.setPinom(pClient.getAdresse().getNom());
      entree.setPicpl(pClient.getAdresse().getComplementNom());
      entree.setPirue(pClient.getAdresse().getRue());
      entree.setPiloc(pClient.getAdresse().getLocalisation());
      entree.setPicdp(pClient.getAdresse().getCodePostalFormate());
      entree.setPicdp1(pClient.getAdresse().getCodePostal());
      entree.setPivil(pClient.getAdresse().getVille());
      entree.setPipay(pClient.getAdresse().getPays());
      entree.setPitop1(pClient.getCritereAnalyse1());
      entree.setPitop2(pClient.getCritereAnalyse2());
      entree.setPitop3(pClient.getCritereAnalyse3());
      entree.setPitop4(pClient.getCritereAnalyse4());
      entree.setPitop5(pClient.getCritereAnalyse5());
      if (pClient.getIdCategorieClient() != null) {
        entree.setPicat(pClient.getIdCategorieClient().getCode());
      }
      entree.setPiclk(pClient.getCleClassement());
      entree.setPiape(pClient.getCodeAPE());
      entree.setPitel(pClient.getNumeroTelephone());
      entree.setPifax(pClient.getNumeroFax());
      entree.setPilan(pClient.getCodeLangue());
      entree.setPiobs(pClient.getObservations());
      if (pClient.getIdRepresentant() != null) {
        entree.setPirep(pClient.getIdRepresentant().getCode());
      }
      if (pClient.getIdRepresentant2() != null) {
        entree.setPirep2(pClient.getIdRepresentant2().getCode());
      }
      if (pClient.getTypeCommissionRepresentant() != null && !pClient.getTypeCommissionRepresentant().isEmpty()) {
        entree.setPitrp(pClient.getTypeCommissionRepresentant().charAt(0));
      }
      entree.setPigeo(pClient.getZoneGeographique());
      entree.setPimex(pClient.getModeExpedition());
      if (pClient.getIdTransporteur() != null) {
        entree.setPictr(pClient.getIdTransporteur().getCode());
      }
      if (pClient.getIdMagasinRattachement() != null) {
        entree.setPimag(pClient.getIdMagasinRattachement().getCode());
      }
      entree.setPifrp(pClient.getMontantFrancoPort());
      if (pClient.getIdTypeFacturation() != null) {
        entree.setPitfa(pClient.getIdTypeFacturation().getCode().charAt(0));
      }
      else {
        entree.setPitfa(' ');
      }
      if (pClient.isFactureEnTTC()) {
        entree.setPittc('1');
      }
      else {
        entree.setPittc(' ');
      }
      entree.setPidev(pClient.getCodeDevise());
      entree.setPiclfp(pClient.getCodeClientFacture());
      entree.setPiclfs(pClient.getSuffixeLivClientFacture());
      entree.setPiclp(pClient.getCodeClientPayeur());
      entree.setPiafa(pClient.getCodeAffacturage());
      entree.setPiesc(pClient.getTauxEscompte());
      entree.setPitar(pClient.getNumeroColonneTarif());
      entree.setPita1(pClient.getNumeroTarif2());
      entree.setPirem1(pClient.getRemise1());
      entree.setPirem2(pClient.getRemise2());
      entree.setPirem3(pClient.getRemise3());
      entree.setPirp1(pClient.getRemisePied1());
      entree.setPirp2(pClient.getRemisePied1());
      entree.setPirp3(pClient.getRemisePied1());
      entree.setPicnv(pClient.getCodeCNV());
      if (pClient.getPeriodiciteFacturation() != null && !pClient.getPeriodiciteFacturation().isEmpty()) {
        entree.setPipfa(pClient.getPeriodiciteFacturation().charAt(0));
      }
      if (pClient.getPeriodiciteReleve() != null && !pClient.getPeriodiciteReleve().isEmpty()) {
        entree.setPiprl(pClient.getPeriodiciteReleve().charAt(0));
      }
      entree.setPirbf(pClient.getRegroupementBonSurFacture());
      if (pClient.getIdReglement() != null) {
        entree.setPirgl(pClient.getIdReglement().getCode());
      }
      entree.setPiech(pClient.getCodeEcheance());
      entree.setPincg(pClient.getCollectifComptable());
      entree.setPinca(pClient.getCompteAuxiliaire());
      entree.setPiact(pClient.getCodeAffaireEnCours());
      entree.setPiplf(pClient.getPlafondEncoursAutorise());
      entree.setPiplf2(pClient.getSurPlafondEncoursAutorise());
      entree.setPitns(pClient.getTopAttention().getCode());
      entree.setPiatt(pClient.getTexteAttentionCommande());
      entree.setPinot(pClient.getNoteClient());
      entree.setPiddv(pClient.getDateDerniereVisite());
      entree.setPidve(pClient.getDateDerniereVente());
      entree.setPisrn(pClient.getNumeroSIREN());
      entree.setPisrt(pClient.getComplementSIRET());
      if (pClient.getDepassementEncours() != null && !pClient.getDepassementEncours().isEmpty()) {
        entree.setPifil1(pClient.getDepassementEncours().charAt(0));
      }
      entree.setPicee(pClient.getCodePaysCEE());
      entree.setPivae(pClient.getVenteAssimileExport());
      entree.setPicde(pClient.getEncoursCommande());
      entree.setPiexp(pClient.getEncoursExpedie());
      entree.setPifac(pClient.getEncoursFacture());
      entree.setPipco(pClient.getPositionComptable());
      entree.setPicgm(pClient.getZoneSystemMajCompta());
      entree.setPimaj(pClient.getTopMaj());
      entree.setPinex1(pClient.getNbrExemplairesBonHom());
      entree.setPinex2(pClient.getNbrExemplairesBonExp());
      entree.setPinex3(pClient.getNbrExemplairesBonFac());
      entree.setPinla(pClient.getNumeroLibelleArticle());
      entree.setPirst(pClient.getZoneRegroupementStats());
      if (pClient.getTopLivraisonPartielle() != null && !pClient.getTopLivraisonPartielle().isEmpty()) {
        entree.setPiin1(pClient.getTopLivraisonPartielle().charAt(0));
      }
      if (pClient.getReliquatsAcceptes() != null && !pClient.getReliquatsAcceptes().isEmpty()) {
        entree.setPiin2(pClient.getReliquatsAcceptes().charAt(0));
      }
      if (pClient.getEditionChiffre() != null) {
        entree.setPiin3(pClient.getEditionChiffre().getCode());
      }
      else {
        entree.setPiin3(' ');
      }
      entree.setPiin4(pClient.getNonUtilisationDGCNV());
      if (pClient.getCodeGBAaRepercuter() != null && !pClient.getCodeGBAaRepercuter().isEmpty()) {
        entree.setPiin5(pClient.getCodeGBAaRepercuter().charAt(0));
      }
      if (pClient.getRfaCentrale() != null && !pClient.getRfaCentrale().isEmpty()) {
        entree.setPiin6(pClient.getRfaCentrale().charAt(0));
      }
      if (pClient.getNonEditionEtqColis() != null && !pClient.getNonEditionEtqColis().isEmpty()) {
        entree.setPiin7(pClient.getNonEditionEtqColis().charAt(0));
      }
      if (pClient.getEditionFacBonExpEnchaine() != null && !pClient.getEditionFacBonExpEnchaine().isEmpty()) {
        entree.setPiin8(pClient.getEditionFacBonExpEnchaine().charAt(0));
      }
      entree.setPiin9(pClient.getTypeImageClient().getCode());
      if (pClient.getExclusionCNVQuantitative() != null && !pClient.getExclusionCNVQuantitative().isEmpty()) {
        entree.setPiin10(pClient.getExclusionCNVQuantitative().charAt(0));
      }
      if (pClient.getPasEtqColisExpedition() != null && !pClient.getPasEtqColisExpedition().isEmpty()) {
        entree.setPiin11(pClient.getPasEtqColisExpedition().charAt(0));
      }
      if (pClient.getFactureDifsurCmdSoldee() != null && !pClient.getFactureDifsurCmdSoldee().isEmpty()) {
        entree.setPiin12(pClient.getFactureDifsurCmdSoldee().charAt(0));
      }
      if (pClient.getNonSoumisTaxeAddtit() != null && !pClient.getNonSoumisTaxeAddtit().isEmpty()) {
        entree.setPiin13(pClient.getNonSoumisTaxeAddtit().charAt(0));
      }
      if (pClient.getCodePEParticipationFraisExp() != null && !pClient.getCodePEParticipationFraisExp().isEmpty()) {
        entree.setPiin14(pClient.getCodePEParticipationFraisExp().charAt(0));
      }
      if (pClient.getGenerationCNVFacPrixNet() != null && !pClient.getGenerationCNVFacPrixNet().isEmpty()) {
        entree.setPiin15(pClient.getGenerationCNVFacPrixNet().charAt(0));
      }
      entree.setPiadh(pClient.getCodeAdherent());
      entree.setPicnc(pClient.getCodeCondCommissionnement());
      entree.setPicc1(pClient.getCentrale1());
      entree.setPicc2(pClient.getCentrale2());
      entree.setPicc3(pClient.getCentrale3());
      entree.setPifir(pClient.getCodeFirme());
      entree.setPitra(pClient.getClientTransitaire());
      entree.setPicnr(pClient.getCodeConditionRistourne());
      entree.setPicnp(pClient.getCodeConditionPromo());
      entree.setPicnb(pClient.getCodeRemisePiedBon());
      entree.setPidpl(pClient.getDateLimitePourSurPlafond());
      if (pClient.getApplicationFraisFixes() != null && !pClient.getApplicationFraisFixes().isEmpty()) {
        entree.setPiin16(pClient.getApplicationFraisFixes().charAt(0));
      }
      if (pClient.getApplicationRemisePiedFac() != null && !pClient.getApplicationRemisePiedFac().isEmpty()) {
        entree.setPiin17(pClient.getApplicationRemisePiedFac().charAt(0));
      }
      entree.setPicrt(pClient.getCodeRegroupementTransport());
      entree.setPioto(pClient.getOrdreDansLaTournee());
      entree.setPican(pClient.getCanalDeVente());
      entree.setPiprn(pClient.getPrenoms());
      if (pClient.getRecenceCmd() != null && !pClient.getRecenceCmd().isEmpty()) {
        entree.setPirec(pClient.getRecenceCmd().charAt(0));
      }
      if (pClient.getFrequenceCmd() != null && !pClient.getFrequenceCmd().isEmpty()) {
        entree.setPifrc(pClient.getFrequenceCmd().charAt(0));
      }
      if (pClient.getMontantCmd() != null && !pClient.getMontantCmd().isEmpty()) {
        entree.setPimtc(pClient.getMontantCmd().charAt(0));
      }
      entree.setPinip(pClient.getCodePaysTVAIntracom());
      entree.setPinik(pClient.getCleTvaIntracom());
      if (pClient.getClasseClient1() != null && !pClient.getClasseClient1().isEmpty()) {
        entree.setPicl1(pClient.getClasseClient1().charAt(0));
      }
      entree.setPicl2(pClient.getClasseClient2());
      entree.setPicl3(pClient.getClasseClient3());
      entree.setPicla(pClient.getCleClassement2());
      if (pClient.getClientSoumisDroitPret() != null && !pClient.getClientSoumisDroitPret().isEmpty()) {
        entree.setPiin18(pClient.getClientSoumisDroitPret().charAt(0));
      }
      if (pClient.getEditionFactures() != null && !pClient.getEditionFactures().isEmpty()) {
        entree.setPiin19(pClient.getEditionFactures().charAt(0));
      }
      if (pClient.getPrecommandesAcceptees() != null && !pClient.getPrecommandesAcceptees().isEmpty()) {
        entree.setPiin20(pClient.getPrecommandesAcceptees().charAt(0));
      }
      if (pClient.getNombreExemplairesAvoirs() != null && !pClient.getNombreExemplairesAvoirs().isEmpty()) {
        entree.setPiin21(pClient.getNombreExemplairesAvoirs().charAt(0));
      }
      if (pClient.getTypeVente() != null && !pClient.getTypeVente().isEmpty()) {
        entree.setPiin22(pClient.getTypeVente().charAt(0));
      }
      if (pClient.getTrancheEffectif() != null && !pClient.getTrancheEffectif().isEmpty()) {
        entree.setPiin23(pClient.getTrancheEffectif().charAt(0));
      }
      if (pClient.getCodePEParticipationFraisExp() != null && !pClient.getCodePEParticipationFraisExp().isEmpty()) {
        entree.setPiin24(pClient.getCodePEParticipationFraisExp().charAt(0));
      }
      if (pClient.getCodePEParticipationFraisExp2() != null && !pClient.getCodePEParticipationFraisExp2().isEmpty()) {
        entree.setPiin25(pClient.getCodePEParticipationFraisExp2().charAt(0));
      }
      if (pClient.getCodePEParticipationFraisExp3() != null && !pClient.getCodePEParticipationFraisExp3().isEmpty()) {
        entree.setPiin26(pClient.getCodePEParticipationFraisExp3().charAt(0));
      }
      if (pClient.getNonEditionPrixBase() != null && !pClient.getNonEditionPrixBase().isEmpty()) {
        entree.setPiin27(pClient.getNonEditionPrixBase().charAt(0));
      }
      entree.setPiapen(pClient.getCodeAPEv2());
      entree.setPiplf3(pClient.getPlafondDemande());
      entree.setPidpl3(pClient.getDateDemandePlafond());
      entree.setPiidas(pClient.getIdentifiantAssurance());
      entree.setPicas(pClient.getCodeAssurance());
      entree.setPidvp3(pClient.getDateValiditePlafond());
      entree.setPidat1(pClient.getCaPrevisionnel());
      entree.setPidat2(pClient.getIdBizyNova());
      entree.setPidat3(pClient.getPlafondMaxEnDeblocage());
      entree.setPiffp1(pClient.getFraisFacturePied1());
      entree.setPiffp2(pClient.getFraisFacturePied2());
      entree.setPiffp3(pClient.getFraisFacturePied3());
      if (pClient.getNonEditionNumerosSeries() != null && !pClient.getNonEditionNumerosSeries().isEmpty()) {
        entree.setPiin28(pClient.getNonEditionNumerosSeries().charAt(0));
      }
      if (pClient.getTypeClient() != null && !pClient.getTypeClient().isEmpty()) {
        entree.setPiin29(pClient.getTypeClient().charAt(0));
      }
      Character PIIN30 = ' ';
      if (pClient.isRefLongueCommandeEstObligatoire()) {
        PIIN30 = '1';
      }
      entree.setPiin30(PIIN30);
      
      // Le contact principal
      if (pClient.getContactPrincipal() != null) {
        if (pClient.getContactPrincipal().getIdCivilite() != null) {
          entree.setPiciv(pClient.getContactPrincipal().getIdCivilite().getCode());
        }
        if (pClient.getContactPrincipal().getNom() != null) {
          entree.setPinomc(pClient.getContactPrincipal().getNom());
        }
        if (pClient.getContactPrincipal().getPrenom() != null) {
          entree.setPipre(pClient.getContactPrincipal().getPrenom());
        }
        if (pClient.getContactPrincipal().getEmail() != null) {
          entree.setPinet(pClient.getContactPrincipal().getEmail());
        }
        if (pClient.getContactPrincipal().getEmail2() != null) {
          entree.setPinet2(pClient.getContactPrincipal().getEmail2());
        }
        if (pClient.getContactPrincipal().getNumeroTelephone1() != null) {
          entree.setPitelc(pClient.getContactPrincipal().getNumeroTelephone1());
        }
        if (pClient.getContactPrincipal().getNumeroFax() != null) {
          entree.setPifaxc(pClient.getContactPrincipal().getNumeroFax());
        }
      }
      
      // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
      entree.setDSInput();
      sortie.setDSInput();
      erreur.setDSInput();
      
      // Préparation de l'environnement
      ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
      rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(),
          EnvironnementExecution.getGvmx());
      
      // Exécution du programme RPG
      rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList);
      
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de la modification du client.");
    }
    return true;
  }
}
