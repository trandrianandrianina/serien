/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.article;

import ri.serien.libas400.dao.gvx.database.files.FFD_Pgvmeacm;
import ri.serien.libas400.database.QueryManager;

public class M_EcArticle extends FFD_Pgvmeacm {
  // Variables de travail
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_EcArticle(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Insère l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PGVMEACM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete =
        genericrecord.createSQLRequestUpdate("PGVMEACM", querymg.getLibrary(), "EBETB=" + getEBETB() + " and EBART='" + getEBART() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   * 
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + ".PGVMEACM where EBETB=" + getEBETB() + " and EBART='" + getEBART() + "'";
    return request(requete);
  }
  
  public String getEBZPX(int index) {
    switch (index) {
      case 1:
        return getEBZP1();
      case 2:
        return getEBZP2();
      case 3:
        return getEBZP3();
      case 4:
        return getEBZP4();
      case 5:
        return getEBZP5();
      case 6:
        return getEBZP6();
      case 7:
        return getEBZP7();
      case 8:
        return getEBZP8();
      case 9:
        return getEBZP9();
      case 10:
        return getEBZP10();
      case 11:
        return getEBZP11();
      case 12:
        return getEBZP12();
      case 13:
        return getEBZP13();
      case 14:
        return getEBZP14();
      case 15:
        return getEBZP15();
      case 16:
        return getEBZP16();
      case 17:
        return getEBZP17();
      case 18:
        return getEBZP18();
    }
    return "";
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  // -- Accesseurs ----------------------------------------------------------
  
}
