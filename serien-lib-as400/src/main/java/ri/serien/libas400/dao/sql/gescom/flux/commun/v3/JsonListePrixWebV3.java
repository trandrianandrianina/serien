/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.outils.Constantes;

/**
 * Article Série N au format SFCC
 * Cette classe est une classe qui sert à l'intégration JAVA vers JSON
 * IL NE FAUT PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonListePrixWebV3 extends JsonEntiteMagento {
  // Variables
  private ArrayList<JsonPrixWebV3> listePrix = new ArrayList<JsonPrixWebV3>();
  
  /**
   * Constructeur.
   */
  public JsonListePrixWebV3(FluxMagento pFluxMagento) {
    idFlux = pFluxMagento.getFLIDF();
    versionFlux = pFluxMagento.getFLVER();
    // code = pFluxMagento.getFLCOD();
    etb = pFluxMagento.getFLETB();
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de construire l'article sur le modèle SFCC dans l'optique d'un export typé.
   */
  public List<IdArticle> mettreAJourArticle(List<GenericRecord> pListeRecord) {
    if (pListeRecord == null) {
      majError("[JsonPrixWebV3] mettreAJourArticle() - La liste de GenericRecord est incorrecte.");
      return null;
    }
    List<IdArticle> listeIdArticle = new ArrayList<IdArticle>();
    
    listePrix.clear();
    for (GenericRecord record : pListeRecord) {
      if (record == null) {
        continue;
      }
      
      JsonPrixWebV3 jsonPrixWeb = new JsonPrixWebV3();
      IdArticle idArticle = jsonPrixWeb.mettreAJourArticle(record);
      if (idArticle != null) {
        listePrix.add(jsonPrixWeb);
        listeIdArticle.add(idArticle);
      }
    }
    
    return listeIdArticle;
  }
  
  /**
   * Permet de mettre à jour les tarifs de l'article pour interprétation dans SFCC.
   */
  public boolean mettreAJourTarif(List<GenericRecord> pListeRecord) {
    if (pListeRecord == null) {
      majError("[JsonPrixWebV3] mettreAJourTarif() - Cet article ne possède pas encore de tarif valide.");
      return false;
    }
    
    for (GenericRecord record : pListeRecord) {
      if (record == null) {
        continue;
      }
      
      // Si pas d'identifiant article, l'enregistrement est ignoré
      if (!record.isPresentField("A1ART")) {
        continue;
      }
      
      // Récupération du code article de l'enregistrement
      String codeArticle = record.getStringValue("A1ART", "", true);
      if (codeArticle.isEmpty()) {
        continue;
      }
      // Récupération de l'élément à mettre à jour
      JsonPrixWebV3 jsonPrixWeb = trouverPrixAvecCodeArticle(codeArticle);
      if (jsonPrixWeb == null) {
        majError("[JsonPrixWebV3] mettreAJourTarif() - L'article " + codeArticle + " n'a pas été trouvé dans la liste jsonPrixWebV3.");
        continue;
      }
      jsonPrixWeb.mettreAJourTarif(record);
    }
    
    return true;
  }
  
  /**
   * Permet de remonter le montant DEEE de cet article pour la notion SFCC.
   */
  public void calculerMontantDEEE(IdArticle pIdArticle, BigDecimal pMontantDeee) {
    if (pIdArticle == null) {
      majError("[JsonPrixWebV3] calculerMontantDEEE() - L'identifiant article est invalide.");
      return;
    }
    
    // Récupération de l'élément à mettre à jour
    JsonPrixWebV3 jsonPrixWeb = trouverPrixAvecCodeArticle(pIdArticle.getCodeArticle());
    if (jsonPrixWeb == null) {
      majError("[JsonPrixWebV3] mettreAJourTarif() - L'article " + pIdArticle.getCodeArticle()
          + " n'a pas été trouvé dans la liste jsonPrixWebV3.");
      return;
    }
    jsonPrixWeb.calculerMontantDEEE(pMontantDeee);
  }
  
  // -- Méthodes privées
  
  /**
   * Transforme un code d'unité de mesure en son libellé.
   */
  private String transformerUniteDeMesure(String pValeur) {
    if (pValeur == null) {
      return "";
    }
    
    String retour = "";
    if (pValeur.trim().equals("") || pValeur.trim().equals("1")) {
      retour = "m";
    }
    else if (pValeur.trim().equals("2")) {
      retour = "cm";
    }
    else if (pValeur.trim().equals("3")) {
      retour = "mm";
    }
    
    return retour;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne le jsonPrixWeb à l'aide du code article.
   */
  private JsonPrixWebV3 trouverPrixAvecCodeArticle(String pCodeArticle) {
    pCodeArticle = Constantes.normerTexte(pCodeArticle);
    if (pCodeArticle.isEmpty()) {
      return null;
    }
    for (JsonPrixWebV3 jsonPrixWeb : listePrix) {
      if (pCodeArticle.equals(jsonPrixWeb.getCode())) {
        return jsonPrixWeb;
      }
    }
    return null;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des prix.
   */
  public ArrayList<JsonPrixWebV3> getListePrix() {
    return listePrix;
  }
  
  /**
   * Initialise la liste des prix.
   */
  public void setListePrix(ArrayList<JsonPrixWebV3> listePrix) {
    this.listePrix = listePrix;
  }
}
