/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_VT pour les VT
 */
public class Pgvmparm_ds_VT extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "VTLIB"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "VTMS1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTDU1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTAU1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTM11"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTE11"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTP11"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTM12"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTE12"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTP12"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTM13"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTE13"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTP13"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "VTMS2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTDU2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTAU2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTM21"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTE21"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTP21"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTM22"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTE22"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTP22"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTM23"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 0), "VTE23"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "VTP23"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "VTRD1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "VTRD2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "VTRMO1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "VTRD3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "VTRD4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "VTRMO3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(4, 2), "VTRPC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "VTRRPC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "VTRIN1"));
    
    length = 300;
  }
}
