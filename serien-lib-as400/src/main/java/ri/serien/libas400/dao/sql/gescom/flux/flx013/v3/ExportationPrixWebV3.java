/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx013.v3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.article.SqlArticle;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonListePrixWebV3;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class ExportationPrixWebV3 extends BaseGroupDB {
  // Constantes
  private static int NOMBRE_PRIX_MAXIMUM = 500;
  private static String DEVISE_EURO = "   ";
  
  // Variables
  private ManagerFluxMagento managerFluxMagento = null;
  private String codeDeviseEtablissement = null;
  
  /**
   * Constructeur avec le queryManager en paramètre pour les requetes sur DB2.
   */
  public ExportationPrixWebV3(ManagerFluxMagento pManagerFluxMagento, QueryManager pQuerymanager) {
    super(pQuerymanager);
    managerFluxMagento = pManagerFluxMagento;
    
    // Création artificielle de la devise afin de récupérer le tarif de la bonne devise.
    // Normalement la devise doit provenir de la DG or rien n'a été prévu donc la devise est initialisée avec ' ' qui symbolise l'euro en
    // attendant de corriger plus proprement l'initialisation de la devise
    codeDeviseEtablissement = DEVISE_EURO;
  }
  
  /**
   * Retourner un prix Série N.
   */
  public JsonListePrixWebV3 retournerListePrixWeb(FluxMagento pFluxMagento) {
    if (pFluxMagento == null || pFluxMagento.getFLETB() == null || pFluxMagento.getFLCOD() == null) {
      majError("[ExportationPrixWebV3] retournerPrixWebJSON() - Valeurs du FluxMagento record corrompues.");
      return null;
    }
    
    // Création d'un flux prix
    JsonListePrixWebV3 jsonListePrixWebV3 = new JsonListePrixWebV3(pFluxMagento);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), jsonListePrixWebV3.getEtb());
    if (numeroInstance == null) {
      majError("[ExportationPrixWebV3] retournerPrixWebJSON() - Problème d'interprétation de l'Etb vers l'instance Web.");
      return null;
    }
    jsonListePrixWebV3.setIdInstanceMagento(numeroInstance);
    
    // Récupération du reliquat sinon chargement des articles depuis la base de données
    List<GenericRecord> listeRecord = null;
    if (pFluxMagento.getListeReliquat() != null) {
      listeRecord = (List<GenericRecord>) (Object) pFluxMagento.getListeReliquat();
    }
    else {
      listeRecord = chargerListeArticle(pFluxMagento.getFLETB());
    }
    if (listeRecord == null || listeRecord.isEmpty()) {
      majError("[ExportationPrixWebV3] retournerPrixWebJSON() - Aucun article chargé.");
      return null;
    }
    // Stockage du nombre d'enregistrements
    pFluxMagento.setFLCOD("" + listeRecord.size());
    
    // Initialisation des prix à l'aide de la liste des articles
    if (listeRecord.size() > NOMBRE_PRIX_MAXIMUM) {
      listeRecord = extraireLigneATraiter(pFluxMagento, listeRecord);
    }
    // Informations articles
    List<IdArticle> listeIdArticle = jsonListePrixWebV3.mettreAJourArticle(listeRecord);
    if (listeIdArticle == null || listeIdArticle.isEmpty()) {
      majError(jsonListePrixWebV3.getMsgError());
      return null;
    }
    // Information tarifs
    listeRecord = chargerTarifArticle(listeIdArticle);
    if (listeRecord == null || listeRecord.isEmpty()) {
      majError("[ExportationPrixWebV3] retournerPrixWebJSON() - Aucun tarif chargé pour la liste des articles.");
      return null;
    }
    if (!jsonListePrixWebV3.mettreAJourTarif(listeRecord)) {
      majError(jsonListePrixWebV3.getMsgError());
      return null;
    }
    // DEEE
    mettreAJourDEEEListeArticle(listeIdArticle, jsonListePrixWebV3);
    
    // Conversion de l'objet en Json pour le stocker dans l'enregistrement et contrôle les données de base
    if (!initJSON()) {
      majError("[ExportationPrixWebV3] retournerPrixWebJSON() - Problème d'initialisation pour la sérialisation Json.");
      return null;
    }
    try {
      // Conversion du record en JSON
      pFluxMagento.setFLJSN(gson.toJson(jsonListePrixWebV3));
    }
    catch (Exception e) {
      majError(
          "[ExportationPrixWebV3] retournerPrixWebJSON() - Erreur lors de la conversion de l'enregistrement en Json: " + e.getMessage());
    }
    
    return jsonListePrixWebV3;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourner une liste d'article Série N sous forme GenericRecord.
   */
  private ArrayList<GenericRecord> chargerListeArticle(String pCodeEtablissement) {
    if (pCodeEtablissement == null) {
      return null;
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    // @formatter:off
    requeteSql.ajouter("select A.A1ART, A.A1ETB, DIGITS(A1GCD) AS GCD, A1UNV, A1NPU, A1LIB, A1LB1, A1LB2, A1LB3, A1PDS, A1LRG, A1LNG, "
        + "A1HTR, A1IN15, A1IN28, A1IN29, A1IN30, A1STK, A1REA, A1TOP1, A1ZP21, A1TOP2, A1TOP3, A1FRS, A1REF1, FRNOM from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE + " A "
        + "left join " + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE_EXTENSION + " E on E.A1ETB = A.A1ETB and E.A1ART = A.A1ART "
        + "left join " + queryManager.getLibrary() + '.' + EnumTableBDD.FOURNISSEUR + " F on F.FRETB = A.A1ETB and F.FRFRS = A.A1FRS "
        + " where");
    // @formatter:on
    requeteSql.ajouterConditionAnd("A.A1ETB", "=", pCodeEtablissement);
    // Exclusion des articles désactivés
    requeteSql.ajouterConditionAnd("A.A1NPU", "<>", "1");
    requeteSql.ajouterConditionAnd("A.A1IN15", "<>", Article.JAMAIS_VU_SUR_LE_WEB);
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      majError("[ExportationPrixWebV3] retournerUnArticleDB2() - Aucun article trouvés sur l'établissement " + pCodeEtablissement);
      return null;
    }
    
    return listeRecord;
  }
  
  /**
   * Retourner Les 10 tarifs d'un article Série N pour un même établissement.
   * ATTENTION SANS COEFFICIENT VU AVEC MARC le 27/04/16.
   */
  private ArrayList<GenericRecord> chargerTarifArticle(List<IdArticle> pListeIdArticle) {
    if (pListeIdArticle == null || pListeIdArticle.isEmpty()) {
      return null;
    }
    
    // Récupèration du dernier tarif < à la date du jour
    String dateJour = DateHeure.getJourHeure(13);
    RequeteSql requeteSql = new RequeteSql();
    // Construction de la liste des codes articles
    for (IdArticle idArticle : pListeIdArticle) {
      requeteSql.ajouterValeurAuGroupe("codearticle", idArticle.getCodeArticle());
    }
    
    // Construction de la requête
    RequeteSql sousRequeteSql = new RequeteSql();
    // @formatter:off
    // Création sous requête
    sousRequeteSql.ajouter("select MAX(B.ATDAP) from " + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE_TARIF
        + " B where");
    sousRequeteSql.ajouterConditionAnd("A1ETB", "=", "B.ATETB", true, false);
    sousRequeteSql.ajouterConditionAnd("A1ART", "=", "B.ATART", true, false);
    sousRequeteSql.ajouterConditionAnd("B.ATDEV", "=", codeDeviseEtablissement);
    sousRequeteSql.ajouter(" group by B.ATETB, B.ATART");
    
    // Création requête principale
    requeteSql.ajouter("select A1ART, A1ETB, ATDAP, ATP01, ATP02 , ATP03, ATP04, ATP05, ATP06, ATP07, ATP08, ATP09, ATP10 from "
        + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE
        + " left join " + queryManager.getLibrary() + '.' + EnumTableBDD.ARTICLE_TARIF + " on A1ETB = ATETB and A1ART = ATART"
        + " where");
    requeteSql.ajouterConditionAnd("ATETB", "=", pListeIdArticle.get(0).getCodeEtablissement());
    requeteSql.ajouterConditionAnd("ATDEV", "=", codeDeviseEtablissement);
    requeteSql.ajouter(" and ATART");
    requeteSql.ajouterGroupeDansRequete("codearticle", "in");
    requeteSql.ajouterConditionAnd("ATDAP", "<=", dateJour, true, false);
    requeteSql.ajouterConditionAnd("ATDAP", "in", "(" + sousRequeteSql.getRequete() + ")", true, false);
    // @formatter:on
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      majError(
          "[ExportationPrixWebV3] retournerLesTarifsUnArticle() - Aucun tarif n'a été récupéré pour la liste d'identifiant articles.");
      return null;
    }
    
    return listeRecord;
  }
  
  /**
   * Retourner la DEEE d'un article.
   */
  private void mettreAJourDEEEListeArticle(List<IdArticle> pListeIdArticle, JsonListePrixWebV3 pJsonListePrixWebV3) {
    if (pListeIdArticle == null || pListeIdArticle.isEmpty()) {
      return;
    }
    
    // Contrôler existance table PGVMTEEM qui n'est pas systématiquement présente
    boolean tablePresente = queryManager.isTableExiste(EnumTableBDD.ARTICLE_TAXE);
    if (!tablePresente) {
      Trace.alerte("La table " + queryManager.getLibrary() + "/" + EnumTableBDD.ARTICLE_TAXE + " n'est pas présente.");
    }
    
    // Charger le montant DEEE pour un article (pas optimisé faudrait réussir à améliorer la requête pour le faire sur une liste mais pas
    // simple)
    SqlArticle sqlArticle = new SqlArticle(queryManager);
    for (IdArticle idArticle : pListeIdArticle) {
      BigDecimal montantDeee = null;
      if (tablePresente) {
        montantDeee = sqlArticle.chargerMontantDEEEArticle(idArticle, 4);
      }
      pJsonListePrixWebV3.calculerMontantDEEE(idArticle, montantDeee);
    }
  }
  
  /**
   * Permet d'isoler des flux de prix trop importants en plusieurs flux plus petits.
   * Le critère étant la quantité de lignes d'articles concernés.
   */
  private List<GenericRecord> extraireLigneATraiter(FluxMagento pFluxMagentoInitial, List<GenericRecord> pListeRecordInitiale) {
    if (managerFluxMagento == null || pListeRecordInitiale == null || pListeRecordInitiale.isEmpty()) {
      return pListeRecordInitiale;
    }
    
    // Decoupage du flux initial en 2 listes : une à traiter et un reliquat
    List<GenericRecord> listeATraiter = new ArrayList<GenericRecord>();
    List<Object> listeResteATraiter = new ArrayList<Object>();
    for (int i = 0; i < pListeRecordInitiale.size(); i++) {
      if (i < NOMBRE_PRIX_MAXIMUM) {
        listeATraiter.add(pListeRecordInitiale.get(i));
      }
      else {
        listeResteATraiter.add(pListeRecordInitiale.get(i));
      }
    }
    // Sauvegarde des lignes à traiter
    if (listeResteATraiter.size() > 0) {
      managerFluxMagento.traiterReliquatFluxSortant(pFluxMagentoInitial, listeResteATraiter);
    }
    
    return listeATraiter;
  }
  
}
