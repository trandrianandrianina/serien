/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.flux.CritereFlux;
import ri.serien.libcommun.exploitation.flux.CritereLienFluxTiersWebSerieN;
import ri.serien.libcommun.exploitation.flux.EnumCodeErreur;
import ri.serien.libcommun.exploitation.flux.EnumSensFlux;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.exploitation.flux.Flux;
import ri.serien.libcommun.exploitation.flux.IdFlux;
import ri.serien.libcommun.exploitation.flux.IdLienFluxTiersWebSerieN;
import ri.serien.libcommun.exploitation.flux.LienFluxTiersWebSerieN;
import ri.serien.libcommun.exploitation.flux.ListeFlux;
import ri.serien.libcommun.exploitation.flux.ListeLienFluxTiersWebSerieN;
import ri.serien.libcommun.exploitation.flux.historiqueenvoistock.HistoriqueEnvoiStock;
import ri.serien.libcommun.exploitation.flux.historiqueenvoistock.IdHistoriqueEnvoiStock;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe contient l'ensemble des requêtes nécessaires à la gestion des flux.
 */
public class SqlFlux {
  // Constantes
  private static final int NB_LIGNES_FLUX_VISIBLES = 200;
  
  // Variables
  protected QueryManager queryManager = null;
  
  /**
   * Constructeur.
   */
  public SqlFlux(QueryManager pQueryManager) {
    queryManager = pQueryManager;
  }
  
  // == Méthodes communes à toutes les versions des flux
  
  // -- Méthodes publiques
  
  /**
   * Genère un enregistrement dans la table des flux afin de générer un envoi de flux vers Magento.
   */
  public void ecrireDemandeEnvoiFluxVersMagento(EnumTypeFlux pEnumTypeFlux, IdEtablissement pIdEtablissement, String pCodeMetier) {
    if (pEnumTypeFlux == null) {
      throw new MessageErreurException("Le type du flux est invalide.");
    }
    if (pIdEtablissement == null) {
      throw new MessageErreurException("L'établissement est invalide.");
    }
    pCodeMetier = Constantes.normerTexte(pCodeMetier);
    if (pCodeMetier.isEmpty()) {
      throw new MessageErreurException("Le code métier est invalide.");
    }
    
    RequeteSql requete = new RequeteSql();
    // @formatter:off
    requete.ajouter("INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX
        + " (FLIDF, FLETB, FLCRE, FLTRT,FLCOD, FLSNS, FLSTT, FLJSN, FLERR, FLVER, FLCPT) VALUES ("
        + RequeteSql.formaterStringSQL(pEnumTypeFlux.getCode()) + ", "
        + RequeteSql.formaterStringSQL(pIdEtablissement.getCodeEtablissement()) + ", "
        + "CURRENT TIMESTAMP, "
        + "CURRENT TIMESTAMP, "
        + RequeteSql.formaterStringSQL(pCodeMetier) + ", "
        + "0, "
        + "1, "
        + RequeteSql.formaterStringSQL("") + ", "
        + "0, "
        + RequeteSql.formaterStringSQL("1.0") + ", "
        + " '0') ");
    // @formatter:on
    
    // Exécution de la requête
    int retour = queryManager.requete(requete.getRequete());
    if (retour <= Constantes.ERREUR) {
      throw new MessageErreurException("Il y a eu une erreur lors de la création de l'enregistrement dans la table des flux.");
    }
  }
  
  /**
   * Charge les informations concernant les instances afin de faire le lien entre le tiers Web (Magento, SFCC, ...) et Série N.
   */
  public ListeLienFluxTiersWebSerieN chargerLienTiersWebSerieN(CritereLienFluxTiersWebSerieN pCritere) {
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select INID, INBIB, INETB, GSID, GSBIBLI, GSACTIF, GSDELAI, GSLETTR, GSVERSI from QGPL."
        + EnumTableBDD.FLUX_LIEN_INSTANCE + " LEFT JOIN QGPL." + EnumTableBDD.FLUX_VERSION_INSTANCE + " ON GSBIBLI = INBIB where ");
    
    // Ajout des conditions si les critères sont présents
    if (pCritere != null) {
      if (pCritere.getNumeroInstance() != null) {
        requeteSql.ajouterConditionAnd("INID", "=", pCritere.getNumeroInstance());
      }
      if (pCritere.getIdEtablissement() != null) {
        requeteSql.ajouterConditionAnd("INETB", "=", pCritere.getIdEtablissement().getCodeEtablissement());
      }
      if (pCritere.getBaseDeDonnees() != null) {
        requeteSql.ajouterConditionAnd("INBIB", "=", pCritere.getBaseDeDonnees().getNom());
      }
    }
    // Supprime le "where" si aucune condition n'a été ajoutée
    requeteSql.supprimerDernierOperateur("where");
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return null;
    }
    
    // Traitement des données lues en table
    ListeLienFluxTiersWebSerieN listeLien = new ListeLienFluxTiersWebSerieN();
    for (GenericRecord record : listeRecord) {
      LienFluxTiersWebSerieN lien = completerLienFluxTiersWebSerieN(record);
      if (lien != null) {
        listeLien.add(lien);
      }
    }
    return listeLien;
  }
  
  /**
   * Retourne une liste d'identifiants de flux sur la base des filtres transmis en paramètres.
   * 
   * @param pCritere Critères de recherche des flux.
   * @return Liste d'identifiants de flux.
   */
  public List<IdFlux> chargerListeIdFlux(CritereFlux pCritere) {
    if (pCritere == null) {
      throw new MessageErreurException("Les critères de recherche sont invalides pour la méthode chargerListeIdFlux");
    }
    RequeteSql requete = new RequeteSql();
    // @formatter:off
    requete.ajouter("SELECT"
        // Identifiant unique
        + " FLID,"
        // Etablissement
        + " FLETB"
        + " FROM " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX);
    // @formatter:on
    // Ajouter les critères de recherche s'ils existent.
    if (!pCritere.getRequeteWhere().isEmpty()) {
      requete.ajouter(" WHERE " + pCritere.getRequeteWhere());
    }
    // Trier les résultats selon la date de création la plus récente.
    requete.ajouter(" ORDER BY FLCRE DESC");
    
    // Execution de la requête
    ArrayList<GenericRecord> listeGenericRecord = queryManager.select(requete.getRequete());
    
    // Alimentation de la liste avec les statuts trouvés.
    
    List<IdFlux> listeIdFlux = new ArrayList<IdFlux>();
    for (GenericRecord record : listeGenericRecord) {
      try {
        // Pour chaque enregistrement, on crée l'identifiant de flux et on l'ajoute à la liste.
        listeIdFlux.add(completerIdFlux(record));
      }
      catch (Exception e) {
        Trace.erreur(e, "Impossible de charger un identifiant de Flux");
      }
    }
    return listeIdFlux;
  }
  
  /**
   * Retourne une liste de flux chargés à partir d'une liste d'identifiants de flux.
   * 
   * @param pListeIdFlux Liste d'identifiants de flux à charger.
   * @return Liste de flux chargée.
   */
  public ListeFlux chargerListeFlux(List<IdFlux> pListeIdFlux) {
    if (pListeIdFlux == null) {
      throw new MessageErreurException("Aucun identifiant n'a été passé à la méthode chargerListeFlux(List<IdFlux>)");
    }
    
    RequeteSql requete = new RequeteSql();
    // Construire la requête
    // @formatter:off
    requete.ajouter("SELECT"
        // Id
        + " FLID,"
        // Etablissement
        + " FLETB,"
        // Type
        + " FLIDF,"
        // Version
        + " FLVER,"
        // Date création
        + " FLCRE,"
        // Code transmis
        + " FLCOD,"
        // Sens
        + " FLSNS,"
        // Statut
        + " FLSTT,"
        // Date traitement
        + " FLTRT,"
        // Compteur tentatives
        + " FLCPT,"
        // Contenu JSON
        + " FLJSN,"
        // Code erreur
        + " FLERR,"
        // Message d'erreur
        + " FLMSGERR"
        + " FROM " + queryManager.getLibrary() + '.' + EnumTableBDD.FLUX);
    // @formatter:on
    
    // Ajouter les identifiants des Flux à charger à la requête
    // Construction du groupe
    for (IdFlux idFlux : pListeIdFlux) {
      requete.ajouterValeurAuGroupe("idFlux", idFlux.getCode());
    }
    requete.ajouter("WHERE FLID");
    requete.ajouterGroupeDansRequete("idFlux", "in");
    
    // Exécuter la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requete.getRequete());
    
    // Lecture des champs
    ListeFlux listeFlux = new ListeFlux();
    for (GenericRecord record : listeRecord) {
      try {
        listeFlux.add(completerFlux(record));
      }
      catch (Exception e) {
        Trace.erreur(e, "Impossible de charger un flux");
      }
    }
    
    return listeFlux;
  }
  
  /**
   * Retourne la version par défaut des flux.
   * 
   * @return Version par défaut des flux.
   */
  public String getVersionDefautFlux() {
    try {
      RequeteSql requeteSql = new RequeteSql();
      // Construire la requête
      // @formatter:off
      requeteSql.ajouter("SELECT "
          // Version des flux par défaut.
          + "GSVERSI "
          + "from QGPL." + EnumTableBDD.FLUX_VERSION_INSTANCE
          + " WHERE ");
      // @formatter:on
      requeteSql.ajouterConditionAnd("GSBIBLI", "=", queryManager.getLibrary());
      ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
      
      // Si pas de record dans la liste,
      if (listeRecord == null || listeRecord.isEmpty()) {
        throw new MessageErreurException("Erreur lors de la lecture de la version par défaut.");
      }
      
      if (!listeRecord.get(0).isPresentField("GSVERSI")) {
        Trace.erreur("La version par défaut n'est pas renseignée.");
      }
      // Si le champ n'est pas renseigné.
      return listeRecord.get(0).getField("GSVERSI").toString().trim();
    }
    // Si une erreur a été rencontrée,
    catch (Exception e) {
      Trace.erreur(e + " Impossible de récupérer la version par défaut.");
    }
    return null;
  }
  
  /**
   * Sauver un flux en base de données.
   * 
   * @param Flux à sauver en base.
   */
  public void sauverFlux(Flux pFlux) {
    if (pFlux == null) {
      throw new MessageErreurException("Le Flux que vous souhaitez créer est invalide.");
    }
    
    // Contrôle des éléments avant création du flux en base.
    if (pFlux.getId().isCree()) {
      if (pFlux.getId() == null) {
        throw new MessageErreurException("L'identifiant du nouveau flux est invalide.");
      }
      if (pFlux.getId().getIdEtablissement() == null || pFlux.getSens() == null || pFlux.getVersion() == null) {
        throw new MessageErreurException("Les valeurs de créations du nouveau flux sont invalides");
      }
    }
    
    RequeteSql requeteSql = new RequeteSql();
    if (pFlux.getId().isCree()) {
      
      // @formatter:off
      requeteSql.ajouter("INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX
          // Type
          + " (FLIDF,"
          // Etablissement
          + "FLETB,"
          // Date de création
          + "FLCRE,"
          // Date de traitement
          + "FLTRT,"
          // Code transmis
          + "FLCOD,"
          // Sens
          + "FLSNS,"
          // Statut
          + "FLSTT,"
          // JSON
          + "FLJSN,"
          // Code erreur
          + "FLERR,"
          // Version
          + "FLVER,"
          // Compteur tentatives
          + "FLCPT"
          + ") VALUES ("
          // Type
          + RequeteSql.formaterStringSQL(pFlux.getType().getCode()) + ", "
          // Etablissement
          + RequeteSql.formaterStringSQL(pFlux.getId().getIdEtablissement().getCodeEtablissement()) + ", "
          // Date création
          + "'" + pFlux.getDateCreation() + "', "
          // Date traitement
          + "'" + pFlux.getDateCreation() + "', "
          // Code transmis
          + RequeteSql.formaterStringSQL(pFlux.getCodeTransmis()) + ", "
          // Sens
          + pFlux.getSens().getNumero() + ", "
          // Statut
          + pFlux.getStatut().getNumero() + ", "
          // JSON
          + RequeteSql.formaterStringSQL(pFlux.getContenu()) + ", "
          // Code erreur
          + pFlux.getCodeErreur().getNumero() + ", "
          // Version
          + RequeteSql.formaterStringSQL(pFlux.getVersion()) + ", "
          // Compteur tentative
          + " '0') ");
      // @formatter:on
      queryManager.requete(requeteSql.getRequete());
    }
  }
  
  /**
   * Rejouer une liste de flux.
   */
  public void rejouerFlux(ListeFlux pListeFlux) {
    // Contrôle des null
    if (pListeFlux == null) {
      return;
    }
    // Créer la requete.
    RequeteSql requeteSql = new RequeteSql();
    
    // Construire la requête.
    requeteSql.ajouter("UPDATE " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX + " SET ");
    
    // Définir le statut du flux en "A traiter".
    // Lorsqu'un flux passe en statut "A traiter", il est rejoué.
    requeteSql.ajouterValeurUpdate("FLSTT", EnumStatutFlux.A_TRAITER.getNumero());
    
    // Ajouter les identifiants des Flux à rejouer à la requête
    // Construction du groupe
    for (Flux flux : pListeFlux) {
      // On contrôle la validité du contenu du flux.
      // Un flux dont le contenu est un JSON doit commencer par { et finir par }.
      // Un flux dont le contenu est un CSV doit commencer par " et finir par ".
      // Si ce n'est pas le cas, le flux ne pourra pas être rejoué.
      // On ajoute au groupe des flux à rejouer les flux qui correspondent à ce critère.
      if ((flux.getContenu().startsWith("{") && flux.getContenu().endsWith("}"))
          || (flux.getContenu().startsWith("\"") && flux.getContenu().endsWith("\""))) {
        requeteSql.ajouterValeurAuGroupe("idFlux", flux.getId().getCode());
      }
    }
    
    // Ajouter les identifiants des flux concernés à la requête.
    requeteSql.ajouter("WHERE FLID");
    requeteSql.ajouterGroupeDansRequete("idFlux", "in");
    
    // Lancer la requête
    queryManager.requete(requeteSql.getRequete());
  }
  
  // -- Méthodes privées
  
  // =======================
  // == NOUVELLES METHODES
  // =======================
  
  /**
   * Créer un identifiant de flux à partir des résultats de la requête.
   * 
   * @param record Enregistrement correspondant à la requête SQL réalisée.
   * @return Identifiant du flux.
   */
  private IdFlux completerIdFlux(GenericRecord record) {
    // Etablissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(record.getStringValue("FLETB"));
    // Numéro unique
    int code = record.getIntValue("FLID", 0);
    // Constitution de l'identifiant
    IdFlux idFlux = IdFlux.getInstance(idEtablissement, code);
    
    return idFlux;
  }
  
  /**
   * Créer un Flux complet à partir des résultats de la requête
   * 
   * @param record Enregistrement correspondant à la requête SQL réalisée.
   * @return Flux.
   */
  private Flux completerFlux(GenericRecord record) {
    // Créer l'identifiant
    IdFlux idFlux = completerIdFlux(record);
    // Créer et compléter l'objet
    Flux flux = new Flux(idFlux);
    
    // Compléter l'identifiant.
    if (record.isPresentField("FLIDF")) {
      EnumTypeFlux type = EnumTypeFlux.valueOfByCode(record.getField("FLIDF").toString().trim());
      flux.setType(type);
    }
    
    // Compléter la date de création
    if (record.isPresentField("FLCRE")) {
      try {
        flux.setDateCreation(Timestamp.valueOf(record.getField("FLCRE").toString().trim()));
      }
      catch (Exception e) {
        flux.setDateCreation(null);
      }
    }
    
    // Compléter le sens
    if (record.isPresentField("FLSNS")) {
      try {
        EnumSensFlux sens = EnumSensFlux.valueOfByCode(Integer.parseInt(record.getField("FLSNS").toString().trim()));
        flux.setSens(sens);
      }
      catch (Exception e) {
        flux.setSens(null);
      }
    }
    
    // Compléter le statut
    if (record.isPresentField("FLSTT")) {
      try {
        EnumStatutFlux statut = EnumStatutFlux.valueOfByNumero(Integer.parseInt(record.getField("FLSTT").toString().trim()));
        flux.setStatut(statut);
      }
      catch (Exception e) {
        flux.setStatut(null);
      }
    }
    
    // Compléter le code transmis
    if (record.isPresentField("FLCOD")) {
      flux.setCodeTransmis(record.getField("FLCOD").toString().trim());
    }
    
    // Compléter le code erreur
    if (record.isPresentField("FLERR")) {
      try {
        EnumCodeErreur codeErreur = EnumCodeErreur.valueOfByCode(Integer.parseInt(record.getField("FLERR").toString().trim()));
        flux.setCodeErreur(codeErreur);
      }
      catch (Exception e) {
        flux.setCodeErreur(null);
      }
    }
    
    // Compléter la date de traitement
    if (record.isPresentField("FLTRT")) {
      try {
        flux.setDateTraitement(Timestamp.valueOf(record.getField("FLTRT").toString().trim()));
      }
      catch (Exception e) {
        flux.setDateTraitement(null);
      }
    }
    
    // Compléter le compteur de tentatives
    if (record.isPresentField("FLCPT")) {
      try {
        flux.setCompteurTentatives(Integer.parseInt(record.getField("FLCPT").toString().trim()));
      }
      catch (Exception e) {
        flux.setCompteurTentatives(-1);
      }
    }
    
    // Compléter la version
    if (record.isPresentField("FLVER")) {
      flux.setVersion(record.getField("FLVER").toString().trim());
    }
    
    // Compléter le message d'erreur
    if (record.isPresentField("FLMSGERR")) {
      flux.setMessageErreur(record.getField("FLMSGERR").toString().trim());
    }
    
    // Compléter le contenu JSON
    if (record.isPresentField("FLJSN")) {
      flux.setContenu(record.getField("FLJSN").toString().trim());
    }
    
    return flux;
  }
  
  // =======================
  
  /**
   * Initialise la classe LienFluxTiersWebSerieN à partir du generic record.
   */
  private LienFluxTiersWebSerieN completerLienFluxTiersWebSerieN(GenericRecord pRecord) {
    // Création de l'identifiant à partir des données lues en table
    Integer numero = null;
    try {
      // Identifiant du lien (GSID est volontairement ignoré car identique)
      numero = pRecord.getIntegerValue("INID");
      IdLienFluxTiersWebSerieN idLien = IdLienFluxTiersWebSerieN.getInstance(numero);
      
      // Création de l'objet
      LienFluxTiersWebSerieN lien = new LienFluxTiersWebSerieN(idLien);
      
      // La base de données (GSBIBLI est volontairement ignoré car identique normalement)
      if (pRecord.isPresentField("INBIB")) {
        String valeur = pRecord.getStringValue("INBIB", "", true);
        IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(valeur);
        lien.setBaseDeDonnees(new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES));
      }
      
      // L'établissement
      if (pRecord.isPresentField("INETB")) {
        String valeur = pRecord.getStringValue("INETB", "", true);
        IdEtablissement idEtablissement = IdEtablissement.getInstance(valeur);
        lien.setIdEtablissement(idEtablissement);
      }
      
      // Actif
      if (pRecord.isPresentField("GSACTIF")) {
        Integer valeur = pRecord.getIntegerValue("GSACTIF", 0);
        if (valeur.intValue() > 0) {
          lien.setActif(Boolean.TRUE);
        }
        else {
          lien.setActif(Boolean.FALSE);
        }
      }
      
      // Délai
      if (pRecord.isPresentField("GSDELAI")) {
        Integer valeur = pRecord.getIntegerValue("GSDELAI", -1);
        if (valeur.intValue() >= 0) {
          lien.setDelaiBalayage(valeur);
        }
      }
      
      // La lettre d'environnement
      if (pRecord.isPresentField("GSLETTR")) {
        Character valeur = pRecord.getCharacterValue("GSLETTR", null);
        lien.setLettre(valeur);
      }
      
      // la Version
      if (pRecord.isPresentField("GSVERSI")) {
        String valeur = pRecord.getStringValue("GSVERSI", null, true);
        lien.setVersionFlux(valeur);
      }
      
      return lien;
    }
    catch (MessageErreurException e) {
      Trace.erreur("L'enregistrement avec l'identifiant " + numero + " de la table " + EnumTableBDD.FLUX_LIEN_INSTANCE
          + " est ignoré car un ou plusieurs champs ont des valeurs invalides.");
      return null;
    }
  }
  
  // == Méthodes pour flux V3
  
  // -- Méthodes publiques
  
  /**
   * Sauve un enregistrement.
   * 
   * @param pEnvoiStock
   */
  public IdHistoriqueEnvoiStock sauverEnvoiStockV3(HistoriqueEnvoiStock pHistoriqueEnvoiStock) {
    if (pHistoriqueEnvoiStock == null) {
      throw new MessageErreurException("L'historique d'envoi du flux stock est invalide.");
    }
    
    Integer numeroFlux = pHistoriqueEnvoiStock.getNumeroFlux();
    if (numeroFlux == null) {
      numeroFlux = Integer.valueOf(0);
    }
    
    RequeteSql requeteSql = new RequeteSql();
    
    // Mise à jour de l'enregistrement
    if (pHistoriqueEnvoiStock.getId().isExistant()) {
      // @formatter:off
      requeteSql.ajouter("update " + queryManager.getLibrary() + '.' + EnumTableBDD.FLUX_HISTORIQUE_ENVOI_STOCK + " set ");
      requeteSql.ajouterValeurUpdate("FKETB", pHistoriqueEnvoiStock.getId().getCodeEtablissement());
      requeteSql.ajouterValeurUpdate("FKMAG", pHistoriqueEnvoiStock.getId().getCodeMagasin());
      requeteSql.ajouterValeurUpdate("FKART", pHistoriqueEnvoiStock.getIdArticle().getCodeArticle());
      requeteSql.ajouterValeurUpdate("FKFLID", numeroFlux);
      requeteSql.ajouterValeurUpdate("FKDAT", ConvertDate.dateToDb2(pHistoriqueEnvoiStock.getDateEnvoi()));
      requeteSql.ajouterValeurUpdate("FKHEU", ConvertDate.dateToHeureDb2(pHistoriqueEnvoiStock.getDateEnvoi(), true));
      requeteSql.ajouterValeurUpdate("FKSAR", pHistoriqueEnvoiStock.getStockAvantRupture());
      requeteSql.ajouterValeurUpdate("FKDTR", ConvertDate.dateToDb2(pHistoriqueEnvoiStock.getDateRupturePossible()));
      requeteSql.ajouterValeurUpdate("FKDTFR", ConvertDate.dateToDb2(pHistoriqueEnvoiStock.getDateFinRupture()));
      requeteSql.ajouter(" where ");
      requeteSql.ajouterConditionAnd("FKID", "=", pHistoriqueEnvoiStock.getId().getNumero());
      // @formatter:on
      // Exécution de la requête
      int retour = queryManager.requete(requeteSql.getRequete());
      if (retour == Constantes.ERREUR) {
        throw new MessageErreurException(
            "Problème lors de la mise à jour de l'enregistrement de l'historique d'envoi du flux stock " + pHistoriqueEnvoiStock);
      }
      return pHistoriqueEnvoiStock.getId();
    }
    
    // Insertion d'un nouvel enregistrement
    // @formatter:off
    requeteSql.ajouter("select FKID, FKETB, FKMAG from new table (insert into " + queryManager.getLibrary() + '.'
        + EnumTableBDD.FLUX_HISTORIQUE_ENVOI_STOCK
        + " (FKETB, FKMAG, FKART, FKFLID, FKDAT, FKHEU, FKSAR, FKDTR, FKDTFR) values ("
        + RequeteSql.formaterStringSQL(pHistoriqueEnvoiStock.getId().getCodeEtablissement())
        + ", " + RequeteSql.formaterStringSQL(pHistoriqueEnvoiStock.getId().getCodeMagasin())
        + ", " + RequeteSql.formaterStringSQL(pHistoriqueEnvoiStock.getIdArticle().getCodeArticle())
        + ", " + numeroFlux
        + ", " + ConvertDate.dateToDb2(pHistoriqueEnvoiStock.getDateEnvoi())
        + ", " + ConvertDate.dateToHeureDb2(pHistoriqueEnvoiStock.getDateEnvoi(), true)
        + ", " + pHistoriqueEnvoiStock.getStockAvantRupture()
        + ", " + ConvertDate.dateToDb2(pHistoriqueEnvoiStock.getDateRupturePossible())
        + ", " + ConvertDate.dateToDb2(pHistoriqueEnvoiStock.getDateFinRupture())
        + "))");
    // @formatter:on
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequete());
    if (listeRecord == null || listeRecord.isEmpty()) {
      throw new MessageErreurException(
          "Erreur lors de l'enregistrement de l'historique d'envoi du flux stock dans la base " + queryManager.getLibrary() + ".");
    }
    
    // Création de l'identifiant définitif de l'historique d'envoi du flux stock
    return creerIdentifiantHistoriqueEnvoiStock(listeRecord.get(0));
  }
  
  /**
   * Supprime tous les enregistrements inférieurs à une date donnée exclue.
   * 
   * @param pDateExclue
   * @return Le nombre d'enregistrements supprimés.
   */
  public int purgerEnregistrementEnvoiStockV3(Date pDateExclue) {
    if (pDateExclue == null) {
      return 0;
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("delete from " + queryManager.getLibrary() + '.' + EnumTableBDD.FLUX_HISTORIQUE_ENVOI_STOCK + " where");
    requeteSql.ajouterConditionAnd("FKDAT", "<", ConvertDate.dateToDb2(pDateExclue));
    // Exécution de la requête
    int retour = queryManager.requete(requeteSql.getRequete());
    if (retour == Constantes.ERREUR) {
      throw new MessageErreurException(
          "Problème lors de la purge des enregistrements dans l'historique d'envoi du flux stock à la date du "
              + DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, pDateExclue));
    }
    return retour;
  }
  
  // -- Méthodes privées
  
  /**
   * Créer l'identifiant d'un objet HistoriqueEnvoiStock à partir d'un GenericRecord.
   */
  private IdHistoriqueEnvoiStock creerIdentifiantHistoriqueEnvoiStock(GenericRecord pGenericRecord) {
    if (pGenericRecord == null) {
      throw new MessageErreurException("L'enregistrement base de données est invalide.");
    }
    
    // Lire le numéro
    Integer numero = null;
    if (pGenericRecord.isPresentField("FKID")) {
      numero = pGenericRecord.getIntegerValue("FKID");
    }
    
    // Lire l'établissement
    IdEtablissement idEtablissement = null;
    if (pGenericRecord.isPresentField("FKETB")) {
      idEtablissement = IdEtablissement.getInstance(pGenericRecord.getStringValue("FKETB"));
    }
    
    // Lire le magasin
    IdMagasin idMagasin = null;
    if (pGenericRecord.isPresentField("FKMAG")) {
      idMagasin = IdMagasin.getInstance(idEtablissement, pGenericRecord.getStringValue("FKMAG"));
    }
    
    // Retourner l'identifiant du document de ventes
    return IdHistoriqueEnvoiStock.getInstance(idMagasin, numero);
  }
}
