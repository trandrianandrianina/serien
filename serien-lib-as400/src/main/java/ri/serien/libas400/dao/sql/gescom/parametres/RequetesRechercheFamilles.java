/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.famille.CritereFamille;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class RequetesRechercheFamilles {
  
  /**
   * Retourner la liste des familles pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   */
  public static String getRequete(String curlib, CritereFamille pCriteres) {
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select * from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where");
    if (pCriteres.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("PARETB", "=", pCriteres.getIdEtablissement().getCodeEtablissement());
    }
    requeteSql.ajouterConditionAnd("PARTYP", "=", "FA");
    if (!pCriteres.isAvecAnnule()) {
      requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    }
    requeteSql.ajouter("ORDER BY PARZ2");
    return requeteSql.getRequete();
    
  }
  
}
