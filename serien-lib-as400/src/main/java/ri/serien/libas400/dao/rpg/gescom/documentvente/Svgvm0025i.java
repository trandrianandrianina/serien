/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0025i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINFA = 7;
  public static final int DECIMAL_PINFA = 0;
  public static final int SIZE_PINOM3 = 30;
  public static final int SIZE_PICPL3 = 30;
  public static final int SIZE_PIRUE3 = 30;
  public static final int SIZE_PILOC3 = 30;
  public static final int SIZE_PICDP3 = 5;
  public static final int SIZE_PIFIL3 = 1;
  public static final int SIZE_PIVIL3 = 24;
  public static final int SIZE_PIPAY3 = 30;
  public static final int SIZE_PINOM2 = 30;
  public static final int SIZE_PICPL2 = 30;
  public static final int SIZE_PIRUE2 = 30;
  public static final int SIZE_PILOC2 = 30;
  public static final int SIZE_PICDP2 = 5;
  public static final int SIZE_PIFIL2 = 1;
  public static final int SIZE_PIVIL2 = 24;
  public static final int SIZE_PIPAY2 = 30;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 389;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINFA = 5;
  public static final int VAR_PINOM3 = 6;
  public static final int VAR_PICPL3 = 7;
  public static final int VAR_PIRUE3 = 8;
  public static final int VAR_PILOC3 = 9;
  public static final int VAR_PICDP3 = 10;
  public static final int VAR_PIFIL3 = 11;
  public static final int VAR_PIVIL3 = 12;
  public static final int VAR_PIPAY3 = 13;
  public static final int VAR_PINOM2 = 14;
  public static final int VAR_PICPL2 = 15;
  public static final int VAR_PIRUE2 = 16;
  public static final int VAR_PILOC2 = 17;
  public static final int VAR_PICDP2 = 18;
  public static final int VAR_PIFIL2 = 19;
  public static final int VAR_PIVIL2 = 20;
  public static final int VAR_PIPAY2 = 21;
  public static final int VAR_PIARR = 22;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9" *
  private String pietb = ""; // Code établissement *
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon *
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe *
  private BigDecimal pinfa = BigDecimal.ZERO; // Numéro de facture
  private String pinom3 = ""; // Nom client
  private String picpl3 = ""; // Complément du nom
  private String pirue3 = ""; // Rue
  private String piloc3 = ""; // Localité
  private String picdp3 = ""; // Code Postal
  private String pifil3 = ""; // Filler
  private String pivil3 = ""; // Ville
  private String pipay3 = ""; // Pays
  private String pinom2 = ""; // Nom client
  private String picpl2 = ""; // Complément du nom
  private String pirue2 = ""; // Rue
  private String piloc2 = ""; // Localité
  private String picdp2 = ""; // Code Postal
  private String pifil2 = ""; // Filler
  private String pivil2 = ""; // Ville
  private String pipay2 = ""; // Pays
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9" *
      new AS400Text(SIZE_PIETB), // Code établissement *
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon *
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe *
      new AS400ZonedDecimal(SIZE_PINFA, DECIMAL_PINFA), // Numéro de facture
      new AS400Text(SIZE_PINOM3), // Nom client
      new AS400Text(SIZE_PICPL3), // Complément du nom
      new AS400Text(SIZE_PIRUE3), // Rue
      new AS400Text(SIZE_PILOC3), // Localité
      new AS400Text(SIZE_PICDP3), // Code Postal
      new AS400Text(SIZE_PIFIL3), // Filler
      new AS400Text(SIZE_PIVIL3), // Ville
      new AS400Text(SIZE_PIPAY3), // Pays
      new AS400Text(SIZE_PINOM2), // Nom client
      new AS400Text(SIZE_PICPL2), // Complément du nom
      new AS400Text(SIZE_PIRUE2), // Rue
      new AS400Text(SIZE_PILOC2), // Localité
      new AS400Text(SIZE_PICDP2), // Code Postal
      new AS400Text(SIZE_PIFIL2), // Filler
      new AS400Text(SIZE_PIVIL2), // Ville
      new AS400Text(SIZE_PIPAY2), // Pays
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinfa, pinom3, picpl3, pirue3, piloc3, picdp3, pifil3, pivil3, pipay3, pinom2,
          picpl2, pirue2, piloc2, picdp2, pifil2, pivil2, pipay2, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinfa = (BigDecimal) output[5];
    pinom3 = (String) output[6];
    picpl3 = (String) output[7];
    pirue3 = (String) output[8];
    piloc3 = (String) output[9];
    picdp3 = (String) output[10];
    pifil3 = (String) output[11];
    pivil3 = (String) output[12];
    pipay3 = (String) output[13];
    pinom2 = (String) output[14];
    picpl2 = (String) output[15];
    pirue2 = (String) output[16];
    piloc2 = (String) output[17];
    picdp2 = (String) output[18];
    pifil2 = (String) output[19];
    pivil2 = (String) output[20];
    pipay2 = (String) output[21];
    piarr = (String) output[22];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinfa(BigDecimal pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = pPinfa.setScale(DECIMAL_PINFA, RoundingMode.HALF_UP);
  }
  
  public void setPinfa(Integer pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(pPinfa);
  }
  
  public void setPinfa(Date pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pPinfa));
  }
  
  public Integer getPinfa() {
    return pinfa.intValue();
  }
  
  public Date getPinfaConvertiEnDate() {
    return ConvertDate.db2ToDate(pinfa.intValue(), null);
  }
  
  public void setPinom3(String pPinom3) {
    if (pPinom3 == null) {
      return;
    }
    pinom3 = pPinom3;
  }
  
  public String getPinom3() {
    return pinom3;
  }
  
  public void setPicpl3(String pPicpl3) {
    if (pPicpl3 == null) {
      return;
    }
    picpl3 = pPicpl3;
  }
  
  public String getPicpl3() {
    return picpl3;
  }
  
  public void setPirue3(String pPirue3) {
    if (pPirue3 == null) {
      return;
    }
    pirue3 = pPirue3;
  }
  
  public String getPirue3() {
    return pirue3;
  }
  
  public void setPiloc3(String pPiloc3) {
    if (pPiloc3 == null) {
      return;
    }
    piloc3 = pPiloc3;
  }
  
  public String getPiloc3() {
    return piloc3;
  }
  
  public void setPicdp3(String pPicdp3) {
    if (pPicdp3 == null) {
      return;
    }
    picdp3 = pPicdp3;
  }
  
  public String getPicdp3() {
    return picdp3;
  }
  
  public void setPifil3(Character pPifil3) {
    if (pPifil3 == null) {
      return;
    }
    pifil3 = String.valueOf(pPifil3);
  }
  
  public Character getPifil3() {
    return pifil3.charAt(0);
  }
  
  public void setPivil3(String pPivil3) {
    if (pPivil3 == null) {
      return;
    }
    pivil3 = pPivil3;
  }
  
  public String getPivil3() {
    return pivil3;
  }
  
  public void setPipay3(String pPipay3) {
    if (pPipay3 == null) {
      return;
    }
    pipay3 = pPipay3;
  }
  
  public String getPipay3() {
    return pipay3;
  }
  
  public void setPinom2(String pPinom2) {
    if (pPinom2 == null) {
      return;
    }
    pinom2 = pPinom2;
  }
  
  public String getPinom2() {
    return pinom2;
  }
  
  public void setPicpl2(String pPicpl2) {
    if (pPicpl2 == null) {
      return;
    }
    picpl2 = pPicpl2;
  }
  
  public String getPicpl2() {
    return picpl2;
  }
  
  public void setPirue2(String pPirue2) {
    if (pPirue2 == null) {
      return;
    }
    pirue2 = pPirue2;
  }
  
  public String getPirue2() {
    return pirue2;
  }
  
  public void setPiloc2(String pPiloc2) {
    if (pPiloc2 == null) {
      return;
    }
    piloc2 = pPiloc2;
  }
  
  public String getPiloc2() {
    return piloc2;
  }
  
  public void setPicdp2(String pPicdp2) {
    if (pPicdp2 == null) {
      return;
    }
    picdp2 = pPicdp2;
  }
  
  public String getPicdp2() {
    return picdp2;
  }
  
  public void setPifil2(Character pPifil2) {
    if (pPifil2 == null) {
      return;
    }
    pifil2 = String.valueOf(pPifil2);
  }
  
  public Character getPifil2() {
    return pifil2.charAt(0);
  }
  
  public void setPivil2(String pPivil2) {
    if (pPivil2 == null) {
      return;
    }
    pivil2 = pPivil2;
  }
  
  public String getPivil2() {
    return pivil2;
  }
  
  public void setPipay2(String pPipay2) {
    if (pPipay2 == null) {
      return;
    }
    pipay2 = pPipay2;
  }
  
  public String getPipay2() {
    return pipay2;
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
