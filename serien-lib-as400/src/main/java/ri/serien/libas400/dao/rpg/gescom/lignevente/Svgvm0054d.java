/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0054d {
  // Constantes
  public static final int SIZE_LOTOP = 1;
  public static final int DECIMAL_LOTOP = 0;
  public static final int SIZE_LOCOD = 1;
  public static final int SIZE_LOETB = 3;
  public static final int SIZE_LONUM = 6;
  public static final int DECIMAL_LONUM = 0;
  public static final int SIZE_LOSUF = 1;
  public static final int DECIMAL_LOSUF = 0;
  public static final int SIZE_LONLI = 4;
  public static final int DECIMAL_LONLI = 0;
  public static final int SIZE_LOERL = 1;
  public static final int SIZE_LOCEX = 1;
  public static final int SIZE_LOQEX = 11;
  public static final int DECIMAL_LOQEX = 3;
  public static final int SIZE_LOTVA = 1;
  public static final int DECIMAL_LOTVA = 0;
  public static final int SIZE_LOTB = 1;
  public static final int DECIMAL_LOTB = 0;
  public static final int SIZE_LOTR = 1;
  public static final int DECIMAL_LOTR = 0;
  public static final int SIZE_LOTN = 1;
  public static final int DECIMAL_LOTN = 0;
  public static final int SIZE_LOTC = 1;
  public static final int DECIMAL_LOTC = 0;
  public static final int SIZE_LOTT = 1;
  public static final int DECIMAL_LOTT = 0;
  public static final int SIZE_LOVAL = 1;
  public static final int DECIMAL_LOVAL = 0;
  public static final int SIZE_LOTAR = 2;
  public static final int DECIMAL_LOTAR = 0;
  public static final int SIZE_LOCOL = 1;
  public static final int DECIMAL_LOCOL = 0;
  public static final int SIZE_LOTPF = 1;
  public static final int DECIMAL_LOTPF = 0;
  public static final int SIZE_LODCV = 1;
  public static final int DECIMAL_LODCV = 0;
  public static final int SIZE_LOTNC = 1;
  public static final int DECIMAL_LOTNC = 0;
  public static final int SIZE_LOSGN = 1;
  public static final int DECIMAL_LOSGN = 0;
  public static final int SIZE_LOSER = 2;
  public static final int DECIMAL_LOSER = 0;
  public static final int SIZE_LOQTE = 11;
  public static final int DECIMAL_LOQTE = 3;
  public static final int SIZE_LOKSV = 7;
  public static final int DECIMAL_LOKSV = 3;
  public static final int SIZE_LOPVB = 9;
  public static final int DECIMAL_LOPVB = 2;
  public static final int SIZE_LOREM1 = 4;
  public static final int DECIMAL_LOREM1 = 2;
  public static final int SIZE_LOREM2 = 4;
  public static final int DECIMAL_LOREM2 = 2;
  public static final int SIZE_LOREM3 = 4;
  public static final int DECIMAL_LOREM3 = 2;
  public static final int SIZE_LOREM4 = 4;
  public static final int DECIMAL_LOREM4 = 2;
  public static final int SIZE_LOREM5 = 4;
  public static final int DECIMAL_LOREM5 = 2;
  public static final int SIZE_LOREM6 = 4;
  public static final int DECIMAL_LOREM6 = 2;
  public static final int SIZE_LOTRL = 1;
  public static final int SIZE_LOBRL = 1;
  public static final int SIZE_LORP1 = 1;
  public static final int SIZE_LORP2 = 1;
  public static final int SIZE_LORP3 = 1;
  public static final int SIZE_LORP4 = 1;
  public static final int SIZE_LORP5 = 1;
  public static final int SIZE_LORP6 = 1;
  public static final int SIZE_LOPVN = 9;
  public static final int DECIMAL_LOPVN = 2;
  public static final int SIZE_LOPVC = 9;
  public static final int DECIMAL_LOPVC = 2;
  public static final int SIZE_LOMHT = 9;
  public static final int DECIMAL_LOMHT = 2;
  public static final int SIZE_LOPRP = 9;
  public static final int DECIMAL_LOPRP = 2;
  public static final int SIZE_LOCOE = 5;
  public static final int DECIMAL_LOCOE = 4;
  public static final int SIZE_LOPRV = 9;
  public static final int DECIMAL_LOPRV = 2;
  public static final int SIZE_LOAVR = 1;
  public static final int SIZE_LOUNV = 2;
  public static final int SIZE_LOART = 20;
  public static final int SIZE_LOCPL = 8;
  public static final int SIZE_LOCND = 9;
  public static final int DECIMAL_LOCND = 3;
  public static final int SIZE_LOIN1 = 1;
  public static final int SIZE_LOPRT = 7;
  public static final int DECIMAL_LOPRT = 2;
  public static final int SIZE_LODLP = 7;
  public static final int DECIMAL_LODLP = 0;
  public static final int SIZE_LOIN2 = 1;
  public static final int SIZE_LOIN3 = 1;
  public static final int SIZE_LOTP1 = 2;
  public static final int SIZE_LOTP2 = 2;
  public static final int SIZE_LOTP3 = 2;
  public static final int SIZE_LOTP4 = 2;
  public static final int SIZE_LOTP5 = 2;
  public static final int SIZE_LOGBA = 1;
  public static final int DECIMAL_LOGBA = 0;
  public static final int SIZE_LOARTS = 20;
  public static final int SIZE_LOIN4 = 1;
  public static final int SIZE_LOIN5 = 1;
  public static final int SIZE_LOIN6 = 1;
  public static final int SIZE_LOIN7 = 1;
  public static final int SIZE_LOQTL = 11;
  public static final int DECIMAL_LOQTL = 3;
  public static final int SIZE_LOMAG = 2;
  public static final int SIZE_LOREP = 2;
  public static final int SIZE_LOIN8 = 1;
  public static final int SIZE_LOIN9 = 1;
  public static final int SIZE_LOIN10 = 1;
  public static final int SIZE_LOIN11 = 1;
  public static final int SIZE_LOIN12 = 1;
  public static final int SIZE_LONLI0 = 4;
  public static final int DECIMAL_LONLI0 = 0;
  public static final int SIZE_LOPRA = 9;
  public static final int DECIMAL_LOPRA = 2;
  public static final int SIZE_LOPVA = 9;
  public static final int DECIMAL_LOPVA = 2;
  public static final int SIZE_LOQTP = 11;
  public static final int DECIMAL_LOQTP = 3;
  public static final int SIZE_LOMTR = 7;
  public static final int DECIMAL_LOMTR = 2;
  public static final int SIZE_LOSER3 = 1;
  public static final int SIZE_LOSER5 = 1;
  public static final int SIZE_LOIN17 = 1;
  public static final int SIZE_LOIN18 = 1;
  public static final int SIZE_LOIN19 = 1;
  public static final int SIZE_LOIN20 = 1;
  public static final int SIZE_LOIN21 = 1;
  public static final int SIZE_LOIN22 = 1;
  public static final int SIZE_LOQTEI = 11;
  public static final int DECIMAL_LOQTEI = 3;
  public static final int SIZE_LOQTLI = 11;
  public static final int DECIMAL_LOQTLI = 3;
  public static final int SIZE_LOQTPI = 11;
  public static final int DECIMAL_LOQTPI = 3;
  public static final int SIZE_LOMHTI = 9;
  public static final int DECIMAL_LOMHTI = 2;
  public static final int SIZE_A1UNL = 2;
  public static final int SIZE_WLIGLB = 124;
  public static final int SIZE_WPBTTC = 9;
  public static final int DECIMAL_WPBTTC = 2;
  public static final int SIZE_WPNTTC = 9;
  public static final int DECIMAL_WPNTTC = 2;
  public static final int SIZE_WPVTTC = 9;
  public static final int DECIMAL_WPVTTC = 2;
  public static final int SIZE_WMTTTC = 9;
  public static final int DECIMAL_WMTTTC = 2;
  public static final int SIZE_WPT1HT = 9;
  public static final int DECIMAL_WPT1HT = 2;
  public static final int SIZE_WPT1TTC = 9;
  public static final int DECIMAL_WPT1TTC = 2;
  public static final int SIZE_WQTEOR = 11;
  public static final int DECIMAL_WQTEOR = 3;
  public static final int SIZE_WQTEEX = 11;
  public static final int DECIMAL_WQTEEX = 3;
  public static final int SIZE_L2QT1 = 8;
  public static final int DECIMAL_L2QT1 = 3;
  public static final int SIZE_L2QT2 = 8;
  public static final int DECIMAL_L2QT2 = 3;
  public static final int SIZE_L2QT3 = 8;
  public static final int DECIMAL_L2QT3 = 3;
  public static final int SIZE_L2NBR = 6;
  public static final int DECIMAL_L2NBR = 0;
  public static final int SIZE_L12QT = 9;
  public static final int DECIMAL_L12QT = 3;
  public static final int SIZE_L12UC = 2;
  public static final int SIZE_WORIPR = 10;
  public static final int SIZE_WTAUTVA = 5;
  public static final int DECIMAL_WTAUTVA = 3;
  public static final int SIZE_LOPRS = 9;
  public static final int DECIMAL_LOPRS = 2;
  public static final int SIZE_WQTEFAC = 11;
  public static final int DECIMAL_WQTEFAC = 3;
  public static final int SIZE_WQTEAV = 11;
  public static final int DECIMAL_WQTEAV = 3;
  public static final int SIZE_WDAT = 7;
  public static final int DECIMAL_WDAT = 0;
  public static final int SIZE_WETA = 1;
  public static final int DECIMAL_WETA = 0;
  public static final int SIZE_WDATCDE = 7;
  public static final int DECIMAL_WDATCDE = 0;
  public static final int SIZE_WMAG = 2;
  public static final int SIZE_WNCC = 8;
  public static final int SIZE_WREP = 2;
  public static final int SIZE_WRCC = 25;
  public static final int SIZE_WNFA = 7;
  public static final int DECIMAL_WNFA = 0;
  public static final int SIZE_WMEX = 2;
  public static final int SIZE_WDUS = 1;
  public static final int SIZE_TOTALE_DS = 707;
  public static final int SIZE_FILLER = 700 - 707;
  
  // Constantes indices Nom DS
  public static final int VAR_LOTOP = 0;
  public static final int VAR_LOCOD = 1;
  public static final int VAR_LOETB = 2;
  public static final int VAR_LONUM = 3;
  public static final int VAR_LOSUF = 4;
  public static final int VAR_LONLI = 5;
  public static final int VAR_LOERL = 6;
  public static final int VAR_LOCEX = 7;
  public static final int VAR_LOQEX = 8;
  public static final int VAR_LOTVA = 9;
  public static final int VAR_LOTB = 10;
  public static final int VAR_LOTR = 11;
  public static final int VAR_LOTN = 12;
  public static final int VAR_LOTC = 13;
  public static final int VAR_LOTT = 14;
  public static final int VAR_LOVAL = 15;
  public static final int VAR_LOTAR = 16;
  public static final int VAR_LOCOL = 17;
  public static final int VAR_LOTPF = 18;
  public static final int VAR_LODCV = 19;
  public static final int VAR_LOTNC = 20;
  public static final int VAR_LOSGN = 21;
  public static final int VAR_LOSER = 22;
  public static final int VAR_LOQTE = 23;
  public static final int VAR_LOKSV = 24;
  public static final int VAR_LOPVB = 25;
  public static final int VAR_LOREM1 = 26;
  public static final int VAR_LOREM2 = 27;
  public static final int VAR_LOREM3 = 28;
  public static final int VAR_LOREM4 = 29;
  public static final int VAR_LOREM5 = 30;
  public static final int VAR_LOREM6 = 31;
  public static final int VAR_LOTRL = 32;
  public static final int VAR_LOBRL = 33;
  public static final int VAR_LORP1 = 34;
  public static final int VAR_LORP2 = 35;
  public static final int VAR_LORP3 = 36;
  public static final int VAR_LORP4 = 37;
  public static final int VAR_LORP5 = 38;
  public static final int VAR_LORP6 = 39;
  public static final int VAR_LOPVN = 40;
  public static final int VAR_LOPVC = 41;
  public static final int VAR_LOMHT = 42;
  public static final int VAR_LOPRP = 43;
  public static final int VAR_LOCOE = 44;
  public static final int VAR_LOPRV = 45;
  public static final int VAR_LOAVR = 46;
  public static final int VAR_LOUNV = 47;
  public static final int VAR_LOART = 48;
  public static final int VAR_LOCPL = 49;
  public static final int VAR_LOCND = 50;
  public static final int VAR_LOIN1 = 51;
  public static final int VAR_LOPRT = 52;
  public static final int VAR_LODLP = 53;
  public static final int VAR_LOIN2 = 54;
  public static final int VAR_LOIN3 = 55;
  public static final int VAR_LOTP1 = 56;
  public static final int VAR_LOTP2 = 57;
  public static final int VAR_LOTP3 = 58;
  public static final int VAR_LOTP4 = 59;
  public static final int VAR_LOTP5 = 60;
  public static final int VAR_LOGBA = 61;
  public static final int VAR_LOARTS = 62;
  public static final int VAR_LOIN4 = 63;
  public static final int VAR_LOIN5 = 64;
  public static final int VAR_LOIN6 = 65;
  public static final int VAR_LOIN7 = 66;
  public static final int VAR_LOQTL = 67;
  public static final int VAR_LOMAG = 68;
  public static final int VAR_LOREP = 69;
  public static final int VAR_LOIN8 = 70;
  public static final int VAR_LOIN9 = 71;
  public static final int VAR_LOIN10 = 72;
  public static final int VAR_LOIN11 = 73;
  public static final int VAR_LOIN12 = 74;
  public static final int VAR_LONLI0 = 75;
  public static final int VAR_LOPRA = 76;
  public static final int VAR_LOPVA = 77;
  public static final int VAR_LOQTP = 78;
  public static final int VAR_LOMTR = 79;
  public static final int VAR_LOSER3 = 80;
  public static final int VAR_LOSER5 = 81;
  public static final int VAR_LOIN17 = 82;
  public static final int VAR_LOIN18 = 83;
  public static final int VAR_LOIN19 = 84;
  public static final int VAR_LOIN20 = 85;
  public static final int VAR_LOIN21 = 86;
  public static final int VAR_LOIN22 = 87;
  public static final int VAR_LOQTEI = 88;
  public static final int VAR_LOQTLI = 89;
  public static final int VAR_LOQTPI = 90;
  public static final int VAR_LOMHTI = 91;
  public static final int VAR_A1UNL = 92;
  public static final int VAR_WLIGLB = 93;
  public static final int VAR_WPBTTC = 94;
  public static final int VAR_WPNTTC = 95;
  public static final int VAR_WPVTTC = 96;
  public static final int VAR_WMTTTC = 97;
  public static final int VAR_WPT1HT = 98;
  public static final int VAR_WPT1TTC = 99;
  public static final int VAR_WQTEOR = 100;
  public static final int VAR_WQTEEX = 101;
  public static final int VAR_L2QT1 = 102;
  public static final int VAR_L2QT2 = 103;
  public static final int VAR_L2QT3 = 104;
  public static final int VAR_L2NBR = 105;
  public static final int VAR_L12QT = 106;
  public static final int VAR_L12UC = 107;
  public static final int VAR_WORIPR = 108;
  public static final int VAR_WTAUTVA = 109;
  public static final int VAR_LOPRS = 110;
  public static final int VAR_WQTEFAC = 111;
  public static final int VAR_WQTEAV = 112;
  public static final int VAR_WDAT = 113;
  public static final int VAR_WETA = 114;
  public static final int VAR_WDATCDE = 115;
  public static final int VAR_WMAG = 116;
  public static final int VAR_WNCC = 117;
  public static final int VAR_WREP = 118;
  public static final int VAR_WRCC = 119;
  public static final int VAR_WNFA = 120;
  public static final int VAR_WMEX = 121;
  public static final int VAR_WDUS = 122;
  
  // Variables AS400
  private BigDecimal lotop = BigDecimal.ZERO; // Code Etat de la Ligne
  private String locod = ""; // Code ERL "E"
  private String loetb = ""; // Code Etablissement
  private BigDecimal lonum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal losuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal lonli = BigDecimal.ZERO; // Numéro de Ligne
  private String loerl = ""; // Code ERL "C"
  private String locex = ""; // Code Extraction
  private BigDecimal loqex = BigDecimal.ZERO; // Quantité extraite en UV
  private BigDecimal lotva = BigDecimal.ZERO; // Code TVA
  private BigDecimal lotb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal lotr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal lotn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal lotc = BigDecimal.ZERO; // Top coefficient saisi
  private BigDecimal lott = BigDecimal.ZERO; // Top colonne tarif saisie
  private BigDecimal loval = BigDecimal.ZERO; // Top Ligne en Valeur
  private BigDecimal lotar = BigDecimal.ZERO; // Numéro colonne tarif
  private BigDecimal locol = BigDecimal.ZERO; // Colonne TVA/E1TVAG
  private BigDecimal lotpf = BigDecimal.ZERO; // Code TPF
  private BigDecimal lodcv = BigDecimal.ZERO; // Top décimalisation
  private BigDecimal lotnc = BigDecimal.ZERO; // Top non commissionné
  private BigDecimal losgn = BigDecimal.ZERO; // Signe de la ligne
  private BigDecimal loser = BigDecimal.ZERO; // Top n° de serie ou lot
  private BigDecimal loqte = BigDecimal.ZERO; // Quantité Commandée
  private BigDecimal loksv = BigDecimal.ZERO; // Nbre d'US pour 1 UV
  private BigDecimal lopvb = BigDecimal.ZERO; // Prix de Vente de Base
  private BigDecimal lorem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal lorem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal lorem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal lorem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal lorem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal lorem6 = BigDecimal.ZERO; // % Remise 6
  private String lotrl = ""; // Type remise ligne, 1=cascade
  private String lobrl = ""; // Base remise ligne, 1=Montant
  private String lorp1 = ""; // Exclusion remise de pied N°1
  private String lorp2 = ""; // Exclusion remise de pied N°2
  private String lorp3 = ""; // Exclusion remise de pied N°3
  private String lorp4 = ""; // Exclusion remise de pied N°4
  private String lorp5 = ""; // Exclusion remise de pied N°5
  private String lorp6 = ""; // Exclusion remise de pied N°6
  private BigDecimal lopvn = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal lopvc = BigDecimal.ZERO; // Prix de Vente Calculé
  private BigDecimal lomht = BigDecimal.ZERO; // Montant Hors Taxes
  private BigDecimal loprp = BigDecimal.ZERO; // Prix de Promo. (Utilisé)
  private BigDecimal locoe = BigDecimal.ZERO; // Coeff. Mult. prix de base
  private BigDecimal loprv = BigDecimal.ZERO; // Prix de Revient
  private String loavr = ""; // Code Ligne Avoir
  private String lounv = ""; // Code Unité de Vente
  private String loart = ""; // Code Article
  private String locpl = ""; // Complément de Libellé
  private BigDecimal locnd = BigDecimal.ZERO; // Conditionnement
  private String loin1 = ""; // Ind. non soumis à escompte
  private BigDecimal loprt = BigDecimal.ZERO; // Prix de transport
  private BigDecimal lodlp = BigDecimal.ZERO; // Date de livraison prévue
  private String loin2 = ""; // Ind. Type de gratuits
  private String loin3 = ""; // Ind. Kit,Taxe etc...
  private String lotp1 = ""; // Top personnal.N°1
  private String lotp2 = ""; // Top personnal.N°2
  private String lotp3 = ""; // Top personnal.N°3
  private String lotp4 = ""; // Top personnal.N°4
  private String lotp5 = ""; // Top personnal.N°5
  private BigDecimal logba = BigDecimal.ZERO; // Génération Bon Achat
  private String loarts = ""; // Code article substitué
  private String loin4 = ""; // Ind. Type substitution
  private String loin5 = ""; // Ind. remise 50/50 sur com/rp
  private String loin6 = ""; // Ind. PRV saisi sur ligne
  private String loin7 = ""; // Ind. recherche prix / kit
  private BigDecimal loqtl = BigDecimal.ZERO; // Quantité Livrable
  private String lomag = ""; // Magasin
  private String lorep = ""; // Représentant
  private String loin8 = ""; // Indice de livraison
  private String loin9 = ""; // Ind cumul(pds,vol,col)/l1qte
  private String loin10 = ""; // Ind cumul(pds,vol,col)/l1qtp
  private String loin11 = ""; // Ligne à extraire
  private String loin12 = ""; // Ind. ASDI =A1IN13
  private BigDecimal lonli0 = BigDecimal.ZERO; // Numéro de Ligne origine
  private BigDecimal lopra = BigDecimal.ZERO; // Prix à ajouter au PRV
  private BigDecimal lopva = BigDecimal.ZERO; // Prix à ajouter au PVC
  private BigDecimal loqtp = BigDecimal.ZERO; // Quantité en Pièces
  private BigDecimal lomtr = BigDecimal.ZERO; // Montant transport
  private String loser3 = ""; // top article loti N.U
  private String loser5 = ""; // top article ads N.U
  private String loin17 = ""; // Type de vente
  private String loin18 = ""; // Prix garanti, affaire, dérog
  private String loin19 = ""; // Applic. condition quantitat.
  private String loin20 = ""; // Cond. Chantier Blanc,A F S G
  private String loin21 = ""; // Ind. regroupement ligne
  private String loin22 = ""; // N.U
  private BigDecimal loqtei = BigDecimal.ZERO; // Quantité Commandée initiale
  private BigDecimal loqtli = BigDecimal.ZERO; // Quantité Livrable initiale
  private BigDecimal loqtpi = BigDecimal.ZERO; // Quantité en Pièces initiale
  private BigDecimal lomhti = BigDecimal.ZERO; // Montant Hors Taxes initiale
  private String a1unl = ""; // Code Unité de conditionnemet
  private String wliglb = ""; // Libellé
  private BigDecimal wpbttc = BigDecimal.ZERO; // Prix de base TTC
  private BigDecimal wpnttc = BigDecimal.ZERO; // Prix net TTC
  private BigDecimal wpvttc = BigDecimal.ZERO; // Prix de vente calculé TTC
  private BigDecimal wmtttc = BigDecimal.ZERO; // Montant TTC
  private BigDecimal wpt1ht = BigDecimal.ZERO; // Prix Tarif 1 H.T
  private BigDecimal wpt1ttc = BigDecimal.ZERO; // Prix Tarif 1 T.T.C
  private BigDecimal wqteor = BigDecimal.ZERO; // Quantité document origine
  private BigDecimal wqteex = BigDecimal.ZERO; // Quantité déjà extraites
  private BigDecimal l2qt1 = BigDecimal.ZERO; // Quantité 1
  private BigDecimal l2qt2 = BigDecimal.ZERO; // Quantité 2
  private BigDecimal l2qt3 = BigDecimal.ZERO; // Quantité 3
  private BigDecimal l2nbr = BigDecimal.ZERO; // Nombre
  private BigDecimal l12qt = BigDecimal.ZERO; // Quantité initiale
  private String l12uc = ""; // Code unité initiale
  private String woripr = ""; // Origine prix
  private BigDecimal wtautva = BigDecimal.ZERO; // Taux de TVA
  private BigDecimal loprs = BigDecimal.ZERO; // Prix de revient standard
  private BigDecimal wqtefac = BigDecimal.ZERO; // Quantité déjà facturée
  private BigDecimal wqteav = BigDecimal.ZERO; // Quantité déjà retournée
  private BigDecimal wdat = BigDecimal.ZERO; //
  private BigDecimal weta = BigDecimal.ZERO; // E1ETA ou E1TDV si devis
  private BigDecimal wdatcde = BigDecimal.ZERO; //
  private String wmag = ""; //
  private String wncc = ""; //
  private String wrep = ""; //
  private String wrcc = ""; //
  private BigDecimal wnfa = BigDecimal.ZERO; // Numéro de facture
  private String wmex = ""; // Mode d"expédition
  private String wdus = ""; // Direct usine E1IN18 = D
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400ZonedDecimal(SIZE_LOTOP, DECIMAL_LOTOP), // Code Etat de la Ligne
      new AS400Text(SIZE_LOCOD), // Code ERL "E"
      new AS400Text(SIZE_LOETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LONUM, DECIMAL_LONUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LOSUF, DECIMAL_LOSUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LONLI, DECIMAL_LONLI), // Numéro de Ligne
      new AS400Text(SIZE_LOERL), // Code ERL "C"
      new AS400Text(SIZE_LOCEX), // Code Extraction
      new AS400PackedDecimal(SIZE_LOQEX, DECIMAL_LOQEX), // Quantité extraite en UV
      new AS400ZonedDecimal(SIZE_LOTVA, DECIMAL_LOTVA), // Code TVA
      new AS400ZonedDecimal(SIZE_LOTB, DECIMAL_LOTB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_LOTR, DECIMAL_LOTR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_LOTN, DECIMAL_LOTN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_LOTC, DECIMAL_LOTC), // Top coefficient saisi
      new AS400ZonedDecimal(SIZE_LOTT, DECIMAL_LOTT), // Top colonne tarif saisie
      new AS400ZonedDecimal(SIZE_LOVAL, DECIMAL_LOVAL), // Top Ligne en Valeur
      new AS400ZonedDecimal(SIZE_LOTAR, DECIMAL_LOTAR), // Numéro colonne tarif
      new AS400ZonedDecimal(SIZE_LOCOL, DECIMAL_LOCOL), // Colonne TVA/E1TVAG
      new AS400ZonedDecimal(SIZE_LOTPF, DECIMAL_LOTPF), // Code TPF
      new AS400ZonedDecimal(SIZE_LODCV, DECIMAL_LODCV), // Top décimalisation
      new AS400ZonedDecimal(SIZE_LOTNC, DECIMAL_LOTNC), // Top non commissionné
      new AS400ZonedDecimal(SIZE_LOSGN, DECIMAL_LOSGN), // Signe de la ligne
      new AS400ZonedDecimal(SIZE_LOSER, DECIMAL_LOSER), // Top n° de serie ou lot
      new AS400PackedDecimal(SIZE_LOQTE, DECIMAL_LOQTE), // Quantité Commandée
      new AS400PackedDecimal(SIZE_LOKSV, DECIMAL_LOKSV), // Nbre d'US pour 1 UV
      new AS400PackedDecimal(SIZE_LOPVB, DECIMAL_LOPVB), // Prix de Vente de Base
      new AS400ZonedDecimal(SIZE_LOREM1, DECIMAL_LOREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_LOREM2, DECIMAL_LOREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_LOREM3, DECIMAL_LOREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_LOREM4, DECIMAL_LOREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_LOREM5, DECIMAL_LOREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_LOREM6, DECIMAL_LOREM6), // % Remise 6
      new AS400Text(SIZE_LOTRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_LOBRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_LORP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_LORP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_LORP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_LORP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_LORP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_LORP6), // Exclusion remise de pied N°6
      new AS400PackedDecimal(SIZE_LOPVN, DECIMAL_LOPVN), // Prix de Vente Net
      new AS400PackedDecimal(SIZE_LOPVC, DECIMAL_LOPVC), // Prix de Vente Calculé
      new AS400PackedDecimal(SIZE_LOMHT, DECIMAL_LOMHT), // Montant Hors Taxes
      new AS400PackedDecimal(SIZE_LOPRP, DECIMAL_LOPRP), // Prix de Promo. (Utilisé)
      new AS400PackedDecimal(SIZE_LOCOE, DECIMAL_LOCOE), // Coeff. Mult. prix de base
      new AS400PackedDecimal(SIZE_LOPRV, DECIMAL_LOPRV), // Prix de Revient
      new AS400Text(SIZE_LOAVR), // Code Ligne Avoir
      new AS400Text(SIZE_LOUNV), // Code Unité de Vente
      new AS400Text(SIZE_LOART), // Code Article
      new AS400Text(SIZE_LOCPL), // Complément de Libellé
      new AS400PackedDecimal(SIZE_LOCND, DECIMAL_LOCND), // Conditionnement
      new AS400Text(SIZE_LOIN1), // Ind. non soumis à escompte
      new AS400PackedDecimal(SIZE_LOPRT, DECIMAL_LOPRT), // Prix de transport
      new AS400PackedDecimal(SIZE_LODLP, DECIMAL_LODLP), // Date de livraison prévue
      new AS400Text(SIZE_LOIN2), // Ind. Type de gratuits
      new AS400Text(SIZE_LOIN3), // Ind. Kit,Taxe etc...
      new AS400Text(SIZE_LOTP1), // Top personnal.N°1
      new AS400Text(SIZE_LOTP2), // Top personnal.N°2
      new AS400Text(SIZE_LOTP3), // Top personnal.N°3
      new AS400Text(SIZE_LOTP4), // Top personnal.N°4
      new AS400Text(SIZE_LOTP5), // Top personnal.N°5
      new AS400ZonedDecimal(SIZE_LOGBA, DECIMAL_LOGBA), // Génération Bon Achat
      new AS400Text(SIZE_LOARTS), // Code article substitué
      new AS400Text(SIZE_LOIN4), // Ind. Type substitution
      new AS400Text(SIZE_LOIN5), // Ind. remise 50/50 sur com/rp
      new AS400Text(SIZE_LOIN6), // Ind. PRV saisi sur ligne
      new AS400Text(SIZE_LOIN7), // Ind. recherche prix / kit
      new AS400PackedDecimal(SIZE_LOQTL, DECIMAL_LOQTL), // Quantité Livrable
      new AS400Text(SIZE_LOMAG), // Magasin
      new AS400Text(SIZE_LOREP), // Représentant
      new AS400Text(SIZE_LOIN8), // Indice de livraison
      new AS400Text(SIZE_LOIN9), // Ind cumul(pds,vol,col)/l1qte
      new AS400Text(SIZE_LOIN10), // Ind cumul(pds,vol,col)/l1qtp
      new AS400Text(SIZE_LOIN11), // Ligne à extraire
      new AS400Text(SIZE_LOIN12), // Ind. ASDI =A1IN13
      new AS400ZonedDecimal(SIZE_LONLI0, DECIMAL_LONLI0), // Numéro de Ligne origine
      new AS400PackedDecimal(SIZE_LOPRA, DECIMAL_LOPRA), // Prix à ajouter au PRV
      new AS400PackedDecimal(SIZE_LOPVA, DECIMAL_LOPVA), // Prix à ajouter au PVC
      new AS400PackedDecimal(SIZE_LOQTP, DECIMAL_LOQTP), // Quantité en Pièces
      new AS400PackedDecimal(SIZE_LOMTR, DECIMAL_LOMTR), // Montant transport
      new AS400Text(SIZE_LOSER3), // top article loti N.U
      new AS400Text(SIZE_LOSER5), // top article ads N.U
      new AS400Text(SIZE_LOIN17), // Type de vente
      new AS400Text(SIZE_LOIN18), // Prix garanti, affaire, dérog
      new AS400Text(SIZE_LOIN19), // Applic. condition quantitat.
      new AS400Text(SIZE_LOIN20), // Cond. Chantier Blanc,A F S G
      new AS400Text(SIZE_LOIN21), // Ind. regroupement ligne
      new AS400Text(SIZE_LOIN22), // N.U
      new AS400PackedDecimal(SIZE_LOQTEI, DECIMAL_LOQTEI), // Quantité Commandée initiale
      new AS400PackedDecimal(SIZE_LOQTLI, DECIMAL_LOQTLI), // Quantité Livrable initiale
      new AS400PackedDecimal(SIZE_LOQTPI, DECIMAL_LOQTPI), // Quantité en Pièces initiale
      new AS400PackedDecimal(SIZE_LOMHTI, DECIMAL_LOMHTI), // Montant Hors Taxes initiale
      new AS400Text(SIZE_A1UNL), // Code Unité de conditionnemet
      new AS400Text(SIZE_WLIGLB), // Libellé
      new AS400ZonedDecimal(SIZE_WPBTTC, DECIMAL_WPBTTC), // Prix de base TTC
      new AS400ZonedDecimal(SIZE_WPNTTC, DECIMAL_WPNTTC), // Prix net TTC
      new AS400ZonedDecimal(SIZE_WPVTTC, DECIMAL_WPVTTC), // Prix de vente calculé TTC
      new AS400ZonedDecimal(SIZE_WMTTTC, DECIMAL_WMTTTC), // Montant TTC
      new AS400ZonedDecimal(SIZE_WPT1HT, DECIMAL_WPT1HT), // Prix Tarif 1 H.T
      new AS400ZonedDecimal(SIZE_WPT1TTC, DECIMAL_WPT1TTC), // Prix Tarif 1 T.T.C
      new AS400ZonedDecimal(SIZE_WQTEOR, DECIMAL_WQTEOR), // Quantité document origine
      new AS400ZonedDecimal(SIZE_WQTEEX, DECIMAL_WQTEEX), // Quantité déjà extraites
      new AS400ZonedDecimal(SIZE_L2QT1, DECIMAL_L2QT1), // Quantité 1
      new AS400ZonedDecimal(SIZE_L2QT2, DECIMAL_L2QT2), // Quantité 2
      new AS400ZonedDecimal(SIZE_L2QT3, DECIMAL_L2QT3), // Quantité 3
      new AS400ZonedDecimal(SIZE_L2NBR, DECIMAL_L2NBR), // Nombre
      new AS400ZonedDecimal(SIZE_L12QT, DECIMAL_L12QT), // Quantité initiale
      new AS400Text(SIZE_L12UC), // Code unité initiale
      new AS400Text(SIZE_WORIPR), // Origine prix
      new AS400ZonedDecimal(SIZE_WTAUTVA, DECIMAL_WTAUTVA), // Taux de TVA
      new AS400ZonedDecimal(SIZE_LOPRS, DECIMAL_LOPRS), // Prix de revient standard
      new AS400ZonedDecimal(SIZE_WQTEFAC, DECIMAL_WQTEFAC), // Quantité déjà facturée
      new AS400ZonedDecimal(SIZE_WQTEAV, DECIMAL_WQTEAV), // Quantité déjà retournée
      new AS400ZonedDecimal(SIZE_WDAT, DECIMAL_WDAT), //
      new AS400ZonedDecimal(SIZE_WETA, DECIMAL_WETA), // E1ETA ou E1TDV si devis
      new AS400ZonedDecimal(SIZE_WDATCDE, DECIMAL_WDATCDE), //
      new AS400Text(SIZE_WMAG), //
      new AS400Text(SIZE_WNCC), //
      new AS400Text(SIZE_WREP), //
      new AS400Text(SIZE_WRCC), //
      new AS400ZonedDecimal(SIZE_WNFA, DECIMAL_WNFA), // Numéro de facture
      new AS400Text(SIZE_WMEX), // Mode d"expédition
      new AS400Text(SIZE_WDUS), // Direct usine E1IN18 = D
  };
  public Object[] o = { lotop, locod, loetb, lonum, losuf, lonli, loerl, locex, loqex, lotva, lotb, lotr, lotn, lotc, lott, loval, lotar,
      locol, lotpf, lodcv, lotnc, losgn, loser, loqte, loksv, lopvb, lorem1, lorem2, lorem3, lorem4, lorem5, lorem6, lotrl, lobrl, lorp1,
      lorp2, lorp3, lorp4, lorp5, lorp6, lopvn, lopvc, lomht, loprp, locoe, loprv, loavr, lounv, loart, locpl, locnd, loin1, loprt, lodlp,
      loin2, loin3, lotp1, lotp2, lotp3, lotp4, lotp5, logba, loarts, loin4, loin5, loin6, loin7, loqtl, lomag, lorep, loin8, loin9,
      loin10, loin11, loin12, lonli0, lopra, lopva, loqtp, lomtr, loser3, loser5, loin17, loin18, loin19, loin20, loin21, loin22, loqtei,
      loqtli, loqtpi, lomhti, a1unl, wliglb, wpbttc, wpnttc, wpvttc, wmtttc, wpt1ht, wpt1ttc, wqteor, wqteex, l2qt1, l2qt2, l2qt3, l2nbr,
      l12qt, l12uc, woripr, wtautva, loprs, wqtefac, wqteav, wdat, weta, wdatcde, wmag, wncc, wrep, wrcc, wnfa, wmex, wdus, };
  
  // -- Accesseurs
  
  public void setLotop(BigDecimal pLotop) {
    if (pLotop == null) {
      return;
    }
    lotop = pLotop.setScale(DECIMAL_LOTOP, RoundingMode.HALF_UP);
  }
  
  public void setLotop(Integer pLotop) {
    if (pLotop == null) {
      return;
    }
    lotop = BigDecimal.valueOf(pLotop);
  }
  
  public Integer getLotop() {
    return lotop.intValue();
  }
  
  public void setLocod(Character pLocod) {
    if (pLocod == null) {
      return;
    }
    locod = String.valueOf(pLocod);
  }
  
  public Character getLocod() {
    return locod.charAt(0);
  }
  
  public void setLoetb(String pLoetb) {
    if (pLoetb == null) {
      return;
    }
    loetb = pLoetb;
  }
  
  public String getLoetb() {
    return loetb;
  }
  
  public void setLonum(BigDecimal pLonum) {
    if (pLonum == null) {
      return;
    }
    lonum = pLonum.setScale(DECIMAL_LONUM, RoundingMode.HALF_UP);
  }
  
  public void setLonum(Integer pLonum) {
    if (pLonum == null) {
      return;
    }
    lonum = BigDecimal.valueOf(pLonum);
  }
  
  public Integer getLonum() {
    return lonum.intValue();
  }
  
  public void setLosuf(BigDecimal pLosuf) {
    if (pLosuf == null) {
      return;
    }
    losuf = pLosuf.setScale(DECIMAL_LOSUF, RoundingMode.HALF_UP);
  }
  
  public void setLosuf(Integer pLosuf) {
    if (pLosuf == null) {
      return;
    }
    losuf = BigDecimal.valueOf(pLosuf);
  }
  
  public Integer getLosuf() {
    return losuf.intValue();
  }
  
  public void setLonli(BigDecimal pLonli) {
    if (pLonli == null) {
      return;
    }
    lonli = pLonli.setScale(DECIMAL_LONLI, RoundingMode.HALF_UP);
  }
  
  public void setLonli(Integer pLonli) {
    if (pLonli == null) {
      return;
    }
    lonli = BigDecimal.valueOf(pLonli);
  }
  
  public Integer getLonli() {
    return lonli.intValue();
  }
  
  public void setLoerl(Character pLoerl) {
    if (pLoerl == null) {
      return;
    }
    loerl = String.valueOf(pLoerl);
  }
  
  public Character getLoerl() {
    return loerl.charAt(0);
  }
  
  public void setLocex(Character pLocex) {
    if (pLocex == null) {
      return;
    }
    locex = String.valueOf(pLocex);
  }
  
  public Character getLocex() {
    return locex.charAt(0);
  }
  
  public void setLoqex(BigDecimal pLoqex) {
    if (pLoqex == null) {
      return;
    }
    loqex = pLoqex.setScale(DECIMAL_LOQEX, RoundingMode.HALF_UP);
  }
  
  public void setLoqex(Double pLoqex) {
    if (pLoqex == null) {
      return;
    }
    loqex = BigDecimal.valueOf(pLoqex).setScale(DECIMAL_LOQEX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqex() {
    return loqex.setScale(DECIMAL_LOQEX, RoundingMode.HALF_UP);
  }
  
  public void setLotva(BigDecimal pLotva) {
    if (pLotva == null) {
      return;
    }
    lotva = pLotva.setScale(DECIMAL_LOTVA, RoundingMode.HALF_UP);
  }
  
  public void setLotva(Integer pLotva) {
    if (pLotva == null) {
      return;
    }
    lotva = BigDecimal.valueOf(pLotva);
  }
  
  public Integer getLotva() {
    return lotva.intValue();
  }
  
  public void setLotb(BigDecimal pLotb) {
    if (pLotb == null) {
      return;
    }
    lotb = pLotb.setScale(DECIMAL_LOTB, RoundingMode.HALF_UP);
  }
  
  public void setLotb(Integer pLotb) {
    if (pLotb == null) {
      return;
    }
    lotb = BigDecimal.valueOf(pLotb);
  }
  
  public Integer getLotb() {
    return lotb.intValue();
  }
  
  public void setLotr(BigDecimal pLotr) {
    if (pLotr == null) {
      return;
    }
    lotr = pLotr.setScale(DECIMAL_LOTR, RoundingMode.HALF_UP);
  }
  
  public void setLotr(Integer pLotr) {
    if (pLotr == null) {
      return;
    }
    lotr = BigDecimal.valueOf(pLotr);
  }
  
  public Integer getLotr() {
    return lotr.intValue();
  }
  
  public void setLotn(BigDecimal pLotn) {
    if (pLotn == null) {
      return;
    }
    lotn = pLotn.setScale(DECIMAL_LOTN, RoundingMode.HALF_UP);
  }
  
  public void setLotn(Integer pLotn) {
    if (pLotn == null) {
      return;
    }
    lotn = BigDecimal.valueOf(pLotn);
  }
  
  public Integer getLotn() {
    return lotn.intValue();
  }
  
  public void setLotc(BigDecimal pLotc) {
    if (pLotc == null) {
      return;
    }
    lotc = pLotc.setScale(DECIMAL_LOTC, RoundingMode.HALF_UP);
  }
  
  public void setLotc(Integer pLotc) {
    if (pLotc == null) {
      return;
    }
    lotc = BigDecimal.valueOf(pLotc);
  }
  
  public Integer getLotc() {
    return lotc.intValue();
  }
  
  public void setLott(BigDecimal pLott) {
    if (pLott == null) {
      return;
    }
    lott = pLott.setScale(DECIMAL_LOTT, RoundingMode.HALF_UP);
  }
  
  public void setLott(Integer pLott) {
    if (pLott == null) {
      return;
    }
    lott = BigDecimal.valueOf(pLott);
  }
  
  public Integer getLott() {
    return lott.intValue();
  }
  
  public void setLoval(BigDecimal pLoval) {
    if (pLoval == null) {
      return;
    }
    loval = pLoval.setScale(DECIMAL_LOVAL, RoundingMode.HALF_UP);
  }
  
  public void setLoval(Integer pLoval) {
    if (pLoval == null) {
      return;
    }
    loval = BigDecimal.valueOf(pLoval);
  }
  
  public Integer getLoval() {
    return loval.intValue();
  }
  
  public void setLotar(BigDecimal pLotar) {
    if (pLotar == null) {
      return;
    }
    lotar = pLotar.setScale(DECIMAL_LOTAR, RoundingMode.HALF_UP);
  }
  
  public void setLotar(Integer pLotar) {
    if (pLotar == null) {
      return;
    }
    lotar = BigDecimal.valueOf(pLotar);
  }
  
  public Integer getLotar() {
    return lotar.intValue();
  }
  
  public void setLocol(BigDecimal pLocol) {
    if (pLocol == null) {
      return;
    }
    locol = pLocol.setScale(DECIMAL_LOCOL, RoundingMode.HALF_UP);
  }
  
  public void setLocol(Integer pLocol) {
    if (pLocol == null) {
      return;
    }
    locol = BigDecimal.valueOf(pLocol);
  }
  
  public Integer getLocol() {
    return locol.intValue();
  }
  
  public void setLotpf(BigDecimal pLotpf) {
    if (pLotpf == null) {
      return;
    }
    lotpf = pLotpf.setScale(DECIMAL_LOTPF, RoundingMode.HALF_UP);
  }
  
  public void setLotpf(Integer pLotpf) {
    if (pLotpf == null) {
      return;
    }
    lotpf = BigDecimal.valueOf(pLotpf);
  }
  
  public Integer getLotpf() {
    return lotpf.intValue();
  }
  
  public void setLodcv(BigDecimal pLodcv) {
    if (pLodcv == null) {
      return;
    }
    lodcv = pLodcv.setScale(DECIMAL_LODCV, RoundingMode.HALF_UP);
  }
  
  public void setLodcv(Integer pLodcv) {
    if (pLodcv == null) {
      return;
    }
    lodcv = BigDecimal.valueOf(pLodcv);
  }
  
  public Integer getLodcv() {
    return lodcv.intValue();
  }
  
  public void setLotnc(BigDecimal pLotnc) {
    if (pLotnc == null) {
      return;
    }
    lotnc = pLotnc.setScale(DECIMAL_LOTNC, RoundingMode.HALF_UP);
  }
  
  public void setLotnc(Integer pLotnc) {
    if (pLotnc == null) {
      return;
    }
    lotnc = BigDecimal.valueOf(pLotnc);
  }
  
  public Integer getLotnc() {
    return lotnc.intValue();
  }
  
  public void setLosgn(BigDecimal pLosgn) {
    if (pLosgn == null) {
      return;
    }
    losgn = pLosgn.setScale(DECIMAL_LOSGN, RoundingMode.HALF_UP);
  }
  
  public void setLosgn(Integer pLosgn) {
    if (pLosgn == null) {
      return;
    }
    losgn = BigDecimal.valueOf(pLosgn);
  }
  
  public Integer getLosgn() {
    return losgn.intValue();
  }
  
  public void setLoser(BigDecimal pLoser) {
    if (pLoser == null) {
      return;
    }
    loser = pLoser.setScale(DECIMAL_LOSER, RoundingMode.HALF_UP);
  }
  
  public void setLoser(Integer pLoser) {
    if (pLoser == null) {
      return;
    }
    loser = BigDecimal.valueOf(pLoser);
  }
  
  public Integer getLoser() {
    return loser.intValue();
  }
  
  public void setLoqte(BigDecimal pLoqte) {
    if (pLoqte == null) {
      return;
    }
    loqte = pLoqte.setScale(DECIMAL_LOQTE, RoundingMode.HALF_UP);
  }
  
  public void setLoqte(Double pLoqte) {
    if (pLoqte == null) {
      return;
    }
    loqte = BigDecimal.valueOf(pLoqte).setScale(DECIMAL_LOQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqte() {
    return loqte.setScale(DECIMAL_LOQTE, RoundingMode.HALF_UP);
  }
  
  public void setLoksv(BigDecimal pLoksv) {
    if (pLoksv == null) {
      return;
    }
    loksv = pLoksv.setScale(DECIMAL_LOKSV, RoundingMode.HALF_UP);
  }
  
  public void setLoksv(Double pLoksv) {
    if (pLoksv == null) {
      return;
    }
    loksv = BigDecimal.valueOf(pLoksv).setScale(DECIMAL_LOKSV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoksv() {
    return loksv.setScale(DECIMAL_LOKSV, RoundingMode.HALF_UP);
  }
  
  public void setLopvb(BigDecimal pLopvb) {
    if (pLopvb == null) {
      return;
    }
    lopvb = pLopvb.setScale(DECIMAL_LOPVB, RoundingMode.HALF_UP);
  }
  
  public void setLopvb(Double pLopvb) {
    if (pLopvb == null) {
      return;
    }
    lopvb = BigDecimal.valueOf(pLopvb).setScale(DECIMAL_LOPVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopvb() {
    return lopvb.setScale(DECIMAL_LOPVB, RoundingMode.HALF_UP);
  }
  
  public void setLorem1(BigDecimal pLorem1) {
    if (pLorem1 == null) {
      return;
    }
    lorem1 = pLorem1.setScale(DECIMAL_LOREM1, RoundingMode.HALF_UP);
  }
  
  public void setLorem1(Double pLorem1) {
    if (pLorem1 == null) {
      return;
    }
    lorem1 = BigDecimal.valueOf(pLorem1).setScale(DECIMAL_LOREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem1() {
    return lorem1.setScale(DECIMAL_LOREM1, RoundingMode.HALF_UP);
  }
  
  public void setLorem2(BigDecimal pLorem2) {
    if (pLorem2 == null) {
      return;
    }
    lorem2 = pLorem2.setScale(DECIMAL_LOREM2, RoundingMode.HALF_UP);
  }
  
  public void setLorem2(Double pLorem2) {
    if (pLorem2 == null) {
      return;
    }
    lorem2 = BigDecimal.valueOf(pLorem2).setScale(DECIMAL_LOREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem2() {
    return lorem2.setScale(DECIMAL_LOREM2, RoundingMode.HALF_UP);
  }
  
  public void setLorem3(BigDecimal pLorem3) {
    if (pLorem3 == null) {
      return;
    }
    lorem3 = pLorem3.setScale(DECIMAL_LOREM3, RoundingMode.HALF_UP);
  }
  
  public void setLorem3(Double pLorem3) {
    if (pLorem3 == null) {
      return;
    }
    lorem3 = BigDecimal.valueOf(pLorem3).setScale(DECIMAL_LOREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem3() {
    return lorem3.setScale(DECIMAL_LOREM3, RoundingMode.HALF_UP);
  }
  
  public void setLorem4(BigDecimal pLorem4) {
    if (pLorem4 == null) {
      return;
    }
    lorem4 = pLorem4.setScale(DECIMAL_LOREM4, RoundingMode.HALF_UP);
  }
  
  public void setLorem4(Double pLorem4) {
    if (pLorem4 == null) {
      return;
    }
    lorem4 = BigDecimal.valueOf(pLorem4).setScale(DECIMAL_LOREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem4() {
    return lorem4.setScale(DECIMAL_LOREM4, RoundingMode.HALF_UP);
  }
  
  public void setLorem5(BigDecimal pLorem5) {
    if (pLorem5 == null) {
      return;
    }
    lorem5 = pLorem5.setScale(DECIMAL_LOREM5, RoundingMode.HALF_UP);
  }
  
  public void setLorem5(Double pLorem5) {
    if (pLorem5 == null) {
      return;
    }
    lorem5 = BigDecimal.valueOf(pLorem5).setScale(DECIMAL_LOREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem5() {
    return lorem5.setScale(DECIMAL_LOREM5, RoundingMode.HALF_UP);
  }
  
  public void setLorem6(BigDecimal pLorem6) {
    if (pLorem6 == null) {
      return;
    }
    lorem6 = pLorem6.setScale(DECIMAL_LOREM6, RoundingMode.HALF_UP);
  }
  
  public void setLorem6(Double pLorem6) {
    if (pLorem6 == null) {
      return;
    }
    lorem6 = BigDecimal.valueOf(pLorem6).setScale(DECIMAL_LOREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem6() {
    return lorem6.setScale(DECIMAL_LOREM6, RoundingMode.HALF_UP);
  }
  
  public void setLotrl(Character pLotrl) {
    if (pLotrl == null) {
      return;
    }
    lotrl = String.valueOf(pLotrl);
  }
  
  public Character getLotrl() {
    return lotrl.charAt(0);
  }
  
  public void setLobrl(Character pLobrl) {
    if (pLobrl == null) {
      return;
    }
    lobrl = String.valueOf(pLobrl);
  }
  
  public Character getLobrl() {
    return lobrl.charAt(0);
  }
  
  public void setLorp1(Character pLorp1) {
    if (pLorp1 == null) {
      return;
    }
    lorp1 = String.valueOf(pLorp1);
  }
  
  public Character getLorp1() {
    return lorp1.charAt(0);
  }
  
  public void setLorp2(Character pLorp2) {
    if (pLorp2 == null) {
      return;
    }
    lorp2 = String.valueOf(pLorp2);
  }
  
  public Character getLorp2() {
    return lorp2.charAt(0);
  }
  
  public void setLorp3(Character pLorp3) {
    if (pLorp3 == null) {
      return;
    }
    lorp3 = String.valueOf(pLorp3);
  }
  
  public Character getLorp3() {
    return lorp3.charAt(0);
  }
  
  public void setLorp4(Character pLorp4) {
    if (pLorp4 == null) {
      return;
    }
    lorp4 = String.valueOf(pLorp4);
  }
  
  public Character getLorp4() {
    return lorp4.charAt(0);
  }
  
  public void setLorp5(Character pLorp5) {
    if (pLorp5 == null) {
      return;
    }
    lorp5 = String.valueOf(pLorp5);
  }
  
  public Character getLorp5() {
    return lorp5.charAt(0);
  }
  
  public void setLorp6(Character pLorp6) {
    if (pLorp6 == null) {
      return;
    }
    lorp6 = String.valueOf(pLorp6);
  }
  
  public Character getLorp6() {
    return lorp6.charAt(0);
  }
  
  public void setLopvn(BigDecimal pLopvn) {
    if (pLopvn == null) {
      return;
    }
    lopvn = pLopvn.setScale(DECIMAL_LOPVN, RoundingMode.HALF_UP);
  }
  
  public void setLopvn(Double pLopvn) {
    if (pLopvn == null) {
      return;
    }
    lopvn = BigDecimal.valueOf(pLopvn).setScale(DECIMAL_LOPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopvn() {
    return lopvn.setScale(DECIMAL_LOPVN, RoundingMode.HALF_UP);
  }
  
  public void setLopvc(BigDecimal pLopvc) {
    if (pLopvc == null) {
      return;
    }
    lopvc = pLopvc.setScale(DECIMAL_LOPVC, RoundingMode.HALF_UP);
  }
  
  public void setLopvc(Double pLopvc) {
    if (pLopvc == null) {
      return;
    }
    lopvc = BigDecimal.valueOf(pLopvc).setScale(DECIMAL_LOPVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopvc() {
    return lopvc.setScale(DECIMAL_LOPVC, RoundingMode.HALF_UP);
  }
  
  public void setLomht(BigDecimal pLomht) {
    if (pLomht == null) {
      return;
    }
    lomht = pLomht.setScale(DECIMAL_LOMHT, RoundingMode.HALF_UP);
  }
  
  public void setLomht(Double pLomht) {
    if (pLomht == null) {
      return;
    }
    lomht = BigDecimal.valueOf(pLomht).setScale(DECIMAL_LOMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomht() {
    return lomht.setScale(DECIMAL_LOMHT, RoundingMode.HALF_UP);
  }
  
  public void setLoprp(BigDecimal pLoprp) {
    if (pLoprp == null) {
      return;
    }
    loprp = pLoprp.setScale(DECIMAL_LOPRP, RoundingMode.HALF_UP);
  }
  
  public void setLoprp(Double pLoprp) {
    if (pLoprp == null) {
      return;
    }
    loprp = BigDecimal.valueOf(pLoprp).setScale(DECIMAL_LOPRP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoprp() {
    return loprp.setScale(DECIMAL_LOPRP, RoundingMode.HALF_UP);
  }
  
  public void setLocoe(BigDecimal pLocoe) {
    if (pLocoe == null) {
      return;
    }
    locoe = pLocoe.setScale(DECIMAL_LOCOE, RoundingMode.HALF_UP);
  }
  
  public void setLocoe(Double pLocoe) {
    if (pLocoe == null) {
      return;
    }
    locoe = BigDecimal.valueOf(pLocoe).setScale(DECIMAL_LOCOE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLocoe() {
    return locoe.setScale(DECIMAL_LOCOE, RoundingMode.HALF_UP);
  }
  
  public void setLoprv(BigDecimal pLoprv) {
    if (pLoprv == null) {
      return;
    }
    loprv = pLoprv.setScale(DECIMAL_LOPRV, RoundingMode.HALF_UP);
  }
  
  public void setLoprv(Double pLoprv) {
    if (pLoprv == null) {
      return;
    }
    loprv = BigDecimal.valueOf(pLoprv).setScale(DECIMAL_LOPRV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoprv() {
    return loprv.setScale(DECIMAL_LOPRV, RoundingMode.HALF_UP);
  }
  
  public void setLoavr(Character pLoavr) {
    if (pLoavr == null) {
      return;
    }
    loavr = String.valueOf(pLoavr);
  }
  
  public Character getLoavr() {
    return loavr.charAt(0);
  }
  
  public void setLounv(String pLounv) {
    if (pLounv == null) {
      return;
    }
    lounv = pLounv;
  }
  
  public String getLounv() {
    return lounv;
  }
  
  public void setLoart(String pLoart) {
    if (pLoart == null) {
      return;
    }
    loart = pLoart;
  }
  
  public String getLoart() {
    return loart;
  }
  
  public void setLocpl(String pLocpl) {
    if (pLocpl == null) {
      return;
    }
    locpl = pLocpl;
  }
  
  public String getLocpl() {
    return locpl;
  }
  
  public void setLocnd(BigDecimal pLocnd) {
    if (pLocnd == null) {
      return;
    }
    locnd = pLocnd.setScale(DECIMAL_LOCND, RoundingMode.HALF_UP);
  }
  
  public void setLocnd(Double pLocnd) {
    if (pLocnd == null) {
      return;
    }
    locnd = BigDecimal.valueOf(pLocnd).setScale(DECIMAL_LOCND, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLocnd() {
    return locnd.setScale(DECIMAL_LOCND, RoundingMode.HALF_UP);
  }
  
  public void setLoin1(Character pLoin1) {
    if (pLoin1 == null) {
      return;
    }
    loin1 = String.valueOf(pLoin1);
  }
  
  public Character getLoin1() {
    return loin1.charAt(0);
  }
  
  public void setLoprt(BigDecimal pLoprt) {
    if (pLoprt == null) {
      return;
    }
    loprt = pLoprt.setScale(DECIMAL_LOPRT, RoundingMode.HALF_UP);
  }
  
  public void setLoprt(Double pLoprt) {
    if (pLoprt == null) {
      return;
    }
    loprt = BigDecimal.valueOf(pLoprt).setScale(DECIMAL_LOPRT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoprt() {
    return loprt.setScale(DECIMAL_LOPRT, RoundingMode.HALF_UP);
  }
  
  public void setLodlp(BigDecimal pLodlp) {
    if (pLodlp == null) {
      return;
    }
    lodlp = pLodlp.setScale(DECIMAL_LODLP, RoundingMode.HALF_UP);
  }
  
  public void setLodlp(Integer pLodlp) {
    if (pLodlp == null) {
      return;
    }
    lodlp = BigDecimal.valueOf(pLodlp);
  }
  
  public void setLodlp(Date pLodlp) {
    if (pLodlp == null) {
      return;
    }
    lodlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pLodlp));
  }
  
  public Integer getLodlp() {
    return lodlp.intValue();
  }
  
  public Date getLodlpConvertiEnDate() {
    return ConvertDate.db2ToDate(lodlp.intValue(), null);
  }
  
  public void setLoin2(Character pLoin2) {
    if (pLoin2 == null) {
      return;
    }
    loin2 = String.valueOf(pLoin2);
  }
  
  public Character getLoin2() {
    return loin2.charAt(0);
  }
  
  public void setLoin3(Character pLoin3) {
    if (pLoin3 == null) {
      return;
    }
    loin3 = String.valueOf(pLoin3);
  }
  
  public Character getLoin3() {
    return loin3.charAt(0);
  }
  
  public void setLotp1(String pLotp1) {
    if (pLotp1 == null) {
      return;
    }
    lotp1 = pLotp1;
  }
  
  public String getLotp1() {
    return lotp1;
  }
  
  public void setLotp2(String pLotp2) {
    if (pLotp2 == null) {
      return;
    }
    lotp2 = pLotp2;
  }
  
  public String getLotp2() {
    return lotp2;
  }
  
  public void setLotp3(String pLotp3) {
    if (pLotp3 == null) {
      return;
    }
    lotp3 = pLotp3;
  }
  
  public String getLotp3() {
    return lotp3;
  }
  
  public void setLotp4(String pLotp4) {
    if (pLotp4 == null) {
      return;
    }
    lotp4 = pLotp4;
  }
  
  public String getLotp4() {
    return lotp4;
  }
  
  public void setLotp5(String pLotp5) {
    if (pLotp5 == null) {
      return;
    }
    lotp5 = pLotp5;
  }
  
  public String getLotp5() {
    return lotp5;
  }
  
  public void setLogba(BigDecimal pLogba) {
    if (pLogba == null) {
      return;
    }
    logba = pLogba.setScale(DECIMAL_LOGBA, RoundingMode.HALF_UP);
  }
  
  public void setLogba(Integer pLogba) {
    if (pLogba == null) {
      return;
    }
    logba = BigDecimal.valueOf(pLogba);
  }
  
  public Integer getLogba() {
    return logba.intValue();
  }
  
  public void setLoarts(String pLoarts) {
    if (pLoarts == null) {
      return;
    }
    loarts = pLoarts;
  }
  
  public String getLoarts() {
    return loarts;
  }
  
  public void setLoin4(Character pLoin4) {
    if (pLoin4 == null) {
      return;
    }
    loin4 = String.valueOf(pLoin4);
  }
  
  public Character getLoin4() {
    return loin4.charAt(0);
  }
  
  public void setLoin5(Character pLoin5) {
    if (pLoin5 == null) {
      return;
    }
    loin5 = String.valueOf(pLoin5);
  }
  
  public Character getLoin5() {
    return loin5.charAt(0);
  }
  
  public void setLoin6(Character pLoin6) {
    if (pLoin6 == null) {
      return;
    }
    loin6 = String.valueOf(pLoin6);
  }
  
  public Character getLoin6() {
    return loin6.charAt(0);
  }
  
  public void setLoin7(Character pLoin7) {
    if (pLoin7 == null) {
      return;
    }
    loin7 = String.valueOf(pLoin7);
  }
  
  public Character getLoin7() {
    return loin7.charAt(0);
  }
  
  public void setLoqtl(BigDecimal pLoqtl) {
    if (pLoqtl == null) {
      return;
    }
    loqtl = pLoqtl.setScale(DECIMAL_LOQTL, RoundingMode.HALF_UP);
  }
  
  public void setLoqtl(Double pLoqtl) {
    if (pLoqtl == null) {
      return;
    }
    loqtl = BigDecimal.valueOf(pLoqtl).setScale(DECIMAL_LOQTL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtl() {
    return loqtl.setScale(DECIMAL_LOQTL, RoundingMode.HALF_UP);
  }
  
  public void setLomag(String pLomag) {
    if (pLomag == null) {
      return;
    }
    lomag = pLomag;
  }
  
  public String getLomag() {
    return lomag;
  }
  
  public void setLorep(String pLorep) {
    if (pLorep == null) {
      return;
    }
    lorep = pLorep;
  }
  
  public String getLorep() {
    return lorep;
  }
  
  public void setLoin8(Character pLoin8) {
    if (pLoin8 == null) {
      return;
    }
    loin8 = String.valueOf(pLoin8);
  }
  
  public Character getLoin8() {
    return loin8.charAt(0);
  }
  
  public void setLoin9(Character pLoin9) {
    if (pLoin9 == null) {
      return;
    }
    loin9 = String.valueOf(pLoin9);
  }
  
  public Character getLoin9() {
    return loin9.charAt(0);
  }
  
  public void setLoin10(Character pLoin10) {
    if (pLoin10 == null) {
      return;
    }
    loin10 = String.valueOf(pLoin10);
  }
  
  public Character getLoin10() {
    return loin10.charAt(0);
  }
  
  public void setLoin11(Character pLoin11) {
    if (pLoin11 == null) {
      return;
    }
    loin11 = String.valueOf(pLoin11);
  }
  
  public Character getLoin11() {
    return loin11.charAt(0);
  }
  
  public void setLoin12(Character pLoin12) {
    if (pLoin12 == null) {
      return;
    }
    loin12 = String.valueOf(pLoin12);
  }
  
  public Character getLoin12() {
    return loin12.charAt(0);
  }
  
  public void setLonli0(BigDecimal pLonli0) {
    if (pLonli0 == null) {
      return;
    }
    lonli0 = pLonli0.setScale(DECIMAL_LONLI0, RoundingMode.HALF_UP);
  }
  
  public void setLonli0(Integer pLonli0) {
    if (pLonli0 == null) {
      return;
    }
    lonli0 = BigDecimal.valueOf(pLonli0);
  }
  
  public Integer getLonli0() {
    return lonli0.intValue();
  }
  
  public void setLopra(BigDecimal pLopra) {
    if (pLopra == null) {
      return;
    }
    lopra = pLopra.setScale(DECIMAL_LOPRA, RoundingMode.HALF_UP);
  }
  
  public void setLopra(Double pLopra) {
    if (pLopra == null) {
      return;
    }
    lopra = BigDecimal.valueOf(pLopra).setScale(DECIMAL_LOPRA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopra() {
    return lopra.setScale(DECIMAL_LOPRA, RoundingMode.HALF_UP);
  }
  
  public void setLopva(BigDecimal pLopva) {
    if (pLopva == null) {
      return;
    }
    lopva = pLopva.setScale(DECIMAL_LOPVA, RoundingMode.HALF_UP);
  }
  
  public void setLopva(Double pLopva) {
    if (pLopva == null) {
      return;
    }
    lopva = BigDecimal.valueOf(pLopva).setScale(DECIMAL_LOPVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopva() {
    return lopva.setScale(DECIMAL_LOPVA, RoundingMode.HALF_UP);
  }
  
  public void setLoqtp(BigDecimal pLoqtp) {
    if (pLoqtp == null) {
      return;
    }
    loqtp = pLoqtp.setScale(DECIMAL_LOQTP, RoundingMode.HALF_UP);
  }
  
  public void setLoqtp(Double pLoqtp) {
    if (pLoqtp == null) {
      return;
    }
    loqtp = BigDecimal.valueOf(pLoqtp).setScale(DECIMAL_LOQTP, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtp() {
    return loqtp.setScale(DECIMAL_LOQTP, RoundingMode.HALF_UP);
  }
  
  public void setLomtr(BigDecimal pLomtr) {
    if (pLomtr == null) {
      return;
    }
    lomtr = pLomtr.setScale(DECIMAL_LOMTR, RoundingMode.HALF_UP);
  }
  
  public void setLomtr(Double pLomtr) {
    if (pLomtr == null) {
      return;
    }
    lomtr = BigDecimal.valueOf(pLomtr).setScale(DECIMAL_LOMTR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomtr() {
    return lomtr.setScale(DECIMAL_LOMTR, RoundingMode.HALF_UP);
  }
  
  public void setLoser3(Character pLoser3) {
    if (pLoser3 == null) {
      return;
    }
    loser3 = String.valueOf(pLoser3);
  }
  
  public Character getLoser3() {
    return loser3.charAt(0);
  }
  
  public void setLoser5(Character pLoser5) {
    if (pLoser5 == null) {
      return;
    }
    loser5 = String.valueOf(pLoser5);
  }
  
  public Character getLoser5() {
    return loser5.charAt(0);
  }
  
  public void setLoin17(Character pLoin17) {
    if (pLoin17 == null) {
      return;
    }
    loin17 = String.valueOf(pLoin17);
  }
  
  public Character getLoin17() {
    return loin17.charAt(0);
  }
  
  public void setLoin18(Character pLoin18) {
    if (pLoin18 == null) {
      return;
    }
    loin18 = String.valueOf(pLoin18);
  }
  
  public Character getLoin18() {
    return loin18.charAt(0);
  }
  
  public void setLoin19(Character pLoin19) {
    if (pLoin19 == null) {
      return;
    }
    loin19 = String.valueOf(pLoin19);
  }
  
  public Character getLoin19() {
    return loin19.charAt(0);
  }
  
  public void setLoin20(Character pLoin20) {
    if (pLoin20 == null) {
      return;
    }
    loin20 = String.valueOf(pLoin20);
  }
  
  public Character getLoin20() {
    return loin20.charAt(0);
  }
  
  public void setLoin21(Character pLoin21) {
    if (pLoin21 == null) {
      return;
    }
    loin21 = String.valueOf(pLoin21);
  }
  
  public Character getLoin21() {
    return loin21.charAt(0);
  }
  
  public void setLoin22(Character pLoin22) {
    if (pLoin22 == null) {
      return;
    }
    loin22 = String.valueOf(pLoin22);
  }
  
  public Character getLoin22() {
    return loin22.charAt(0);
  }
  
  public void setLoqtei(BigDecimal pLoqtei) {
    if (pLoqtei == null) {
      return;
    }
    loqtei = pLoqtei.setScale(DECIMAL_LOQTEI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtei(Double pLoqtei) {
    if (pLoqtei == null) {
      return;
    }
    loqtei = BigDecimal.valueOf(pLoqtei).setScale(DECIMAL_LOQTEI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtei() {
    return loqtei.setScale(DECIMAL_LOQTEI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtli(BigDecimal pLoqtli) {
    if (pLoqtli == null) {
      return;
    }
    loqtli = pLoqtli.setScale(DECIMAL_LOQTLI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtli(Double pLoqtli) {
    if (pLoqtli == null) {
      return;
    }
    loqtli = BigDecimal.valueOf(pLoqtli).setScale(DECIMAL_LOQTLI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtli() {
    return loqtli.setScale(DECIMAL_LOQTLI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtpi(BigDecimal pLoqtpi) {
    if (pLoqtpi == null) {
      return;
    }
    loqtpi = pLoqtpi.setScale(DECIMAL_LOQTPI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtpi(Double pLoqtpi) {
    if (pLoqtpi == null) {
      return;
    }
    loqtpi = BigDecimal.valueOf(pLoqtpi).setScale(DECIMAL_LOQTPI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtpi() {
    return loqtpi.setScale(DECIMAL_LOQTPI, RoundingMode.HALF_UP);
  }
  
  public void setLomhti(BigDecimal pLomhti) {
    if (pLomhti == null) {
      return;
    }
    lomhti = pLomhti.setScale(DECIMAL_LOMHTI, RoundingMode.HALF_UP);
  }
  
  public void setLomhti(Double pLomhti) {
    if (pLomhti == null) {
      return;
    }
    lomhti = BigDecimal.valueOf(pLomhti).setScale(DECIMAL_LOMHTI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomhti() {
    return lomhti.setScale(DECIMAL_LOMHTI, RoundingMode.HALF_UP);
  }
  
  public void setA1unl(String pA1unl) {
    if (pA1unl == null) {
      return;
    }
    a1unl = pA1unl;
  }
  
  public String getA1unl() {
    return a1unl;
  }
  
  public void setWliglb(String pWliglb) {
    if (pWliglb == null) {
      return;
    }
    wliglb = pWliglb;
  }
  
  public String getWliglb() {
    return wliglb;
  }
  
  public void setWpbttc(BigDecimal pWpbttc) {
    if (pWpbttc == null) {
      return;
    }
    wpbttc = pWpbttc.setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpbttc(Double pWpbttc) {
    if (pWpbttc == null) {
      return;
    }
    wpbttc = BigDecimal.valueOf(pWpbttc).setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpbttc() {
    return wpbttc.setScale(DECIMAL_WPBTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpnttc(BigDecimal pWpnttc) {
    if (pWpnttc == null) {
      return;
    }
    wpnttc = pWpnttc.setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpnttc(Double pWpnttc) {
    if (pWpnttc == null) {
      return;
    }
    wpnttc = BigDecimal.valueOf(pWpnttc).setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpnttc() {
    return wpnttc.setScale(DECIMAL_WPNTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(BigDecimal pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = pWpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpvttc(Double pWpvttc) {
    if (pWpvttc == null) {
      return;
    }
    wpvttc = BigDecimal.valueOf(pWpvttc).setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvttc() {
    return wpvttc.setScale(DECIMAL_WPVTTC, RoundingMode.HALF_UP);
  }
  
  public void setWmtttc(BigDecimal pWmtttc) {
    if (pWmtttc == null) {
      return;
    }
    wmtttc = pWmtttc.setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public void setWmtttc(Double pWmtttc) {
    if (pWmtttc == null) {
      return;
    }
    wmtttc = BigDecimal.valueOf(pWmtttc).setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmtttc() {
    return wmtttc.setScale(DECIMAL_WMTTTC, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ht(BigDecimal pWpt1ht) {
    if (pWpt1ht == null) {
      return;
    }
    wpt1ht = pWpt1ht.setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ht(Double pWpt1ht) {
    if (pWpt1ht == null) {
      return;
    }
    wpt1ht = BigDecimal.valueOf(pWpt1ht).setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpt1ht() {
    return wpt1ht.setScale(DECIMAL_WPT1HT, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ttc(BigDecimal pWpt1ttc) {
    if (pWpt1ttc == null) {
      return;
    }
    wpt1ttc = pWpt1ttc.setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public void setWpt1ttc(Double pWpt1ttc) {
    if (pWpt1ttc == null) {
      return;
    }
    wpt1ttc = BigDecimal.valueOf(pWpt1ttc).setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpt1ttc() {
    return wpt1ttc.setScale(DECIMAL_WPT1TTC, RoundingMode.HALF_UP);
  }
  
  public void setWqteor(BigDecimal pWqteor) {
    if (pWqteor == null) {
      return;
    }
    wqteor = pWqteor.setScale(DECIMAL_WQTEOR, RoundingMode.HALF_UP);
  }
  
  public void setWqteor(Double pWqteor) {
    if (pWqteor == null) {
      return;
    }
    wqteor = BigDecimal.valueOf(pWqteor).setScale(DECIMAL_WQTEOR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqteor() {
    return wqteor.setScale(DECIMAL_WQTEOR, RoundingMode.HALF_UP);
  }
  
  public void setWqteex(BigDecimal pWqteex) {
    if (pWqteex == null) {
      return;
    }
    wqteex = pWqteex.setScale(DECIMAL_WQTEEX, RoundingMode.HALF_UP);
  }
  
  public void setWqteex(Double pWqteex) {
    if (pWqteex == null) {
      return;
    }
    wqteex = BigDecimal.valueOf(pWqteex).setScale(DECIMAL_WQTEEX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqteex() {
    return wqteex.setScale(DECIMAL_WQTEEX, RoundingMode.HALF_UP);
  }
  
  public void setL2qt1(BigDecimal pL2qt1) {
    if (pL2qt1 == null) {
      return;
    }
    l2qt1 = pL2qt1.setScale(DECIMAL_L2QT1, RoundingMode.HALF_UP);
  }
  
  public void setL2qt1(Double pL2qt1) {
    if (pL2qt1 == null) {
      return;
    }
    l2qt1 = BigDecimal.valueOf(pL2qt1).setScale(DECIMAL_L2QT1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL2qt1() {
    return l2qt1.setScale(DECIMAL_L2QT1, RoundingMode.HALF_UP);
  }
  
  public void setL2qt2(BigDecimal pL2qt2) {
    if (pL2qt2 == null) {
      return;
    }
    l2qt2 = pL2qt2.setScale(DECIMAL_L2QT2, RoundingMode.HALF_UP);
  }
  
  public void setL2qt2(Double pL2qt2) {
    if (pL2qt2 == null) {
      return;
    }
    l2qt2 = BigDecimal.valueOf(pL2qt2).setScale(DECIMAL_L2QT2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL2qt2() {
    return l2qt2.setScale(DECIMAL_L2QT2, RoundingMode.HALF_UP);
  }
  
  public void setL2qt3(BigDecimal pL2qt3) {
    if (pL2qt3 == null) {
      return;
    }
    l2qt3 = pL2qt3.setScale(DECIMAL_L2QT3, RoundingMode.HALF_UP);
  }
  
  public void setL2qt3(Double pL2qt3) {
    if (pL2qt3 == null) {
      return;
    }
    l2qt3 = BigDecimal.valueOf(pL2qt3).setScale(DECIMAL_L2QT3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL2qt3() {
    return l2qt3.setScale(DECIMAL_L2QT3, RoundingMode.HALF_UP);
  }
  
  public void setL2nbr(BigDecimal pL2nbr) {
    if (pL2nbr == null) {
      return;
    }
    l2nbr = pL2nbr.setScale(DECIMAL_L2NBR, RoundingMode.HALF_UP);
  }
  
  public void setL2nbr(Integer pL2nbr) {
    if (pL2nbr == null) {
      return;
    }
    l2nbr = BigDecimal.valueOf(pL2nbr);
  }
  
  public Integer getL2nbr() {
    return l2nbr.intValue();
  }
  
  public void setL12qt(BigDecimal pL12qt) {
    if (pL12qt == null) {
      return;
    }
    l12qt = pL12qt.setScale(DECIMAL_L12QT, RoundingMode.HALF_UP);
  }
  
  public void setL12qt(Double pL12qt) {
    if (pL12qt == null) {
      return;
    }
    l12qt = BigDecimal.valueOf(pL12qt).setScale(DECIMAL_L12QT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getL12qt() {
    return l12qt.setScale(DECIMAL_L12QT, RoundingMode.HALF_UP);
  }
  
  public void setL12uc(String pL12uc) {
    if (pL12uc == null) {
      return;
    }
    l12uc = pL12uc;
  }
  
  public String getL12uc() {
    return l12uc;
  }
  
  public void setWoripr(String pWoripr) {
    if (pWoripr == null) {
      return;
    }
    woripr = pWoripr;
  }
  
  public String getWoripr() {
    return woripr;
  }
  
  public void setWtautva(BigDecimal pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = pWtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtautva(Double pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = BigDecimal.valueOf(pWtautva).setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWtautva() {
    return wtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setLoprs(BigDecimal pLoprs) {
    if (pLoprs == null) {
      return;
    }
    loprs = pLoprs.setScale(DECIMAL_LOPRS, RoundingMode.HALF_UP);
  }
  
  public void setLoprs(Double pLoprs) {
    if (pLoprs == null) {
      return;
    }
    loprs = BigDecimal.valueOf(pLoprs).setScale(DECIMAL_LOPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoprs() {
    return loprs.setScale(DECIMAL_LOPRS, RoundingMode.HALF_UP);
  }
  
  public void setWqtefac(BigDecimal pWqtefac) {
    if (pWqtefac == null) {
      return;
    }
    wqtefac = pWqtefac.setScale(DECIMAL_WQTEFAC, RoundingMode.HALF_UP);
  }
  
  public void setWqtefac(Double pWqtefac) {
    if (pWqtefac == null) {
      return;
    }
    wqtefac = BigDecimal.valueOf(pWqtefac).setScale(DECIMAL_WQTEFAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqtefac() {
    return wqtefac.setScale(DECIMAL_WQTEFAC, RoundingMode.HALF_UP);
  }
  
  public void setWqteav(BigDecimal pWqteav) {
    if (pWqteav == null) {
      return;
    }
    wqteav = pWqteav.setScale(DECIMAL_WQTEAV, RoundingMode.HALF_UP);
  }
  
  public void setWqteav(Double pWqteav) {
    if (pWqteav == null) {
      return;
    }
    wqteav = BigDecimal.valueOf(pWqteav).setScale(DECIMAL_WQTEAV, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqteav() {
    return wqteav.setScale(DECIMAL_WQTEAV, RoundingMode.HALF_UP);
  }
  
  public void setWdat(BigDecimal pWdat) {
    if (pWdat == null) {
      return;
    }
    wdat = pWdat.setScale(DECIMAL_WDAT, RoundingMode.HALF_UP);
  }
  
  public void setWdat(Integer pWdat) {
    if (pWdat == null) {
      return;
    }
    wdat = BigDecimal.valueOf(pWdat);
  }
  
  public void setWdat(Date pWdat) {
    if (pWdat == null) {
      return;
    }
    wdat = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdat));
  }
  
  public Integer getWdat() {
    return wdat.intValue();
  }
  
  public Date getWdatConvertiEnDate() {
    return ConvertDate.db2ToDate(wdat.intValue(), null);
  }
  
  public void setWeta(BigDecimal pWeta) {
    if (pWeta == null) {
      return;
    }
    weta = pWeta.setScale(DECIMAL_WETA, RoundingMode.HALF_UP);
  }
  
  public void setWeta(Integer pWeta) {
    if (pWeta == null) {
      return;
    }
    weta = BigDecimal.valueOf(pWeta);
  }
  
  public Integer getWeta() {
    return weta.intValue();
  }
  
  public void setWdatcde(BigDecimal pWdatcde) {
    if (pWdatcde == null) {
      return;
    }
    wdatcde = pWdatcde.setScale(DECIMAL_WDATCDE, RoundingMode.HALF_UP);
  }
  
  public void setWdatcde(Integer pWdatcde) {
    if (pWdatcde == null) {
      return;
    }
    wdatcde = BigDecimal.valueOf(pWdatcde);
  }
  
  public void setWdatcde(Date pWdatcde) {
    if (pWdatcde == null) {
      return;
    }
    wdatcde = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdatcde));
  }
  
  public Integer getWdatcde() {
    return wdatcde.intValue();
  }
  
  public Date getWdatcdeConvertiEnDate() {
    return ConvertDate.db2ToDate(wdatcde.intValue(), null);
  }
  
  public void setWmag(String pWmag) {
    if (pWmag == null) {
      return;
    }
    wmag = pWmag;
  }
  
  public String getWmag() {
    return wmag;
  }
  
  public void setWncc(String pWncc) {
    if (pWncc == null) {
      return;
    }
    wncc = pWncc;
  }
  
  public String getWncc() {
    return wncc;
  }
  
  public void setWrep(String pWrep) {
    if (pWrep == null) {
      return;
    }
    wrep = pWrep;
  }
  
  public String getWrep() {
    return wrep;
  }
  
  public void setWrcc(String pWrcc) {
    if (pWrcc == null) {
      return;
    }
    wrcc = pWrcc;
  }
  
  public String getWrcc() {
    return wrcc;
  }
  
  public void setWnfa(BigDecimal pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = pWnfa.setScale(DECIMAL_WNFA, RoundingMode.HALF_UP);
  }
  
  public void setWnfa(Integer pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = BigDecimal.valueOf(pWnfa);
  }
  
  public void setWnfa(Date pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pWnfa));
  }
  
  public Integer getWnfa() {
    return wnfa.intValue();
  }
  
  public Date getWnfaConvertiEnDate() {
    return ConvertDate.db2ToDate(wnfa.intValue(), null);
  }
  
  public void setWmex(String pWmex) {
    if (pWmex == null) {
      return;
    }
    wmex = pWmex;
  }
  
  public String getWmex() {
    return wmex;
  }
  
  public void setWdus(Character pWdus) {
    if (pWdus == null) {
      return;
    }
    wdus = String.valueOf(pWdus);
  }
  
  public Character getWdus() {
    return wdus.charAt(0);
  }
}
