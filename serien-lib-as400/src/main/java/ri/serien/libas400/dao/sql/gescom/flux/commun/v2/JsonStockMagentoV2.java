/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Stock Série N au format Magento
 * Cette classe sert à l'intégration JAVA vers JSON
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonStockMagentoV2 extends JsonEntiteMagento {
  private ArrayList<JsonStockArticleV2> listeStocks = null;
  
  public JsonStockMagentoV2(FluxMagento record) {
    idFlux = record.getFLIDF();
    versionFlux = record.getFLVER();
    etb = record.getFLETB();
    
    listeStocks = new ArrayList<JsonStockArticleV2>();
  }
  
  /**
   * Permet de mettre à jour la liste des articles de ce stock Magento
   */
  public void mettreAJourLaListeArticles(List<GenericRecord> liste) {
    if (liste == null) {
      return;
    }
    
    if (liste.size() > 0) {
      for (int i = 0; i < liste.size(); i++) {
        if (liste.get(i).isPresentField("S1ART") && liste.get(i).isPresentField("STK")) {
          JsonStockArticleV2 article = new JsonStockArticleV2();
          article.setCodeArticle(liste.get(i).getField("S1ART").toString().trim());
          article.setStockDispo(liste.get(i).getField("STK").toString().trim());
          article.setStockAttendu(liste.get(i).getField("S1ATT").toString().trim());
          article.setDateReassort("");
          listeStocks.add(article);
        }
      }
    }
  }
  
  public ArrayList<JsonStockArticleV2> getListeStocks() {
    return listeStocks;
  }
  
  public void setListeStocks(ArrayList<JsonStockArticleV2> listeStocks) {
    this.listeStocks = listeStocks;
  }
  
}
