/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.database.field.FieldAlpha;
import ri.serien.libas400.database.field.FieldDecimal;
import ri.serien.libas400.system.ContexteProgrammeRPG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.MessageErreurException;

public class GestionCommandes {
  // Constantes
  private static final int LG_DATASTRUCT = 64;
  
  private static final int LG_TYPEBON = 1;
  private static final int LG_ETB = 3;
  private static final int LG_NUMBON = 6;
  private static final int LG_SUF = 1;
  
  // Variables spécifiques du programme RPG
  private char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', ' ', ' ' };
  private FieldAlpha typeBon = new FieldAlpha(LG_TYPEBON);
  private FieldAlpha etablissement = new FieldAlpha(LG_ETB);
  private FieldDecimal numeroBon = new FieldDecimal(LG_NUMBON, 0);
  private FieldDecimal suffixeBon = new FieldDecimal(new BigDecimal(0), LG_SUF, 0);
  
  // Variables
  private boolean init = false;
  private SystemeManager system = null;
  private ProgramParameter[] parameterList = new ProgramParameter[1];
  private AS400Text ppar1 = new AS400Text(LG_DATASTRUCT);
  private char lettre = 'W';
  private String curlib = null;
  private ContexteProgrammeRPG rpg = null;
  private StringBuilder sb = new StringBuilder();
  private Bibliotheque bibliothequeEnvironnement = null;
  
  /**
   * Constructeur
   * @param sys
   * @param let
   * @param curlib
   */
  public GestionCommandes(SystemeManager sys, char let, String curlib, Bibliotheque pBibliothequeEnvironnement) {
    if (pBibliothequeEnvironnement == null) {
      throw new MessageErreurException("La bibliothèque d'environnement est invalide.");
    }
    system = sys;
    setLettre(let);
    setCurlib(curlib);
    bibliothequeEnvironnement = pBibliothequeEnvironnement;
  }
  
  /**
   * Initialise l'environnment d'execution du programme RPG
   * @return
   */
  public boolean init() {
    if (init) {
      return true;
    }
    
    parameterList[0] = new ProgramParameter(LG_DATASTRUCT);
    rpg = new ContexteProgrammeRPG(system.getSystem(), curlib);
    init = rpg.initialiserLettreEnvironnement(lettre);
    if (!init) {
      return false;
    }
    
    rpg.ajouterBibliotheque(curlib, true);
    rpg.ajouterBibliotheque(bibliothequeEnvironnement.getNom(), false);
    rpg.ajouterBibliotheque(lettre + "EXPAS", false);
    rpg.ajouterBibliotheque(lettre + "GVMAS", false);
    init = rpg.ajouterBibliotheque(lettre + "GVMX", false);
    
    return init;
  }
  
  /**
   * Effectue la recherche du tarif
   * @param valEtb
   * @param valNumCli
   * @param valLivCli
   * @param valCodeArt
   * @param valQte
   * @return
   */
  public boolean execute(String valEtb, BigDecimal valNumBon, BigDecimal valSufBon) {
    setValueTypeBon("E");
    setValueEtablissement(valEtb);
    setValueNumeroBon(valNumBon);
    setValueSuffixeBon(valSufBon);
    
    return execute();
  }
  
  /**
   * Effectue la recherche du tarif
   * @return
   */
  private boolean execute() {
    if (rpg == null) {
      return false;
    }
    
    // Construction du paramètre AS400 PPAR1
    sb.setLength(0);
    sb.append(getIndicateurs()).append(typeBon.toFormattedString()).append(etablissement.toFormattedString())
        .append(numeroBon.toFormattedString()).append(suffixeBon.toFormattedString());
    try {
      parameterList[0].setInputData(ppar1.toBytes(sb.toString()));
      // Appel du programme RPG
      Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(lettre + "GVMAS"), EnumTypeBibliotheque.PROGRAMMES_SERIEN);
      return rpg.executerProgramme("BONDL0", bibliotheque, parameterList);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution d'un programme RPG.");
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Formate une variable de type String (avec cadrage à droite, notamment pour l'établissement)
   * @param valeur
   * @param lg
   * @return
   */
  private String formateVar(String valeur, int lg) {
    if (valeur == null) {
      return String.format("%" + lg + "." + lg + "s", "");
    }
    else {
      return String.format("%" + lg + "." + lg + "s", valeur);
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le lettre
   */
  public char getLettre() {
    return lettre;
  }
  
  /**
   * @param lettre le lettre à définir
   */
  public void setLettre(char lettre) {
    if (lettre != ' ') {
      this.lettre = lettre;
    }
  }
  
  /**
   * @return le curlib
   */
  public String getCurlib() {
    return curlib;
  }
  
  /**
   * @param curlib le curlib à définir
   */
  public void setCurlib(String curlib) {
    this.curlib = curlib;
  }
  
  /**
   * @return le indicateurs
   */
  public char[] getIndicateurs() {
    return indicateurs;
  }
  
  /**
   * @param indicateurs le indicateurs à définir
   */
  public void setIndicateurs(char[] indicateurs) {
    this.indicateurs = indicateurs;
  }
  
  public FieldAlpha getTypeBon() {
    return typeBon;
  }
  
  public void setValueTypeBon(String typeBon) {
    this.typeBon.setValue(typeBon);
  }
  
  /**
   * Retourne le champ Etablissement
   * @return le etablissement
   */
  public FieldAlpha getEtablissement() {
    return etablissement;
  }
  
  /**
   * Initialise la valeur de l'établissement
   * @param etablissement le etablissement à définir
   */
  public void setValueEtablissement(String etablissement) {
    this.etablissement.setValue(formateVar(etablissement, LG_ETB));
  }
  
  public FieldDecimal getNumeroBon() {
    return numeroBon;
  }
  
  public void setValueNumeroBon(BigDecimal numeroBon) {
    this.numeroBon.setValue(numeroBon);
  }
  
  public FieldDecimal getSuffixeBon() {
    return suffixeBon;
  }
  
  public void setValueSuffixeBon(BigDecimal suffixeBon) {
    this.suffixeBon.setValue(suffixeBon);
  }
  
}
