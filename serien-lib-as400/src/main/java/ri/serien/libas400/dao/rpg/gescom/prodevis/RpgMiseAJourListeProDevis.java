/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.prodevis;

import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;

public class RpgMiseAJourListeProDevis extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SGVM9GC4";
  
  /**
   * Appel du programme RPG qui va injecter un prodevis.
   */
  public void mettreAJourListeDocumentProDevis(SystemeManager pSysteme, IdEtablissement pIdEtablissement) {
    if (pSysteme == null) {
      throw new MessageErreurException("L'environnement du serveur n'est pas initialisé.");
    }
    if (pIdEtablissement == null || pIdEtablissement.getCodeEtablissement().isEmpty()) {
      throw new MessageErreurException("L'établissement n'est pas renseigné.");
    }
    
    // Préparation des paramètres
    ProgramParameter[] listeParametres = new ProgramParameter[1];
    AS400Text pfile = new AS400Text(IdEtablissement.LONGUEUR_CODE_ETABLISSEMENT, pSysteme.getSystem());
    listeParametres[0] =
        new ProgramParameter(pfile.toBytes(pIdEtablissement.getCodeEtablissement()), IdEtablissement.LONGUEUR_CODE_ETABLISSEMENT);
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), listeParametres)) {
      
      // erreur.setDSOutput();
      // Gestion des erreurs
      controlerErreur();
    }
  }
  
}
