/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx004.v3;

import java.util.ArrayList;

import com.google.gson.JsonSyntaxException;

import ri.serien.libas400.dao.gvm.programs.client.NumerotationClient;
import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.SqlFlux;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonAdresseClientFacturationWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonAdresseWebV3;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v3.JsonClientWebV3;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Version 3 des flux.
 * Cette classe permet d'importer un client d'un tiers vers Série N.
 */
public class ImportationClientWebV3 extends BaseGroupDB {
  // Constantes
  // Afficher les libellés articles sur les 2 premiers caractères
  private final String MODE_LIBS_ARTICLES = "**  ";
  private final String COMPTE_GENERAL_DEFAUT = "411000";
  
  // Variables
  private ManagerFluxMagento managerFluxMagento = null;
  private ParametresFluxBibli parametresBibli = null;
  private Integer numeroClientARenvoyerVersMagento = null;
  
  /**
   * Constructeur.
   */
  public ImportationClientWebV3(QueryManager pQueryManager, ParametresFluxBibli pParametreFlux, ManagerFluxMagento pManagerFluxMagento) {
    super(pQueryManager);
    parametresBibli = pParametreFlux;
    managerFluxMagento = pManagerFluxMagento;
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de vérifier le contenu du JSON pour le matcher avec un client Série N.
   */
  public boolean recupererUnClientMagento(FluxMagento pFlux) {
    if (pFlux == null) {
      return false;
    }
    
    if (pFlux.getFLJSN() == null || pFlux.getFLJSN().trim().length() == 0) {
      pFlux.construireMessageErreur("[ImportClientMagento] recupererUnClientMagento() etb ou json corrompus ");
      return false;
    }
    
    boolean retour = false;
    // Remise à null du numéro du client à renvoyer à Magento (ce sera déterminé lors de l'analyse du flux reçu)
    numeroClientARenvoyerVersMagento = null;
    
    // Debut de la construction du JSON
    if (initJSON()) {
      // Récupèration du client Magento
      JsonClientWebV3 jsonClientMagento = null;
      try {
        // Conversion de la chaine Json en objet
        jsonClientMagento = gson.fromJson(pFlux.getFLJSN(), JsonClientWebV3.class);
      }
      catch (JsonSyntaxException e) {
        pFlux.construireMessageErreur("[ImportClientMagento] recupererUnClientMagento() Objet json corrompu: " + e.getMessage());
        return false;
      }
      
      if (jsonClientMagento == null) {
        return false;
      }
      
      // Traitement du fichier json reçu
      jsonClientMagento.setEtb(pFlux.getFLETB());
      if (jsonClientMagento.getIdClientWeb() != null) {
        pFlux.setFLCOD(jsonClientMagento.getIdClientWeb());
      }
      // Mise à jour du client dans la bdd Série N
      retour = mettreAJourUnClient(jsonClientMagento, pFlux);
      if (retour) {
        retour = mettreAJourUnClientCompta(jsonClientMagento, pFlux);
      }
      
      // Renvoyer le flux client à Magento pour l'idClient car le flux reçu était une création client côté Magento
      if (numeroClientARenvoyerVersMagento != null) {
        // Contrôle si le numéro client Série N n'est pas invalide
        if (numeroClientARenvoyerVersMagento.intValue() <= 0) {
          Trace.erreur("Le flux " + EnumTypeFlux.CLIENT.getCode() + " reçu " + jsonClientMagento.getIdFlux()
              + " est un flux de création client envoyé par Magento mais il n'a pas pu être renvoyé à Magento pour le compléter car le"
              + " numéro client (idClient) est invalide.");
        }
        // Le numéro client est valide donc création de l'enregistrement de renvoi du flux vers Magento
        else {
          try {
            IdEtablissement idEtablissement = IdEtablissement.getInstance(pFlux.getFLETB());
            String codeMetier = String.format("%06d/000", numeroClientARenvoyerVersMagento);
            SqlFlux daoGestionFlux = new SqlFlux(queryManager);
            daoGestionFlux.ecrireDemandeEnvoiFluxVersMagento(EnumTypeFlux.CLIENT, idEtablissement, codeMetier);
          }
          catch (Exception e) {
            Trace.erreur("Erreur lors de la génération de la demande d'envoi du flux client.");
          }
        }
      }
    }
    
    return retour;
  }
  
  // -- Méthodes privées
  
  /**
   * Permet d'insérer ou de mettre à jour un client Série N avec les données importées.
   */
  private boolean mettreAJourUnClient(JsonClientWebV3 pJsonClientMagento, FluxMagento pFlux) {
    if (pJsonClientMagento == null || pFlux == null) {
      majError("[ImportClientMagento] mettreAJourUnClient() client à NULL");
      return false;
    }
    
    boolean retour = false;
    String champs = "";
    String valeurs = "";
    String requete = "";
    String nomREPAC = "";
    
    if (verifierLesDonneesRentrantes(pJsonClientMagento, pFlux)) {
      int nbClientsDB2 = verifierSonExistenceDB2(pJsonClientMagento, pFlux);
      // Dans PGVMCLIM CLTTC
      // CLTTC = ' ' est un client HT éditions TTC
      // CLTTC = '1' est un client TTC éditions TTC
      // CLTTC = '2' est un client HT éditions TTC
      String clientTTC = " ";
      if (pJsonClientMagento.getIsUnParticulier() == JsonClientWebV3.ISPARTICULIER) {
        clientTTC = "1";
      }
      
      // UPDATE EXISTANT
      if (nbClientsDB2 == 1) {
        // Catégorie client
        String majCAT = "";
        if (pJsonClientMagento.getCategorie() != null && !pJsonClientMagento.getCategorie().trim().isEmpty()) {
          majCAT = ", CLCAT = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCategorie());
        }
        // Tarif client
        String majTarif = "";
        if (pJsonClientMagento.getTarif() != null) {
          majTarif = ", CLTAR = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getTarif().toString());
        }
        // Mode de réglement client
        String majReglement = "";
        if (pJsonClientMagento.getCodeReglement() != null && !pJsonClientMagento.getCodeReglement().trim().isEmpty()) {
          majReglement = ", CLRGL = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCodeReglement());
        }
        // TVA Client
        String majTVAClient = "";
        if (pJsonClientMagento.getTvaClient() != null && !pJsonClientMagento.getTvaClient().trim().isEmpty()) {
          majTVAClient = ", CLTFA = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getTvaClient());
        }
        
        // @formatter:off
        requete = "UPDATE " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " SET"
            + " CLAPEN = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCodeAPE())
            + majCAT
            + majReglement
            + ", CLNOM = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLNOM(), true, 30)
            + ", CLCPL = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getComplementNomClient(), true, 30)
            + ", CLRUE = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLRUE(), true, 30)
            + ", CLLOC = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLLOC(), true, 30)
            + ", CLVIL = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getVilleClient(), true, 30)
            + ", CLCDP1 = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCdpClient())
            + ", CLCOP = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getPaysClient())
            + ", CLPAY = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getLibellePays(), true, 26)
            + ", CLTEL = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getTelClient())
            + ", CLFAX = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getFaxClient())
            + ", CLSRN = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLSRN())
            + ", CLSRT = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLSRT())
            + majTVAClient
            + ", CLTOP1 = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getMagClient())
            + ", CLTOP2 = "  + RequeteSql.formaterStringSQL(pJsonClientMagento.getSiteClient())
            // Gestion du TTC pour les clients particuliers
            + ", CLTTC = " + RequeteSql.formaterStringSQL(clientTTC)
            + majTarif
            + ", CLIN9 = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLIN9())
            + ", CLADH = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getIdClientWeb())
            + " WHERE CLETB = " + RequeteSql.formaterStringSQL(pJsonClientMagento.getEtb())
            + " AND CLCLI = " + pJsonClientMagento.getCLCLI()
            + " AND CLLIV = " + pJsonClientMagento.getCLLIV();
        // @formatter:on
        
        if (queryManager.requete(requete) > 0) {
          GenericRecord record = recupererContactduClientMagento(pJsonClientMagento);
          // Le contact existe et est cohérent
          // ON NE CREE PAS S'IL N'EXISTE PAS CAR UN CLIENT MAGENTO A AUTOMATIQUEMENT UN CONTACT ASSOCIE (mail)
          if (record != null) {
            if (record.isPresentField("RENUM")) {
              nomREPAC = traiterREPAC(pJsonClientMagento.getNomClient(), pJsonClientMagento.getPrenomClient());
              
              // Mise à jour du contact
              // Construction de la requête
              RequeteSql requeteSql = new RequeteSql();
              requeteSql.ajouter("update " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT + " set");
              requeteSql.ajouterValeurUpdate("RECIV", pJsonClientMagento.getCiviliteClient());
              requeteSql.ajouterValeurUpdate("REPAC", nomREPAC);
              requeteSql.ajouterValeurUpdate("RENOM", pJsonClientMagento.getNomClient());
              requeteSql.ajouterValeurUpdate("REPRE", pJsonClientMagento.getPrenomClient());
              requeteSql.ajouterValeurUpdate("RENET", pJsonClientMagento.getEmail());
              requeteSql.ajouter("where");
              requeteSql.ajouterConditionAnd("RENUM", "=", record.getIntegerValue("RENUM"));
              try {
                if (queryManager.requete(requeteSql.getRequete()) > 0) {
                  retour = gererActiviteWeb(pJsonClientMagento, pFlux);
                }
                else {
                  pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClient() UPDATE Contact en échec IDMagento : "
                      + pJsonClientMagento.getIdClientWeb() + " " + queryManager.getMsgError());
                }
              }
              catch (Exception e) {
                pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClient() Requête en erreur: " + requeteSql.getRequete());
              }
            }
            else {
              pFlux.construireMessageErreur(
                  "[ImportClientMagento] mettreAJourUnClient() RECUP RENUM EN ECHEC client : " + pJsonClientMagento.getIdClientWeb());
            }
          }
          else {
            pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClient() SELECT contact EN ECHEC client : "
                + pJsonClientMagento.getIdClientWeb() + " " + queryManager.getMsgError());
          }
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClient() UPDATE client " + pJsonClientMagento.getIdClientWeb()
              + " EN ERREUR " + queryManager.getMsgError());
        }
      }
      // CREATION
      else if (nbClientsDB2 == 0) {
        // Tarif
        Integer valeurInsertTarif = JsonClientWebV3.TARIF_DEFAUT;
        if (pJsonClientMagento.getTarif() != null) {
          valeurInsertTarif = pJsonClientMagento.getTarif();
        }
        String valeurCAT = "";
        if (pJsonClientMagento.getCategorie() != null && !pJsonClientMagento.getCategorie().trim().isEmpty()) {
          valeurCAT = RequeteSql.formaterStringSQL(pJsonClientMagento.getCategorie()) + ", ";
        }
        else {
          valeurCAT = RequeteSql.formaterStringSQL(JsonClientWebV3.CATEGORIE_DEFAUT) + ", ";
        }
        // Mode de réglement client
        if (pJsonClientMagento.getCodeReglement() == null || pJsonClientMagento.getCodeReglement().trim().isEmpty()) {
          pJsonClientMagento.setCodeReglement(parametresBibli.getReglementParticulierMg());
        }
        // TVA Client
        if (pJsonClientMagento.getTvaClient() == null || pJsonClientMagento.getTvaClient().trim().isEmpty()) {
          pJsonClientMagento.setTvaClient(parametresBibli.getModeFacturationClient());
        }
        
        int numeroAutoClient = NumerotationClient.numeroterAutomatiquement(pJsonClientMagento.getEtb(), queryManager);
        
        if (numeroAutoClient > 0) {
          champs = " CLCRE,CLETB,CLCLI,CLLIV,CLADH,CLAPEN,CLCAT,CLRGL,CLNOM,CLCPL,CLRUE,"
              + "CLLOC,CLVIL,CLCDP1,CLCOP,CLPAY,CLTEL,CLFAX,CLSRN,CLSRT,CLTFA,CLTTC,CLTAR,CLTOP1,CLTOP2,CLCLK,CLNLA,CLIN9 ";
          // @formatter:off
          valeurs = RequeteSql.formaterStringSQL(pJsonClientMagento.retournerDateDuJourSN()) + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getEtb())           + ", "
              + numeroAutoClient                                                      + ", "
              + "'000', "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getIdClientWeb()) + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCodeAPE())        + ", "
              + valeurCAT
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCodeReglement())  + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLNOM(), true, 30)    + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getComplementNomClient(), true, 30) + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLRUE(), true, 30)    + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLLOC(), true, 30)    + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getVilleClient(), true, 30)    + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCdpClient())      + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getPaysClient())     + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getLibellePays(), true, 26) + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getTelClient())      + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getFaxClient())      + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLSRN())          + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLSRT())          + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getTvaClient())      + ", "
              + RequeteSql.formaterStringSQL(clientTTC)                              + ", "
              + valeurInsertTarif                                                      + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getMagClient())      + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getSiteClient())     + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.retournerUneCleDeClassement(pJsonClientMagento.getCLNOM(), 15)) + ", "
              // CLNLA -> '****' le client se voit afficher les 4 libellés article dans la commande
              // On retire ce précédent paramètre pour mettre sur les 2 premiers libellés
              + RequeteSql.formaterStringSQL(MODE_LIBS_ARTICLES)                     + ", "
              + RequeteSql.formaterStringSQL(pJsonClientMagento.getCLIN9());
          // @formatter:on
          requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " (" + champs + ") VALUES ("
              + valeurs + ") ";
          
          // ETAPE 1 INSERTION CLIENT
          if (queryManager.requete(requete) > 0) {
            pJsonClientMagento.setCLCLI("" + numeroAutoClient);
            pJsonClientMagento.setCLLIV("000");
            
            // Il s'agit d'un nouveau client créé par Magento, il faut donc renvoyer à Magento le flux client afin de lui retourner
            // l'idClient. Cet envoi se fera en fin de traitement après la réception totale du fluc en cours
            numeroClientARenvoyerVersMagento = Constantes.convertirTexteEnInteger("" + numeroAutoClient);
            
            // Retire le contrôle sur le client Particulier pour la création d'un contact
            int numeroAutoContact = numerotationAutomatiqueContact(pFlux);
            // Si le numéro généré est cohérent
            if (numeroAutoContact > 0) {
              nomREPAC = traiterREPAC(pJsonClientMagento.getNomClient(), pJsonClientMagento.getPrenomClient());
              champs = " RENUM,RECIV,REPAC,RENOM,REPRE,RENET ";
              //@formatter:off
              valeurs = numeroAutoContact + ", "
                  + RequeteSql.formaterStringSQL(pJsonClientMagento.getCiviliteClient(), true) + ", "
                  + RequeteSql.formaterStringSQL(nomREPAC) + ", "
                  + RequeteSql.formaterStringSQL(pJsonClientMagento.getNomClient(), true)      + ", "
                  + RequeteSql.formaterStringSQL(pJsonClientMagento.getPrenomClient(), true)   + ", "
                  + RequeteSql.formaterStringSQL(pJsonClientMagento.getEmail(), true);
              //@formatter:on
              
              requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT + " (" + champs + ") VALUES ("
                  + valeurs + ")";
              if (queryManager.requete(requete) > 0) {
                champs = " RLCOD,RLETB,RLIND,RLNUMT,RLIN1 ";
                //@formatter:off
                valeurs = "'C'"
                    + ", " + RequeteSql.formaterStringSQL(pJsonClientMagento.getEtb())
                    + ", " + RequeteSql.formaterStringSQL(formaterLeClientPourRLIND(numeroAutoClient) + "000")
                    + ", " + numeroAutoContact
                    + ", 'P'";
                //@formatter:on
                requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT_LIEN + " (" + champs
                    + ") VALUES (" + valeurs + ")";
                if (queryManager.requete(requete) > 0) {
                  retour = insererUneNouvelleExtensionClient(pJsonClientMagento, pFlux);
                  // On renvoie un flux client vers Magento pour transmettre les IDS ERP
                  if (retour) {
                    renvoyerUnFluxClient(pJsonClientMagento);
                  }
                }
                else {
                  pFlux.construireMessageErreur("[mettreAJourUnClient] Liaison Contact n'est pas insérée correctement pour :"
                      + pJsonClientMagento.getIdClientWeb() + " /  " + queryManager.getMsgError() + " requete -> " + requete);
                }
              }
              else {
                pFlux.construireMessageErreur("[mettreAJourUnClient] Contact n'est pas inséré correctement pour :"
                    + pJsonClientMagento.getIdClientWeb() + " /  " + queryManager.getMsgError() + " requete -> " + requete);
              }
            }
          }
          else {
            pFlux.construireMessageErreur("[ImportClientMagento] INSERT Client EN ERREUR POUR :" + pJsonClientMagento.getIdClientWeb() + " /  "
                + queryManager.getMsgError() + " requete -> " + requete);
          }
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] Erreur de numérotation du client :" + pJsonClientMagento.getIdClientWeb() + " /  "
              + numeroAutoClient);
        }
      }
      else if (nbClientsDB2 < 0) {
        pFlux.construireMessageErreur("[ImportClientMagento] PB DE RECUP DU CLIENT :" + pJsonClientMagento.getIdClientWeb() + " /  "
            + queryManager.getMsgError() + " requete -> " + requete);
      }
      else {
        pFlux.construireMessageErreur("[ImportClientMagento] PLUSIEURS CLIENTS POUR CE CODE :" + pJsonClientMagento.getIdClientWeb() + " /  "
            + queryManager.getMsgError() + " requete -> " + requete);
      }
    }
    
    // Gestion du bloc adresse du client de facturation s'il est présent
    if (retour) {
      retour = mettreAJourClientFacturation(pJsonClientMagento, pFlux);
    }
    
    // Gestion des adresses de livraison du client en modifiant les clients livrés associés
    if (retour) {
      retour = mettreAJourClientsLivres(pJsonClientMagento, pFlux);
    }
    
    return retour;
  }
  
  /**
   * Import Client: gestion du bloc adresse du client de facturation.
   * Note: c'est le même principe que pour les clients livrés, le client de facturation n'est pas crée car nous ne transférons que les
   * données du bloc adresse. S'il faut le créer alors il faut envoyer un flux client dédié avec toutes les informations le concernant.
   */
  private boolean mettreAJourClientFacturation(JsonClientWebV3 pJsonClientMagento, FluxMagento pFlux) {
    if (pJsonClientMagento == null || pJsonClientMagento.getCLCLI() == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourClientFacturation() pJsonClientMagento ou identifiant client invalide");
      return false;
    }
    
    JsonAdresseClientFacturationWebV3 adresse = pJsonClientMagento.getAdresseClientFacturation();
    
    // Contrôle de la présence d'un identifiant d'un client de facturation
    if (adresse == null || adresse.getIdClientFacturation() == null || adresse.getIdClientFacturation().trim().isEmpty()) {
      return true;
    }
    
    boolean retour = false;
    if (verifierClientFacturation(pJsonClientMagento, adresse, pFlux)) {
      adresse.miseEnFormeBlocAdresseImport();
      
      // Interprétation des codes pays et libellé pays
      if (adresse.getPaysClientFacturation() == null || adresse.getPaysClientFacturation().trim().isEmpty()) {
        adresse.setPaysClientFacturation(JsonClientWebV3.codePaysDefaut);
        adresse.setLibellePaysClientFacturation(JsonClientWebV3.libPaysDefaut);
      }
      else {
        if (adresse.getLibellePaysClientFacturation() == null || adresse.getLibellePaysClientFacturation().trim().isEmpty()) {
          adresse.setLibellePaysClientFacturation(recupererLibellePaysSerieN(adresse.getPaysClientFacturation()));
        }
      }
      
      // Construction de la requête
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.ajouter("update " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " set");
      // Ajout des valeurs
      requeteSql.ajouterValeurUpdate("CLNOM", adresse.getCLNOM());
      requeteSql.ajouterValeurUpdate("CLCPL", adresse.getComplementNomClientFacturation());
      requeteSql.ajouterValeurUpdate("CLRUE", adresse.getCLRUE());
      requeteSql.ajouterValeurUpdate("CLLOC", adresse.getCLLOC());
      requeteSql.ajouterValeurUpdate("CLVIL", adresse.getCLVIL());
      requeteSql.ajouterValeurUpdate("CLCDP1", adresse.getCLCDP1());
      requeteSql.ajouterValeurUpdate("CLCOP", adresse.getPaysClientFacturation());
      requeteSql.ajouterValeurUpdate("CLPAY", adresse.getLibellePaysClientFacturation());
      if (adresse.getIdClientFacturationWeb() != null) {
        requeteSql.ajouterValeurUpdate("CLADH", adresse.getIdClientFacturationWeb());
      }
      requeteSql.ajouterValeurUpdate("CLTEL", adresse.getCLTEL());
      // Ajout des conditions
      requeteSql.ajouter("where");
      requeteSql.ajouterConditionAnd("CLETB", "=", pJsonClientMagento.getEtb());
      requeteSql.ajouterConditionAnd("CLCLI", "=", adresse.getCLCLI());
      requeteSql.ajouterConditionAnd("CLLIV", "=", adresse.getCLLIV());
      
      // Mise à jour du client de facturation
      try {
        if (queryManager.requete(requeteSql.getRequete()) > 0) {
          retour = majContactAdresseClientFacturation(pJsonClientMagento, adresse, pFlux);
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourClientFacturation() problème UPDATE: " + queryManager.getMsgError());
          retour = false;
        }
      }
      catch (Exception e) {
        pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourClientFacturation() requête en erreur: " + requeteSql.getRequete());
        retour = false;
      }
    }
    
    return retour;
  }
  
  /**
   * Vérification de l'identifiant Série N du client de facturation.
   */
  private boolean verifierClientFacturation(JsonClientWebV3 pJsonClientMagento, JsonAdresseClientFacturationWebV3 pAdresse,
      FluxMagento pFlux) {
    if (pJsonClientMagento == null || pJsonClientMagento.getCLCLI() == null || pJsonClientMagento.getEtb() == null) {
      majError("[ImportClientMagento] verifierClientFacturation() Le fichier JSON est corrompu: " + pJsonClientMagento);
      return false;
    }
    if (pAdresse == null || pAdresse.getIdClientFacturation() == null || pAdresse.getIdClientFacturation().trim().isEmpty()) {
      majError("[ImportClientMagento] verifierClientFacturation() IdClientFacture est invalide");
      return false;
    }
    if (pAdresse.decouperCodeERP()) {
      if (pJsonClientMagento.getCLCLI().equals(pAdresse.getCLCLI())) {
        majError("[ImportClientMagento] verifierClientFacturation() Les codes clients sont identiques: " + pJsonClientMagento.getCLCLI()
            + " - " + pAdresse.getCLCLI());
        return false;
      }
      
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.ajouter("select CLCLI, CLLIV from " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " " + " where ");
      requeteSql.ajouterConditionAnd("CLETB", "=", pJsonClientMagento.getEtb());
      requeteSql.ajouterConditionAnd("CLCLI", "=", pAdresse.getCLCLI());
      requeteSql.ajouterConditionAnd("CLLIV", "=", pAdresse.getCLLIV());
      
      ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
      // Notre client existe bien dans Série N -> récupèration des données
      if (liste != null && liste.size() == 1) {
        return true;
      }
      else {
        pFlux.construireMessageErreur("[ImportClientMagento] verifierClientFacturation() L'adresse du client de facturation n'est pas valide: "
            + pAdresse.getCLCLI() + " - " + pAdresse.getCLLIV());
        return false;
      }
      
    }
    else {
      pFlux.construireMessageErreur(
          "[ImportClientMagento] decouperCodeERP() Identifiant du client de facturation corrompu: " + pAdresse.getIdClientFacturation());
      return false;
    }
  }
  
  /**
   * Mettre à jour le contact principal d'un client lorsqu'il s'agit d'une adresse d'un client de facturation Magento.
   */
  private boolean majContactAdresseClientFacturation(JsonClientWebV3 pJsonClientMagento, JsonAdresseClientFacturationWebV3 pAdresse,
      FluxMagento pFlux) {
    GenericRecord record = recupererContactdeAdresseClientFacturationMagento(pJsonClientMagento, pAdresse, pFlux);
    // Le contact existe et est cohérent
    // ON NE CREE PAS S'IL N'EXISTE PAS CAR UN CLIENT MAGENTO AUTOMATIQUEMENT UN CONTACT ASSOCIE (mail)
    if (record == null) {
      return false;
    }
    
    if (!record.isPresentField("RENUM")) {
      pFlux.construireMessageErreur("[ImportClientMagento] majContactAdresseClientFacturation() RENUM à NULL ");
      return false;
    }
    
    String nomREPAC = traiterREPAC(OutilsMagento.nettoyerSaisiesSQL(pAdresse.getNomClientFacturation()),
        OutilsMagento.nettoyerSaisiesSQL(pAdresse.getPrenomClientFacturation()));
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("update " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT + " set");
    requeteSql.ajouterValeurUpdate("RECIV", pAdresse.getCiviliteClientFacturation());
    requeteSql.ajouterValeurUpdate("REPAC", nomREPAC);
    requeteSql.ajouterValeurUpdate("RENOM", pAdresse.getNomClientFacturation());
    requeteSql.ajouterValeurUpdate("REPRE", pAdresse.getPrenomClientFacturation());
    requeteSql.ajouter("where");
    requeteSql.ajouterConditionAnd("RENUM", "=", record.getIntegerValue("RENUM"));
    
    // Mise à jour du contact
    try {
      if (queryManager.requete(requeteSql.getRequete()) > 0) {
        return true;
      }
    }
    catch (Exception e) {
      pFlux.construireMessageErreur("[ImportClientMagento] majContactAdresseClientFacturation() Requête en erreur: " + requeteSql.getRequete());
      return false;
    }
    
    pFlux.construireMessageErreur(
        "[ImportClientMagento] majContactAdresseClientFacturation() Maj du contact en Erreur: " + queryManager.getMsgError());
    return false;
  }
  
  /**
   * Récupèration du contact principal du client de facturation Magento.
   */
  private GenericRecord recupererContactdeAdresseClientFacturationMagento(JsonClientWebV3 pJsonClientMagento,
      JsonAdresseClientFacturationWebV3 pAdresse, FluxMagento pFlux) {
    if (pAdresse == null || pJsonClientMagento == null) {
      return null;
    }
    
    String requete = "SELECT RLETB,RENUM,RLCOD,RLIND,RLNUMT FROM " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT
        + " " + " LEFT JOIN " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT_LIEN + " "
        + " ON RENUM=RLNUMT AND RLCOD='C' " + " LEFT JOIN " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " "
        + " ON DIGITS(CLCLI)=SUBSTR(RLIND, 1, 6) AND DIGITS(CLLIV)=SUBSTR(RLIND, 7, 3) " + " WHERE CLETB = '"
        + pJsonClientMagento.getEtb() + "' " + " AND CLCLI = '" + pAdresse.getCLCLI() + "' " + " AND CLLIV ='" + pAdresse.getCLLIV()
        + "' AND RLIN1 = 'P' ";
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    else {
      pFlux.construireMessageErreur(
          "[ImportClientMagento] recupererContactdeAdresseClientFacturationMagento(): Probleme de récupération du contact Client de facturation "
              + queryManager.getMsgError());
      return null;
    }
  }
  
  /**
   * Import Client: On va gérer les adresses de livraison du client en modifiant les clients livrés associés.
   * Note: les clients livrés ne sont pas crée car nous ne transférons que les données du bloc adresse. S'il faut le créer alors il faut
   * envoyer un flux client dédié avec toutes les informations le concernant.
   */
  private boolean mettreAJourClientsLivres(JsonClientWebV3 pClient, FluxMagento pFlux) {
    if (pClient == null || pClient.getCLCLI() == null || pClient.getAdressesLivraison() == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourClientsLivres() pClient ou Adresses à NULL ");
      return false;
    }
    
    if (pClient.getAdressesLivraison().size() == 0) {
      return true;
    }
    
    int ok = 0;
    for (JsonAdresseWebV3 adresse : pClient.getAdressesLivraison()) {
      if (mettreAJourUnClientLivre(pClient, adresse, pFlux)) {
        ok++;
      }
    }
    
    return (ok == pClient.getAdressesLivraison().size());
  }
  
  /**
   * Mettre à jour un client livré sur la base de son code client Série N.
   */
  private boolean mettreAJourUnClientLivre(JsonClientWebV3 pClient, JsonAdresseWebV3 pAdresse, FluxMagento pFlux) {
    boolean retour = false;
    
    if (pClient == null || pAdresse == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClientLivre() pClient ou pAdress à NULL ");
      return retour;
    }
    
    // Dans ce cas il s'agit d'une adresse de livraison éphémère on ne la sauvegarde pas dans Série N
    if (pAdresse.getIdClientLivraison() == null || pAdresse.getIdClientLivraison().trim().isEmpty()) {
      return true;
    }
    
    if (verifierClientLivre(pClient, pAdresse, pFlux)) {
      pAdresse.miseEnFormeBlocAdresseImport(pClient.getIsUnParticulier());
      
      // Interprétation des codes pays et libellé pays
      if (pAdresse.getPaysLivraison() == null || pAdresse.getPaysLivraison().trim().isEmpty()) {
        pAdresse.setPaysLivraison(JsonClientWebV3.codePaysDefaut);
        pAdresse.setLibellePaysLivraison(JsonClientWebV3.libPaysDefaut);
      }
      else {
        if (pAdresse.getLibellePaysLivraison() == null || pAdresse.getLibellePaysLivraison().trim().isEmpty()) {
          pAdresse.setLibellePaysLivraison(recupererLibellePaysSerieN(pAdresse.getPaysLivraison()));
        }
      }
      // Construction de la requête
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.ajouter("update " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " set");
      // Ajout des valeurs
      requeteSql.ajouterValeurUpdate("CLNOM", pAdresse.getCLNOM());
      requeteSql.ajouterValeurUpdate("CLCPL", pAdresse.getComplementNomLivraison());
      requeteSql.ajouterValeurUpdate("CLRUE", pAdresse.getCLRUE());
      requeteSql.ajouterValeurUpdate("CLLOC", pAdresse.getCLLOC());
      requeteSql.ajouterValeurUpdate("CLVIL", pAdresse.getCLVIL());
      requeteSql.ajouterValeurUpdate("CLCDP1", pAdresse.getCLCDP1());
      requeteSql.ajouterValeurUpdate("CLCOP", pAdresse.getPaysLivraison());
      requeteSql.ajouterValeurUpdate("CLPAY", pAdresse.getLibellePaysLivraison());
      requeteSql.ajouterValeurUpdate("CLTEL", pAdresse.getCLTEL());
      // Ajout des conditions
      requeteSql.ajouter("where");
      requeteSql.ajouterConditionAnd("CLETB", "=", pClient.getEtb());
      requeteSql.ajouterConditionAnd("CLCLI", "=", pAdresse.getCLCLI());
      requeteSql.ajouterConditionAnd("CLLIV", "=", pAdresse.getCLLIV());
      // Exécution de la requête
      try {
        if (queryManager.requete(requeteSql.getRequete()) > 0) {
          retour = majContactAdresseLivraison(pClient, pAdresse, pFlux);
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClientLivre() probleme UPDATE: " + queryManager.getMsgError());
          retour = false;
        }
      }
      catch (Exception e) {
        pFlux.construireMessageErreur("[ImportClientMagento] mettreAJourUnClientLivre() requête en erreur: " + requeteSql.getRequete());
        retour = false;
      }
    }
    
    return retour;
  }
  
  /**
   * Mettre à jour le contact principal d'un client lorsqu'il s'agit d'une adresse de livraison Magento.
   */
  private boolean majContactAdresseLivraison(JsonClientWebV3 pClient, JsonAdresseWebV3 pAdresse, FluxMagento pFlux) {
    GenericRecord record = recupererContactdeAdresseMagento(pClient, pAdresse, pFlux);
    // Le contact existe et est cohérent
    // ON NE CREE PAS S'IL N'EXISTE PAS CAR UN CLIENT MAGENTO AUTOMATIQUEMENT UN CONTACT ASSOCIE (mail)
    if (record == null) {
      return false;
    }
    
    if (!record.isPresentField("RENUM")) {
      pFlux.construireMessageErreur("[ImportClientMagento] majContactAdresseLivraison() RENUM à NULL ");
      return false;
    }
    
    String nomREPAC = traiterREPAC(OutilsMagento.nettoyerSaisiesSQL(pAdresse.getNomLivraison()),
        OutilsMagento.nettoyerSaisiesSQL(pAdresse.getPrenomLiv()));
    
    // Mise à jour du contact
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("update " + queryManager.getLibrary() + '.' + EnumTableBDD.CONTACT + " set ");
    requeteSql.ajouterValeurUpdate("RECIV", pAdresse.getCiviliteLivraison());
    requeteSql.ajouterValeurUpdate("REPAC", nomREPAC);
    requeteSql.ajouterValeurUpdate("RENOM", pAdresse.getNomLivraison());
    requeteSql.ajouterValeurUpdate("REPRE", pAdresse.getPrenomLiv());
    requeteSql.ajouter(" where ");
    requeteSql.ajouterConditionAnd("RENUM", "=", record.getIntegerValue("RENUM"));
    try {
      if (queryManager.requete(requeteSql.getRequete()) > 0) {
        return true;
      }
    }
    catch (Exception e) {
      pFlux.construireMessageErreur("[ImportClientMagento] majContactAdresseLivraison() Requête en erreur: " + requeteSql.getRequete());
      return false;
    }
    
    pFlux.construireMessageErreur("[ImportClientMagento] majContactAdresseLivraison() Maj du contact en Erreur: " + queryManager.getMsgError());
    return false;
  }
  
  /**
   * On vérifie que le code Série N de l'adresse de livraison soit bien un client livré du client transmis dans le flux.
   */
  private boolean verifierClientLivre(JsonClientWebV3 pClient, JsonAdresseWebV3 pAdresse, FluxMagento pFlux) {
    if (pClient == null || pClient.getCLCLI() == null || pClient.getEtb() == null) {
      majError("[ImportClientMagento] verifierClientLivre() pClient corrompu: " + pClient);
      return false;
    }
    if (pAdresse == null || pAdresse.getIdClientLivraison() == null || pAdresse.getIdClientLivraison().trim().isEmpty()) {
      majError("[ImportClientMagento] verifierClientLivre() pAdresse.getCodeSerieNLiv() corrompu");
      return false;
    }
    if (pAdresse.decouperCodeERP()) {
      if (!pClient.getCLCLI().equals(pAdresse.getCLCLI())) {
        majError("[ImportClientMagento] verifierClientLivre() Les codes clients sont différents: " + pClient.getCLCLI() + " - "
            + pAdresse.getCLCLI());
        return false;
      }
      
      RequeteSql requeteSql = new RequeteSql();
      requeteSql.ajouter("select CLCLI, CLLIV from " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " " + " where ");
      requeteSql.ajouterConditionAnd("CLETB", "=", pClient.getEtb());
      requeteSql.ajouterConditionAnd("CLCLI", "=", pAdresse.getCLCLI());
      requeteSql.ajouterConditionAnd("CLLIV", "=", pAdresse.getCLLIV());
      
      ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
      // Notre client existe bien dans Série N -> on récupère les données
      if (liste != null && liste.size() == 1) {
        return true;
      }
      else {
        pFlux.construireMessageErreur("[ImportClientMagento] verifierClientLivre() L'adresse n'est pas un client livré valide: "
            + pAdresse.getCLCLI() + " - " + pAdresse.getCLLIV());
        return false;
      }
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] decouperCodeERP() codeSerieNLiv corrompu: " + pAdresse.getIdClientLivraison());
      return false;
    }
  }
  
  /**
   * Formatage de l'affichage du code client pour un insert dans le RLIND de PSEMRTLM.
   */
  private String formaterLeClientPourRLIND(int pNumeroClient) {
    return String.format("%06d", pNumeroClient);
  }
  
  /**
   * Formatage l'affichage du fucking compte général concaténé au code client pour un insert dans le fucking RLIND de PSEMRTLM.
   */
  private String formaterLeClientPourRLINDCompta(JsonClientWebV3 client) {
    if (client == null) {
      return null;
    }
    
    String retour = "";
    int compteNum = 0;
    try {
      compteNum = Integer.parseInt(client.getCLCLI());
    }
    catch (NumberFormatException e) {
      compteNum = 0;
    }
    String compte = formaterLeClientPourRLIND(compteNum);
    // On suppose que le compte général est toujours sur 6 Numériques
    retour = client.getCompteGeneral() + compte;
    
    return retour;
  }
  
  /**
   * Mise à jour l'extension du client si elle existe, sinon on la créé.
   */
  private boolean gererActiviteWeb(JsonClientWebV3 pClient, FluxMagento pFlux) {
    if (pClient == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] majExtensionClient() client à NULL ");
      return false;
    }
    
    boolean retour = false;
    
    RequeteSql requeteSql = new RequeteSql();
    requeteSql
        .ajouter("select EBCLI from " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_EXTENSION + " " + " where ");
    requeteSql.ajouterConditionAnd("EBETB", "=", pClient.getEtb());
    requeteSql.ajouterConditionAnd("EBCLI", "=", pClient.getCLCLI());
    requeteSql.ajouterConditionAnd("EBLIV", "=", pClient.getCLLIV());
    
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (liste == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] majExtensionClient() liste à NULL pour client " + pClient.getEtb() + "/"
          + pClient.getCLCLI() + "/" + pClient.getCLLIV());
    }
    else {
      if (liste.size() == 1) {
        // on met à jour l'extension de ce client
        retour = mettreAJourExtensionClient(pClient, pFlux);
      }
      else if (liste.size() == 0) {
        // Il faut créer l'extension de ce client
        retour = insererUneNouvelleExtensionClient(pClient, pFlux);
      }
      else {
        pFlux.construireMessageErreur("[ImportClientMagento] majExtensionClient() taille de liste: " + liste.size() + "  pour client "
            + pClient.getEtb() + "/" + pClient.getCLCLI() + "/" + pClient.getCLLIV());
      }
    }
    
    return retour;
  }
  
  /**
   * On insère une nouvelle extension client avec la gestion Web intégré.
   */
  private boolean insererUneNouvelleExtensionClient(JsonClientWebV3 pClient, FluxMagento pFlux) {
    if (pClient == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] insererUneNouvelleExtensionClient() pClient à NULL ");
      return false;
    }
    int nb = queryManager.requete(" INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_EXTENSION + " "
        + "(EBETB,EBCLI,EBLIV,EBTZP,EBIN09) VALUES " + "('" + pClient.getEtb() + "','" + pClient.getCLCLI() + "'," + "'"
        + pClient.getCLLIV() + "','C'," + "'" + pClient.getEBIN09() + "')");
    if (nb > 0) {
      return true;
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] majExtensionClient() erreur intégration extension  pour client " + pClient.getEtb() + "/"
          + pClient.getCLCLI() + "/" + pClient.getCLLIV() + " - " + queryManager.getMsgError());
      return false;
    }
  }
  
  /**
   * On met à jour l'extension client avec la gestion Web intégré.
   */
  private boolean mettreAJourExtensionClient(JsonClientWebV3 pClient, FluxMagento pFlux) {
    if (pClient == null) {
      pFlux.construireMessageErreur("[ImportClientMagento] insererUneNouvelleExtensionClient() pClient à NULL ");
      return false;
    }
    int nb = queryManager.requete(" UPDATE " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_EXTENSION + " "
        + " SET EBIN09 = '" + pClient.getEBIN09() + "' " + " WHERE EBETB = '" + pClient.getEtb() + "' AND EBCLI = '" + pClient.getCLCLI()
        + "' AND EBLIV = '" + pClient.getCLLIV() + "' ");
    if (nb > 0) {
      return true;
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] majExtensionClient() erreur intégration extension  pour client " + pClient.getEtb() + "/"
          + pClient.getCLCLI() + "/" + pClient.getCLLIV() + " - " + queryManager.getMsgError());
      return false;
    }
  }
  
  /**
   * Permet de numéroter automatiquement les contacts.
   */
  private int numerotationAutomatiqueContact(FluxMagento pFlux) {
    int retour = -1;
    
    ArrayList<GenericRecord> liste = queryManager
        .select("select (MAX(INT(RENUM))+1) as AUTO from " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT + " ");
    if (liste != null && liste.size() > 0) {
      try {
        if (liste.get(0).isPresentField("AUTO")) {
          retour = Integer.parseInt(liste.get(0).getField("AUTO").toString().trim());
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] getField(AUTO) n'est pas présent: ");
        }
      }
      catch (NumberFormatException e) {
        pFlux.construireMessageErreur("[ImportClientMagento] Probleme de PARSEINT : " + liste.get(0).getField("AUTO").toString().trim());
        retour = 0;
      }
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] numerotationAutomatiqueContact() liste à NULL ou 0 : ");
    }
    
    return retour;
  }
  
  /**
   * Permet de vérifier si les données transmises à Série N sont bonnes.
   */
  private boolean verifierLesDonneesRentrantes(JsonClientWebV3 jsonClientMagento, FluxMagento pFlux) {
    // TODO faudra rendre cette méthode dynamique sans avoir à recoder le
    // nom des zones !!
    boolean retour = false;
    int nbErreurs = 0;
    String messageErreur = "";
    if (jsonClientMagento == null) {
      return retour;
    }
    
    // TODO RAJOUTER EXPRESSION REGULIERE NUMERIQUE AU TEST + CLADH MAX 15
    // CARACTERES
    if (jsonClientMagento.getIdClientWeb() == null || jsonClientMagento.getIdClientWeb().toString().trim().isEmpty()) {
      nbErreurs++;
      messageErreur += "IdClientMagento/";
    }
    
    // TODO RAJOUTER EXPRESSION REGULIERE NUMERIQUE AU TEST
    if (jsonClientMagento.getEmail() == null || jsonClientMagento.getEmail().toString().trim().isEmpty()) {
      nbErreurs++;
      messageErreur += "Email/";
    }
    
    // Désormais plus en relation avec la catégorie client
    jsonClientMagento.gererModeParticulier();
    
    if (!JsonEntiteMagento.controlerUnCodePostal(true, jsonClientMagento.getCdpClient())) {
      nbErreurs++;
      messageErreur += "codePostal du client/";
    }
    
    if (jsonClientMagento.getIdClient() != null && jsonClientMagento.getIdClient().trim().length() > 0) {
      if (!jsonClientMagento.decouperCodeERP()) {
        nbErreurs++;
        messageErreur += "codeSerieN/";
      }
    }
    
    if (nbErreurs > 0) {
      pFlux.construireMessageErreur(
          "[ImportClientMagento] verifierLesDonneesRentrantes() - Problème de données: " + nbErreurs + " erreurs: " + messageErreur);
      return false;
    }
    
    if (jsonClientMagento.getNomSociete() == null) {
      jsonClientMagento.setNomSociete("");
    }
    
    // Le numéro de SIREN qui est stocké dans CLSRN et CLSRT
    // CLSRN = 9 caractères
    // CLSRT = 5 caractères donc 9 + 5 = 14 caractères
    if (jsonClientMagento.getSiren() != null && jsonClientMagento.getSiren().toString().trim().length() > 14) {
      nbErreurs++;
      messageErreur += "Le numéro de SIREN reçu dépasse les 14 caractères maximum autorisés/";
    }
    else if (jsonClientMagento.getSiren() != null && jsonClientMagento.getSiren().toString().trim().length() > 9) {
      jsonClientMagento.setCLSRN(jsonClientMagento.getSiren().substring(0, 9));
      jsonClientMagento.setCLSRT(jsonClientMagento.getSiren().substring(9).trim());
    }
    else {
      jsonClientMagento.setCLSRN(jsonClientMagento.getSiren());
      jsonClientMagento.setCLSRT("");
    }
    
    if (jsonClientMagento.getCodeAPE() == null) {
      jsonClientMagento.setCodeAPE("");
    }
    
    // Bloc adresse du client
    if (jsonClientMagento.getCiviliteClient() == null) {
      jsonClientMagento.setCiviliteClient("");
    }
    if (jsonClientMagento.getNomClient() == null) {
      jsonClientMagento.setNomClient("");
    }
    if (jsonClientMagento.getComplementNomClient() == null) {
      jsonClientMagento.setComplementNomClient("");
    }
    if (jsonClientMagento.getPrenomClient() == null) {
      jsonClientMagento.setPrenomClient("");
    }
    if (jsonClientMagento.getRueClient() == null) {
      jsonClientMagento.setRueClient("");
    }
    if (jsonClientMagento.getRue2Client() == null) {
      jsonClientMagento.setRue2Client("");
    }
    jsonClientMagento.majCdpVilleImport();
    // Interprétation du code pays et libellé pays : ils ne sont modifiés que s'ils sont vides ou blancs
    if (jsonClientMagento.getPaysClient() == null || jsonClientMagento.getPaysClient().trim().isEmpty()) {
      jsonClientMagento.setPaysClient(JsonClientWebV3.codePaysDefaut);
      jsonClientMagento.setLibellePaysClient(JsonClientWebV3.libPaysDefaut);
    }
    else {
      if (jsonClientMagento.getLibellePays() == null || jsonClientMagento.getLibellePays().trim().isEmpty()) {
        jsonClientMagento.setLibellePaysClient(recupererLibellePaysSerieN(jsonClientMagento.getPaysClient()));
      }
    }
    if (jsonClientMagento.getTelClient() == null) {
      jsonClientMagento.setTelClient("");
    }
    if (jsonClientMagento.getFaxClient() == null) {
      jsonClientMagento.setFaxClient("");
    }
    if (jsonClientMagento.getMagClient() == null) {
      jsonClientMagento.setMagClient("");
    }
    if (jsonClientMagento.getSiteClient() == null) {
      jsonClientMagento.setSiteClient("");
    }
    jsonClientMagento.miseEnFormeBlocAdresseImport();
    
    // Contrôle des longueurs de certains champs qui seront tronqués si trop long lors de l'enregistrement en base mais il fait avertir
    // les utilisateurs.
    StringBuffer message = new StringBuffer();
    if (jsonClientMagento.getCLNOM() != null && jsonClientMagento.getCLNOM().length() > 30) {
      message.append("La longueur du champ CLNOM est trop longue (>30), elle va être tronquée pour l'enregistrement en base.\n");
    }
    if (jsonClientMagento.getComplementNomClient() != null && jsonClientMagento.getComplementNomClient().length() > 30) {
      message.append("La longueur du champ CLCLP est trop longue (>30), elle va être tronquée pour l'enregistrement en base.\n");
    }
    if (jsonClientMagento.getCLRUE() != null && jsonClientMagento.getCLRUE().length() > 30) {
      message.append("La longueur du champ CLRUE est trop longue (>30), elle va être tronquée pour l'enregistrement en base.\n");
    }
    if (jsonClientMagento.getCLLOC() != null && jsonClientMagento.getCLLOC().length() > 30) {
      message.append("La longueur du champ CLLOC est trop longue (>30), elle va être tronquée pour l'enregistrement en base.\n");
    }
    if (jsonClientMagento.getVilleClient() != null && jsonClientMagento.getVilleClient().length() > 30) {
      message.append("La longueur du champ VilleClient est trop longue (>30), elle va être tronquée pour l'enregistrement en base.\n");
    }
    if (jsonClientMagento.getLibellePays() != null && jsonClientMagento.getLibellePays().length() > 26) {
      message.append("La longueur du champ CLPAY est trop longue (>26), elle va être tronquée pour l'enregistrement en base.\n");
    }
    if (message.length() > 0) {
      try {
        String sujet = "Problème de longueur de champ pour l'enregistrement en base (FLID = " + pFlux.getFLID() + ")";
        IdEtablissement idEtablissement = IdEtablissement.getInstance(pFlux.getFLETB());
        managerFluxMagento.signalerProbleme(sujet, message.toString(), idEtablissement);
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    
    if (nbErreurs == 0) {
      retour = true;
    }
    else {
      pFlux.construireMessageErreur(
          "[ImportClientMagento] verifierLesDonneesRentrantes() - Problème de données: " + nbErreurs + " erreurs: " + messageErreur);
    }
    
    return retour;
  }
  
  /**
   * Récupèration du libellé d'un pays sur la base de son code Série N.
   */
  private String recupererLibellePaysSerieN(String pCodePays) {
    if (pCodePays == null) {
      return "";
    }
    
    // Construction de la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql
        .ajouter("select PARFIL from " + queryManager.getLibrary() + "." + EnumTableBDD.PARAMETRE_EXPLOITATION + " where");
    requeteSql.ajouterConditionAnd("PARCLE", "like", "   CP%");
    requeteSql.ajouterConditionAnd("SUBSTR(PARCLE, 6, 3)", "=", pCodePays);
    // Exécution de la requête
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    
    if (liste != null && liste.size() == 1) {
      if (liste.get(0).isPresentField("PARFIL")) {
        return liste.get(0).getField("PARFIL").toString().trim();
      }
    }
    
    return "";
  }
  
  /**
   * Permet de traiter la zone REPAC correctement.
   */
  private String traiterREPAC(String pNom, String pPrenom) {
    String repac = Constantes.normerTexte(pNom) + " " + Constantes.normerTexte(pPrenom);
    repac = repac.trim();
    
    if (repac.length() > JsonEntiteMagento.NB_MAX_CARACTERES_BLOC_ADRESSE) {
      repac = repac.substring(0, JsonEntiteMagento.NB_MAX_CARACTERES_BLOC_ADRESSE);
    }
    
    return repac;
  }
  
  /**
   * Permet de vérifier si le client importé existe déjà dans Série N ou non.
   */
  private int verifierSonExistenceDB2(JsonClientWebV3 client, FluxMagento pFlux) {
    String numeroClient = "0";
    String suffixeClient = "0";
    
    if (client.getCLCLI() != null) {
      numeroClient = client.getCLCLI();
    }
    if (client.getCLLIV() != null) {
      suffixeClient = client.getCLLIV();
    }
    
    ArrayList<GenericRecord> listeClient = queryManager
        .select("SELECT CLCLI,CLLIV,CLCAT,CLNCG,EBIN09 FROM " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " "
            + " LEFT JOIN " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_EXTENSION
            + " ON CLETB = EBETB AND EBCLI = CLCLI AND EBLIV = CLLIV " + " WHERE CLETB = '" + client.getEtb() + "' AND (CLADH = '"
            + client.getIdClientWeb() + "' OR (CLCLI = '" + numeroClient + "' AND CLLIV = '" + suffixeClient + "') )");
    
    if (listeClient != null) {
      // Le client existe en un exemplaire
      if (listeClient.size() == 1) {
        // TODO On contrôle sur l'adresse mail
        if (client.getEmail() != null && client.getEmail().trim().length() > 4) {
          if (listeClient.get(0).isPresentField("CLCLI")) {
            client.setCLCLI(listeClient.get(0).getField("CLCLI").toString().trim());
          }
          if (listeClient.get(0).isPresentField("CLLIV")) {
            client.setCLLIV(listeClient.get(0).getField("CLLIV").toString().trim());
          }
          if (listeClient.get(0).isPresentField("CLNCG")) {
            client.setCLNCG(listeClient.get(0).getField("CLNCG").toString().trim());
          }
          // On met à jour la gestion Web à partir de ces données existantes dans Série N
          if (listeClient.get(0).isPresentField("EBIN09")) {
            client.majGestionWeb(listeClient.get(0).getField("EBIN09").toString().trim());
          }
          
          return 1;
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] verifierSonExistenceDB2() email client NULL ou corrompu ");
          return -1;
        }
      }
      // Tiens tiens y en a plusieurs c'est rigolo
      else if (listeClient.size() > 1) {
        pFlux.construireMessageErreur("[ImportClientMagento] verifierSonExistenceDB2() On a " + listeClient.size()
            + " clients sous le même ID Magento pour le client " + client.getIdClientWeb());
        return listeClient.size();
      }
      // Sinon la liste == 0 on est en création de client
      else {
        // on met à jour la gestion Web
        client.majGestionWeb(null);
        return 0;
      }
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] verifierSonExistenceDB2() listeClient EXISTE PAS: " + queryManager.getMsgError());
      return -1;
    }
  }
  
  /**
   * Permet de vérifier si le client importé existe déjà dans la comptabilité de Série N ou non.
   */
  private int verifierSonExistenceComptaDB2(JsonClientWebV3 client, FluxMagento pFlux) {
    gererETBComptable(client);
    gererCompteGeneral(client, pFlux);
    
    ArrayList<GenericRecord> listeClient =
        queryManager.select(" SELECT A1NCA FROM " + queryManager.getLibrary() + ".PCGMPCAM " + " LEFT JOIN " + queryManager.getLibrary()
            + "." + EnumTableBDD.CLIENT + " ON CLETB = A1SOC AND CLCLI = A1NCA " + " WHERE A1SOC = '" + client.getEtbComptable()
            + "' AND CLADH = '" + client.getIdClientWeb() + "' AND A1NCG = '" + client.getCompteGeneral() + "' ");
    
    if (listeClient != null) {
      if (listeClient.size() > 1) {
        pFlux.construireMessageErreur("[ImportClientMagento] verifierSonExistenceComptaDB2() On a " + listeClient.size()
            + " clients sous le même ID Magento pour le client " + client.getIdClientWeb());
      }
      
      return listeClient.size();
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] verifierSonExistenceDB2() listeClient EXISTE PAS: " + queryManager.getMsgError());
      return -1;
    }
  }
  
  /**
   * Permet d'insérer ou de mettre à jour un client de Compta Série N avec les données importées.
   */
  private boolean mettreAJourUnClientCompta(JsonClientWebV3 client, FluxMagento pFlux) {
    int nbCLientsExistants = verifierSonExistenceComptaDB2(client, pFlux);
    
    nettoyerDonneesClientComptable(client);
    // On update le client compta
    if (nbCLientsExistants == 1) {
      return modifierUnClientCompta(client, pFlux);
    }
    else if (nbCLientsExistants == 0) {
      return creerUnClientCompta(client, pFlux);
      // Si y a un fucking souci
    }
    else {
      return false;
    }
  }
  
  /**
   * Permet de préparer certaines données avant l'intégration en base de données du client comptable.
   * Par exemple le traitement de la ville est différent de celui de la GESCOM : On enlève le code postal au début (24 caractères Max).
   */
  private void nettoyerDonneesClientComptable(JsonClientWebV3 client) {
    if (client == null || client.getVilleClient() == null) {
      return;
    }
    
    String[] tableauVillePourrie = client.getVilleClient().trim().split(" ");
    String villeFinale = null;
    if (tableauVillePourrie.length > 1) {
      villeFinale = "";
      for (int i = 1; i < tableauVillePourrie.length; i++) {
        villeFinale += tableauVillePourrie[i] + " ";
      }
    }
    if (villeFinale != null) {
      client.setVilleClient(villeFinale.trim());
    }
  }
  
  /**
   * Modifie le client comptable sur la base du client Magento.
   */
  private boolean modifierUnClientCompta(JsonClientWebV3 client, FluxMagento pFlux) {
    if (client.getCLCLI() == null || client.getCLCLI().trim().isEmpty()) {
      return false;
    }
    
    // Mode de réglement client
    String majReglement = "";
    if (client.getCodeReglement() != null && !client.getCodeReglement().trim().isEmpty()) {
      majReglement = " A1RGL = '" + client.getCodeReglement() + "',";
    }
    // @formatter:off
    String requete = " UPDATE " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_COMPTABILITE
        + " SET " + majReglement
        + " A1MOD = '" + client.retournerDateDuJourSN() + "',"
        + " A1LIB = '" + OutilsMagento.nettoyerSaisiesSQL(client.getCLNOM()) + "',"
        + " A1RUE = '" + OutilsMagento.nettoyerSaisiesSQL(client.getCLRUE()) + "',"
        + " A1LOC = '" + OutilsMagento.nettoyerSaisiesSQL(client.getCLLOC()) + "',"
        + " A1VIL = '" + OutilsMagento.nettoyerSaisiesSQL(client.getVilleClient()) + "',"
        + " A1CDP = '" + client.getCdpClient() + "',"
        + " A1COP = '" + client.getPaysClient() + "',"
        + " A1PAY = '" + OutilsMagento.nettoyerSaisiesSQL(client.getLibellePays()) + "',"
        + " A1TEL = '" + client.getTelClient() + "',"
        + " A1FAX = '" + client.getFaxClient() + "',"
        + " A1SRN = '" + client.getCLSRN() + "',"
        + " A1SRT = '" + client.getCLSRT() + "',"
        + " A1NCG = '" + client.getCompteGeneral() + "'"
        + " WHERE A1SOC = '" + client.getEtbComptable() + "' AND A1NCA = '" + client.getCLCLI() + "' AND A1NCG = '" + client.getCompteGeneral() + "'";
    // @formatter:on
    // UPDATE CLIENT
    if (queryManager.requete(requete) > 0) {
      // On touche pas au contact on considère qu'il est bon pour le moment.
      return true;
    }
    else {
      pFlux.construireMessageErreur(
          "[ImportClientMagento] modifierUnClientCompta() UPDATE client comptable en erreur : " + queryManager.getMsgError());
    }
    
    return false;
  }
  
  /**
   * Créer un client en comptabilité sur la base du client gestion commerciale.
   */
  private boolean creerUnClientCompta(JsonClientWebV3 client, FluxMagento pFlux) {
    if (client.getCLCLI() == null || client.getCLCLI().trim().isEmpty()) {
      return false;
    }
    
    // Mode de réglement client
    if (client.getCodeReglement() == null || client.getCodeReglement().trim().isEmpty()) {
      client.setCodeReglement(parametresBibli.getReglementParticulierMg());
    }
    
    String champs = "A1CRE,A1MOD,A1SOC,A1NCG,A1NCA,A1LIB,A1PTG,A1RPT,A1RGL,A1RUE,A1LOC,A1CDP,A1VIL,A1COP,A1PAY,A1TEL,A1FAX,A1SRN,A1SRT";
    // @formatter:off
    String valeurs = "'" + client.retournerDateDuJourSN() + "','"
        + client.retournerDateDuJourSN() + "','"
        + client.getEtbComptable() + "','"
        + client.getCompteGeneral() + "','"
        + client.getCLCLI() + "','"
        + OutilsMagento.nettoyerSaisiesSQL(client.getCLNOM()) + "','D','1','"
        + client.getCodeReglement() + "',"
        + RequeteSql.formaterStringSQL(client.getCLRUE(), true, 30) + ","
        + RequeteSql.formaterStringSQL(client.getCLLOC(), true, 30) + ",'"
        + client.getCdpClient() + "','"
        + OutilsMagento.nettoyerSaisiesSQL(client.getVilleClient()) + "','"
        + client.getPaysClient() + "','"
        + OutilsMagento.nettoyerSaisiesSQL(client.getLibellePays()) + "','"
        + client.getTelClient() + "','"
        + client.getFaxClient() + "','"
        + client.getCLSRN() + "','"
        + client.getCLSRT() + "'";
    // @formatter:on
    String requete = "INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT_COMPTABILITE + " (" + champs
        + ") VALUES (" + valeurs + ") ";
    
    if (queryManager.requete(requete) > 0) {
      // On va créer le lien compta avec le contact Série N gescom.
      // Normalement un client magento possède automatiquement un contact lié.
      GenericRecord record = recupererContactduClientMagento(client);
      if (record != null) {
        if (record.isPresentField("RLNUMT")) {
          champs = " RLCOD,RLETB,RLIND,RLNUMT,RLIN1 ";
          // @formatter:off
          valeurs = " 'A','"
              + client.getEtbComptable() + "','"
              + formaterLeClientPourRLINDCompta(client) + "','"
              + record.getField("RLNUMT").toString().trim() + "','' ";
          // @formatter:on
          if (queryManager.requete("INSERT INTO " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT_LIEN + " (" + champs
              + ") VALUES (" + valeurs + ") ") > 0) {
            return true;
          }
          else {
            pFlux.construireMessageErreur(
                "[ImportClientMagento] creerUnClientCompta() INSERT nouvelle liaison Contact en erreur : " + queryManager.getMsgError());
          }
        }
        else {
          pFlux.construireMessageErreur("[ImportClientMagento] creerUnClientCompta() Zones RLETB,RLIND,RLNUMT absentes ");
        }
      }
      else {
        pFlux.construireMessageErreur("[ImportClientMagento] creerUnClientCompta() RECUP Contact en erreur : " + queryManager.getMsgError());
      }
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] creerUnClientCompta() echec creation client compta : " + queryManager.getMsgError());
    }
    
    return false;
  }
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  /**
   * Permet de retourner l'établissement comptable du client En effet, celui n'est pas forcément l'établissement de gestion commerciale.
   */
  private void gererETBComptable(JsonClientWebV3 client) {
    // On contrôle le paramètre CV s'il existe et pas blanc (CVETC de 121 à 123)
    String etb = null;
    // Sinon on prend le paramètre CV (CVCOL en position 79 à 84)
    // -> étonnant je trouve rien à cette position du coup je fais de 56 sur 6
    ArrayList<GenericRecord> listeParam = queryManager.select(
        "SELECT SUBSTR(PARZ2, 98, 3) AS ETB FROM " + queryManager.getLibrary() + "." + EnumTableBDD.PARAMETRE_GESCOM + " "
            + " WHERE PARETB = '" + client.getEtbComptable() + "' AND PARTYP = 'CV' " + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
    
    if (listeParam != null && listeParam.size() == 1) {
      if (listeParam.get(0).isPresentField("ETB") && !listeParam.get(0).getField("ETB").toString().trim().isEmpty()) {
        etb = listeParam.get(0).getField("ETB").toString().trim();
      }
    }
    
    // Si le paramètre est existant on attribue sa valeur à l'établissement comptable
    if (etb != null) {
      client.setEtbComptable(etb);
    }
    else {
      // S'il n'existe pas on prend la valeur de l'établissement de l'utilisateur
      client.setEtbComptable(client.getEtb());
    }
  }
  
  /**
   * Retourne le compte général à associer automatiquement à client s'il n'en possède pas On lit le paramètre CV dans la table de
   * paramètre PGVMPARM.
   */
  private void gererCompteGeneral(JsonClientWebV3 client, FluxMagento pFlux) {
    // Si CLNCG est implémenté on prend cette valeur
    if (client.getCLNCG() != null && !client.getCLNCG().trim().equals("0")) {
      client.setCompteGeneral(client.getCLNCG());
    }
    else {
      String compte = null;
      // Sinon on prend le paramètre CV (CVCOL en position 79 à 84)
      // -> étonnant je trouve rien à cette position du coup je fais de 56 sur 6
      ArrayList<GenericRecord> listeParam = queryManager.select("SELECT SUBSTR(PARZ2, 56, 6) AS CPT FROM " + queryManager.getLibrary()
          + "." + EnumTableBDD.PARAMETRE_GESCOM + " " + " WHERE PARETB = '" + client.getEtbComptable() + "' AND PARTYP = 'CV' "
          + " FETCH FIRST 1 ROWS ONLY OPTIMIZE FOR 1 ROWS ");
      
      if (listeParam != null && listeParam.size() == 1) {
        if (listeParam.get(0).isPresentField("CPT")) {
          compte = listeParam.get(0).getField("CPT").toString().trim();
        }
        if (compte == null || compte.trim().length() == 0) {
          compte = COMPTE_GENERAL_DEFAUT;
        }
      }
      else {
        pFlux
            .construireMessageErreur("[ImportClientMagento] retournerCompteGeneral() listeParam EXISTE PAS ou ZERO : " + queryManager.getMsgError());
        compte = COMPTE_GENERAL_DEFAUT;
      }
      
      client.setCompteGeneral(compte);
    }
  }
  
  /**
   * Récupèration du contact principal du client Magento.
   */
  private GenericRecord recupererContactduClientMagento(JsonClientWebV3 client) {
    if (client == null) {
      return null;
    }
    
    String requete = "SELECT RLETB,RENUM,RLCOD,RLIND,RLNUMT " + " FROM " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT
        + " " + " LEFT JOIN " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT_LIEN + " "
        + " ON DIGITS(CLCLI)=SUBSTR(RLIND, 1, 6) " + " AND DIGITS(CLLIV)=SUBSTR(RLIND, 7, 3) " + " AND RLCOD = 'C' " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PSEMRTEM " + " ON RENUM=RLNUMT " + " WHERE CLETB = '" + client.getEtb() + "' " + " AND CLADH='"
        + client.getIdClientWeb() + "' " + " AND RLIN1 = 'P' ";
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    else {
      return null;
    }
  }
  
  /**
   * Récupèration du contact principal du client Magento.
   */
  private GenericRecord recupererContactdeAdresseMagento(JsonClientWebV3 pClient, JsonAdresseWebV3 pAdresse, FluxMagento pFlux) {
    if (pAdresse == null || pClient == null) {
      return null;
    }
    
    String requete = " SELECT RLETB,RENUM,RLCOD,RLIND,RLNUMT FROM " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT
        + " " + " LEFT JOIN " + queryManager.getLibrary() + "." + EnumTableBDD.CONTACT_LIEN + " "
        + " ON RENUM=RLNUMT AND RLCOD='C' " + " LEFT JOIN " + queryManager.getLibrary() + "." + EnumTableBDD.CLIENT + " "
        + " ON DIGITS(CLCLI)=SUBSTR(RLIND, 1, 6) AND DIGITS(CLLIV)=SUBSTR(RLIND, 7, 3) " + " WHERE CLETB = '" + pClient.getEtb() + "' "
        + " AND CLCLI = '" + pAdresse.getCLCLI() + "' " + " AND CLLIV ='" + pAdresse.getCLLIV() + "' AND RLIN1 = 'P' ";
    
    ArrayList<GenericRecord> liste = queryManager.select(requete);
    if (liste != null && liste.size() > 0) {
      return liste.get(0);
    }
    else {
      pFlux.construireMessageErreur("[ImportClientMagento] recupererContactdeAdresseMagento(): Probleme de récupération du contact Client "
          + queryManager.getMsgError());
      return null;
    }
  }
  
  /**
   * Créer un flux client vers Magento sur la base du client transmis en paramètre.
   */
  private boolean renvoyerUnFluxClient(JsonClientWebV3 pClient) {
    if (pClient == null) {
      return false;
    }
    
    ManagerFluxMagento gestionFM = new ManagerFluxMagento(queryManager.getLibrary());
    FluxMagento fm;
    
    // Ajout d'un enregistrement: Instanciation d'enregistrement
    fm = gestionFM.getRecord(true);
    fm.setFLSNS(FluxMagento.SNS_VERS_MAGENTO);
    fm.setFLETB(pClient.getEtb());
    fm.setFLVER(pClient.getVersionFlux());
    
    fm.setFLIDF(FluxMagento.IDF_FLX004_CLIENT);
    fm.setFLCOD(pClient.getCLCLI() + "/" + pClient.getCLLIV());
    fm.setFLSTT(EnumStatutFlux.A_TRAITER);
    
    // Insertion
    boolean ret = gestionFM.insererFluxDansTableNonTraite(fm);
    if (!ret) {
      majError("[ImportClientMagento] renvoyerUnFluxClient(): " + gestionFM.getMsgError());
    }
    
    gestionFM.dispose();
    
    return ret;
  }
  
}
