/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs.article;

import ri.serien.libas400.dao.gvx.database.files.FFD_Pgvmeaam;
import ri.serien.libas400.database.QueryManager;

public class M_EaArticle extends FFD_Pgvmeaam {
  // Variables de travail
  
  // -- Méthodes publiques --------------------------------------------------
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_EaArticle(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Insère l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PGVMEAAM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   * @return
   * 
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete =
        genericrecord.createSQLRequestUpdate("PGVMEAAM", querymg.getLibrary(), "A1ETB=" + getA1ETB() + " and A1ART='" + getA1ART() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   * 
   */
  @Override
  public boolean deleteInDatabase() {
    String requete = "delete from " + querymg.getLibrary() + ".PGVMEAAM where A1ETB=" + getA1ETB() + " and A1ART='" + getA1ART() + "'";
    return request(requete);
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  // -- Accesseurs ----------------------------------------------------------
  
}
