/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.util.Date;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.client.Negociation;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgChargerNegociation extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVM0001";
  
  /**
   * Appel du programme RPG qui va lire une négociation pour une ligne article.
   */
  public Negociation chargerNegociation(SystemeManager pSysteme, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    LigneVente.controlerId(pLigneVente, true);
    
    // Préparation des paramètres du programme RPG
    Svgvm0001i entree = new Svgvm0001i();
    Svgvm0001o sortie = new Svgvm0001o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    entree.setPietb(pLigneVente.getId().getCodeEtablissement());
    entree.setPicod(pLigneVente.getId().getCodeEntete().getCode());
    entree.setPinum(pLigneVente.getId().getNumero());
    entree.setPisuf(pLigneVente.getId().getSuffixe());
    entree.setPinli(pLigneVente.getId().getNumeroLigne());
    entree.setPidat(ConvertDate.dateToDb2(pDateTraitement));
    entree.setPiart(pLigneVente.getIdArticle().getCodeArticle());
    entree.setPiopt(pOptionTraitement);
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécution du programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmas(), parameterList)) {
      return null;
      
    }
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput(); // TODO A voir si l'on peut commenter cette ligne
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Initialisation de la classe métier
    Negociation negociation = new Negociation();
    negociation.setIdEtablissement(pLigneVente.getId().getIdEtablissement()); // Etablissement de GVM
    negociation.setCodeEnteteDocumentVente(pLigneVente.getId().getCodeEntete()); // Code ERL
    negociation.setNumeroBon(pLigneVente.getId().getNumero()); // Numéro du bon
    negociation.setSuffixeBon(pLigneVente.getId().getSuffixe()); // Suffixe du bon
    negociation.setNumeroLigne(pLigneVente.getId().getNumeroLigne()); // Numéro de ligne
    negociation.setCodeArticle(pLigneVente.getIdArticle()); // Code article
    negociation.setDateTraitement(ConvertDate.dateToDb2(pDateTraitement)); // Date de traitement
    negociation.setTypeCondition(sortie.getT1tcd()); // Type de condition
    negociation.setValeur(sortie.getT1val()); // Valeur
    negociation.setRemise1(sortie.getT1rem1()); // Remise 1
    negociation.setRemise2(sortie.getT1rem2()); // Remise 2
    negociation.setRemise3(sortie.getT1rem3()); // Remise 3
    negociation.setRemise4(sortie.getT1rem4()); // Remise 4
    negociation.setRemise5(sortie.getT1rem5()); // Remise 5
    negociation.setRemise6(sortie.getT1rem6()); // Remise 6
    negociation.setCoefficient(sortie.getT1coe()); // Coefficient
    negociation.setDateDebut((sortie.getT1dtd())); // Date de création
    negociation.setDateFin((sortie.getT1dtf())); // Date de création
    negociation.setFormulePrix(sortie.getT1fpr()); // Formule de prix
    return negociation;
  }
}
