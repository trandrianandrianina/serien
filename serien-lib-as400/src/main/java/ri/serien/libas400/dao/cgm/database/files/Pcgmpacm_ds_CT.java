/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.cgm.database.files;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pcgmpacm_ds_CT pour les CT
 * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG
 */

public class Pcgmpacm_ds_CT extends DataStructureRecord {

  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "CTLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(60), "CTC"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(40, 0), "CTF")); // A contrôler car à l'origine c'est une zone packed
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CTO"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CTE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CTSNS"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CTSNE"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "CTNPL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CTIN1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CTCOD"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CTCOC"));

    length = 300;
  }
}
