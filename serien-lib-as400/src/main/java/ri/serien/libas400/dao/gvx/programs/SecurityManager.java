/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.programs;

import ri.serien.libas400.dao.gvx.database.PgvmsecmManager;
import ri.serien.libas400.database.DatabaseManager;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;

public class SecurityManager {
  // Variables
  private DatabaseManager database = null;
  private PgvmsecmManager pgvmsecm = null;
  private final String[] tabSuperUtilisateurs = { "SEU001", "SEU002", "SEU003", "SEU004", "SEU005", "SEU006", "SEU007", "SEU008",
      "SEU009", "SEU010", "SEU011", "SEU012", "SEU013", "SEU014", "SEU015", "SEU016", "SEU017", "SEU018", "SEU019", "SEU020" };
  private final String SECURITE_GLOBALE = "SEACT";
  
  /**
   * Constructeur
   * @param adatabase
   * @param bibuser
   */
  public SecurityManager(DatabaseManager adatabase, String abibuser) {
    database = adatabase;
    if (database == null) {
      return;
    }
    
    pgvmsecm = new PgvmsecmManager(database.getConnection());
    pgvmsecm.setLibrary(abibuser);
  }
  
  /**
   * Retourne s'il y a une sécurité globale
   * @param listUser
   * @return
   *
   */
  public boolean isGlobalSecurity(String[] listUser) {
    if (pgvmsecm == null) {
      return false;
    }
    
    // On recherche l'enregistrement pour la sécurité globale
    IdEtablissement idEtablissement = IdEtablissement.getInstancePourEtablissementVide();
    ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB("", idEtablissement);
    if (record == null) {
      return false;
    }
    
    // On récupère la liste des users qui ont tous les droits
    if (listUser != null) {
      for (int i = 0; i < listUser.length; i++) {
        listUser[i] = ((String) record.getField(7 + i)).trim();
      }
    }
    
    return ((String) record.getField("SEACT")).equals("OUI");
  }
  
  /**
   * Retourne l'enregistrement de la sécurité utilisateur
   * @param dgetb
   * @return
   *
   */
  public ExtendRecord getUserSecurity(String auser, IdEtablissement pIdEtablissement) {
    if (pgvmsecm == null) {
      return null;
    }
    if (auser == null) {
      return null;
    }
    
    // On recherche l'établissement de l'utilisateur s'il y en a un
    IdEtablissement idEtablissement = pgvmsecm.getEtablissementUser(auser);
    // Sinon on prend celui de la DG
    if (idEtablissement == null) {
      idEtablissement = pIdEtablissement;
    }
    
    // Lecture de l'enregistrement de la sécurité utilisateur
    ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB(auser, idEtablissement);
    if (record == null) {
      return null;
    }
    
    // On contrôle s'il n'y a pas une sécurité de groupe
    String usergrp = ((String) record.getField("SEGRP")).trim();
    // Si le profil de groupe est à blanc alors pas de sécurité de groupe, on retourne directement l'enregitrement déjà
    // lu
    if (usergrp.equals("")) {
      return record;
    }
    // On retourne la sécurité du profil de groupe
    return pgvmsecm.getRecordsbyUSERandETB(usergrp, idEtablissement);
  }
  
  /**
   * Permet de controler si l'ID de sécurité passé en paramètre est opérationnel pour l'utilisateur et l'etb concernés.
   * 0 utilisateur possède les droits sur cet ID
   * Autres valeurs, blanc ou null n'a pas les droits sur cet ID
   */
  public boolean possedeLesDroits(String utilisateur, IdEtablissement pIdEtablissement, String securite) {
    if (possedeTousLesDroits(utilisateur)) {
      return true;
    }
    else {
      ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB(utilisateur, pIdEtablissement);
      if (record == null) {
        return false;
      }
      
      return (record.getField(securite) != null && record.getField(securite).toString().trim().equals("0"));
    }
  }
  
  /**
   * Permet de contrôler si l'utilisateur à le droit à tout ou non selon les 2 critères ci dessous.
   * 1 La sécurité globale est-elle active ou non ? Si "non" tout le monde a le droit à tout.
   * 2 L'utilisateur a-t-il le droit à tout ? POur cela son login doit être présent dans la 1ere ligne des sécurités du module
   * CONCLUSION: Si la sécurité globale n'est pas activée et que l'utilisateur n'est pas dans la 1er ligne des utilisateurs
   * Il faudra rentrer dans le détail de chaque zone associées à son profil
   * via les autres lignes d'enregistrements des sécurités du module: obtenirLesSecuritesUtilisateur()
   */
  public boolean possedeTousLesDroits(String utilisateur) {
    if (pgvmsecm == null) {
      return false;
    }
    
    // On recherche l'enregistrement pour la sécurité globale
    IdEtablissement idEtablissement = IdEtablissement.getInstancePourEtablissementVide();
    ExtendRecord record = pgvmsecm.getRecordsbyUSERandETB("", idEtablissement);
    if (record == null) {
      return false;
    }
    // Sécurité globale
    // TODO Attention Bug sur le isNullField()
    if (record.getField(SECURITE_GLOBALE) != null && !record.getField(SECURITE_GLOBALE).toString().equals("OUI")) {
      return true;
    }
    
    // TODO On fuck la sécurité de groupe pour le moment vu avec Pascaline le 20/09/16
    // Il faudra intégrer la méthode de Steph qui le fait très bien ultérieurement
    
    // Super utilisateur ?
    // le tableau SEU (tabSuperUtilisateurs) contient l'ensemble des super utilisateurs le notre y est il ?
    for (int i = 0; i < tabSuperUtilisateurs.length; i++) {
      if (record.getField(tabSuperUtilisateurs[i]) != null
          && record.getField(tabSuperUtilisateurs[i]).toString().trim().equals(utilisateur)) {
        return true;
      }
    }
    
    return false;
  }
}
