/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PgvmclimManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public PgvmclimManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne la liste des modules (pour les menus)
   * @param bibenv
   * @param prf
   * @return
   * 
   */
  public ArrayList<GenericRecord> getTousEnregistrements() {
    return select("Select CLCLI,CLTNS,CLNOM,CLCPL,CLRUE,CLLOC,CLTEL,CLVIL from " + getLibrary() + "+pgvmclim");
  }
  
}
