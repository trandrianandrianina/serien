/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lienligne;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvx0022d {
  // Constantes
  public static final int SIZE_LITYP = 1;
  public static final int SIZE_LICOD = 1;
  public static final int SIZE_LIETB = 3;
  public static final int SIZE_LINUM = 6;
  public static final int DECIMAL_LINUM = 0;
  public static final int SIZE_LISUF = 1;
  public static final int DECIMAL_LISUF = 0;
  public static final int SIZE_LINLI = 4;
  public static final int DECIMAL_LINLI = 0;
  public static final int SIZE_LINFA = 7;
  public static final int DECIMAL_LINFA = 0;
  public static final int SIZE_LOTYP = 1;
  public static final int SIZE_LOCOD = 1;
  public static final int SIZE_LOETB = 3;
  public static final int SIZE_LONUM = 6;
  public static final int DECIMAL_LONUM = 0;
  public static final int SIZE_LOSUF = 1;
  public static final int DECIMAL_LOSUF = 0;
  public static final int SIZE_LONLI = 4;
  public static final int DECIMAL_LONLI = 0;
  public static final int SIZE_LONFA = 7;
  public static final int DECIMAL_LONFA = 0;
  public static final int SIZE_TOTALE_DS = 46;
  public static final int SIZE_FILLER = 50 - 46;
  
  // Constantes indices Nom DS
  public static final int VAR_LITYP = 0;
  public static final int VAR_LICOD = 1;
  public static final int VAR_LIETB = 2;
  public static final int VAR_LINUM = 3;
  public static final int VAR_LISUF = 4;
  public static final int VAR_LINLI = 5;
  public static final int VAR_LINFA = 6;
  public static final int VAR_LOTYP = 7;
  public static final int VAR_LOCOD = 8;
  public static final int VAR_LOETB = 9;
  public static final int VAR_LONUM = 10;
  public static final int VAR_LOSUF = 11;
  public static final int VAR_LONLI = 12;
  public static final int VAR_LONFA = 13;
  
  // Variables AS400
  private String lityp = ""; // A=ACHAT, V=VENTE
  private String licod = ""; // Code ERL
  private String lietb = ""; // Code Etablissement
  private BigDecimal linum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal lisuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal linli = BigDecimal.ZERO; // Numéro de Ligne
  private BigDecimal linfa = BigDecimal.ZERO; // Numéro de Facture
  private String lotyp = ""; // A=ACHAT, V=VENTE
  private String locod = ""; // Code ERL
  private String loetb = ""; // Code Etablissement
  private BigDecimal lonum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal losuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal lonli = BigDecimal.ZERO; // Numéro de Ligne
  private BigDecimal lonfa = BigDecimal.ZERO; // Numéro de Facture
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_LITYP), // A=ACHAT, V=VENTE
      new AS400Text(SIZE_LICOD), // Code ERL
      new AS400Text(SIZE_LIETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LINUM, DECIMAL_LINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LISUF, DECIMAL_LISUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LINLI, DECIMAL_LINLI), // Numéro de Ligne
      new AS400ZonedDecimal(SIZE_LINFA, DECIMAL_LINFA), // Numéro de Facture
      new AS400Text(SIZE_LOTYP), // A=ACHAT, V=VENTE
      new AS400Text(SIZE_LOCOD), // Code ERL
      new AS400Text(SIZE_LOETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LONUM, DECIMAL_LONUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LOSUF, DECIMAL_LOSUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LONLI, DECIMAL_LONLI), // Numéro de Ligne
      new AS400ZonedDecimal(SIZE_LONFA, DECIMAL_LONFA), // Numéro de Facture
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { lityp, licod, lietb, linum, lisuf, linli, linfa, lotyp, locod, loetb, lonum, losuf, lonli, lonfa, filler, };
  
  // -- Accesseurs
  
  public void setLityp(Character pLityp) {
    if (pLityp == null) {
      return;
    }
    lityp = String.valueOf(pLityp);
  }
  
  public Character getLityp() {
    return lityp.charAt(0);
  }
  
  public void setLicod(Character pLicod) {
    if (pLicod == null) {
      return;
    }
    licod = String.valueOf(pLicod);
  }
  
  public Character getLicod() {
    return licod.charAt(0);
  }
  
  public void setLietb(String pLietb) {
    if (pLietb == null) {
      return;
    }
    lietb = pLietb;
  }
  
  public String getLietb() {
    return lietb;
  }
  
  public void setLinum(BigDecimal pLinum) {
    if (pLinum == null) {
      return;
    }
    linum = pLinum.setScale(DECIMAL_LINUM, RoundingMode.HALF_UP);
  }
  
  public void setLinum(Integer pLinum) {
    if (pLinum == null) {
      return;
    }
    linum = BigDecimal.valueOf(pLinum);
  }
  
  public Integer getLinum() {
    return linum.intValue();
  }
  
  public void setLisuf(BigDecimal pLisuf) {
    if (pLisuf == null) {
      return;
    }
    lisuf = pLisuf.setScale(DECIMAL_LISUF, RoundingMode.HALF_UP);
  }
  
  public void setLisuf(Integer pLisuf) {
    if (pLisuf == null) {
      return;
    }
    lisuf = BigDecimal.valueOf(pLisuf);
  }
  
  public Integer getLisuf() {
    return lisuf.intValue();
  }
  
  public void setLinli(BigDecimal pLinli) {
    if (pLinli == null) {
      return;
    }
    linli = pLinli.setScale(DECIMAL_LINLI, RoundingMode.HALF_UP);
  }
  
  public void setLinli(Integer pLinli) {
    if (pLinli == null) {
      return;
    }
    linli = BigDecimal.valueOf(pLinli);
  }
  
  public Integer getLinli() {
    return linli.intValue();
  }
  
  public void setLinfa(BigDecimal pLinfa) {
    if (pLinfa == null) {
      return;
    }
    linfa = pLinfa.setScale(DECIMAL_LINFA, RoundingMode.HALF_UP);
  }
  
  public void setLinfa(Integer pLinfa) {
    if (pLinfa == null) {
      return;
    }
    linfa = BigDecimal.valueOf(pLinfa);
  }
  
  public void setLinfa(Date pLinfa) {
    if (pLinfa == null) {
      return;
    }
    linfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pLinfa));
  }
  
  public Integer getLinfa() {
    return linfa.intValue();
  }
  
  public Date getLinfaConvertiEnDate() {
    return ConvertDate.db2ToDate(linfa.intValue(), null);
  }
  
  public void setLotyp(Character pLotyp) {
    if (pLotyp == null) {
      return;
    }
    lotyp = String.valueOf(pLotyp);
  }
  
  public Character getLotyp() {
    return lotyp.charAt(0);
  }
  
  public void setLocod(Character pLocod) {
    if (pLocod == null) {
      return;
    }
    locod = String.valueOf(pLocod);
  }
  
  public Character getLocod() {
    return locod.charAt(0);
  }
  
  public void setLoetb(String pLoetb) {
    if (pLoetb == null) {
      return;
    }
    loetb = pLoetb;
  }
  
  public String getLoetb() {
    return loetb;
  }
  
  public void setLonum(BigDecimal pLonum) {
    if (pLonum == null) {
      return;
    }
    lonum = pLonum.setScale(DECIMAL_LONUM, RoundingMode.HALF_UP);
  }
  
  public void setLonum(Integer pLonum) {
    if (pLonum == null) {
      return;
    }
    lonum = BigDecimal.valueOf(pLonum);
  }
  
  public Integer getLonum() {
    return lonum.intValue();
  }
  
  public void setLosuf(BigDecimal pLosuf) {
    if (pLosuf == null) {
      return;
    }
    losuf = pLosuf.setScale(DECIMAL_LOSUF, RoundingMode.HALF_UP);
  }
  
  public void setLosuf(Integer pLosuf) {
    if (pLosuf == null) {
      return;
    }
    losuf = BigDecimal.valueOf(pLosuf);
  }
  
  public Integer getLosuf() {
    return losuf.intValue();
  }
  
  public void setLonli(BigDecimal pLonli) {
    if (pLonli == null) {
      return;
    }
    lonli = pLonli.setScale(DECIMAL_LONLI, RoundingMode.HALF_UP);
  }
  
  public void setLonli(Integer pLonli) {
    if (pLonli == null) {
      return;
    }
    lonli = BigDecimal.valueOf(pLonli);
  }
  
  public Integer getLonli() {
    return lonli.intValue();
  }
  
  public void setLonfa(BigDecimal pLonfa) {
    if (pLonfa == null) {
      return;
    }
    lonfa = pLonfa.setScale(DECIMAL_LONFA, RoundingMode.HALF_UP);
  }
  
  public void setLonfa(Integer pLonfa) {
    if (pLonfa == null) {
      return;
    }
    lonfa = BigDecimal.valueOf(pLonfa);
  }
  
  public void setLonfa(Date pLonfa) {
    if (pLonfa == null) {
      return;
    }
    lonfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pLonfa));
  }
  
  public Integer getLonfa() {
    return lonfa.intValue();
  }
  
  public Date getLonfaConvertiEnDate() {
    return ConvertDate.db2ToDate(lonfa.intValue(), null);
  }
}
