/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_TF pour les TF
 */
public class DsTypeFacturation extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier pour les types de facturation.
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "TFETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "TFTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "TFCOD"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TFTF1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFTV1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTP1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TFLI1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTE1"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TFTF2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFTV2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTP2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TFLI2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTE2"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TFTF3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFTV3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTP3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TFLI3"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTE3"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TFTF4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFTV4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTP4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TFLI4"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTE4"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TFTF5"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFTV5"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTP5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TFLI5"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTE5"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "TFTF6"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFTV6"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTP6"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "TFLI6"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFTE6"));
    
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 1), "TFFF1"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 1), "TFFF2"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "TFNCO"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TFPMP"));
    // A contrôler car à l'origine c'est une zone packed
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(18, 0), "TFC"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(5, 3), "TFTXP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(2, 0), "TFRBP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "TFCP"));
    
    length = 300;
  }
}
