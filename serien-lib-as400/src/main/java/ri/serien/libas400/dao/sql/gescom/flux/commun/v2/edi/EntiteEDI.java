/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public abstract class EntiteEDI {
  protected QueryManager query = null;
  // Cette entité est elle valide au format
  // déclarations de fichier ORDERS @GP
  protected final String DECLA_GP = "@GP";
  protected boolean isValide = true;
  protected String msgErreur = "";
  protected String contenuBrut = null;
  protected String[] tabDonnees = null;
  protected final String SEPARATEUR_LIGNE = "\n";
  protected final String SEPARATEUR_CHAMP = ";";
  // VARIABLES
  protected String ETB = null;
  
  // ZONES TYPE DE FICHIER @GP
  // ID de @GP
  protected String id_gp_1 = null;
  // logiciel EDI
  protected String logiciel_edi_2 = null;
  // Type de données ORDERS DESADV, FACTURES...
  protected String type_fichier_3 = null;
  // Format de fichier STANDARD ou SPECIFIQUE
  protected String format_fichier_4 = null;
  // Code EAN émetteur du fichier
  protected String code_emetteur_5 = null;
  // Qualifiant émetteur du fichier
  protected String qualif_emetteur_6 = null;
  // Code EAN destinataire du fichier
  protected String code_destinataire_7 = null;
  // qualifiant EAN destinataire du fichier
  protected String qualif_destinataire_8 = null;
  // Numéro d'interchange
  protected String numero_interchange_9 = null;
  // Date d'interchange
  protected String date_interchange_10 = null;
  // Heure d'interchange
  protected String heure_interchange_11 = null;
  
  protected ParametresFluxBibli parametresFlux = null;
  
  public EntiteEDI(ParametresFluxBibli pParams) {
    parametresFlux = pParams;
  }
  
  /**
   * On découpe le contenu Brut avec un séparateur de champs -> ligne EDI
   */
  protected String[] decoupageDeLigne() {
    if (contenuBrut == null) {
      majError("(EntiteEDI] decoupageDeLigne() contenuBrut NULLE ou SEPARATEUR ");
      return null;
    }
    
    return contenuBrut.split(SEPARATEUR_CHAMP);
  }
  
  /**
   * On découpe une ligne passée en paramètre avec un séparateur de champs -> ligne EDI
   */
  protected String[] decoupageDeLigne(String ligne) {
    if (ligne == null) {
      majError("(EntiteEDI] decoupageDeLigne() ligne NULLE ou SEPARATEUR ");
      return null;
    }
    
    return ligne.split(SEPARATEUR_CHAMP);
  }
  
  /**
   * Mise à jour des infos de type fichier @GP
   */
  protected boolean majDesInfosDeFichier(String ligne) {
    if (ligne == null) {
      majError("(EntiteEDI]majDesInfosDeFichier() ligne NULLE");
      return false;
    }
    
    boolean retour = false;
    
    String[] tabTemp = decoupageDeLigne(ligne);
    
    if (tabTemp != null) {
      int compteur = 0;
      for (int i = 0; i < tabTemp.length; i++) {
        switch (i) {
          case 0:
            id_gp_1 = tabTemp[i];
            compteur++;
            break;
          case 1:
            logiciel_edi_2 = tabTemp[i];
            compteur++;
            break;
          case 2:
            type_fichier_3 = tabTemp[i];
            compteur++;
            break;
          case 3:
            format_fichier_4 = tabTemp[i];
            compteur++;
            break;
          case 4:
            code_emetteur_5 = tabTemp[i];
            compteur++;
            break;
          case 5:
            qualif_emetteur_6 = tabTemp[i];
            compteur++;
            break;
          case 6:
            code_destinataire_7 = tabTemp[i];
            compteur++;
            break;
          case 7:
            qualif_destinataire_8 = tabTemp[i];
            compteur++;
            break;
          case 8:
            numero_interchange_9 = tabTemp[i];
            compteur++;
            break;
          case 9:
            date_interchange_10 = tabTemp[i];
            compteur++;
            break;
          case 10:
            heure_interchange_11 = tabTemp[i];
            compteur++;
            break;
          
          default:
            break;
        }
      }
      // Si toutes les variables ont été mises à jour
      if (compteur == tabTemp.length) {
        retour = true;
      }
      else {
        majError("[EntiteEDI] tabTemp length: " + tabTemp.length + " -> compteur: " + compteur);
      }
      
      retour = controlerCoherenceFichier();
    }
    else {
      majError("[EntiteEDI] tabTemp à NULL ");
    }
    
    return retour;
  }
  
  /**
   * Controle la cohérence des données extraite du fichier EDI
   */
  private boolean controlerCoherenceFichier() {
    if (id_gp_1 == null || !id_gp_1.equals(DECLA_GP)) {
      majError("[EntiteEDI] controlerCoherenceFichier: id_gp_1: " + id_gp_1);
      return false;
    }
    
    if (code_destinataire_7 == null || !code_destinataire_7.equals(parametresFlux.getEAN_founisseur_EDI())) {
      majError("[EntiteEDI] controlerCoherenceFichier: code_destinataire_7: " + code_destinataire_7);
      return false;
    }
    // TODO IL FAUT CONTROLER LA COHERENCE DES AUTRES DONNEES
    
    return true;
  }
  
  /**
   * formater une date EDI @GP en format SERIE N
   */
  public String formaterUneDateEDI_SN(String date) {
    if (date == null) {
      return null;
    }
    
    String retour = date;
    String[] tab = retour.split("/");
    
    if (tab != null && tab.length == 3) {
      if (tab[0].length() == 2 && tab[1].length() == 2 && tab[2].length() == 4) {
        retour = tab[2].substring(2) + tab[1] + tab[0];
      }
    }
    
    return retour;
  }
  
  /**
   * formater une date SERIE N en format @GP
   */
  public String formaterUneDateSN_EDI(String date) {
    if (date == null) {
      majError("[EntiteEDI] formaterUneDateSN_EDI() date nulle");
      return null;
    }
    
    if (date.length() != 7) {
      majError("[EntiteEDI] formaterUneDateSN_EDI() date mauvais format: " + date);
      return null;
    }
    
    String retour = date.substring(5) + "/" + date.substring(3, 5) + "/20" + date.substring(1, 3);
    if (retour.length() != 10) {
      majError("[EntiteEDI] formaterUneDateSN_EDI() retour mauvais format: " + retour);
      return null;
    }
    
    return retour;
  }
  
  /**
   * nettoyer des caractères spéciaux avant INSERT OU UPDATE
   */
  public String nettoyerSaisies(String donnee) {
    if (donnee == null) {
      return donnee;
    }
    
    return (donnee.replace("'", "''")).trim();
  }
  
  /**
   * Construit le message d'erreur proprement
   */
  protected void majError(String nvMessage) {
    if (msgErreur == null) {
      msgErreur = nvMessage;
    }
    else {
      msgErreur += "\n" + nvMessage;
    }
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgError() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    // if(onDetruit)
    msgErreur = null;
    
    return chaine;
  }
  
  protected abstract boolean traitementContenu();
  
  /**
   * ON récupère les infos nécessaires à la définition d'un partenaire EDI
   * Code EAN, NOM et COMPLEMENT pour le moment
   */
  protected GenericRecord recupererInfosPartenaire(String cletb, String clcli, String clliv) {
    if (cletb == null || clcli == null || clliv == null) {
      majError("(EntiteEDI] recupererInfosPartenaire() cletb clcli clliv  NULL ou corrompu ");
      return null;
    }
    
    if (query == null) {
      majError("(EntiteEDI] recupererInfosPartenaire() QUERY NULL ou corrompu ");
      return null;
    }
    
    String requete = " SELECT CLETB,CLCLI,CLLIV,CLNOM,CLCPL,CLCLP,DIGITS(GCGCD) AS GCGCD " + " FROM " + query.getLibrary() + ".PGVMCLIM "
        + " LEFT JOIN " + query.getLibrary() + ".PGVMGCDM ON GCETB = CLETB AND GCTYP = 'C' AND DIGITS(CLCLI)||DIGITS(CLLIV) = GCCOD "
        + " WHERE CLETB = '" + cletb + "' AND CLCLI = '" + clcli + "' AND CLLIV = '" + clliv + "' ";
    
    ArrayList<GenericRecord> liste = query.select(requete);
    if (liste != null) {
      if (liste.size() == 1) {
        return liste.get(0);
      }
      else {
        majError("[EntiteEDI] recupererInfosPartenaire() taille de liste 0 ou > 1 " + liste.size());
        return null;
      }
    }
    else {
      majError("[EntiteEDI] recupererInfosPartenaire() liste nulle pour " + clcli + "/" + clliv + " " + query.getMsgError());
      return null;
    }
  }
  
  /**
   * Permet de tronquer une zone
   */
  public String tronquerUneZone(String chaine, int NbMax) {
    String retour = null;
    if (chaine != null) {
      retour = chaine;
      if (retour.trim().length() > NbMax) {
        retour = retour.trim().substring(0, NbMax);
      }
    }
    
    return retour;
  }
  
  // ACCESSEURS +++++++++++++++++++++++++++++++++++++
  
  public boolean isValide() {
    return isValide;
  }
  
  public void setValide(boolean isValide) {
    this.isValide = isValide;
  }
  
  public String getMsgErreur() {
    return msgErreur;
  }
  
  public void setMsgErreur(String msgErreur) {
    this.msgErreur = msgErreur;
  }
  
  public String getContenuBrut() {
    return contenuBrut;
  }
  
  public void setContenuBrut(String contenuBrut) {
    this.contenuBrut = contenuBrut;
  }
  
  public String getETB() {
    return ETB;
  }
  
  public void setETB(String eTB) {
    ETB = eTB;
  }
  
  public String getId_gp_1() {
    return id_gp_1;
  }
  
  public void setId_gp_1(String id_gp_1) {
    this.id_gp_1 = id_gp_1;
  }
  
  public String getLogiciel_edi_2() {
    return logiciel_edi_2;
  }
  
  public void setLogiciel_edi_2(String logiciel_edi_2) {
    this.logiciel_edi_2 = logiciel_edi_2;
  }
  
  public String getType_fichier_3() {
    return type_fichier_3;
  }
  
  public void setType_fichier_3(String type_fichier_3) {
    this.type_fichier_3 = type_fichier_3;
  }
  
  public String getFormat_fichier_4() {
    return format_fichier_4;
  }
  
  public void setFormat_fichier_4(String format_fichier_4) {
    this.format_fichier_4 = format_fichier_4;
  }
  
  public String getCode_emetteur_5() {
    return code_emetteur_5;
  }
  
  public void setCode_emetteur_5(String code_emetteur_5) {
    this.code_emetteur_5 = code_emetteur_5;
  }
  
  public String getQualif_emetteur_6() {
    return qualif_emetteur_6;
  }
  
  public void setQualif_emetteur_6(String qualif_emetteur_6) {
    this.qualif_emetteur_6 = qualif_emetteur_6;
  }
  
  public String getCode_destinataire_7() {
    return code_destinataire_7;
  }
  
  public void setCode_destinataire_7(String code_destinataire_7) {
    this.code_destinataire_7 = code_destinataire_7;
  }
  
  public String getQualif_destinataire_8() {
    return qualif_destinataire_8;
  }
  
  public void setQualif_destinataire_8(String qualif_destinataire_8) {
    this.qualif_destinataire_8 = qualif_destinataire_8;
  }
  
  public String getNumero_interchange_9() {
    return numero_interchange_9;
  }
  
  public void setNumero_interchange_9(String numero_interchange_9) {
    this.numero_interchange_9 = numero_interchange_9;
  }
  
  public String getDate_interchange_10() {
    return date_interchange_10;
  }
  
  public void setDate_interchange_10(String date_interchange_10) {
    this.date_interchange_10 = date_interchange_10;
  }
  
  public String getHeure_interchange_11() {
    return heure_interchange_11;
  }
  
  public void setHeure_interchange_11(String heure_interchange_11) {
    this.heure_interchange_11 = heure_interchange_11;
  }
  
}
