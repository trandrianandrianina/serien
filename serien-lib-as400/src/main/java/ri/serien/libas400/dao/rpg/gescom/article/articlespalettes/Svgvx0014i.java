/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlespalettes;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0014i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PIARG = 30;
  public static final int SIZE_PICLI = 6;
  public static final int DECIMAL_PICLI = 0;
  public static final int SIZE_PILIV = 3;
  public static final int DECIMAL_PILIV = 0;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PICHAN = 6;
  public static final int DECIMAL_PICHAN = 0;
  public static final int SIZE_PILIG = 3;
  public static final int DECIMAL_PILIG = 0;
  public static final int SIZE_PIPAG = 3;
  public static final int DECIMAL_PIPAG = 0;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 75;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PIARG = 2;
  public static final int VAR_PICLI = 3;
  public static final int VAR_PILIV = 4;
  public static final int VAR_PIMAG = 5;
  public static final int VAR_PICHAN = 6;
  public static final int VAR_PILIG = 7;
  public static final int VAR_PIPAG = 8;
  public static final int VAR_PICOD = 9;
  public static final int VAR_PINUM = 10;
  public static final int VAR_PISUF = 11;
  public static final int VAR_PIARR = 12;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private String piarg = ""; // Argument de recherche
  private BigDecimal picli = BigDecimal.ZERO; // Numéro client OU ZERO
  private BigDecimal piliv = BigDecimal.ZERO; // Suffixe livraison client
  private String pimag = ""; // Magasin ou Blanc pour Stock
  private BigDecimal pichan = BigDecimal.ZERO; // Numéro chantier éventuel
  private BigDecimal pilig = BigDecimal.ZERO; // Nombre de ligne par page
  private BigDecimal pipag = BigDecimal.ZERO; // Numéro de la page
  private String picod = ""; // Code du document
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro du document
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe du document
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400Text(SIZE_PIARG), // Argument de recherche
      new AS400ZonedDecimal(SIZE_PICLI, DECIMAL_PICLI), // Numéro client OU ZERO
      new AS400ZonedDecimal(SIZE_PILIV, DECIMAL_PILIV), // Suffixe livraison client
      new AS400Text(SIZE_PIMAG), // Magasin ou Blanc pour Stock
      new AS400ZonedDecimal(SIZE_PICHAN, DECIMAL_PICHAN), // Numéro chantier éventuel
      new AS400ZonedDecimal(SIZE_PILIG, DECIMAL_PILIG), // Nombre de ligne par page
      new AS400ZonedDecimal(SIZE_PIPAG, DECIMAL_PIPAG), // Numéro de la page
      new AS400Text(SIZE_PICOD), // Code du document
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro du document
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe du document
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { piind, pietb, piarg, picli, piliv, pimag, pichan, pilig, pipag, picod, pinum, pisuf, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    piarg = (String) output[2];
    picli = (BigDecimal) output[3];
    piliv = (BigDecimal) output[4];
    pimag = (String) output[5];
    pichan = (BigDecimal) output[6];
    pilig = (BigDecimal) output[7];
    pipag = (BigDecimal) output[8];
    picod = (String) output[9];
    pinum = (BigDecimal) output[10];
    pisuf = (BigDecimal) output[11];
    piarr = (String) output[12];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPiarg(String pPiarg) {
    if (pPiarg == null) {
      return;
    }
    piarg = pPiarg;
  }
  
  public String getPiarg() {
    return piarg;
  }
  
  public void setPicli(BigDecimal pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = pPicli.setScale(DECIMAL_PICLI, RoundingMode.HALF_UP);
  }
  
  public void setPicli(Integer pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = BigDecimal.valueOf(pPicli);
  }
  
  public Integer getPicli() {
    return picli.intValue();
  }
  
  public void setPiliv(BigDecimal pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = pPiliv.setScale(DECIMAL_PILIV, RoundingMode.HALF_UP);
  }
  
  public void setPiliv(Integer pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = BigDecimal.valueOf(pPiliv);
  }
  
  public Integer getPiliv() {
    return piliv.intValue();
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPichan(BigDecimal pPichan) {
    if (pPichan == null) {
      return;
    }
    pichan = pPichan.setScale(DECIMAL_PICHAN, RoundingMode.HALF_UP);
  }
  
  public void setPichan(Integer pPichan) {
    if (pPichan == null) {
      return;
    }
    pichan = BigDecimal.valueOf(pPichan);
  }
  
  public Integer getPichan() {
    return pichan.intValue();
  }
  
  public void setPilig(BigDecimal pPilig) {
    if (pPilig == null) {
      return;
    }
    pilig = pPilig.setScale(DECIMAL_PILIG, RoundingMode.HALF_UP);
  }
  
  public void setPilig(Integer pPilig) {
    if (pPilig == null) {
      return;
    }
    pilig = BigDecimal.valueOf(pPilig);
  }
  
  public Integer getPilig() {
    return pilig.intValue();
  }
  
  public void setPipag(BigDecimal pPipag) {
    if (pPipag == null) {
      return;
    }
    pipag = pPipag.setScale(DECIMAL_PIPAG, RoundingMode.HALF_UP);
  }
  
  public void setPipag(Integer pPipag) {
    if (pPipag == null) {
      return;
    }
    pipag = BigDecimal.valueOf(pPipag);
  }
  
  public Integer getPipag() {
    return pipag.intValue();
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
