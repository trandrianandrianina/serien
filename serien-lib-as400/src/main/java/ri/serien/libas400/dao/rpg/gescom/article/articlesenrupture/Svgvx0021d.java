/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.articlesenrupture;

import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

public class Svgvx0021d {
  // Constantes
  public static final int SIZE_WART = 20;
  public static final int SIZE_WLIB = 30;
  public static final int SIZE_WUCA = 2;
  public static final int SIZE_WDCC = 1;
  public static final int DECIMAL_WDCC = 0;
  public static final int SIZE_WSPE = 1;
  public static final int DECIMAL_WSPE = 0;
  public static final int SIZE_WABC = 1;
  public static final int SIZE_WQTC = 11;
  public static final int DECIMAL_WQTC = 3;
  public static final int SIZE_WSTK = 11;
  public static final int DECIMAL_WSTK = 3;
  public static final int SIZE_WRES = 11;
  public static final int DECIMAL_WRES = 3;
  public static final int SIZE_WATT = 11;
  public static final int DECIMAL_WATT = 3;
  public static final int SIZE_WDIA = 11;
  public static final int DECIMAL_WDIA = 3;
  public static final int SIZE_WMAX = 11;
  public static final int DECIMAL_WMAX = 3;
  public static final int SIZE_WMIN = 11;
  public static final int DECIMAL_WMIN = 3;
  public static final int SIZE_WIDEAL = 11;
  public static final int DECIMAL_WIDEAL = 3;
  public static final int SIZE_WLIB2 = 30;
  public static final int SIZE_WLIB3 = 30;
  public static final int SIZE_WLIB4 = 30;
  public static final int SIZE_TOTALE_DS = 233;
  public static final int SIZE_FILLER = 250 - 233;
  
  // Constantes indices Nom DS
  public static final int VAR_WART = 0;
  public static final int VAR_WLIB = 1;
  public static final int VAR_WUCA = 2;
  public static final int VAR_WDCC = 3;
  public static final int VAR_WSPE = 4;
  public static final int VAR_WABC = 5;
  public static final int VAR_WQTC = 6;
  public static final int VAR_WSTK = 7;
  public static final int VAR_WRES = 8;
  public static final int VAR_WATT = 9;
  public static final int VAR_WDIA = 10;
  public static final int VAR_WMAX = 11;
  public static final int VAR_WMIN = 12;
  public static final int VAR_WIDEAL = 13;
  public static final int VAR_WLIB2 = 14;
  public static final int VAR_WLIB3 = 15;
  public static final int VAR_WLIB4 = 16;
  
  // Variables AS400
  private String wart = ""; // Code article
  private String wlib = ""; // Libellé article
  private String wuca = ""; // Code unité de commande achat
  private BigDecimal wdcc = BigDecimal.ZERO; // Nombre de décimales de l uni
  private BigDecimal wspe = BigDecimal.ZERO; // Code spécial, = 1 SPÉCIAL
  private String wabc = ""; // Zone ABC, R=HORS GAMME
  private BigDecimal wqtc = BigDecimal.ZERO; // Quantité commandée
  private BigDecimal wstk = BigDecimal.ZERO; // Quantité stock
  private BigDecimal wres = BigDecimal.ZERO; // Quantité réservée
  private BigDecimal watt = BigDecimal.ZERO; // Quantité attendue
  private BigDecimal wdia = BigDecimal.ZERO; // Quantité disponible Achat
  private BigDecimal wmax = BigDecimal.ZERO; // Quantité stock maximum
  private BigDecimal wmin = BigDecimal.ZERO; // Quantité stock minimum
  private BigDecimal wideal = BigDecimal.ZERO; // Quantité stock idéal
  private String wlib2 = ""; // Libellé article 2
  private String wlib3 = ""; // Libellé article 3
  private String wlib4 = ""; // Libellé article 4
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WART), // Code article
      new AS400Text(SIZE_WLIB), // Libellé article
      new AS400Text(SIZE_WUCA), // Code unité de commande achat
      new AS400ZonedDecimal(SIZE_WDCC, DECIMAL_WDCC), // Nombre de décimales de l uni
      new AS400ZonedDecimal(SIZE_WSPE, DECIMAL_WSPE), // Code spécial, = 1 SPÉCIAL
      new AS400Text(SIZE_WABC), // Zone ABC, R=HORS GAMME
      new AS400ZonedDecimal(SIZE_WQTC, DECIMAL_WQTC), // Quantité commandée
      new AS400ZonedDecimal(SIZE_WSTK, DECIMAL_WSTK), // Quantité stock
      new AS400ZonedDecimal(SIZE_WRES, DECIMAL_WRES), // Quantité réservée
      new AS400ZonedDecimal(SIZE_WATT, DECIMAL_WATT), // Quantité attendue
      new AS400ZonedDecimal(SIZE_WDIA, DECIMAL_WDIA), // Quantité disponible Achat
      new AS400ZonedDecimal(SIZE_WMAX, DECIMAL_WMAX), // Quantité stock maximum
      new AS400ZonedDecimal(SIZE_WMIN, DECIMAL_WMIN), // Quantité stock minimum
      new AS400ZonedDecimal(SIZE_WIDEAL, DECIMAL_WIDEAL), // Quantité stock idéal
      new AS400Text(SIZE_WLIB2), // Libellé article 2
      new AS400Text(SIZE_WLIB3), // Libellé article 3
      new AS400Text(SIZE_WLIB4), // Libellé article 4
      new AS400Text(SIZE_FILLER), // Filler
  };
  public Object[] o =
      { wart, wlib, wuca, wdcc, wspe, wabc, wqtc, wstk, wres, watt, wdia, wmax, wmin, wideal, wlib2, wlib3, wlib4, filler, };
  
  // -- Accesseurs
  
  public void setWart(String pWart) {
    if (pWart == null) {
      return;
    }
    wart = pWart;
  }
  
  public String getWart() {
    return wart;
  }
  
  public void setWlib(String pWlib) {
    if (pWlib == null) {
      return;
    }
    wlib = pWlib;
  }
  
  public String getWlib() {
    return wlib;
  }
  
  public void setWuca(String pWuca) {
    if (pWuca == null) {
      return;
    }
    wuca = pWuca;
  }
  
  public String getWuca() {
    return wuca;
  }
  
  public void setWdcc(BigDecimal pWdcc) {
    if (pWdcc == null) {
      return;
    }
    wdcc = pWdcc.setScale(DECIMAL_WDCC, RoundingMode.HALF_UP);
  }
  
  public void setWdcc(Integer pWdcc) {
    if (pWdcc == null) {
      return;
    }
    wdcc = BigDecimal.valueOf(pWdcc);
  }
  
  public Integer getWdcc() {
    return wdcc.intValue();
  }
  
  public void setWspe(BigDecimal pWspe) {
    if (pWspe == null) {
      return;
    }
    wspe = pWspe.setScale(DECIMAL_WSPE, RoundingMode.HALF_UP);
  }
  
  public void setWspe(Integer pWspe) {
    if (pWspe == null) {
      return;
    }
    wspe = BigDecimal.valueOf(pWspe);
  }
  
  public Integer getWspe() {
    return wspe.intValue();
  }
  
  public void setWabc(Character pWabc) {
    if (pWabc == null) {
      return;
    }
    wabc = String.valueOf(pWabc);
  }
  
  public Character getWabc() {
    return wabc.charAt(0);
  }
  
  public void setWqtc(BigDecimal pWqtc) {
    if (pWqtc == null) {
      return;
    }
    wqtc = pWqtc.setScale(DECIMAL_WQTC, RoundingMode.HALF_UP);
  }
  
  public void setWqtc(Double pWqtc) {
    if (pWqtc == null) {
      return;
    }
    wqtc = BigDecimal.valueOf(pWqtc).setScale(DECIMAL_WQTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqtc() {
    return wqtc.setScale(DECIMAL_WQTC, RoundingMode.HALF_UP);
  }
  
  public void setWstk(BigDecimal pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = pWstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWstk(Double pWstk) {
    if (pWstk == null) {
      return;
    }
    wstk = BigDecimal.valueOf(pWstk).setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWstk() {
    return wstk.setScale(DECIMAL_WSTK, RoundingMode.HALF_UP);
  }
  
  public void setWres(BigDecimal pWres) {
    if (pWres == null) {
      return;
    }
    wres = pWres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWres(Double pWres) {
    if (pWres == null) {
      return;
    }
    wres = BigDecimal.valueOf(pWres).setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWres() {
    return wres.setScale(DECIMAL_WRES, RoundingMode.HALF_UP);
  }
  
  public void setWatt(BigDecimal pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = pWatt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWatt(Double pWatt) {
    if (pWatt == null) {
      return;
    }
    watt = BigDecimal.valueOf(pWatt).setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWatt() {
    return watt.setScale(DECIMAL_WATT, RoundingMode.HALF_UP);
  }
  
  public void setWdia(BigDecimal pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = pWdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWdia(Double pWdia) {
    if (pWdia == null) {
      return;
    }
    wdia = BigDecimal.valueOf(pWdia).setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWdia() {
    return wdia.setScale(DECIMAL_WDIA, RoundingMode.HALF_UP);
  }
  
  public void setWmax(BigDecimal pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = pWmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWmax(Double pWmax) {
    if (pWmax == null) {
      return;
    }
    wmax = BigDecimal.valueOf(pWmax).setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmax() {
    return wmax.setScale(DECIMAL_WMAX, RoundingMode.HALF_UP);
  }
  
  public void setWmin(BigDecimal pWmin) {
    if (pWmin == null) {
      return;
    }
    wmin = pWmin.setScale(DECIMAL_WMIN, RoundingMode.HALF_UP);
  }
  
  public void setWmin(Double pWmin) {
    if (pWmin == null) {
      return;
    }
    wmin = BigDecimal.valueOf(pWmin).setScale(DECIMAL_WMIN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWmin() {
    return wmin.setScale(DECIMAL_WMIN, RoundingMode.HALF_UP);
  }
  
  public void setWideal(BigDecimal pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = pWideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWideal(Double pWideal) {
    if (pWideal == null) {
      return;
    }
    wideal = BigDecimal.valueOf(pWideal).setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWideal() {
    return wideal.setScale(DECIMAL_WIDEAL, RoundingMode.HALF_UP);
  }
  
  public void setWlib2(String pWlib2) {
    if (pWlib2 == null) {
      return;
    }
    wlib2 = pWlib2;
  }
  
  public String getWlib2() {
    return wlib2;
  }
  
  public void setWlib3(String pWlib3) {
    if (pWlib3 == null) {
      return;
    }
    wlib3 = pWlib3;
  }
  
  public String getWlib3() {
    return wlib3;
  }
  
  public void setWlib4(String pWlib4) {
    if (pWlib4 == null) {
      return;
    }
    wlib4 = pWlib4;
  }
  
  public String getWlib4() {
    return wlib4;
  }
}
