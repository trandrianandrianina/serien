/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.programs.contact;

import java.util.ArrayList;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;

public class GM_ContactExtension extends BaseGroupDB {
  /**
   * Constructeur
   * @param aquerymg
   */
  public GM_ContactExtension(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourne l'extension d'un contact à partir de son id
   * @param id
   * @return
   * 
   */
  public M_ContactExtension readOneContact(int id) {
    if (id < 1) {
      msgErreur += "\nL'id (RYNUM) est inférieur à 1.";
      return null;
    }
    if (queryManager.getLibrary() == null) {
      msgErreur += "\nLa CURLIB n'est pas initialisée.";
      return null;
    }
    
    ArrayList<GenericRecord> listrcd = queryManager.select("select * from " + queryManager.getLibrary() + ".PSEMRTYM where RYNUM = " + id);
    if (listrcd == null) {
      return null;
    }
    // zones venant du PSEMRTYM
    M_ContactExtension ec = new M_ContactExtension(queryManager);
    ec.initObject(listrcd.get(0), true);
    
    return ec;
  }
  
  /**
   * Retourne l'extension d'un contact à partir de son alias (RYSRVM)
   * @param id
   * @return
   * 
   *         public M_ContactExtension readOneContact(String alias){
   *         if( (alias == null) || (alias.trim().length() == 0) ){
   *         msgErreur += "\nL'alias (REPRF) est inférieur null ou blanc.";
   *         return null;
   *         }
   *         if( querymg.getLibrary() == null ){
   *         msgErreur += "\nLa CURLIB n'est pas initialisée.";
   *         return null;
   *         }
   * 
   *         ArrayList<GenericRecord> listrcd = querymg.select("select * from " + querymg.getLibrary() + ".PSEMRTYM
   *         where RYSRVM = '" + alias + "'");
   *         if( listrcd == null ){
   *         return null;
   *         }
   *         // zones venant du PSEMRTYM
   *         M_ContactExtension ec = new M_ContactExtension(querymg);
   *         ec.initObject(listrcd.get(0), true);
   * 
   *         return ec;
   *         }
   */
  
  /*
  public InfosMailServer getInfosMailServer()
  {
    // Récupération de l'alias dans le fichier PSEMRTYM
    M_ContactExtension extcontact = new M_ContactExtension(querymg);
    String alias = extcontact.getRYMAIL();
  }*/
  
  @Override
  public void dispose() {
    queryManager = null;
  }
  
}
