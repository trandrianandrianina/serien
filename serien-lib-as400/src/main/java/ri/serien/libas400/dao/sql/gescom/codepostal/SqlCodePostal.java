/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.codepostal;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.codepostal.CodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.CriteresCodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.IdCodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.ListeCodePostal;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlCodePostal {
  
  // Variables
  protected QueryManager querymg = null;
  
  /**
   * Constructeur.
   */
  public SqlCodePostal(QueryManager aquerymg) {
    querymg = aquerymg;
  }
  
  /**
   * Retourne la liste des code postaux pour un établissement donnée.
   */
  public ListeCodePostal chargerListeCodePostal(CriteresCodePostal pCodePostal) {
    if (pCodePostal == null) {
      return null;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    // La liste avec les noms des villes étant plus longue on ne fait la recherche seulement si nécessaire
    if (!pCodePostal.getNomVille().isEmpty()) {
      requeteSql.ajouter("Select DISTINCT RCCDP, RCDEP,RCNOM from " + querymg.getLibrary() + '.' + EnumTableBDD.COMMUNE
          + " WHERE RCNOM = '" + pCodePostal.getNomVille() + "' ORDER BY RCCDP");
    }
    else {
      requeteSql.ajouter("Select DISTINCT RCCDP, RCDEP from " + querymg.getLibrary() + '.' + EnumTableBDD.COMMUNE + " ORDER BY RCCDP");
    }
    
    ArrayList<GenericRecord> listeRecord = querymg.select(requeteSql.getRequeteLectureSeule());
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente la liste avec les enregistrements trouvés
    ListeCodePostal listeCodePostal = new ListeCodePostal();
    
    for (GenericRecord record : listeRecord) {
      if (record != null) {
        listeCodePostal.add(initCodePostal(record));
      }
    }
    return listeCodePostal;
  }
  
  /**
   * Initialise un code postal à partir de l'enregistrement donnée et du type de la recherche.
   */
  private CodePostal initCodePostal(GenericRecord pRecord) {
    CodePostal codepostal = null;
    
    IdCodePostal idCodePostal = IdCodePostal.getInstance(pRecord.getIntegerValue("RCCDP"));
    codepostal = new CodePostal(idCodePostal);
    codepostal.setDepartement((pRecord.getIntegerValue("RCDEP")));
    try {
      codepostal.setNomVille((pRecord.getStringValue("RCNOM")));
    }
    catch (Exception e) {
      // Ne rien faire si le record n'a aucun nom de ville (évite un plantage)
    }
    
    return codepostal;
  }
}
