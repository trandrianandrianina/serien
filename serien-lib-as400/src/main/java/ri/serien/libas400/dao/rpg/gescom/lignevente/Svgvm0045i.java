/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0045i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PITRT = 1;
  public static final int DECIMAL_PITRT = 0;
  public static final int SIZE_PICODO = 1;
  public static final int SIZE_PIETBO = 3;
  public static final int SIZE_PINUMO = 6;
  public static final int DECIMAL_PINUMO = 0;
  public static final int SIZE_PISUFO = 1;
  public static final int DECIMAL_PISUFO = 0;
  public static final int SIZE_PINLIO = 4;
  public static final int DECIMAL_PINLIO = 0;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINLI = 4;
  public static final int DECIMAL_PINLI = 0;
  public static final int SIZE_PIQTE = 11;
  public static final int DECIMAL_PIQTE = 3;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 53;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PITRT = 1;
  public static final int VAR_PICODO = 2;
  public static final int VAR_PIETBO = 3;
  public static final int VAR_PINUMO = 4;
  public static final int VAR_PISUFO = 5;
  public static final int VAR_PINLIO = 6;
  public static final int VAR_PICOD = 7;
  public static final int VAR_PIETB = 8;
  public static final int VAR_PINUM = 9;
  public static final int VAR_PISUF = 10;
  public static final int VAR_PINLI = 11;
  public static final int VAR_PIQTE = 12;
  public static final int VAR_PIARR = 13;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private BigDecimal pitrt = BigDecimal.ZERO; // type de traitement
  private String picodo = ""; // Code ERL
  private String pietbo = ""; // Code Etablissement
  private BigDecimal pinumo = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisufo = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal pinlio = BigDecimal.ZERO; // Numéro de Ligne
  private String picod = ""; // Code ERL
  private String pietb = ""; // Code Etablissement
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal pinli = BigDecimal.ZERO; // Numéro de Ligne ou Zéro
  private BigDecimal piqte = BigDecimal.ZERO; // Quantité demandée
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PITRT, DECIMAL_PITRT), // type de traitement
      new AS400Text(SIZE_PICODO), // Code ERL
      new AS400Text(SIZE_PIETBO), // Code Etablissement
      new AS400ZonedDecimal(SIZE_PINUMO, DECIMAL_PINUMO), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUFO, DECIMAL_PISUFO), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_PINLIO, DECIMAL_PINLIO), // Numéro de Ligne
      new AS400Text(SIZE_PICOD), // Code ERL
      new AS400Text(SIZE_PIETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_PINLI, DECIMAL_PINLI), // Numéro de Ligne ou Zéro
      new AS400ZonedDecimal(SIZE_PIQTE, DECIMAL_PIQTE), // Quantité demandée
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, pitrt, picodo, pietbo, pinumo, pisufo, pinlio, picod, pietb, pinum, pisuf, pinli, piqte, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pitrt = (BigDecimal) output[1];
    picodo = (String) output[2];
    pietbo = (String) output[3];
    pinumo = (BigDecimal) output[4];
    pisufo = (BigDecimal) output[5];
    pinlio = (BigDecimal) output[6];
    picod = (String) output[7];
    pietb = (String) output[8];
    pinum = (BigDecimal) output[9];
    pisuf = (BigDecimal) output[10];
    pinli = (BigDecimal) output[11];
    piqte = (BigDecimal) output[12];
    piarr = (String) output[13];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPitrt(BigDecimal pPitrt) {
    if (pPitrt == null) {
      return;
    }
    pitrt = pPitrt.setScale(DECIMAL_PITRT, RoundingMode.HALF_UP);
  }
  
  public void setPitrt(Integer pPitrt) {
    if (pPitrt == null) {
      return;
    }
    pitrt = BigDecimal.valueOf(pPitrt);
  }
  
  public Integer getPitrt() {
    return pitrt.intValue();
  }
  
  public void setPicodo(Character pPicodo) {
    if (pPicodo == null) {
      return;
    }
    picodo = String.valueOf(pPicodo);
  }
  
  public Character getPicodo() {
    return picodo.charAt(0);
  }
  
  public void setPietbo(String pPietbo) {
    if (pPietbo == null) {
      return;
    }
    pietbo = pPietbo;
  }
  
  public String getPietbo() {
    return pietbo;
  }
  
  public void setPinumo(BigDecimal pPinumo) {
    if (pPinumo == null) {
      return;
    }
    pinumo = pPinumo.setScale(DECIMAL_PINUMO, RoundingMode.HALF_UP);
  }
  
  public void setPinumo(Integer pPinumo) {
    if (pPinumo == null) {
      return;
    }
    pinumo = BigDecimal.valueOf(pPinumo);
  }
  
  public Integer getPinumo() {
    return pinumo.intValue();
  }
  
  public void setPisufo(BigDecimal pPisufo) {
    if (pPisufo == null) {
      return;
    }
    pisufo = pPisufo.setScale(DECIMAL_PISUFO, RoundingMode.HALF_UP);
  }
  
  public void setPisufo(Integer pPisufo) {
    if (pPisufo == null) {
      return;
    }
    pisufo = BigDecimal.valueOf(pPisufo);
  }
  
  public Integer getPisufo() {
    return pisufo.intValue();
  }
  
  public void setPinlio(BigDecimal pPinlio) {
    if (pPinlio == null) {
      return;
    }
    pinlio = pPinlio.setScale(DECIMAL_PINLIO, RoundingMode.HALF_UP);
  }
  
  public void setPinlio(Integer pPinlio) {
    if (pPinlio == null) {
      return;
    }
    pinlio = BigDecimal.valueOf(pPinlio);
  }
  
  public Integer getPinlio() {
    return pinlio.intValue();
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinli(BigDecimal pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = pPinli.setScale(DECIMAL_PINLI, RoundingMode.HALF_UP);
  }
  
  public void setPinli(Integer pPinli) {
    if (pPinli == null) {
      return;
    }
    pinli = BigDecimal.valueOf(pPinli);
  }
  
  public Integer getPinli() {
    return pinli.intValue();
  }
  
  public void setPiqte(BigDecimal pPiqte) {
    if (pPiqte == null) {
      return;
    }
    piqte = pPiqte.setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public void setPiqte(Double pPiqte) {
    if (pPiqte == null) {
      return;
    }
    piqte = BigDecimal.valueOf(pPiqte).setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqte() {
    return piqte.setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
