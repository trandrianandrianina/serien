/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.representant.recherche;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.commun.representant.CriteresRechercheRepresentant;

public class RequetesRechercheRepresentants {
  
  /**
   * Retourne la requête en fonction du type de recherche.
   */
  public static String getRequete(String curlib, CriteresRechercheRepresentant criteres) {
    String requete = null;
    if (criteres == null) {
      return requete;
    }
    
    switch (criteres.getTypeRecherche()) {
      case CriteresRechercheRepresentant.RECHERCHE_COMPTOIR:
        requete = getRequeteRechercheRepresentantComptoir(curlib, criteres);
        break;
    }
    
    return ajouterPagination(requete, criteres);
  }
  
  /**
   * Construit la requête spécifique pour le comptoir.
   * Critères: code etablissement, code representant, civilité, nom
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   * Retour : Voir classe ModeleComptoir
   */
  private static String getRequeteRechercheRepresentantComptoir(String curlib, CriteresRechercheRepresentant criteres) {
    return "select row_number() over() as numrow, rep.RPETB, rep.RPREP, rep.RPCIV, rep.RPNOM from " + curlib + '.'
        + EnumTableBDD.REPRESENTANT + " rep where rep.RPETB = '" + criteres.getCodeEtablissement() + "' ORDER BY RPNOM";
  }
  
  /**
   * Ajoute les ordres afin de récupérer qu'une quantité limité de lignes pour la pagination.
   */
  private static String ajouterPagination(String requete, CriteresBaseRecherche criteres) {
    if (criteres.getNbrLignesParPage() != CriteresBaseRecherche.TOUTES_LES_LIGNES) {
      int nbreltpage = criteres.getNbrLignesParPage();
      int indexdeb = (criteres.getNumeroPage() * nbreltpage) - (nbreltpage - 1);
      int indexfin = (indexdeb + nbreltpage) - 1;
      requete = "with vue as (" + requete + ") select * from vue where numrow between " + indexdeb + " and " + indexfin;
    }
    return requete;
  }
}
