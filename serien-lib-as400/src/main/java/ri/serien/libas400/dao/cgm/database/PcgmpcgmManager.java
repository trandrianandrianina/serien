/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.cgm.database;

import java.sql.Connection;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PcgmpcgmManager extends QueryManager {
  /**
   * Constructeur
   * @param database
   */
  public PcgmpcgmManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne l'enregistrement pour un environnement
   * @param bibenv
   * @param prf
   * @return
   ** 
   *         public ArrayList<Pgvmparm_dsGR> getRecords()
   *         {
   *         //Pgvmparm_dsGR record = null;
   *         //final ArrayList<Pgvmparm_dsGR> listeRecord =
   *         return (ArrayList<Pgvmparm_dsGR>) select("Select * from "+getBibliotheque()+".PGVMPARM where PARTYP =
   *         'GR'", "gvx.database.Pgvmparm_dsGR");
   *
   *         //if ((listeRecord != null) && (listeRecord.size() > 0))
   *         // record = (Pgvmparm_dsGR)listeRecord.get(0);
   *         //else
   *         // msgErreur += "\n[PgvmparmManagement] (getRecordForBibEnv) Enregistrements non trouvés pour le type GR";
   *        
   *         //return record;
   *         }
   * 
   */
  
  public void getRecords() {
    try {
      select("select * from " + getLibrary() + ".pcgmpcgm where G1NCG = 101000 and G1SOC = 'DEM'");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
    
    // byte[] data = {8,7,0,3,2,8};
    // AS400PackedDecimal pdconverter = new AS400PackedDecimal(4, 0);
    // long result = ((BigDecimal) pdconverter.toObject(data)).longValue();
    
  }
}
