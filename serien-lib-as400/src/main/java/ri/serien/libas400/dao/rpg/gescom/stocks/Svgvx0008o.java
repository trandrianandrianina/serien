/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvx0008o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POLI01 = 200;
  public static final int SIZE_POLI02 = 200;
  public static final int SIZE_POLI03 = 200;
  public static final int SIZE_POLI04 = 200;
  public static final int SIZE_POLI05 = 200;
  public static final int SIZE_POLI06 = 200;
  public static final int SIZE_POLI07 = 200;
  public static final int SIZE_POLI08 = 200;
  public static final int SIZE_POLI09 = 200;
  public static final int SIZE_POLI10 = 200;
  public static final int SIZE_POLI11 = 200;
  public static final int SIZE_POLI12 = 200;
  public static final int SIZE_POLI13 = 200;
  public static final int SIZE_POLI14 = 200;
  public static final int SIZE_POLI15 = 200;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 3011;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POLI01 = 1;
  public static final int VAR_POLI02 = 2;
  public static final int VAR_POLI03 = 3;
  public static final int VAR_POLI04 = 4;
  public static final int VAR_POLI05 = 5;
  public static final int VAR_POLI06 = 6;
  public static final int VAR_POLI07 = 7;
  public static final int VAR_POLI08 = 8;
  public static final int VAR_POLI09 = 9;
  public static final int VAR_POLI10 = 10;
  public static final int VAR_POLI11 = 11;
  public static final int VAR_POLI12 = 12;
  public static final int VAR_POLI13 = 13;
  public static final int VAR_POLI14 = 14;
  public static final int VAR_POLI15 = 15;
  public static final int VAR_POARR = 16;
  
  private Svgvx0008d[] dsd = { new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(),
      new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(), new Svgvx0008d(),
      new Svgvx0008d(), new Svgvx0008d(), };
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private Object[] poli01 = dsd[0].o; // Zones Ligne 1
  private Object[] poli02 = dsd[1].o; // Zones Ligne 2
  private Object[] poli03 = dsd[2].o; // Zones Ligne 3
  private Object[] poli04 = dsd[3].o; // Zones Ligne 4
  private Object[] poli05 = dsd[4].o; // Zones Ligne 5
  private Object[] poli06 = dsd[5].o; // Zones Ligne 6
  private Object[] poli07 = dsd[6].o; // Zones Ligne 7
  private Object[] poli08 = dsd[7].o; // Zones Ligne 8
  private Object[] poli09 = dsd[8].o; // Zones Ligne 9
  private Object[] poli10 = dsd[9].o; // Zones Ligne 10
  private Object[] poli11 = dsd[10].o; // Zones Ligne 11
  private Object[] poli12 = dsd[11].o; // Zones Ligne 12
  private Object[] poli13 = dsd[12].o; // Zones Ligne 13
  private Object[] poli14 = dsd[13].o; // Zones Ligne 14
  private Object[] poli15 = dsd[14].o; // Zones Ligne 15
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Structure(dsd[0].structure), // Zones Ligne 1
      new AS400Structure(dsd[1].structure), // Zones Ligne 2
      new AS400Structure(dsd[2].structure), // Zones Ligne 3
      new AS400Structure(dsd[3].structure), // Zones Ligne 4
      new AS400Structure(dsd[4].structure), // Zones Ligne 5
      new AS400Structure(dsd[5].structure), // Zones Ligne 6
      new AS400Structure(dsd[6].structure), // Zones Ligne 7
      new AS400Structure(dsd[7].structure), // Zones Ligne 8
      new AS400Structure(dsd[8].structure), // Zones Ligne 9
      new AS400Structure(dsd[9].structure), // Zones Ligne 10
      new AS400Structure(dsd[10].structure), // Zones Ligne 11
      new AS400Structure(dsd[11].structure), // Zones Ligne 12
      new AS400Structure(dsd[12].structure), // Zones Ligne 13
      new AS400Structure(dsd[13].structure), // Zones Ligne 14
      new AS400Structure(dsd[14].structure), // Zones Ligne 15
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, poli01, poli02, poli03, poli04, poli05, poli06, poli07, poli08, poli09, poli10, poli11, poli12, poli13,
          poli14, poli15, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400 et leur redécoupage.
   */
  public void setDSOutput() {
    byte[] as400out = getOutputData();
    int offset = 0;
    poind = (String) ds.getMembers(0).toObject(as400out, offset);
    offset += SIZE_POIND;
    poli01 = convertTObject(as400out, offset, 1);
    offset += SIZE_POLI01;
    poli02 = convertTObject(as400out, offset, 2);
    offset += SIZE_POLI02;
    poli03 = convertTObject(as400out, offset, 3);
    offset += SIZE_POLI03;
    poli04 = convertTObject(as400out, offset, 4);
    offset += SIZE_POLI04;
    poli05 = convertTObject(as400out, offset, 5);
    offset += SIZE_POLI05;
    poli06 = convertTObject(as400out, offset, 6);
    offset += SIZE_POLI06;
    poli07 = convertTObject(as400out, offset, 7);
    offset += SIZE_POLI07;
    poli08 = convertTObject(as400out, offset, 8);
    offset += SIZE_POLI08;
    poli09 = convertTObject(as400out, offset, 9);
    offset += SIZE_POLI09;
    poli10 = convertTObject(as400out, offset, 10);
    offset += SIZE_POLI10;
    poli11 = convertTObject(as400out, offset, 11);
    offset += SIZE_POLI11;
    poli12 = convertTObject(as400out, offset, 12);
    offset += SIZE_POLI12;
    poli13 = convertTObject(as400out, offset, 13);
    offset += SIZE_POLI13;
    poli14 = convertTObject(as400out, offset, 14);
    offset += SIZE_POLI14;
    poli15 = convertTObject(as400out, offset, 15);
    offset += SIZE_POLI15;
    poarr = (String) ds.getMembers(16).toObject(as400out, offset);
    offset += SIZE_POARR;
  }
  
  /**
   * Retourne un tableau des valeurs correspondants aux lignes.
   */
  public Object[] getValeursBrutesLignes() {
    Object[] o =
        { poli01, poli02, poli03, poli04, poli05, poli06, poli07, poli08, poli09, poli10, poli11, poli12, poli13, poli14, poli15, };
    controlerProblemeConversionElement(o);
    return o;
  }
  
  /**
   * Convertit les valeurs de la datastructure.
   */
  private Object[] convertTObject(byte[] out, int offset, int indice) {
    try {
      return (Object[]) ds.getMembers(indice).toObject(out, offset);
    }
    catch (NumberFormatException e) {
      return null;
    }
  }
  
  /**
   * Permet de contrôler qu'un élément du tableau ne contient pas une erreur de données décimale lors de la conversion RPG -> Java.
   */
  private void controlerProblemeConversionElement(Object[] pTableau) {
    if (pTableau == null) {
      return;
    }
    for (int i = 0; i < pTableau.length; i++) {
      int j = i + 1;
      if (j < pTableau.length && pTableau[i] == null && pTableau[j] != null) {
        throw new MessageErreurException(
            "Il y a eu un problème lors de la conversion (RPG -> Java) des données à l'indice " + i + " du tableau. ");
      }
    }
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
