/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Locale;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.Stock;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Stock Série N au format SFCC
 * Cette classe sert à l'intégration JAVA vers JSON
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonStockWebV3 extends JsonEntiteMagento {
  // Variables
  private ArrayList<JsonStockArticleV3> listeStocks = null;
  
  /**
   * Constructeur.
   */
  public JsonStockWebV3(FluxMagento pFluxMagento) {
    idFlux = pFluxMagento.getFLIDF();
    versionFlux = pFluxMagento.getFLVER();
    etb = pFluxMagento.getFLETB();
    
    listeStocks = new ArrayList<JsonStockArticleV3>();
  }
  
  /**
   * Permet de mettre à jour la liste des stock SFCC.
   */
  public void mettreAJourListeStock(ListeStock pListeStock) {
    if (pListeStock == null || pListeStock.isEmpty()) {
      return;
    }
    
    // Chargement de la liste des stocks à partir des données chargées en base
    for (Stock stock : pListeStock) {
      if (stock == null) {
        continue;
      }
      
      JsonStockArticleV3 article = new JsonStockArticleV3();
      // Code article
      article.setCodeArticle(stock.getId().getIdArticle().getCodeArticle());
      // Stock avant rupture
      BigDecimal valeur = stock.getStockAvantRupture();
      if (valeur == null) {
        article.setstockAvantRupture("0");
      }
      else {
        article.setstockAvantRupture(String.format(Locale.ROOT, "%.3f", valeur));
      }
      // Stock attendu
      valeur = stock.getStockAttendu();
      if (valeur == null) {
        article.setStockAttendu("0");
      }
      else {
        article.setStockAttendu(String.format(Locale.ROOT, "%.3f", valeur));
      }
      // Date de fin de rupture
      article.setDateFinRupture(DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, stock.getDateFinRupture()));
      listeStocks.add(article);
    }
  }
  
  // -- Accesseurs
  
  public ArrayList<JsonStockArticleV3> getListeStocks() {
    return listeStocks;
  }
  
  public void setListeStocks(ArrayList<JsonStockArticleV3> listeStocks) {
    this.listeStocks = listeStocks;
  }
  
}
