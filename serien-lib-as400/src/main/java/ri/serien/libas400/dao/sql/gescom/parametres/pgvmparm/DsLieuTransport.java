/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

public class DsLieuTransport extends DataStructureRecord {
  
  @Override
  protected void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "INDETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "INDTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "INDIND"));
    
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "LVLB1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "LVLB2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "LVLB3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "LVLB4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "LVLB5"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "LVLB6"));
    
    length = 300;
  }
  
}
