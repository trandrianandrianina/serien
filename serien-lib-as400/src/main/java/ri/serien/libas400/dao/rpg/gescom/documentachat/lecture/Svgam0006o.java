/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.lecture;

/**
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */
import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0006o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_EATOP = 1;
  public static final int DECIMAL_EATOP = 0;
  public static final int SIZE_EACOD = 1;
  public static final int SIZE_EAETB = 3;
  public static final int SIZE_EANUM = 6;
  public static final int DECIMAL_EANUM = 0;
  public static final int SIZE_EASUF = 1;
  public static final int DECIMAL_EASUF = 0;
  public static final int SIZE_EACRE = 7;
  public static final int DECIMAL_EACRE = 0;
  public static final int SIZE_EADAT = 7;
  public static final int DECIMAL_EADAT = 0;
  public static final int SIZE_EAHOM = 7;
  public static final int DECIMAL_EAHOM = 0;
  public static final int SIZE_EAREC = 7;
  public static final int DECIMAL_EAREC = 0;
  public static final int SIZE_EAFAC = 7;
  public static final int DECIMAL_EAFAC = 0;
  public static final int SIZE_EANFA = 6;
  public static final int DECIMAL_EANFA = 0;
  public static final int SIZE_EANLS = 4;
  public static final int DECIMAL_EANLS = 0;
  public static final int SIZE_EAPLI = 4;
  public static final int DECIMAL_EAPLI = 0;
  public static final int SIZE_EADLI = 4;
  public static final int DECIMAL_EADLI = 0;
  public static final int SIZE_EANLI = 4;
  public static final int DECIMAL_EANLI = 0;
  public static final int SIZE_EATLV = 1;
  public static final int DECIMAL_EATLV = 0;
  public static final int SIZE_EATLL = 1;
  public static final int DECIMAL_EATLL = 0;
  public static final int SIZE_EADSU = 1;
  public static final int DECIMAL_EADSU = 0;
  public static final int SIZE_EAETA = 1;
  public static final int DECIMAL_EAETA = 0;
  public static final int SIZE_EAEDT = 1;
  public static final int DECIMAL_EAEDT = 0;
  public static final int SIZE_EATFC = 1;
  public static final int DECIMAL_EATFC = 0;
  public static final int SIZE_EATT = 1;
  public static final int DECIMAL_EATT = 0;
  public static final int SIZE_EANRG = 1;
  public static final int DECIMAL_EANRG = 0;
  public static final int SIZE_EATVA1 = 1;
  public static final int DECIMAL_EATVA1 = 0;
  public static final int SIZE_EATVA2 = 1;
  public static final int DECIMAL_EATVA2 = 0;
  public static final int SIZE_EATVA3 = 1;
  public static final int DECIMAL_EATVA3 = 0;
  public static final int SIZE_EARLV = 1;
  public static final int DECIMAL_EARLV = 0;
  public static final int SIZE_EANEX = 1;
  public static final int DECIMAL_EANEX = 0;
  public static final int SIZE_EAEXC = 1;
  public static final int DECIMAL_EAEXC = 0;
  public static final int SIZE_EAEXE = 1;
  public static final int DECIMAL_EAEXE = 0;
  public static final int SIZE_EANRR = 1;
  public static final int DECIMAL_EANRR = 0;
  public static final int SIZE_EASGN = 1;
  public static final int DECIMAL_EASGN = 0;
  public static final int SIZE_EALIV = 1;
  public static final int DECIMAL_EALIV = 0;
  public static final int SIZE_EACPT = 1;
  public static final int DECIMAL_EACPT = 0;
  public static final int SIZE_EAPGC = 1;
  public static final int DECIMAL_EAPGC = 0;
  public static final int SIZE_EACHF = 1;
  public static final int DECIMAL_EACHF = 0;
  public static final int SIZE_EAINA = 1;
  public static final int DECIMAL_EAINA = 0;
  public static final int SIZE_EACOL = 1;
  public static final int DECIMAL_EACOL = 0;
  public static final int SIZE_EAFRS = 6;
  public static final int DECIMAL_EAFRS = 0;
  public static final int SIZE_EADLP = 7;
  public static final int DECIMAL_EADLP = 0;
  public static final int SIZE_EANUM0 = 6;
  public static final int DECIMAL_EANUM0 = 0;
  public static final int SIZE_EASUF0 = 1;
  public static final int DECIMAL_EASUF0 = 0;
  public static final int SIZE_EAAVR = 1;
  public static final int SIZE_EATFA = 1;
  public static final int SIZE_EANAT = 1;
  public static final int SIZE_EAESC = 3;
  public static final int DECIMAL_EAESC = 2;
  public static final int SIZE_EAE1G = 7;
  public static final int DECIMAL_EAE1G = 0;
  public static final int SIZE_EAE2G = 7;
  public static final int DECIMAL_EAE2G = 0;
  public static final int SIZE_EAE3G = 7;
  public static final int DECIMAL_EAE3G = 0;
  public static final int SIZE_EAE4G = 7;
  public static final int DECIMAL_EAE4G = 0;
  public static final int SIZE_EAR1G = 11;
  public static final int DECIMAL_EAR1G = 2;
  public static final int SIZE_EAR2G = 11;
  public static final int DECIMAL_EAR2G = 2;
  public static final int SIZE_EAR3G = 11;
  public static final int DECIMAL_EAR3G = 2;
  public static final int SIZE_EAR4G = 11;
  public static final int DECIMAL_EAR4G = 2;
  public static final int SIZE_EASL1 = 11;
  public static final int DECIMAL_EASL1 = 2;
  public static final int SIZE_EASL2 = 11;
  public static final int DECIMAL_EASL2 = 2;
  public static final int SIZE_EASL3 = 11;
  public static final int DECIMAL_EASL3 = 2;
  public static final int SIZE_EATHTL = 11;
  public static final int DECIMAL_EATHTL = 2;
  public static final int SIZE_EATL1 = 11;
  public static final int DECIMAL_EATL1 = 2;
  public static final int SIZE_EATL2 = 11;
  public static final int DECIMAL_EATL2 = 2;
  public static final int SIZE_EATL3 = 11;
  public static final int DECIMAL_EATL3 = 2;
  public static final int SIZE_EATV1 = 5;
  public static final int DECIMAL_EATV1 = 3;
  public static final int SIZE_EATV2 = 5;
  public static final int DECIMAL_EATV2 = 3;
  public static final int SIZE_EATV3 = 5;
  public static final int DECIMAL_EATV3 = 3;
  public static final int SIZE_EAMES = 9;
  public static final int DECIMAL_EAMES = 2;
  public static final int SIZE_EATTC = 11;
  public static final int DECIMAL_EATTC = 2;
  public static final int SIZE_EARG1 = 2;
  public static final int SIZE_EAEC1 = 2;
  public static final int SIZE_EAPC1 = 2;
  public static final int SIZE_EARG2 = 2;
  public static final int SIZE_EAEC2 = 2;
  public static final int SIZE_EAPC2 = 2;
  public static final int SIZE_EARG3 = 2;
  public static final int SIZE_EAEC3 = 2;
  public static final int SIZE_EAPC3 = 2;
  public static final int SIZE_EARG4 = 2;
  public static final int SIZE_EAEC4 = 2;
  public static final int SIZE_EAPC4 = 2;
  public static final int SIZE_EAMAG = 2;
  public static final int SIZE_EARBL = 10;
  public static final int SIZE_EASAN = 4;
  public static final int SIZE_EAACT = 4;
  public static final int SIZE_EALLV = 5;
  public static final int SIZE_EAACH = 3;
  public static final int SIZE_EACJA = 2;
  public static final int SIZE_EADEV = 3;
  public static final int SIZE_EACHG = 11;
  public static final int DECIMAL_EACHG = 6;
  public static final int SIZE_EACPR = 5;
  public static final int DECIMAL_EACPR = 4;
  public static final int SIZE_EABAS = 5;
  public static final int DECIMAL_EABAS = 0;
  public static final int SIZE_EADOS = 7;
  public static final int SIZE_EACNT = 13;
  public static final int SIZE_EAPDS = 8;
  public static final int DECIMAL_EAPDS = 3;
  public static final int SIZE_EAVOL = 8;
  public static final int DECIMAL_EAVOL = 3;
  public static final int SIZE_EAMTA = 11;
  public static final int DECIMAL_EAMTA = 2;
  public static final int SIZE_EAREM1 = 4;
  public static final int DECIMAL_EAREM1 = 2;
  public static final int SIZE_EAREM2 = 4;
  public static final int DECIMAL_EAREM2 = 2;
  public static final int SIZE_EAREM3 = 4;
  public static final int DECIMAL_EAREM3 = 2;
  public static final int SIZE_EAREM4 = 4;
  public static final int DECIMAL_EAREM4 = 2;
  public static final int SIZE_EAREM5 = 4;
  public static final int DECIMAL_EAREM5 = 2;
  public static final int SIZE_EAREM6 = 4;
  public static final int DECIMAL_EAREM6 = 2;
  public static final int SIZE_EATRL = 1;
  public static final int SIZE_EABRL = 1;
  public static final int SIZE_EARP1 = 4;
  public static final int DECIMAL_EARP1 = 2;
  public static final int SIZE_EARP2 = 4;
  public static final int DECIMAL_EARP2 = 2;
  public static final int SIZE_EARP3 = 4;
  public static final int DECIMAL_EARP3 = 2;
  public static final int SIZE_EARP4 = 4;
  public static final int DECIMAL_EARP4 = 2;
  public static final int SIZE_EARP5 = 4;
  public static final int DECIMAL_EARP5 = 2;
  public static final int SIZE_EARP6 = 4;
  public static final int DECIMAL_EARP6 = 2;
  public static final int SIZE_EATRP = 1;
  public static final int SIZE_EATP1 = 2;
  public static final int SIZE_EATP2 = 2;
  public static final int SIZE_EATP3 = 2;
  public static final int SIZE_EATP4 = 2;
  public static final int SIZE_EATP5 = 2;
  public static final int SIZE_EAIN1 = 1;
  public static final int SIZE_EAIN2 = 1;
  public static final int SIZE_EAIN3 = 1;
  public static final int SIZE_EAIN4 = 1;
  public static final int SIZE_EAIN5 = 1;
  public static final int SIZE_EACCT = 10;
  public static final int SIZE_EADAT1 = 7;
  public static final int DECIMAL_EADAT1 = 0;
  public static final int SIZE_EARBC = 10;
  public static final int SIZE_EAINT1 = 7;
  public static final int DECIMAL_EAINT1 = 0;
  public static final int SIZE_EAINT2 = 7;
  public static final int DECIMAL_EAINT2 = 0;
  public static final int SIZE_EAETAI = 1;
  public static final int SIZE_EAMEX = 2;
  public static final int SIZE_EACTR = 2;
  public static final int SIZE_EADILI = 1;
  public static final int SIZE_EAIN6 = 1;
  public static final int SIZE_EAIN7 = 1;
  public static final int SIZE_EAIN8 = 1;
  public static final int SIZE_EAIN9 = 1;
  public static final int SIZE_EARFL = 50;
  public static final int SIZE_EAPOR0 = 11;
  public static final int DECIMAL_EAPOR0 = 2;
  public static final int SIZE_EAPOR1 = 11;
  public static final int DECIMAL_EAPOR1 = 2;
  public static final int SIZE_EAIN10 = 1;
  public static final int SIZE_EAIN11 = 1;
  public static final int SIZE_EAIN12 = 1;
  public static final int SIZE_EAIN13 = 1;
  public static final int SIZE_EAIN14 = 1;
  public static final int SIZE_EAIN15 = 1;
  public static final int SIZE_PONOM2 = 30;
  public static final int SIZE_POCPL2 = 30;
  public static final int SIZE_PORUE2 = 30;
  public static final int SIZE_POLOC2 = 30;
  public static final int SIZE_POCDP2 = 5;
  public static final int SIZE_POVIL2 = 24;
  public static final int SIZE_POENV = 1;
  public static final int SIZE_POINEX = 1;
  public static final int DECIMAL_POINEX = 0;
  public static final int SIZE_POTYPPX = 1;
  public static final int SIZE_PONOM3 = 30;
  public static final int SIZE_POCPL3 = 30;
  public static final int SIZE_PORUE3 = 30;
  public static final int SIZE_POLOC3 = 30;
  public static final int SIZE_POCDP3 = 5;
  public static final int SIZE_POFIL3 = 1;
  public static final int SIZE_POVIL3 = 24;
  public static final int SIZE_WEAFAC = 7;
  public static final int DECIMAL_WEAFAC = 0;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 942;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_EATOP = 1;
  public static final int VAR_EACOD = 2;
  public static final int VAR_EAETB = 3;
  public static final int VAR_EANUM = 4;
  public static final int VAR_EASUF = 5;
  public static final int VAR_EACRE = 6;
  public static final int VAR_EADAT = 7;
  public static final int VAR_EAHOM = 8;
  public static final int VAR_EAREC = 9;
  public static final int VAR_EAFAC = 10;
  public static final int VAR_EANFA = 11;
  public static final int VAR_EANLS = 12;
  public static final int VAR_EAPLI = 13;
  public static final int VAR_EADLI = 14;
  public static final int VAR_EANLI = 15;
  public static final int VAR_EATLV = 16;
  public static final int VAR_EATLL = 17;
  public static final int VAR_EADSU = 18;
  public static final int VAR_EAETA = 19;
  public static final int VAR_EAEDT = 20;
  public static final int VAR_EATFC = 21;
  public static final int VAR_EATT = 22;
  public static final int VAR_EANRG = 23;
  public static final int VAR_EATVA1 = 24;
  public static final int VAR_EATVA2 = 25;
  public static final int VAR_EATVA3 = 26;
  public static final int VAR_EARLV = 27;
  public static final int VAR_EANEX = 28;
  public static final int VAR_EAEXC = 29;
  public static final int VAR_EAEXE = 30;
  public static final int VAR_EANRR = 31;
  public static final int VAR_EASGN = 32;
  public static final int VAR_EALIV = 33;
  public static final int VAR_EACPT = 34;
  public static final int VAR_EAPGC = 35;
  public static final int VAR_EACHF = 36;
  public static final int VAR_EAINA = 37;
  public static final int VAR_EACOL = 38;
  public static final int VAR_EAFRS = 39;
  public static final int VAR_EADLP = 40;
  public static final int VAR_EANUM0 = 41;
  public static final int VAR_EASUF0 = 42;
  public static final int VAR_EAAVR = 43;
  public static final int VAR_EATFA = 44;
  public static final int VAR_EANAT = 45;
  public static final int VAR_EAESC = 46;
  public static final int VAR_EAE1G = 47;
  public static final int VAR_EAE2G = 48;
  public static final int VAR_EAE3G = 49;
  public static final int VAR_EAE4G = 50;
  public static final int VAR_EAR1G = 51;
  public static final int VAR_EAR2G = 52;
  public static final int VAR_EAR3G = 53;
  public static final int VAR_EAR4G = 54;
  public static final int VAR_EASL1 = 55;
  public static final int VAR_EASL2 = 56;
  public static final int VAR_EASL3 = 57;
  public static final int VAR_EATHTL = 58;
  public static final int VAR_EATL1 = 59;
  public static final int VAR_EATL2 = 60;
  public static final int VAR_EATL3 = 61;
  public static final int VAR_EATV1 = 62;
  public static final int VAR_EATV2 = 63;
  public static final int VAR_EATV3 = 64;
  public static final int VAR_EAMES = 65;
  public static final int VAR_EATTC = 66;
  public static final int VAR_EARG1 = 67;
  public static final int VAR_EAEC1 = 68;
  public static final int VAR_EAPC1 = 69;
  public static final int VAR_EARG2 = 70;
  public static final int VAR_EAEC2 = 71;
  public static final int VAR_EAPC2 = 72;
  public static final int VAR_EARG3 = 73;
  public static final int VAR_EAEC3 = 74;
  public static final int VAR_EAPC3 = 75;
  public static final int VAR_EARG4 = 76;
  public static final int VAR_EAEC4 = 77;
  public static final int VAR_EAPC4 = 78;
  public static final int VAR_EAMAG = 79;
  public static final int VAR_EARBL = 80;
  public static final int VAR_EASAN = 81;
  public static final int VAR_EAACT = 82;
  public static final int VAR_EALLV = 83;
  public static final int VAR_EAACH = 84;
  public static final int VAR_EACJA = 85;
  public static final int VAR_EADEV = 86;
  public static final int VAR_EACHG = 87;
  public static final int VAR_EACPR = 88;
  public static final int VAR_EABAS = 89;
  public static final int VAR_EADOS = 90;
  public static final int VAR_EACNT = 91;
  public static final int VAR_EAPDS = 92;
  public static final int VAR_EAVOL = 93;
  public static final int VAR_EAMTA = 94;
  public static final int VAR_EAREM1 = 95;
  public static final int VAR_EAREM2 = 96;
  public static final int VAR_EAREM3 = 97;
  public static final int VAR_EAREM4 = 98;
  public static final int VAR_EAREM5 = 99;
  public static final int VAR_EAREM6 = 100;
  public static final int VAR_EATRL = 101;
  public static final int VAR_EABRL = 102;
  public static final int VAR_EARP1 = 103;
  public static final int VAR_EARP2 = 104;
  public static final int VAR_EARP3 = 105;
  public static final int VAR_EARP4 = 106;
  public static final int VAR_EARP5 = 107;
  public static final int VAR_EARP6 = 108;
  public static final int VAR_EATRP = 109;
  public static final int VAR_EATP1 = 110;
  public static final int VAR_EATP2 = 111;
  public static final int VAR_EATP3 = 112;
  public static final int VAR_EATP4 = 113;
  public static final int VAR_EATP5 = 114;
  public static final int VAR_EAIN1 = 115;
  public static final int VAR_EAIN2 = 116;
  public static final int VAR_EAIN3 = 117;
  public static final int VAR_EAIN4 = 118;
  public static final int VAR_EAIN5 = 119;
  public static final int VAR_EACCT = 120;
  public static final int VAR_EADAT1 = 121;
  public static final int VAR_EARBC = 122;
  public static final int VAR_EAINT1 = 123;
  public static final int VAR_EAINT2 = 124;
  public static final int VAR_EAETAI = 125;
  public static final int VAR_EAMEX = 126;
  public static final int VAR_EACTR = 127;
  public static final int VAR_EADILI = 128;
  public static final int VAR_EAIN6 = 129;
  public static final int VAR_EAIN7 = 130;
  public static final int VAR_EAIN8 = 131;
  public static final int VAR_EAIN9 = 132;
  public static final int VAR_EARFL = 133;
  public static final int VAR_EAPOR0 = 134;
  public static final int VAR_EAPOR1 = 135;
  public static final int VAR_EAIN10 = 136;
  public static final int VAR_EAIN11 = 137;
  public static final int VAR_EAIN12 = 138;
  public static final int VAR_EAIN13 = 139;
  public static final int VAR_EAIN14 = 140;
  public static final int VAR_EAIN15 = 141;
  public static final int VAR_PONOM2 = 142;
  public static final int VAR_POCPL2 = 143;
  public static final int VAR_PORUE2 = 144;
  public static final int VAR_POLOC2 = 145;
  public static final int VAR_POCDP2 = 146;
  public static final int VAR_POVIL2 = 147;
  public static final int VAR_POENV = 148;
  public static final int VAR_POINEX = 149;
  public static final int VAR_POTYPPX = 150;
  public static final int VAR_PONOM3 = 151;
  public static final int VAR_POCPL3 = 152;
  public static final int VAR_PORUE3 = 153;
  public static final int VAR_POLOC3 = 154;
  public static final int VAR_POCDP3 = 155;
  public static final int VAR_POFIL3 = 156;
  public static final int VAR_POVIL3 = 157;
  public static final int VAR_WEAFAC = 158;
  public static final int VAR_POARR = 159;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal eatop = BigDecimal.ZERO; // Top système
  private String eacod = ""; // Code ERL "E","F","T"
  private String eaetb = ""; // Code Etablissement
  private BigDecimal eanum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal easuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal eacre = BigDecimal.ZERO; // Date Création
  private BigDecimal eadat = BigDecimal.ZERO; // Date dernier traitemt
  private BigDecimal eahom = BigDecimal.ZERO; // Date Homologation
  private BigDecimal earec = BigDecimal.ZERO; // Date Réception
  private BigDecimal eafac = BigDecimal.ZERO; // Date Facturation
  private BigDecimal eanfa = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal eanls = BigDecimal.ZERO; // Ligne sélectable/Fac
  private BigDecimal eapli = BigDecimal.ZERO; // N°Première ligne
  private BigDecimal eadli = BigDecimal.ZERO; // N°Dernière ligne
  private BigDecimal eanli = BigDecimal.ZERO; // N°Ligne en cours
  private BigDecimal eatlv = BigDecimal.ZERO; // Top ligne véritable
  private BigDecimal eatll = BigDecimal.ZERO; // Top ligne liée
  private BigDecimal eadsu = BigDecimal.ZERO; // Dernier suffixe
  private BigDecimal eaeta = BigDecimal.ZERO; // Etat du bon
  private BigDecimal eaedt = BigDecimal.ZERO; // Top édition
  private BigDecimal eatfc = BigDecimal.ZERO; // Top Frs de commande
  private BigDecimal eatt = BigDecimal.ZERO; // Montants facture forcés
  private BigDecimal eanrg = BigDecimal.ZERO; // Rang réglement
  private BigDecimal eatva1 = BigDecimal.ZERO; // N° TVA 1
  private BigDecimal eatva2 = BigDecimal.ZERO; // N° TVA 2
  private BigDecimal eatva3 = BigDecimal.ZERO; // N° TVA 3
  private BigDecimal earlv = BigDecimal.ZERO; // Top relevé
  private BigDecimal eanex = BigDecimal.ZERO; // Nbre d'exemplaires
  private BigDecimal eaexc = BigDecimal.ZERO; // Rang extension commande
  private BigDecimal eaexe = BigDecimal.ZERO; // Rang extension expédition
  private BigDecimal eanrr = BigDecimal.ZERO; // Nbre réglements réel
  private BigDecimal easgn = BigDecimal.ZERO; // Signe géré
  private BigDecimal ealiv = BigDecimal.ZERO; // Top livraison
  private BigDecimal eacpt = BigDecimal.ZERO; // Top comptabilisation
  private BigDecimal eapgc = BigDecimal.ZERO; // Top prix garanti
  private BigDecimal eachf = BigDecimal.ZERO; // Top chiffrage
  private BigDecimal eaina = BigDecimal.ZERO; // Top affectation
  private BigDecimal eacol = BigDecimal.ZERO; // Collectif FRS
  private BigDecimal eafrs = BigDecimal.ZERO; // Code Fournisseur
  private BigDecimal eadlp = BigDecimal.ZERO; // Date liv.Prévue
  private BigDecimal eanum0 = BigDecimal.ZERO; // N°Bon origine
  private BigDecimal easuf0 = BigDecimal.ZERO; // N°Suf.origine
  private String eaavr = ""; // Code Avoir (A ou Blanc)
  private String eatfa = ""; // Code Type de Facturation
  private String eanat = ""; // Nature Bon
  private BigDecimal eaesc = BigDecimal.ZERO; // % Escompte
  private BigDecimal eae1g = BigDecimal.ZERO; // Date échéance 1
  private BigDecimal eae2g = BigDecimal.ZERO; // Date échéance 2
  private BigDecimal eae3g = BigDecimal.ZERO; // Date échéance 3
  private BigDecimal eae4g = BigDecimal.ZERO; // Date échéance 4
  private BigDecimal ear1g = BigDecimal.ZERO; // Mtt à régler échéance 1
  private BigDecimal ear2g = BigDecimal.ZERO; // Mtt à régler échéance 2
  private BigDecimal ear3g = BigDecimal.ZERO; // Mtt à régler échéance 3
  private BigDecimal ear4g = BigDecimal.ZERO; // Mtt à régler échéance 4
  private BigDecimal easl1 = BigDecimal.ZERO; // Soumis T.V.A 1
  private BigDecimal easl2 = BigDecimal.ZERO; // Soumis T.V.A 2
  private BigDecimal easl3 = BigDecimal.ZERO; // Soumis T.V.A 3
  private BigDecimal eathtl = BigDecimal.ZERO; // Total Hors Taxes Lignes
  private BigDecimal eatl1 = BigDecimal.ZERO; // Montant TVA 1
  private BigDecimal eatl2 = BigDecimal.ZERO; // Montant TVA 2
  private BigDecimal eatl3 = BigDecimal.ZERO; // Montant TVA 3
  private BigDecimal eatv1 = BigDecimal.ZERO; // Taux TVA 1
  private BigDecimal eatv2 = BigDecimal.ZERO; // Taux TVA 2
  private BigDecimal eatv3 = BigDecimal.ZERO; // Taux TVA 3
  private BigDecimal eames = BigDecimal.ZERO; // Montant Escompte
  private BigDecimal eattc = BigDecimal.ZERO; // Montant TTC
  private String earg1 = ""; // Code Règlement 1
  private String eaec1 = ""; // Code Echéance 1
  private String eapc1 = ""; // % Règlement 1
  private String earg2 = ""; // Code Règlement 2
  private String eaec2 = ""; // Code Echéance 2
  private String eapc2 = ""; // % Règlement 2
  private String earg3 = ""; // Code Règlement 3
  private String eaec3 = ""; // Code Echéance 3
  private String eapc3 = ""; // % Règlement 3
  private String earg4 = ""; // Code Règlement 4
  private String eaec4 = ""; // Code Echéance 4
  private String eapc4 = ""; // % Règlement 4
  private String eamag = ""; // Code Magasin de Sortie
  private String earbl = ""; // Référence Bon de livraison
  private String easan = ""; // Section Analytique
  private String eaact = ""; // Code activité ou Affaire
  private String eallv = ""; // Code Lieu de Livraison
  private String eaach = ""; // Code Acheteur
  private String eacja = ""; // Code Journal d"Achat
  private String eadev = ""; // Code Devise
  private BigDecimal eachg = BigDecimal.ZERO; // Taux de Change
  private BigDecimal eacpr = BigDecimal.ZERO; // Coeff. Achat (frais d"appro)
  private BigDecimal eabas = BigDecimal.ZERO; // Base de la Devise
  private String eados = ""; // Code Dossier appro
  private String eacnt = ""; // Code Container
  private BigDecimal eapds = BigDecimal.ZERO; // Poids
  private BigDecimal eavol = BigDecimal.ZERO; // Volume
  private BigDecimal eamta = BigDecimal.ZERO; // Montant frais à répartir
  private BigDecimal earem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal earem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal earem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal earem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal earem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal earem6 = BigDecimal.ZERO; // % Remise 6
  private String eatrl = ""; // Type remise ligne, 1=cascade
  private String eabrl = ""; // Base remise ligne, 1=Montant
  private BigDecimal earp1 = BigDecimal.ZERO; // % Remise 1 en Pied
  private BigDecimal earp2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal earp3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal earp4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal earp5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal earp6 = BigDecimal.ZERO; // % Remise 6
  private String eatrp = ""; // Type remise/Pied, 1=cascade
  private String eatp1 = ""; // Top personnalisé n°1
  private String eatp2 = ""; // Top personnalisé n°2
  private String eatp3 = ""; // Top personnalisé n°3
  private String eatp4 = ""; // Top personnalisé n°4
  private String eatp5 = ""; // Top personnalisé n°5
  private String eain1 = ""; // Litige (1=Non Rap,2=Non Cpt.
  private String eain2 = ""; // utilisé par module S33
  private String eain3 = ""; // Réception définitive
  private String eain4 = ""; // Cpt stocks flottants
  private String eain5 = ""; // blocage comptabilisation
  private String eacct = ""; // Code Contrat
  private BigDecimal eadat1 = BigDecimal.ZERO; // Dte Confirmation Fournisseur
  private String earbc = ""; // Référence Commande
  private BigDecimal eaint1 = BigDecimal.ZERO; // Dte Intermédiaire 1
  private BigDecimal eaint2 = BigDecimal.ZERO; // Dte Intermédiaire 2
  private String eaetai = ""; // Etat intermédiaire
  private String eamex = ""; // Mode d'expédition
  private String eactr = ""; // Code transporteur
  private String eadili = ""; // Traitée par commande DILICOM
  private String eain6 = ""; // Blocage commande,
  private String eain7 = ""; // Scénario de dates
  private String eain8 = ""; // Etat frais fixes
  private String eain9 = ""; // I = Commande Interne
  private String earfl = ""; // Référence commande longue
  private BigDecimal eapor0 = BigDecimal.ZERO; // montant port théorique
  private BigDecimal eapor1 = BigDecimal.ZERO; // montant port facturé
  private String eain10 = ""; // 1=Port non facturé
  private String eain11 = ""; // N.U.
  private String eain12 = ""; // N.U.
  private String eain13 = ""; // N.U.
  private String eain14 = ""; // N.U.
  private String eain15 = ""; // N.U.
  private String ponom2 = ""; // Nom fournisseur
  private String pocpl2 = ""; // Complément du nom
  private String porue2 = ""; // Rue
  private String poloc2 = ""; // Localité
  private String pocdp2 = ""; // Code Postal
  private String povil2 = ""; // Ville
  private String poenv = ""; // Enlevement ou livraison
  private BigDecimal poinex = BigDecimal.ZERO; // Indicateur Extraction
  private String potyppx = ""; // Type prix d'achat
  private String ponom3 = ""; // Nom du client
  private String pocpl3 = ""; // Complément du nom
  private String porue3 = ""; // Rue
  private String poloc3 = ""; // Localité
  private String pocdp3 = ""; // Code Postal
  private String pofil3 = ""; // Code Postal
  private String povil3 = ""; // Ville
  private BigDecimal weafac = BigDecimal.ZERO; // DATE DE FACTURE SI RÉCEPTION
  private String poarr = "X"; // Fin
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_EATOP, DECIMAL_EATOP), // Top système
      new AS400Text(SIZE_EACOD), // Code ERL "E","F","T"
      new AS400Text(SIZE_EAETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_EANUM, DECIMAL_EANUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_EASUF, DECIMAL_EASUF), // Suffixe
      new AS400PackedDecimal(SIZE_EACRE, DECIMAL_EACRE), // Date Création
      new AS400PackedDecimal(SIZE_EADAT, DECIMAL_EADAT), // Date dernier traitemt
      new AS400PackedDecimal(SIZE_EAHOM, DECIMAL_EAHOM), // Date Homologation
      new AS400PackedDecimal(SIZE_EAREC, DECIMAL_EAREC), // Date Réception
      new AS400PackedDecimal(SIZE_EAFAC, DECIMAL_EAFAC), // Date Facturation
      new AS400ZonedDecimal(SIZE_EANFA, DECIMAL_EANFA), // Numéro de facture
      new AS400ZonedDecimal(SIZE_EANLS, DECIMAL_EANLS), // Ligne sélectable/Fac
      new AS400PackedDecimal(SIZE_EAPLI, DECIMAL_EAPLI), // N°Première ligne
      new AS400PackedDecimal(SIZE_EADLI, DECIMAL_EADLI), // N°Dernière ligne
      new AS400PackedDecimal(SIZE_EANLI, DECIMAL_EANLI), // N°Ligne en cours
      new AS400ZonedDecimal(SIZE_EATLV, DECIMAL_EATLV), // Top ligne véritable
      new AS400ZonedDecimal(SIZE_EATLL, DECIMAL_EATLL), // Top ligne liée
      new AS400ZonedDecimal(SIZE_EADSU, DECIMAL_EADSU), // Dernier suffixe
      new AS400ZonedDecimal(SIZE_EAETA, DECIMAL_EAETA), // Etat du bon
      new AS400ZonedDecimal(SIZE_EAEDT, DECIMAL_EAEDT), // Top édition
      new AS400ZonedDecimal(SIZE_EATFC, DECIMAL_EATFC), // Top Frs de commande
      new AS400ZonedDecimal(SIZE_EATT, DECIMAL_EATT), // Montants facture forcés
      new AS400ZonedDecimal(SIZE_EANRG, DECIMAL_EANRG), // Rang réglement
      new AS400ZonedDecimal(SIZE_EATVA1, DECIMAL_EATVA1), // N° TVA 1
      new AS400ZonedDecimal(SIZE_EATVA2, DECIMAL_EATVA2), // N° TVA 2
      new AS400ZonedDecimal(SIZE_EATVA3, DECIMAL_EATVA3), // N° TVA 3
      new AS400ZonedDecimal(SIZE_EARLV, DECIMAL_EARLV), // Top relevé
      new AS400ZonedDecimal(SIZE_EANEX, DECIMAL_EANEX), // Nbre d'exemplaires
      new AS400ZonedDecimal(SIZE_EAEXC, DECIMAL_EAEXC), // Rang extension commande
      new AS400ZonedDecimal(SIZE_EAEXE, DECIMAL_EAEXE), // Rang extension expédition
      new AS400ZonedDecimal(SIZE_EANRR, DECIMAL_EANRR), // Nbre réglements réel
      new AS400ZonedDecimal(SIZE_EASGN, DECIMAL_EASGN), // Signe géré
      new AS400ZonedDecimal(SIZE_EALIV, DECIMAL_EALIV), // Top livraison
      new AS400ZonedDecimal(SIZE_EACPT, DECIMAL_EACPT), // Top comptabilisation
      new AS400ZonedDecimal(SIZE_EAPGC, DECIMAL_EAPGC), // Top prix garanti
      new AS400ZonedDecimal(SIZE_EACHF, DECIMAL_EACHF), // Top chiffrage
      new AS400ZonedDecimal(SIZE_EAINA, DECIMAL_EAINA), // Top affectation
      new AS400ZonedDecimal(SIZE_EACOL, DECIMAL_EACOL), // Collectif FRS
      new AS400ZonedDecimal(SIZE_EAFRS, DECIMAL_EAFRS), // Code Fournisseur
      new AS400PackedDecimal(SIZE_EADLP, DECIMAL_EADLP), // Date liv.Prévue
      new AS400ZonedDecimal(SIZE_EANUM0, DECIMAL_EANUM0), // N°Bon origine
      new AS400ZonedDecimal(SIZE_EASUF0, DECIMAL_EASUF0), // N°Suf.origine
      new AS400Text(SIZE_EAAVR), // Code Avoir (A ou Blanc)
      new AS400Text(SIZE_EATFA), // Code Type de Facturation
      new AS400Text(SIZE_EANAT), // Nature Bon
      new AS400PackedDecimal(SIZE_EAESC, DECIMAL_EAESC), // % Escompte
      new AS400PackedDecimal(SIZE_EAE1G, DECIMAL_EAE1G), // Date échéance 1
      new AS400PackedDecimal(SIZE_EAE2G, DECIMAL_EAE2G), // Date échéance 2
      new AS400PackedDecimal(SIZE_EAE3G, DECIMAL_EAE3G), // Date échéance 3
      new AS400PackedDecimal(SIZE_EAE4G, DECIMAL_EAE4G), // Date échéance 4
      new AS400PackedDecimal(SIZE_EAR1G, DECIMAL_EAR1G), // Mtt à régler échéance 1
      new AS400PackedDecimal(SIZE_EAR2G, DECIMAL_EAR2G), // Mtt à régler échéance 2
      new AS400PackedDecimal(SIZE_EAR3G, DECIMAL_EAR3G), // Mtt à régler échéance 3
      new AS400PackedDecimal(SIZE_EAR4G, DECIMAL_EAR4G), // Mtt à régler échéance 4
      new AS400PackedDecimal(SIZE_EASL1, DECIMAL_EASL1), // Soumis T.V.A 1
      new AS400PackedDecimal(SIZE_EASL2, DECIMAL_EASL2), // Soumis T.V.A 2
      new AS400PackedDecimal(SIZE_EASL3, DECIMAL_EASL3), // Soumis T.V.A 3
      new AS400PackedDecimal(SIZE_EATHTL, DECIMAL_EATHTL), // Total Hors Taxes Lignes
      new AS400PackedDecimal(SIZE_EATL1, DECIMAL_EATL1), // Montant TVA 1
      new AS400PackedDecimal(SIZE_EATL2, DECIMAL_EATL2), // Montant TVA 2
      new AS400PackedDecimal(SIZE_EATL3, DECIMAL_EATL3), // Montant TVA 3
      new AS400PackedDecimal(SIZE_EATV1, DECIMAL_EATV1), // Taux TVA 1
      new AS400PackedDecimal(SIZE_EATV2, DECIMAL_EATV2), // Taux TVA 2
      new AS400PackedDecimal(SIZE_EATV3, DECIMAL_EATV3), // Taux TVA 3
      new AS400PackedDecimal(SIZE_EAMES, DECIMAL_EAMES), // Montant Escompte
      new AS400PackedDecimal(SIZE_EATTC, DECIMAL_EATTC), // Montant TTC
      new AS400Text(SIZE_EARG1), // Code Règlement 1
      new AS400Text(SIZE_EAEC1), // Code Echéance 1
      new AS400Text(SIZE_EAPC1), // % Règlement 1
      new AS400Text(SIZE_EARG2), // Code Règlement 2
      new AS400Text(SIZE_EAEC2), // Code Echéance 2
      new AS400Text(SIZE_EAPC2), // % Règlement 2
      new AS400Text(SIZE_EARG3), // Code Règlement 3
      new AS400Text(SIZE_EAEC3), // Code Echéance 3
      new AS400Text(SIZE_EAPC3), // % Règlement 3
      new AS400Text(SIZE_EARG4), // Code Règlement 4
      new AS400Text(SIZE_EAEC4), // Code Echéance 4
      new AS400Text(SIZE_EAPC4), // % Règlement 4
      new AS400Text(SIZE_EAMAG), // Code Magasin de Sortie
      new AS400Text(SIZE_EARBL), // Référence Bon de livraison
      new AS400Text(SIZE_EASAN), // Section Analytique
      new AS400Text(SIZE_EAACT), // Code activité ou Affaire
      new AS400Text(SIZE_EALLV), // Code Lieu de Livraison
      new AS400Text(SIZE_EAACH), // Code Acheteur
      new AS400Text(SIZE_EACJA), // Code Journal d"Achat
      new AS400Text(SIZE_EADEV), // Code Devise
      new AS400PackedDecimal(SIZE_EACHG, DECIMAL_EACHG), // Taux de Change
      new AS400PackedDecimal(SIZE_EACPR, DECIMAL_EACPR), // Coeff. Achat (frais d"appro)
      new AS400PackedDecimal(SIZE_EABAS, DECIMAL_EABAS), // Base de la Devise
      new AS400Text(SIZE_EADOS), // Code Dossier appro
      new AS400Text(SIZE_EACNT), // Code Container
      new AS400PackedDecimal(SIZE_EAPDS, DECIMAL_EAPDS), // Poids
      new AS400PackedDecimal(SIZE_EAVOL, DECIMAL_EAVOL), // Volume
      new AS400PackedDecimal(SIZE_EAMTA, DECIMAL_EAMTA), // Montant frais à répartir
      new AS400ZonedDecimal(SIZE_EAREM1, DECIMAL_EAREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_EAREM2, DECIMAL_EAREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_EAREM3, DECIMAL_EAREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_EAREM4, DECIMAL_EAREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_EAREM5, DECIMAL_EAREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_EAREM6, DECIMAL_EAREM6), // % Remise 6
      new AS400Text(SIZE_EATRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_EABRL), // Base remise ligne, 1=Montant
      new AS400ZonedDecimal(SIZE_EARP1, DECIMAL_EARP1), // % Remise 1 en Pied
      new AS400ZonedDecimal(SIZE_EARP2, DECIMAL_EARP2), // % Remise 2
      new AS400ZonedDecimal(SIZE_EARP3, DECIMAL_EARP3), // % Remise 3
      new AS400ZonedDecimal(SIZE_EARP4, DECIMAL_EARP4), // % Remise 4
      new AS400ZonedDecimal(SIZE_EARP5, DECIMAL_EARP5), // % Remise 5
      new AS400ZonedDecimal(SIZE_EARP6, DECIMAL_EARP6), // % Remise 6
      new AS400Text(SIZE_EATRP), // Type remise/Pied, 1=cascade
      new AS400Text(SIZE_EATP1), // Top personnalisé n°1
      new AS400Text(SIZE_EATP2), // Top personnalisé n°2
      new AS400Text(SIZE_EATP3), // Top personnalisé n°3
      new AS400Text(SIZE_EATP4), // Top personnalisé n°4
      new AS400Text(SIZE_EATP5), // Top personnalisé n°5
      new AS400Text(SIZE_EAIN1), // Litige (1=Non Rap,2=Non Cpt.
      new AS400Text(SIZE_EAIN2), // utilisé par module S33
      new AS400Text(SIZE_EAIN3), // Réception définitive
      new AS400Text(SIZE_EAIN4), // Cpt stocks flottants
      new AS400Text(SIZE_EAIN5), // blocage comptabilisation
      new AS400Text(SIZE_EACCT), // Code Contrat
      new AS400PackedDecimal(SIZE_EADAT1, DECIMAL_EADAT1), // Dte Confirmation Fournisseur
      new AS400Text(SIZE_EARBC), // Référence Commande
      new AS400PackedDecimal(SIZE_EAINT1, DECIMAL_EAINT1), // Dte Intermédiaire 1
      new AS400PackedDecimal(SIZE_EAINT2, DECIMAL_EAINT2), // Dte Intermédiaire 2
      new AS400Text(SIZE_EAETAI), // Etat intermédiaire
      new AS400Text(SIZE_EAMEX), // Mode d'expédition
      new AS400Text(SIZE_EACTR), // Code transporteur
      new AS400Text(SIZE_EADILI), // Traitée par commande DILICOM
      new AS400Text(SIZE_EAIN6), // Blocage commande,
      new AS400Text(SIZE_EAIN7), // Scénario de dates
      new AS400Text(SIZE_EAIN8), // Etat frais fixes
      new AS400Text(SIZE_EAIN9), // I = Commande Interne
      new AS400Text(SIZE_EARFL), // Référence commande longue
      new AS400ZonedDecimal(SIZE_EAPOR0, DECIMAL_EAPOR0), // montant port théorique
      new AS400ZonedDecimal(SIZE_EAPOR1, DECIMAL_EAPOR1), // montant port facturé
      new AS400Text(SIZE_EAIN10), // 1=Port non facturé
      new AS400Text(SIZE_EAIN11), // N.U.
      new AS400Text(SIZE_EAIN12), // N.U.
      new AS400Text(SIZE_EAIN13), // N.U.
      new AS400Text(SIZE_EAIN14), // N.U.
      new AS400Text(SIZE_EAIN15), // N.U.
      new AS400Text(SIZE_PONOM2), // Nom fournisseur
      new AS400Text(SIZE_POCPL2), // Complément du nom
      new AS400Text(SIZE_PORUE2), // Rue
      new AS400Text(SIZE_POLOC2), // Localité
      new AS400Text(SIZE_POCDP2), // Code Postal
      new AS400Text(SIZE_POVIL2), // Ville
      new AS400Text(SIZE_POENV), // Enlevement ou livraison
      new AS400ZonedDecimal(SIZE_POINEX, DECIMAL_POINEX), // Indicateur Extraction
      new AS400Text(SIZE_POTYPPX), // Type prix d'achat
      new AS400Text(SIZE_PONOM3), // Nom du client
      new AS400Text(SIZE_POCPL3), // Complément du nom
      new AS400Text(SIZE_PORUE3), // Rue
      new AS400Text(SIZE_POLOC3), // Localité
      new AS400Text(SIZE_POCDP3), // Code Postal
      new AS400Text(SIZE_POFIL3), // Code Postal
      new AS400Text(SIZE_POVIL3), // Ville
      new AS400PackedDecimal(SIZE_WEAFAC, DECIMAL_WEAFAC), // DATE DE FACTURE SI RÉCEPTION
      new AS400Text(SIZE_POARR), // Fin
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { poind, eatop, eacod, eaetb, eanum, easuf, eacre, eadat, eahom, earec, eafac, eanfa, eanls, eapli, eadli, eanli,
          eatlv, eatll, eadsu, eaeta, eaedt, eatfc, eatt, eanrg, eatva1, eatva2, eatva3, earlv, eanex, eaexc, eaexe, eanrr, easgn, ealiv,
          eacpt, eapgc, eachf, eaina, eacol, eafrs, eadlp, eanum0, easuf0, eaavr, eatfa, eanat, eaesc, eae1g, eae2g, eae3g, eae4g, ear1g,
          ear2g, ear3g, ear4g, easl1, easl2, easl3, eathtl, eatl1, eatl2, eatl3, eatv1, eatv2, eatv3, eames, eattc, earg1, eaec1, eapc1,
          earg2, eaec2, eapc2, earg3, eaec3, eapc3, earg4, eaec4, eapc4, eamag, earbl, easan, eaact, eallv, eaach, eacja, eadev, eachg,
          eacpr, eabas, eados, eacnt, eapds, eavol, eamta, earem1, earem2, earem3, earem4, earem5, earem6, eatrl, eabrl, earp1, earp2,
          earp3, earp4, earp5, earp6, eatrp, eatp1, eatp2, eatp3, eatp4, eatp5, eain1, eain2, eain3, eain4, eain5, eacct, eadat1, earbc,
          eaint1, eaint2, eaetai, eamex, eactr, eadili, eain6, eain7, eain8, eain9, earfl, eapor0, eapor1, eain10, eain11, eain12, eain13,
          eain14, eain15, ponom2, pocpl2, porue2, poloc2, pocdp2, povil2, poenv, poinex, potyppx, ponom3, pocpl3, porue3, poloc3, pocdp3,
          pofil3, povil3, weafac, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    eatop = (BigDecimal) output[1];
    eacod = (String) output[2];
    eaetb = (String) output[3];
    eanum = (BigDecimal) output[4];
    easuf = (BigDecimal) output[5];
    eacre = (BigDecimal) output[6];
    eadat = (BigDecimal) output[7];
    eahom = (BigDecimal) output[8];
    earec = (BigDecimal) output[9];
    eafac = (BigDecimal) output[10];
    eanfa = (BigDecimal) output[11];
    eanls = (BigDecimal) output[12];
    eapli = (BigDecimal) output[13];
    eadli = (BigDecimal) output[14];
    eanli = (BigDecimal) output[15];
    eatlv = (BigDecimal) output[16];
    eatll = (BigDecimal) output[17];
    eadsu = (BigDecimal) output[18];
    eaeta = (BigDecimal) output[19];
    eaedt = (BigDecimal) output[20];
    eatfc = (BigDecimal) output[21];
    eatt = (BigDecimal) output[22];
    eanrg = (BigDecimal) output[23];
    eatva1 = (BigDecimal) output[24];
    eatva2 = (BigDecimal) output[25];
    eatva3 = (BigDecimal) output[26];
    earlv = (BigDecimal) output[27];
    eanex = (BigDecimal) output[28];
    eaexc = (BigDecimal) output[29];
    eaexe = (BigDecimal) output[30];
    eanrr = (BigDecimal) output[31];
    easgn = (BigDecimal) output[32];
    ealiv = (BigDecimal) output[33];
    eacpt = (BigDecimal) output[34];
    eapgc = (BigDecimal) output[35];
    eachf = (BigDecimal) output[36];
    eaina = (BigDecimal) output[37];
    eacol = (BigDecimal) output[38];
    eafrs = (BigDecimal) output[39];
    eadlp = (BigDecimal) output[40];
    eanum0 = (BigDecimal) output[41];
    easuf0 = (BigDecimal) output[42];
    eaavr = (String) output[43];
    eatfa = (String) output[44];
    eanat = (String) output[45];
    eaesc = (BigDecimal) output[46];
    eae1g = (BigDecimal) output[47];
    eae2g = (BigDecimal) output[48];
    eae3g = (BigDecimal) output[49];
    eae4g = (BigDecimal) output[50];
    ear1g = (BigDecimal) output[51];
    ear2g = (BigDecimal) output[52];
    ear3g = (BigDecimal) output[53];
    ear4g = (BigDecimal) output[54];
    easl1 = (BigDecimal) output[55];
    easl2 = (BigDecimal) output[56];
    easl3 = (BigDecimal) output[57];
    eathtl = (BigDecimal) output[58];
    eatl1 = (BigDecimal) output[59];
    eatl2 = (BigDecimal) output[60];
    eatl3 = (BigDecimal) output[61];
    eatv1 = (BigDecimal) output[62];
    eatv2 = (BigDecimal) output[63];
    eatv3 = (BigDecimal) output[64];
    eames = (BigDecimal) output[65];
    eattc = (BigDecimal) output[66];
    earg1 = (String) output[67];
    eaec1 = (String) output[68];
    eapc1 = (String) output[69];
    earg2 = (String) output[70];
    eaec2 = (String) output[71];
    eapc2 = (String) output[72];
    earg3 = (String) output[73];
    eaec3 = (String) output[74];
    eapc3 = (String) output[75];
    earg4 = (String) output[76];
    eaec4 = (String) output[77];
    eapc4 = (String) output[78];
    eamag = (String) output[79];
    earbl = (String) output[80];
    easan = (String) output[81];
    eaact = (String) output[82];
    eallv = (String) output[83];
    eaach = (String) output[84];
    eacja = (String) output[85];
    eadev = (String) output[86];
    eachg = (BigDecimal) output[87];
    eacpr = (BigDecimal) output[88];
    eabas = (BigDecimal) output[89];
    eados = (String) output[90];
    eacnt = (String) output[91];
    eapds = (BigDecimal) output[92];
    eavol = (BigDecimal) output[93];
    eamta = (BigDecimal) output[94];
    earem1 = (BigDecimal) output[95];
    earem2 = (BigDecimal) output[96];
    earem3 = (BigDecimal) output[97];
    earem4 = (BigDecimal) output[98];
    earem5 = (BigDecimal) output[99];
    earem6 = (BigDecimal) output[100];
    eatrl = (String) output[101];
    eabrl = (String) output[102];
    earp1 = (BigDecimal) output[103];
    earp2 = (BigDecimal) output[104];
    earp3 = (BigDecimal) output[105];
    earp4 = (BigDecimal) output[106];
    earp5 = (BigDecimal) output[107];
    earp6 = (BigDecimal) output[108];
    eatrp = (String) output[109];
    eatp1 = (String) output[110];
    eatp2 = (String) output[111];
    eatp3 = (String) output[112];
    eatp4 = (String) output[113];
    eatp5 = (String) output[114];
    eain1 = (String) output[115];
    eain2 = (String) output[116];
    eain3 = (String) output[117];
    eain4 = (String) output[118];
    eain5 = (String) output[119];
    eacct = (String) output[120];
    eadat1 = (BigDecimal) output[121];
    earbc = (String) output[122];
    eaint1 = (BigDecimal) output[123];
    eaint2 = (BigDecimal) output[124];
    eaetai = (String) output[125];
    eamex = (String) output[126];
    eactr = (String) output[127];
    eadili = (String) output[128];
    eain6 = (String) output[129];
    eain7 = (String) output[130];
    eain8 = (String) output[131];
    eain9 = (String) output[132];
    earfl = (String) output[133];
    eapor0 = (BigDecimal) output[134];
    eapor1 = (BigDecimal) output[135];
    eain10 = (String) output[136];
    eain11 = (String) output[137];
    eain12 = (String) output[138];
    eain13 = (String) output[139];
    eain14 = (String) output[140];
    eain15 = (String) output[141];
    ponom2 = (String) output[142];
    pocpl2 = (String) output[143];
    porue2 = (String) output[144];
    poloc2 = (String) output[145];
    pocdp2 = (String) output[146];
    povil2 = (String) output[147];
    poenv = (String) output[148];
    poinex = (BigDecimal) output[149];
    potyppx = (String) output[150];
    ponom3 = (String) output[151];
    pocpl3 = (String) output[152];
    porue3 = (String) output[153];
    poloc3 = (String) output[154];
    pocdp3 = (String) output[155];
    pofil3 = (String) output[156];
    povil3 = (String) output[157];
    weafac = (BigDecimal) output[158];
    poarr = (String) output[159];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setEatop(BigDecimal pEatop) {
    if (pEatop == null) {
      return;
    }
    eatop = pEatop.setScale(DECIMAL_EATOP, RoundingMode.HALF_UP);
  }
  
  public void setEatop(Integer pEatop) {
    if (pEatop == null) {
      return;
    }
    eatop = BigDecimal.valueOf(pEatop);
  }
  
  public Integer getEatop() {
    return eatop.intValue();
  }
  
  public void setEacod(Character pEacod) {
    if (pEacod == null) {
      return;
    }
    eacod = String.valueOf(pEacod);
  }
  
  public Character getEacod() {
    return eacod.charAt(0);
  }
  
  public void setEaetb(String pEaetb) {
    if (pEaetb == null) {
      return;
    }
    eaetb = pEaetb;
  }
  
  public String getEaetb() {
    return eaetb;
  }
  
  public void setEanum(BigDecimal pEanum) {
    if (pEanum == null) {
      return;
    }
    eanum = pEanum.setScale(DECIMAL_EANUM, RoundingMode.HALF_UP);
  }
  
  public void setEanum(Integer pEanum) {
    if (pEanum == null) {
      return;
    }
    eanum = BigDecimal.valueOf(pEanum);
  }
  
  public Integer getEanum() {
    return eanum.intValue();
  }
  
  public void setEasuf(BigDecimal pEasuf) {
    if (pEasuf == null) {
      return;
    }
    easuf = pEasuf.setScale(DECIMAL_EASUF, RoundingMode.HALF_UP);
  }
  
  public void setEasuf(Integer pEasuf) {
    if (pEasuf == null) {
      return;
    }
    easuf = BigDecimal.valueOf(pEasuf);
  }
  
  public Integer getEasuf() {
    return easuf.intValue();
  }
  
  public void setEacre(BigDecimal pEacre) {
    if (pEacre == null) {
      return;
    }
    eacre = pEacre.setScale(DECIMAL_EACRE, RoundingMode.HALF_UP);
  }
  
  public void setEacre(Integer pEacre) {
    if (pEacre == null) {
      return;
    }
    eacre = BigDecimal.valueOf(pEacre);
  }
  
  public void setEacre(Date pEacre) {
    if (pEacre == null) {
      return;
    }
    eacre = BigDecimal.valueOf(ConvertDate.dateToDb2(pEacre));
  }
  
  public Integer getEacre() {
    return eacre.intValue();
  }
  
  public Date getEacreConvertiEnDate() {
    return ConvertDate.db2ToDate(eacre.intValue(), null);
  }
  
  public void setEadat(BigDecimal pEadat) {
    if (pEadat == null) {
      return;
    }
    eadat = pEadat.setScale(DECIMAL_EADAT, RoundingMode.HALF_UP);
  }
  
  public void setEadat(Integer pEadat) {
    if (pEadat == null) {
      return;
    }
    eadat = BigDecimal.valueOf(pEadat);
  }
  
  public void setEadat(Date pEadat) {
    if (pEadat == null) {
      return;
    }
    eadat = BigDecimal.valueOf(ConvertDate.dateToDb2(pEadat));
  }
  
  public Integer getEadat() {
    return eadat.intValue();
  }
  
  public Date getEadatConvertiEnDate() {
    return ConvertDate.db2ToDate(eadat.intValue(), null);
  }
  
  public void setEahom(BigDecimal pEahom) {
    if (pEahom == null) {
      return;
    }
    eahom = pEahom.setScale(DECIMAL_EAHOM, RoundingMode.HALF_UP);
  }
  
  public void setEahom(Integer pEahom) {
    if (pEahom == null) {
      return;
    }
    eahom = BigDecimal.valueOf(pEahom);
  }
  
  public void setEahom(Date pEahom) {
    if (pEahom == null) {
      return;
    }
    eahom = BigDecimal.valueOf(ConvertDate.dateToDb2(pEahom));
  }
  
  public Integer getEahom() {
    return eahom.intValue();
  }
  
  public Date getEahomConvertiEnDate() {
    return ConvertDate.db2ToDate(eahom.intValue(), null);
  }
  
  public void setEarec(BigDecimal pEarec) {
    if (pEarec == null) {
      return;
    }
    earec = pEarec.setScale(DECIMAL_EAREC, RoundingMode.HALF_UP);
  }
  
  public void setEarec(Integer pEarec) {
    if (pEarec == null) {
      return;
    }
    earec = BigDecimal.valueOf(pEarec);
  }
  
  public void setEarec(Date pEarec) {
    if (pEarec == null) {
      return;
    }
    earec = BigDecimal.valueOf(ConvertDate.dateToDb2(pEarec));
  }
  
  public Integer getEarec() {
    return earec.intValue();
  }
  
  public Date getEarecConvertiEnDate() {
    return ConvertDate.db2ToDate(earec.intValue(), null);
  }
  
  public void setEafac(BigDecimal pEafac) {
    if (pEafac == null) {
      return;
    }
    eafac = pEafac.setScale(DECIMAL_EAFAC, RoundingMode.HALF_UP);
  }
  
  public void setEafac(Integer pEafac) {
    if (pEafac == null) {
      return;
    }
    eafac = BigDecimal.valueOf(pEafac);
  }
  
  public void setEafac(Date pEafac) {
    if (pEafac == null) {
      return;
    }
    eafac = BigDecimal.valueOf(ConvertDate.dateToDb2(pEafac));
  }
  
  public Integer getEafac() {
    return eafac.intValue();
  }
  
  public Date getEafacConvertiEnDate() {
    return ConvertDate.db2ToDate(eafac.intValue(), null);
  }
  
  public void setEanfa(BigDecimal pEanfa) {
    if (pEanfa == null) {
      return;
    }
    eanfa = pEanfa.setScale(DECIMAL_EANFA, RoundingMode.HALF_UP);
  }
  
  public void setEanfa(Integer pEanfa) {
    if (pEanfa == null) {
      return;
    }
    eanfa = BigDecimal.valueOf(pEanfa);
  }
  
  public Integer getEanfa() {
    return eanfa.intValue();
  }
  
  public void setEanls(BigDecimal pEanls) {
    if (pEanls == null) {
      return;
    }
    eanls = pEanls.setScale(DECIMAL_EANLS, RoundingMode.HALF_UP);
  }
  
  public void setEanls(Integer pEanls) {
    if (pEanls == null) {
      return;
    }
    eanls = BigDecimal.valueOf(pEanls);
  }
  
  public Integer getEanls() {
    return eanls.intValue();
  }
  
  public void setEapli(BigDecimal pEapli) {
    if (pEapli == null) {
      return;
    }
    eapli = pEapli.setScale(DECIMAL_EAPLI, RoundingMode.HALF_UP);
  }
  
  public void setEapli(Integer pEapli) {
    if (pEapli == null) {
      return;
    }
    eapli = BigDecimal.valueOf(pEapli);
  }
  
  public Integer getEapli() {
    return eapli.intValue();
  }
  
  public void setEadli(BigDecimal pEadli) {
    if (pEadli == null) {
      return;
    }
    eadli = pEadli.setScale(DECIMAL_EADLI, RoundingMode.HALF_UP);
  }
  
  public void setEadli(Integer pEadli) {
    if (pEadli == null) {
      return;
    }
    eadli = BigDecimal.valueOf(pEadli);
  }
  
  public Integer getEadli() {
    return eadli.intValue();
  }
  
  public void setEanli(BigDecimal pEanli) {
    if (pEanli == null) {
      return;
    }
    eanli = pEanli.setScale(DECIMAL_EANLI, RoundingMode.HALF_UP);
  }
  
  public void setEanli(Integer pEanli) {
    if (pEanli == null) {
      return;
    }
    eanli = BigDecimal.valueOf(pEanli);
  }
  
  public Integer getEanli() {
    return eanli.intValue();
  }
  
  public void setEatlv(BigDecimal pEatlv) {
    if (pEatlv == null) {
      return;
    }
    eatlv = pEatlv.setScale(DECIMAL_EATLV, RoundingMode.HALF_UP);
  }
  
  public void setEatlv(Integer pEatlv) {
    if (pEatlv == null) {
      return;
    }
    eatlv = BigDecimal.valueOf(pEatlv);
  }
  
  public Integer getEatlv() {
    return eatlv.intValue();
  }
  
  public void setEatll(BigDecimal pEatll) {
    if (pEatll == null) {
      return;
    }
    eatll = pEatll.setScale(DECIMAL_EATLL, RoundingMode.HALF_UP);
  }
  
  public void setEatll(Integer pEatll) {
    if (pEatll == null) {
      return;
    }
    eatll = BigDecimal.valueOf(pEatll);
  }
  
  public Integer getEatll() {
    return eatll.intValue();
  }
  
  public void setEadsu(BigDecimal pEadsu) {
    if (pEadsu == null) {
      return;
    }
    eadsu = pEadsu.setScale(DECIMAL_EADSU, RoundingMode.HALF_UP);
  }
  
  public void setEadsu(Integer pEadsu) {
    if (pEadsu == null) {
      return;
    }
    eadsu = BigDecimal.valueOf(pEadsu);
  }
  
  public Integer getEadsu() {
    return eadsu.intValue();
  }
  
  public void setEaeta(BigDecimal pEaeta) {
    if (pEaeta == null) {
      return;
    }
    eaeta = pEaeta.setScale(DECIMAL_EAETA, RoundingMode.HALF_UP);
  }
  
  public void setEaeta(Integer pEaeta) {
    if (pEaeta == null) {
      return;
    }
    eaeta = BigDecimal.valueOf(pEaeta);
  }
  
  public Integer getEaeta() {
    return eaeta.intValue();
  }
  
  public void setEaedt(BigDecimal pEaedt) {
    if (pEaedt == null) {
      return;
    }
    eaedt = pEaedt.setScale(DECIMAL_EAEDT, RoundingMode.HALF_UP);
  }
  
  public void setEaedt(Integer pEaedt) {
    if (pEaedt == null) {
      return;
    }
    eaedt = BigDecimal.valueOf(pEaedt);
  }
  
  public Integer getEaedt() {
    return eaedt.intValue();
  }
  
  public void setEatfc(BigDecimal pEatfc) {
    if (pEatfc == null) {
      return;
    }
    eatfc = pEatfc.setScale(DECIMAL_EATFC, RoundingMode.HALF_UP);
  }
  
  public void setEatfc(Integer pEatfc) {
    if (pEatfc == null) {
      return;
    }
    eatfc = BigDecimal.valueOf(pEatfc);
  }
  
  public Integer getEatfc() {
    return eatfc.intValue();
  }
  
  public void setEatt(BigDecimal pEatt) {
    if (pEatt == null) {
      return;
    }
    eatt = pEatt.setScale(DECIMAL_EATT, RoundingMode.HALF_UP);
  }
  
  public void setEatt(Integer pEatt) {
    if (pEatt == null) {
      return;
    }
    eatt = BigDecimal.valueOf(pEatt);
  }
  
  public Integer getEatt() {
    return eatt.intValue();
  }
  
  public void setEanrg(BigDecimal pEanrg) {
    if (pEanrg == null) {
      return;
    }
    eanrg = pEanrg.setScale(DECIMAL_EANRG, RoundingMode.HALF_UP);
  }
  
  public void setEanrg(Integer pEanrg) {
    if (pEanrg == null) {
      return;
    }
    eanrg = BigDecimal.valueOf(pEanrg);
  }
  
  public Integer getEanrg() {
    return eanrg.intValue();
  }
  
  public void setEatva1(BigDecimal pEatva1) {
    if (pEatva1 == null) {
      return;
    }
    eatva1 = pEatva1.setScale(DECIMAL_EATVA1, RoundingMode.HALF_UP);
  }
  
  public void setEatva1(Integer pEatva1) {
    if (pEatva1 == null) {
      return;
    }
    eatva1 = BigDecimal.valueOf(pEatva1);
  }
  
  public Integer getEatva1() {
    return eatva1.intValue();
  }
  
  public void setEatva2(BigDecimal pEatva2) {
    if (pEatva2 == null) {
      return;
    }
    eatva2 = pEatva2.setScale(DECIMAL_EATVA2, RoundingMode.HALF_UP);
  }
  
  public void setEatva2(Integer pEatva2) {
    if (pEatva2 == null) {
      return;
    }
    eatva2 = BigDecimal.valueOf(pEatva2);
  }
  
  public Integer getEatva2() {
    return eatva2.intValue();
  }
  
  public void setEatva3(BigDecimal pEatva3) {
    if (pEatva3 == null) {
      return;
    }
    eatva3 = pEatva3.setScale(DECIMAL_EATVA3, RoundingMode.HALF_UP);
  }
  
  public void setEatva3(Integer pEatva3) {
    if (pEatva3 == null) {
      return;
    }
    eatva3 = BigDecimal.valueOf(pEatva3);
  }
  
  public Integer getEatva3() {
    return eatva3.intValue();
  }
  
  public void setEarlv(BigDecimal pEarlv) {
    if (pEarlv == null) {
      return;
    }
    earlv = pEarlv.setScale(DECIMAL_EARLV, RoundingMode.HALF_UP);
  }
  
  public void setEarlv(Integer pEarlv) {
    if (pEarlv == null) {
      return;
    }
    earlv = BigDecimal.valueOf(pEarlv);
  }
  
  public Integer getEarlv() {
    return earlv.intValue();
  }
  
  public void setEanex(BigDecimal pEanex) {
    if (pEanex == null) {
      return;
    }
    eanex = pEanex.setScale(DECIMAL_EANEX, RoundingMode.HALF_UP);
  }
  
  public void setEanex(Integer pEanex) {
    if (pEanex == null) {
      return;
    }
    eanex = BigDecimal.valueOf(pEanex);
  }
  
  public Integer getEanex() {
    return eanex.intValue();
  }
  
  public void setEaexc(BigDecimal pEaexc) {
    if (pEaexc == null) {
      return;
    }
    eaexc = pEaexc.setScale(DECIMAL_EAEXC, RoundingMode.HALF_UP);
  }
  
  public void setEaexc(Integer pEaexc) {
    if (pEaexc == null) {
      return;
    }
    eaexc = BigDecimal.valueOf(pEaexc);
  }
  
  public Integer getEaexc() {
    return eaexc.intValue();
  }
  
  public void setEaexe(BigDecimal pEaexe) {
    if (pEaexe == null) {
      return;
    }
    eaexe = pEaexe.setScale(DECIMAL_EAEXE, RoundingMode.HALF_UP);
  }
  
  public void setEaexe(Integer pEaexe) {
    if (pEaexe == null) {
      return;
    }
    eaexe = BigDecimal.valueOf(pEaexe);
  }
  
  public Integer getEaexe() {
    return eaexe.intValue();
  }
  
  public void setEanrr(BigDecimal pEanrr) {
    if (pEanrr == null) {
      return;
    }
    eanrr = pEanrr.setScale(DECIMAL_EANRR, RoundingMode.HALF_UP);
  }
  
  public void setEanrr(Integer pEanrr) {
    if (pEanrr == null) {
      return;
    }
    eanrr = BigDecimal.valueOf(pEanrr);
  }
  
  public Integer getEanrr() {
    return eanrr.intValue();
  }
  
  public void setEasgn(BigDecimal pEasgn) {
    if (pEasgn == null) {
      return;
    }
    easgn = pEasgn.setScale(DECIMAL_EASGN, RoundingMode.HALF_UP);
  }
  
  public void setEasgn(Integer pEasgn) {
    if (pEasgn == null) {
      return;
    }
    easgn = BigDecimal.valueOf(pEasgn);
  }
  
  public Integer getEasgn() {
    return easgn.intValue();
  }
  
  public void setEaliv(BigDecimal pEaliv) {
    if (pEaliv == null) {
      return;
    }
    ealiv = pEaliv.setScale(DECIMAL_EALIV, RoundingMode.HALF_UP);
  }
  
  public void setEaliv(Integer pEaliv) {
    if (pEaliv == null) {
      return;
    }
    ealiv = BigDecimal.valueOf(pEaliv);
  }
  
  public Integer getEaliv() {
    return ealiv.intValue();
  }
  
  public void setEacpt(BigDecimal pEacpt) {
    if (pEacpt == null) {
      return;
    }
    eacpt = pEacpt.setScale(DECIMAL_EACPT, RoundingMode.HALF_UP);
  }
  
  public void setEacpt(Integer pEacpt) {
    if (pEacpt == null) {
      return;
    }
    eacpt = BigDecimal.valueOf(pEacpt);
  }
  
  public Integer getEacpt() {
    return eacpt.intValue();
  }
  
  public void setEapgc(BigDecimal pEapgc) {
    if (pEapgc == null) {
      return;
    }
    eapgc = pEapgc.setScale(DECIMAL_EAPGC, RoundingMode.HALF_UP);
  }
  
  public void setEapgc(Integer pEapgc) {
    if (pEapgc == null) {
      return;
    }
    eapgc = BigDecimal.valueOf(pEapgc);
  }
  
  public Integer getEapgc() {
    return eapgc.intValue();
  }
  
  public void setEachf(BigDecimal pEachf) {
    if (pEachf == null) {
      return;
    }
    eachf = pEachf.setScale(DECIMAL_EACHF, RoundingMode.HALF_UP);
  }
  
  public void setEachf(Integer pEachf) {
    if (pEachf == null) {
      return;
    }
    eachf = BigDecimal.valueOf(pEachf);
  }
  
  public Integer getEachf() {
    return eachf.intValue();
  }
  
  public void setEaina(BigDecimal pEaina) {
    if (pEaina == null) {
      return;
    }
    eaina = pEaina.setScale(DECIMAL_EAINA, RoundingMode.HALF_UP);
  }
  
  public void setEaina(Integer pEaina) {
    if (pEaina == null) {
      return;
    }
    eaina = BigDecimal.valueOf(pEaina);
  }
  
  public Integer getEaina() {
    return eaina.intValue();
  }
  
  public void setEacol(BigDecimal pEacol) {
    if (pEacol == null) {
      return;
    }
    eacol = pEacol.setScale(DECIMAL_EACOL, RoundingMode.HALF_UP);
  }
  
  public void setEacol(Integer pEacol) {
    if (pEacol == null) {
      return;
    }
    eacol = BigDecimal.valueOf(pEacol);
  }
  
  public Integer getEacol() {
    return eacol.intValue();
  }
  
  public void setEafrs(BigDecimal pEafrs) {
    if (pEafrs == null) {
      return;
    }
    eafrs = pEafrs.setScale(DECIMAL_EAFRS, RoundingMode.HALF_UP);
  }
  
  public void setEafrs(Integer pEafrs) {
    if (pEafrs == null) {
      return;
    }
    eafrs = BigDecimal.valueOf(pEafrs);
  }
  
  public Integer getEafrs() {
    return eafrs.intValue();
  }
  
  public void setEadlp(BigDecimal pEadlp) {
    if (pEadlp == null) {
      return;
    }
    eadlp = pEadlp.setScale(DECIMAL_EADLP, RoundingMode.HALF_UP);
  }
  
  public void setEadlp(Integer pEadlp) {
    if (pEadlp == null) {
      return;
    }
    eadlp = BigDecimal.valueOf(pEadlp);
  }
  
  public void setEadlp(Date pEadlp) {
    if (pEadlp == null) {
      return;
    }
    eadlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pEadlp));
  }
  
  public Integer getEadlp() {
    return eadlp.intValue();
  }
  
  public Date getEadlpConvertiEnDate() {
    return ConvertDate.db2ToDate(eadlp.intValue(), null);
  }
  
  public void setEanum0(BigDecimal pEanum0) {
    if (pEanum0 == null) {
      return;
    }
    eanum0 = pEanum0.setScale(DECIMAL_EANUM0, RoundingMode.HALF_UP);
  }
  
  public void setEanum0(Integer pEanum0) {
    if (pEanum0 == null) {
      return;
    }
    eanum0 = BigDecimal.valueOf(pEanum0);
  }
  
  public Integer getEanum0() {
    return eanum0.intValue();
  }
  
  public void setEasuf0(BigDecimal pEasuf0) {
    if (pEasuf0 == null) {
      return;
    }
    easuf0 = pEasuf0.setScale(DECIMAL_EASUF0, RoundingMode.HALF_UP);
  }
  
  public void setEasuf0(Integer pEasuf0) {
    if (pEasuf0 == null) {
      return;
    }
    easuf0 = BigDecimal.valueOf(pEasuf0);
  }
  
  public Integer getEasuf0() {
    return easuf0.intValue();
  }
  
  public void setEaavr(Character pEaavr) {
    if (pEaavr == null) {
      return;
    }
    eaavr = String.valueOf(pEaavr);
  }
  
  public Character getEaavr() {
    return eaavr.charAt(0);
  }
  
  public void setEatfa(Character pEatfa) {
    if (pEatfa == null) {
      return;
    }
    eatfa = String.valueOf(pEatfa);
  }
  
  public Character getEatfa() {
    return eatfa.charAt(0);
  }
  
  public void setEanat(Character pEanat) {
    if (pEanat == null) {
      return;
    }
    eanat = String.valueOf(pEanat);
  }
  
  public Character getEanat() {
    return eanat.charAt(0);
  }
  
  public void setEaesc(BigDecimal pEaesc) {
    if (pEaesc == null) {
      return;
    }
    eaesc = pEaesc.setScale(DECIMAL_EAESC, RoundingMode.HALF_UP);
  }
  
  public void setEaesc(Double pEaesc) {
    if (pEaesc == null) {
      return;
    }
    eaesc = BigDecimal.valueOf(pEaesc).setScale(DECIMAL_EAESC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEaesc() {
    return eaesc.setScale(DECIMAL_EAESC, RoundingMode.HALF_UP);
  }
  
  public void setEae1g(BigDecimal pEae1g) {
    if (pEae1g == null) {
      return;
    }
    eae1g = pEae1g.setScale(DECIMAL_EAE1G, RoundingMode.HALF_UP);
  }
  
  public void setEae1g(Integer pEae1g) {
    if (pEae1g == null) {
      return;
    }
    eae1g = BigDecimal.valueOf(pEae1g);
  }
  
  public void setEae1g(Date pEae1g) {
    if (pEae1g == null) {
      return;
    }
    eae1g = BigDecimal.valueOf(ConvertDate.dateToDb2(pEae1g));
  }
  
  public Integer getEae1g() {
    return eae1g.intValue();
  }
  
  public Date getEae1gConvertiEnDate() {
    return ConvertDate.db2ToDate(eae1g.intValue(), null);
  }
  
  public void setEae2g(BigDecimal pEae2g) {
    if (pEae2g == null) {
      return;
    }
    eae2g = pEae2g.setScale(DECIMAL_EAE2G, RoundingMode.HALF_UP);
  }
  
  public void setEae2g(Integer pEae2g) {
    if (pEae2g == null) {
      return;
    }
    eae2g = BigDecimal.valueOf(pEae2g);
  }
  
  public void setEae2g(Date pEae2g) {
    if (pEae2g == null) {
      return;
    }
    eae2g = BigDecimal.valueOf(ConvertDate.dateToDb2(pEae2g));
  }
  
  public Integer getEae2g() {
    return eae2g.intValue();
  }
  
  public Date getEae2gConvertiEnDate() {
    return ConvertDate.db2ToDate(eae2g.intValue(), null);
  }
  
  public void setEae3g(BigDecimal pEae3g) {
    if (pEae3g == null) {
      return;
    }
    eae3g = pEae3g.setScale(DECIMAL_EAE3G, RoundingMode.HALF_UP);
  }
  
  public void setEae3g(Integer pEae3g) {
    if (pEae3g == null) {
      return;
    }
    eae3g = BigDecimal.valueOf(pEae3g);
  }
  
  public void setEae3g(Date pEae3g) {
    if (pEae3g == null) {
      return;
    }
    eae3g = BigDecimal.valueOf(ConvertDate.dateToDb2(pEae3g));
  }
  
  public Integer getEae3g() {
    return eae3g.intValue();
  }
  
  public Date getEae3gConvertiEnDate() {
    return ConvertDate.db2ToDate(eae3g.intValue(), null);
  }
  
  public void setEae4g(BigDecimal pEae4g) {
    if (pEae4g == null) {
      return;
    }
    eae4g = pEae4g.setScale(DECIMAL_EAE4G, RoundingMode.HALF_UP);
  }
  
  public void setEae4g(Integer pEae4g) {
    if (pEae4g == null) {
      return;
    }
    eae4g = BigDecimal.valueOf(pEae4g);
  }
  
  public void setEae4g(Date pEae4g) {
    if (pEae4g == null) {
      return;
    }
    eae4g = BigDecimal.valueOf(ConvertDate.dateToDb2(pEae4g));
  }
  
  public Integer getEae4g() {
    return eae4g.intValue();
  }
  
  public Date getEae4gConvertiEnDate() {
    return ConvertDate.db2ToDate(eae4g.intValue(), null);
  }
  
  public void setEar1g(BigDecimal pEar1g) {
    if (pEar1g == null) {
      return;
    }
    ear1g = pEar1g.setScale(DECIMAL_EAR1G, RoundingMode.HALF_UP);
  }
  
  public void setEar1g(Double pEar1g) {
    if (pEar1g == null) {
      return;
    }
    ear1g = BigDecimal.valueOf(pEar1g).setScale(DECIMAL_EAR1G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEar1g() {
    return ear1g.setScale(DECIMAL_EAR1G, RoundingMode.HALF_UP);
  }
  
  public void setEar2g(BigDecimal pEar2g) {
    if (pEar2g == null) {
      return;
    }
    ear2g = pEar2g.setScale(DECIMAL_EAR2G, RoundingMode.HALF_UP);
  }
  
  public void setEar2g(Double pEar2g) {
    if (pEar2g == null) {
      return;
    }
    ear2g = BigDecimal.valueOf(pEar2g).setScale(DECIMAL_EAR2G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEar2g() {
    return ear2g.setScale(DECIMAL_EAR2G, RoundingMode.HALF_UP);
  }
  
  public void setEar3g(BigDecimal pEar3g) {
    if (pEar3g == null) {
      return;
    }
    ear3g = pEar3g.setScale(DECIMAL_EAR3G, RoundingMode.HALF_UP);
  }
  
  public void setEar3g(Double pEar3g) {
    if (pEar3g == null) {
      return;
    }
    ear3g = BigDecimal.valueOf(pEar3g).setScale(DECIMAL_EAR3G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEar3g() {
    return ear3g.setScale(DECIMAL_EAR3G, RoundingMode.HALF_UP);
  }
  
  public void setEar4g(BigDecimal pEar4g) {
    if (pEar4g == null) {
      return;
    }
    ear4g = pEar4g.setScale(DECIMAL_EAR4G, RoundingMode.HALF_UP);
  }
  
  public void setEar4g(Double pEar4g) {
    if (pEar4g == null) {
      return;
    }
    ear4g = BigDecimal.valueOf(pEar4g).setScale(DECIMAL_EAR4G, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEar4g() {
    return ear4g.setScale(DECIMAL_EAR4G, RoundingMode.HALF_UP);
  }
  
  public void setEasl1(BigDecimal pEasl1) {
    if (pEasl1 == null) {
      return;
    }
    easl1 = pEasl1.setScale(DECIMAL_EASL1, RoundingMode.HALF_UP);
  }
  
  public void setEasl1(Double pEasl1) {
    if (pEasl1 == null) {
      return;
    }
    easl1 = BigDecimal.valueOf(pEasl1).setScale(DECIMAL_EASL1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEasl1() {
    return easl1.setScale(DECIMAL_EASL1, RoundingMode.HALF_UP);
  }
  
  public void setEasl2(BigDecimal pEasl2) {
    if (pEasl2 == null) {
      return;
    }
    easl2 = pEasl2.setScale(DECIMAL_EASL2, RoundingMode.HALF_UP);
  }
  
  public void setEasl2(Double pEasl2) {
    if (pEasl2 == null) {
      return;
    }
    easl2 = BigDecimal.valueOf(pEasl2).setScale(DECIMAL_EASL2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEasl2() {
    return easl2.setScale(DECIMAL_EASL2, RoundingMode.HALF_UP);
  }
  
  public void setEasl3(BigDecimal pEasl3) {
    if (pEasl3 == null) {
      return;
    }
    easl3 = pEasl3.setScale(DECIMAL_EASL3, RoundingMode.HALF_UP);
  }
  
  public void setEasl3(Double pEasl3) {
    if (pEasl3 == null) {
      return;
    }
    easl3 = BigDecimal.valueOf(pEasl3).setScale(DECIMAL_EASL3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEasl3() {
    return easl3.setScale(DECIMAL_EASL3, RoundingMode.HALF_UP);
  }
  
  public void setEathtl(BigDecimal pEathtl) {
    if (pEathtl == null) {
      return;
    }
    eathtl = pEathtl.setScale(DECIMAL_EATHTL, RoundingMode.HALF_UP);
  }
  
  public void setEathtl(Double pEathtl) {
    if (pEathtl == null) {
      return;
    }
    eathtl = BigDecimal.valueOf(pEathtl).setScale(DECIMAL_EATHTL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEathtl() {
    return eathtl.setScale(DECIMAL_EATHTL, RoundingMode.HALF_UP);
  }
  
  public void setEatl1(BigDecimal pEatl1) {
    if (pEatl1 == null) {
      return;
    }
    eatl1 = pEatl1.setScale(DECIMAL_EATL1, RoundingMode.HALF_UP);
  }
  
  public void setEatl1(Double pEatl1) {
    if (pEatl1 == null) {
      return;
    }
    eatl1 = BigDecimal.valueOf(pEatl1).setScale(DECIMAL_EATL1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEatl1() {
    return eatl1.setScale(DECIMAL_EATL1, RoundingMode.HALF_UP);
  }
  
  public void setEatl2(BigDecimal pEatl2) {
    if (pEatl2 == null) {
      return;
    }
    eatl2 = pEatl2.setScale(DECIMAL_EATL2, RoundingMode.HALF_UP);
  }
  
  public void setEatl2(Double pEatl2) {
    if (pEatl2 == null) {
      return;
    }
    eatl2 = BigDecimal.valueOf(pEatl2).setScale(DECIMAL_EATL2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEatl2() {
    return eatl2.setScale(DECIMAL_EATL2, RoundingMode.HALF_UP);
  }
  
  public void setEatl3(BigDecimal pEatl3) {
    if (pEatl3 == null) {
      return;
    }
    eatl3 = pEatl3.setScale(DECIMAL_EATL3, RoundingMode.HALF_UP);
  }
  
  public void setEatl3(Double pEatl3) {
    if (pEatl3 == null) {
      return;
    }
    eatl3 = BigDecimal.valueOf(pEatl3).setScale(DECIMAL_EATL3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEatl3() {
    return eatl3.setScale(DECIMAL_EATL3, RoundingMode.HALF_UP);
  }
  
  public void setEatv1(BigDecimal pEatv1) {
    if (pEatv1 == null) {
      return;
    }
    eatv1 = pEatv1.setScale(DECIMAL_EATV1, RoundingMode.HALF_UP);
  }
  
  public void setEatv1(Double pEatv1) {
    if (pEatv1 == null) {
      return;
    }
    eatv1 = BigDecimal.valueOf(pEatv1).setScale(DECIMAL_EATV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEatv1() {
    return eatv1.setScale(DECIMAL_EATV1, RoundingMode.HALF_UP);
  }
  
  public void setEatv2(BigDecimal pEatv2) {
    if (pEatv2 == null) {
      return;
    }
    eatv2 = pEatv2.setScale(DECIMAL_EATV2, RoundingMode.HALF_UP);
  }
  
  public void setEatv2(Double pEatv2) {
    if (pEatv2 == null) {
      return;
    }
    eatv2 = BigDecimal.valueOf(pEatv2).setScale(DECIMAL_EATV2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEatv2() {
    return eatv2.setScale(DECIMAL_EATV2, RoundingMode.HALF_UP);
  }
  
  public void setEatv3(BigDecimal pEatv3) {
    if (pEatv3 == null) {
      return;
    }
    eatv3 = pEatv3.setScale(DECIMAL_EATV3, RoundingMode.HALF_UP);
  }
  
  public void setEatv3(Double pEatv3) {
    if (pEatv3 == null) {
      return;
    }
    eatv3 = BigDecimal.valueOf(pEatv3).setScale(DECIMAL_EATV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEatv3() {
    return eatv3.setScale(DECIMAL_EATV3, RoundingMode.HALF_UP);
  }
  
  public void setEames(BigDecimal pEames) {
    if (pEames == null) {
      return;
    }
    eames = pEames.setScale(DECIMAL_EAMES, RoundingMode.HALF_UP);
  }
  
  public void setEames(Double pEames) {
    if (pEames == null) {
      return;
    }
    eames = BigDecimal.valueOf(pEames).setScale(DECIMAL_EAMES, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEames() {
    return eames.setScale(DECIMAL_EAMES, RoundingMode.HALF_UP);
  }
  
  public void setEattc(BigDecimal pEattc) {
    if (pEattc == null) {
      return;
    }
    eattc = pEattc.setScale(DECIMAL_EATTC, RoundingMode.HALF_UP);
  }
  
  public void setEattc(Double pEattc) {
    if (pEattc == null) {
      return;
    }
    eattc = BigDecimal.valueOf(pEattc).setScale(DECIMAL_EATTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEattc() {
    return eattc.setScale(DECIMAL_EATTC, RoundingMode.HALF_UP);
  }
  
  public void setEarg1(String pEarg1) {
    if (pEarg1 == null) {
      return;
    }
    earg1 = pEarg1;
  }
  
  public String getEarg1() {
    return earg1;
  }
  
  public void setEaec1(String pEaec1) {
    if (pEaec1 == null) {
      return;
    }
    eaec1 = pEaec1;
  }
  
  public String getEaec1() {
    return eaec1;
  }
  
  public void setEapc1(String pEapc1) {
    if (pEapc1 == null) {
      return;
    }
    eapc1 = pEapc1;
  }
  
  public String getEapc1() {
    return eapc1;
  }
  
  public void setEarg2(String pEarg2) {
    if (pEarg2 == null) {
      return;
    }
    earg2 = pEarg2;
  }
  
  public String getEarg2() {
    return earg2;
  }
  
  public void setEaec2(String pEaec2) {
    if (pEaec2 == null) {
      return;
    }
    eaec2 = pEaec2;
  }
  
  public String getEaec2() {
    return eaec2;
  }
  
  public void setEapc2(String pEapc2) {
    if (pEapc2 == null) {
      return;
    }
    eapc2 = pEapc2;
  }
  
  public String getEapc2() {
    return eapc2;
  }
  
  public void setEarg3(String pEarg3) {
    if (pEarg3 == null) {
      return;
    }
    earg3 = pEarg3;
  }
  
  public String getEarg3() {
    return earg3;
  }
  
  public void setEaec3(String pEaec3) {
    if (pEaec3 == null) {
      return;
    }
    eaec3 = pEaec3;
  }
  
  public String getEaec3() {
    return eaec3;
  }
  
  public void setEapc3(String pEapc3) {
    if (pEapc3 == null) {
      return;
    }
    eapc3 = pEapc3;
  }
  
  public String getEapc3() {
    return eapc3;
  }
  
  public void setEarg4(String pEarg4) {
    if (pEarg4 == null) {
      return;
    }
    earg4 = pEarg4;
  }
  
  public String getEarg4() {
    return earg4;
  }
  
  public void setEaec4(String pEaec4) {
    if (pEaec4 == null) {
      return;
    }
    eaec4 = pEaec4;
  }
  
  public String getEaec4() {
    return eaec4;
  }
  
  public void setEapc4(String pEapc4) {
    if (pEapc4 == null) {
      return;
    }
    eapc4 = pEapc4;
  }
  
  public String getEapc4() {
    return eapc4;
  }
  
  public void setEamag(String pEamag) {
    if (pEamag == null) {
      return;
    }
    eamag = pEamag;
  }
  
  public String getEamag() {
    return eamag;
  }
  
  public void setEarbl(String pEarbl) {
    if (pEarbl == null) {
      return;
    }
    earbl = pEarbl;
  }
  
  public String getEarbl() {
    return earbl;
  }
  
  public void setEasan(String pEasan) {
    if (pEasan == null) {
      return;
    }
    easan = pEasan;
  }
  
  public String getEasan() {
    return easan;
  }
  
  public void setEaact(String pEaact) {
    if (pEaact == null) {
      return;
    }
    eaact = pEaact;
  }
  
  public String getEaact() {
    return eaact;
  }
  
  public void setEallv(String pEallv) {
    if (pEallv == null) {
      return;
    }
    eallv = pEallv;
  }
  
  public String getEallv() {
    return eallv;
  }
  
  public void setEaach(String pEaach) {
    if (pEaach == null) {
      return;
    }
    eaach = pEaach;
  }
  
  public String getEaach() {
    return eaach;
  }
  
  public void setEacja(String pEacja) {
    if (pEacja == null) {
      return;
    }
    eacja = pEacja;
  }
  
  public String getEacja() {
    return eacja;
  }
  
  public void setEadev(String pEadev) {
    if (pEadev == null) {
      return;
    }
    eadev = pEadev;
  }
  
  public String getEadev() {
    return eadev;
  }
  
  public void setEachg(BigDecimal pEachg) {
    if (pEachg == null) {
      return;
    }
    eachg = pEachg.setScale(DECIMAL_EACHG, RoundingMode.HALF_UP);
  }
  
  public void setEachg(Double pEachg) {
    if (pEachg == null) {
      return;
    }
    eachg = BigDecimal.valueOf(pEachg).setScale(DECIMAL_EACHG, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEachg() {
    return eachg.setScale(DECIMAL_EACHG, RoundingMode.HALF_UP);
  }
  
  public void setEacpr(BigDecimal pEacpr) {
    if (pEacpr == null) {
      return;
    }
    eacpr = pEacpr.setScale(DECIMAL_EACPR, RoundingMode.HALF_UP);
  }
  
  public void setEacpr(Double pEacpr) {
    if (pEacpr == null) {
      return;
    }
    eacpr = BigDecimal.valueOf(pEacpr).setScale(DECIMAL_EACPR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEacpr() {
    return eacpr.setScale(DECIMAL_EACPR, RoundingMode.HALF_UP);
  }
  
  public void setEabas(BigDecimal pEabas) {
    if (pEabas == null) {
      return;
    }
    eabas = pEabas.setScale(DECIMAL_EABAS, RoundingMode.HALF_UP);
  }
  
  public void setEabas(Integer pEabas) {
    if (pEabas == null) {
      return;
    }
    eabas = BigDecimal.valueOf(pEabas);
  }
  
  public Integer getEabas() {
    return eabas.intValue();
  }
  
  public void setEados(String pEados) {
    if (pEados == null) {
      return;
    }
    eados = pEados;
  }
  
  public String getEados() {
    return eados;
  }
  
  public void setEacnt(String pEacnt) {
    if (pEacnt == null) {
      return;
    }
    eacnt = pEacnt;
  }
  
  public String getEacnt() {
    return eacnt;
  }
  
  public void setEapds(BigDecimal pEapds) {
    if (pEapds == null) {
      return;
    }
    eapds = pEapds.setScale(DECIMAL_EAPDS, RoundingMode.HALF_UP);
  }
  
  public void setEapds(Double pEapds) {
    if (pEapds == null) {
      return;
    }
    eapds = BigDecimal.valueOf(pEapds).setScale(DECIMAL_EAPDS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEapds() {
    return eapds.setScale(DECIMAL_EAPDS, RoundingMode.HALF_UP);
  }
  
  public void setEavol(BigDecimal pEavol) {
    if (pEavol == null) {
      return;
    }
    eavol = pEavol.setScale(DECIMAL_EAVOL, RoundingMode.HALF_UP);
  }
  
  public void setEavol(Double pEavol) {
    if (pEavol == null) {
      return;
    }
    eavol = BigDecimal.valueOf(pEavol).setScale(DECIMAL_EAVOL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEavol() {
    return eavol.setScale(DECIMAL_EAVOL, RoundingMode.HALF_UP);
  }
  
  public void setEamta(BigDecimal pEamta) {
    if (pEamta == null) {
      return;
    }
    eamta = pEamta.setScale(DECIMAL_EAMTA, RoundingMode.HALF_UP);
  }
  
  public void setEamta(Double pEamta) {
    if (pEamta == null) {
      return;
    }
    eamta = BigDecimal.valueOf(pEamta).setScale(DECIMAL_EAMTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEamta() {
    return eamta.setScale(DECIMAL_EAMTA, RoundingMode.HALF_UP);
  }
  
  public void setEarem1(BigDecimal pEarem1) {
    if (pEarem1 == null) {
      return;
    }
    earem1 = pEarem1.setScale(DECIMAL_EAREM1, RoundingMode.HALF_UP);
  }
  
  public void setEarem1(Double pEarem1) {
    if (pEarem1 == null) {
      return;
    }
    earem1 = BigDecimal.valueOf(pEarem1).setScale(DECIMAL_EAREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarem1() {
    return earem1.setScale(DECIMAL_EAREM1, RoundingMode.HALF_UP);
  }
  
  public void setEarem2(BigDecimal pEarem2) {
    if (pEarem2 == null) {
      return;
    }
    earem2 = pEarem2.setScale(DECIMAL_EAREM2, RoundingMode.HALF_UP);
  }
  
  public void setEarem2(Double pEarem2) {
    if (pEarem2 == null) {
      return;
    }
    earem2 = BigDecimal.valueOf(pEarem2).setScale(DECIMAL_EAREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarem2() {
    return earem2.setScale(DECIMAL_EAREM2, RoundingMode.HALF_UP);
  }
  
  public void setEarem3(BigDecimal pEarem3) {
    if (pEarem3 == null) {
      return;
    }
    earem3 = pEarem3.setScale(DECIMAL_EAREM3, RoundingMode.HALF_UP);
  }
  
  public void setEarem3(Double pEarem3) {
    if (pEarem3 == null) {
      return;
    }
    earem3 = BigDecimal.valueOf(pEarem3).setScale(DECIMAL_EAREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarem3() {
    return earem3.setScale(DECIMAL_EAREM3, RoundingMode.HALF_UP);
  }
  
  public void setEarem4(BigDecimal pEarem4) {
    if (pEarem4 == null) {
      return;
    }
    earem4 = pEarem4.setScale(DECIMAL_EAREM4, RoundingMode.HALF_UP);
  }
  
  public void setEarem4(Double pEarem4) {
    if (pEarem4 == null) {
      return;
    }
    earem4 = BigDecimal.valueOf(pEarem4).setScale(DECIMAL_EAREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarem4() {
    return earem4.setScale(DECIMAL_EAREM4, RoundingMode.HALF_UP);
  }
  
  public void setEarem5(BigDecimal pEarem5) {
    if (pEarem5 == null) {
      return;
    }
    earem5 = pEarem5.setScale(DECIMAL_EAREM5, RoundingMode.HALF_UP);
  }
  
  public void setEarem5(Double pEarem5) {
    if (pEarem5 == null) {
      return;
    }
    earem5 = BigDecimal.valueOf(pEarem5).setScale(DECIMAL_EAREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarem5() {
    return earem5.setScale(DECIMAL_EAREM5, RoundingMode.HALF_UP);
  }
  
  public void setEarem6(BigDecimal pEarem6) {
    if (pEarem6 == null) {
      return;
    }
    earem6 = pEarem6.setScale(DECIMAL_EAREM6, RoundingMode.HALF_UP);
  }
  
  public void setEarem6(Double pEarem6) {
    if (pEarem6 == null) {
      return;
    }
    earem6 = BigDecimal.valueOf(pEarem6).setScale(DECIMAL_EAREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarem6() {
    return earem6.setScale(DECIMAL_EAREM6, RoundingMode.HALF_UP);
  }
  
  public void setEatrl(Character pEatrl) {
    if (pEatrl == null) {
      return;
    }
    eatrl = String.valueOf(pEatrl);
  }
  
  public Character getEatrl() {
    return eatrl.charAt(0);
  }
  
  public void setEabrl(Character pEabrl) {
    if (pEabrl == null) {
      return;
    }
    eabrl = String.valueOf(pEabrl);
  }
  
  public Character getEabrl() {
    return eabrl.charAt(0);
  }
  
  public void setEarp1(BigDecimal pEarp1) {
    if (pEarp1 == null) {
      return;
    }
    earp1 = pEarp1.setScale(DECIMAL_EARP1, RoundingMode.HALF_UP);
  }
  
  public void setEarp1(Double pEarp1) {
    if (pEarp1 == null) {
      return;
    }
    earp1 = BigDecimal.valueOf(pEarp1).setScale(DECIMAL_EARP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarp1() {
    return earp1.setScale(DECIMAL_EARP1, RoundingMode.HALF_UP);
  }
  
  public void setEarp2(BigDecimal pEarp2) {
    if (pEarp2 == null) {
      return;
    }
    earp2 = pEarp2.setScale(DECIMAL_EARP2, RoundingMode.HALF_UP);
  }
  
  public void setEarp2(Double pEarp2) {
    if (pEarp2 == null) {
      return;
    }
    earp2 = BigDecimal.valueOf(pEarp2).setScale(DECIMAL_EARP2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarp2() {
    return earp2.setScale(DECIMAL_EARP2, RoundingMode.HALF_UP);
  }
  
  public void setEarp3(BigDecimal pEarp3) {
    if (pEarp3 == null) {
      return;
    }
    earp3 = pEarp3.setScale(DECIMAL_EARP3, RoundingMode.HALF_UP);
  }
  
  public void setEarp3(Double pEarp3) {
    if (pEarp3 == null) {
      return;
    }
    earp3 = BigDecimal.valueOf(pEarp3).setScale(DECIMAL_EARP3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarp3() {
    return earp3.setScale(DECIMAL_EARP3, RoundingMode.HALF_UP);
  }
  
  public void setEarp4(BigDecimal pEarp4) {
    if (pEarp4 == null) {
      return;
    }
    earp4 = pEarp4.setScale(DECIMAL_EARP4, RoundingMode.HALF_UP);
  }
  
  public void setEarp4(Double pEarp4) {
    if (pEarp4 == null) {
      return;
    }
    earp4 = BigDecimal.valueOf(pEarp4).setScale(DECIMAL_EARP4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarp4() {
    return earp4.setScale(DECIMAL_EARP4, RoundingMode.HALF_UP);
  }
  
  public void setEarp5(BigDecimal pEarp5) {
    if (pEarp5 == null) {
      return;
    }
    earp5 = pEarp5.setScale(DECIMAL_EARP5, RoundingMode.HALF_UP);
  }
  
  public void setEarp5(Double pEarp5) {
    if (pEarp5 == null) {
      return;
    }
    earp5 = BigDecimal.valueOf(pEarp5).setScale(DECIMAL_EARP5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarp5() {
    return earp5.setScale(DECIMAL_EARP5, RoundingMode.HALF_UP);
  }
  
  public void setEarp6(BigDecimal pEarp6) {
    if (pEarp6 == null) {
      return;
    }
    earp6 = pEarp6.setScale(DECIMAL_EARP6, RoundingMode.HALF_UP);
  }
  
  public void setEarp6(Double pEarp6) {
    if (pEarp6 == null) {
      return;
    }
    earp6 = BigDecimal.valueOf(pEarp6).setScale(DECIMAL_EARP6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEarp6() {
    return earp6.setScale(DECIMAL_EARP6, RoundingMode.HALF_UP);
  }
  
  public void setEatrp(Character pEatrp) {
    if (pEatrp == null) {
      return;
    }
    eatrp = String.valueOf(pEatrp);
  }
  
  public Character getEatrp() {
    return eatrp.charAt(0);
  }
  
  public void setEatp1(String pEatp1) {
    if (pEatp1 == null) {
      return;
    }
    eatp1 = pEatp1;
  }
  
  public String getEatp1() {
    return eatp1;
  }
  
  public void setEatp2(String pEatp2) {
    if (pEatp2 == null) {
      return;
    }
    eatp2 = pEatp2;
  }
  
  public String getEatp2() {
    return eatp2;
  }
  
  public void setEatp3(String pEatp3) {
    if (pEatp3 == null) {
      return;
    }
    eatp3 = pEatp3;
  }
  
  public String getEatp3() {
    return eatp3;
  }
  
  public void setEatp4(String pEatp4) {
    if (pEatp4 == null) {
      return;
    }
    eatp4 = pEatp4;
  }
  
  public String getEatp4() {
    return eatp4;
  }
  
  public void setEatp5(String pEatp5) {
    if (pEatp5 == null) {
      return;
    }
    eatp5 = pEatp5;
  }
  
  public String getEatp5() {
    return eatp5;
  }
  
  public void setEain1(Character pEain1) {
    if (pEain1 == null) {
      return;
    }
    eain1 = String.valueOf(pEain1);
  }
  
  public Character getEain1() {
    return eain1.charAt(0);
  }
  
  public void setEain2(Character pEain2) {
    if (pEain2 == null) {
      return;
    }
    eain2 = String.valueOf(pEain2);
  }
  
  public Character getEain2() {
    return eain2.charAt(0);
  }
  
  public void setEain3(Character pEain3) {
    if (pEain3 == null) {
      return;
    }
    eain3 = String.valueOf(pEain3);
  }
  
  public Character getEain3() {
    return eain3.charAt(0);
  }
  
  public void setEain4(Character pEain4) {
    if (pEain4 == null) {
      return;
    }
    eain4 = String.valueOf(pEain4);
  }
  
  public Character getEain4() {
    return eain4.charAt(0);
  }
  
  public void setEain5(Character pEain5) {
    if (pEain5 == null) {
      return;
    }
    eain5 = String.valueOf(pEain5);
  }
  
  public Character getEain5() {
    return eain5.charAt(0);
  }
  
  public void setEacct(String pEacct) {
    if (pEacct == null) {
      return;
    }
    eacct = pEacct;
  }
  
  public String getEacct() {
    return eacct;
  }
  
  public void setEadat1(BigDecimal pEadat1) {
    if (pEadat1 == null) {
      return;
    }
    eadat1 = pEadat1.setScale(DECIMAL_EADAT1, RoundingMode.HALF_UP);
  }
  
  public void setEadat1(Integer pEadat1) {
    if (pEadat1 == null) {
      return;
    }
    eadat1 = BigDecimal.valueOf(pEadat1);
  }
  
  public void setEadat1(Date pEadat1) {
    if (pEadat1 == null) {
      return;
    }
    eadat1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pEadat1));
  }
  
  public Integer getEadat1() {
    return eadat1.intValue();
  }
  
  public Date getEadat1ConvertiEnDate() {
    return ConvertDate.db2ToDate(eadat1.intValue(), null);
  }
  
  public void setEarbc(String pEarbc) {
    if (pEarbc == null) {
      return;
    }
    earbc = pEarbc;
  }
  
  public String getEarbc() {
    return earbc;
  }
  
  public void setEaint1(BigDecimal pEaint1) {
    if (pEaint1 == null) {
      return;
    }
    eaint1 = pEaint1.setScale(DECIMAL_EAINT1, RoundingMode.HALF_UP);
  }
  
  public void setEaint1(Integer pEaint1) {
    if (pEaint1 == null) {
      return;
    }
    eaint1 = BigDecimal.valueOf(pEaint1);
  }
  
  public void setEaint1(Date pEaint1) {
    if (pEaint1 == null) {
      return;
    }
    eaint1 = BigDecimal.valueOf(ConvertDate.dateToDb2(pEaint1));
  }
  
  public Integer getEaint1() {
    return eaint1.intValue();
  }
  
  public Date getEaint1ConvertiEnDate() {
    return ConvertDate.db2ToDate(eaint1.intValue(), null);
  }
  
  public void setEaint2(BigDecimal pEaint2) {
    if (pEaint2 == null) {
      return;
    }
    eaint2 = pEaint2.setScale(DECIMAL_EAINT2, RoundingMode.HALF_UP);
  }
  
  public void setEaint2(Integer pEaint2) {
    if (pEaint2 == null) {
      return;
    }
    eaint2 = BigDecimal.valueOf(pEaint2);
  }
  
  public void setEaint2(Date pEaint2) {
    if (pEaint2 == null) {
      return;
    }
    eaint2 = BigDecimal.valueOf(ConvertDate.dateToDb2(pEaint2));
  }
  
  public Integer getEaint2() {
    return eaint2.intValue();
  }
  
  public Date getEaint2ConvertiEnDate() {
    return ConvertDate.db2ToDate(eaint2.intValue(), null);
  }
  
  public void setEaetai(Character pEaetai) {
    if (pEaetai == null) {
      return;
    }
    eaetai = String.valueOf(pEaetai);
  }
  
  public Character getEaetai() {
    return eaetai.charAt(0);
  }
  
  public void setEamex(String pEamex) {
    if (pEamex == null) {
      return;
    }
    eamex = pEamex;
  }
  
  public String getEamex() {
    return eamex;
  }
  
  public void setEactr(String pEactr) {
    if (pEactr == null) {
      return;
    }
    eactr = pEactr;
  }
  
  public String getEactr() {
    return eactr;
  }
  
  public void setEadili(Character pEadili) {
    if (pEadili == null) {
      return;
    }
    eadili = String.valueOf(pEadili);
  }
  
  public Character getEadili() {
    return eadili.charAt(0);
  }
  
  public void setEain6(Character pEain6) {
    if (pEain6 == null) {
      return;
    }
    eain6 = String.valueOf(pEain6);
  }
  
  public Character getEain6() {
    return eain6.charAt(0);
  }
  
  public void setEain7(Character pEain7) {
    if (pEain7 == null) {
      return;
    }
    eain7 = String.valueOf(pEain7);
  }
  
  public Character getEain7() {
    return eain7.charAt(0);
  }
  
  public void setEain8(Character pEain8) {
    if (pEain8 == null) {
      return;
    }
    eain8 = String.valueOf(pEain8);
  }
  
  public Character getEain8() {
    return eain8.charAt(0);
  }
  
  public void setEain9(Character pEain9) {
    if (pEain9 == null) {
      return;
    }
    eain9 = String.valueOf(pEain9);
  }
  
  public Character getEain9() {
    return eain9.charAt(0);
  }
  
  public void setEarfl(String pEarfl) {
    if (pEarfl == null) {
      return;
    }
    earfl = pEarfl;
  }
  
  public String getEarfl() {
    return earfl;
  }
  
  public void setEapor0(BigDecimal pEapor0) {
    if (pEapor0 == null) {
      return;
    }
    eapor0 = pEapor0.setScale(DECIMAL_EAPOR0, RoundingMode.HALF_UP);
  }
  
  public void setEapor0(Double pEapor0) {
    if (pEapor0 == null) {
      return;
    }
    eapor0 = BigDecimal.valueOf(pEapor0).setScale(DECIMAL_EAPOR0, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEapor0() {
    return eapor0.setScale(DECIMAL_EAPOR0, RoundingMode.HALF_UP);
  }
  
  public void setEapor1(BigDecimal pEapor1) {
    if (pEapor1 == null) {
      return;
    }
    eapor1 = pEapor1.setScale(DECIMAL_EAPOR1, RoundingMode.HALF_UP);
  }
  
  public void setEapor1(Double pEapor1) {
    if (pEapor1 == null) {
      return;
    }
    eapor1 = BigDecimal.valueOf(pEapor1).setScale(DECIMAL_EAPOR1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getEapor1() {
    return eapor1.setScale(DECIMAL_EAPOR1, RoundingMode.HALF_UP);
  }
  
  public void setEain10(Character pEain10) {
    if (pEain10 == null) {
      return;
    }
    eain10 = String.valueOf(pEain10);
  }
  
  public Character getEain10() {
    return eain10.charAt(0);
  }
  
  public void setEain11(Character pEain11) {
    if (pEain11 == null) {
      return;
    }
    eain11 = String.valueOf(pEain11);
  }
  
  public Character getEain11() {
    return eain11.charAt(0);
  }
  
  public void setEain12(Character pEain12) {
    if (pEain12 == null) {
      return;
    }
    eain12 = String.valueOf(pEain12);
  }
  
  public Character getEain12() {
    return eain12.charAt(0);
  }
  
  public void setEain13(Character pEain13) {
    if (pEain13 == null) {
      return;
    }
    eain13 = String.valueOf(pEain13);
  }
  
  public Character getEain13() {
    return eain13.charAt(0);
  }
  
  public void setEain14(Character pEain14) {
    if (pEain14 == null) {
      return;
    }
    eain14 = String.valueOf(pEain14);
  }
  
  public Character getEain14() {
    return eain14.charAt(0);
  }
  
  public void setEain15(Character pEain15) {
    if (pEain15 == null) {
      return;
    }
    eain15 = String.valueOf(pEain15);
  }
  
  public Character getEain15() {
    return eain15.charAt(0);
  }
  
  public void setPonom2(String pPonom2) {
    if (pPonom2 == null) {
      return;
    }
    ponom2 = pPonom2;
  }
  
  public String getPonom2() {
    return ponom2;
  }
  
  public void setPocpl2(String pPocpl2) {
    if (pPocpl2 == null) {
      return;
    }
    pocpl2 = pPocpl2;
  }
  
  public String getPocpl2() {
    return pocpl2;
  }
  
  public void setPorue2(String pPorue2) {
    if (pPorue2 == null) {
      return;
    }
    porue2 = pPorue2;
  }
  
  public String getPorue2() {
    return porue2;
  }
  
  public void setPoloc2(String pPoloc2) {
    if (pPoloc2 == null) {
      return;
    }
    poloc2 = pPoloc2;
  }
  
  public String getPoloc2() {
    return poloc2;
  }
  
  public void setPocdp2(String pPocdp2) {
    if (pPocdp2 == null) {
      return;
    }
    pocdp2 = pPocdp2;
  }
  
  public String getPocdp2() {
    return pocdp2;
  }
  
  public void setPovil2(String pPovil2) {
    if (pPovil2 == null) {
      return;
    }
    povil2 = pPovil2;
  }
  
  public String getPovil2() {
    return povil2;
  }
  
  public void setPoenv(Character pPoenv) {
    if (pPoenv == null) {
      return;
    }
    poenv = String.valueOf(pPoenv);
  }
  
  public Character getPoenv() {
    return poenv.charAt(0);
  }
  
  public void setPoinex(BigDecimal pPoinex) {
    if (pPoinex == null) {
      return;
    }
    poinex = pPoinex.setScale(DECIMAL_POINEX, RoundingMode.HALF_UP);
  }
  
  public void setPoinex(Integer pPoinex) {
    if (pPoinex == null) {
      return;
    }
    poinex = BigDecimal.valueOf(pPoinex);
  }
  
  public Integer getPoinex() {
    return poinex.intValue();
  }
  
  public void setPotyppx(Character pPotyppx) {
    if (pPotyppx == null) {
      return;
    }
    potyppx = String.valueOf(pPotyppx);
  }
  
  public Character getPotyppx() {
    return potyppx.charAt(0);
  }
  
  public void setPonom3(String pPonom3) {
    if (pPonom3 == null) {
      return;
    }
    ponom3 = pPonom3;
  }
  
  public String getPonom3() {
    return ponom3;
  }
  
  public void setPocpl3(String pPocpl3) {
    if (pPocpl3 == null) {
      return;
    }
    pocpl3 = pPocpl3;
  }
  
  public String getPocpl3() {
    return pocpl3;
  }
  
  public void setPorue3(String pPorue3) {
    if (pPorue3 == null) {
      return;
    }
    porue3 = pPorue3;
  }
  
  public String getPorue3() {
    return porue3;
  }
  
  public void setPoloc3(String pPoloc3) {
    if (pPoloc3 == null) {
      return;
    }
    poloc3 = pPoloc3;
  }
  
  public String getPoloc3() {
    return poloc3;
  }
  
  public void setPocdp3(String pPocdp3) {
    if (pPocdp3 == null) {
      return;
    }
    pocdp3 = pPocdp3;
  }
  
  public String getPocdp3() {
    return pocdp3;
  }
  
  public void setPofil3(Character pPofil3) {
    if (pPofil3 == null) {
      return;
    }
    pofil3 = String.valueOf(pPofil3);
  }
  
  public Character getPofil3() {
    return pofil3.charAt(0);
  }
  
  public void setPovil3(String pPovil3) {
    if (pPovil3 == null) {
      return;
    }
    povil3 = pPovil3;
  }
  
  public String getPovil3() {
    return povil3;
  }
  
  public void setWeafac(BigDecimal pWeafac) {
    if (pWeafac == null) {
      return;
    }
    weafac = pWeafac.setScale(DECIMAL_WEAFAC, RoundingMode.HALF_UP);
  }
  
  public void setWeafac(Integer pWeafac) {
    if (pWeafac == null) {
      return;
    }
    weafac = BigDecimal.valueOf(pWeafac);
  }
  
  public void setWeafac(Date pWeafac) {
    if (pWeafac == null) {
      return;
    }
    weafac = BigDecimal.valueOf(ConvertDate.dateToDb2(pWeafac));
  }
  
  public Integer getWeafac() {
    return weafac.intValue();
  }
  
  public Date getWeafacConvertiEnDate() {
    return ConvertDate.db2ToDate(weafac.intValue(), null);
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
