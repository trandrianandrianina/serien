/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.initialisation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0003o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_POWSER = 1;
  public static final int SIZE_LOTOP = 1;
  public static final int DECIMAL_LOTOP = 0;
  public static final int SIZE_LOCOD = 1;
  public static final int SIZE_LOETB = 3;
  public static final int SIZE_LONUM = 6;
  public static final int DECIMAL_LONUM = 0;
  public static final int SIZE_LOSUF = 1;
  public static final int DECIMAL_LOSUF = 0;
  public static final int SIZE_LONLI = 4;
  public static final int DECIMAL_LONLI = 0;
  public static final int SIZE_LOERL = 1;
  public static final int SIZE_LOCEX = 1;
  public static final int SIZE_LOQCX = 11;
  public static final int DECIMAL_LOQCX = 3;
  public static final int SIZE_LOTVA = 1;
  public static final int DECIMAL_LOTVA = 0;
  public static final int SIZE_LOTB = 1;
  public static final int DECIMAL_LOTB = 0;
  public static final int SIZE_LOTR = 1;
  public static final int DECIMAL_LOTR = 0;
  public static final int SIZE_LOTN = 1;
  public static final int DECIMAL_LOTN = 0;
  public static final int SIZE_LOTU = 1;
  public static final int DECIMAL_LOTU = 0;
  public static final int SIZE_LOTQ = 1;
  public static final int DECIMAL_LOTQ = 0;
  public static final int SIZE_LOTH = 1;
  public static final int DECIMAL_LOTH = 0;
  public static final int SIZE_LODCC = 1;
  public static final int DECIMAL_LODCC = 0;
  public static final int SIZE_LODCA = 1;
  public static final int DECIMAL_LODCA = 0;
  public static final int SIZE_LODCS = 1;
  public static final int DECIMAL_LODCS = 0;
  public static final int SIZE_LOVAL = 1;
  public static final int DECIMAL_LOVAL = 0;
  public static final int SIZE_LOCOL = 1;
  public static final int DECIMAL_LOCOL = 0;
  public static final int SIZE_LOSGN = 1;
  public static final int DECIMAL_LOSGN = 0;
  public static final int SIZE_LOQTC = 11;
  public static final int DECIMAL_LOQTC = 3;
  public static final int SIZE_LOUNC = 2;
  public static final int SIZE_LOQTS = 11;
  public static final int DECIMAL_LOQTS = 3;
  public static final int SIZE_LOKSC = 8;
  public static final int DECIMAL_LOKSC = 3;
  public static final int SIZE_LOPAB = 9;
  public static final int DECIMAL_LOPAB = 2;
  public static final int SIZE_LOPAN = 9;
  public static final int DECIMAL_LOPAN = 2;
  public static final int SIZE_LOPAC = 9;
  public static final int DECIMAL_LOPAC = 2;
  public static final int SIZE_LOMHT = 11;
  public static final int DECIMAL_LOMHT = 2;
  public static final int SIZE_LOAVR = 1;
  public static final int SIZE_LOQTA = 11;
  public static final int DECIMAL_LOQTA = 3;
  public static final int SIZE_LOUNA = 2;
  public static final int SIZE_LOKAC = 8;
  public static final int DECIMAL_LOKAC = 3;
  public static final int SIZE_LOART = 20;
  public static final int SIZE_LOMAG = 2;
  public static final int SIZE_LOMAGA = 2;
  public static final int SIZE_LONUMR = 6;
  public static final int DECIMAL_LONUMR = 0;
  public static final int SIZE_LOSUFR = 1;
  public static final int DECIMAL_LOSUFR = 0;
  public static final int SIZE_LONLIR = 4;
  public static final int DECIMAL_LONLIR = 0;
  public static final int SIZE_LODATH = 7;
  public static final int DECIMAL_LODATH = 0;
  public static final int SIZE_LOORDH = 3;
  public static final int DECIMAL_LOORDH = 0;
  public static final int SIZE_LOSAN = 4;
  public static final int SIZE_LOACT = 4;
  public static final int SIZE_LONAT = 1;
  public static final int SIZE_LOMTA = 9;
  public static final int DECIMAL_LOMTA = 2;
  public static final int SIZE_LODLP = 7;
  public static final int DECIMAL_LODLP = 0;
  public static final int SIZE_LOSER = 2;
  public static final int DECIMAL_LOSER = 0;
  public static final int SIZE_LOPAI = 9;
  public static final int DECIMAL_LOPAI = 2;
  public static final int SIZE_LOPAR = 9;
  public static final int DECIMAL_LOPAR = 2;
  public static final int SIZE_LOREM1 = 4;
  public static final int DECIMAL_LOREM1 = 2;
  public static final int SIZE_LOREM2 = 4;
  public static final int DECIMAL_LOREM2 = 2;
  public static final int SIZE_LOREM3 = 4;
  public static final int DECIMAL_LOREM3 = 2;
  public static final int SIZE_LOREM4 = 4;
  public static final int DECIMAL_LOREM4 = 2;
  public static final int SIZE_LOREM5 = 4;
  public static final int DECIMAL_LOREM5 = 2;
  public static final int SIZE_LOREM6 = 4;
  public static final int DECIMAL_LOREM6 = 2;
  public static final int SIZE_LOTRL = 1;
  public static final int SIZE_LOBRL = 1;
  public static final int SIZE_LORP1 = 1;
  public static final int SIZE_LORP2 = 1;
  public static final int SIZE_LORP3 = 1;
  public static final int SIZE_LORP4 = 1;
  public static final int SIZE_LORP5 = 1;
  public static final int SIZE_LORP6 = 1;
  public static final int SIZE_LOTP1 = 2;
  public static final int SIZE_LOTP2 = 2;
  public static final int SIZE_LOTP3 = 2;
  public static final int SIZE_LOTP4 = 2;
  public static final int SIZE_LOTP5 = 2;
  public static final int SIZE_LOIN1 = 1;
  public static final int SIZE_LOIN2 = 1;
  public static final int SIZE_LOIN3 = 1;
  public static final int SIZE_LOIN4 = 1;
  public static final int SIZE_LOIN5 = 1;
  public static final int SIZE_LODLC = 7;
  public static final int DECIMAL_LODLC = 0;
  public static final int SIZE_LOQTAI = 11;
  public static final int DECIMAL_LOQTAI = 3;
  public static final int SIZE_LOQTCI = 11;
  public static final int DECIMAL_LOQTCI = 3;
  public static final int SIZE_LOQTSI = 11;
  public static final int DECIMAL_LOQTSI = 3;
  public static final int SIZE_LOMHTI = 11;
  public static final int DECIMAL_LOMHTI = 2;
  public static final int SIZE_PONATAN = 5;
  public static final int SIZE_WTYPART = 3;
  public static final int SIZE_WKCSA = 8;
  public static final int DECIMAL_WKCSA = 3;
  public static final int SIZE_WPRBRU = 9;
  public static final int DECIMAL_WPRBRU = 2;
  public static final int SIZE_WPRNET = 9;
  public static final int DECIMAL_WPRNET = 2;
  public static final int SIZE_WPRVFR = 9;
  public static final int DECIMAL_WPRVFR = 2;
  public static final int SIZE_WPORTF = 9;
  public static final int DECIMAL_WPORTF = 2;
  public static final int SIZE_POXXX1 = 7;
  public static final int DECIMAL_POXXX1 = 4;
  public static final int SIZE_POLFK1 = 7;
  public static final int DECIMAL_POLFK1 = 4;
  public static final int SIZE_POLFV1 = 9;
  public static final int DECIMAL_POLFV1 = 2;
  public static final int SIZE_POLFP1 = 4;
  public static final int DECIMAL_POLFP1 = 2;
  public static final int SIZE_POLFK3 = 7;
  public static final int DECIMAL_POLFK3 = 4;
  public static final int SIZE_POLFV3 = 9;
  public static final int DECIMAL_POLFV3 = 2;
  public static final int SIZE_LOQTAT = 11;
  public static final int DECIMAL_LOQTAT = 3;
  public static final int SIZE_LOQTCT = 11;
  public static final int DECIMAL_LOQTCT = 3;
  public static final int SIZE_LOQTST = 11;
  public static final int DECIMAL_LOQTST = 3;
  public static final int SIZE_LOMHTT = 11;
  public static final int DECIMAL_LOMHTT = 2;
  public static final int SIZE_LOARR = 1;
  public static final int SIZE_TOTALE_DS = 473;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_POWSER = 1;
  public static final int VAR_LOTOP = 2;
  public static final int VAR_LOCOD = 3;
  public static final int VAR_LOETB = 4;
  public static final int VAR_LONUM = 5;
  public static final int VAR_LOSUF = 6;
  public static final int VAR_LONLI = 7;
  public static final int VAR_LOERL = 8;
  public static final int VAR_LOCEX = 9;
  public static final int VAR_LOQCX = 10;
  public static final int VAR_LOTVA = 11;
  public static final int VAR_LOTB = 12;
  public static final int VAR_LOTR = 13;
  public static final int VAR_LOTN = 14;
  public static final int VAR_LOTU = 15;
  public static final int VAR_LOTQ = 16;
  public static final int VAR_LOTH = 17;
  public static final int VAR_LODCC = 18;
  public static final int VAR_LODCA = 19;
  public static final int VAR_LODCS = 20;
  public static final int VAR_LOVAL = 21;
  public static final int VAR_LOCOL = 22;
  public static final int VAR_LOSGN = 23;
  public static final int VAR_LOQTC = 24;
  public static final int VAR_LOUNC = 25;
  public static final int VAR_LOQTS = 26;
  public static final int VAR_LOKSC = 27;
  public static final int VAR_LOPAB = 28;
  public static final int VAR_LOPAN = 29;
  public static final int VAR_LOPAC = 30;
  public static final int VAR_LOMHT = 31;
  public static final int VAR_LOAVR = 32;
  public static final int VAR_LOQTA = 33;
  public static final int VAR_LOUNA = 34;
  public static final int VAR_LOKAC = 35;
  public static final int VAR_LOART = 36;
  public static final int VAR_LOMAG = 37;
  public static final int VAR_LOMAGA = 38;
  public static final int VAR_LONUMR = 39;
  public static final int VAR_LOSUFR = 40;
  public static final int VAR_LONLIR = 41;
  public static final int VAR_LODATH = 42;
  public static final int VAR_LOORDH = 43;
  public static final int VAR_LOSAN = 44;
  public static final int VAR_LOACT = 45;
  public static final int VAR_LONAT = 46;
  public static final int VAR_LOMTA = 47;
  public static final int VAR_LODLP = 48;
  public static final int VAR_LOSER = 49;
  public static final int VAR_LOPAI = 50;
  public static final int VAR_LOPAR = 51;
  public static final int VAR_LOREM1 = 52;
  public static final int VAR_LOREM2 = 53;
  public static final int VAR_LOREM3 = 54;
  public static final int VAR_LOREM4 = 55;
  public static final int VAR_LOREM5 = 56;
  public static final int VAR_LOREM6 = 57;
  public static final int VAR_LOTRL = 58;
  public static final int VAR_LOBRL = 59;
  public static final int VAR_LORP1 = 60;
  public static final int VAR_LORP2 = 61;
  public static final int VAR_LORP3 = 62;
  public static final int VAR_LORP4 = 63;
  public static final int VAR_LORP5 = 64;
  public static final int VAR_LORP6 = 65;
  public static final int VAR_LOTP1 = 66;
  public static final int VAR_LOTP2 = 67;
  public static final int VAR_LOTP3 = 68;
  public static final int VAR_LOTP4 = 69;
  public static final int VAR_LOTP5 = 70;
  public static final int VAR_LOIN1 = 71;
  public static final int VAR_LOIN2 = 72;
  public static final int VAR_LOIN3 = 73;
  public static final int VAR_LOIN4 = 74;
  public static final int VAR_LOIN5 = 75;
  public static final int VAR_LODLC = 76;
  public static final int VAR_LOQTAI = 77;
  public static final int VAR_LOQTCI = 78;
  public static final int VAR_LOQTSI = 79;
  public static final int VAR_LOMHTI = 80;
  public static final int VAR_PONATAN = 81;
  public static final int VAR_WTYPART = 82;
  public static final int VAR_WKCSA = 83;
  public static final int VAR_WPRBRU = 84;
  public static final int VAR_WPRNET = 85;
  public static final int VAR_WPRVFR = 86;
  public static final int VAR_WPORTF = 87;
  public static final int VAR_POXXX1 = 88;
  public static final int VAR_POLFK1 = 89;
  public static final int VAR_POLFV1 = 90;
  public static final int VAR_POLFP1 = 91;
  public static final int VAR_POLFK3 = 92;
  public static final int VAR_POLFV3 = 93;
  public static final int VAR_LOQTAT = 94;
  public static final int VAR_LOQTCT = 95;
  public static final int VAR_LOQTST = 96;
  public static final int VAR_LOMHTT = 97;
  public static final int VAR_LOARR = 98;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private String powser = ""; // Saisie Lot ou série
  private BigDecimal lotop = BigDecimal.ZERO; // Code Etat de la Ligne
  private String locod = ""; // Code ERL "E" OU "F"
  private String loetb = ""; // Code Etablissement
  private BigDecimal lonum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal losuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal lonli = BigDecimal.ZERO; // Numéro de Ligne
  private String loerl = ""; // Code ERL "C"
  private String locex = ""; // Code Extraction
  private BigDecimal loqcx = BigDecimal.ZERO; // Quantité extraite en UV
  private BigDecimal lotva = BigDecimal.ZERO; // Code TVA
  private BigDecimal lotb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal lotr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal lotn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal lotu = BigDecimal.ZERO; // Top unité saisie
  private BigDecimal lotq = BigDecimal.ZERO; // Top Qté saisie
  private BigDecimal loth = BigDecimal.ZERO; // Top montant H.T saisi
  private BigDecimal lodcc = BigDecimal.ZERO; // Décimalisation QTC
  private BigDecimal lodca = BigDecimal.ZERO; // " " QTA
  private BigDecimal lodcs = BigDecimal.ZERO; // " " QTS
  private BigDecimal loval = BigDecimal.ZERO; // Top Ligne en Valeur
  private BigDecimal locol = BigDecimal.ZERO; // Colonne TVA
  private BigDecimal losgn = BigDecimal.ZERO; // Signe
  private BigDecimal loqtc = BigDecimal.ZERO; // Quantité en unités de cde
  private String lounc = ""; // Unité de commande
  private BigDecimal loqts = BigDecimal.ZERO; // Quantité en unités de stock
  private BigDecimal loksc = BigDecimal.ZERO; // Coeff. Stock/Cde
  private BigDecimal lopab = BigDecimal.ZERO; // Prix d"achat de base
  private BigDecimal lopan = BigDecimal.ZERO; // Prix d"achat net
  private BigDecimal lopac = BigDecimal.ZERO; // Prix d"achat calculé
  private BigDecimal lomht = BigDecimal.ZERO; // Montant hors taxes
  private String loavr = ""; // Code Avoir
  private BigDecimal loqta = BigDecimal.ZERO; // Quantité en unités d"achat
  private String louna = ""; // Unité d"achat
  private BigDecimal lokac = BigDecimal.ZERO; // Coeff. Achat/Cde
  private String loart = ""; // Code Article
  private String lomag = ""; // Magasin
  private String lomaga = ""; // Magasin avant modif
  private BigDecimal lonumr = BigDecimal.ZERO; // N°Bon ou Fac.
  private BigDecimal losufr = BigDecimal.ZERO; // Suffixe
  private BigDecimal lonlir = BigDecimal.ZERO; // N°ligne
  private BigDecimal lodath = BigDecimal.ZERO; // Date sur Histo stocks
  private BigDecimal loordh = BigDecimal.ZERO; // Ordre sur Histo stocks
  private String losan = ""; // Section Analytique
  private String loact = ""; // Activité ou Affaire
  private String lonat = ""; // Nature d"achat (M,F,I)
  private BigDecimal lomta = BigDecimal.ZERO; // Montant affecté
  private BigDecimal lodlp = BigDecimal.ZERO; // Date livraison prévue
  private BigDecimal loser = BigDecimal.ZERO; // Top n° de série ou lot
  private BigDecimal lopai = BigDecimal.ZERO; // Prix d'achat initial
  private BigDecimal lopar = BigDecimal.ZERO; // Prix d'achat rem. déduites
  private BigDecimal lorem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal lorem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal lorem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal lorem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal lorem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal lorem6 = BigDecimal.ZERO; // % Remise 6
  private String lotrl = ""; // Type remise ligne, 1=cascade
  private String lobrl = ""; // Base remise ligne, 1=Montant
  private String lorp1 = ""; // Exclusion remise de pied N°1
  private String lorp2 = ""; // Exclusion remise de pied N°2
  private String lorp3 = ""; // Exclusion remise de pied N°3
  private String lorp4 = ""; // Exclusion remise de pied N°4
  private String lorp5 = ""; // Exclusion remise de pied N°5
  private String lorp6 = ""; // Exclusion remise de pied N°6
  private String lotp1 = ""; // Top personnal.N°1
  private String lotp2 = ""; // Top personnal.N°2
  private String lotp3 = ""; // Top personnal.N°3
  private String lotp4 = ""; // Top personnal.N°4
  private String lotp5 = ""; // Top personnal.N°5
  private String loin1 = ""; // Imputation frais
  private String loin2 = ""; // type de frais
  private String loin3 = ""; // regroupement article
  private String loin4 = ""; // compt. stock flottant
  private String loin5 = ""; // Condit° en nb de gratuits
  private BigDecimal lodlc = BigDecimal.ZERO; // Date livraison calculée
  private BigDecimal loqtai = BigDecimal.ZERO; // Qte initiale unités d"achat
  private BigDecimal loqtci = BigDecimal.ZERO; // Qte initiale unités de cde
  private BigDecimal loqtsi = BigDecimal.ZERO; // Qte initiale unités de stock
  private BigDecimal lomhti = BigDecimal.ZERO; // Montant initial hors taxe
  private String ponatan = ""; // Nature analytique
  private String wtypart = ""; // Type ligne
  private BigDecimal wkcsa = BigDecimal.ZERO; // Coeff. SurStock/Achat
  private BigDecimal wprbru = BigDecimal.ZERO; // Prix d achat brut
  private BigDecimal wprnet = BigDecimal.ZERO; // Prix d achat net sans port
  private BigDecimal wprvfr = BigDecimal.ZERO; // Prix revient fournisseur
  private BigDecimal wportf = BigDecimal.ZERO; // Port fournissseur eur
  private BigDecimal poxxx1 = BigDecimal.ZERO; // Plus utilisé (anc.PoLFK1)
  private BigDecimal polfk1 = BigDecimal.ZERO; // Frais 1 coef.(anc.PoLFK1p)
  private BigDecimal polfv1 = BigDecimal.ZERO; // Frais 1 valeur
  private BigDecimal polfp1 = BigDecimal.ZERO; // Frais 1 % PORT
  private BigDecimal polfk3 = BigDecimal.ZERO; // Frais 3 coefficient
  private BigDecimal polfv3 = BigDecimal.ZERO; // Frais 3 valeur
  private BigDecimal loqtat = BigDecimal.ZERO; // Qte Traitée unités d"achat
  private BigDecimal loqtct = BigDecimal.ZERO; // Qte Traitée unités de cde
  private BigDecimal loqtst = BigDecimal.ZERO; // Qte Traitée unités de stock
  private BigDecimal lomhtt = BigDecimal.ZERO; // Montant Traité hors taxe
  private String loarr = ""; // Valeur = X
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Text(SIZE_POWSER), // Saisie Lot ou série
      new AS400ZonedDecimal(SIZE_LOTOP, DECIMAL_LOTOP), // Code Etat de la Ligne
      new AS400Text(SIZE_LOCOD), // Code ERL "E" OU "F"
      new AS400Text(SIZE_LOETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_LONUM, DECIMAL_LONUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_LOSUF, DECIMAL_LOSUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_LONLI, DECIMAL_LONLI), // Numéro de Ligne
      new AS400Text(SIZE_LOERL), // Code ERL "C"
      new AS400Text(SIZE_LOCEX), // Code Extraction
      new AS400PackedDecimal(SIZE_LOQCX, DECIMAL_LOQCX), // Quantité extraite en UV
      new AS400ZonedDecimal(SIZE_LOTVA, DECIMAL_LOTVA), // Code TVA
      new AS400ZonedDecimal(SIZE_LOTB, DECIMAL_LOTB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_LOTR, DECIMAL_LOTR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_LOTN, DECIMAL_LOTN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_LOTU, DECIMAL_LOTU), // Top unité saisie
      new AS400ZonedDecimal(SIZE_LOTQ, DECIMAL_LOTQ), // Top Qté saisie
      new AS400ZonedDecimal(SIZE_LOTH, DECIMAL_LOTH), // Top montant H.T saisi
      new AS400ZonedDecimal(SIZE_LODCC, DECIMAL_LODCC), // Décimalisation QTC
      new AS400ZonedDecimal(SIZE_LODCA, DECIMAL_LODCA), // " " QTA
      new AS400ZonedDecimal(SIZE_LODCS, DECIMAL_LODCS), // " " QTS
      new AS400ZonedDecimal(SIZE_LOVAL, DECIMAL_LOVAL), // Top Ligne en Valeur
      new AS400ZonedDecimal(SIZE_LOCOL, DECIMAL_LOCOL), // Colonne TVA
      new AS400ZonedDecimal(SIZE_LOSGN, DECIMAL_LOSGN), // Signe
      new AS400PackedDecimal(SIZE_LOQTC, DECIMAL_LOQTC), // Quantité en unités de cde
      new AS400Text(SIZE_LOUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_LOQTS, DECIMAL_LOQTS), // Quantité en unités de stock
      new AS400PackedDecimal(SIZE_LOKSC, DECIMAL_LOKSC), // Coeff. Stock/Cde
      new AS400PackedDecimal(SIZE_LOPAB, DECIMAL_LOPAB), // Prix d"achat de base
      new AS400PackedDecimal(SIZE_LOPAN, DECIMAL_LOPAN), // Prix d"achat net
      new AS400PackedDecimal(SIZE_LOPAC, DECIMAL_LOPAC), // Prix d"achat calculé
      new AS400PackedDecimal(SIZE_LOMHT, DECIMAL_LOMHT), // Montant hors taxes
      new AS400Text(SIZE_LOAVR), // Code Avoir
      new AS400PackedDecimal(SIZE_LOQTA, DECIMAL_LOQTA), // Quantité en unités d"achat
      new AS400Text(SIZE_LOUNA), // Unité d"achat
      new AS400PackedDecimal(SIZE_LOKAC, DECIMAL_LOKAC), // Coeff. Achat/Cde
      new AS400Text(SIZE_LOART), // Code Article
      new AS400Text(SIZE_LOMAG), // Magasin
      new AS400Text(SIZE_LOMAGA), // Magasin avant modif
      new AS400ZonedDecimal(SIZE_LONUMR, DECIMAL_LONUMR), // N°Bon ou Fac.
      new AS400ZonedDecimal(SIZE_LOSUFR, DECIMAL_LOSUFR), // Suffixe
      new AS400ZonedDecimal(SIZE_LONLIR, DECIMAL_LONLIR), // N°ligne
      new AS400ZonedDecimal(SIZE_LODATH, DECIMAL_LODATH), // Date sur Histo stocks
      new AS400ZonedDecimal(SIZE_LOORDH, DECIMAL_LOORDH), // Ordre sur Histo stocks
      new AS400Text(SIZE_LOSAN), // Section Analytique
      new AS400Text(SIZE_LOACT), // Activité ou Affaire
      new AS400Text(SIZE_LONAT), // Nature d"achat (M,F,I)
      new AS400PackedDecimal(SIZE_LOMTA, DECIMAL_LOMTA), // Montant affecté
      new AS400PackedDecimal(SIZE_LODLP, DECIMAL_LODLP), // Date livraison prévue
      new AS400ZonedDecimal(SIZE_LOSER, DECIMAL_LOSER), // Top n° de série ou lot
      new AS400PackedDecimal(SIZE_LOPAI, DECIMAL_LOPAI), // Prix d'achat initial
      new AS400PackedDecimal(SIZE_LOPAR, DECIMAL_LOPAR), // Prix d'achat rem. déduites
      new AS400ZonedDecimal(SIZE_LOREM1, DECIMAL_LOREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_LOREM2, DECIMAL_LOREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_LOREM3, DECIMAL_LOREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_LOREM4, DECIMAL_LOREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_LOREM5, DECIMAL_LOREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_LOREM6, DECIMAL_LOREM6), // % Remise 6
      new AS400Text(SIZE_LOTRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_LOBRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_LORP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_LORP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_LORP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_LORP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_LORP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_LORP6), // Exclusion remise de pied N°6
      new AS400Text(SIZE_LOTP1), // Top personnal.N°1
      new AS400Text(SIZE_LOTP2), // Top personnal.N°2
      new AS400Text(SIZE_LOTP3), // Top personnal.N°3
      new AS400Text(SIZE_LOTP4), // Top personnal.N°4
      new AS400Text(SIZE_LOTP5), // Top personnal.N°5
      new AS400Text(SIZE_LOIN1), // Imputation frais
      new AS400Text(SIZE_LOIN2), // type de frais
      new AS400Text(SIZE_LOIN3), // regroupement article
      new AS400Text(SIZE_LOIN4), // compt. stock flottant
      new AS400Text(SIZE_LOIN5), // Condit° en nb de gratuits
      new AS400PackedDecimal(SIZE_LODLC, DECIMAL_LODLC), // Date livraison calculée
      new AS400PackedDecimal(SIZE_LOQTAI, DECIMAL_LOQTAI), // Qte initiale unités d"achat
      new AS400PackedDecimal(SIZE_LOQTCI, DECIMAL_LOQTCI), // Qte initiale unités de cde
      new AS400PackedDecimal(SIZE_LOQTSI, DECIMAL_LOQTSI), // Qte initiale unités de stock
      new AS400PackedDecimal(SIZE_LOMHTI, DECIMAL_LOMHTI), // Montant initial hors taxe
      new AS400Text(SIZE_PONATAN), // Nature analytique
      new AS400Text(SIZE_WTYPART), // Type ligne
      new AS400ZonedDecimal(SIZE_WKCSA, DECIMAL_WKCSA), // Coeff. SurStock/Achat
      new AS400ZonedDecimal(SIZE_WPRBRU, DECIMAL_WPRBRU), // Prix d achat brut
      new AS400ZonedDecimal(SIZE_WPRNET, DECIMAL_WPRNET), // Prix d achat net sans port
      new AS400ZonedDecimal(SIZE_WPRVFR, DECIMAL_WPRVFR), // Prix revient fournisseur
      new AS400ZonedDecimal(SIZE_WPORTF, DECIMAL_WPORTF), // Port fournissseur eur
      new AS400ZonedDecimal(SIZE_POXXX1, DECIMAL_POXXX1), // Plus utilisé (anc.PoLFK1)
      new AS400ZonedDecimal(SIZE_POLFK1, DECIMAL_POLFK1), // Frais 1 coef.(anc.PoLFK1p)
      new AS400ZonedDecimal(SIZE_POLFV1, DECIMAL_POLFV1), // Frais 1 valeur
      new AS400ZonedDecimal(SIZE_POLFP1, DECIMAL_POLFP1), // Frais 1 % PORT
      new AS400ZonedDecimal(SIZE_POLFK3, DECIMAL_POLFK3), // Frais 3 coefficient
      new AS400ZonedDecimal(SIZE_POLFV3, DECIMAL_POLFV3), // Frais 3 valeur
      new AS400PackedDecimal(SIZE_LOQTAT, DECIMAL_LOQTAT), // Qte Traitée unités d"achat
      new AS400PackedDecimal(SIZE_LOQTCT, DECIMAL_LOQTCT), // Qte Traitée unités de cde
      new AS400PackedDecimal(SIZE_LOQTST, DECIMAL_LOQTST), // Qte Traitée unités de stock
      new AS400PackedDecimal(SIZE_LOMHTT, DECIMAL_LOMHTT), // Montant Traité hors taxe
      new AS400Text(SIZE_LOARR), // Valeur = X
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, powser, lotop, locod, loetb, lonum, losuf, lonli, loerl, locex, loqcx, lotva, lotb, lotr, lotn, lotu, lotq,
          loth, lodcc, lodca, lodcs, loval, locol, losgn, loqtc, lounc, loqts, loksc, lopab, lopan, lopac, lomht, loavr, loqta, louna,
          lokac, loart, lomag, lomaga, lonumr, losufr, lonlir, lodath, loordh, losan, loact, lonat, lomta, lodlp, loser, lopai, lopar,
          lorem1, lorem2, lorem3, lorem4, lorem5, lorem6, lotrl, lobrl, lorp1, lorp2, lorp3, lorp4, lorp5, lorp6, lotp1, lotp2, lotp3,
          lotp4, lotp5, loin1, loin2, loin3, loin4, loin5, lodlc, loqtai, loqtci, loqtsi, lomhti, ponatan, wtypart, wkcsa, wprbru, wprnet,
          wprvfr, wportf, poxxx1, polfk1, polfv1, polfp1, polfk3, polfv3, loqtat, loqtct, loqtst, lomhtt, loarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    powser = (String) output[1];
    lotop = (BigDecimal) output[2];
    locod = (String) output[3];
    loetb = (String) output[4];
    lonum = (BigDecimal) output[5];
    losuf = (BigDecimal) output[6];
    lonli = (BigDecimal) output[7];
    loerl = (String) output[8];
    locex = (String) output[9];
    loqcx = (BigDecimal) output[10];
    lotva = (BigDecimal) output[11];
    lotb = (BigDecimal) output[12];
    lotr = (BigDecimal) output[13];
    lotn = (BigDecimal) output[14];
    lotu = (BigDecimal) output[15];
    lotq = (BigDecimal) output[16];
    loth = (BigDecimal) output[17];
    lodcc = (BigDecimal) output[18];
    lodca = (BigDecimal) output[19];
    lodcs = (BigDecimal) output[20];
    loval = (BigDecimal) output[21];
    locol = (BigDecimal) output[22];
    losgn = (BigDecimal) output[23];
    loqtc = (BigDecimal) output[24];
    lounc = (String) output[25];
    loqts = (BigDecimal) output[26];
    loksc = (BigDecimal) output[27];
    lopab = (BigDecimal) output[28];
    lopan = (BigDecimal) output[29];
    lopac = (BigDecimal) output[30];
    lomht = (BigDecimal) output[31];
    loavr = (String) output[32];
    loqta = (BigDecimal) output[33];
    louna = (String) output[34];
    lokac = (BigDecimal) output[35];
    loart = (String) output[36];
    lomag = (String) output[37];
    lomaga = (String) output[38];
    lonumr = (BigDecimal) output[39];
    losufr = (BigDecimal) output[40];
    lonlir = (BigDecimal) output[41];
    lodath = (BigDecimal) output[42];
    loordh = (BigDecimal) output[43];
    losan = (String) output[44];
    loact = (String) output[45];
    lonat = (String) output[46];
    lomta = (BigDecimal) output[47];
    lodlp = (BigDecimal) output[48];
    loser = (BigDecimal) output[49];
    lopai = (BigDecimal) output[50];
    lopar = (BigDecimal) output[51];
    lorem1 = (BigDecimal) output[52];
    lorem2 = (BigDecimal) output[53];
    lorem3 = (BigDecimal) output[54];
    lorem4 = (BigDecimal) output[55];
    lorem5 = (BigDecimal) output[56];
    lorem6 = (BigDecimal) output[57];
    lotrl = (String) output[58];
    lobrl = (String) output[59];
    lorp1 = (String) output[60];
    lorp2 = (String) output[61];
    lorp3 = (String) output[62];
    lorp4 = (String) output[63];
    lorp5 = (String) output[64];
    lorp6 = (String) output[65];
    lotp1 = (String) output[66];
    lotp2 = (String) output[67];
    lotp3 = (String) output[68];
    lotp4 = (String) output[69];
    lotp5 = (String) output[70];
    loin1 = (String) output[71];
    loin2 = (String) output[72];
    loin3 = (String) output[73];
    loin4 = (String) output[74];
    loin5 = (String) output[75];
    lodlc = (BigDecimal) output[76];
    loqtai = (BigDecimal) output[77];
    loqtci = (BigDecimal) output[78];
    loqtsi = (BigDecimal) output[79];
    lomhti = (BigDecimal) output[80];
    ponatan = (String) output[81];
    wtypart = (String) output[82];
    wkcsa = (BigDecimal) output[83];
    wprbru = (BigDecimal) output[84];
    wprnet = (BigDecimal) output[85];
    wprvfr = (BigDecimal) output[86];
    wportf = (BigDecimal) output[87];
    poxxx1 = (BigDecimal) output[88];
    polfk1 = (BigDecimal) output[89];
    polfv1 = (BigDecimal) output[90];
    polfp1 = (BigDecimal) output[91];
    polfk3 = (BigDecimal) output[92];
    polfv3 = (BigDecimal) output[93];
    loqtat = (BigDecimal) output[94];
    loqtct = (BigDecimal) output[95];
    loqtst = (BigDecimal) output[96];
    lomhtt = (BigDecimal) output[97];
    loarr = (String) output[98];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPowser(Character pPowser) {
    if (pPowser == null) {
      return;
    }
    powser = String.valueOf(pPowser);
  }
  
  public Character getPowser() {
    return powser.charAt(0);
  }
  
  public void setLotop(BigDecimal pLotop) {
    if (pLotop == null) {
      return;
    }
    lotop = pLotop.setScale(DECIMAL_LOTOP, RoundingMode.HALF_UP);
  }
  
  public void setLotop(Integer pLotop) {
    if (pLotop == null) {
      return;
    }
    lotop = BigDecimal.valueOf(pLotop);
  }
  
  public Integer getLotop() {
    return lotop.intValue();
  }
  
  public void setLocod(Character pLocod) {
    if (pLocod == null) {
      return;
    }
    locod = String.valueOf(pLocod);
  }
  
  public Character getLocod() {
    return locod.charAt(0);
  }
  
  public void setLoetb(String pLoetb) {
    if (pLoetb == null) {
      return;
    }
    loetb = pLoetb;
  }
  
  public String getLoetb() {
    return loetb;
  }
  
  public void setLonum(BigDecimal pLonum) {
    if (pLonum == null) {
      return;
    }
    lonum = pLonum.setScale(DECIMAL_LONUM, RoundingMode.HALF_UP);
  }
  
  public void setLonum(Integer pLonum) {
    if (pLonum == null) {
      return;
    }
    lonum = BigDecimal.valueOf(pLonum);
  }
  
  public Integer getLonum() {
    return lonum.intValue();
  }
  
  public void setLosuf(BigDecimal pLosuf) {
    if (pLosuf == null) {
      return;
    }
    losuf = pLosuf.setScale(DECIMAL_LOSUF, RoundingMode.HALF_UP);
  }
  
  public void setLosuf(Integer pLosuf) {
    if (pLosuf == null) {
      return;
    }
    losuf = BigDecimal.valueOf(pLosuf);
  }
  
  public Integer getLosuf() {
    return losuf.intValue();
  }
  
  public void setLonli(BigDecimal pLonli) {
    if (pLonli == null) {
      return;
    }
    lonli = pLonli.setScale(DECIMAL_LONLI, RoundingMode.HALF_UP);
  }
  
  public void setLonli(Integer pLonli) {
    if (pLonli == null) {
      return;
    }
    lonli = BigDecimal.valueOf(pLonli);
  }
  
  public Integer getLonli() {
    return lonli.intValue();
  }
  
  public void setLoerl(Character pLoerl) {
    if (pLoerl == null) {
      return;
    }
    loerl = String.valueOf(pLoerl);
  }
  
  public Character getLoerl() {
    return loerl.charAt(0);
  }
  
  public void setLocex(Character pLocex) {
    if (pLocex == null) {
      return;
    }
    locex = String.valueOf(pLocex);
  }
  
  public Character getLocex() {
    return locex.charAt(0);
  }
  
  public void setLoqcx(BigDecimal pLoqcx) {
    if (pLoqcx == null) {
      return;
    }
    loqcx = pLoqcx.setScale(DECIMAL_LOQCX, RoundingMode.HALF_UP);
  }
  
  public void setLoqcx(Double pLoqcx) {
    if (pLoqcx == null) {
      return;
    }
    loqcx = BigDecimal.valueOf(pLoqcx).setScale(DECIMAL_LOQCX, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqcx() {
    return loqcx.setScale(DECIMAL_LOQCX, RoundingMode.HALF_UP);
  }
  
  public void setLotva(BigDecimal pLotva) {
    if (pLotva == null) {
      return;
    }
    lotva = pLotva.setScale(DECIMAL_LOTVA, RoundingMode.HALF_UP);
  }
  
  public void setLotva(Integer pLotva) {
    if (pLotva == null) {
      return;
    }
    lotva = BigDecimal.valueOf(pLotva);
  }
  
  public Integer getLotva() {
    return lotva.intValue();
  }
  
  public void setLotb(BigDecimal pLotb) {
    if (pLotb == null) {
      return;
    }
    lotb = pLotb.setScale(DECIMAL_LOTB, RoundingMode.HALF_UP);
  }
  
  public void setLotb(Integer pLotb) {
    if (pLotb == null) {
      return;
    }
    lotb = BigDecimal.valueOf(pLotb);
  }
  
  public Integer getLotb() {
    return lotb.intValue();
  }
  
  public void setLotr(BigDecimal pLotr) {
    if (pLotr == null) {
      return;
    }
    lotr = pLotr.setScale(DECIMAL_LOTR, RoundingMode.HALF_UP);
  }
  
  public void setLotr(Integer pLotr) {
    if (pLotr == null) {
      return;
    }
    lotr = BigDecimal.valueOf(pLotr);
  }
  
  public Integer getLotr() {
    return lotr.intValue();
  }
  
  public void setLotn(BigDecimal pLotn) {
    if (pLotn == null) {
      return;
    }
    lotn = pLotn.setScale(DECIMAL_LOTN, RoundingMode.HALF_UP);
  }
  
  public void setLotn(Integer pLotn) {
    if (pLotn == null) {
      return;
    }
    lotn = BigDecimal.valueOf(pLotn);
  }
  
  public Integer getLotn() {
    return lotn.intValue();
  }
  
  public void setLotu(BigDecimal pLotu) {
    if (pLotu == null) {
      return;
    }
    lotu = pLotu.setScale(DECIMAL_LOTU, RoundingMode.HALF_UP);
  }
  
  public void setLotu(Integer pLotu) {
    if (pLotu == null) {
      return;
    }
    lotu = BigDecimal.valueOf(pLotu);
  }
  
  public Integer getLotu() {
    return lotu.intValue();
  }
  
  public void setLotq(BigDecimal pLotq) {
    if (pLotq == null) {
      return;
    }
    lotq = pLotq.setScale(DECIMAL_LOTQ, RoundingMode.HALF_UP);
  }
  
  public void setLotq(Integer pLotq) {
    if (pLotq == null) {
      return;
    }
    lotq = BigDecimal.valueOf(pLotq);
  }
  
  public Integer getLotq() {
    return lotq.intValue();
  }
  
  public void setLoth(BigDecimal pLoth) {
    if (pLoth == null) {
      return;
    }
    loth = pLoth.setScale(DECIMAL_LOTH, RoundingMode.HALF_UP);
  }
  
  public void setLoth(Integer pLoth) {
    if (pLoth == null) {
      return;
    }
    loth = BigDecimal.valueOf(pLoth);
  }
  
  public Integer getLoth() {
    return loth.intValue();
  }
  
  public void setLodcc(BigDecimal pLodcc) {
    if (pLodcc == null) {
      return;
    }
    lodcc = pLodcc.setScale(DECIMAL_LODCC, RoundingMode.HALF_UP);
  }
  
  public void setLodcc(Integer pLodcc) {
    if (pLodcc == null) {
      return;
    }
    lodcc = BigDecimal.valueOf(pLodcc);
  }
  
  public Integer getLodcc() {
    return lodcc.intValue();
  }
  
  public void setLodca(BigDecimal pLodca) {
    if (pLodca == null) {
      return;
    }
    lodca = pLodca.setScale(DECIMAL_LODCA, RoundingMode.HALF_UP);
  }
  
  public void setLodca(Integer pLodca) {
    if (pLodca == null) {
      return;
    }
    lodca = BigDecimal.valueOf(pLodca);
  }
  
  public Integer getLodca() {
    return lodca.intValue();
  }
  
  public void setLodcs(BigDecimal pLodcs) {
    if (pLodcs == null) {
      return;
    }
    lodcs = pLodcs.setScale(DECIMAL_LODCS, RoundingMode.HALF_UP);
  }
  
  public void setLodcs(Integer pLodcs) {
    if (pLodcs == null) {
      return;
    }
    lodcs = BigDecimal.valueOf(pLodcs);
  }
  
  public Integer getLodcs() {
    return lodcs.intValue();
  }
  
  public void setLoval(BigDecimal pLoval) {
    if (pLoval == null) {
      return;
    }
    loval = pLoval.setScale(DECIMAL_LOVAL, RoundingMode.HALF_UP);
  }
  
  public void setLoval(Integer pLoval) {
    if (pLoval == null) {
      return;
    }
    loval = BigDecimal.valueOf(pLoval);
  }
  
  public Integer getLoval() {
    return loval.intValue();
  }
  
  public void setLocol(BigDecimal pLocol) {
    if (pLocol == null) {
      return;
    }
    locol = pLocol.setScale(DECIMAL_LOCOL, RoundingMode.HALF_UP);
  }
  
  public void setLocol(Integer pLocol) {
    if (pLocol == null) {
      return;
    }
    locol = BigDecimal.valueOf(pLocol);
  }
  
  public Integer getLocol() {
    return locol.intValue();
  }
  
  public void setLosgn(BigDecimal pLosgn) {
    if (pLosgn == null) {
      return;
    }
    losgn = pLosgn.setScale(DECIMAL_LOSGN, RoundingMode.HALF_UP);
  }
  
  public void setLosgn(Integer pLosgn) {
    if (pLosgn == null) {
      return;
    }
    losgn = BigDecimal.valueOf(pLosgn);
  }
  
  public Integer getLosgn() {
    return losgn.intValue();
  }
  
  public void setLoqtc(BigDecimal pLoqtc) {
    if (pLoqtc == null) {
      return;
    }
    loqtc = pLoqtc.setScale(DECIMAL_LOQTC, RoundingMode.HALF_UP);
  }
  
  public void setLoqtc(Double pLoqtc) {
    if (pLoqtc == null) {
      return;
    }
    loqtc = BigDecimal.valueOf(pLoqtc).setScale(DECIMAL_LOQTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtc() {
    return loqtc.setScale(DECIMAL_LOQTC, RoundingMode.HALF_UP);
  }
  
  public void setLounc(String pLounc) {
    if (pLounc == null) {
      return;
    }
    lounc = pLounc;
  }
  
  public String getLounc() {
    return lounc;
  }
  
  public void setLoqts(BigDecimal pLoqts) {
    if (pLoqts == null) {
      return;
    }
    loqts = pLoqts.setScale(DECIMAL_LOQTS, RoundingMode.HALF_UP);
  }
  
  public void setLoqts(Double pLoqts) {
    if (pLoqts == null) {
      return;
    }
    loqts = BigDecimal.valueOf(pLoqts).setScale(DECIMAL_LOQTS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqts() {
    return loqts.setScale(DECIMAL_LOQTS, RoundingMode.HALF_UP);
  }
  
  public void setLoksc(BigDecimal pLoksc) {
    if (pLoksc == null) {
      return;
    }
    loksc = pLoksc.setScale(DECIMAL_LOKSC, RoundingMode.HALF_UP);
  }
  
  public void setLoksc(Double pLoksc) {
    if (pLoksc == null) {
      return;
    }
    loksc = BigDecimal.valueOf(pLoksc).setScale(DECIMAL_LOKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoksc() {
    return loksc.setScale(DECIMAL_LOKSC, RoundingMode.HALF_UP);
  }
  
  public void setLopab(BigDecimal pLopab) {
    if (pLopab == null) {
      return;
    }
    lopab = pLopab.setScale(DECIMAL_LOPAB, RoundingMode.HALF_UP);
  }
  
  public void setLopab(Double pLopab) {
    if (pLopab == null) {
      return;
    }
    lopab = BigDecimal.valueOf(pLopab).setScale(DECIMAL_LOPAB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopab() {
    return lopab.setScale(DECIMAL_LOPAB, RoundingMode.HALF_UP);
  }
  
  public void setLopan(BigDecimal pLopan) {
    if (pLopan == null) {
      return;
    }
    lopan = pLopan.setScale(DECIMAL_LOPAN, RoundingMode.HALF_UP);
  }
  
  public void setLopan(Double pLopan) {
    if (pLopan == null) {
      return;
    }
    lopan = BigDecimal.valueOf(pLopan).setScale(DECIMAL_LOPAN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopan() {
    return lopan.setScale(DECIMAL_LOPAN, RoundingMode.HALF_UP);
  }
  
  public void setLopac(BigDecimal pLopac) {
    if (pLopac == null) {
      return;
    }
    lopac = pLopac.setScale(DECIMAL_LOPAC, RoundingMode.HALF_UP);
  }
  
  public void setLopac(Double pLopac) {
    if (pLopac == null) {
      return;
    }
    lopac = BigDecimal.valueOf(pLopac).setScale(DECIMAL_LOPAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopac() {
    return lopac.setScale(DECIMAL_LOPAC, RoundingMode.HALF_UP);
  }
  
  public void setLomht(BigDecimal pLomht) {
    if (pLomht == null) {
      return;
    }
    lomht = pLomht.setScale(DECIMAL_LOMHT, RoundingMode.HALF_UP);
  }
  
  public void setLomht(Double pLomht) {
    if (pLomht == null) {
      return;
    }
    lomht = BigDecimal.valueOf(pLomht).setScale(DECIMAL_LOMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomht() {
    return lomht.setScale(DECIMAL_LOMHT, RoundingMode.HALF_UP);
  }
  
  public void setLoavr(Character pLoavr) {
    if (pLoavr == null) {
      return;
    }
    loavr = String.valueOf(pLoavr);
  }
  
  public Character getLoavr() {
    return loavr.charAt(0);
  }
  
  public void setLoqta(BigDecimal pLoqta) {
    if (pLoqta == null) {
      return;
    }
    loqta = pLoqta.setScale(DECIMAL_LOQTA, RoundingMode.HALF_UP);
  }
  
  public void setLoqta(Double pLoqta) {
    if (pLoqta == null) {
      return;
    }
    loqta = BigDecimal.valueOf(pLoqta).setScale(DECIMAL_LOQTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqta() {
    return loqta.setScale(DECIMAL_LOQTA, RoundingMode.HALF_UP);
  }
  
  public void setLouna(String pLouna) {
    if (pLouna == null) {
      return;
    }
    louna = pLouna;
  }
  
  public String getLouna() {
    return louna;
  }
  
  public void setLokac(BigDecimal pLokac) {
    if (pLokac == null) {
      return;
    }
    lokac = pLokac.setScale(DECIMAL_LOKAC, RoundingMode.HALF_UP);
  }
  
  public void setLokac(Double pLokac) {
    if (pLokac == null) {
      return;
    }
    lokac = BigDecimal.valueOf(pLokac).setScale(DECIMAL_LOKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLokac() {
    return lokac.setScale(DECIMAL_LOKAC, RoundingMode.HALF_UP);
  }
  
  public void setLoart(String pLoart) {
    if (pLoart == null) {
      return;
    }
    loart = pLoart;
  }
  
  public String getLoart() {
    return loart;
  }
  
  public void setLomag(String pLomag) {
    if (pLomag == null) {
      return;
    }
    lomag = pLomag;
  }
  
  public String getLomag() {
    return lomag;
  }
  
  public void setLomaga(String pLomaga) {
    if (pLomaga == null) {
      return;
    }
    lomaga = pLomaga;
  }
  
  public String getLomaga() {
    return lomaga;
  }
  
  public void setLonumr(BigDecimal pLonumr) {
    if (pLonumr == null) {
      return;
    }
    lonumr = pLonumr.setScale(DECIMAL_LONUMR, RoundingMode.HALF_UP);
  }
  
  public void setLonumr(Integer pLonumr) {
    if (pLonumr == null) {
      return;
    }
    lonumr = BigDecimal.valueOf(pLonumr);
  }
  
  public Integer getLonumr() {
    return lonumr.intValue();
  }
  
  public void setLosufr(BigDecimal pLosufr) {
    if (pLosufr == null) {
      return;
    }
    losufr = pLosufr.setScale(DECIMAL_LOSUFR, RoundingMode.HALF_UP);
  }
  
  public void setLosufr(Integer pLosufr) {
    if (pLosufr == null) {
      return;
    }
    losufr = BigDecimal.valueOf(pLosufr);
  }
  
  public Integer getLosufr() {
    return losufr.intValue();
  }
  
  public void setLonlir(BigDecimal pLonlir) {
    if (pLonlir == null) {
      return;
    }
    lonlir = pLonlir.setScale(DECIMAL_LONLIR, RoundingMode.HALF_UP);
  }
  
  public void setLonlir(Integer pLonlir) {
    if (pLonlir == null) {
      return;
    }
    lonlir = BigDecimal.valueOf(pLonlir);
  }
  
  public Integer getLonlir() {
    return lonlir.intValue();
  }
  
  public void setLodath(BigDecimal pLodath) {
    if (pLodath == null) {
      return;
    }
    lodath = pLodath.setScale(DECIMAL_LODATH, RoundingMode.HALF_UP);
  }
  
  public void setLodath(Integer pLodath) {
    if (pLodath == null) {
      return;
    }
    lodath = BigDecimal.valueOf(pLodath);
  }
  
  public void setLodath(Date pLodath) {
    if (pLodath == null) {
      return;
    }
    lodath = BigDecimal.valueOf(ConvertDate.dateToDb2(pLodath));
  }
  
  public Integer getLodath() {
    return lodath.intValue();
  }
  
  public Date getLodathConvertiEnDate() {
    return ConvertDate.db2ToDate(lodath.intValue(), null);
  }
  
  public void setLoordh(BigDecimal pLoordh) {
    if (pLoordh == null) {
      return;
    }
    loordh = pLoordh.setScale(DECIMAL_LOORDH, RoundingMode.HALF_UP);
  }
  
  public void setLoordh(Integer pLoordh) {
    if (pLoordh == null) {
      return;
    }
    loordh = BigDecimal.valueOf(pLoordh);
  }
  
  public Integer getLoordh() {
    return loordh.intValue();
  }
  
  public void setLosan(String pLosan) {
    if (pLosan == null) {
      return;
    }
    losan = pLosan;
  }
  
  public String getLosan() {
    return losan;
  }
  
  public void setLoact(String pLoact) {
    if (pLoact == null) {
      return;
    }
    loact = pLoact;
  }
  
  public String getLoact() {
    return loact;
  }
  
  public void setLonat(Character pLonat) {
    if (pLonat == null) {
      return;
    }
    lonat = String.valueOf(pLonat);
  }
  
  public Character getLonat() {
    return lonat.charAt(0);
  }
  
  public void setLomta(BigDecimal pLomta) {
    if (pLomta == null) {
      return;
    }
    lomta = pLomta.setScale(DECIMAL_LOMTA, RoundingMode.HALF_UP);
  }
  
  public void setLomta(Double pLomta) {
    if (pLomta == null) {
      return;
    }
    lomta = BigDecimal.valueOf(pLomta).setScale(DECIMAL_LOMTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomta() {
    return lomta.setScale(DECIMAL_LOMTA, RoundingMode.HALF_UP);
  }
  
  public void setLodlp(BigDecimal pLodlp) {
    if (pLodlp == null) {
      return;
    }
    lodlp = pLodlp.setScale(DECIMAL_LODLP, RoundingMode.HALF_UP);
  }
  
  public void setLodlp(Integer pLodlp) {
    if (pLodlp == null) {
      return;
    }
    lodlp = BigDecimal.valueOf(pLodlp);
  }
  
  public void setLodlp(Date pLodlp) {
    if (pLodlp == null) {
      return;
    }
    lodlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pLodlp));
  }
  
  public Integer getLodlp() {
    return lodlp.intValue();
  }
  
  public Date getLodlpConvertiEnDate() {
    return ConvertDate.db2ToDate(lodlp.intValue(), null);
  }
  
  public void setLoser(BigDecimal pLoser) {
    if (pLoser == null) {
      return;
    }
    loser = pLoser.setScale(DECIMAL_LOSER, RoundingMode.HALF_UP);
  }
  
  public void setLoser(Integer pLoser) {
    if (pLoser == null) {
      return;
    }
    loser = BigDecimal.valueOf(pLoser);
  }
  
  public Integer getLoser() {
    return loser.intValue();
  }
  
  public void setLopai(BigDecimal pLopai) {
    if (pLopai == null) {
      return;
    }
    lopai = pLopai.setScale(DECIMAL_LOPAI, RoundingMode.HALF_UP);
  }
  
  public void setLopai(Double pLopai) {
    if (pLopai == null) {
      return;
    }
    lopai = BigDecimal.valueOf(pLopai).setScale(DECIMAL_LOPAI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopai() {
    return lopai.setScale(DECIMAL_LOPAI, RoundingMode.HALF_UP);
  }
  
  public void setLopar(BigDecimal pLopar) {
    if (pLopar == null) {
      return;
    }
    lopar = pLopar.setScale(DECIMAL_LOPAR, RoundingMode.HALF_UP);
  }
  
  public void setLopar(Double pLopar) {
    if (pLopar == null) {
      return;
    }
    lopar = BigDecimal.valueOf(pLopar).setScale(DECIMAL_LOPAR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLopar() {
    return lopar.setScale(DECIMAL_LOPAR, RoundingMode.HALF_UP);
  }
  
  public void setLorem1(BigDecimal pLorem1) {
    if (pLorem1 == null) {
      return;
    }
    lorem1 = pLorem1.setScale(DECIMAL_LOREM1, RoundingMode.HALF_UP);
  }
  
  public void setLorem1(Double pLorem1) {
    if (pLorem1 == null) {
      return;
    }
    lorem1 = BigDecimal.valueOf(pLorem1).setScale(DECIMAL_LOREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem1() {
    return lorem1.setScale(DECIMAL_LOREM1, RoundingMode.HALF_UP);
  }
  
  public void setLorem2(BigDecimal pLorem2) {
    if (pLorem2 == null) {
      return;
    }
    lorem2 = pLorem2.setScale(DECIMAL_LOREM2, RoundingMode.HALF_UP);
  }
  
  public void setLorem2(Double pLorem2) {
    if (pLorem2 == null) {
      return;
    }
    lorem2 = BigDecimal.valueOf(pLorem2).setScale(DECIMAL_LOREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem2() {
    return lorem2.setScale(DECIMAL_LOREM2, RoundingMode.HALF_UP);
  }
  
  public void setLorem3(BigDecimal pLorem3) {
    if (pLorem3 == null) {
      return;
    }
    lorem3 = pLorem3.setScale(DECIMAL_LOREM3, RoundingMode.HALF_UP);
  }
  
  public void setLorem3(Double pLorem3) {
    if (pLorem3 == null) {
      return;
    }
    lorem3 = BigDecimal.valueOf(pLorem3).setScale(DECIMAL_LOREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem3() {
    return lorem3.setScale(DECIMAL_LOREM3, RoundingMode.HALF_UP);
  }
  
  public void setLorem4(BigDecimal pLorem4) {
    if (pLorem4 == null) {
      return;
    }
    lorem4 = pLorem4.setScale(DECIMAL_LOREM4, RoundingMode.HALF_UP);
  }
  
  public void setLorem4(Double pLorem4) {
    if (pLorem4 == null) {
      return;
    }
    lorem4 = BigDecimal.valueOf(pLorem4).setScale(DECIMAL_LOREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem4() {
    return lorem4.setScale(DECIMAL_LOREM4, RoundingMode.HALF_UP);
  }
  
  public void setLorem5(BigDecimal pLorem5) {
    if (pLorem5 == null) {
      return;
    }
    lorem5 = pLorem5.setScale(DECIMAL_LOREM5, RoundingMode.HALF_UP);
  }
  
  public void setLorem5(Double pLorem5) {
    if (pLorem5 == null) {
      return;
    }
    lorem5 = BigDecimal.valueOf(pLorem5).setScale(DECIMAL_LOREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem5() {
    return lorem5.setScale(DECIMAL_LOREM5, RoundingMode.HALF_UP);
  }
  
  public void setLorem6(BigDecimal pLorem6) {
    if (pLorem6 == null) {
      return;
    }
    lorem6 = pLorem6.setScale(DECIMAL_LOREM6, RoundingMode.HALF_UP);
  }
  
  public void setLorem6(Double pLorem6) {
    if (pLorem6 == null) {
      return;
    }
    lorem6 = BigDecimal.valueOf(pLorem6).setScale(DECIMAL_LOREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLorem6() {
    return lorem6.setScale(DECIMAL_LOREM6, RoundingMode.HALF_UP);
  }
  
  public void setLotrl(Character pLotrl) {
    if (pLotrl == null) {
      return;
    }
    lotrl = String.valueOf(pLotrl);
  }
  
  public Character getLotrl() {
    return lotrl.charAt(0);
  }
  
  public void setLobrl(Character pLobrl) {
    if (pLobrl == null) {
      return;
    }
    lobrl = String.valueOf(pLobrl);
  }
  
  public Character getLobrl() {
    return lobrl.charAt(0);
  }
  
  public void setLorp1(Character pLorp1) {
    if (pLorp1 == null) {
      return;
    }
    lorp1 = String.valueOf(pLorp1);
  }
  
  public Character getLorp1() {
    return lorp1.charAt(0);
  }
  
  public void setLorp2(Character pLorp2) {
    if (pLorp2 == null) {
      return;
    }
    lorp2 = String.valueOf(pLorp2);
  }
  
  public Character getLorp2() {
    return lorp2.charAt(0);
  }
  
  public void setLorp3(Character pLorp3) {
    if (pLorp3 == null) {
      return;
    }
    lorp3 = String.valueOf(pLorp3);
  }
  
  public Character getLorp3() {
    return lorp3.charAt(0);
  }
  
  public void setLorp4(Character pLorp4) {
    if (pLorp4 == null) {
      return;
    }
    lorp4 = String.valueOf(pLorp4);
  }
  
  public Character getLorp4() {
    return lorp4.charAt(0);
  }
  
  public void setLorp5(Character pLorp5) {
    if (pLorp5 == null) {
      return;
    }
    lorp5 = String.valueOf(pLorp5);
  }
  
  public Character getLorp5() {
    return lorp5.charAt(0);
  }
  
  public void setLorp6(Character pLorp6) {
    if (pLorp6 == null) {
      return;
    }
    lorp6 = String.valueOf(pLorp6);
  }
  
  public Character getLorp6() {
    return lorp6.charAt(0);
  }
  
  public void setLotp1(String pLotp1) {
    if (pLotp1 == null) {
      return;
    }
    lotp1 = pLotp1;
  }
  
  public String getLotp1() {
    return lotp1;
  }
  
  public void setLotp2(String pLotp2) {
    if (pLotp2 == null) {
      return;
    }
    lotp2 = pLotp2;
  }
  
  public String getLotp2() {
    return lotp2;
  }
  
  public void setLotp3(String pLotp3) {
    if (pLotp3 == null) {
      return;
    }
    lotp3 = pLotp3;
  }
  
  public String getLotp3() {
    return lotp3;
  }
  
  public void setLotp4(String pLotp4) {
    if (pLotp4 == null) {
      return;
    }
    lotp4 = pLotp4;
  }
  
  public String getLotp4() {
    return lotp4;
  }
  
  public void setLotp5(String pLotp5) {
    if (pLotp5 == null) {
      return;
    }
    lotp5 = pLotp5;
  }
  
  public String getLotp5() {
    return lotp5;
  }
  
  public void setLoin1(Character pLoin1) {
    if (pLoin1 == null) {
      return;
    }
    loin1 = String.valueOf(pLoin1);
  }
  
  public Character getLoin1() {
    return loin1.charAt(0);
  }
  
  public void setLoin2(Character pLoin2) {
    if (pLoin2 == null) {
      return;
    }
    loin2 = String.valueOf(pLoin2);
  }
  
  public Character getLoin2() {
    return loin2.charAt(0);
  }
  
  public void setLoin3(Character pLoin3) {
    if (pLoin3 == null) {
      return;
    }
    loin3 = String.valueOf(pLoin3);
  }
  
  public Character getLoin3() {
    return loin3.charAt(0);
  }
  
  public void setLoin4(Character pLoin4) {
    if (pLoin4 == null) {
      return;
    }
    loin4 = String.valueOf(pLoin4);
  }
  
  public Character getLoin4() {
    return loin4.charAt(0);
  }
  
  public void setLoin5(Character pLoin5) {
    if (pLoin5 == null) {
      return;
    }
    loin5 = String.valueOf(pLoin5);
  }
  
  public Character getLoin5() {
    return loin5.charAt(0);
  }
  
  public void setLodlc(BigDecimal pLodlc) {
    if (pLodlc == null) {
      return;
    }
    lodlc = pLodlc.setScale(DECIMAL_LODLC, RoundingMode.HALF_UP);
  }
  
  public void setLodlc(Integer pLodlc) {
    if (pLodlc == null) {
      return;
    }
    lodlc = BigDecimal.valueOf(pLodlc);
  }
  
  public void setLodlc(Date pLodlc) {
    if (pLodlc == null) {
      return;
    }
    lodlc = BigDecimal.valueOf(ConvertDate.dateToDb2(pLodlc));
  }
  
  public Integer getLodlc() {
    return lodlc.intValue();
  }
  
  public Date getLodlcConvertiEnDate() {
    return ConvertDate.db2ToDate(lodlc.intValue(), null);
  }
  
  public void setLoqtai(BigDecimal pLoqtai) {
    if (pLoqtai == null) {
      return;
    }
    loqtai = pLoqtai.setScale(DECIMAL_LOQTAI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtai(Double pLoqtai) {
    if (pLoqtai == null) {
      return;
    }
    loqtai = BigDecimal.valueOf(pLoqtai).setScale(DECIMAL_LOQTAI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtai() {
    return loqtai.setScale(DECIMAL_LOQTAI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtci(BigDecimal pLoqtci) {
    if (pLoqtci == null) {
      return;
    }
    loqtci = pLoqtci.setScale(DECIMAL_LOQTCI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtci(Double pLoqtci) {
    if (pLoqtci == null) {
      return;
    }
    loqtci = BigDecimal.valueOf(pLoqtci).setScale(DECIMAL_LOQTCI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtci() {
    return loqtci.setScale(DECIMAL_LOQTCI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtsi(BigDecimal pLoqtsi) {
    if (pLoqtsi == null) {
      return;
    }
    loqtsi = pLoqtsi.setScale(DECIMAL_LOQTSI, RoundingMode.HALF_UP);
  }
  
  public void setLoqtsi(Double pLoqtsi) {
    if (pLoqtsi == null) {
      return;
    }
    loqtsi = BigDecimal.valueOf(pLoqtsi).setScale(DECIMAL_LOQTSI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtsi() {
    return loqtsi.setScale(DECIMAL_LOQTSI, RoundingMode.HALF_UP);
  }
  
  public void setLomhti(BigDecimal pLomhti) {
    if (pLomhti == null) {
      return;
    }
    lomhti = pLomhti.setScale(DECIMAL_LOMHTI, RoundingMode.HALF_UP);
  }
  
  public void setLomhti(Double pLomhti) {
    if (pLomhti == null) {
      return;
    }
    lomhti = BigDecimal.valueOf(pLomhti).setScale(DECIMAL_LOMHTI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomhti() {
    return lomhti.setScale(DECIMAL_LOMHTI, RoundingMode.HALF_UP);
  }
  
  public void setPonatan(String pPonatan) {
    if (pPonatan == null) {
      return;
    }
    ponatan = pPonatan;
  }
  
  public String getPonatan() {
    return ponatan;
  }
  
  public void setWtypart(String pWtypart) {
    if (pWtypart == null) {
      return;
    }
    wtypart = pWtypart;
  }
  
  public String getWtypart() {
    return wtypart;
  }
  
  public void setWkcsa(BigDecimal pWkcsa) {
    if (pWkcsa == null) {
      return;
    }
    wkcsa = pWkcsa.setScale(DECIMAL_WKCSA, RoundingMode.HALF_UP);
  }
  
  public void setWkcsa(Double pWkcsa) {
    if (pWkcsa == null) {
      return;
    }
    wkcsa = BigDecimal.valueOf(pWkcsa).setScale(DECIMAL_WKCSA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkcsa() {
    return wkcsa.setScale(DECIMAL_WKCSA, RoundingMode.HALF_UP);
  }
  
  public void setWprbru(BigDecimal pWprbru) {
    if (pWprbru == null) {
      return;
    }
    wprbru = pWprbru.setScale(DECIMAL_WPRBRU, RoundingMode.HALF_UP);
  }
  
  public void setWprbru(Double pWprbru) {
    if (pWprbru == null) {
      return;
    }
    wprbru = BigDecimal.valueOf(pWprbru).setScale(DECIMAL_WPRBRU, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprbru() {
    return wprbru.setScale(DECIMAL_WPRBRU, RoundingMode.HALF_UP);
  }
  
  public void setWprnet(BigDecimal pWprnet) {
    if (pWprnet == null) {
      return;
    }
    wprnet = pWprnet.setScale(DECIMAL_WPRNET, RoundingMode.HALF_UP);
  }
  
  public void setWprnet(Double pWprnet) {
    if (pWprnet == null) {
      return;
    }
    wprnet = BigDecimal.valueOf(pWprnet).setScale(DECIMAL_WPRNET, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprnet() {
    return wprnet.setScale(DECIMAL_WPRNET, RoundingMode.HALF_UP);
  }
  
  public void setWprvfr(BigDecimal pWprvfr) {
    if (pWprvfr == null) {
      return;
    }
    wprvfr = pWprvfr.setScale(DECIMAL_WPRVFR, RoundingMode.HALF_UP);
  }
  
  public void setWprvfr(Double pWprvfr) {
    if (pWprvfr == null) {
      return;
    }
    wprvfr = BigDecimal.valueOf(pWprvfr).setScale(DECIMAL_WPRVFR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWprvfr() {
    return wprvfr.setScale(DECIMAL_WPRVFR, RoundingMode.HALF_UP);
  }
  
  public void setWportf(BigDecimal pWportf) {
    if (pWportf == null) {
      return;
    }
    wportf = pWportf.setScale(DECIMAL_WPORTF, RoundingMode.HALF_UP);
  }
  
  public void setWportf(Double pWportf) {
    if (pWportf == null) {
      return;
    }
    wportf = BigDecimal.valueOf(pWportf).setScale(DECIMAL_WPORTF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWportf() {
    return wportf.setScale(DECIMAL_WPORTF, RoundingMode.HALF_UP);
  }
  
  public void setPoxxx1(BigDecimal pPoxxx1) {
    if (pPoxxx1 == null) {
      return;
    }
    poxxx1 = pPoxxx1.setScale(DECIMAL_POXXX1, RoundingMode.HALF_UP);
  }
  
  public void setPoxxx1(Double pPoxxx1) {
    if (pPoxxx1 == null) {
      return;
    }
    poxxx1 = BigDecimal.valueOf(pPoxxx1).setScale(DECIMAL_POXXX1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPoxxx1() {
    return poxxx1.setScale(DECIMAL_POXXX1, RoundingMode.HALF_UP);
  }
  
  public void setPolfk1(BigDecimal pPolfk1) {
    if (pPolfk1 == null) {
      return;
    }
    polfk1 = pPolfk1.setScale(DECIMAL_POLFK1, RoundingMode.HALF_UP);
  }
  
  public void setPolfk1(Double pPolfk1) {
    if (pPolfk1 == null) {
      return;
    }
    polfk1 = BigDecimal.valueOf(pPolfk1).setScale(DECIMAL_POLFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPolfk1() {
    return polfk1.setScale(DECIMAL_POLFK1, RoundingMode.HALF_UP);
  }
  
  public void setPolfv1(BigDecimal pPolfv1) {
    if (pPolfv1 == null) {
      return;
    }
    polfv1 = pPolfv1.setScale(DECIMAL_POLFV1, RoundingMode.HALF_UP);
  }
  
  public void setPolfv1(Double pPolfv1) {
    if (pPolfv1 == null) {
      return;
    }
    polfv1 = BigDecimal.valueOf(pPolfv1).setScale(DECIMAL_POLFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPolfv1() {
    return polfv1.setScale(DECIMAL_POLFV1, RoundingMode.HALF_UP);
  }
  
  public void setPolfp1(BigDecimal pPolfp1) {
    if (pPolfp1 == null) {
      return;
    }
    polfp1 = pPolfp1.setScale(DECIMAL_POLFP1, RoundingMode.HALF_UP);
  }
  
  public void setPolfp1(Double pPolfp1) {
    if (pPolfp1 == null) {
      return;
    }
    polfp1 = BigDecimal.valueOf(pPolfp1).setScale(DECIMAL_POLFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPolfp1() {
    return polfp1.setScale(DECIMAL_POLFP1, RoundingMode.HALF_UP);
  }
  
  public void setPolfk3(BigDecimal pPolfk3) {
    if (pPolfk3 == null) {
      return;
    }
    polfk3 = pPolfk3.setScale(DECIMAL_POLFK3, RoundingMode.HALF_UP);
  }
  
  public void setPolfk3(Double pPolfk3) {
    if (pPolfk3 == null) {
      return;
    }
    polfk3 = BigDecimal.valueOf(pPolfk3).setScale(DECIMAL_POLFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPolfk3() {
    return polfk3.setScale(DECIMAL_POLFK3, RoundingMode.HALF_UP);
  }
  
  public void setPolfv3(BigDecimal pPolfv3) {
    if (pPolfv3 == null) {
      return;
    }
    polfv3 = pPolfv3.setScale(DECIMAL_POLFV3, RoundingMode.HALF_UP);
  }
  
  public void setPolfv3(Double pPolfv3) {
    if (pPolfv3 == null) {
      return;
    }
    polfv3 = BigDecimal.valueOf(pPolfv3).setScale(DECIMAL_POLFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPolfv3() {
    return polfv3.setScale(DECIMAL_POLFV3, RoundingMode.HALF_UP);
  }
  
  public void setLoqtat(BigDecimal pLoqtat) {
    if (pLoqtat == null) {
      return;
    }
    loqtat = pLoqtat.setScale(DECIMAL_LOQTAT, RoundingMode.HALF_UP);
  }
  
  public void setLoqtat(Double pLoqtat) {
    if (pLoqtat == null) {
      return;
    }
    loqtat = BigDecimal.valueOf(pLoqtat).setScale(DECIMAL_LOQTAT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtat() {
    return loqtat.setScale(DECIMAL_LOQTAT, RoundingMode.HALF_UP);
  }
  
  public void setLoqtct(BigDecimal pLoqtct) {
    if (pLoqtct == null) {
      return;
    }
    loqtct = pLoqtct.setScale(DECIMAL_LOQTCT, RoundingMode.HALF_UP);
  }
  
  public void setLoqtct(Double pLoqtct) {
    if (pLoqtct == null) {
      return;
    }
    loqtct = BigDecimal.valueOf(pLoqtct).setScale(DECIMAL_LOQTCT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtct() {
    return loqtct.setScale(DECIMAL_LOQTCT, RoundingMode.HALF_UP);
  }
  
  public void setLoqtst(BigDecimal pLoqtst) {
    if (pLoqtst == null) {
      return;
    }
    loqtst = pLoqtst.setScale(DECIMAL_LOQTST, RoundingMode.HALF_UP);
  }
  
  public void setLoqtst(Double pLoqtst) {
    if (pLoqtst == null) {
      return;
    }
    loqtst = BigDecimal.valueOf(pLoqtst).setScale(DECIMAL_LOQTST, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLoqtst() {
    return loqtst.setScale(DECIMAL_LOQTST, RoundingMode.HALF_UP);
  }
  
  public void setLomhtt(BigDecimal pLomhtt) {
    if (pLomhtt == null) {
      return;
    }
    lomhtt = pLomhtt.setScale(DECIMAL_LOMHTT, RoundingMode.HALF_UP);
  }
  
  public void setLomhtt(Double pLomhtt) {
    if (pLomhtt == null) {
      return;
    }
    lomhtt = BigDecimal.valueOf(pLomhtt).setScale(DECIMAL_LOMHTT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getLomhtt() {
    return lomhtt.setScale(DECIMAL_LOMHTT, RoundingMode.HALF_UP);
  }
  
  public void setLoarr(Character pLoarr) {
    if (pLoarr == null) {
      return;
    }
    loarr = String.valueOf(pLoarr);
  }
  
  public Character getLoarr() {
    return loarr.charAt(0);
  }
}
