/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.client;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;

import ri.serien.libas400.dao.exp.programs.contact.GM_LienContactAvecTiers;
import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libas400.dao.gvm.database.files.FFD_Pgvmclim;
import ri.serien.libas400.dao.rpg.gescom.client.creation.Svgvm0022i;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.EnumCodeAttentionClient;
import ri.serien.libcommun.gescom.commun.client.EnumEtatFicheClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeImageClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.IdCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.reglement.IdModeReglement;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEdition;
import ri.serien.libcommun.outils.Constantes;

/**
 * Gestion bas niveau de la table PGVMCLIM - Concerne un enregistrement
 */
public class M_Client extends FFD_Pgvmclim {
  // Variables
  private ArrayList<M_Contact> listContact = null;
  
  /**
   * Constructeur
   * @param aquerymg
   */
  public M_Client(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  // -- Méthodes publiques
  
  /**
   * Insère l'enregistrement dans le table
   * @return
   */
  @Override
  public boolean insertInDatabase() {
    initGenericRecord(genericrecord, true);
    String requete = genericrecord.createSQLRequestInsert("PGVMCLIM", querymg.getLibrary());
    return request(requete);
  }
  
  /**
   * Modifie l'enregistrement dans le table
   * @return
   */
  @Override
  public boolean updateInDatabase() {
    initGenericRecord(genericrecord, false);
    String requete = genericrecord.createSQLRequestUpdate("PGVMCLIM", querymg.getLibrary(),
        "CLNUM=" + getCLCLI() + " and CLLIV=" + getCLLIV() + " and CLETB='" + getCLETB() + "'");
    return request(requete);
  }
  
  /**
   * Suppression de l'enregistrement courant
   * @return
   */
  @Override
  public boolean deleteInDatabase() {
    return deleteInDatabase(false);
  }
  
  /**
   * Suppression de l'enregistrement courant ou de tous
   * @return
   */
  public boolean deleteInDatabase(boolean deleteAll) {
    String requete = "delete from " + querymg.getLibrary() + ".PGVMCLIM where CLETB='" + getCLETB() + "'";
    if (!deleteAll) {
      requete += " and CLNUM=" + getCLCLI() + " and CLLIV=" + getCLLIV();
    }
    return request(requete);
  }
  
  /**
   * Récupération des contacts pour le client (Attention il est possible que le client n'est pas de contact)
   * @param id
   * @return
   */
  public boolean loadContacts() {
    GM_LienContactAvecTiers lct = new GM_LienContactAvecTiers(querymg);
    
    // Chargement des classes avec les données de la base
    if (listContact != null) {
      listContact.clear();
    }
    listContact = lct.readContact4Client(this);
    return true;
  }
  
  /**
   * Récupération des contacts pour le client (Attention il est possible que le client n'ai pas de contact)
   */
  public void loadExtensionClient(Client pClient) {
    M_ExtensionClient extension = new M_ExtensionClient(querymg);
    extension.load(pClient);
    if (extension.getEBETB() == null && !pClient.isClientProspect()) {
      pClient.setTypeCompteClient(EnumTypeCompteClient.EN_COMPTE);
    }
  }
  
  /**
   * Retourne le numéro client et livré concaténés
   * @return
   */
  public String getCLIandLIV() {
    // return String.format("%0"+M_Client.SIZE_CLCLI+"d%0"+M_Client.SIZE_CLLIV+"d", getCLCLI(), getCLLIV());
    return getCLIandLIV(getCLCLI(), getCLLIV());
  }
  
  /**
   * Retourne le numéro client et livré concaténés.
   */
  public static String getCLIandLIV(int aCodeClient, int aSuffixeClient) {
    return String.format("%0" + FFD_Pgvmclim.SIZE_CLCLI + "d%0" + FFD_Pgvmclim.SIZE_CLLIV + "d", aCodeClient, aSuffixeClient);
  }
  
  /**
   * Récupération des informations d'un client bien définit.
   */
  public Client load(IdClient pIdClient) {
    IdClient.controlerId(pIdClient, true);
    
    // Lecture de la base afin de récupérer un client bien définit
    String requete = "select * from " + querymg.getLibrary() + ".pgvmclim where clcli = " + pIdClient.getNumero() + " and clliv = "
        + pIdClient.getSuffixe() + " and cletb = '" + pIdClient.getCodeEtablissement() + "'";
    ArrayList<GenericRecord> listrcd = querymg.select(requete);
    if ((listrcd == null) || (listrcd.size() < 1)) {
      return null;
    }
    // Chargement des classes avec les données de la base
    boolean ret = initObject(listrcd.get(0), true);
    if (!ret) {
      return null;
    }
    
    Client client = initClientGlobal();
    
    // Chargement de l'extension client pour le client en cours
    loadExtensionClient(client);
    
    return client;
  }
  
  /**
   * Permet d'extraire le code postal du champ CLVIL.
   */
  private int extraireCodePostalDeCLVIL() {
    String ville = Constantes.normerTexte(getCLVIL());
    if (ville.isEmpty() || ville.length() < Client.TAILLE_ZONE_CODEPOSTAL) {
      return 0;
    }
    
    String codepostal = ville.substring(0, Client.TAILLE_ZONE_CODEPOSTAL);
    return Constantes.convertirTexteEnInteger(codepostal);
  }
  
  /**
   * Retourne le code postal.
   */
  private int getCodePostal() {
    if (getCLCDP1() > 0) {
      return getCLCDP1();
    }
    else {
      return extraireCodePostalDeCLVIL();
    }
  }
  
  /**
   * Initialise les champs de la classe Client
   * @param client
   * @return
   */
  public Client initClient() {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(getCLETB());
    Client client = new Client(IdClient.getInstance(idEtablissement, getCLCLI(), getCLLIV()));
    if (getCLIN9() == null || getCLIN9().isEmpty()) {
      client.setTypeImageClient(EnumTypeImageClient.PROFESSIONNEL);
    }
    else {
      client.setTypeImageClient(EnumTypeImageClient.valueOfByCode(getCLIN9().charAt(0)));
    }
    
    client.setNumeroTelephone(getCLTEL());
    if (!getCLCAT().trim().isEmpty()) {
      IdCategorieClient idCategorieClient = IdCategorieClient.getInstance(idEtablissement, getCLCAT());
      client.setIdCategorieClient(idCategorieClient);
    }
    Adresse adresse = new Adresse();
    adresse.setNom(getCLNOM());
    adresse.setComplementNom(getCLCPL());
    adresse.setRue(getCLRUE());
    adresse.setLocalisation(getCLLOC());
    adresse.setCodePostal(getCodePostal());
    adresse.setVilleAvecCodePostal(getCLVIL());
    adresse.setPays(getCLPAY());
    client.setAdresse(adresse);
    
    return client;
  }
  
  /**
   * Initialise les champs de la classe Client
   * @param client
   * @return
   */
  public ClientBase initClientBase() {
    IdEtablissement idEtablissement = IdEtablissement.getInstance(getCLETB());
    ClientBase client = new ClientBase(IdClient.getInstance(idEtablissement, getCLCLI(), getCLLIV()));
    if (getCLIN9() == null || getCLIN9().isEmpty()) {
      client.setTypeImageClient(EnumTypeImageClient.PROFESSIONNEL);
    }
    else {
      client.setTypeImageClient(EnumTypeImageClient.valueOfByCode(getCLIN9().charAt(0)));
    }
    
    client.setNumeroTelephone(getCLTEL());
    if (!getCLCAT().trim().isEmpty()) {
      IdCategorieClient idCategorieClient = IdCategorieClient.getInstance(idEtablissement, getCLCAT());
      client.setIdCategorieClient(idCategorieClient);
    }
    Adresse adresse = new Adresse();
    adresse.setNom(getCLNOM());
    adresse.setComplementNom(getCLCPL());
    adresse.setRue(getCLRUE());
    adresse.setLocalisation(getCLLOC());
    adresse.setCodePostal(getCodePostal());
    adresse.setVilleAvecCodePostal(getCLVIL());
    adresse.setPays(getCLPAY());
    client.setAdresse(adresse);
    
    return client;
  }
  
  /**
   * Initialise tous les champs de la classe client (pour l'instant le fichier PGVMCLIM)
   * @param pClient
   */
  private Client initClientGlobal() {
    
    IdEtablissement idEtablissement = IdEtablissement.getInstance(getCLETB());
    Client client = new Client(IdClient.getInstance(idEtablissement, getCLCLI(), getCLLIV()));
    
    client.setEtatFiche(EnumEtatFicheClient.valueOfByCode(getCLTOP()));
    Adresse adresse = new Adresse();
    adresse.setNom(getCLNOM());
    adresse.setComplementNom(getCLCPL());
    adresse.setRue(getCLRUE());
    adresse.setLocalisation(getCLLOC());
    adresse.setCodePostal(getCodePostal());
    adresse.setVilleAvecCodePostal(getCLVIL());
    adresse.setPays(getCLPAY());
    adresse.setCodePays(getCLCOP());
    client.setAdresse(adresse);
    
    client.setCritereAnalyse1(getCLTOP1());
    client.setCritereAnalyse2(getCLTOP2());
    client.setCritereAnalyse3(getCLTOP3());
    client.setCritereAnalyse4(getCLTOP4());
    client.setCritereAnalyse5(getCLTOP5());
    
    if (!getCLCAT().trim().isEmpty()) {
      IdCategorieClient idCategorieClient = IdCategorieClient.getInstance(idEtablissement, getCLCAT());
      client.setIdCategorieClient(idCategorieClient);
    }
    client.setCleClassement(getCLCLK());
    client.setCodeAPE(getCLAPE());
    client.setNumeroTelephone(getCLTEL());
    client.setNumeroFax(getCLFAX());
    client.setNumeroTelex(getCLTLX());
    client.setCodeLangue(getCLLAN());
    client.setObservations(getCLOBS());
    if (!getCLREP().trim().isEmpty()) {
      client.setIdRepresentant(IdRepresentant.getInstance(idEtablissement, getCLREP()));
    }
    if (!getCLREP2().trim().isEmpty()) {
      client.setIdRepresentant2(IdRepresentant.getInstance(idEtablissement, getCLREP2()));
    }
    client.setTypeCommissionRepresentant(getCLTRP());
    client.setZoneGeographique(getCLGEO());
    client.setModeExpedition(getCLMEX());
    if (getCLCTR() != null && !getCLCTR().trim().isEmpty()) {
      IdTransporteur idTransporteur = IdTransporteur.getInstance(idEtablissement, getCLCTR());
      client.setIdTransporteur(idTransporteur);
    }
    if (!getCLMAG().trim().isEmpty()) {
      client.setIdMagasinRattachement(IdMagasin.getInstance(idEtablissement, getCLMAG()));
    }
    client.setMontantFrancoPort(getCLFRP());
    if (!getCLTFA().trim().isEmpty()) {
      client.setIdTypeFacturation(IdTypeFacturation.getInstance(idEtablissement, getCLTFA()));
    }
    client.setFactureEnTTC(!getCLTTC().trim().isEmpty());
    client.setCodeDevise(getCLDEV());
    client.setCodeClientFacture(getCLCLFP());
    client.setSuffixeLivClientFacture(getCLCLFS());
    client.setCodeClientPayeur(getCLCLP());
    client.setCodeAffacturage(getCLAFA());
    client.setTauxEscompte(BigDecimal.valueOf(getCLESC()).setScale(Svgvm0022i.DECIMAL_PIESC, RoundingMode.HALF_UP));
    client.setNumeroTarif(getCLTAR());
    client.setNumeroTarif2(getCLTA1());
    client.setRemise1(BigDecimal.valueOf(getCLREM1()).setScale(Svgvm0022i.DECIMAL_PIREM1, RoundingMode.HALF_UP));
    client.setRemise2(BigDecimal.valueOf(getCLREM2()).setScale(Svgvm0022i.DECIMAL_PIREM2, RoundingMode.HALF_UP));
    client.setRemise3(BigDecimal.valueOf(getCLREM3()).setScale(Svgvm0022i.DECIMAL_PIREM3, RoundingMode.HALF_UP));
    client.setRemisePied1(BigDecimal.valueOf(getCLRP1()).setScale(Svgvm0022i.DECIMAL_PIRP1, RoundingMode.HALF_UP));
    client.setRemisePied2(BigDecimal.valueOf(getCLRP2()).setScale(Svgvm0022i.DECIMAL_PIRP2, RoundingMode.HALF_UP));
    client.setRemisePied3(BigDecimal.valueOf(getCLRP3()).setScale(Svgvm0022i.DECIMAL_PIRP3, RoundingMode.HALF_UP));
    client.setCodeCNV(getCLCNV());
    client.setPeriodiciteFacturation(getCLPFA());
    client.setPeriodiciteReleve(getCLPRL());
    client.setCodeReleve(getCLRLV());
    client.setRegroupementBonSurFacture(getCLRBF());
    if (getCLRGL() != null && !getCLRGL().trim().isEmpty()) {
      IdModeReglement idReglement = IdModeReglement.getInstance(getCLRGL());
      client.setIdReglement(idReglement);
    }
    client.setCodeEcheance(getCLECH());
    client.setCollectifComptable(getCLNCG());
    client.setCompteAuxiliaire(getCLNCA());
    client.setCodeAffaireEnCours(getCLACT());
    client.setPlafondEncoursAutorise(getCLPLF());
    client.setSurPlafondEncoursAutorise(getCLPLF2());
    client.setTopAttention(EnumCodeAttentionClient.valueOfByCode(getCLTNS()));
    client.setTexteAttentionCommande(getCLATT());
    client.setNoteClient(getCLNOT());
    client.setDateDerniereVisite(getCLDDV());
    client.setDateDerniereVente(getCLDVE());
    client.setNumeroSIREN(getCLSRN());
    client.setComplementSIRET(getCLSRT());
    client.setDepassementEncours(getCLFIL1());
    client.setCodePaysCEE(getCLCEE());
    client.setVenteAssimileExport(getCLVAE());
    client.setEncoursCommande(BigDecimal.valueOf(getCLCDE()).setScale(Svgvm0022i.DECIMAL_PICDE, RoundingMode.HALF_UP));
    client.setEncoursExpedie(BigDecimal.valueOf(getCLEXP()).setScale(Svgvm0022i.DECIMAL_PIEXP, RoundingMode.HALF_UP));
    client.setEncoursFacture(BigDecimal.valueOf(getCLFAC()).setScale(Svgvm0022i.DECIMAL_PIFAC, RoundingMode.HALF_UP));
    client.setPositionComptable(getCLPCO());
    
    client.setZoneSystemMajCompta(getCLCGM());
    client.setTopMaj(getCLMAJ());
    client.setNbrExemplairesBonHom(getCLNEX1());
    client.setNbrExemplairesBonExp(getCLNEX2());
    client.setNbrExemplairesBonFac(getCLNEX3());
    client.setNumeroLibelleArticle(getCLNLA());
    client.setZoneRegroupementStats(getCLRST());
    client.setTopLivraisonPartielle("" + getCLIN1());
    client.setReliquatsAcceptes(getCLIN2());
    String clin3 = Constantes.normerTexte(getCLIN3());
    if (clin3.isEmpty() || clin3.charAt(0) == '0') {
      client.setEditionChiffre(EnumTypeEdition.EDITION_CHIFFREE);
    }
    else if (clin3.charAt(0) == '1') {
      client.setEditionChiffre(EnumTypeEdition.EDITION_NON_CHIFFREE);
    }
    else {
      client.setEditionChiffre(EnumTypeEdition.EDITION_SANS_TOTAUX);
    }
    String clin4 = getCLIN4();
    if (clin4 == null || clin4.isEmpty()) {
      client.setNonUtilisationDGCNV(' ');
    }
    else {
      client.setNonUtilisationDGCNV(clin4.charAt(0));
    }
    client.setCodeGBAaRepercuter(getCLIN5());
    client.setRfaCentrale(getCLIN6());
    client.setNonEditionEtqColis(getCLIN7());
    client.setEditionFacBonExpEnchaine(getCLIN8());
    if (getCLIN9() == null || getCLIN9().isEmpty()) {
      client.setTypeImageClient(EnumTypeImageClient.PROFESSIONNEL);
    }
    else {
      client.setTypeImageClient(EnumTypeImageClient.valueOfByCode(getCLIN9().charAt(0)));
    }
    client.setExclusionCNVQuantitative(getCLIN10());
    client.setPasEtqColisExpedition(getCLIN11());
    client.setFactureDifsurCmdSoldee(getCLIN12());
    client.setNonSoumisTaxeAddtit(getCLIN13());
    client.setCodePEParticipationFraisExp(getCLIN14());
    client.setGenerationCNVFacPrixNet(getCLIN15());
    client.setCodeAdherent(getCLADH());
    client.setCodeCondCommissionnement(getCLCNC());
    client.setCentrale1(getCLCC1());
    client.setCentrale2(getCLCC2());
    client.setCentrale3(getCLCC3());
    client.setCodeFirme(getCLFIR());
    client.setClientTransitaire(getCLTRA());
    client.setCodeConditionRistourne(getCLCNR());
    client.setCodeConditionPromo(getCLCNP());
    client.setCodeRemisePiedBon(getCLCNB());
    client.setDateLimitePourSurPlafond(getCLDPL());
    client.setApplicationFraisFixes(getCLIN16());
    client.setApplicationRemisePiedFac(getCLIN17());
    client.setCodeRegroupementTransport(getCLCRT());
    client.setOrdreDansLaTournee(getCLOTO());
    client.setCanalDeVente(getCLCAN());
    client.setPrenoms(getCLPRN());
    client.setRecenceCmd(getCLREC());
    client.setFrequenceCmd(getCLFRC());
    client.setMontantCmd(getCLMTC());
    client.setCodePaysTVAIntracom(getCLNIP());
    client.setCleTvaIntracom(getCLNIK());
    client.setClasseClient1(getCLCL1());
    client.setClasseClient2(getCLCL2());
    client.setClasseClient3(getCLCL3());
    client.setCleClassement2(getCLCLA());
    client.setClientSoumisDroitPret(getCLIN18());
    client.setEditionFactures(getCLIN19());
    client.setPrecommandesAcceptees(getCLIN20());
    client.setNombreExemplairesAvoirs(getCLIN21());
    client.setTypeVente(getCLIN22());
    client.setTrancheEffectif(getCLIN23());
    client.setCodePEParticipationFraisExp2(getCLIN24());
    client.setCodePEParticipationFraisExp3(getCLIN25());
    client.setNonEditionRemises(getCLIN26());
    client.setNonEditionPrixBase(getCLIN27());
    client.setCodeAPEv2(getCLAPEN());
    client.setPlafondDemande(getCLPLF3());
    client.setDateDemandePlafond(getCLDPL3());
    client.setIdentifiantAssurance(getCLIDAS());
    client.setCodeAssurance(getCLCAS());
    client.setDateValiditePlafond(getCLDVP3());
    client.setCaPrevisionnel(getCLDAT1());
    client.setIdBizyNova(getCLDAT2());
    client.setPlafondMaxEnDeblocage(getCLDAT3());
    client.setFraisFacturePied1(BigDecimal.valueOf(getCLFFP1()).setScale(Svgvm0022i.DECIMAL_PIFFP1, RoundingMode.HALF_UP));
    client.setFraisFacturePied2(BigDecimal.valueOf(getCLFFP2()).setScale(Svgvm0022i.DECIMAL_PIFFP2, RoundingMode.HALF_UP));
    client.setFraisFacturePied3(BigDecimal.valueOf(getCLFFP3()).setScale(Svgvm0022i.DECIMAL_PIFFP3, RoundingMode.HALF_UP));
    client.setNonEditionNumerosSeries(getCLIN28());
    client.setTypeClient(getCLIN29());
    if (getCLIN29().trim().equalsIgnoreCase("P")) {
      client.setTypeCompteClient(EnumTypeCompteClient.PROSPECT);
    }
    boolean isRefLongueObligatoire = (getCLIN30() != null && getCLIN30().equals("1"));
    client.setRefLongueCommandeObligatoire(isRefLongueObligatoire);
    
    return client;
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    querymg = null;
    genericrecord.dispose();
    
    if (listContact != null) {
      for (M_Contact c : listContact) {
        c.dispose();
      }
      listContact.clear();
    }
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le listContact
   */
  public ArrayList<M_Contact> getListContact(boolean refresh) {
    if (refresh) {
      loadContacts();
    }
    return listContact;
  }
  
  /**
   * @param listContact le listContact à définir
   * 
   *          public void setListContact(ArrayList<M_Contact> listContact)
   *          {
   *          this.listContact = listContact;
   *          }
   */
  
}
