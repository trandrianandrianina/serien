/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.comptabilite.parametres;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.ibm.as400.access.Record;

import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.ExtendRecord;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.comptabilite.personnalisation.journal.CriteresRechercheJournaux;
import ri.serien.libcommun.comptabilite.personnalisation.journal.IdJournal;
import ri.serien.libcommun.comptabilite.personnalisation.journal.Journal;
import ri.serien.libcommun.comptabilite.personnalisation.journal.ListeJournal;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class SqlComptabiliteParametres extends BaseGroupDB {
  
  /**
   * Constructeur.
   */
  public SqlComptabiliteParametres(QueryManager aquerymg) {
    super(aquerymg);
  }
  
  /**
   * Retourne la liste des journaux.
   */
  public ListeJournal chargerListeJournal(CriteresRechercheJournaux aCriteres) {
    if (aCriteres == null) {
      return null;
    }
    
    final ArrayList<Record> listeRecord = queryManager.selectAvecDataStructure(retournerRequete(aCriteres),
        "ri.serien.libas400.dao.sql.comptabilite.parametres.pcgmpacm.DsJournal");
    if ((listeRecord == null) || (listeRecord.size() == 0)) {
      return null;
    }
    
    // On alimente le tableau avec les enregistrements trouvés
    ListeJournal listeJournal = new ListeJournal();
    ExtendRecord record = new ExtendRecord();
    for (Record rcd : listeRecord) {
      record.setRecord(rcd);
      Journal journal = initRechercheJournaux(aCriteres, record);
      listeJournal.add(journal);
    }
    return listeJournal;
  }
  
  /**
   * Initialise un journal à partir de l'enregistrement donnée et du type de la recherche.
   */
  private Journal initRechercheJournaux(CriteresRechercheJournaux aCriteres, ExtendRecord record) {
    IdJournal idJournal = IdJournal.getInstance(aCriteres.getIdEtablissement(), ((String) record.getField("JOCOD")).trim());
    Journal journal = new Journal(idJournal);
    journal.setLibelle(((String) record.getField("JOLIB")).trim());
    journal.setType(((BigDecimal) record.getField("JOTYPE")).intValueExact());
    journal.setTresorerie(((String) record.getField("JOTRE")).trim());
    return journal;
  }
  
  /**
   * Retourne la requête
   */
  private String retournerRequete(CriteresRechercheJournaux aCriteres) {
    return "select PACTOP, PACFI1, PACCLE, PACFIL from " + queryManager.getLibrary() + "."
        + EnumTableBDD.PARAMETRE_COMPTABILITE + " where PACTOP <> '9' " + "and substring(PACCLE, 1, 3) = '"
        + aCriteres.getIdEtablissement() + "' " + "and substring(PACCLE, 4, 2) = 'JO' ";
  }
  
  /**
   * Construction de la requête et envoi de la demande.
   */
  private String retournerRequetePourLibEcran(CriteresRechercheJournaux aCriteres) {
    // Construction de la requête (Attention le mot libelle est sans accent car soucis si on envoie depuis un mac à
    // cause de l'encodage, il va falloir utiliser l'encodage de Constantes)
    String requete =
        "select PACTOP, PACFI1, PACCLE, PACFIL from " + queryManager.getLibrary() + "." + EnumTableBDD.PARAMETRE_COMPTABILITE
            + " where " + " substring(PACCLE, 1, 5) = '" + aCriteres.getIdEtablissement() + "JO'";
    switch (aCriteres.getTypeJournal()) {
      case Journal.TYPE_NORMAL:
        requete += " and substring(PACFIL, 31, 1) = '1'" + getFiltre(aCriteres.getFiltre());
        break;
      case Journal.TYPE_PREVISIONNEL:
        requete += " and substring(PACFIL, 31, 1) = '2'" + getFiltre(aCriteres.getFiltre());
        break;
      case Journal.TYPE_ANALYTIQUE:
        requete += " and substring(PACFIL, 31, 1) = '3'" + getFiltre(CriteresRechercheJournaux.FILTRE_BLANC);
        break;
      default:
        requete = "select PACTOP, PACFI1, PACCLE, PACFIL from " + queryManager.getLibrary() + "."
            + EnumTableBDD.PARAMETRE_COMPTABILITE + " where substring(PACCLE, 1, 5) = '" + aCriteres.getIdEtablissement()
            + "JO'" + getFiltre(aCriteres.getFiltre());
    }
    
    return requete;
  }
  
  /**
   * Retourne les conditions correspondant au filtre souhaité.
   */
  private String getFiltre(int filtre) {
    switch (filtre) {
      case CriteresRechercheJournaux.FILTRE_TRESORERIE:
        return " and (substring(PACFIL, 37, 1) = 'B' or substring(PACFIL, 37, 1) = 'C')";
      case CriteresRechercheJournaux.FILTRE_NON_TRESORERIE:
        return " and (substring(PACFIL, 37, 1) <> 'B' and substring(PACFIL, 37, 1) <> 'C')";
      case CriteresRechercheJournaux.FILTRE_BANQUE:
        return " and substring(PACFIL, 37, 1) = 'B'";
      case CriteresRechercheJournaux.FILTRE_CAISSE:
        return " and substring(PACFIL, 37, 1) = 'C'";
      case CriteresRechercheJournaux.FILTRE_BLANC:
        return " and substring(PACFIL, 37, 1) = ' '";
      default:
        return "";
    }
  }
  
  /**
   * Retourne le nombres de sociétés de la base de données.
   */
  public int getNombreSociete() {
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select PACTOP, PACFI1, PACCLE, PACFIL from " + queryManager.getLibrary() + "."
        + EnumTableBDD.PARAMETRE_COMPTABILITE + " where PACTOP <> '9' "
        + "and substring(PACCLE, 1, 3) <> '' and substring(PACCLE, 4, 2) = 'DG' and substring(paccle,  6, 5) = '     '");
    
    // Exécution de la requête
    ArrayList<GenericRecord> listeRecord = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (listeRecord == null || listeRecord.isEmpty()) {
      return 0;
    }
    return listeRecord.size();
  }
  
  /**
   * Libère les ressources.
   */
  @Override
  public void dispose() {
    queryManager = null;
  }
  
}
