/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.util.ArrayList;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;

/**
 * Suivi de commande Série N au format SFCC.
 * Cette classe sert à l'intégration JAVA vers JSON.
 * IL NE FAUT DONC PAS RENOMMER OU MODIFIER SES CHAMPS.
 */
public class JsonSuiviCommandeWebV3 extends JsonEntiteMagento {
  private String idCommande = null;
  private String statut = null;
  private ArrayList<String> numeroColis = null;
  
  /**
   * Constructeur.
   */
  public JsonSuiviCommandeWebV3(FluxMagento pRecord) {
    idFlux = pRecord.getFLIDF();
    etb = pRecord.getFLETB();
    versionFlux = pRecord.getFLVER();
    numeroColis = new ArrayList<String>();
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de construire le suivi commande sur le modèle SFCC dans l'optique d'un export typé.
   */
  public boolean mettreAJoursuiviCommande(ArrayList<GenericRecord> pListeRecord) {
    if (pListeRecord == null || pListeRecord.size() < 1) {
      majError("[JsonSuiviCommandeWebV3] mettreAJoursuiviCommande()records à NULL ou corrompu");
      return false;
    }
    
    if (pListeRecord.get(0).isPresentField("E1CCT") && pListeRecord.get(0).getField("E1CCT").toString().trim().length() > 1) {
      idCommande = pListeRecord.get(0).getField("E1CCT").toString().trim();
    }
    else {
      majError("[JsonSuiviCommandeWebV3] mettreAJoursuiviCommande() E1CCT à NULL ou corrompu ");
      return false;
    }
    
    if (pListeRecord.get(0).isPresentField("E1ETA") && pListeRecord.get(0).isPresentField("E1COD")) {
      if (pListeRecord.get(0).getField("E1COD").toString().trim().equals("9")) {
        statut = "9";
      }
      else {
        statut = pListeRecord.get(0).getField("E1ETA").toString().trim();
      }
    }
    else {
      statut = "0";
      majError("[JsonSuiviCommandeWebV3] mettreAJoursuiviCommande() E1ETA à NULL ");
      return false;
    }
    
    for (int i = 0; i < pListeRecord.size(); i++) {
      if (pListeRecord.get(i).isPresentField("COTRK")) {
        numeroColis.add(pListeRecord.get(i).getField("COTRK").toString().trim());
      }
    }
    
    return true;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant de la commande.
   */
  public String getCode() {
    return idCommande;
  }
  
  /**
   * Initialise l'identifiant de la commande.
   */
  public void setCode(String code) {
    this.idCommande = code;
  }
  
  /**
   * Retourne le statut.
   */
  public String getStatut() {
    return statut;
  }
  
  /**
   * Initialise le statut.
   */
  public void setStatut(String statut) {
    this.statut = statut;
  }
}
