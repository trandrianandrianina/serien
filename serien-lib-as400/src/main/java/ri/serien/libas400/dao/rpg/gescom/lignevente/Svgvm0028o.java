/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class Svgvm0028o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_PONLI = 4;
  public static final int DECIMAL_PONLI = 0;
  public static final int SIZE_PONLIF = 4;
  public static final int DECIMAL_PONLIF = 0;
  public static final int SIZE_POARR = 1;
  public static final int SIZE_TOTALE_DS = 19;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_PONLI = 1;
  public static final int VAR_PONLIF = 2;
  public static final int VAR_POARR = 3;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private BigDecimal ponli = BigDecimal.ZERO; // Numéro de ligne début
  private BigDecimal ponlif = BigDecimal.ZERO; // Numéro de ligne fin
  private String poarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400ZonedDecimal(SIZE_PONLI, DECIMAL_PONLI), // Numéro de ligne début
      new AS400ZonedDecimal(SIZE_PONLIF, DECIMAL_PONLIF), // Numéro de ligne fin
      new AS400Text(SIZE_POARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, ponli, ponlif, poarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    ponli = (BigDecimal) output[1];
    ponlif = (BigDecimal) output[2];
    poarr = (String) output[3];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setPonli(BigDecimal pPonli) {
    if (pPonli == null) {
      return;
    }
    ponli = pPonli.setScale(DECIMAL_PONLI, RoundingMode.HALF_UP);
  }
  
  public void setPonli(Integer pPonli) {
    if (pPonli == null) {
      return;
    }
    ponli = BigDecimal.valueOf(pPonli);
  }
  
  public Integer getPonli() {
    return ponli.intValue();
  }
  
  public void setPonlif(BigDecimal pPonlif) {
    if (pPonlif == null) {
      return;
    }
    ponlif = pPonlif.setScale(DECIMAL_PONLIF, RoundingMode.HALF_UP);
  }
  
  public void setPonlif(Integer pPonlif) {
    if (pPonlif == null) {
      return;
    }
    ponlif = BigDecimal.valueOf(pPonlif);
  }
  
  public Integer getPonlif() {
    return ponlif.intValue();
  }
  
  public void setPoarr(Character pPoarr) {
    if (pPoarr == null) {
      return;
    }
    poarr = String.valueOf(pPoarr);
  }
  
  public Character getPoarr() {
    return poarr.charAt(0);
  }
}
