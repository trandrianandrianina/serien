/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx006.v2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonStockArticleV2;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.JsonStockMagentoV2;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.outils.sql.RequeteSql;

/**
 * Cette classe traite l'exportation des stocks pour les flux V2.
 */
public class ExportationStockMagentoV2 extends BaseGroupDB {
  // Constantes
  private static int NB_ART_STOCK_MAX = 500;
  
  // Variables
  private ManagerFluxMagento gestionFlux = null;
  
  /**
   * Constructeur.
   */
  public ExportationStockMagentoV2(ManagerFluxMagento gestFlux) {
    super(gestFlux.getQueryManager());
    gestionFlux = gestFlux;
  }
  
  /**
   * Retourne l'ensemble des stocks articles au format JSON.
   * 
   * @param pRecord
   * @return
   */
  public String retournerStocksJSON(FluxMagento pRecord) {
    if (pRecord == null || pRecord.getFLETB() == null) {
      majError("[GM_Export_Stocks] retournerStocksJSON() record null ou corrompu ");
      return null;
    }
    String retour = null;
    
    List<GenericRecord> liste = null;
    // On récupère le reliquat ou on traite de A à Z
    if (pRecord.getListeReliquat() != null) {
      liste = (List<GenericRecord>) (Object) pRecord.getListeReliquat();
    }
    else {
      liste = retournerStocksMouvementsDuJour(pRecord);
    }
    
    // On créee le flux de stock
    JsonStockMagentoV2 stocks = new JsonStockMagentoV2(pRecord);
    Integer numeroInstance = ManagerFluxMagento.retournerInstanceLien(queryManager.getLibrary(), stocks.getEtb());
    if (numeroInstance == null) {
      majError("[ExportStockMagento] retournerStocksJSON() : probleme d'interpretation de l'ETb vers l'instance Magento");
      return null;
    }
    stocks.setIdInstanceMagento(numeroInstance);
    
    if (liste != null) {
      // Si des articles ont été modifiés dans cet interval
      if (liste.size() > 0) {
        if (liste.size() > NB_ART_STOCK_MAX) {
          liste = decoupageDuGrosFluxEnPlusieursPetitsFlux(pRecord, liste);
        }
        // on associe les articles
        stocks.mettreAJourLaListeArticles(liste);
        
        if (stocks.getListeStocks() != null && stocks.getListeStocks().size() > 0) {
          BigDecimal attendu = null;
          for (JsonStockArticleV2 article : stocks.getListeStocks()) {
            try {
              attendu = new BigDecimal(article.getStockAttendu().trim());
              
              if (attendu.compareTo(BigDecimal.ZERO) > 0) {
                article.setDateReassort(retournerDateReassort(pRecord.getFLETB(), article.getCodeArticle(),
                    gestionFlux.getParametresFlux().getMagasinMagento()));
              }
            }
            catch (Exception e) {
              majError(e.getMessage());
              return null;
            }
          }
        }
        // on transforme en JSON
        if (stocks.getListeStocks() != null && stocks.getListeStocks().size() > 0) {
          pRecord.setFLCOD("" + stocks.getListeStocks().size());
          if (initJSON()) {
            try {
              retour = gson.toJson(stocks);
            }
            catch (Exception e) {
              majError("[GM_Export_Stocks] retournerStocksJSON() PB  PARSE JSON ");
              return null;
            }
          }
          else {
            majError("[GM_Export_Stocks] retournerStocksJSON() PB initJSON ");
          }
        }
        else {
          majError("[GM_Export_Stocks] retournerStocksJSON() PB de getListeArticles() à NULL ou 0 ");
        }
      }
      // On a pas de modif dans ce laps de temps
      else {
        retour = "";
      }
    }
    
    return retour;
  }
  
  /**
   * Met en forme la date de SERIEN à partir d'un String.
   * 
   * @param pDateSerieN
   * @return
   */
  public static String transformerDateSerieNEnHumaine(String pDateSerieN) {
    String separateurDate = "/";
    String retour = "";
    
    if (pDateSerieN == null) {
      return retour;
    }
    
    // date pourrave du siècle dernier
    if (pDateSerieN.length() == 7) {
      // récupération du jour
      retour = pDateSerieN.substring(5, 7) + separateurDate;
      // récupération du mois
      retour += pDateSerieN.substring(3, 5) + separateurDate;
      // récupération de l'année
      retour += 1900 + Integer.parseInt(pDateSerieN.substring(0, 3));
    }
    
    return retour;
  }
  
  /**
   * Met à null les variables.
   */
  @Override
  public void dispose() {
    queryManager = null;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne la prochaine date de réassort d'un article.
   * 
   * @param pEtb
   * @param pCodeArticle
   * @param pMag
   * @return
   */
  private String retournerDateReassort(String pEtb, String pCodeArticle, String pMag) {
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter(
        "select case when LADLP <> 0 then LADLP else EADLP end as DTREASS from " + queryManager.getLibrary() + ".PGVMLBFM inner join "
            + queryManager.getLibrary() + ".PGVMEBFM on LAETB = EAETB and LACOD = EACOD and LANUM = EANUM and EASUF = LASUF where ");
    requeteSql.ajouterConditionAnd("LACOD", "=", "E");
    requeteSql.ajouterConditionAnd("LAETB", "=", pEtb);
    requeteSql.ajouterConditionAnd("LAMAG", "=", pMag);
    requeteSql.ajouterConditionAnd("LAART", "=", pCodeArticle);
    requeteSql.ajouterConditionAnd("1", "=", "(case when LACEX = 'M' and LAQTC = 0 then 0 when lacex = 'A' then 0 else 1 end)", true,
        false);
    requeteSql.ajouterConditionAnd("EAREC", "=", 0);
    requeteSql.ajouterConditionAnd("EAETA", "=", 1);
    requeteSql.ajouterConditionAnd("EAIN9", "<>", "I");
    requeteSql.ajouter(" order by DTREASS asc");
    
    ArrayList<GenericRecord> liste = queryManager.select(requeteSql.getRequeteLectureSeule());
    if (liste == null) {
      majError("[GM_Export_Stocks] retournerDateReassort() Aucune date de réassort n'a été trouvé pour l'article " + pCodeArticle);
      return null;
    }
    
    if (liste.size() > 0 && liste.get(0).isPresentField("DTREASS")) {
      return transformerDateSerieNEnHumaine(liste.get(0).getField("DTREASS").toString().trim());
    }
    
    return "";
  }
  
  /**
   * Permet d'isoler des flux de stocks trop importants en plusieurs flux plus petits.
   * Le critère étant la quantité de lignes d'articles concernés.
   * 
   * @param pFluxInitial
   * @param pListeInitiale
   * @return
   */
  private List<GenericRecord> decoupageDuGrosFluxEnPlusieursPetitsFlux(FluxMagento pFluxInitial, List<GenericRecord> pListeInitiale) {
    if (gestionFlux == null || pListeInitiale == null || pListeInitiale.size() == 0) {
      return pListeInitiale;
    }
    
    List<GenericRecord> listeAmaigrie = new ArrayList<GenericRecord>();
    List<Object> listeReliquat = new ArrayList<Object>();
    
    // On découpe le flux initial en 2 listes 1 à traiter et un reliquat
    for (int i = 0; i < pListeInitiale.size(); i++) {
      if (i < NB_ART_STOCK_MAX) {
        listeAmaigrie.add(pListeInitiale.get(i));
      }
      else {
        listeReliquat.add(pListeInitiale.get(i));
      }
    }
    
    if (listeReliquat.size() > 0) {
      gestionFlux.traiterReliquatFluxSortant(pFluxInitial, listeReliquat);
    }
    
    return listeAmaigrie;
  }
  
  /**
   * Permet de retourner tous les articles ainsi que leur stock, qui ont subi des mouvements à la date du jour.
   * 
   * @param pRecord
   * @return
   */
  private ArrayList<GenericRecord> retournerStocksMouvementsDuJour(FluxMagento pRecord) {
    if (pRecord == null || pRecord.getFLETB() == null) {
      majError("[GM_Export_Stocks] retournerStocksMouvementsDuJour() record null ou corrompu ");
      return null;
    }
    
    // CONTROLE DU DERNIER SUCCES ENVOI FLUX DES STOCKS
    ArrayList<GenericRecord> liste = null;
    String dateDernierEnvoi = "0";
    String heureDernierEnvoi = "0";
    String requete = "" + " SELECT  VARCHAR_FORMAT(MAX(FLTRT) ,'YYMMDD') AS MADATE, VARCHAR_FORMAT(MAX(FLTRT) ,'HH24MISS') AS MONHEURE "
        + " FROM " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX + " " + " WHERE FLETB = '" + pRecord.getFLETB()
        + "' AND FLIDF = '" + FluxMagento.IDF_FLX006_STOCK + "' " + " AND FLSTT = '" + EnumStatutFlux.TRAITE.getNumero()
        + "' AND FLERR = '" + FluxMagento.ERR_PAS_ERREUR + "' ";
    
    liste = queryManager.select(requete);
    if (liste != null) {
      // Date
      if (liste.size() > 0 && liste.get(0).isPresentField("MADATE")) {
        dateDernierEnvoi = "1" + liste.get(0).getField("MADATE").toString().trim();
      }
      else {
        dateDernierEnvoi = "0";
      }
      // HEure
      if (liste.size() > 0 && liste.get(0).isPresentField("MONHEURE")) {
        heureDernierEnvoi = liste.get(0).getField("MONHEURE").toString().trim();
      }
      else {
        heureDernierEnvoi = "0";
      }
    }
    else {
      majError("[GM_Export_Stocks] retournerStocksMouvementsDuJour() LISTE 1 NULL " + queryManager.getMsgError() + " / " + requete);
      return null;
    }
    
    // RECUP DES STOCKS
    liste = null;
    requete = "" + " SELECT S1ETB,S1ART,(S1STD+ S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM+ S1QES "
        + " + S1QSS + S1QDS- S1RES) AS STK, S1ATT, S1DTMJ, S1HRMJ " + " FROM " + queryManager.getLibrary() + ".PGVMSTKM " + " LEFT JOIN "
        + queryManager.getLibrary() + ".PGVMARTM ON S1ETB = A1ETB AND S1ART = A1ART " + " WHERE S1ETB ='" + pRecord.getFLETB()
        + "' AND S1MAG = '" + gestionFlux.getParametresFlux().getMagasinMagento() + "' " + " AND ( S1DTMJ > '" + dateDernierEnvoi + "' "
        + " OR  (S1DTMJ = '" + dateDernierEnvoi + "' AND S1HRMJ >= (" + heureDernierEnvoi + "-300) ))" // On laisse 5mn de rab
        + " AND S1DTMJ <> '0' AND A1IN15 <> '1' " + " ORDER BY S1DTMJ,S1HRMJ ";
    
    liste = queryManager.select(requete);
    
    if (liste == null) {
      majError("[GM_Export_Stocks] retournerStocksMouvementsDuJour() LISTE 2 NULL " + queryManager.getMsgError() + " / " + requete);
    }
    
    return liste;
  }
  
}
