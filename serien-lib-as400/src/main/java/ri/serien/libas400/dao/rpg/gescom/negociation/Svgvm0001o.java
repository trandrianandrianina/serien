/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.negociation;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0001o extends ProgramParameter {
  // Constantes
  public static final int SIZE_POIND = 10;
  public static final int SIZE_T1TCD = 1;
  public static final int SIZE_T1VAL = 9;
  public static final int DECIMAL_T1VAL = 2;
  public static final int SIZE_T1REM1 = 4;
  public static final int DECIMAL_T1REM1 = 2;
  public static final int SIZE_T1REM2 = 4;
  public static final int DECIMAL_T1REM2 = 2;
  public static final int SIZE_T1REM3 = 4;
  public static final int DECIMAL_T1REM3 = 2;
  public static final int SIZE_T1REM4 = 4;
  public static final int DECIMAL_T1REM4 = 2;
  public static final int SIZE_T1REM5 = 4;
  public static final int DECIMAL_T1REM5 = 2;
  public static final int SIZE_T1REM6 = 4;
  public static final int DECIMAL_T1REM6 = 2;
  public static final int SIZE_T1COE = 5;
  public static final int DECIMAL_T1COE = 4;
  public static final int SIZE_T1DTD = 7;
  public static final int DECIMAL_T1DTD = 0;
  public static final int SIZE_T1DTF = 7;
  public static final int DECIMAL_T1DTF = 0;
  public static final int SIZE_T1FPR = 5;
  public static final int SIZE_TOTALE_DS = 68;
  
  // Constantes indices Nom DS
  public static final int VAR_POIND = 0;
  public static final int VAR_T1TCD = 1;
  public static final int VAR_T1VAL = 2;
  public static final int VAR_T1REM1 = 3;
  public static final int VAR_T1REM2 = 4;
  public static final int VAR_T1REM3 = 5;
  public static final int VAR_T1REM4 = 6;
  public static final int VAR_T1REM5 = 7;
  public static final int VAR_T1REM6 = 8;
  public static final int VAR_T1COE = 9;
  public static final int VAR_T1DTD = 10;
  public static final int VAR_T1DTF = 11;
  public static final int VAR_T1FPR = 12;
  
  // Variables AS400
  private String poind = ""; // Indicateurs
  private String t1tcd = ""; // Type de condition
  private BigDecimal t1val = BigDecimal.ZERO; // Valeur en EUROS
  private BigDecimal t1rem1 = BigDecimal.ZERO; // Remise 1
  private BigDecimal t1rem2 = BigDecimal.ZERO; // Remise 2
  private BigDecimal t1rem3 = BigDecimal.ZERO; // Remise 3
  private BigDecimal t1rem4 = BigDecimal.ZERO; // Remise 4
  private BigDecimal t1rem5 = BigDecimal.ZERO; // Remise 5
  private BigDecimal t1rem6 = BigDecimal.ZERO; // Remise 6
  private BigDecimal t1coe = BigDecimal.ZERO; // Coefficient < ou > à 1
  private BigDecimal t1dtd = BigDecimal.ZERO; // Date de début de validité
  private BigDecimal t1dtf = BigDecimal.ZERO; // Date de fin de validité
  private String t1fpr = ""; // Formule de prix
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_POIND), // Indicateurs
      new AS400Text(SIZE_T1TCD), // Type de condition
      new AS400ZonedDecimal(SIZE_T1VAL, DECIMAL_T1VAL), // Valeur en EUROS
      new AS400ZonedDecimal(SIZE_T1REM1, DECIMAL_T1REM1), // Remise 1
      new AS400ZonedDecimal(SIZE_T1REM2, DECIMAL_T1REM2), // Remise 2
      new AS400ZonedDecimal(SIZE_T1REM3, DECIMAL_T1REM3), // Remise 3
      new AS400ZonedDecimal(SIZE_T1REM4, DECIMAL_T1REM4), // Remise 4
      new AS400ZonedDecimal(SIZE_T1REM5, DECIMAL_T1REM5), // Remise 5
      new AS400ZonedDecimal(SIZE_T1REM6, DECIMAL_T1REM6), // Remise 6
      new AS400ZonedDecimal(SIZE_T1COE, DECIMAL_T1COE), // Coefficient < ou > à 1
      new AS400ZonedDecimal(SIZE_T1DTD, DECIMAL_T1DTD), // Date de début de validité
      new AS400ZonedDecimal(SIZE_T1DTF, DECIMAL_T1DTF), // Date de fin de validité
      new AS400Text(SIZE_T1FPR), // Formule de prix
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /*
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { poind, t1tcd, t1val, t1rem1, t1rem2, t1rem3, t1rem4, t1rem5, t1rem6, t1coe, t1dtd, t1dtf, t1fpr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /*
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    poind = (String) output[0];
    t1tcd = (String) output[1];
    t1val = (BigDecimal) output[2];
    t1rem1 = (BigDecimal) output[3];
    t1rem2 = (BigDecimal) output[4];
    t1rem3 = (BigDecimal) output[5];
    t1rem4 = (BigDecimal) output[6];
    t1rem5 = (BigDecimal) output[7];
    t1rem6 = (BigDecimal) output[8];
    t1coe = (BigDecimal) output[9];
    t1dtd = (BigDecimal) output[10];
    t1dtf = (BigDecimal) output[11];
    t1fpr = (String) output[12];
  }
  
  // -- Accesseurs
  
  public void setPoind(String pPoind) {
    if (pPoind == null) {
      return;
    }
    poind = pPoind;
  }
  
  public String getPoind() {
    return poind;
  }
  
  public void setT1tcd(Character pT1tcd) {
    if (pT1tcd == null) {
      return;
    }
    t1tcd = String.valueOf(pT1tcd);
  }
  
  public Character getT1tcd() {
    return t1tcd.charAt(0);
  }
  
  public void setT1val(BigDecimal pT1val) {
    if (pT1val == null) {
      return;
    }
    t1val = pT1val.setScale(DECIMAL_T1VAL, RoundingMode.HALF_UP);
  }
  
  public void setT1val(Double pT1val) {
    if (pT1val == null) {
      return;
    }
    t1val = BigDecimal.valueOf(pT1val).setScale(DECIMAL_T1VAL, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1val() {
    return t1val.setScale(DECIMAL_T1VAL, RoundingMode.HALF_UP);
  }
  
  public void setT1rem1(BigDecimal pT1rem1) {
    if (pT1rem1 == null) {
      return;
    }
    t1rem1 = pT1rem1.setScale(DECIMAL_T1REM1, RoundingMode.HALF_UP);
  }
  
  public void setT1rem1(Double pT1rem1) {
    if (pT1rem1 == null) {
      return;
    }
    t1rem1 = BigDecimal.valueOf(pT1rem1).setScale(DECIMAL_T1REM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1rem1() {
    return t1rem1.setScale(DECIMAL_T1REM1, RoundingMode.HALF_UP);
  }
  
  public void setT1rem2(BigDecimal pT1rem2) {
    if (pT1rem2 == null) {
      return;
    }
    t1rem2 = pT1rem2.setScale(DECIMAL_T1REM2, RoundingMode.HALF_UP);
  }
  
  public void setT1rem2(Double pT1rem2) {
    if (pT1rem2 == null) {
      return;
    }
    t1rem2 = BigDecimal.valueOf(pT1rem2).setScale(DECIMAL_T1REM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1rem2() {
    return t1rem2.setScale(DECIMAL_T1REM2, RoundingMode.HALF_UP);
  }
  
  public void setT1rem3(BigDecimal pT1rem3) {
    if (pT1rem3 == null) {
      return;
    }
    t1rem3 = pT1rem3.setScale(DECIMAL_T1REM3, RoundingMode.HALF_UP);
  }
  
  public void setT1rem3(Double pT1rem3) {
    if (pT1rem3 == null) {
      return;
    }
    t1rem3 = BigDecimal.valueOf(pT1rem3).setScale(DECIMAL_T1REM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1rem3() {
    return t1rem3.setScale(DECIMAL_T1REM3, RoundingMode.HALF_UP);
  }
  
  public void setT1rem4(BigDecimal pT1rem4) {
    if (pT1rem4 == null) {
      return;
    }
    t1rem4 = pT1rem4.setScale(DECIMAL_T1REM4, RoundingMode.HALF_UP);
  }
  
  public void setT1rem4(Double pT1rem4) {
    if (pT1rem4 == null) {
      return;
    }
    t1rem4 = BigDecimal.valueOf(pT1rem4).setScale(DECIMAL_T1REM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1rem4() {
    return t1rem4.setScale(DECIMAL_T1REM4, RoundingMode.HALF_UP);
  }
  
  public void setT1rem5(BigDecimal pT1rem5) {
    if (pT1rem5 == null) {
      return;
    }
    t1rem5 = pT1rem5.setScale(DECIMAL_T1REM5, RoundingMode.HALF_UP);
  }
  
  public void setT1rem5(Double pT1rem5) {
    if (pT1rem5 == null) {
      return;
    }
    t1rem5 = BigDecimal.valueOf(pT1rem5).setScale(DECIMAL_T1REM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1rem5() {
    return t1rem5.setScale(DECIMAL_T1REM5, RoundingMode.HALF_UP);
  }
  
  public void setT1rem6(BigDecimal pT1rem6) {
    if (pT1rem6 == null) {
      return;
    }
    t1rem6 = pT1rem6.setScale(DECIMAL_T1REM6, RoundingMode.HALF_UP);
  }
  
  public void setT1rem6(Double pT1rem6) {
    if (pT1rem6 == null) {
      return;
    }
    t1rem6 = BigDecimal.valueOf(pT1rem6).setScale(DECIMAL_T1REM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1rem6() {
    return t1rem6.setScale(DECIMAL_T1REM6, RoundingMode.HALF_UP);
  }
  
  public void setT1coe(BigDecimal pT1coe) {
    if (pT1coe == null) {
      return;
    }
    t1coe = pT1coe.setScale(DECIMAL_T1COE, RoundingMode.HALF_UP);
  }
  
  public void setT1coe(Double pT1coe) {
    if (pT1coe == null) {
      return;
    }
    t1coe = BigDecimal.valueOf(pT1coe).setScale(DECIMAL_T1COE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getT1coe() {
    return t1coe.setScale(DECIMAL_T1COE, RoundingMode.HALF_UP);
  }
  
  public void setT1dtd(BigDecimal pT1dtd) {
    if (pT1dtd == null) {
      return;
    }
    t1dtd = pT1dtd.setScale(DECIMAL_T1DTD, RoundingMode.HALF_UP);
  }
  
  public void setT1dtd(Integer pT1dtd) {
    if (pT1dtd == null) {
      return;
    }
    t1dtd = BigDecimal.valueOf(pT1dtd);
  }
  
  public void setT1dtd(Date pT1dtd) {
    if (pT1dtd == null) {
      return;
    }
    t1dtd = BigDecimal.valueOf(ConvertDate.dateToDb2(pT1dtd));
  }
  
  public Integer getT1dtd() {
    return t1dtd.intValue();
  }
  
  public Date getT1dtdConvertiEnDate() {
    return ConvertDate.db2ToDate(t1dtd.intValue(), null);
  }
  
  public void setT1dtf(BigDecimal pT1dtf) {
    if (pT1dtf == null) {
      return;
    }
    t1dtf = pT1dtf.setScale(DECIMAL_T1DTF, RoundingMode.HALF_UP);
  }
  
  public void setT1dtf(Integer pT1dtf) {
    if (pT1dtf == null) {
      return;
    }
    t1dtf = BigDecimal.valueOf(pT1dtf);
  }
  
  public void setT1dtf(Date pT1dtf) {
    if (pT1dtf == null) {
      return;
    }
    t1dtf = BigDecimal.valueOf(ConvertDate.dateToDb2(pT1dtf));
  }
  
  public Integer getT1dtf() {
    return t1dtf.intValue();
  }
  
  public Date getT1dtfConvertiEnDate() {
    return ConvertDate.db2ToDate(t1dtf.intValue(), null);
  }
  
  public void setT1fpr(String pT1fpr) {
    if (pT1fpr == null) {
      return;
    }
    t1fpr = pT1fpr;
  }
  
  public String getT1fpr() {
    return t1fpr;
  }
}
