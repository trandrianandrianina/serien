/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2;

import java.util.ArrayList;

public class BlocCommentairesV2 {
  // Constantes
  public static final String CRLF = "\n";
  
  // Variables
  private int nbrlignesmax = 0;
  private int taillemaxligne = 0;
  
  private final int NB_MAX_LIGNES = 4;
  private final int NB_MAX_CAR_PAR_ZONE = 30;
  private final int NB_MAX_CARACTERES = 120;
  
  private String commentaire = null;
  private ArrayList<String> lignes = new ArrayList<String>();
  
  /**
   * Constructeur
   * @param _nbrlignesmax
   * @param _taillemaxligne
   * @param _taillemaxzones
   */
  public BlocCommentairesV2(String commentaire) {
    setCommentaire(commentaire);
    nbrlignesmax = NB_MAX_LIGNES;
    taillemaxligne = NB_MAX_CAR_PAR_ZONE;
  }
  
  /**
   *
   * @return
   */
  public ArrayList<String> getFormateCommentaire() {
    if ((commentaire == null) || commentaire.equals("")) {
      return null;
    }
    
    if (!analyseCommentaire()) {
      // Si ça passe pas, on traite de manière bourrine : on concatène tout et on découpe à la taille max de chaque ligne
      StringBuilder sb = new StringBuilder();
      for (String ligne : lignes) {
        sb.append(ligne).append(' ');
      }
      sb.deleteCharAt(sb.length() - 1); // On supprime le dernier espace ajouter par la boucle
      lignes.clear();
      lignes = getZones(sb.toString(), taillemaxligne);
    }
    
    return lignes;
  }
  
  /**
   * Découpe la chaine en X chaines de longueur donnée
   * @param ligne
   * @param longueurZone
   * @return
   */
  public ArrayList<String> getZones(String chaine, int longueurZone) {
    int deb = 0;
    int fin = longueurZone;
    ArrayList<String> liste = new ArrayList<String>();
    do {
      if (fin < chaine.length()) {
        liste.add(chaine.substring(deb, fin));
        deb = fin;
        fin += longueurZone;
      }
      else {
        liste.add(chaine.substring(deb));
        deb += longueurZone;
      }
    }
    while (deb < chaine.length());
    
    return liste;
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Analyse le bloc commentaire
   */
  private boolean analyseCommentaire() {
    boolean capasse = true;
    String[] lignes_origines = commentaire.split(CRLF);
    
    // On s'occupe d'éliminer les lignes vides si le nombre de lignes est supérieur à NOMBRE_MAX_LIGNES_COMMENTAIRES
    // et on contrôle la longueur des lignes
    if (lignes_origines.length <= nbrlignesmax) {
      for (String ligne : lignes_origines) {
        lignes.add(ligne.trim());
        if (ligne.trim().length() > taillemaxligne) {
          capasse = false;
        }
      }
    }
    else {
      // On élimine autant que possible les lignes vides
      for (String ligne : lignes_origines) {
        if (!ligne.trim().equals("")) {
          lignes.add(ligne.trim());
          if (ligne.trim().length() > taillemaxligne) {
            capasse = false;
          }
        }
      }
      // On contrôle le nombre de lignes après le nettoyage
      if (lignes.size() > nbrlignesmax) {
        capasse = false;
      }
    }
    return capasse;
  }
  
  // -- Accesseurs ----------------------------------------------------------
  
  /**
   * @return le commentaire
   */
  public String getCommentaire() {
    return commentaire;
  }
  
  /**
   * @param commentaire le commentaire à définir
   */
  public void setCommentaire(String commentaire) {
    this.commentaire = commentaire;
  }
}
