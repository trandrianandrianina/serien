/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.standard;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Classe qui encapsule le programme RPG SVGAM0002.
 */
public class RpgChargerListeArticlesAchat extends RpgBase {
  // Constantes
  private static final String PROGRAMME = "SVGAM0002";
  private static final int NOMBRE_MAX_ARTICLE = 65;
  
  /**
   * Utilise un programme RPG pour lire les données d'un article.
   */
  public Article lireArticle(SystemeManager pSysteme, IdArticle idArticle, ParametresLireArticle pParametres) {
    new ArrayList<Article>();
    return null;
  }
  
  /**
   * Utilise un programme RPG pour lire les données d'une liste d'articles.
   * Le programme RPG peut traiter 65 articles à la fois, il est appelé en boucle pour l'intégralité des articles à lire.
   */
  public ListeArticle chargerListeInformationAchat(SystemeManager pSysteme, List<IdArticle> pListeIdArticles,
      ParametresLireArticle pParametres) {
    ListeArticle listeArticles = new ListeArticle();
    int indexDepart = 0;
    while (indexDepart < pListeIdArticles.size()) {
      int indexFin = Math.min(indexDepart + NOMBRE_MAX_ARTICLE - 1, pListeIdArticles.size() - 1);
      lireListe65Articles(pSysteme, listeArticles, pListeIdArticles, indexDepart, indexFin, pParametres);
      indexDepart = indexFin + 1;
    }
    return listeArticles;
  }
  
  /**
   * Appeler le programme RPG qui peut lire les données de 65 articles.
   */
  private void lireListe65Articles(SystemeManager pSysteme, ListeArticle pListeArticles, List<IdArticle> pListeIdArticles,
      int indexDepart, int indexFin, ParametresLireArticle pParametres) {
    if (pSysteme == null || pListeIdArticles == null || pParametres == null || pListeArticles == null) {
      throw new MessageErreurException("Erreur lors de la lecture des données des articles.");
    }
    if (indexFin - indexDepart + 1 > NOMBRE_MAX_ARTICLE) {
      throw new MessageErreurException("Il n'est pas possible de lire plus de " + NOMBRE_MAX_ARTICLE + " articles à la fois.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgam0002i entree = new Svgam0002i();
    Svgam0002o sortie = new Svgam0002o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pParametres.getIdEtablissement().getCodeEtablissement());
    if (pParametres.getIdFournisseur() != null) {
      entree.setPicol(pParametres.getIdFournisseur().getCollectif());
      entree.setPifrs(pParametres.getIdFournisseur().getNumero());
    }
    entree.setPimag(pParametres.getIdMagasin().getCode());
    
    int index = indexDepart;
    entree.setPia01(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia02(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia03(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia04(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia05(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia06(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia07(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia08(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia09(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia10(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia11(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia12(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia13(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia14(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia15(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia16(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia17(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia18(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia19(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia20(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia21(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia22(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia23(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia24(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia25(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia26(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia27(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia28(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia29(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia30(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia31(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia32(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia33(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia34(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia35(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia36(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia37(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia38(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia39(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia40(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia41(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia42(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia43(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia44(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia45(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia46(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia47(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia48(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia49(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia50(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia51(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia52(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia53(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia54(alimenterCodeArticle(pListeIdArticles, index++));
    entree.setPia55(alimenterCodeArticle(pListeIdArticles, index++));
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécuter le programme RPG
    if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGamas(), parameterList)) {
      // Récupération et conversion des paramètres du RPG
      entree.setDSOutput();
      sortie.setDSOutput();
      erreur.setDSOutput();
      
      // Gestion des erreurs
      controlerErreur();
      
      // Renseigner le résultat
      for (Object ligne : sortie.getValeursBrutesLignes()) {
        if (ligne == null) {
          break;
        }
        pListeArticles.add(completerArticle((Object[]) ligne, pParametres.getIdFournisseur()));
      }
    }
  }
  
  /**
   * Méthode interne pour alimenter facilement les 65 champs comportant le code article.
   */
  private String alimenterCodeArticle(List<IdArticle> pListeIdArticles, int index) {
    if (index < pListeIdArticles.size()) {
      return pListeIdArticles.get(index).getCodeArticle();
    }
    else {
      return "";
    }
  }
  
  /**
   * Créer un article à partir des données d'une ligne renvoyée par le programme RPG.
   */
  private Article completerArticle(Object[] line, IdFournisseur pIdFournisseur) {
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) line[Svgam0002d.VAR_WETB]);
    Article article = new Article(IdArticle.getInstance(idEtablissement, (String) line[Svgam0002d.VAR_WART]));
    article.setArticleCharge(true);
    article.setIdFournisseur(pIdFournisseur);
    article.setLibelle1((String) line[Svgam0002d.VAR_WLIB]);
    article.setLibelle2((String) line[Svgam0002d.VAR_WLB1]);
    article.setLibelle3((String) line[Svgam0002d.VAR_WLB2]);
    article.setLibelle4((String) line[Svgam0002d.VAR_WLB3]);
    article.setObservationCourte((String) line[Svgam0002d.VAR_WOBS]);
    article.setQuantitePhysique(((BigDecimal) line[Svgam0002d.VAR_WSTK]));
    article.setQuantiteReserveeUCA(((BigDecimal) line[Svgam0002d.VAR_WRES]));
    article.setQuantiteAttendueUCA(((BigDecimal) line[Svgam0002d.VAR_WATT]));
    article.setQuantiteDisponibleVente(((BigDecimal) line[Svgam0002d.VAR_WDIV]));
    article.setQuantiteDisponibleAchat(((BigDecimal) line[Svgam0002d.VAR_WDIA]));
    article.setQuantiteStockMinimumUCA(((BigDecimal) line[Svgam0002d.VAR_WMIN]));
    article.setQuantiteStockMaximumUCA(((BigDecimal) line[Svgam0002d.VAR_WMAX]));
    article.setQuantiteStockIdealUCA(((BigDecimal) line[Svgam0002d.VAR_WIDEAL]));
    article.setConsommationMoyenneUCA(((BigDecimal) line[Svgam0002d.VAR_WCONSM]));
    article.setAlerteConditionnementFournisseur(((String) line[Svgam0002d.VAR_WIN5]).trim().equals("1"));
    article.setQuantiteConditionnementFournisseurUS(((BigDecimal) line[Svgam0002d.VAR_WNUA]));
    
    // L'unité de stockage
    if (!((String) line[Svgam0002d.VAR_WUS]).trim().isEmpty()) {
      IdUnite idUS = IdUnite.getInstance((String) line[Svgam0002d.VAR_WUS]);
      article.setIdUS(idUS);
    }
    // L'unité de conditionnement de stockage
    if (!((String) line[Svgam0002d.VAR_WUCS]).trim().isEmpty()) {
      IdUnite idUCS = IdUnite.getInstance((String) line[Svgam0002d.VAR_WUCS]);
      article.setIdUCS(idUCS);
    }
    
    // On initialise les données du prix d'achat de l'article
    ParametrePrixAchat parametre = new ParametrePrixAchat();
    
    // Renseigner les unités
    if (!((String) line[Svgam0002d.VAR_WUNA]).trim().isEmpty()) {
      IdUnite idUA = IdUnite.getInstance((String) line[Svgam0002d.VAR_WUNA]);
      parametre.setIdUA(idUA);
    }
    if (!((String) line[Svgam0002d.VAR_WUCA]).trim().isEmpty()) {
      IdUnite idUCA = IdUnite.getInstance((String) line[Svgam0002d.VAR_WUCA]);
      parametre.setIdUCA(idUCA);
    }
    parametre.setNombreDecimaleUA(((BigDecimal) line[Svgam0002d.VAR_WDCA]).intValue());
    parametre.setNombreDecimaleUCA(((BigDecimal) line[Svgam0002d.VAR_WDCC]).intValue());
    parametre.setNombreDecimaleUS(((BigDecimal) line[Svgam0002d.VAR_WDCS]).intValue());
    article.majQuantiteStock(parametre);
    
    // Renseigner les ratios
    parametre.setNombreUAParUCA(((BigDecimal) line[Svgam0002d.VAR_WKUAUCA]));
    parametre.setNombreUSParUCA(((BigDecimal) line[Svgam0002d.VAR_WKUSUCA]));
    parametre.setNombreUCAParUCS(((BigDecimal) line[Svgam0002d.VAR_WKUCAUCS]));
    parametre.setNombreUSParUCS(((BigDecimal) line[Svgam0002d.VAR_WKUSUCS]));
    
    // Renseigner les montants
    parametre.setPrixAchatBrutHT(((BigDecimal) line[Svgam0002d.VAR_WPRA]));
    parametre.setPourcentageTaxeOuMajoration(((BigDecimal) line[Svgam0002d.VAR_WFK3]));
    parametre.setMontantConditionnement(((BigDecimal) line[Svgam0002d.VAR_WFV3]));
    parametre.setPrixNetHT(((BigDecimal) line[Svgam0002d.VAR_WNET]));
    parametre.setMontantAuPoidsPortHT(((BigDecimal) line[Svgam0002d.VAR_WFV1]));
    parametre.setPoidsPort(((BigDecimal) line[Svgam0002d.VAR_WFK1]));
    parametre.setPourcentagePort(new BigPercentage((BigDecimal) line[Svgam0002d.VAR_WFP1], EnumFormatPourcentage.POURCENTAGE));
    parametre.setPrixRevientFournisseurHT(((BigDecimal) line[Svgam0002d.VAR_WNETP]));
    parametre.setPourcentageMajoration(new BigPercentage((BigDecimal) line[Svgam0002d.VAR_WFP2], EnumFormatPourcentage.POURCENTAGE));
    parametre
        .setPrixRevientStandardHT(((BigDecimal) line[Svgam0002d.VAR_WPRSA]).setScale(Svgam0002d.DECIMAL_WPRSA, RoundingMode.HALF_UP));
    
    // Initialiser le prix d'achat de l'article
    if (article.getPrixAchat() == null) {
      article.setPrixAchat(new PrixAchat());
    }
    article.getPrixAchat().initialiser(parametre);
    
    return article;
  }
}
