/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.stocks;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.document.CritereAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.EnumAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class RpgStocksAttenduCommande extends RpgBase {
  // Constantes
  protected static final String PROGRAMME = "SVGVX0006";
  
  /**
   * Appel du programme RPG qui va lire les lignes d'Attendu/Commandé.
   */
  public List<LigneAttenduCommande> chargerListeLigneAttenduCommande(SystemeManager pSysteme,
      CritereAttenduCommande pCritereAttenduCommande) {
    if (pSysteme == null) {
      throw new MessageErreurException("Le paramètre pSysteme du service est à null.");
    }
    if (pCritereAttenduCommande == null) {
      throw new MessageErreurException("Il n'y a pas de critères de recherche.");
    }
    IdEtablissement.controlerId(pCritereAttenduCommande.getIdEtablissement(), true);
    IdArticle.controlerId(pCritereAttenduCommande.getIdArticle(), true);
    
    // Préparation des paramètres du programme RPG
    Svgvx0006i entree = new Svgvx0006i();
    Svgvx0006o sortie = new Svgvx0006o();
    ProgramParameter[] parameterList = { entree, // Paramètres d'entrée
        sortie, // Paramètres de sortie
        erreur // Paramètres d'erreur
    };
    
    // Initialisation des paramètres d'entrée
    char[] tabInd = { ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ' };
    if (pCritereAttenduCommande.getTypefiltrage() != null) {
      tabInd[0] = pCritereAttenduCommande.getTypefiltrage().getConvertirCodeEnCaractere();
      entree.setPiind(new String(tabInd));
    }
    entree.setPietb(pCritereAttenduCommande.getIdEtablissement().getCodeEtablissement());
    entree.setPiart(pCritereAttenduCommande.getIdArticle().getCodeArticle());
    if (pCritereAttenduCommande.getIdMagasin() != null) {
      entree.setPimag(pCritereAttenduCommande.getIdMagasin().getCode());
    }
    
    // Préparation de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // On force une seule page pour l'instant car le service retourne des données bizarres
    boolean charger = true;
    List<LigneAttenduCommande> stocks = new ArrayList<LigneAttenduCommande>();
    int page = 1;
    do {
      int nombreLignes = 15;
      entree.setPilig(nombreLignes);
      entree.setPipag(page);
      
      // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
      entree.setDSInput();
      sortie.setDSInput();
      erreur.setDSInput();
      
      // Exécution du programme RPG
      if (rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
        // Récupération et conversion des paramètres du RPG
        entree.setDSOutput();
        sortie.setDSOutput();
        erreur.setDSOutput();
        
        // Gestion des erreurs
        controlerErreur();
        
        // Initialisation de la classe métier
        Object[] lignes = sortie.getValeursBrutesLignes();
        for (int i = 0; i < lignes.length; i++) {
          if (lignes[i] == null) {
            continue;
          }
          LigneAttenduCommande ligneAttenduCommande = traiterUneLigneAttenduCommande((Object[]) lignes[i], pCritereAttenduCommande);
          if (ligneAttenduCommande != null) {
            stocks.add(ligneAttenduCommande);
            nombreLignes--;
          }
        }
      }
      // Controle qui définit s'il va y avoir un autre appel du programme RPG
      if (nombreLignes > 0) {
        charger = false;
      }
      page++;
    }
    while (charger);
    return stocks;
  }
  
  /**
   * Découpage d'une ligne attendu/commande
   */
  private LigneAttenduCommande traiterUneLigneAttenduCommande(Object[] pLigne, CritereAttenduCommande pCritereAttenduCommande) {
    if (pLigne == null) {
      return null;
    }
    // "Affine" le test de fin de liste
    int typeFiltrage = ((BigDecimal) pLigne[Svgvx0006d.VAR_WTYP]).intValue();
    if (typeFiltrage == 0) {
      return null;
    }
    
    LigneAttenduCommande ligneStock = new LigneAttenduCommande();
    
    // Initialisation des données communes
    ligneStock.setTypefiltrage(EnumAttenduCommande.valueOfByCode(typeFiltrage));
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) pLigne[Svgvx0006d.VAR_WETB]);
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, (String) pLigne[Svgvx0006d.VAR_WART]);
    ligneStock.setIdArticle(idArticle);
    ligneStock.setLibelleArticle(((String) pLigne[Svgvx0006d.VAR_WLIB]).trim());
    if (!((String) pLigne[Svgvx0006d.VAR_WMAG]).trim().isEmpty()) {
      ligneStock.setIdMagasin(IdMagasin.getInstance(idEtablissement, (String) pLigne[Svgvx0006d.VAR_WMAG]));
    }
    ligneStock.setCodeUniteStock(((String) pLigne[Svgvx0006d.VAR_WUNS]).trim());
    ligneStock.setNombreDecimaleUS(((BigDecimal) pLigne[Svgvx0006d.VAR_WDCS]).intValue());
    ligneStock.setQuantiteAttendueCommandeeUS((BigDecimal) pLigne[Svgvx0006d.VAR_WQTEUS]);
    ligneStock.setNomTiersMouvement(((String) pLigne[Svgvx0006d.VAR_WNOM]).trim());
    ligneStock.setDateValidite(ConvertDate.db2ToDate(((BigDecimal) pLigne[Svgvx0006d.VAR_WHOM]).intValue(), null));
    ligneStock.setDateLivraisonPrevue(ConvertDate.db2ToDate(((BigDecimal) pLigne[Svgvx0006d.VAR_WDLP]).intValue(), null));
    ligneStock.setDateLivraisonSouhaitee(ConvertDate.db2ToDate(((BigDecimal) pLigne[Svgvx0006d.VAR_WDLS]).intValue(), null));
    ligneStock.setCodeEtatDocumentLien(((BigDecimal) pLigne[Svgvx0006d.VAR_ALAETA]).intValue());
    ligneStock.setQuantiteAffecteeUS((BigDecimal) pLigne[Svgvx0006d.VAR_ALAQTE]);
    BigDecimal stockGlisse = (BigDecimal) pLigne[Svgvx0006d.VAR_WQTSTK];
    ligneStock.setQuantiteStockGlisse(stockGlisse);
    ligneStock.setNomDocumentLien(((String) pLigne[Svgvx0006d.VAR_ALANOM]).trim());
    
    // Initialisation en fonction de l'origine de la ligne
    ligneStock.setOrigineLigne(((String) pLigne[Svgvx0006d.VAR_WORI]).charAt(0));
    switch (ligneStock.getOrigineLigne()) {
      case LigneAttenduCommande.ORIGINE_LIGNE_VENTE:
        // Construction de l'id du document de vente
        EnumCodeEnteteDocumentVente codeEntete =
            EnumCodeEnteteDocumentVente.valueOfByCode(((String) pLigne[Svgvx0006d.VAR_WCOD]).charAt(0));
        Integer numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_WNUM]).intValue();
        Integer suffixe = ((BigDecimal) pLigne[Svgvx0006d.VAR_WSUF]).intValue();
        Integer numeroLigne = ((BigDecimal) pLigne[Svgvx0006d.VAR_WNLI]).intValue();
        IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
        // Construction de l'id du client
        Integer numeroclient = ((BigDecimal) pLigne[Svgvx0006d.VAR_WCLI]).intValue();
        Integer suffixeclient = ((BigDecimal) pLigne[Svgvx0006d.VAR_WLIV]).intValue();
        IdClient idClient = IdClient.getInstance(idEtablissement, numeroclient, suffixeclient);
        // Autres variables
        Date dateReservation = ConvertDate.db2ToDate(((BigDecimal) pLigne[Svgvx0006d.VAR_WDRES]).intValue(), null);
        ligneStock.initialiserLigneVenteOrigine(idLigneVente, idClient, dateReservation);
        ligneStock.setNomClientFacture(((String) pLigne[Svgvx0006d.VAR_WNOMFAC]).trim());
        break;
      case LigneAttenduCommande.ORIGINE_LIGNE_ACHAT:
        // Construction de l'id du document d'achat
        EnumCodeEnteteDocumentAchat codeEnteteDocumentAchat =
            EnumCodeEnteteDocumentAchat.valueOfByCode(((String) pLigne[Svgvx0006d.VAR_WCOD]).charAt(0));
        numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_WNUM]).intValue();
        suffixe = ((BigDecimal) pLigne[Svgvx0006d.VAR_WSUF]).intValue();
        IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(idEtablissement, codeEnteteDocumentAchat, numero, suffixe);
        ligneStock.setIdDocumentAchat(idDocumentAchat);
        IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(idDocumentAchat, ((BigDecimal) pLigne[Svgvx0006d.VAR_WNLI]).intValue());
        // Construction de l'id du fournisseur
        int collectif = ((BigDecimal) pLigne[Svgvx0006d.VAR_WCOF]).intValue();
        numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_WFRS]).intValue();
        IdFournisseur idFournisseur = IdFournisseur.getInstance(idEtablissement, collectif, numero);
        // Autres variables
        int adresseCommandeFournisseur = ((BigDecimal) pLigne[Svgvx0006d.VAR_WEXT]).intValue();
        Date dateConfirmation = ConvertDate.db2ToDate(((BigDecimal) pLigne[Svgvx0006d.VAR_WDLC]).intValue(), null);
        ligneStock.initialiserLigneAchatOrigine(idLigneAchat, idFournisseur, adresseCommandeFournisseur, dateConfirmation);
        break;
    }
    
    // Initialisation en fonction de l'origine de la ligne liée
    ligneStock.setOrigineLigneLien(((String) pLigne[Svgvx0006d.VAR_ALAORI]).charAt(0));
    switch (ligneStock.getOrigineLigneLien().charValue()) {
      case LigneAttenduCommande.ORIGINE_LIGNE_VENTE:
        // Construction de l'id du document de vente
        EnumCodeEnteteDocumentVente codeEntete =
            EnumCodeEnteteDocumentVente.valueOfByCode(((String) pLigne[Svgvx0006d.VAR_ALACOD]).charAt(0));
        Integer numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALANUM]).intValue();
        Integer suffixe = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALASUF]).intValue();
        Integer numeroLigne = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALANLI]).intValue();
        IdLigneVente idLigneVente = IdLigneVente.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
        // Construction de l'id du client
        numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALACLI]).intValue();
        suffixe = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALALIV]).intValue();
        IdClient idClient = IdClient.getInstance(idEtablissement, numero, suffixe);
        ligneStock.initialiserLigneVenteLien(idLigneVente, idClient);
        break;
      case LigneAttenduCommande.ORIGINE_LIGNE_ACHAT:
        // Construction de l'id du document d'achat
        EnumCodeEnteteDocumentAchat codeEnteteDocumentAchat =
            EnumCodeEnteteDocumentAchat.valueOfByCode(((String) pLigne[Svgvx0006d.VAR_ALACOD]).charAt(0));
        numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALANUM]).intValue();
        suffixe = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALASUF]).intValue();
        IdDocumentAchat idDocumentAchat = IdDocumentAchat.getInstance(idEtablissement, codeEnteteDocumentAchat, numero, suffixe);
        IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(idDocumentAchat, ((BigDecimal) pLigne[Svgvx0006d.VAR_ALANLI]).intValue());
        // Construction de l'id du fournisseur
        int collectif = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALACOF]).intValue();
        numero = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALAFRS]).intValue();
        IdFournisseur idFournisseur = IdFournisseur.getInstance(idEtablissement, collectif, numero);
        // Autres variables
        int adresseCommandeFournisseur = ((BigDecimal) pLigne[Svgvx0006d.VAR_ALAEXT]).intValue();
        ligneStock.initialiserLigneAchatLien(idLigneAchat, idFournisseur, adresseCommandeFournisseur);
        break;
    }
    
    return ligneStock;
  }
}
