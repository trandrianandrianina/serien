/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.recherche;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgam0059d {
  // Constantes
  public static final int SIZE_WLACOD = 1;
  public static final int SIZE_WLAETB = 3;
  public static final int SIZE_WLANUM = 6;
  public static final int DECIMAL_WLANUM = 0;
  public static final int SIZE_WLASUF = 1;
  public static final int DECIMAL_WLASUF = 0;
  public static final int SIZE_WLANLI = 4;
  public static final int DECIMAL_WLANLI = 0;
  public static final int SIZE_WLAERL = 1;
  public static final int SIZE_WLACEX = 1;
  public static final int SIZE_WLAART = 20;
  public static final int SIZE_WLIGLB = 124;
  public static final int SIZE_WLADCC = 1;
  public static final int DECIMAL_WLADCC = 0;
  public static final int SIZE_WLADCA = 1;
  public static final int DECIMAL_WLADCA = 0;
  public static final int SIZE_WLADCS = 1;
  public static final int DECIMAL_WLADCS = 0;
  public static final int SIZE_WLAQTC = 11;
  public static final int DECIMAL_WLAQTC = 3;
  public static final int SIZE_WLAUNC = 2;
  public static final int SIZE_WLAQTS = 11;
  public static final int DECIMAL_WLAQTS = 3;
  public static final int SIZE_WLAKSC = 8;
  public static final int DECIMAL_WLAKSC = 3;
  public static final int SIZE_WLAUNS = 2;
  public static final int SIZE_WLAQTA = 11;
  public static final int DECIMAL_WLAQTA = 3;
  public static final int SIZE_WLAUNA = 2;
  public static final int SIZE_WLAKAC = 8;
  public static final int DECIMAL_WLAKAC = 3;
  public static final int SIZE_WLAMAG = 2;
  public static final int SIZE_WLADLP = 7;
  public static final int DECIMAL_WLADLP = 0;
  public static final int SIZE_WLAPAB = 9;
  public static final int DECIMAL_WLAPAB = 2;
  public static final int SIZE_WLAPAN = 9;
  public static final int DECIMAL_WLAPAN = 2;
  public static final int SIZE_WLAPAC = 9;
  public static final int DECIMAL_WLAPAC = 2;
  public static final int SIZE_WLAMHT = 11;
  public static final int DECIMAL_WLAMHT = 2;
  public static final int SIZE_WLAAVR = 1;
  public static final int SIZE_WLAREM1 = 4;
  public static final int DECIMAL_WLAREM1 = 2;
  public static final int SIZE_WLAREM2 = 4;
  public static final int DECIMAL_WLAREM2 = 2;
  public static final int SIZE_WLAREM3 = 4;
  public static final int DECIMAL_WLAREM3 = 2;
  public static final int SIZE_WLAREM4 = 4;
  public static final int DECIMAL_WLAREM4 = 2;
  public static final int SIZE_WLAREM5 = 4;
  public static final int DECIMAL_WLAREM5 = 2;
  public static final int SIZE_WLAREM6 = 4;
  public static final int DECIMAL_WLAREM6 = 2;
  public static final int SIZE_WLATRL = 1;
  public static final int SIZE_WLABRL = 1;
  public static final int SIZE_WLARP1 = 1;
  public static final int SIZE_WLARP2 = 1;
  public static final int SIZE_WLARP3 = 1;
  public static final int SIZE_WLARP4 = 1;
  public static final int SIZE_WLARP5 = 1;
  public static final int SIZE_WLARP6 = 1;
  public static final int SIZE_WLATB = 1;
  public static final int DECIMAL_WLATB = 0;
  public static final int SIZE_WLATR = 1;
  public static final int DECIMAL_WLATR = 0;
  public static final int SIZE_WLATN = 1;
  public static final int DECIMAL_WLATN = 0;
  public static final int SIZE_WLATU = 1;
  public static final int DECIMAL_WLATU = 0;
  public static final int SIZE_WLATQ = 1;
  public static final int DECIMAL_WLATQ = 0;
  public static final int SIZE_WLATH = 1;
  public static final int DECIMAL_WLATH = 0;
  public static final int SIZE_WLATVA = 1;
  public static final int DECIMAL_WLATVA = 0;
  public static final int SIZE_WTAUTVA = 5;
  public static final int DECIMAL_WTAUTVA = 3;
  public static final int SIZE_WLOPRS = 9;
  public static final int DECIMAL_WLOPRS = 2;
  public static final int SIZE_WKCSA = 8;
  public static final int DECIMAL_WKCSA = 3;
  public static final int SIZE_W7PRBRU = 9;
  public static final int DECIMAL_W7PRBRU = 2;
  public static final int SIZE_W7PRNET = 9;
  public static final int DECIMAL_W7PRNET = 2;
  public static final int SIZE_W7PRVFR = 9;
  public static final int DECIMAL_W7PRVFR = 2;
  public static final int SIZE_W7PORTF = 9;
  public static final int DECIMAL_W7PORTF = 2;
  public static final int SIZE_W7XXX1 = 7;
  public static final int DECIMAL_W7XXX1 = 4;
  public static final int SIZE_W7LFK1 = 7;
  public static final int DECIMAL_W7LFK1 = 4;
  public static final int SIZE_W7LFV1 = 9;
  public static final int DECIMAL_W7LFV1 = 2;
  public static final int SIZE_W7LFP1 = 4;
  public static final int DECIMAL_W7LFP1 = 2;
  public static final int SIZE_W7LFK3 = 7;
  public static final int DECIMAL_W7LFK3 = 4;
  public static final int SIZE_W7LFV3 = 9;
  public static final int DECIMAL_W7LFV3 = 2;
  public static final int SIZE_WTYPART = 3;
  public static final int SIZE_WLAQTAI = 11;
  public static final int DECIMAL_WLAQTAI = 3;
  public static final int SIZE_WLAQTCI = 11;
  public static final int DECIMAL_WLAQTCI = 3;
  public static final int SIZE_WLAQTSI = 11;
  public static final int DECIMAL_WLAQTSI = 3;
  public static final int SIZE_WLAMHTI = 11;
  public static final int DECIMAL_WLAMHTI = 2;
  public static final int SIZE_WLAQTAT = 11;
  public static final int DECIMAL_WLAQTAT = 3;
  public static final int SIZE_WLAQTCT = 11;
  public static final int DECIMAL_WLAQTCT = 3;
  public static final int SIZE_WLAQTST = 11;
  public static final int DECIMAL_WLAQTST = 3;
  public static final int SIZE_WLAMHTT = 11;
  public static final int DECIMAL_WLAMHTT = 2;
  public static final int SIZE_TOTALE_DS = 498;
  public static final int SIZE_FILLER = 500 - 498;
  
  // Constantes indices Nom DS
  public static final int VAR_WLACOD = 0;
  public static final int VAR_WLAETB = 1;
  public static final int VAR_WLANUM = 2;
  public static final int VAR_WLASUF = 3;
  public static final int VAR_WLANLI = 4;
  public static final int VAR_WLAERL = 5;
  public static final int VAR_WLACEX = 6;
  public static final int VAR_WLAART = 7;
  public static final int VAR_WLIGLB = 8;
  public static final int VAR_WLADCC = 9;
  public static final int VAR_WLADCA = 10;
  public static final int VAR_WLADCS = 11;
  public static final int VAR_WLAQTC = 12;
  public static final int VAR_WLAUNC = 13;
  public static final int VAR_WLAQTS = 14;
  public static final int VAR_WLAKSC = 15;
  public static final int VAR_WLAUNS = 16;
  public static final int VAR_WLAQTA = 17;
  public static final int VAR_WLAUNA = 18;
  public static final int VAR_WLAKAC = 19;
  public static final int VAR_WLAMAG = 20;
  public static final int VAR_WLADLP = 21;
  public static final int VAR_WLAPAB = 22;
  public static final int VAR_WLAPAN = 23;
  public static final int VAR_WLAPAC = 24;
  public static final int VAR_WLAMHT = 25;
  public static final int VAR_WLAAVR = 26;
  public static final int VAR_WLAREM1 = 27;
  public static final int VAR_WLAREM2 = 28;
  public static final int VAR_WLAREM3 = 29;
  public static final int VAR_WLAREM4 = 30;
  public static final int VAR_WLAREM5 = 31;
  public static final int VAR_WLAREM6 = 32;
  public static final int VAR_WLATRL = 33;
  public static final int VAR_WLABRL = 34;
  public static final int VAR_WLARP1 = 35;
  public static final int VAR_WLARP2 = 36;
  public static final int VAR_WLARP3 = 37;
  public static final int VAR_WLARP4 = 38;
  public static final int VAR_WLARP5 = 39;
  public static final int VAR_WLARP6 = 40;
  public static final int VAR_WLATB = 41;
  public static final int VAR_WLATR = 42;
  public static final int VAR_WLATN = 43;
  public static final int VAR_WLATU = 44;
  public static final int VAR_WLATQ = 45;
  public static final int VAR_WLATH = 46;
  public static final int VAR_WLATVA = 47;
  public static final int VAR_WTAUTVA = 48;
  public static final int VAR_WLOPRS = 49;
  public static final int VAR_WKCSA = 50;
  public static final int VAR_W7PRBRU = 51;
  public static final int VAR_W7PRNET = 52;
  public static final int VAR_W7PRVFR = 53;
  public static final int VAR_W7PORTF = 54;
  public static final int VAR_W7XXX1 = 55;
  public static final int VAR_W7LFK1 = 56;
  public static final int VAR_W7LFV1 = 57;
  public static final int VAR_W7LFP1 = 58;
  public static final int VAR_W7LFK3 = 59;
  public static final int VAR_W7LFV3 = 60;
  public static final int VAR_WTYPART = 61;
  public static final int VAR_WLAQTAI = 62;
  public static final int VAR_WLAQTCI = 63;
  public static final int VAR_WLAQTSI = 64;
  public static final int VAR_WLAMHTI = 65;
  public static final int VAR_WLAQTAT = 66;
  public static final int VAR_WLAQTCT = 67;
  public static final int VAR_WLAQTST = 68;
  public static final int VAR_WLAMHTT = 69;
  
  // Variables AS400
  private String wlacod = ""; // Code ERL "E" OU "F"
  private String wlaetb = ""; // Code Etablissement
  private BigDecimal wlanum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal wlasuf = BigDecimal.ZERO; // Suffixe du Bon
  private BigDecimal wlanli = BigDecimal.ZERO; // Numéro de Ligne
  private String wlaerl = ""; // Code ERL "C"
  private String wlacex = ""; // Code Extraction
  private String wlaart = ""; // Code article
  private String wliglb = ""; // Libellé
  private BigDecimal wladcc = BigDecimal.ZERO; // Décimalisation QTC
  private BigDecimal wladca = BigDecimal.ZERO; // " " QTA
  private BigDecimal wladcs = BigDecimal.ZERO; // " " QTS
  private BigDecimal wlaqtc = BigDecimal.ZERO; // Quantité en unités de cde
  private String wlaunc = ""; // Unité de commande
  private BigDecimal wlaqts = BigDecimal.ZERO; // Quantité en unités de stock
  private BigDecimal wlaksc = BigDecimal.ZERO; // Coeff. Stock/Cde
  private String wlauns = ""; // Unité de stocks
  private BigDecimal wlaqta = BigDecimal.ZERO; // Quantité en unités d"achat
  private String wlauna = ""; // Unité d"achat
  private BigDecimal wlakac = BigDecimal.ZERO; // Coeff. Achat/Cde
  private String wlamag = ""; // Magasin
  private BigDecimal wladlp = BigDecimal.ZERO; // Date livraison prévue
  private BigDecimal wlapab = BigDecimal.ZERO; // Prix d"achat de base
  private BigDecimal wlapan = BigDecimal.ZERO; // Prix d"achat net
  private BigDecimal wlapac = BigDecimal.ZERO; // Prix d"achat calculé
  private BigDecimal wlamht = BigDecimal.ZERO; // Montant hors taxes
  private String wlaavr = ""; // Code Avoir
  private BigDecimal wlarem1 = BigDecimal.ZERO; // % Remise 1 / ligne
  private BigDecimal wlarem2 = BigDecimal.ZERO; // % Remise 2
  private BigDecimal wlarem3 = BigDecimal.ZERO; // % Remise 3
  private BigDecimal wlarem4 = BigDecimal.ZERO; // % Remise 4
  private BigDecimal wlarem5 = BigDecimal.ZERO; // % Remise 5
  private BigDecimal wlarem6 = BigDecimal.ZERO; // % Remise 6
  private String wlatrl = ""; // Type remise ligne, 1=cascade
  private String wlabrl = ""; // Base remise ligne, 1=Montant
  private String wlarp1 = ""; // Exclusion remise de pied N°1
  private String wlarp2 = ""; // Exclusion remise de pied N°2
  private String wlarp3 = ""; // Exclusion remise de pied N°3
  private String wlarp4 = ""; // Exclusion remise de pied N°4
  private String wlarp5 = ""; // Exclusion remise de pied N°5
  private String wlarp6 = ""; // Exclusion remise de pied N°6
  private BigDecimal wlatb = BigDecimal.ZERO; // Top prix de base saisi
  private BigDecimal wlatr = BigDecimal.ZERO; // Top remises saisies
  private BigDecimal wlatn = BigDecimal.ZERO; // Top prix net saisi
  private BigDecimal wlatu = BigDecimal.ZERO; // Top unité saisie
  private BigDecimal wlatq = BigDecimal.ZERO; // Top Qté saisie
  private BigDecimal wlath = BigDecimal.ZERO; // Top montant H.T saisi
  private BigDecimal wlatva = BigDecimal.ZERO; // Code TVA
  private BigDecimal wtautva = BigDecimal.ZERO; // Taux de TVA
  private BigDecimal wloprs = BigDecimal.ZERO; // Prix de revient standard
  private BigDecimal wkcsa = BigDecimal.ZERO; // Coeff. SurStock/Achat
  private BigDecimal w7prbru = BigDecimal.ZERO; // Prix d achat brut
  private BigDecimal w7prnet = BigDecimal.ZERO; // Prix d achat net sans port
  private BigDecimal w7prvfr = BigDecimal.ZERO; // Prix revient fournisseur
  private BigDecimal w7portf = BigDecimal.ZERO; // Port fournissseur
  private BigDecimal w7xxx1 = BigDecimal.ZERO; // Plus utilisé (anc.PiLFK1)
  private BigDecimal w7lfk1 = BigDecimal.ZERO; // Frais 1 coef.poids port
  private BigDecimal w7lfv1 = BigDecimal.ZERO; // Frais 1 valeur port
  private BigDecimal w7lfp1 = BigDecimal.ZERO; // Frais 1 % PORT
  private BigDecimal w7lfk3 = BigDecimal.ZERO; // Frais 3 taxe ou majoration
  private BigDecimal w7lfv3 = BigDecimal.ZERO; // Frais 3 val.conditionnement
  private String wtypart = ""; // Type ligne
  private BigDecimal wlaqtai = BigDecimal.ZERO; // Qte initiale unités d"achat
  private BigDecimal wlaqtci = BigDecimal.ZERO; // Qte initiale unités de cde
  private BigDecimal wlaqtsi = BigDecimal.ZERO; // Qte initiale unités de stock
  private BigDecimal wlamhti = BigDecimal.ZERO; // Montant initial hors taxe
  private BigDecimal wlaqtat = BigDecimal.ZERO; // Qte traitées unités d"achat
  private BigDecimal wlaqtct = BigDecimal.ZERO; // Qte traitées unités de cde
  private BigDecimal wlaqtst = BigDecimal.ZERO; // Qte traitées unités de stock
  private BigDecimal wlamhtt = BigDecimal.ZERO; // Montant Traité hors taxe
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WLACOD), // Code ERL "E" OU "F"
      new AS400Text(SIZE_WLAETB), // Code Etablissement
      new AS400ZonedDecimal(SIZE_WLANUM, DECIMAL_WLANUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_WLASUF, DECIMAL_WLASUF), // Suffixe du Bon
      new AS400ZonedDecimal(SIZE_WLANLI, DECIMAL_WLANLI), // Numéro de Ligne
      new AS400Text(SIZE_WLAERL), // Code ERL "C"
      new AS400Text(SIZE_WLACEX), // Code Extraction
      new AS400Text(SIZE_WLAART), // Code article
      new AS400Text(SIZE_WLIGLB), // Libellé
      new AS400ZonedDecimal(SIZE_WLADCC, DECIMAL_WLADCC), // Décimalisation QTC
      new AS400ZonedDecimal(SIZE_WLADCA, DECIMAL_WLADCA), // " " QTA
      new AS400ZonedDecimal(SIZE_WLADCS, DECIMAL_WLADCS), // " " QTS
      new AS400PackedDecimal(SIZE_WLAQTC, DECIMAL_WLAQTC), // Quantité en unités de cde
      new AS400Text(SIZE_WLAUNC), // Unité de commande
      new AS400PackedDecimal(SIZE_WLAQTS, DECIMAL_WLAQTS), // Quantité en unités de stock
      new AS400PackedDecimal(SIZE_WLAKSC, DECIMAL_WLAKSC), // Coeff. Stock/Cde
      new AS400Text(SIZE_WLAUNS), // Unité de stocks
      new AS400PackedDecimal(SIZE_WLAQTA, DECIMAL_WLAQTA), // Quantité en unités d"achat
      new AS400Text(SIZE_WLAUNA), // Unité d"achat
      new AS400PackedDecimal(SIZE_WLAKAC, DECIMAL_WLAKAC), // Coeff. Achat/Cde
      new AS400Text(SIZE_WLAMAG), // Magasin
      new AS400PackedDecimal(SIZE_WLADLP, DECIMAL_WLADLP), // Date livraison prévue
      new AS400PackedDecimal(SIZE_WLAPAB, DECIMAL_WLAPAB), // Prix d"achat de base
      new AS400PackedDecimal(SIZE_WLAPAN, DECIMAL_WLAPAN), // Prix d"achat net
      new AS400PackedDecimal(SIZE_WLAPAC, DECIMAL_WLAPAC), // Prix d"achat calculé
      new AS400PackedDecimal(SIZE_WLAMHT, DECIMAL_WLAMHT), // Montant hors taxes
      new AS400Text(SIZE_WLAAVR), // Code Avoir
      new AS400ZonedDecimal(SIZE_WLAREM1, DECIMAL_WLAREM1), // % Remise 1 / ligne
      new AS400ZonedDecimal(SIZE_WLAREM2, DECIMAL_WLAREM2), // % Remise 2
      new AS400ZonedDecimal(SIZE_WLAREM3, DECIMAL_WLAREM3), // % Remise 3
      new AS400ZonedDecimal(SIZE_WLAREM4, DECIMAL_WLAREM4), // % Remise 4
      new AS400ZonedDecimal(SIZE_WLAREM5, DECIMAL_WLAREM5), // % Remise 5
      new AS400ZonedDecimal(SIZE_WLAREM6, DECIMAL_WLAREM6), // % Remise 6
      new AS400Text(SIZE_WLATRL), // Type remise ligne, 1=cascade
      new AS400Text(SIZE_WLABRL), // Base remise ligne, 1=Montant
      new AS400Text(SIZE_WLARP1), // Exclusion remise de pied N°1
      new AS400Text(SIZE_WLARP2), // Exclusion remise de pied N°2
      new AS400Text(SIZE_WLARP3), // Exclusion remise de pied N°3
      new AS400Text(SIZE_WLARP4), // Exclusion remise de pied N°4
      new AS400Text(SIZE_WLARP5), // Exclusion remise de pied N°5
      new AS400Text(SIZE_WLARP6), // Exclusion remise de pied N°6
      new AS400ZonedDecimal(SIZE_WLATB, DECIMAL_WLATB), // Top prix de base saisi
      new AS400ZonedDecimal(SIZE_WLATR, DECIMAL_WLATR), // Top remises saisies
      new AS400ZonedDecimal(SIZE_WLATN, DECIMAL_WLATN), // Top prix net saisi
      new AS400ZonedDecimal(SIZE_WLATU, DECIMAL_WLATU), // Top unité saisie
      new AS400ZonedDecimal(SIZE_WLATQ, DECIMAL_WLATQ), // Top Qté saisie
      new AS400ZonedDecimal(SIZE_WLATH, DECIMAL_WLATH), // Top montant H.T saisi
      new AS400ZonedDecimal(SIZE_WLATVA, DECIMAL_WLATVA), // Code TVA
      new AS400ZonedDecimal(SIZE_WTAUTVA, DECIMAL_WTAUTVA), // Taux de TVA
      new AS400ZonedDecimal(SIZE_WLOPRS, DECIMAL_WLOPRS), // Prix de revient standard
      new AS400ZonedDecimal(SIZE_WKCSA, DECIMAL_WKCSA), // Coeff. SurStock/Achat
      new AS400ZonedDecimal(SIZE_W7PRBRU, DECIMAL_W7PRBRU), // Prix d achat brut
      new AS400ZonedDecimal(SIZE_W7PRNET, DECIMAL_W7PRNET), // Prix d achat net sans port
      new AS400ZonedDecimal(SIZE_W7PRVFR, DECIMAL_W7PRVFR), // Prix revient fournisseur
      new AS400ZonedDecimal(SIZE_W7PORTF, DECIMAL_W7PORTF), // Port fournissseur
      new AS400ZonedDecimal(SIZE_W7XXX1, DECIMAL_W7XXX1), // Plus utilisé (anc.PiLFK1)
      new AS400ZonedDecimal(SIZE_W7LFK1, DECIMAL_W7LFK1), // Frais 1 coef.poids port
      new AS400ZonedDecimal(SIZE_W7LFV1, DECIMAL_W7LFV1), // Frais 1 valeur port
      new AS400ZonedDecimal(SIZE_W7LFP1, DECIMAL_W7LFP1), // Frais 1 % PORT
      new AS400ZonedDecimal(SIZE_W7LFK3, DECIMAL_W7LFK3), // Frais 3 taxe ou majoration
      new AS400ZonedDecimal(SIZE_W7LFV3, DECIMAL_W7LFV3), // Frais 3 val.conditionnement
      new AS400Text(SIZE_WTYPART), // Type ligne
      new AS400PackedDecimal(SIZE_WLAQTAI, DECIMAL_WLAQTAI), // Qte initiale unités d"achat
      new AS400PackedDecimal(SIZE_WLAQTCI, DECIMAL_WLAQTCI), // Qte initiale unités de cde
      new AS400PackedDecimal(SIZE_WLAQTSI, DECIMAL_WLAQTSI), // Qte initiale unités de stock
      new AS400PackedDecimal(SIZE_WLAMHTI, DECIMAL_WLAMHTI), // Montant initial hors taxe
      new AS400PackedDecimal(SIZE_WLAQTAT, DECIMAL_WLAQTAT), // Qte traitées unités d"achat
      new AS400PackedDecimal(SIZE_WLAQTCT, DECIMAL_WLAQTCT), // Qte traitées unités de cde
      new AS400PackedDecimal(SIZE_WLAQTST, DECIMAL_WLAQTST), // Qte traitées unités de stock
      new AS400PackedDecimal(SIZE_WLAMHTT, DECIMAL_WLAMHTT), // Montant Traité hors taxe
      new AS400Text(SIZE_FILLER), // Filler
  };
  public Object[] o = { wlacod, wlaetb, wlanum, wlasuf, wlanli, wlaerl, wlacex, wlaart, wliglb, wladcc, wladca, wladcs, wlaqtc, wlaunc,
      wlaqts, wlaksc, wlauns, wlaqta, wlauna, wlakac, wlamag, wladlp, wlapab, wlapan, wlapac, wlamht, wlaavr, wlarem1, wlarem2, wlarem3,
      wlarem4, wlarem5, wlarem6, wlatrl, wlabrl, wlarp1, wlarp2, wlarp3, wlarp4, wlarp5, wlarp6, wlatb, wlatr, wlatn, wlatu, wlatq, wlath,
      wlatva, wtautva, wloprs, wkcsa, w7prbru, w7prnet, w7prvfr, w7portf, w7xxx1, w7lfk1, w7lfv1, w7lfp1, w7lfk3, w7lfv3, wtypart,
      wlaqtai, wlaqtci, wlaqtsi, wlamhti, wlaqtat, wlaqtct, wlaqtst, wlamhtt, filler, };
  
  // -- Accesseurs
  
  public void setWlacod(Character pWlacod) {
    if (pWlacod == null) {
      return;
    }
    wlacod = String.valueOf(pWlacod);
  }
  
  public Character getWlacod() {
    return wlacod.charAt(0);
  }
  
  public void setWlaetb(String pWlaetb) {
    if (pWlaetb == null) {
      return;
    }
    wlaetb = pWlaetb;
  }
  
  public String getWlaetb() {
    return wlaetb;
  }
  
  public void setWlanum(BigDecimal pWlanum) {
    if (pWlanum == null) {
      return;
    }
    wlanum = pWlanum.setScale(DECIMAL_WLANUM, RoundingMode.HALF_UP);
  }
  
  public void setWlanum(Integer pWlanum) {
    if (pWlanum == null) {
      return;
    }
    wlanum = BigDecimal.valueOf(pWlanum);
  }
  
  public Integer getWlanum() {
    return wlanum.intValue();
  }
  
  public void setWlasuf(BigDecimal pWlasuf) {
    if (pWlasuf == null) {
      return;
    }
    wlasuf = pWlasuf.setScale(DECIMAL_WLASUF, RoundingMode.HALF_UP);
  }
  
  public void setWlasuf(Integer pWlasuf) {
    if (pWlasuf == null) {
      return;
    }
    wlasuf = BigDecimal.valueOf(pWlasuf);
  }
  
  public Integer getWlasuf() {
    return wlasuf.intValue();
  }
  
  public void setWlanli(BigDecimal pWlanli) {
    if (pWlanli == null) {
      return;
    }
    wlanli = pWlanli.setScale(DECIMAL_WLANLI, RoundingMode.HALF_UP);
  }
  
  public void setWlanli(Integer pWlanli) {
    if (pWlanli == null) {
      return;
    }
    wlanli = BigDecimal.valueOf(pWlanli);
  }
  
  public Integer getWlanli() {
    return wlanli.intValue();
  }
  
  public void setWlaerl(Character pWlaerl) {
    if (pWlaerl == null) {
      return;
    }
    wlaerl = String.valueOf(pWlaerl);
  }
  
  public Character getWlaerl() {
    return wlaerl.charAt(0);
  }
  
  public void setWlacex(Character pWlacex) {
    if (pWlacex == null) {
      return;
    }
    wlacex = String.valueOf(pWlacex);
  }
  
  public Character getWlacex() {
    return wlacex.charAt(0);
  }
  
  public void setWlaart(String pWlaart) {
    if (pWlaart == null) {
      return;
    }
    wlaart = pWlaart;
  }
  
  public String getWlaart() {
    return wlaart;
  }
  
  public void setWliglb(String pWliglb) {
    if (pWliglb == null) {
      return;
    }
    wliglb = pWliglb;
  }
  
  public String getWliglb() {
    return wliglb;
  }
  
  public void setWladcc(BigDecimal pWladcc) {
    if (pWladcc == null) {
      return;
    }
    wladcc = pWladcc.setScale(DECIMAL_WLADCC, RoundingMode.HALF_UP);
  }
  
  public void setWladcc(Integer pWladcc) {
    if (pWladcc == null) {
      return;
    }
    wladcc = BigDecimal.valueOf(pWladcc);
  }
  
  public Integer getWladcc() {
    return wladcc.intValue();
  }
  
  public void setWladca(BigDecimal pWladca) {
    if (pWladca == null) {
      return;
    }
    wladca = pWladca.setScale(DECIMAL_WLADCA, RoundingMode.HALF_UP);
  }
  
  public void setWladca(Integer pWladca) {
    if (pWladca == null) {
      return;
    }
    wladca = BigDecimal.valueOf(pWladca);
  }
  
  public Integer getWladca() {
    return wladca.intValue();
  }
  
  public void setWladcs(BigDecimal pWladcs) {
    if (pWladcs == null) {
      return;
    }
    wladcs = pWladcs.setScale(DECIMAL_WLADCS, RoundingMode.HALF_UP);
  }
  
  public void setWladcs(Integer pWladcs) {
    if (pWladcs == null) {
      return;
    }
    wladcs = BigDecimal.valueOf(pWladcs);
  }
  
  public Integer getWladcs() {
    return wladcs.intValue();
  }
  
  public void setWlaqtc(BigDecimal pWlaqtc) {
    if (pWlaqtc == null) {
      return;
    }
    wlaqtc = pWlaqtc.setScale(DECIMAL_WLAQTC, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtc(Double pWlaqtc) {
    if (pWlaqtc == null) {
      return;
    }
    wlaqtc = BigDecimal.valueOf(pWlaqtc).setScale(DECIMAL_WLAQTC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtc() {
    return wlaqtc.setScale(DECIMAL_WLAQTC, RoundingMode.HALF_UP);
  }
  
  public void setWlaunc(String pWlaunc) {
    if (pWlaunc == null) {
      return;
    }
    wlaunc = pWlaunc;
  }
  
  public String getWlaunc() {
    return wlaunc;
  }
  
  public void setWlaqts(BigDecimal pWlaqts) {
    if (pWlaqts == null) {
      return;
    }
    wlaqts = pWlaqts.setScale(DECIMAL_WLAQTS, RoundingMode.HALF_UP);
  }
  
  public void setWlaqts(Double pWlaqts) {
    if (pWlaqts == null) {
      return;
    }
    wlaqts = BigDecimal.valueOf(pWlaqts).setScale(DECIMAL_WLAQTS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqts() {
    return wlaqts.setScale(DECIMAL_WLAQTS, RoundingMode.HALF_UP);
  }
  
  public void setWlaksc(BigDecimal pWlaksc) {
    if (pWlaksc == null) {
      return;
    }
    wlaksc = pWlaksc.setScale(DECIMAL_WLAKSC, RoundingMode.HALF_UP);
  }
  
  public void setWlaksc(Double pWlaksc) {
    if (pWlaksc == null) {
      return;
    }
    wlaksc = BigDecimal.valueOf(pWlaksc).setScale(DECIMAL_WLAKSC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaksc() {
    return wlaksc.setScale(DECIMAL_WLAKSC, RoundingMode.HALF_UP);
  }
  
  public void setWlauns(String pWlauns) {
    if (pWlauns == null) {
      return;
    }
    wlauns = pWlauns;
  }
  
  public String getWlauns() {
    return wlauns;
  }
  
  public void setWlaqta(BigDecimal pWlaqta) {
    if (pWlaqta == null) {
      return;
    }
    wlaqta = pWlaqta.setScale(DECIMAL_WLAQTA, RoundingMode.HALF_UP);
  }
  
  public void setWlaqta(Double pWlaqta) {
    if (pWlaqta == null) {
      return;
    }
    wlaqta = BigDecimal.valueOf(pWlaqta).setScale(DECIMAL_WLAQTA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqta() {
    return wlaqta.setScale(DECIMAL_WLAQTA, RoundingMode.HALF_UP);
  }
  
  public void setWlauna(String pWlauna) {
    if (pWlauna == null) {
      return;
    }
    wlauna = pWlauna;
  }
  
  public String getWlauna() {
    return wlauna;
  }
  
  public void setWlakac(BigDecimal pWlakac) {
    if (pWlakac == null) {
      return;
    }
    wlakac = pWlakac.setScale(DECIMAL_WLAKAC, RoundingMode.HALF_UP);
  }
  
  public void setWlakac(Double pWlakac) {
    if (pWlakac == null) {
      return;
    }
    wlakac = BigDecimal.valueOf(pWlakac).setScale(DECIMAL_WLAKAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlakac() {
    return wlakac.setScale(DECIMAL_WLAKAC, RoundingMode.HALF_UP);
  }
  
  public void setWlamag(String pWlamag) {
    if (pWlamag == null) {
      return;
    }
    wlamag = pWlamag;
  }
  
  public String getWlamag() {
    return wlamag;
  }
  
  public void setWladlp(BigDecimal pWladlp) {
    if (pWladlp == null) {
      return;
    }
    wladlp = pWladlp.setScale(DECIMAL_WLADLP, RoundingMode.HALF_UP);
  }
  
  public void setWladlp(Integer pWladlp) {
    if (pWladlp == null) {
      return;
    }
    wladlp = BigDecimal.valueOf(pWladlp);
  }
  
  public void setWladlp(Date pWladlp) {
    if (pWladlp == null) {
      return;
    }
    wladlp = BigDecimal.valueOf(ConvertDate.dateToDb2(pWladlp));
  }
  
  public Integer getWladlp() {
    return wladlp.intValue();
  }
  
  public Date getWladlpConvertiEnDate() {
    return ConvertDate.db2ToDate(wladlp.intValue(), null);
  }
  
  public void setWlapab(BigDecimal pWlapab) {
    if (pWlapab == null) {
      return;
    }
    wlapab = pWlapab.setScale(DECIMAL_WLAPAB, RoundingMode.HALF_UP);
  }
  
  public void setWlapab(Double pWlapab) {
    if (pWlapab == null) {
      return;
    }
    wlapab = BigDecimal.valueOf(pWlapab).setScale(DECIMAL_WLAPAB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlapab() {
    return wlapab.setScale(DECIMAL_WLAPAB, RoundingMode.HALF_UP);
  }
  
  public void setWlapan(BigDecimal pWlapan) {
    if (pWlapan == null) {
      return;
    }
    wlapan = pWlapan.setScale(DECIMAL_WLAPAN, RoundingMode.HALF_UP);
  }
  
  public void setWlapan(Double pWlapan) {
    if (pWlapan == null) {
      return;
    }
    wlapan = BigDecimal.valueOf(pWlapan).setScale(DECIMAL_WLAPAN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlapan() {
    return wlapan.setScale(DECIMAL_WLAPAN, RoundingMode.HALF_UP);
  }
  
  public void setWlapac(BigDecimal pWlapac) {
    if (pWlapac == null) {
      return;
    }
    wlapac = pWlapac.setScale(DECIMAL_WLAPAC, RoundingMode.HALF_UP);
  }
  
  public void setWlapac(Double pWlapac) {
    if (pWlapac == null) {
      return;
    }
    wlapac = BigDecimal.valueOf(pWlapac).setScale(DECIMAL_WLAPAC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlapac() {
    return wlapac.setScale(DECIMAL_WLAPAC, RoundingMode.HALF_UP);
  }
  
  public void setWlamht(BigDecimal pWlamht) {
    if (pWlamht == null) {
      return;
    }
    wlamht = pWlamht.setScale(DECIMAL_WLAMHT, RoundingMode.HALF_UP);
  }
  
  public void setWlamht(Double pWlamht) {
    if (pWlamht == null) {
      return;
    }
    wlamht = BigDecimal.valueOf(pWlamht).setScale(DECIMAL_WLAMHT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlamht() {
    return wlamht.setScale(DECIMAL_WLAMHT, RoundingMode.HALF_UP);
  }
  
  public void setWlaavr(Character pWlaavr) {
    if (pWlaavr == null) {
      return;
    }
    wlaavr = String.valueOf(pWlaavr);
  }
  
  public Character getWlaavr() {
    return wlaavr.charAt(0);
  }
  
  public void setWlarem1(BigDecimal pWlarem1) {
    if (pWlarem1 == null) {
      return;
    }
    wlarem1 = pWlarem1.setScale(DECIMAL_WLAREM1, RoundingMode.HALF_UP);
  }
  
  public void setWlarem1(Double pWlarem1) {
    if (pWlarem1 == null) {
      return;
    }
    wlarem1 = BigDecimal.valueOf(pWlarem1).setScale(DECIMAL_WLAREM1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlarem1() {
    return wlarem1.setScale(DECIMAL_WLAREM1, RoundingMode.HALF_UP);
  }
  
  public void setWlarem2(BigDecimal pWlarem2) {
    if (pWlarem2 == null) {
      return;
    }
    wlarem2 = pWlarem2.setScale(DECIMAL_WLAREM2, RoundingMode.HALF_UP);
  }
  
  public void setWlarem2(Double pWlarem2) {
    if (pWlarem2 == null) {
      return;
    }
    wlarem2 = BigDecimal.valueOf(pWlarem2).setScale(DECIMAL_WLAREM2, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlarem2() {
    return wlarem2.setScale(DECIMAL_WLAREM2, RoundingMode.HALF_UP);
  }
  
  public void setWlarem3(BigDecimal pWlarem3) {
    if (pWlarem3 == null) {
      return;
    }
    wlarem3 = pWlarem3.setScale(DECIMAL_WLAREM3, RoundingMode.HALF_UP);
  }
  
  public void setWlarem3(Double pWlarem3) {
    if (pWlarem3 == null) {
      return;
    }
    wlarem3 = BigDecimal.valueOf(pWlarem3).setScale(DECIMAL_WLAREM3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlarem3() {
    return wlarem3.setScale(DECIMAL_WLAREM3, RoundingMode.HALF_UP);
  }
  
  public void setWlarem4(BigDecimal pWlarem4) {
    if (pWlarem4 == null) {
      return;
    }
    wlarem4 = pWlarem4.setScale(DECIMAL_WLAREM4, RoundingMode.HALF_UP);
  }
  
  public void setWlarem4(Double pWlarem4) {
    if (pWlarem4 == null) {
      return;
    }
    wlarem4 = BigDecimal.valueOf(pWlarem4).setScale(DECIMAL_WLAREM4, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlarem4() {
    return wlarem4.setScale(DECIMAL_WLAREM4, RoundingMode.HALF_UP);
  }
  
  public void setWlarem5(BigDecimal pWlarem5) {
    if (pWlarem5 == null) {
      return;
    }
    wlarem5 = pWlarem5.setScale(DECIMAL_WLAREM5, RoundingMode.HALF_UP);
  }
  
  public void setWlarem5(Double pWlarem5) {
    if (pWlarem5 == null) {
      return;
    }
    wlarem5 = BigDecimal.valueOf(pWlarem5).setScale(DECIMAL_WLAREM5, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlarem5() {
    return wlarem5.setScale(DECIMAL_WLAREM5, RoundingMode.HALF_UP);
  }
  
  public void setWlarem6(BigDecimal pWlarem6) {
    if (pWlarem6 == null) {
      return;
    }
    wlarem6 = pWlarem6.setScale(DECIMAL_WLAREM6, RoundingMode.HALF_UP);
  }
  
  public void setWlarem6(Double pWlarem6) {
    if (pWlarem6 == null) {
      return;
    }
    wlarem6 = BigDecimal.valueOf(pWlarem6).setScale(DECIMAL_WLAREM6, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlarem6() {
    return wlarem6.setScale(DECIMAL_WLAREM6, RoundingMode.HALF_UP);
  }
  
  public void setWlatrl(Character pWlatrl) {
    if (pWlatrl == null) {
      return;
    }
    wlatrl = String.valueOf(pWlatrl);
  }
  
  public Character getWlatrl() {
    return wlatrl.charAt(0);
  }
  
  public void setWlabrl(Character pWlabrl) {
    if (pWlabrl == null) {
      return;
    }
    wlabrl = String.valueOf(pWlabrl);
  }
  
  public Character getWlabrl() {
    return wlabrl.charAt(0);
  }
  
  public void setWlarp1(Character pWlarp1) {
    if (pWlarp1 == null) {
      return;
    }
    wlarp1 = String.valueOf(pWlarp1);
  }
  
  public Character getWlarp1() {
    return wlarp1.charAt(0);
  }
  
  public void setWlarp2(Character pWlarp2) {
    if (pWlarp2 == null) {
      return;
    }
    wlarp2 = String.valueOf(pWlarp2);
  }
  
  public Character getWlarp2() {
    return wlarp2.charAt(0);
  }
  
  public void setWlarp3(Character pWlarp3) {
    if (pWlarp3 == null) {
      return;
    }
    wlarp3 = String.valueOf(pWlarp3);
  }
  
  public Character getWlarp3() {
    return wlarp3.charAt(0);
  }
  
  public void setWlarp4(Character pWlarp4) {
    if (pWlarp4 == null) {
      return;
    }
    wlarp4 = String.valueOf(pWlarp4);
  }
  
  public Character getWlarp4() {
    return wlarp4.charAt(0);
  }
  
  public void setWlarp5(Character pWlarp5) {
    if (pWlarp5 == null) {
      return;
    }
    wlarp5 = String.valueOf(pWlarp5);
  }
  
  public Character getWlarp5() {
    return wlarp5.charAt(0);
  }
  
  public void setWlarp6(Character pWlarp6) {
    if (pWlarp6 == null) {
      return;
    }
    wlarp6 = String.valueOf(pWlarp6);
  }
  
  public Character getWlarp6() {
    return wlarp6.charAt(0);
  }
  
  public void setWlatb(BigDecimal pWlatb) {
    if (pWlatb == null) {
      return;
    }
    wlatb = pWlatb.setScale(DECIMAL_WLATB, RoundingMode.HALF_UP);
  }
  
  public void setWlatb(Integer pWlatb) {
    if (pWlatb == null) {
      return;
    }
    wlatb = BigDecimal.valueOf(pWlatb);
  }
  
  public Integer getWlatb() {
    return wlatb.intValue();
  }
  
  public void setWlatr(BigDecimal pWlatr) {
    if (pWlatr == null) {
      return;
    }
    wlatr = pWlatr.setScale(DECIMAL_WLATR, RoundingMode.HALF_UP);
  }
  
  public void setWlatr(Integer pWlatr) {
    if (pWlatr == null) {
      return;
    }
    wlatr = BigDecimal.valueOf(pWlatr);
  }
  
  public Integer getWlatr() {
    return wlatr.intValue();
  }
  
  public void setWlatn(BigDecimal pWlatn) {
    if (pWlatn == null) {
      return;
    }
    wlatn = pWlatn.setScale(DECIMAL_WLATN, RoundingMode.HALF_UP);
  }
  
  public void setWlatn(Integer pWlatn) {
    if (pWlatn == null) {
      return;
    }
    wlatn = BigDecimal.valueOf(pWlatn);
  }
  
  public Integer getWlatn() {
    return wlatn.intValue();
  }
  
  public void setWlatu(BigDecimal pWlatu) {
    if (pWlatu == null) {
      return;
    }
    wlatu = pWlatu.setScale(DECIMAL_WLATU, RoundingMode.HALF_UP);
  }
  
  public void setWlatu(Integer pWlatu) {
    if (pWlatu == null) {
      return;
    }
    wlatu = BigDecimal.valueOf(pWlatu);
  }
  
  public Integer getWlatu() {
    return wlatu.intValue();
  }
  
  public void setWlatq(BigDecimal pWlatq) {
    if (pWlatq == null) {
      return;
    }
    wlatq = pWlatq.setScale(DECIMAL_WLATQ, RoundingMode.HALF_UP);
  }
  
  public void setWlatq(Integer pWlatq) {
    if (pWlatq == null) {
      return;
    }
    wlatq = BigDecimal.valueOf(pWlatq);
  }
  
  public Integer getWlatq() {
    return wlatq.intValue();
  }
  
  public void setWlath(BigDecimal pWlath) {
    if (pWlath == null) {
      return;
    }
    wlath = pWlath.setScale(DECIMAL_WLATH, RoundingMode.HALF_UP);
  }
  
  public void setWlath(Integer pWlath) {
    if (pWlath == null) {
      return;
    }
    wlath = BigDecimal.valueOf(pWlath);
  }
  
  public Integer getWlath() {
    return wlath.intValue();
  }
  
  public void setWlatva(BigDecimal pWlatva) {
    if (pWlatva == null) {
      return;
    }
    wlatva = pWlatva.setScale(DECIMAL_WLATVA, RoundingMode.HALF_UP);
  }
  
  public void setWlatva(Integer pWlatva) {
    if (pWlatva == null) {
      return;
    }
    wlatva = BigDecimal.valueOf(pWlatva);
  }
  
  public Integer getWlatva() {
    return wlatva.intValue();
  }
  
  public void setWtautva(BigDecimal pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = pWtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWtautva(Double pWtautva) {
    if (pWtautva == null) {
      return;
    }
    wtautva = BigDecimal.valueOf(pWtautva).setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWtautva() {
    return wtautva.setScale(DECIMAL_WTAUTVA, RoundingMode.HALF_UP);
  }
  
  public void setWloprs(BigDecimal pWloprs) {
    if (pWloprs == null) {
      return;
    }
    wloprs = pWloprs.setScale(DECIMAL_WLOPRS, RoundingMode.HALF_UP);
  }
  
  public void setWloprs(Double pWloprs) {
    if (pWloprs == null) {
      return;
    }
    wloprs = BigDecimal.valueOf(pWloprs).setScale(DECIMAL_WLOPRS, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWloprs() {
    return wloprs.setScale(DECIMAL_WLOPRS, RoundingMode.HALF_UP);
  }
  
  public void setWkcsa(BigDecimal pWkcsa) {
    if (pWkcsa == null) {
      return;
    }
    wkcsa = pWkcsa.setScale(DECIMAL_WKCSA, RoundingMode.HALF_UP);
  }
  
  public void setWkcsa(Double pWkcsa) {
    if (pWkcsa == null) {
      return;
    }
    wkcsa = BigDecimal.valueOf(pWkcsa).setScale(DECIMAL_WKCSA, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWkcsa() {
    return wkcsa.setScale(DECIMAL_WKCSA, RoundingMode.HALF_UP);
  }
  
  public void setW7prbru(BigDecimal pW7prbru) {
    if (pW7prbru == null) {
      return;
    }
    w7prbru = pW7prbru.setScale(DECIMAL_W7PRBRU, RoundingMode.HALF_UP);
  }
  
  public void setW7prbru(Double pW7prbru) {
    if (pW7prbru == null) {
      return;
    }
    w7prbru = BigDecimal.valueOf(pW7prbru).setScale(DECIMAL_W7PRBRU, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7prbru() {
    return w7prbru.setScale(DECIMAL_W7PRBRU, RoundingMode.HALF_UP);
  }
  
  public void setW7prnet(BigDecimal pW7prnet) {
    if (pW7prnet == null) {
      return;
    }
    w7prnet = pW7prnet.setScale(DECIMAL_W7PRNET, RoundingMode.HALF_UP);
  }
  
  public void setW7prnet(Double pW7prnet) {
    if (pW7prnet == null) {
      return;
    }
    w7prnet = BigDecimal.valueOf(pW7prnet).setScale(DECIMAL_W7PRNET, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7prnet() {
    return w7prnet.setScale(DECIMAL_W7PRNET, RoundingMode.HALF_UP);
  }
  
  public void setW7prvfr(BigDecimal pW7prvfr) {
    if (pW7prvfr == null) {
      return;
    }
    w7prvfr = pW7prvfr.setScale(DECIMAL_W7PRVFR, RoundingMode.HALF_UP);
  }
  
  public void setW7prvfr(Double pW7prvfr) {
    if (pW7prvfr == null) {
      return;
    }
    w7prvfr = BigDecimal.valueOf(pW7prvfr).setScale(DECIMAL_W7PRVFR, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7prvfr() {
    return w7prvfr.setScale(DECIMAL_W7PRVFR, RoundingMode.HALF_UP);
  }
  
  public void setW7portf(BigDecimal pW7portf) {
    if (pW7portf == null) {
      return;
    }
    w7portf = pW7portf.setScale(DECIMAL_W7PORTF, RoundingMode.HALF_UP);
  }
  
  public void setW7portf(Double pW7portf) {
    if (pW7portf == null) {
      return;
    }
    w7portf = BigDecimal.valueOf(pW7portf).setScale(DECIMAL_W7PORTF, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7portf() {
    return w7portf.setScale(DECIMAL_W7PORTF, RoundingMode.HALF_UP);
  }
  
  public void setW7xxx1(BigDecimal pW7xxx1) {
    if (pW7xxx1 == null) {
      return;
    }
    w7xxx1 = pW7xxx1.setScale(DECIMAL_W7XXX1, RoundingMode.HALF_UP);
  }
  
  public void setW7xxx1(Double pW7xxx1) {
    if (pW7xxx1 == null) {
      return;
    }
    w7xxx1 = BigDecimal.valueOf(pW7xxx1).setScale(DECIMAL_W7XXX1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7xxx1() {
    return w7xxx1.setScale(DECIMAL_W7XXX1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfk1(BigDecimal pW7lfk1) {
    if (pW7lfk1 == null) {
      return;
    }
    w7lfk1 = pW7lfk1.setScale(DECIMAL_W7LFK1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfk1(Double pW7lfk1) {
    if (pW7lfk1 == null) {
      return;
    }
    w7lfk1 = BigDecimal.valueOf(pW7lfk1).setScale(DECIMAL_W7LFK1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7lfk1() {
    return w7lfk1.setScale(DECIMAL_W7LFK1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfv1(BigDecimal pW7lfv1) {
    if (pW7lfv1 == null) {
      return;
    }
    w7lfv1 = pW7lfv1.setScale(DECIMAL_W7LFV1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfv1(Double pW7lfv1) {
    if (pW7lfv1 == null) {
      return;
    }
    w7lfv1 = BigDecimal.valueOf(pW7lfv1).setScale(DECIMAL_W7LFV1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7lfv1() {
    return w7lfv1.setScale(DECIMAL_W7LFV1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfp1(BigDecimal pW7lfp1) {
    if (pW7lfp1 == null) {
      return;
    }
    w7lfp1 = pW7lfp1.setScale(DECIMAL_W7LFP1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfp1(Double pW7lfp1) {
    if (pW7lfp1 == null) {
      return;
    }
    w7lfp1 = BigDecimal.valueOf(pW7lfp1).setScale(DECIMAL_W7LFP1, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7lfp1() {
    return w7lfp1.setScale(DECIMAL_W7LFP1, RoundingMode.HALF_UP);
  }
  
  public void setW7lfk3(BigDecimal pW7lfk3) {
    if (pW7lfk3 == null) {
      return;
    }
    w7lfk3 = pW7lfk3.setScale(DECIMAL_W7LFK3, RoundingMode.HALF_UP);
  }
  
  public void setW7lfk3(Double pW7lfk3) {
    if (pW7lfk3 == null) {
      return;
    }
    w7lfk3 = BigDecimal.valueOf(pW7lfk3).setScale(DECIMAL_W7LFK3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7lfk3() {
    return w7lfk3.setScale(DECIMAL_W7LFK3, RoundingMode.HALF_UP);
  }
  
  public void setW7lfv3(BigDecimal pW7lfv3) {
    if (pW7lfv3 == null) {
      return;
    }
    w7lfv3 = pW7lfv3.setScale(DECIMAL_W7LFV3, RoundingMode.HALF_UP);
  }
  
  public void setW7lfv3(Double pW7lfv3) {
    if (pW7lfv3 == null) {
      return;
    }
    w7lfv3 = BigDecimal.valueOf(pW7lfv3).setScale(DECIMAL_W7LFV3, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getW7lfv3() {
    return w7lfv3.setScale(DECIMAL_W7LFV3, RoundingMode.HALF_UP);
  }
  
  public void setWtypart(String pWtypart) {
    if (pWtypart == null) {
      return;
    }
    wtypart = pWtypart;
  }
  
  public String getWtypart() {
    return wtypart;
  }
  
  public void setWlaqtai(BigDecimal pWlaqtai) {
    if (pWlaqtai == null) {
      return;
    }
    wlaqtai = pWlaqtai.setScale(DECIMAL_WLAQTAI, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtai(Double pWlaqtai) {
    if (pWlaqtai == null) {
      return;
    }
    wlaqtai = BigDecimal.valueOf(pWlaqtai).setScale(DECIMAL_WLAQTAI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtai() {
    return wlaqtai.setScale(DECIMAL_WLAQTAI, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtci(BigDecimal pWlaqtci) {
    if (pWlaqtci == null) {
      return;
    }
    wlaqtci = pWlaqtci.setScale(DECIMAL_WLAQTCI, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtci(Double pWlaqtci) {
    if (pWlaqtci == null) {
      return;
    }
    wlaqtci = BigDecimal.valueOf(pWlaqtci).setScale(DECIMAL_WLAQTCI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtci() {
    return wlaqtci.setScale(DECIMAL_WLAQTCI, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtsi(BigDecimal pWlaqtsi) {
    if (pWlaqtsi == null) {
      return;
    }
    wlaqtsi = pWlaqtsi.setScale(DECIMAL_WLAQTSI, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtsi(Double pWlaqtsi) {
    if (pWlaqtsi == null) {
      return;
    }
    wlaqtsi = BigDecimal.valueOf(pWlaqtsi).setScale(DECIMAL_WLAQTSI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtsi() {
    return wlaqtsi.setScale(DECIMAL_WLAQTSI, RoundingMode.HALF_UP);
  }
  
  public void setWlamhti(BigDecimal pWlamhti) {
    if (pWlamhti == null) {
      return;
    }
    wlamhti = pWlamhti.setScale(DECIMAL_WLAMHTI, RoundingMode.HALF_UP);
  }
  
  public void setWlamhti(Double pWlamhti) {
    if (pWlamhti == null) {
      return;
    }
    wlamhti = BigDecimal.valueOf(pWlamhti).setScale(DECIMAL_WLAMHTI, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlamhti() {
    return wlamhti.setScale(DECIMAL_WLAMHTI, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtat(BigDecimal pWlaqtat) {
    if (pWlaqtat == null) {
      return;
    }
    wlaqtat = pWlaqtat.setScale(DECIMAL_WLAQTAT, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtat(Double pWlaqtat) {
    if (pWlaqtat == null) {
      return;
    }
    wlaqtat = BigDecimal.valueOf(pWlaqtat).setScale(DECIMAL_WLAQTAT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtat() {
    return wlaqtat.setScale(DECIMAL_WLAQTAT, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtct(BigDecimal pWlaqtct) {
    if (pWlaqtct == null) {
      return;
    }
    wlaqtct = pWlaqtct.setScale(DECIMAL_WLAQTCT, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtct(Double pWlaqtct) {
    if (pWlaqtct == null) {
      return;
    }
    wlaqtct = BigDecimal.valueOf(pWlaqtct).setScale(DECIMAL_WLAQTCT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtct() {
    return wlaqtct.setScale(DECIMAL_WLAQTCT, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtst(BigDecimal pWlaqtst) {
    if (pWlaqtst == null) {
      return;
    }
    wlaqtst = pWlaqtst.setScale(DECIMAL_WLAQTST, RoundingMode.HALF_UP);
  }
  
  public void setWlaqtst(Double pWlaqtst) {
    if (pWlaqtst == null) {
      return;
    }
    wlaqtst = BigDecimal.valueOf(pWlaqtst).setScale(DECIMAL_WLAQTST, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlaqtst() {
    return wlaqtst.setScale(DECIMAL_WLAQTST, RoundingMode.HALF_UP);
  }
  
  public void setWlamhtt(BigDecimal pWlamhtt) {
    if (pWlamhtt == null) {
      return;
    }
    wlamhtt = pWlamhtt.setScale(DECIMAL_WLAMHTT, RoundingMode.HALF_UP);
  }
  
  public void setWlamhtt(Double pWlamhtt) {
    if (pWlamhtt == null) {
      return;
    }
    wlamhtt = BigDecimal.valueOf(pWlamhtt).setScale(DECIMAL_WLAMHTT, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWlamhtt() {
    return wlamhtt.setScale(DECIMAL_WLAMHTT, RoundingMode.HALF_UP);
  }
}
