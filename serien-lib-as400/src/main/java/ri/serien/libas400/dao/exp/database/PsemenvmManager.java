/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.exp.database;

import java.sql.Connection;
import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Gère les opérations sur le fichier (création, suppression, lecture, ...)
 */
public class PsemenvmManager extends QueryManager {
  
  /**
   * Constructeur
   * @param database
   */
  public PsemenvmManager(Connection database) {
    super(database);
  }
  
  /**
   * Retourne l'enregistrement pour un environnement
   * @param bibenv
   * @param prf
   * @return
   *
   */
  public GenericRecord getRecordForBibEnv(String bibenv) {
    GenericRecord psemenvm = null;
    ArrayList<GenericRecord> listeRecord;
    try {
      listeRecord = select("Select * from QGPL.psemenvm where ENVCLI = '" + bibenv + "'");
    }
    catch (Exception e) {
      throw new MessageErreurException("Requête SQL en erreur, merci de contacter le support");
    }
    
    if ((listeRecord != null) && (listeRecord.size() > 0)) {
      psemenvm = listeRecord.get(0);
    }
    else {
      throw new MessageErreurException(
          "[PsemenvmManagement] (getRecordForBibEnv) Environnement " + bibenv + " non trouvé dans le fichier QGPL.PSEMENVM");
    }
    
    return psemenvm;
  }
  
}
