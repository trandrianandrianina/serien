/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi;

import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;

public class Partenaire_EDI extends EntiteEDI {
  private String separateurMots = " ";
  
  private String[] ligneBrute = null;
  
  private String typePartenaire = null;
  private String codeEANPartenaire = null;
  private String codeInternePartenaire = null;
  private String raisonSocialePart = null;
  private String adresse1Part = null;
  private String adresse2Part = null;
  private String adresse3Part = null;
  private String cdpPart = null;
  private String villePart = null;
  private String codePays = null;
  private String libellePays = null;
  // Découpage logique du bloc adresse
  private String BENOM = null;
  private String BECPL = null;
  private String BERUE = null;
  private String BELOC = null;
  
  // Série N
  private String idSerieN = null;
  private String suffixeSerieN = null;
  
  public Partenaire_EDI(ParametresFluxBibli pParams, String[] ligne) {
    super(pParams);
    if (ligne == null) {
      return;
    }
    
    ligneBrute = ligne;
    
    isValide = traitementContenu();
  }
  
  @Override
  protected boolean traitementContenu() {
    if (ligneBrute == null || ligneBrute.length == 0) {
      majError("(Partenaire_EDI] traitementContenu() ligneBrute NULL");
      return false;
    }
    
    for (int i = 0; i < ligneBrute.length; i++) {
      switch (i) {
        case 1:
          typePartenaire = ligneBrute[i];
          break;
        case 2:
          codeEANPartenaire = ligneBrute[i];
          break;
        case 3:
          codeInternePartenaire = ligneBrute[i];
          break;
        case 4:
          raisonSocialePart = ligneBrute[i];
          break;
        case 5:
          adresse1Part = ligneBrute[i];
          break;
        case 6:
          adresse2Part = ligneBrute[i];
          break;
        case 7:
          adresse3Part = ligneBrute[i];
          break;
        case 8:
          cdpPart = ligneBrute[i];
          break;
        case 9:
          villePart = ligneBrute[i];
          break;
        case 10:
          codePays = ligneBrute[i];
          break;
        
        default:
          break;
      }
    }
    
    formaterBlocAdresse();
    interpreterLeCodePays();
    
    return true;
  }
  
  // TODO REMONTER CETTE FUCKING METHODE DANS ENTITEEDI ET ENTITE TOUT COURT D'AILLEURS
  // DANS L'OPTIQUE D'UN ALGO PLUS PROPRE
  /**
   * Formater le bloc adresse des partenaires EDI avec les fucking zones de Série N
   */
  public void formaterBlocAdresse() {
    // TODO A AMELIORER CAR LE DEBUT D'UN LIGNE PEUT SE TROUVER EN FIN DE LIGNE PRECEDENTE
    // Ensuite on découpe les chaines trop longues
    BENOM = raisonSocialePart.trim();
    
    // On checke si trop long
    if (BENOM != null && BENOM.length() > ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES) {
      String[] tab = decoupeLogiqueAdresses(BENOM);
      if (tab != null && tab.length == 2) {
        BENOM = tab[0];
        BECPL = tab[1];
      }
    }
    
    boolean cplISpourNOM = false;
    // Si le complément n'est pas complété
    if (BECPL == null) {
      // $$1 BECPL = blocAdresse;
      BECPL = adresse1Part.trim();
      if (BECPL != null && BECPL.length() > ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES) {
        String[] tab = decoupeLogiqueAdresses(BECPL);
        if (tab != null && tab.length == 2) {
          BECPL = tab[0].trim();
          // $$1 BERUE = tab[1];
          BERUE = tab[1].trim() + separateurMots + adresse2Part.trim();
        }
      }
      else {
        BERUE = adresse2Part.trim();
      }
    }
    // S'il s'agit du complément de raison sociale on tronque une longueur excessive
    else {
      cplISpourNOM = true;
      if (BECPL.length() > ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES) {
        BECPL = BECPL.substring(0, ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES);
      }
      
      // $$1 BERUE = blocAdresse;
      BERUE = adresse1Part.trim();
    }
    
    // Si la rue est déjà complétée par le surplus de longueur du CPL
    if (BERUE != null && BERUE.length() > ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES) {
      String[] tab = decoupeLogiqueAdresses(BERUE);
      if (tab != null && tab.length == 2) {
        BERUE = tab[0].trim();
        // $$1 BELOC = tab[1];
        if (cplISpourNOM) {
          BELOC = tab[1] + separateurMots + adresse2Part.trim() + separateurMots + adresse3Part.trim();
        }
        else {
          BELOC = tab[1] + separateurMots + adresse3Part.trim();
        }
      }
    }
    else {
      if (cplISpourNOM) {
        BELOC = adresse2Part.trim() + separateurMots + adresse3Part.trim();
      }
      else {
        BELOC = adresse3Part.trim();
      }
    }
    
    if (BELOC != null && BELOC.length() > ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES) {
      BELOC = BELOC.substring(0, ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES);
    }
    
    // PAS de NULL à l'affichage
    if (BENOM == null) {
      BENOM = "";
    }
    if (BECPL == null) {
      BECPL = "";
    }
    if (BERUE == null) {
      BERUE = "";
    }
    if (BELOC == null) {
      BELOC = "";
    }
  }
  
  /**
   * ON découpe une fucking chaine de carcère pour en faire deux blocs logiques
   */
  private String[] decoupeLogiqueAdresses(String ligneBrute) {
    if (ligneBrute == null || ligneBrute.length() <= ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES) {
      majError("(Partenaire_EDI] decoupeLogiqueAdresses() ligneBrute NULLE ");
      isValide = false;
      return null;
    }
    
    ligneBrute = ligneBrute.trim();
    
    String[] tabTemp = ligneBrute.split(separateurMots);
    String part1 = "";
    String part2 = "";
    // Si on a une découpe par des espaces
    if (tabTemp != null && tabTemp.length > 0) {
      // On change ligne
      boolean onChange = false;
      // Si il s'agit d'un seul mot (eh oui c'est fucking possibeul !!
      if (tabTemp.length == 1) {
        part1 = ligneBrute.substring(0, ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES).trim();
        part2 = ligneBrute.substring(ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES).trim();
      }
      else {
        for (int i = 0; i < tabTemp.length; i++) {
          if (tabTemp[i] != null && !tabTemp[i].isEmpty()) {
            if ((part1.length() + tabTemp[i].trim().length() + separateurMots.length()) <= ParametresFluxInit.TAILLE_MAX_CHAMPS_ADRESSES
                && !onChange) {
              part1 = part1 + tabTemp[i].trim() + separateurMots;
            }
            else {
              onChange = true;
              part2 = part2 + tabTemp[i].trim() + separateurMots;
            }
          }
        }
      }
    }
    else {
      majError("(Partenaire_EDI] decoupeLogiqueAdresses() tabTemp NULL ");
      isValide = false;
    }
    
    String[] tabFinal = { part1, part2 };
    if (tabFinal.length != 2) {
      return null;
    }
    
    return tabFinal;
  }
  
  private void interpreterLeCodePays() {
    if (codePays == null || codePays.trim().equals("")) {
      libellePays = "FRANCE";
    }
    else {
      if (codePays.trim().equals("FR")) {
        libellePays = "FRANCE";
      }
      if (codePays.trim().equals("ES")) {
        libellePays = "ESPAGNE";
      }
    }
  }
  
  public String[] getLigneBrute() {
    return ligneBrute;
  }
  
  public void setLigneBrute(String[] ligneBrute) {
    this.ligneBrute = ligneBrute;
  }
  
  public String getTypePartenaire() {
    return typePartenaire;
  }
  
  public void setTypePartenaire(String typePartenaire) {
    this.typePartenaire = typePartenaire;
  }
  
  public String getCodeEANPartenaire() {
    return codeEANPartenaire;
  }
  
  public void setCodeEANPartenaire(String codeEANPartenaire) {
    this.codeEANPartenaire = codeEANPartenaire;
  }
  
  public String getCodeInternePartenaire() {
    return codeInternePartenaire;
  }
  
  public void setCodeInternePartenaire(String codeInternePartenaire) {
    this.codeInternePartenaire = codeInternePartenaire;
  }
  
  public String getCdpPart() {
    return cdpPart;
  }
  
  public void setCdpPart(String cdpPart) {
    this.cdpPart = cdpPart;
  }
  
  public String getVillePart() {
    return villePart;
  }
  
  public void setVillePart(String villePart) {
    this.villePart = villePart;
  }
  
  public String getLibellePays() {
    return libellePays;
  }
  
  public String getIdSerieN() {
    return idSerieN;
  }
  
  public void setIdSerieN(String idSerieN) {
    this.idSerieN = idSerieN;
  }
  
  public String getSuffixeSerieN() {
    return suffixeSerieN;
  }
  
  public void setSuffixeSerieN(String suffixeSerieN) {
    this.suffixeSerieN = suffixeSerieN;
  }
  
  public String getBENOM() {
    return BENOM;
  }
  
  public void setBENOM(String bENOM) {
    BENOM = bENOM;
  }
  
  public String getBECPL() {
    return BECPL;
  }
  
  public void setBECPL(String bECPL) {
    BECPL = bECPL;
  }
  
  public String getBERUE() {
    return BERUE;
  }
  
  public void setBERUE(String bERUE) {
    BERUE = bERUE;
  }
  
  public String getBELOC() {
    return BELOC;
  }
  
  public void setBELOC(String bELOC) {
    BELOC = bELOC;
  }
  
}
