/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.historiqueventesclientarticle;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;

import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0020d {
  // Constantes
  public static final int SIZE_WETB = 3;
  public static final int SIZE_WNUM = 6;
  public static final int DECIMAL_WNUM = 0;
  public static final int SIZE_WSUF = 1;
  public static final int DECIMAL_WSUF = 0;
  public static final int SIZE_WTYP = 3;
  public static final int SIZE_WDAT = 7;
  public static final int DECIMAL_WDAT = 0;
  public static final int SIZE_WETA = 1;
  public static final int DECIMAL_WETA = 0;
  public static final int SIZE_WVDE = 3;
  public static final int SIZE_WFAC = 7;
  public static final int DECIMAL_WFAC = 0;
  public static final int SIZE_WNFA = 7;
  public static final int DECIMAL_WNFA = 0;
  public static final int SIZE_WQTE = 11;
  public static final int DECIMAL_WQTE = 3;
  public static final int SIZE_WPVB = 9;
  public static final int DECIMAL_WPVB = 2;
  public static final int SIZE_WPVN = 9;
  public static final int DECIMAL_WPVN = 2;
  public static final int SIZE_WPVC = 9;
  public static final int DECIMAL_WPVC = 2;
  public static final int SIZE_WENLV = 1;
  public static final int SIZE_TOTALE_DS = 77;
  public static final int SIZE_FILLER = 200 - 77;
  
  // Constantes indices Nom DS
  public static final int VAR_WETB = 0;
  public static final int VAR_WNUM = 1;
  public static final int VAR_WSUF = 2;
  public static final int VAR_WTYP = 3;
  public static final int VAR_WDAT = 4;
  public static final int VAR_WETA = 5;
  public static final int VAR_WVDE = 6;
  public static final int VAR_WFAC = 7;
  public static final int VAR_WNFA = 8;
  public static final int VAR_WQTE = 9;
  public static final int VAR_WPVB = 10;
  public static final int VAR_WPVN = 11;
  public static final int VAR_WPVC = 12;
  public static final int VAR_WENLV = 13;
  
  // Variables AS400
  private String wetb = ""; //
  private BigDecimal wnum = BigDecimal.ZERO; //
  private BigDecimal wsuf = BigDecimal.ZERO; //
  private String wtyp = ""; // Type de documents
  private BigDecimal wdat = BigDecimal.ZERO; //
  private BigDecimal weta = BigDecimal.ZERO; //
  private String wvde = ""; // Code vendeur
  private BigDecimal wfac = BigDecimal.ZERO; // Date facture
  private BigDecimal wnfa = BigDecimal.ZERO; // Numéro de facture
  private BigDecimal wqte = BigDecimal.ZERO; // Quantité Commandée
  private BigDecimal wpvb = BigDecimal.ZERO; // Prix de Vente de Base
  private BigDecimal wpvn = BigDecimal.ZERO; // Prix de Vente Net
  private BigDecimal wpvc = BigDecimal.ZERO; // Prix de Vente Calculé
  private String wenlv = ""; // Enlevement ou livraison
  private String filler = "";
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_WETB), //
      new AS400ZonedDecimal(SIZE_WNUM, DECIMAL_WNUM), //
      new AS400ZonedDecimal(SIZE_WSUF, DECIMAL_WSUF), //
      new AS400Text(SIZE_WTYP), // Type de documents
      new AS400ZonedDecimal(SIZE_WDAT, DECIMAL_WDAT), //
      new AS400ZonedDecimal(SIZE_WETA, DECIMAL_WETA), //
      new AS400Text(SIZE_WVDE), // Code vendeur
      new AS400ZonedDecimal(SIZE_WFAC, DECIMAL_WFAC), // Date facture
      new AS400ZonedDecimal(SIZE_WNFA, DECIMAL_WNFA), // Numéro de facture
      new AS400ZonedDecimal(SIZE_WQTE, DECIMAL_WQTE), // Quantité Commandée
      new AS400ZonedDecimal(SIZE_WPVB, DECIMAL_WPVB), // Prix de Vente de Base
      new AS400ZonedDecimal(SIZE_WPVN, DECIMAL_WPVN), // Prix de Vente Net
      new AS400ZonedDecimal(SIZE_WPVC, DECIMAL_WPVC), // Prix de Vente Calculé
      new AS400Text(SIZE_WENLV), // Enlevement ou livraison
      new AS400Text(SIZE_FILLER), // Filler
  };
  private AS400Structure ds = new AS400Structure(structure);
  public Object[] o = { wetb, wnum, wsuf, wtyp, wdat, weta, wvde, wfac, wnfa, wqte, wpvb, wpvn, wpvc, wenlv, filler, };
  
  // -- Accesseurs
  
  public void setWetb(String pWetb) {
    if (pWetb == null) {
      return;
    }
    wetb = pWetb;
  }
  
  public String getWetb() {
    return wetb;
  }
  
  public void setWnum(BigDecimal pWnum) {
    if (pWnum == null) {
      return;
    }
    wnum = pWnum.setScale(DECIMAL_WNUM, RoundingMode.HALF_UP);
  }
  
  public void setWnum(Integer pWnum) {
    if (pWnum == null) {
      return;
    }
    wnum = BigDecimal.valueOf(pWnum);
  }
  
  public Integer getWnum() {
    return wnum.intValue();
  }
  
  public void setWsuf(BigDecimal pWsuf) {
    if (pWsuf == null) {
      return;
    }
    wsuf = pWsuf.setScale(DECIMAL_WSUF, RoundingMode.HALF_UP);
  }
  
  public void setWsuf(Integer pWsuf) {
    if (pWsuf == null) {
      return;
    }
    wsuf = BigDecimal.valueOf(pWsuf);
  }
  
  public Integer getWsuf() {
    return wsuf.intValue();
  }
  
  public void setWtyp(String pWtyp) {
    if (pWtyp == null) {
      return;
    }
    wtyp = pWtyp;
  }
  
  public String getWtyp() {
    return wtyp;
  }
  
  public void setWdat(BigDecimal pWdat) {
    if (pWdat == null) {
      return;
    }
    wdat = pWdat.setScale(DECIMAL_WDAT, RoundingMode.HALF_UP);
  }
  
  public void setWdat(Integer pWdat) {
    if (pWdat == null) {
      return;
    }
    wdat = BigDecimal.valueOf(pWdat);
  }
  
  public void setWdat(Date pWdat) {
    if (pWdat == null) {
      return;
    }
    wdat = BigDecimal.valueOf(ConvertDate.dateToDb2(pWdat));
  }
  
  public Integer getWdat() {
    return wdat.intValue();
  }
  
  public Date getWdatConvertiEnDate() {
    return ConvertDate.db2ToDate(wdat.intValue(), null);
  }
  
  public void setWeta(BigDecimal pWeta) {
    if (pWeta == null) {
      return;
    }
    weta = pWeta.setScale(DECIMAL_WETA, RoundingMode.HALF_UP);
  }
  
  public void setWeta(Integer pWeta) {
    if (pWeta == null) {
      return;
    }
    weta = BigDecimal.valueOf(pWeta);
  }
  
  public Integer getWeta() {
    return weta.intValue();
  }
  
  public void setWvde(String pWvde) {
    if (pWvde == null) {
      return;
    }
    wvde = pWvde;
  }
  
  public String getWvde() {
    return wvde;
  }
  
  public void setWfac(BigDecimal pWfac) {
    if (pWfac == null) {
      return;
    }
    wfac = pWfac.setScale(DECIMAL_WFAC, RoundingMode.HALF_UP);
  }
  
  public void setWfac(Integer pWfac) {
    if (pWfac == null) {
      return;
    }
    wfac = BigDecimal.valueOf(pWfac);
  }
  
  public void setWfac(Date pWfac) {
    if (pWfac == null) {
      return;
    }
    wfac = BigDecimal.valueOf(ConvertDate.dateToDb2(pWfac));
  }
  
  public Integer getWfac() {
    return wfac.intValue();
  }
  
  public Date getWfacConvertiEnDate() {
    return ConvertDate.db2ToDate(wfac.intValue(), null);
  }
  
  public void setWnfa(BigDecimal pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = pWnfa.setScale(DECIMAL_WNFA, RoundingMode.HALF_UP);
  }
  
  public void setWnfa(Integer pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = BigDecimal.valueOf(pWnfa);
  }
  
  public void setWnfa(Date pWnfa) {
    if (pWnfa == null) {
      return;
    }
    wnfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pWnfa));
  }
  
  public Integer getWnfa() {
    return wnfa.intValue();
  }
  
  public Date getWnfaConvertiEnDate() {
    return ConvertDate.db2ToDate(wnfa.intValue(), null);
  }
  
  public void setWqte(BigDecimal pWqte) {
    if (pWqte == null) {
      return;
    }
    wqte = pWqte.setScale(DECIMAL_WQTE, RoundingMode.HALF_UP);
  }
  
  public void setWqte(Double pWqte) {
    if (pWqte == null) {
      return;
    }
    wqte = BigDecimal.valueOf(pWqte).setScale(DECIMAL_WQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWqte() {
    return wqte.setScale(DECIMAL_WQTE, RoundingMode.HALF_UP);
  }
  
  public void setWpvb(BigDecimal pWpvb) {
    if (pWpvb == null) {
      return;
    }
    wpvb = pWpvb.setScale(DECIMAL_WPVB, RoundingMode.HALF_UP);
  }
  
  public void setWpvb(Double pWpvb) {
    if (pWpvb == null) {
      return;
    }
    wpvb = BigDecimal.valueOf(pWpvb).setScale(DECIMAL_WPVB, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvb() {
    return wpvb.setScale(DECIMAL_WPVB, RoundingMode.HALF_UP);
  }
  
  public void setWpvn(BigDecimal pWpvn) {
    if (pWpvn == null) {
      return;
    }
    wpvn = pWpvn.setScale(DECIMAL_WPVN, RoundingMode.HALF_UP);
  }
  
  public void setWpvn(Double pWpvn) {
    if (pWpvn == null) {
      return;
    }
    wpvn = BigDecimal.valueOf(pWpvn).setScale(DECIMAL_WPVN, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvn() {
    return wpvn.setScale(DECIMAL_WPVN, RoundingMode.HALF_UP);
  }
  
  public void setWpvc(BigDecimal pWpvc) {
    if (pWpvc == null) {
      return;
    }
    wpvc = pWpvc.setScale(DECIMAL_WPVC, RoundingMode.HALF_UP);
  }
  
  public void setWpvc(Double pWpvc) {
    if (pWpvc == null) {
      return;
    }
    wpvc = BigDecimal.valueOf(pWpvc).setScale(DECIMAL_WPVC, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getWpvc() {
    return wpvc.setScale(DECIMAL_WPVC, RoundingMode.HALF_UP);
  }
  
  public void setWenlv(Character pWenlv) {
    if (pWenlv == null) {
      return;
    }
    wenlv = String.valueOf(pWenlv);
  }
  
  public Character getWenlv() {
    return wenlv.charAt(0);
  }
}
