/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libcommun.outils.Constantes;

public class JsonAdresseWebV3 {
  // Constantes
  private static final int LONGUEUR_TELEPHONE = 20;
  
  // Détail d'une adresse de livraison
  private String idClientLivraison = null;
  private String societeLivraison = null;
  private String civiliteLivraison = null;
  private String nomLivraison = null;
  private String complementNomLivraison = null;
  private String prenomLivraison = null;
  private String rueLivraison = null;
  private String rue2Livraison = null;
  private String cdpLivraison = null;
  private String villeLivraison = null;
  private String paysLivraison = null;
  private String libellePaysLivraison = null;
  private String telLivraison = null;
  
  private transient boolean isParticulier = false;
  private transient String CLCLI = null;
  private transient String CLLIV = null;
  private transient String CLNOM = null;
  private transient String CLRUE = null;
  private transient String CLLOC = null;
  private transient String CLCDP1 = null;
  private transient String CLVIL = null;
  private transient String CLTEL = null;
  
  // -- Méthodes publiques
  
  /**
   * Permet de découper l'algorithme de Série N pour le cdp et la ville sur la zone CLVIL.
   */
  public void decouperVilleEtCDP(String zoneMoisie) {
    if (zoneMoisie == null || zoneMoisie.trim().length() < 7) {
      cdpLivraison = "";
      villeLivraison = "";
      return;
    }
    
    cdpLivraison = zoneMoisie.substring(0, 5);
    villeLivraison = zoneMoisie.substring(6);
  }
  
  /**
   * Vérification que le code SérieNLiv transmis soit bien formaté et on l'attribue aux zones code client et suffixe client du client
   * livré.
   */
  public boolean decouperCodeERP() {
    if (idClientLivraison == null || idClientLivraison.trim().equals("")) {
      return false;
    }
    
    String[] tab = idClientLivraison.split("/");
    if (tab != null && tab.length == 2) {
      CLCLI = tab[0];
      CLLIV = tab[1];
      
      if (CLCLI.length() > 0 && CLLIV.length() > 0) {
        return true;
      }
    }
    
    return false;
  }
  
  /**
   * Mise en forme du bloc adresse en import de données depuis SFCC.
   */
  public void miseEnFormeBlocAdresseImport(int pParticulier) {
    CLNOM = "";
    
    // Si on a affaire à un particulier on va concaténer le nom et le prénom dans la même zone
    // Les véritables données sont dans le contact
    if (pParticulier == JsonClientWebV3.ISPARTICULIER) {
      if (nomLivraison != null) {
        CLNOM = nomLivraison.trim();
      }
      if (prenomLivraison != null) {
        CLNOM += " " + prenomLivraison.trim();
      }
      CLNOM = OutilsMagento.formaterTexteLongueurMax(CLNOM, 30);
      complementNomLivraison = "";
    }
    // Un pro (alors on checke la zone "nom")
    else {
      if (societeLivraison != null) {
        if (societeLivraison.trim().length() < 30) {
          CLNOM = societeLivraison.trim();
        }
        else {
          // On réduit à 60 Max (2 x 30)
          societeLivraison = OutilsMagento.formaterTexteLongueurMax(societeLivraison.trim(), 60);
          CLNOM = societeLivraison.substring(0, 30);
        }
        if (complementNomLivraison != null) {
          complementNomLivraison = OutilsMagento.formaterTexteLongueurMax(complementNomLivraison, 30);
        }
      }
    }
    
    CLRUE = "";
    CLLOC = "";
    
    if (rueLivraison != null) {
      CLRUE = rueLivraison;
    }
    if (rue2Livraison != null) {
      CLLOC = rue2Livraison;
    }
    
    if (cdpLivraison == null) {
      CLCDP1 = "00000";
    }
    else {
      CLCDP1 = OutilsMagento.formaterUnCodePostalpourSN(cdpLivraison);
    }
    
    if (villeLivraison == null) {
      CLVIL = CLCDP1 + "";
    }
    else {
      CLVIL = CLCDP1 + " " + OutilsMagento.formaterTexteLongueurMax(villeLivraison, 24);
    }
    
    // Le numéro de téléphone
    CLTEL = Constantes.normerTexte(telLivraison);
    if (CLTEL.length() > LONGUEUR_TELEPHONE) {
      CLTEL = CLTEL.substring(0, LONGUEUR_TELEPHONE);
    }
  }
  
  // -- Accesseurs
  
  public String getIdClientLivraison() {
    return idClientLivraison;
  }
  
  public void setIdClientLivraison(String pIdClientLivraison) {
    this.idClientLivraison = pIdClientLivraison;
  }
  
  public String getSocieteLivraison() {
    return societeLivraison;
  }
  
  public void setSocieteLivraison(String societeLivraison) {
    this.societeLivraison = societeLivraison;
  }
  
  public String getCiviliteLivraison() {
    return civiliteLivraison;
  }
  
  public void setCiviliteLivraison(String civiliteLivraison) {
    this.civiliteLivraison = civiliteLivraison;
  }
  
  public String getNomLivraison() {
    return nomLivraison;
  }
  
  public void setNomLivraison(String nomLivraison) {
    this.nomLivraison = nomLivraison;
  }
  
  public String getComplementNomLivraison() {
    return complementNomLivraison;
  }
  
  public void setComplementNomLivraison(String complementNomLivraison) {
    this.complementNomLivraison = complementNomLivraison;
  }
  
  public String getPrenomLiv() {
    return prenomLivraison;
  }
  
  public void setPrenomLivraison(String prenomLivraison) {
    this.prenomLivraison = prenomLivraison;
  }
  
  public String getRueLivraison() {
    return rueLivraison;
  }
  
  public void setRueLivraison(String rueLivraison) {
    this.rueLivraison = rueLivraison;
  }
  
  public String getRue2Livraison() {
    return rue2Livraison;
  }
  
  public void setRue2Livraison(String rue2Livraison) {
    this.rue2Livraison = rue2Livraison;
  }
  
  public String getCdpLivraison() {
    return cdpLivraison;
  }
  
  public void setCdpLivraison(String cdpLivraison) {
    this.cdpLivraison = cdpLivraison;
  }
  
  public String getVilleLivraison() {
    return villeLivraison;
  }
  
  public void setVilleLivraison(String villeLivraison) {
    this.villeLivraison = villeLivraison;
  }
  
  public String getPaysLivraison() {
    return paysLivraison;
  }
  
  public void setPaysLivraison(String paysLivraison) {
    this.paysLivraison = paysLivraison;
  }
  
  public String getLibellePaysLivraison() {
    return libellePaysLivraison;
  }
  
  public void setLibellePaysLivraison(String libPaysLivraison) {
    this.libellePaysLivraison = libPaysLivraison;
  }
  
  public boolean isParticulier() {
    return isParticulier;
  }
  
  public void setParticulier(boolean isParticulier) {
    this.isParticulier = isParticulier;
  }
  
  public String getTelLivraison() {
    return telLivraison;
  }
  
  public void setTelLivraison(String telLivraison) {
    this.telLivraison = telLivraison;
  }
  
  public String getCLCLI() {
    return CLCLI;
  }
  
  public String getCLLIV() {
    return CLLIV;
  }
  
  public String getCLNOM() {
    return CLNOM;
  }
  
  public String getCLRUE() {
    return CLRUE;
  }
  
  public String getCLLOC() {
    return CLLOC;
  }
  
  public String getCLCDP1() {
    return CLCDP1;
  }
  
  public String getCLVIL() {
    return CLVIL;
  }
  
  public String getCLTEL() {
    return CLTEL;
  }
}
