/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.lignevente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0054i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PICLI = 6;
  public static final int DECIMAL_PICLI = 0;
  public static final int SIZE_PILIV = 3;
  public static final int DECIMAL_PILIV = 0;
  public static final int SIZE_PITYP = 3;
  public static final int SIZE_PILIG = 3;
  public static final int DECIMAL_PILIG = 0;
  public static final int SIZE_PIPAG = 3;
  public static final int DECIMAL_PIPAG = 0;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PIDEBC = 7;
  public static final int DECIMAL_PIDEBC = 0;
  public static final int SIZE_PIFINC = 7;
  public static final int DECIMAL_PIFINC = 0;
  public static final int SIZE_PIART = 20;
  public static final int SIZE_PIMAG = 2;
  public static final int SIZE_PIQTE = 11;
  public static final int DECIMAL_PIQTE = 3;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 85;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PIETB = 1;
  public static final int VAR_PICLI = 2;
  public static final int VAR_PILIV = 3;
  public static final int VAR_PITYP = 4;
  public static final int VAR_PILIG = 5;
  public static final int VAR_PIPAG = 6;
  public static final int VAR_PINUM = 7;
  public static final int VAR_PIDEBC = 8;
  public static final int VAR_PIFINC = 9;
  public static final int VAR_PIART = 10;
  public static final int VAR_PIMAG = 11;
  public static final int VAR_PIQTE = 12;
  public static final int VAR_PIARR = 13;
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String pietb = ""; // Code établissement
  private BigDecimal picli = BigDecimal.ZERO; // Numéro client
  private BigDecimal piliv = BigDecimal.ZERO; // Suffixe livraison client
  private String pityp = ""; // Type de documents
  private BigDecimal pilig = BigDecimal.ZERO; // Nombre de ligne par page
  private BigDecimal pipag = BigDecimal.ZERO; // Numéro de la page
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de bon
  private BigDecimal pidebc = BigDecimal.ZERO; // Date début création document
  private BigDecimal pifinc = BigDecimal.ZERO; // Date fin création document
  private String piart = ""; // Article
  private String pimag = ""; // Magasin
  private BigDecimal piqte = BigDecimal.ZERO; // Quantité si retour
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PICLI, DECIMAL_PICLI), // Numéro client
      new AS400ZonedDecimal(SIZE_PILIV, DECIMAL_PILIV), // Suffixe livraison client
      new AS400Text(SIZE_PITYP), // Type de documents
      new AS400ZonedDecimal(SIZE_PILIG, DECIMAL_PILIG), // Nombre de ligne par page
      new AS400ZonedDecimal(SIZE_PIPAG, DECIMAL_PIPAG), // Numéro de la page
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de bon
      new AS400ZonedDecimal(SIZE_PIDEBC, DECIMAL_PIDEBC), // Date début création document
      new AS400ZonedDecimal(SIZE_PIFINC, DECIMAL_PIFINC), // Date fin création document
      new AS400Text(SIZE_PIART), // Article
      new AS400Text(SIZE_PIMAG), // Magasin
      new AS400ZonedDecimal(SIZE_PIQTE, DECIMAL_PIQTE), // Quantité si retour
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      
      Object[] o = { piind, pietb, picli, piliv, pityp, pilig, pipag, pinum, pidebc, pifinc, piart, pimag, piqte, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    pietb = (String) output[1];
    picli = (BigDecimal) output[2];
    piliv = (BigDecimal) output[3];
    pityp = (String) output[4];
    pilig = (BigDecimal) output[5];
    pipag = (BigDecimal) output[6];
    pinum = (BigDecimal) output[7];
    pidebc = (BigDecimal) output[8];
    pifinc = (BigDecimal) output[9];
    piart = (String) output[10];
    pimag = (String) output[11];
    piqte = (BigDecimal) output[12];
    piarr = (String) output[13];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPicli(BigDecimal pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = pPicli.setScale(DECIMAL_PICLI, RoundingMode.HALF_UP);
  }
  
  public void setPicli(Integer pPicli) {
    if (pPicli == null) {
      return;
    }
    picli = BigDecimal.valueOf(pPicli);
  }
  
  public Integer getPicli() {
    return picli.intValue();
  }
  
  public void setPiliv(BigDecimal pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = pPiliv.setScale(DECIMAL_PILIV, RoundingMode.HALF_UP);
  }
  
  public void setPiliv(Integer pPiliv) {
    if (pPiliv == null) {
      return;
    }
    piliv = BigDecimal.valueOf(pPiliv);
  }
  
  public Integer getPiliv() {
    return piliv.intValue();
  }
  
  public void setPityp(String pPityp) {
    if (pPityp == null) {
      return;
    }
    pityp = pPityp;
  }
  
  public String getPityp() {
    return pityp;
  }
  
  public void setPilig(BigDecimal pPilig) {
    if (pPilig == null) {
      return;
    }
    pilig = pPilig.setScale(DECIMAL_PILIG, RoundingMode.HALF_UP);
  }
  
  public void setPilig(Integer pPilig) {
    if (pPilig == null) {
      return;
    }
    pilig = BigDecimal.valueOf(pPilig);
  }
  
  public Integer getPilig() {
    return pilig.intValue();
  }
  
  public void setPipag(BigDecimal pPipag) {
    if (pPipag == null) {
      return;
    }
    pipag = pPipag.setScale(DECIMAL_PIPAG, RoundingMode.HALF_UP);
  }
  
  public void setPipag(Integer pPipag) {
    if (pPipag == null) {
      return;
    }
    pipag = BigDecimal.valueOf(pPipag);
  }
  
  public Integer getPipag() {
    return pipag.intValue();
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPidebc(BigDecimal pPidebc) {
    if (pPidebc == null) {
      return;
    }
    pidebc = pPidebc.setScale(DECIMAL_PIDEBC, RoundingMode.HALF_UP);
  }
  
  public void setPidebc(Integer pPidebc) {
    if (pPidebc == null) {
      return;
    }
    pidebc = BigDecimal.valueOf(pPidebc);
  }
  
  public void setPidebc(Date pPidebc) {
    if (pPidebc == null) {
      return;
    }
    pidebc = BigDecimal.valueOf(ConvertDate.dateToDb2(pPidebc));
  }
  
  public Integer getPidebc() {
    return pidebc.intValue();
  }
  
  public Date getPidebcConvertiEnDate() {
    return ConvertDate.db2ToDate(pidebc.intValue(), null);
  }
  
  public void setPifinc(BigDecimal pPifinc) {
    if (pPifinc == null) {
      return;
    }
    pifinc = pPifinc.setScale(DECIMAL_PIFINC, RoundingMode.HALF_UP);
  }
  
  public void setPifinc(Integer pPifinc) {
    if (pPifinc == null) {
      return;
    }
    pifinc = BigDecimal.valueOf(pPifinc);
  }
  
  public void setPifinc(Date pPifinc) {
    if (pPifinc == null) {
      return;
    }
    pifinc = BigDecimal.valueOf(ConvertDate.dateToDb2(pPifinc));
  }
  
  public Integer getPifinc() {
    return pifinc.intValue();
  }
  
  public Date getPifincConvertiEnDate() {
    return ConvertDate.db2ToDate(pifinc.intValue(), null);
  }
  
  public void setPiart(String pPiart) {
    if (pPiart == null) {
      return;
    }
    piart = pPiart;
  }
  
  public String getPiart() {
    return piart;
  }
  
  public void setPimag(String pPimag) {
    if (pPimag == null) {
      return;
    }
    pimag = pPimag;
  }
  
  public String getPimag() {
    return pimag;
  }
  
  public void setPiqte(BigDecimal pPiqte) {
    if (pPiqte == null) {
      return;
    }
    piqte = pPiqte.setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public void setPiqte(Double pPiqte) {
    if (pPiqte == null) {
      return;
    }
    piqte = BigDecimal.valueOf(pPiqte).setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public BigDecimal getPiqte() {
    return piqte.setScale(DECIMAL_PIQTE, RoundingMode.HALF_UP);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
