/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentachat.recherche;

import java.math.BigDecimal;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatBase;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchatBase;
import ri.serien.libcommun.gescom.achat.document.prix.ParametrePrixAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.document.ListeRemise;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;

/**
 * Centralise les accès aux programmes RPG permettant de manipuler les lignes des documents de ventes.
 */
public class RpgLigneAchatBase extends RpgBase {
  // Constantes
  private static final String SVGAM0059_CHARGER_LISTE_LIGNE_ACHAT_BASE = "SVGAM0059";
  
  /**
   * Charger les lignes d'achat correspondant aux identifiants passés en paramètre.
   */
  public ListeLigneAchatBase chargerListeLigneAchatBase(SystemeManager pSystemManager, List<IdLigneAchat> pListeIdLigneAchat) {
    
    // Préparation des paramètres du programme RPG
    Svgam0059i entree = new Svgam0059i();
    Svgam0059o sortie = new Svgam0059o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    ListeLigneAchatBase listeLigneAchatBase = new ListeLigneAchatBase();
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pListeIdLigneAchat.get(0).getCodeEtablissement());
    
    int index = 0;
    entree.setPiid01(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid02(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid03(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid04(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid05(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid06(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid07(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid08(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid09(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid10(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid11(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid12(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid13(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid14(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid15(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid16(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid17(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid18(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid19(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid20(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid21(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid22(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid23(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid24(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid25(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid26(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid27(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid28(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid29(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid30(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid31(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid32(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid33(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid34(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid35(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid36(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid37(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid38(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid39(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid40(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid41(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid42(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid43(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid44(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid45(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid46(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid47(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid48(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid49(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid50(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid51(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid52(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid53(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid54(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid55(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid56(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid57(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid58(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid59(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid60(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid61(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid62(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid63(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid64(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    entree.setPiid65(alimenterIdLigneAchat(pListeIdLigneAchat, index++));
    
    // Initialiser des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    // Préparer de l'environnement
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSystemManager);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx(),
        EnvironnementExecution.getGamas());
    
    // Exécuter le programme RPG
    if (!rpg.executerProgramme(SVGAM0059_CHARGER_LISTE_LIGNE_ACHAT_BASE, EnvironnementExecution.getGamas(), parameterList)) {
      throw new MessageErreurException("Erreur lors du chargement des lignes");
    }
    
    // Récupérer les paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Vérifier la présence d'erreurs
    controlerErreur();
    
    // Renseigner le résultat
    for (Object ligne : sortie.getValeursBrutesLignes()) {
      if (ligne == null) {
        break;
      }
      listeLigneAchatBase.add(traiterUneLigne((Object[]) ligne));
    }
    
    return listeLigneAchatBase;
    
  }
  
  /**
   * Découpage d'une ligne.
   */
  private LigneAchatBase traiterUneLigne(Object[] ligneBrute) {
    if (ligneBrute == null) {
      return null;
    }
    
    // Générer l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) ligneBrute[Svgam0059d.VAR_WLAETB]);
    
    // Génèrer l'identifiant de la ligne de ventes
    EnumCodeEnteteDocumentAchat codeEntete =
        EnumCodeEnteteDocumentAchat.valueOfByCode(((String) ligneBrute[Svgam0059d.VAR_WLACOD]).charAt(0));
    Integer numero = ((BigDecimal) ligneBrute[Svgam0059d.VAR_WLANUM]).intValue();
    Integer suffixe = ((BigDecimal) ligneBrute[Svgam0059d.VAR_WLASUF]).intValue();
    Integer numeroLigne = ((BigDecimal) ligneBrute[Svgam0059d.VAR_WLANLI]).intValue();
    IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(idEtablissement, codeEntete, numero, suffixe, numeroLigne);
    
    // Créer la ligne de ventes
    LigneAchatBase ligneAchatBase = new LigneAchatBase(idLigneAchat);
    
    // Libellé de la ligne
    ligneAchatBase.setLibelleArticle((String) ligneBrute[Svgam0059d.VAR_WLIGLB]);
    
    // Type de ligne
    ligneAchatBase.setTypeLigne(EnumTypeLigneAchat.valueOfByCode(((String) ligneBrute[Svgam0059d.VAR_WLAERL]).charAt(0)));
    
    // Type d'article
    ligneAchatBase.setTypeArticle(EnumTypeArticle.valueOfByCodeAlpha(((String) ligneBrute[Svgam0059d.VAR_WTYPART])));
    
    // Date de livraison
    ligneAchatBase.setDateLivraisonPrevue(ConvertDate.db2ToDate(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLADLP]).intValue(), null));
    
    // Alimentation de la classe ParamètrePrixAchat avec les données retournées
    ParametrePrixAchat parametre = new ParametrePrixAchat();
    
    // Renseigner les unités
    if (!((String) ligneBrute[Svgam0059d.VAR_WLAUNA]).trim().isEmpty()) {
      IdUnite idUA = IdUnite.getInstance((String) ligneBrute[Svgam0059d.VAR_WLAUNA]);
      parametre.setIdUA(idUA);
    }
    if (!((String) ligneBrute[Svgam0059d.VAR_WLAUNC]).trim().isEmpty()) {
      IdUnite idUCA = IdUnite.getInstance((String) ligneBrute[Svgam0059d.VAR_WLAUNC]);
      parametre.setIdUCA(idUCA);
    }
    parametre.setNombreDecimaleUA(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLADCA]).intValue());
    parametre.setNombreDecimaleUCA(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLADCC]).intValue());
    parametre.setNombreDecimaleUS(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLADCS]).intValue());
    
    // Renseigner les ratios
    parametre.setNombreUAParUCA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAKAC]);
    parametre.setNombreUSParUCA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAKSC]);
    
    // Renseigner les quantités
    parametre.setQuantiteReliquatUA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTA]);
    parametre.setQuantiteReliquatUCA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTC]);
    parametre.setQuantiteReliquatUS((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTS]);
    // Renseigner les quantités initiales
    parametre.setQuantiteInitialeUA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTAI]);
    parametre.setQuantiteInitialeUCA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTCI]);
    parametre.setQuantiteInitialeUS((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTSI]);
    // Renseigner les quantités traitées
    parametre.setQuantiteTraiteeUA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTAT]);
    parametre.setQuantiteTraiteeUCA((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTCT]);
    parametre.setQuantiteTraiteeUS((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAQTST]);
    
    // Renseigner les montants
    parametre.setPrixAchatBrutHT((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAPAB]);
    ListeRemise listeRemise = parametre.getListeRemise();
    listeRemise.setPourcentageRemise1((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAREM1]);
    listeRemise.setPourcentageRemise2((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAREM2]);
    listeRemise.setPourcentageRemise3((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAREM3]);
    listeRemise.setPourcentageRemise4((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAREM4]);
    listeRemise.setPourcentageRemise5((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAREM5]);
    listeRemise.setPourcentageRemise6((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAREM6]);
    listeRemise.setTypeRemise(((String) ligneBrute[Svgam0059d.VAR_WLATRL]).charAt(0));
    listeRemise.setBaseRemise(((String) ligneBrute[Svgam0059d.VAR_WLABRL]).charAt(0));
    parametre.setPourcentageTaxeOuMajoration(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFK3]));
    parametre.setMontantConditionnement(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFV3]));
    parametre.setPrixNetHT((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAPAC]);
    parametre.setMontantAuPoidsPortHT(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFV1]));
    parametre.setPoidsPort(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFK1]));
    parametre.setPourcentagePort(new BigPercentage(((BigDecimal) ligneBrute[Svgam0059d.VAR_W7LFP1]), EnumFormatPourcentage.POURCENTAGE));
    parametre.setMontantPortHT((BigDecimal) ligneBrute[Svgam0059d.VAR_W7PORTF]);
    parametre.setPrixRevientFournisseurHT((BigDecimal) ligneBrute[Svgam0059d.VAR_W7PRVFR]);
    parametre.setMontantReliquatHT((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAMHT]);
    parametre.setMontantInitialHT((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAMHTI]);
    parametre.setMontantTraiteHT((BigDecimal) ligneBrute[Svgam0059d.VAR_WLAMHTT]);
    
    // Renseigner les exclusions de remises de pied
    Character[] exlusionremise = parametre.getExclusionRemisePied();
    exlusionremise[0] = ((String) ligneBrute[Svgam0059d.VAR_WLARP1]).charAt(0);
    exlusionremise[1] = ((String) ligneBrute[Svgam0059d.VAR_WLARP2]).charAt(0);
    exlusionremise[2] = ((String) ligneBrute[Svgam0059d.VAR_WLARP3]).charAt(0);
    exlusionremise[3] = ((String) ligneBrute[Svgam0059d.VAR_WLARP4]).charAt(0);
    exlusionremise[4] = ((String) ligneBrute[Svgam0059d.VAR_WLARP5]).charAt(0);
    exlusionremise[5] = ((String) ligneBrute[Svgam0059d.VAR_WLARP6]).charAt(0);
    
    // Renseigner les indicateurs
    parametre.setTopPrixBaseSaisie(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATB]).intValue());
    parametre.setTopRemiseSaisie(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATR]).intValue());
    parametre.setTopPrixNetSaisi(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATN]).intValue());
    parametre.setTopUniteSaisie(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATU]).intValue());
    parametre.setTopQuantiteSaisie(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATQ]).intValue());
    parametre.setTopMontantHTSaisi(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATH]).intValue());
    
    // Renseigner les éléments de TVA
    parametre.setCodeTVA(((BigDecimal) ligneBrute[Svgam0059d.VAR_WLATVA]).intValue());
    parametre.setColonneTVA(((BigDecimal) ligneBrute[Svgam0059d.VAR_WTAUTVA]).intValue());
    
    // Mettre à jour l'article
    if (ligneAchatBase.getPrixAchat() == null) {
      ligneAchatBase.setPrixAchat(new PrixAchat());
    }
    ligneAchatBase.getPrixAchat().initialiser(parametre);
    
    return ligneAchatBase;
  }
  
  /**
   * Méthode interne pour alimenter facilement les 65 champs comportant l'id des lignes de vente.
   */
  private String alimenterIdLigneAchat(List<IdLigneAchat> pListeIdLigneAchat, int index) {
    if (index < pListeIdLigneAchat.size()) {
      return pListeIdLigneAchat.get(index).getIndicatif(IdLigneAchat.INDICATIF_ENTETE_ETB_NUM_SUF_NLI);
    }
    else {
      return "               ";
    }
  }
  
}
