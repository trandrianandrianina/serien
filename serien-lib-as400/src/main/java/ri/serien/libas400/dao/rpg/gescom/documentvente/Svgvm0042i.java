/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.documentvente;

import java.beans.PropertyVetoException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.ConvertDate;

public class Svgvm0042i extends ProgramParameter {
  // Constantes
  public static final int SIZE_PIIND = 10;
  public static final int SIZE_PICOD = 1;
  public static final int SIZE_PIETB = 3;
  public static final int SIZE_PINUM = 6;
  public static final int DECIMAL_PINUM = 0;
  public static final int SIZE_PISUF = 1;
  public static final int DECIMAL_PISUF = 0;
  public static final int SIZE_PINFA = 7;
  public static final int DECIMAL_PINFA = 0;
  public static final int SIZE_PILI01 = 100;
  public static final int SIZE_PILI02 = 100;
  public static final int SIZE_PILI03 = 100;
  public static final int SIZE_PILI04 = 100;
  public static final int SIZE_PILI05 = 100;
  public static final int SIZE_PILI06 = 100;
  public static final int SIZE_PILI07 = 100;
  public static final int SIZE_PILI08 = 100;
  public static final int SIZE_PILI09 = 100;
  public static final int SIZE_PILI10 = 100;
  public static final int SIZE_PILI11 = 100;
  public static final int SIZE_PILI12 = 100;
  public static final int SIZE_PILI13 = 100;
  public static final int SIZE_PILI14 = 100;
  public static final int SIZE_PILI15 = 100;
  public static final int SIZE_PILI16 = 100;
  public static final int SIZE_PILI17 = 100;
  public static final int SIZE_PILI18 = 100;
  public static final int SIZE_PILI01S = 100;
  public static final int SIZE_PILI02S = 100;
  public static final int SIZE_PILI03S = 100;
  public static final int SIZE_PILI04S = 100;
  public static final int SIZE_PILI05S = 100;
  public static final int SIZE_PILI06S = 100;
  public static final int SIZE_PILI07S = 100;
  public static final int SIZE_PILI08S = 100;
  public static final int SIZE_PILI09S = 100;
  public static final int SIZE_PILI10S = 100;
  public static final int SIZE_PILI11S = 100;
  public static final int SIZE_PILI12S = 100;
  public static final int SIZE_PILI13S = 100;
  public static final int SIZE_PILI14S = 100;
  public static final int SIZE_PILI15S = 100;
  public static final int SIZE_PILI16S = 100;
  public static final int SIZE_PILI17S = 100;
  public static final int SIZE_PILI18S = 100;
  public static final int SIZE_PIARR = 1;
  public static final int SIZE_TOTALE_DS = 3629;
  
  // Constantes indices Nom DS
  public static final int VAR_PIIND = 0;
  public static final int VAR_PICOD = 1;
  public static final int VAR_PIETB = 2;
  public static final int VAR_PINUM = 3;
  public static final int VAR_PISUF = 4;
  public static final int VAR_PINFA = 5;
  public static final int VAR_PILI01 = 6;
  public static final int VAR_PILI02 = 7;
  public static final int VAR_PILI03 = 8;
  public static final int VAR_PILI04 = 9;
  public static final int VAR_PILI05 = 10;
  public static final int VAR_PILI06 = 11;
  public static final int VAR_PILI07 = 12;
  public static final int VAR_PILI08 = 13;
  public static final int VAR_PILI09 = 14;
  public static final int VAR_PILI10 = 15;
  public static final int VAR_PILI11 = 16;
  public static final int VAR_PILI12 = 17;
  public static final int VAR_PILI13 = 18;
  public static final int VAR_PILI14 = 19;
  public static final int VAR_PILI15 = 20;
  public static final int VAR_PILI16 = 21;
  public static final int VAR_PILI17 = 22;
  public static final int VAR_PILI18 = 23;
  public static final int VAR_PILI01S = 24;
  public static final int VAR_PILI02S = 25;
  public static final int VAR_PILI03S = 26;
  public static final int VAR_PILI04S = 27;
  public static final int VAR_PILI05S = 28;
  public static final int VAR_PILI06S = 29;
  public static final int VAR_PILI07S = 30;
  public static final int VAR_PILI08S = 31;
  public static final int VAR_PILI09S = 32;
  public static final int VAR_PILI10S = 33;
  public static final int VAR_PILI11S = 34;
  public static final int VAR_PILI12S = 35;
  public static final int VAR_PILI13S = 36;
  public static final int VAR_PILI14S = 37;
  public static final int VAR_PILI15S = 38;
  public static final int VAR_PILI16S = 39;
  public static final int VAR_PILI17S = 40;
  public static final int VAR_PILI18S = 41;
  public static final int VAR_PIARR = 42;
  
  public Svgvm0042j[] dsd = { new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(),
      new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(),
      new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(),
      new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(),
      new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(), new Svgvm0042j(),
      new Svgvm0042j(), new Svgvm0042j(), };
  
  // Variables AS400
  private String piind = ""; // Indicateurs
  private String picod = ""; // Code ERL "D","E",X","9"
  private String pietb = ""; // Code établissement
  private BigDecimal pinum = BigDecimal.ZERO; // Numéro de Bon
  private BigDecimal pisuf = BigDecimal.ZERO; // Suffixe
  private BigDecimal pinfa = BigDecimal.ZERO; // Numéro de facture
  private Object[] pili01 = null; // Zones Ligne 1
  private Object[] pili02 = null; // Zones Ligne 2
  private Object[] pili03 = null; // Zones Ligne 3
  private Object[] pili04 = null; // Zones Ligne 4
  private Object[] pili05 = null; // Zones Ligne 5
  private Object[] pili06 = null; // Zones Ligne 6
  private Object[] pili07 = null; // Zones Ligne 7
  private Object[] pili08 = null; // Zones Ligne 8
  private Object[] pili09 = null; // Zones Ligne 9
  private Object[] pili10 = null; // Zones Ligne 10
  private Object[] pili11 = null; // Zones Ligne 11
  private Object[] pili12 = null; // Zones Ligne 12
  private Object[] pili13 = null; // Zones Ligne 13
  private Object[] pili14 = null; // Zones Ligne 14
  private Object[] pili15 = null; // Zones Ligne 15
  private Object[] pili16 = null; // Zones Ligne 16
  private Object[] pili17 = null; // Zones Ligne 17
  private Object[] pili18 = null; // Zones Ligne 18
  private Object[] pili01s = null; // Zones Ligne 1 Etat origine
  private Object[] pili02s = null; // Zones Ligne 2 Etat origine
  private Object[] pili03s = null; // Zones Ligne 3 Etat origine
  private Object[] pili04s = null; // Zones Ligne 4 Etat origine
  private Object[] pili05s = null; // Zones Ligne 5 Etat origine
  private Object[] pili06s = null; // Zones Ligne 6 Etat origine
  private Object[] pili07s = null; // Zones Ligne 7 Etat origine
  private Object[] pili08s = null; // Zones Ligne 8 Etat origine
  private Object[] pili09s = null; // Zones Ligne 9 Etat origine
  private Object[] pili10s = null; // Zones Ligne 10 Etat origine
  private Object[] pili11s = null; // Zones Ligne 11 Etat origine
  private Object[] pili12s = null; // Zones Ligne 12 Etat origine
  private Object[] pili13s = null; // Zones Ligne 13 Etat origine
  private Object[] pili14s = null; // Zones Ligne 14 Etat origine
  private Object[] pili15s = null; // Zones Ligne 15 Etat origine
  private Object[] pili16s = null; // Zones Ligne 16 Etat origine
  private Object[] pili17s = null; // Zones Ligne 17 Etat origine
  private Object[] pili18s = null; // Zones Ligne 18 Etat origine
  private String piarr = "X"; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PIIND), // Indicateurs
      new AS400Text(SIZE_PICOD), // Code ERL "D","E",X","9"
      new AS400Text(SIZE_PIETB), // Code établissement
      new AS400ZonedDecimal(SIZE_PINUM, DECIMAL_PINUM), // Numéro de Bon
      new AS400ZonedDecimal(SIZE_PISUF, DECIMAL_PISUF), // Suffixe
      new AS400ZonedDecimal(SIZE_PINFA, DECIMAL_PINFA), // Numéro de facture
      new AS400Structure(dsd[0].structure), // Zones Ligne 1
      new AS400Structure(dsd[1].structure), // Zones Ligne 2
      new AS400Structure(dsd[2].structure), // Zones Ligne 3
      new AS400Structure(dsd[3].structure), // Zones Ligne 4
      new AS400Structure(dsd[4].structure), // Zones Ligne 5
      new AS400Structure(dsd[5].structure), // Zones Ligne 6
      new AS400Structure(dsd[6].structure), // Zones Ligne 7
      new AS400Structure(dsd[7].structure), // Zones Ligne 8
      new AS400Structure(dsd[8].structure), // Zones Ligne 9
      new AS400Structure(dsd[9].structure), // Zones Ligne 10
      new AS400Structure(dsd[10].structure), // Zones Ligne 11
      new AS400Structure(dsd[11].structure), // Zones Ligne 12
      new AS400Structure(dsd[12].structure), // Zones Ligne 13
      new AS400Structure(dsd[13].structure), // Zones Ligne 14
      new AS400Structure(dsd[14].structure), // Zones Ligne 15
      new AS400Structure(dsd[15].structure), // Zones Ligne 16
      new AS400Structure(dsd[16].structure), // Zones Ligne 17
      new AS400Structure(dsd[17].structure), // Zones Ligne 18
      new AS400Structure(dsd[18].structure), // Zones Ligne 1 Etat origine
      new AS400Structure(dsd[19].structure), // Zones Ligne 2 Etat origine
      new AS400Structure(dsd[20].structure), // Zones Ligne 3 Etat origine
      new AS400Structure(dsd[21].structure), // Zones Ligne 4 Etat origine
      new AS400Structure(dsd[22].structure), // Zones Ligne 5 Etat origine
      new AS400Structure(dsd[23].structure), // Zones Ligne 6 Etat origine
      new AS400Structure(dsd[24].structure), // Zones Ligne 7 Etat origine
      new AS400Structure(dsd[25].structure), // Zones Ligne 8 Etat origine
      new AS400Structure(dsd[26].structure), // Zones Ligne 9 Etat origine
      new AS400Structure(dsd[27].structure), // Zones Ligne 10 Etat origine
      new AS400Structure(dsd[28].structure), // Zones Ligne 11 Etat origine
      new AS400Structure(dsd[29].structure), // Zones Ligne 12 Etat origine
      new AS400Structure(dsd[30].structure), // Zones Ligne 13 Etat origine
      new AS400Structure(dsd[31].structure), // Zones Ligne 14 Etat origine
      new AS400Structure(dsd[32].structure), // Zones Ligne 15 Etat origine
      new AS400Structure(dsd[33].structure), // Zones Ligne 16 Etat origine
      new AS400Structure(dsd[34].structure), // Zones Ligne 17 Etat origine
      new AS400Structure(dsd[35].structure), // Zones Ligne 18 Etat origine
      new AS400Text(SIZE_PIARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      pili01 = dsd[0].getStructureObject();
      pili02 = dsd[1].getStructureObject();
      pili03 = dsd[2].getStructureObject();
      pili04 = dsd[3].getStructureObject();
      pili05 = dsd[4].getStructureObject();
      pili06 = dsd[5].getStructureObject();
      pili07 = dsd[6].getStructureObject();
      pili08 = dsd[7].getStructureObject();
      pili09 = dsd[8].getStructureObject();
      pili10 = dsd[9].getStructureObject();
      pili11 = dsd[10].getStructureObject();
      pili12 = dsd[11].getStructureObject();
      pili13 = dsd[12].getStructureObject();
      pili14 = dsd[13].getStructureObject();
      pili15 = dsd[14].getStructureObject();
      pili16 = dsd[15].getStructureObject();
      pili17 = dsd[16].getStructureObject();
      pili18 = dsd[17].getStructureObject();
      pili01s = dsd[18].getStructureObject();
      pili02s = dsd[19].getStructureObject();
      pili03s = dsd[20].getStructureObject();
      pili04s = dsd[21].getStructureObject();
      pili05s = dsd[22].getStructureObject();
      pili06s = dsd[23].getStructureObject();
      pili07s = dsd[24].getStructureObject();
      pili08s = dsd[25].getStructureObject();
      pili09s = dsd[26].getStructureObject();
      pili10s = dsd[27].getStructureObject();
      pili11s = dsd[28].getStructureObject();
      pili12s = dsd[29].getStructureObject();
      pili13s = dsd[30].getStructureObject();
      pili14s = dsd[31].getStructureObject();
      pili15s = dsd[32].getStructureObject();
      pili16s = dsd[33].getStructureObject();
      pili17s = dsd[34].getStructureObject();
      pili18s = dsd[35].getStructureObject();
      Object[] o = { piind, picod, pietb, pinum, pisuf, pinfa, pili01, pili02, pili03, pili04, pili05, pili06, pili07, pili08, pili09,
          pili10, pili11, pili12, pili13, pili14, pili15, pili16, pili17, pili18, pili01s, pili02s, pili03s, pili04s, pili05s, pili06s,
          pili07s, pili08s, pili09s, pili10s, pili11s, pili12s, pili13s, pili14s, pili15s, pili16s, pili17s, pili18s, piarr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(SIZE_TOTALE_DS);
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    piind = (String) output[0];
    picod = (String) output[1];
    pietb = (String) output[2];
    pinum = (BigDecimal) output[3];
    pisuf = (BigDecimal) output[4];
    pinfa = (BigDecimal) output[5];
    pili01 = (Object[]) output[6];
    pili02 = (Object[]) output[7];
    pili03 = (Object[]) output[8];
    pili04 = (Object[]) output[9];
    pili05 = (Object[]) output[10];
    pili06 = (Object[]) output[11];
    pili07 = (Object[]) output[12];
    pili08 = (Object[]) output[13];
    pili09 = (Object[]) output[14];
    pili10 = (Object[]) output[15];
    pili11 = (Object[]) output[16];
    pili12 = (Object[]) output[17];
    pili13 = (Object[]) output[18];
    pili14 = (Object[]) output[19];
    pili15 = (Object[]) output[20];
    pili16 = (Object[]) output[21];
    pili17 = (Object[]) output[22];
    pili18 = (Object[]) output[23];
    pili01s = (Object[]) output[24];
    pili02s = (Object[]) output[25];
    pili03s = (Object[]) output[26];
    pili04s = (Object[]) output[27];
    pili05s = (Object[]) output[28];
    pili06s = (Object[]) output[29];
    pili07s = (Object[]) output[30];
    pili08s = (Object[]) output[31];
    pili09s = (Object[]) output[32];
    pili10s = (Object[]) output[33];
    pili11s = (Object[]) output[34];
    pili12s = (Object[]) output[35];
    pili13s = (Object[]) output[36];
    pili14s = (Object[]) output[37];
    pili15s = (Object[]) output[38];
    pili16s = (Object[]) output[39];
    pili17s = (Object[]) output[40];
    pili18s = (Object[]) output[41];
    piarr = (String) output[42];
  }
  
  // -- Accesseurs
  
  public void setPiind(String pPiind) {
    if (pPiind == null) {
      return;
    }
    piind = pPiind;
  }
  
  public String getPiind() {
    return piind;
  }
  
  public void setPicod(Character pPicod) {
    if (pPicod == null) {
      return;
    }
    picod = String.valueOf(pPicod);
  }
  
  public Character getPicod() {
    return picod.charAt(0);
  }
  
  public void setPietb(String pPietb) {
    if (pPietb == null) {
      return;
    }
    pietb = pPietb;
  }
  
  public String getPietb() {
    return pietb;
  }
  
  public void setPinum(BigDecimal pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = pPinum.setScale(DECIMAL_PINUM, RoundingMode.HALF_UP);
  }
  
  public void setPinum(Integer pPinum) {
    if (pPinum == null) {
      return;
    }
    pinum = BigDecimal.valueOf(pPinum);
  }
  
  public Integer getPinum() {
    return pinum.intValue();
  }
  
  public void setPisuf(BigDecimal pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = pPisuf.setScale(DECIMAL_PISUF, RoundingMode.HALF_UP);
  }
  
  public void setPisuf(Integer pPisuf) {
    if (pPisuf == null) {
      return;
    }
    pisuf = BigDecimal.valueOf(pPisuf);
  }
  
  public Integer getPisuf() {
    return pisuf.intValue();
  }
  
  public void setPinfa(BigDecimal pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = pPinfa.setScale(DECIMAL_PINFA, RoundingMode.HALF_UP);
  }
  
  public void setPinfa(Integer pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(pPinfa);
  }
  
  public void setPinfa(Date pPinfa) {
    if (pPinfa == null) {
      return;
    }
    pinfa = BigDecimal.valueOf(ConvertDate.dateToDb2(pPinfa));
  }
  
  public Integer getPinfa() {
    return pinfa.intValue();
  }
  
  public Date getPinfaConvertiEnDate() {
    return ConvertDate.db2ToDate(pinfa.intValue(), null);
  }
  
  public void setPiarr(Character pPiarr) {
    if (pPiarr == null) {
      return;
    }
    piarr = String.valueOf(pPiarr);
  }
  
  public Character getPiarr() {
    return piarr.charAt(0);
  }
}
