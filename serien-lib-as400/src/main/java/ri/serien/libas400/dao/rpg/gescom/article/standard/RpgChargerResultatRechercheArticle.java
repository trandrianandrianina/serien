/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg.gescom.article.standard;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import com.ibm.as400.access.ProgramParameter;

import ri.serien.libas400.dao.rpg.RpgBase;
import ri.serien.libas400.system.ContexteServiceRPG;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.IdResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ListeResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ResultatRechercheArticle;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceArticle;

/**
 * Classe qui encapsule le programme RPG SVGVX0017.
 */
public class RpgChargerResultatRechercheArticle extends RpgBase {
  // Constantes
  private static final String PROGRAMME = "SVGVX0017";
  private static final int NOMBRE_MAX_ARTICLE = 65;
  
  private IdSession idSession = null;
  
  /**
   * Construction.
   */
  public RpgChargerResultatRechercheArticle(IdSession pIdSession) {
    idSession = pIdSession;
  }
  
  /**
   * Utilise un programme RPG pour lire les données d'une liste d'articles en vue d'une information de ligne de ventes.
   * Le programme RPG peut traiter 65 articles à la fois, il est appelé en boucle pour l'intégralité des articles à lire.
   */
  public ListeResultatRechercheArticle chargerListeResultatRechercheArticle(SystemeManager pSysteme,
      List<IdResultatRechercheArticle> pListeIdResultatRechercheArticle, ParametresLireArticle pParametre) {
    ListeResultatRechercheArticle listeResultatRechercheArticle = new ListeResultatRechercheArticle();
    int indexDepart = 0;
    while (indexDepart < pListeIdResultatRechercheArticle.size()) {
      int indexFin = Math.min(indexDepart + NOMBRE_MAX_ARTICLE - 1, pListeIdResultatRechercheArticle.size() - 1);
      chargerListe65ResultatRechercheArticle(pSysteme, listeResultatRechercheArticle, pListeIdResultatRechercheArticle, indexDepart,
          indexFin, pParametre);
      indexDepart = indexFin + 1;
    }
    return listeResultatRechercheArticle;
  }
  
  /**
   * Appeler le programme RPG qui peut lire les données de 65 articles.
   */
  private void chargerListe65ResultatRechercheArticle(SystemeManager pSysteme,
      ListeResultatRechercheArticle pListeResultatRechercheArticle, List<IdResultatRechercheArticle> pListeIdResultatRechercheArticle,
      int pIndexDepart, int pIndexFin, ParametresLireArticle pParametresLireArticle) {
    if (pSysteme == null || pListeIdResultatRechercheArticle == null || pParametresLireArticle == null
        || pListeResultatRechercheArticle == null) {
      throw new MessageErreurException("Erreur lors de la lecture des données des articles pour la préparation des lignes de ventes.");
    }
    if (pIndexFin - pIndexDepart + 1 > NOMBRE_MAX_ARTICLE) {
      throw new MessageErreurException("Il n'est pas possible de lire plus de " + NOMBRE_MAX_ARTICLE + " articles à la fois.");
    }
    
    // Préparation des paramètres du programme RPG
    Svgvx0017i entree = new Svgvx0017i();
    Svgvx0017o sortie = new Svgvx0017o();
    ProgramParameter[] parameterList = { entree, sortie, erreur };
    
    // Initialisation des paramètres d'entrée
    char[] indicateurs = { '0', '0', '0', '0', '0', '0', '0', '0', '0', '0' };
    entree.setPiind(new String(indicateurs));
    entree.setPietb(pParametresLireArticle.getIdEtablissement().getCodeEtablissement());
    if (pParametresLireArticle.getIdMagasin() != null) {
      entree.setPimag(pParametresLireArticle.getIdMagasin().getCode());
    }
    
    if (pParametresLireArticle.getIdClient() != null) {
      entree.setPicli(pParametresLireArticle.getIdClient().getNumero());
      entree.setPiliv(pParametresLireArticle.getIdClient().getSuffixe());
    }
    
    if (pParametresLireArticle.getIdDocumentVente() != null) {
      entree.setPicod(pParametresLireArticle.getIdDocumentVente().getEntete());
      entree.setPinum(pParametresLireArticle.getIdDocumentVente().getNumero());
      entree.setPisuf(pParametresLireArticle.getIdDocumentVente().getSuffixe());
    }
    
    if (pParametresLireArticle.getIdChantier() != null) {
      entree.setPichcod(pParametresLireArticle.getIdChantier().getCodeEntete().getCode());
      entree.setPichnum(pParametresLireArticle.getIdChantier().getNumero());
      entree.setPichsuf(pParametresLireArticle.getIdChantier().getSuffixe());
    }
    
    if (pParametresLireArticle.getNumeroColonneTarifForcee() != null
        && pParametresLireArticle.getNumeroColonneTarifForcee().intValue() > 0) {
      entree.setPitar(pParametresLireArticle.getNumeroColonneTarifForcee() - 1);
    }
    if (pParametresLireArticle.getTauxRemiseForcee() != null) {
      entree.setPirem(pParametresLireArticle.getTauxRemiseForcee().getTauxEnPourcentage());
    }
    
    int index = pIndexDepart;
    entree.setPia01(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia02(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia03(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia04(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia05(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia06(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia07(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia08(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia09(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia10(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia11(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia12(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia13(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia14(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia15(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia16(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia17(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia18(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia19(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia20(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia21(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia22(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia23(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia24(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia25(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia26(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia27(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia28(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia29(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia30(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia31(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia32(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia33(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia34(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia35(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia36(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia37(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia38(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia39(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia40(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia41(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia42(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia43(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia44(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia45(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia46(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia47(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia48(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia49(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia50(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia51(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia52(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia53(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia54(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia55(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia56(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia57(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia58(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia59(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia60(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia61(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia62(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia63(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia64(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    entree.setPia65(alimenterCodeArticle(pListeIdResultatRechercheArticle, index++));
    
    // Initialisation des paramètres d'entrée avant d'appeler le programme RPG
    entree.setDSInput();
    sortie.setDSInput();
    erreur.setDSInput();
    
    ContexteServiceRPG rpg = new ContexteServiceRPG(pSysteme);
    rpg.initialiserEnvironnement(EnvironnementExecution.getExpas(), EnvironnementExecution.getGvmas(), EnvironnementExecution.getGvmx());
    
    // Exécuter le programme RPG
    if (!rpg.executerProgramme(PROGRAMME, EnvironnementExecution.getGvmx(), parameterList)) {
      return;
    }
    
    // Récupération et conversion des paramètres du RPG
    entree.setDSOutput();
    sortie.setDSOutput();
    erreur.setDSOutput();
    
    // Gestion des erreurs
    controlerErreur();
    
    // Renseigner le résultat
    for (Object ligne : sortie.getValeursBrutesLignes()) {
      // Le RPG retourne des résultats null pour compléter le nombre de lignes qu'il doit retourner
      if (ligne == null) {
        continue;
      }
      
      // Ne pas stopper le chargement de l'ensemble des résultats si un résultat est erroné
      try {
        // Créer l'objet à partir des données renvoyées par le programme RPG
        ResultatRechercheArticle resultatRechercheArticle = completerResultatRechercheArticle((Object[]) ligne, pParametresLireArticle);
        
        // Ajouter l'objet à la liste de résultats
        pListeResultatRechercheArticle.add(resultatRechercheArticle);
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors de la lecture d'une des lignes du résultat de la recherche article");
      }
    }
  }
  
  /**
   * Méthode interne pour alimenter facilement les 65 champs comportant le code article.
   */
  private String alimenterCodeArticle(List<IdResultatRechercheArticle> pListeIdResultatRechercheArticle, int pIndex) {
    if (pIndex < pListeIdResultatRechercheArticle.size()) {
      return pListeIdResultatRechercheArticle.get(pIndex).getIdArticle().getCodeArticle();
    }
    return "";
  }
  
  /**
   * Créer un article à partir des données d'une ligne renvoyée par le programme RPG.
   * @param pLigne Ligne retournée par le RPG (soius forme d'un tableau d'objets).
   * @param pParametresLireArticle Paramètres de recherche.
   * @return Objet métier correspondant à une ligne du résultat de la recherche.
   */
  private ResultatRechercheArticle completerResultatRechercheArticle(Object[] pLigne, ParametresLireArticle pParametresLireArticle) {
    // Déterminer l'identifiant de l'établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance((String) pLigne[Svgvx0017d.VAR_WETB]);
    
    // Déterminer l'identifiant de l'article
    String codeArticle = Constantes.normerTexte((String) pLigne[Svgvx0017d.VAR_WART]);
    IdArticle idArticle = IdArticle.getInstance(idEtablissement, codeArticle);
    
    // Charger les paramètres de calcul du prix de vente
    ParametreChargement parametreChargement = new ParametreChargement();
    parametreChargement.setIdEtablissement(pParametresLireArticle.getIdEtablissement());
    parametreChargement.setIdMagasin(pParametresLireArticle.getIdMagasin());
    parametreChargement.setIdArticle(idArticle);
    parametreChargement.setIdClientFacture(pParametresLireArticle.getIdClient());
    parametreChargement.setIdDocumentVente(pParametresLireArticle.getIdDocumentVente());
    parametreChargement.setIdChantier(pParametresLireArticle.getIdChantier());
    CalculPrixVente calculPrixVente = ManagerServiceArticle.calculerPrixVente(idSession, parametreChargement);
    
    // Modifier les paramètres et recalculer le prix de vente
    if (calculPrixVente != null) {
      ParametreLigneVente parametreLigneVente = new ParametreLigneVente();
      
      // Utiliser le numéro de colonne forcée pour le calcul du prix de vente
      if (pParametresLireArticle.getNumeroColonneTarifForcee() != null) {
        parametreLigneVente.setNumeroColonneTarif(pParametresLireArticle.getNumeroColonneTarifForcee());
        parametreLigneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
      }
      
      // Utiliser le taux de remise forcée pour le calcul du prix de vente
      if (pParametresLireArticle.getTauxRemiseForcee() != null && !pParametresLireArticle.getTauxRemiseForcee().isZero()) {
        parametreLigneVente.setTauxRemise1(pParametresLireArticle.getTauxRemiseForcee());
        parametreLigneVente.setProvenanceTauxRemise(EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE);
      }
      
      // Refaire le calcul
      calculPrixVente.setParametreLigneVente(parametreLigneVente);
      calculPrixVente.calculer();
    }
    
    // Créer l'objet contenant le résultat de la recherche pour une ligne du tableau
    IdResultatRechercheArticle idResultatRechercheArticle = IdResultatRechercheArticle.getInstance(idArticle);
    ResultatRechercheArticle resultatRechercheArticle = new ResultatRechercheArticle(idResultatRechercheArticle);
    resultatRechercheArticle.setInformationCharge(true);
    
    // Renseigner l'unité de conditionnement de vente (colonne 2)
    if (!((String) pLigne[Svgvx0017d.VAR_WUNL]).trim().isEmpty()) {
      IdUnite idUCV = IdUnite.getInstance((String) pLigne[Svgvx0017d.VAR_WUNL]);
      resultatRechercheArticle.setIdUCV(idUCV);
    }
    
    // Renseigner le libellé (colonne 3)
    resultatRechercheArticle.setLibelle1((String) pLigne[Svgvx0017d.VAR_WLIB]);
    resultatRechercheArticle.setLibelle2((String) pLigne[Svgvx0017d.VAR_WLB1]);
    resultatRechercheArticle.setLibelle3((String) pLigne[Svgvx0017d.VAR_WLB2]);
    resultatRechercheArticle.setLibelle4((String) pLigne[Svgvx0017d.VAR_WLB3]);
    
    // Renseigner le stock (colonne 4)
    BigDecimal nombreDecimaleStock = (BigDecimal) pLigne[Svgvx0017d.VAR_WDCS];
    BigDecimal stock = (BigDecimal) pLigne[Svgvx0017d.VAR_WDIV];
    stock.setScale(nombreDecimaleStock.intValue(), RoundingMode.HALF_UP);
    resultatRechercheArticle.setStock(stock);
    
    // Renseigner le prix net (colonne 5)
    // Calcul du prix (en Java) pour l'article en cours
    if (calculPrixVente != null) {
      resultatRechercheArticle.setPrixNetHT(calculPrixVente.getPrixNetHT());
      resultatRechercheArticle.setPrixNetTTC(calculPrixVente.getPrixNetTTC());
    }
    
    // Renseigner l'unité de vente (colonne 6)
    if (!((String) pLigne[Svgvx0017d.VAR_WUNV]).trim().isEmpty()) {
      IdUnite idUV = IdUnite.getInstance((String) pLigne[Svgvx0017d.VAR_WUNV]);
      resultatRechercheArticle.setIdUV(idUV);
    }
    
    // Renseigner le nombre d'UC par UCV (colonne 7)
    resultatRechercheArticle.setNombreUVParUCV(((BigDecimal) pLigne[Svgvx0017d.VAR_WCND]));
    
    // Renseigner l'origine (colonne 8)
    if (calculPrixVente != null && calculPrixVente.getOriginePrixVente() != null) {
      EnumOriginePrixVente originePrixVente = calculPrixVente.getOriginePrixVente();
      
      // Afficher "Colonne n" dans le cas d'un prix standard
      if (originePrixVente == EnumOriginePrixVente.STANDARD && calculPrixVente.getNumeroColonneTarif() != null) {
        resultatRechercheArticle.setOrigine("Colonne " + calculPrixVente.getNumeroColonneTarif());
      }
      // Afficher le groupe de la CNV
      else if (originePrixVente == EnumOriginePrixVente.GROUPE_CNV && calculPrixVente.getParametreCNV() != null
          && calculPrixVente.getParametreCNV().getIdRattachementClient() != null) {
        resultatRechercheArticle.setOrigine("CNV " + calculPrixVente.getParametreCNV().getIdRattachementClient().getCode());
      }
      // Afficher le libellé de l'origine du prix de vente remonté par la formule de calcul
      else {
        resultatRechercheArticle.setOrigine(originePrixVente.getLibelle());
      }
    }
    
    // Renseigner l'observation (colonne 9)
    // Commencer par tester la présence d'un prix flash, auquel cas on préfixe l'observation avec "(Prix flash)".
    String observation = (String) pLigne[Svgvx0017d.VAR_WOBS];
    BigDecimal prixFlash = (BigDecimal) pLigne[Svgvx0017d.VAR_WFPVN];
    if (prixFlash.compareTo(BigDecimal.ZERO) == 1) {
      resultatRechercheArticle
          .setObservation(String.format("<html>%s<span style='color:#8A084B'>(Prix flash)</span></html>", observation));
    }
    else {
      resultatRechercheArticle.setObservation(observation);
    }
    
    return resultatRechercheArticle;
  }
}
