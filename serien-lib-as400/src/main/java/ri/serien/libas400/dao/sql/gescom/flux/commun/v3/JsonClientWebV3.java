/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.commun.v3;

import java.util.ArrayList;
import java.util.Date;

import ri.serien.libas400.dao.sql.gescom.flux.OutilsMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.JsonEntiteMagento;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Client Série N au format Web.
 * Cette classe est une classe qui sert à l'intégration JAVA vers JSON ou JSON vers JAVA
 * IL NE FAUT PAS RENOMMER OU MODIFIER SES CHAMPS
 */
public class JsonClientWebV3 extends JsonEntiteMagento {
  // Données internes à la classe.
  // Elles ne transitent pas dans les flux mais participent aux traitement de ces derniers
  private transient String CLCLI = null;
  private transient String CLLIV = null;
  // SIRET découpé en 2
  private transient String CLSRN = null;
  private transient String CLSRT = null;
  // BLOC ADRESSE FICHIER SERIE N
  private transient String CLNOM = null;
  private transient String CLRUE = null;
  private transient String CLLOC = null;
  private transient String CLTAR = "1";
  // Compte Général compta
  private transient String CLNCG = null;
  private transient String codeReglement = null;
  // zone technique exclusion Web
  private transient String EBIN09 = " ";
  // Client particulier ou NON
  private transient String CLIN9 = " ";
  
  // ZONE CLIENT FACTURE
  private transient String CLCLFP = null;
  private transient String CLCLFS = null;
  // Variable d'exclusion fonctionnelle (sera échangée dans le flux ultérieurement
  private transient String exclusWeb = null;
  // Catégorie client par Défaut
  public static transient String CATEGORIE_DEFAUT = "";
  
  // Les 3 notions qui gèrent les échanges de flux d'un client
  // Le client n'est pas échangé avec SFCC
  public static transient String EXCLUS_WEB = "";
  // Le client est échangé dans les flux SFCC et actif sur SFCC
  public static transient String GERE_WEB = "1";
  // Le client est échangé dans les flux SFCC mais inactif sur SFCC
  public static transient String GERE_WEB_INACTIF = "2";
  // Les valeurs constantes correspondants à Mode particulier ou pro
  public static final transient int ISPROFESSIONNEL = 0;
  public static final transient int ISPARTICULIER = 1;
  // Colonne tarifaire par défaut du client intégré
  public static final Integer TARIF_DEFAUT = 1;
  
  public static final transient String codePaysDefaut = "FR";
  public static final transient String libPaysDefaut = "FRANCE";
  
  // Infos pour insertion en Compta
  private transient String etbComptable = null;
  private transient String compteGeneral = null;
  
  // DONNEES ECHANGEES DANS LE FLUX
  // Données SFCC
  private String timestampCrea = null;
  private String idClientWeb = null;
  private String siteClient = null;
  private String magClient = null;
  
  // Données standard du client
  private String idClient = null;
  private String email = null;
  private String siren = null;
  private String codeAPE = null;
  private String categorie = null;
  private String nomSociete = null;
  private String tvaClient = null;
  private Integer tarif = null;
  private int isUnParticulier = ISPROFESSIONNEL;
  
  // Coordonnées de facturation (le bloc adresse de la fiche client correspondant à la variable idClient)
  private String civiliteClient = null;
  private String nomClient = null;
  private String complementNomClient = null;
  private String prenomClient = null;
  private String rueClient = null;
  private String rue2Client = null;
  private String cdpClient = null;
  private String villeClient = null;
  private String paysClient = null;
  private String libellePaysClient = null;
  private String telClient = null;
  private String faxClient = null;
  
  // Coordonnées de facturation si précisé dans l'onglet Facturation de la fiche client
  private JsonAdresseClientFacturationWebV3 adresseClientFacturation = null;
  
  // Adresses de livraison du client
  private ArrayList<JsonAdresseWebV3> adressesLivraison = null;
  
  /**
   * Constructeur par défaut pour les flux rentrants.
   */
  public JsonClientWebV3() {
    adressesLivraison = new ArrayList<JsonAdresseWebV3>();
  }
  
  /**
   * Constructeur Client pour Les flux sortants.
   */
  public JsonClientWebV3(FluxMagento pRecord) {
    idFlux = pRecord.getFLIDF();
    versionFlux = pRecord.getFLVER();
    etb = pRecord.getFLETB();
    adressesLivraison = new ArrayList<JsonAdresseWebV3>();
    
    // Gestion du timestamp de création du flux en millisecondes
    long tempsMs = new Date().getTime();
    timestampCrea = String.valueOf(tempsMs);
  }
  
  /**
   * Mise à jour du client sur la base des informations DB2 / flux sortants.
   * Pour le moment, il a été choisi de stocker les données clients Série N - SFCC de la manière suivante:
   * Particulier
   * nomFac : CLNOM,
   * prenomFac : CLCPL,
   * rueFac : CLRUE,
   * rue2Fac : CLLOC
   * la zone "nom" transmise à SFCC doit être blanche ou nulle dans le cas des particuliers
   * Pros:
   * nom : CLNOM,
   * complementNomFac: CLCPL,
   * rueFac : CLRUE,
   * rue2Fac : CLLOC
   */
  public boolean majDesInfosPourExport(GenericRecord pRecordPrincipal) {
    if (pRecordPrincipal == null) {
      majError("[JsonClientWebV3] majDesInfos() clientDB2 à NULL");
      return false;
    }
    
    boolean retour = false;
    int nbErreurs = 0;
    
    // L'identifiant du client traité
    if (!pRecordPrincipal.isPresentField("CLCLI") || !pRecordPrincipal.isPresentField("CLLIV")) {
      majError("[JsonClientWebV3] majDesInfos() clientDB2 corrompu CLCLI CLLIV");
      return false;
    }
    CLCLI = pRecordPrincipal.getField("CLCLI").toString().trim();
    CLLIV = pRecordPrincipal.getField("CLLIV").toString().trim();
    idClient = CLCLI + "/" + CLLIV;
    
    // Numéro d'adhérent (dans Série N) correspondant à l'identifiant du client dans SFCC
    if (pRecordPrincipal.isPresentField("CLADH")) {
      idClientWeb = pRecordPrincipal.getField("CLADH").toString().trim();
    }
    else {
      idClientWeb = "";
    }
    
    // Important car il détermine s'il s'agit d'un particulier ou non
    if (pRecordPrincipal.isPresentField("CLCAT")) {
      categorie = pRecordPrincipal.getField("CLCAT").toString().trim();
    }
    else {
      majError("[ClientWeb] majDesInfos(): categorie NULL");
      nbErreurs++;
    }
    
    if (pRecordPrincipal.isPresentField("CLIN9")) {
      if (pRecordPrincipal.getField("CLIN9").toString().trim().equals("1")) {
        isUnParticulier = ISPARTICULIER;
      }
      CLIN9 = pRecordPrincipal.getField("CLIN9").toString();
    }
    
    // On ne transmet le CLNOM (nom société) et le CLCPL que dans la mesure où le client n'est pas un client particuliers
    if (isUnParticulier == ISPARTICULIER) {
      nomSociete = "";
    }
    else {
      // Le nom de la société
      if (pRecordPrincipal.isPresentField("CLNOM")) {
        nomSociete = pRecordPrincipal.getField("CLNOM").toString().trim();
      }
      else {
        nomSociete = "";
      }
      // Le complément de nom
      if (pRecordPrincipal.isPresentField("CLCPL")) {
        complementNomClient = pRecordPrincipal.getField("CLCPL").toString().trim();
      }
      else {
        complementNomClient = "";
      }
    }
    
    if (pRecordPrincipal.isPresentField("CLAPEN")) {
      codeAPE = pRecordPrincipal.getField("CLAPEN").toString().trim();
    }
    else {
      codeAPE = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLRGL")) {
      codeReglement = pRecordPrincipal.getField("CLRGL").toString().trim();
    }
    else {
      codeReglement = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLRUE")) {
      rueClient = pRecordPrincipal.getField("CLRUE").toString().trim();
    }
    else {
      rueClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLLOC")) {
      rue2Client = pRecordPrincipal.getField("CLLOC").toString().trim();
    }
    else {
      rue2Client = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLVIL") && pRecordPrincipal.isPresentField("CLCDP1")) {
      if (!decouperVilleEtCDPexport(pRecordPrincipal.getField("CLVIL").toString(), pRecordPrincipal.getField("CLCDP1").toString())) {
        cdpClient = "";
        villeClient = "";
      }
    }
    
    // Si le code pays est null ou blanc alors le code pays est initialisé avec la France
    if (pRecordPrincipal.isPresentField("CLCOP") && !pRecordPrincipal.getField("CLCOP").toString().trim().equals("")) {
      paysClient = pRecordPrincipal.getField("CLCOP").toString().trim();
    }
    else {
      paysClient = codePaysDefaut;
    }
    
    if (pRecordPrincipal.isPresentField("CLPAY") && !pRecordPrincipal.getField("CLPAY").toString().trim().equals("")) {
      libellePaysClient = pRecordPrincipal.getField("CLPAY").toString().trim();
    }
    else {
      libellePaysClient = libPaysDefaut;
    }
    
    if (pRecordPrincipal.isPresentField("CLTEL")) {
      telClient = pRecordPrincipal.getField("CLTEL").toString().trim();
    }
    else {
      telClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLFAX")) {
      faxClient = pRecordPrincipal.getField("CLFAX").toString().trim();
    }
    else {
      faxClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLSRN") && pRecordPrincipal.isPresentField("CLSRT")) {
      siren = pRecordPrincipal.getField("CLSRN").toString().trim() + pRecordPrincipal.getField("CLSRT").toString().trim();
    }
    else {
      siren = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLTOP1")) {
      magClient = pRecordPrincipal.getField("CLTOP1").toString().trim();
    }
    else {
      magClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLTOP2")) {
      siteClient = pRecordPrincipal.getField("CLTOP2").toString().trim();
    }
    else {
      siteClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLTAR")) {
      try {
        tarif = Integer.parseInt(pRecordPrincipal.getField("CLTAR").toString().trim());
      }
      catch (Exception e) {
        tarif = null;
      }
    }
    if (pRecordPrincipal.isPresentField("EBIN09")) {
      exclusWeb = pRecordPrincipal.getField("EBIN09").toString().trim();
    }
    
    // SI LE CLIENT EST UN PARTICULIER VA FALLOIR RECUPERER SES INFOS
    if (pRecordPrincipal.isPresentField("RENET")) {
      email = pRecordPrincipal.getField("RENET").toString().trim();
    }
    else {
      email = "";
    }
    
    if (pRecordPrincipal.isPresentField("RECIV")) {
      civiliteClient = pRecordPrincipal.getField("RECIV").toString().trim();
    }
    else {
      civiliteClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("RENOM")) {
      nomClient = pRecordPrincipal.getField("RENOM").toString().trim();
    }
    else {
      nomClient = "";
    }
    if (pRecordPrincipal.isPresentField("REPRE")) {
      prenomClient = pRecordPrincipal.getField("REPRE").toString().trim();
    }
    else {
      prenomClient = "";
    }
    
    if (pRecordPrincipal.isPresentField("CLTFA")) {
      tvaClient = pRecordPrincipal.getField("CLTFA").toString().trim();
    }
    else {
      tvaClient = "";
    }
    
    // Récupération du client facturé si présent
    if (pRecordPrincipal.isPresentField("CLCLFP") && !pRecordPrincipal.getField("CLCLFP").toString().trim().equals("0")) {
      CLCLFP = Constantes.normerTexte(pRecordPrincipal.getField("CLCLFP").toString());
      CLCLFS = "0";
      if (pRecordPrincipal.isPresentField("CLCLFS")) {
        CLCLFS = Constantes.normerTexte(pRecordPrincipal.getField("CLCLFS").toString());
      }
    }
    
    // On fait le point à ce stade
    if (nbErreurs == 0) {
      retour = true;
    }
    
    return retour;
  }
  
  /**
   * Formatage des codes postaux et de la ville du client pour la gestion de Série N :
   * Si on a un code postal numérique on peut mettre le code postal dans la bonne zone cdpClient
   * alors la zone ville c'est [NNNNN][ ][AAAAAAAAA....] où N est numérique sur 5 DIGITS calé à droite
   * et représente le code postal du client et A est le nom de la ville sur 24 caractères Max
   * Si on a un code postal Alpha on colle du 00000 dans cdpClient
   * et on fait le découpage suivant dans la zone de ville CLVIL [00000][ ][AAAAAAAA....]
   * où 00000 signifie qu'on ne contrôle pas ce code postal et qu'il est intégré dans la partie AAA
   * Sous la forme AAAAA AAAAA....
   * Il faut donc le nom de la ville ne fasse plus que 16 caractères maximum
   * Ex : 00000 31840 AUSSONNE
   */
  public void majCdpVilleImport() {
    if (cdpClient == null) {
      cdpClient = "";
    }
    if (villeClient == null) {
      villeClient = "";
    }
    
    String vraiCodePostal = "";
    boolean cdpNumerique = true;
    
    // Formatage du code postal
    cdpClient = cdpClient.trim();
    if (cdpClient.length() > Adresse.LONGUEUR_CODEPOSTAL) {
      cdpClient = cdpClient.substring(0, Adresse.LONGUEUR_CODEPOSTAL);
    }
    vraiCodePostal = cdpClient;
    cdpNumerique = codePostalEstUnEntier(cdpClient);
    if (!cdpNumerique) {
      cdpClient = "99999";
    }
    vraiCodePostal = OutilsMagento.formaterUnCodePostalpourSN(vraiCodePostal);
    
    // Formatage de la ville
    villeClient = villeClient.trim();
    if (villeClient.length() > Adresse.LONGUEUR_VILLE) {
      villeClient = villeClient.substring(0, Adresse.LONGUEUR_VILLE);
    }
    
    // Code postal alphanumérique
    if (!cdpNumerique) {
      villeClient = vraiCodePostal.trim() + " " + villeClient;
      
      if (villeClient.length() > Adresse.LONGUEUR_VILLE) {
        villeClient = villeClient.substring(0, Adresse.LONGUEUR_VILLE);
      }
      villeClient = "00000 " + villeClient;
    }
    // Cas classique
    else {
      villeClient = vraiCodePostal + " " + villeClient;
    }
  }
  
  /**
   * Mise en forme du bloc adresse du client en import de données depuis SFCC.
   */
  public void miseEnFormeBlocAdresseImport() {
    CLNOM = "";
    
    // Si on a affaire à un particulier on va concaténer le nom et le prénom dans la même zone
    // Les véritables données sont dans le contact
    if (isUnParticulier == ISPARTICULIER) {
      if (nomClient != null) {
        CLNOM = nomClient.trim();
      }
      if (prenomClient != null) {
        CLNOM += " " + prenomClient.trim();
      }
      CLNOM = OutilsMagento.formaterTexteLongueurMax(CLNOM, 30);
      complementNomClient = "";
    }
    // Un pro (alors on checke la zone "nom")
    else {
      if (nomSociete != null) {
        if (nomSociete.trim().length() < 30) {
          CLNOM = nomSociete.trim();
        }
        else {
          // On réduit à 60 Max (2 x 30)
          nomSociete = OutilsMagento.formaterTexteLongueurMax(nomSociete.trim(), 60);
          CLNOM = nomSociete.substring(0, 30);
        }
      }
      if (complementNomClient != null) {
        complementNomClient = OutilsMagento.formaterTexteLongueurMax(complementNomClient, 30);
      }
      
    }
    
    CLRUE = "";
    CLLOC = "";
    
    // TODO alog de découp propre VOIR EDI
    if (rueClient != null) {
      CLRUE = rueClient;
    }
    if (rue2Client != null) {
      CLLOC = rue2Client;
    }
  }
  
  /**
   * Lorsqu'on réceptionne un flux Client, on met à jour l'exclusion Web sur la base de la valeur initiale du client Série N
   * N'existe pas -> Géré
   * Exclut -> Géré
   * Géré -> Géré
   * Géré inactif -> Géré inactif
   */
  public void majGestionWeb(String pValeurInit) {
    if (pValeurInit == null || pValeurInit.equals(EXCLUS_WEB) || pValeurInit.equals(GERE_WEB)) {
      EBIN09 = "1";
    }
    else if (pValeurInit.equals(GERE_WEB_INACTIF)) {
      EBIN09 = "2";
    }
    else {
      EBIN09 = " ";
    }
  }
  
  /**
   * Mise à jour du bloc adresse du client facturé sur la base d'un generic record.
   */
  public void majAdresseClientFacturation(GenericRecord pRecord) {
    if (pRecord == null) {
      return;
    }
    
    adresseClientFacturation = new JsonAdresseClientFacturationWebV3();
    
    if (!pRecord.isPresentField("CLCLI") || !pRecord.isPresentField("CLLIV")) {
      majError("[ClientWeb] majAdresseClientFacture() record corrompu CLCLI CLLIV");
      return;
    }
    
    adresseClientFacturation
        .setIdClientFacturation(pRecord.getField("CLCLI").toString().trim() + "/" + pRecord.getField("CLLIV").toString().trim());
    
    if (pRecord.isPresentField("CLADH")) {
      adresseClientFacturation.setIdClientFacturationWeb(pRecord.getField("CLADH").toString().trim());
    }
    
    if (pRecord.isPresentField("CLIN9")) {
      if (pRecord.getField("CLIN9").toString().trim().equals("1")) {
        adresseClientFacturation.setIsUnParticulierClientFacturation(ISPARTICULIER);
      }
      else {
        adresseClientFacturation.setIsUnParticulierClientFacturation(ISPROFESSIONNEL);
      }
    }
    
    if (adresseClientFacturation.getIsUnParticulierClientFacturation() == ISPARTICULIER) {
      adresseClientFacturation.setSocieteClientFacturation("");
    }
    else {
      if (pRecord.isPresentField("CLNOM")) {
        adresseClientFacturation.setSocieteClientFacturation(pRecord.getField("CLNOM").toString().trim());
      }
      if (pRecord.isPresentField("CLCPL")) {
        adresseClientFacturation.setComplementNomClientFacturation(pRecord.getField("CLCPL").toString().trim());
      }
    }
    
    if (pRecord.isPresentField("RECIV")) {
      adresseClientFacturation.setCiviliteClientFacturation(pRecord.getField("RECIV").toString().trim());
    }
    else {
      adresseClientFacturation.setCiviliteClientFacturation("");
    }
    
    if (pRecord.isPresentField("RENOM")) {
      adresseClientFacturation.setNomClientFacturation(pRecord.getField("RENOM").toString().trim());
    }
    else {
      adresseClientFacturation.setNomClientFacturation("");
    }
    
    if (pRecord.isPresentField("REPRE")) {
      adresseClientFacturation.setPrenomClientFacturation(pRecord.getField("REPRE").toString().trim());
    }
    else {
      adresseClientFacturation.setPrenomClientFacturation("");
    }
    
    if (pRecord.isPresentField("CLRUE")) {
      adresseClientFacturation.setRueClientFacturation(pRecord.getField("CLRUE").toString().trim());
    }
    else {
      adresseClientFacturation.setRueClientFacturation("");
    }
    
    if (pRecord.isPresentField("CLLOC")) {
      adresseClientFacturation.setRue2ClientFacturation(pRecord.getField("CLLOC").toString().trim());
    }
    else {
      adresseClientFacturation.setRue2ClientFacturation("");
    }
    
    if (pRecord.isPresentField("CLVIL")) {
      adresseClientFacturation.decouperVilleEtCDP(pRecord.getField("CLVIL").toString().trim());
    }
    
    if (pRecord.isPresentField("CLCOP")) {
      adresseClientFacturation.setPaysClientFacturation(pRecord.getField("CLCOP").toString().trim());
    }
    
    if (pRecord.isPresentField("CLPAY")) {
      adresseClientFacturation.setLibellePaysClientFacturation(pRecord.getField("CLPAY").toString().trim());
    }
    
    if (pRecord.isPresentField("CLTEL")) {
      adresseClientFacturation.setTelClientFacturation(pRecord.getField("CLTEL").toString().trim());
    }
  }
  
  /**
   * Mise à jour de la liste des clients livrés sur la base d'une liste de generic records.
   */
  public void majAdressesClientsLivres(ArrayList<GenericRecord> rcClientsLivres) {
    if (rcClientsLivres == null || rcClientsLivres.size() == 0) {
      return;
    }
    
    for (GenericRecord record : rcClientsLivres) {
      JsonAdresseWebV3 adresse = new JsonAdresseWebV3();
      
      if (!record.isPresentField("CLCLI") || !record.isPresentField("CLLIV")) {
        majError("[ClientWeb] majClientsLivres() record corrompu CLCLI CLLIV");
        return;
      }
      
      adresse.setIdClientLivraison(record.getField("CLCLI").toString().trim() + "/" + record.getField("CLLIV").toString().trim());
      
      if (record.isPresentField("CLIN9")) {
        adresse.setParticulier(record.getField("CLIN9").toString().trim().equals("1"));
      }
      
      if (adresse.isParticulier()) {
        adresse.setSocieteLivraison("");
      }
      else {
        if (record.isPresentField("CLNOM")) {
          adresse.setSocieteLivraison(record.getField("CLNOM").toString().trim());
        }
        if (record.isPresentField("CLCPL")) {
          adresse.setComplementNomLivraison(record.getField("CLCPL").toString().trim());
        }
      }
      
      if (record.isPresentField("RECIV")) {
        adresse.setCiviliteLivraison(record.getField("RECIV").toString().trim());
      }
      else {
        adresse.setCiviliteLivraison("");
      }
      
      if (record.isPresentField("RENOM")) {
        adresse.setNomLivraison(record.getField("RENOM").toString().trim());
      }
      else {
        adresse.setNomLivraison("");
      }
      
      if (record.isPresentField("REPRE")) {
        adresse.setPrenomLivraison(record.getField("REPRE").toString().trim());
      }
      else {
        adresse.setPrenomLivraison("");
      }
      
      if (record.isPresentField("CLRUE")) {
        adresse.setRueLivraison(record.getField("CLRUE").toString().trim());
      }
      else {
        adresse.setRueLivraison("");
      }
      
      if (record.isPresentField("CLLOC")) {
        adresse.setRue2Livraison(record.getField("CLLOC").toString().trim());
      }
      else {
        adresse.setRue2Livraison("");
      }
      
      if (record.isPresentField("CLVIL")) {
        adresse.decouperVilleEtCDP(record.getField("CLVIL").toString().trim());
      }
      
      if (record.isPresentField("CLCOP")) {
        adresse.setPaysLivraison(record.getField("CLCOP").toString().trim());
      }
      
      if (record.isPresentField("CLPAY")) {
        adresse.setLibellePaysLivraison(record.getField("CLPAY").toString().trim());
      }
      
      if (record.isPresentField("CLTEL")) {
        adresse.setTelLivraison(record.getField("CLTEL").toString().trim());
      }
      
      adressesLivraison.add(adresse);
    }
  }
  
  /**
   * Met à jour les informations propres aux clients particulier.
   */
  public void gererModeParticulier() {
    CLIN9 = " ";
    
    if (isUnParticulier == ISPARTICULIER) {
      CLIN9 = "1";
    }
  }
  
  /**
   * Vérification du code codeSerieN transmis soit bien formaté et on l'attribue aux zones code client et suffixe client du client livré
   */
  public boolean decouperCodeERP() {
    if (idClient == null || idClient.trim().equals("")) {
      return false;
    }
    
    String[] tab = idClient.split("/");
    if (tab != null && tab.length == 2) {
      CLCLI = tab[0];
      CLLIV = tab[1];
      
      if (CLCLI.length() > 0 && CLLIV.length() > 0) {
        return true;
      }
    }
    
    return false;
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de découper l'algorithme de Série N pour le cdp et la ville sur la zone CLVIL.
   * Il y a un traitement particulier pour les villes non française: celles ayant le CLCDP1 = 99999.
   * Dans ce cas on découpe le champ CLVIL qui contient : ????? XXXXXX|NOMVILLE
   * - cdpClient = ""
   * - villeClient = "XXXXXX|NOMVILLE"
   * Pour les villes étrangères ne contenant pas de pipe, CLVIL qui contient ????? NOMVILLE
   * - cdpClient = ""
   * - villeClient = "NOMVILLE"
   * Dans le cas des villes françaises le champ CLVIL est sous la forme : 12345 NOMVILLE
   * - cdpClient = "12345"
   * - villeClient = "NOMVILLE"
   */
  private boolean decouperVilleEtCDPexport(String pVille, String pCodePostal) {
    pVille = Constantes.normerTexte(pVille);
    pCodePostal = Constantes.normerTexte(pCodePostal);
    // Contrôle des paramètres
    if (pVille.isEmpty() || pCodePostal.isEmpty()) {
      return false;
    }
    
    // Test pour savoir s'il s'agit d'une ville étrangère.
    if (pCodePostal.equals("99999")) {
      // Récupération du séparateur du code postal
      int indexSpace = pVille.trim().indexOf(' ');
      if (indexSpace == -1) {
        Trace.alerte(
            "[FLUX004] Lors du découpage de la ville et du code postal, le code postal n'est pas codé comme attendu (00000 ?????). Valeur du champ CLVIL=\""
                + pVille + "\".");
        return false;
      }
      
      // S'il y a un pipe il reste dans la ville car c'est un cas particulier pour certains pays comme la Belgique
      cdpClient = "";
      villeClient = pVille.substring(indexSpace + 1);
      return true;
    }
    // Cas pour les villes françaises (ou ayant le même format)
    // Il n'y a pas de pipe dans le champ CLVIL
    // Le code postal est sur 5 caractères suivis d'un espace pour séparer la ville
    else {
      int indexSpace = pVille.indexOf(' ');
      if (indexSpace == -1 || indexSpace != 5) {
        Trace.erreur(
            "[FLUX004] Lors du découpage de la ville et du code postal, le code postal n'est pas codé comme attendu (12345 NOM_VILLE). Valeur du champ CLVIL=\""
                + pVille + "\".");
        return false;
      }
      cdpClient = pVille.substring(0, indexSpace);
      villeClient = pVille.substring(indexSpace + 1);
    }
    
    return true;
  }
  
  /**
   * Controle si les données de base sont valides.
   */
  public boolean isValide() {
    // Contrôle les données de base
    if (getCLCLI() == null || getCLLIV() == null) {
      Trace.erreur("Le numéro et/ou le suffixe du client sont invalides.");
      return false;
    }
    if (getNomSociete() == null) {
      Trace.erreur("Le nom de la société du client est invalide.");
      return false;
    }
    if (getEmail() == null) {
      Trace.erreur("L'adresse mail du client est invalide.");
      return false;
    }
    return true;
  }
  
  // -- Accesseurs
  
  public String getIdClientWeb() {
    return idClientWeb;
  }
  
  public void setIdClientWeb(String idClientWeb) {
    this.idClientWeb = idClientWeb;
  }
  
  public String getEmail() {
    return email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public String getSiren() {
    return siren;
  }
  
  public void setSiren(String siren) {
    this.siren = siren;
  }
  
  public String getCodeAPE() {
    return codeAPE;
  }
  
  public void setCodeAPE(String codeAPE) {
    this.codeAPE = codeAPE;
  }
  
  public String getCategorie() {
    return categorie;
  }
  
  public void setCategorie(String categorie) {
    this.categorie = categorie;
  }
  
  public String getTvaClient() {
    return tvaClient;
  }
  
  public String getNomSociete() {
    return nomSociete;
  }
  
  public void setNomSociete(String nomSociete) {
    this.nomSociete = nomSociete;
  }
  
  public void setTvaClient(String tvaClient) {
    this.tvaClient = tvaClient;
  }
  
  public String getCiviliteClient() {
    return civiliteClient;
  }
  
  public void setCiviliteClient(String civiliteClient) {
    this.civiliteClient = civiliteClient;
  }
  
  public String getNomClient() {
    return nomClient;
  }
  
  public void setNomClient(String nomClient) {
    this.nomClient = nomClient;
  }
  
  public String getComplementNomClient() {
    return complementNomClient;
  }
  
  public void setComplementNomClient(String complementNomClient) {
    this.complementNomClient = complementNomClient;
  }
  
  public String getPrenomClient() {
    return prenomClient;
  }
  
  public void setPrenomClient(String prenomClient) {
    this.prenomClient = prenomClient;
  }
  
  public String getRueClient() {
    return rueClient;
  }
  
  public void setRueClient(String rueClient) {
    this.rueClient = rueClient;
  }
  
  public String getCdpClient() {
    return cdpClient;
  }
  
  public void setCdpClient(String cdpClient) {
    this.cdpClient = cdpClient;
  }
  
  public String getVilleClient() {
    return villeClient;
  }
  
  public void setVilleClient(String villeClient) {
    this.villeClient = villeClient;
  }
  
  public String getPaysClient() {
    return paysClient;
  }
  
  public void setPaysClient(String paysClient) {
    this.paysClient = paysClient;
  }
  
  public String getTelClient() {
    return telClient;
  }
  
  public void setTelClient(String telClient) {
    this.telClient = telClient;
  }
  
  public String getMagClient() {
    return magClient;
  }
  
  public void setMagClient(String magClient) {
    this.magClient = magClient;
  }
  
  public String getSiteClient() {
    return siteClient;
  }
  
  public void setSiteClient(String siteClient) {
    this.siteClient = siteClient;
  }
  
  public String getCLSRN() {
    return CLSRN;
  }
  
  public void setCLSRN(String cLSRN) {
    CLSRN = cLSRN;
  }
  
  public String getCLSRT() {
    return CLSRT;
  }
  
  public void setCLSRT(String cLSRT) {
    CLSRT = cLSRT;
  }
  
  public String getCodeReglement() {
    return codeReglement;
  }
  
  public void setCodeReglement(String codeReglement) {
    this.codeReglement = codeReglement;
  }
  
  public String getCLIN9() {
    return CLIN9;
  }
  
  public void setCLIN9(String cLIN9) {
    CLIN9 = cLIN9;
  }
  
  public String getFaxClient() {
    return faxClient;
  }
  
  public void setFaxClient(String faxClient) {
    this.faxClient = faxClient;
  }
  
  public String getCLCLI() {
    return CLCLI;
  }
  
  public void setCLCLI(String cLCLI) {
    CLCLI = cLCLI;
  }
  
  public String getCLLIV() {
    return CLLIV;
  }
  
  public void setCLLIV(String cLLIV) {
    CLLIV = cLLIV;
  }
  
  /**
   * Retourner le libellé du pays de l'adresse de facturation du client.
   */
  public String getLibellePays() {
    return libellePaysClient;
  }
  
  /**
   * Mettre à jour le libellé du pays de l'adresse du client.
   */
  public void setLibellePaysClient(String libellePaysClient) {
    this.libellePaysClient = libellePaysClient;
  }
  
  public String getRue2Client() {
    return rue2Client;
  }
  
  public void setRue2Client(String rue2Client) {
    this.rue2Client = rue2Client;
  }
  
  public String getCLNOM() {
    return CLNOM;
  }
  
  public void setCLNOM(String cLNOM) {
    CLNOM = cLNOM;
  }
  
  public String getCLRUE() {
    return CLRUE;
  }
  
  public void setCLRUE(String cLRUE) {
    CLRUE = cLRUE;
  }
  
  public String getCLLOC() {
    return CLLOC;
  }
  
  public void setCLLOC(String cLLOC) {
    CLLOC = cLLOC;
  }
  
  public String getTimestampCrea() {
    return timestampCrea;
  }
  
  public void setTimestampCrea(String timestampCrea) {
    this.timestampCrea = timestampCrea;
  }
  
  public String getCLTAR() {
    return CLTAR;
  }
  
  public void setCLTAR(String cLTAR) {
    CLTAR = cLTAR;
  }
  
  public String getCLNCG() {
    return CLNCG;
  }
  
  public void setCLNCG(String cLNCG) {
    CLNCG = cLNCG;
  }
  
  public String getCLCLFP() {
    return CLCLFP;
  }
  
  public String getCLCLFS() {
    return CLCLFS;
  }
  
  public String getEtbComptable() {
    return etbComptable;
  }
  
  public void setEtbComptable(String etbComptable) {
    this.etbComptable = etbComptable;
  }
  
  public String getCompteGeneral() {
    return compteGeneral;
  }
  
  public void setCompteGeneral(String compteGeneral) {
    this.compteGeneral = compteGeneral;
  }
  
  public String getIdClient() {
    return idClient;
  }
  
  public void setIdClient(String pIdClient) {
    this.idClient = pIdClient;
  }
  
  public Integer getTarif() {
    return tarif;
  }
  
  public void setTarif(Integer pTarif) {
    tarif = pTarif;
  }
  
  public ArrayList<JsonAdresseWebV3> getAdressesLivraison() {
    return adressesLivraison;
  }
  
  public void setAdressesLivraison(ArrayList<JsonAdresseWebV3> adressesLivraison) {
    this.adressesLivraison = adressesLivraison;
  }
  
  public String getEBIN09() {
    return EBIN09;
  }
  
  public String getExclusWeb() {
    return exclusWeb;
  }
  
  public int getIsUnParticulier() {
    return isUnParticulier;
  }
  
  public JsonAdresseClientFacturationWebV3 getAdresseClientFacturation() {
    return adresseClientFacturation;
  }
  
  public void setAdresseClientFacturation(JsonAdresseClientFacturationWebV3 adresseClientFacturation) {
    this.adresseClientFacturation = adresseClientFacturation;
  }
  
}
