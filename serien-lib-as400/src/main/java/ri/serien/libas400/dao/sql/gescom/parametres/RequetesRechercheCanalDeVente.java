/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import ri.serien.libcommun.commun.recherche.CriteresBaseRecherche;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.CriteresRechercheCanalDeVente;

public class RequetesRechercheCanalDeVente {
  /**
   * Retourne la requête en fonction du type de recherche.
   */
  public static String getRequete(String curlib, CriteresRechercheCanalDeVente criteres) {
    String requete = null;
    if (criteres == null) {
      return requete;
    }
    
    switch (criteres.getTypeRecherche()) {
      case CriteresRechercheCanalDeVente.RECHERCHE_CANAL_DE_VENTE:
        criteres.setNumeroPage(1);
        criteres.setNbrLignesParPage(CriteresBaseRecherche.TOUTES_LES_LIGNES);
        requete = getRequeteRechercheToutCanauxDeVente(curlib, criteres);
        break;
    }
    return ajouterPagination(requete, criteres);
  }
  
  /**
   * Retourner la liste des canaux de vente pour un établissement donné.
   * L'ordre du résultat conditionne l'affichage dans les listes déroulantes (ordre alphabétique).
   */
  private static String getRequeteRechercheToutCanauxDeVente(String curlib, CriteresRechercheCanalDeVente aCriteres) {
    return "Select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM
        + " where PARTOP <> '9' and PARTYP = 'KV' and PARETB = '" + aCriteres.getCodeEtablissement() + "' ORDER BY PARZ2";
  }
  
  /**
   * Construit la requête qui permet de rechercher une expression dans le nom du canal de vente.
   */
  private static String getRequeteRechercheDansLibelle(String curlib, CriteresRechercheCanalDeVente criteres) {
    String conditions;
    String critere = criteres.getCriterePourRequete();
    if (criteres.isIgnoreCasse()) {
      conditions = "UPPER(SUBSTR(PARZ2, 1, 40)) like '" + critere + "'";
    }
    else {
      conditions = "SUBSTR(PARZ2, 1, 40) like '" + critere + "'";
    }
    return "select PARTOP, PARZ1, PARETB, PARTYP, PARIND, PARZ2, PARZ3, PARZ4 from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM
        + " where PARTOP <> '9' and PARTYP = 'KV' and PARETB = '" + criteres.getCodeEtablissement() + "' and (" + conditions + ")";
  }
  
  /**
   * Ajoute les ordres afin de récupérer qu'une quantité limité de lignes pour la pagination.
   */
  private static String ajouterPagination(String requete, CriteresBaseRecherche criteres) {
    if (criteres.getNbrLignesParPage() != CriteresBaseRecherche.TOUTES_LES_LIGNES) {
      int nbreltpage = criteres.getNbrLignesParPage();
      int indexdeb = (criteres.getNumeroPage() * nbreltpage) - (nbreltpage - 1);
      int indexfin = (indexdeb + nbreltpage) - 1;
      requete = "with vue as (" + requete + ") select * from vue where numrow between " + indexdeb + " and " + indexfin;
    }
    return requete;
  }
}
