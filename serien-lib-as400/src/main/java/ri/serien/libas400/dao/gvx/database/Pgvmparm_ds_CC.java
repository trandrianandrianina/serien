/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvx.database;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_CC pour les CC
 */
public class Pgvmparm_ds_CC extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier.
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "CCETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "CCTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "CCCOD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "CCLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCGSP"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "CCMIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCGLA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCART"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "CCSAN"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCTXA"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCARTC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCDEVC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCART1"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCART2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCART3"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCART4"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCART5"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "CCMMI"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(6, 0), "CCCLR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCNRM"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCARTI"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 2), "CCMMX"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(5, 0), "CCMFC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "CCAFC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "CCSCO"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(7, 0), "CCPLE"));
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(3, 0), "CCRGL"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "CCFMT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "CCFPR"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCFPE"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(1), "CCGCD"));
    
    length = 300;
  }
}
