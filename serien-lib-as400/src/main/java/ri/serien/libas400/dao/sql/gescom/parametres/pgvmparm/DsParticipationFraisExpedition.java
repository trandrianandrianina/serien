/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_PE pour les PE
 */
public class DsParticipationFraisExpedition extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier.
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "PARETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "PARTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PARIND"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES01"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES02"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES03"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES04"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES05"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES06"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES07"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES08"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES09"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "PES10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP01"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP02"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP03"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP04"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP05"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP06"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP07"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP08"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP09"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(4), "PEP10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF01"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF02"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF03"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF04"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF05"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF06"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF07"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF08"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF09"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(6), "PEF10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "PEART"));
    
    length = 300;
  }
}
