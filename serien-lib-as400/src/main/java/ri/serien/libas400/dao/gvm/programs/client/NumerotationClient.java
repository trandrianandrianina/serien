/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.gvm.programs.client;

import java.util.ArrayList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;

public class NumerotationClient {
  
  private static int NUM_MINI_DEFAUT = 1;
  private static int NUM_MAX_DEFAUT = 800000;
  public final static String TABLE_COMPTEURS_AUTO = "PSEMCPTM";
  
  /**
   * Permet de retourner l'identifiant client créé automatiquement
   * sur la base de la règle suivante :
   * On récupère le prochain identifiant disponible dans la table des clients
   * à partir de l'identifiant valorisé en table des compteurs de numérotation
   * automatique des clients : dernierId
   * idClient = dernierId + 1
   * Tant que idClient existe dans la table clients
   * et que NUM_MAX > idClient > NUM_MINI
   * on recherche idClient = idClient + 1
   *
   * Lorsqu'un idClient est disponible on récupère idClient pour numéroter et
   * On met à jour la table des compteurs: dernierId = idClient
   */
  public static int numeroterAutomatiquement(String pEtb, QueryManager pQuery) {
    int nvNumero = -1;
    
    if (pQuery == null || pEtb == null) {
      // TODO LOGS
      Trace.erreur("[NumerotationClient] numeroterAutomatiquement() query ou etb corrompus");
      return nvNumero;
    }
    
    CompteurAutomatique compteur = recupererDernierNumeroClient(pEtb, pQuery);
    if (compteur == null) {
      return nvNumero;
    }
    
    nvNumero = retournerNumeroClientDispo(pQuery, compteur);
    
    if (nvNumero > 0) {
      if (!mettreAjourCompteur(pQuery, pEtb, nvNumero)) {
        nvNumero = -1;
      }
    }
    
    return nvNumero;
  }
  
  /**
   * Permet de retourner le prochain numéro de clietn disponible à partir du cuméro du compteur automatique
   */
  private static int retournerNumeroClientDispo(QueryManager pQuery, CompteurAutomatique pComteur) {
    int numeroDispo = -1;
    int increment = 500;
    if (pQuery == null || pComteur == null) {
      // TODO LOGS
      Trace.erreur("[NumerotationClient] retournerNumeroClientDispo() query ou pComteur corrompus");
      return numeroDispo;
    }
    
    ArrayList<GenericRecord> liste = null;
    int nMini = pComteur.getNumeroActif();
    int nMax = nMini + increment;
    int i = 0;
    int idTrouve = 0;
    int idPrecedent = 0;
    
    while (numeroDispo < 0 && nMax <= NUM_MAX_DEFAUT) {
      liste = pQuery.select(" SELECT CLCLI FROM " + pQuery.getLibrary() + ".PGVMCLIM " + " WHERE CLETB = '" + pComteur.getEtb() + "' "
          + " AND CLCLI >= '" + nMini + "' AND CLCLI <= '" + nMax + "' AND CLLIV = '0' " + " ORDER BY CLCLI ASC ");
      if (liste == null) {
        // TODO log de problème de requete
        Trace.erreur("[NumerotationClient] retournerNumeroClientDispo() liste Nulle");
        nMax = NUM_MAX_DEFAUT + 1;
        break;
      }
      
      // Si la liste c'est vide on prend le dernier id existant
      if (liste.size() == 0) {
        if (idTrouve > 0) {
          numeroDispo = idTrouve + 1;
        }
        else {
          numeroDispo = nMini;
        }
      }
      
      i = 0;
      idPrecedent = nMini;
      idTrouve = 0;
      while (i < liste.size() && numeroDispo < 0) {
        if (liste.get(i).isPresentField("CLCLI")) {
          try {
            idTrouve = Integer.parseInt(liste.get(i).getField("CLCLI").toString().trim());
          }
          catch (NumberFormatException e) {
            // Log l'exception
            Trace.erreur("[NumerotationClient] retournerNumeroClientDispo() " + e.getMessage());
            nMax = NUM_MAX_DEFAUT + 1;
            i = liste.size();
            break;
          }
        }
        if (idTrouve > 0) {
          if (idTrouve == idPrecedent + 1) {
            idPrecedent = idTrouve;
          }
          else if (idTrouve > idPrecedent) {
            numeroDispo = idPrecedent + 1;
          }
        }
        
        i++;
      }
      
      // Si on a pas trouvé de numéro dispo et qu'on est au bout des numéros autorisés
      // alors On break
      if (numeroDispo < 0 && nMax >= NUM_MAX_DEFAUT) {
        numeroDispo = 0;
      }
      
      nMini = nMax + 1;
      nMax = nMini + increment;
      if (nMax > NUM_MAX_DEFAUT) {
        nMax = NUM_MAX_DEFAUT;
      }
    }
    
    return numeroDispo;
  }
  
  /**
   * On récupère le dernier identifiant client saisi en table des compteurs
   * Si on aucun identifiant saisi on retourne 0
   */
  private static CompteurAutomatique recupererDernierNumeroClient(String pEtb, QueryManager pQuery) {
    if (pQuery == null || pEtb == null) {
      // TODO LOGS
      Trace.erreur("[NumerotationClient] recupererDernierNumeroClient()query ou etb corrompus ");
      return null;
    }
    
    ArrayList<GenericRecord> liste = pQuery.select(" SELECT CKETB,CKTYP,CKNUM,CKDEB,CKFIN  FROM " + pQuery.getLibrary() + "."
        + TABLE_COMPTEURS_AUTO + " WHERE CKETB = '" + pEtb + "' AND CKTYP = '" + CompteurAutomatique.CPT_CLIENT + "' ");
    if (liste == null || liste.size() > 1) {
      // TODO LOGS
      Trace.erreur("[NumerotationClient] recupererDernierNumeroClient() liste nulle ou plusieurs résultats");
      return null;
    }
    else if (liste.size() == 0) {
      CompteurAutomatique compteur = new CompteurAutomatique(pEtb, CompteurAutomatique.CPT_CLIENT, 0, NUM_MINI_DEFAUT, NUM_MAX_DEFAUT);
      int retour = pQuery.requete(" INSERT INTO " + pQuery.getLibrary() + "." + TABLE_COMPTEURS_AUTO + " "
          + " (CKETB,CKTYP,CKNUM,CKDEB,CKFIN) VALUES " + " ('" + compteur.getEtb() + "','" + compteur.getType() + "','"
          + compteur.getNumeroActif() + "','" + compteur.getNumeroMini() + "','" + compteur.getNumeroMaxi() + "') ");
      
      if (retour <= 0) {
        // TODO Logs
        Trace.erreur("[NumerotationClient] recupererDernierNumeroClient() INSERT en erreur " + pQuery.getMsgError());
        return null;
      }
      else {
        return compteur;
      }
    }
    else {
      String etb = null;
      String type = null;
      int numero = -1;
      int mini = 0;
      int maxi = 0;
      
      if (liste.get(0).isPresentField("CKETB")) {
        etb = liste.get(0).getField("CKETB").toString().trim();
      }
      if (liste.get(0).isPresentField("CKTYP")) {
        type = liste.get(0).getField("CKTYP").toString().trim();
      }
      if (liste.get(0).isPresentField("CKNUM")) {
        try {
          numero = Integer.parseInt(liste.get(0).getField("CKNUM").toString().trim());
        }
        catch (Exception e) {
          Trace.erreur("[NumerotationClient] recupererDernierNumeroClient() CKNUM valeur non numérique : "
              + liste.get(0).getField("CKNUM").toString().trim());
        }
      }
      if (liste.get(0).isPresentField("CKDEB")) {
        try {
          mini = Integer.parseInt(liste.get(0).getField("CKDEB").toString().trim());
        }
        catch (Exception e) {
          Trace.erreur("[NumerotationClient] recupererDernierNumeroClient() CKDEB valeur non numérique : "
              + liste.get(0).getField("CKDEB").toString().trim());
        }
      }
      if (liste.get(0).isPresentField("CKFIN")) {
        try {
          maxi = Integer.parseInt(liste.get(0).getField("CKFIN").toString().trim());
        }
        catch (Exception e) {
          Trace.erreur("[NumerotationClient] recupererDernierNumeroClient() CKFIN valeur non numérique : "
              + liste.get(0).getField("CKFIN").toString().trim());
        }
      }
      
      if (numero > -1) {
        CompteurAutomatique compteur = new CompteurAutomatique(etb, type, numero, mini, maxi);
        return compteur;
      }
      else {
        Trace.erreur("[NumerotationClient] recupererDernierNumeroClient()  UPDATE en erreur RETOUR: " + numero);
        return null;
      }
    }
  }
  
  /**
   * Permet de mettre à jour la table des compteurs de numérotation automatique des clients
   * J'ai préféré mettre à jour directement la table pour éviter ldes doublons lors des recherches
   */
  private static boolean mettreAjourCompteur(QueryManager pQuery, String pEtb, int pNouveauId) {
    boolean retour = false;
    
    if (pQuery == null || pEtb == null || pNouveauId <= 0) {
      Trace.erreur("[NumerotationClient] mettreAjourCompteur() paramètres corrompus ");
      return retour;
    }
    
    int codeR = pQuery.requete(" UPDATE " + pQuery.getLibrary() + "." + TABLE_COMPTEURS_AUTO + " SET CKNUM = '" + pNouveauId
        + "' WHERE CKTYP = '" + CompteurAutomatique.CPT_CLIENT + "' AND CKETB = '" + pEtb + "' ");
    
    retour = (codeR > 0);
    if (!retour) {
      Trace.erreur("[NumerotationClient] mettreAjourCompteur()  UPDATE en ereur " + pQuery.getMsgError());
    }
    
    return retour;
  }
  
}
