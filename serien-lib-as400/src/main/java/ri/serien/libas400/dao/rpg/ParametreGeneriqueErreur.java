/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.rpg;

import java.beans.PropertyVetoException;

import com.ibm.as400.access.AS400DataType;
import com.ibm.as400.access.AS400Structure;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.ProgramParameter;

import ri.serien.libcommun.outils.MessageErreurException;

public class ParametreGeneriqueErreur extends ProgramParameter {
  // Constantes
  public static final int SIZE_PEIND = 10;
  public static final int SIZE_PENIV = 10;
  public static final int SIZE_PEERR1 = 100;
  public static final int SIZE_PEERR2 = 100;
  public static final int SIZE_PEERR3 = 100;
  public static final int SIZE_PEERR4 = 100;
  public static final int SIZE_PEERR5 = 100;
  public static final int SIZE_PEERR6 = 100;
  public static final int SIZE_PEERR7 = 100;
  public static final int SIZE_PEERR8 = 100;
  public static final int SIZE_PEERR9 = 100;
  public static final int SIZE_PEERR10 = 100;
  public static final int SIZE_PEARR = 1;
  public static final int SIZE_TOTALE_DS = 1021;
  
  // Constantes indices Nom DS
  public static final int VAR_PEIND = 0;
  public static final int VAR_PENIV = 1;
  public static final int VAR_PEERR1 = 2;
  public static final int VAR_PEERR2 = 3;
  public static final int VAR_PEERR3 = 4;
  public static final int VAR_PEERR4 = 5;
  public static final int VAR_PEERR5 = 6;
  public static final int VAR_PEERR6 = 7;
  public static final int VAR_PEERR7 = 8;
  public static final int VAR_PEERR8 = 9;
  public static final int VAR_PEERR9 = 10;
  public static final int VAR_PEERR10 = 11;
  public static final int VAR_PEARR = 12;
  
  // Variables AS400
  public String peind = ""; // Indicateurs
  public String peniv = ""; // Niveaux alertes / erreurs: '9'=erreur, ' '=nop, '0' à '8' niveau d'alerte
  public String peerr1 = ""; // Libellé erreur 1
  public String peerr2 = ""; // Libellé erreur 2
  public String peerr3 = ""; // Libellé erreur 3
  public String peerr4 = ""; // Libellé erreur 4
  public String peerr5 = ""; // Libellé erreur 5
  public String peerr6 = ""; // Libellé erreur 6
  public String peerr7 = ""; // Libellé erreur 7
  public String peerr8 = ""; // Libellé erreur 8
  public String peerr9 = ""; // Libellé erreur 9
  public String peerr10 = ""; // Libellé erreur 10
  public String pearr = ""; // Fin paramètre
  
  // Création de la datastructure
  public AS400DataType[] structure = { new AS400Text(SIZE_PEIND), // Indicateurs
      new AS400Text(SIZE_PENIV), // Niveaux alertes / erreurs
      new AS400Text(SIZE_PEERR1), // Libellé erreur 1
      new AS400Text(SIZE_PEERR2), // Libellé erreur 2
      new AS400Text(SIZE_PEERR3), // Libellé erreur 3
      new AS400Text(SIZE_PEERR4), // Libellé erreur 4
      new AS400Text(SIZE_PEERR5), // Libellé erreur 5
      new AS400Text(SIZE_PEERR6), // Libellé erreur 6
      new AS400Text(SIZE_PEERR7), // Libellé erreur 7
      new AS400Text(SIZE_PEERR8), // Libellé erreur 8
      new AS400Text(SIZE_PEERR9), // Libellé erreur 9
      new AS400Text(SIZE_PEERR10), // Libellé erreur 10
      new AS400Text(SIZE_PEARR), // Fin paramètre
  };
  private AS400Structure ds = new AS400Structure(structure);
  
  /**
   * Initialise la datastructure avec les variables métiers.
   */
  public void setDSInput() {
    try {
      Object[] o = { peind, peniv, peerr1, peerr2, peerr3, peerr4, peerr5, peerr6, peerr7, peerr8, peerr9, peerr10, pearr, };
      setInputData(ds.toBytes(o));
      setOutputDataLength(ds.getByteLength());
    }
    catch (PropertyVetoException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
    catch (IllegalArgumentException e) {
      throw new MessageErreurException(e, "Impossible d'échanger les paramètres avec le programme RPG.");
    }
  }
  
  /**
   * Initialise la datastructure avec les variables AS400.
   */
  public void setDSOutput() {
    Object[] output = (Object[]) ds.toObject(getOutputData());
    peind = (String) output[0];
    peniv = (String) output[1];
    peerr1 = (String) output[2];
    peerr2 = (String) output[3];
    peerr3 = (String) output[4];
    peerr4 = (String) output[5];
    peerr5 = (String) output[6];
    peerr6 = (String) output[7];
    peerr7 = (String) output[8];
    peerr8 = (String) output[9];
    peerr9 = (String) output[10];
    peerr10 = (String) output[11];
    pearr = (String) output[12];
  }
}
