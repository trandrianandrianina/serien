/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres;

import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.gescom.personnalisation.groupe.CritereGroupe;
import ri.serien.libcommun.outils.sql.RequeteSql;

public class RequetesRechercheGroupes {
  
  /**
   * Retourner la requête en fonction des critères de recherche.
   */
  public static String getRequete(String curlib, CritereGroupe pCritere) {
    
    // Construire la requête
    RequeteSql requeteSql = new RequeteSql();
    requeteSql.ajouter("select * from " + curlib + '.' + EnumTableBDD.PARAMETRE_GESCOM + " where");
    if (pCritere.getIdEtablissement() != null) {
      requeteSql.ajouterConditionAnd("PARETB", "=", pCritere.getIdEtablissement().getCodeEtablissement());
    }
    requeteSql.ajouterConditionAnd("PARTYP", "=", "GR");
    if (!pCritere.isAvecAnnule()) {
      requeteSql.ajouterConditionAnd("PARTOP", "<>", "9");
    }
    requeteSql.ajouter("ORDER BY PARZ2");
    return requeteSql.getRequete();
  }
}
