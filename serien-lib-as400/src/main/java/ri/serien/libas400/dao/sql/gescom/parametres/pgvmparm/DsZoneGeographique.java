/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.parametres.pgvmparm;

import com.ibm.as400.access.AS400PackedDecimal;
import com.ibm.as400.access.AS400Text;
import com.ibm.as400.access.AS400ZonedDecimal;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.database.record.DataStructureRecord;

/**
 * Description de l'enregistrement du fichier Pgvmparm_ds_ZG pour les ZG
 */
public class DsZoneGeographique extends DataStructureRecord {
  
  /**
   * Création de la data structure pour l'enregistrement du fichier
   */
  @Override
  public void initRecord() {
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGETB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGTYP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(5), "ZGCOD"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "ZGLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD01"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD02"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD03"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD04"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD05"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD06"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD07"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD08"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD09"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD10"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD11"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD12"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD13"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD14"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD15"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD16"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD17"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD18"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD19"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD20"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD21"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD22"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD23"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD24"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD25"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD26"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD27"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD28"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD29"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD30"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD31"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD32"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD33"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD34"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD35"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD36"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD37"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD38"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD39"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGD40"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD012"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD022"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD032"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD042"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD052"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD062"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD072"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD082"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD092"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(3), "ZGD102"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGREG"));
    
    /*
    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(1, 0), "TOPSYS"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATCRE"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATMOD"));
    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal(7, 0), "DATTRT"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(10), "INDIC"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "ZGLIB"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(80), "ZGDEP"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(30), "ZGDEP2"));
    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(2), "ZGREG"));*/
    
    length = 300;
  }
}
