/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libas400.dao.sql.gescom.flux.flx008.v2;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;

import ri.serien.libas400.dao.gvm.programs.InjecteurBdVetF;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxInit;
import ri.serien.libas400.dao.sql.gescom.flux.commun.FluxMagento;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi.CommandeEDI_GP;
import ri.serien.libas400.dao.sql.gescom.flux.commun.v2.edi.Partenaire_EDI;
import ri.serien.libas400.database.BaseGroupDB;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.AlmostQtemp;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.ftp.FTPManager;

/**
 * classe de traitement des imports de commandes EDI
 */
public class GM_Import_Cdes_EDI extends BaseGroupDB {
  private FTPManager serveurFTP = null;
  // QueryManager querymg = null;
  private SystemeManager system = null;
  private String lettreEnv = null;
  private File dirEdi = null;
  private File dirEdiCdes = null;
  private File dirEdiExp = null;
  private File dirEdiFact = null;
  private File dirEdiArch = null;
  private File dirEdi40Aine = null;
  private ParametresFluxBibli parametresFlux = null;
  private SurveillanceJob surveillanceJob = null;
  
  /**
   * Constructeur.
   */
  public GM_Import_Cdes_EDI(ParametresFluxBibli pParametresFluxBibli, QueryManager pQueryManager, SystemeManager pSystemeManager,
      String pLettre, SurveillanceJob pSurveillanceJob) {
    super(pQueryManager);
    parametresFlux = pParametresFluxBibli;
    system = pSystemeManager;
    lettreEnv = pLettre;
    surveillanceJob = pSurveillanceJob;
  }
  
  /**
   * Récupère les commandes EDI sur le serveur EDI de la classe
   */
  public boolean recupererCommandeEDI(FluxMagento record) {
    if (record == null) {
      majError("(GM_Import_Cdes_EDI] recupererCommandeEDI() record NULL");
      return false;
    }
    
    gestionArborescence();
    
    boolean retour = false;
    ArrayList<CommandeEDI_GP> listeCdes = null;
    // On récupère les commandes et on les traite
    int nbCommandesRecup = telechargerCommandesFTP(record);
    // Si on a récupéré des commandes
    if (nbCommandesRecup > 0) {
      listeCdes = traiterLesCommandesRecues(record);
      
      // On envoie les commandes valides
      if (listeCdes != null && listeCdes.size() > 0) {
        retour = injecterLesCommandes(listeCdes, record);
      }
      // On ne renvoie un résultat positif que si toutes les commandes récupérées ont été injectées
      if (!retour || (listeCdes != null && listeCdes.size() != nbCommandesRecup)) {
        return false;
      }
    }
    // Si on en a pas récupéré et que c'est normal
    else if (nbCommandesRecup == 0) {
      return true;
    }
    else if (nbCommandesRecup < 0) {
      return false;
    }
    
    return retour;
  }
  
  /**
   * Telecharger d'éventuelles commandes EDI à partir du serveur FTP @GP -> dossier temporaire EDI sur AS400
   */
  private int telechargerCommandesFTP(FluxMagento record) {
    if (record == null) {
      majError("(GM_Import_Cdes_EDI] telechargerCommandesFTP() record NULL");
      return -1;
    }
    // Etape 1 récupérer les commandes sur le serveur FTP @GP
    if (serveurFTP == null) {
      serveurFTP = new FTPManager();
    }
    
    // boolean retour = true;
    int nbCommandesrecuperees = -1;
    int nbCommandesTheo = 0;
    // On se connecte au serveur FTP distant
    if (serveurFTP.connexion(parametresFlux.getUrl_ftp_EDI(), parametresFlux.getLogin_ftp_EDI(), parametresFlux.getPassword_ftp_EDI())) {
      try { // On se positionne dans le bon dossier
        if (serveurFTP.getConnexion().changeWorkingDirectory(parametresFlux.getDossierCdes_ftp_EDI())) {
          serveurFTP.getConnexion().enterLocalPassiveMode();
          serveurFTP.getConnexion().setFileType(FTP.BINARY_FILE_TYPE);
          
          FTPFile[] listeFichiers = serveurFTP.getConnexion().listFiles();
          
          // S'il existe de fichiers à récupérer
          if (listeFichiers != null && listeFichiers.length > 0) {
            nbCommandesrecuperees = 0;
            for (int i = 0; i < listeFichiers.length; i++) {
              // Désormais on ne récupère pas toutes les commandes dans un flux. UN FLUX = UN FICHIER EDI = UNE COMMANDE
              // on vérifie que la commande recherchée
              // if(listeFichiers[i].getName().startsWith(ParametresFlux.getValeurString("PREFIXE_EDI_ORDERS")) &&
              // listeFichiers[i].isFile())
              if (listeFichiers[i].getName().equals(record.getFLCOD().trim()) && listeFichiers[i].isFile()) {
                nbCommandesTheo++;
                String remoteFile1 = listeFichiers[i].getName();
                File downloadFile1 = new File(parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator
                    + parametresFlux.getDossier_local_cdes_EDI() + listeFichiers[i].getName());
                
                OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
                boolean success = serveurFTP.getConnexion().retrieveFile(remoteFile1, outputStream1);
                outputStream1.close();
                
                if (success) {
                  if (serveurFTP.getConnexion().deleteFile(listeFichiers[i].getName())) {
                    nbCommandesrecuperees++;
                  }
                  else {
                    majError("telechargerCommandesFTP() "
                        + (listeFichiers[i].getName() + " PB de suppression fichier " + listeFichiers[i].getName()));
                  }
                }
              }
            }
            
            // On teste la cohérence entre les fichiers scannés et ceux récupérés
            if (nbCommandesTheo != nbCommandesrecuperees) {
              nbCommandesrecuperees = -1;
            }
          }
          // Pas de fichier à récupérer mais pas d'erreur
          else if (listeFichiers != null) {
            nbCommandesrecuperees = 0;
          }
          else {
            majError("telechargerCommandesFTP() listeFichiers NULLE ");
          }
          
          serveurFTP.disconnect();
        }
        else {
          majError("[GM_Import_Cdes_EDI] changeWorkingDirectory(): " + serveurFTP.getMsgError());
          return nbCommandesrecuperees;
        }
      }
      catch (IOException e) {
        e.printStackTrace();
        majError("[GM_Import_Cdes_EDI] recupererCommandeEDI(): PD changeWorkingDirectory " + parametresFlux.getDossierCdes_ftp_EDI()
            + " -> " + e.getMessage());
        return nbCommandesrecuperees;
      }
      serveurFTP.disconnect();
    }
    else {
      majError("[GM_Import_Cdes_EDI] recupererCommandeEDI(): " + serveurFTP.getMsgError());
      return nbCommandesrecuperees;
    }
    
    return nbCommandesrecuperees;
  }
  
  /**
   * On scanne tous les fichiers reçus, on les transforme en Entités commandes et on teste leur structure
   */
  private ArrayList<CommandeEDI_GP> traiterLesCommandesRecues(FluxMagento record) {
    ArrayList<CommandeEDI_GP> tableauDeCommandes = null;
    // String retour = "";
    String fileContent = null;
    // boolean res = false;
    FileNG repertoire = new FileNG(
        parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator + parametresFlux.getDossier_local_cdes_EDI());
    File[] files = repertoire.listFiles();
    
    if (files != null && files.length > 0) {
      tableauDeCommandes = new ArrayList<CommandeEDI_GP>();
      
      for (int i = 0; i < files.length; i++) {
        fileContent = "";
        
        // On contrôle à nouveau la commande
        if (files[i].getName().startsWith(parametresFlux.getPrefixe_orders_EDI()) && files[i].isFile()) {
          try {
            // lecture fichier
            InputStream flux = new FileInputStream(files[i]);
            InputStreamReader lecture = new InputStreamReader(flux, "UTF-8");
            BufferedReader buff = new BufferedReader(lecture);
            String ligne;
            while ((ligne = buff.readLine()) != null) {
              // On écrit le contenu du fichier
              fileContent += ligne + "\n";
            }
            buff.close();
          }
          catch (Exception e) {
            majError(e.toString());
          }
          // ON a bien du contenu
          if (fileContent.length() > 0) {
            CommandeEDI_GP commande = new CommandeEDI_GP(parametresFlux, fileContent, record.getFLETB(), files[i].getName());
            // On vérifie qu'à ce stade elle soit déjà valide et on teste les données métier
            if (commande.isValide()) {
              controlerLesDonneesSN(commande);
              tableauDeCommandes.add(commande);
              // TODO ENVOI DU MAIL avec la gestion des erreurs de la commande
            }
            else {
              record.setFLERR(FluxMagento.ERR_TRAITEMENT);
              copierFichier(dirEdiCdes.getAbsolutePath() + File.separator + commande.getFichierOrigine(),
                  dirEdi40Aine.getAbsolutePath() + File.separator + commande.getFichierOrigine());
            }
          }
          else {
            majError("[GM_Import_Cdes_EDI] contenu fileContent corrompu ");
          }
        }
      }
    }
    
    return tableauDeCommandes;
  }
  
  /**
   * On contrôle l'existence des données de cette commande dans Série N (client, articles...)
   */
  private void controlerLesDonneesSN(CommandeEDI_GP commande) {
    if (commande == null || commande.getETB() == null) {
      majError("controlerLesDonneesSN() commande nulle ou corrompue");
      return;
    }
    
    // On vérifie les clients et le fournisseur
    controlerDifferentsPartenaires(commande);
    // ON vérifie si cette commande n'existe pas déjà afin de le repasser par mégarde
    if (commande.isValide()) {
      controlerCommandeClient(commande);
    }
    // On vérifie les articles
    if (commande.isValide()) {
      controlerDifferentsArticles(commande);
    }
  }
  
  private void gestionArborescence() {
    dirEdi = new File(parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator);
    if (!dirEdi.exists()) {
      dirEdi.mkdirs();
    }
    
    dirEdiCdes = new File(
        parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator + parametresFlux.getDossier_local_cdes_EDI());
    if (!dirEdiCdes.exists()) {
      dirEdiCdes.mkdirs();
    }
    dirEdiExp = new File(
        parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator + parametresFlux.getDossier_local_expe_EDI());
    if (!dirEdiExp.exists()) {
      dirEdiExp.mkdirs();
    }
    dirEdiFact = new File(
        parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator + parametresFlux.getDossier_local_fact_EDI());
    if (!dirEdiFact.exists()) {
      dirEdiFact.mkdirs();
    }
    dirEdiArch = new File(
        parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator + parametresFlux.getDossier_local_arch_EDI());
    if (!dirEdiArch.exists()) {
      dirEdiArch.mkdirs();
    }
    dirEdi40Aine = new File(
        parametresFlux.getDossier_local_tmp_EDI() + "ENV" + lettreEnv + File.separator + parametresFlux.getDossier_local_qtaine_EDI());
    if (!dirEdi40Aine.exists()) {
      dirEdi40Aine.mkdirs();
    }
  }
  
  /**
   * On contrôle l'existence de la commande EDI dans la base Série N.
   * On se base sur l'dentifiant du client et le numéro de commande dans "commande initiale"
   */
  private void controlerCommandeClient(CommandeEDI_GP commande) {
    if (commande == null) {
      majError("controlerCommandeClient() commande nulle");
      return;
    }
    
    commande.setValide(false);
    if (commande.getETB() == null) {
      majError("controlerCommandeClient() commande corrompue");
      return;
    }
    if (commande.getClientAcheteur() == null) {
      return;
    }
    
    ArrayList<GenericRecord> liste = null;
    String requete = "" + " SELECT E1NUM " + " FROM " + queryManager.getLibrary() + ".PGVMEBCM " + " WHERE E1COD = 'E' AND E1ETB = '"
        + commande.getETB() + "' " + " AND E1VDE = '" + parametresFlux.getVendeur_EDI() + "' " + " AND E1CLFP = '"
        + commande.getClientAcheteur().getIdSerieN() + "' " + " AND E1CLFS = '" + commande.getClientAcheteur().getSuffixeSerieN() + "' "
        + " AND E1CCT = '" + commande.getNumero_document_3() + "' ";
    
    liste = queryManager.select(requete);
    
    if (liste == null) {
      majError("[GM_Import_Cdes_EDI] controlerCommandeClient() LISTE nulle pour le fichier " + commande.getFichierOrigine() + " "
          + queryManager.getMsgError());
    }
    if (liste != null && liste.size() > 0) {
      majError(
          "[GM_Import_Cdes_EDI] controlerCommandeClient() il existe déjà des commandes pour le fichier " + commande.getFichierOrigine());
    }
    
    commande.setValide(liste != null && liste.size() == 0);
  }
  
  /**
   * On contrôle les différents partenaire clients et fournisseur et on attribue le code Série N
   */
  private void controlerDifferentsPartenaires(CommandeEDI_GP commande) {
    if (commande == null || commande.getETB() == null) {
      majError("controlerLesDonneesSN() commande nulle ou corrompue");
      return;
    }
    commande.setValide(false);
    
    ArrayList<GenericRecord> liste = null;
    ArrayList<Partenaire_EDI> listePartenaires = new ArrayList<Partenaire_EDI>();
    String codeEAN = null;
    String conditionGCD = " AND GCGCD = '' ";
    String requete = "" + " SELECT DIGITS(CLCLI) AS IDCLI, DIGITS(CLLIV) AS SUFCLI " + " FROM " + queryManager.getLibrary() + ".PGVMCLIM "
        + " LEFT JOIN " + queryManager.getLibrary() + ".PGVMGCDM ON GCETB = CLETB AND GCTYP = 'C' AND DIGITS(CLCLI)||DIGITS(CLLIV) = GCCOD "
        + " WHERE GCETB = '" + commande.getETB() + "' ";
    
    // On a un acheteur
    if (commande.getClientAcheteur() != null) {
      listePartenaires.add(commande.getClientAcheteur());
    }
    // On a un facturé
    if (commande.getClientFacture() != null) {
      listePartenaires.add(commande.getClientFacture());
    }
    // On a un livré
    if (commande.getClientLivre() != null) {
      listePartenaires.add(commande.getClientLivre());
    }
    
    // On parcourt la liste et on checke les clients
    int controles = 0;
    for (int i = 0; i < listePartenaires.size(); i++) {
      codeEAN = listePartenaires.get(i).getCodeEANPartenaire();
      conditionGCD = "";
      if (codeEAN != null && codeEAN.length() == 13) {
        conditionGCD = " AND GCGCD = '" + codeEAN + "' ";
        liste = queryManager.select(requete + conditionGCD);
        if (liste != null) {
          if (liste.size() == 1) {
            if (liste.get(0).isPresentField("IDCLI") && liste.get(0).isPresentField("SUFCLI")) {
              listePartenaires.get(i).setIdSerieN(liste.get(0).getField("IDCLI").toString().trim());
              listePartenaires.get(i).setSuffixeSerieN(liste.get(0).getField("SUFCLI").toString().trim());
              controles++;
            }
            else {
              majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " IDCLI/SUFCLI pas présents pour " + codeEAN);
              break;
            }
          }
          else {
            majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " Client inexistant (ou pls clients) avec le gencod "
                + codeEAN + ": " + liste.size());
            break;
          }
        }
        else {
          majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " PB de récupération du client avec le gencod " + codeEAN
              + ": " + queryManager.getMsgError());
          break;
        }
      }
      else {
        majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " le partenaire " + listePartenaires.get(i).getTypePartenaire()
            + " a un code EAN corrompu pour la commande " + commande.getNumero_document_3() + "/ code EAN: " + codeEAN);
        break;
      }
    }
    // on retourne le résultat de la cohérence entre les maj et le nb de partenaires concernés
    commande.setValide((controles == listePartenaires.size() && controles > 0));
  }
  
  /**
   * On contrôle les différents articles de la commande et on attribue le code Série N
   */
  private void controlerDifferentsArticles(CommandeEDI_GP commande) {
    if (commande == null || commande.getETB() == null) {
      majError("controlerLesDonneesSN() commande nulle ou corrompue");
      return;
    }
    commande.setValide(false);
    if (commande.getLignesArticles() == null || commande.getLignesArticles().size() == 0) {
      majError("controlerLesDonneesSN() " + commande.getFichierOrigine() + " getLignesArticles nulle ou corrompue");
      return;
    }
    
    ArrayList<GenericRecord> liste = null;
    String conditionGCD = " AND A1GCD = '' ";
    String requete =
        "" + " SELECT A1ART " + " FROM " + queryManager.getLibrary() + ".PGVMARTM " + " WHERE A1ETB = '" + commande.getETB() + "' ";
    
    int controles = 0;
    for (int i = 0; i < commande.getLignesArticles().size(); i++) {
      conditionGCD = "";
      if (commande.getLignesArticles().get(i).getCode_EAN_produit_3() != null) {
        conditionGCD = " AND A1GCD = '" + commande.getLignesArticles().get(i).getCode_EAN_produit_3() + "' ";
        liste = queryManager.select(requete + conditionGCD);
        if (liste != null) {
          if (liste.size() == 1) {
            if (liste.get(0).isPresentField("A1ART")) {
              commande.getLignesArticles().get(i).setCodeArticleSN(liste.get(0).getField("A1ART").toString().trim());
              controles++;
            }
            else {
              majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " A1ART pas présents pour "
                  + commande.getLignesArticles().get(i).getCode_EAN_produit_3());
              break;
            }
          }
          else {
            majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " Article inexistant (ou pls articles) avec le gencod "
                + commande.getLignesArticles().get(i).getCode_EAN_produit_3() + ": " + liste.size());
            break;
          }
        }
        else {
          majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " PB de récupération de l'article avec le gencod "
              + commande.getLignesArticles().get(i).getCode_EAN_produit_3() + ": " + queryManager.getMsgError());
          break;
        }
      }
      else {
        majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + " getCode_EAN_produit_3 non présent pour  "
            + commande.getLignesArticles().get(i).getCode_client_produit_5());
        break;
      }
    }
    
    // on retourne le résultat de la cohérence entre les maj et le nb de partenaires concernés
    commande.setValide((controles == commande.getLignesArticles().size() && controles > 0));
  }
  
  /**
   * On teste et on injecte une commandes réceptionnée
   */
  private boolean injecterUneCommande(CommandeEDI_GP commande, FluxMagento record) {
    if (commande == null || record == null) {
      return false;
    }
    SystemeManager system = new SystemeManager(true);
    
    boolean retour = false;
    LinkedHashMap<String, String> rapport = new LinkedHashMap<String, String>();
    // TODO Remplacer par le DIGITS de la commande Magento
    int numeroAleatoire = 155;
    // Création de la "QTEMP" à partir de l'ID utilisateur ici valeur
    AlmostQtemp qtemp = new AlmostQtemp(system.getSystem(), numeroAleatoire);
    if (qtemp.create()) {
      // On initialise le gestionnaire d'injection de bons JAVA
      if (lettreEnv != null && queryManager != null) {
        InjecteurBdVetF sgvmiv = new InjecteurBdVetF(system, qtemp.getName(), queryManager.getLibrary(), lettreEnv.charAt(0),
            ParametresFluxBibli.getBibliothequeEnvironnement());
        retour = sgvmiv.initialiser(surveillanceJob);
        if (!retour) {
          majError("[GM_Import_Cdes_EDI]  sgvmiv.init(): " + sgvmiv.getMsgError());
          system.deconnecter();
          return retour;
        }
        else {
          sgvmiv.setGestionComplementEntete(true);
          // Nettoyage des fichiers physiques PINJBDV*
          retour = sgvmiv.clearAndCreateAllFilesInWrkLib();
          if (!retour) {
            majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + "  sgvmiv.clearAndCreateAllFilesInWrkLib(): "
                + sgvmiv.getMsgError());
            system.deconnecter();
            return retour;
          }
          else {
            ArrayList<GenericRecord> listeRcds = new ArrayList<GenericRecord>();
            
            // Création de la ligne d'entête du bon
            GenericRecord enteteBon = new GenericRecord();
            
            enteteBon.setField("BEETB", record.getFLETB());
            enteteBon.setField("BEMAG", parametresFlux.getMagasinMagento());
            enteteBon.setField("BEDAT", commande.formaterUneDateEDI_SN(commande.getDate_document_4()));
            enteteBon.setField("BEDLS", commande.formaterUneDateEDI_SN(commande.getDate_livraison_7()));
            enteteBon.setField("BERBV", "");
            enteteBon.setField("BEERL", "E");
            enteteBon.setField("BENLI", 0);
            
            // TODO Faudra paramétrer un Code vendeur EDI
            enteteBon.setField("BEVDE", parametresFlux.getVendeur_EDI());
            enteteBon.setField("BEMEX", parametresFlux.getEnlevement_EDI());
            // TODO BOn là on a une confuse à l'analyse: la donnée dans la livraison est en fait ... le code transporteur Du cou p on se
            // sert pas de BEMEX
            // enteteBon.setField("BECTR",Mode);
            // enteteBon.setField("BERG2", commande.getModeReglement());
            // enteteBon.setField("BESAN", commande.getIdSite());
            enteteBon.setField("BEOPT", "0");
            
            // TODO ALLEZ HOP ON REPART POUR UNE FUCKING SEANCE DE REDECOUPAGE DES ZONES EDI ADRESSES EN ZONES SERIE N -> PLUS TAAAARD
            enteteBon.setField("BENCLP", commande.getClientLivre().getIdSerieN());
            enteteBon.setField("BENCLS", commande.getClientLivre().getSuffixeSerieN());
            enteteBon.setField("BENOML", commande.getClientLivre().getBENOM());
            enteteBon.setField("BECPLL", commande.getClientLivre().getBECPL());
            enteteBon.setField("BERUEL", commande.getClientLivre().getBERUE());
            enteteBon.setField("BELOCL", commande.getClientLivre().getBELOC());
            enteteBon.setField("BECDPL", commande.getClientLivre().getCdpPart());
            enteteBon.setField("BEVILL",
                commande.tronquerUneZone(commande.getClientLivre().getVillePart(), ParametresFluxInit.TAILLE_MAX_CHAMP_VILLE));
            enteteBon.setField("BEPAYL", commande.getClientLivre().getLibellePays());
            
            // TODO CA VA BOUGER AVEC LE FONCTIONNEMENT DE OPNK CLIENT PAYEUR CLIENT LIVRE CLIENT FACTURE
            // CLIENT PAYEUR (COMMANDE PAR) EDI = CLIENT FACTURE SERIE N
            // CLIENT LIVRE EDI = CLIENT LIVRE SERIE N
            // CLIENT FACTURE (QUE POUR BRICO DEPOT) = CLIENT PAYEUR SERIE N CLCLP dans PGVMCLIM FACTURE
            Partenaire_EDI partenaireAfacturer = null;
            if (commande.getClientAcheteur() != null) {
              partenaireAfacturer = commande.getClientAcheteur();
            }
            
            if (partenaireAfacturer != null) {
              enteteBon.setField("BECLF", partenaireAfacturer.getIdSerieN() + partenaireAfacturer.getSuffixeSerieN());
              enteteBon.setField("BENOMF", partenaireAfacturer.getBENOM());
              enteteBon.setField("BECPLF", partenaireAfacturer.getBECPL());
              enteteBon.setField("BERUEF", partenaireAfacturer.getBERUE());
              enteteBon.setField("BELOCF", partenaireAfacturer.getBELOC());
              enteteBon.setField("BECDPF", partenaireAfacturer.getCdpPart());
              enteteBon.setField("BEVILF",
                  partenaireAfacturer.tronquerUneZone(partenaireAfacturer.getVillePart(), ParametresFluxInit.TAILLE_MAX_CHAMP_VILLE));
              enteteBon.setField("BEPAYF", partenaireAfacturer.getLibellePays());
            }
            
            listeRcds.add(enteteBon);
            
            // sgvmiv.setGestionComplementEntete(false);
            if (sgvmiv.isGestionComplementEntete()) {
              // Création de la ligne d'entête du bon complémentaire
              GenericRecord enteteComp = new GenericRecord();
              
              enteteComp.setField("BFETB", commande.getETB());
              enteteComp.setField("BFMAG", parametresFlux.getMagasinMagento());
              enteteComp.setField("BFDAT", commande.formaterUneDateEDI_SN(commande.getDate_document_4()));
              enteteComp.setField("BFRBV", "");
              enteteComp.setField("BFERL", "F");
              enteteComp.setField("BFNLI", 0);
              enteteComp.setField("BFCCT", commande.getNumero_document_3());
              /*enteteComp.setField("BFTTC", commande.getTotalTTC());
              enteteComp.setField("BFTHTL", commande.getTotalHT());*/
              
              listeRcds.add(enteteComp);
            }
            
            int numLignes = 0;
            // Articles commentaires
            if (commande.getBlocCommentaires() != null) {
              GenericRecord commentaire = new GenericRecord();
              numLignes = numLignes + 10;
              // lignes commentaires
              commentaire.setField("BLETB", commande.getETB());
              commentaire.setField("BLMAGE", parametresFlux.getMagasinMagento());
              commentaire.setField("BLDAT", commande.formaterUneDateEDI_SN(commande.getDate_document_4()));
              commentaire.setField("BLRBV", "");
              commentaire.setField("BLNLI", numLignes);
              commentaire.setField("BLERL", "L");
              for (int j = 0; j < commande.getBlocCommentaires().size(); j++) {
                switch (j) {
                  case 0:
                    commentaire.setField("BLLIB1", commande.getBlocCommentaires().get(j));
                    break;
                  case 1:
                    commentaire.setField("BLLIB2", commande.getBlocCommentaires().get(j));
                    break;
                  case 2:
                    commentaire.setField("BLLIB3", commande.getBlocCommentaires().get(j));
                    break;
                  case 3:
                    commentaire.setField("BLLIB4", commande.getBlocCommentaires().get(j));
                    break;
                  
                  default:
                    break;
                }
              }
              listeRcds.add(commentaire);
            }
            
            // Préparation de la clef pour les lignes du futur bon
            for (int j = 0; j < commande.getLignesArticles().size(); j++) {
              numLignes = numLignes + 10;
              GenericRecord article = new GenericRecord();
              article.setField("BLETB", commande.getETB());
              article.setField("BLMAGE", parametresFlux.getMagasinMagento());
              article.setField("BLDAT", commande.formaterUneDateEDI_SN(commande.getDate_document_4()));
              article.setField("BLRBV", "");
              article.setField("BLNLI", "" + (numLignes));
              article.setField("BLERL", "L");
              article.setField("BLART", commande.getLignesArticles().get(j).getCodeArticleSN());
              
              article.setField("BLQTE", commande.getLignesArticles().get(j).getQuantite_ligne_7());
              article.setField("BLPRX", commande.getLignesArticles().get(j).getPrixU_net_ligne_9());
              
              listeRcds.add(article);
            }
            
            // On insert dans le
            retour = sgvmiv.insertRecord(listeRcds);
            if (!retour) {
              majError("[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + "  sgvmiv.insertRecord: " + sgvmiv.getMsgError());
              system.deconnecter();
              return retour;
            }
            else {
              // On injecte le PINJBDV avec le rapport mis à jour: soit avec des erreurs soit avec les infos du bon créé
              retour = sgvmiv.injectFile(rapport);
              // On a un souci
              if (!retour) {
                record.setFLJSN("");
                majError(
                    "[GM_Import_Cdes_EDI] " + commande.getFichierOrigine() + "  PB sgvmiv.injectFile: " + sgvmiv.getMsgError() + rapport);
                system.deconnecter();
                return retour;
              }
              else {
                if (rapport.containsKey("TYPE") && rapport.containsKey("ETB") && rapport.containsKey("NUMERO")
                    && rapport.containsKey("SUFFIXE")) {
                  record.setFLJSN("Commande Série N -" + rapport.get("ETB") + "/" + rapport.get("NUMERO") + "/" + rapport.get("SUFFIXE")
                      + "- pour le fichier -" + commande.getFichierOrigine() + "-\n");
                  retour = true;
                }
              }
            }
          }
        }
      }
      else {
        majError("[GM_Import_Commandes]  PB  lettre: " + lettreEnv + " ou system ou queryMng Nulls");
      }
      
      retour = qtemp.delete();
      
      if (!retour) {
        majError("[GM_Import_Cdes_EDI]  qtemp.delete(): " + qtemp.getMsgError());
      }
    }
    else {
      majError("[GM_Import_Commandes]  PB qtemp.create(): " + qtemp.getMsgError());
    }
    
    try {
      system.deconnecter();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la déconnexion.");
    }
    
    return retour;
  }
  
  /**
   * On teste et on injecte les commandes réceptionnées
   */
  private boolean injecterLesCommandes(ArrayList<CommandeEDI_GP> listeCdes, FluxMagento record) {
    if (listeCdes == null || record == null) {
      return false;
    }
    
    // boolean retour = false;
    String logCdes = "";
    int nbCdsTheo = 0;
    int nbCdsInj = 0;
    nbCdsTheo = listeCdes.size();
    // On tente d'injecter une à une
    for (int i = 0; i < listeCdes.size(); i++) {
      if (injecterUneCommande(listeCdes.get(i), record)) {
        copierFichier(dirEdiCdes.getAbsolutePath() + File.separator + listeCdes.get(i).getFichierOrigine(),
            dirEdiArch.getAbsolutePath() + File.separator + listeCdes.get(i).getFichierOrigine());
        if (record.getFLJSN() != null) {
          logCdes += record.getFLJSN();
        }
        nbCdsInj++;
      }
      else {
        copierFichier(dirEdiCdes.getAbsolutePath() + File.separator + listeCdes.get(i).getFichierOrigine(),
            dirEdi40Aine.getAbsolutePath() + File.separator + listeCdes.get(i).getFichierOrigine());
      }
      
    }
    
    if (nbCdsInj > 0) {
      record.setFLJSN(logCdes);
    }
    
    return nbCdsTheo == nbCdsInj;
  }
  
  private boolean copierFichier(String srce, String dest) {
    File Source = new File(srce);
    if (!Source.isFile()) {
      return false;
    }
    
    File Destination = new File(dest);
    
    boolean resultat = false;
    FileInputStream filesource = null;
    FileOutputStream fileDestination = null;
    try {
      filesource = new FileInputStream(Source);
      fileDestination = new FileOutputStream(Destination);
      byte buffer[] = new byte[512 * 1024];
      int nblecture;
      while ((nblecture = filesource.read(buffer)) != -1) {
        fileDestination.write(buffer, 0, nblecture);
      }
      resultat = true;
    }
    catch (FileNotFoundException nf) {
      majError("[copierFichier]  " + nf.getMessage());
    }
    catch (IOException io) {
      majError("[copierFichier]  " + io.getMessage());
    }
    finally {
      try {
        if (filesource != null) {
          filesource.close();
        }
      }
      catch (Exception e) {
        majError("[copierFichier]  " + e.getMessage());
      }
      try {
        if (fileDestination != null) {
          fileDestination.close();
        }
      }
      catch (Exception e) {
        majError("[copierFichier]  " + e.getMessage());
      }
    }
    // On supprime le fichier que si la copie s'est bien passée
    if (resultat) {
      resultat = Source.delete();
    }
    
    return resultat;
  }
  
  @Override
  public void dispose() {
    
  }
}
